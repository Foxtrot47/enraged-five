using System;
using System.Xml;

namespace rage.ToolLib
{
    public enum ResolveType
    {
        Auto,
        AcceptYours,
        AcceptTheirs,
        AcceptMerged,
    }

    public class BuildFile
    {
        public BuildFile(XmlNode node)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }

            if (node.Name != "InputFile" && node.Name != "OutputFile")
            {
                throw new ArgumentException("Expected a node named either \"InputFile\" or \"OutputFile\"");
            }

            if (node.Attributes != null)
            {
                switch (node.Name)
                {
                    case "InputFile":
                        {
                            Add = ParseBooleanAttribute("add", node.Attributes);
                            break;
                        }
                    case "OutputFile":
                        {
                            Add = ParseBooleanAttribute("add", node.Attributes, true);
                            break;
                        }
                }

                Checkout = ParseBooleanAttribute("checkout", node.Attributes);
                Lock = ParseBooleanAttribute("lock", node.Attributes, true);

                var resolveType = string.Empty;
                if (node.Attributes["resolve"] != null)
                {
                    resolveType = node.Attributes["resolve"].Value;
                }
                Resolve = Enum<ResolveType>.TryParse(resolveType, true, ResolveType.AcceptMerged);
            }
            else
            {
                SetDefaults(node);
            }
            File = node.InnerText;
        }

        public bool Add { get; private set; }

        public bool Checkout { get; private set; }

        public bool Lock { get; private set; }

        public ResolveType Resolve { get; private set; }

        public string File { get; set; }

        private void SetDefaults(XmlNode node)
        {
            switch (node.Name)
            {
                case "InputFile":
                    {
                        Add = false;
                        break;
                    }
                case "OutputFile":
                    {
                        Add = true;
                        break;
                    }
            }
            Checkout = false;
            Lock = true;
            Resolve = ResolveType.AcceptMerged;
        }

        private static bool ParseBooleanAttribute(string attributeName,
                                                  XmlAttributeCollection attributes,
                                                  bool defaultValue = false)
        {
            var attribute = attributes[attributeName];
            if (attribute != null)
            {
                bool val;
                if (bool.TryParse(attribute.Value, out val))
                {
                    return val;
                }
            }
            return defaultValue;
        }
    }
}