namespace rage.ToolLib.WavesFiles
{
    public interface IWavesFileCombiner
    {
        string Run(string packListFile, string destinationDirectory);
    }
}