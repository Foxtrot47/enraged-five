using System;
using System.IO;
using System.Text;

namespace rage.ToolLib.Logging
{
    public class FileLogWriter : ILogWriter
    {
        private readonly StreamWriter m_writer;

        private bool m_disposed;

        public FileLogWriter(string logFile)
        {
            if (string.IsNullOrEmpty(logFile))
            {
                throw new ArgumentNullException("logFile");
            }
            m_writer = new StreamWriter(logFile, false, Encoding.ASCII);
        }

        #region ILogWriter Members

        public void WriteLine(object content)
        {
            m_writer.WriteLine(content);
        }

        public void WriteLine(string format, params object[] args)
        {
            m_writer.WriteLine(format, args);
        }

        public void Dispose()
        {
            if (!m_disposed)
            {
                m_disposed = true;
                m_writer.Close();
            }
        }

        #endregion
    }
}