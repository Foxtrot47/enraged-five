﻿// -----------------------------------------------------------------------
// <copyright file="IWaveList.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.DataManager.WaveLists.Interfaces
{
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.Rave.DataManager.WaveLists.Enums;

    /// <summary>
    /// Wave list interface.
    /// </summary>
    public interface IWaveList
    {
        /// <summary>
        /// Gets the asset.
        /// </summary>
        IAsset Asset { get; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        WaveListTypes Type { get; }
    }
}
