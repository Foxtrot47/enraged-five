﻿// -----------------------------------------------------------------------
// <copyright file="IWaveListManager.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.DataManager.WaveLists.Interfaces
{
    /// <summary>
    /// Wave list manager interface.
    /// </summary>
    public interface IWaveListManager
    {
    }
}
