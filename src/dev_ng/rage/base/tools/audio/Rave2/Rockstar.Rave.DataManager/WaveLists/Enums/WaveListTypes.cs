﻿// -----------------------------------------------------------------------
// <copyright file="WaveListTypes.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.DataManager.WaveLists.Enums
{
    /// <summary>
    /// Wave list types.
    /// </summary>
    public enum WaveListTypes
    {
        /// <summary>
        /// Pending wave list.
        /// </summary>
        Pending,

        /// <summary>
        /// Built wave list.
        /// </summary>
        Built
    }
}
