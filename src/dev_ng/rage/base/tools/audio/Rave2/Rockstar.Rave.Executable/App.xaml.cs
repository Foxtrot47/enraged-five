﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Interaction logic for App.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.Rave.Executable
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// The bootstrapper.
        /// </summary>
        private readonly RaveBootstrapper bootstrapper = new RaveBootstrapper();

        /// <summary>
        /// Initiates application startup process.
        /// </summary>
        /// <param name="e">
        /// The startup event args.
        /// </param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            this.bootstrapper.Run();
        }

        /// <summary>
        /// Disposes of bootstrapper on exit.
        /// </summary>
        /// <param name="e">
        /// The exit event args.
        /// </param>
        protected override void OnExit(ExitEventArgs e)
        {
            this.bootstrapper.Container.Dispose();
            base.OnExit(e);
        }
    }
}
