﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RaveBootstrapper.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The rave bootstrapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.Rave.Executable
{
    using System.Windows;
    
    using Microsoft.Practices.Prism.UnityExtensions;
    using Microsoft.Practices.Unity;

    using Rockstar.Rave.Executable.Views;

    /// <summary>
    /// The rave bootstrapper.
    /// </summary>
    public class RaveBootstrapper : UnityBootstrapper
    {
        /// <summary>
        /// The shell.
        /// </summary>
        private ShellView shell;

        /// <summary>
        /// Create the shell.
        /// </summary>
        /// <returns>
        /// The initialized shell instance <see cref="DependencyObject"/>.
        /// </returns>
        protected override DependencyObject CreateShell()
        {
            this.shell = new ShellView();
            this.shell.Show();
            return this.shell;
        }

        /// <summary>
        /// Configure the bootstrapper container.
        /// </summary>
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            // TODO
        }

        /// <summary>
        /// Initialize the modules required by the application.
        /// </summary>
        protected override void InitializeModules()
        {
            base.InitializeModules();
            Container.BuildUp(shell);

            //var raveMainModuleInfo = new ModuleInfo(typeof(RaveConsoleModule).Name, typeof(RaveConsoleModule).ToString());
            //ModuleCatalog.AddModule(raveMainModuleInfo);
        }
    }
}
