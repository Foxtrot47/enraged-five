// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Builder.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The audio data builder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace audioDataBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using System.Xml.Linq;

    using rage;
    using rage.Compiler;
    using rage.Reflection;
    using rage.ToolLib.Logging;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The builder.
    /// </summary>
    public class Builder
    {
        #region Fields

        /// <summary>
        /// The wave build timeout (3 hours).
        /// </summary>
        private const int WaveBuildTimeout = 1000 * 60 * 60 * 3;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager m_assetMgr;

        /// <summary>
        /// The_build metadata flag.
        /// </summary>
        private readonly bool m_buildMetadata;

        /// <summary>
        /// The log.
        /// </summary>
        private readonly ILog m_log;

        /// <summary>
        /// The metadata compiler path.
        /// </summary>
        private readonly string m_metadataCompilerPath;

        /// <summary>
        /// The perforce connection settings.
        /// </summary>
        private readonly PerforceConnectionSettings m_perforceConnectionSettings;

        /// <summary>
        /// The platform.
        /// </summary>
        private readonly string m_platform;

        /// <summary>
        /// The project settings.
        /// </summary>
        private readonly audProjectSettings m_projectSettings;

        /// <summary>
        /// The project settings path.
        /// </summary>
        private readonly string m_projectSettingsPath;

        /// <summary>
        /// The sync changelist number.
        /// </summary>
        private readonly string m_syncChangelistNumber;

        /// <summary>
        /// The temp path.
        /// </summary>
        private readonly string m_tempPath;

        /// <summary>
        /// The working path.
        /// </summary>
        private readonly string m_workingPath;

        /// <summary>
        /// The wave build path.
        /// </summary>
        private readonly string m_waveBuildPath;

        /// <summary>
        /// The wave pack name.
        /// </summary>
        private readonly string m_wavePackName;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Builder"/> class.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="assetMgr">
        /// The asset manager.
        /// </param>
        /// <param name="projectSettings">
        /// The project settings.
        /// </param>
        /// <param name="perforceConnectionSettings">
        /// The perforce connection settings.
        /// </param>
        /// <param name="projectSettingsPath">
        /// The project settings path.
        /// </param>
        /// <param name="syncChangelistNumber">
        /// The sync change list number.
        /// </param>
        /// <param name="wavePackName">
        /// The wave pack name.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="buildMetadata">
        /// The build metadata.
        /// </param>
        /// <param name="tempPath">
        /// The temp path.
        /// </param>
        /// <param name="workingPath">
        /// The working path.
        /// </param>
        public Builder(
            ILog log, 
            IAssetManager assetMgr, 
            audProjectSettings projectSettings, 
            PerforceConnectionSettings perforceConnectionSettings, 
            string projectSettingsPath, 
            string syncChangelistNumber, 
            string wavePackName, 
            string platform, 
            bool buildMetadata, 
            string tempPath,
            string workingPath)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }

            if (assetMgr == null)
            {
                throw new ArgumentNullException("assetMgr");
            }

            if (projectSettings == null)
            {
                throw new ArgumentNullException("projectSettings");
            }

            if (perforceConnectionSettings == null)
            {
                throw new ArgumentNullException("perforceConnectionSettings");
            }

            if (string.IsNullOrEmpty(projectSettingsPath))
            {
                throw new ArgumentException("projectSettingsPath");
            }

            if (string.IsNullOrEmpty(syncChangelistNumber))
            {
                throw new ArgumentException("syncChangelistNumber");
            }

            this.m_log = log;
            this.m_assetMgr = assetMgr;
            this.m_projectSettings = projectSettings;
            this.m_perforceConnectionSettings = perforceConnectionSettings;
            this.m_projectSettingsPath = projectSettingsPath;
            this.m_syncChangelistNumber = syncChangelistNumber;
            this.m_wavePackName = wavePackName;
            this.m_platform = platform;
            this.m_buildMetadata = buildMetadata;
            this.m_tempPath = tempPath;
            this.m_workingPath = workingPath;

            SetJitPaths(assetMgr, projectSettings);

            this.m_waveBuildPath = assetMgr.GetLocalPath(projectSettings.GetBuildPath());

            if (buildMetadata)
            {
                this.m_metadataCompilerPath = assetMgr.GetLocalPath(projectSettings.GetMetadataCompilerPath());
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the JIT paths.
        /// </summary>
        public static string[] JitPaths { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Run the builder.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Run()
        {
            var success = true;

            // Try building and submitting waves per platform
            foreach (PlatformSetting platformSetting in this.m_projectSettings.GetPlatformSettings())
            {
                // If "build enabled" flag not set and building for all platforms, skip current platform
                // (Always build platform if explicitly specified in args regardless of "build enabled" flag
                if (null == this.m_platform && !platformSetting.BuildEnabled)
                {
                    continue;
                }

                // Either run for all platforms or for specified platform (if set)
                if (null == this.m_platform || platformSetting.PlatformTag.ToLower() == this.m_platform.ToLower())
                {
                    this.m_log.Write("Building data for " + platformSetting.PlatformTag);
                    this.m_projectSettings.SetCurrentPlatform(platformSetting);
                    var innerSuccess = this.RunForPlatform(platformSetting.Name, platformSetting.PlatformTag);

                    // Want to keep track of any failures, but also keep processing for each platform
                    if (success)
                    {
                        success = innerSuccess;
                    }
                }
            }

            return success;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The set JIT paths.
        /// </summary>
        /// <param name="assetMgr">
        /// The asset manager.
        /// </param>
        /// <param name="projectSettings">
        /// The project settings.
        /// </param>
        private static void SetJitPaths(IAssetManager assetMgr, audProjectSettings projectSettings)
        {
            var jitPaths = projectSettings.MetadataCompilerJitPaths;
            if (jitPaths != null)
            {
                var count = jitPaths.Length;
                JitPaths = new string[count];
                for (var i = 0; i < count; ++i)
                {
                    JitPaths[i] = assetMgr.GetLocalPath(jitPaths[i]);
                }
            }
        }

        /// <summary>
        /// Add metadata files to perforce.
        /// </summary>
        /// <param name="cl">
        /// The change list.
        /// </param>
        /// <param name="filesToAdd">
        /// The files to add.
        /// </param>
        private void AddMetadataFilesToPerforce(IChangeList cl, IEnumerable<string> filesToAdd)
        {
            foreach (var fileToAdd in filesToAdd)
            {
                if (File.Exists(fileToAdd) && !this.m_assetMgr.IsMarkedForAdd(fileToAdd))
                {
                    cl.MarkAssetForAdd(fileToAdd, "binary");
                }
            }
        }

        /// <summary>
        /// Build the waves.
        /// </summary>
        /// <param name="cl">
        /// The change list.
        /// </param>
        /// <param name="container">
        /// The container.
        /// </param>
        /// <param name="pack">
        /// The pack.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool BuildWaves(IChangeList cl, XContainer container, string pack = null)
        {
            var platformSetting = this.m_projectSettings.GetCurrentPlatform();
            var incrediBuildLogFile = string.Format(
                "{0}{1}_{2}_{3}",
                Path.GetTempPath(),
                platformSetting.PlatformTag,
                pack, 
                "IncrediBuildLog.txt");

            Console.WriteLine("Logging to {0}", incrediBuildLogFile);

            var process = new Process
            {
                StartInfo =
                    new ProcessStartInfo(this.m_waveBuildPath)
                        {
                            CreateNoWindow = true, 
                            UseShellExecute = false, 
                            RedirectStandardOutput = true, 
                            RedirectStandardError = true, 
                            WorkingDirectory = Path.GetDirectoryName(this.m_waveBuildPath),
                            Arguments =
                                this
                                .GetWaveBuildCommandLine(
                                    platformSetting, 
                                    cl.NumberAsString, 
                                    incrediBuildLogFile,
                                    pack)
                        }
            };

            var output = new StringBuilder();
            var error = new StringBuilder();

            using (var outputWaitHandle = new AutoResetEvent(false))
            using (var errorWaitHandle = new AutoResetEvent(false))
            {
                process.OutputDataReceived += (sender, e) =>
                {
                    if (e.Data == null)
                    {
                        outputWaitHandle.Set();
                    }
                    else
                    {
                        output.AppendLine(e.Data);
                    }
                };
                process.ErrorDataReceived += (sender, e) =>
                {
                    if (e.Data == null)
                    {
                        errorWaitHandle.Set();
                    }
                    else
                    {
                        error.AppendLine(e.Data);
                    }
                };

                process.Start();

                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                if (process.WaitForExit(WaveBuildTimeout) &&
                    outputWaitHandle.WaitOne(WaveBuildTimeout) &&
                    errorWaitHandle.WaitOne(WaveBuildTimeout))
                {
                    // Process completed successfully.
                }
                else
                {
                    Console.Error.WriteLine("Wave build process timed out");
                    return false;
                }
            }

            var consoleOutputBuilder = new StringBuilder();
            consoleOutputBuilder.Append(output);
            consoleOutputBuilder.Append(Environment.NewLine);
            consoleOutputBuilder.Append(Environment.NewLine);
            consoleOutputBuilder.Append(error);

            string incrediBuildOutput = "No incrediBuildOutput";
            if (File.Exists(incrediBuildLogFile))
            {
                using (var reader = File.OpenText(incrediBuildLogFile))
                {
                    incrediBuildOutput = reader.ReadToEnd();
                }
            }
            container.Add(
                    new XElement(
                        "WaveBuild",
                        new XElement("ConsoleOutput", consoleOutputBuilder.ToString()),
                        new XElement("IncrediBuildOutput", incrediBuildOutput)));

            return process.ExitCode == 0;
        }

        /// <summary>
        /// Build the metadata.
        /// </summary>
        /// <param name="cl">
        /// The change list.
        /// </param>
        /// <param name="container">
        /// The container.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool BuildMetadata(IChangeList cl, XContainer container)
        {
            var workingPath = this.m_workingPath;
            if (string.IsNullOrEmpty(workingPath))
            {
                workingPath = this.m_assetMgr.GetLocalPath(string.Empty);
                if (string.IsNullOrWhiteSpace(workingPath))
                {
                    workingPath = this.m_projectSettings.GetBackupWorkingPath();
                }
            }

            foreach (var metadataType in this.m_projectSettings.GetMetadataTypes())
            {
                var filesToAdd = this.CheckoutMetadataFiles(workingPath, metadataType, cl);

                if (null == filesToAdd)
                {
                    return false;
                }

                // Display files to add for debug info
                var toAdd = filesToAdd as IList<string> ?? filesToAdd.ToList();
                if (toAdd.Any())
                {
                    this.m_log.Write("Files to add:");
                    foreach (var file in toAdd)
                    {
                        this.m_log.WriteFormatted("\t{0}", file);
                    }
                }
                else
                {
                    this.m_log.Write("No new files to add");
                }

                if (!this.BuildMetadataHelper(workingPath, container, metadataType, "dev"))
                {
                    return false;
                }

                if (!this.BuildMetadataHelper(workingPath, container, metadataType, "release"))
                {
                    return false;
                }

                this.AddMetadataFilesToPerforce(cl, toAdd);
            }

            return true;
        }

        /// <summary>
        /// The build metadata helper.
        /// </summary>
        /// <param name="workingPath">
        /// The working path.
        /// </param>
        /// <param name="container">
        /// The container.
        /// </param>
        /// <param name="metadataType">
        /// The metadata type.
        /// </param>
        /// <param name="runtimeMode">
        /// The runtime mode.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool BuildMetadataHelper(string workingPath, XContainer container, audMetadataType metadataType, string runtimeMode)
        {
            var args = this.GetMetadataCompilerCommandLine(workingPath, metadataType, runtimeMode);

            Console.WriteLine("Running metadata compiler with args " + args);

            var process = new Process
            {
                StartInfo =
                    new ProcessStartInfo(this.m_metadataCompilerPath)
                    {
                        RedirectStandardOutput
                            = true,
                        UseShellExecute
                            = false,
                        CreateNoWindow =
                            true,
                        Arguments = args
                    }
            };

            process.Start();
            var output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            var index = output.IndexOf("<MetadataCompilerOutput>");
            output = output.Substring(index);

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(output);

            var tempFile = Path.GetTempPath() + "MetadataCompilerOutput.xml";
            xmlDoc.Save(tempFile);

            var doc = XDocument.Load(tempFile);
            container.Add(doc.Root);

            if (doc.Descendants("Error").Any() || doc.Descendants("Exception").Any())
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checkout metadata files.
        /// </summary>
        /// <param name="workingPath">
        /// The working path.
        /// </param>
        /// <param name="metadataType">
        /// The metadata type.
        /// </param>
        /// <param name="cl">
        /// The change list.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="IEnumerable"/>.
        /// </returns>
        private IEnumerable<string> CheckoutMetadataFiles(
            string workingPath, audMetadataType metadataType, IChangeList cl)
        {
            var filesToAdd = new List<string>();
            var nullLog = new TextLog(new NullLogWriter(), new ContextStack());
            var workingDirectory =
                string.Concat(workingPath, this.m_projectSettings.GetSoundXmlPath())
                      .Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);

            var compiler = new MetaDataCompiler(
                nullLog, 
                workingPath, 
                workingDirectory, 
                this.m_projectSettings, 
                new Reflector(nullLog, JitPaths), 
                new ObjectSizeTracker(), 
                new StringTable(), 
                new CompiledObjectLookup(), 
                CompilationMode.TransformAndCompile);
            compiler.LoadTypeDefinitions(metadataType.ObjectDefinitions);

            foreach (var metadataFile in this.m_projectSettings.GetMetadataSettings())
            {
                if (string.Compare(metadataType.Type, metadataFile.Type, true) == 0)
                {
                    var filePath =
                        string.Concat(
                            this.m_assetMgr.GetLocalPath(this.m_projectSettings.ResolvePath(metadataFile.OutputFile)), 
                            compiler.Version);

                    if (this.m_assetMgr.ExistsAsAsset(filePath))
                    {
                        // Checkout/mark for add the .DAT?? file
                        if (this.m_assetMgr.ExistsAsAsset(filePath))
                        {
                            if (!this.m_assetMgr.IsCheckedOut(filePath))
                            {
                                if (null != cl.CheckoutAsset(filePath, true))
                                {
                                    this.m_log.Write("Checked out asset: " + filePath);
                                }
                                else
                                {
                                    this.m_log.Error("Failed to check out asset: " + filePath);
                                    return null;
                                }
                            }
                        }
                        else
                        {
                            filesToAdd.Add(filePath);
                        }
                    }
                    else
                    {
                        filesToAdd.Add(filePath);
                    }

                    // Checkout/mark for add the .DAT??.NAMETABLE file
                    var nameTablePath = string.Concat(filePath, ".nametable");
                    if (this.m_assetMgr.ExistsAsAsset(nameTablePath))
                    {
                        if (!this.m_assetMgr.IsCheckedOut(nameTablePath))
                        {
                            cl.CheckoutAsset(nameTablePath, false);
                        }
                    }
                    else
                    {
                        filesToAdd.Add(nameTablePath);
                    }

                    // Checkout/add .dat??.rel file if required
                    var releasePath = filePath + ".rel";
                    if (this.m_assetMgr.ExistsAsAsset(releasePath))
                    {
                        if (!this.m_assetMgr.IsCheckedOut(releasePath))
                        {
                            cl.CheckoutAsset(releasePath, false);
                        }
                    }
                    else
                    {
                        filesToAdd.Add(releasePath);
                    }
                }
            }

            return filesToAdd;
        }

        /// <summary>
        /// Get the wave build command line.
        /// </summary>
        /// <param name="platformSetting">
        /// The platform setting.
        /// </param>
        /// <param name="changelistNumber">
        /// The change list number.
        /// </param>
        /// <param name="incrediBuildLogFile">
        /// The IncrediBuild log file.
        /// </param>
        /// <param name="pack">
        /// The pack.
        /// </param>
        /// <returns>
        /// The wave build command line <see cref="string"/>.
        /// </returns>
        private string GetWaveBuildCommandLine(
            PlatformSetting platformSetting, string changelistNumber, string incrediBuildLogFile, string pack = null)
        {
            var commandLineBuilder = new StringBuilder();
            commandLineBuilder.Append("-ASSETSERVER ");
            commandLineBuilder.Append(this.m_perforceConnectionSettings.Host);
            commandLineBuilder.Append(" -ASSETPROJECT ");
            commandLineBuilder.Append(this.m_perforceConnectionSettings.Client);
            commandLineBuilder.Append(" -ASSETUSERNAME ");
            commandLineBuilder.Append(this.m_perforceConnectionSettings.Username);
            if (!string.IsNullOrEmpty(this.m_perforceConnectionSettings.Password))
            {
                commandLineBuilder.Append(" -ASSETPASSWORD ");
                commandLineBuilder.Append(this.m_perforceConnectionSettings.Password);
            }

            commandLineBuilder.Append(" -PROJECTSETTINGS ");
            commandLineBuilder.Append(this.m_assetMgr.GetLocalPath(this.m_projectSettingsPath));
            commandLineBuilder.Append(" -PLATFORM ");
            commandLineBuilder.Append(platformSetting.PlatformTag);
            commandLineBuilder.Append(" -CHANGE ");
            commandLineBuilder.Append(changelistNumber);
            commandLineBuilder.Append(" -DEPOTROOT ");
            commandLineBuilder.Append(this.m_perforceConnectionSettings.DepotRoot);
            commandLineBuilder.Append(string.Format(" -ADDITIONAL_INCREDIBUILD_ARGS \"/Log={0}\"", incrediBuildLogFile));
            commandLineBuilder.Append(" -SYNCNUMBER ");
            commandLineBuilder.Append(this.m_syncChangelistNumber);

            if (!string.IsNullOrEmpty(pack))
            {
                commandLineBuilder.Append(" -PACK ");
                commandLineBuilder.Append(pack);
            }

            if (!string.IsNullOrEmpty(this.m_tempPath))
            {
                commandLineBuilder.Append(" -TEMPPATH ");
                commandLineBuilder.Append(this.m_tempPath);
            }

            return commandLineBuilder.ToString();
        }

        /// <summary>
        /// Get the metadata compiler command line.
        /// </summary>
        /// <param name="workingPath">
        /// The working path.
        /// </param>
        /// <param name="metadataType">
        /// The metadata type.
        /// </param>
        /// <param name="runtimeMode">
        /// The runtime mode.
        /// </param>
        /// <returns>
        /// The metadata command line <see cref="string"/>.
        /// </returns>
        private string GetMetadataCompilerCommandLine(string workingPath, audMetadataType metadataType, string runtimeMode)
        {
            var commandLineBuilder = new StringBuilder();
            commandLineBuilder.Append(" -workingpath ");
            commandLineBuilder.Append(workingPath);
            commandLineBuilder.Append(" -project ");
            commandLineBuilder.Append(this.m_projectSettingsPath);
            commandLineBuilder.Append(" -metadata ");
            commandLineBuilder.Append(metadataType.Type);
            commandLineBuilder.Append(" -platform ");
            commandLineBuilder.Append("{Platform}");
            commandLineBuilder.Append(" -logformat XML");
            commandLineBuilder.Append(" -runtimemode ");
            commandLineBuilder.Append(runtimeMode);

            if (!string.IsNullOrEmpty(this.m_syncChangelistNumber))
            {
                commandLineBuilder.Append(" -cl ");
                commandLineBuilder.Append(this.m_syncChangelistNumber);
            }

            return this.m_projectSettings.ResolvePath(commandLineBuilder.ToString());
        }

        /// <summary>
        /// Run build process for platform.
        /// </summary>
        /// <param name="platformName">
        /// The platform name.
        /// </param>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool RunForPlatform(string platformName, string platformTag)
        {
            return this.RunWaveBuild(platformTag) && 
                this.RunMetadataBuild(platformName, platformTag);
        }

        /// <summary>
        /// Run the wave build.
        /// </summary>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool RunWaveBuild(string platformTag)
        {
            if (!string.IsNullOrEmpty(this.m_wavePackName))
            {
                return this.RunWaveBuildForPack(platformTag, this.m_wavePackName);
            }

            var buildPath = this.m_projectSettings.GetCurrentPlatform().BuildInfo;
            var packListPath = Path.Combine(buildPath, "BuiltWaves", "BuiltWaves_PACK_LIST.xml");

            var packListDoc = XDocument.Load(packListPath);

            var success = true;

            // Run wave build for each pack in pack list file
            foreach (var packFileElem in packListDoc.Descendants("PackFile"))
            {
                var packFilePath = Path.Combine(buildPath, "BuiltWaves", packFileElem.Value);

                var packFileDoc = XDocument.Load(packFilePath);
                var packElem = packFileDoc.Descendants("Pack").First();
                var packName = packElem.Attribute("name").Value;

                // Check if there are pending waves for this pack
                var pendingWavesPath = Path.Combine(buildPath, "PendingWaves", packName + ".xml");
                if (!File.Exists(pendingWavesPath))
                {
                    continue;
                }

                if (!this.RunWaveBuildForPack(platformTag, packName))
                {
                    success = false;
                }
            }

            return success;
        }

        /// <summary>
        /// Run the wave build for a particular pack.
        /// </summary>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <param name="pack">
        /// The pack.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool RunWaveBuildForPack(string platformTag, string pack)
        {
            // Create new change list for current platform and pack
            var changeList =
                this.m_assetMgr.CreateChangeList(
                    string.Format(
                        "[{0}] Wave Build - {1} ({2}) @ CL {3}",
                        this.m_projectSettings.GetProjectName(),
                        platformTag,
                        pack,
                        this.m_syncChangelistNumber));
            try
            {
                var outputElement = new XElement("Output");
                outputElement.SetAttributeValue("platform", platformTag.ToUpper());

                // Build waves
                if (!this.BuildWaves(changeList, outputElement, pack))
                {
                    changeList.Revert(true);
                    outputElement.SetAttributeValue("waves", "failed");
                    this.m_log.Write(outputElement);
                    return false;
                }

                outputElement.SetAttributeValue("waves", "succeeded");
                this.m_log.Write(outputElement);
            }
            catch (Exception ex)
            {
                this.m_log.Exception(ex);
                changeList.Revert(true);
                return false;
            }

            return this.RevertUnchangedAndSubmit(changeList);
        }

        /// <summary>
        /// Run the metadata build.
        /// </summary>
        /// <param name="platformName">
        /// The platform name.
        /// </param>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool RunMetadataBuild(string platformName, string platformTag)
        {
            // Create new change list for current platform
            var changeList =
                this.m_assetMgr.CreateChangeList(
                    string.Format(
                        "[{0}] Metadata Build - {1} @ CL {2}",
                        this.m_projectSettings.GetProjectName(),
                        platformName,
                        this.m_syncChangelistNumber));

            try
            {
                var outputElement = new XElement("Output");
                outputElement.SetAttributeValue("platform", platformTag.ToUpper());

                // Build metadata
                if (this.m_buildMetadata && !this.BuildMetadata(changeList, outputElement))
                {
                    changeList.Revert(true);
                    outputElement.SetAttributeValue("metadata", "failed");
                    this.m_log.Write(outputElement);
                    return false;
                }

                outputElement.SetAttributeValue("metadata", "succeeded");
                this.m_log.Write(outputElement);
            }
            catch (Exception ex)
            {
                this.m_log.Exception(ex);
                changeList.Revert(true);
                return false;
            }

            return this.RevertUnchangedAndSubmit(changeList);
        }

        /// <summary>
        /// Revert unchanged and submit change list.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool RevertUnchangedAndSubmit(IChangeList changeList)
        {
            // Revert any unchanged files before submitting
            try
            {
                if (!changeList.RevertUnchanged())
                {
                    this.m_log.Warning("Failed to revert unchanged in change list" + changeList.NumberAsString);
                    return false;
                }
            }
            catch (Exception ex)
            {
                this.m_log.Warning("Failed to revert unchanged in change list" + changeList.NumberAsString, ex.Message);
            }

            // Submit change list (or delete if empty)
            return changeList.Assets.Any() ? changeList.Submit() : changeList.Delete();
        }

        #endregion
    }
}