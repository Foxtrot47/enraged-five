// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Reporter.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The reporter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace audioDataBuilder
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    using rage;
    using rage.ToolLib.Logging;
    using rage.ToolLib.Mail;

    /// <summary>
    /// The reporter.
    /// </summary>
    public class Reporter
    {
        #region Fields

        /// <summary>
        /// The log.
        /// </summary>
        private readonly ILog m_log;

        /// <summary>
        /// The log file path.
        /// </summary>
        private readonly string m_logFilePath;

        /// <summary>
        /// The mail sender.
        /// </summary>
        private readonly IMailSender m_mailSender;

        /// <summary>
        /// The project settings.
        /// </summary>
        private readonly audProjectSettings m_projectSettings;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Reporter"/> class.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="logFilePath">
        /// The log file path.
        /// </param>
        /// <param name="mailSender">
        /// The mail sender.
        /// </param>
        /// <param name="projectSettings">
        /// The project settings.
        /// </param>
        public Reporter(ILog log, string logFilePath, IMailSender mailSender, audProjectSettings projectSettings)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }

            if (string.IsNullOrEmpty(logFilePath))
            {
                throw new ArgumentException("logFilePath");
            }

            if (!File.Exists(logFilePath))
            {
                throw new ArgumentException(string.Format("File: {0} does not exist!", logFilePath));
            }

            if (mailSender == null)
            {
                throw new ArgumentNullException("mailSender");
            }

            if (projectSettings == null)
            {
                throw new ArgumentNullException("projectSettings");
            }

            this.m_log = log;
            this.m_logFilePath = logFilePath;
            this.m_mailSender = mailSender;
            this.m_projectSettings = projectSettings;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Run the report.
        /// </summary>
        public void Run()
        {
            try
            {
                var mailMessage = this.CreateMailMessage();
                var doc = XDocument.Load(this.m_logFilePath);

                var messageTextBuilder = new StringBuilder();
                foreach (var outputElement in doc.Root.Descendants("Output"))
                {
                    var platform = outputElement.Attribute("platform").Value;

                    var wavesAttribute = outputElement.Attribute("waves");
                    if (null != wavesAttribute && wavesAttribute.Value != "succeeded")
                    {
                        ProcessFailedWaveBuild(platform, mailMessage, messageTextBuilder, outputElement);
                    }

                    var metadataAttribute = outputElement.Attribute("metadata");
                    if (metadataAttribute != null && metadataAttribute.Value != "succeeded")
                    {
                        ProcessFailedMetadataBuild(platform, messageTextBuilder, outputElement);
                    }
                }

                var messageBody = messageTextBuilder.ToString();
                if (messageBody.Length > 0)
                {
                    mailMessage.Body = messageBody;
                    this.m_mailSender.Send(mailMessage);
                }
            }
            catch (Exception ex)
            {
                this.m_log.Exception(ex);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Attach wave log file to message.
        /// </summary>
        /// <param name="mailMessage">
        /// The mail message.
        /// </param>
        /// <param name="waveLogData">
        /// The wave log data.
        /// </param>
        /// <param name="waveLogFileName">
        /// The wave log file name.
        /// </param>
        private static void AttachWaveLogFileToMessage(
            MailMessage mailMessage, string waveLogData, string waveLogFileName)
        {
            var tempWaveLogPath = Path.GetTempPath() + waveLogFileName;
            using (var streamWriter = new StreamWriter(tempWaveLogPath))
            {
                streamWriter.Write(waveLogData);
            }

            mailMessage.Attachments.Add(new Attachment(tempWaveLogPath, MediaTypeNames.Text.Plain));
        }

        /// <summary>
        /// Process failed metadata build.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="messageTextBuilder">
        /// The message text builder.
        /// </param>
        /// <param name="outputElement">
        /// The output element.
        /// </param>
        private static void ProcessFailedMetadataBuild(
            string platform, StringBuilder messageTextBuilder, XContainer outputElement)
        {
            messageTextBuilder.Append(string.Format("{0} - metadata build failed!", platform));
            messageTextBuilder.Append(Environment.NewLine);
            messageTextBuilder.Append(Environment.NewLine);

            var errorAndExceptionElements =
                from metadataCompilerOutputElement in outputElement.Descendants("MetadataCompilerOutput")
                from subElement in metadataCompilerOutputElement.Descendants()
                where subElement.Name == "Error" || subElement.Name == "Exception"
                select subElement;
            foreach (var element in errorAndExceptionElements)
            {
                var context = element.Attribute("context").Value.Replace("\\[", "[");
                var contextElements = Regex.Matches(context, "\\[.+?\\]");
                foreach (var contextElement in contextElements)
                {
                    messageTextBuilder.Append(contextElement);
                    messageTextBuilder.Append(Environment.NewLine);
                }

                messageTextBuilder.Append(element.Value);
                messageTextBuilder.Append(Environment.NewLine);
                messageTextBuilder.Append(Environment.NewLine);
            }
        }

        /// <summary>
        /// Process failed wave build.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="mailMessage">
        /// The mail message.
        /// </param>
        /// <param name="messageTextBuilder">
        /// The message text builder.
        /// </param>
        /// <param name="outputElement">
        /// The output element.
        /// </param>
        private static void ProcessFailedWaveBuild(
            string platform, MailMessage mailMessage, StringBuilder messageTextBuilder, XContainer outputElement)
        {
            var waveLogFileName = string.Format("{0}-WaveBuildLog.txt", platform);
            messageTextBuilder.Append(
                string.Format(
                    "{0} - wave build failed! See the attached file \"{1}\" for the complete log.", 
                    platform, 
                    waveLogFileName));
            messageTextBuilder.Append(Environment.NewLine);
            messageTextBuilder.Append(Environment.NewLine);

            var builder = new StringBuilder();
            if (!(outputElement.Descendants("WaveBuild").Count() > 0))
            {
                builder.Append("No messages found");
            }
            else 
            {
                var waveBuildElement = outputElement.Descendants("WaveBuild").First();
                if (waveBuildElement.Descendants("ConsoleOutput").Count() > 0)
                {
                    string consoleLogData = waveBuildElement.Descendants("ConsoleOutput").First().Value;
                    builder.Append(consoleLogData);

                    var invokingIncrediBuildIndex = consoleLogData.IndexOf("Invoking Incredibuild...");
                    messageTextBuilder.Append(
                        invokingIncrediBuildIndex >= 0
                            ? consoleLogData.Substring(invokingIncrediBuildIndex)
                            : consoleLogData.Length > 500
                                ? consoleLogData.Substring(consoleLogData.Length - 500)
                                :consoleLogData);
                }
                if(waveBuildElement.Descendants("IncrediBuildOutput").Count() > 0)
                {
                    string incrediBuildLogData = waveBuildElement.Descendants("IncrediBuildOutput").First().Value;

                    builder.Append(Environment.NewLine);
                    builder.Append(Environment.NewLine);

                    builder.Append(incrediBuildLogData);

                    messageTextBuilder.Append(Environment.NewLine);
                    messageTextBuilder.Append(Environment.NewLine);

                    var pipeIndex = incrediBuildLogData.LastIndexOf('|');
                    messageTextBuilder.Append(
                        pipeIndex >= 0
                            ? incrediBuildLogData.Substring(pipeIndex)
                            : incrediBuildLogData.Length > 1000
                                ? incrediBuildLogData.Substring(incrediBuildLogData.Length - 1000)
                                : incrediBuildLogData);
                }
            }
            

            AttachWaveLogFileToMessage(mailMessage, builder.ToString(), waveLogFileName);   
        }

        /// <summary>
        /// Create mail message.
        /// </summary>
        /// <returns>
        /// The mail message <see cref="MailMessage"/>.
        /// </returns>
        private MailMessage CreateMailMessage()
        {
            var mailSettings = this.m_projectSettings.AudioDataBuilderSettings.MailSettings;
            var mailMessage = new MailMessage
                                  {
                                      Priority = MailPriority.High, 
                                      From = new MailAddress(mailSettings.FromAddress), 
                                      Subject =
                                          string.Format(
                                              "{0} - Audio Data Build Failed", 
                                              this.m_projectSettings.GetProjectName()), 
                                  };
            foreach (var address in mailSettings.Addresses)
            {
                mailMessage.To.Add(new MailAddress(address));
            }

            return mailMessage;
        }

        #endregion
    }
}