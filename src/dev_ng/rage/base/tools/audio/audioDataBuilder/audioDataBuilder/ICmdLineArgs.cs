// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICmdLineArgs.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The CmdLineArgs interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace audioDataBuilder
{
    /// <summary>
    /// The CmdLineArgs interface.
    /// </summary>
    public interface ICmdLineArgs
    {
        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether help requested.
        /// </summary>
        bool HelpRequested { get; }

        /// <summary>
        /// Gets the pack name.
        /// </summary>
        string PackName { get; }

        /// <summary>
        /// Gets the perforce client.
        /// </summary>
        string PerforceClient { get; }

        /// <summary>
        /// Gets the perforce depot root.
        /// </summary>
        string PerforceDepotRoot { get; }

        /// <summary>
        /// Gets the perforce host.
        /// </summary>
        string PerforceHost { get; }

        /// <summary>
        /// Gets the perforce password.
        /// </summary>
        string PerforcePassword { get; }

        /// <summary>
        /// Gets the perforce username.
        /// </summary>
        string PerforceUsername { get; }

        /// <summary>
        /// Gets the platform.
        /// </summary>
        string Platform { get; }

        /// <summary>
        /// Gets the project settings.
        /// </summary>
        string ProjectSettings { get; }

        /// <summary>
        /// Gets the working path.
        /// </summary>
        string WorkingPath { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Parse the command line args.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool Parse(string[] args);

        #endregion
    }
}