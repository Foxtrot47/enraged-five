// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdLineArgs.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The cmd line args.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace audioDataBuilder
{
    using System;
    using System.Text;

    using rage.ToolLib.CmdLine;
    using rage.ToolLib.Logging;

    /// <summary>
    /// The cmd line args.
    /// </summary>
    public class CmdLineArgs : ICmdLineArgs
    {
        #region Constants

        /// <summary>
        /// The buildmetadata string.
        /// </summary>
        private const string BUILDMETADATA = "buildmetadata";

        /// <summary>
        /// The change list number string.
        /// </summary>
        private const string CHANGE_LIST_NUMBER = "changelistnumber";

        /// <summary>
        /// The working path.
        /// </summary>
        private const string WORKING_PATH = "workingpath";

        /// <summary>
        /// The help string.
        /// </summary>
        private const string HELP = "help";

        /// <summary>
        /// The missing P4 client string.
        /// </summary>
        private const string MISSING_P4_CLIENT = "Missing \"-p4client\" command line argument";

        /// <summary>
        /// The missing P4 depo t_ root string.
        /// </summary>
        private const string MISSING_P4_DEPOT_ROOT = "Missing \"-p4depotroot\" command line argument";

        /// <summary>
        /// The missing P4 host string.
        /// </summary>
        private const string MISSING_P4_HOST = "Missing \"-p4host\" command line argument";

        /// <summary>
        /// The missing P4 user string.
        /// </summary>
        private const string MISSING_P4_USER = "Missing \"-p4user\" command line argument";

        /// <summary>
        /// The missing project string.
        /// </summary>
        private const string MISSING_PROJECT = "Missing \"-project\" command line argument.";

        /// <summary>
        /// The P4 client string.
        /// </summary>
        private const string P4_CLIENT = "p4client";

        /// <summary>
        /// The P4 depot root string.
        /// </summary>
        private const string P4_DEPOT_ROOT = "p4depotroot";

        /// <summary>
        /// The P4 host string.
        /// </summary>
        private const string P4_HOST = "p4host";

        /// <summary>
        /// The P4 password string.
        /// </summary>
        private const string P4_PASSWD = "p4passwd";

        /// <summary>
        /// The P4 user string.
        /// </summary>
        private const string P4_USER = "p4user";

        /// <summary>
        /// The pack string.
        /// </summary>
        private const string PACK = "pack";

        /// <summary>
        /// The platform string.
        /// </summary>
        private const string PLATFORM = "platform";

        /// <summary>
        /// The project string.
        /// </summary>
        private const string PROJECT = "project";

        /// <summary>
        /// The question mark string.
        /// </summary>
        private const string QUESTION_MARK = "?";

        /// <summary>
        /// The temp path string.
        /// </summary>
        private const string TEMPPATH = "temppath";

        #endregion

        #region Fields

        /// <summary>
        /// The log.
        /// </summary>
        private readonly ILog m_log;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CmdLineArgs"/> class.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        public CmdLineArgs(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }

            this.m_log = log;
            this.PerforcePassword = string.Empty;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether build metadata.
        /// </summary>
        public bool BuildMetadata { get; private set; }

        /// <summary>
        /// Gets the change list number.
        /// </summary>
        public string ChangeListNumber { get; private set; }

        /// <summary>
        /// Gets a value indicating whether help requested.
        /// </summary>
        public bool HelpRequested { get; private set; }

        /// <summary>
        /// Gets the pack name.
        /// </summary>
        public string PackName { get; private set; }

        /// <summary>
        /// Gets the perforce client.
        /// </summary>
        public string PerforceClient { get; private set; }

        /// <summary>
        /// Gets the perforce depot root.
        /// </summary>
        public string PerforceDepotRoot { get; private set; }

        /// <summary>
        /// Gets the perforce host.
        /// </summary>
        public string PerforceHost { get; private set; }

        /// <summary>
        /// Gets the perforce password.
        /// </summary>
        public string PerforcePassword { get; private set; }

        /// <summary>
        /// Gets the perforce username.
        /// </summary>
        public string PerforceUsername { get; private set; }

        /// <summary>
        /// Gets the platform.
        /// </summary>
        public string Platform { get; private set; }

        /// <summary>
        /// Gets the project settings.
        /// </summary>
        public string ProjectSettings { get; private set; }

        /// <summary>
        /// Gets the working path.
        /// </summary>
        public string WorkingPath { get; private set; }

        /// <summary>
        /// Gets the temp path.
        /// </summary>
        public string TempPath { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Parse the command line args.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Parse(string[] args)
        {
            var cmdLineParser = new CmdLineParser(args);
            try
            {
                this.HelpRequested = cmdLineParser[HELP] != null || cmdLineParser[QUESTION_MARK] != null;
                if (this.HelpRequested)
                {
                    this.m_log.Write(GetUsage());
                    return true;
                }

                if (!this.SetProject(cmdLineParser))
                {
                    return false;
                }

                // BuildMetadata is optional (false by default)
                this.BuildMetadata = null != cmdLineParser[BUILDMETADATA];

                // PackName is allowed to remain null
                this.PackName = cmdLineParser[PACK];

                // Platform is allowed to remain null
                this.Platform = cmdLineParser[PLATFORM];

                // TempPath is optional (default path used by default)
                this.TempPath = cmdLineParser[TEMPPATH];

                // Change list number is allowed to remain null
                this.ChangeListNumber = cmdLineParser[CHANGE_LIST_NUMBER];

                // Working path is allowed to remain null
                this.WorkingPath = cmdLineParser[WORKING_PATH];

                return this.SetPerforceInfo(cmdLineParser);
            }
            catch (Exception ex)
            {
                this.m_log.Exception(ex);
                return false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the command line arg usage details.
        /// </summary>
        /// <returns>
        /// The usage details <see cref="string"/>.
        /// </returns>
        private static string GetUsage()
        {
            var builder = new StringBuilder();
            builder.Append(Environment.NewLine);
            builder.Append("Builds audio data.");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);

            builder.Append("Usage:");
            builder.Append(Environment.NewLine);
            builder.Append("AUDIODATABUILDER -project <file> -p4host <server:port> -p4client <workspace>");
            builder.Append(Environment.NewLine);
            builder.Append("-p4user <user_name> [-p4passwd <password>] [-help/-?]");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);

            builder.Append("-project\tthe path to the project settings file.");
            builder.Append(Environment.NewLine);
            builder.Append("-p4host\t\tthe connection info for perforce ex. rsgedip4s1:1666.");
            builder.Append(Environment.NewLine);
            builder.Append("-p4client\tthe perforce client workspace ex. EDIW-HBUNYAN.");
            builder.Append(Environment.NewLine);
            builder.Append("-p4user\t\tthe perforce user name ex. hughel.bunyan.");
            builder.Append(Environment.NewLine);
            builder.Append("-p4passwd\tthe perforce password.");
            builder.Append(Environment.NewLine);
            builder.Append("-depotroot\tthe depot root.");
            builder.Append(Environment.NewLine);
            builder.Append("-changelistnumber\tthe change list number (optional).");
            builder.Append(Environment.NewLine);
            builder.Append("-pack\t\tlimit wave the build to the specified wave pack");
            builder.Append(Environment.NewLine);
            builder.Append("-platform\t\tlimit wave the build to the specified platform");
            builder.Append(Environment.NewLine);
            builder.Append("-temppath\t\tpath to temp directory (optional)");
            builder.Append(Environment.NewLine);
            builder.Append("-buildmetadata\t\tflag to indicate that metadata should be built");
            builder.Append(Environment.NewLine);
            builder.Append("-help or -?\tdisplay usage information.");
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }

        /// <summary>
        /// Set the perforce info.
        /// </summary>
        /// <param name="cmdLineParser">
        /// The command line parser.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool SetPerforceInfo(CmdLineParser cmdLineParser)
        {
            this.PerforceHost = cmdLineParser[P4_HOST];
            if (string.IsNullOrEmpty(this.PerforceHost))
            {
                this.WriteError(MISSING_P4_HOST);
                return false;
            }

            this.PerforceClient = cmdLineParser[P4_CLIENT];
            if (string.IsNullOrEmpty(this.PerforceHost))
            {
                this.WriteError(MISSING_P4_CLIENT);
                return false;
            }

            this.PerforceUsername = cmdLineParser[P4_USER];
            if (string.IsNullOrEmpty(this.PerforceUsername))
            {
                this.WriteError(MISSING_P4_USER);
                return false;
            }

            this.PerforcePassword = string.Empty;
            var passwd = cmdLineParser[P4_PASSWD];
            if (!string.IsNullOrEmpty(passwd))
            {
                this.PerforcePassword = passwd;
            }

            this.PerforceDepotRoot = cmdLineParser[P4_DEPOT_ROOT];
            if (string.IsNullOrEmpty(this.PerforceDepotRoot))
            {
                this.WriteError(MISSING_P4_DEPOT_ROOT);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Set the project.
        /// </summary>
        /// <param name="cmdLineParser">
        /// The command line parser.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool SetProject(CmdLineParser cmdLineParser)
        {
            this.ProjectSettings = cmdLineParser[PROJECT];
            if (string.IsNullOrEmpty(this.ProjectSettings))
            {
                this.WriteError(MISSING_PROJECT);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Write error message.
        /// </summary>
        /// <param name="msg">
        /// The error message.
        /// </param>
        private void WriteError(string msg)
        {
            this.m_log.Write(string.Empty);
            this.m_log.Error(msg);
            this.m_log.Write(GetUsage());
        }

        #endregion
    }
}