﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The audio data builder program.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace audioDataBuilder
{
    using System;
    using System.IO;
    using System.Reflection;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    using rage;
    using rage.ToolLib.Logging;
    using rage.ToolLib.Mail;

    /// <summary>
    /// The audio data builder program.
    /// </summary>
    internal class Program
    {
        #region Constants

        /// <summary>
        /// The asset manager creation failed string.
        /// </summary>
        private const string ASSET_MGR_CREATION_FAILED = "Failed to create asset manager to connect to Perforce.";

        /// <summary>
        /// The DLL extension string.
        /// </summary>
        private const string DLL_EXTENSION = ".dll";

        #endregion

        #region Methods

        /// <summary>
        /// Create the asset manager.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="host">
        /// The host.
        /// </param>
        /// <param name="client">
        /// The client.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="passwd">
        /// The password.
        /// </param>
        /// <param name="depotRoot">
        /// The depot root.
        /// </param>
        /// <returns>
        /// The asset manager instance <see cref="assetManager"/>.
        /// </returns>
        private static IAssetManager CreateAssetManager(
            ILog log, string host, string client, string user, string passwd, string depotRoot)
        {
            IAssetManager assetMgr = null;
            try
            {
                assetMgr = AssetManagerFactory.GetInstance(
                    AssetManagerType.Perforce, null, host, client, user, passwd, depotRoot);
            }
            catch (Exception e)
            {
                try
                {
                    if (assetMgr != null)
                    {
                        assetMgr.Disconnect();
                        assetMgr = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Exception(ex);
                }

                log.Exception(e);
            }

            return assetMgr;
        }

        /// <summary>
        /// Create the perforce connection settings.
        /// </summary>
        /// <param name="cmdLineArgs">
        /// The command line args.
        /// </param>
        /// <returns>
        /// The perforce connection settings <see cref="PerforceConnectionSettings"/>.
        /// </returns>
        private static PerforceConnectionSettings CreatePerforceConnectionSettings(ICmdLineArgs cmdLineArgs)
        {
            return new PerforceConnectionSettings
            {
                Client = cmdLineArgs.PerforceClient, 
                Host = cmdLineArgs.PerforceHost, 
                Username = cmdLineArgs.PerforceUsername, 
                Password = cmdLineArgs.PerforcePassword, 
                DepotRoot = cmdLineArgs.PerforceDepotRoot
            };
        }

        /// <summary>
        /// Get latest for path at specified change list.
        /// </summary>
        /// <param name="assetMgr">
        /// The asset manager.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="changelistNumber">
        /// The change list number.
        /// </param>
        private static void GetLatest(IAssetManager assetMgr, string path, string changelistNumber)
        {
            try
            {
                assetMgr.GetLatest(path, false, changelistNumber);
            }
            catch (Exception)
            {
                assetMgr.GetLatest(path, true, changelistNumber);
            }
        }

        /// <summary>
        /// Get latest for path.
        /// </summary>
        /// <param name="assetMgr">
        /// The asset manager.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        private static void GetLatest(IAssetManager assetMgr, string path)
        {
            try
            {
                assetMgr.GetLatest(path, false);
            }
            catch (Exception)
            {
                assetMgr.GetLatest(path, true);
            }
        }

        /// <summary>
        /// Get the latest build.
        /// </summary>
        /// <param name="assetMgr">
        /// The asset manager.
        /// </param>
        /// <param name="projSettings">
        /// The project settings.
        /// </param>
        private static void GetLatestBuild(IAssetManager assetMgr, audProjectSettings projSettings)
        {
            foreach (PlatformSetting platformSetting in projSettings.GetPlatformSettings())
            {
                var buildInfoPath = assetMgr.GetLocalPath(platformSetting.BuildInfo);
                var buildOutputPath = assetMgr.GetLocalPath(platformSetting.BuildOutput);

                GetLatest(assetMgr, buildInfoPath);
                GetLatest(assetMgr, buildOutputPath);
            }
        }

        /// <summary>
        /// Get the latest data.
        /// </summary>
        /// <param name="assetMgr">
        /// The asset manager.
        /// </param>
        /// <param name="projSettings">
        /// The project settings.
        /// </param>
        /// <param name="changelistNumber">
        /// The change list number.
        /// </param>
        private static void GetLatestData(
            IAssetManager assetMgr, audProjectSettings projSettings, string changelistNumber)
        {
            var soundsPath = assetMgr.GetLocalPath(projSettings.GetSoundXmlPath());
            var waveSlotSettingsPath = assetMgr.GetLocalPath(projSettings.GetWaveSlotSettings());

            GetLatest(assetMgr, soundsPath, changelistNumber);
            GetLatest(assetMgr, waveSlotSettingsPath, changelistNumber);
        }

        /// <summary>
        /// Get the latest tools.
        /// </summary>
        /// <param name="assetMgr">
        /// The asset manager.
        /// </param>
        /// <param name="projSettings">
        /// The project settings.
        /// </param>
        private static void GetLatestTools(IAssetManager assetMgr, audProjectSettings projSettings)
        {
            var buildPath = assetMgr.GetLocalPath(projSettings.GetBuildPath());
            var buildToolsPath = assetMgr.GetLocalPath(projSettings.GetBuildToolsPath());
            var metadataCompilerPath =
                assetMgr.GetLocalPath(Path.GetDirectoryName(projSettings.GetMetadataCompilerPath()) + "\\");

            GetLatest(assetMgr, buildPath);
            GetLatest(assetMgr, buildToolsPath);
            GetLatest(assetMgr, metadataCompilerPath);
        }

        /// <summary>
        /// Get version.
        /// </summary>
        /// <returns>
        /// The version <see cref="string"/>.
        /// </returns>
        private static string GetVersion()
        {
            return Assembly.GetEntryAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Main entry point.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The return code <see cref="int"/>.
        /// </returns>
        private static int Main(string[] args)
        {
            var screenLog = new TextLog(new ConsoleLogWriter(), new ContextStack());
            var cmdLineArgs = new CmdLineArgs(screenLog);

            if (!cmdLineArgs.Parse(args))
            {
                return -1;
            }

            if (cmdLineArgs.HelpRequested)
            {
                return 0;
            }

            var assetMgr = CreateAssetManager(
                screenLog, 
                cmdLineArgs.PerforceHost, 
                cmdLineArgs.PerforceClient, 
                cmdLineArgs.PerforceUsername, 
                cmdLineArgs.PerforcePassword, 
                cmdLineArgs.PerforceDepotRoot);
            if (assetMgr == null)
            {
                screenLog.Error(ASSET_MGR_CREATION_FAILED);
                return -1;
            }

            audProjectSettings projectSettings;
            try
            {
                projectSettings = new audProjectSettings(assetMgr.GetLocalPath(cmdLineArgs.ProjectSettings));
            }
            catch (Exception ex)
            {
                screenLog.Exception(ex);
                return -1;
            }

            var exitCode = 0;
            var logFilePath = Path.GetTempPath() + "AudioDataBuilderLog.xml";
            using (var logWriter = new FileLogWriter(logFilePath))
            {
                using (var log = new XmlLog(logWriter, new ContextStack(), "AudioDataBuilderLog"))
                {
                    var versionInfo = string.Concat("Audio Data Builder v", GetVersion());
                    screenLog.Write(versionInfo);
                    screenLog.Write(string.Empty);

                    try
                    {
                        var changeListNumber = cmdLineArgs.ChangeListNumber;
                        if (string.IsNullOrEmpty(changeListNumber))
                        {
                            // Use latest change list number from P4
                            changeListNumber = assetMgr.GetLatestChangeListNumber().ToString();
                        }

                        screenLog.Write("Getting latest data...");
                        GetLatestData(assetMgr, projectSettings, changeListNumber);
                        screenLog.Write("Getting latest build...");
                        GetLatestBuild(assetMgr, projectSettings);
                        screenLog.Write("Getting latest tools...");
                        GetLatestTools(assetMgr, projectSettings);

                        AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;

                        var builder = new Builder(
                            log, 
                            assetMgr, 
                            projectSettings, 
                            CreatePerforceConnectionSettings(cmdLineArgs), 
                            cmdLineArgs.ProjectSettings, 
                            changeListNumber, 
                            cmdLineArgs.PackName, 
                            cmdLineArgs.Platform, 
                            cmdLineArgs.BuildMetadata, 
                            cmdLineArgs.TempPath,
                            cmdLineArgs.WorkingPath);

                        screenLog.Write("Running builder module...");
                        if (builder.Run())
                        {
                            screenLog.Write("Succeeded");
                        }
                        else
                        {
                            screenLog.Write("Failed");
                            exitCode = -1;
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Exception(ex);
                        screenLog.Exception(ex);
                        screenLog.WriteFormatted("Failed");
                        exitCode = -1;
                    }

                    AppDomain.CurrentDomain.AssemblyResolve -= OnAssemblyResolve;
                }
            }

            if (exitCode == -1)
            {
                Console.Error.WriteLine(string.Format("Log file path: {0}", logFilePath));
                Console.Error.WriteLine("Sending failure email");
                var reporter = new Reporter(
                    screenLog, 
                    logFilePath, 
                    new SmtpMailSender(screenLog, projectSettings.AudioDataBuilderSettings.MailSettings.Host), 
                    projectSettings);
                reporter.Run();
            }

            return exitCode;
        }

        /// <summary>
        /// Assembly resolve method to load JIT DLLs.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The loaded assembly <see cref="Assembly"/>.
        /// </returns>
        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (Builder.JitPaths != null)
            {
                var name = new AssemblyName(args.Name);
                foreach (var jitPath in Builder.JitPaths)
                {
                    var assemblyFile = Path.Combine(jitPath, string.Concat(name.Name, DLL_EXTENSION));
                    if (File.Exists(assemblyFile))
                    {
                        return Assembly.LoadFile(assemblyFile);
                    }
                }
            }

            return null;
        }

        #endregion
    }
}