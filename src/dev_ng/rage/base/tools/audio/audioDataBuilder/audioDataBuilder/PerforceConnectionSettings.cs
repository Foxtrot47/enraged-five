// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PerforceConnectionSettings.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The perforce connection settings.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace audioDataBuilder
{
    /// <summary>
    /// The perforce connection settings.
    /// </summary>
    public class PerforceConnectionSettings
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the client.
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// Gets or sets the depot root.
        /// </summary>
        public string DepotRoot { get; set; }

        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string Username { get; set; }

        #endregion
    }
}