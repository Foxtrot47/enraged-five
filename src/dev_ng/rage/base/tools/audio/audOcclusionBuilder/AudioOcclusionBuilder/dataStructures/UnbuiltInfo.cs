﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AudioOcclusionBuilder
{
    class UnbuiltInfo
    {
        public long m_InteriorHash { get; set; }
        public string m_InteriorName { get; set; }

        public UnbuiltInfo(long interiorHash, string interiorName)
        {
            m_InteriorHash = interiorHash;
            m_InteriorName = interiorName;
        }
    }
}
