﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;

using Rockstar.AssetManager.Interfaces;
using Rockstar.AssetManager.Perforce;

using ProjectLoader2;

using rage;

namespace AudioOcclusionBuilder.model
{
    class p4 : modelbase
    {
        private audProjectSettings m_ProjectSettings;
        private IChangeList m_Changelist;
        private IAssetManager m_AssetManager;

        public void Init()
        {
            try
            {
                var pl =
                    new frmProjectLoader(
                        string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                      @"\Rave\ProjectList.xml"));
                pl.ShowDialog();

                if (pl.IsCancelled)
                {
                    System.Diagnostics.Debug.WriteLine("frmProjectLoader cancelled");
                }

                m_AssetManager = pl.AssetManager;
                m_ProjectSettings = new audProjectSettings(pl.Project.ProjectSettings);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                System.Diagnostics.Debug.WriteLine("frmProjectLoader exception");
            }

            System.Diagnostics.Debug.WriteLine("Looking for existing 'Audio Occlusion Build' CL");
            m_Changelist = null;
            m_AssetManager.LoadChangeLists();
            foreach (KeyValuePair<string, IChangeList> entry in m_AssetManager.ChangeLists)
            {
                IChangeList changeList = entry.Value;
                if (changeList.Description.Equals("Audio Occlusion Build\n", StringComparison.Ordinal))
                {
                    System.Diagnostics.Debug.WriteLine("Found existing CL to use");
                    m_Changelist = changeList;
                    break;
                }
            }
        }

        public void CheckOutMetadataFiles(ObservableCollection<InteriorInfo> interiorList)
        {
            WriteToLog("Checking out metadata files");

            // Update the start time property so we know when we started the build in case the program/computer crashes
            Properties.Settings.Default.StartDate = System.DateTime.Now;
            Properties.Settings.Default.Save();

            // Create the changelist
            if (m_Changelist == null)
            {
                m_Changelist = m_AssetManager.CreateChangeList("Audio Occlusion Build");
            }

            // Get latest and checkout the metadata files from p4
            m_AssetManager.GetLatest(Properties.Settings.Default.PathAssets, true);

            // If we've specified individual interiors to build, then check those out individually, otherwise check them all out
            if (interiorList.Count > 0)
            {
                foreach (InteriorInfo info in interiorList)
                {
                    if (info.IsSelected || info.IsDependency)
                    {
                        string file = m_AssetManager.GetLocalPath(Properties.Settings.Default.PathAssets + info.InteriorHashkey.ToString() + ".pso.meta");
                        if (m_AssetManager.ExistsAsAsset(file) && m_AssetManager.IsCheckedOut(file) == false)
                        {
                            WriteToLog("Checking out " + file);
                            m_Changelist.CheckoutAsset(file, false);
                        }
                    }
                }
            }
            else
            {
                string path = m_AssetManager.GetLocalPath(Properties.Settings.Default.PathAssets);

                if (Directory.Exists(path))
                {
                    foreach (string file in Directory.EnumerateFiles(path))
                    {
                        if (m_AssetManager.IsCheckedOut(file) == false && m_AssetManager.ExistsAsAsset(file))
                        {
                            WriteToLog("Checking out " + file);
                            m_Changelist.CheckoutAsset(file, false);
                        }
                    }
                }
            }
        }

        public void FinalizeChangelist()
        {
            WriteToLog("Finished audio occlusion build, deleting unused assets, reverting unchanged assets, adding new assets.");

            // If the occlusion file wasn't edited during the occlusion build, then it's old and not needed
            foreach (IAsset asset in m_Changelist.Assets.ToList())
            {
                DateTime lastWriteTime = File.GetLastWriteTime(asset.LocalPath);
                if (DateTime.Compare(Properties.Settings.Default.StartDate, lastWriteTime) > 0)
                {
                    WriteToLog("Deleting " + asset.LocalPath);
                    m_Changelist.RevertAsset(m_AssetManager.GetCheckedOutAsset(asset.LocalPath), true);
                    m_Changelist.MarkAssetForDelete(asset.LocalPath);
                }
            }

            // Revert the unchanged ones
            WriteToLog("Reverting unchanged files");
            m_Changelist.RevertUnchanged();

            // And finally add any new files
            WriteToLog("Adding new files");
            string localFilePath = m_AssetManager.GetLocalPath(Properties.Settings.Default.PathAssets);
            foreach (string file in Directory.EnumerateFiles(localFilePath))
            {
                m_Changelist.MarkAssetForAdd(file);
            }
        }
    }
}
