﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AudioOcclusionBuilder
{
    // This needs to be kept in-sync with the game version of this enum
    public enum gameBuildStep
    {
        AUD_OCC_BUILD_STEP_START,
        AUD_OCC_BUILD_STEP_INTRA_LOAD,
        AUD_OCC_BUILD_STEP_INTRA_PORTALS,
        AUD_OCC_BUILD_STEP_INTRA_PATHS,
        AUD_OCC_BUILD_STEP_INTRA_UNLOAD,
        AUD_OCC_BUILD_STEP_INTER_PATHS,
        AUD_OCC_BUILD_STEP_INTER_UNLOAD,
        AUD_OCC_BUILD_STEP_TUNNEL_PATHS,
        AUD_OCC_BUILD_STEP_TUNNEL_UNLOAD,
        AUD_OCC_BUILD_STEP_FINISH,
        NUM_AUD_OCC_BUILD_STEPS
    };

    public enum buildErrorCode
    {
        buildErrorNone,
        buildErrorUserStoppage,
        buildErrorTimeoutLaunch,
        buildErrorTimeoutInterior,
        buildErrorCrash
    };

    public enum platformType : uint
    {
        XBOX = 0,
        PS3,
        PC,
        XBOXONE,
        PS4
    };

    public enum configType : uint
    {
        BETA = 0,
        BANKRELEASE,
    };

}
