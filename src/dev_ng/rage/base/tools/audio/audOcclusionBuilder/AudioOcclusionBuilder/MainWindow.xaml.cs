﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace AudioOcclusionBuilder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public delegate void DelegateFailedInteriorsLog(string line);

    public partial class MainWindow : Window
    {

        public static DelegateFailedInteriorsLog sm_DelegateFailedInteriorsLog;

        private viewModel.MainWindowViewModel m_ViewModel;
        
        public MainWindow()
        {
            InitializeComponent();

            model.launcher l = Application.Current.FindResource("model_launcher") as model.launcher;
            if (l != null)
            {
                l.Init();
            }

            model.p4 p4 = Application.Current.FindResource("model_p4") as model.p4;
            if (p4 != null)
            {
                p4.Init();
            }

            textboxPathAssets.Text = Properties.Settings.Default.PathAssets;


            m_ViewModel = new viewModel.MainWindowViewModel();
            m_ViewModel.Init();
            this.DataContext = m_ViewModel;
        }

        private void StartBuild_Click(object sender, RoutedEventArgs e)
        {
            m_ViewModel.StartBuild(tboxCommandline.Text);
        }

        private void ContinueBuild_Click(object sender, RoutedEventArgs e)
        {
            m_ViewModel.ContinueBuild(tboxCommandline.Text);
        }

        private void StopBuild_Click(object sender, RoutedEventArgs e)
        {
            m_ViewModel.StopBuild();
        }

        private void BuildInteriorList_Click(object sender, RoutedEventArgs e)
        {
            m_ViewModel.BuildInteriorList(tboxCommandline.Text);
        }

        private void ClearInteriorList_Click(object sender, RoutedEventArgs e)
        {
            m_ViewModel.ClearInteriorList();
        }

        private void listBoxFailures_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                m_ViewModel.CommandFailureDelete();
            }
        }

        private void buttonRemoveFailure_Click(object sender, RoutedEventArgs e)
        {
            m_ViewModel.CommandFailureDelete();
        }

        private void buttonRebuildUnbuilt_Click(object sender, RoutedEventArgs e)
        {
            m_ViewModel.CommandRebuildUnbuilt();
        }

        private void listBoxInteriors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            m_ViewModel.UpdateDependencies();
        }


    }
}
