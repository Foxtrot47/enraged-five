﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows;

namespace AudioOcclusionBuilder
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string sm_ProgressFilePath = Environment.GetEnvironmentVariable("TEMP") + "\\audOcclusionBuilder\\Progress.txt";
        public static string sm_LogFilePath = Environment.GetEnvironmentVariable("TEMP") + "\\audOcclusionBuilder\\Console.log";
        public static string sm_FailedInteriorLogDir = Environment.GetEnvironmentVariable("TEMP") + "\\audOcclusionBuilder\\FailureLogs\\";
        public static string sm_FailedInteriorFile = Environment.GetEnvironmentVariable("TEMP") + "\\audOcclusionBuilder\\Failures.xml";
        public static string sm_InteriorListFile = Environment.GetEnvironmentVariable("TEMP") + "\\audOcclusionBuilder\\InteriorList.xml";
        public static string sm_InteriorBuildListFile = Environment.GetEnvironmentVariable("TEMP") + "\\audOcclusionBuilder\\InteriorBuildList.xml";
        public static string sm_UnbuiltInteriorFile = Environment.GetEnvironmentVariable("TEMP") + "\\audOcclusionBuilder\\Unbuilt.xml";
        public static string sm_BuiltIntraPathsFile = Environment.GetEnvironmentVariable("TEMP") + "\\audOcclusionBuilder\\BuiltIntraPaths.txt";
        public static string sm_BuiltInterPathsFile = Environment.GetEnvironmentVariable("TEMP") + "\\audOcclusionBuilder\\BuiltInterPaths.txt";
        public static string sm_BuiltTunnelPathsFile = Environment.GetEnvironmentVariable("TEMP") + "\\audOcclusionBuilder\\BuiltTunnelPaths.txt";
    }
}
