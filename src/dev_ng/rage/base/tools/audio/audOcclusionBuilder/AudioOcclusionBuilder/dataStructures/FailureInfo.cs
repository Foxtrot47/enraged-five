﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace AudioOcclusionBuilder
{
    class FailureInfo
    {
        public string m_FailureName { get; set; }

        public uint m_InteriorHash { get; set; }
        public gameBuildStep m_BuildStep { get; set; }
        public string m_InteriorName { get; set; }
        public string m_RoomName { get; set; }
        public uint m_PortalIdx { get; set; }
        public string m_ConsoleFileName { get; set; }
        public buildErrorCode m_ErrorCode { get; set; }

        public FailureInfo(string failureName)
        {
            m_FailureName = failureName;
        }

        public bool Init()
        {
            XElement root = XElement.Load(App.sm_FailedInteriorFile);
            IEnumerable<XElement> failure =
                from el in root.Elements("Failure")
                where (string)el.Attribute("Name") == m_FailureName
                select el;
            foreach (XElement el in failure)
            {
                m_InteriorHash = Convert.ToUInt32(el.Attribute("InteriorHash").Value);
                m_BuildStep = (gameBuildStep)Convert.ToUInt32(el.Attribute("BuildStep").Value);
                m_InteriorName = el.Attribute("InteriorName").Value;
                m_RoomName = el.Attribute("RoomName").Value;
                m_PortalIdx = Convert.ToUInt32(el.Attribute("PortalIdx").Value);
                m_ConsoleFileName = el.Attribute("ConsoleFileName").Value;
                m_ErrorCode = (buildErrorCode)Convert.ToUInt32(el.Attribute("ErrorCode").Value);

                return true;
            }

            return false;
        }
    }
}
