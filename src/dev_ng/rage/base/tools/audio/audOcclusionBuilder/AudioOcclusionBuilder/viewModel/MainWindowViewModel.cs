﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Windows;

namespace AudioOcclusionBuilder.viewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        public delegate void DelegateProgressBar(uint currentInteriorIndex, uint numInteriors, 
                                                string currentInteriorName, string currentRoomName, 
                                                uint currentPortalIdx, gameBuildStep buildStep);
        public static DelegateProgressBar sm_DelegateProgressBar;

        public delegate void DelegateLog(string line);
        public static DelegateLog sm_DelegateLog;

        public delegate void DelegateLogGameStop(string fileName);
        public static DelegateLogGameStop sm_DelegateLogGameStop;

        public delegate void DelegateUpdateUnbuiltInteriorList();
        public static DelegateUpdateUnbuiltInteriorList sm_DelegateUpdateUnbuiltInteriorList;

        public delegate void DelegateUpdateInteriorList();
        public static DelegateUpdateInteriorList sm_DelegateUpdateInteriorList;

        private model.builder m_Builder;
        private Thread m_BuilderThread;

#region Properties

        private float _PercentCurrent;
        public float PercentCurrent
        {
            get
            {
                return _PercentCurrent;
            }
            set
            {
                if(_PercentCurrent != value)
                {
                    _PercentCurrent = value;
                    NotifyPropertyChanged("PercentCurrent");
                }
            }
        }

        private float _PercentTotal;
        public float PercentTotal
        {
            get
            {
                return _PercentTotal;
            }
            set
            {
                if(_PercentTotal != value)
                {
                    _PercentTotal = value;
                    NotifyPropertyChanged("PercentTotal");
                }
            }
        }

        private string _CurrentBuildStep;
        public string CurrentBuildStep
        {
            get
            {
                return _CurrentBuildStep;
            }
            set
            {
                if (_CurrentBuildStep != value)
                {
                    _CurrentBuildStep = value;
                    NotifyPropertyChanged("CurrentBuildStep");
                }
            }
        }

        private string _CurrentInterior;
        public string CurrentInterior
        {
            get
            {
                return _CurrentInterior;
            }
            set
            {
                if (_CurrentInterior != value)
                {
                    _CurrentInterior = value;
                    NotifyPropertyChanged("CurrentInterior");
                }
            }
        }

        private string _CurrentRoom;
        public string CurrentRoom
        {
            get
            {
                return _CurrentRoom;
            }
            set
            {
                if (_CurrentRoom != value)
                {
                    _CurrentRoom = value;
                    NotifyPropertyChanged("CurrentRoom");
                }
            }
        }

        private string _CurrentPortal;
        public string CurrentPortal
        {
            get
            {
                return _CurrentPortal;
            }
            set
            {
                if (_CurrentPortal != value)
                {
                    _CurrentPortal = value;
                    NotifyPropertyChanged("CurrentPortal");
                }
            }
        }

        private string _Commandline;
        public string Commandline
        {
            get
            {
                return _Commandline;
            }
            set
            {
                if (_Commandline != value)
                {
                    _Commandline = value;
                    Properties.Settings.Default.Commandline = value;
                    Properties.Settings.Default.Save();
                    NotifyPropertyChanged("Commandline");
                }
            }
        }

        private platformType _Platform;
        public platformType Platform
        {
            get
            {
                return _Platform;
            }
            set
            {
                if (_Platform != value)
                {
                    _Platform = value;
                    Properties.Settings.Default.PlatformType = (uint)value;
                    Properties.Settings.Default.Save();
                    NotifyPropertyChanged("Platform");
                }
            }
        }

        private configType _Config;
        public configType Config
        {
            get
            {
                return _Config;
            }
            set
            {
                if (_Config != value)
                {
                    _Config = value;
                    Properties.Settings.Default.ConfigType = (uint)value;
                    Properties.Settings.Default.Save();
                    NotifyPropertyChanged("Config");
                }
            }
        }

        private string _Log;
        public string Log
        {
            get
            {
                return _Log;
            }
            set
            {
                _Log = value;
                NotifyPropertyChanged("Log");
            }
        }

        private ObservableCollection<UnbuiltInfo> _UnbuiltList;
        public ObservableCollection<UnbuiltInfo> UnbuiltList
        {
            get
            {
                return _UnbuiltList;
            }
            set
            {
                _UnbuiltList = value;
                NotifyPropertyChanged("UnbuiltList");
            }
        }

        private UnbuiltInfo _UnbuiltSelection;
        public UnbuiltInfo UnbuiltSelection
        {
            get
            {
                return _UnbuiltSelection;
            }
            set
            {
                _UnbuiltSelection = value;
                NotifyPropertyChanged("UnbuiltSelection");
            }
        }

        private ObservableCollection<FailureInfo> _FailureList;
        public ObservableCollection<FailureInfo> FailureList
        {
            get
            {
                return _FailureList;
            }
            set
            {
                _FailureList = value;
                NotifyPropertyChanged("FailureList");
            }
        }

        private FailureInfo _FailureSelection;
        public FailureInfo FailureSelection
        {
            get
            {
                return _FailureSelection;
            }
            set
            {
                _FailureSelection = value;
                UpdateFailureOutputLog();               
                NotifyPropertyChanged("FailureSelection");
            }
        }

        private string _FailureOutput;
        public string FailureOutput
        {
            get
            {
                return _FailureOutput;
            }
            set
            {
                _FailureOutput = value;
                NotifyPropertyChanged("FailureOutput");
            }
        }

        private bool _DisplayFullLogs;
        public bool DisplayFullLogs
        {
            get
            {
                return _DisplayFullLogs;
            }
            set
            {
                _DisplayFullLogs = value;
                UpdateFailureOutputLog();
                NotifyPropertyChanged("DisplayFullLogs");
            }
        }

        private bool _BuildInterDependencies;
        public bool BuildInterDependencies
        {
            get
            {
                return _BuildInterDependencies;
            }
            set
            {
                _BuildInterDependencies = value;
                NotifyPropertyChanged("BuildInterDependencies");
            }
        }

        private ObservableCollection<InteriorInfo> _InteriorList;
        public ObservableCollection<InteriorInfo> InteriorList
        {
            get
            {
                return _InteriorList;
            }
            set
            {
                _InteriorList = value;
                NotifyPropertyChanged("InteriorList");
            }
        }

        private string _PathExe;
        public string PathExe
        {
            get
            {
                return _PathExe;
            }
            set
            {
                _PathExe = value;
                Properties.Settings.Default.PathExe = value;
                Properties.Settings.Default.Save();
                NotifyPropertyChanged("PathExe");
            }
        }

        private string _PathAssets;
        public string PathAssets
        {
            get
            {
                return _PathAssets;
            }
            set
            {
                _PathAssets = value;
                Properties.Settings.Default.PathAssets = value;
                Properties.Settings.Default.Save();
                NotifyPropertyChanged("PathAssets");
            }
        }


#endregion

        public void Init()
        {
            FailureList = new ObservableCollection<FailureInfo>();
            UnbuiltList = new ObservableCollection<UnbuiltInfo>();
            InteriorList = new ObservableCollection<InteriorInfo>();

            Commandline = Properties.Settings.Default.Commandline;
            Platform = (platformType)Properties.Settings.Default.PlatformType;
            Config = (configType)Properties.Settings.Default.ConfigType;

            if (Properties.Settings.Default.PathExe == "")
            {
                Properties.Settings.Default.PathExe = Environment.GetEnvironmentVariable("RS_BUILDBRANCH");
                Properties.Settings.Default.Save();
            }
            PathExe = Properties.Settings.Default.PathExe;

            if (Properties.Settings.Default.PathAssets == "")
            {
                Properties.Settings.Default.PathAssets = Environment.GetEnvironmentVariable("RS_PROJROOT") + "\\assets";
                Properties.Settings.Default.Save();
            }
            PathAssets = Properties.Settings.Default.PathAssets;
            
            sm_DelegateProgressBar = new DelegateProgressBar(this.UpdateProgressBar);
            sm_DelegateLog = new DelegateLog(this.UpdateLog);
            sm_DelegateLogGameStop = new DelegateLogGameStop(this.LogGameStop);
            sm_DelegateUpdateUnbuiltInteriorList = new DelegateUpdateUnbuiltInteriorList(this.UpdateUnbuiltInteriorList);
            sm_DelegateUpdateInteriorList = new DelegateUpdateInteriorList(this.UpdateInteriorList);

            if (!Directory.Exists(App.sm_FailedInteriorLogDir))
            {
                Directory.CreateDirectory(App.sm_FailedInteriorLogDir);
            }

            if (!File.Exists(App.sm_FailedInteriorFile))
            {
                CreateNewFailedInteriorFile();
            }

            if (!File.Exists(App.sm_InteriorListFile))
            {
                CreateNewInteriorListFile();
            }

            if (!File.Exists(App.sm_ProgressFilePath))
            {
                FileStream newFile = File.Create(App.sm_ProgressFilePath);
                newFile.Dispose();
            }

            if (!File.Exists(App.sm_BuiltIntraPathsFile))
            {
                FileStream newFile = File.Create(App.sm_BuiltIntraPathsFile);
                newFile.Dispose();
            }

            if (!File.Exists(App.sm_BuiltInterPathsFile))
            {
                FileStream newFile = File.Create(App.sm_BuiltInterPathsFile);
                newFile.Dispose();
            }

            if (!File.Exists(App.sm_BuiltTunnelPathsFile))
            {
                FileStream newFile = File.Create(App.sm_BuiltTunnelPathsFile);
                newFile.Dispose();
            }

            if (!File.Exists(App.sm_UnbuiltInteriorFile))
            {
                CreateNewUnbuiltInteriorFile();
            }
            

            // If we're restarting the tool in the middle of a build, then load where we've already failed previously
            CreateFailedInteriorList();
            UpdateUnbuiltInteriorList();
            UpdateInteriorList();

            m_Builder = new model.builder();
            m_Builder.Init();

            m_BuilderThread = null;

            BuildInterDependencies = true;
        }

        public void StartBuild(string commandline)
        {
            bool shouldStartBuild = GetShouldStartBuildThread();

            if (shouldStartBuild)
            {
                CreateNewBuiltInteriorFiles();
                CreateNewFailedInteriorFile();
                CreateFailedInteriorList();
                CreateInteriorBuildListFile();
                UnbuiltList.Clear();

                Properties.Settings.Default.FailureIndex = 0;
                Properties.Settings.Default.Save();

                // Kick off the builder thread
                m_Builder.SetShouldStop(false);
                ThreadStart starter = delegate { m_Builder.StartBuild(commandline, Platform, Config, InteriorList); };
                m_BuilderThread = new Thread(starter);
                m_BuilderThread.Start();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false, "Build is already running, use 'Stop Build' first to stop the current build");
            }
        }

        public void ContinueBuild(string commandline)
        {
            bool shouldStartBuild = GetShouldStartBuildThread();

            if (shouldStartBuild)
            {
                CreateInteriorBuildListFile();

                m_Builder.SetShouldStop(false);
                ThreadStart starter = delegate { m_Builder.ContinueBuild(commandline, Platform, Config); };
                m_BuilderThread = new Thread(starter);
                m_BuilderThread.Start();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false, "Build is already running, use 'Stop Build' first to stop the current build");
            }
        }

        public void StopBuild()
        {
            if (m_BuilderThread != null)
            {
                System.Diagnostics.Debug.Assert(!m_BuilderThread.IsAlive || !m_Builder.GetShouldStop(), "Builder thread != null and and isn't running or set to be stopped");
                m_Builder.SetShouldStop(true);
            }
            else
            {
                model.launcher mL = App.Current.FindResource("model_launcher") as model.launcher;
                if (mL != null)
                {
                    mL.StopGame(Platform);
                }
            }
        }

        public void BuildInteriorList(string commandline)
        {
            bool shouldStartBuild = GetShouldStartBuildThread();

            if (shouldStartBuild)
            {
                CreateNewInteriorListFile();
                UpdateInteriorList();
                CreateInteriorBuildListFile();

                m_Builder.SetShouldStop(false);
                ThreadStart starter = delegate { m_Builder.BuildInteriorList(commandline, Platform, Config); };
                m_BuilderThread = new Thread(starter);
                m_BuilderThread.Start();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false, "Build is already running, use 'Stop Build' first to stop the current build");
            }
        }

        public void ClearInteriorList()
        {
            CreateNewInteriorListFile();
            UpdateInteriorList();
            CreateInteriorBuildListFile();
        }

        public void CommandFailureDelete()
        {
            if (FailureSelection != null)
            {
                string failureName = FailureSelection.m_FailureName;

                // Remove them from the failure.xml document
                XDocument doc = XDocument.Load(App.sm_FailedInteriorFile);
                XElement root = doc.Root;
                IEnumerable<XElement> failureEnumerable =
                    from el in root.Elements("Failure")
                    where (string)el.Attribute("Name") == failureName
                    select el;

                foreach (XElement el in failureEnumerable)
                {
                    el.Remove();
                }
                doc.Save(App.sm_FailedInteriorFile);

                // Remove them from the view
                foreach (FailureInfo info in FailureList.ToList())
                {
                    if (info.m_FailureName == failureName)
                    {
                        FailureList.Remove(info);
                    }
                }
            }
        }

        public void CommandRebuildUnbuilt()
        {
            System.Diagnostics.Debug.Assert(UnbuiltSelection != null, "Need to select Interior to remove from Unbuilt List");
            if (UnbuiltSelection != null)
            {
                XDocument doc = XDocument.Load(App.sm_FailedInteriorFile);
                XElement root = doc.Root;
                IEnumerable<XElement> failureEnumerable =
                    from el in root.Elements("Failure")
                    where Convert.ToUInt32(el.Attribute("InteriorHash").Value) == (uint)UnbuiltSelection.m_InteriorHash
                    select el;

                foreach (XElement el in failureEnumerable)
                {
                    el.Remove();
                }
                doc.Save(App.sm_FailedInteriorFile);

                // Remove them from the view
                foreach (FailureInfo info in FailureList.ToList())
                {
                    if (info.m_InteriorHash == UnbuiltSelection.m_InteriorHash)
                    {
                        FailureList.Remove(info);
                    }
                }

                // Also remove the Unbuilt Selection from the display
                foreach (UnbuiltInfo info in UnbuiltList.ToList())
                {
                    if (info.m_InteriorHash == UnbuiltSelection.m_InteriorHash)
                    {
                        UnbuiltList.Remove(info);
                    }
                }
            }
        }

        private bool GetShouldStartBuildThread()
        {
            bool shouldBuild = false;

            model.builder mB = App.Current.FindResource("model_builder") as model.builder;
            if (mB != null)
            {
                // Check if the thread is still alive but should be stopped
                if (m_BuilderThread != null)
                {
                    System.Diagnostics.Debug.Assert(!m_BuilderThread.IsAlive || !mB.GetShouldStop(), "Builder thread is still running and isn't set to be stopped");
                }

                if (m_BuilderThread == null || !m_BuilderThread.IsAlive)
                {
                    shouldBuild = true;
                }
            }

            return shouldBuild;
        }

        private void UpdateProgressBar(uint currentInteriorIndex, uint numInteriors, string currentInteriorName, string currentRoomName, uint currentPortalIdx, gameBuildStep buildStep)
        {
            System.Diagnostics.Debug.Assert(buildStep < gameBuildStep.NUM_AUD_OCC_BUILD_STEPS, "Invalid gameBuildStep");
            switch (buildStep)
            {
                case gameBuildStep.AUD_OCC_BUILD_STEP_START:
                case gameBuildStep.AUD_OCC_BUILD_STEP_FINISH:
                    {
                        break;
                    }
                case gameBuildStep.AUD_OCC_BUILD_STEP_INTRA_LOAD:
                case gameBuildStep.AUD_OCC_BUILD_STEP_INTRA_PORTALS:
                case gameBuildStep.AUD_OCC_BUILD_STEP_INTRA_PATHS:
                case gameBuildStep.AUD_OCC_BUILD_STEP_INTRA_UNLOAD:
                    {
                        CurrentBuildStep = "Intra";
                        CurrentInterior = currentInteriorName;
                        CurrentRoom = currentRoomName;
                        CurrentPortal = currentRoomName != null ? Convert.ToString(currentPortalIdx) : null;
                        PercentCurrent = (float)currentInteriorIndex / (float)numInteriors;
                        PercentTotal = (float)currentInteriorIndex / (float)(numInteriors * 3);
                        break;
                    }
                case gameBuildStep.AUD_OCC_BUILD_STEP_INTER_PATHS:
                case gameBuildStep.AUD_OCC_BUILD_STEP_INTER_UNLOAD:
                    {
                        CurrentBuildStep = "Inter";
                        CurrentInterior = currentInteriorName;
                        CurrentRoom = currentRoomName;
                        CurrentPortal = currentRoomName != null ? Convert.ToString(currentPortalIdx) : null; ;
                        PercentCurrent = (float)currentInteriorIndex / (float)numInteriors;
                        PercentTotal = (float)(currentInteriorIndex + numInteriors) / (float)(numInteriors * 3);
                        break;
                    }
                case gameBuildStep.AUD_OCC_BUILD_STEP_TUNNEL_PATHS:
                case gameBuildStep.AUD_OCC_BUILD_STEP_TUNNEL_UNLOAD:
                    {
                        CurrentBuildStep = "Tunnel";
                        CurrentInterior = currentInteriorName;
                        CurrentRoom = currentRoomName;
                        CurrentPortal = currentRoomName != null ? Convert.ToString(currentPortalIdx) : null; ;
                        PercentCurrent = (float)currentInteriorIndex / (float)numInteriors;
                        PercentTotal = (float)(currentInteriorIndex + numInteriors + numInteriors) / (float)(numInteriors * 3);
                        break;
                    }
            }
        }

        private void UpdateLog(string line)
        {
            Log += (line + "\n");
        }

        private void LogGameStop(string fileName)
        {
            FailureInfo failure = new FailureInfo(fileName);
            bool initSuccess = failure.Init();
            System.Diagnostics.Debug.Assert(initSuccess, "FailureInfo file not successfully initialized");
            FailureList.Add(failure);
        }

        private void UpdateFailureOutputLog()
        {
            if (FailureSelection != null)
            {
                if (DisplayFullLogs)
                {
                    FailureOutput = File.ReadAllText(FailureSelection.m_ConsoleFileName);
                }
                else
                {
                    FileStream fs = File.Open(FailureSelection.m_ConsoleFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    if (fs.Length > 5120)
                    {
                        fs.Seek(-5120, SeekOrigin.End);
                    }
                    StreamReader reader = new StreamReader(fs);
                    FailureOutput = reader.ReadToEnd(); 
                }
            }
            else
            {
                FailureOutput = "";
            }
        }

        private void CreateNewBuiltInteriorFiles()
        {
            FileStream s;
            s = File.Create(App.sm_BuiltIntraPathsFile);
            s.Dispose();
            s = File.Create(App.sm_BuiltInterPathsFile);
            s.Dispose();
            s = File.Create(App.sm_BuiltTunnelPathsFile);
            s.Dispose();
            s = File.Create(App.sm_ProgressFilePath);
            s.Dispose();
        }

        private void CreateNewFailedInteriorFile()
        {
            if (File.Exists(App.sm_FailedInteriorFile))
            {
                File.Delete(App.sm_FailedInteriorFile);
            }

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlElement element = doc.CreateElement("Failures");
            doc.AppendChild(element);

            doc.Save(App.sm_FailedInteriorFile);
        }

        private void CreateFailedInteriorList()
        {
            FailureList.Clear();

            try
            {
                XElement root = XElement.Load(App.sm_FailedInteriorFile);
                foreach (XElement child in root.Descendants())
                {
                    string failureName = child.Attribute("Name").Value;
                    FailureInfo info = new FailureInfo(failureName);
                    info.Init();
                    FailureList.Add(info);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                System.Diagnostics.Debug.WriteLine("Error loading the Failure.xml, creating new one");
                CreateNewFailedInteriorFile();
                CreateFailedInteriorList();
            }
        }

        private void UpdateUnbuiltInteriorList()
        {
            UnbuiltList.Clear();

            XElement root = XElement.Load(App.sm_UnbuiltInteriorFile);
            foreach (XElement child in root.Descendants("Interior"))
            {
                long interiorHash = Convert.ToInt64(child.Element("InteriorHash").Attribute("value").Value);
                string interiorName = child.Element("InteriorName").Value;
                UnbuiltInfo info = new UnbuiltInfo(interiorHash, interiorName);
                UnbuiltList.Add(info);
            }
        }

        private void CreateInteriorBuildListFile()
        {
            if (File.Exists(App.sm_InteriorBuildListFile))
            {
                File.Delete(App.sm_InteriorBuildListFile);
            }

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlElement root = doc.CreateElement("Interiors");
            doc.AppendChild(root);

            foreach (InteriorInfo interior in InteriorList)
            {
                if(interior.IsSelected || interior.IsDependency)
                {
                    XmlElement interiorElement = doc.CreateElement("UniqueProxyHashkey");
                    interiorElement.SetAttribute("value", interior.InteriorHashkey.ToString());
                    root.AppendChild(interiorElement);
                }                
            }

            doc.Save(App.sm_InteriorBuildListFile);
        }

        private void CreateNewInteriorListFile()
        {
            if (File.Exists(App.sm_InteriorListFile))
            {
                File.Delete(App.sm_InteriorListFile);
            }

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlElement element = doc.CreateElement("Interiors");
            doc.AppendChild(element);

            doc.Save(App.sm_InteriorListFile);
        }

        private void CreateNewUnbuiltInteriorFile()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);
            XmlElement element = doc.CreateElement("Interior");
            doc.AppendChild(element);
            doc.Save(App.sm_UnbuiltInteriorFile);
        }

        private void UpdateInteriorList()
        {
            InteriorList.Clear();

            try
            {
                XElement root = XElement.Load(App.sm_InteriorListFile);
                foreach (XElement child in root.Descendants("Interior"))
                {
                    string interiorName = child.Element("InteriorName").Value;
                    uint interiorHashkey = (uint)Convert.ToInt32(child.Element("InteriorHash").Attribute("value").Value);
                    int mapSlotIndex = Convert.ToInt32(child.Element("MapSlotIndex").Attribute("value").Value);
                    float posX = (float)Convert.ToDouble(child.Element("PosX").Attribute("value").Value);
                    float posY = (float)Convert.ToDouble(child.Element("PosY").Attribute("value").Value);
                    float posZ = (float)Convert.ToDouble(child.Element("PosZ").Attribute("value").Value);

                    InteriorInfo info = new InteriorInfo(interiorName, interiorHashkey, mapSlotIndex, posX, posY, posZ);
                    InteriorList.Add(info);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                System.Diagnostics.Debug.WriteLine("Error loading the InteriorList.xml, creating new one");
                CreateNewInteriorListFile();
                UpdateInteriorList();
            }

            foreach (InteriorInfo info in InteriorList)
            {
                info.BuildDependencies(this, InteriorList);
            }
        }

        public void UpdateDependencies()
        {
            foreach (InteriorInfo info in InteriorList)
            {
                info.IsDependency = false;
            }

            if (BuildInterDependencies)
            {
                foreach (InteriorInfo info in InteriorList)
                {
                    if (info.IsSelected)
                    {
                        foreach (uint dependentHashkey in info.DependentInteriorHashkeyList)
                        {
                            foreach (InteriorInfo dependentInfo in InteriorList)
                            {
                                if (dependentInfo.InteriorHashkey == dependentHashkey)
                                {
                                    dependentInfo.IsDependency = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
