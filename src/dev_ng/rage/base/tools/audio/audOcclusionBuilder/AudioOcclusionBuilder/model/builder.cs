﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using System.Windows;



namespace AudioOcclusionBuilder.model
{
    class builder : modelbase
    {
        public enum buildState
        {
            buildStateIdle,
            buildStateLaunch,
            buildStateLaunching,
            buildStateRunning,
            buildStateStop,
            buildStateFinish,
            buildStateAbort
        }

        public enum buildType
        {
            buildTypeStart,
            buildTypeContinue,
            buildTypeInteriorList
        }

        private buildState m_BuildState;
        private DateTime m_CurrentInteriorStartTime;
        private DateTime m_LastProgressUpdate;
        private buildType m_BuildType;
        private gameBuildStep m_BuildStep;
        private uint? m_CurrentInteriorHash;
        private string m_CurrentInteriorName;
        private string m_CurrentRoomName;
        private uint m_CurrentPortalIdx;
        private uint? m_CurrentInteriorIdx;
        private uint? m_NumInteriors;

        public DateTime m_LastLaunchTime { get; set; }
        public buildErrorCode m_BuildErr { get; set; }
        private object m_Lock = new object();
        public bool m_ShouldStop;

        public void Init()
        {
            m_BuildState = buildState.buildStateIdle;
            m_BuildErr = buildErrorCode.buildErrorNone;
            m_BuildType = buildType.buildTypeStart;
            m_ShouldStop = false;

            m_CurrentInteriorName = "";
            m_CurrentRoomName = "";
            m_CurrentPortalIdx = 0;

            // Create the progress file
            WriteToLog("Creating progress.txt file");
            StreamWriter w = new StreamWriter(App.sm_ProgressFilePath);
            w.WriteLine(0); // Current build step
            w.WriteLine(0); // Is finished
            w.WriteLine(0); // Interior hash
            w.Close();
        }

        public void StartBuild(string commandline, platformType platform, configType config, ObservableCollection<InteriorInfo> interiorList)
        {
            WriteToLog("Starting new audio occlusion build");

            m_BuildType = buildType.buildTypeStart;
            m_BuildState = buildState.buildStateLaunch;
            m_BuildErr = buildErrorCode.buildErrorNone;

            model.p4 mP4 = App.Current.FindResource("model_p4") as model.p4;
            if (mP4 != null)
            {
                mP4.CheckOutMetadataFiles(interiorList);
            }

            Build(commandline, platform, config);
        }

        public void ContinueBuild(string commandline, platformType platform, configType config)
        {
            WriteToLog("Continuing audio occlusion build");

            m_BuildType = buildType.buildTypeContinue;
            m_BuildState = buildState.buildStateLaunch;
            m_BuildErr = buildErrorCode.buildErrorNone;

            Build(commandline, platform, config);
        }

        public void BuildInteriorList(string commandline, platformType platform, configType config)
        {
            WriteToLog("Building Interior List");

            m_BuildType = buildType.buildTypeInteriorList;
            m_BuildState = buildState.buildStateLaunch;
            m_BuildErr = buildErrorCode.buildErrorNone;

            Build(commandline, platform, config);
        }

        private void Build(string commandline, platformType platform, configType config)
        {
            model.launcher mL = App.Current.FindResource("model_launcher") as model.launcher;

            while (m_BuildState != buildState.buildStateFinish)
            {
                if (GetShouldStop())
                {
                    WriteOutFailureToFile();
                    
                    if (mL != null)
                    {
                        mL.StopGame(platform);
                    }
                    return;
                }

                Update(commandline, platform, config);
            }

            if (mL != null)
            {
                mL.StopGame(platform);
            }

            model.p4 mP4 = App.Current.FindResource("model_p4") as model.p4;
            if (mP4 != null)
            {
                mP4.FinalizeChangelist();
                WriteToLog("Occlusion Build Complete");
            }
        }

        private void Update(string commandline, platformType platform, configType config)
        {
            switch (m_BuildState)
            {
                case buildState.buildStateIdle:
                case buildState.buildStateLaunch:
                    {
                        model.launcher mL = App.Current.FindResource("model_launcher") as model.launcher;
                        if (mL != null)
                        {
                            WriteToLog("Launching game");
                            mL.LaunchGame(commandline, platform, config, m_BuildType);
                            if (m_BuildType == buildType.buildTypeStart)
                            {
                                m_BuildType = buildType.buildTypeContinue;
                            }
                            m_BuildState = buildState.buildStateLaunching;
                            m_LastLaunchTime = DateTime.Now;
                        }

                        break;
                    }
                case buildState.buildStateLaunching:
                    {
                        if (m_BuildType == buildType.buildTypeInteriorList)
                        {
                            DateTime lastWriteTime = File.GetLastWriteTime(App.sm_InteriorListFile);

                            // Once it's writing the file, give it a while to finish up with the file before accessing it
                            if (lastWriteTime.CompareTo(m_LastLaunchTime) > 0 
                                && (DateTime.Now.Subtract(lastWriteTime)).CompareTo(new TimeSpan(0, 0, 10)) > 0)
                            {
                                Application.Current.Dispatcher.BeginInvoke(viewModel.MainWindowViewModel.sm_DelegateUpdateInteriorList, null);
                                SetShouldStop(true, false);
                            }
                        }
                        else
                        {
                            // Determine the last time we updated the progress file
                            DateTime lastWriteTime = File.GetLastWriteTime(App.sm_ProgressFilePath);

                            m_CurrentInteriorHash = null;

                            // See if we're still launching the game
                            if (DateTime.Compare(lastWriteTime, m_LastLaunchTime) < 0)
                            {
                                // If we're taking too long to launch the game then restart it
                                TimeSpan maxTimeToLaunchGame = new TimeSpan(0, 10, 0);
                                if ((DateTime.Now.Subtract(m_LastLaunchTime)).CompareTo(maxTimeToLaunchGame) > 0)
                                {
                                    WriteToLog("Taking longer than 10 min to launch the game");
                                    m_BuildState = buildState.buildStateStop;
                                    m_BuildErr = buildErrorCode.buildErrorTimeoutLaunch;
                                }
                            }
                            // We finished launching the game
                            else
                            {
                                WriteToLog("Game has finished launching");

                                // Update the tool with any unbuilt interiors
                                Application.Current.Dispatcher.BeginInvoke(viewModel.MainWindowViewModel.sm_DelegateUpdateUnbuiltInteriorList, null);
                                m_BuildState = buildState.buildStateRunning;
                            }
                        }

                        break;
                    }
                case buildState.buildStateRunning:
                    {
                        // Determine the last time we updated the progress file
                        DateTime lastWriteTime = File.GetLastWriteTime(App.sm_ProgressFilePath);

                        // If the game has been idle too long and the progress isn't updated, then it's probably hung, so kill it
                        TimeSpan maxTimeBetweenUpdates = new TimeSpan(0, 0, 30);
                        if ((DateTime.Now.Subtract(lastWriteTime)).CompareTo(maxTimeBetweenUpdates) > 0)
                        {
                            m_BuildErr = buildErrorCode.buildErrorCrash;
                            WriteToLog("Progress hasn't changed in 30 seconds, assuming the game has crashed");
                            m_BuildState = buildState.buildStateStop;
                        }
                        else
                        {
                            // Determine the current progress so we can update the main window display.
                            // Also we can check to see if we've been building the same interior too long, 
                            // in which case we're stuck streaming or something and need a restart.
                            // The game will write out it's progress to a file.  So we need to parse that.
                            FileStream file = File.Open(App.sm_ProgressFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                            if (file != null)
                            {
                                // Only update our info if the progress file has been updated.
                                if (lastWriteTime.CompareTo(m_LastProgressUpdate) > 0)
                                {
                                    m_LastProgressUpdate = lastWriteTime;

                                    TextReader r = new StreamReader(file);
                                    string buildStepAsString = r.ReadLine(); // Current build step
                                    string isFinishedAsString = r.ReadLine();
                                    string interiorHashAsString = r.ReadLine();
                                    string interiorName = r.ReadLine();
                                    string roomName = r.ReadLine();
                                    string portalIdxAsString = r.ReadLine();
                                    string interiorIndexAsString = r.ReadLine();
                                    string numInteriorsAsString = r.ReadLine();
                                    r.Close();
                                    file.Dispose();

                                    // If we're reading when the game is writing to Progress.txt, we may miss some values, so make sure we have everything
                                    if (buildStepAsString != null && isFinishedAsString != null && interiorHashAsString != null
                                        && interiorName != null && roomName != null && portalIdxAsString != null
                                        && interiorIndexAsString != null && numInteriorsAsString != null)
                                    {
                                        // Convert
                                        gameBuildStep buildStep = (gameBuildStep)Convert.ToUInt32(buildStepAsString);
                                        uint isFinishedAsUInt = Convert.ToUInt32(isFinishedAsString);
                                        bool isFinished = isFinishedAsUInt == 0 ? false : true;
                                        uint interiorHash = Convert.ToUInt32(interiorHashAsString);
                                        uint portalIdx = Convert.ToUInt32(portalIdxAsString);
                                        uint interiorIdx = Convert.ToUInt32(interiorIndexAsString);
                                        uint numInteriors = Convert.ToUInt32(numInteriorsAsString);

                                        // If anything changes then update the progress
                                        if (m_CurrentInteriorHash == null
                                            || interiorHash != m_CurrentInteriorHash.Value
                                            || !roomName.Equals(m_CurrentRoomName)
                                            || portalIdx != m_CurrentPortalIdx)
                                        {
                                            // Update the main view with the change
                                            Application.Current.Dispatcher.BeginInvoke(viewModel.MainWindowViewModel.sm_DelegateProgressBar,
                                                new object[] { interiorIdx, numInteriors, interiorName, roomName, portalIdx, buildStep });
                                        }

                                        // Keep track of how long we've been processing an interior, so if we're stuck due to streaming we can kill it and restart.
                                        if (m_CurrentInteriorHash == null || (interiorHash != m_CurrentInteriorHash.Value))
                                        {
                                            m_CurrentInteriorStartTime = DateTime.Now;
                                        }

                                        // Cache the new values
                                        m_BuildStep = buildStep;
                                        m_CurrentInteriorHash = interiorHash;
                                        m_CurrentInteriorName = interiorName;
                                        m_CurrentRoomName = roomName;
                                        m_CurrentPortalIdx = portalIdx;
                                        m_CurrentInteriorIdx = interiorIdx;
                                        m_NumInteriors = numInteriors;

                                        // Stop th game if we've been on the same interior too long
                                        if (m_CurrentInteriorHash.HasValue)
                                        {
                                            TimeSpan maxTimePerInterior = new TimeSpan(0, 10, 0);
                                            if ((DateTime.Now.Subtract(m_CurrentInteriorStartTime)).CompareTo(maxTimePerInterior) > 0)
                                            {
                                                WriteToLog("Exceeded 10 min building current interior, restarting and trying again");
                                                m_BuildState = buildState.buildStateStop;
                                                m_BuildErr = buildErrorCode.buildErrorTimeoutInterior;
                                            }
                                        }

                                        if (isFinished)
                                        {
                                            WriteToLog("Finished building");
                                            m_BuildState = buildState.buildStateFinish;
                                        }
                                    }
                                }
                            }
                        }

                        break;
                    }
                case buildState.buildStateStop:
                    {
                        model.launcher mL = App.Current.FindResource("model_launcher") as model.launcher;
                        if (mL != null)
                        {
                            mL.StopGame(platform);
                        }
                        m_BuildState = buildState.buildStateLaunch;

                        // Write out the error to file
                        WriteOutFailureToFile();

                        break;
                    }
            }
        }

        private void WriteOutFailureToFile()
        {
            // Backup the current console log
            if (m_BuildErr != buildErrorCode.buildErrorNone)
            {
                if (File.Exists(App.sm_LogFilePath))
                {
                    string backupFileName = App.sm_FailedInteriorLogDir + "aobConsole_" + Convert.ToString(Properties.Settings.Default.FailureIndex) + ".log";
                    File.Copy(App.sm_LogFilePath, backupFileName, true);

                    string failureName = "aobFailure_" + Convert.ToString(Properties.Settings.Default.FailureIndex);

                    XDocument xd = XDocument.Load(App.sm_FailedInteriorFile);
                    xd.Element("Failures").Add(
                        new XElement("Failure", new XAttribute("Name", failureName),
                            new XAttribute("InteriorHash", m_CurrentInteriorHash.HasValue ? m_CurrentInteriorHash.Value.ToString() : "0"),
                            new XAttribute("BuildStep", (uint)m_BuildStep),
                            new XAttribute("InteriorName", m_CurrentInteriorHash.HasValue ? m_CurrentInteriorName : ""),
                            new XAttribute("RoomName", m_CurrentInteriorHash.HasValue ? m_CurrentRoomName : ""),
                            new XAttribute("PortalIdx", m_CurrentInteriorHash.HasValue ? m_CurrentPortalIdx : 0),
                            new XAttribute("ConsoleFileName", backupFileName),
                            new XAttribute("ErrorCode", (uint)m_BuildErr)

                            )

                        );

                    xd.Save(App.sm_FailedInteriorFile);

                    Properties.Settings.Default.FailureIndex += 1;
                    Properties.Settings.Default.Save();

                    Application.Current.Dispatcher.BeginInvoke(viewModel.MainWindowViewModel.sm_DelegateLogGameStop, new object[] { failureName });
                }
            }
        }

        public void SetShouldStop(bool shouldStop, bool shouldWriteError = true)
        {
            lock (m_Lock)
            {
                m_ShouldStop = shouldStop;
                if (shouldStop)
                {
                    if (shouldWriteError)
                    {
                        m_BuildErr = buildErrorCode.buildErrorUserStoppage;
                    }
                    else
                    {
                        m_BuildErr = buildErrorCode.buildErrorNone;
                    }
                }
            }
        }

        public bool GetShouldStop()
        {
            lock (m_Lock)
            {
                return m_ShouldStop;
            }
        }
    }
}
