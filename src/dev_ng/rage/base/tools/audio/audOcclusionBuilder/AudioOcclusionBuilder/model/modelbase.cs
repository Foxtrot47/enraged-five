﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AudioOcclusionBuilder.model
{
    class modelbase
    {
        protected void WriteToLog(string line)
        {
            System.Diagnostics.Debug.WriteLine(line);
            App.Current.Dispatcher.BeginInvoke(viewModel.MainWindowViewModel.sm_DelegateLog, new object[] { line });
        }
    }
}
