﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace AudioOcclusionBuilder
{
    class InteriorInfo : INotifyPropertyChanged, IComparable
    {
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        public string InteriorName { get; set; }
        public uint InteriorHashkey { get; set; }
        public int MapSlotIndex { get; set; }
        public float PosX { get; set; }
        public float PosY { get; set; }
        public float PosZ { get; set; }
        public bool IsSelected { get; set; }
        public ObservableCollection<uint> DependentInteriorHashkeyList { get; set; }

        private bool _IsDependency;
        public bool IsDependency
        {
            get
            {
                return _IsDependency;
            }
            set
            {
                _IsDependency = value;
                NotifyPropertyChanged("IsDependency");
            }
        }

        public InteriorInfo(string interiorName, uint interiorHashkey, int mapSlotIndex, float posX, float posY, float posZ)
        {
            InteriorName = interiorName;
            InteriorHashkey = interiorHashkey;
            MapSlotIndex = mapSlotIndex;
            PosX = posX;
            PosY = posY;
            PosZ = posZ;
            IsSelected = false;
            IsDependency = false;
            DependentInteriorHashkeyList = new ObservableCollection<uint>();
        }

        public void BuildDependencies(viewModel.MainWindowViewModel viewModel, ObservableCollection<InteriorInfo> interiorList)
        {
            // Generate a list of all interiors that have inter-interior paths that use this interior
            string metaDir = viewModel.PathAssets;
            string filePath = metaDir + InteriorHashkey.ToString() + ".pso.meta";

            bool isValidFile = false;

            if (File.Exists(filePath))
            {
                // If the DestInteriorHash == m_InteriorHashkey, then the InteriorProxyHash == the interior leading into this one
                // Which will need to be rebuilt, because this interior's Intra-Interior paths may change when built.
                FileStream file = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                if (file != null)
                {
                    isValidFile = true;

                    XElement root = XElement.Load(filePath);
                    foreach (XElement portalInfoList in root.Descendants("PortalInfoList"))
                    {
                        foreach (XElement item in portalInfoList.Elements())
                        {
                            uint destInteriorHashkey = Convert.ToUInt32(item.Element("DestInteriorHash").Attribute("value").Value);
                            if (destInteriorHashkey == InteriorHashkey)
                            {
                                uint interiorHashkey = Convert.ToUInt32(item.Element("InteriorProxyHash").Attribute("value").Value);

                                // We don't need to know that the interior is dependent on itself
                                if (interiorHashkey != InteriorHashkey && !DependentInteriorHashkeyList.Contains(interiorHashkey))
                                {
                                    DependentInteriorHashkeyList.Add(interiorHashkey);
                                }
                            }
                        }
                    }
                }
            }

            if (!isValidFile)
            {
                // The interior is either new or there's an error with the file, so every interior is potentially a dependency
                foreach (InteriorInfo interior in interiorList)
                {
                    DependentInteriorHashkeyList.Add(interior.InteriorHashkey);
                }
            }
        }

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public int CompareTo(object obj)
        {
            InteriorInfo info = obj as InteriorInfo;
            if (info == null)
            {
                throw new ArgumentException("Object is not InteriorInfo");
            }
            return this.InteriorName.CompareTo(info.InteriorName);
        }
    }
}
