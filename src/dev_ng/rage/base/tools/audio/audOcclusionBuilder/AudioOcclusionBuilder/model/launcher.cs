﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;

using Rockstar.AssetManager.Interfaces;

namespace AudioOcclusionBuilder.model
{
    class launcher
    {
        public void Init()
        {

        }

        public void LaunchGame(string commandline, platformType platform, configType config, builder.buildType buildType)
        {
            if (platform == platformType.PC)
            {
                if (commandline.Contains('@'))
                {
                    commandline += " " + Properties.Settings.Default.PathExe;
                }
                else
                {
                    System.Diagnostics.Debug.Assert(false, "For PC you need to specify a commandline file");
                    Application.Current.Dispatcher.BeginInvoke(viewModel.MainWindowViewModel.sm_DelegateLog, new object[] { "ERROR: Stopping Build. For PC you need to specify a commandline file." });
                    StopGame(platformType.PC);
                    return;
                }
            }

            // Build occlusion
            commandline += " -audOcclusionBuildEnabled";

            // Point to the temp directory
            commandline += " -audOcclusionBuildTempDir=" + Environment.GetEnvironmentVariable("TEMP") + "\\audOcclusionBuilder";

            // Point to the assets directory
            commandline += " -audOcclusionBuildAssetsDir=X:" + Properties.Settings.Default.PathAssets;

            // Export the logfile specifically to this location so we're able to copy it and view it later for failed builds
            commandline += " -logfile=" + App.sm_LogFilePath;

            switch(buildType)
            {
                case builder.buildType.buildTypeStart:
                    commandline += " -audOcclusionBuildStart";
                    break;
                case builder.buildType.buildTypeContinue:
                    commandline += " -audOcclusionBuildContinue";
                    break;
                case builder.buildType.buildTypeInteriorList:
                    commandline += " -audOcclusionBuildList";
                    break;
            }

            switch (platform)
            {
                case platformType.XBOX:
                {
                    LaunchGameXBOX(commandline, config);
                    break;
                }
                case platformType.PS3:
                {
                    LaunchGamePS3(commandline, config);
                    break;
                }
                case platformType.PC:
                {
                    LaunchGamePC(commandline, config);
                    break;
                }
                case platformType.XBOXONE:
                {
                    LaunchGameXBOXONE(commandline, config);
                    break;
                }
                case platformType.PS4:
                {
                    LaunchGamePS4(commandline, config);
                    break;
                }
            }
        }

        private void LaunchGameXBOX(string commandline, configType config)
        {
            string exeName = "";
            string symbolsName = "";
            switch(config)
            {
                case configType.BETA:
                {
                    exeName = "game_xenon_beta.xex";
                    symbolsName = "game_xenon_beta.cmp";
                    break;
                }
                case configType.BANKRELEASE:
                {
                    exeName = "game_xenon_bankrelease.xex";
                    symbolsName = "game_xenon_bankrelease.cmp";
                    break;
                }
            }

            string xdkDir = Environment.GetEnvironmentVariable("XEDK") + "\\bin\\win32\\";
            string tempDir = Environment.GetEnvironmentVariable("TEMP");

            System.Diagnostics.Process newProcess;
            newProcess = System.Diagnostics.Process.Start(xdkDir + "xbdel.exe", "/r xe:\\Game");
            newProcess.WaitForExit();
            newProcess = System.Diagnostics.Process.Start(xdkDir + "xbmkdir.exe", "xe:\\Game");
            newProcess.WaitForExit();
            newProcess = System.Diagnostics.Process.Start(xdkDir + "xbcp.exe", "/y " + tempDir + "\\rfs.dat" + " xe:\\Game\\rfs.dat");
            newProcess.WaitForExit();
            newProcess = System.Diagnostics.Process.Start(xdkDir + "xbcp.exe", "/y " + Properties.Settings.Default.PathExe + "\\" + exeName + " xe:\\Game\\" + exeName);
            newProcess.WaitForExit();
            newProcess = System.Diagnostics.Process.Start(xdkDir + "xbcp.exe", "/y " + Properties.Settings.Default.PathExe + "\\" + symbolsName + " xe:\\Game\\" + symbolsName);
            newProcess.WaitForExit();

            string arguments = "xe:\\Game\\" + exeName + " " + commandline;
            newProcess = System.Diagnostics.Process.Start(xdkDir + "xbreboot.exe", arguments);
            newProcess.WaitForExit();
        }


        private void LaunchGamePS3(string commandline, configType config)
        {
            string exeName = "";
            switch (config)
            {
                case configType.BETA:
                {
                    exeName = "game_psn_beta_snc.self";
                    break;
                }
                case configType.BANKRELEASE:
                {
                    exeName = "game_psn_bankrelease_snc.self";
                    break;
                }
            }

            string ps3ToolsDir = Environment.GetEnvironmentVariable("SN_PS3_PATH");
            System.Diagnostics.Process newProcess;
            newProcess = System.Diagnostics.Process.Start(ps3ToolsDir + "\\bin\\ps3run.exe", "-r " + Properties.Settings.Default.PathExe + "\\" + exeName + " " + commandline); 
            newProcess.WaitForExit();
        }

        private void LaunchGamePC(string commandline, configType config)
        {
            string toolsBranch = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            string exeBat = "";

            switch (config)
            {
                case configType.BETA:
                    {
                        exeBat = "launch_win64_beta.bat ";
                        break;
                    }
                case configType.BANKRELEASE:
                    {
                        exeBat = "launch_win64_bankrelease.bat ";
                        break;
                    }
            }

            System.Diagnostics.Process newProcess;
            newProcess = System.Diagnostics.Process.Start("cmd.exe", "/c " + toolsBranch + "\\bin\\audio\\AudioOcclusionBuilder\\" + exeBat + commandline);
        }

        private void LaunchGameXBOXONE(string commandline, configType config)
        {
            string toolsBranch = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            string exeBat = "";

            switch (config)
            {
                case configType.BETA:
                    {
                        exeBat = "launch_durango_beta.bat ";
                        break;
                    }
                case configType.BANKRELEASE:
                    {
                        exeBat = "launch_durango_bankrelease.bat ";
                        break;
                    }
            }

            System.Diagnostics.Process newProcess;
            newProcess = System.Diagnostics.Process.Start("cmd.exe", "/c " + toolsBranch + "\\bin\\audio\\AudioOcclusionBuilder\\" + exeBat + commandline);

            // Don't wait for the process to exit as the launch.bat's are keeping their cmd windows open until there's input "/k".
            //newProcess.WaitForExit();
        }

        private void LaunchGamePS4(string commandline, configType config)
        {

        }

        public void StopGame(platformType platform)
        {
            Application.Current.Dispatcher.BeginInvoke(viewModel.MainWindowViewModel.sm_DelegateLog, new object[] { "Stopping Build" });

            switch (platform)
            {
                case platformType.XBOX:
                {
                    StopGameXBOX();
                    break;
                }
                case platformType.PS3:
                {
                    StopGamePS3();
                    break;
                }
                case platformType.PC:
                {
                    StopGamePC();
                    break;
                }
                case platformType.XBOXONE:
                {
                    StopGameXBOXONE();
                    break;
                }
                case platformType.PS4:
                {
                    StopGamePS4();
                    break;
                }
            }
        }

        private void StopGameXBOX()
        {
            string xdkDir = Environment.GetEnvironmentVariable("XEDK") + "\\bin\\win32\\";
            System.Diagnostics.Process newProcess = null;
            newProcess = System.Diagnostics.Process.Start(xdkDir + "xbreboot.exe");//, "/c");
            System.Diagnostics.Debug.Assert(newProcess != null, "Unable to reboot 360, calling wrong process");
            newProcess.WaitForExit();

            // On a cold reboot, wait 15 seconds to keep moving forward
            //System.Threading.Thread.Sleep(15000);
        }

        private void StopGamePS3()
        {
			// We don't need to actually shut the ps3 down, it'll restart when we Launch().

            //string ps3ToolsDir = Environment.GetEnvironmentVariable("SN_PS3_PATH");
            //System.Diagnostics.Process newProcess;
            //newProcess = System.Diagnostics.Process.Start(ps3ToolsDir + "\\bin\\ps3ctrl.exe", "power -t MyTarget -off"); 

            ////string ps3Dir = Environment.GetEnvironmentVariable("SCE_PS3_ROOT");
            ////System.Diagnostics.Process newProcess = null;
            ////newProcess = System.Diagnostics.Process.Start(ps3Dir + "\\host-win32\\bin\\dtpreset.exe", "-d -f");
            ////System.Diagnostics.Debug.Assert(newProcess != null, "Unable to reboot PS3, calling wrong process");
            //newProcess.WaitForExit();

            //// On a cold reboot, wait 15 seconds to keep moving forward
            //System.Threading.Thread.Sleep(15000);
        }

        private void StopGamePC()
        {
            string toolsBranch = Environment.GetEnvironmentVariable("RS_TOOLSROOT");

            System.Diagnostics.Process newProcess;            
            newProcess = System.Diagnostics.Process.Start("cmd.exe", "/c " + toolsBranch + "\\bin\\audio\\AudioOcclusionBuilder\\shutdown_win64.bat");           
        }

        private void StopGameXBOXONE()
        {
            System.Diagnostics.Debug.Assert(Directory.Exists(Environment.GetEnvironmentVariable("DurangoXDK") + "bin\\"), "Durango toolset not installed properly");

            string toolsBranch = Environment.GetEnvironmentVariable("RS_TOOLSROOT");

            System.Diagnostics.Process newProcess;
            newProcess = System.Diagnostics.Process.Start("cmd.exe", "/c " + toolsBranch + "\\bin\\audio\\AudioOcclusionBuilder\\shutdown_durango.bat");
        }

        private void StopGamePS4()
        {

        }
    }
}
