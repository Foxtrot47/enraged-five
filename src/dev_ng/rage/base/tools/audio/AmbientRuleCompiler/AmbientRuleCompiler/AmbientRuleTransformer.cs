﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;
 
using rage;
using rage.ToolLib.Logging;
using rage.Transform;

namespace rage.AmbientRuleCompiler
{

	[CanTransform("AmbientRule")]
	[CanTransform("StaticEmitter")]
    public class AmbientRuleTransformer : ITransformer
    {
        public bool Init(ILog log, params string[] args)
        {
			log.Information("AmbientRuleTransformer init");
            return true;
        }

        public TransformResult Transform(ILog log, XElement element)
        {
            TransformResult result = new TransformResult { Result = null };

			XElement startTimeFloat = element.Element("MinTime");
			XElement endTimeFloat = element.Element("MaxTime");

			if (startTimeFloat != null)
			{
				element.SetElementValue("MinTimeMinutes", (int)(float.Parse(startTimeFloat.Value) * 60));
			}

			if (endTimeFloat != null)
			{
				element.SetElementValue("MaxTimeMinutes", (int)(float.Parse(endTimeFloat.Value) * 60));
			}

			result.Result = element;
            return result;
        }

        public void Shutdown()
        {

        }
    }
}
