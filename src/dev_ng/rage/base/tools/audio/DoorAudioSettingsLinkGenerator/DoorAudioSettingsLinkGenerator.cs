﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rage.Generator;
using rage.ToolLib;
using System.Xml.Linq;

namespace rage
{
    public class DoorAudioSettingsLinkGenerator : IGenerator
    {
        #region IGenerator Members

        public bool Init(ToolLib.Logging.ILog log, string workingPath, audProjectSettings projectSettings, Types.ITypeDefinitionsManager typeDefManager, params string[] args)
        {
            return true;
        }

        public System.Xml.Linq.XDocument Generate(ToolLib.Logging.ILog log, System.Xml.Linq.XDocument inputDoc)
        {
            var outputDoc = new XDocument();
            var objsElem = new XElement("Objects");
            outputDoc.Add(objsElem);
           
            foreach (var n in inputDoc.Descendants("DoorAudioSettings"))
            {
                string objName = n.Attribute("name").Value;
                if (!objName.StartsWith("D_") || objName.Length < 3)
                {
                    continue;
                }

                string propName = objName.Substring(2);

                var objectRefHash = new Hash();
                objectRefHash.Value = propName;

                var newObjName = string.Format("DASL_{0:X8}", objectRefHash.Key);
                var newObjElem = new XElement("DoorAudioSettingsLink", new XAttribute("name", newObjName), new XAttribute("folder", propName),
                                                new XElement("DoorAudioSettings", objName));
                objsElem.Add(newObjElem);
            }

            
            return outputDoc;
        }

        public void Shutdown()
        {
            
        }

        #endregion
    }
}
