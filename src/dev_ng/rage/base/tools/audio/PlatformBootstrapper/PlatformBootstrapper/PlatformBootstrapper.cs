﻿// -----------------------------------------------------------------------
// <copyright file="PlatformBootstrapper.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace PlatformBootstrapper
{
    using System;
    using System.IO;
    using System.Xml.Linq;

    /// <summary>
    /// Platform bootstrapper.
    /// </summary>
    public class PlatformBootstrapper
    {
        /// <summary>
        /// The build path.
        /// </summary>
        private const string BuildPath = @"x:\gta5\audio\dev\build\";

        /// <summary>
        /// The source platform name.
        /// </summary>
        private readonly string sourcePlatformName;

        /// <summary>
        /// The platform name.
        /// </summary>
        private readonly string platformName;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlatformBootstrapper"/> class.
        /// </summary>
        /// <param name="sourcePlatformName">
        /// The source platform name.
        /// </param>
        /// <param name="platformName">
        /// The platform name.
        /// </param>
        public PlatformBootstrapper(string sourcePlatformName, string platformName)
        {
            this.sourcePlatformName = sourcePlatformName;
            this.platformName = platformName;
        }

        /// <summary>
        /// Run the process.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Run()
        {
            var srcBuiltWavesPath = Path.Combine(BuildPath, this.sourcePlatformName, "BuiltWaves");

            if (!Directory.Exists(srcBuiltWavesPath))
            {
                Console.WriteLine("Failed to find source directory - is source platform correct?");
                return false;
            }

            var destBuiltWavesPath = Path.Combine(BuildPath, this.platformName, "BuiltWaves");
            var destPendingWavesPath = Path.Combine(BuildPath, this.platformName, "PendingWaves");

            if (!Directory.Exists(destBuiltWavesPath))
            {
                Directory.CreateDirectory(destBuiltWavesPath);
            }

            if (!Directory.Exists(destPendingWavesPath))
            {
                Directory.CreateDirectory(destPendingWavesPath);
            }

            if (!this.SetupPendingWaves(srcBuiltWavesPath, destPendingWavesPath))
            {
                Console.WriteLine("Failed to setup pending waves");
                return false;
            }

            if (!this.SetupBuiltWaves(srcBuiltWavesPath, destBuiltWavesPath))
            {
                Console.WriteLine("Failed to setup builts waves");
                return false;
            }

            Console.WriteLine("Success! Manually add new platform settings to projectSettings.xml to finish");
            return true;
        }

        /// <summary>
        /// Setup the pending waves.
        /// </summary>
        /// <param name="srcDir">
        /// The src directory.
        /// </param>
        /// <param name="destDir">
        /// The dest directory.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool SetupPendingWaves(string srcDir, string destDir)
        {
            foreach (var file in Directory.GetFiles(srcDir, "*.xml", SearchOption.TopDirectoryOnly))
            {
                try
                {
                    var doc = XDocument.Load(file);
                    var newRoot = new XElement("PendingWaves");
                    newRoot.Add(doc.Root);
                    doc.Root.Remove();
                    doc.Add(newRoot);

                    foreach (var elem in doc.Descendants())
                    {
                        elem.SetAttributeValue("operation", "add");
                    }

                    var fileName = Path.GetFileName(file).Replace("_PACK_FILE", string.Empty);
                    var destPath = Path.Combine(destDir, fileName);
                    doc.Save(destPath);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed to setup pending waves file: " + ex.Message);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Setup the built waves.
        /// </summary>
        /// <param name="srcDir">
        /// The src directory.
        /// </param>
        /// <param name="destDir">
        /// The dest directory.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool SetupBuiltWaves(string srcDir, string destDir)
        {
            foreach (var file in Directory.GetFiles(srcDir, "*.xml", SearchOption.TopDirectoryOnly))
            {
                try
                {
                    var fileName = Path.GetFileName(file);
                    var destPath = Path.Combine(destDir, fileName);

                    if (fileName.ToUpper() == "BUILTWAVES_PACK_LIST.XML")
                    {
                        var doc = XDocument.Load(file);
                        doc.Save(destPath);
                    }
                    else
                    {
                        var doc = new XDocument();
                        var rootElem = new XElement("BuiltWaves");
                        doc.Add(rootElem);

                        doc.Save(destPath);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed to setup built waves file: " + ex.Message);
                    return false;
                }
            }

            return true;
        }
    }
}
