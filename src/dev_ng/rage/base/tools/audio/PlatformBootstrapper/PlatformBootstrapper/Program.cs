﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Platform bootsrapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlatformBootstrapper
{
    using System;

    /// <summary>
    /// Platform bootstrapper.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The main entry point.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            Console.WriteLine("*** Ensure you have grabbed latest data and there are no pending changes for source platform before you begin ***");
            Console.WriteLine();

            Console.WriteLine("Enter source platform name to copy from: ");
            var srcPlatformName = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(srcPlatformName))
            {
                Console.WriteLine("Enter a valid platform name: ");
                srcPlatformName = Console.ReadLine();
            }

            Console.WriteLine("Enter new platform name: ");
            var platformName = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(platformName))
            {
                Console.WriteLine("Enter a valid platform name: ");
                platformName = Console.ReadLine();
            }

            Console.WriteLine("Press Enter to setup new platform {0}...", platformName);
            Console.ReadKey();

            Console.WriteLine("Initializing data for new platform...");

            var bootstrapper = new PlatformBootstrapper(srcPlatformName, platformName);

            Console.WriteLine(bootstrapper.Run() ? "Failed" : "Done!");

            Console.WriteLine("Press Enter to close...");
            Console.ReadKey();
        }
    }
}
