//
// tools/audwavebuilder/wavepak.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#include "stdafx.h"

#include "wavepak.h"

audWavePak::audWavePak(String *name, String *path, audWave::audBuildCommands buildCommand) : Object()
{
	m_Name = name;
	m_FullPath = path;
	m_BuildCommand = buildCommand;
	m_BankList = new ArrayList();
	m_Compression = -1;
	m_SampleRate = -1;
	m_StreamingBlockBytes = 0;
}

void audWavePak::Build(String *outputPath, audBuildClient *buildClient, int totalWaves, int &wavesBuilt,
	DateTime waveBuildStartDateTime,bool isDeferredBuild)
{
	String *pakFolderPath = String::Concat(outputPath, m_Name, S"\\");

	//Ensure that output Wave Pak folder exist locally.
	Directory::CreateDirectory(pakFolderPath);

	System::Collections::IEnumerator* bankEnum = m_BankList->GetEnumerator();
	while(bankEnum->MoveNext())
	{
		audWaveBank *waveBank = __try_cast<audWaveBank *>(bankEnum->Current);

		if(waveBank->m_BuildCommand != audWave::audBuildCommands::REMOVE)
		{
			waveBank->Build(pakFolderPath, m_Name, m_Compression, m_SampleRate, m_StreamingBlockBytes, buildClient,
				totalWaves, wavesBuilt, waveBuildStartDateTime,isDeferredBuild);
		}
	}
}
