//
// tools/audwavebuilder/wavebank.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef WAVE_BANK_H
#define WAVE_BANK_H

#include "wave.h"

#include "audiohardware/wavedefs.h"

using namespace audBuildCommon;
using namespace rage;
using namespace System;

//
// PURPOSE
//	A simple managed wrapper that allows load block seek table entries to be added to a managed ArrayList.
//
public __gc class audLoadBlockSeekTableEntryWrapper : public Object
{
public:
	audLoadBlockSeekTableEntryWrapper(audLoadBlockSeekTableEntry *entry)
	{
		m_Entry = entry;
	}
	audLoadBlockSeekTableEntry *GetEntry(void)
	{
		return m_Entry;
	}

protected:
	audLoadBlockSeekTableEntry *m_Entry;
};

//
// PURPOSE
//  Represents a physical Wave Bank resource throughout the build process. Writes itself to the specified Wave
//	Pak folder (partially via cascaded requests to the constituent Waves.)
// SEE ALSO
//  audWave, audWavePak
//
public __gc class audWaveBank : public Object
{
public:
	//
	// PURPOSE
	//  Initializes member variables.
	// PARAMS
	//  name			- The formalized name of the physical Wave Bank folder on the local filesystem.
	//  path			- The full path of the physical Wave Bank folder on the local filesystem.
	//	buildCommand	- Defines the command that applies to this Wave Bank resource for this build (add, modify or
	//						remove.)
	//
	audWaveBank(String *name, String *path, audWave::audBuildCommands buildCommand);
	//
	// PURPOSE
	//  Accessor function that returns the number of Waves contained in this Wave Bank.
	// RETURNS
	//  The number of Waves in this Wave Bank.
	//
	int GetNumWaves(void)
	{
		int NumWaves=0;

		if(m_WaveList)
		{
			NumWaves =  m_WaveList->get_Count();
		}

		return NumWaves;
	}
	//
	// PURPOSE
	//  Processes any pending build commands on this Wave Bank and in constituent Waves, adding or removing itself,
	//	or reconfiguring the child Waves.
	// PARAMS
	//  pakFolderPath			- The path of the Wave Pak folder into which this Wave Bank is to be written.
	//	parentName				- The name of the parent Wave Pak.
	//	compression				- The compression setting to be applied to the constituent Waves.
	//	sampleRate				- The sample rate setting to be applied to the constituent Waves.
	//	streamingBlockBytes		- The block size to be used when interleaving the constituent Waves, or 0 if the Waves are
	//								to be concatenated.
	//	buildClient				- An encapsulation of the connection to the build client.
	//	totalWaves				- The total number of waves to be built, for use in progress reporting.
	//	wavesBuilt				- The number of the waves currently built, for use in progress reporting.
	//								(Also a return parameter).
	//  waveBuildStartDateTime	- The date and time at which the wave build was started, for use in progress
	//								reporting.
	// SEE ALSO
	//	audWave
	//
	void Build(String *pakFolderPath, String *parentName, int compression, int sampleRate,
		unsigned int streamingBlockBytes, audBuildClient *buildClient, int totalWaves, int &wavesBuilt,
		DateTime waveBuildStartDateTime,bool isDeferredBuild);
	//
	// PURPOSE
	//  An accessor function for the compression member variable. Ensures that the compression setting is never overridden
	//	once set, as this function is guaranteed to be called from the bottom-upwards in the wave resource hierachy - and
	//	children always override their parents with this parameter.
	// PARAMS
	//	compression	- A Wave compression level to be set (and later propagated to the constituent Waves when built.)
	//
	void SetCompression(int compression)
	{
		if(m_Compression == -1)
		{
			m_Compression = compression;
		}
	}
	//
	// PURPOSE
	//  An accessor function for the sample rate member variable. Ensures that the sample rate setting is never overridden
	//	once set, as this function is guaranteed to be called from the bottom-upwards in the wave resource hierachy - and
	//	children always override their parents with this parameter.
	// PARAMS
	//	sampleRate	- A Wave sample rate to be set (and later propagated to the constituent Waves when built.)
	//
	void SetSampleRate(int sampleRate)
	{
		if(m_SampleRate == -1)
		{
			m_SampleRate = sampleRate;
		}
	}
	//
	// PURPOSE
	//  An accessor function for the interleave bytes member variable. Ensures that this setting is never overridden
	//	once set, as this function is guaranteed to be called from the bottom-upwards in the wave resource hierachy - and
	//	children always override their parents with this parameter.
	// PARAMS
	//	streamingBlockBytes	- The block size to be used when interleaving Waves within this Bank, or 0 if the Waves are
	//							to be concatenated (propagated to the constituent Waves when built.)
	//
	void SetStreamingBlockBytes(unsigned int streamingBlockBytes)
	{
		if(m_StreamingBlockBytes == 0)
		{
			m_StreamingBlockBytes = streamingBlockBytes;
		}
	}
	//
	// PURPOSE
	//	Determines if the Bank is constructed of interleaved Waves.
	// RETURNS
	//	Returns true if the Bank is constructed of interleaved Waves.
	//
	bool IsInterleaved(void)
	{
		return (m_StreamingBlockBytes > 0);
	}

	int m_MetadataSizeBytes, m_WaveMetadataLookupSizeBytes, m_CustomMetadataLookupSizeBytes, m_BuiltSize;
	String *m_Name, *m_FullPath;
	ArrayList *m_WaveList;
	audWave::audBuildCommands m_BuildCommand;

private:
	//
	// PURPOSE
	//  Handles the process of 'adding' this Wave Bank. This is achieved by building the Bank data and header from
	//	scratch.
	// PARAMS
	//  pakFolderPath			- The path of the Wave Pak folder into which this Wave Bank is to be written.
	//	compression				- The compression setting to be applied to the constituent Waves.
	//	sampleRate				- The sample rate setting to be applied to the constituent Waves.
	//	wavesBuilt				- The number of the waves currently built, for use in progress reporting.
	//								(Also a return parameter).
	// SEE ALSO
	//	audWave
	//
	void AddWaveBank(String *pakFolderPath, int compression, int sampleRate, int &wavesBuilt);
	//
	// PURPOSE
	//  Handles the process of 'adding' this streaming Wave Bank. This is achieved by building the Bank data and header
	//	from scratch.
	// PARAMS
	//  pakFolderPath			- The path of the Wave Pak folder into which this Wave Bank is to be written.
	//	compression				- The compression setting to be applied to the constituent Waves.
	//	sampleRate				- The sample rate setting to be applied to the constituent Waves.
	//	streamingBlockBytes		- The block size to be used when interleaving the constituent Waves, or 0 if the Waves are
	//								to be concatenated.
	//	wavesBuilt				- The number of the waves currently built, for use in progress reporting.
	//								(Also a return parameter).
	// SEE ALSO
	//	audWave
	//
	void AddStreamingWaveBank(String *pakFolderPath, int compression, int sampleRate, unsigned int streamingBlockBytes,
		int &wavesBuilt);
	//
	// PURPOSE
	//  Interleaves Wave and data streams within a streaming Wave Bank.
	// PARAMS
	//	waveBankMetadata				- The Wave Bank metadata defining the contents of the streaming Bank.
	//	waveMetadataLists				- An array of lists, each containing the metadata for a constituent Wave.
	//	waveSampleDataLists				- An array of lists, each containing the sample data for a constituent Wave.
	//	interleavedStreamDataList		- Used to return the interleaved Wave and data streams.
	//
	void InterleaveStreams(audStreamingWaveBankMetadata *waveBankMetadata, audByteArrayList *waveMetadataLists[],
		audByteArrayList *waveSampleDataLists[], audByteArrayList *interleavedStreamDataList);
	//
	// PURPOSE
	//  Interleaves Wave and data streams into a load block for storage within a streaming Wave Bank.
	// PARAMS
	//	waveBankMetadata			- The Wave Bank metadata defining the contents of the streaming Bank.
	//	blockMetadata				- Used to store the metadata associated with the generated load block.
	//	numPacketsPerBlock			- The number of packets that can be accomodated within a load block.
	//	fullPacketsAddedPerWave		- The number of full packets added to the streaming bank for each constituent Wave.
	//	waveSamplesToSkipNextPacket	- The number of samples of Wave data that should be skipped for the first packet in the
	//									next load block for each constituent Wave.
	//	waveMetadataArray			- Metadata associated with each of the constituent Waves.
	//	waveSampleDataLists			- An array of lists, each containing the sample data for a constituent Wave.
	//	outputDataList				- Used to return the concatenated blocks of interleaved streams (and block headers.)
	// RETURNS
	//	The seek table entry for the generated load block, or NULL if the last block has already been generated.
	//
	audLoadBlockSeekTableEntry *GenerateInterleavedBlock(audStreamingWaveBankMetadata *waveBankMetadata,
		audStreamingBlockMetadata *blockMetadata, unsigned int numPacketsPerBlock, unsigned int *fullPacketsAddedPerWave,
		unsigned int *waveSamplesToSkipNextPacket, audWaveMetadataBase **waveMetadataArray,
		audByteArrayList *waveSampleDataLists[], audByteArrayList *outputDataList);
	//
	// PURPOSE
	//  Calculates the start sample index of a packet of Wave data.
	// PARAMS
	//	packetIndex		- The index of the packet for which the start sample index is required.
	//	waveMetadata	- The metadata for the Wave stored within the stream of packets.
	// RETURNS
	//	The packet start sample index, or -1 if packetIndex is beyond the end of the Wave data.
	//
	int ComputePacketStartSampleIndex(unsigned int packetIndex, audWaveMetadataBase *waveMetadata);
	//
	// PURPOSE
	//  Calculates the end sample index of a packet of Wave data.
	// PARAMS
	//	packetIndex		- The index of the packet for which the end sample index is required.
	//	waveMetadata	- The metadata for the Wave stored within the stream of packets.
	// RETURNS
	//	The packet end sample index, or -1 if packetIndex is beyond the end of the Wave data.
	//
	int ComputePacketEndSampleIndex(unsigned int packetIndex, audWaveMetadataBase *waveMetadata);
	//
	// PURPOSE
	//  Calculates the combined size, in bytes, of the MP3 frames packed into the specified streaming packet.
	// PARAMS
	//	packetIndex		- The index of the packet for which the combined MP3 frame size is required.
	//	waveMetadata	- The metadata for the Wave stored within the stream of packets.
	//	numFrames		- Used to return the number of MP3 frames that could be packed into the specified streaming
	//						packet.
	// RETURNS
	//	The combined size, in bytes, of the packed MP3 frames, in bytes, or -1 if packetIndex is beyond the
	//	end of the Wave data.
	//
	int ComputeMp3PacketSize(unsigned int packetIndex, audWaveMetadataBase *waveMetadata, unsigned int &numFrames);
	//
	// PURPOSE
	//  Calculates the MP3 frame offset, in bytes, that corresponds to the specified streaming packet
	//	index, given that an arbitrary number of frames can be packed into a single streaming packet.
	// PARAMS
	//	packetIndex		- The index of the packet for which the equivalent MP3 frame offset is required.
	//	waveMetadata	- The metadata for the Wave stored within the stream of packets.
	// RETURNS
	//	The MP3 frame offset, in bytes, or -1 if packetIndex is beyond the end of the Wave data.
	//
	int ComputeMp3FrameOffsetFromPacket(unsigned int packetIndex, audWaveMetadataBase *waveMetadata);
	//
	// PURPOSE
	//  Calculates the MP3 frame index that corresponds to the specified streaming packet index, given that
	//	an arbitrary number of frames can be packed into a single streaming packet.
	// PARAMS
	//	packetIndex		- The index of the packet for which the equivalent MP3 frame index is required.
	//	waveMetadata	- The metadata for the Wave stored within the stream of packets.
	// RETURNS
	//	The MP3 frame index, or -1 if packetIndex is beyond the end of the Wave data.
	//
	int ComputeMp3FrameIndexFromPacket(unsigned int packetIndex, audWaveMetadataBase *waveMetadata);
	//
	// PURPOSE
	//  Calculates the start time of a packet of Wave data, in milliseconds.
	// PARAMS
	//	packetIndex		- The index of the packet for which the start time is required.
	//	waveMetadata	- The metadata for the Wave stored within the stream of packets.
	// RETURNS
	//	The packet start time, in milliseconds, or -1 if packetIndex is beyond the end of the Wave data.
	//
	float ComputePacketStartTimeMs(unsigned int packetIndex, audWaveMetadataBase *waveMetadata);
	//
	// PURPOSE
	//  Calculates the end time of a packet of Wave data, in milliseconds.
	// PARAMS
	//	packetIndex		- The index of the packet for which the end time is required.
	//	waveMetadata	- The metadata for the Wave stored within the stream of packets.
	// RETURNS
	//	The packet end time, in milliseconds, or -1 if packetIndex is beyond the end of the Wave data.
	//
	float ComputePacketEndTimeMs(unsigned int packetIndex, audWaveMetadataBase *waveMetadata);
	//
	// PURPOSE
	//  Calculates the number of samples that correspond to a time period, in milliseconds, for a Wave with the given
	//	metadata.
	// PARAMS
	//	numMilliseconds	- The time period to be converted to samples, in milliseconds.
	//	waveMetadata	- The metadata for the Wave stored within the stream of packets.
	// RETURNS
	//	The number of samples that correspond to the specified time period for this Wave.
	//
	unsigned int ComputeSamplesFromMs(float numMilliseconds, audWaveMetadataBase *waveMetadata);
	//
	// PURPOSE
	//  Writes a block of interleaved Wave and data streams, along with a block header, into the specified audByteArrayList.
	// PARAMS
	//	waveBankMetadata			- The Wave Bank metadata defining the contents of the streaming Bank.
	//	blockMetadata				- The metadata associated with the load block to be written.
	//	interleavedStreamDataList	- A block of interleaved Wave and data streams.
	//	numPacketsPerBlock			- The number of packets that are accomodated within the load block.
	//	outputDataList				- Used to return the concatenated blocks of interleaved streams (and block headers.)
	//
	void WriteInterleavedBlock(audStreamingWaveBankMetadata *waveBankMetadata, audStreamingBlockMetadata *blockMetadata,
		audByteArrayList *interleavedStreamDataList, unsigned int numPacketsPerBlock, audByteArrayList *outputDataList);
	//
	// PURPOSE
	//  Writes the constituent parts of the header and the sample data to a Wave Bank.
	// PARAMS
	//  pakFolderPath					- The path of the Wave Pak folder into which this Wave Bank is to be written.
	//	waveBankMetadata				- The Wave Bank metadata to be written to the Bank header.
	//	waveBankMetadataBytes			- The size of the Wave Bank metadata, in bytes.
	//	waveMetadataLookup				- The Wave metadata lookup table to be written to the Bank header.
	//	waveMetadataLookupBytes			- The size of the Wave metadata lookup table, in bytes.
	//	waveMetadataList				- A managed list containing the Bank's concatenated Wave metadata.
	//	customMetadataLookup			- The custom metadata lookup table to be written to the Bank header.
	//	customMetadataLookupBytes		- The size of the custom metadata lookup table, in bytes.
	//	customMetadataList				- A managed list containing the Bank's concatenated custom metadata.
	//	loadBlockSeekTable				- The load block seek table to be written to the Bank header.
	//	loadBlockSeekTableBytes			- The size of the load block seek table, in bytes.
	//	headerPaddingBytes				- The amount of padding to be added to the end of the Bank header in order to
	//										satisfy any sample data alignment constraint.
	//	waveSampleDataList				- A managed list containing the Bank's concatenated Wave sample data.
	//
	void WriteWaveBank(String *pakFolderPath, void *waveBankMetadata, unsigned int waveBankMetadataBytes,
		audMetadataLookupEntry *waveMetadataLookup, unsigned int waveMetadataLookupBytes,
		audByteArrayList *waveMetadataList, audMetadataLookupEntry *customMetadataLookup,
		unsigned int customMetadataLookupBytes, audByteArrayList *customMetadataList,
		audLoadBlockSeekTableEntry *loadBlockSeekTable, unsigned int loadBlockSeekTableBytes,
		unsigned int headerPaddingBytes, audByteArrayList *waveSampleDataList);
	//
	// PURPOSE
	//  Handles the process of reconfiguring the child Waves within this Bank. This is achieved by loading the
	//	previously built Bank and adding, removing or modifying Waves as appropriate.
	// PARAMS
	//  pakFolderPath			- The path of the Wave Pak folder into which this Wave Bank is to be written.
	//	compression				- The compression setting to be applied to the constituent Waves.
	//	sampleRate				- The sample rate setting to be applied to the constituent Waves.
	//	wavesBuilt				- The number of the waves currently built, for use in progress reporting.
	//								(Also a return parameter).
	// SEE ALSO
	//	audWave
	//
	void ReconfigureWaves(String *pakFolderPath, int compression, int sampleRate, int &wavesBuilt);
	//
	// PURPOSE
	//  Handles the process of adding a single child Wave to this Bank.
	// PARAMS
	//	wave				- The encapsulated Wave to be added.
	//	compression			- The compression setting to be applied to the Wave.
	//	sampleRate			- The sample rate setting to be applied to the Wave.
	//	waveBankMetadata	- The Wave Bank metadata contained in the Bank header prior to actioning the current
	//							command.
	//	waveMetadataLookup	- A pointer to the Wave metadata lookup table contained in the Bank header prior to
	//							actioning the current command.
	//	waveMetadataList	- A managed list containing the Bank's concatenated Wave metadata.
	//	customMetadataLookup- A pointer to the custom metadata lookup table contained in the Bank header prior to
	//							actioning the current command.
	//	customMetadataList	- A managed list containing the Bank's concatenated custom metadata.
	//	waveSampleDataList	- A managed list containing the Bank's concatenated Wave sample data.
	// NOTES
	//	The Wave metadata and sample data is concatenated onto the end of any existing metadata and sample data
	//	whilst it's lookup entry is added to the Bank metadata (in the correct order by hashed name.)
	// SEE ALSO
	//	audWave
	//
	void AddWaveToBank(audWave *wave, int compression, int sampleRate, audWaveBankMetadata *waveBankMetadata,
		audMetadataLookupEntry **waveMetadataLookup, audByteArrayList *waveMetadataList,
		audMetadataLookupEntry **customMetadataLookup, audByteArrayList *customMetadataList,
		audByteArrayList *waveSampleDataList);
	//
	// PURPOSE
	//  Handles the process of removing a single child Wave from this Bank.
	// PARAMS
	//	wave				- The encapsulated Wave to be removed.
	//	waveBankMetadata	- The Wave Bank metadata contained in the Bank header prior to actioning the current
	//							command.
	//	waveMetadataLookup	- A pointer to the Wave metadata lookup table contained in the Bank header prior to
	//							actioning the current command.
	//	waveMetadataList	- A managed list containing the Bank's concatenated Wave metadata.
	//	customMetadataLookup- A pointer to the custom metadata lookup table contained in the Bank header prior to
	//							actioning the current command.
	//	customMetadataList	- A managed list containing the Bank's concatenated custom metadata.
	//	waveSampleDataList	- A managed list containing the Bank's concatenated Wave sample data.
	// SEE ALSO
	//	audWave
	//
	void RemoveWaveFromBank(audWave *wave, audWaveBankMetadata *waveBankMetadata,
		audMetadataLookupEntry **waveMetadataLookup, audByteArrayList *waveMetadataList,
		audMetadataLookupEntry **customMetadataLookup, audByteArrayList *customMetadataList,
		audByteArrayList *waveSampleDataList);

	int m_Compression;
	int m_SampleRate;
	unsigned int m_StreamingBlockBytes;
};

#endif // WAVE_BANK_H
