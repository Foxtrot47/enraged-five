//
// tools/audwavebuilder/wavebuilder.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef WAVE_BUILDER_H
#define WAVE_BUILDER_H

using namespace audAssetManagement;
using namespace audBuildCommon;
using namespace Microsoft::Win32;
using namespace rage;
using namespace System;
using namespace System::Collections;
using namespace System::Diagnostics;
using namespace System::IO;
using namespace System::Net::Sockets;
using namespace System::Xml;

namespace audWaveBuild
{
	//
	// PURPOSE
	//  Performs the full wave asset build, including interaction with the asset management system.
	// SEE ALSO
	//  audAssetManager, audWavePak, audWaveBank, audWave, audBuildException, audPlatformSpecific
	//
	public __gc class audWaveBuilder : public audBuilderBase
	{
	public:
		audWaveBuilder();
		//
		// PURPOSE
		//  Build the Wave assets for the project specified in the projectSettings.
		// PARAMS
		//  assetManager		- The Asset Manager instance to be used for accessing the requisite audio assets.
		//  projectSettings		- Defines the project for which audio assets are to be built.
		//	buildPlatform		- The name of the target build platform.
		//	buildClient			- An encapsulation of the connection to the build client.
		//	pendingWavesXml		- An XML document containing a list of all Waves assets to be built (with associated
		//							metadata tags.)
		//  builtWavesXml		- An XML document containing a list of all currently built Waves assets (with associated
		//							metadata tags.)
		//	shouldBuildLocally	- If true, the build is to be performed locally, which may require special handling.
		//	buildComponent		- An encapsulation of this build step that includes custom settings.
		//
		void Build(audAssetManager *assetManager, audProjectSettings *projectSettings, String *buildPlatform,
			audBuildClient *buildClient, XmlDocument *pendingWavesXml, XmlDocument *builtWavesXml, bool shouldBuildLocally,
			audBuildComponent *buildComponent, bool bDeferredBuild);

	private:
		//Define the levels in a wave resource hierarchy.
		__value enum audWaveResourceLevels
		{
			WAVE_PAK,
			WAVE_BANK,
			WAVE,
			NUM_LEVELS
		};

		__value enum hasTag
		{
			DIRECT_CHILD,
			DISTANT_CHILD,
			NO
		};
		//
		// PURPOSE
		//  Initializes the Asset Manager working paths.
		// SEE ALSO
		//  audAssetManager
		//
		void Init(void);
		//
		// PURPOSE
		//  Parses the XML Pending Waves List produced by the Wave Editor and the XML Built Waves Log to generate a Wave
		//	resource hierarchy that details the resources to be added, modified and removed. Also propagates any new XML
		//	changes to the Built Waves Log.
		//
		void ParsePendingAndBuiltWaveXml(void);
		// PURPOSE
		//  Combines the XML nodes of the Pending Waves List and the Built Waves Log to generate a complete picture of the
		//	wave resources (and Tags) with any associated modification operations.
		//
		void CombinePendingAndBuiltWaveXml(void);
		// PURPOSE
		//  Merges a single XML node from the Pending Waves List with a corresponding node in the Built Waves Log, or
		//	simply adds the new node if a corresponding node cannot be found.
		// PARAMS
		//  pendingNode		- The XML node from the Pending Wave List to be merged.
		//  builtNode		- The XML node from the Built Waves Log to be merged.
		//
		void MergePendingWaveNode(XmlNode *pendingNode, XmlNode *builtNode);
		// PURPOSE
		//  Parses the combined Pending Waves List and Built Waves Log XML to generate a Wave resource hierarchy that
		//	details the resources to be added, modified and removed.
		//
		void ParseCombinedPendingAndBuiltWaveXml(void);
		// PURPOSE
		//  Parses a single node from the combined Pending Waves List and Built Waves Log XML (recursing down all child
		//	nodes) and adds any wave resources to be added, modified and removed to the resource hierarchy used to manage
		//	the build process.
		// PARAMS
		//  node						- The XML node from the (combined) Built Waves Log to be parsed.
		//  parentWaveResourceList		- The appropriate level in the Wave resource hierachy at which any parsed resource
		//									changes should be added.
		//	fullWaveResourcePath		- The full path of any wave resources detailed within this node (less their names.)
		//	parentCompression			- Used to return the value of any compression tag found within this node, or -1 if
		//									no compression tag is found.
		//	parentSampleRate			- Used to return the value of any sampleRate tag found within this node, or -1 if
		//									no sampleRate tag is found.
		//	parentStreamingBlockBytes	- Used to return the value of any streamingBlockBytes tag found within this node,
		//									or -1 if no streamingBlockBytes tag is found.
		//	shouldModifyParent			- Used to return an indication as to whether the parent node should be modified,
		//									based upon whether or not compression Tags belonging to that node were
		//									modified.
		//
		void ParseCombinedPendingAndBuiltWaveNode(XmlNode *node, ArrayList *parentWaveResourceList,
			String *fullWaveResourcePath, int &parentCompression, int &parentSampleRate,
			unsigned int &parentStreamingBlockBytes, bool &shouldModifyParent);
		//
		// PURPOSE
		//  Deletes any XML nodes that are marked for removal and clears all 'operation' attributes from the (combined)
		//	Built Waves Log XML document.
		//
		void TrimBuiltWavesXml(void);
		// PURPOSE
		//  Clears all 'operation' attributes from a single node and all its child nodes from the (combined) Built Waves
		//	Log XML document. Also informs the parent node whether or not this node requires removal (based upon the
		//	presence of a 'remove' operation.)
		// PARAMS
		//  node				- The XML node from the (combined) Built Waves Log to be trimmed.
		//	shouldRemoveNode	- Used to return and indication as to whether the parent node needs to remove this node.
		//
		void TrimBuiltWavesNode(XmlNode *node, bool &shouldRemoveNode);
		//
		// PURPOSE
		//  Exclusively checks-out the assets to be modified and gets the latest wave data from the asset management
		//	system.
		// SEE ALSO
		//  audAssetManager
		//
		void PrepareAssets(void);
		//
		// PURPOSE
		//  Forces a specified directory to be writeable, along with all files and folders within (called recursively).
		// PARAMS
		//	dirInfo	- A DirectoryInfo instance representing a physical directory to be made writeable.
		//
		void MakeDirectoryWriteable(DirectoryInfo *dirInfo);
		//
		// PURPOSE
		//  Generates the Wave Banks in the associated Wave Pak folders.
		// NOTES
		//	Buffered file output is used to reduce disk access and improve the build speed at the expense of
		//	system memory.
		// SEE ALSO
		//  audWavePak
		//
		void BuildWaveBanks(void);
		//
		// PURPOSE
		//  Adds the raw and compressed Wave sizes (plus the Wave and Bank metadata and maximum Bank header sizes) to the
		//	Built Waves Log XML document.
		//
		void AddWaveSizesToBuiltWavesXml(void);
		//
		// PURPOSE
		//  Searches the children of the specified parent XML node for a Wave Bank node with the specified name.
		// PARAMS
		//	parentNode	- The node for which the child nodes are to be searched.
		//	name		- The name of the Wave Bank node to be located.
		// RETURNS
		//	The located Wave Bank node, or NULL if the node could not be found.
		//
		XmlNode *FindWaveBankNode(XmlNode *parentNode, String *name);
		//
		// PURPOSE
		//  Searches the children of the specified parent XML node for a Wave node with the specified name.
		// PARAMS
		//	parentNode	- The node for which the child nodes are to be searched.
		//	name		- The name of the Wave node to be located.
		// RETURNS
		//	The located Wave node, or NULL if the node could not be found.
		//
		XmlNode *FindWaveNode(XmlNode *parentNode, String *name);
		//
		// PURPOSE
		//  Undoes the check-out of all assets from the asset management system.
		// SEE ALSO
		//  audAssetManager
		//
		void ReleaseAssets(void);
		//
		// PURPOSE
		//  Checks-in all built assets and imports any new Wave Paks.
		// SEE ALSO
		//  audAssetManager
		//
		void CommitAssets(void);
		//
		// PURPOSE
		//  Restores the original Asset Manager working paths.
		// SEE ALSO
		//  audAssetManager
		//
		void Shutdown(void);

		//
		// PURPOSE
		// Checks if a pack has a deferredBuild Tag either in the pending wave list or built wave list
		hasTag HasDeferredTag(XmlNode *pendingNode,XmlNode *builtNode,int level);
		//
		// PURPOSE
		// Given a Node, this function checks through the nodes descendants for a deferredBuild Tag
		hasTag FindTag(XmlNode *parentNode, String *name, int level);
		//
		// PURPOSE
		// Calls DeleteNode on each Pending Node
		void DeletePendingNode(XmlNode *nodeToDel);
		//
		// PURPOSE
		// If the node to delete is the pending node or any of its decendants it is deleted from the pending wave list
		bool DeleteNode(XmlNode *nodeToDel, XmlNode *pendingNode);
		
		
		
		bool m_IsDeferredBuild;
		bool m_ShouldBuildLocally;
		int m_TotalWaves;
		String *m_BuildPlatform;
		String *m_LocalAudioDataPath;
		String *m_AssetRawWavesPath, *m_LocalRawWavesPath, *m_AssetBuildDataPath, *m_LocalBuildDataPath;
		ArrayList *m_WaveBuildList;
		ArrayList *m_PendingNodesForDeletion;
		XmlDocument *m_PendingWavesXml, *m_BuiltWavesXml;

		audAssetManager *m_AssetManager;
		audProjectSettings *m_ProjectSettings;
		audBuildClient *m_BuildClient;
	};
		
		

} //namespace audWaveBuild

#endif // WAVE_BUILDER_H
