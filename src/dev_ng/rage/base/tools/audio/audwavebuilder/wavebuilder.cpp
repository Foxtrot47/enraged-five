//
// tools/audwavebuilder/wavebuilder.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//
// This is the main DLL file.

#include "stdafx.h"

#include "wavebuilder.h"

#include "wavepak.h"

#include "audbuildmanager/builddefs.h"
#include "audassetmanager/assetmanagertypes.h"

using namespace System::Text;

namespace audWaveBuild {

audWaveBuilder::audWaveBuilder()
{

}

void audWaveBuilder::Build(audAssetManager *assetManager, audProjectSettings *projectSettings, String *buildPlatform,
	audBuildClient *buildClient, XmlDocument *pendingWavesXml, XmlDocument *builtWavesXml, bool shouldBuildLocally,
	audBuildComponent *UNUSED_PARAM(buildComponent), bool isDeferredBuild)
{
	//throw new audBuildException("");

	m_AssetManager = assetManager;
	m_ProjectSettings = projectSettings;
	m_BuildPlatform = buildPlatform;
	m_BuildClient = buildClient;
	m_PendingWavesXml = pendingWavesXml;
	m_BuiltWavesXml = builtWavesXml;
	m_ShouldBuildLocally = shouldBuildLocally;
	m_IsDeferredBuild = isDeferredBuild;
	m_PendingNodesForDeletion = new ArrayList();

	try
	{
		m_BuildClient->ReportProgress(-1, -1, S"Starting Wave build", true);
		Init();
		ParsePendingAndBuiltWaveXml();
		PrepareAssets();
		BuildWaveBanks();
		AddWaveSizesToBuiltWavesXml();
		CommitAssets();
		Shutdown();

		m_BuildClient->ReportProgress(-1, -1, S"Finished Wave build", true);
	}
	catch(audBuildException *e)
	{
		ReleaseAssets();
		Shutdown();

		//Pass internal exceptions upwards to Build Manager.
		throw(e);
	}
	catch(Exception *e)
	{
		ReleaseAssets();
		Shutdown();

		//Pass internal exceptions upwards to Build Manager.
		audBuildException *exception = new audBuildException(
			String::Concat(S"Error writing Wave Banks - ", e->ToString()));
		throw(exception);
	}
}

void audWaveBuilder::Init(void)
{
	m_WaveBuildList = new ArrayList();

	audBuildCommon::audPlatformSpecific::InitClass(m_BuildPlatform);

	//Generate commonly-used paths.
	m_AssetRawWavesPath = m_ProjectSettings->GetWaveInputPath();
	m_LocalRawWavesPath = m_AssetManager->GetWorkingPath(m_AssetRawWavesPath);
	if(m_LocalRawWavesPath == 0)
	{
		audBuildException *exception = new audBuildException(S"Failed to find working path for raw Waves folder");
		throw(exception);
	}

	m_AssetBuildDataPath = m_ProjectSettings->GetBuildOutputPath();
	m_LocalBuildDataPath = m_AssetManager->GetWorkingPath(m_AssetBuildDataPath);
	if(m_LocalBuildDataPath == 0)
	{
		audBuildException *exception = new audBuildException(S"Failed to find working path for Build Data folder");
		throw(exception);
	}
}

void audWaveBuilder::ParsePendingAndBuiltWaveXml(void)
{
	m_TotalWaves = 0;

	m_BuildClient->ReportProgress(-1, -1, S"Parsing Pending and Built Wave Lists", true);

	CombinePendingAndBuiltWaveXml();
	ParseCombinedPendingAndBuiltWaveXml();
	TrimBuiltWavesXml();

	System::Collections::IEnumerator *nodeEnum = m_PendingNodesForDeletion->GetEnumerator();
	while(nodeEnum->MoveNext())
	{
		XmlNode *node = __try_cast<XmlNode *>(nodeEnum->Current);
		DeletePendingNode(node);
	}
	//m_PendingWavesXml->DocumentElement->RemoveAll();
}

void audWaveBuilder::DeletePendingNode(XmlNode *nodeToDel)
{
	XmlNode *pendingNode = m_PendingWavesXml->DocumentElement->get_FirstChild();
	bool deleted = false;
	while(pendingNode!=0 && deleted!=true)
	{
		deleted = DeleteNode(nodeToDel, pendingNode);
		pendingNode = pendingNode->NextSibling;
	}
}

bool audWaveBuilder::DeleteNode(XmlNode *nodeToDel, XmlNode *pendingNode)
{
	if(nodeToDel->Equals(pendingNode))
	{
		pendingNode->ParentNode->RemoveChild(pendingNode);
		return true;
	}
	else
	{
		XmlNode *childNode = pendingNode->get_FirstChild();
		while(childNode)
		{
			if(!DeleteNode(nodeToDel, childNode))
			{
				childNode = childNode->NextSibling;
			}
			else
			{
				return true;
			}
		}
	}
	return false;
}

void audWaveBuilder::CombinePendingAndBuiltWaveXml(void)
{
	XmlNode *pendingNode = m_PendingWavesXml->DocumentElement->get_FirstChild();
	while(pendingNode)
	{
		MergePendingWaveNode(pendingNode, m_BuiltWavesXml->DocumentElement);
		pendingNode = pendingNode->NextSibling;
	}
}



void audWaveBuilder::MergePendingWaveNode(XmlNode *pendingNode, XmlNode *builtNode)
{
	//Extract Pending node 'name'.
	XmlAttribute *nameAttribute = pendingNode->Attributes->get_ItemOf(S"name");
	String *name = NULL;
	if(nameAttribute)
	{
		name = nameAttribute->get_Value();
	}

	//Extract Pending node 'platform'.
	XmlAttribute *platformAttribute = pendingNode->Attributes->get_ItemOf(S"platform");
	String *platform = NULL;
	if(platformAttribute)
	{
		platform = platformAttribute->get_Value();
	}

	//Attempt to find a node that matches the Pending node in the Built Waves List.
	XmlNode *newBuiltNode = NULL;
	if(builtNode)
	{
		XmlNode *builtChildNode = builtNode->get_FirstChild();
		while(builtChildNode)
		{
			if(builtChildNode->Name->Equals(pendingNode->Name))
			{
				bool namesMatch = false;
				//Extract Built node 'name'.
				XmlAttribute *childNameAttribute = builtChildNode->Attributes->get_ItemOf(S"name");
				String *childName = NULL;
				if(childNameAttribute)
				{
					childName = childNameAttribute->get_Value();
				}
				if(childName)
				{
					if(childName->Equals(name))
					{
						namesMatch = true;
					}
				}
				else if(name == NULL)
				{
					namesMatch = true;
				}

				bool platformsMatch = false;
				//Extract Built node 'platform'.
				XmlAttribute *childPlatformAttribute = builtChildNode->Attributes->get_ItemOf(S"platform");
				String *childPlatform = NULL;
				if(childPlatformAttribute)
				{
					childPlatform = childPlatformAttribute->get_Value();
				}
				if(childPlatform)
				{
					if(childPlatform->Equals(platform))
					{
						platformsMatch = true;
					}
				}
				else if(platform == NULL)
				{
					platformsMatch = true;
				}

				if(namesMatch && platformsMatch)
				{
					newBuiltNode = builtChildNode;
					break;
				}
			}

			builtChildNode = builtChildNode->NextSibling;
		}
	}
	audWaveBuilder::hasTag deferredTagType = HasDeferredTag(pendingNode,newBuiltNode,1);
	audWaveBuilder::hasTag doNotBuildTagType = FindTag(pendingNode,S"DoNotBuild",1);

	if(deferredTagType == audWaveBuilder::hasTag::DISTANT_CHILD)
	{
		throw new audBuildException(S"Deferred build tags may only be placed at pack level");
	}
	else if((m_IsDeferredBuild || (deferredTagType == audWaveBuilder::hasTag::NO)) 
					&& doNotBuildTagType != audWaveBuilder::hasTag::DIRECT_CHILD){
		if(newBuiltNode)
		{
			System::Diagnostics::Debug::WriteLine(String::Concat("*** Updating built node ",name));
			//Copy attributes.
			
			for(int i=0; i<pendingNode->Attributes->get_Count(); i++)
				{
					XmlAttribute *builtAttribute = __try_cast<XmlAttribute*>(newBuiltNode->Attributes->GetNamedItem(pendingNode->Attributes->Item(i)->Name));
					XmlAttribute *pendingAttribute = __try_cast<XmlAttribute*>(pendingNode->Attributes->Item(i));

					//existing attributes i.e name 
					if(builtAttribute)
					{
						builtAttribute->set_Value(pendingAttribute->Value);
					}
					//new attributes i.e operation
					else
					{
						XmlAttribute *newAttribute = newBuiltNode->OwnerDocument->CreateAttribute(pendingAttribute->Name);
						newAttribute->set_Value(pendingAttribute->Value);
						newBuiltNode->Attributes->Append(newAttribute);
					}
				}
			
			//m_PendingNodesForDeletion->Add(pendingNode);
			
		}
		else
		{

			System::Diagnostics::Debug::WriteLine(String::Concat("*** Adding to built nodes ",name));
			//Add new resource to Built Waves Log.
			newBuiltNode = m_BuiltWavesXml->ImportNode(pendingNode, false);
			builtNode->AppendChild(newBuiltNode);
			//m_PendingNodesForDeletion->Add(pendingNode);
		}

		if(deferredTagType!= audWaveBuilder::hasTag::DISTANT_CHILD 
			&& doNotBuildTagType != audWaveBuilder::hasTag::DISTANT_CHILD)
		{
			m_PendingNodesForDeletion->Add(pendingNode);
		}

		if(doNotBuildTagType == audWaveBuilder::hasTag::DISTANT_CHILD)
		{
			pendingNode->Attributes->RemoveNamedItem(S"operation");
		}
		
		XmlNode *childNode = pendingNode->get_FirstChild();
		while(childNode)
		{
			MergePendingWaveNode(childNode, newBuiltNode);
			childNode = childNode->NextSibling;
		}
	}
}

audWaveBuilder::hasTag audWaveBuilder::HasDeferredTag(XmlNode *pendingNode, XmlNode *builtNode, int level)
{
	audWaveBuilder::hasTag hasTag = FindTag(pendingNode,S"deferredBuild",level);

	if(hasTag== audWaveBuilder::hasTag::NO && builtNode)
	{
		hasTag = FindTag(builtNode,S"deferredBuild",level);
	}
	return hasTag;
}

audWaveBuilder::hasTag audWaveBuilder::FindTag(XmlNode *parentNode, String *name, int level){
	audWaveBuilder::hasTag hasTag = audWaveBuilder::hasTag::NO;
	XmlNode *childNode = parentNode->FirstChild;
	while (childNode)
	{
		if (childNode->Name->Equals(S"Tag"))
		{
			XmlAttribute *nameAttribute = childNode->Attributes->get_ItemOf(S"name");
			if (nameAttribute)
			{
				if (nameAttribute->Value->Equals(name))
				{
					if(level==1)
					{
						hasTag = audWaveBuilder::hasTag::DIRECT_CHILD;
					}
					else
					{
						hasTag = audWaveBuilder::hasTag::DISTANT_CHILD;
					}
					break;
				}
			}
		}

		childNode = childNode->NextSibling;
	}

	if(hasTag == audWaveBuilder::hasTag::NO)
	{
		level++;
		for(int i =0; i<parentNode->ChildNodes->Count; i++)
		{
				hasTag = FindTag(parentNode->ChildNodes->Item(i),name,level);
				if(hasTag != audWaveBuilder::hasTag::NO)
				{
					break;
				}
		}
	}

	return hasTag;
}

void audWaveBuilder::ParseCombinedPendingAndBuiltWaveXml(void)
{
	bool shouldModifyParent = false;
	int compression = -1;
	int sampleRate = -1;
	unsigned int streamingBlockBytes = 0;
	String *fullWaveResourcePath = 0;
	XmlNode *builtNode = m_BuiltWavesXml->DocumentElement->get_FirstChild();
	while(builtNode)
	{
		ParseCombinedPendingAndBuiltWaveNode(builtNode, m_WaveBuildList, fullWaveResourcePath, compression, sampleRate,
			streamingBlockBytes, shouldModifyParent);
		builtNode = builtNode->NextSibling;
	}
}

void audWaveBuilder::ParseCombinedPendingAndBuiltWaveNode(XmlNode *node, ArrayList *parentWaveResourceList,
	String *fullWaveResourcePath, int &parentCompression, int &parentSampleRate, unsigned int &parentStreamingBlockBytes,
	bool &shouldModifyParent)
{
	audWave::audBuildCommands buildCommand = audWave::audBuildCommands::UNASSIGNED;
	audWavePak *wavePak = NULL;
	audWaveBank *waveBank = NULL;
	audWave *wave = NULL;

	XmlAttribute *nameAttribute = node->Attributes->get_ItemOf(S"name");
	String *name = NULL;
	if(nameAttribute)
	{
		name = nameAttribute->get_Value();
		if(name)
		{
			fullWaveResourcePath = String::Concat(fullWaveResourcePath, S"\\", name);
		}
	}

	XmlAttribute *operationAttribute = node->Attributes->get_ItemOf(S"operation");
	String *operation = NULL;
	if(operationAttribute)
	{
		operation = operationAttribute->get_Value();
		
		if(operation)
		{
			if(operation->Equals(S"add"))
			{
				buildCommand = audWave::audBuildCommands::ADD;
			}
			else if(operation->Equals(S"modify"))
			{
				buildCommand = audWave::audBuildCommands::MODIFY;
			}
			else if(operation->Equals(S"remove"))
			{
				buildCommand = audWave::audBuildCommands::REMOVE;
			}
		}
	}

	XmlAttribute *platformAttribute = node->Attributes->get_ItemOf(S"platform");
	String *platform = NULL;
	if(platformAttribute)
	{
		platform = platformAttribute->get_Value();
	}

	if(node->Name->Equals(S"Pack"))
	{
		//Parse this Pak definition.
		if(name == NULL)
		{
			audBuildException *exception = new audBuildException(
				S"XML Error: Missing name in Pack definition");
			throw(exception);
		}

		//Formalize name format.
		String *formalName = name->TrimEnd(0)->ToUpper()->Replace(' ', '_');

		//Check if this Pak has already been added.
		System::Collections::IEnumerator *parentWaveResourceListEnum = parentWaveResourceList->GetEnumerator();
		while(parentWaveResourceListEnum->MoveNext())
		{
			audWavePak *existingWavePak = __try_cast<audWavePak *>(parentWaveResourceListEnum->Current);
			if(existingWavePak->m_Name->Equals(formalName))
			{
				audBuildException *exception = new audBuildException(
					String::Concat(S"Duplicate Pack name found - ", name));
				throw(exception);
			}
		}

		if(wavePak == NULL)
		{
			//This Pak was not found, so add a new one.
			wavePak = new audWavePak(formalName, fullWaveResourcePath, buildCommand);
			parentWaveResourceList->Add(wavePak);
		}

		parentWaveResourceList = wavePak->m_BankList;
	}
	else if(node->Name->Equals(S"BankFolder"))
	{
		//Parse this BankFolder definition.
		if(name == NULL)
		{
			audBuildException *exception = new audBuildException(
				S"XML Error: Missing name in BankFolder definition");
			throw(exception);
		}
	}
	else if(node->Name->Equals(S"Bank"))
	{
		//Parse this Bank definition.
		if(name == NULL)
		{
			audBuildException *exception = new audBuildException(
				S"XML Error: Missing name in Bank definition");
			throw(exception);
		}

		//Formalize name format.
		String *formalName = name->TrimEnd(0)->ToUpper()->Replace(' ', '_');

		//Check if this Bank has already been added.
		System::Collections::IEnumerator *parentWaveResourceListEnum = parentWaveResourceList->GetEnumerator();
		while(parentWaveResourceListEnum->MoveNext())
		{
			audWaveBank *existingWaveBank = __try_cast<audWaveBank *>(parentWaveResourceListEnum->Current);
			if(existingWaveBank->m_Name->Equals(formalName))
			{
				if((buildCommand == audWave::audBuildCommands::ADD) &&
					(existingWaveBank->m_BuildCommand == audWave::audBuildCommands::REMOVE))
				{
					//A bank has been removed from one bank folder and added to another under a common parent pack, so
					//simply change the command of the existing bank to MODIFY and update the full path to point to the
					//bank to be added.
					existingWaveBank->m_FullPath = fullWaveResourcePath;
					existingWaveBank->m_BuildCommand = audWave::audBuildCommands::MODIFY;
					waveBank = existingWaveBank;
				}
				else if((buildCommand == audWave::audBuildCommands::REMOVE) &&
					(existingWaveBank->m_BuildCommand == audWave::audBuildCommands::ADD))
				{
					//A bank has been removed from one bank folder and added to another under a common parent pack, so
					//simply change the command of the existing bank to MODIFY.
					existingWaveBank->m_BuildCommand = audWave::audBuildCommands::MODIFY;
					waveBank = existingWaveBank;
				}
				else
				{
					//This is a duplicate bank name, so stop the build and report the error.
					audBuildException *exception = new audBuildException(
						String::Concat(S"Duplicate Bank name found - ", name));
					throw(exception);
				}
			}
		}

		if(waveBank == NULL)
		{
			//This Bank was not found, so add a new one.
			waveBank = new audWaveBank(formalName, fullWaveResourcePath, buildCommand);
			parentWaveResourceList->Add(waveBank);
		}
		
		parentWaveResourceList = waveBank->m_WaveList;
	}
	else if(node->Name->Equals(S"WaveFolder"))
	{
		//Parse this WaveFolder definition.
		if(name == NULL)
		{
			audBuildException *exception = new audBuildException(
				S"XML Error: Missing name in WaveFolder definition");
			throw(exception);
		}
	}
	else if(node->Name->Equals(S"Wave"))
	{
		//Parse this Wave definition.
		if(name == NULL)
		{
			audBuildException *exception = new audBuildException(
				S"XML Error: Missing name in Wave definition");
			throw(exception);
		}

		//Formalize name format.
		String *formalName = name->TrimEnd(0)->ToUpper()->Replace(' ', '_');

		//Remove .WAV extension if included.
		if(formalName->EndsWith(S".WAV"))
		{
			formalName = formalName->Substring(0, formalName->Length - 4);
		}

		//Check if this Wave has already been added (just a safety check).
		System::Collections::IEnumerator *parentWaveResourceListEnum = parentWaveResourceList->GetEnumerator();
		while(parentWaveResourceListEnum->MoveNext())
		{
			audWave *existingWave = __try_cast<audWave *>(parentWaveResourceListEnum->Current);
			if(existingWave->m_Name->Equals(formalName))
			{
				if((buildCommand == audWave::audBuildCommands::ADD) &&
					(existingWave->m_BuildCommand == audWave::audBuildCommands::REMOVE))
				{
					//A wave has been removed from one wave folder and added to another under a common parent bank, so
					//simply change the command of the existing wave to MODIFY and update the full paths to point to the
					//wave to be added.
					existingWave->m_FullAssetPath = fullWaveResourcePath;
					existingWave->m_FullLocalPath = String::Concat(m_LocalRawWavesPath, fullWaveResourcePath);
					existingWave->m_BuildCommand = audWave::audBuildCommands::MODIFY;
					wave = existingWave;
				}
				else if((buildCommand == audWave::audBuildCommands::REMOVE) &&
					(existingWave->m_BuildCommand == audWave::audBuildCommands::ADD))
				{
					//A wave has been removed from one wave folder and added to another under a common parent bank, so
					//simply change the command of the existing wave to MODIFY.
					existingWave->m_BuildCommand = audWave::audBuildCommands::MODIFY;
					wave = existingWave;
				}
				else
				{
					//This is a duplicate wave name, so stop the build and report the error.
					audBuildException *exception = new audBuildException(
						String::Concat(S"Duplicate Wave name found - ", name));
					throw(exception);
				}
			}
		}

		if(wave == NULL)
		{
			//check for a rename tag
			String *alias;
			String *tagOperation ="";

			XmlNode *childNode = node->FirstChild;
			while(childNode)
			{
				if(childNode->Name->Equals(S"Tag") && childNode->Attributes->get_ItemOf(S"name") 
					&& childNode->Attributes->get_ItemOf(S"name")->Value->Equals(S"rename"))
				{
					if(childNode->Attributes->get_ItemOf(S"value"))
					{
						alias = childNode->Attributes->get_ItemOf(S"value")->Value;
					}

					if(childNode->Attributes->get_ItemOf(S"operation"))
					{
						tagOperation = childNode->Attributes->get_ItemOf(S"operation")->Value;
					}
				}

				childNode = childNode->NextSibling;
			}

			
			if(tagOperation && tagOperation->Equals(S"remove"))
			{
				//wave is will be set to modify. The Alias should be used to remove the wave from the bank
				//the formal name should be used to re-add. Hence why we have swapped the round
				wave = new audWave(alias, formalName, fullWaveResourcePath, String::Concat(m_LocalRawWavesPath, fullWaveResourcePath),
					buildCommand);
			}
			else {
				wave = new audWave(formalName, alias, fullWaveResourcePath, String::Concat(m_LocalRawWavesPath, fullWaveResourcePath),
				buildCommand);
				
				if(tagOperation && tagOperation->Equals(S"modify"))
				{
					//rebuild bank as can't don't know what old alias was
					shouldModifyParent = true;
				}
			}
			
			parentWaveResourceList->Add(wave);
			m_TotalWaves++;
		}
	}
	else if(node->Name->Equals(S"Tag"))
	{
		//Parse this Tag definition.
		if(name == NULL)
		{
			audBuildException *exception = new audBuildException(
				S"XML Error: Missing name in Tag definition");
			throw(exception);
		}

		//Check for a compression tag/attribute and report to (owning) parent node if found to match the target build
		//platform (or is cross-platform and non-overriding).
		if(name->Equals(S"compression"))
		{
			XmlAttribute *compressionValueAttribute = node->Attributes->get_ItemOf(S"value");
			if(compressionValueAttribute)
			{
				if(!platform)
				{
					//Never override a compression with a cross-platform setting, as only one cross-platform should exist
					//and we should not allow it to override a platform-specific setting.
					if(parentCompression == -1)
					{
						try
						{
							parentCompression = Int32::Parse(compressionValueAttribute->get_Value());	

							if(buildCommand != audWave::UNASSIGNED)
							{
								//This is a Pending Wave command, so also inform parent that it is to be modified.
								shouldModifyParent = true;
							}
						}
						catch(FormatException *e)
						{
							e = NULL;
							audBuildException *exception = new audBuildException(
								S"XML Error: Invalid compression value");
							throw(exception);
						}
					}
				}
				else if(m_BuildPlatform->Equals(platform))
				{
					//Allow a platform-specific setting to override any existing cross-platform setting.
					try
					{
						parentCompression = Int32::Parse(compressionValueAttribute->get_Value());

						if(buildCommand != audWave::UNASSIGNED)
						{
							//This is a Pending Wave command, so also inform parent that it is to be modified.
							shouldModifyParent = true;
						}
					}
					catch(FormatException *e)
					{
						e = NULL;
						audBuildException *exception = new audBuildException(
							S"XML Error: Invalid compression value");
						throw(exception);
					}
				}
			}

		} // name->Equals(S"compression")

		//Check for a sampleRate tag/attribute and report to (owning) parent node if found to match the target build
		//platform (or is cross-platform and non-overriding).
		else if(name->Equals(S"sampleRate"))
		{
			XmlAttribute *sampleRateValueAttribute = node->Attributes->get_ItemOf(S"value");
			if(sampleRateValueAttribute)
			{
				if(!platform)
				{
					//Never override a sample rate with a cross-platform setting, as only one cross-platform should exist
					//and we should not allow it to override a platform-specific setting.
					if(parentSampleRate == -1)
					{
						try
						{
							parentSampleRate = Int32::Parse(sampleRateValueAttribute->get_Value());	

							if(buildCommand != audWave::UNASSIGNED)
							{
								//This is a Pending Wave command, so also inform parent that it is to be modified.
								shouldModifyParent = true;
							}
						}
						catch(FormatException *e)
						{
							e = NULL;
							audBuildException *exception = new audBuildException(
								S"XML Error: Invalid sampleRate value");
							throw(exception);
						}
					}
				}
				else if(m_BuildPlatform->Equals(platform))
				{
					//Allow a platform-specific setting to override any existing cross-platform setting.
					try
					{
						parentSampleRate = Int32::Parse(sampleRateValueAttribute->get_Value());

						if(buildCommand != audWave::UNASSIGNED)
						{
							//This is a Pending Wave command, so also inform parent that it is to be modified.
							shouldModifyParent = true;
						}
					}
					catch(FormatException *e)
					{
						e = NULL;
						audBuildException *exception = new audBuildException(
							S"XML Error: Invalid sampleRate value");
						throw(exception);
					}
				}
			}

		} // name->Equals(S"sampleRate")

		//Check for an streaming block bytes tag/attribute and report to (owning) parent node if found to match the target
		//build platform (or is cross-platform and non-overriding).
		else if(name->Equals(S"streamingBlockBytes"))
		{
			XmlAttribute *streamingBlockBytesValueAttribute = node->Attributes->get_ItemOf(S"value");
			if(streamingBlockBytesValueAttribute)
			{
				if(!platform)
				{
					//Never override with a cross-platform setting, as only one cross-platform should exist and we should
					//not allow it to override a platform-specific setting.
					if(parentStreamingBlockBytes == 0)
					{
						try
						{
							parentStreamingBlockBytes = UInt32::Parse(streamingBlockBytesValueAttribute->get_Value());

							if(buildCommand != audWave::UNASSIGNED)
							{
								//This is a Pending Wave command, so also inform parent that it is to be modified.
								shouldModifyParent = true;
							}
						}
						catch(FormatException *e)
						{
							e = NULL;
							audBuildException *exception = new audBuildException(
								S"XML Error: Invalid streamingBlockBytes value");
							throw(exception);
						}
					}
				}
				else if(m_BuildPlatform->Equals(platform))
				{
					//Allow a platform-specific setting to override any existing cross-platform setting.
					try
					{
						parentStreamingBlockBytes = UInt32::Parse(streamingBlockBytesValueAttribute->get_Value());

						if(buildCommand != audWave::UNASSIGNED)
						{
							//This is a Pending Wave command, so also inform parent that it is to be modified.
							shouldModifyParent = true;
						}
					}
					catch(FormatException *e)
					{
						e = NULL;
						audBuildException *exception = new audBuildException(
							S"XML Error: Invalid streamingBlockBytes value");
						throw(exception);
					}
				}
			}

		} // name->Equals(S"streamingBlockBytes")

		//Check for a rebuild tag/attribute and report to (owning) parent node if found to match the target build
		//platform.
		else if(name->Equals(S"rename") || name->Equals(S"rebuild"))
		{
			if((!platform || m_BuildPlatform->Equals(platform)) && (buildCommand != audWave::UNASSIGNED))
			{
				//This is a Pending Wave command, so also inform parent that it is to be modified.
				shouldModifyParent = true;
			}

		} // name->Equals(S"rebuild")
	}

	bool shouldModify = false;
	int compression = -1;
	int sampleRate = -1;
	unsigned int streamingBlockBytes = 0;
	XmlNode *childNode = node->get_FirstChild();
	while(childNode)
	{
		ParseCombinedPendingAndBuiltWaveNode(childNode, parentWaveResourceList, fullWaveResourcePath, compression,
			sampleRate, streamingBlockBytes, shouldModify);
		childNode = childNode->NextSibling;
	}

	if((compression >= 0) || (sampleRate >= 0) || (streamingBlockBytes > 0) || (shouldModify))
	{
		//Apply compression, sampleRate and/or streamingBlockBytes settings to next Wave resource down the hierarchy.
		if(node->Name->Equals(S"Pack"))
		{
			if(wavePak)
			{
				if(compression >= 0)
				{
					wavePak->SetCompression(compression);
				}

				if(sampleRate >= 0)
				{
					wavePak->SetSampleRate(sampleRate);
				}

				if(streamingBlockBytes >= 0)
				{
					wavePak->SetStreamingBlockBytes(streamingBlockBytes);
				}

				if(shouldModify && (wavePak->m_BuildCommand == audWave::UNASSIGNED))
				{
					wavePak->m_BuildCommand = audWave::MODIFY;

					//Ensure all child banks are at least marked for modification.
					System::Collections::IEnumerator* bankEnum = wavePak->m_BankList->GetEnumerator();
					while(bankEnum->MoveNext())
					{
						audWaveBank *waveBank = __try_cast<audWaveBank *>(bankEnum->Current);

						if(waveBank->m_BuildCommand == audWave::UNASSIGNED)
						{
							waveBank->m_BuildCommand = audWave::MODIFY;
						}
					}
				}
			}
		}
		else if(node->Name->Equals(S"BankFolder"))
		{
			System::Collections::IEnumerator *parentWaveResourceListEnum = parentWaveResourceList->GetEnumerator();
			while(parentWaveResourceListEnum->MoveNext())
			{
				waveBank = __try_cast<audWaveBank *>(parentWaveResourceListEnum->Current);
				if(waveBank)
				{
					//Check that the Bank is a valid child of this Bank Folder before cascading anything.
					bool isValidChild = (FindWaveBankNode(node, waveBank->m_Name) != NULL);

					if(isValidChild)
					{
						if(compression >= 0)
						{
							waveBank->SetCompression(compression);
						}

						if(sampleRate >= 0)
						{
							waveBank->SetSampleRate(sampleRate);
						}

						if(streamingBlockBytes >= 0)
						{
							waveBank->SetStreamingBlockBytes(streamingBlockBytes);
						}

						if(shouldModify && (waveBank->m_BuildCommand == audWave::UNASSIGNED))
						{
							waveBank->m_BuildCommand = audWave::MODIFY;
						}
					}
				}
			}
		}
		else if(node->Name->Equals(S"Bank"))
		{
			if(waveBank)
			{
				if(compression >= 0)
				{
					waveBank->SetCompression(compression);
				}

				if(sampleRate >= 0)
				{
					waveBank->SetSampleRate(sampleRate);
				}

				if(streamingBlockBytes >= 0)
				{
					waveBank->SetStreamingBlockBytes(streamingBlockBytes);
				}

				if(shouldModify && (waveBank->m_BuildCommand == audWave::UNASSIGNED))
				{
					waveBank->m_BuildCommand = audWave::MODIFY;
				}
			}
		}
		else if(node->Name->Equals(S"WaveFolder"))
		{
			System::Collections::IEnumerator *parentWaveResourceListEnum = parentWaveResourceList->GetEnumerator();
			while(parentWaveResourceListEnum->MoveNext())
			{
				wave = __try_cast<audWave *>(parentWaveResourceListEnum->Current);
				if(wave)
				{
					//Check that the Wave is a valid child of this Wave Folder before cascading anything.
					bool isValidChild = (FindWaveNode(node, String::Concat(wave->m_Name, S".WAV")) != NULL);

					if(isValidChild)
					{
						if(compression >= 0)
						{
							wave->SetCompression(compression);
						}

						if(sampleRate >= 0)
						{
							wave->SetSampleRate(sampleRate);
						}

						if(shouldModify && (wave->m_BuildCommand == audWave::UNASSIGNED))
						{
							wave->m_BuildCommand = audWave::MODIFY;
						}
					}
				}
			}
		}
		else if(node->Name->Equals(S"Wave"))
		{
			if(wave)
			{
				if(compression >= 0)
				{
					wave->SetCompression(compression);
				}

				if(sampleRate >= 0)
				{
					wave->SetSampleRate(sampleRate);
				}

				if(shouldModify && (wave->m_BuildCommand == audWave::UNASSIGNED))
				{
					wave->m_BuildCommand = audWave::MODIFY;
				}
			}
		}
	}
}

void audWaveBuilder::TrimBuiltWavesXml(void)
{
	bool shouldRemoveNode = false;
	XmlNode *builtNode = m_BuiltWavesXml->DocumentElement->get_FirstChild();
	XmlNode *nextBuiltNode = NULL;

	while(builtNode)
	{
		TrimBuiltWavesNode(builtNode, shouldRemoveNode);
		nextBuiltNode = builtNode->NextSibling;

		if(shouldRemoveNode)
		{
			m_BuiltWavesXml->DocumentElement->RemoveChild(builtNode);
		}

		builtNode = nextBuiltNode;
	}
}

void audWaveBuilder::TrimBuiltWavesNode(XmlNode *node, bool &shouldRemoveNode)
{
	if(node->Name->Equals("Tag")){
		System::Diagnostics::Debug::WriteLine(node->Attributes->Item(0)->Value);
	}
	shouldRemoveNode = false;

	XmlAttribute *operationAttribute = node->Attributes->get_ItemOf(S"operation");
	String *operation = NULL;
	if(operationAttribute)
	{
		operation = operationAttribute->get_Value();
		if(operation)
		{
			if(operation->Equals(S"remove"))
			{
				//Inform parent node that this node requires removal.
				shouldRemoveNode = true;
			}
			else
			{
				//Trim operation attribute.
				node->Attributes->RemoveNamedItem(S"operation");
			}
		}
	}

	//Check if this is a 'rebuild' tag, as these are never propagated to the built waves list.
	if(!shouldRemoveNode && node->Name->Equals(S"Tag"))
	{
		XmlAttribute *nameAttribute = node->Attributes->get_ItemOf(S"name");
		String *name = NULL;
		if(nameAttribute)
		{
			name = nameAttribute->get_Value();
			if(name && name->Equals(S"rebuild"))
			{
				//Inform parent node that this node requires removal.
				shouldRemoveNode = true;
			}
		}
	}

	bool shouldRemoveChildNode = false;
	XmlNode *childNode = node->get_FirstChild();
	XmlNode *nextChildNode = NULL;

	while(childNode)
	{
		TrimBuiltWavesNode(childNode, shouldRemoveChildNode);
		nextChildNode = childNode->NextSibling;

		if(shouldRemoveChildNode)
		{
			node->RemoveChild(childNode);
		}

		childNode = nextChildNode;
	}
}

void audWaveBuilder::PrepareAssets(void)
{
	String *localWavePakPath = String::Concat(m_LocalBuildDataPath, g_WavePaksPath);

	//Clear all previously built Wave data from local Pak destination folder to facilitate a 'clean' iterative
	//build.
	/*DirectoryInfo *localWavePakPathInfo = new DirectoryInfo(localWavePakPath);
	if(localWavePakPathInfo->Exists)
	{
		//Clear read-only attributes of subfolders and files.
		MakeDirectoryWriteable(localWavePakPathInfo);

		localWavePakPathInfo->Delete(true); //Recursive delete.
	}*/

	//Ensure that all output paths exist locally.
	Directory::CreateDirectory(m_LocalBuildDataPath);
	Directory::CreateDirectory(localWavePakPath);
	
	//This fixes a failure to get latest on a file within a folder that doesn't exist yet
	if (m_AssetManager->GetAssetManagementType() == audAssetManagement::ASSET_MANAGER_PERFORCE)
	{
		m_AssetManager->GetLatest(m_LocalBuildDataPath);
		m_AssetManager->GetLatest(localWavePakPath);
		m_AssetManager->GetLatest(m_AssetRawWavesPath);
	}
	
	m_BuildClient->ReportProgress(-1, -1, S"Getting latest Wave resources and checking-out Banks", true);

	if(m_ShouldBuildLocally)
	{
		//Make all local built wave assets writeable in order to deal with any new assets that have already been built
		//locally but never added to the database.
		m_AssetManager->MakeLocalAssetWriteable(localWavePakPath);
	}

	//Get new/modified raw Wave resources and check-out all previously built Wave Banks that require
	//modification.
	System::Collections::IEnumerator *waveBuildEnum = m_WaveBuildList->GetEnumerator();
	while(waveBuildEnum->MoveNext())
	{
		audWavePak *wavePak = __try_cast<audWavePak *>(waveBuildEnum->Current);

		if(wavePak->m_BuildCommand == audWave::audBuildCommands::ADD)
		{
			//This entire Wave Pak is to be added, so get the latest version of all raw Wave Pak assets.
			if(!m_AssetManager->GetLatest(String::Concat(m_AssetRawWavesPath, wavePak->m_FullPath)))
			{
				if(!m_ShouldBuildLocally) //These assets may not yet be in the database for local builds.
				{
					audBuildException *exception = new audBuildException(
						String::Concat(S"Failed to get latest Raw Waves in Wave Pak - ", wavePak->m_FullPath));
					throw(exception);
				}
			}

			//No assets need to be checked-out as this is a new Wave Pak.
		}
		//check out files for rebuild if we are running a deferred build
		else if(wavePak->m_BuildCommand == audWave::audBuildCommands::MODIFY)
		{
			//This entire Wave Pak is to be modified, so get the latest version of all raw Wave Pak assets.
			if(!m_AssetManager->GetLatest(String::Concat(m_AssetRawWavesPath, wavePak->m_FullPath)))
			{
				if(!m_ShouldBuildLocally) //These assets may not yet be in the database for local builds.
				{
					audBuildException *exception = new audBuildException(
						String::Concat(S"Failed to get latest Raw Waves in Wave Pak - ", wavePak->m_FullPath));
					throw(exception);
				}
			}

			//Check-out all child Wave Banks.
			System::Collections::IEnumerator *waveBankEnum = wavePak->m_BankList->GetEnumerator();
			while(waveBankEnum->MoveNext())
			{
				audWaveBank *waveBank = __try_cast<audWaveBank *>(waveBankEnum->Current);

				if((waveBank->m_BuildCommand != audWave::audBuildCommands::ADD) &&
					(waveBank->m_BuildCommand != audWave::audBuildCommands::REMOVE))
				{
					String *wavePaksPath = new String(g_WavePaksPath);
					if(!m_AssetManager->CheckOut(String::Concat(m_AssetBuildDataPath, wavePaksPath,
						wavePak->m_Name, S"\\", waveBank->m_Name), S"Building Waves"))
					{
						audBuildException *exception = new audBuildException(
							String::Concat(S"Failed to check-out previously built Wave Bank - ",
							wavePak->m_Name, S"\\", waveBank->m_Name));
						throw(exception);
					}
				}
			}
		}
		else if(wavePak->m_BuildCommand == audWave::audBuildCommands::REMOVE)
		{
			//	- No assets are required for Wave Pak removal.
		}
		else if(wavePak->m_BuildCommand == audWave::audBuildCommands::UNASSIGNED)
		{
			//This Wave Pak is not to be directly manipulated, so check the child Wave Banks.
			System::Collections::IEnumerator *waveBankEnum = wavePak->m_BankList->GetEnumerator();
			while(waveBankEnum->MoveNext())
			{
				audWaveBank *waveBank = __try_cast<audWaveBank *>(waveBankEnum->Current);
				
				bool shouldBuildWaveBank = false;
				System::Collections::IEnumerator *waveEnum = waveBank->m_WaveList->GetEnumerator();
				while(waveEnum->MoveNext())
				{
					audWave *wave = __try_cast<audWave *>(waveEnum->Current);
					if(wave->m_BuildCommand != audWave::audBuildCommands::UNASSIGNED)
					{
						shouldBuildWaveBank = true;
						break;
					}
				}

				if(waveBank->m_BuildCommand == audWave::audBuildCommands::ADD)
				{
					//This entire Wave Bank is to be added, so get the latest version of all raw Bank assets.
					if(!m_AssetManager->GetLatest(String::Concat(m_AssetRawWavesPath, waveBank->m_FullPath)))
					{
						if(!m_ShouldBuildLocally) //These assets may not yet be in the database for local builds.
						{
							audBuildException *exception = new audBuildException(
								String::Concat(S"Failed to get latest Raw Waves in Wave Bank - ",
								waveBank->m_FullPath));
							throw(exception);
						}
					}

					//No assets need to be checked-out as this is a new Wave Bank.
				}
				else if(waveBank->m_BuildCommand == audWave::audBuildCommands::MODIFY || (waveBank->m_BuildCommand == audWave::audBuildCommands::UNASSIGNED && m_IsDeferredBuild&&shouldBuildWaveBank))
				{
					//This entire Wave Bank is to be modified, so get the latest version of all raw Bank assets.
					if(!m_AssetManager->GetLatest(String::Concat(m_AssetRawWavesPath, waveBank->m_FullPath)))
					{
						if(!m_ShouldBuildLocally) //These assets may not yet be in the database for local builds.
						{
							audBuildException *exception = new audBuildException(
								String::Concat(S"Failed to get latest Raw Waves in Wave Bank - ",
								waveBank->m_FullPath));
							throw(exception);
						}
					}

					//Check-out the previously built parent Wave Bank.
					String *wavePaksPath = new String(g_WavePaksPath);
					if(!m_AssetManager->CheckOut(String::Concat(m_AssetBuildDataPath, wavePaksPath,
						wavePak->m_Name, S"\\", waveBank->m_Name), S"Building Waves"))
					{
						audBuildException *exception = new audBuildException(
							String::Concat(S"Failed to check-out previously built Wave Bank - ",
							wavePak->m_Name, S"\\", waveBank->m_Name));
						throw(exception);
					}

					//force deferred build banks to rebuild
					waveBank->m_BuildCommand = audWave::audBuildCommands::MODIFY;
				}
				else if(waveBank->m_BuildCommand == audWave::audBuildCommands::REMOVE)
				{
					//	- No assets are required for Wave Bank removal.
				}
				else if(waveBank->m_BuildCommand == audWave::audBuildCommands::UNASSIGNED)
				{
					//This Wave Bank is not to be directly manipulated, so check the child Waves.
					bool shouldCheckOutParentWaveBank = false;
					System::Collections::IEnumerator *waveEnum = waveBank->m_WaveList->GetEnumerator();
					while(waveEnum->MoveNext())
					{
						audWave *wave = __try_cast<audWave *>(waveEnum->Current);

						if(wave->m_BuildCommand != audWave::audBuildCommands::UNASSIGNED)
						{
							if((wave->m_BuildCommand == audWave::audBuildCommands::ADD) ||
								(wave->m_BuildCommand == audWave::audBuildCommands::MODIFY))
							{
								//This Wave is to be added or modified, so get the latest version of the asset.
								if(!m_AssetManager->GetLatest(String::Concat(m_AssetRawWavesPath,
									wave->m_FullAssetPath)))
								{
									//These assets may not yet be in the database for local builds.
									if(!m_ShouldBuildLocally)
									{
										audBuildException *exception = new audBuildException(
											String::Concat(S"Failed to get latest Raw Wave - ",
											wave->m_FullAssetPath));
										throw(exception);
									}
								}
							}

							//This Wave is to be manipulated, so we need to check-out the previously built
							//parent Wave Bank (that should exist.)
							shouldCheckOutParentWaveBank = true;
						}
					}

					if(shouldCheckOutParentWaveBank)
					{
						//Check-out the previously built parent Wave Bank to enable manipulation of one or more
						//child Waves.
						String *wavePaksPath = new String(g_WavePaksPath);
						if(!m_AssetManager->CheckOut(String::Concat(m_AssetBuildDataPath, wavePaksPath,
							wavePak->m_Name, S"\\", waveBank->m_Name), S"Building Waves"))
						{
							audBuildException *exception = new audBuildException(
								String::Concat(S"Failed to check-out previously built Wave Bank - ",
								wavePak->m_Name, S"\\", waveBank->m_Name));
							throw(exception);
						}

						if(waveBank->IsInterleaved())
						{
							//Interleaved Wave Banks are rebuilt exhaustively, so get the latest version of all raw Bank
							//assets.
							if(!m_AssetManager->GetLatest(String::Concat(m_AssetRawWavesPath, waveBank->m_FullPath)))
							{
								if(!m_ShouldBuildLocally) //These assets may not yet be in the database for local builds.
								{
									audBuildException *exception = new audBuildException(
										String::Concat(S"Failed to get latest Raw Waves in Wave Bank - ",
										waveBank->m_FullPath));
									throw(exception);
								}
							}
						}
					}
				}
			}
		}
	}
}

void audWaveBuilder::MakeDirectoryWriteable(DirectoryInfo *dirInfo)
{
	dirInfo->set_Attributes(FileAttributes::Archive);

	FileInfo *files[] = dirInfo->GetFiles();
	System::Collections::IEnumerator* fileEnum = files->GetEnumerator();
	while(fileEnum->MoveNext())
	{
		__try_cast<FileInfo *>(fileEnum->Current)->set_Attributes(FileAttributes::Archive);
	}

	DirectoryInfo *subDirs[] = dirInfo->GetDirectories();
	System::Collections::IEnumerator* dirEnum = subDirs->GetEnumerator();
	while(dirEnum->MoveNext())
	{
		MakeDirectoryWriteable(__try_cast<DirectoryInfo *>(dirEnum->Current));
	}
}

void audWaveBuilder::BuildWaveBanks(void)
{
	int wavesBuilt=0;
	DateTime waveBuildStartDateTime = DateTime::Now;

	try
	{
		m_BuildClient->ReportProgress(-1, -1, S"Writing Wave Banks", true);

		System::Collections::IEnumerator* pakEnum = m_WaveBuildList->GetEnumerator();
		while(pakEnum->MoveNext())
		{
			audWavePak *wavePak = __try_cast<audWavePak *>(pakEnum->Current);

			if(wavePak->m_BuildCommand != audWave::audBuildCommands::REMOVE)
			{
				wavePak->Build(String::Concat(m_LocalBuildDataPath, g_WavePaksPath), m_BuildClient, m_TotalWaves,
					wavesBuilt, waveBuildStartDateTime, m_IsDeferredBuild);
			}
		}

	}
	catch(audBuildException *e)
	{
		//Pass internal exceptions upwards.
		GC::Collect();
		throw(e);
	}
	catch(Exception *e)
	{
		//Pass internal exceptions upwards.
		GC::Collect();
		audBuildException *exception = new audBuildException(
			String::Concat(S"Error writing Wave Banks - ", e->ToString()));
		throw(exception);
	}
}

void audWaveBuilder::AddWaveSizesToBuiltWavesXml(void)
{
	XmlNode *packNode, *bankNode, *waveNode;
	XmlNode *builtNode = m_BuiltWavesXml->DocumentElement->get_FirstChild();

	System::Collections::IEnumerator* pakEnum = m_WaveBuildList->GetEnumerator();
	while(pakEnum->MoveNext())
	{
		audWavePak *wavePak = __try_cast<audWavePak *>(pakEnum->Current);
		if(wavePak->m_BuildCommand != audWave::audBuildCommands::REMOVE)
		{
			//Find this Wave Pak in the Built Waves XML.
			packNode = NULL;
			while(builtNode)
			{
				if(builtNode->Name->Equals(S"Pack"))
				{
					XmlAttribute *nameAttribute = builtNode->Attributes->get_ItemOf(S"name");
					if(nameAttribute)
					{
						if(nameAttribute->Value->Equals(wavePak->m_Name))
						{
							packNode = builtNode;
							break;
						}
					}
				}

				builtNode = builtNode->NextSibling;
			}

			if(packNode)
			{
				System::Collections::IEnumerator* bankEnum = wavePak->m_BankList->GetEnumerator();
				while(bankEnum->MoveNext())
				{
					audWaveBank *waveBank = __try_cast<audWaveBank *>(bankEnum->Current);
					if(waveBank->m_BuildCommand != audWave::audBuildCommands::REMOVE)
					{
						//Find this Wave Bank in the Built Waves XML.
						bankNode = FindWaveBankNode(packNode, waveBank->m_Name);

						if(bankNode)
						{
							if(waveBank->m_MetadataSizeBytes >= 0)
							{
								//Modify/add Metadata Size attribute.
								XmlAttribute *metadataSizeAttribute = bankNode->Attributes->get_ItemOf(S"metadataSize");
								if(metadataSizeAttribute)
								{
									metadataSizeAttribute->set_Value(__box(waveBank->m_MetadataSizeBytes)->ToString());
								}
								else
								{
									metadataSizeAttribute = m_BuiltWavesXml->CreateAttribute(S"metadataSize");
									metadataSizeAttribute->set_Value(__box(waveBank->m_MetadataSizeBytes)->ToString());
									bankNode->Attributes->Append(metadataSizeAttribute);
								}
							}

							if(waveBank->m_WaveMetadataLookupSizeBytes >= 0)
							{
								//Modify/add Wave metadata lookup size attribute.
								XmlAttribute *waveMetadataLookupSizeAttribute = bankNode->Attributes->get_ItemOf(
									S"waveMetadataLookupSize");
								if(waveMetadataLookupSizeAttribute)
								{
									waveMetadataLookupSizeAttribute->set_Value(__box(waveBank->
										m_WaveMetadataLookupSizeBytes)->ToString());
								}
								else
								{
									waveMetadataLookupSizeAttribute = m_BuiltWavesXml->CreateAttribute(
										S"waveMetadataLookupSize");
									waveMetadataLookupSizeAttribute->set_Value(__box(waveBank->
										m_WaveMetadataLookupSizeBytes)->ToString());
									bankNode->Attributes->Append(waveMetadataLookupSizeAttribute);
								}
							}

							if(waveBank->m_CustomMetadataLookupSizeBytes >= 0)
							{
								//Modify/add custom metadata lookup size attribute.
								XmlAttribute *customMetadataLookupSizeAttribute = bankNode->Attributes->get_ItemOf(
									S"customMetadataLookupSize");
								if(customMetadataLookupSizeAttribute)
								{
									customMetadataLookupSizeAttribute->set_Value(__box(waveBank->
										m_CustomMetadataLookupSizeBytes)->ToString());
								}
								else
								{
									customMetadataLookupSizeAttribute = m_BuiltWavesXml->CreateAttribute(
										S"customMetadataLookupSize");
									customMetadataLookupSizeAttribute->set_Value(__box(waveBank->
										m_CustomMetadataLookupSizeBytes)->ToString());
									bankNode->Attributes->Append(customMetadataLookupSizeAttribute);
								}
							}
							//size of interleaved waves
							if(waveBank->m_BuiltSize >0)
							{
								//Modify/add custom metadata lookup size attribute.
								XmlAttribute *builtSizeAttribute = bankNode->Attributes->get_ItemOf(
									S"builtSize");
								if(builtSizeAttribute)
								{
									builtSizeAttribute->set_Value(__box(waveBank->
										m_BuiltSize)->ToString());
								}
								else
								{
									builtSizeAttribute = m_BuiltWavesXml->CreateAttribute(
										S"builtSize");
									builtSizeAttribute->set_Value(__box(waveBank->
										m_BuiltSize)->ToString());
									bankNode->Attributes->Append(builtSizeAttribute);
								}
							}

							System::Collections::IEnumerator* waveEnum = waveBank->m_WaveList->GetEnumerator();
							while(waveEnum->MoveNext())
							{
								audWave *wave = __try_cast<audWave *>(waveEnum->Current);
								if((wave->m_BuildCommand != audWave::audBuildCommands::REMOVE) &&
									(wave->m_BuiltSizeBytes > 0))
								{
									//We just built this wave, so update the XML attributes.

									//Find this Wave in the Built Waves XML.
									String *waveName = String::Concat(wave->m_Name, S".WAV");
									String *builtWaveName = String::Concat(wave->m_Alias,S".WAV");
									waveNode = FindWaveNode(bankNode, String::Concat(waveName));

									if(waveNode)
									{
										
										//Take a copy of the tag nodes before wave node is wiped
										ArrayList *tagNodes = new ArrayList();
										XmlNode *waveChild = waveNode->FirstChild;
										
										while(waveChild)
										{
											if(waveChild->Name->Equals("Tag"))
											{
												tagNodes->Add(waveChild);
											}

											waveChild = waveChild->NextSibling;
										}
										
										//Clear all existing attributes and child nodes before we start.
										waveNode->RemoveAll();

										//add tag nodes
										System::Collections::IEnumerator* tagEnum = tagNodes->GetEnumerator();
										while(tagEnum->MoveNext())
										{
											waveNode->AppendChild(__try_cast<XmlNode*>(tagEnum->Current));
										}

										//Add Wave Name attribute.
										XmlAttribute *nameAttribute = m_BuiltWavesXml->CreateAttribute(S"name");
										nameAttribute->set_Value(waveName);
										waveNode->Attributes->Append(nameAttribute);

										//Add Built Name attribute
										XmlAttribute *builtNameAttribute = m_BuiltWavesXml->CreateAttribute(S"builtName");
										builtNameAttribute->set_Value(builtWaveName);
										waveNode->Attributes->Append(builtNameAttribute);

										//Add Wave Name Hash attribute.
										XmlAttribute *nameHashAttribute = m_BuiltWavesXml->CreateAttribute(S"nameHash");
										nameHashAttribute->set_Value(__box(wave->ComputeNameHash(true))->ToString());
										waveNode->Attributes->Append(nameHashAttribute);

										//Add Raw Size attribute.
										if(wave->m_RawSizeBytes > 0)
										{
											XmlAttribute *rawSizeAttribute = m_BuiltWavesXml->CreateAttribute(S"rawSize");
											rawSizeAttribute->set_Value(__box(wave->m_RawSizeBytes)->ToString());
											waveNode->Attributes->Append(rawSizeAttribute);
										}

										//Add Built Size attribute.
										if(wave->m_BuiltSizeBytes > 0)
										{
											XmlAttribute *builtSizeAttribute = m_BuiltWavesXml->CreateAttribute(S"builtSize");
											builtSizeAttribute->set_Value(__box(wave->m_BuiltSizeBytes)->ToString());
											waveNode->Attributes->Append(builtSizeAttribute);
										}

										//Add Metadata Size attribute.
										if(wave->m_MetadataSizeBytes > 0)
										{
											XmlAttribute *metadataSizeAttribute = m_BuiltWavesXml->CreateAttribute(S"metadataSize");
											metadataSizeAttribute->set_Value(__box(wave->m_MetadataSizeBytes)->ToString());
											waveNode->Attributes->Append(metadataSizeAttribute);
										}

										//Add Sample Rate attributes.
										if((wave->m_RawSampleRate > 0) && (wave->m_BuiltSampleRate > 0))
										{
											XmlAttribute *rawSampleRateAttribute = m_BuiltWavesXml->CreateAttribute(S"rawSampleRate");
											rawSampleRateAttribute->set_Value(__box(wave->m_RawSampleRate)->ToString());
											waveNode->Attributes->Append(rawSampleRateAttribute);

											XmlAttribute *builtSampleRateAttribute = m_BuiltWavesXml->CreateAttribute(S"builtSampleRate");
											builtSampleRateAttribute->set_Value(__box(wave->m_BuiltSampleRate)->ToString());
											waveNode->Attributes->Append(builtSampleRateAttribute);
										}

										//Add Preloop Padding attribute if non-zero.
										if(wave->m_BuiltPreloopPadding > 0)
										{
											XmlAttribute *preloopPaddingAttribute = m_BuiltWavesXml->CreateAttribute(S"preloopPadding");
											preloopPaddingAttribute->set_Value(__box(wave->m_BuiltPreloopPadding)->ToString());
											waveNode->Attributes->Append(preloopPaddingAttribute);
										}

										//Add Custom Metadata Size attribute.
										if(wave->m_CustomMetadataSizeBytes > 0)
										{
											XmlAttribute *customMetadataSizeAttribute = m_BuiltWavesXml->CreateAttribute(S"customMetadataSize");
											customMetadataSizeAttribute->set_Value(__box(wave->m_CustomMetadataSizeBytes)->
												ToString());
											waveNode->Attributes->Append(customMetadataSizeAttribute);

											//We have custom wave metadata so serialize any markers.
											wave->SerializeWaveMarkerList(waveNode);
										}

										if(wave->m_NoOfBitsInLastPacket > 0)
										{
											XmlAttribute *NoOfBitsAttribute = m_BuiltWavesXml->CreateAttribute(S"bitsInLastPacket");
											NoOfBitsAttribute->set_Value(__box(wave->m_NoOfBitsInLastPacket)->ToString());
											waveNode->Attributes->Append(NoOfBitsAttribute);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

XmlNode *audWaveBuilder::FindWaveBankNode(XmlNode *parentNode, String *name)
{
	XmlNode *bankNode = NULL;

	XmlNode *node = parentNode->FirstChild;
	while(node && !bankNode)
	{
		if(node->Name->Equals(S"Bank"))
		{
			XmlAttribute *nameAttribute = node->Attributes->get_ItemOf(S"name");
			if(nameAttribute)
			{
				if(nameAttribute->Value->Equals(name))
				{
					bankNode = node;
				}
			}
		}
		else if(node->Name->Equals(S"BankFolder"))
		{
			bankNode = FindWaveBankNode(node, name);
		}

		node = node->NextSibling;
	}

	return bankNode;
}

XmlNode *audWaveBuilder::FindWaveNode(XmlNode *parentNode, String *name)
{
	XmlNode *waveNode = NULL;

	XmlNode *node = parentNode->FirstChild;
	while(node && !waveNode)
	{
		if(node->Name->Equals(S"Wave"))
		{
			XmlAttribute *nameAttribute = node->Attributes->get_ItemOf(S"name");
			if(nameAttribute)
			{
				if(nameAttribute->Value->Equals(name))
				{
					waveNode = node;
				}
			}
		}
		else if(node->Name->Equals(S"WaveFolder"))
		{
			waveNode = FindWaveNode(node, name);
		}

		node = node->NextSibling;
	}

	return waveNode;
}

void audWaveBuilder::ReleaseAssets(void)
{
	if(m_AssetManager->GetAssetManagementType() == audAssetManagement::ASSET_MANAGER_PERFORCE)
	{
		//Don't attempt to undo the checkout of individual assets here as the build thread clean-up code
		//should revert and delete the changelist.
		return;
	}

	m_BuildClient->ReportProgress(-1, -1, S"Undoing check-out of Wave build assets", true);

	//Undo check-out of any Wave Banks.
	System::Collections::IEnumerator *waveBuildEnum = m_WaveBuildList->GetEnumerator();
	while(waveBuildEnum->MoveNext())
	{
		audWavePak *wavePak = __try_cast<audWavePak *>(waveBuildEnum->Current);

		if(wavePak->m_BuildCommand == audWave::audBuildCommands::UNASSIGNED)
		{
			//This Wave Pak was not directly manipulated, so check the child Wave Banks.
			System::Collections::IEnumerator *waveBankEnum = wavePak->m_BankList->GetEnumerator();
			while(waveBankEnum->MoveNext())
			{
				audWaveBank *waveBank = __try_cast<audWaveBank *>(waveBankEnum->Current);

				if(waveBank->m_BuildCommand == audWave::audBuildCommands::UNASSIGNED)
				{
					//This Wave Bank was not directly manipulated, so check the child Waves.
					bool shouldUndoCheckOutOfParentWaveBank = false;
					System::Collections::IEnumerator *waveEnum = waveBank->m_WaveList->GetEnumerator();
					while(waveEnum->MoveNext())
					{
						audWave *wave = __try_cast<audWave *>(waveEnum->Current);

						if(wave->m_BuildCommand != audWave::audBuildCommands::UNASSIGNED)
						{
							//This Wave was manipulated, so we need to undo the check-out of the previously built
							//parent Wave Bank.
							shouldUndoCheckOutOfParentWaveBank = true;
							break;
						}
					}

					if(shouldUndoCheckOutOfParentWaveBank)
					{
						//Undo the check-out of the previously built parent Wave Bank as a child Wave was
						//manipulated.
						String *wavePaksPath = new String(g_WavePaksPath);
						if(!m_AssetManager->SimpleUndoCheckOut(String::Concat(m_AssetBuildDataPath,
							wavePaksPath, wavePak->m_Name, S"\\", waveBank->m_Name)))
						{
							audBuildException *exception = new audBuildException(
								String::Concat(S"Failed to undo check-out of previously built Wave Bank - ",
								wavePak->m_Name, S"\\", waveBank->m_Name));
							throw(exception);
						}
					}
				}
				else if(waveBank->m_BuildCommand == audWave::audBuildCommands::MODIFY)
				{
					//Undo the check-out of the previously built Wave Bank.
					String *wavePaksPath = new String(g_WavePaksPath);
					if(!m_AssetManager->SimpleUndoCheckOut(String::Concat(m_AssetBuildDataPath, wavePaksPath,
						wavePak->m_Name, S"\\", waveBank->m_Name)))
					{
						audBuildException *exception = new audBuildException(
							String::Concat(S"Failed to undo check-out of previously built Wave Bank - ",
							wavePak->m_Name, S"\\", waveBank->m_Name));
						throw(exception);
					}
				}
				//else if(waveBank->m_BuildCommand == audWave::audBuildCommands::ADD)
				//	- No Wave Bank assets were required for Wave Bank creation.
				//else if(waveBank->m_BuildCommand == audWave::audBuildCommands::REMOVE)
				//	- No Wave Bank assets were required for Wave Bank removal.
			}
		}
		else if(wavePak->m_BuildCommand == audWave::audBuildCommands::MODIFY)
		{
			//Undo the check-out of all previously built Wave Banks.
			System::Collections::IEnumerator *waveBankEnum = wavePak->m_BankList->GetEnumerator();
			while(waveBankEnum->MoveNext())
			{
				audWaveBank *waveBank = __try_cast<audWaveBank *>(waveBankEnum->Current);

				if((waveBank->m_BuildCommand != audWave::audBuildCommands::ADD) &&
					(waveBank->m_BuildCommand != audWave::audBuildCommands::REMOVE))
				{
					String *wavePaksPath = new String(g_WavePaksPath);
					if(!m_AssetManager->SimpleUndoCheckOut(String::Concat(m_AssetBuildDataPath, wavePaksPath,
						wavePak->m_Name, S"\\", waveBank->m_Name)))
					{
						audBuildException *exception = new audBuildException(
							String::Concat(S"Failed to undo check-out of previously built Wave Bank - ",
							wavePak->m_Name, S"\\", waveBank->m_Name));
						throw(exception);
					}
				}
			}
		}
		//else if(wavePak->m_BuildCommand == audWave::audBuildCommands::ADD)
		//	- No Wave Bank assets were required for Wave Pak creation.
		//else if(wavePak->m_BuildCommand == audWave::audBuildCommands::REMOVE)
		//	- No Wave Bank assets were required for Wave Pak removal.
	}

	if(m_ShouldBuildLocally)
	{
		//Restore the read-only status of all local built wave assets.
		m_AssetManager->MakeLocalAssetReadOnly(String::Concat(m_LocalBuildDataPath, g_WavePaksPath));
	}
}

void audWaveBuilder::CommitAssets(void)
{
	m_BuildClient->ReportProgress(-1, -1, S"Checking-in / importing Wave build assets", true);

	// This is a bit dodge.  If perforce is our asset management abstraction, we can't check-in based on 
	// a directory name since perforce doesn't track folders.  Instead, bypass the folder checks and iterate
	// over all the wavebanks in the build.  Note that we skip any 'if' check that does an import/checkIn 
	// by folder.
	const bool isPerforce = m_AssetManager->GetAssetManagementType() == audAssetManagement::ASSET_MANAGER_PERFORCE;

	//Check that the BuildData folder exists in the project database.
	if(m_AssetManager->ExistsAsAsset(m_AssetBuildDataPath) || isPerforce)
	{
		//Check that the parent asset folder exists in the project database.
		if(m_AssetManager->ExistsAsAsset(String::Concat(m_AssetBuildDataPath, g_WavePaksPath)) || isPerforce)
		{
			//Import any newly built Wave Paks/Banks, check-in any modified Wave Banks and delete any removed Wave
			//Paks/Banks.
			System::Collections::IEnumerator *waveBuildEnum = m_WaveBuildList->GetEnumerator();
			while(waveBuildEnum->MoveNext())
			{
				audWavePak *wavePak = __try_cast<audWavePak *>(waveBuildEnum->Current);
				String *localWavePakPath = String::Concat(m_LocalBuildDataPath, g_WavePaksPath, wavePak->m_Name);
				String *assetWavePakPath = String::Concat(m_AssetBuildDataPath, g_WavePaksPath, wavePak->m_Name);

				if(wavePak->m_BuildCommand == audWave::audBuildCommands::ADD && !isPerforce)
				{
					//This entire Wave Pak was added, so import this folder (recursively importing all child Wave
					//Banks.)
					if(!m_AssetManager->Import(localWavePakPath, String::Concat(m_AssetBuildDataPath,
						g_WavePaksPath), S"Wave Build"))
					{
						audBuildException *exception = new audBuildException(
							String::Concat(S"Failed to import new Wave Pak folder - ", wavePak->m_Name));
						throw(exception);
					}
				}
				else if(wavePak->m_BuildCommand == audWave::audBuildCommands::MODIFY && !isPerforce)
				{
					//Import all added Wave Banks, delete all removed Wave Banks and check-in all modified Wave Banks.
					System::Collections::IEnumerator *waveBankEnum = wavePak->m_BankList->GetEnumerator();
					while(waveBankEnum->MoveNext())
					{
						audWaveBank *waveBank = __try_cast<audWaveBank *>(waveBankEnum->Current);

						if(waveBank->m_BuildCommand == audWave::audBuildCommands::ADD)
						{
							if(!m_AssetManager->Import(String::Concat(localWavePakPath, S"\\", waveBank->m_Name),
								String::Concat(assetWavePakPath, S"\\", waveBank->m_Name), S"Wave Build"))
							{
								audBuildException *exception = new audBuildException(
									String::Concat(S"Failed to import new Wave Bank - ", wavePak->m_Name, S"\\",
									waveBank->m_Name));
								throw(exception);
							}
						}
						else if(waveBank->m_BuildCommand == audWave::audBuildCommands::REMOVE)
						{
							if(!m_AssetManager->DeleteAsset(String::Concat(localWavePakPath, S"\\", waveBank->m_Name),
								String::Concat(assetWavePakPath, S"\\", waveBank->m_Name), true))
							{
								audBuildException *exception = new audBuildException(
									String::Concat(S"Failed to delete Wave Bank - ", wavePak->m_Name, S"\\",
									waveBank->m_Name));
								throw(exception);
							}
						}
						else
						{
							if(!m_AssetManager->CheckIn(String::Concat(assetWavePakPath, S"\\",
								waveBank->m_Name), S"Wave Build"))
							{
								audBuildException *exception = new audBuildException(
									String::Concat(S"Failed to check-in modified Wave Bank - ",
									wavePak->m_Name, S"\\", waveBank->m_Name));
								throw(exception);
							}
						}
					}
				}
				else if(wavePak->m_BuildCommand == audWave::audBuildCommands::REMOVE && !isPerforce)
				{
					//This entire Wave Pak was removed, so delete this folder in the project database.
					if(!m_AssetManager->DeleteAsset(localWavePakPath, assetWavePakPath, true))
					{
						audBuildException *exception = new audBuildException(
							String::Concat(S"Failed to delete Wave Pak folder - ", wavePak->m_Name));
						throw(exception);
					}
				}
				else if(wavePak->m_BuildCommand == audWave::audBuildCommands::UNASSIGNED || isPerforce)
				{
					//This Wave Pak was not directly manipulated, so check the child Wave Banks.
					System::Collections::IEnumerator *waveBankEnum = wavePak->m_BankList->GetEnumerator();
					while(waveBankEnum->MoveNext())
					{
						audWaveBank *waveBank = __try_cast<audWaveBank *>(waveBankEnum->Current);

						if(waveBank->m_BuildCommand == audWave::audBuildCommands::ADD)
						{
							//This entire Wave Bank was added, so import this asset.
							if(!m_AssetManager->Import(String::Concat(localWavePakPath, S"\\", waveBank->m_Name),
								String::Concat(assetWavePakPath, S"\\", waveBank->m_Name), S"Wave Build"))
							{
								audBuildException *exception = new audBuildException(
									String::Concat(S"Failed to import new Wave Bank - ", wavePak->m_Name, S"\\",
									waveBank->m_Name));
								throw(exception);
							}
						}
						else if(waveBank->m_BuildCommand == audWave::audBuildCommands::MODIFY)
						{
							//Check-in the modified Wave Bank.
							if(!m_AssetManager->CheckIn(String::Concat(assetWavePakPath, S"\\", waveBank->m_Name),
								S"Wave Build"))
							{
								audBuildException *exception = new audBuildException(
									String::Concat(S"Failed to check-in modified Wave Bank - ",
									wavePak->m_Name, S"\\", waveBank->m_Name));
								throw(exception);
							}
						}
						else if(waveBank->m_BuildCommand == audWave::audBuildCommands::REMOVE)
						{
							//This entire Wave Bank was removed, so delete this asset in the project database.
							if(!m_AssetManager->DeleteAsset(String::Concat(localWavePakPath, S"\\",
								waveBank->m_Name), String::Concat(assetWavePakPath, S"\\", waveBank->m_Name), true))
							{
								audBuildException *exception = new audBuildException(
									String::Concat(S"Failed to delete Wave Bank - ", wavePak->m_Name, S"\\",
									waveBank->m_Name));
								throw(exception);
							}
						}
						else if(waveBank->m_BuildCommand == audWave::audBuildCommands::UNASSIGNED)
						{
							//This Wave Bank was not directly manipulated, so check the child Waves.
							bool shouldCheckInParentWaveBank = false;
							System::Collections::IEnumerator *waveEnum = waveBank->m_WaveList->GetEnumerator();
							while(waveEnum->MoveNext())
							{
								audWave *wave = __try_cast<audWave *>(waveEnum->Current);

								if(wave->m_BuildCommand != audWave::audBuildCommands::UNASSIGNED)
								{
									//This Wave was manipulated, so we need to check-in the modified parent
									//Wave Bank.
									shouldCheckInParentWaveBank = true;
									break;
								}
							}

							if(shouldCheckInParentWaveBank)
							{
								//Check-in the modified parent Wave Bank as one of the child Waves was
								//manipulated.
								if(!m_AssetManager->CheckIn(String::Concat(assetWavePakPath, S"\\",
									waveBank->m_Name), S"Wave Build"))
								{
									audBuildException *exception = new audBuildException(
										String::Concat(S"Failed to check-in modified Wave Bank - ",
										wavePak->m_Name, S"\\", waveBank->m_Name));
									throw(exception);
								}
							}
						}
					}
				}
			}
		}
		else
		{
			//Parent asset folder does not exist in the project database, so simply import the parent
			//folder.
			if(!m_AssetManager->Import(String::Concat(m_LocalBuildDataPath, g_WavePaksPath),
				m_AssetBuildDataPath, S"Wave Build"))
			{
				audBuildException *exception = new audBuildException(
					S"Failed to import Wave Paks folder");
				throw(exception);
			}
		}
	}
	else //The BuildData folder does not exist in the project database, so import the whole thing.
	{
		if(!m_AssetManager->Import(m_LocalBuildDataPath, m_AssetBuildDataPath->Substring(0,
			m_AssetBuildDataPath->LastIndexOf('\\')), S"Wave Build"))
		{
			audBuildException *exception = new audBuildException(
				S"Failed to import BuildData folder");
			throw(exception);
		}
	}

	if(m_ShouldBuildLocally)
	{
		//Restore the read-only status of all local built wave assets.
		m_AssetManager->MakeLocalAssetReadOnly(String::Concat(m_LocalBuildDataPath, g_WavePaksPath));
	}
}

void audWaveBuilder::Shutdown(void)
{
}

} // namespace audWaveBuild
