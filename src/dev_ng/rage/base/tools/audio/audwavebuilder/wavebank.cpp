//
// tools/audwavebuilder/wavebank.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#include "stdafx.h"

#include "wavebank.h"

#include "math.h"
#include "memory.h"

using namespace rage;

const int g_WaveBankFileBufferBytes = 50*1024*1024;

const float g_Epsilon = 1.00001f;

#ifndef MAX
#define MAX(x,y) ((x)>(y) ?(x):(y))
#endif
#ifndef MIN
#define MIN(x,y) ((x)<(y) ?(x):(y))
#endif

audWaveBank::audWaveBank(String *name, String *path, audWave::audBuildCommands buildCommand) : Object()
{
	m_Name = name;
	m_FullPath = path;
	m_BuildCommand = buildCommand;
	m_WaveList = new ArrayList();
	m_MetadataSizeBytes = -1;
	m_WaveMetadataLookupSizeBytes = -1;
	m_CustomMetadataLookupSizeBytes = -1;
	m_Compression = -1;
	m_SampleRate = -1;
	m_BuiltSize=-1;
	m_StreamingBlockBytes = 0;
}

void audWaveBank::Build(String *pakFolderPath, String *parentName, int compression, int sampleRate,
	unsigned int streamingBlockBytes, audBuildClient *buildClient, int totalWaves, int &wavesBuilt,
	DateTime waveBuildStartDateTime, bool /*isDeferred*/)
{
	if(m_WaveList->get_Count() == 0)
	{
		audBuildException *exception = new audBuildException(String::Concat(
			S"Wave Bank contains no Waves: ", m_Name));
		throw(exception);
	}

	if(m_Compression >= 0)
	{
		//Child Wave resources always override the compression setting of the parent.
		compression = m_Compression;
	}

	if(m_SampleRate >= 0)
	{
		//Child Wave resources always override the compression setting of the parent.
		sampleRate = m_SampleRate;
	}

	if(m_StreamingBlockBytes > 0)
	{
		//Child Wave resources always override the interleave bytes setting of the parent.
		streamingBlockBytes = m_StreamingBlockBytes;
	}

	
	bool shouldBuildWaveBank = false;
	//This Wave Bank is not to be directly manipulated, so check the child Waves.
	if(m_BuildCommand == audWave::audBuildCommands::UNASSIGNED)
	{
		System::Collections::IEnumerator *waveEnum = m_WaveList->GetEnumerator();
		while(waveEnum->MoveNext())
		{
			audWave *wave = __try_cast<audWave *>(waveEnum->Current);

			if(wave->m_BuildCommand != audWave::audBuildCommands::UNASSIGNED)
			{
				shouldBuildWaveBank = true;
				break;
			}
		}
	}

	//No need to build banks that are set to be removed. 
	if(shouldBuildWaveBank || (m_BuildCommand == audWave::audBuildCommands::ADD) || (m_BuildCommand == audWave::audBuildCommands::MODIFY))
	{
		int totalEstimatedTimeRemainingSecs = (int)Math::Floor((((TimeSpan)(DateTime::Now -
			waveBuildStartDateTime)).TotalMilliseconds / (double)wavesBuilt) *
			(double)(totalWaves - wavesBuilt) / 1000.0);
		buildClient->ReportProgress((int)Math::Floor((double)wavesBuilt * 100.0 / (double)totalWaves),
			totalEstimatedTimeRemainingSecs, String::Concat(S"Building ", parentName, S"\\", m_Name, S"..."),
			false); //Don't log.

		//This bank is to be added or modified, so build it from scratch.
		if(streamingBlockBytes > 0)
		{
			AddStreamingWaveBank(pakFolderPath, compression, sampleRate, streamingBlockBytes, wavesBuilt);
		}
		else
		{
			AddWaveBank(pakFolderPath, compression, sampleRate, wavesBuilt);
		}
	}
}

void audWaveBank::AddWaveBank(String *pakFolderPath, int compression, int sampleRate, int &wavesBuilt)
{
	//Prepare Bank header (to be populated as each Wave is built).
	audWaveBankMetadata waveBankMetadata = {0};
	waveBankMetadata.numWaves = (unsigned int)m_WaveList->get_Count();
	waveBankMetadata.waveMetadataLookupOffsetBytes = (unsigned __int64)sizeof(audWaveBankMetadata);
	waveBankMetadata.numCustomMetadataElements = 0;

	m_WaveMetadataLookupSizeBytes = waveBankMetadata.numWaves * sizeof(audMetadataLookupEntry);

	audMetadataLookupEntry *waveMetadataLookup = new audMetadataLookupEntry[waveBankMetadata.numWaves];
	memset(waveMetadataLookup, 0, m_WaveMetadataLookupSizeBytes);
	//Allow for as many custom metadata elements as we have Waves.
	audMetadataLookupEntry *customMetadataLookup = new audMetadataLookupEntry[waveBankMetadata.numWaves];
	memset(customMetadataLookup, 0, m_WaveMetadataLookupSizeBytes);

	unsigned int waveMetadataStartOffsetBytes = (unsigned int)waveBankMetadata.waveMetadataLookupOffsetBytes +
		(unsigned int)m_WaveMetadataLookupSizeBytes;

	//Write Wave metadata and sample data into audByteArrayLists.
	audByteArrayList *waveMetadataList = new audByteArrayList();
	audByteArrayList *customMetadataList = new audByteArrayList();
	audByteArrayList *waveSampleDataList = new audByteArrayList();

	System::Collections::IEnumerator* waveEnum = m_WaveList->GetEnumerator();
	int waveIndex = 0, waveIndex2;
	while(waveEnum->MoveNext())
	{
		audWave *wave = __try_cast<audWave *>(waveEnum->Current);

		if(wave->m_BuildCommand != audWave::REMOVE)
		{
			//Fix endianness after ordering.
			waveMetadataLookup[waveIndex].nameHash = wave->ComputeNameHash(true); 
			waveMetadataLookup[waveIndex].metadataOffsetBytes = (unsigned __int64)(waveMetadataList->get_Count());
			customMetadataLookup[waveBankMetadata.numCustomMetadataElements].nameHash =
				waveMetadataLookup[waveIndex].nameHash;
			customMetadataLookup[waveBankMetadata.numCustomMetadataElements].metadataOffsetBytes =
				(unsigned __int64)(customMetadataList->get_Count());

			//Ensure that this wave's name hash does not collide with that of any other wave.
			for(waveIndex2=0; waveIndex2<waveIndex; waveIndex2++)
			{
				if(waveMetadataLookup[waveIndex].nameHash == waveMetadataLookup[waveIndex2].nameHash)
				{
					audBuildException *exception = new audBuildException(String::Concat(
						S"Duplicate Wave name hash found for ", wave->m_Name));
					throw(exception);
				}
			}

			wave->Build(waveMetadataList, customMetadataList, waveSampleDataList, compression, sampleRate);

			//Fix endianness after ordering.
			waveMetadataLookup[waveIndex].metadataLengthBytes = (unsigned int)(waveMetadataList->get_Count()
				- waveMetadataLookup[waveIndex].metadataOffsetBytes);
			customMetadataLookup[waveBankMetadata.numCustomMetadataElements].metadataLengthBytes = (unsigned int)(
				customMetadataList->get_Count() - customMetadataLookup[waveBankMetadata.numCustomMetadataElements].
				metadataOffsetBytes);

			if(customMetadataLookup[waveBankMetadata.numCustomMetadataElements].metadataLengthBytes > 0)
			{
				waveBankMetadata.numCustomMetadataElements++;
			}

			waveIndex++;
			wavesBuilt++;
		}
	}

	if(waveIndex == 0)
	{
		return;
	}

	m_CustomMetadataLookupSizeBytes = waveBankMetadata.numCustomMetadataElements * sizeof(audMetadataLookupEntry);
	waveBankMetadata.customMetadataLookupOffsetBytes = (unsigned __int64)(waveMetadataStartOffsetBytes +
		(unsigned int)waveMetadataList->get_Count());

	//Format Wave Bank metadata for target platform endianness and order Wave and custom metadata lookup tables by
	//ascending name hash.
	audWaveBankMetadata formattedWaveBankMetadata = {0};
	formattedWaveBankMetadata.numWaves = audPlatformSpecific::FixEndian((unsigned int)(waveBankMetadata.numWaves));
	formattedWaveBankMetadata.waveMetadataLookupOffsetBytes = audPlatformSpecific::FixEndian((unsigned __int64)
		(waveBankMetadata.waveMetadataLookupOffsetBytes));
	formattedWaveBankMetadata.numCustomMetadataElements = audPlatformSpecific::FixEndian((unsigned int)
		(waveBankMetadata.numCustomMetadataElements));
	formattedWaveBankMetadata.customMetadataLookupOffsetBytes = audPlatformSpecific::FixEndian((unsigned __int64)
		(waveBankMetadata.customMetadataLookupOffsetBytes));

	audMetadataLookupEntry *formattedWaveMetadataLookup = new audMetadataLookupEntry[waveBankMetadata.numWaves];
	memset(formattedWaveMetadataLookup, 0, m_WaveMetadataLookupSizeBytes);

	audMetadataLookupEntry *formattedCustomMetadataLookup = NULL;
	if(waveBankMetadata.numCustomMetadataElements > 0)
	{
		formattedCustomMetadataLookup = new audMetadataLookupEntry[waveBankMetadata.numCustomMetadataElements];
		memset(formattedCustomMetadataLookup, 0, m_CustomMetadataLookupSizeBytes);
	}

	unsigned int prevLowestNameHash = 0, currentLowestNameHash;
	int waveIndexToAdd =0;
	for(int wavesAddedToLookup=0; wavesAddedToLookup<(int)waveBankMetadata.numWaves; wavesAddedToLookup++)
	{
		currentLowestNameHash = 4294967295;
		for(waveIndex=0; waveIndex<(int)waveBankMetadata.numWaves; waveIndex++)
		{
			if((waveMetadataLookup[waveIndex].nameHash > prevLowestNameHash) &&
				(waveMetadataLookup[waveIndex].nameHash < currentLowestNameHash))
			{
				currentLowestNameHash = waveMetadataLookup[waveIndex].nameHash;
				waveIndexToAdd = waveIndex;
			}
		}

		formattedWaveMetadataLookup[wavesAddedToLookup].nameHash = audPlatformSpecific::FixEndian((unsigned int)
			(waveMetadataLookup[waveIndexToAdd].nameHash));
		formattedWaveMetadataLookup[wavesAddedToLookup].metadataOffsetBytes =
			audPlatformSpecific::FixEndian(waveMetadataLookup[waveIndexToAdd].metadataOffsetBytes);
		formattedWaveMetadataLookup[wavesAddedToLookup].metadataLengthBytes =
			audPlatformSpecific::FixEndian(waveMetadataLookup[waveIndexToAdd].metadataLengthBytes);

		prevLowestNameHash = currentLowestNameHash;
	}

	int customElementIndex, customElementIndexToAdd=0;
	prevLowestNameHash = 0;
	for(int customElementsAddedToLookup=0; customElementsAddedToLookup<(int)waveBankMetadata.numCustomMetadataElements;
		customElementsAddedToLookup++)
	{
		currentLowestNameHash = 4294967295;
		for(customElementIndex=0; customElementIndex<(int)waveBankMetadata.numCustomMetadataElements;
			customElementIndex++)
		{
			if((customMetadataLookup[customElementIndex].nameHash > prevLowestNameHash) &&
				(customMetadataLookup[customElementIndex].nameHash < currentLowestNameHash))
			{
				currentLowestNameHash = customMetadataLookup[customElementIndex].nameHash;
				customElementIndexToAdd = customElementIndex;
			}
		}

		formattedCustomMetadataLookup[customElementsAddedToLookup].nameHash = audPlatformSpecific::FixEndian(
			(unsigned int)(customMetadataLookup[customElementIndexToAdd].nameHash));
		formattedCustomMetadataLookup[customElementsAddedToLookup].metadataOffsetBytes =
			audPlatformSpecific::FixEndian(customMetadataLookup[customElementIndexToAdd].metadataOffsetBytes);
		formattedCustomMetadataLookup[customElementsAddedToLookup].metadataLengthBytes =
			audPlatformSpecific::FixEndian(customMetadataLookup[customElementIndexToAdd].metadataLengthBytes);

		prevLowestNameHash = currentLowestNameHash;
	}

	unsigned int bankHeaderBytes = (unsigned int)waveBankMetadata.customMetadataLookupOffsetBytes +
		(unsigned int)m_CustomMetadataLookupSizeBytes + (unsigned int)customMetadataList->get_Count();

	//Determine any padding to guarantee the Bank header length is an integer multiple of 2048 bytes.
	unsigned int padLengthBytes = 2048 - (bankHeaderBytes % 2048);
	if(padLengthBytes == 2048)
	{
		padLengthBytes = 0;
	}

	bankHeaderBytes += padLengthBytes;
	formattedWaveBankMetadata.waveDataStartOffsetBytes = audPlatformSpecific::FixEndian(bankHeaderBytes);

	m_MetadataSizeBytes = sizeof(audWaveBankMetadata);

	WriteWaveBank(pakFolderPath, (void *)&formattedWaveBankMetadata, sizeof(audWaveBankMetadata),
		formattedWaveMetadataLookup, m_WaveMetadataLookupSizeBytes, waveMetadataList, formattedCustomMetadataLookup,
		m_CustomMetadataLookupSizeBytes, customMetadataList, NULL, 0, padLengthBytes, waveSampleDataList);

	delete[] waveMetadataLookup;
	delete[] customMetadataLookup;
	delete[] formattedWaveMetadataLookup;
	if(formattedCustomMetadataLookup)
	{
		delete[] formattedCustomMetadataLookup;
	}
}

void audWaveBank::AddStreamingWaveBank(String *pakFolderPath, int compression, int sampleRate,
	unsigned int streamingBlockBytes, int &wavesBuilt)
{
	unsigned int waveIndex;
	audWave *wave;

	//Prepare streaming Bank header (to be populated as each Wave is built).
	audStreamingWaveBankMetadata waveBankMetadata = {0};
	waveBankMetadata.base.waveMetadataLookupOffsetBytes = (unsigned __int64)sizeof(audStreamingWaveBankMetadata);
	waveBankMetadata.base.numCustomMetadataElements = 0;
	waveBankMetadata.streamingBlockBytes = streamingBlockBytes;

	waveBankMetadata.base.numWaves= 0;
	for(waveIndex=0; waveIndex<(unsigned int)m_WaveList->get_Count(); waveIndex++)
	{
		wave = __try_cast<audWave *>(m_WaveList->get_Item(waveIndex));
		if(wave->m_BuildCommand != audWave::REMOVE)
		{
			waveBankMetadata.base.numWaves++;
		}
	}

	m_WaveMetadataLookupSizeBytes = waveBankMetadata.base.numWaves * sizeof(audMetadataLookupEntry);

	audMetadataLookupEntry *waveMetadataLookup = new audMetadataLookupEntry[waveBankMetadata.base.numWaves];
	memset(waveMetadataLookup, 0, m_WaveMetadataLookupSizeBytes);
	//Allow for as many custom metadata elements as we have Waves.
	audMetadataLookupEntry *customMetadataLookup = new audMetadataLookupEntry[waveBankMetadata.base.numWaves];
	memset(customMetadataLookup, 0, m_WaveMetadataLookupSizeBytes);

	unsigned int waveMetadataStartOffsetBytes = (unsigned int)waveBankMetadata.base.waveMetadataLookupOffsetBytes +
		m_WaveMetadataLookupSizeBytes;

	//Write metadata and sample data for each Wave into separate audByteArrayLists.
	audByteArrayList *waveMetadataLists[] = new audByteArrayList *[waveBankMetadata.base.numWaves];
	//Allow for as many custom metadata elements as we have Waves.
	audByteArrayList *customMetadataLists[] = new audByteArrayList *[waveBankMetadata.base.numWaves];
	audByteArrayList *waveSampleDataLists[] = new audByteArrayList *[waveBankMetadata.base.numWaves];
	for(waveIndex=0; waveIndex<waveBankMetadata.base.numWaves; waveIndex++)
	{
		waveMetadataLists[waveIndex] = new audByteArrayList();
		customMetadataLists[waveIndex] = new audByteArrayList();
		waveSampleDataLists[waveIndex] = new audByteArrayList();
	}

	//
	//Build waves in order of increasing name hash (to ensure a consistent ordering is achieved between the waveMetadata
	//and the waveBlockMetadata Lists.)
	//
	unsigned int metadataOffsetBytes = 0;
	unsigned int customMetadataOffsetBytes = 0;
	unsigned int prevLowestNameHash = 0, currentLowestNameHash;
	unsigned int waveIndexToAdd = 0;
	unsigned int wavesAddedToLookup = 0;
	for(unsigned int currentWaveIndex=0; currentWaveIndex<(unsigned int)m_WaveList->get_Count(); currentWaveIndex++)
	{
		currentLowestNameHash = 4294967295;
		waveIndexToAdd = ~0U;
		for(waveIndex=0; waveIndex<(unsigned int)m_WaveList->get_Count(); waveIndex++)
		{
			wave = __try_cast<audWave *>(m_WaveList->get_Item(waveIndex));
			unsigned int nameHash = wave->ComputeNameHash(true);

			if(wave->m_BuildCommand != audWave::REMOVE && ((nameHash > prevLowestNameHash) && (nameHash < currentLowestNameHash)))
			{
				currentLowestNameHash = nameHash;
				waveIndexToAdd = waveIndex;
			}
		}

		if(waveIndexToAdd != ~0U)
		{
			prevLowestNameHash = currentLowestNameHash;
			wave = __try_cast<audWave *>(m_WaveList->get_Item(waveIndexToAdd));
			if(wave->m_BuildCommand != audWave::REMOVE)
			{
				//Fix endianness after ordering.
				waveMetadataLookup[wavesAddedToLookup].nameHash = currentLowestNameHash; 
				waveMetadataLookup[wavesAddedToLookup].metadataOffsetBytes = (unsigned __int64)metadataOffsetBytes;
				customMetadataLookup[waveBankMetadata.base.numCustomMetadataElements].nameHash =
					waveMetadataLookup[wavesAddedToLookup].nameHash;
				customMetadataLookup[waveBankMetadata.base.numCustomMetadataElements].metadataOffsetBytes =
					(unsigned __int64)customMetadataOffsetBytes;

				//Ensure that this wave's name hash does not collide with that of any other wave.
				for(waveIndex=0; waveIndex<wavesAddedToLookup; waveIndex++)
				{
					if(waveMetadataLookup[wavesAddedToLookup].nameHash == waveMetadataLookup[waveIndex].nameHash)
					{
						audBuildException *exception = new audBuildException(String::Concat(
							S"Duplicate Wave name hash found for ", wave->m_Name));
						throw(exception);
					}
				}

				wave->Build(waveMetadataLists[wavesAddedToLookup], customMetadataLists[waveBankMetadata.base.
					numCustomMetadataElements], waveSampleDataLists[wavesAddedToLookup], compression, sampleRate);

				// on PS3 we want to strip out per-frame seek table data leaving just the audWaveMetadataPsn struct
				if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
				{
					waveMetadataLookup[wavesAddedToLookup].metadataLengthBytes = sizeof(audWaveMetadataPsn);
					// also need to update the cached value in the wave object
					wave->m_MetadataSizeBytes = sizeof(audWaveMetadataPsn);
					if(wave->m_CustomMetadataSizeBytes)
					{
						wave->m_MetadataSizeBytes += wave->m_CustomMetadataSizeBytes;
					}
				}
				else
				{
					waveMetadataLookup[wavesAddedToLookup].metadataLengthBytes =
						(unsigned int)(waveMetadataLists[wavesAddedToLookup]->get_Count());
				}

				metadataOffsetBytes += waveMetadataLookup[wavesAddedToLookup].metadataLengthBytes;
			
				customMetadataLookup[waveBankMetadata.base.numCustomMetadataElements].metadataLengthBytes =
					(unsigned int)(customMetadataLists[waveBankMetadata.base.numCustomMetadataElements]->get_Count());
				customMetadataOffsetBytes += customMetadataLookup[waveBankMetadata.base.numCustomMetadataElements].metadataLengthBytes;

				if(customMetadataLookup[waveBankMetadata.base.numCustomMetadataElements].metadataLengthBytes > 0)
				{
					waveBankMetadata.base.numCustomMetadataElements++;
				}

				wavesAddedToLookup++;
				wavesBuilt++;
			}
		}
	}

	m_CustomMetadataLookupSizeBytes = waveBankMetadata.base.numCustomMetadataElements * sizeof(audMetadataLookupEntry);

	//buildClient->ReportProgress(-1, -1, String::Concat(S"Interleaving ", m_Name), true);

	GC::Collect();

	audByteArrayList *interleavedStreamDataList = new audByteArrayList();

	InterleaveStreams(&waveBankMetadata, waveMetadataLists, waveSampleDataLists, interleavedStreamDataList);

	audByteArrayList *concatenatedWaveMetadataList = new audByteArrayList();
	for(waveIndex=0; waveIndex<waveBankMetadata.base.numWaves; waveIndex++)
	{
		// on PS3 we want to strip out per-frame seek table data leaving just the audWaveMetadataPsn struct
		if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
		{
			const int sizeofMetadata = sizeof(audWaveMetadataPsn);
			waveMetadataLists[waveIndex]->RemoveRange(sizeofMetadata, waveMetadataLists[waveIndex]->get_Count() - sizeofMetadata);
		}
		concatenatedWaveMetadataList->AddRange(waveMetadataLists[waveIndex]);
		waveMetadataLists[waveIndex]->Clear();
		//waveMetadataLists[waveIndex]->TrimToSize();
		waveMetadataLists[waveIndex] = NULL;

		waveSampleDataLists[waveIndex]->Clear();
		//waveSampleDataLists[waveIndex]->TrimToSize();
		waveSampleDataLists[waveIndex] = NULL;
	}

	audByteArrayList *concatenatedCustomMetadataList = new audByteArrayList();
	for(waveIndex=0; waveIndex<waveBankMetadata.base.numCustomMetadataElements; waveIndex++)
	{
		if(customMetadataLists[waveIndex]->get_Count() > 0)
		{
			concatenatedCustomMetadataList->AddRange(customMetadataLists[waveIndex]);
			customMetadataLists[waveIndex]->Clear();
			//customMetadataLists[waveIndex]->TrimToSize();
		}

		customMetadataLists[waveIndex] = NULL;
	}

	waveMetadataLists = NULL;
	customMetadataLists = NULL;

	waveBankMetadata.base.customMetadataLookupOffsetBytes = (unsigned __int64)(waveMetadataStartOffsetBytes +
		(unsigned int)concatenatedWaveMetadataList->get_Count());

	//Format Wave Bank metadata and Wave and custom metadata lookup tables for target platform endianness.
	audStreamingWaveBankMetadata formattedWaveBankMetadata = {0};
	formattedWaveBankMetadata.base.numWaves = audPlatformSpecific::FixEndian((unsigned int)
		(waveBankMetadata.base.numWaves));
	formattedWaveBankMetadata.base.waveMetadataLookupOffsetBytes = audPlatformSpecific::FixEndian((unsigned __int64)
		(waveBankMetadata.base.waveMetadataLookupOffsetBytes));
	formattedWaveBankMetadata.base.numCustomMetadataElements = audPlatformSpecific::FixEndian((unsigned int)
		(waveBankMetadata.base.numCustomMetadataElements));
	formattedWaveBankMetadata.base.customMetadataLookupOffsetBytes = audPlatformSpecific::FixEndian((unsigned __int64)
		(waveBankMetadata.base.customMetadataLookupOffsetBytes));
	formattedWaveBankMetadata.numDataStreams = audPlatformSpecific::FixEndian((unsigned int)
		(waveBankMetadata.numDataStreams));
	formattedWaveBankMetadata.numLoadBlocks = audPlatformSpecific::FixEndian((unsigned int)
		(waveBankMetadata.numLoadBlocks));
	formattedWaveBankMetadata.streamingBlockBytes = audPlatformSpecific::FixEndian((unsigned int)
		(waveBankMetadata.streamingBlockBytes));

	audMetadataLookupEntry *formattedWaveMetadataLookup = new audMetadataLookupEntry[waveBankMetadata.base.numWaves];
	memset(formattedWaveMetadataLookup, 0, m_WaveMetadataLookupSizeBytes);

	audMetadataLookupEntry *formattedCustomMetadataLookup = NULL;
	if(waveBankMetadata.base.numCustomMetadataElements > 0)
	{
		formattedCustomMetadataLookup = new audMetadataLookupEntry[waveBankMetadata.base.numCustomMetadataElements];
		memset(formattedCustomMetadataLookup, 0, m_CustomMetadataLookupSizeBytes);
	}

	for(waveIndex=0; waveIndex<waveBankMetadata.base.numWaves; waveIndex++)
	{
		formattedWaveMetadataLookup[waveIndex].nameHash = audPlatformSpecific::FixEndian((unsigned int)
			(waveMetadataLookup[waveIndex].nameHash));
		formattedWaveMetadataLookup[waveIndex].metadataOffsetBytes =
			audPlatformSpecific::FixEndian(waveMetadataLookup[waveIndex].metadataOffsetBytes);
		formattedWaveMetadataLookup[waveIndex].metadataLengthBytes =
			audPlatformSpecific::FixEndian(waveMetadataLookup[waveIndex].metadataLengthBytes);
	}

	for(waveIndex=0; waveIndex<waveBankMetadata.base.numCustomMetadataElements; waveIndex++)
	{
		formattedCustomMetadataLookup[waveIndex].nameHash = audPlatformSpecific::FixEndian((unsigned int)
			(customMetadataLookup[waveIndex].nameHash));
		formattedCustomMetadataLookup[waveIndex].metadataOffsetBytes =
			audPlatformSpecific::FixEndian(customMetadataLookup[waveIndex].metadataOffsetBytes);
		formattedCustomMetadataLookup[waveIndex].metadataLengthBytes =
			audPlatformSpecific::FixEndian(customMetadataLookup[waveIndex].metadataLengthBytes);
	}

	unsigned int bankHeaderBytes = (unsigned int)waveBankMetadata.base.customMetadataLookupOffsetBytes +
		(unsigned int)m_CustomMetadataLookupSizeBytes + (unsigned int)concatenatedCustomMetadataList->get_Count();

	audLoadBlockSeekTableEntry *formattedLoadBlockSeekTable = new audLoadBlockSeekTableEntry[
		waveBankMetadata.numLoadBlocks];
	for(unsigned int blockIndex=0; blockIndex<waveBankMetadata.numLoadBlocks; blockIndex++)
	{
		formattedLoadBlockSeekTable[blockIndex].startSampleIndex = audPlatformSpecific::FixEndian(waveBankMetadata.
			loadBlockSeekTable[blockIndex].startSampleIndex);
		formattedLoadBlockSeekTable[blockIndex].sampleRate = audPlatformSpecific::FixEndian(waveBankMetadata.
			loadBlockSeekTable[blockIndex].sampleRate);
	}
	delete[] waveBankMetadata.loadBlockSeekTable;

	formattedWaveBankMetadata.loadBlockSeekTableOffsetBytes = audPlatformSpecific::FixEndian(
		(unsigned __int64)bankHeaderBytes);
	unsigned int loadBlockSeekTableSizeBytes = waveBankMetadata.numLoadBlocks * sizeof(audLoadBlockSeekTableEntry);
	bankHeaderBytes += loadBlockSeekTableSizeBytes;

	//Determine any padding to guarantee the Bank header length is an integer multiple of 2048 bytes.
	unsigned int padLengthBytes = 2048 - (bankHeaderBytes % 2048);
	if(padLengthBytes == 2048)
	{
		padLengthBytes = 0;
	}

	bankHeaderBytes += padLengthBytes;
	formattedWaveBankMetadata.base.waveDataStartOffsetBytes = audPlatformSpecific::FixEndian(bankHeaderBytes);

	//Include load block seek table in the size of the Bank metadata for reporting purposes.
	m_MetadataSizeBytes = sizeof(audStreamingWaveBankMetadata) + loadBlockSeekTableSizeBytes;

	GC::Collect();

	WriteWaveBank(pakFolderPath, (void *)&formattedWaveBankMetadata, sizeof(audStreamingWaveBankMetadata),
		formattedWaveMetadataLookup, m_WaveMetadataLookupSizeBytes, concatenatedWaveMetadataList,
		formattedCustomMetadataLookup, m_CustomMetadataLookupSizeBytes, concatenatedCustomMetadataList,
		formattedLoadBlockSeekTable, loadBlockSeekTableSizeBytes, padLengthBytes, interleavedStreamDataList);

	delete[] waveMetadataLookup;
	delete[] customMetadataLookup;
	delete[] formattedWaveMetadataLookup;
	if(formattedCustomMetadataLookup)
	{
		delete[] formattedCustomMetadataLookup;
	}
	delete[] formattedLoadBlockSeekTable;

	concatenatedWaveMetadataList->Clear();
	//concatenatedWaveMetadataList->TrimToSize();
	concatenatedWaveMetadataList = NULL;

	concatenatedCustomMetadataList->Clear();
	//concatenatedCustomMetadataList->TrimToSize();
	concatenatedCustomMetadataList = NULL;

	interleavedStreamDataList->Clear();
	//interleavedStreamDataList->TrimToSize();
	interleavedStreamDataList = NULL;

	GC::Collect();
}

void audWaveBank::InterleaveStreams(audStreamingWaveBankMetadata *waveBankMetadata, audByteArrayList *waveMetadataLists[],
	audByteArrayList *waveSampleDataLists[], audByteArrayList *interleavedStreamDataList)
{
	unsigned int waveIndex;

	//Nastiness to determine the correct size of a platform-specific structure in non-specific code... sigh.
	unsigned int waveBlockMetadataBytes;
	if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
	{
		waveBlockMetadataBytes = sizeof(audWaveBlockMetadataPsn);
	}
	else
	{
		waveBlockMetadataBytes = sizeof(audWaveBlockMetadataCommon);
	}

	//Calculate the number of packets per block and the block header size (aligned to the packet size).
	unsigned int baseBlockHeaderBytes = sizeof(audStreamingBlockMetadata) + (waveBankMetadata->base.numWaves * waveBlockMetadataBytes) +
		(waveBankMetadata->numDataStreams * sizeof(audDataBlockMetadata));
	unsigned int numPacketsPerBlock = (unsigned int)floorf((float)(waveBankMetadata->streamingBlockBytes - baseBlockHeaderBytes) /
		(float)(g_StreamingPacketBytes + sizeof(audPacketSeekTableEntry)));

	//The block header needs to be aligned to the packet size, so drop one streaming packet to accomodate this.
	numPacketsPerBlock--;

	//Prepare reusable block header.
	audStreamingBlockMetadata blockMetadata = {0};

	if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
	{
		blockMetadata.waveBlockMetadataList = (audWaveBlockMetadata *)new audWaveBlockMetadataPsn[waveBankMetadata->base.numWaves];
	}
	else
	{
		blockMetadata.waveBlockMetadataList = (audWaveBlockMetadata *)new audWaveBlockMetadataCommon[waveBankMetadata->base.numWaves];
	}

	memset(blockMetadata.waveBlockMetadataList, 0, waveBankMetadata->base.numWaves * waveBlockMetadataBytes);
	if(waveBankMetadata->numDataStreams > 0)
	{
		blockMetadata.dataBlockMetadataList = new audDataBlockMetadata[waveBankMetadata->numDataStreams];
		memset(blockMetadata.dataBlockMetadataList, 0, waveBankMetadata->numDataStreams * sizeof(audDataBlockMetadata));
	}
	blockMetadata.packetSeekTable = new audPacketSeekTableEntry[numPacketsPerBlock];
	memset(blockMetadata.packetSeekTable, 0, numPacketsPerBlock * sizeof(audPacketSeekTableEntry));

	unsigned int *waveSamplesToSkipNextPacket = new unsigned int[waveBankMetadata->base.numWaves];
	memset(waveSamplesToSkipNextPacket, 0, waveBankMetadata->base.numWaves * sizeof(unsigned int));

	unsigned int *fullPacketsAddedPerWave = new unsigned int[waveBankMetadata->base.numWaves];
	memset(fullPacketsAddedPerWave, 0, waveBankMetadata->base.numWaves * sizeof(unsigned int));

	//Convert managed Wave metadata lists to usable structures.
	audWaveMetadataBase **waveMetadataArray = new audWaveMetadataBase *[waveBankMetadata->base.numWaves];
	for(waveIndex=0; waveIndex<waveBankMetadata->base.numWaves; waveIndex++)
	{
		Byte managedWaveMetadataArray __gc[] = static_cast<Byte __gc[]>(waveMetadataLists[waveIndex]->ToArray(__typeof(Byte)));
		if (managedWaveMetadataArray != NULL)
		{
			waveMetadataArray[waveIndex] = (audWaveMetadataBase *)new unsigned char[waveMetadataLists[waveIndex]->
				get_Count()];
			Byte __pin *waveMetadataArrayPinned = &managedWaveMetadataArray[0];
			memcpy(waveMetadataArray[waveIndex], (void *)waveMetadataArrayPinned, waveMetadataLists[waveIndex]->get_Count());
		}
	}

	audLoadBlockSeekTableEntry *loadBlockSeekTableEntry;
	unsigned int numLoadBlocksAdded = 0;
	ArrayList *loadBlockSeekTableList = new ArrayList();
	while(1)
	{
		//Add load block.
		loadBlockSeekTableEntry = GenerateInterleavedBlock(waveBankMetadata, &blockMetadata, numPacketsPerBlock,
			fullPacketsAddedPerWave, waveSamplesToSkipNextPacket, waveMetadataArray, waveSampleDataLists,
			interleavedStreamDataList);

		if(loadBlockSeekTableEntry)
		{
			loadBlockSeekTableList->Add(new audLoadBlockSeekTableEntryWrapper(loadBlockSeekTableEntry));
			numLoadBlocksAdded++;
		}
		else
		{
			//Finished interleaving.
			break;
		}
	}

	//Note: Populate the waveDataOffsetBytes element of wave Metadata with the correct offset/address for the current load
	//block at run-time, so NULL for now.
	for(waveIndex=0; waveIndex<waveBankMetadata->base.numWaves; waveIndex++)
	{
		waveMetadataArray[waveIndex]->waveDataOffsetBytes = 0;
	}

	waveBankMetadata->numLoadBlocks = numLoadBlocksAdded;

	//Extract simple load block start time lookup table from managed ArrayList.
	waveBankMetadata->loadBlockSeekTable = new audLoadBlockSeekTableEntry[numLoadBlocksAdded];
	System::Collections::IEnumerator* blockEnum = loadBlockSeekTableList->GetEnumerator();
	int blockIndex = 0;
	audLoadBlockSeekTableEntryWrapper *loadBlockSeekTableEntryWrapper;
	while(blockEnum->MoveNext())
	{
		loadBlockSeekTableEntryWrapper = __try_cast<audLoadBlockSeekTableEntryWrapper *>(blockEnum->Current);
		memcpy(&(waveBankMetadata->loadBlockSeekTable[blockIndex]), loadBlockSeekTableEntryWrapper->GetEntry(),
			sizeof(audLoadBlockSeekTableEntry));
		blockIndex++;
	}

	delete[] blockMetadata.waveBlockMetadataList;
	if(blockMetadata.dataBlockMetadataList)
	{
		delete[] blockMetadata.dataBlockMetadataList;
	}
	delete[] blockMetadata.packetSeekTable;
	delete[] waveSamplesToSkipNextPacket;
	delete[] fullPacketsAddedPerWave;

	for(waveIndex=0; waveIndex<waveBankMetadata->base.numWaves; waveIndex++)
	{
		delete[] (unsigned char *)(waveMetadataArray[waveIndex]);
	}
	delete[] waveMetadataArray;
}

audLoadBlockSeekTableEntry *audWaveBank::GenerateInterleavedBlock(audStreamingWaveBankMetadata *waveBankMetadata,
	audStreamingBlockMetadata *blockMetadata, unsigned int numPacketsPerBlock, unsigned int *fullPacketsAddedPerWave,
	unsigned int *waveSamplesToSkipNextPacket, audWaveMetadataBase **waveMetadataArray,
	audByteArrayList *waveSampleDataLists[], audByteArrayList *outputDataList)
{
	bool *hasWaveCompleted = new bool[waveBankMetadata->base.numWaves];
	unsigned int waveIndex;
	int packetIndex;
	unsigned int totalPacketsToAdd = 0;
	unsigned int *wavePacketsToAdd = new unsigned int[waveBankMetadata->base.numWaves];
	memset(wavePacketsToAdd, 0, waveBankMetadata->base.numWaves * sizeof(unsigned int));

	audLoadBlockSeekTableEntry *loadBlockSeekTableEntry = NULL;

	audByteArrayList *interleavedSampleDataList = new audByteArrayList();

	unsigned int *numFramesInLastPacket = new unsigned int[waveBankMetadata->base.numWaves];
	unsigned int *numBytesInLastPacket = new unsigned int[waveBankMetadata->base.numWaves];

	//NOTE: Ignore data blocks for now.

	for(waveIndex=0; waveIndex<waveBankMetadata->base.numWaves; waveIndex++)
	{
		if(waveSamplesToSkipNextPacket[waveIndex] > 0)
		{
			//We need to replicate the last packet we added for this Wave in the last block (it was partial.)
			// - Assume we're safe to add at least one packet of every Wave.
			wavePacketsToAdd[waveIndex]++;
			totalPacketsToAdd++;
		}

		audWaveBlockMetadata *waveBlockMetadata;
		if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
		{
			waveBlockMetadata = (audWaveBlockMetadata *)(((audWaveBlockMetadataPsn *)(blockMetadata->waveBlockMetadataList)) +
				waveIndex);
		}
		else
		{
			waveBlockMetadata = blockMetadata->waveBlockMetadataList + waveIndex;
		}

		waveBlockMetadata->samplesToSkip = waveSamplesToSkipNextPacket[waveIndex];
	}

	float packetStartTimeMs, earliestPacketStartTimeMs;
	int earliestWaveIndex;
	while(totalPacketsToAdd < numPacketsPerBlock)
	{
		//Determine which of the next Wave packets starts earliest.
		earliestPacketStartTimeMs = 3600000.0f; //1 hour.
		earliestWaveIndex = -1;
		for(waveIndex=0; waveIndex<waveBankMetadata->base.numWaves; waveIndex++)
		{
			packetStartTimeMs = ComputePacketStartTimeMs(fullPacketsAddedPerWave[waveIndex] + wavePacketsToAdd[waveIndex],
				waveMetadataArray[waveIndex]);

			if(packetStartTimeMs < -0.5f)
			{
				hasWaveCompleted[waveIndex] = true;
			}
			else if(packetStartTimeMs < earliestPacketStartTimeMs)
			{
				hasWaveCompleted[waveIndex] = false;
				earliestPacketStartTimeMs = packetStartTimeMs;
				earliestWaveIndex = waveIndex;
			}
		}

		//TODO: Consider data packet start times.

		if(earliestWaveIndex < 0)
		{
			//Out of packets for all Waves.
			break;
		}

		wavePacketsToAdd[earliestWaveIndex]++;
		totalPacketsToAdd++;
	}

	if(totalPacketsToAdd > 0)
	{
		earliestPacketStartTimeMs = 3600000.0f; //1 hour.
		int earliestEndWaveIndex = -1;
		float packetEndTimeMs;
		float earliestPacketEndTimeMs = 3600000.0f; //1 hour.
		unsigned int totalPacketsAdded = 0;
		unsigned int paddingBytes;
		int bytesToCopy;
		int sourceOffsetBytes;
		int totalPaddingBytes;
		for(waveIndex=0; waveIndex<waveBankMetadata->base.numWaves; waveIndex++)
		{
			audWaveBlockMetadata *waveBlockMetadata;
			if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
			{
				waveBlockMetadata = (audWaveBlockMetadata *)(((audWaveBlockMetadataPsn *)(blockMetadata->waveBlockMetadataList)) +
					waveIndex);
			}
			else
			{
				waveBlockMetadata = blockMetadata->waveBlockMetadataList + waveIndex;
			}

			packetStartTimeMs = ComputePacketStartTimeMs(fullPacketsAddedPerWave[waveIndex], waveMetadataArray[waveIndex]);
			if((packetStartTimeMs >= -0.5f) && (packetStartTimeMs < earliestPacketStartTimeMs) &&
				(waveBlockMetadata->samplesToSkip == 0))
			{
				earliestPacketStartTimeMs = packetStartTimeMs;

				if(loadBlockSeekTableEntry == NULL)
				{
					loadBlockSeekTableEntry = new audLoadBlockSeekTableEntry;
				}

				loadBlockSeekTableEntry->startSampleIndex = (unsigned int )ComputePacketStartSampleIndex(
					fullPacketsAddedPerWave[waveIndex], waveMetadataArray[waveIndex]);
				loadBlockSeekTableEntry->sampleRate = (unsigned int)audPlatformSpecific::FixEndian(
					waveMetadataArray[waveIndex]->sampleRate);
			}

			totalPaddingBytes = 0;

			//Populate the Wave block metadata for this Wave.
			waveBlockMetadata->startOffsetPackets = totalPacketsAdded;
			waveBlockMetadata->numPackets = wavePacketsToAdd[waveIndex];
			waveBlockMetadata->numSamples = 0;

			if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
			{
				((audWaveBlockMetadataPsn *)waveBlockMetadata)->numBytes = 0;
				((audWaveBlockMetadataPsn *)waveBlockMetadata)->numFrames = 0;
			}

			for(packetIndex=0; packetIndex<(int)(wavePacketsToAdd[waveIndex]); packetIndex++)
			{
				//Populate the packet seek table entry for this packet.
				blockMetadata->packetSeekTable[totalPacketsAdded].startSampleIndex = (unsigned int)
					ComputePacketStartSampleIndex(fullPacketsAddedPerWave[waveIndex], waveMetadataArray[waveIndex]);
				if(packetIndex == 0)
				{
					if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::
						BUILD_PLATFORM_PC)
					{
						blockMetadata->packetSeekTable[totalPacketsAdded].startSampleIndex +=
							waveBlockMetadata->samplesToSkip;
					}
				}

				blockMetadata->packetSeekTable[totalPacketsAdded].endSampleIndex =
					ComputePacketEndSampleIndex(fullPacketsAddedPerWave[waveIndex], waveMetadataArray[waveIndex]);

				waveBlockMetadata->numSamples += blockMetadata->packetSeekTable[totalPacketsAdded].endSampleIndex -
					blockMetadata->packetSeekTable[totalPacketsAdded].startSampleIndex + 1;

				//Add Wave sample data packet.

				if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
				{
					//Determine the number of MP3 frames to fit into this streaming packet.
					unsigned int numFrames;
					bytesToCopy = ComputeMp3PacketSize(fullPacketsAddedPerWave[waveIndex], waveMetadataArray[waveIndex],
						numFrames);
					sourceOffsetBytes = ComputeMp3FrameOffsetFromPacket(fullPacketsAddedPerWave[waveIndex],
						waveMetadataArray[waveIndex]);

					((audWaveBlockMetadataPsn *)waveBlockMetadata)->numBytes += bytesToCopy;
					((audWaveBlockMetadataPsn *)waveBlockMetadata)->numFrames += numFrames;

					numFramesInLastPacket[waveIndex] = numFrames;
					numBytesInLastPacket[waveIndex] = bytesToCopy;

					if((bytesToCopy < 0) || (sourceOffsetBytes < 0))
					{
						audBuildException *exception = new audBuildException(S"Error parsing MP3 data for Wave");
						throw(exception);
					}
				}
				else
				{
					bytesToCopy = MAX(MIN(g_StreamingPacketBytes, audPlatformSpecific::FixEndian(
						waveMetadataArray[waveIndex]->lengthBytes) - (fullPacketsAddedPerWave[waveIndex] *
						g_StreamingPacketBytes)), 0);

					sourceOffsetBytes = fullPacketsAddedPerWave[waveIndex] * g_StreamingPacketBytes;
				}

				if(bytesToCopy > 0)
				{
					interleavedSampleDataList->AddRange(waveSampleDataLists[waveIndex]->GetRange(
						sourceOffsetBytes, bytesToCopy));
				}

				if(bytesToCopy < g_StreamingPacketBytes)
				{
					if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
					{
						//Accumulate the padding required for this Wave, to be applied at the end of the concatenated packets.
						paddingBytes = g_StreamingPacketBytes - bytesToCopy;
						totalPaddingBytes += paddingBytes;
					}
					else
					{
						//Pad remainder of packet.
						paddingBytes = g_StreamingPacketBytes - bytesToCopy;
						Byte zeros __gc[] = new Byte[paddingBytes];
						Array::Clear(zeros, 0, paddingBytes);
						interleavedSampleDataList->AddRange(static_cast<Array *>(zeros));
						zeros = NULL;
					}
				}

				totalPacketsAdded++;
				fullPacketsAddedPerWave[waveIndex]++;
			}

			if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
			{
				//Apply the padding for all of the streaming packets at the end of the concatenated (unpadded) packets.
				if(totalPaddingBytes > 0)
				{
					Byte zeros __gc[] = new Byte[totalPaddingBytes];
					Array::Clear(zeros, 0, totalPaddingBytes);
					interleavedSampleDataList->AddRange(static_cast<Array *>(zeros));
					zeros = NULL;
				}
			}

			if((packetIndex > 0) && !hasWaveCompleted[waveIndex])
			{
				packetEndTimeMs = ComputePacketEndTimeMs(fullPacketsAddedPerWave[waveIndex] - 1,
					waveMetadataArray[waveIndex]);
				if((packetEndTimeMs >= 0.0f) && (packetEndTimeMs < earliestPacketEndTimeMs))
				{
					earliestPacketEndTimeMs = packetEndTimeMs;
					earliestEndWaveIndex = waveIndex;
				}
			}
		}

		memset(waveSamplesToSkipNextPacket, 0, waveBankMetadata->base.numWaves * sizeof(unsigned int));

		if(earliestEndWaveIndex >= 0)
		{
			//Ensure that all other Waves are shortened to match the time of the earliest finishing Wave with this Block.
			// - Mark their final packets as part-utilized using waveSamplesToSkipNextPacket.
			unsigned int targetSamples;
			unsigned int numSamplesTrimmed;
			for(waveIndex=0; waveIndex<waveBankMetadata->base.numWaves; waveIndex++)
			{
				if(!hasWaveCompleted[waveIndex] && (waveIndex != (unsigned int)earliestEndWaveIndex))
				{
					audWaveBlockMetadata *waveBlockMetadata;
					if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
					{
						waveBlockMetadata = (audWaveBlockMetadata *)(((audWaveBlockMetadataPsn *)(blockMetadata->
							waveBlockMetadataList)) + waveIndex);
					}
					else
					{
						waveBlockMetadata = blockMetadata->waveBlockMetadataList + waveIndex;
					}

					//Calculate the number of samples to trim in order to match the end time for this Wave's last packet.
					targetSamples = (unsigned int)ComputeSamplesFromMs(earliestPacketEndTimeMs,
						waveMetadataArray[waveIndex]);
					packetIndex = waveBlockMetadata->startOffsetPackets + waveBlockMetadata->numPackets - 1;
					numSamplesTrimmed = blockMetadata->packetSeekTable[packetIndex].endSampleIndex - targetSamples;

					if((numSamplesTrimmed > 0) && (packetIndex >= 0))
					{
						blockMetadata->packetSeekTable[packetIndex].endSampleIndex = targetSamples;
						waveBlockMetadata->numSamples -= numSamplesTrimmed;

						waveSamplesToSkipNextPacket[waveIndex] = blockMetadata->packetSeekTable[packetIndex].
							endSampleIndex - blockMetadata->packetSeekTable[packetIndex].startSampleIndex + 1;

						if(waveSamplesToSkipNextPacket[waveIndex] == 0)
						{
							//This packet needs to be trimmed completely.
							waveBlockMetadata->numPackets--;

							// on PS3 we also need to reduce the numFrames and numBytes in this loadblock to compensate
							// for this removed packet.  Failing to do this results in submitting the packet twice at runtime
							if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
							{
								((audWaveBlockMetadataPsn *)waveBlockMetadata)->numBytes -= numBytesInLastPacket[waveIndex];
								((audWaveBlockMetadataPsn *)waveBlockMetadata)->numFrames -= numFramesInLastPacket[waveIndex];
							}
						}

						//This is no longer a 'full' packet, so ensure it gets added again in the next block.
						fullPacketsAddedPerWave[waveIndex]--;
					}
				}
			}
		}

		WriteInterleavedBlock(waveBankMetadata, blockMetadata, interleavedSampleDataList, numPacketsPerBlock,
			outputDataList);
	}
	//else
	//{
		//No packets to add another block.
	//}

	interleavedSampleDataList->Clear();
	//interleavedSampleDataList->TrimToSize();
	interleavedSampleDataList = NULL;

	delete[] wavePacketsToAdd;
	delete[] numFramesInLastPacket;
	delete[] numBytesInLastPacket;
	return loadBlockSeekTableEntry;
}

int audWaveBank::ComputePacketStartSampleIndex(unsigned int packetIndex, audWaveMetadataBase *waveMetadata)
{
	int sampleIndex = -1;

	if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_XENON)
	{
		//Parse XMA seek table.
		audWaveMetadataXenon *platformWaveMetadata = (audWaveMetadataXenon *)waveMetadata;
		if(packetIndex < audPlatformSpecific::FixEndian(platformWaveMetadata->platform.seekTableLengthSamples))
		{
			u32 *seekTable = (u32 *)((char *)platformWaveMetadata + (u32)audPlatformSpecific::FixEndian(
				platformWaveMetadata->platform.seekTableOffsetBytes));
			sampleIndex = audPlatformSpecific::FixEndian(seekTable[packetIndex]);
		}
	}
	else if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
	{
		if(packetIndex == 0)
		{
			sampleIndex = 0;
		}
		else
		{
			int frameIndex = ComputeMp3FrameIndexFromPacket(packetIndex, waveMetadata);
			if(frameIndex >= 0)
			{
				audWaveMetadataPsn *platformWaveMetadata = (audWaveMetadataPsn *)waveMetadata;
				sampleIndex = frameIndex * audPlatformSpecific::FixEndian(platformWaveMetadata->platform.samplesPerFrame);
			}
		}
	}
	else
	{
		//Simple 16-bit PCM conversion.
		sampleIndex = packetIndex * g_StreamingPacketBytes / 2;

		if(sampleIndex >= (int)audPlatformSpecific::FixEndian(waveMetadata->lengthSamples))
		{
			sampleIndex = -1;
		}
	}

	return sampleIndex;
}

int audWaveBank::ComputePacketEndSampleIndex(unsigned int packetIndex, audWaveMetadataBase *waveMetadata)
{
	int sampleIndex = -1;

	if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_XENON)
	{
		//Parse XMA seek table.
		audWaveMetadataXenon *platformWaveMetadata = (audWaveMetadataXenon *)waveMetadata;
		if(packetIndex < audPlatformSpecific::FixEndian(platformWaveMetadata->platform.seekTableLengthSamples) - 1)
		{
			u32 *seekTable = (u32 *)((char *)platformWaveMetadata + (u32)audPlatformSpecific::FixEndian(
				platformWaveMetadata->platform.seekTableOffsetBytes));
			sampleIndex = audPlatformSpecific::FixEndian(seekTable[packetIndex + 1]) - 1;
		}
		else if(packetIndex == audPlatformSpecific::FixEndian(platformWaveMetadata->platform.seekTableLengthSamples) - 1)
		{
			//Special-case for last packet in the Wave.
			sampleIndex = audPlatformSpecific::FixEndian(waveMetadata->lengthSamples) - 1;
		}
	}
	else if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
	{
		int frameIndex = ComputeMp3FrameIndexFromPacket(packetIndex, waveMetadata);
		if(frameIndex >= 0)
		{
			//Parse MP3 seek table.
			audWaveMetadataPsn *platformWaveMetadata = (audWaveMetadataPsn *)waveMetadata;
			u16 *seekTable = (u16 *)((char *)platformWaveMetadata + (u32)audPlatformSpecific::FixEndian(
				platformWaveMetadata->platform.seekTableOffsetBytes));

			u32 numFrames = audPlatformSpecific::FixEndian(platformWaveMetadata->platform.numFrames);
			u32 totalBytes = 0;
			u32 i;
			for(i=frameIndex; i<numFrames; i++)
			{
				u16 numFrameBytes = audPlatformSpecific::FixEndian(seekTable[i]);
				if(totalBytes + numFrameBytes > g_StreamingPacketBytes)
				{
					//This is a new streaming packet.
					sampleIndex = (int)(i * audPlatformSpecific::FixEndian(platformWaveMetadata->platform.samplesPerFrame)) - 1;
					break;
				}
				else
				{
					//Keep filling the current streaming packet.
					totalBytes += numFrameBytes;
				}
			}

			if(i == numFrames)
			{
				//This is the last streaming packet.
				sampleIndex = (int)audPlatformSpecific::FixEndian(waveMetadata->lengthSamples) - 1;
			}
		}
	}
	else
	{
		//Simple 16-bit PCM conversion.
		sampleIndex = ((packetIndex + 1) * g_StreamingPacketBytes / 2) - 1;

		if(sampleIndex > (int)audPlatformSpecific::FixEndian(waveMetadata->lengthSamples))
		{
			sampleIndex = (int)audPlatformSpecific::FixEndian(waveMetadata->lengthSamples) - 1;
		}
	}

	return sampleIndex;
}

int audWaveBank::ComputeMp3PacketSize(unsigned int packetIndex, audWaveMetadataBase *waveMetadata, unsigned int &numFrames)
{
	int sizeBytes = -1;
	int frameIndex = ComputeMp3FrameIndexFromPacket(packetIndex, waveMetadata);

	numFrames = 0;
	if(frameIndex >= 0)
	{
		//Parse MP3 seek table.
		audWaveMetadataPsn *platformWaveMetadata = (audWaveMetadataPsn *)waveMetadata;
		u16 *seekTable = (u16 *)((char *)platformWaveMetadata + (u32)audPlatformSpecific::FixEndian(
			platformWaveMetadata->platform.seekTableOffsetBytes));

		u32 totalBytes = 0;
		u32 totalFrames = audPlatformSpecific::FixEndian(platformWaveMetadata->platform.numFrames);
		for(u32 i=frameIndex; i<totalFrames; i++)
		{
			u16 numFrameBytes = audPlatformSpecific::FixEndian(seekTable[i]);
			if(totalBytes + numFrameBytes > g_StreamingPacketBytes)
			{
				//This is a new streaming packet.
				break;
			}
			else
			{
				//Keep filling the current streaming packet.
				totalBytes += numFrameBytes;
				numFrames++;
			}
		}

		sizeBytes = (int)totalBytes;
	}

	return sizeBytes;
}

int audWaveBank::ComputeMp3FrameOffsetFromPacket(unsigned int packetIndex, audWaveMetadataBase *waveMetadata)
{
	int frameOffset = -1;
	int frameIndex = ComputeMp3FrameIndexFromPacket(packetIndex, waveMetadata);

	if(frameIndex >= 0)
	{
		//Parse MP3 seek table.
		audWaveMetadataPsn *platformWaveMetadata = (audWaveMetadataPsn *)waveMetadata;
		u16 *seekTable = (u16 *)((char *)platformWaveMetadata + (u32)audPlatformSpecific::FixEndian(
			platformWaveMetadata->platform.seekTableOffsetBytes));

		//Accumulate the sizes of all frames prior to this one.
		frameOffset = 0;
		for(int i=0; i<frameIndex; i++)
		{
			u16 numFrameBytes = audPlatformSpecific::FixEndian(seekTable[i]);
			frameOffset += (int)numFrameBytes;
		}
	}

	return frameOffset;
}

int audWaveBank::ComputeMp3FrameIndexFromPacket(unsigned int packetIndex, audWaveMetadataBase *waveMetadata)
{
	int frameIndex = -1;

	//Parse MP3 seek table.
	audWaveMetadataPsn *platformWaveMetadata = (audWaveMetadataPsn *)waveMetadata;
	u16 *seekTable = (u16 *)((char *)platformWaveMetadata + (u32)audPlatformSpecific::FixEndian(
		platformWaveMetadata->platform.seekTableOffsetBytes));

	//Iterate through the MP3 frames until we reach the start of the specified streaming packet.
	u32 totalBytes = 0;
	u32 packetCount = 0;
	u32 numFrames = audPlatformSpecific::FixEndian(platformWaveMetadata->platform.numFrames);
	u32 i;
	for(i=0; i<numFrames; i++)
	{
		u16 numFrameBytes = audPlatformSpecific::FixEndian(seekTable[i]);
		if(totalBytes + numFrameBytes > g_StreamingPacketBytes)
		{
			//This is a new streaming packet.
			totalBytes = numFrameBytes;
			packetCount++;
		}
		else
		{
			//Keep filling the current streaming packet.
			totalBytes += numFrameBytes;
		}

		if(packetCount == packetIndex)
		{
			frameIndex = (int)i;
			break;
		}
	}

	return frameIndex;
}

float audWaveBank::ComputePacketStartTimeMs(unsigned int packetIndex, audWaveMetadataBase *waveMetadata)
{
	float startTimeMs = -1.0f;

	int sampleIndex = ComputePacketStartSampleIndex(packetIndex, waveMetadata);
	if(sampleIndex >= 0)
	{
		startTimeMs = (float)sampleIndex / ((float)(audPlatformSpecific::FixEndian(waveMetadata->sampleRate)) * 0.001f);
	}

	return startTimeMs;
}

float audWaveBank::ComputePacketEndTimeMs(unsigned int packetIndex, audWaveMetadataBase *waveMetadata)
{
	float endTimeMs = -1.0f;

	int sampleIndex = ComputePacketEndSampleIndex(packetIndex, waveMetadata);
	if(sampleIndex >= 0)
	{
		endTimeMs = (float)sampleIndex / ((float)(audPlatformSpecific::FixEndian(waveMetadata->sampleRate)) * 0.001f);
	}

	return endTimeMs;
}

unsigned int audWaveBank::ComputeSamplesFromMs(float numMilliseconds, audWaveMetadataBase *waveMetadata)
{
	return (unsigned int)floorf((numMilliseconds * (float)(audPlatformSpecific::FixEndian(waveMetadata->
		sampleRate)) * 0.001f) * g_Epsilon);
}

void audWaveBank::WriteInterleavedBlock(audStreamingWaveBankMetadata *waveBankMetadata,
	audStreamingBlockMetadata *blockMetadata, audByteArrayList *interleavedStreamDataList, unsigned int numPacketsPerBlock,
	audByteArrayList *outputDataList)
{
	//
	//Write block header (metadata) to output data list.
	//

	//Nastiness to determine the correct size of a platform-specific structure in non-specific code... sigh.
	unsigned int waveBlockMetadataBytes;
	if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
	{
		waveBlockMetadataBytes = sizeof(audWaveBlockMetadataPsn);
	}
	else
	{
		waveBlockMetadataBytes = sizeof(audWaveBlockMetadataCommon);
	}

	//Prepare a separate set of metadata, ready to be formatted for the target platform.
	audStreamingBlockMetadata formattedBlockMetadata = {0};
	unsigned __int64 waveBlockMetadataListOffsetBytes = (unsigned __int64)sizeof(audStreamingBlockMetadata);
	formattedBlockMetadata.waveBlockMetadataListOffsetBytes = audPlatformSpecific::FixEndian(
		waveBlockMetadataListOffsetBytes);
	formattedBlockMetadata.dataBlockMetadataListOffsetBytes = audPlatformSpecific::FixEndian(
		waveBlockMetadataListOffsetBytes + (unsigned __int64)(waveBankMetadata->base.numWaves *
		waveBlockMetadataBytes));
	formattedBlockMetadata.packetSeekTableOffsetBytes = audPlatformSpecific::FixEndian(waveBlockMetadataListOffsetBytes +
		(unsigned __int64)(waveBankMetadata->base.numWaves * waveBlockMetadataBytes) +
		(unsigned __int64)(waveBankMetadata->numDataStreams * sizeof(audDataBlockMetadata)));

	//Convert block metadata to a managed array and add to output data list.
	Byte managedBlockMetadata __gc[] = new Byte[sizeof(audStreamingBlockMetadata)];
	System::Runtime::InteropServices::Marshal::Copy(&formattedBlockMetadata, managedBlockMetadata, 0,
		sizeof(audStreamingBlockMetadata));
	outputDataList->AddRange(static_cast<Array *>(managedBlockMetadata));
	managedBlockMetadata = NULL;

	//Format Wave block metadata for target platform.

	audWaveBlockMetadata *formattedWaveBlockMetadataList;
	if(audPlatformSpecific::GetBuildPlatform() == audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3)
	{
		audWaveBlockMetadataPsn *formattedWaveBlockMetadataListPsn = new audWaveBlockMetadataPsn[waveBankMetadata->base.numWaves];
		formattedWaveBlockMetadataList = (audWaveBlockMetadata *)formattedWaveBlockMetadataListPsn;
		audWaveBlockMetadataPsn *unformattedBlockWaveMetadataListPsn = (audWaveBlockMetadataPsn *)(blockMetadata->waveBlockMetadataList);

		for(unsigned int waveIndex=0; waveIndex<waveBankMetadata->base.numWaves; waveIndex++)
		{
			formattedWaveBlockMetadataListPsn[waveIndex].startOffsetPackets = audPlatformSpecific::FixEndian(
				unformattedBlockWaveMetadataListPsn[waveIndex].startOffsetPackets);
			formattedWaveBlockMetadataListPsn[waveIndex].numPackets = audPlatformSpecific::FixEndian(
				unformattedBlockWaveMetadataListPsn[waveIndex].numPackets);
			formattedWaveBlockMetadataListPsn[waveIndex].samplesToSkip = audPlatformSpecific::FixEndian(
				unformattedBlockWaveMetadataListPsn[waveIndex].samplesToSkip);
			formattedWaveBlockMetadataListPsn[waveIndex].numSamples = (int)audPlatformSpecific::FixEndian((unsigned int)
				unformattedBlockWaveMetadataListPsn[waveIndex].numSamples);
			formattedWaveBlockMetadataListPsn[waveIndex].numFrames = audPlatformSpecific::FixEndian(
				unformattedBlockWaveMetadataListPsn[waveIndex].numFrames);
			formattedWaveBlockMetadataListPsn[waveIndex].numBytes = audPlatformSpecific::FixEndian(
				unformattedBlockWaveMetadataListPsn[waveIndex].numBytes);
		}
	}
	else
	{
		formattedWaveBlockMetadataList = (audWaveBlockMetadata *)new audWaveBlockMetadataCommon[waveBankMetadata->base.numWaves];

		for(unsigned int waveIndex=0; waveIndex<waveBankMetadata->base.numWaves; waveIndex++)
		{
			formattedWaveBlockMetadataList[waveIndex].startOffsetPackets = audPlatformSpecific::FixEndian(
				blockMetadata->waveBlockMetadataList[waveIndex].startOffsetPackets);
			formattedWaveBlockMetadataList[waveIndex].numPackets = audPlatformSpecific::FixEndian(
				blockMetadata->waveBlockMetadataList[waveIndex].numPackets);
			formattedWaveBlockMetadataList[waveIndex].samplesToSkip = audPlatformSpecific::FixEndian(
				blockMetadata->waveBlockMetadataList[waveIndex].samplesToSkip);
			formattedWaveBlockMetadataList[waveIndex].numSamples = (int)audPlatformSpecific::FixEndian((unsigned int)
				blockMetadata->waveBlockMetadataList[waveIndex].numSamples);
		}
	}

	//Convert Wave block metadata to a managed array and add to output data list.
	Byte managedWaveBlockMetadata __gc[] = new Byte[waveBankMetadata->base.numWaves * waveBlockMetadataBytes];
	System::Runtime::InteropServices::Marshal::Copy(formattedWaveBlockMetadataList, managedWaveBlockMetadata, 0,
		waveBankMetadata->base.numWaves * waveBlockMetadataBytes);
	outputDataList->AddRange(static_cast<Array *>(managedWaveBlockMetadata));
	delete[] formattedWaveBlockMetadataList;
	managedWaveBlockMetadata = NULL;

	audDataBlockMetadata *formattedDataBlockMetadataList = NULL;
	if(waveBankMetadata->numDataStreams > 0)
	{
		//Format data block metadata for target platform.
		formattedDataBlockMetadataList = new audDataBlockMetadata[waveBankMetadata->numDataStreams];
		for(unsigned int streamIndex=0; streamIndex<waveBankMetadata->numDataStreams; streamIndex++)
		{
			formattedDataBlockMetadataList[streamIndex].startOffsetPackets = audPlatformSpecific::FixEndian(
				blockMetadata->dataBlockMetadataList[streamIndex].startOffsetPackets);
			formattedDataBlockMetadataList[streamIndex].numPackets = audPlatformSpecific::FixEndian(
				blockMetadata->dataBlockMetadataList[streamIndex].numPackets);
		}

		//Convert data block metadata to a managed array and add to output data list.
		Byte managedDataBlockMetadata __gc[] = new Byte[waveBankMetadata->numDataStreams * sizeof(audDataBlockMetadata)];
		System::Runtime::InteropServices::Marshal::Copy(formattedDataBlockMetadataList, managedDataBlockMetadata, 0,
			waveBankMetadata->numDataStreams * sizeof(audDataBlockMetadata));
		outputDataList->AddRange(static_cast<Array *>(managedDataBlockMetadata));
		delete[] formattedDataBlockMetadataList;
		managedDataBlockMetadata = NULL;
	}

	//Format packet seek table for target platform.
	audPacketSeekTableEntry *formattedPacketSeekTable = new audPacketSeekTableEntry[numPacketsPerBlock];
	for(unsigned int packetIndex=0; packetIndex<numPacketsPerBlock; packetIndex++)
	{
		formattedPacketSeekTable[packetIndex].startSampleIndex = audPlatformSpecific::FixEndian(
			blockMetadata->packetSeekTable[packetIndex].startSampleIndex);
		formattedPacketSeekTable[packetIndex].endSampleIndex = audPlatformSpecific::FixEndian(
			blockMetadata->packetSeekTable[packetIndex].endSampleIndex);
	}

	//Convert Packet seek table to a managed array and add to output data list.
	Byte managedPacketSeekTable __gc[] = new Byte[numPacketsPerBlock * sizeof(audPacketSeekTableEntry)];
	System::Runtime::InteropServices::Marshal::Copy(formattedPacketSeekTable, managedPacketSeekTable, 0,
		numPacketsPerBlock * sizeof(audPacketSeekTableEntry));
	outputDataList->AddRange(static_cast<Array *>(managedPacketSeekTable));
	delete[] formattedPacketSeekTable;
	managedPacketSeekTable = NULL;

	//Pad block header to force byte-alignment to packet size.
	unsigned int paddingBytes = g_StreamingPacketBytes - (outputDataList->get_Count() % g_StreamingPacketBytes);
	if((paddingBytes > 0) && (paddingBytes < g_StreamingPacketBytes))
	{
		Byte zeros __gc[] = new Byte[paddingBytes];
		Array::Clear(zeros, 0, paddingBytes);
		outputDataList->AddRange(static_cast<Array *>(zeros));
		zeros = NULL;
	}

	//Add interleaved stream data to output data list.
	outputDataList->AddRange(interleavedStreamDataList);

	//Pad block to force byte-alignment to block size.
	paddingBytes = waveBankMetadata->streamingBlockBytes - (outputDataList->get_Count() %
		waveBankMetadata->streamingBlockBytes);
	if((paddingBytes > 0) && (paddingBytes < waveBankMetadata->streamingBlockBytes))
	{
		Byte zeros __gc[] = new Byte[paddingBytes];
		Array::Clear(zeros, 0, paddingBytes);
		outputDataList->AddRange(static_cast<Array *>(zeros));
		zeros = NULL;
	}
}

void audWaveBank::WriteWaveBank(String *pakFolderPath, void *waveBankMetadata, unsigned int waveBankMetadataBytes,
	audMetadataLookupEntry *waveMetadataLookup, unsigned int waveMetadataLookupBytes, audByteArrayList *waveMetadataList,
	audMetadataLookupEntry *customMetadataLookup, unsigned int customMetadataLookupBytes,
	audByteArrayList *customMetadataList, audLoadBlockSeekTableEntry *loadBlockSeekTable,
	unsigned int loadBlockSeekTableBytes, unsigned int headerPaddingBytes, audByteArrayList *waveSampleDataList)
{
	BinaryWriter *bankFileOut = NULL;

	try
	{
		FileStream *bankFileStream = File::Open(String::Concat(pakFolderPath, m_Name), FileMode::Create);
		BufferedStream *bankFileBufStream = new BufferedStream(bankFileStream, g_WaveBankFileBufferBytes);
		bankFileOut = new BinaryWriter(bankFileBufStream);

		//Convert Wave Bank metadata to a managed array and write out.
		Byte managedWaveBankMetadata __gc[] = new Byte[waveBankMetadataBytes];
		System::Runtime::InteropServices::Marshal::Copy(waveBankMetadata, managedWaveBankMetadata, 0,
			waveBankMetadataBytes);
		bankFileOut->Write(managedWaveBankMetadata);
		managedWaveBankMetadata = NULL;

		//Convert Wave metadata lookup table to a managed array and write out.
		Byte managedWaveMetadataLookup __gc[] = new Byte[waveMetadataLookupBytes];
		System::Runtime::InteropServices::Marshal::Copy(waveMetadataLookup, managedWaveMetadataLookup, 0,
			waveMetadataLookupBytes);
		bankFileOut->Write(managedWaveMetadataLookup);
		managedWaveMetadataLookup = NULL;

		//Write concatenated Wave metadata.
		//waveMetadataList->TrimToSize();
		Byte waveMetadataBytes __gc[] = static_cast<Byte __gc[]>(waveMetadataList->ToArray(__typeof(Byte)));
		bankFileOut->Write(waveMetadataBytes);
		waveMetadataBytes = NULL;

		//Convert custom metadata lookup table to a managed array and write out.
		if(customMetadataLookup)
		{
			Byte managedCustomMetadataLookup __gc[] = new Byte[customMetadataLookupBytes];
			System::Runtime::InteropServices::Marshal::Copy(customMetadataLookup, managedCustomMetadataLookup, 0,
				customMetadataLookupBytes);
			bankFileOut->Write(managedCustomMetadataLookup);
			managedCustomMetadataLookup = NULL;
		}

		//Write concatenated custom metadata.
		//customMetadataList->TrimToSize();
		if(customMetadataList->get_Count() > 0)
		{
			Byte customMetadataBytes __gc[] = static_cast<Byte __gc[]>(customMetadataList->ToArray(__typeof(Byte)));
			bankFileOut->Write(customMetadataBytes);
			customMetadataBytes = NULL;
		}

		if(loadBlockSeekTableBytes > 0)
		{
			//Convert load block start time lookup table to a managed array and write out.
			Byte managedLoadBlockSeekTable __gc[] = new Byte[loadBlockSeekTableBytes];
			System::Runtime::InteropServices::Marshal::Copy(loadBlockSeekTable, managedLoadBlockSeekTable, 0,
				loadBlockSeekTableBytes);
			bankFileOut->Write(managedLoadBlockSeekTable);
			managedLoadBlockSeekTable = NULL;
		}

		//Pad Bank header to guarantee a length that is an integer multiple of 2048 bytes.
		if(headerPaddingBytes > 0)
		{
			Byte alignmentPadBytes __gc[] = new Byte __gc[headerPaddingBytes];
			Array::Clear(alignmentPadBytes, 0, headerPaddingBytes);
			bankFileOut->Write(alignmentPadBytes);
			alignmentPadBytes = NULL;
		}

		//Write concatenated Wave sample data.
		//waveSampleDataList->TrimToSize();
		Byte waveSampleDataBytes __gc[] = static_cast<Byte __gc[]>(waveSampleDataList->ToArray(__typeof(Byte)));
		bankFileOut->Write(waveSampleDataBytes);
		m_BuiltSize = waveSampleDataBytes->Count;
		waveSampleDataBytes = NULL;

		//Flush data to disk.
		bankFileOut->Flush();
	}
	__finally
	{
		if(bankFileOut)
		{
			bankFileOut->Close();
		}
	}
}

void audWaveBank::ReconfigureWaves(String *pakFolderPath, int compression, int sampleRate, int &wavesBuilt)
{
	BinaryReader *bankFileIn=NULL;

	try
	{
		FileStream *bankFileStream = File::Open(String::Concat(pakFolderPath, m_Name), FileMode::Open);
		BufferedStream *bankFileBufStreamIn = new BufferedStream(bankFileStream, g_WaveBankFileBufferBytes);
		bankFileIn = new BinaryReader(bankFileBufStreamIn);

		//Read entire Wave Bank into memory.
		unsigned int waveBankLengthBytes = (unsigned int)bankFileStream->Length;
		Byte waveBankBytes __gc[] = bankFileIn->ReadBytes(waveBankLengthBytes);

		//Close previously built Wave Bank file.
		bankFileIn->Close();
		bankFileIn = NULL;
		bankFileStream = NULL;

		//Extract Wave Bank metadata.
		Byte __pin *waveBankBytesPinned = &waveBankBytes[0];
		audWaveBankMetadata *waveBankMetadata = (audWaveBankMetadata *)waveBankBytesPinned;

		unsigned int numWaves = audPlatformSpecific::FixEndian(waveBankMetadata->numWaves);

		//Extract Wave metadata lookup table (into a new buffer - so it can be resized later.)
		unsigned int waveMetadataLookupOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(
			waveBankMetadata->waveMetadataLookupOffsetBytes);
		audMetadataLookupEntry *waveMetadataLookup = new audMetadataLookupEntry[numWaves];
		memcpy(waveMetadataLookup, waveBankBytesPinned + waveMetadataLookupOffsetBytes, numWaves *
			sizeof(audMetadataLookupEntry));

		//Calculate length of concatenated Wave metadata.
		unsigned int waveMetadataLengthBytes = 0;
		for(unsigned int waveIndex=0; waveIndex<numWaves; waveIndex++)
		{
			waveMetadataLengthBytes += audPlatformSpecific::FixEndian(waveMetadataLookup[waveIndex].metadataLengthBytes);
		}

		//Extract Wave metadata.
		unsigned int waveMetadataStartOffsetBytes = waveMetadataLookupOffsetBytes + (numWaves *
			sizeof(audMetadataLookupEntry));
		Byte waveMetadataBytes __gc[] = new Byte[waveMetadataLengthBytes];
		Array::Copy(waveBankBytes, waveMetadataStartOffsetBytes, waveMetadataBytes, 0, waveMetadataLengthBytes);
		//Convert managed Wave metadata array into an audByteArrayList to simplify reconfiguration.
		audByteArrayList *waveMetadataList = new audByteArrayList(static_cast<Array *>(waveMetadataBytes));
		waveMetadataBytes = NULL;

		unsigned int numCustomMetadataElements = audPlatformSpecific::FixEndian(waveBankMetadata->
			numCustomMetadataElements);

		unsigned int customMetadataLookupOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(
				waveBankMetadata->customMetadataLookupOffsetBytes);

		audMetadataLookupEntry *customMetadataLookup = NULL;
		audByteArrayList *customMetadataList = NULL;
		if(numCustomMetadataElements > 0)
		{
			//Extract custom metadata lookup table (into a new buffer - so it can be resized later.)
			customMetadataLookup = new audMetadataLookupEntry[numCustomMetadataElements];
			memcpy(customMetadataLookup, waveBankBytesPinned + customMetadataLookupOffsetBytes,
				numCustomMetadataElements * sizeof(audMetadataLookupEntry));

			//Calculate length of concatenated custom metadata.
			unsigned int customMetadataLengthBytes = 0;
			for(unsigned int elementIndex=0; elementIndex<numCustomMetadataElements; elementIndex++)
			{
				customMetadataLengthBytes += audPlatformSpecific::FixEndian(customMetadataLookup[elementIndex].
					metadataLengthBytes);
			}

			//Extract Custom metadata.
			unsigned int customMetadataStartOffsetBytes = customMetadataLookupOffsetBytes + (numCustomMetadataElements *
				sizeof(audMetadataLookupEntry));
			Byte customMetadataBytes __gc[] = new Byte[customMetadataLengthBytes];
			Array::Copy(waveBankBytes, customMetadataStartOffsetBytes, customMetadataBytes, 0, customMetadataLengthBytes);
			//Convert managed custom metadata array into an audByteArrayList to simplify reconfiguration.
			customMetadataList = new audByteArrayList(static_cast<Array *>(customMetadataBytes));
			customMetadataBytes = NULL;
		}
		else
		{
			//Create a new audByteArrayList in case we need to add some custom metadata.
			customMetadataList = new audByteArrayList;
		}

		//Extract Wave sample data.
		unsigned int waveSampleDataStartOffsetBytes = audPlatformSpecific::FixEndian(waveBankMetadata->
			waveDataStartOffsetBytes);
		unsigned int waveSampleDataLengthBytes = waveBankLengthBytes - waveSampleDataStartOffsetBytes;
		Byte waveSampleDataBytes __gc[] = new Byte[waveSampleDataLengthBytes];
		Array::Copy(waveBankBytes, waveSampleDataStartOffsetBytes, waveSampleDataBytes, 0, waveSampleDataLengthBytes);
		//Convert managed Wave sample data array into an audByteArrayList to simplify reconfiguration.
		audByteArrayList *waveSampleDataList = new audByteArrayList(static_cast<Array *>(waveSampleDataBytes));
		waveSampleDataBytes = NULL;

		System::Collections::IEnumerator* waveEnum = m_WaveList->GetEnumerator();
		while(waveEnum->MoveNext())
		{
			audWave *wave = __try_cast<audWave *>(waveEnum->Current);
			if(wave->m_BuildCommand == audWave::audBuildCommands::ADD)
			{
				AddWaveToBank(wave, compression, sampleRate, waveBankMetadata, &waveMetadataLookup, waveMetadataList,
					&customMetadataLookup, customMetadataList, waveSampleDataList);
			}
			else if(wave->m_BuildCommand == audWave::audBuildCommands::REMOVE)
			{
				RemoveWaveFromBank(wave, waveBankMetadata, &waveMetadataLookup, waveMetadataList, &customMetadataLookup,
					customMetadataList, waveSampleDataList);
			}
			else if(wave->m_BuildCommand == audWave::audBuildCommands::MODIFY)
			{
				RemoveWaveFromBank(wave, waveBankMetadata, &waveMetadataLookup, waveMetadataList, &customMetadataLookup,
					customMetadataList, waveSampleDataList);
				AddWaveToBank(wave, compression, sampleRate, waveBankMetadata, &waveMetadataLookup, waveMetadataList,
					&customMetadataLookup, customMetadataList, waveSampleDataList);
			}

			wavesBuilt++;
		}

		//waveMetadataList->TrimToSize();
		//customMetadataList->TrimToSize();
		//waveSampleDataList->TrimToSize();

		m_WaveMetadataLookupSizeBytes = audPlatformSpecific::FixEndian(waveBankMetadata->numWaves) *
			sizeof(audMetadataLookupEntry);
		m_CustomMetadataLookupSizeBytes = audPlatformSpecific::FixEndian(waveBankMetadata->numCustomMetadataElements) *
			sizeof(audMetadataLookupEntry);

		customMetadataLookupOffsetBytes = waveMetadataLookupOffsetBytes + (unsigned int)m_WaveMetadataLookupSizeBytes +
			(unsigned int)waveMetadataList->get_Count();
		unsigned __int64 customMetadataLookupOffsetBytes64 = (unsigned __int64)customMetadataLookupOffsetBytes;
		waveBankMetadata->customMetadataLookupOffsetBytes = audPlatformSpecific::FixEndian(
			customMetadataLookupOffsetBytes64);

		unsigned int bankHeaderBytes = customMetadataLookupOffsetBytes + (unsigned int)m_CustomMetadataLookupSizeBytes +
			(unsigned int)customMetadataList->get_Count();

		//Determine any padding to guarantee the Bank header length is an integer multiple of 2048 bytes.
		unsigned int padLengthBytes = 2048 - (bankHeaderBytes % 2048);
		if(padLengthBytes == 2048)
		{
			padLengthBytes = 0;
		}

		bankHeaderBytes += padLengthBytes;
		waveBankMetadata->waveDataStartOffsetBytes = audPlatformSpecific::FixEndian(bankHeaderBytes);

		m_MetadataSizeBytes = sizeof(audWaveBankMetadata);

		//Write the Wave Bank back to the original file.
		WriteWaveBank(pakFolderPath, waveBankMetadata, sizeof(audWaveBankMetadata), waveMetadataLookup,
			m_WaveMetadataLookupSizeBytes, waveMetadataList, customMetadataLookup, m_CustomMetadataLookupSizeBytes,
			customMetadataList, NULL, 0, padLengthBytes, waveSampleDataList);

		delete[] waveMetadataLookup;
		delete[] customMetadataLookup;
	}
	__finally
	{
		if(bankFileIn)
		{
			bankFileIn->Close();
		}
	}
}

void audWaveBank::AddWaveToBank(audWave *wave, int compression, int sampleRate, audWaveBankMetadata *waveBankMetadata,
	audMetadataLookupEntry **waveMetadataLookup, audByteArrayList *waveMetadataList,
	audMetadataLookupEntry **customMetadataLookup, audByteArrayList *customMetadataList,
	audByteArrayList *waveSampleDataList)
{
	unsigned int numWaves = audPlatformSpecific::FixEndian(waveBankMetadata->numWaves);
	unsigned int numCustomMetadataElements = audPlatformSpecific::FixEndian(waveBankMetadata->numCustomMetadataElements);

	//
	//Generate encoded Wave metadata and sample data - appending onto existing data.
	//
	unsigned __int64 waveMetadataOffsetBytes = (unsigned __int64)waveMetadataList->get_Count();
	unsigned __int64 customMetadataOffsetBytes = (unsigned __int64)customMetadataList->get_Count();

	wave->Build(waveMetadataList, customMetadataList, waveSampleDataList, compression, sampleRate);

	//Find correct ordered position for this Wave in the Wave metadata lookup table, based upon it's name hash.
	//(Also ensuring that the name hash is unique in this Bank.)
	unsigned int newWaveNameHash = wave->ComputeNameHash(true);
	unsigned int waveIndex = 0;
	for( ; waveIndex<numWaves; waveIndex++)
	{
		unsigned int waveNameHash = audPlatformSpecific::FixEndian((*waveMetadataLookup)[waveIndex].nameHash);

		if(waveNameHash == newWaveNameHash)
		{
			audBuildException *exception = new audBuildException(String::Concat(
				S"Duplicate Wave name hash found when adding ", wave->m_Name, S" to Bank: ", m_Name));
			throw(exception);
		}
		else if(waveNameHash > newWaveNameHash)
		{
			break;
		}
	}

	//Allocate a new larger wave metadata lookup table.
	audMetadataLookupEntry *newWaveMetadataLookup = new audMetadataLookupEntry[numWaves + 1];

	//Insert Wave metadata (at correct ordered position) into the new lookup table.
	if(waveIndex > 0)
	{
		memcpy(newWaveMetadataLookup, *waveMetadataLookup, waveIndex * sizeof(audMetadataLookupEntry));
	}
	if(waveIndex < numWaves)
	{
		memcpy(&(newWaveMetadataLookup[waveIndex + 1]), &((*waveMetadataLookup)[waveIndex]), (numWaves - waveIndex) *
			sizeof(audMetadataLookupEntry));
	}

	newWaveMetadataLookup[waveIndex].nameHash = audPlatformSpecific::FixEndian(newWaveNameHash);
	newWaveMetadataLookup[waveIndex].metadataOffsetBytes = audPlatformSpecific::FixEndian(waveMetadataOffsetBytes);
	newWaveMetadataLookup[waveIndex].metadataLengthBytes = audPlatformSpecific::FixEndian(
		(unsigned int)(waveMetadataList->get_Count() - (unsigned int)waveMetadataOffsetBytes));

	delete[] *waveMetadataLookup;
	*waveMetadataLookup = newWaveMetadataLookup;

	waveBankMetadata->numWaves = audPlatformSpecific::FixEndian(numWaves + 1);

	if((unsigned int)customMetadataList->get_Count() > (unsigned int)customMetadataOffsetBytes)
	{
		//New custom metadata was added for this Wave.

		//Find correct ordered position for this custom metadata in the custom metadata lookup table, based upon the
		//associated Wave's name hash.
		unsigned int customElementIndex = 0;
		for( ; customElementIndex<numCustomMetadataElements; customElementIndex++)
		{
			unsigned int waveNameHash = audPlatformSpecific::FixEndian((*customMetadataLookup)[customElementIndex].
				nameHash);

			if(waveNameHash > newWaveNameHash)
			{
				break;
			}
		}

		//Allocate a new larger custom metadata lookup table.
		audMetadataLookupEntry *newCustomMetadataLookup = new audMetadataLookupEntry[numCustomMetadataElements + 1];

		//Insert custom metadata (at correct ordered position) into the new lookup table.
		if(customElementIndex > 0)
		{
			memcpy(newCustomMetadataLookup, *customMetadataLookup, customElementIndex * sizeof(audMetadataLookupEntry));
		}
		if(customElementIndex < numCustomMetadataElements)
		{
			memcpy(&(newCustomMetadataLookup[customElementIndex + 1]), &((*customMetadataLookup)[customElementIndex]),
				(numCustomMetadataElements - customElementIndex) * sizeof(audMetadataLookupEntry));
		}

		newCustomMetadataLookup[customElementIndex].nameHash = audPlatformSpecific::FixEndian(newWaveNameHash);
		newCustomMetadataLookup[customElementIndex].metadataOffsetBytes = audPlatformSpecific::FixEndian(
			customMetadataOffsetBytes);
		newCustomMetadataLookup[customElementIndex].metadataLengthBytes = audPlatformSpecific::FixEndian(
			(unsigned int)(customMetadataList->get_Count() - (unsigned int)customMetadataOffsetBytes));

		if(customMetadataLookup)
		{
			delete[] *customMetadataLookup;
		}
		*customMetadataLookup = newCustomMetadataLookup;

		waveBankMetadata->numCustomMetadataElements = audPlatformSpecific::FixEndian(numCustomMetadataElements + 1);
	}
}

void audWaveBank::RemoveWaveFromBank(audWave *wave, audWaveBankMetadata *waveBankMetadata,
	audMetadataLookupEntry **waveMetadataLookup, audByteArrayList *waveMetadataList,
	audMetadataLookupEntry **customMetadataLookup, audByteArrayList *customMetadataList,
	audByteArrayList *waveSampleDataList)
{
	bool removedWave = false;
	bool removedCustomElement = false;
	unsigned int waveIndex, customElementIndex;
	unsigned int removedWaveMetadataOffsetBytes =0, removedWaveMetadataSize =0;
	unsigned int removedCustomMetadataOffsetBytes =0, removedCustomMetadataSize =0;
	unsigned int removedWaveSampleDataOffsetBytes =0, removedWaveSampleDataSize =0;

	unsigned int numWaves = audPlatformSpecific::FixEndian(waveBankMetadata->numWaves);
	unsigned int numCustomMetadataElements = audPlatformSpecific::FixEndian(waveBankMetadata->numCustomMetadataElements);

	//Find Wave item in Wave metadata lookup table from name Hash.
	unsigned int nameHash = wave->ComputeNameHash(false);
	for(waveIndex=0; waveIndex<numWaves; waveIndex++)
	{
		if(audPlatformSpecific::FixEndian((*waveMetadataLookup)[waveIndex].nameHash) == nameHash)
		{
			//Found the Wave.
			removedWaveMetadataOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(
				(*waveMetadataLookup)[waveIndex].metadataOffsetBytes);
			removedWaveMetadataSize = audPlatformSpecific::FixEndian((*waveMetadataLookup)[waveIndex].
				metadataLengthBytes);

			Byte waveMetadataBytes __gc[] = static_cast<Byte __gc[]>((waveMetadataList->GetRange(
				removedWaveMetadataOffsetBytes,	sizeof(audWaveMetadataBase))->ToArray(__typeof(Byte))));
			Byte __pin *waveMetadataBytesPinned = &waveMetadataBytes[0];
			audWaveMetadataBase *waveMetadata = (audWaveMetadataBase *)waveMetadataBytesPinned;

			//Remove the Wave sample data.
			removedWaveSampleDataOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(
				waveMetadata->waveDataOffsetBytes);
			removedWaveSampleDataSize = audPlatformSpecific::FixEndian(waveMetadata->lengthBytes);
			//Ensure that we use the actual wave size (aligned to the packet size.)
			removedWaveSampleDataSize = (unsigned int)ceil((float)removedWaveSampleDataSize /
				(float)g_StreamingPacketBytes) * g_StreamingPacketBytes;
			waveSampleDataList->RemoveRange(removedWaveSampleDataOffsetBytes, removedWaveSampleDataSize);

			//Check if the Wave has any custom metadata.
			for(customElementIndex=0; customElementIndex<numCustomMetadataElements; customElementIndex++)
			{
				if(audPlatformSpecific::FixEndian((*customMetadataLookup)[customElementIndex].nameHash) == nameHash)
				{
					//Found custom metadata for this Wave.
					removedCustomMetadataOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(
						(*customMetadataLookup)[customElementIndex].metadataOffsetBytes);
					removedCustomMetadataSize = audPlatformSpecific::FixEndian((*customMetadataLookup)
						[customElementIndex].metadataLengthBytes);

					//Remove custom metadata.
					customMetadataList->RemoveRange(removedCustomMetadataOffsetBytes, removedCustomMetadataSize);

					audMetadataLookupEntry *newCustomMetadataLookup = NULL;
					if(numCustomMetadataElements - 1 > 0)
					{
						//Allocate a new smaller custom metadata lookup table.
						newCustomMetadataLookup = new audMetadataLookupEntry[numCustomMetadataElements - 1];

						//Remove entry in custom metadata lookup table and copy to new table.
						if(customElementIndex > 0)
						{
							memcpy(newCustomMetadataLookup, *customMetadataLookup, customElementIndex *
								sizeof(audMetadataLookupEntry));
						}
						if(customElementIndex < numCustomMetadataElements - 1)
						{
							//Slide other custom lookup entries up to cover removed item.
							memcpy(&(newCustomMetadataLookup[customElementIndex]), &((*customMetadataLookup)
								[customElementIndex + 1]), (numCustomMetadataElements - customElementIndex - 1) *
								sizeof(audMetadataLookupEntry));
						}
					}

					delete[] *customMetadataLookup;
					*customMetadataLookup = newCustomMetadataLookup;

					numCustomMetadataElements--;
					waveBankMetadata->numCustomMetadataElements = audPlatformSpecific::FixEndian(
						numCustomMetadataElements);
					removedCustomElement = true;
					break;
				}
			}

			//Remove Wave metadata.
			waveMetadataList->RemoveRange(removedWaveMetadataOffsetBytes, removedWaveMetadataSize);

			//Allocate a new smaller wave metadata lookup table.
			audMetadataLookupEntry *newWaveMetadataLookup = new audMetadataLookupEntry[numWaves - 1];

			//Remove entry in Wave metadata lookup table and copy to new table.
			if(waveIndex > 0)
			{
				memcpy(newWaveMetadataLookup, *waveMetadataLookup, waveIndex * sizeof(audMetadataLookupEntry));
			}
			if(waveIndex < numWaves - 1)
			{
				//Slide other Wave lookup entries up to cover removed item.
				memcpy(&(newWaveMetadataLookup[waveIndex]), &((*waveMetadataLookup)[waveIndex + 1]),
					(numWaves - waveIndex - 1) * sizeof(audMetadataLookupEntry));
			}

			delete[] *waveMetadataLookup;
			*waveMetadataLookup = newWaveMetadataLookup;

			numWaves--;
			waveBankMetadata->numWaves = audPlatformSpecific::FixEndian(numWaves);
			removedWave = true;
			break;
		}
	}

	if(removedWave)
	{
		//Fix Wave metadata and sample data offsets of any Waves in the Bank stored after the Wave that was removed.
		unsigned int waveMetadataOffsetBytes;
		unsigned int waveSampleDataOffsetBytes;
		Byte waveMetadataBytes __gc[] = 0;
		Byte __pin *waveMetadataBytesPinned = NULL;
		audWaveMetadataBase *waveMetadata;
		for(waveIndex=0; waveIndex<numWaves; waveIndex++)
		{
			waveMetadataOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian((*waveMetadataLookup)[waveIndex].
				metadataOffsetBytes);

			if(waveMetadataOffsetBytes > removedWaveMetadataOffsetBytes)
			{
				//Fix Wave metadata offset.
				waveMetadataOffsetBytes -= removedWaveMetadataSize;
				unsigned __int64 waveMetadataOffsetBytes64 = (unsigned __int64)(waveMetadataOffsetBytes);
				(*waveMetadataLookup)[waveIndex].metadataOffsetBytes = audPlatformSpecific::FixEndian(
					waveMetadataOffsetBytes64);

				//Fix Wave sample data offset.
				waveMetadataBytes = static_cast<Byte __gc[]>((waveMetadataList->GetRange(waveMetadataOffsetBytes,
					sizeof(audWaveMetadataBase))->ToArray(__typeof(Byte))));
				waveMetadataBytesPinned = &waveMetadataBytes[0];
				waveMetadata = (audWaveMetadataBase *)waveMetadataBytesPinned;

				waveSampleDataOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian(waveMetadata->
					waveDataOffsetBytes);
				unsigned __int64 waveDataOffsetBytes = (unsigned __int64)(waveSampleDataOffsetBytes -
					removedWaveSampleDataSize);
				waveMetadata->waveDataOffsetBytes = audPlatformSpecific::FixEndian(waveDataOffsetBytes);

				waveMetadataList->SetRange(waveMetadataOffsetBytes, static_cast<Array *>(waveMetadataBytes));
			}
		}
	}

	if(removedCustomElement)
	{
		//Fix custom metadata offsets of any Waves in the Bank stored after the Wave that was removed.
		unsigned int customMetadataOffsetBytes;
		for(customElementIndex=0; customElementIndex<numCustomMetadataElements; customElementIndex++)
		{
			customMetadataOffsetBytes = (unsigned int)audPlatformSpecific::FixEndian((*customMetadataLookup)
				[customElementIndex].metadataOffsetBytes);

			if(customMetadataOffsetBytes > removedCustomMetadataOffsetBytes)
			{
				//Fix custom metadata offset.
				customMetadataOffsetBytes -= removedCustomMetadataSize;
				unsigned __int64 customMetadataOffsetBytes64 = (unsigned __int64)(customMetadataOffsetBytes);
				(*customMetadataLookup)[customElementIndex].metadataOffsetBytes = audPlatformSpecific::FixEndian(
					customMetadataOffsetBytes64);
			}
		}
	}
}
