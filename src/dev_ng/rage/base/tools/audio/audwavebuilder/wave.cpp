//
// tools/audwavebuilder/wave.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#include "stdafx.h"

#include <vcclr.h>

#include "wave.h"

#include "audiosoundtypes/wavemetadatadefs.h"
#include "audiosoundtypes/wavemetadatamarkerdefs.h"

#pragma warning(push)
#pragma warning(disable: 4201)	//Nonstandard extension used : nameless struct/union
#pragma warning(disable: 4668)	//#if condition not defined

#define __servprov_h__
#include "windows.h"

#pragma warning(pop)			//Reenable the warnings

#include "wavReader.h"
#include "audiohardware/wavedefs.h"

using namespace audBuildCommon;
using namespace audWaveEncoding;
using namespace rage;
using namespace System::Runtime::InteropServices;

const double g_DesiredPeakLevel				= -2.0; //in dB.

audWave::audWave(String *name, String *alias, String *assetPath, String *localPath, audBuildCommands buildCommand) : Object()
{
	m_Name = name;
	if(!alias || alias->Equals(S""))
	{
		m_Alias = m_Name;
	}
	else
	{
		m_Alias = alias;
	}
	m_FullAssetPath = assetPath;
	m_FullLocalPath = localPath;
	m_BuildCommand = buildCommand;
	m_RawSizeBytes = 0;
	m_BuiltSizeBytes = 0;
	m_MetadataSizeBytes = 0;
	m_CustomMetadataSizeBytes = 0;
	m_RawSampleRate = 0;
	m_BuiltSampleRate = 0;
	m_BuiltPreloopPadding = 0;
	m_Compression = -1;
	m_SampleRate = -1;

	m_WaveMarkerList = new ArrayList;
}

void audWave::Build(audByteArrayList *waveMetadataList, audByteArrayList *customMetadataList,
	audByteArrayList *waveSampleDataList, int compression, int sampleRate)
{
	if(m_Compression >= 0)
	{
		//Child Wave resources always override the compression setting of the parent.
		compression = m_Compression;
	}

	if(m_SampleRate >= 0)
	{
		//Child Wave resources always override the sample rate setting of the parent.
		sampleRate = m_SampleRate;
	}

	audWavReader wavReader;
	if(!wavReader.Init((char *)(Marshal::StringToHGlobalAnsi(m_FullLocalPath).ToPointer())))
	{
		audBuildException *exception = new audBuildException(String::Concat(S"Error parsing Wave file - ", m_FullLocalPath));
		throw(exception);
	}

	audFmtChunk *fmtChunk = (audFmtChunk *)wavReader.GetChunk(FMT_CHUNK_ID);
	audSmplChunk *smplChunk = (audSmplChunk *)wavReader.GetChunk(SMPL_CHUNK_ID);
	audCueChunk *cueChunk = (audCueChunk *)wavReader.GetChunk(CUE_CHUNK_ID);
	audRiffChunk *dataChunk = (audRiffChunk *)wavReader.GetChunk(DATA_CHUNK_ID);

	m_WaveLabelList = new ArrayList;
	audRiffChunk *listChunk = NULL;
	do
	{
		listChunk = wavReader.GetNextChunk(LIST_CHUNK_ID, listChunk);
		if(listChunk)
		{
			audLabel *label = NULL;
			do
			{
				label = ((audListChunk *)listChunk)->GetNextLabel(label);
				if(label)
				{
					m_WaveLabelList->Add(new audLabelWrapper(label));
				}

			} while(label);
		}

	} while(listChunk);

	const audFmtHeader *formatHeader = fmtChunk->GetFmtHeader();
	if(formatHeader->compressionCode != WAVE_FORMAT_PCM)
	{
		audBuildException *exception = new audBuildException(String::Concat(S"Wave resource is not PCM - ", m_FullLocalPath));
		throw(exception);
	}
	if(formatHeader->numChannels != 1)
	{
		audBuildException *exception = new audBuildException(String::Concat(S"Wave resource is not mono - ", m_FullLocalPath));
		throw(exception);
	}
	if(formatHeader->bitsPerSample != 16)
	{
		audBuildException *exception = new audBuildException(String::Concat(S"Wave resource is not 16-bit - ", m_FullLocalPath));
		throw(exception);
	}

	audWaveMetadataBase waveMetadata; //Generic-format (no platform-specific elements.)
	waveMetadata.lengthBytes = (int)(dataChunk->GetChunkSize());
	waveMetadata.lengthSamples = waveMetadata.lengthBytes / 2;
	waveMetadata.sampleRate = (unsigned short)(formatHeader->sampleRate);

	//Copy the wave data into a new'ed buffer, so that we can delete it when resizing in follow-on processing.
	waveMetadata.waveData = new unsigned char[waveMetadata.lengthBytes];
	memcpy(waveMetadata.waveData, dataChunk->GetData(), waveMetadata.lengthBytes);

	m_RawSizeBytes = waveMetadata.lengthBytes;

	if((smplChunk != NULL) && (smplChunk->GetSamplerHeader()->cSampleLoops == 1))
	{
		waveMetadata.loopStartOffsetSamples = smplChunk->GetSamplerHeader()->Loops[0].dwStart;
	}
	else
	{
		waveMetadata.loopStartOffsetSamples = -1;
	}

	//Peak normalise the Wave data and derive it's headroom (converting to fixed-point mB).
	waveMetadata.headroom = (short)Math::Floor(PeakNormalise((short *)(waveMetadata.waveData), waveMetadata.lengthSamples, g_DesiredPeakLevel) * 100.0);

	if(compression < 0) //If no compression has been specified, use highest quality by default.
	{
		compression = 100;
	}

	//Create a suitable encoder and process.
	audWaveEncoder *encoder = NULL;
	switch(audPlatformSpecific::GetBuildPlatform())
	{
		case audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_XENON:
			if(compression <= 100)
			{
				encoder = new audWaveEncoderXenon();
			}
			else //A compression setting above 100 is used to specify no compression (PCM.)
			{
				encoder = new audWaveEncoder();
			}

			break;

		case audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PC:
			encoder = new audWaveEncoder();
			break;

		case audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3:
			if (compression <= 100)
			{
				// compression settings below 100 uses ATRAC3
				encoder = new audWaveEncoderPSN(m_FullLocalPath);
			}
			else
			{
				// PCM
				encoder = new audWaveEncoder();
			}
			break;

		//Add new encode-supported platforms here.

		default:
			break;
	}

	if(encoder == NULL)
	{
		audBuildException *exception = new audBuildException(S"Could not create a Wave encoder for the target platform");
		throw(exception);
	}

	void *waveMetadataOut = NULL;
	unsigned int waveMetadataOutLengthBytes = 0;
	void *waveSampleDataOut = NULL;
	unsigned int waveSampleDataOutLengthBytes = 0;
	unsigned int noOfBitsInLastPacket = 0;

	m_RawSampleRate = (int)(waveMetadata.sampleRate);
	int preloopPadding = 0;

	if(sampleRate > 0)
	{
		try{
			//Resample raw Wave data.
			if(!encoder->Resample(new audWaveMetadataBaseWrapper(&waveMetadata), sampleRate))
			{
				audBuildException *exception = new audBuildException(String::Concat(S"Error resampling Wave file - ", m_FullLocalPath));
				throw(exception);
			}
		}
		catch(Exception *e)
		{
			audBuildException *exception = new audBuildException(String::Concat(S"Error resampling Wave file - ", m_FullLocalPath,e->ToString()));
			throw(exception);
		}
	}

	//Encode the Wave data using the appropriate encoder for this build platform.
	if(!encoder->Encode(new audWaveMetadataBaseWrapper(&waveMetadata), (unsigned int)waveSampleDataList->get_Count(),
		&waveMetadataOut, waveMetadataOutLengthBytes, &waveSampleDataOut, waveSampleDataOutLengthBytes, compression,
		preloopPadding, noOfBitsInLastPacket))
	{
		audBuildException *exception = new audBuildException(String::Concat(S"Error encoding Wave file - ", m_FullLocalPath));
		throw(exception);
	}

	((audWaveMetadataBase *)waveMetadataOut)->nameHash = audPlatformSpecific::FixEndian(ComputeNameHash(true));

	m_BuiltPreloopPadding = preloopPadding;
	m_BuiltSizeBytes = waveSampleDataOutLengthBytes;
	m_BuiltSampleRate = (int)(waveMetadata.sampleRate);
	m_NoOfBitsInLastPacket = noOfBitsInLastPacket;

	//Add any markers in the Wave as custom metadata.
	if(m_WaveLabelList->get_Count() > 0)
	{
		m_CustomMetadataSizeBytes = sizeof(audCustomMetadataHeader) + (m_WaveLabelList->get_Count() * sizeof(audWaveMetadataMarker));

		audCustomMetadataHeader header;
		header.typeHash = audPlatformSpecific::FixEndian(HashString(WaveMetadataMarkerType));
		header.size = audPlatformSpecific::FixEndian((unsigned int)(m_WaveLabelList->get_Count() * sizeof(audWaveMetadataMarker)));

		//Convert custom metadata header to a managed array and add to list.
		Byte managedCustomMetadataHeader __gc[] = new Byte[sizeof(audCustomMetadataHeader)];
		Marshal::Copy(&header, managedCustomMetadataHeader, 0, sizeof(audCustomMetadataHeader));
		customMetadataList->AddRange(static_cast<Array *>(managedCustomMetadataHeader));

		Byte managedCustomMetadataMarker __gc[] = new Byte[sizeof(audWaveMetadataMarker)];
		System::Collections::IEnumerator* labelEnum = m_WaveLabelList->GetEnumerator();
		while(labelEnum->MoveNext())
		{
			audLabelWrapper *labelWrapper = __try_cast<audLabelWrapper *>(labelEnum->Current);
			audLabel *label = labelWrapper->GetLabel();
			audCuePoint *cuePoint = cueChunk->GetCuePoint(label->cuePointId);
			String *categoryString;
			String *nameString;

			audWaveMetadataMarker marker = {0};
			if(label)
			{
				String *labelString = new String(label->text);
				//A ':' is used to separate the category and the marker name.
				String *delimiters = S":";
				String *splitStrings[] = labelString->Split(delimiters->ToCharArray());

				if(splitStrings->get_Count() >= 1)
				{
					categoryString = __try_cast<String *>(splitStrings->get_Item(0));
					if(categoryString)
					{
						IntPtr categoryPtr = Marshal::StringToHGlobalAnsi(categoryString);
						if(categoryPtr != 0)
						{
							marker.categoryHash = audPlatformSpecific::FixEndian(HashString((char *)categoryPtr.ToPointer()));
						}
					}
				}
				if(splitStrings->get_Count() >= 2)
				{
					nameString = __try_cast<String *>(splitStrings->get_Item(1));
					if(nameString)
					{
						try
						{
							Single nameValue = Single::Parse(nameString);
							//The name is actually a number, so store it as such.
							marker.value = (float)audPlatformSpecific::FixEndian(nameValue);
						}
						catch(FormatException *e)
						{
							//I'm a name, not a number... :0)
							IntPtr namePtr = Marshal::StringToHGlobalAnsi(nameString);
							if(namePtr != 0)
							{
								marker.nameHash = audPlatformSpecific::FixEndian(HashString((char *)namePtr.ToPointer()));
							}

							e = NULL;
						}
					}
				}
				if(splitStrings->get_Count() >= 3)
				{
					String *dataString = __try_cast<String *>(splitStrings->get_Item(2));
					if(dataString)
					{
						try
						{
							UInt32 dataValue = UInt32::Parse(dataString);
							marker.data = audPlatformSpecific::FixEndian((unsigned int)dataValue);
						}
						catch(FormatException *e)
						{
							//This really should be an unsigned int, so don't store anything in the data field.
							e = NULL;
						}
					}
				}
			}

			if(cuePoint)
			{
				unsigned int timeOffsetMs = (unsigned int)Math::Floor(((double)(cuePoint->sampleOffset) * 1000.0) /	(double)(m_RawSampleRate));
				marker.timeOffset = audPlatformSpecific::FixEndian(timeOffsetMs);
			}

			//Convert custom metadata marker to a managed array and add to list.
			Marshal::Copy(&marker, managedCustomMetadataMarker, 0, sizeof(audWaveMetadataMarker));
			customMetadataList->AddRange(static_cast<Array *>(managedCustomMetadataMarker));

			AddWaveMarkerToListIfUnique(new audWaveMarker(categoryString, nameString));
		}

		managedCustomMetadataHeader = NULL;
		managedCustomMetadataMarker = NULL;
	}

	//Add any arbitrary data as custom metadata for this platform.
	ProcessArbitraryData(&wavReader, customMetadataList);

	m_MetadataSizeBytes = waveMetadataOutLengthBytes;
	if(m_CustomMetadataSizeBytes > 0) //Let's not subtract 1 for fun...
	{
		m_MetadataSizeBytes += m_CustomMetadataSizeBytes; //Total metadata.
	}

	//Convert Wave metadata to a managed array and add to list.
	Byte managedWaveMetadata __gc[] = new Byte[waveMetadataOutLengthBytes];
	Marshal::Copy(waveMetadataOut, managedWaveMetadata, 0, waveMetadataOutLengthBytes);
	delete[] waveMetadataOut;
	waveMetadataList->AddRange(static_cast<Array *>(managedWaveMetadata));
	managedWaveMetadata = NULL;

	//Convert Wave sample data to a managed array and add to list.
	Byte managedWaveSampleData __gc[] = new Byte[waveSampleDataOutLengthBytes];
	Marshal::Copy(waveSampleDataOut, managedWaveSampleData, 0, waveSampleDataOutLengthBytes);
	delete[] waveSampleDataOut;
	waveSampleDataList->AddRange(static_cast<Array *>(managedWaveSampleData));
	managedWaveSampleData = NULL;

	//Pad Wave sample data to force byte-alignment to packet size.
	unsigned int paddingBytes = g_StreamingPacketBytes - (waveSampleDataOutLengthBytes % g_StreamingPacketBytes);
	if((paddingBytes > 0) && (paddingBytes < g_StreamingPacketBytes))
	{
		Byte zeros __gc[] = new Byte[paddingBytes];
		Array::Clear(zeros, 0, paddingBytes);
		waveSampleDataList->AddRange(static_cast<Array *>(zeros));
		zeros = NULL;

		m_BuiltSizeBytes += paddingBytes;
	}

	delete[] waveMetadata.waveData;
}

void audWave::ProcessArbitraryData(audWavReader *wavReader, audByteArrayList *customMetadataList)
{
	u32 chunkId;
	switch(audPlatformSpecific::GetBuildPlatform())
	{
		case audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_XENON:
			chunkId = ARB_DATA_XENON_CHUNK_ID;
			break;

		case audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PC:
			chunkId = ARB_DATA_PC_CHUNK_ID;
			break;

		case audPlatformSpecific::audBuildPlatforms::BUILD_PLATFORM_PS3:
			chunkId = ARB_DATA_PS3_CHUNK_ID;
			break;

		default:
			chunkId = 0;
			break;
	}

	audRiffChunk *dataChunk = NULL;
	do
	{
		dataChunk = (audRiffChunk *)wavReader->GetNextChunk(chunkId, dataChunk);
		if(dataChunk)
		{
			unsigned int dataSize = dataChunk->GetChunkSize();

			//Convert custom metadata to a managed array.
			Byte managedCustomMetadata __gc[] = new Byte[dataSize];
			Marshal::Copy(dataChunk->GetData(), managedCustomMetadata, 0, dataSize);

			//This is now performed in Rave
			//Trim all the 'cd's of padding from the end of the animation data.
			/*int i = dataSize;
			while(Convert::ToInt32(managedCustomMetadata->GetValue(i - 1)) == 0xcd)
			{
				i--;
			}
			Array::Resize(&managedCustomMetadata, i);
			dataSize = i;*/

			m_CustomMetadataSizeBytes += sizeof(audCustomMetadataHeader) + dataSize;

			audCustomMetadataHeader header;
			header.typeHash = audPlatformSpecific::FixEndian(g_WaveMetadataArbitraryDataTypeHash);
			header.size = audPlatformSpecific::FixEndian(dataSize);

			//Convert custom metadata header to a managed array and add to list.
			Byte managedCustomMetadataHeader __gc[] = new Byte[sizeof(audCustomMetadataHeader)];
			Marshal::Copy(&header, managedCustomMetadataHeader, 0, sizeof(audCustomMetadataHeader));
			customMetadataList->AddRange(static_cast<Array *>(managedCustomMetadataHeader));
			managedCustomMetadataHeader = NULL;

			//Add anim data to list.
			customMetadataList->AddRange(static_cast<Array *>(managedCustomMetadata));
			managedCustomMetadata = NULL;
		}

	} while(dataChunk);
}

unsigned int audWave::ComputeNameHash(bool useAlias)
{
	char __pin *nameStr;

	//alias used to add name to lookups
		if(useAlias){
			//Convert .Net Wave name to a system string and then to a Rage hash code.
			char utf8 __gc[] = Text::Encoding::UTF8->GetBytes(m_Alias);
			nameStr = &utf8[0];
		}
		else
		{
			char utf8 __gc[] = Text::Encoding::UTF8->GetBytes(m_Name);
			nameStr = &utf8[0];
		}

		return HashString(nameStr);
}


//Stolen from Rage::atStringHash constructor.
unsigned int audWave::HashString(const char *string)
{
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)

	bool quotes = (*string == '\"');
	unsigned int key = 0;

	if (quotes) 
		string++;

	while(*string && (!quotes || *string != '\"'))
	{
		char character = *string++;
		if (character >= 'A' && character <= 'Z')
		{
			character += 'a'-'A';
		}
		else if (character == '\\')
		{
			character = '/';
		}

		key += character;
		key += (key << 10);
		key ^= (key >> 6);
	}

	key += (key << 3);
	key ^= (key >> 11);
	key += (key << 15);

	return key;
}

double audWave::PeakNormalise(short *waveSamples, int numSamples, double desiredPeakLevel)
{
	short maxSampleLevel=0, maxNormalisedLevel;
	int i;
	double scalingFactor;

	//Scan wave data for peak (absolute) sample level.
	for(i=0; i<numSamples; i++)
	{
		if(waveSamples[i] == -32768) //Ensure that we don't attempt an invalid Abs.
			waveSamples[i] = -32767;

		int absValue = Math::Abs(waveSamples[i]);
		if(absValue > maxSampleLevel)
			maxSampleLevel = (short)absValue;
	}

	//Calculate scaling factor.
	maxNormalisedLevel = (short)Math::Floor((double)0x7FFF * Math::Pow(10.0, desiredPeakLevel / 20.0));
	scalingFactor = (double)maxNormalisedLevel / (double)maxSampleLevel;

	//Scale audio data.
	for(i=0; i<numSamples; i++)
		waveSamples[i] = (short)Math::Floor((double)waveSamples[i] * scalingFactor);

	//Convert scaling factor to dB.
	return (Math::Log10(scalingFactor) * 20.0);
}

void audWave::AddWaveMarkerToListIfUnique(audWaveMarker *marker)
{
	//Check if this marker is already in our list.
	System::Collections::IEnumerator *markerEnum = m_WaveMarkerList->GetEnumerator();
	while(markerEnum->MoveNext())
	{
		audWaveMarker *existingMarker = __try_cast<audWaveMarker *>(markerEnum->Current);
		if(existingMarker && existingMarker->Equals(marker))
		{
			//We already have this marker.
			return;
		}
	}

	m_WaveMarkerList->Add(marker);
}

void audWave::SerializeWaveMarkerList(XmlNode *parentNode)
{
	System::Collections::IEnumerator *markerEnum = m_WaveMarkerList->GetEnumerator();
	while(markerEnum->MoveNext())
	{
		audWaveMarker *marker = __try_cast<audWaveMarker *>(markerEnum->Current);
		if(marker)
		{
			marker->Serialize(parentNode);
		}
	}
}
