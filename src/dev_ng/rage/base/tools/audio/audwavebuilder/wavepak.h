//
// tools/audwavebuilder/wavepak.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef WAVE_PAK_H
#define WAVE_PAK_H

#include "wavebank.h"

using namespace audBuildCommon;
using namespace System;

//
// PURPOSE
//  Represents a physical Wave Pak resource throughout the build process. Cascades requests to the constituent
//	Wave Banks to add themselves and their constituent Waves to this Wave Pak folder.
// SEE ALSO
//  audWaveBank
//
public __gc class audWavePak : public Object
{
public:
	//
	// PURPOSE
	//  Initializes member variables.
	// PARAMS
	//  name			- The formalized name of the physical Wave Pak folder on the local filesystem.
	//  path			- The full path of the physical Wave Bank folder on the local filesystem.
	//	buildCommand	- Defines the command that applies to this Wave Pak resource for this build (add, modify or
	//						remove.)
	//
	audWavePak(String *name, String *path, audWave::audBuildCommands buildCommand);
	//
	// PURPOSE
	//  Opens the physical Wave Pak folder and cascades requests to the constituent Wave Banks to build themselves
	//	and their constituent Waves to it.
	// PARAMS
	//	outputPath				- The path of the Wave Pak folder to be generated.
	//	buildClient				- An encapsulation of the connection to the build client.
	//	totalWaves				- The total number of waves to be built, for use in progress reporting.
	//	wavesBuilt				- The number of the waves currently built, for use in progress reporting.
	//								(Also a return parameter).
	//  waveBuildStartDateTime	- The date and time at which the wave build was started, for use in progress
	//								reporting.
	// SEE ALSO
	//  audWaveBank
	//
	void Build(String *outputPath, audBuildClient *buildClient, int totalWaves, int &wavesBuilt,
		DateTime waveBuildStartDateTime,bool isDeferredBuild);
	//
	// PURPOSE
	//  An accessor function for the compression member variable. Ensures that the compression setting is never overridden
	//	once set, as this function is guaranteed to be called from the bottom-upwards in the wave resource hierachy - and
	//	children always override their parents with this parameter.
	// PARAMS
	//	compression	- A Wave compression level to be set (and later propagated to the constituent Wave Banks when built.)
	//
	void SetCompression(int compression)
	{
		if(m_Compression == -1)
		{
			m_Compression = compression;
		}
	}
	//
	// PURPOSE
	//  An accessor function for the sample rate member variable. Ensures that the sample rate setting is never overridden
	//	once set, as this function is guaranteed to be called from the bottom-upwards in the wave resource hierachy - and
	//	children always override their parents with this parameter.
	// PARAMS
	//	sampleRate	- A Wave sample rate to be set (and later propagated to the constituent Wave Banks when built.)
	//
	void SetSampleRate(int sampleRate)
	{
		if(m_SampleRate == -1)
		{
			m_SampleRate = sampleRate;
		}
	}
	//
	// PURPOSE
	//  An accessor function for the interleave bytes member variable. Ensures that this setting is never overridden
	//	once set, as this function is guaranteed to be called from the bottom-upwards in the wave resource hierachy - and
	//	children always override their parents with this parameter.
	// PARAMS
	//	streamingBlockBytes	- The block size to be used when interleaving Waves within this Pak, or 0 if the Waves are to
	//							be concatenated (propagated to the constituent Wave Banks when built.)
	//
	void SetStreamingBlockBytes(unsigned int streamingBlockBytes)
	{
		if(m_StreamingBlockBytes == 0)
		{
			m_StreamingBlockBytes = streamingBlockBytes;
		}
	}

	String *m_Name, *m_FullPath;
	ArrayList *m_BankList;
	audWave::audBuildCommands m_BuildCommand;

private:
	int m_Compression;
	int m_SampleRate;
	unsigned int m_StreamingBlockBytes;
	bool m_Build;
};

#endif // WAVE_PAK_H
