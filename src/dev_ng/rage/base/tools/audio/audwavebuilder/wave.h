//
// tools/audwavebuilder/wave.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef WAVE_H
#define WAVE_H

using namespace audBuildCommon;
using namespace System;
using namespace System::Collections;
using namespace System::IO;
using namespace System::Xml;
using namespace rage;

#include "chunkdefs.h"
#include "memory.h"

class audWavReader;

//
// PURPOSE
//  A managed wrapper for a label chunk within the wave file.
//
public __gc class audLabelWrapper
{
public:
	audLabelWrapper(audLabel *label)
	{
		m_Label = new audLabel;
		memcpy(m_Label, label, sizeof(audLabel));
		m_Label->text = (char *)&(label->text);
	}
	~audLabelWrapper()
	{
		delete m_Label;
	}
	audLabel *GetLabel(void)
	{
		return m_Label;
	}
private:
	audLabel *m_Label;
};

//
// PURPOSE
//  A simple class for serializing a wave marker to XML.
//
public __gc class audWaveMarker
{
public:
	audWaveMarker(String *category, String *name) : m_Category(category), m_Name(name)
	{
		if(!m_Category)
		{
			m_Category = S"None";
		}
		if(!m_Name)
		{
			m_Name = S"None";
		}
	}
	void Serialize(XmlNode *parentNode)
	{
		XmlNode *markerNode = parentNode->OwnerDocument->CreateElement(S"Marker");
		if(!String::IsNullOrEmpty(m_Category))
		{
			XmlAttribute *categoryAttribute = parentNode->OwnerDocument->CreateAttribute(S"category");
			categoryAttribute->set_Value(m_Category);
			markerNode->Attributes->Append(categoryAttribute);
		}
		if(!String::IsNullOrEmpty(m_Name))
		{
			XmlAttribute *nameAttribute = parentNode->OwnerDocument->CreateAttribute(S"name");
			nameAttribute->set_Value(m_Name);
			markerNode->Attributes->Append(nameAttribute);
		}

		parentNode->AppendChild(markerNode);
	}
	bool Equals(String *category, String *name)
	{
		return (m_Category->Equals(category) && m_Name->Equals(name));
	}
	bool Equals(audWaveMarker *marker)
	{
		return (marker ? Equals(marker->m_Category, marker->m_Name) : false);
	}
private:
	void AppendStringNode(XmlNode *parentNode, String *nodeName, String *nodeValue)
	{
		XmlNode *node = parentNode->OwnerDocument->CreateElement(nodeName);
		node->InnerText = nodeValue;
		parentNode->AppendChild(node);
	}

	String *m_Category;
	String *m_Name;
};

//
// PURPOSE
//  Represents a physical Wave resource throughout the build process. Adds itself to the specified Wave Bank file,
//	performing any necessary audio encoding.
// SEE ALSO
//  audWaveBank
//
public __gc class audWave : public Object
{
public:
	//Define the wave build commands.
	__value enum audBuildCommands
	{
		UNASSIGNED,
		ADD,
		MODIFY,
		REMOVE,
		NUM_COMMANDS
	};
	//
	// PURPOSE
	//  Initializes member variables.
	// PARAMS
	//  name			- The formalized name of the physical Wave on the local filesystem.
	//  assetPath		- The full path of the physical Wave in the asset management project.
	//  localPath		- The full path of the physical Wave on the local filesystem.
	//	buildCommand	- Defines the command that applies to this Wave resource for this build, e.g. add,
	//						modify or remove.
	//
	audWave(String *name, String *alias, String *assetPath, String *localPath, audBuildCommands buildCommand);
	//
	// PURPOSE
	//  Performs any necessary audio encoding (including fixing endianness) for the target platform and adds the resulting
	//	Wave metadata and encoded sample data to the specified lists.
	// PARAMS
	//	waveMetadataList	- A list of concatenated Wave metadata to be added to.
	//	customMetadataList	- A list of concatenated custom Wave metadata to be added to.
	//	waveSampleDataList	- A list of concatenated Wave sample data to be added to.
	//	compression			- The compression setting to be applied.
	//	sampleRate			- The sample rate setting to be applied.
	// SEE ALSO
	//	audWavReader
	//
	void Build(audByteArrayList *waveMetadataList, audByteArrayList *customMetadataList,
		audByteArrayList *waveSampleDataList, int compression, int sampleRate);
	//
	// PURPOSE
	//  Calculates the hash of this Wave's name, using the same algorithm as Rage.
	// RETURNS
	//	The hash of this Wave's name.
	//
	unsigned int ComputeNameHash(bool useAlias);
	//
	// PURPOSE
	//  An accessor function for the compression member variable. Ensures that the compression setting is never overridden
	//	once set, as this function is guaranteed to be called from the bottom-upwards in the wave resource hierachy - and
	//	children always override their parents with this parameter.
	// PARAMS
	//	compression	- A Wave compression level to be set (and later used when built.)
	//
	void SetCompression(int compression)
	{
		if(m_Compression == -1)
		{
			m_Compression = compression;
		}
	}
	//
	// PURPOSE
	//  An accessor function for the sample rate member variable. Ensures that the sample rate setting is never overridden
	//	once set, as this function is guaranteed to be called from the bottom-upwards in the wave resource hierachy - and
	//	children always override their parents with this parameter.
	// PARAMS
	//	sampleRate	- A Wave sample rate to be set (and later used when built.)
	//
	void SetSampleRate(int sampleRate)
	{
		if(m_SampleRate == -1)
		{
			m_SampleRate = sampleRate;
		}
	}
	//
	// PURPOSE
	//  Serializes the wave marker list to the specified XML node.
	// PARAMS
	//	parentNode	- The XML node under which the wave marker XML will be serialized.
	//
	void SerializeWaveMarkerList(XmlNode *parentNode);

	int m_NoOfBitsInLastPacket;
	int m_RawSampleRate, m_BuiltSampleRate, m_BuiltPreloopPadding, m_RawSizeBytes, m_BuiltSizeBytes, m_MetadataSizeBytes, m_CustomMetadataSizeBytes;
	String *m_Name, *m_Alias, *m_FullAssetPath, *m_FullLocalPath;
	audBuildCommands m_BuildCommand;
	bool m_bSkip;

private:
	//
	// PURPOSE
	//  Hashes a specified string using the same algorithm as Rage.
	// PARAMS
	//  string	- The string to be hashed.
	// RETURNS
	//	The hash of the specified string.
	//
	unsigned int HashString(const char *string);
	//
	// PURPOSE
	//  Peak normalises the specified wave data to desired level.
	// PARAMS
	//  waveSamples			- The wave data to be normalised.
	//	numSamples			- The number of samples of wave data.
	//	desiredPeakLevel	- The desired peak level after normalisation, in dB.
	// RETURNS
	//	The scaling factor applied within the normalisation process, in dB - the Wave's headroom.
	//
	double PeakNormalise(short *waveSamples, int numSamples, double desiredPeakLevel);
	//
	// PURPOSE
	//  Adds the specified marker to the marker list, but only if it is unique, as we do not want duplicate entries in XML.
	// PARAMS
	//  marker 	- The wave marker to be added to the list.
	//
	void AddWaveMarkerToListIfUnique(audWaveMarker *marker);
	//
	// PURPOSE
	//  Adds any arbitrary data for this platform to the wave bank as custom metadata.
	// PARAMS
	//  wavReader	 		- The wave reader to be used to extract the arbitrary data chunks.
	//	customMetadataList	- The list of custom metadata that the converted anim data is to be added to.
	//
	void ProcessArbitraryData(audWavReader *wavReader, audByteArrayList *customMetadataList);
	int m_Compression;
	int m_SampleRate;
	ArrayList *m_WaveLabelList;
	ArrayList *m_WaveMarkerList;
	static const char* WaveMetadataMarkerType = "WAVE_METADATA_MARKER";
};

#endif // WAVE_H
