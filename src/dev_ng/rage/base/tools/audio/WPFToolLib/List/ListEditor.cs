using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace WPFToolLib.List
{
    public class ListEditor<T> : IListEditor<T> where T : class
    {
        private readonly BindingList<T> m_items = new BindingList<T>();
        private readonly HashSet<T> m_itemsSet = new HashSet<T>();

        #region IListEditor<T> Members

        public BindingList<T> Items
        {
            get { return m_items; }
        }

        public void Add(T item)
        {
            if (!m_itemsSet.Contains(item))
            {
                m_items.Add(item);
                m_itemsSet.Add(item);
            }
        }

        public int GetIndex(T item)
        {
            return m_items.IndexOf(item);
        }

        public void InsertAt(int index, T item)
        {
            if (!m_itemsSet.Contains(item))
            {
                if (index >= 0 &&
                    index < m_items.Count)
                {
                    m_items.Insert(index, item);
                    m_itemsSet.Add(item);
                }
                else
                {
                    Add(item);
                }
            }
        }

        public void Move(int srcIndex, int destIndex)
        {
            if ((srcIndex >= 0 && srcIndex < m_items.Count) &&
                srcIndex != destIndex)
            {
                var srcItem = m_items[srcIndex];
                Remove(srcItem);
                if (destIndex > srcIndex)
                {
                    destIndex--;
                }
                InsertAt(destIndex, srcItem);
            }
        }

        public void Remove(T item)
        {
            m_itemsSet.Remove(item);
            m_items.Remove(item);
        }

        #endregion
    }
}