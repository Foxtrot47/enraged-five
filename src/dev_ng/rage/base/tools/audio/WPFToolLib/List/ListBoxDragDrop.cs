using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using WPFToolLib.Extensions;

namespace WPFToolLib.List
{
    public class ListBoxDragDrop<T> : IDisposable where T : class
    {
        private readonly IListEditor<T> m_editor;
        private readonly ListBox m_listBox;
        private ListBoxDragDropInsertionAdorner m_insertionAdorner;
        private Point m_startPoint;

        public ListBoxDragDrop(IListEditor<T> editor, ListBox listBox)
        {
            if (editor == null)
            {
                throw new ArgumentNullException("editor");
            }
            if (listBox == null)
            {
                throw new ArgumentNullException("listBox");
            }
            m_editor = editor;
            m_listBox = listBox;

            m_listBox.PreviewMouseLeftButtonDown += OnPreviewMouseLeftButtonDown;
            m_listBox.PreviewMouseMove += OnPreviewMouseMove;
            m_listBox.DragEnter += OnDragEnter;
            m_listBox.DragOver += OnDragOver;
            m_listBox.DragLeave += OnDragLeave;
            m_listBox.Drop += OnDrop;
            m_listBox.AllowDrop = true;
        }

        #region IDisposable Members

        public void Dispose()
        {
            m_listBox.PreviewMouseLeftButtonDown -= OnPreviewMouseLeftButtonDown;
            m_listBox.PreviewMouseMove -= OnPreviewMouseMove;
            m_listBox.DragEnter -= OnDragEnter;
            m_listBox.DragOver -= OnDragOver;
            m_listBox.DragLeave -= OnDragLeave;
            m_listBox.Drop -= OnDrop;
        }

        #endregion

        private void OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender == m_listBox)
            {
                m_startPoint = m_listBox.GetMousePosition();
            }
        }

        private void OnPreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (sender == m_listBox &&
                e.LeftButton == MouseButtonState.Pressed)
            {
                var currentPoint = m_listBox.GetMousePosition();
                var diff = m_startPoint - currentPoint;
                if (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                    Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance)
                {
                    var originalSource = e.OriginalSource as DependencyObject;
                    var listBoxItem = originalSource.FindAncestor<ListBoxItem>();
                    if (listBoxItem != null)
                    {
                        var data = m_listBox.ItemContainerGenerator.ItemFromContainer(listBoxItem) as T;
                        if (data != null)
                        {
                            var dragData = new DataObject(typeof (T), data);
                            DragDrop.DoDragDrop(m_listBox, dragData, DragDropEffects.Move);
                        }
                    }
                }
            }
        }

        private void OnDragEnter(object sender, DragEventArgs e)
        {
            if (sender == m_listBox &&
                e.Data.GetDataPresent(typeof (T)))
            {
                CreateInsertionAdorner(e);
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
            e.Handled = true;
        }

        private void OnDragOver(object sender, DragEventArgs e)
        {
            if (sender == m_listBox &&
                e.Data.GetDataPresent(typeof (T)))
            {
                UpdateInsertionAdornerPosition(e);
                e.Handled = true;
            }
        }

        private void OnDragLeave(object sender, DragEventArgs e)
        {
            if (sender == m_listBox &&
                e.Data.GetDataPresent(typeof (T)))
            {
                RemoveInsertionAdorner();
                e.Handled = true;
            }
        }

        private void OnDrop(object sender, DragEventArgs e)
        {
            if (sender == m_listBox &&
                e.Data.GetDataPresent(typeof (T)))
            {
                ListBoxItem item;
                int srcIndex, destIndex, dragDirection;
                if (GetDragDropInfo(e, out item, out srcIndex, out destIndex, out dragDirection))
                {
                    m_editor.Move(srcIndex, destIndex);
                }
                RemoveInsertionAdorner();
                e.Handled = true;
            }
        }

        private bool GetDragDropInfo(DragEventArgs e,
                                     out ListBoxItem item,
                                     out int srcIndex,
                                     out int destIndex,
                                     out int dragDirection)
        {
            var originalSource = e.OriginalSource as DependencyObject;
            var srcData = e.Data.GetData(typeof (T)) as T;
            srcIndex = m_editor.GetIndex(srcData);
            destIndex = -1;
            dragDirection = 0;
            item = originalSource.FindAncestor<ListBoxItem>();

            var count = m_editor.Items.Count;
            if (count > 0)
            {
                if (item != null)
                {
                    var data = m_listBox.ItemContainerGenerator.ItemFromContainer(item) as T;
                    if (data != null)
                    {
                        dragDirection = GetDragDirection(item);
                        destIndex = dragDirection + m_editor.GetIndex(data);
                        return true;
                    }
                }
                else
                {
                    item = m_listBox.ItemContainerGenerator.ContainerFromIndex(count - 1) as ListBoxItem;

                    if (null == item)
                    {
                        return false;
                    }

                    dragDirection = GetDragDirection(item);
                    destIndex = -1;
                    return true;
                }
            }
            return false;
        }

        private static int GetDragDirection(FrameworkElement item)
        {
            if (null == item)
            {
                return 0;
            }

            var currentPoint = item.GetMousePosition();
            return currentPoint.Y < item.ActualHeight / 2 ? 0 : 1;
        }

        private void CreateInsertionAdorner(DragEventArgs e)
        {
            ListBoxItem item;
            int srcIndex, destIndex, dragDirection;
            if (GetDragDropInfo(e, out item, out srcIndex, out destIndex, out dragDirection))
            {
                var adornerLayer = AdornerLayer.GetAdornerLayer(item);
                m_insertionAdorner = new ListBoxDragDropInsertionAdorner(item, adornerLayer)
                                         {DragDirection = dragDirection};
            }
        }

        private void UpdateInsertionAdornerPosition(DragEventArgs e)
        {
            if (m_insertionAdorner != null)
            {
                ListBoxItem item;
                int srcIndex, destIndex, dragDirection;
                if (GetDragDropInfo(e, out item, out srcIndex, out destIndex, out dragDirection))
                {
                    m_insertionAdorner.DragDirection = dragDirection;
                }
                m_insertionAdorner.InvalidateVisual();
            }
        }

        private void RemoveInsertionAdorner()
        {
            if (m_insertionAdorner != null)
            {
                m_insertionAdorner.Detach();
                m_insertionAdorner = null;
            }
        }
    }
}