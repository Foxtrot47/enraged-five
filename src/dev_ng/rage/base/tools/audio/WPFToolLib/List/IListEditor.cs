using System.ComponentModel;

namespace WPFToolLib.List
{
    public interface IListEditor<T> where T : class
    {
        BindingList<T> Items { get; }

        void Add(T item);

        int GetIndex(T item);

        void InsertAt(int index, T item);

        void Move(int srcIndex, int destIndex);

        void Remove(T item);
    }
}