using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace WPFToolLib.List
{
    public class ListBoxDragDropInsertionAdorner : Adorner
    {
        private static readonly Pen ms_pen;
        private static readonly PathGeometry ms_triangle;
        private readonly AdornerLayer m_adornerLayer;

        static ListBoxDragDropInsertionAdorner()
        {
            ms_pen = new Pen {Brush = Brushes.OrangeRed, Thickness = 3};
            ms_pen.Freeze();

            var firstLine = new LineSegment(new Point(0, -6), false);
            firstLine.Freeze();
            var secondLine = new LineSegment(new Point(0, 6), false);
            secondLine.Freeze();

            var figure = new PathFigure {StartPoint = new Point(6, 0)};
            figure.Segments.Add(firstLine);
            figure.Segments.Add(secondLine);
            figure.Freeze();

            ms_triangle = new PathGeometry();
            ms_triangle.Figures.Add(figure);
            ms_triangle.Freeze();
        }

        public ListBoxDragDropInsertionAdorner(UIElement adornedElement, AdornerLayer adornerLayer)
            : base(adornedElement)
        {
            IsHitTestVisible = false;
            m_adornerLayer = adornerLayer;

            m_adornerLayer.Add(this);
        }

        public int DragDirection { get; set; }

        protected override void OnRender(DrawingContext drawingContext)
        {
            Point startPoint;
            Point endPoint;

            CalculateStartAndEndPoint(out startPoint, out endPoint);
            drawingContext.DrawLine(ms_pen, startPoint, endPoint);

            DrawTriangle(drawingContext, startPoint, 0);
            DrawTriangle(drawingContext, endPoint, 180);
        }

        private static void DrawTriangle(DrawingContext drawingContext, Point origin, double angle)
        {
            drawingContext.PushTransform(new TranslateTransform(origin.X, origin.Y));
            drawingContext.PushTransform(new RotateTransform(angle));

            drawingContext.DrawGeometry(ms_pen.Brush, ms_pen, ms_triangle);

            drawingContext.Pop();
            drawingContext.Pop();
        }

        private void CalculateStartAndEndPoint(out Point startPoint, out Point endPoint)
        {
            startPoint = new Point();
            endPoint = new Point {X = AdornedElement.RenderSize.Width};
            if (DragDirection > 0)
            {
                var height = AdornedElement.RenderSize.Height;
                startPoint.Y = height;
                endPoint.Y = height;
            }
        }

        public void Detach()
        {
            m_adornerLayer.Remove(this);
        }
    }
}