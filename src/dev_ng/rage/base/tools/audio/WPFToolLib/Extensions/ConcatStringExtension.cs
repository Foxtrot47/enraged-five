﻿// -----------------------------------------------------------------------
// <copyright file="ConcatStringExtension.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace WPFToolLib.Extensions
{
    using System;
    using System.Windows.Data;
    using System.Windows.Markup;

    /// <summary>
    /// The concat string extension.
    /// </summary>
    public class ConcatStringExtension : MarkupExtension
    {
        /// <summary>
        /// The concat string.
        /// </summary>
        public class ConcatString : IValueConverter
        {
            /// <summary>
            /// Gets or sets the initial string.
            /// </summary>
            public string InitString { get; set; }

            #region IValueConverter Members

            /// <summary>
            /// The convert.
            /// </summary>
            /// <param name="value">
            /// The value.
            /// </param>
            /// <param name="targetType">
            /// The target type.
            /// </param>
            /// <param name="parameter">
            /// The parameter.
            /// </param>
            /// <param name="culture">
            /// The culture.
            /// </param>
            /// <returns>
            /// The <see cref="object"/>.
            /// </returns>
            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return this.InitString + value;
            }

            /// <summary>
            /// The convert back.
            /// </summary>
            /// <param name="value">
            /// The value.
            /// </param>
            /// <param name="targetType">
            /// The target type.
            /// </param>
            /// <param name="parameter">
            /// The parameter.
            /// </param>
            /// <param name="culture">
            /// The culture.
            /// </param>
            /// <returns>
            /// Not implemented <see cref="object"/>.
            /// </returns>
            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                throw new NotImplementedException();
            }

            #endregion
        }

        /// <summary>
        /// Gets or sets the bind to.
        /// </summary>
        public Binding BindTo { get; set; }

        /// <summary>
        /// Gets or sets the string to attach in front of the value.
        /// </summary>
        public string AttachString { get; set; }

        /// <summary>
        /// The provide value.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider.
        /// </param>
        /// <returns>
        /// The value <see cref="object"/>.
        /// </returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            BindTo.Converter = new ConcatString { InitString = this.AttachString };
            return BindTo.ProvideValue(serviceProvider);
        }
    }
}
