using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media;

namespace WPFToolLib.Extensions
{
    public static class VisualExtensions
    {
        public static Point GetMousePosition(this Visual relativeTo)
        {
            var w32Mouse = new Win32Point();
            GetCursorPos(ref w32Mouse);

            if (null == relativeTo)
            {
                return new Point(w32Mouse.X, w32Mouse.Y);
            }

            return relativeTo.PointFromScreen(new Point(w32Mouse.X, w32Mouse.Y));
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(ref Win32Point pt);

        #region Nested type: Win32Point

        [StructLayout(LayoutKind.Sequential)]
        private struct Win32Point
        {
            public readonly Int32 X;
            public readonly Int32 Y;
        } ;

        #endregion
    }
}