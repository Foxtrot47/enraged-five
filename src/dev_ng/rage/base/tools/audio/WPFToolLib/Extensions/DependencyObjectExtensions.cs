using System.Windows;
using System.Windows.Media;

namespace WPFToolLib.Extensions
{
    public static class DependencyObjectExtensions
    {
        public static T FindAncestor<T>(this DependencyObject current) where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return current as T;
                }
                current = VisualTreeHelper.GetParent(current);
            }
            while (current != null);
            return null;
        }

        public static T FindLogicalAncestor<T>(this DependencyObject current) where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return current as T;
                }
                current = LogicalTreeHelper.GetParent(current);
            }
            while (current != null);
            return null;
        }
    }
}