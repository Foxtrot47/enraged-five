using System.Windows;

namespace WPFToolLib.Extensions
{
    public static class FrameworkElementExtensions
    {
        public static T FindDataContext<T>(this FrameworkElement element) where T : class
        {
            while (true)
            {
                if (element.DataContext != null &&
                    element.DataContext is T)
                {
                    return element.DataContext as T;
                }

                if (element.Parent != null &&
                    element.Parent is FrameworkElement)
                {
                    element = element.Parent as FrameworkElement;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}