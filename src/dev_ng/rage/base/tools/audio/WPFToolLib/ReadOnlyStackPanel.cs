﻿using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace WPFToolLib
{
    public class ReadOnlyStackPanel : StackPanel
    {
        public ReadOnlyStackPanel()
        {
            Loaded += PanelExtension_Loaded;
        }

        public bool IsReadOnly
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }

        private static void OnIsReadOnlyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((ReadOnlyStackPanel)d).OnIsReadOnlyChanged(e);
        }

        protected virtual void OnIsReadOnlyChanged(DependencyPropertyChangedEventArgs e)
        {
            SetIsEnabledOfChildren();
        }

        private void PanelExtension_Loaded(object sender, RoutedEventArgs e)
        {
            SetIsEnabledOfChildren();
        }

        private void SetIsEnabledOfChildren()
        {
            foreach (UIElement child in Children)
            {
                PropertyInfo readOnlyProperty =
                    child.GetType().GetProperties().FirstOrDefault(prop => prop.Name.Equals("IsReadOnly"));
                if (readOnlyProperty != null)
                {
                    readOnlyProperty.SetValue(child, IsReadOnly);
                }
                else
                {
                    PropertyInfo isEnabledProperty =
                    child.GetType().GetProperties().FirstOrDefault(prop => prop.Name.Equals("IsEnabled"));

                    if (isEnabledProperty != null)
                    {
                        isEnabledProperty.SetValue(child, !IsReadOnly);
                    }
                }
            }
        }

        public static readonly DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(ReadOnlyStackPanel),
                new PropertyMetadata(OnIsReadOnlyChanged));
    }
}