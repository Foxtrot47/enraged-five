using System;
using System.Globalization;
using System.Windows.Data;

namespace WPFToolLib.Convertors
{
	public class NumberToIncrementConvertor : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value.GetType() != typeof(float))
			{
				return string.Empty;
			}
			return (FormatNumber((float)value));
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		static string FormatNumber(float num)
		{
			if (float.IsInfinity(num) || float.IsNaN(num))
			{
				return "";
			}
			if (Math.Abs(num) < 1)
			{
				return ("0.01");
			}
			else if (Math.Abs(num) < 10)
			{
				return ("0.1");
			}
			else if (Math.Abs(num) > 10 && Math.Abs(num) < 100)
			{
				return ("0.5");
			}
			else if (Math.Abs(num) >= 1000)
			{
				return 100.ToString();
			}

			return (num/100).ToString(CultureInfo.InvariantCulture);
		}
	}
}
