﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace WPFToolLib.Convertors
{
	public class NumberToFormatConvertor : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value.GetType() != typeof(float))
			{
				return string.Empty;
			}
			return (FormatNumber((float)value));
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		static string FormatNumber(float num)
		{
			if (float.IsInfinity(num) || float.IsNaN(num))
			{
				return "";
			}
			if (Math.Abs(num) < 1)
			{
				return ("0.##");
			}
			else if (Math.Abs(num) < 10)
			{
				return ("#0.#");
			}
			else if (Math.Abs(num) >= 1000)
			{
				return "###,.0k";
			}

			return "N1";
		}
	}
}
