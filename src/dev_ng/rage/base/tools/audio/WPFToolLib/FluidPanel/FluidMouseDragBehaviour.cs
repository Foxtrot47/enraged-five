﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace WPFToolLib.FluidPanel
{
    /// <summary>
    /// Defines the Drag Behavior in the FluidWrapPanel using the Mouse
    /// </summary>
    public class FluidMouseDragBehavior : Behavior<UIElement>
    {

        private FluidWrapPanel parentFWPanel = null;
        private ListBoxItem parentLBItem = null;

        private bool mouseDownFired = false;
        private int timeAtMouseDown = int.MaxValue;

        /// <summary>
        /// DragButton Dependency Property
        /// </summary>
        public static readonly DependencyProperty DragButtonProperty =
            DependencyProperty.Register("DragButton", typeof(MouseButton), typeof(FluidMouseDragBehavior),
                new FrameworkPropertyMetadata(MouseButton.Left));

        /// <summary>
        /// Gets or sets the DragButton property. This dependency property
        /// indicates which Mouse button should participate in the drag interaction.
        /// </summary>
        public MouseButton DragButton
        {
            get { return (MouseButton)GetValue(DragButtonProperty); }
            set { SetValue(DragButtonProperty, value); }
        }

        /// <summary>
        ///
        /// </summary>
        protected override void OnAttached()
        {
            // Subscribe to the Loaded event
            (this.AssociatedObject as FrameworkElement).Loaded += new RoutedEventHandler(OnAssociatedObjectLoaded);

        }


        private void OnAssociatedObjectLoaded(object sender, RoutedEventArgs e)
        {
            // Get the parent FluidWrapPanel and check if the AssociatedObject is
            // hosted inside a ListBoxItem (this scenario will occur if the FluidWrapPanel
            // is the ItemsPanel for a ListBox).
            GetParentPanel();

            // Subscribe to the Mouse down/move/up events
            if (parentLBItem != null)
            {
                parentLBItem.MouseDown += new MouseButtonEventHandler(OnMouseDown);
                parentLBItem.MouseMove += new MouseEventHandler(OnMouseMove);
                parentLBItem.MouseUp += new MouseButtonEventHandler(OnMouseUp);
            }
            else
            {
                this.AssociatedObject.MouseDown += new MouseButtonEventHandler(OnMouseDown);
                this.AssociatedObject.MouseMove += new MouseEventHandler(OnMouseMove);
                this.AssociatedObject.MouseUp += new MouseButtonEventHandler(OnMouseUp);
            }
        }

        /// <summary>
        /// Get the parent FluidWrapPanel and check if the AssociatedObject is
        /// hosted inside a ListBoxItem (this scenario will occur if the FluidWrapPanel
        /// is the ItemsPanel for a ListBox).
        /// </summary>
        private void GetParentPanel()
        {
            FrameworkElement ancestor = this.AssociatedObject as FrameworkElement;

            while (ancestor != null)
            {
                if (ancestor is ListBoxItem)
                {
                    parentLBItem = ancestor as ListBoxItem;
                }

                if (ancestor is FluidWrapPanel)
                {
                    parentFWPanel = ancestor as FluidWrapPanel;
                    // No need to go further up
                    return;
                }

                // Find the visual ancestor of the current item
                ancestor = VisualTreeHelper.GetParent(ancestor) as FrameworkElement;
            }
        }

        protected override void OnDetaching()
        {
            (this.AssociatedObject as FrameworkElement).Loaded -= OnAssociatedObjectLoaded;
            if (parentLBItem != null)
            {
                parentLBItem.MouseDown -= OnMouseDown;
                parentLBItem.MouseMove -= OnMouseMove;
                parentLBItem.MouseUp -= OnMouseUp;
            }
            else
            {
                this.AssociatedObject.MouseDown -= OnMouseDown;
                this.AssociatedObject.MouseMove -= OnMouseMove;
                this.AssociatedObject.MouseUp -= OnMouseUp;
            }
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ChangedButton == DragButton)
            {
                mouseDownFired = true;
                timeAtMouseDown = DateTime.Now.Millisecond;
            }
        }


        private void StartDrag(MouseEventArgs e)
        {
           Point initPosition = parentLBItem != null ? e.GetPosition(parentLBItem) : e.GetPosition(this.AssociatedObject);
            FrameworkElement fElem = this.AssociatedObject as FrameworkElement;
            if ((fElem != null) && (parentFWPanel != null))
            {
                if (parentLBItem != null)
                    parentFWPanel.BeginFluidDrag(parentLBItem, initPosition);
                else
                    parentFWPanel.BeginFluidDrag(this.AssociatedObject, initPosition);
            }
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDownFired && timeAtMouseDown < (DateTime.Now.Ticks - 100))
            {
                StartDrag(e);
                mouseDownFired = false;
            }
           
            bool isDragging = false;
         
            switch (DragButton)
            {
                case MouseButton.Left:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        isDragging = true;
                    }
                    break;

                case MouseButton.Middle:
                    if (e.MiddleButton == MouseButtonState.Pressed)
                    {
                        isDragging = true;
                    }
                    break;

                case MouseButton.Right:
                    if (e.RightButton == MouseButtonState.Pressed)
                    {
                        isDragging = true;
                    }
                    break;

                case MouseButton.XButton1:
                    if (e.XButton1 == MouseButtonState.Pressed)
                    {
                        isDragging = true;
                    }
                    break;

                case MouseButton.XButton2:
                    if (e.XButton2 == MouseButtonState.Pressed)
                    {
                        isDragging = true;
                    }
                    break;

                default:
                    break;
            }

            if (isDragging)
            {
                Point position = parentLBItem != null ? e.GetPosition(parentLBItem) : e.GetPosition(this.AssociatedObject);

                FrameworkElement fElem = this.AssociatedObject as FrameworkElement;
                if ((fElem != null) && (parentFWPanel != null))
                {
                    Point positionInParent = e.GetPosition(parentFWPanel);
                    if (parentLBItem != null)
                        parentFWPanel.FluidDrag(parentLBItem, position, positionInParent);
                    else
                        parentFWPanel.FluidDrag(this.AssociatedObject, position, positionInParent);
                }
            }
        }

        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            mouseDownFired = false;
            if (e.ChangedButton == DragButton)
            {
                Point position = parentLBItem != null ? e.GetPosition(parentLBItem) : e.GetPosition(this.AssociatedObject);

                FrameworkElement fElem = this.AssociatedObject as FrameworkElement;
                if ((fElem != null) && (parentFWPanel != null))
                {
                    Point positionInParent = e.GetPosition(parentFWPanel);
                    if (parentLBItem != null)
                        parentFWPanel.EndFluidDrag(parentLBItem, position, positionInParent);
                    else
                        parentFWPanel.EndFluidDrag(this.AssociatedObject, position, positionInParent);
                }
            }
        }

    }
}