﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace WPFToolLib.FluidPanel
{
    public class FluidBorder : Border
    {
        public FluidBorder()
            : base()
        {
            BehaviorCollection behaviors = Interaction.GetBehaviors(this);
            behaviors.Add(new FluidMouseDragBehavior { DragButton = MouseButton.Left });
        }

    }
}