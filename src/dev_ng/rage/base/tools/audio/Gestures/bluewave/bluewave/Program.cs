using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BlueWave
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            string[] args = Environment.GetCommandLineArgs();
            
            Application.Run(new frmMain(args));
        }
    }
}