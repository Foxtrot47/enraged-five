using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BlueWave
{
    public partial class frmSimpleInput : Form
    {
        private string m_Result;

        public static string GetInput(string helpText)
        {
            return GetInput(helpText, null);
        }
        public static string GetInput(string helpText, string initialValue)
        {
            frmSimpleInput form = new frmSimpleInput();
            form.m_HelpText.Text = helpText + ":";
            if (initialValue != null)
            {
                form.m_TextBox.Text = initialValue;
                form.m_TextBox.Select(initialValue.Length, 0);
            }
            form.ShowDialog();
            return form.m_Result;
        }

        public frmSimpleInput()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            m_Result = m_TextBox.Text;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            m_Result = null;
            Close();
        }
    }
}