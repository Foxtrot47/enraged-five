namespace BlueWave
{
    partial class ctrlMarkerTrack
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addMarkerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addregionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_ContextMenu
            // 
            this.m_ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addMarkerToolStripMenuItem,
            this.addregionToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.m_ContextMenu.Name = "m_ContextMenu";
            this.m_ContextMenu.Size = new System.Drawing.Size(153, 92);
            this.m_ContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextMenu_Opening);
            // 
            // addMarkerToolStripMenuItem
            // 
            this.addMarkerToolStripMenuItem.Name = "addMarkerToolStripMenuItem";
            this.addMarkerToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.addMarkerToolStripMenuItem.Text = "Add &marker";
            this.addMarkerToolStripMenuItem.Click += new System.EventHandler(this.addMarkerToolStripMenuItem_Click);
            // 
            // addregionToolStripMenuItem
            // 
            this.addregionToolStripMenuItem.Name = "addregionToolStripMenuItem";
            this.addregionToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.addregionToolStripMenuItem.Text = "Add &region";
            this.addregionToolStripMenuItem.Click += new System.EventHandler(this.addregionToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // ctrlMarkerTrack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ContextMenuStrip = this.m_ContextMenu;
            this.DoubleBuffered = true;
            this.Name = "ctrlMarkerTrack";
            this.Size = new System.Drawing.Size(614, 42);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ctrlMarkerTrack_Paint);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ctrlMarkerTrack_MouseClick);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ctrlMarkerTrack_MouseDoubleClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ctrlMarkerTrack_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ctrlMarkerTrack_MouseMove);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(ctrlMarkerTrack_KeyDown);
            this.Resize += new System.EventHandler(this.ctrlMarkerTrack_Resize);
            this.m_ContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip m_ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem addMarkerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addregionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
    }
}
