using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Microsoft.DirectX;
using Microsoft.DirectX.DirectSound;
using BlueWave.Script;
using Wavelib;
namespace BlueWave
{
    class WavePlayback
    {
        private bwWaveFile m_Wave;
        private static Device m_Device;
        private Microsoft.DirectX.DirectSound.Buffer m_Buffer;

        public static void Init(Control owner)
        {
            m_Device = new Device();
            m_Device.SetCooperativeLevel(owner, CooperativeLevel.Priority);
        }

        public WavePlayback(bwWaveFile wave)
        {
            m_Wave = wave;
            ConstructBuffer();
        }

        public void Play()
        {
            m_Buffer.Play(10, BufferPlayFlags.Default);
        }

        public void Stop()
        {
            m_Buffer.Stop();
        }

        public int PlayPosition
        {
            get
            {
                return m_Buffer.PlayPosition;
            }
            set
            {
                int position = value * m_Wave.Format.BlockAlign;
                position = Math.Min(m_Wave.Data.RawData.Length - 1, position);               
                m_Buffer.SetCurrentPosition(position);
            }
        }

        public bool IsPlaying
        {
            get
            {
                return m_Buffer.Status.Playing;
            }
        }

        private void ConstructBuffer()
        {
            BufferDescription desc = new BufferDescription();
            desc.CanGetCurrentPosition = true;
            desc.Control3D = false;
            desc.ControlEffects = false;
            desc.ControlFrequency = false;
            desc.ControlPan = false;
            desc.ControlPositionNotify = true;
            desc.ControlVolume = false;
            desc.DeferLocation = true;
            desc.GlobalFocus = true;
            WaveFormat wfx = new WaveFormat();
            wfx.AverageBytesPerSecond = (int)m_Wave.Format.AverageBytesPerSecond;
            wfx.BitsPerSample = (short)m_Wave.Format.SignificantBitsPerSample;
            wfx.BlockAlign = (short)m_Wave.Format.BlockAlign;
            wfx.Channels = (short)m_Wave.Format.NumChannels;
            wfx.FormatTag = WaveFormatTag.Pcm;
            wfx.SamplesPerSecond = (int)m_Wave.Format.SampleRate;
            desc.Format = wfx;
            desc.BufferBytes = m_Wave.Data.RawData.Length;

            m_Buffer = new Microsoft.DirectX.DirectSound.Buffer(desc, m_Device);
            m_Buffer.Write(0, m_Wave.Data.RawData, LockFlag.EntireBuffer);
        }
    }
}
