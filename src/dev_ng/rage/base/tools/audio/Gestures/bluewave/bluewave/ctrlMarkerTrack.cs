using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Text;
using System.Windows.Forms;

using BlueWave.Script;
using Wavelib;
using Gesturelib;
using System.Collections.Specialized;
using System.Collections.ObjectModel;

namespace BlueWave
{
    public partial class ctrlMarkerTrack : UserControl, IMarkerTrack
    {
        private frmWaveContainer m_Container;
        private string m_MarkerPrefix;
        private string m_Caption;
      
        private int m_LastMouseSample;
        private int m_LastMouseY;
        private int m_LastMarkerOffset;
        private bwGestureMarker m_LastMarker;
        private bwWaveFile m_LastMarkerWave;
        private bwGestureFile m_LastGestureFile;
        private bool m_IsMovingFrameEnd;
        private bool m_IsMovingFrameStart;
        private bool m_IsMovingLeftBlend;
        private bool m_IsMovingRightBlend;
        private bool m_IsMovingStartCrop;
        private bool m_IsMovingEndCrop;

        private bool m_MovingRight = false;
        private bool hasBeenRendered = false;

        public event OnContextMenuPopupDelegate OnContextMenuPopup;
        public event FindAnimationDefaultDurationDelegate OnAnimationDefaultDuration;

        const int tolerance = 500;

        public ctrlMarkerTrack(frmWaveContainer container, string markerPrefix)
        {
            m_Container = container;
            m_MarkerPrefix = markerPrefix;
            m_Caption = markerPrefix;
            m_LastMouseSample = 0;
            m_LastMarkerOffset = 0;
            m_LastMarker = null;
            m_LastMarkerWave = null;
            m_LastGestureFile = null;
            m_IsMovingFrameEnd = false;
            m_IsMovingFrameStart = false;
            InitializeComponent();
            this.KeyDown += new KeyEventHandler(ctrlMarkerTrack_KeyPress);
            if (m_Container != null)
            {
                this.m_Container.GetGestureFiles().CollectionChanged += this.OnGestureFilesChanged;
            }
        }

        void OnGestureFilesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var newItem in e.NewItems)
                {
                    bwGestureFile file = newItem as bwGestureFile;
                    if (file == null)
                    {
                        continue;
                    }

                    file.Markers.CollectionChanged += this.OnMarkersChanged;
                    this.OnMarkersChanged(file.Markers,
                        new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, file.Markers));
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var newItem in e.OldItems)
                {
                    bwGestureFile file = newItem as bwGestureFile;
                    if (file == null)
                    {
                        continue;
                    }

                    file.Markers.CollectionChanged -= this.OnMarkersChanged;
                    this.OnMarkersChanged(file.Markers,
                        new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, file.Markers));
                }
            }
        }
        
        void OnMarkersChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var markers = sender as ObservableCollection<bwGestureMarker>;
            if (markers == null || !hasBeenRendered)
            {
                return;
            }

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                int newItemCount = 0;
                foreach (var newItem in e.NewItems)
                {
                    bwGestureMarker marker = newItem as bwGestureMarker;
                    if (marker == null)
                    {
                        continue;
                    }

                    if (marker.Name.StartsWith(this.m_MarkerPrefix))
                    {
                        bwWaveFile wave = null;
                        bwGestureFile gesture;
                        int offset;
                        m_Container.ComputeWaveAndOffset(marker.Position, out wave, out gesture, out offset);
                        newItemCount++;
                        if (wave != null)
                        {
                            marker.Clip.SampleRate = (float)wave.Format.SampleRate;
                        }
                    }
                }

                if (newItemCount == 0)
                {
                    return;
                }

                int currentCount = 0;
                foreach (bwGestureMarker marker in markers)
                {
                    if (marker == null)
                    {
                        continue;
                    }

                    if (marker.Name.StartsWith(this.m_MarkerPrefix))
                    {
                        currentCount++;
                    }
                }

                int maxGestureCount = 0;
                int filesWithMaxCount = 0;
                int previousMaxCount = 0;
                ObservableCollection<bwGestureFile> gestureFiles = m_Container.GetGestureFiles();
                for (int i = 0; i < gestureFiles.Count; i++)
                {
                    bwGestureFile gestureFile = gestureFiles[i];
                    int gestureCount = 0;
                    foreach (bwGestureMarker marker in gestureFile.Markers)
                    {
                        if (marker.Name.StartsWith(this.m_MarkerPrefix))
                        {
                            gestureCount++;
                        }
                    }

                    maxGestureCount = Math.Max(gestureCount, maxGestureCount);
                    if (!object.ReferenceEquals(markers, gestureFile.Markers))
                    {
                        previousMaxCount = Math.Max(gestureCount, previousMaxCount);
                    }
                    else
                    {
                        previousMaxCount = Math.Max(gestureCount - newItemCount, previousMaxCount);
                    }
                }

                for (int i = 0; i < gestureFiles.Count; i++)
                {
                    bwGestureFile gestureFile = gestureFiles[i];
                    int gestureCount = 0;
                    foreach (bwGestureMarker marker in gestureFile.Markers)
                    {
                        if (marker.Name.StartsWith(this.m_MarkerPrefix))
                        {
                            gestureCount++;
                        }
                    }

                    if (gestureCount == maxGestureCount)
                    {
                        filesWithMaxCount++;
                    }
                }

                if (currentCount == maxGestureCount && filesWithMaxCount == 1)
                {
                    previousMaxCount = Math.Max(1, previousMaxCount);
                    int heightPerItem = (this.Height - ((previousMaxCount - 1) * 5)) / previousMaxCount;
                    this.Height = (currentCount * heightPerItem) + ((currentCount - 1) * 5);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                int oldItemCount = 0;
                foreach (var oldItem in e.OldItems)
                {
                    bwGestureMarker marker = oldItem as bwGestureMarker;
                    if (marker == null)
                    {
                        continue;
                    }

                    if (marker.Name.StartsWith(this.m_MarkerPrefix))
                    {
                        oldItemCount++;
                    }
                }

                if (oldItemCount == 0)
                {
                    return;
                }

                int currentCount = 0;
                foreach (bwGestureMarker marker in markers)
                {
                    if (marker == null)
                    {
                        continue;
                    }

                    if (marker.Name.StartsWith(this.m_MarkerPrefix))
                    {
                        currentCount++;
                    }
                }

                int maxGestureCount = 0;
                int filesWithMaxCount = 0;
                ObservableCollection<bwGestureFile> gestureFiles = m_Container.GetGestureFiles();
                for (int i = 0; i < gestureFiles.Count; i++)
                {
                    bwGestureFile gestureFile = gestureFiles[i];
                    int gestureCount = 0;
                    foreach (bwGestureMarker marker in gestureFile.Markers)
                    {
                        if (marker.Name.StartsWith(this.m_MarkerPrefix))
                        {
                            gestureCount++;
                        }
                    }

                    maxGestureCount = Math.Max(gestureCount, maxGestureCount);
                }

                for (int i = 0; i < gestureFiles.Count; i++)
                {
                    bwGestureFile gestureFile = gestureFiles[i];
                    int gestureCount = 0;
                    foreach (bwGestureMarker marker in gestureFile.Markers)
                    {
                        if (marker.Name.StartsWith(this.m_MarkerPrefix))
                        {
                            gestureCount++;
                        }
                    }

                    if (gestureCount == maxGestureCount)
                    {
                        filesWithMaxCount++;
                    }
                }

                int oldCount = currentCount + oldItemCount;
                if (oldCount > maxGestureCount)
                {
                    int heightPerItem = (this.Height - ((oldCount - 1) * 5)) / oldCount;
                    currentCount = Math.Max(currentCount, 1);
                    this.Height = (currentCount * heightPerItem) + ((currentCount - 1) * 5);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {

            }
        }

        public void Update(bwGestureMarker marker)
        {
            if (marker.Name.StartsWith(this.m_MarkerPrefix))
            {
                UpdateRates(marker);
            }
        }

        public void SetCaption(string caption)
        {
            m_Caption = (caption!=null?caption:m_MarkerPrefix);
            Invalidate();
        }

        private void ctrlMarkerTrack_Paint(object sender, PaintEventArgs e)
        {
            string s = (m_Caption != null ? m_Caption : "");
            SizeF szf = e.Graphics.MeasureString(s, m_Container.WaveNameFont);

            if (m_Container != null)
            {
                m_Container.RenderVisibleWaveFiles(new frmWaveContainer.WaveFileRenderCallback(FileRenderCallback), e.Graphics);
            }
            e.Graphics.FillRectangle(m_Container.InactiveBrush, 0, 0, m_Container.XOffset, Height);
            e.Graphics.DrawString(s, m_Container.WaveNameFont, m_Container.InactiveTextBrush, (m_Container.XOffset / 2.0f) - (szf.Width / 2.0f), (Height / 2.0f) - (szf.Height / 2.0f));
        }
        int i = 0;
        private void FileRenderCallback(Graphics g, bwWaveFile wave, bwGestureFile gesture, uint waveStart, uint waveEnd, uint startX, uint endX)
        {
            Font tahomaFont = new Font("Tahoma", 7.0f);

            g.FillRectangle(m_Container.WaveBackgroundBrush, startX, 0, endX - startX, Height);
            g.DrawRectangle(m_Container.WaveOutlinePen, startX, 0, endX - startX, Height-1);

            int maxGestureCount = 0;
            ObservableCollection<bwGestureFile> gestureFiles = m_Container.GetGestureFiles();
            List<bwGestureMarker> overlappedMarkers = new List<bwGestureMarker>();
            for (int i = 0; i < gestureFiles.Count; i++)
            {
                bwGestureFile gestureFile = gestureFiles[i];
                int gestureCount = 0;
                foreach (bwGestureMarker marker in gestureFile.Markers)
                {
                    if (marker.Name.StartsWith(this.m_MarkerPrefix))
                    {
                        gestureCount++;
                        long markerEnd = marker.CropPosition + marker.CropLength;
                        List<bwGestureMarker> individualOverlaps = new List<bwGestureMarker>();
                        foreach (bwGestureMarker other in gestureFile.Markers)
                        {
                            if (other != marker && other.Name.StartsWith(this.m_MarkerPrefix))
                            {
                                if (marker.CropPosition > other.CropPosition &&
                                    marker.CropPosition < other.CropPosition + other.CropLength)
                                {
                                    individualOverlaps.Add(other);
                                }
                            }
                        }

                        if (individualOverlaps.Count >= 2)
                        {
                            if (!overlappedMarkers.Contains(marker))
                            {
                                overlappedMarkers.Add(marker);
                            }

                            foreach (var overlap in individualOverlaps)
                            {
                                if (!overlappedMarkers.Contains(overlap))
                                {
                                    overlappedMarkers.Add(overlap);
                                }
                            }
                        }
                    }
                }

                maxGestureCount = Math.Max(gestureCount, maxGestureCount);
            }

            float renderHeight = 0.0f;
            if (hasBeenRendered)
            {
                renderHeight = (Height - ((maxGestureCount - 1) * 5.0f)) / maxGestureCount;
            }
            else
            {
                renderHeight = Math.Max(Height, (Height * maxGestureCount) + ((maxGestureCount - 1) * 5.0f));
                Height = (int)renderHeight;
            }

            hasBeenRendered = true;
            float top = 0.0f;
            foreach (bwGestureMarker marker in gesture.Markers)
            {
               uint regionLength = 0;
               if (marker != null)
               {
                   regionLength = marker.Length;
               }

                if ((m_MarkerPrefix==null||marker.Name.StartsWith(m_MarkerPrefix)) && marker.Position + regionLength >= waveStart /*&& marker.Position <= waveEnd*/) // Render outside of the end range
                {
                    float ratio = ((float)(endX - startX)) / ((float)(waveEnd - waveStart));
                    float x = (float)startX + (ratio * (float)((int)marker.Position - (int)waveStart));
                    float cropStartX = (float)startX + (ratio * (float)((int)marker.CropPosition - (int)waveStart));
                    float stringX = 0;
                    Pen linePen = new Pen(m_Container.WaveNameBrush);
                    Pen cropLinePen = new Pen(new SolidBrush(Color.Red)) { DashStyle = DashStyle.Dash };
                    if (marker != null)
                    {
                        // 0.1 will render over 3 frames 0.1 * 30 = 3. 0.1 seconds with 30 frames being 1 second
                        float blendIn = ((float)marker.Clip.BlendInTime * (float)wave.Format.SampleRate) * ratio;
                        float blendOut = ((float)marker.Clip.BlendOutTime * (float)wave.Format.SampleRate) * ratio;
                        float width = marker.Length * ratio;
                        RectangleF cropRect = new RectangleF(cropStartX, top, marker.CropLength * ratio, renderHeight);

                        // position the text at the highlight
                        stringX = cropRect.Left + (cropRect.Width / 2);
                        g.FillRectangle(Brushes.LightGreen, cropRect);

                        Point[] p1 = new Point[3];
                        p1[0] = new Point((int)cropRect.Left, (int)top);
                        p1[1] = new Point((int)cropRect.Left + (int)blendIn, (int)top);
                        p1[2] = new Point((int)cropRect.Left, (int)top + (int)renderHeight);
                        g.FillPolygon(Brushes.LightPink, p1);

                        Point[] p2 = new Point[3];
                        p2[0] = new Point((int)cropRect.Right, (int)top);
                        p2[1] = new Point((int)cropRect.Right - (int)blendOut, (int)top);
                        p2[2] = new Point((int)cropRect.Right, (int)top + (int)renderHeight);
                        g.FillPolygon(Brushes.LightPink, p2);

                        g.DrawLine(linePen, new Point((int)cropRect.Left, (int)top + (int)renderHeight), new Point((int)cropRect.Left + (int)blendIn, (int)top));
                        g.DrawLine(linePen, new Point((int)cropRect.Right, (int)top + (int)renderHeight), new Point((int)cropRect.Right - (int)blendOut, (int)top));

                        int startFrame = (int)((marker.Position / (float)wave.Format.SampleRate) * 30.0);
                        int endFrame = (int)(((marker.Position + marker.Length) / (float)wave.Format.SampleRate) * 30.0);

                        // print frames
                        string startText = startFrame.ToString();
                        SizeF startTextMeasure = g.MeasureString(startText, tahomaFont);
                        g.FillRectangle(Brushes.White, new Rectangle((int)cropRect.Left, (int)top, (int)startTextMeasure.Width, (int)startTextMeasure.Height));
                        g.DrawString(startText, tahomaFont, Brushes.Black, new Point((int)cropRect.Left + 1, (int)top + 1));

                        string startTrimText = string.Format("{0:N3}", marker.Clip.StartFrame);
                        SizeF startTrimTextMeasure = g.MeasureString(startTrimText, tahomaFont);
                        g.FillRectangle(Brushes.White, new Rectangle((int)cropRect.Left, (int)top + (int)startTextMeasure.Height, (int)startTrimTextMeasure.Width, (int)startTrimTextMeasure.Height));
                        g.DrawString(startTrimText, tahomaFont, Brushes.Black, new Point((int)cropRect.Left + 1, (int)top + (int)startTrimTextMeasure.Height));

                        string endText = endFrame.ToString();
                        SizeF endTextMeasure = g.MeasureString(endText, tahomaFont);
                        g.FillRectangle(Brushes.White, new Rectangle(((int)cropRect.Right - (int)endTextMeasure.Width), (int)top, (int)endTextMeasure.Width, (int)endTextMeasure.Height));
                        g.DrawString(endText, tahomaFont, Brushes.Black, new Point((int)(cropRect.Right - endTextMeasure.Width), (int)top + 1));

                        string endTrimText = string.Format("{0:N3}", marker.Clip.EndFrame);
                        SizeF endTrimTextMeasure = g.MeasureString(endTrimText, tahomaFont);
                        g.FillRectangle(Brushes.White, new Rectangle(((int)cropRect.Right - (int)endTrimTextMeasure.Width), (int)top + (int)endTextMeasure.Height, (int)endTrimTextMeasure.Width, (int)endTrimTextMeasure.Height));
                        g.DrawString(endTrimText, tahomaFont, Brushes.Black, new Point((int)(cropRect.Right - endTrimTextMeasure.Width), (int)top + (int)endTrimTextMeasure.Height));

                        // print blends
                        g.FillRectangle(Brushes.White, new Rectangle((int)cropRect.Left, (int)top + (int)renderHeight - (int)g.MeasureString(marker.Clip.BlendInTime.ToString(), tahomaFont).Height, (int)g.MeasureString(marker.Clip.BlendInTime.ToString(), tahomaFont).Width, (int)g.MeasureString(marker.Clip.BlendInTime.ToString(), tahomaFont).Height));
                        g.DrawString(marker.Clip.BlendInTime.ToString(), tahomaFont, Brushes.Black, new Point((int)cropRect.Left, (int)top + (int)renderHeight - 12));

                        g.FillRectangle(Brushes.White, new Rectangle((int)cropRect.Right - ((int)g.MeasureString(marker.Clip.BlendOutTime.ToString(), tahomaFont).Width), (int)top + (int)renderHeight - 12, (int)g.MeasureString(marker.Clip.BlendOutTime.ToString(), tahomaFont).Width, (int)g.MeasureString(marker.Clip.BlendInTime.ToString(), tahomaFont).Height));
                        g.DrawString(marker.Clip.BlendOutTime.ToString(), tahomaFont, Brushes.Black, new Point((int)cropRect.Right - ((int)g.MeasureString(marker.Clip.BlendOutTime.ToString(), tahomaFont).Width), (int)top + (int)renderHeight - 12));

                        g.DrawLine(linePen, new Point((int)cropRect.Left, (int)top), new Point((int)cropRect.Right, (int)top));
                        if ((int)top + (int)renderHeight - this.Height == 0)
                        {
                            g.DrawLine(linePen, new Point((int)cropRect.Left, (int)top + (int)renderHeight - 1), new Point((int)cropRect.Right, (int)top + (int)renderHeight - 1));
                        }
                        else
                        {
                            g.DrawLine(linePen, new Point((int)cropRect.Left, (int)top + (int)renderHeight), new Point((int)cropRect.Right, (int)top + (int)renderHeight));
                        }

                        g.DrawLine(cropLinePen, new Point((int)cropRect.Left, (int)top), new Point((int)cropRect.Left, (int)top + (int)renderHeight));
                        g.DrawLine(cropLinePen, new Point((int)cropRect.Right, (int)top), new Point((int)cropRect.Right, (int)top + (int)renderHeight));
                        i++;
                    }

                    linePen.Dispose();

                    if (overlappedMarkers.Contains(marker))
                    {
                        SizeF szfDir = g.MeasureString(marker.Dir + " (overlap warning)", new Font("Tahoma", 8.0f, FontStyle.Bold));
                        g.DrawString(marker.Dir + " (overlap warning)", new Font("Tahoma", 8.0f, FontStyle.Bold), Brushes.Black, stringX - (szfDir.Width / 2), (int)top + (renderHeight / 3.0f) - (szfDir.Height / 2.0f));

                        string name = (m_MarkerPrefix != null ? marker.Name.Substring(m_MarkerPrefix.Length) : marker.Name) + " (x" + marker.Clip.Rate.ToString("n3") + ")";
                        SizeF szf = g.MeasureString(name, new Font("Tahoma", 8.0f, FontStyle.Bold));
                        g.DrawString(name, new Font("Tahoma", 8.0f, FontStyle.Bold), Brushes.Black, stringX - (szf.Width / 2), (int)top + (renderHeight / 1.5f) - (szf.Height / 2.0f));
                    }
                    else
                    {
                        SizeF szfDir = g.MeasureString(marker.Dir, new Font("Tahoma", 8.0f, FontStyle.Bold));
                        g.DrawString(marker.Dir, new Font("Tahoma", 8.0f, FontStyle.Bold), Brushes.Black, stringX - (szfDir.Width / 2), (int)top + (renderHeight / 3.0f) - (szfDir.Height / 2.0f));

                        string name = (m_MarkerPrefix != null ? marker.Name.Substring(m_MarkerPrefix.Length) : marker.Name) + " (x" + marker.Clip.Rate.ToString("n3") + ")";
                        SizeF szf = g.MeasureString(name, new Font("Tahoma", 8.0f, FontStyle.Bold));
                        g.DrawString(name, new Font("Tahoma", 8.0f, FontStyle.Bold), Brushes.Black, stringX - (szf.Width / 2), (int)top + (renderHeight / 1.5f) - (szf.Height / 2.0f));
                    }   
                }

                if (m_MarkerPrefix==null || marker.Name.StartsWith(m_MarkerPrefix))
                {
                    top += renderHeight + 5.0f;
                }
            }

            // Render over all tracks, even if they are empty.
            if (wave == m_Container.PlayingWave)
            {
                int playPosition = m_Container.PlayPosition;
                int cx = (int)startX + m_Container.ConvertSamplesToPixels((playPosition - (int)waveStart));
                g.DrawLine(m_Container.PlaybackPositionPen, cx, 0, cx, 100);
            }
        }

        private void addMarkerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bwWaveFile wave;
            bwGestureFile gesture;
            int offset;
            m_Container.ComputeWaveAndOffset(m_LastMouseSample, out wave, out gesture, out offset);
            if (wave != null)
            {
                string markerName = frmSimpleInput.GetInput("Marker name", (m_MarkerPrefix!=null?m_MarkerPrefix:null));
                if (markerName != null)
                {
                    var marker = new bwGestureMarker(markerName, offset);

                    int startFrame = (int)(((double)marker.Position / (double)wave.Format.SampleRate) * 30);
                    int endFrame = (int)(((double)(marker.Position + marker.Length) / (double)wave.Format.SampleRate) * 30);

                    marker.Clip.StartFrame = startFrame;
                    marker.Clip.EndFrame = endFrame;

                    gesture.Markers.Add(marker);
                    m_Container.Invalidate(true);
                }
            }
        }

        private void addregionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bwWaveFile wave;
            bwGestureFile gesture;
            int offset;
            m_Container.ComputeWaveAndOffset(m_LastMouseSample, out wave, out gesture, out offset);
            if (wave != null)
            {
                string regionName = frmSimpleInput.GetInput("Region name", (m_MarkerPrefix != null ? m_MarkerPrefix : null));
                if (regionName != null)
                {
                    gesture.Markers.Add(new bwGestureMarker(regionName, offset, tolerance));
                    m_Container.Invalidate(true);
                }
            }
        }

        private void UpdateRates(bwGestureMarker marker)
        {
            bwGestureFile gesture;
            bwWaveFile wave;
            int offset;
            m_Container.ComputeWaveAndOffset(marker.CropPosition, out wave, out gesture, out offset);

            if (wave == null) return;

            if (OnAnimationDefaultDuration != null)
            {
                if (!marker.Clip.m_bRateChange)
                {
                    uint duration = OnAnimationDefaultDuration(marker.Name, marker.Dir);
                    duration = wave.ConvertMsToSamples(duration);

                    marker.Clip.Rate = (float)duration / (float)marker.Length;
                }
                else
                {
                    uint duration = OnAnimationDefaultDuration(marker.Name, marker.Dir);
                    duration = wave.ConvertMsToSamples(duration);

                    int newLength = (int)((float)duration / marker.Clip.Rate);
                    int newPivot = marker.Position + (int)(marker.Clip.StartFrame * newLength);
                    int pivotDifference = newPivot - marker.CropPosition;

                    int cropStart = (int)(newLength * marker.Clip.StartFrame);
                    int cropEnd = (int)(newLength * marker.Clip.EndFrame);
                    int newCropLength = cropEnd - cropStart;
                    float blendInLength = marker.Clip.BlendInTime * marker.Clip.SampleRate;
                    float blendOutLength = marker.Clip.BlendOutTime * marker.Clip.SampleRate;
                    if (newCropLength < blendInLength || newCropLength < blendOutLength)
                    {
                        newCropLength = (int)Math.Max(blendInLength, blendOutLength);
                        float cropProportion = marker.Clip.EndFrame - marker.Clip.StartFrame;
                        if (cropProportion == 1.0)
                        {
                            newLength = newCropLength;
                        }
                        else
                        {
                            newLength = (int)(newCropLength / (1.0 - (marker.Clip.EndFrame - marker.Clip.StartFrame)));
                        }

                        newPivot = marker.Position + (int)(marker.Clip.StartFrame * newLength);
                        pivotDifference = newPivot - marker.CropPosition;

                        marker.Clip.Rate = (float)duration / (float)newLength;
                        marker.Length = (uint)newLength;
                        marker.Position -= pivotDifference;
                    }
                    else
                    {
                        marker.Length = (uint)newLength;
                        marker.Position -= pivotDifference;
                    }

                    marker.Clip.m_bRateChange = false;
                }
            }
        }

        private void ctrlMarkerTrack_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
        }

        private void ctrlMarkerTrack_MouseMove(object sender, MouseEventArgs e)
        {
            m_LastMouseSample = m_Container.ConvertXToSamples(e.X);
            m_LastMouseY = e.Y;

            bwGestureMarker region;
            int offset;
            bwWaveFile wave = null;
            bwGestureFile gesture;

            Cursor = Cursors.Arrow;
            if (e.Button == MouseButtons.Left)
            {
                if (m_LastMarker == null || m_LastMarkerWave == null)
                {
                    return;
                }

                offset = (int)m_LastMouseSample - m_Container.ComputeWaveOffset(m_LastMarkerWave);
                if (this.m_IsMovingLeftBlend)
                {
                    Cursor = Cursors.SizeWE;
                    m_LastMarker.Clip.BlendInTime = (offset - m_LastMarker.CropPosition) / (float)m_LastMarkerWave.Format.SampleRate;

                    Invalidate();
                    return;
                }
                else if (this.m_IsMovingRightBlend)
                {
                    Cursor = Cursors.SizeWE;
                    float endPosition = m_LastMarker.CropPosition + m_LastMarker.CropLength;
                    m_LastMarker.Clip.BlendOutTime = (endPosition - offset) / (float)m_LastMarkerWave.Format.SampleRate;

                    Invalidate();
                    return;
                }

                if (this.IsCtrlPressed())
                {
                    if (m_IsMovingStartCrop)
                    {
                        Cursor = Cursors.VSplit;
                        int dif = (int)offset - (int)m_LastMarker.CropPosition;
                        int cropLength = (int)Math.Max(m_LastMarker.CropLength - dif, tolerance);
                        float blendInLength = m_LastMarker.Clip.BlendInTime * m_LastMarker.Clip.SampleRate;
                        float blendOutLength = m_LastMarker.Clip.BlendOutTime * m_LastMarker.Clip.SampleRate;
                        if (cropLength < blendInLength || cropLength < blendOutLength)
                        {
                            cropLength = (int)Math.Max(blendInLength, blendOutLength);
                        }

                        m_LastMarker.Clip.StartFrame = m_LastMarker.Clip.EndFrame - ((float)cropLength / (float)m_LastMarker.Length);
                    }
                    else if (m_IsMovingEndCrop)
                    {
                        Cursor = Cursors.VSplit;
                        int cropLength = (int)Math.Max(m_LastMarker.CropPosition + tolerance, offset) - m_LastMarker.CropPosition;
                        cropLength = (int)Math.Min(cropLength, m_LastMarker.Length);
                        cropLength = Math.Max(cropLength, 0);
                        
                        float blendInLength = m_LastMarker.Clip.BlendInTime * m_LastMarker.Clip.SampleRate;
                        float blendOutLength = m_LastMarker.Clip.BlendOutTime * m_LastMarker.Clip.SampleRate;
                        if (cropLength < blendInLength || cropLength < blendOutLength)
                        {
                            cropLength = (int)Math.Max(blendInLength, blendOutLength);
                        }

                        m_LastMarker.Clip.EndFrame = m_LastMarker.Clip.StartFrame + ((float)cropLength / (float)m_LastMarker.Length);
                    }
                }
                else
                {
                    // Need to make sure that the other end point stays static which means scale around a different pivot point.
                    if (m_IsMovingFrameEnd)
                    {
                        Cursor = Cursors.VSplit;
                        int length = (int)Math.Max(m_LastMarker.CropPosition + tolerance, offset) - m_LastMarker.CropPosition;
                        int lengthDifferent = (int)(m_LastMarker.Length - (m_LastMarker.CropLength));
                        int newLength = length + lengthDifferent;
                        int newPivot = m_LastMarker.Position + (int)(m_LastMarker.Clip.StartFrame * newLength);
                        int pivotDifference = newPivot - m_LastMarker.CropPosition;
                        int newPosition = m_LastMarker.Position - (newPivot - m_LastMarker.CropPosition);

                        // Check to make sure blend out hasn't broken its bounds.
                        int cropStart = (int)(newLength * m_LastMarker.Clip.StartFrame);
                        int cropEnd = (int)(newLength * m_LastMarker.Clip.EndFrame);
                        int newCropLength = cropEnd - cropStart;
                        float blendOutOffset = (m_LastMarker.CropPosition + newCropLength) - (m_LastMarker.Clip.BlendOutTime * m_LastMarker.Clip.SampleRate);
                        float blendInOffset = m_LastMarker.CropPosition + (m_LastMarker.Clip.BlendInTime * m_LastMarker.Clip.SampleRate);
                        if (m_LastMarker.CropPosition > blendOutOffset)
                        {
                            if (m_LastMarker.CropLength == m_LastMarker.Clip.BlendInTime * m_LastMarker.Clip.SampleRate)
                            {
                                return;
                            }

                            length = (int)Math.Max(tolerance, m_LastMarker.Clip.BlendOutTime * m_LastMarker.Clip.SampleRate);
                            lengthDifferent = (int)(m_LastMarker.Length - (m_LastMarker.CropLength));
                            newLength = length + lengthDifferent;
                            newPivot = m_LastMarker.Position + (int)(m_LastMarker.Clip.StartFrame * newLength);
                            pivotDifference = newPivot - m_LastMarker.CropPosition;
                            newPosition = m_LastMarker.Position - (newPivot - m_LastMarker.CropPosition);

                            m_LastMarker.Length = (uint)(newLength);
                            m_LastMarker.Position = newPosition;
                        }
                        else if (m_LastMarker.CropPosition + newCropLength < blendInOffset)
                        {
                            if (m_LastMarker.CropLength == m_LastMarker.Clip.BlendInTime * m_LastMarker.Clip.SampleRate)
                            {
                                return;
                            }

                            length = (int)Math.Max(tolerance, m_LastMarker.Clip.BlendInTime * m_LastMarker.Clip.SampleRate);
                            lengthDifferent = (int)(m_LastMarker.Length - (m_LastMarker.CropLength));
                            newLength = length + lengthDifferent;
                            newPivot = m_LastMarker.Position + (int)(m_LastMarker.Clip.StartFrame * newLength);
                            pivotDifference = newPivot - m_LastMarker.CropPosition;
                            newPosition = m_LastMarker.Position - (newPivot - m_LastMarker.CropPosition);

                            m_LastMarker.Length = (uint)(newLength);
                            m_LastMarker.Position = newPosition;
                        }
                        else
                        {
                            m_LastMarker.Length = (uint)(newLength);
                            m_LastMarker.Position = newPosition;
                        }

                        blendInOffset = m_LastMarker.CropPosition + (m_LastMarker.Clip.BlendInTime * m_LastMarker.Clip.SampleRate);
                        System.Diagnostics.Debug.Print(string.Format("{0} : {1}", blendInOffset, m_LastMarker.CropPosition + m_LastMarker.CropLength));


                        m_Container.SetStatus(string.Format("length: {0} (samp: {1})", m_LastMarker.Length, offset));
                        UpdateRates(m_LastMarker);
                    }
                    else if (m_IsMovingFrameStart)
                    {
                        Cursor = Cursors.VSplit;
                        offset = Math.Max(offset, 0);
                        int delta = m_LastMarker.CropPosition - offset;
                        if (delta == 0)
                        {
                            return;
                        }

                        int oldPivot = m_LastMarker.Position + (int)(m_LastMarker.Clip.EndFrame * m_LastMarker.Length);
                        float newLength = (oldPivot - offset) / (m_LastMarker.Clip.EndFrame - m_LastMarker.Clip.StartFrame);

                        float newPosition = offset - (m_LastMarker.Clip.StartFrame * newLength);
                        // Check to make sure blend out hasn't broken its bounds.
                        int newCropPosition = (int)(newPosition + (int)(m_LastMarker.Clip.StartFrame * newLength));

                        int cropStart = (int)(newLength * m_LastMarker.Clip.StartFrame);
                        int cropEnd = (int)(newLength * m_LastMarker.Clip.EndFrame);
                        int newCropLength = cropEnd - cropStart;

                        float blendOutOffset = newCropPosition + newCropLength - (m_LastMarker.Clip.BlendOutTime * m_LastMarker.Clip.SampleRate);
                        float blendInOffset = newCropPosition + (m_LastMarker.Clip.BlendInTime * m_LastMarker.Clip.SampleRate);
                        if (newCropPosition > blendOutOffset)
                        {
                            if (m_LastMarker.CropLength == m_LastMarker.Clip.BlendInTime * m_LastMarker.Clip.SampleRate)
                            {
                                return;
                            }

                            newCropPosition = oldPivot - (int)(m_LastMarker.Clip.BlendOutTime * m_LastMarker.Clip.SampleRate);
                            newLength = (oldPivot - newCropPosition) / (m_LastMarker.Clip.EndFrame - m_LastMarker.Clip.StartFrame);
                            newPosition = newCropPosition - (m_LastMarker.Clip.StartFrame * newLength);
                            m_LastMarker.Length = (uint)newLength;
                            m_LastMarker.Position = (int)newPosition;
                        }
                        else if (oldPivot < blendInOffset)
                        {
                            newCropPosition = oldPivot - (int)(m_LastMarker.Clip.BlendInTime * m_LastMarker.Clip.SampleRate);
                            newLength = (oldPivot - newCropPosition) / (m_LastMarker.Clip.EndFrame - m_LastMarker.Clip.StartFrame);
                            newPosition = newCropPosition - (m_LastMarker.Clip.StartFrame * newLength);
                            m_LastMarker.Length = (uint)newLength;
                            m_LastMarker.Position = (int)newPosition;
                        }
                        else
                        {
                            if (newLength < tolerance)
                            {
                                newLength = tolerance;
                                newPosition = oldPivot - (m_LastMarker.Clip.EndFrame * newLength);
                                m_LastMarker.Length = (uint)newLength;
                                m_LastMarker.Position = (int)newPosition;
                            }
                            else
                            {
                                m_LastMarker.Length = (uint)newLength;
                                m_LastMarker.Position = (int)newPosition;
                            }
                        }

                        UpdateRates(m_LastMarker);
                        m_Container.SetStatus(string.Format("length: {0} (samp: {1})", m_LastMarker.Length, offset));
                    }
                    else
                    {
                        Cursor = Cursors.Hand;

                        int offset2 = 0;
                        m_Container.ComputeWaveAndOffset(m_LastMouseSample, out wave, out gesture, out offset2);

                        if (wave != null)
                        {
                            int cropPos = (int)Math.Max(0, ((int)offset + m_LastMarkerOffset));
                            int diff = m_LastMarker.CropPosition - cropPos;
                            m_LastMarker.Position -= diff;

                            TimeSpan r = TimeSpan.FromSeconds((double)m_LastMarker.Position / (double)wave.Format.SampleRate);
                            String dd = String.Format("{0}:{1}:{2}.{3}", r.Hours, r.Minutes, r.Seconds, r.Milliseconds);
                            int startFrame = (int)(((double)m_LastMarker.Position / (double)wave.Format.SampleRate) * 30);
                            int endFrame = (int)(((double)(m_LastMarker.Position + m_LastMarker.Length) / (double)wave.Format.SampleRate) * 30);

                            //m_LastMarker.Clip.StartFrame = startFrame;
                            //m_LastMarker.Clip.EndFrame = endFrame;

                            m_Container.SetStatus(string.Format("{0} : {1} / {2} / {3}", m_LastMarker.Name, m_LastMarker.Position, dd, startFrame.ToString()));
                        }
                    }
                }
                
                Invalidate();
            }
            else if (e.Button == MouseButtons.None)
            {
                m_IsMovingFrameEnd = false;
                m_IsMovingFrameStart = false;
                m_IsMovingEndCrop = false;
                m_IsMovingStartCrop = false;
                m_IsMovingLeftBlend = false;
                m_IsMovingRightBlend = false;
                // see if we are at the edge of a region
                m_Container.ComputeWaveAndOffset(m_LastMouseSample, out wave, out gesture, out offset);
                bwGestureMarker marker;
                if (wave != null)
                {
                    marker = ComputeMarkerAtSample(gesture, offset, e.Y);
                }
                else
                {
                    // search outwith this wave
                    ComputeRegionAtMouse(out wave, out region, out offset);
                    marker = region as bwGestureMarker;
                }
                if (marker != null && gesture != null)
                {
                    m_Container.SetStatus(string.Format("{0}: {1} ({2})", m_MarkerPrefix, offset, marker.Name));
                    region = marker as bwGestureMarker;
                    if (region == null)
                    {
                        return;
                    }

                    float blendInOffset = region.CropPosition + (marker.Clip.BlendInTime * (float)wave.Format.SampleRate);
                    float blendOutOffset = region.CropPosition + region.CropLength - (marker.Clip.BlendOutTime * (float)wave.Format.SampleRate);

                    int maxGestureCount = 0;
                    ObservableCollection<bwGestureFile> gestureFiles = m_Container.GetGestureFiles();
                    for (int i = 0; i < gestureFiles.Count; i++)
                    {
                        bwGestureFile gestureFile = gestureFiles[i];
                        int gestureCount = 0;
                        foreach (bwGestureMarker mark in gestureFile.Markers)
                        {
                            if (mark.Name.StartsWith(this.m_MarkerPrefix))
                            {
                                gestureCount++;
                            }
                        }

                        maxGestureCount = Math.Max(gestureCount, maxGestureCount );
                    }

                    float renderHeight = (Height - ((maxGestureCount - 1) * 5.0f)) / maxGestureCount;
                    // Markers are in different categories/tracks so we cant just use the index as this will yield incorrect positioning. 
                    // We need the index per track entry so we look for the prefix and count to return the correct index.
                    int markerIndex = ComputeMarkerIndex(marker, gesture.Markers, m_MarkerPrefix);
                    float top = (renderHeight + 5.0f) * markerIndex;

                    if (e.Y >= top && e.Y <= top + 10 && Math.Abs(blendInOffset - offset) <= tolerance)
                    {
                        Cursor = Cursors.SizeWE;
                        m_IsMovingLeftBlend = true;
                        return;
                    }
                    else if (e.Y >= top && e.Y <= top + 10 && Math.Abs(blendOutOffset - offset) <= tolerance)
                    {
                        Cursor = Cursors.SizeWE;
                        m_IsMovingRightBlend = true;
                        return;
                    }
                    
                    if (Math.Abs((int)region.CropPosition + (int)region.CropLength - (int)offset) <= tolerance)
                    {
                        Cursor = Cursors.VSplit;
                        if (this.IsCtrlPressed())
                        {
                            m_IsMovingEndCrop = true;
                        }
                        else
                        {
                            m_IsMovingFrameEnd = true;
                        }
                    }
                    else if (Math.Abs((Math.Abs((int)region.CropPosition + (int)region.CropLength - (int)offset) - (int)region.CropLength)) <= tolerance)
                    {
                        Cursor = Cursors.VSplit;
                        if (this.IsCtrlPressed())
                        {
                            m_IsMovingStartCrop = true;
                        }
                        else
                        {
                            m_IsMovingFrameStart = true;
                        }
                    }
                    else
                    {
                        Cursor = Cursors.Hand;
                    }
                }
            }
        }

        private bool IsCtrlPressed()
        {
            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl))
            {
                return true;
            }

            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl))
            {
                return true;
            }

            return false;
        }

        private void ctrlMarkerTrack_MouseClick(object sender, MouseEventArgs e)
        {
           
        }


        private void ctrlMarkerTrack_MouseDown(object sender, MouseEventArgs e)
        {
            bwWaveFile wave;
            bwGestureFile gesture;
            int offset;
            m_Container.ComputeWaveAndOffset(m_LastMouseSample, out wave, out gesture, out offset);
            if (wave != null)
            {
                m_LastMarker = ComputeMarkerAtSample(gesture, offset, e.Y);
                m_LastMarkerWave = wave;
                m_LastGestureFile = gesture;
            }
            else
            {
                m_Container.ComputeRegionAtSample(m_LastMouseSample, out wave, out m_LastMarker, out offset);
                m_LastMarkerWave = wave;
                m_LastGestureFile = gesture;
            }
            if (m_LastMarker != null)
            {
                m_LastMarkerOffset = m_LastMarker.CropPosition - offset;
            }
        }

        private int ComputeMarkerIndex(bwGestureMarker marker, ObservableCollection<bwGestureMarker> markers, string markerPrefix)
        {
            int index = 0;
            foreach (bwGestureMarker m in markers)
            {
                if (m.Name.StartsWith(markerPrefix))
                {
                    if (marker == m) return index;
                    index++;
                }
            }

            return index;
        }

        private bwGestureMarker ComputeMarkerAtSample(bwGestureFile gesture, int sample, int yoffset)
        {
            int maxGestureCount = 0;
            ObservableCollection<bwGestureFile> gestureFiles = m_Container.GetGestureFiles();
            List<bwGestureMarker> markers = new List<bwGestureMarker>();
            for (int i = 0; i < gestureFiles.Count; i++)
            {
                bwGestureFile gestureFile = gestureFiles[i];
                int gestureCount = 0;
                foreach (bwGestureMarker marker in gestureFile.Markers)
                {
                    if (marker.Name.StartsWith(this.m_MarkerPrefix))
                    {
                        gestureCount++;
                        if (object.ReferenceEquals(gestureFile, gesture))
                        {
                            markers.Add(marker);
                        }
                    }
                }

                maxGestureCount = Math.Max(gestureCount, maxGestureCount);
            }

            float renderHeight = (Height - ((maxGestureCount - 1) * 5.0f)) / maxGestureCount;
            float top = 0.0f;
            foreach (bwGestureMarker marker in markers)
            {
                if (yoffset < top || yoffset > (top + renderHeight))
                {
                    top += renderHeight + 5.0f;
                    continue;
                }
                
                if (marker is bwGestureMarker)
                {
                    int s = (int)sample;
                    int pos = (int)marker.CropPosition;
                    int end = (int)(marker.CropPosition + marker.CropLength);
                    if (s >= pos - tolerance && s <= end + tolerance)
                    {
                        return marker;
                    }
                }
                else
                {
                    if (Math.Abs(marker.Position - sample) <= tolerance)
                    {
                        return marker;
                    }
                }

                top += renderHeight + 5.0f;
            }

            return null;
        }

        private void ctrlMarkerTrack_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.X <= m_Container.XOffset)
            {
                SetCaption(frmSimpleInput.GetInput("Track Name", m_Caption));
            }
            else
            {
                bwWaveFile wave;
                bwGestureMarker marker;
                bwGestureFile gesture;
                int offset;
                m_Container.ComputeWaveAndOffset(m_LastMouseSample, out wave, out gesture, out offset);
                if (wave == null)
                {
                    // search beyond this wave
                    ComputeRegionAtMouse(out wave, out marker, out offset);
                }
                else
                {
                    marker = ComputeMarkerAtSample(gesture, offset, e.Y);
                }

                if (wave != null && marker != null)
                {
                    string result = frmSimpleInput.GetInput("Name", marker.Name);
                    if (result != null)
                    {
                        marker.Name = result;
                        Invalidate();
                    }
                }
            }
        }

        private void ComputeRegionAtMouse(out bwWaveFile wave, out bwGestureMarker region, out int offset)
        {
            m_Container.ComputeRegionAtSample(m_LastMouseSample, out wave, out region, out offset);
        }

        private void ctrlMarkerTrack_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }

        void ctrlMarkerTrack_KeyPress(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                //delete the gesture...taken from deleteToolStripMenuItem_Click
                bwWaveFile wave;
                bwGestureMarker marker;
                bwGestureFile gesture;
                int offset;
                m_Container.ComputeWaveAndOffset(m_LastMouseSample, out wave, out gesture, out offset);
                if (wave == null)
                {
                    // search beyond this wave
                    ComputeRegionAtMouse(out wave, out marker, out offset);
                }
                else
                {
                    marker = ComputeMarkerAtSample(gesture, offset, m_LastMouseY);
                }

                if (marker != null)
                {
                    gesture.Markers.Remove(marker);
                    Invalidate();
                }
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bwWaveFile wave;
            bwGestureMarker marker;
            bwGestureFile gesture;

            int offset;
            m_Container.ComputeWaveAndOffset(m_LastMouseSample, out wave, out gesture, out offset);
            if (wave == null)
            {
                // search beyond this wave
                ComputeRegionAtMouse(out wave, out marker, out offset);
            }
            else
            {
                marker = ComputeMarkerAtSample(gesture, offset, m_LastMouseY);
            }

            if (marker != null)
            {
                gesture.Markers.Remove(marker);
                Invalidate();
            }
        }

        private void m_ContextMenu_Opening(object sender, CancelEventArgs e)
        {
            if (OnContextMenuPopup != null)
            {
                bwWaveFile wave;
                bwGestureMarker marker;
                bwGestureFile gesture;
                int offset;
                m_Container.ComputeWaveAndOffset(m_LastMouseSample, out wave, out gesture, out offset);
                if (wave == null)
                {
                    // search beyond this wave
                    ComputeRegionAtMouse(out wave, out marker, out offset);
                }
                else
                {
                    marker = ComputeMarkerAtSample(gesture, offset, m_LastMouseY);
                }

                OnContextMenuPopup(m_MarkerPrefix,marker, wave, gesture, offset, m_ContextMenu);
            }
        }
    }
}
