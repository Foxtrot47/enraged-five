using System;
using System.Collections.Generic;
using System.Text;

namespace BlueWave.Script
{
    public interface IBlueWave
    {
        IWaveContainer NewContainer(string title);
    }
}
