using System;
using System.Collections.Generic;
using System.Text;

namespace BlueWave.Script
{
    public interface IBlueWaveScript
    {
        void Init(IBlueWave app, string[] arguments);
        void Shutdown();
    }
}
