using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

using BlueWave.Script;

namespace BlueWave
{
    class ScriptHost : IBlueWaveScript
    {
        Assembly m_Assembly;
        IBlueWaveScript m_Script;
        public ScriptHost(string script)
        {
            m_Assembly = Assembly.LoadFile(script);
            if (m_Assembly != null)
            {
                foreach (Type t in m_Assembly.GetExportedTypes())
                {
                    if (t.GetInterface("BlueWave.Script.IBlueWaveScript") != null)
                    {
                        m_Script = (IBlueWaveScript)Activator.CreateInstance(t);
                        break;
                    }
                }
            }
        }

        public void Init(IBlueWave app, string[] arguments)
        {
            m_Script.Init(app, arguments);
        }

        public void Shutdown()
        {
            m_Script.Shutdown();
        }
    }
}
