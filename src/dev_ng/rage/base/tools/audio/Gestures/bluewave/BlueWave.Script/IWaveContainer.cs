using System;
using System.Collections.Generic;
using System.Text;
using Wavelib;
using System.Windows.Forms;
using Gesturelib;
using System.Collections.ObjectModel;


namespace BlueWave.Script
{
    public delegate void OnMarkerHitDelegate(bwGestureMarker marker, bwWaveFile wave);
    public delegate void OnWaveEventDelegate(bwGestureFile gesture, bwWaveFile wave);
    public delegate void OnCloseDelegate();
    public delegate void OnKeyPressDelegate(int pos, bwWaveFile wave, KeyEventArgs e);

    public interface IWaveContainer
    {    
        event OnMarkerHitDelegate OnMarkerHit;
        event OnWaveEventDelegate OnWaveSave;
        event OnWaveEventDelegate OnWaveOpen;
        event OnCloseDelegate OnClose;
        event OnKeyPressDelegate OnProcessKey;

        bwWaveFileList GetWaveFiles();
        ObservableCollection<bwGestureFile> GetGestureFiles();
        IMarkerTrack AddMarkerTrack(string prefix, string caption);
        void AddFile(string fileName);
        void Close();
        void Refresh();
        void GetCursor(out bwWaveFile wave, out bwGestureFile gesture, out int offset);


        void Invalidate();
    }
}
