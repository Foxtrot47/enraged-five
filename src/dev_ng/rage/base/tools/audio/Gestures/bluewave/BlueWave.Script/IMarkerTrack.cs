using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Wavelib;
using Gesturelib;

namespace BlueWave.Script
{
    public delegate void OnContextMenuPopupDelegate(string markerPrefix, bwGestureMarker marker, bwWaveFile wave, bwGestureFile gesture, int offset, ContextMenuStrip menu);
    public delegate uint FindAnimationDefaultDurationDelegate(string markerName, string markerDir);

    public interface IMarkerTrack
    {
        event OnContextMenuPopupDelegate OnContextMenuPopup;
        event FindAnimationDefaultDurationDelegate OnAnimationDefaultDuration;

        void Invalidate();
        void Update(bwGestureMarker marker);
    }
}
