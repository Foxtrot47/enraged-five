using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using BlueWave.Script;
using Wavelib;
using Gesturelib;

namespace BlueWave
{
    public partial class ctrlWaveChannel : UserControl
    {
        frmWaveContainer m_Container;
        Int32 m_ChannelIndex;
        Hashtable m_Bitmaps;
        public Button m_PlayButton = new Button();
        
        static Color sm_WaveFormColour;
        static Pen sm_XAxisPen;

        const int ruler = 100;

        public ctrlWaveChannel()
        {
            m_ChannelIndex = 0;
            m_Bitmaps = new Hashtable();
            m_PlayButton.Location = new Point(0, 0);
            m_PlayButton.Text = "Play";
            Controls.Add(m_PlayButton);
            InitializeComponent();
        }

        static ctrlWaveChannel()
        {
            InitBrushes();
        }

        public static void InitBrushes()
        {
           sm_WaveFormColour = Color.CornflowerBlue;
           sm_XAxisPen = new Pen(Color.Black);
        }

        void ctrlWaveChannel_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //if (e.Delta < 0)
            //{
            //    m_Container.ZoomRatio *= 0.9f;
            //}
            //else
            //{
            //    m_Container.ZoomRatio *= 1.1f;
            //}
        }

        private void ctrlWaveChannel_Paint(object sender, PaintEventArgs e)
        {
            if (m_Container==null)
            {
                m_Container = ParentForm as frmWaveContainer;
            }
            if (m_Container != null)
            {
                e.Graphics.FillRectangle(m_Container.InactiveBrush, 0, 0, m_Container.XOffset, Height);
                m_Container.RenderVisibleWaveFiles(new frmWaveContainer.WaveFileRenderCallback(FileRenderCallback), e.Graphics);
            }
        }


        private void FileRenderCallback(Graphics g, bwWaveFile wave, bwGestureFile gesture, uint waveStart, uint waveEnd, uint startX, uint endX)
        {
            try
            {
                g.FillRectangle(m_Container.WaveBackgroundBrush, (float)startX, 0, (float)(endX - startX), Height);
                g.DrawLine(sm_XAxisPen, startX, Height / 2, endX, Height / 2);
                g.DrawRectangle(m_Container.WaveOutlinePen, startX, 0, (float)(endX - startX), Height - 1);

                Bitmap waveBmp = (Bitmap)m_Bitmaps[wave];
                if (waveBmp == null)
                {
                    if (!renderThread.IsBusy)
                    {
                        renderThread.RunWorkerAsync(wave);
                    }
                    string s = "Rendering wave ...";
                    SizeF sz = g.MeasureString(s, m_Container.WaveNameFont);
                    g.DrawString(s, m_Container.WaveNameFont, m_Container.WaveNameBrush, startX + ((endX - startX) / 2.0f) - (sz.Width / 2.0f), (Height / 2.0f) - (sz.Height/2.0f));
                }
                else
                {
                    float imageRatio = waveBmp.Width / (float)wave.Data.NumSamples;
                    g.DrawImage(waveBmp, new Rectangle((int)startX, 0, (int)(endX - startX), Height), new Rectangle((int)(waveStart * imageRatio), 0, (int)((waveEnd - waveStart) * imageRatio), waveBmp.Height), GraphicsUnit.Pixel);
                }

                if (m_Container.PlayingWave == wave)
                {
                    int playPosition = m_Container.PlayPosition;
                    if (playPosition > waveStart && playPosition <= waveEnd)
                    {
                       int x = (int)startX + m_Container.ConvertSamplesToPixels((int)(playPosition - (int)waveStart));
                       g.DrawLine(m_Container.PlaybackPositionPen, x, 0, x, Height);
                    }
                }

                if (m_Container.CursorWave == wave)
                {
                    int cursorPosition = m_Container.CursorPosition;
                    if (cursorPosition > waveStart && cursorPosition <= waveEnd)
                    {
                        int x = (int)startX + m_Container.ConvertSamplesToPixels((int)(cursorPosition - (int)waveStart));
                        g.DrawLine(m_Container.CursorPen, x, 0, x, Height);
                    }
                }
                g.DrawString(wave.FileName.Substring(wave.FileName.LastIndexOf('\\') + 1), m_Container.WaveNameFont, m_Container.WaveNameBrush, startX, Height - m_Container.WaveNameFont.Height);

                int frames = (int)(((float)wave.Data.NumSamples / (float)wave.Format.SampleRate) * 30.0f);
                int count = (int)wave.Data.NumSamples / frames;
                float pixelsPerFrame = (float)count / (float)m_Container.ZoomRatio;

                float pixelWaveStart = (float)waveStart / (float)m_Container.ZoomRatio;
                float ratio = 1 / m_Container.ZoomRatio;
                Font font = new Font("Tahoma", 8.0f);
                for (int i = 0; i <= frames - 1; i++)
                {
                    float x = (float)startX + (ratio * (float)((count * i) - (int)waveStart));
                    if (i * pixelsPerFrame < pixelWaveStart)
                    {
                        continue;
                    }
                    
                    if (i % 5 != 0)
                    {
                        g.DrawLine(m_Container.RulerPen, x, 0.0f, x, 5.0f);
                    }
                    else
                    {
                        g.DrawLine(m_Container.RulerPen, x, 0.0f, x, 15.0f);
                        g.DrawString(i.ToString(), font, m_Container.WaveNameBrush, x, 16);
                    }
                }

                m_PlayButton.Size = new Size((int)120, this.Size.Height);
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        private void renderThread_DoWork(object sender, DoWorkEventArgs e)
        {
            Bitmap waveBmp;
            bwWaveFile wave = e.Argument as bwWaveFile;
            float z = 200.0f;// (waveEnd - waveStart) / (float)(endX - startX);
            waveBmp = new Bitmap((int)(wave.Data.NumSamples / z), 400, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics gbmp = Graphics.FromImage(waveBmp);
            gbmp.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Pen pen = new Pen(sm_WaveFormColour,1.0f);
            for (int i = 0; i < waveBmp.Width - 1; i++)
            {
                float xratio = i / (float)waveBmp.Width;
                // rescale sample to [0,1]
                float yratio1 = (wave.Data.ChannelData[m_ChannelIndex, (int)(xratio * wave.Data.NumSamples)] + 1.0f) * 0.5f;

                xratio = (i + 1) / (float)waveBmp.Width;
                // rescale sample to [0,1]
                float yratio2 = (wave.Data.ChannelData[m_ChannelIndex, (int)(xratio * wave.Data.NumSamples)] + 1.0f) * 0.5f;

                gbmp.DrawLine(pen, i, yratio1 * (waveBmp.Height - 1), i + 1, yratio2 * (waveBmp.Height - 1));
            }
            m_Bitmaps.Add(wave, waveBmp);
            pen.Dispose();
            //waveBmp.Save(wave.FileName + ".jpg",System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        private void renderThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Invalidate(true);
        }

        private void ctrlWaveChannel_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_Container != null)
            {
                bwWaveFile wave;
                bwGestureFile gesture;
                int offset;
                m_Container.ComputeWaveAndOffset(m_Container.ConvertXToSamples(e.X), out wave, out gesture, out offset);
                if(wave != null)
                {
                    //System.Diagnostics.Debug.Print(((double)offset / (double)wave.Format.SampleRate).ToString());
                    TimeSpan r = TimeSpan.FromSeconds((double)offset / (double)wave.Format.SampleRate);
                    String dd = String.Format("{0}:{1}:{2}.{3}", r.Hours, r.Minutes, r.Seconds, r.Milliseconds);
                    int frame = (int)(((double)offset / (double)wave.Format.SampleRate) * 30);
                    m_Container.SetStatus(string.Format("{0} : {1} / {2} / {3}", (wave != null ? wave.FileName : ""), m_Container.ConvertXToSamples(e.X), dd, frame.ToString()));
                }
            }
        }

        private void ctrlWaveChannel_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void ctrlWaveChannel_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.X > m_Container.XOffset)
            {
                bwWaveFile wave;
                bwGestureFile gesture;
                int offset;
                m_Container.ComputeWaveAndOffset(m_Container.ConvertXToSamples(e.X), out wave, out gesture, out offset);
                if (wave != null)
                {
                    m_Container.CursorWave = wave;
                    m_Container.CursorGesture = gesture;
                    m_Container.CursorPosition = offset;
                    Invalidate();
                }
            }
        }

    }
}
