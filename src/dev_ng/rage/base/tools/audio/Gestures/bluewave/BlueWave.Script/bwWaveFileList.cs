using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Wavelib;

namespace BlueWave.Script
{
    public class bwWaveFileList
    {
        private ArrayList m_ArrayList;

        public uint TotalLength
        {
            get { uint ret = 0; foreach (bwWaveFile w in m_ArrayList) { ret += w.Data.NumSamples; } return ret; }
        }
        public bwWaveFileList()
        {
            m_ArrayList = new ArrayList();
        }

        public bwWaveFile this[int index]
        {
            get { return (bwWaveFile)m_ArrayList[index]; }
            set { m_ArrayList[index] = value; }
        }

        public int Add(bwWaveFile value)
        {
            return m_ArrayList.Add(value);
        }

        public void Clear()
        {
            m_ArrayList.Clear();
        }

        public bool Contains(bwWaveFile value)
        {
            return m_ArrayList.Contains(value);
        }

        public int IndexOf(bwWaveFile value)
        {
            return m_ArrayList.IndexOf(value);
        }

        public void Insert(int index, bwWaveFile value)
        {
            m_ArrayList.Insert(index, value);
        }

        public void Remove(bwWaveFile value)
        {
            m_ArrayList.Remove(value);
        }

        public void RemoveAt(int index)
        {
            m_ArrayList.RemoveAt(index);
        }

        

        public int Count
        {
            get { return m_ArrayList.Count; }
        }
        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return m_ArrayList.GetEnumerator();
        }

        #endregion
    }
}
