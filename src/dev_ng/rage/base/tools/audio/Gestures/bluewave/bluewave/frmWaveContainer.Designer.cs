namespace BlueWave
{
    partial class frmWaveContainer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_ScrollBar = new System.Windows.Forms.HScrollBar();
            this.m_Menu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileInContainerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addMarkerTrackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_Panel = new System.Windows.Forms.Panel();
            this.m_PlaybackTimer = new System.Windows.Forms.Timer(this.components);
            this.m_OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.m_Status = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_StatusStrip = new System.Windows.Forms.StatusStrip();
            this.ctrlWaveChannel1 = new BlueWave.ctrlWaveChannel();
            this.m_Menu.SuspendLayout();
            this.m_Panel.SuspendLayout();
            this.m_StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_ScrollBar
            // 
            this.m_ScrollBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_ScrollBar.Location = new System.Drawing.Point(0, 325);
            this.m_ScrollBar.Name = "m_ScrollBar";
            this.m_ScrollBar.Size = new System.Drawing.Size(716, 17);
            this.m_ScrollBar.TabIndex = 1;
            this.m_ScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.m_ScrollBar_Scroll);
            // 
            // m_Menu
            // 
            this.m_Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.m_Menu.Location = new System.Drawing.Point(0, 0);
            this.m_Menu.Name = "m_Menu";
            this.m_Menu.Size = new System.Drawing.Size(716, 24);
            this.m_Menu.TabIndex = 3;
            this.m_Menu.Text = "menuStrip1";
            this.m_Menu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileInContainerToolStripMenuItem,
            this.addMarkerTrackToolStripMenuItem,
            this.saveFilesToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.fileToolStripMenuItem.Text = "&Container";
            // 
            // openFileInContainerToolStripMenuItem
            // 
            this.openFileInContainerToolStripMenuItem.Name = "openFileInContainerToolStripMenuItem";
            this.openFileInContainerToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.openFileInContainerToolStripMenuItem.Text = "&Open File";
            this.openFileInContainerToolStripMenuItem.Click += new System.EventHandler(this.openFileInContainerToolStripMenuItem_Click);
            // 
            // addMarkerTrackToolStripMenuItem
            // 
            this.addMarkerTrackToolStripMenuItem.Name = "addMarkerTrackToolStripMenuItem";
            this.addMarkerTrackToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.addMarkerTrackToolStripMenuItem.Text = "Add &Marker Track";
            this.addMarkerTrackToolStripMenuItem.Click += new System.EventHandler(this.addMarkerTrackToolStripMenuItem_Click);
            // 
            // saveFilesToolStripMenuItem
            // 
            this.saveFilesToolStripMenuItem.Name = "saveFilesToolStripMenuItem";
            this.saveFilesToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.saveFilesToolStripMenuItem.Text = "&Save Files";
            this.saveFilesToolStripMenuItem.Click += new System.EventHandler(this.saveFilesToolStripMenuItem_Click);
            // 
            // m_Panel
            // 
            this.m_Panel.Controls.Add(this.ctrlWaveChannel1);
            this.m_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_Panel.Location = new System.Drawing.Point(0, 24);
            this.m_Panel.Name = "m_Panel";
            this.m_Panel.Size = new System.Drawing.Size(716, 301);
            this.m_Panel.TabIndex = 5;
            // 
            // m_PlaybackTimer
            // 
            this.m_PlaybackTimer.Interval = 8;
            this.m_PlaybackTimer.Tick += new System.EventHandler(this.m_PlaybackTimer_Tick);
            // 
            // m_OpenFileDialog
            // 
            this.m_OpenFileDialog.FileName = "*.wav";
            this.m_OpenFileDialog.Filter = "Wave Files|*.wav";
            this.m_OpenFileDialog.Multiselect = true;
            // 
            // m_Status
            // 
            this.m_Status.Name = "m_Status";
            this.m_Status.Size = new System.Drawing.Size(0, 17);
            // 
            // m_StatusStrip
            // 
            this.m_StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_Status});
            this.m_StatusStrip.Location = new System.Drawing.Point(0, 342);
            this.m_StatusStrip.Name = "m_StatusStrip";
            this.m_StatusStrip.Size = new System.Drawing.Size(716, 22);
            this.m_StatusStrip.TabIndex = 2;
            this.m_StatusStrip.Text = "statusStrip1";
            // 
            // ctrlWaveChannel1
            // 
            this.ctrlWaveChannel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlWaveChannel1.Location = new System.Drawing.Point(0, 0);
            this.ctrlWaveChannel1.Name = "ctrlWaveChannel1";
            this.ctrlWaveChannel1.Size = new System.Drawing.Size(716, 301);
            this.ctrlWaveChannel1.TabIndex = 4;
            this.ctrlWaveChannel1.Load += new System.EventHandler(this.ctrlWaveChannel1_Load);
            this.ctrlWaveChannel1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ctrlWaveChannel1_KeyDown);
            this.ctrlWaveChannel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ctrlWaveChannel1_MouseClick);
            this.ctrlWaveChannel1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ctrlWaveChannel1_MouseDoubleClick);
            this.ctrlWaveChannel1.m_PlayButton.Click += new System.EventHandler(this.ctrlWaveChannel1_Play);
            // 
            // frmWaveContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 364);
            this.Controls.Add(this.m_Panel);
            this.Controls.Add(this.m_ScrollBar);
            this.Controls.Add(this.m_StatusStrip);
            this.Controls.Add(this.m_Menu);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.m_Menu;
            this.Name = "frmWaveContainer";
            this.Text = "frmWaveContainer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmWaveContainer_FormClosing);
            this.SizeChanged += new System.EventHandler(this.frmWaveContainer_SizeChanged);
            this.Enter += new System.EventHandler(this.frmWaveContainer_Enter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmWaveContainer_MouseMove);
            this.Resize += new System.EventHandler(this.frmWaveContainer_Resize);
            this.m_Menu.ResumeLayout(false);
            this.m_Menu.PerformLayout();
            this.m_Panel.ResumeLayout(false);
            this.m_StatusStrip.ResumeLayout(false);
            this.m_StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.HScrollBar m_ScrollBar;
        private System.Windows.Forms.MenuStrip m_Menu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileInContainerToolStripMenuItem;
        private ctrlWaveChannel ctrlWaveChannel1;
        private System.Windows.Forms.ToolStripMenuItem addMarkerTrackToolStripMenuItem;
        private System.Windows.Forms.Panel m_Panel;
        private System.Windows.Forms.Timer m_PlaybackTimer;
        private System.Windows.Forms.ToolStripMenuItem saveFilesToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog m_OpenFileDialog;
        private System.Windows.Forms.ToolStripStatusLabel m_Status;
        private System.Windows.Forms.StatusStrip m_StatusStrip;

    }
}