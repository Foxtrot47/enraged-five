using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BlueWave.Script;

namespace BlueWave
{
    public partial class frmMain : Form, IBlueWave
    {
        ScriptHost m_Script;

        public frmMain(string[] args)
        {
            InitializeComponent();
            WavePlayback.Init(this);

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-script")
                {
                    m_Script = new ScriptHost(args[++i]);
                    m_Script.Init(this, args);
                    break;
                }
            }
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void tileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void tileVerticallyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void newContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewContainer("Container " + (MdiChildren.Length+1));
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        public IWaveContainer NewContainer(string title)
        {
            frmWaveContainer container = new frmWaveContainer(title);
            container.MdiParent = this;
            container.Show();
            return container;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (m_Script != null)
            {
                m_Script.Shutdown();
            }
        }
    }
}