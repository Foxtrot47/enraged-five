using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using BlueWave.Script;
using Wavelib;
using Gesturelib;
using System.IO;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace BlueWave
{
    public partial class frmWaveContainer : Form, IWaveContainer
    {
        ObservableCollection<bwGestureFile> m_GestureFiles;
        bwWaveFileList m_WaveFiles;
        List<WavePlayback> m_WavePlayback;

        WavePlayback m_PlayingWave;
        int m_LastPlayPosition;
        
        public bwWaveFile CursorWave;
        public bwGestureFile CursorGesture;
        public int CursorPosition;

        public int XOffset = 120;
        public Brush InactiveBrush;
        public Brush InactiveTextBrush;
        public SolidBrush WaveBackgroundBrush;
        public SolidBrush WaveNameBrush;
        
        public Font WaveNameFont;
        public Pen WaveOutlinePen;
        public Pen PlaybackPositionPen;
        public Pen CursorPen;
        public Pen RulerPen;

        public event OnMarkerHitDelegate OnMarkerHit;
        public event OnWaveEventDelegate OnWaveSave;
        public event OnWaveEventDelegate OnWaveOpen;
        public event OnCloseDelegate OnClose;
        public event OnKeyPressDelegate OnProcessKey;

        const int WAVE_FILE_GAP = 100000;

        float m_ZoomRatio = 100.0f;
        int m_VisibleStart, m_VisibleEnd;

       
        public bwWaveFile PlayingWave
        {
            get
            {
                return (m_PlayingWave==null?null:m_WaveFiles[m_WavePlayback.IndexOf(m_PlayingWave)]);
            }
        }

        public int PlayPosition
        {
            get
            {
                bwWaveFile wave = PlayingWave;
                int playPositionSamples = m_PlayingWave.PlayPosition / (wave.Format.NumChannels * wave.Format.BlockAlign); 
                return playPositionSamples;
            }
        }

        public uint TotalLength
        {
            get
            {
                return ((uint)m_WaveFiles.Count * WAVE_FILE_GAP) + m_WaveFiles.TotalLength;
            }
        }

        public frmWaveContainer(string title)
        {
            m_WaveFiles = new bwWaveFileList();
            m_WavePlayback = new List<WavePlayback>();
            m_GestureFiles = new ObservableCollection<bwGestureFile>();

            InitializeComponent();
            Text = title;
          
            RectangleF rect = new RectangleF(0, 0, XOffset, Height);
            InactiveBrush = new LinearGradientBrush(rect, Color.DarkGray, Color.DarkGray, 0, false);
            ((LinearGradientBrush)InactiveBrush).SetSigmaBellShape(0.1f, 0.5f);
            InactiveTextBrush = new SolidBrush(Color.White);

            WaveBackgroundBrush = new SolidBrush(Color.WhiteSmoke);
            WaveOutlinePen = new Pen(Color.Black);
            WaveNameBrush = new SolidBrush(Color.DarkBlue);
            WaveNameFont = new Font("Tahoma", 8.0f, FontStyle.Bold);
            
            PlaybackPositionPen = new Pen(Color.Crimson,2.0f);
            
            CursorPen = new Pen(Color.Blue);
            CursorPen.DashStyle = DashStyle.Dash;

            RulerPen = new Pen(Color.Black,1.0f);
        }

        public float ZoomRatio
        {
            get { return m_ZoomRatio; }

            set
            {
                if(value<10.0f)
                {
                    m_ZoomRatio = 10.0f;
                }
                else if(value>1000.0f)
                {
                    m_ZoomRatio = 1000.0f;
                }
                else{
                    m_ZoomRatio = value;
                }
                ComputeScrollBarRange();
                ComputeVisibleArea();
                Refresh();
            }
        }
        #region IWaveContainer Members

        public bwWaveFileList GetWaveFiles()
        {
            return m_WaveFiles;
        }

        public ObservableCollection<bwGestureFile> GetGestureFiles()
        {
            return m_GestureFiles;
        }

        public void GetVisibleRegion(out int start, out int end)
        {
            start = m_VisibleStart;
            end = m_VisibleEnd;
        }

        #endregion

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void openFileInContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (string fileName in m_OpenFileDialog.FileNames)
                {
                    AddFile(fileName);
                }
            }
        }

        public void AddFile(string fileName)
        {
            bwWaveFile waveFile = new bwWaveFile(fileName);
            bwGestureSerializer ser = new bwGestureSerializer();
            bwGestureFile gestFile = ser.DeSerializeObject(Path.Combine(Path.GetDirectoryName(fileName), Path.GetFileNameWithoutExtension(fileName) + ".gesture"));

            if (OnWaveOpen != null)
            {
                OnWaveOpen(gestFile, waveFile);
            }

            m_GestureFiles.Add(gestFile);
            foreach (bwGestureMarker marker in gestFile.Markers)
            {
                marker.Clip.SampleRate = waveFile.Format.SampleRate;
            }

            m_WaveFiles.Add(waveFile);
            m_WavePlayback.Add(new WavePlayback(waveFile));

            ComputeScrollBarRange();
            ComputeVisibleArea();
            Invalidate(true);
        }

        public delegate void WaveFileRenderCallback(Graphics g, bwWaveFile wave, bwGestureFile gesture, uint visibleStart, uint visibleEnd, uint startX, uint endX);

        public void RenderVisibleWaveFiles(WaveFileRenderCallback cb, Graphics g)
        {
            uint startX, endX;
            uint current = 0;
            int visibleStart, visibleEnd;

            GetVisibleRegion(out visibleStart, out visibleEnd);

            for(int i=0; i < m_WaveFiles.Count; ++i)
            {
                bwWaveFile wave = m_WaveFiles[i];

                uint waveStart = current;
                uint waveEnd = current + wave.Data.NumSamples;
                if (waveStart <= visibleEnd && waveEnd >= visibleStart)
                {
                    // this file is atleast partially visible
                    uint waveVisibleStart;
                    if (waveStart >= visibleStart)
                    {
                        // the start of the wave is visible
                        waveVisibleStart = 0;
                        startX = (uint)((current - visibleStart)/m_ZoomRatio);
                    }
                    else
                    {
                        waveVisibleStart = (uint)visibleStart - waveStart;
                        startX = 0;
                    }
                    uint waveVisibleEnd;
                    if (waveEnd <= visibleEnd)
                    {
                        // the end of the wave is visible
                        waveVisibleEnd = wave.Data.NumSamples;
                        endX = startX + (uint)(((waveVisibleEnd-waveVisibleStart) / m_ZoomRatio));
                    }
                    else
                    {
                        waveVisibleEnd = wave.Data.NumSamples - (waveEnd - (uint)visibleEnd);
                        endX = (uint)Width;
                    }

                    startX += (uint)XOffset;
                    endX += (uint)XOffset;
                    cb(g, wave, m_GestureFiles[i], waveVisibleStart, waveVisibleEnd, startX, endX);
                    //System.Diagnostics.Debug.Print(String.Format("{0}", startX));
                }
                current += wave.Data.NumSamples + WAVE_FILE_GAP;
            }
        }

        private void frmWaveContainer_Resize(object sender, EventArgs e)
        {
            ComputeScrollBarRange();
            ComputeVisibleArea();
        }
     
        private void m_ScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            ComputeVisibleArea();
        }

        private void ComputeScrollBarRange()
        {
            // set up the scroll bar to scroll from sample 0 to the last sample - (width in samples)
            m_ScrollBar.Minimum = 0;
            m_ScrollBar.Maximum = (int)TotalLength - ConvertPixelsToSamples(Width-XOffset);
            m_ScrollBar.SmallChange = (int)(ZoomRatio * 15.0f);
            m_ScrollBar.LargeChange = (int)(ZoomRatio * 50.0f);

        }

        private void ComputeVisibleArea()
        {
            if (m_ScrollBar.Value<0){
                m_VisibleStart = 0;
            }
            else{
                m_VisibleStart = m_ScrollBar.Value;
            }
           
            m_VisibleEnd = m_VisibleStart + ConvertPixelsToSamples(Width);
            Invalidate(true);

            Text = m_VisibleStart.ToString() + " - " + m_VisibleEnd.ToString();
        }

        private void ctrlWaveChannel1_Load(object sender, EventArgs e)
        {

        }

        private void addMarkerTrackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string prefix = frmSimpleInput.GetInput("Marker track prefix");
            if (prefix == string.Empty)
            {
                prefix = null;
            }

            AddMarkerTrack(prefix, null);
        }

        public IMarkerTrack AddMarkerTrack(string prefix, string caption)
        {
            Splitter splitter = new Splitter();
            splitter.Dock = DockStyle.Top;

            m_Panel.Controls.Add(splitter);
            ctrlMarkerTrack track = new ctrlMarkerTrack(this, prefix);
            track.Dock = DockStyle.Top;
            track.SetCaption(caption);
            m_Panel.Controls.Add(track);

            Invalidate(true);

            return track;
        }

        // PURPOSE
        //  Returns the wave file that contains the specified global sample offset, and the appropriate offset
        //  into that wave file.
        // NOTES
        //  Returns wave = null if no hit
        public void ComputeWaveAndOffset(int sample, out bwWaveFile wave, out bwGestureFile gesture, out int offset)
        {
            int current = 0;
            wave = null;
            offset = 0;
            gesture = null;

            for(int i=0; i < m_WaveFiles.Count; ++i)
            {
                int waveStart = current;
                int waveEnd = current + (int)m_WaveFiles[i].Data.NumSamples + WAVE_FILE_GAP;

                if (sample >= current && sample <= waveEnd)
                {
                    wave = m_WaveFiles[i];
                    offset = sample - current;
                    gesture = m_GestureFiles[i];
                    return;
                }

                current += (int)m_WaveFiles[i].Data.NumSamples + WAVE_FILE_GAP;
            }
        }

        public int ConvertSamplesToPixels(int samples)
        {
            return (int)(samples / ZoomRatio);
        }

        public int ConvertPixelsToSamples(int pixels)
        {
            return (int)(pixels * ZoomRatio);
        }

        public int ConvertXToSamples(int x)
        {
            return (m_VisibleStart + ConvertPixelsToSamples(Math.Max(0,x-XOffset)));
        }

        public int ConvertSamplesToX(int samples)
        {
            return XOffset + (ConvertSamplesToPixels(samples) - m_VisibleStart);
        }

        public void SetStatus(string status)
        {
            m_Status.Text = status;
        }

        private void frmWaveContainer_MouseMove(object sender, MouseEventArgs e)
        {
            m_Status.Text = string.Format("{0}: {1}", e.X, ConvertXToSamples(e.X).ToString());
        }

        private void ctrlWaveChannel1_KeyDown(object sender, KeyEventArgs e)
        {
            if(OnProcessKey !=null && m_PlayingWave!=null)
            {
                OnProcessKey(Math.Max(0,PlayPosition - (int)PlayingWave.ConvertMsToSamples(80)), PlayingWave, e);
            }   
        }

        private List<bwGestureMarker> playedGestures = new List<bwGestureMarker>();
        private void ctrlWaveChannel1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int samples = ConvertXToSamples(e.X);
            bwWaveFile wave;
            bwGestureFile gesture;
            int offset;
            ComputeWaveAndOffset(samples, out wave, out gesture, out offset);
            if (wave != null)
            {
                if (m_PlayingWave == null)
                {
                    m_PlayingWave = m_WavePlayback[m_WaveFiles.IndexOf(wave)];
                    //click off the wave will play from the start
                    if (CursorWave == wave && offset != 0)
                    {                     
                        m_LastPlayPosition = m_PlayingWave.PlayPosition = (int)CursorPosition;
                    }
                    else
                    {
                        m_LastPlayPosition = -1;
                        m_PlayingWave.PlayPosition = 0;
                    }
                    m_PlayingWave.PlayPosition = (int)offset;
                    m_PlayingWave.Play();
                    playedGestures.Clear();
                    m_PlaybackTimer.Enabled = true;
                }
            }
        }

        private void m_PlaybackTimer_Tick(object sender, EventArgs e)
        {
            if (!m_PlayingWave.IsPlaying)
            {
                m_PlayingWave = null;
                m_PlaybackTimer.Enabled = false;
                Invalidate(true);
            }
            else
            {
                bwWaveFile wave = m_WaveFiles[m_WavePlayback.IndexOf(m_PlayingWave)];
                bwGestureFile gesture = m_GestureFiles[m_WavePlayback.IndexOf(m_PlayingWave)];
                int playPosition = PlayPosition;
                
                foreach (bwGestureMarker marker in gesture.Markers)
                {
                    if (playedGestures.Contains(marker))
                    {
                        continue;
                    }

                    if (marker.CropPosition >= m_LastPlayPosition && marker.CropPosition <= playPosition)
                    {
                        playedGestures.Add(marker);
                        if (OnMarkerHit != null)
                        {
                            System.Diagnostics.Debug.Print("Hit Marker {0}", playPosition);
                            OnMarkerHit(marker, wave);
                        }
                    }
                }

                m_LastPlayPosition = playPosition;
                Invalidate(true);
            }
        }

        private void frmWaveContainer_SizeChanged(object sender, EventArgs e)
        {
            Invalidate(true);
        }

        public void GetCursor(out bwWaveFile wave, out bwGestureFile gesture, out int offset)
        {
            wave = CursorWave;
            gesture = CursorGesture;
            offset = CursorPosition;
        }

        public override void Refresh()
        {
            Invalidate(true);
        }


        public void ComputeRegionAtSample(int lastMouseSample, out bwWaveFile wave, out bwGestureMarker region, out int offset)
        {
            wave = null;
            region = null;
            offset = 0;
            int currentSample = 0;

            for (int i = 0; i < m_GestureFiles.Count; ++i)
            {
                for (int j = 0; j < m_GestureFiles[i].Markers.Count; ++j)
                {
                    bwGestureMarker r = m_GestureFiles[i].Markers[j];
                    if (r != null)
                    {
                        if (lastMouseSample >= currentSample + r.Position && lastMouseSample <= currentSample + r.Position + r.Length)
                        {
                            wave = m_WaveFiles[i];
                            region = r;
                            // note that offset is relative to the wave, not the marker
                            offset = lastMouseSample - currentSample;
                        }
                    }
                }

                currentSample += (int)m_WaveFiles[i].Data.NumSamples;
                currentSample += WAVE_FILE_GAP;
            }
        }

        public int ComputeWaveOffset(bwWaveFile wave)
        {
            int currentSample = 0;
            foreach (bwWaveFile w in m_WaveFiles)
            {
                if (wave == w)
                {
                    return currentSample;
                }
                currentSample += (int)w.Data.NumSamples;
                currentSample += WAVE_FILE_GAP;
            }

            return 0;
        }

        private void saveFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < m_WaveFiles.Count; i++)
            {
                m_WaveFiles[i].Save(@"c:\test" + i.ToString() + ".wav");
            }

            for (int i = 0; i < m_GestureFiles.Count; i++)
            {
                if (OnWaveSave != null)
                {
                    OnWaveSave(m_GestureFiles[i], m_WaveFiles[i]);
                }

                bwGestureSerializer ser = new bwGestureSerializer();
                ser.SerializeObject(m_GestureFiles[i]);
            }
        }

        private void frmWaveContainer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (OnClose != null)
            {
                OnClose();
            }
        }

        private void ctrlWaveChannel1_MouseClick(object sender, MouseEventArgs e)
        {
            if (m_PlayingWave != null && m_PlayingWave.IsPlaying)
            {
                m_PlayingWave.Stop();
            }
        }

        private void frmWaveContainer_Enter(object sender, EventArgs e)
        {
            Refresh();
            Invalidate(true);
        }

        private void ctrlWaveChannel1_Play(object sender, EventArgs e)
        {
            ctrlWaveChannel1_MouseClick(null, null);

            int samples = ConvertXToSamples(0);
            bwWaveFile wave;
            bwGestureFile gesture;
            int offset;
            ComputeWaveAndOffset(samples, out wave, out gesture, out offset);
            if (wave == null)
            {
                samples += (int)WAVE_FILE_GAP;
                ComputeWaveAndOffset(samples, out wave, out gesture, out offset);
            }

            if (wave != null)
            {
                if (m_PlayingWave == null)
                {
                    m_PlayingWave = m_WavePlayback[m_WaveFiles.IndexOf(wave)];

                    m_PlayingWave.PlayPosition = 0;
                    m_LastPlayPosition = -1;
                    m_PlayingWave.Play();
                    playedGestures.Clear();
                    m_PlaybackTimer.Enabled = true;
                }
            }
        }
    }
}