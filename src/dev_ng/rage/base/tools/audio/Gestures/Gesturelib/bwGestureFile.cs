﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="bwGestureFile.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The bw gesture serializer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gesturelib
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.IO;
    using System.Xml.Linq;

    /// <summary>
    /// The bw gesture serializer.
    /// </summary>
    public class bwGestureSerializer
    {
        #region Public Methods and Operators

        /// <summary>
        /// Deserialize the object.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <returns>
        /// The deserialized object <see cref="bwGestureFile"/>.
        /// </returns>
        public bwGestureFile DeSerializeObject(string filename)
        {
            if (!File.Exists(filename))
            {
                return new bwGestureFile(filename);
            }

            var gestureFile = new bwGestureFile();

            // Load and parse file
            var doc = XDocument.Load(filename);
            var fileElem = doc.Element("bwGestureFile");
            var fileNameElem = fileElem.Element("FileName");
            var markersElem = fileElem.Element("Markers");

            // Set file name
            gestureFile.FileName = fileNameElem.Value;

            // Create markers
            foreach (var elem in markersElem.Elements("Item"))
            {
                deserializeGestureMarker(elem, gestureFile);
            }

            foreach (var elem in markersElem.Elements("bwGestureMarker"))
            {
                deserializeGestureMarker(elem, gestureFile);
            }

            return gestureFile;
        }

        private void deserializeGestureMarker(XElement elem, bwGestureFile gestureFile)
        {
             var positionElem = elem.Element("Position");
                var nameElem = elem.Element("Name");
                var dirElem = elem.Element("Dir");
                var lengthElem = elem.Element("Length");
                var clipElem = elem.Element("Clip");

                int position;
                if (!int.TryParse(positionElem.Value, out position))
                {
                    throw new Exception("Failed to parse <Position> value: " + positionElem.Value);
                }

                uint length;
                if (!uint.TryParse(lengthElem.Value, out length))
                {
                    throw new Exception("Failed to parse <Length> value: " + lengthElem.Value);
                }

                // Create clip
                var startFrameElem = clipElem.Element("StartFrame");
                var endFrameElem = clipElem.Element("EndFrame");
                var rateElem = clipElem.Element("Rate");
                var maxWeightElem = clipElem.Element("MaxWeight");
                var blendInTimeElem = clipElem.Element("BlendInTime");
                var blendOutTimeElem = clipElem.Element("BlendOutTime");

                float startFrame, endFrame, rate, maxWeight, blendInTime, blendOutTime;
                if (!float.TryParse(startFrameElem.Value, out startFrame)
                    || !float.TryParse(endFrameElem.Value, out endFrame)
                    || !float.TryParse(rateElem.Value, out rate)
                    || !float.TryParse(maxWeightElem.Value, out maxWeight)
                    || !float.TryParse(blendInTimeElem.Value, out blendInTime)
                    || !float.TryParse(blendOutTimeElem.Value, out blendOutTime))
                {
                    throw new Exception("Failed to parse Gesture Clip data");
                }

                var clip = new bwGestureClip(startFrame, endFrame, rate, maxWeight, blendInTime, blendOutTime);

                // Create marker object and add to gesture file
                var marker = new bwGestureMarker(nameElem.Value, position, dirElem.Value, length, clip);

                if (clip.StartFrame != 0.0f)
                {
                    var crop = (int)(length * clip.StartFrame);
                    marker.Position -= crop;
                }

                gestureFile.Markers.Add(marker);
        }

        /// <summary>
        /// Serialize an object.
        /// </summary>
        /// <param name="gestureFileObj">
        /// The gesture file object to serialize.
        /// </param>
        public void SerializeObject(bwGestureFile gestureFileObj)
        {
            // Setup document and root
            var doc = new XDocument();
            var fileElem = new XElement("bwGestureFile");
            doc.Add(fileElem);

            // Add file name element
            var fileNameElem = new XElement("FileName") { Value = gestureFileObj.FileName };
            fileElem.Add(fileNameElem);

            // Add markers element
            var markersElem = new XElement("Markers");
            fileElem.Add(markersElem);

            // Add marker elements
            foreach (var marker in gestureFileObj.Markers)
            {
                var markerElem = new XElement("Item");
                markersElem.Add(markerElem);

                var positionElem = new XElement("Position");
                positionElem.Value = marker.Clip.StartFrame != 0.0f ? 
                    marker.CropPosition.ToString() : marker.Position.ToString();
                markerElem.Add(positionElem);

                var nameElem = new XElement("Name") { Value = marker.Name };
                markerElem.Add(nameElem);

                var dirElem = new XElement("Dir") { Value = marker.Dir };
                markerElem.Add(dirElem);

                var lengthElem = new XElement("Length") { Value = marker.Length.ToString() };
                markerElem.Add(lengthElem);

                // Add clip element
                var clipElem = new XElement("Clip");
                markerElem.Add(clipElem);

                var startFrameElem = new XElement("StartFrame") { Value = marker.Clip.StartFrame.ToString() };
                clipElem.Add(startFrameElem);

                var endFrameElem = new XElement("EndFrame") { Value = marker.Clip.EndFrame.ToString() };
                clipElem.Add(endFrameElem);

                var rateElem = new XElement("Rate") { Value = marker.Clip.Rate.ToString() };
                clipElem.Add(rateElem);

                var maxWeightElem = new XElement("MaxWeight") { Value = marker.Clip.MaxWeight.ToString() };
                clipElem.Add(maxWeightElem);

                var blendInTimeElem = new XElement("BlendInTime") { Value = marker.Clip.BlendInTime.ToString() };
                clipElem.Add(blendInTimeElem);

                var blendOutTimeElem = new XElement("BlendOutTime") { Value = marker.Clip.BlendOutTime.ToString() };
                clipElem.Add(blendOutTimeElem);
            }

            doc.Save(gestureFileObj.FileName);
        }

        #endregion
    }

    /// <summary>
    /// The bw gesture file.
    /// </summary>
    public class bwGestureFile
    {
        #region Fields

        /// <summary>
        /// The file name.
        /// </summary>
        private string m_FileName = string.Empty;

        /// <summary>
        /// The markers.
        /// </summary>
        private ObservableCollection<bwGestureMarker> m_Markers = new ObservableCollection<bwGestureMarker>();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="bwGestureFile"/> class.
        /// </summary>
        public bwGestureFile()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="bwGestureFile"/> class.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        public bwGestureFile(string fileName)
        {
            this.m_FileName = fileName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="bwGestureFile"/> class.
        /// </summary>
        /// <param name="markers">
        /// The markers.
        /// </param>
        public bwGestureFile(ObservableCollection<bwGestureMarker> markers)
        {
            this.m_Markers = markers;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        public string FileName
        {
            get
            {
                return this.m_FileName;
            }

            set
            {
                this.m_FileName = value;
            }
        }

        /// <summary>
        /// Gets or sets the markers.
        /// </summary>
        public ObservableCollection<bwGestureMarker> Markers
        {
            get
            {
                return this.m_Markers;
            }

            set
            {
                this.m_Markers = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The setup clips.
        /// </summary>
        public void SetupClips()
        {
            foreach (var marker in this.Markers)
            {
                marker.Clip.Tag = marker;
            }
        }

        #endregion
    }

    /// <summary>
    /// The bw gesture marker.
    /// </summary>
    public class bwGestureMarker
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="bwGestureMarker"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        /// <param name="dir">
        /// The directory.
        /// </param>
        /// <param name="length">
        /// The length.
        /// </param>
        /// <param name="clip">
        /// The clip.
        /// </param>
        public bwGestureMarker(string name, int position, string dir, uint length, bwGestureClip clip = null)
        {
            this.Name = name;
            this.Position = position;
            this.Dir = dir;
            this.Length = length;
            this.Clip = clip ?? new bwGestureClip(this);
            this.Clip.Tag = this;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="bwGestureMarker"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        /// <param name="length">
        /// The length.
        /// </param>
        public bwGestureMarker(string name, int position, uint length)
            : this(name, position, null, length)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="bwGestureMarker"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        public bwGestureMarker(string name, int position)
            : this(name, position, 0)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the clip.
        /// </summary>
        public bwGestureClip Clip { get; set; }

        /// <summary>
        /// Gets the crop length.
        /// </summary>
        public uint CropLength
        {
            get
            {
                var cropStart = (uint)(this.Length * this.Clip.StartFrame);
                var cropEnd = (uint)(this.Length * this.Clip.EndFrame);
                return cropEnd - cropStart;
            }
        }

        /// <summary>
        /// Gets the crop position.
        /// </summary>
        public int CropPosition
        {
            get
            {
                return this.Position + (int)(this.Clip.StartFrame * this.Length);
            }
        }

        /// <summary>
        /// Gets or sets the directory.
        /// </summary>
        public string Dir { get; set; }

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        public uint Length { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public int Position { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }

    /// <summary>
    /// The bw gesture clip.
    /// </summary>
    public class bwGestureClip : INotifyPropertyChanged
    {
        #region Fields

        /// <summary>
        /// The m_b rate change.
        /// </summary>
        public bool m_bRateChange = false;

        /// <summary>
        /// The m_ blend in time.
        /// </summary>
        private float m_BlendInTime = 0.25f;

        /// <summary>
        /// The m_ blend out time.
        /// </summary>
        private float m_BlendOutTime = 0.5f;

        /// <summary>
        /// The m_ end frame.
        /// </summary>
        private float m_EndFrame = 1.0f;

        /// <summary>
        /// The m_ max weight.
        /// </summary>
        private float m_MaxWeight = 1;

        /// <summary>
        /// The m_ rate.
        /// </summary>
        private float m_Rate = 1;

        /// <summary>
        /// The m_ start frame.
        /// </summary>
        private float m_StartFrame;

        /// <summary>
        /// The m_ tag.
        /// </summary>
        private object m_Tag;

        /// <summary>
        /// The m_sample rate.
        /// </summary>
        private float m_sampleRate;

        /// <summary>
        /// The marker.
        /// </summary>
        private bwGestureMarker marker;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="bwGestureClip"/> class.
        /// </summary>
        /// <param name="startframe">
        /// The start frame.
        /// </param>
        /// <param name="endframe">
        /// The end frame.
        /// </param>
        /// <param name="rate">
        /// The rate.
        /// </param>
        /// <param name="maxWeight">
        /// The max weight.
        /// </param>
        /// <param name="blendInTime">
        /// The blend-in time.
        /// </param>
        /// <param name="blendOutTime">
        /// The blend-out time.
        /// </param>
        public bwGestureClip(
            float startframe, float endframe, float rate, float maxWeight, float blendInTime, float blendOutTime)
        {
            this.m_StartFrame = startframe;
            this.m_EndFrame = endframe;
            this.m_Rate = rate;
            this.m_MaxWeight = maxWeight;
            this.m_BlendInTime = blendInTime;
            this.m_BlendOutTime = blendOutTime;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="bwGestureClip"/> class.
        /// </summary>
        /// <param name="tag">
        /// The tag.
        /// </param>
        public bwGestureClip(bwGestureMarker tag)
        {
            this.Tag = tag;
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the blend in time.
        /// </summary>
        [DisplayName("Blend-In Time")]
        public float BlendInTime
        {
            get
            {
                return this.m_BlendInTime;
            }

            set
            {
                this.m_BlendInTime = Math.Max(value, 0);
                float blendInOffset = this.marker.CropPosition + (this.BlendInTime * this.SampleRate);
                int endPoint = this.marker.CropPosition + (int)this.marker.CropLength;
                if (blendInOffset > endPoint)
                {
                    float diff = blendInOffset - endPoint;
                    diff /= this.SampleRate;
                    this.m_BlendInTime -= diff;
                }

                this.OnPropertyChanged("BlendInTime");
            }
        }

        /// <summary>
        /// Gets or sets the blend out time.
        /// </summary>
        [DisplayName("Blend-Out Time")]
        public float BlendOutTime
        {
            get
            {
                return this.m_BlendOutTime;
            }

            set
            {
                this.m_BlendOutTime = Math.Max(value, 0);

                // Mark sure it is within the bounds
                float blendOutOffset = this.marker.CropPosition + this.marker.CropLength
                                       - (this.BlendOutTime * this.SampleRate);
                if (blendOutOffset < this.marker.CropPosition)
                {
                    float diff = blendOutOffset - this.marker.CropPosition;
                    diff /= this.SampleRate;
                    this.m_BlendOutTime += diff;
                }

                this.OnPropertyChanged("BlendOutTime");
            }
        }

        /// <summary>
        /// Gets or sets the end frame.
        /// </summary>
        [DisplayName("Normalised Trim End")]
        public float EndFrame
        {
            get
            {
                return this.m_EndFrame;
            }

            set
            {
                this.m_EndFrame = Math.Max(0.0f, Math.Min(value, 1.0f));

                float blendInLength = this.marker.Clip.BlendInTime * this.marker.Clip.SampleRate;
                float blendOutLength = this.marker.Clip.BlendOutTime * this.marker.Clip.SampleRate;
                if (this.marker.CropLength < blendInLength || this.marker.CropLength < blendOutLength)
                {
                    var newCropLength = (int)Math.Max(blendInLength, blendOutLength);
                    this.m_EndFrame = (newCropLength + this.StartFrame) / this.marker.Length;
                }

                this.OnPropertyChanged("EndFrame");
            }
        }

        /// <summary>
        /// Gets or sets the max weight.
        /// </summary>
        [DisplayName("Max Weight")]
        public float MaxWeight
        {
            get
            {
                return this.m_MaxWeight;
            }

            set
            {
                this.m_MaxWeight = Math.Min(Math.Max(value, 0), 1);
                this.OnPropertyChanged("MaxWeight");
            }
        }

        /// <summary>
        /// Gets or sets the rate.
        /// </summary>
        public float Rate
        {
            get
            {
                return this.m_Rate;
            }

            set
            {
                this.m_Rate = Math.Max(value, 0);
                this.OnPropertyChanged("Rate");
            }
        }

        /// <summary>
        /// Gets or sets the sample rate.
        /// </summary>
        public float SampleRate
        {
            get
            {
                return this.m_sampleRate;
            }

            set
            {
                this.m_sampleRate = value;

                // Make sure the blend in/out are clamped.
                float blendInOffset = this.marker.CropPosition + (this.BlendInTime * this.SampleRate);
                if (blendInOffset > this.marker.CropPosition + this.marker.CropLength)
                {
                }

                float blendOutOffset = this.marker.CropPosition + this.marker.CropLength
                                       - (this.BlendOutTime * this.SampleRate);
                if (blendOutOffset < this.marker.CropPosition)
                {
                    float diff = blendOutOffset - this.marker.CropPosition;
                    diff /= this.SampleRate;
                    this.BlendOutTime += diff;
                }
            }
        }

        /// <summary>
        /// Gets or sets the start frame.
        /// </summary>
        [DisplayName("Normalised Trim Start")]
        public float StartFrame
        {
            get
            {
                return this.m_StartFrame;
            }

            set
            {
                this.m_StartFrame = Math.Min(Math.Max(value, 0.0f), 1.0f);
                float blendInLength = this.marker.Clip.BlendInTime * this.marker.Clip.SampleRate;
                float blendOutLength = this.marker.Clip.BlendOutTime * this.marker.Clip.SampleRate;
                if (this.marker.CropLength < blendInLength || this.marker.CropLength < blendOutLength)
                {
                    var newCropLength = (int)Math.Max(blendInLength, blendOutLength);
                    this.m_StartFrame = this.EndFrame - (newCropLength / (float)this.marker.Length);
                }

                this.OnPropertyChanged("StartFrame");
            }
        }

        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        public object Tag
        {
            get
            {
                return this.m_Tag;
            }

            set
            {
                this.m_Tag = value;
                this.marker = this.m_Tag as bwGestureMarker;
            }
        }

        /// <summary>
        /// Gets the tag length.
        /// </summary>
        [DisplayName("Sample Length")]
        [ReadOnly(true)]
        public float TagLength
        {
            get
            {
                return (this.Tag as bwGestureMarker).Length;
            }
        }

        /// <summary>
        /// Gets the tag position.
        /// </summary>
        [DisplayName("Sample Position")]
        [ReadOnly(true)]
        public float TagPosition
        {
            get
            {
                return (this.Tag as bwGestureMarker).Position;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on property changed.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null)
            {
                return;
            }

            handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}