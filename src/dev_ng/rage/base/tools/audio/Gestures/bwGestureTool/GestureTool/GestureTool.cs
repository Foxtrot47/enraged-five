using System;
using System.Collections.Generic;
using System.Text;

using BlueWave.Script;

namespace GestureTool
{
    public class GestureTool : IBlueWaveScript
    {
        frmGestureTool m_Form;
        public void Init(IBlueWave app, string[] arguments)
        {
            m_Form = new frmGestureTool(app, arguments);
            m_Form.MdiParent = (System.Windows.Forms.Form)app;
            m_Form.Show();
        }

        public void Shutdown()
        {
            m_Form.Close();
        }

    }
}
