using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;


using BlueWave.Script;
using Wavelib;
using Gesturelib;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace GestureTool
{
    
    /// <summary>
    /// Summary description for frmGestureTool.
    /// </summary>
    public class frmGestureTool : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Button btnFS;
        private System.Windows.Forms.ComboBox m_MarkerNamesFS;

        private IBlueWave m_App;
        private IWaveContainer m_Container;

        private TcpClient m_TcpClient;

        Hashtable m_Directories;
        Hashtable m_FacialDirectories;
        //string m_FDirectoryName = "facialGestures";
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private ComboBox m_DirectoryListS;
        private Button btnGS;
        private ComboBox m_MarkerNamesGS;
        private GroupBox groupBox3;
        private ComboBox m_DirectoryListL;
        private Button btnGL;
        private ComboBox m_MarkerNamesGL;
        private ArrayList m_GenericAnimationList;
        IMarkerTrack m_GSTrack;
        IMarkerTrack m_GLTrack;
        IMarkerTrack m_FSTrack;
        private ComboBox m_MarkerNamesFL;
        private Button btnFL;
        private GroupBox groupBox4;
        private GroupBox groupBox5;
        private Button btnPopulateMarkers;
        private Button btnMarkersPopulate;
        private ListBox lbMarkers;
        private GroupBox groupBox6;
        private PropertyGrid propertyGrid1;
        private ComboBox m_DirectoryListFS;
        private ComboBox m_DirectoryListFL;
        IMarkerTrack m_FLTrack;

        public frmGestureTool(IBlueWave app, string[] arguments)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            m_Container = app.NewContainer("Gesture Tool");
            m_Container.OnWaveOpen += new OnWaveEventDelegate(m_Container_OnWaveOpen);
            m_Container.OnWaveSave += new OnWaveEventDelegate(m_Container_OnWaveSave);
            m_Container.OnMarkerHit += new OnMarkerHitDelegate(m_Container_OnMarkerHit);
            m_Container.OnClose += new OnCloseDelegate(m_Container_OnClose);

            m_GLTrack = m_Container.AddMarkerTrack("G_L:", "Gesture Listener");
            m_FLTrack = m_Container.AddMarkerTrack("F_L:", "Facial Listener");
            m_GSTrack = m_Container.AddMarkerTrack("G_S:", "Gesture Speaker");
            m_FSTrack = m_Container.AddMarkerTrack("F_S:", "Facial Speaker");

            m_GSTrack.OnContextMenuPopup += new OnContextMenuPopupDelegate(onContextMenuPopup);
            m_GLTrack.OnContextMenuPopup += new OnContextMenuPopupDelegate(onContextMenuPopup);
            m_FLTrack.OnContextMenuPopup += new OnContextMenuPopupDelegate(onContextMenuPopup);
            m_FSTrack.OnContextMenuPopup += new OnContextMenuPopupDelegate(onContextMenuPopup);

            m_GSTrack.OnAnimationDefaultDuration += new FindAnimationDefaultDurationDelegate(FindAnimLength);
            m_GLTrack.OnAnimationDefaultDuration += new FindAnimationDefaultDurationDelegate(FindAnimLength);
            m_FLTrack.OnAnimationDefaultDuration += new FindAnimationDefaultDurationDelegate(FindAnimLength);
            m_FSTrack.OnAnimationDefaultDuration += new FindAnimationDefaultDurationDelegate(FindAnimLength);

            string host = "alastair-magrg.rockstarnorth.com";

            for (int i = 0; i < arguments.Length; i++)
            {
                if (arguments[i] == "-files")
                {
                    for (int j = i + 1; j < arguments.Length; j++)
                    {
                        string fileName = arguments[j];
                        m_Container.AddFile(fileName);
                    }
                }
                if (arguments[i] == "-host")
                {
                    host = arguments[++i];
                }
            }

            m_App = app;

            m_Directories = new Hashtable();
            m_FacialDirectories = new Hashtable();
            m_GenericAnimationList = new ArrayList();
           
            if (host != "test")
            {
                try
                {
                    m_TcpClient = new TcpClient(host, 7565);

                    byte[] msg = System.Text.Encoding.ASCII.GetBytes("!INIT!\n");
                    m_TcpClient.GetStream().Write(msg, 0, msg.Length);

                    StringBuilder builder = new StringBuilder();

                    // read and append all the data into a buffer so we don't get any fragmented messages.
                    bool finished = false;
                    while (!finished)
                    {
                        byte[] buf = new byte[256 * 1024];
                        int len = m_TcpClient.GetStream().Read(buf, 0, buf.Length);
                        // len-1 to strip off trailing \n
                        string msgbuf = System.Text.Encoding.ASCII.GetString(buf, 0, len);

                        builder.Append(msgbuf);

                        if (msgbuf == null || msgbuf.TrimEnd().EndsWith("END"))
                        {
                            finished = true;
                            continue;
                        }
                    }

                    // split and process all messages
                    string[] messages = builder.ToString().TrimEnd().Split('\n');
                    Array.Sort(messages);

                    foreach (string str in messages)
                    {
                        string[] tmp = str.ToUpper().Split(':');

                        if (tmp.Length == 2)
                        {
                            string animPath = tmp[0];
                            string duration = tmp[1];

                            int end = animPath.LastIndexOf('/');
                            string animDir = animPath.Substring(0, end);
                            string animName = animPath.Substring(end + 1);

                            System.Diagnostics.Debug.WriteLine(animDir);
                            //check if facial
                            if (animDir.ToLower().StartsWith("face_human") || animDir.ToLower().StartsWith("face_creatures"))
                            {
                                AddGestures(m_FacialDirectories, m_DirectoryListFL, m_DirectoryListFS, animDir, animName, duration, m_GenericAnimationList);
                            }
                            else
                            {
                                AddGestures(m_Directories, m_DirectoryListL, m_DirectoryListS, animDir, animName, duration, m_GenericAnimationList);
                            }
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("invalid data: " + str);
                        }
                    }
                }
                catch (Exception /*e*/)
                {
                    Environment.Exit(-1);
                }
            }
            else
            {
                string animDir = "male_generic";
                m_DirectoryListS.Items.Add(animDir);
                m_DirectoryListL.Items.Add(animDir);
                m_Directories.Add(animDir, new ArrayList());
                m_GenericAnimationList.Add(new StringListItem("wave", "250"));
                m_GenericAnimationList.Add(new StringListItem("nod_yes", "500"));
                m_GenericAnimationList.Add(new StringListItem("shake_no", "650"));
                ((ArrayList)m_Directories[animDir]).Add(new StringListItem("wave", "250"));
                ((ArrayList)m_Directories[animDir]).Add(new StringListItem("nod_yes", "500"));
                ((ArrayList)m_Directories[animDir]).Add(new StringListItem("shake_no", "650"));

                string testDir = "test_specific";
                m_DirectoryListS.Items.Add(testDir);
                m_DirectoryListL.Items.Add(testDir);
                m_Directories.Add(testDir, new ArrayList());
                ((ArrayList)m_Directories[testDir]).Add(new StringListItem("wave", "250"));
                ((ArrayList)m_Directories[testDir]).Add(new StringListItem("cross_arms", "150"));
                ((ArrayList)m_Directories[testDir]).Add(new StringListItem("wink", "200"));

            }

            m_DirectoryListS.Text = m_DirectoryListS.Items[0].ToString();
            m_DirectoryListL.Text = m_DirectoryListL.Items[0].ToString();
            m_DirectoryListFL.Text = m_DirectoryListFL.Items[0].ToString();
            m_DirectoryListFS.Text = m_DirectoryListFS.Items[0].ToString();

            SetGestureList(ref m_MarkerNamesGL, (ArrayList)m_Directories[m_DirectoryListL.Text]);
            SetGestureList(ref m_MarkerNamesGS, (ArrayList)m_Directories[m_DirectoryListS.Text]);

            SetGestureList(ref m_MarkerNamesFL, (ArrayList)m_FacialDirectories[m_DirectoryListFL.Text]);
            SetGestureList(ref m_MarkerNamesFS, (ArrayList)m_FacialDirectories[m_DirectoryListFS.Text]);
        }

        public void AddGestures(Hashtable directories, ComboBox listener, ComboBox speaker, string animDir, string animName, string duration, ArrayList genericAnims)
        {
            if (!directories.ContainsKey(animDir))
            {
                listener.Items.Add(animDir);
                speaker.Items.Add(animDir);
                directories.Add(animDir, new ArrayList());
            }

            // check to see if this is a variation anim, and if so only store one item for it - the base anim name (_001 etc)
            if (animName.LastIndexOf("_") != -1)
            {
                if (animName.LastIndexOf("_") == animName.Length - 2)
                {
                    animName = animName.Substring(0, animName.Length - 2) + "_a";
                }
            }
            
            // ignore duplicates
            bool ignore = false;
            foreach (StringListItem sli in (ArrayList)directories[animDir])
            {
                if (sli.ToString() == animName)
                {
                    ignore = true;
                    break;
                }
            }
            if (!ignore)
            {
                ((ArrayList)directories[animDir]).Add(new StringListItem(animName, duration));

                if (genericAnims != null)
                {
                    bool inGenericList = false;
                    foreach (StringListItem sli in genericAnims)
                    {
                        if (sli.ToString() == animName)
                        {
                            inGenericList = true;
                        }
                    }

                    if (animDir.ToUpper().IndexOf("MALE") != -1 && !inGenericList)
                    {
                        //genericAnims.Add(new StringListItem(animName, duration));
                    }
                }
            }

        }

        bool GLisBuilt = false;
        bool GSisBuilt = false;
        bool FLisBuilt = false;
        bool FSisBuilt = false;

        bwGestureMarker m_RCMarker;
        bwWaveFile m_RCWave;
        bwGestureFile m_RCGesture;
        int m_RCOffset;

        void onContextMenuPopup(string prefix, bwGestureMarker marker, bwWaveFile wave, bwGestureFile gesture, int offset, ContextMenuStrip menu)
        {
            m_RCMarker = marker;
            m_RCWave = wave;
            m_RCOffset = offset;
            m_RCGesture = gesture;

            switch (prefix)
            {
                case "G_L:":
                    populateMenu(menu, prefix, m_MarkerNamesGL, m_GenericAnimationList, ref GLisBuilt);
                    break;
                case "G_S:":
                    populateMenu(menu, prefix, m_MarkerNamesGS, m_GenericAnimationList, ref GSisBuilt);
                    break;
                case "F_L:":
                    populateMenu(menu, prefix, m_MarkerNamesFL, null, ref FLisBuilt);
                    break;
                case "F_S:":
                    populateMenu(menu, prefix, m_MarkerNamesFS, null, ref FSisBuilt);
                    break;
            }
        }


        void populateMenu(ContextMenuStrip menu, string prefix, ComboBox markerNames, ArrayList genericAnims, ref bool isBuilt)
        {

            if (!isBuilt)
            {
                menu.Items.Clear();

                ToolStripMenuItem remove = new ToolStripMenuItem();
                remove.Text = "Remove";
                remove.Click += new EventHandler(remove_Click);
                menu.Items.Add(remove);

                ToolStripMenuItem resetSize = new ToolStripMenuItem();
                resetSize.Text = "Reset Duration";
                resetSize.Click += new EventHandler(resetSize_Click);
                menu.Items.Add(resetSize);

                foreach (StringListItem anim in markerNames.Items)
                {
                    ToolStripItem animMenu = new ToolStripMenuItem();
                    animMenu.Click += new EventHandler(animMenu_Click);
                    //set key to anim.ToString() so we can search for duplicates
                    animMenu.Text = anim.ToString();
                    animMenu.Tag = prefix.Substring(0, prefix.Length - 1);
                    animMenu.ForeColor = Color.Red;
                    menu.Items.Add(animMenu);
                }

                if (genericAnims != null)
                {
                    ArrayList temp = new ArrayList();
                    foreach (StringListItem anim in genericAnims)
                    {
                        bool contains = false;

                        foreach (ToolStripMenuItem ts in menu.Items)
                        {
                            if (ts.Text == anim.ToString())
                            {
                                contains = true;
                                ts.ForeColor = Color.Black;
                            }
                        }

                        //build a temp list of non duplicates
                        if (!contains)
                        {
                            ToolStripItem animMenu = new ToolStripMenuItem();
                            animMenu.Click += new EventHandler(animMenu_Click);
                            animMenu.Text = anim.ToString();
                            animMenu.Tag = prefix.Substring(0, prefix.Length - 1);
                            temp.Add(animMenu);
                        }
                    }

                    if (temp.Count > 0)
                    {
                        menu.Items.AddRange((ToolStripItem[])temp.ToArray());
                    }
                }
                isBuilt = true;
            }
        }

        void resetSize_Click(object sender, EventArgs e)
        {
            if (m_RCMarker != null)
            {
                m_RCMarker.Length = m_RCWave.ConvertMsToSamples(FindAnimLength(m_RCMarker.Name, m_RCMarker.Dir));
                m_RCMarker.Clip.Rate = 1;
                m_Container.Refresh();
            }
        }

        void remove_Click(object sender, EventArgs e)
        {
            if (m_RCMarker != null)
            {
                m_RCGesture.Markers.Remove(m_RCMarker);
                m_Container.Refresh();
            }
        }

        void animMenu_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
            string anim = menuItem.Text;
            string track = menuItem.Tag.ToString();

            string dictionaryName = String.Empty;

            switch (track)
            {
                case "G_S":
                case "F_S":
                    dictionaryName = (string)m_DirectoryListS.SelectedItem;
                    break;
                case "G_L":
                case "F_L":
                    dictionaryName = (string)m_DirectoryListL.SelectedItem;
                    break;
            }
            
            string markerName = track + ":" + anim;
            uint length = m_RCWave.ConvertMsToSamples(FindAnimLength(markerName, dictionaryName));

            bwGestureMarker marker = new bwGestureMarker(markerName, m_RCOffset, dictionaryName, length);

            m_RCGesture.Markers.Add(marker);
            m_Container.Refresh();
            //m_RCGesture.Markers.Sort((t1, t2) => t1.Position.CompareTo(t2.Position));
            //m_GLTrack.Update(m_RCGesture.Markers[m_RCGesture.Markers.Count-1]);
        }

        void m_Container_OnClose()
        {
            Collection<bwGestureFile> gestureFiles = m_Container.GetGestureFiles();
            bwWaveFileList waveFiles = m_Container.GetWaveFiles();
            bwGestureSerializer serializer = new bwGestureSerializer();
            for (int i = 0; i < gestureFiles.Count; i++)
            {
                string filename = gestureFiles[i].FileName;
                FileInfo fileInfo = new FileInfo(filename);
                if (fileInfo.Exists && fileInfo.IsReadOnly)
                {
                    string msg = string.Format(
                        "Unable to save the file '{0}' due to it being read-only.\nDo you wish to over-write the file or cancel the save?", filename);
                    var result = MessageBox.Show(msg, "Read-Only Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                    if (result == System.Windows.Forms.DialogResult.Cancel)
                    {
                        continue;
                    }

                    fileInfo.IsReadOnly = false;
                }

                ConvertRegionsToMarkers(gestureFiles[i], waveFiles[i]);
                serializer.SerializeObject(gestureFiles[i]);
            }

            //for (int i = 0; i < m_Container.GetWaveFiles().Count; ++i)
            //{
            //    //m_Container.GetWaveFiles()[i].Save();

            //    ConvertRegionsToMarkers(m_Container.GetGestureFiles()[i], m_Container.GetWaveFiles()[i]);

            //    bwGestureFile file = m_Container.GetGestureFiles()[i]];
            //    bwGestureSerializer ser = new bwGestureSerializer();
            //    ser.SerializeObject(m_Container.GetGestureFiles()[i]);
            //}
        }

        void m_Container_OnWaveSave(bwGestureFile gesture, bwWaveFile wave)
        {
            ConvertRegionsToMarkers(gesture, wave);
        }

        void m_Container_OnWaveOpen(bwGestureFile gesture, bwWaveFile wave)
        {
            ConvertMarkersToRegions(gesture, wave);
        }

        private uint FindAnimLength(string markerName, string markerDir)
        {
            Hashtable ht = m_Directories;
            string animName = markerName.Substring(markerName.IndexOf(':') + 1);

            if (markerName.StartsWith("F_S") || markerName.StartsWith("F_L"))
            {
                ht = m_FacialDirectories;
            }

            foreach (DictionaryEntry de in ht)
            {
                if (de.Key.ToString() == markerDir)
                {
                    foreach (StringListItem s in (ArrayList)de.Value)
                    {
                        if (s.ToString() == animName)
                        {
                            return uint.Parse(s.GetData());
                        }
                    }
                }
            }
            return 0;
        }


        void m_Container_OnMarkerHit(bwGestureMarker marker, bwWaveFile wave)
        {
            if (IsGestureMarkerName(marker.Name) && marker is bwGestureMarker)
            {
                string directory = marker.Dir;
                uint animLength = animLength = wave.ConvertSamplesToMs(marker.Length);
                if (m_TcpClient != null)
                {
                    int startFrame = (int)((marker.Position / (float)wave.Format.SampleRate) * 30.0);
                    string format = "{0}:{1}:{2}:{3}:{4}:{5}:{6}:{7}:{8}:{9}\n";
                    format = string.Format(format,
                        directory,
                        marker.Name,
                        animLength,
                        marker.CropPosition,
                        marker.Clip.StartFrame,
                        marker.Clip.EndFrame,
                        marker.Clip.Rate,
                        marker.Clip.MaxWeight,
                        marker.Clip.BlendInTime,
                        marker.Clip.BlendOutTime);
                    
                    byte[] buf = System.Text.Encoding.ASCII.GetBytes(format);
                    m_TcpClient.GetStream().Write(buf, 0, buf.Length);
                }
            }
            
        }

        public class Asc : IComparer
        {
            public int Compare(object x, object y)
            {
                string xstring = x.ToString();
                string ystring = y.ToString();

                return xstring.CompareTo(ystring);
            }
        }

        private void SetGestureList(ref ComboBox combobox, ArrayList gestures)
        {
            combobox.Items.Clear();
            if (gestures != null)
            {
                ArrayList comboContents = new ArrayList();
                comboContents.AddRange(gestures.ToArray());

                //Add generic animations for body
                if (combobox == m_MarkerNamesGS || combobox == m_MarkerNamesGL)
                {
                    ArrayList temp = new ArrayList();

                    foreach (StringListItem genGesture in m_GenericAnimationList)
                    {
                        bool contains = false;

                        //does combox collection contain genGesture
                        foreach (StringListItem sli in combobox.Items)
                        {
                            if (genGesture.ToString() == sli.ToString())
                            {
                                contains = true;
                            }
                        }

                        if (!contains)
                        {
                            temp.Add(genGesture);
                        }
                    }

                    comboContents.AddRange(temp.ToArray());
                }

                comboContents.Sort(0, comboContents.Count, new Asc());
                combobox.Items.AddRange(comboContents.ToArray());

                if (gestures.Count > 0)
                {
                    combobox.Text = gestures[0].ToString();
                }
                
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFS = new System.Windows.Forms.Button();
            this.m_MarkerNamesFS = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_DirectoryListFS = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_DirectoryListS = new System.Windows.Forms.ComboBox();
            this.btnGS = new System.Windows.Forms.Button();
            this.m_MarkerNamesGS = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.m_DirectoryListL = new System.Windows.Forms.ComboBox();
            this.btnGL = new System.Windows.Forms.Button();
            this.m_MarkerNamesGL = new System.Windows.Forms.ComboBox();
            this.m_MarkerNamesFL = new System.Windows.Forms.ComboBox();
            this.btnFL = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.m_DirectoryListFL = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lbMarkers = new System.Windows.Forms.ListBox();
            this.btnMarkersPopulate = new System.Windows.Forms.Button();
            this.btnPopulateMarkers = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnFS
            // 
            this.btnFS.Location = new System.Drawing.Point(32, 77);
            this.btnFS.Name = "btnFS";
            this.btnFS.Size = new System.Drawing.Size(128, 23);
            this.btnFS.TabIndex = 0;
            this.btnFS.Text = "Place Gesture";
            this.btnFS.Click += new System.EventHandler(this.btnFS_Click);
            // 
            // m_MarkerNamesFS
            // 
            this.m_MarkerNamesFS.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.m_MarkerNamesFS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_MarkerNamesFS.Location = new System.Drawing.Point(32, 50);
            this.m_MarkerNamesFS.Name = "m_MarkerNamesFS";
            this.m_MarkerNamesFS.Size = new System.Drawing.Size(437, 21);
            this.m_MarkerNamesFS.TabIndex = 1;
            this.m_MarkerNamesFS.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.m_MarkerNames_DrawItem);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.m_DirectoryListFS);
            this.groupBox1.Controls.Add(this.btnFS);
            this.groupBox1.Controls.Add(this.m_MarkerNamesFS);
            this.groupBox1.Location = new System.Drawing.Point(565, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(475, 110);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Facial Speaker";
            // 
            // m_DirectoryListFS
            // 
            this.m_DirectoryListFS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_DirectoryListFS.Location = new System.Drawing.Point(32, 26);
            this.m_DirectoryListFS.Name = "m_DirectoryListFS";
            this.m_DirectoryListFS.Size = new System.Drawing.Size(437, 21);
            this.m_DirectoryListFS.TabIndex = 15;
            this.m_DirectoryListFS.SelectedIndexChanged += new System.EventHandler(this.m_DirectoryListFS_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.m_DirectoryListS);
            this.groupBox2.Controls.Add(this.btnGS);
            this.groupBox2.Controls.Add(this.m_MarkerNamesGS);
            this.groupBox2.Location = new System.Drawing.Point(2, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(557, 110);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Gesture Speaker";
            // 
            // m_DirectoryListS
            // 
            this.m_DirectoryListS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_DirectoryListS.Location = new System.Drawing.Point(32, 26);
            this.m_DirectoryListS.Name = "m_DirectoryListS";
            this.m_DirectoryListS.Size = new System.Drawing.Size(519, 21);
            this.m_DirectoryListS.TabIndex = 14;
            this.m_DirectoryListS.SelectedIndexChanged += new System.EventHandler(this.m_DirectoryListS_SelectedIndexChanged);
            // 
            // btnGS
            // 
            this.btnGS.Location = new System.Drawing.Point(32, 77);
            this.btnGS.Name = "btnGS";
            this.btnGS.Size = new System.Drawing.Size(128, 23);
            this.btnGS.TabIndex = 0;
            this.btnGS.Text = "Place Gesture";
            this.btnGS.Click += new System.EventHandler(this.btnGS_Click);
            // 
            // m_MarkerNamesGS
            // 
            this.m_MarkerNamesGS.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.m_MarkerNamesGS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_MarkerNamesGS.Location = new System.Drawing.Point(32, 50);
            this.m_MarkerNamesGS.Name = "m_MarkerNamesGS";
            this.m_MarkerNamesGS.Size = new System.Drawing.Size(519, 21);
            this.m_MarkerNamesGS.TabIndex = 1;
            this.m_MarkerNamesGS.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.m_MarkerNamesGS_DrawItem);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.m_DirectoryListL);
            this.groupBox3.Controls.Add(this.btnGL);
            this.groupBox3.Controls.Add(this.m_MarkerNamesGL);
            this.groupBox3.Location = new System.Drawing.Point(2, 124);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(557, 115);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Gesture Listener";
            // 
            // m_DirectoryListL
            // 
            this.m_DirectoryListL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_DirectoryListL.Location = new System.Drawing.Point(32, 26);
            this.m_DirectoryListL.Name = "m_DirectoryListL";
            this.m_DirectoryListL.Size = new System.Drawing.Size(519, 21);
            this.m_DirectoryListL.TabIndex = 14;
            this.m_DirectoryListL.SelectedIndexChanged += new System.EventHandler(this.m_DirectoryListL_SelectedIndexChanged);
            // 
            // btnGL
            // 
            this.btnGL.Location = new System.Drawing.Point(32, 77);
            this.btnGL.Name = "btnGL";
            this.btnGL.Size = new System.Drawing.Size(128, 23);
            this.btnGL.TabIndex = 0;
            this.btnGL.Text = "Place Gesture";
            this.btnGL.Click += new System.EventHandler(this.btnGL_Click);
            // 
            // m_MarkerNamesGL
            // 
            this.m_MarkerNamesGL.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.m_MarkerNamesGL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_MarkerNamesGL.Location = new System.Drawing.Point(32, 50);
            this.m_MarkerNamesGL.Name = "m_MarkerNamesGL";
            this.m_MarkerNamesGL.Size = new System.Drawing.Size(519, 21);
            this.m_MarkerNamesGL.TabIndex = 1;
            this.m_MarkerNamesGL.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.m_MarkerNamesGL_DrawItem);
            // 
            // m_MarkerNamesFL
            // 
            this.m_MarkerNamesFL.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.m_MarkerNamesFL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_MarkerNamesFL.Location = new System.Drawing.Point(32, 50);
            this.m_MarkerNamesFL.Name = "m_MarkerNamesFL";
            this.m_MarkerNamesFL.Size = new System.Drawing.Size(431, 21);
            this.m_MarkerNamesFL.TabIndex = 1;
            this.m_MarkerNamesFL.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.m_MarkerNamesFL_DrawItem);
            // 
            // btnFL
            // 
            this.btnFL.Location = new System.Drawing.Point(32, 77);
            this.btnFL.Name = "btnFL";
            this.btnFL.Size = new System.Drawing.Size(128, 23);
            this.btnFL.TabIndex = 0;
            this.btnFL.Text = "Place Gesture";
            this.btnFL.Click += new System.EventHandler(this.btnFL_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.m_DirectoryListFL);
            this.groupBox4.Controls.Add(this.btnFL);
            this.groupBox4.Controls.Add(this.m_MarkerNamesFL);
            this.groupBox4.Location = new System.Drawing.Point(565, 124);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(469, 115);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Facial Listener";
            // 
            // m_DirectoryListFL
            // 
            this.m_DirectoryListFL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_DirectoryListFL.Location = new System.Drawing.Point(32, 26);
            this.m_DirectoryListFL.Name = "m_DirectoryListFL";
            this.m_DirectoryListFL.Size = new System.Drawing.Size(431, 21);
            this.m_DirectoryListFL.TabIndex = 15;
            this.m_DirectoryListFL.SelectedIndexChanged += new System.EventHandler(this.m_DirectoryListFL_SelectedIndexChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lbMarkers);
            this.groupBox5.Controls.Add(this.btnMarkersPopulate);
            this.groupBox5.Location = new System.Drawing.Point(2, 246);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(262, 181);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Gesture Markers";
            // 
            // lbMarkers
            // 
            this.lbMarkers.FormattingEnabled = true;
            this.lbMarkers.Location = new System.Drawing.Point(32, 19);
            this.lbMarkers.Name = "lbMarkers";
            this.lbMarkers.Size = new System.Drawing.Size(223, 121);
            this.lbMarkers.TabIndex = 20;
            this.lbMarkers.SelectedIndexChanged += new System.EventHandler(this.lbMarkers_SelectedIndexChanged);
            // 
            // btnMarkersPopulate
            // 
            this.btnMarkersPopulate.Location = new System.Drawing.Point(32, 146);
            this.btnMarkersPopulate.Name = "btnMarkersPopulate";
            this.btnMarkersPopulate.Size = new System.Drawing.Size(128, 23);
            this.btnMarkersPopulate.TabIndex = 20;
            this.btnMarkersPopulate.Text = "Populate Markers";
            this.btnMarkersPopulate.UseVisualStyleBackColor = true;
            this.btnMarkersPopulate.Click += new System.EventHandler(this.btnMarkersPopulate_Click);
            // 
            // btnPopulateMarkers
            // 
            this.btnPopulateMarkers.Location = new System.Drawing.Point(0, 0);
            this.btnPopulateMarkers.Name = "btnPopulateMarkers";
            this.btnPopulateMarkers.Size = new System.Drawing.Size(75, 23);
            this.btnPopulateMarkers.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.propertyGrid1);
            this.groupBox6.Location = new System.Drawing.Point(275, 246);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(284, 181);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Gesture Marker Properties";
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.HelpVisible = false;
            this.propertyGrid1.Location = new System.Drawing.Point(6, 19);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.propertyGrid1.Size = new System.Drawing.Size(272, 156);
            this.propertyGrid1.TabIndex = 0;
            this.propertyGrid1.ToolbarVisible = false;
            this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
            // 
            // frmGestureTool
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1046, 439);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmGestureTool";
            this.Text = "GTA Gesture Tool";
            this.Enter += new System.EventHandler(this.frmGestureTool_Enter);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void AddMarker(string markerName, uint durationMs, string markerDir)
        {
            bwWaveFile wave;
            bwGestureFile gesture;
            int position;
            m_Container.GetCursor(out wave, out gesture, out position);

            if (wave != null)
            {
                uint lengthSamples = wave.ConvertMsToSamples(durationMs);

                bwGestureMarker marker = new bwGestureMarker(markerName, position, markerDir, lengthSamples);
                marker.Clip.BlendInTime = 0.0f;
                marker.Clip.BlendOutTime = 0.0f;
                gesture.Markers.Add(marker);
                m_Container.Refresh();
            }
        }

        private void m_Track_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //			if(m_Track.SelectedIndex == 0)
            //			{
            //				SetGestureList(m_BodyGestures);
            //			}
            //			else
            //			{
            //				SetGestureList(m_FacialGestures);
            //			}
        }

        private bool IsGestureMarkerName(string name)
        {
            string[] elems = name.Split(':');

            switch (elems[0])
            {
                case "F_L":
                case "F_S":
                case "G_L":
                case "G_S":
                    return true;
                default:
                    return false;
            }
        }

        private void ConvertRegionsToMarkers(bwGestureFile gesture, bwWaveFile wave)
        {
            // convert any gesture regions to markers    
            //ArrayList toBeDeleted = new ArrayList();
            //ArrayList toBeAdded = new ArrayList();
            //foreach (bwGestureMarker marker in gesture.Markers)
            //{
            //    marker.Length = 
            //    if (IsGestureMarkerName(marker.Name) && marker is bwGestureMarker)
            //    {
            //        // preserve length for regions
            //        bwGestureMarker gm = new bwGestureMarker(marker.Name + ":" + wave.ConvertSamplesToMs(((bwGestureMarker)marker).Length).ToString(), marker.Position);
            //        gm.Clip = marker.Clip;
            //        toBeAdded.Add(gm);
            //        toBeDeleted.Add(marker);
            //    }
            //    //else
            //    //{
            //    //    toBeDeleted.Add(marker);
            //    //}
            //}
            //foreach (bwGestureMarker marker in toBeDeleted)
            //{
            //    gesture.Markers.Remove(marker);
            //}
            //foreach (bwGestureMarker marker in toBeAdded)
            //{
            //    gesture.Markers.Add(marker);
            //}

            m_Container.Refresh();
        }

        private void ConvertMarkersToRegions(bwGestureFile gesture, bwWaveFile wave)
        {
            //foreach (bwGestureMarker marker in gesture.Markers)
            //{
            //    if (IsGestureMarkerName(marker.Name))
            //    {
            //        toBeDeleted.Add(marker);
            //        string[] elems = marker.Name.Split(':');
            //        if (elems.Length == 3)
            //        {
            //            bwGestureMarker gr = new bwGestureMarker(elems[0] + ':' + elems[1], marker.Position, wave.ConvertMsToSamples(uint.Parse(elems[2])));
            //            gr.Clip = marker.Clip;
            //            toBeAdded.Add(gr);
            //        }
            //    }
            //}

            //foreach (bwGestureMarker marker in toBeDeleted)
            //{
            //    gesture.Markers.Remove(marker);
            //}
            //foreach (bwGestureMarker marker in toBeAdded)
            //{
            //    gesture.Markers.Add(marker);
            //}

            m_Container.Refresh();
        }

        private void m_MarkerNames_DrawItem(object sender, DrawItemEventArgs e)
        {
            DrawMarkerListItem((ComboBox)sender, e);
        }

        private void m_DirectoryListS_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetGestureList(ref m_MarkerNamesGS, (ArrayList)m_Directories[m_DirectoryListS.Text]);
            GSisBuilt = false;
        }

        private void m_DirectoryListL_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetGestureList(ref m_MarkerNamesGL, (ArrayList)m_Directories[m_DirectoryListL.Text]);
            GLisBuilt = false;
        }

        private void btnGS_Click(object sender, EventArgs e)
        {
            string markerName = "G_S:" + m_MarkerNamesGS.Text;
            AddMarker(markerName, uint.Parse(((StringListItem)m_MarkerNamesGS.SelectedItem).GetData()), (string)m_DirectoryListS.SelectedItem);
        }

        private void btnFS_Click(object sender, System.EventArgs e)
        {
            string markerName = "F_S:" + m_MarkerNamesFS.Text;
            AddMarker(markerName, uint.Parse(((StringListItem)m_MarkerNamesFS.SelectedItem).GetData()), (string)m_DirectoryListFS.SelectedItem);
        } 

        private void btnGL_Click(object sender, EventArgs e)
        {
            string markerName = "G_L:" + m_MarkerNamesGL.Text;
            AddMarker(markerName, uint.Parse(((StringListItem)m_MarkerNamesGL.SelectedItem).GetData()), (string)m_DirectoryListL.SelectedItem);
        }

        private void btnFL_Click(object sender, EventArgs e)
        {
            string markerName = "F_L:" + m_MarkerNamesFL.Text;
            AddMarker(markerName, uint.Parse(((StringListItem)m_MarkerNamesFL.SelectedItem).GetData()), (string)m_DirectoryListFL.SelectedItem);
        }

        private void DrawMarkerListItem(ComboBox box, DrawItemEventArgs e)
        {
            Rectangle rc = new Rectangle(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height);

            if (e.Index >= 0 && e.Index < box.Items.Count)
            {
                string str = box.Items[e.Index].ToString();
                bool isGeneric = false;

                if (box == m_MarkerNamesGS || box == m_MarkerNamesGL)
                {
                    foreach (StringListItem sli in m_GenericAnimationList)
                    {
                        if (sli.ToString() == str)
                        {
                            isGeneric = true;
                        }
                    }
                }


                if (e.State == DrawItemState.Selected)
                {
                    e.Graphics.FillRectangle(new SolidBrush(Color.CornflowerBlue), rc);
                    e.Graphics.DrawString(str, box.Font, new SolidBrush((isGeneric ? Color.Cyan : Color.Red)), rc);
                }
                else
                {
                    e.Graphics.FillRectangle(new SolidBrush(Color.White), rc);
                    e.Graphics.DrawString(str, box.Font, new SolidBrush((isGeneric ? Color.Black : Color.Red)), rc);
                }
            }
        }

        private void m_MarkerNamesGS_DrawItem(object sender, DrawItemEventArgs e)
        {
            DrawMarkerListItem((ComboBox)sender, e);
        }

        private void m_MarkerNamesGL_DrawItem(object sender, DrawItemEventArgs e)
        {
            DrawMarkerListItem((ComboBox)sender, e);
        }

        private void m_MarkerNamesFL_DrawItem(object sender, DrawItemEventArgs e)
        {
            DrawMarkerListItem((ComboBox)sender, e);
        }

        private void btnMarkersPopulate_Click(object sender, EventArgs e)
        {
            bwWaveFile wave;
            bwGestureFile gesture;
            int position;
            m_Container.GetCursor(out wave, out gesture, out position);
            if (wave != null)
            {
                lbMarkers.Items.Clear();
                for (int i = 0; i < gesture.Markers.Count; ++i)
                {
                    lbMarkers.Items.Add(gesture.Markers[i]);
                }

                m_Container.Refresh();
            }
        }

        private void lbMarkers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbMarkers.SelectedIndex != -1)
            {
                bwGestureClip clip = ((bwGestureMarker)lbMarkers.SelectedItem).Clip;
                propertyGrid1.SelectedObject = clip;
            }
            else
            {
                propertyGrid1.SelectedObject = null;
            }
        }

        private void frmGestureTool_Enter(object sender, EventArgs e)
        {
            int index = lbMarkers.SelectedIndex;
            lbMarkers.SelectedIndex = -1;
            lbMarkers.SelectedIndex = index;
            Invalidate(true);
        }

        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            bwGestureClip c = (bwGestureClip)e.ChangedItem.Parent.Value;

            if (e.ChangedItem.Label == "Rate")
            {
                c.m_bRateChange = true;
            }

            m_GLTrack.Update((bwGestureMarker)c.Tag);
            m_FLTrack.Update((bwGestureMarker)c.Tag);
            m_GSTrack.Update((bwGestureMarker)c.Tag);
            m_FSTrack.Update((bwGestureMarker)c.Tag);

            m_GLTrack.Invalidate();
            m_FLTrack.Invalidate();
            m_GSTrack.Invalidate();
            m_FSTrack.Invalidate();
        }

        private void m_DirectoryListFS_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetGestureList(ref m_MarkerNamesFS, (ArrayList)m_FacialDirectories[m_DirectoryListFS.Text]);
            FSisBuilt = false;
        }

        private void m_DirectoryListFL_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetGestureList(ref m_MarkerNamesFL, (ArrayList)m_FacialDirectories[m_DirectoryListFL.Text]);
            FLisBuilt = false;
        }
     }

    public class StringListItem
    {
        public StringListItem(string display, string data)
        {
            m_Display = display;
            m_Data = data;
        }

        public override string ToString()
        {
            return m_Display;
        }

        public string GetData()
        {
            return m_Data;
        }

        private string m_Display, m_Data;
    }
}