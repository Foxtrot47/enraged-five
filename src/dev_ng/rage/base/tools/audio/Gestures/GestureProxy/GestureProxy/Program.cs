using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace GestureProxy
{

    enum eThreadId : int
    {
        PreviewThread = 0,
        ToolThread,
    }

    class ProxyConnectionState
    {
        public ProxyConnectionState(eThreadId threadId, int port)
        {
            Port = port;
            ThreadId = threadId;
            Client = null;
            Listener = null;
        }

        public readonly eThreadId ThreadId;
        public readonly int Port;
        public TcpListener Listener;
        public TcpClient Client;
    }

    class Program
    {
        const int GesturePreviewPort   = 7564;
        const int GestureToolPort       = 7565;

        static ProxyConnectionState[] ConnectionStates;

        static void Main(string[] args)
        {
            ConnectionStates = new ProxyConnectionState[2];
            ConnectionStates[(int)eThreadId.PreviewThread] = new ProxyConnectionState(eThreadId.PreviewThread, GesturePreviewPort);
            ConnectionStates[(int)eThreadId.ToolThread] = new ProxyConnectionState(eThreadId.ToolThread, GestureToolPort);

            Thread previewThread = new Thread(new ParameterizedThreadStart(ConnectionThread));
            
            previewThread.Start(eThreadId.PreviewThread);
            
            Console.WriteLine("Gesture tool proxy, listening on ports " + GesturePreviewPort + " and " + GestureToolPort);
            // may aswell run the tool connection on the main thread
            ConnectionThread(eThreadId.ToolThread);
        }

        static void ConnectionThread(object o)
        {
            eThreadId threadId = (eThreadId)o;
            eThreadId otherThread = (eThreadId)(((int)threadId + 1) % 2);

            ConnectionStates[(int)threadId].Listener = new TcpListener(IPAddress.Any, ConnectionStates[(int)threadId].Port);
            ConnectionStates[(int)threadId].Listener.Start();
            while (true)
            {
                ConnectionStates[(int)threadId].Client = ConnectionStates[(int)threadId].Listener.AcceptTcpClient();
                Console.WriteLine(threadId.ToString() + " received client: " + ConnectionStates[(int)threadId].Client.Client.RemoteEndPoint.ToString());
                while (ConnectionStates[(int)threadId].Client.Connected)
                {
                    try
                    {
                        byte[] buf = new byte[ConnectionStates[(int)threadId].Client.ReceiveBufferSize];
                        int read = ConnectionStates[(int)threadId].Client.GetStream().Read(buf, 0, buf.Length);
                        if (ConnectionStates[(int)otherThread].Client != null && ConnectionStates[(int)otherThread].Client.Connected)
                        {
                            ConnectionStates[(int)otherThread].Client.GetStream().Write(buf, 0, read);

                            string msg = System.Text.Encoding.ASCII.GetString(buf,0, read);
                            Console.WriteLine("[" + threadId.ToString() + " -> " + otherThread.ToString() + "]: \"" + msg + "\"");
                        }
                    }
                    catch (Exception)
                    {
                    }
                }

                ConnectionStates[(int)threadId].Client = null;
            }            
        }
    }
}
