using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Diagnostics;

namespace schemasubmit
{
    class Program
    {
        static string RunCommand(string command, string args)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = command;
            proc.EnableRaisingEvents = false;
            proc.StartInfo.Arguments = args;
            proc.StartInfo.ErrorDialog = false;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.CreateNoWindow = true;
            Console.WriteLine("Running {0} {1}", command, args);
            proc.Start();

            string output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();
            return output;
        }

        static bool CheckOut(string file)
        {
            string output = RunCommand("p4", "edit " + file);
            if(output.Trim().EndsWith(" - opened for edit"))
            {
                return true;
            }
            return false;
        }

        static bool ImportFile(string file)
        {
            string output = RunCommand("p4", "add " + file);
            if (output.Trim().EndsWith(" - opened for add"))
            {
                return true;
            }
            return false;
        }

        static bool SubmitFile(string file)
        {
            string output = RunCommand("p4", "submit -d schemasubmit " + file);
            if (output.Trim().EndsWith(" - opened for add"))
            {
                return true;
            }
            return false;
        }

        static void Main(string[] args)
        {
            String schemaFile = null;
            bool archiveOnly = false;
            for(int i = 0; i < args.Length; i++)
            {
                if(args[i] == "-schema")
                {
                    schemaFile = args[i + 1];
                    i++;
                }
                else if(args[i] == "-archiveonly")
                {
                    archiveOnly = true;
                }
            }

            if(schemaFile == null)
            {
                Console.WriteLine("Usage: schemasubmit -schema Definitions.xml [-archiveonly]");
                return;
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(schemaFile);

            FileInfo fileInfo = new FileInfo(schemaFile);

            string submitFiles = "";
            
            if(doc.DocumentElement.Name == "TypeDefinitions")
            {
                if(doc.DocumentElement.Attributes["version"] != null)
                {
                    string archiveFolder = fileInfo.Directory + @"\archive\";
                    if(!Directory.Exists(archiveFolder))
                    {
                        Directory.CreateDirectory(archiveFolder);
                    }
                    string archivePath = archiveFolder + fileInfo.Name + doc.DocumentElement.Attributes["version"].Value;
                    bool needToAdd = true;
                    if(File.Exists(archivePath))
                    {
                        if(CheckOut(archivePath))
                        {
                            needToAdd = false;
                        }
                        File.SetAttributes(archivePath, FileAttributes.Normal);
                    }

                    File.Copy(schemaFile, archivePath, true);

                    if(needToAdd)
                    {
                        ImportFile(archivePath);
                    }

                    submitFiles += archivePath + " ";
                }
                else
                {
                    Console.WriteLine("No schema version specified - archive disabled");
                }
            }
            else
            {
                Console.WriteLine("Schema file doesn't missing TypeDefinitions root node");
            }

            if(!archiveOnly)
            {
                submitFiles += schemaFile;
            }
            SubmitFile(submitFiles);
        }
    }
}
