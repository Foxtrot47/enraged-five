﻿using DstarToReaperConverter.Models;
using NAudio.Wave;
using Reaper;

namespace DstarToReaperConverter
{
    class ReaperClip : ReaperTrack.Clip
    {
        public ReaperClip(Line line, string wavfileLocation, double position)
            : base()
        {
            WaveFileReader wavfile = new WaveFileReader(wavfileLocation);
            ClipName = line.FileName;
            Position = position;
            Length = (wavfile.SampleCount / (double)wavfile.WaveFormat.Channels) / (double)wavfile.WaveFormat.SampleRate;
            SourceWave = wavfileLocation;

        }
    }
}
