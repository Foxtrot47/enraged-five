﻿using System;
using System.Windows;
using RSG.Editor.Controls;

namespace DstarToReaperConverter
{
    class App : RsApplication
    {
        [STAThread]
        public static void Main(string[] args)
        {
            App app = new App();
            OnEntry(app,ApplicationMode.Multiple);
        }

        protected override Window CreateMainWindow()
        {
            return new MainWindow();
        }
    }
}
