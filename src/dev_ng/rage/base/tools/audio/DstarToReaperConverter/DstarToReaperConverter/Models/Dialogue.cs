﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace DstarToReaperConverter.Models
{
    public class Dialogue
    {
        public string Name { get; set; }
        public string Subtitle { get; set; }
        public bool Multiplayer { get; set; }
        public bool Ambient { get; set; }
        public List<Conversation> Conversations { get; set; }

        public Dialogue(string path)
        {
            XDocument doc = XDocument.Load(path);

            var MissionDialogue = doc.Descendants("MissionDialogue").FirstOrDefault();

            Name = MissionDialogue.Attribute("name").Value;

            var xElement = MissionDialogue.Attribute("subtitle");
            if (xElement != null)
                Subtitle = xElement.Value;

            var multiplayerElement = MissionDialogue.Attribute("multiplayer");
            if (multiplayerElement != null)
            {
                bool result;
                Multiplayer = Boolean.TryParse(multiplayerElement.Value,out result);
            }
               
            var ambientElement = MissionDialogue.Attribute("ambient");
            if (ambientElement != null)
            {
                bool result;
                Ambient = Boolean.TryParse(multiplayerElement.Value,out result);
            }

            var conversationsElements = MissionDialogue.Descendants("Conversation").ToList();
            if (conversationsElements.Any())
            {
                var conversations = new List<Conversation>();
                foreach (var conv in conversationsElements)
                {
                    conversations.Add(new Conversation(conv));
          
                }
                Conversations = conversations;
            }
        }
    }
}
