﻿using System.Windows.Controls;

namespace DstarToReaperConverter
{
    /// <summary>
    /// Interaction logic for LoadingIndicator.xaml
    /// </summary>
    public partial class LoadingIndicator : UserControl
    {
        public LoadingIndicator()
        {
            InitializeComponent();
            DataContext = LoadingIndicatorVM.Instance;
        }
    }
}
