﻿using DstarToReaperConverter.Models;
using Reaper;
using RSG.Base.Logging;
using RSG.Editor.Controls.Perforce;
using RSG.Interop.Perforce;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;


namespace DstarToReaperConverter
{

    public class DstarConvertorHelper
    {

        private readonly IPerforceService2 _perforceService = new PerforceService(new Log(), "Dstar Converter", "x:", TimeSpan.FromMinutes(5));

        public Action<string> NonRecordedFileEncountered;
        public string AudioBranch { get; set; }
        public string WavsLocation { get; set; }

        public DstarConvertorHelper(string audioBranch = @"x:\depot\gta5\audio\dev_ng\ASSETS\WAVES\DLC_VINEWOOD\SCRIPTED_SPEECH\")
        {

            SecondsBetweenConv = 6;
            SecondsBetweenLine = 4;
            AudioBranch = audioBranch;


        }
        public ReaperProject ConvertToReaperProject(string fullPath, IList<string> selectedDialogueRoots, string configPath)
        {

            var serializer = new XmlSerializer(typeof(Configurations));
            StreamReader reader = new StreamReader(configPath);
            var configs = (Configurations) serializer.Deserialize(reader);
            reader.Close();

            ReaperProject reaperProject = new ReaperProject();

            Dialogue dialogue = new Dialogue(fullPath);

            //Convert xml to models

            Dictionary<int, ReaperTrack> trackDictionary = new Dictionary<int, ReaperTrack>();

            double conversationStartPosition = 0;
            
            foreach (Conversation conversation in dialogue.Conversations)
            {
                if (!selectedDialogueRoots.Contains(conversation.Root))
                { continue; }
                double cuePosition = 0;

                reaperProject.Markers.Add(new ReaperMarker
                {
                    Name = conversation.Root,
                    Position = conversationStartPosition
                 });
                Line first = null;
                foreach (var d in conversation.Lines)
                {
                    if (d.FileName == "PAUSE")
                    {
                        first = d;
                        break;
                    }
                }

                if (first != null) SecondsBetweenLine = first.PauseTime / 1000;
                foreach (Line line in conversation.Lines)
                {
                    if (line.Recorded == false)
                    {
                        NonRecordedFileEncountered(line.FileName);
                        continue;
                    }

                    if (line.PauseTime > 0)
                    {
                        SecondsBetweenLine = line.PauseTime / 1000;
                        continue;
                    }
                    string wavfile = string.Empty;

                    try
                    {
                        wavfile = WaveHelper.GetLocalWavFile(
                     line.FileName);

                    }
                    catch (Exception)
                    {
                        continue;
                    }

                    _perforceService.Sync(wavfile);

                    ReaperClip reaperClip;

                    reaperClip = new ReaperClip(line, wavfile, conversationStartPosition + cuePosition);
                    cuePosition += reaperClip.Length + SecondsBetweenLine;

                    if (!trackDictionary.ContainsKey(line.Speaker) || trackDictionary[line.Speaker] == null)
                    {
                        trackDictionary[line.Speaker] = new ReaperTrack
                        {
                            Name = configs.Characters.FirstOrDefault(d=>d.guid == line.Guid.ToString().ToUpper()).name
                             
                        };
                    }
                    if (trackDictionary[line.Speaker].Clips == null)
                    {
                        trackDictionary[line.Speaker].Clips = new List<ReaperTrack.Clip>();
                    }
                    trackDictionary[line.Speaker].Clips.Add(reaperClip);
                }

                conversationStartPosition += Math.Ceiling(cuePosition + SecondsBetweenConv);
            }

            reaperProject.Tracks = trackDictionary.Values.ToList();
            return reaperProject;
        }

        public double SecondsBetweenConv { get; set; }
        public double SecondsBetweenLine { get; set; }


        public List<Conversation> GetRootList(string fullPath)
        {
            _perforceService.Sync(fullPath);

            Dialogue dialogue = new Dialogue(fullPath);

            return dialogue.Conversations.ToList();

        }

    }
}
