﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace DstarToReaperConverter.Models
{
    public class Conversation
    {
        public bool IsPlaceHolder { get; set; }
        public string Root {get; set; }
        public string Description { get; set; }
        public bool Unrecorded { get; set; }
        public List<Line> Lines { get; set; }

        public Conversation(XElement element)
        {
          
            Root = element.Attribute("root")?.Value;
            Description = element.Attribute("description")?.Value;
            IsPlaceHolder = bool.TryParse(element.Attribute("placeholder")?.Value,out _);     

            var lineElements = element.Descendants("Line").ToList();
            if (lineElements.Any())
            {
                var lines = new List<Line>();
                foreach (var line in lineElements)
                {
                    lines.Add(new Line(line));
       
                }
                Lines = lines;
            }

            Unrecorded = !Lines.All(r => r.Recorded);
        }
            
    }
}
