﻿using DstarToReaperConverter.Models;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using Reaper;
using RSG.Editor.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPFToolLib.Extensions;

namespace DstarToReaperConverter
{
    public class MainWindowVm : INotifyPropertyChanged
    {
        private DstarConvertorHelper _dStarConvertor;
        private string _dStarPath = @"x:\gta5_dlc\mpPacks\mpVinewood\assets_ng\dialogue\";
        private string wavsLocation = @"x:\depot\gta5\audio\dev_ng\ASSETS\WAVES\";
        private string _configPath = @"x:\depot\gta5\assets_ng\Dialogue\Config\Configurations.xml";
        private bool _isLoading;
        private readonly string _saveAsPath;

        public MainWindowVm()
        {
            BrowseFileCommand = new RelayCommand<string>(SetObjectToOpenFileDialogue);
            BrowseFolderCommand = new RelayCommand<string>(SetObjectToFolderDialogue);
            SaveFileCommand = new RelayCommand(SaveAs, () => (IsPathDstar() && SelectedDialogueRoots.Count > 0));
            SelectedDialogueRoots = new List<string>();
            SelectedItemsChanged = new RelayCommand<IList>(p =>
            {
                SelectedDialogueRoots =
                    p.Cast<object>().Where(r => r is Conversation).Cast<Conversation>().Select(s => s.Root).ToList();
                SaveFileCommand.RaiseCanExecuteChanged();
            });
            DStarConvertor.NonRecordedFileEncountered += p => _nonRecordedFiles.Add(p);
            WaveHelper.CurrentAction += LoadingIndicatorVM.Instance.SetText;
            WaveHelper.StartWaveListAcquisition(AudioBranch,wavsLocation);
        }

        private bool IsPathDstar()
        {
            return (!string.IsNullOrEmpty(DStarPath) &&
                    Path.GetExtension(DStarPath)
                        .Equals(".dstar",
                            StringComparison.InvariantCultureIgnoreCase));
        }

        private List<string> _nonRecordedFiles;

        public RelayCommand SaveFileCommand { get; private set; }

        private DstarConvertorHelper DStarConvertor => _dStarConvertor ?? (_dStarConvertor = new DstarConvertorHelper());


        public string AudioBranch
        {
            get => DStarConvertor.AudioBranch;
            set
            {
                if (value.IsAudioBranch())
                {
                    DStarConvertor.AudioBranch = value;
              
                }
                PropertyChanged.Raise(this);
            }
        }

        public string WavsLocation
        {
            get => DStarConvertor.WavsLocation;
            set
            {

                DStarConvertor.WavsLocation = value;
            
                PropertyChanged.Raise(this);
                WaveHelper.StartWaveListAcquisition(AudioBranch,WavsLocation);
            }
        }

        public string DStarPath
        {
            get => _dStarPath;
            set
            {

                _dStarPath = value;
                PropertyChanged.Raise(this);
                SaveFileCommand.RaiseCanExecuteChanged();
                if (IsPathDstar())
                {
                    IsLoading = true;
                    Task getDialogueRootsTask = Task.Factory.StartNew(() =>
                   {
                       DialogueRoots =
                           DStarConvertor.GetRootList(DStarPath);

                       _allRoots = DialogueRoots;
                   });

                    getDialogueRootsTask.ContinueWith(z => IsLoading = false);

                }
                else
                {
                    DialogueRoots = new List<Conversation>();
                }
            }
        }

        public RelayCommand<string> BrowseFileCommand { get; private set; }
        public RelayCommand<string> BrowseFolderCommand { get; private set; }

        public bool IsLoading
        {
            get => _isLoading;
            set
            {
                _isLoading = value;
                PropertyChanged.Raise(this);
                PropertyChanged.Raise(this, "IsEnabled");
            }
        }

        public bool IsEnabled => !IsLoading;

        public List<Conversation> DialogueRoots
        {
            get => _dialogueRoots;

            set
            {
                _dialogueRoots = value.OrderBy(d => d.Root).ToList();
                PropertyChanged.Raise(this);
            }
        }

        public List<string> SelectedDialogueRoots
        {
            get => _selectedDialogueRoots;
            set
            {
                _selectedDialogueRoots = value;
                PropertyChanged.Raise(this);
            }
        }

        public RelayCommand<IList> SelectedItemsChanged { get; }

        private List<Conversation> _dialogueRoots;
        private List<Conversation> _allRoots;
        private List<string> _selectedDialogueRoots;

        private readonly string _searchBar = "";


        public event PropertyChangedEventHandler PropertyChanged;

        private void SetObjectToOpenFileDialogue(string propertyToSet)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            PropertyInfo propertyInfo = GetType().GetProperty(propertyToSet);
            string currentPath = (string)propertyInfo.GetValue(this);
            if (File.Exists(currentPath))
            {
                openFileDialog.InitialDirectory = Path.GetDirectoryName(currentPath);
            }
            else if (Directory.Exists(currentPath))
            {
                openFileDialog.InitialDirectory = currentPath;
            }
            openFileDialog.ShowDialog();


            propertyInfo.SetValue(this, Convert.ChangeType(openFileDialog.FileName, propertyInfo.PropertyType), null);
        }

        private void SetObjectToFolderDialogue(string propertyToSet)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            PropertyInfo propertyInfo = GetType().GetProperty(propertyToSet);
            string currentPath = (string)propertyInfo.GetValue(this);
            if (Directory.Exists(currentPath))
            {
                dialog.SelectedPath = currentPath;
            }
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                propertyInfo.SetValue(this, Convert.ChangeType(dialog.SelectedPath, propertyInfo.PropertyType), null);
            }
        }




        private void SaveAs()
        {
            _nonRecordedFiles = new List<string>();
            if (SelectedDialogueRoots != null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog
                {
                    AddExtension = true,
                    DefaultExt = ".rpp",
                    FileName = Path.GetFileNameWithoutExtension(SelectedDialogueRoots.FirstOrDefault()) ?? throw new InvalidOperationException(),
                    Filter = "Reaper projects (.rpp)|*.rpp",

                    InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                };
                bool? result = saveFileDialog.ShowDialog();
                if (result == true)
                {
                    Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            IsLoading = true;
                            ReaperProject rpp = DStarConvertor.ConvertToReaperProject(DStarPath, SelectedDialogueRoots, _configPath);
                            rpp.Save(saveFileDialog.FileName);
                            Process.Start(new ProcessStartInfo(saveFileDialog.FileName));
                            IsLoading = false;
                        }

                        catch (Exception e)
                        {
                            IsLoading = false;
                            StringBuilder message = new StringBuilder();
                            if (_nonRecordedFiles.Count > 0)
                            {
                                foreach (string file in _nonRecordedFiles)
                                {
                                    message.Append(
                                        string.Format("{0}, ", file));
                                }
                                message.Append(" have not been recorded.");
                                message.AppendLine();
                            }

                            message.AppendLine(e.Message);
                            message.AppendLine();
                            message.AppendLine(e.StackTrace);
                            Application.Current.Dispatcher.Invoke(() => RsMessageBox.Show(message.ToString(), "Errors encountered"));
                        }
                    });
                }
            }
        }


    }
}
