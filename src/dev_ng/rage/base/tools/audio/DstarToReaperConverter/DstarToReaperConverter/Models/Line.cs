﻿using System;
using System.Xml.Linq;

namespace DstarToReaperConverter.Models
{
    public class Line
    {
        public Guid Guid { get; set; }
        public string Dialogue { get; set; }
        public string Volume { get; set; }
        public int Speaker { get; set; }
        public int Listener { get; set; }
        public  string Audio { get; set; }
        public string FileName { get; set; }
        public bool Recorded { get; set; }
        public double PauseTime { get; set; }
        public Type LineType { get; set; }

        public Line(XElement element)
        {

            Guid = Guid.Parse(element.Attribute("guid")?.Value ?? throw new InvalidOperationException());
            Dialogue = element.Attribute("dialogue")?.Value;
            Volume = element.Attribute("volume")?.Value;
            Recorded = bool.TryParse(element.Attribute("recorded")?.Value,out _);
            Speaker = int.Parse(element.Attribute("speaker")?.Value);
            Listener = int.Parse(element.Attribute("listener")?.Value);
            Audio = element.Attribute("audio")?.Value;

            var name =  element.Attribute("filename")?.Value;
            if (name != null && name.Contains("SFX_"))
            {
                LineType = Type.Pause;
             
                var time = name.Replace("SFX_PAUSE_","");
                PauseTime = double.Parse(time);
                FileName = "PAUSE";
            }
            else
            {
                FileName = name;
            }

        }


        public enum Type
        {
            Pause,
            Line
        }
    }
}
