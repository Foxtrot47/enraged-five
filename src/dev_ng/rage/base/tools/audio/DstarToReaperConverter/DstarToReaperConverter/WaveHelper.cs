﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Perforce.P4;
using RSG.Base.Logging;
using RSG.Editor.Controls.Perforce;
using RSG.Interop.Perforce;
using File = Perforce.P4.File;


namespace DstarToReaperConverter
{
    public static class WaveHelper
    {
        private static string _lastAudioBranch = string.Empty;
        private static Lookup<string, string> _psWaveslist;

        private static Lookup<string, string> _waveslist;

        private static bool _working;

        public static Action<string> CurrentAction;

        #region Public methods


        public static bool IsAudioBranch(this string audioBranch)
        {
            string wavesDepotPath = Path.Combine(audioBranch, "projectSettings.xml");
            wavesDepotPath = audioBranch.Contains(":")
                ? $"//{wavesDepotPath.Replace('\\', '/').Substring(3)}"
                : wavesDepotPath.Replace('\\', '/');
            Server srv = new Server(new ServerAddress(null));
            Repository p4 = new Repository(srv);
            p4.Connection.Connect(new Options());
            try
            {
                IList<File> result =
                    p4.GetFiles(new List<FileSpec> {new FileSpec(new DepotPath(wavesDepotPath), VersionSpec.Head)},
                        new Options());
                return (result != null && result.Count > 0);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static bool _acquisitionStarted;
        private static bool _acquisitionComplete;

        public static void StartWaveListAcquisition(string branch, string wavspath)
        {
            _acquisitionStarted = true;
            _acquisitionComplete = false;
            if (!branch.ToUpper().Contains("SCRIPTED_SPEECH") ||
                _lastAudioBranch.Equals(branch, StringComparison.InvariantCultureIgnoreCase))
            {
                _acquisitionComplete = true;
                return;
            }

            _lastAudioBranch = branch;
           
            List<Task>
            getWavesTasks = new List<Task>();
            getWavesTasks.Add(
                Task.Factory.StartNew(() => GetListOfDepotWaves(wavspath)).ContinueWith(p => { _waveslist = p.Result;
                    _working = false;
                }));


            Task task = Task.WhenAll(getWavesTasks);
            task.ContinueWith(result =>
            {
                if (result.IsCompleted)
                {
                    _acquisitionComplete = true;
                 }
            });
        }

        public static string GetLocalWavFile(string clipName)
        {
            
            if (string.IsNullOrEmpty(clipName))
            {
                return string.Empty;
            }
            CurrentAction?.Invoke("Acquiring _waveslList...");
            if (!_acquisitionStarted)
            {
                throw new Exception("Acquisition of waves has not started.");
            }
            DateTime started = DateTime.Now;

            while (!_acquisitionComplete )
            {
                if(DateTime.Now > (started + TimeSpan.FromMinutes(1)))
                {
                    throw new Exception("Acquisition of waves timed out.");
                }
                Thread.Sleep(500);
            }

            CurrentAction?.Invoke($"Loading clip {clipName} ....");

            // The wav file gets postfixed with _01.
            string lineFilenameWithPostfix = clipName;
            if (!lineFilenameWithPostfix.EndsWith("01"))
            {
                lineFilenameWithPostfix = $"{lineFilenameWithPostfix}_01";
            }
            lineFilenameWithPostfix = $"{lineFilenameWithPostfix}.WAV";

            int variation;
            if (int.TryParse(clipName.Split('_').Last(), out variation) != true)
            {
                // throw new Exception(string.Format("{0} does not contain a valid variation (skipping)", clipName));
            }
            string wavesDepotPath = _waveslist[lineFilenameWithPostfix.ToUpper()].FirstOrDefault();

            if (string.IsNullOrEmpty(wavesDepotPath))
            {
                wavesDepotPath = _psWaveslist[lineFilenameWithPostfix.ToUpper()].FirstOrDefault();
            }

            if (string.IsNullOrEmpty(wavesDepotPath))
            {
                throw new FileNotFoundException($"{lineFilenameWithPostfix} couldn't be found");
            }

            IPerforceService2 service = new PerforceService(new Log(), "Dstar Converter", "x:", TimeSpan.FromMinutes(5));

            FileSpec[] fileSpecs = service.GetFileSpecs(wavesDepotPath);
            service.Sync(fileSpecs);
            PerforceResult result = service.Where(fileSpecs);


            return result.Files[0].LocalPath.Path;
        }

        #endregion

        #region Private Methods

        private static Lookup<string, string> GetListOfDepotWaves(string wavsPath, string folderPrefix = "scripted_speech")
        {
            _working = true;
            Server srv = new Server(new ServerAddress(null));
            Repository p4 = new Repository(srv);
            p4.Connection.Connect(new Options());

            GetDepotFilesCmdOptions opts =
                new GetDepotFilesCmdOptions(GetDepotFilesCmdFlags.NotDeleted, 0);
            string wavesDepotPath = Path.Combine(wavsPath,
                "dlc_vinewood");
            wavesDepotPath = wavsPath.Contains(":")
                ? $"//{wavesDepotPath.Replace('\\', '/').Substring(3)}"
                : wavesDepotPath.Replace('\\', '/');
            FileSpec fs = new FileSpec(new DepotPath(wavesDepotPath + $"/{folderPrefix}*/..."), null);

            IList<File> target = p4.GetFiles(opts, fs);
            List<string> files = target.Select(p => p.DepotPath.Path.ToUpper()).ToList();
            p4.Connection.Disconnect();
            Lookup<string, string> lookup = (Lookup<string, string>) files.ToLookup(Path.GetFileName);
            return lookup;
        }

        #endregion
    }
}
