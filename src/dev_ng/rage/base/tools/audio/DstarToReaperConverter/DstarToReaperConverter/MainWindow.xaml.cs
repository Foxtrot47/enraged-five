﻿using System.Windows;

namespace DstarToReaperConverter
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowVm();
        }

        private void SelectAll(bool select)
        {
            if (select)
            {
                DialogueRootListBox.SelectAll();
            }
            else
            {
                DialogueRootListBox.UnselectAll();
            }

            if (DataContext is MainWindowVm mainWindowVm)
            {
                mainWindowVm.SelectedItemsChanged.Execute(DialogueRootListBox.SelectedItems);
            }
        }

        private void CheckedChanged(object sender, RoutedEventArgs e)
        {
            bool check = DialogueRootListBox.SelectedItems.Count < DialogueRootListBox.Items.Count;
            SelectAll(check);
        }
    }
}

