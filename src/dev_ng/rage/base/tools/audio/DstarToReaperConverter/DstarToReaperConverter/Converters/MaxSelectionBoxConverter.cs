﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DstarToReaperConverter.Converters
{
    public class MaxSelectionBoxConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value - 150;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
