﻿namespace DstarToReaperConverter.Models
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Configurations
    {

        private ConfigurationsCharacter[] charactersField;

        private string[] voicesField;

        private string[] specialsField;

        private string versionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Character", IsNullable = false)]
        public ConfigurationsCharacter[] Characters
        {
            get { return this.charactersField; }
            set { this.charactersField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Voice", IsNullable = false)]
        public string[] Voices
        {
            get { return this.voicesField; }
            set { this.voicesField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Special", IsNullable = false)]
        public string[] Specials
        {
            get { return this.specialsField; }
            set { this.specialsField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string version
        {
            get { return this.versionField; }
            set { this.versionField = value; }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConfigurationsCharacter
    {

        private string guidField;

        private string nameField;

        private string voiceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string guid
        {
            get { return this.guidField; }
            set { this.guidField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get { return this.nameField; }
            set { this.nameField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string voice
        {
            get { return this.voiceField; }
            set { this.voiceField = value; }
        }
    }
}