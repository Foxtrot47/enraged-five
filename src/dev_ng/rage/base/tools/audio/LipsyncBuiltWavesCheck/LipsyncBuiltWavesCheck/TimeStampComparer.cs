﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LipsyncBuiltWavesChecker
{
    class TimeStampComparer
    {
        List<string> outOfDateBanks = new List<string>();

        public List<string> OutOfDateBanks
        {
            get { return outOfDateBanks; }
        }

        public void CheckAnimationFiles(FileInfo file, Dictionary<PackBank, DateTime> bankTimeStamps)
        {

            foreach (PackBank packBank in bankTimeStamps.Keys)
            {
                if (file.FullName.Contains("\\" + packBank.Bank + "\\"))
                {
                    //if the animation file is greater than the value
                    DateTime latestTime = AutoAssetManager.Instance.AssetManager.GetLatestCheckinTime(file.FullName);
                    if (latestTime.CompareTo(bankTimeStamps[packBank]) > 0)
                    {


                        outOfDateBanks.Add(packBank.Pack + "," + packBank.Bank + "," + bankTimeStamps[packBank] + "," + file.Name + "," + latestTime);
                    }
                    return;
                }
            }
        }

    }
}
