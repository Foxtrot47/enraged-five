﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace LipsyncBuiltWavesChecker
{
    internal class PackFileHelper
    {
        static public Dictionary<PackBank, DateTime> GetBankNames(FileInfo fileInfo)
        {
            Dictionary<PackBank, DateTime> bankNames = new Dictionary<PackBank, DateTime>();
            if (IsPackFile(fileInfo))
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(fileInfo.FullName);
                XmlNodeList list = xdoc.DocumentElement.SelectNodes("//Bank");
                foreach (XmlNode xmlNode in list)
                {
                    bankNames.Add(new PackBank(xdoc.DocumentElement.Attributes["name"].Value, xmlNode.Attributes["name"].Value), Convert.ToDateTime(xmlNode.Attributes["timeStamp"].Value));
                }
            }
            return bankNames;
        }

        static private bool IsPackFile(FileInfo fileInfo)
        {
            if (fileInfo.Exists && fileInfo.FullName.EndsWith("PACK_FILE.xml", StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}