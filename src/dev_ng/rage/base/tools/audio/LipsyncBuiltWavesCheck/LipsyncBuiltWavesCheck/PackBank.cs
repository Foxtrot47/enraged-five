﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LipsyncBuiltWavesChecker
{
    internal class PackBank
    {
        public string Pack { get; private set; }
        public string Bank { get; private set; }

        public PackBank(string pack, string bank)
        {
            Pack = pack;
            Bank = bank;
        }
    }
}
