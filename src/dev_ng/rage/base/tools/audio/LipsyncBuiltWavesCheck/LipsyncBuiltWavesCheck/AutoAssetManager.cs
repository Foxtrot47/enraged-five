﻿using rage;
using Rockstar.AssetManager.Infrastructure.Enums;
using Rockstar.AssetManager.Interfaces;
using Rockstar.AssetManager.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LipsyncBuiltWavesChecker
{
    internal class AutoAssetManager
    {

        private IAssetManager assetManager;
        
        
        private static AutoAssetManager instance;


      public static AutoAssetManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AutoAssetManager();
                }
                return instance;
            }
            set
            {
                instance = value;
            }
        }

        private AutoAssetManager()
            : this("")
        {
        }

        /// <summary>
        /// Constructor for p4Handler
        /// </summary>
        /// <param name="password">
        /// user password
        /// </param>
        private AutoAssetManager(string password)
        {
            audProjectList projectList = new audProjectList(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
@"\Rave\ProjectList.xml");
            if (projectList == null)
            {
                throw new Exception("It seems like rave is not installed on your system, please log on with credentials");
            }

            //Create asset manager from project list
            ConnectAssetManagerWithProjectList(projectList, password);
        }

        public IAssetManager AssetManager { get { return assetManager; } }

        public void ConnectWithCredentials(string user, string password, string workspace, string server, string host, string depotRoot)
        {
            //Create asset manager from project list
            assetManager = AssetManagerFactory.GetInstance(
    AssetManagerType.Perforce,
    null,
   server,
    workspace,
    user,
    password,
    depotRoot,
    null,
   null);
            //Check if connection worked
            bool connected = assetManager.Connect();
            if (!connected)
            {
                throw new Exception("Connection with username" + user + " to " + server + "failed");
            }
        }

        /// <summary>
        /// Creates asset manager from project list
        /// </summary>
        /// <param name="projectList">
        /// The project list (usually available in localAppData folder
        /// </param>
        /// <returns>
        /// The asset manager instance
        /// </returns>
        private void ConnectAssetManagerWithProjectList(audProjectList projectList, string userPassword)
        {
            ConnectWithCredentials(projectList.Projects[0].UserName, userPassword, projectList.Projects[0].AssetProject, projectList.Projects[0].AssetServer, projectList.Projects[0].AssetManager, projectList.Projects[0].DepotRoot);
        }
    }
}
