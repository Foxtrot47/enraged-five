﻿using System;
using System.Collections.Generic;
using System.IO;
using rage.ToolLib.CmdLine;

namespace LipsyncBuiltWavesChecker
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length < 4)
            {
                Console.WriteLine("Please use arguments -animFolder -fileExtensions -builtWavesFolder -csvFile to run this script");
                return;
            }
            CmdLineParser cmdParser = new CmdLineParser(args);           

            string animFolder = cmdParser.Arguments["animFolder"];
            string[] fileExtensions = cmdParser.Arguments["fileExtensions"].Split(new string[] {"AND", "OR" }, StringSplitOptions.RemoveEmptyEntries);
            string packFolder = cmdParser.Arguments["builtWavesFolder"];
            string csvFile = cmdParser.Arguments["csvFile"];
            if (animFolder == null || fileExtensions == null || packFolder == null || csvFile == null)
            {
                Console.WriteLine("Please use arguments -animFolder -fileExtensions -builtWavesFolder -csvFile to run this script");
                return;
            }
            csvFile = Path.ChangeExtension(csvFile, ".csv");
            
            Console.WriteLine("Getting all {0} in {1}", string.Join(", ", fileExtensions), animFolder);
            AutoAssetManager.Instance.AssetManager.GetLatest(packFolder, false);

            RecursiveFileSearch.WalkDirectoryTree(new DirectoryInfo(animFolder), fileExtensions);
            Dictionary<PackBank, DateTime> bankTimeStamps = new Dictionary<PackBank, DateTime>();

            Console.WriteLine("Getting all pack files in: {0}", packFolder);

            foreach (string file in Directory.GetFiles(packFolder))
            {
                foreach (KeyValuePair<PackBank, DateTime> kvp in PackFileHelper.GetBankNames(new FileInfo(file)))
                {
                    bankTimeStamps[kvp.Key] = kvp.Value;
                }
            }

            TimeStampComparer timeStampComparer = new TimeStampComparer();

            for (int i = 0; i < RecursiveFileSearch.filesInDirectory.Count; i++)
            {
                FileInfo file = RecursiveFileSearch.filesInDirectory[i];
                Console.Write("\r{1} % Checking file: {0} ........................", file.Name, (((float)i / (float)RecursiveFileSearch.filesInDirectory.Count) * 100).ToString("0.00"));

                timeStampComparer.CheckAnimationFiles(file, bankTimeStamps);
            }
            
            Console.WriteLine("\nWriting to {0}", csvFile);
            string joined = string.Join("\n", timeStampComparer.OutOfDateBanks.ToArray());
            
            try
            {
                File.WriteAllText(csvFile, joined);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}