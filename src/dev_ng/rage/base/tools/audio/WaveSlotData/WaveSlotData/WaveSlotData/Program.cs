﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Wave slot data generator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WaveSlotData
{
    using WaveSlotDataLib;

    /// <summary>
    /// The program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Run the wave slot data generator.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            var path = @"X:\gta5\audio\dev\RUNTIME\PS3\CONFIG\AUDIOCONFIG.DAT4"; // TODO get from args - do not commit
            var output = @"c:\temp\waveslotdata.xml";
            var bigEndian = true; // TODO - get from projectSettings

            var generator = new WaveSlotDataGenerator("PS3", bigEndian, path, output);
            generator.Run();
        }
    }
}
