﻿// -----------------------------------------------------------------------
// <copyright file="Generator.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

using System.Text;

namespace WaveSlotDataLib
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    using MetadataLib;

    using rage.ToolLib;

    using WaveSlotDataLib.Enums;

    using BinaryReader = rage.ToolLib.Reader.BinaryReader;

    /// <summary>
    /// Wave slot data generator.
    /// </summary>
    public class WaveSlotDataGenerator
    {
        /// <summary>
        /// The Error description.
        /// </summary>
        private StringBuilder error = new StringBuilder();

        /// <summary>
        /// The zero hash.
        /// </summary>
        private readonly uint zeroHash;

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveSlotDataGenerator"/> class.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="bigEndian">
        /// The big endian.
        /// </param>
        /// <param name="inputPath">
        /// The input path.
        /// </param>
        /// <param name="outputPath">
        /// The output path.
        /// </param>
        /// <param name="inputStream">
        /// The input stream.
        /// </param>
        public WaveSlotDataGenerator(string schemaPathLocal, string packListPathLocal, bool bigEndian, string inputPath, string outputPath=null)
        {

            if (string.IsNullOrEmpty(inputPath))
            {
                throw new Exception("inputPath is not set");
            }
            if (string.IsNullOrEmpty(schemaPathLocal))
            {
                throw new Exception("schemaPathLocal is not set");
            }
            if (string.IsNullOrEmpty(packListPathLocal))
            {
                throw new Exception("packListPathLocal is not set");
            }

            if (!File.Exists(inputPath))
            {
                throw new Exception("Failed to find file: " + inputPath);
            }
            if (!File.Exists(schemaPathLocal))
            {
                throw new Exception("Failed to find file: " + schemaPathLocal);
            }
            if (!File.Exists(packListPathLocal))
            {
                throw new Exception("Failed to find file: " + packListPathLocal);
            }

            this.InputPath = inputPath;
            this.SchemaPathLocal = schemaPathLocal;
            this.PackListPathLocal = packListPathLocal;
            this.OutputPath = outputPath;
            this.BigEndian = bigEndian;

            this.MetadataFile = new MetadataFile(this.InputPath, this.BigEndian, this.SchemaPathLocal);

            var hash = new Hash { Value = "0" };
            this.zeroHash = hash.Key;
        }


        /// <summary>
        /// Gets the pack list path.
        /// </summary>
        public string PackListPathLocal { get; private set; }

        /// <summary>
        /// Gets the schema path.
        /// </summary>
        public string SchemaPathLocal { get; private set; }

        /// <summary>
        /// Gets the input path.
        /// </summary>
        public string InputPath { get; private set; }

        /// <summary>
        /// Gets the output path.
        /// </summary>
        public string OutputPath { get; private set; }

        /// <summary>
        /// Gets a value indicating whether big endian.
        /// </summary>
        public bool BigEndian { get; private set; }

        /// <summary>
        /// Gets the banks.
        /// </summary>
        public IDictionary<uint, string> Banks { get; private set; } 

        /// <summary>
        /// Gets the metadata file.
        /// </summary>
        public MetadataFile MetadataFile { get; private set; }

        /// <summary>
        /// Gets the wave slot objects.
        /// </summary>
        public IDictionary<uint, MetadataObject> WaveSlotObjects { get; private set; }

        /// <summary>
        /// Gets the wave slots object.
        /// </summary>
        public MetadataObject WaveSlotsObject { get; private set; }

        /// <summary>
        /// Run the wave slot data generator.
        /// </summary>
        /// <returns>
        /// Populated XDocument result <see cref="XDocument"/>.
        /// </returns>
        public XDocument Run()
        {
            if (!this.LoadWaveBanks(this.PackListPathLocal))
            {
                string errorDescription = "Failed to load wave bank data";
                error.AppendLine(errorDescription);
                Console.WriteLine(errorDescription);
                return null;
            }

            // Get the WaveSlots object from the data file
            MetadataObject waveSlotsObject;
            if (!this.MetadataFile.ObjectLookup.TryGetValue("WAVESLOTSLIST", out waveSlotsObject))
            {
                string errorDescription = "Failed to find WaveSlots object in metadata file";
                error.AppendLine(errorDescription);
                Console.WriteLine(errorDescription);
                return null;
            }

            this.WaveSlotsObject = waveSlotsObject;

            if (!this.LoadWaveSlotObjects())
            {
                string errorDescription = "Failed to load wave slot objects";
                error.AppendLine(errorDescription);
                Console.WriteLine(errorDescription);
                return null;
            }

            var doc = this.GenerateResult();
            if(doc==null) throw new Exception("Couldn't generate result: "+this.error);

            if (!string.IsNullOrEmpty(this.OutputPath))
            {
                doc.Save(this.OutputPath);
            }

            return doc;
        }

        /// <summary>
        /// Load the wave banks.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadWaveBanks(string packListPath)
        {
            this.Banks = new Dictionary<uint, string>();

            if (!File.Exists(packListPath))
            {
                string errorDescription = "Failed to find pack list file: " + packListPath;
                error.AppendLine(errorDescription);
                Console.WriteLine(errorDescription);
                return false;
            }

            var packFileDir = new FileInfo(packListPath).Directory.FullName;

            var packListDoc = XDocument.Load(packListPath);
            foreach (var packFileElem in packListDoc.Descendants("PackFile"))
            {
                var packFilePath = Path.Combine(packFileDir, packFileElem.Value);
                if (!File.Exists(packFilePath))
                {
                    string errorDescription = "Failed to find pack file: " + packFilePath;
                    error.AppendLine(errorDescription);
                    Console.WriteLine(errorDescription);
                    return false;
                }

                var packDoc = XDocument.Load(packFilePath);

                foreach (var packNode in packDoc.Descendants("Pack"))
                {
                    // Get pack name
                    var packNameAttr = packNode.Attribute("name");
                    if (null == packNameAttr)
                    {
                        string errorDescription = "Failed to find pack name attribute";
                        error.AppendLine(errorDescription);
                        Console.WriteLine(errorDescription);
                        return false;
                    }

                    var packName = packNameAttr.Value;

                    // Find all banks in pack
                    foreach (var bankElem in packNode.Descendants("Bank"))
                    {
                        var bankNameAttr = bankElem.Attribute("name");
                        if (null == bankNameAttr)
                        {
                            string errorDescription = "Failed to find bank name attribute";
                            error.AppendLine(errorDescription);
                            Console.WriteLine(errorDescription);
                            return false;
                        }

                        var bankName = bankNameAttr.Value;

                        // Add to banks list
                        var combinedName = packName + "\\" + bankName;
                        var hash = new Hash { Value = combinedName };
                        this.Banks[hash.Key] = combinedName;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Load the wave slot objects.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadWaveSlotObjects()
        {
            this.WaveSlotObjects = new Dictionary<uint, MetadataObject>();

            foreach (var obj in this.MetadataFile.Objects)
            {
                if (obj.TypeName == "Slot")
                {
                    var hash = new Hash { Value = obj.Name };
                    this.WaveSlotObjects[hash.Key] = obj;
                }
            }

            return true;
        }

        /// <summary>
        /// Generate the slot details.
        /// </summary>
        /// <returns>
        /// Populated result XDocument <see cref="XDocument"/>.
        /// </returns>
        private XDocument GenerateResult()
        {
            var doc = new XDocument();
            var rootElem = new XElement("WaveSlots");
            doc.Add(rootElem);

            // Write slot details to file
            var stream = new MemoryStream(this.WaveSlotsObject.Data);
            var reader = new BinaryReader(stream, this.BigEndian);

            var offset = reader.ReadUInt();
            var flags = reader.ReadUInt();
            var numSlots = reader.ReadUInt();

            while (reader.BinReader.PeekChar() != -1)
            {
                var hash = reader.ReadUInt();
                MetadataObject obj;

                if (!this.WaveSlotObjects.TryGetValue(hash, out obj))
                {
                    string errorDescription = "Failed to find object with hash: " + hash;
                    error.AppendLine(errorDescription);
                    Console.WriteLine(errorDescription);
                    return null;
                }

                if (!this.WriteObjectToFile(obj, rootElem))
                {
                    string errorDescription = "Failed to write object to file: " + obj.Name;
                    error.AppendLine(errorDescription);
                    Console.WriteLine(errorDescription);
                    return null;
                }
            }

            return doc;
        }

        /// <summary>
        /// Write an object to file.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        /// <param name="rootElem">
        /// The root element.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool WriteObjectToFile(MetadataObject obj, XElement rootElem)
        {
            var stream = new MemoryStream(obj.Data);
            var reader = new BinaryReader(stream, this.BigEndian);

            var offset = reader.ReadUInt();
            var flags = reader.ReadUInt();

            var loadTypeEnum = reader.ReadByte();
            reader.ReadBytes(3, this.BigEndian); // Padding
            var maxHeaderSize = reader.ReadUInt();
            var size = reader.ReadUInt();
            var staticBank = reader.ReadUInt();
            var maxMetadataSize = reader.ReadUInt();
            var maxDataSize = reader.ReadUInt();

            var slotElem = new XElement("Slot");
            rootElem.Add(slotElem);

            var nameElem = new XElement("Name");
            slotElem.Add(nameElem);
            nameElem.SetAttributeValue("content", "ascii");
            nameElem.Value = obj.Name;

            var loadTypeElem = new XElement("LoadType");
            slotElem.Add(loadTypeElem);
            loadTypeElem.SetAttributeValue("content", "ascii");
            loadTypeElem.Value = Enum.ToObject(typeof(SlotLoadType), loadTypeEnum).ToString();

            var maxHeaderSizeElem = new XElement("MaxHeaderSize");
            slotElem.Add(maxHeaderSizeElem);
            maxHeaderSizeElem.SetAttributeValue("value", maxHeaderSize);

            var maxMetadataSizeElem = new XElement("MaxMetadataSize");
            slotElem.Add(maxMetadataSizeElem);
            maxMetadataSizeElem.SetAttributeValue("value", maxMetadataSize);

            var maxDataSizeElem = new XElement("MaxDataSize");
            slotElem.Add(maxDataSizeElem);
            maxDataSizeElem.SetAttributeValue("value", maxDataSize);

            var sizeElem = new XElement("Size");
            slotElem.Add(sizeElem);
            sizeElem.SetAttributeValue("value", size);

            if (staticBank != 0 && staticBank != this.zeroHash)
            {
                try
                {
                    string bankpath;
                    if (!this.Banks.TryGetValue(staticBank, out bankpath))
                    {
                        string errorDescription = "Failed to find static bank path for hash: " + staticBank;
                        error.AppendLine(errorDescription);
                        Console.WriteLine(errorDescription);
                        return false;
                    }

                    var staticBankElem = new XElement("StaticBank");
                    slotElem.Add(staticBankElem);
                    staticBankElem.SetAttributeValue("content", "ascii");
                    staticBankElem.Value = bankpath;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
