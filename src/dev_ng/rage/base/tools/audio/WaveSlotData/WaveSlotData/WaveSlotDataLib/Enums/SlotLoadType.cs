﻿// -----------------------------------------------------------------------
// <copyright file="SlotLoadType.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace WaveSlotDataLib.Enums
{
    /// <summary>
    /// Slot load type enum.
    /// </summary>
    public enum SlotLoadType
    {
        BANK = 0,
        
        WAVE = 1,

        STREAM = 2,
    }
}
