﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage
{
    class WaveSlotAssociationException: Exception
    {
        private string p;

        public WaveSlotAssociationException(string message): base(message) { }
    }
}
