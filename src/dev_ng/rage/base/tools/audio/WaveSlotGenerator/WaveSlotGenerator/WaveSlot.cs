using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using rage.ToolLib;
using rage.ToolLib.Logging;

namespace rage
{
    public class WaveSlot
    {
        private readonly IList<Loadable> m_loadableElements;
        private readonly IDictionary<string, SlotTag> m_slotTags; 
        private int m_noOfLoadableBanks;
        private int m_numElements;
        private string m_staticBankPath;
        private long m_totalSize;

        public WaveSlot(ILog log, XElement slotElement, XDocument builtWaves, audProjectSettings projectSettings)
        {
            if (slotElement == null)
            {
                throw new ArgumentNullException("slotElement");
            }
            if (builtWaves == null)
            {
                throw new ArgumentNullException("builtWaves");
            }

            var nameAttrib = slotElement.Attribute("name");
            if (nameAttrib == null)
            {
                throw new FormatException("Slot does not have a \"name\" attribute");
            }
            Name = nameAttrib.Value;

            var loadTypeAttrib = slotElement.Attribute("loadType");
            if (loadTypeAttrib == null)
            {
                throw new FormatException("Slot does not have a \"loadType\" attribute");
            }
            LoadType = Enum<LoadType>.Parse(loadTypeAttrib.Value);

            m_loadableElements =
                slotElement.Descendants("Loadable").Select(
                    loadableElement => new Loadable(loadableElement, builtWaves, Name)).ToList();

            this.m_slotTags = new Dictionary<string, SlotTag>();
            foreach (var slotTagElem in slotElement.Descendants("Tag"))
            {
                var slotTag = new SlotTag(slotTagElem);

                if (slotTag.Platform == projectSettings.GetCurrentPlatform().PlatformTag)
                {
                    this.m_slotTags[slotTag.Key] = slotTag;
                }
            }

            Build(log);
        }

        public string Name { get; private set; }
        public int MaxHeaderSize { get; private set; }
        public uint MaxMetadataSize { get; private set; }
        public uint MaxDataSize { get; private set; }
        public int Size { get; private set; }
        public LoadType LoadType { get; private set; }
        public List<KeyValuePair<string, int>> BankHeaders { get; private set; }
        public KeyValuePair<string, int> LargestElement { get; private set; }
        public List<WaveSlotEntry> Elements { get; private set; }

        public long AverageSize
        {
            get
            {
                if (m_numElements != 0)
                {
                    return m_totalSize / m_numElements;
                }
                return 0;
            }
        }

        public void SerializeAsMetadataObject(XElement parentElement)
        {
            if (m_noOfLoadableBanks > 0)
            {
                var maxHeaderSize = this.m_slotTags.ContainsKey("HeaderSize")
                                        ? Math.Max(int.Parse(this.m_slotTags["HeaderSize"].Value), MaxDataSize)
                                        : MaxHeaderSize;

                var maxMetadataSize = this.m_slotTags.ContainsKey("MetadataSize")
                                        ? Math.Max(uint.Parse(this.m_slotTags["MetadataSize"].Value), MaxMetadataSize)
                                        : MaxMetadataSize;

                var maxDataSize = this.m_slotTags.ContainsKey("DataSize")
                                        ? Math.Max(int.Parse(this.m_slotTags["DataSize"].Value), MaxDataSize)
                                        : MaxDataSize;

                var slotElement = new XElement("Slot",
                                               new XAttribute("name", Name),
                                               new XElement("LoadType", string.Format("SLOT_LOAD_TYPE_{0}", LoadType)),
                                               new XElement("MaxHeaderSize", maxHeaderSize),
                                               new XElement("MaxMetadataSize", maxMetadataSize),
                                               new XElement("MaxDataSize", maxDataSize),
                                               new XElement("Size", Size));

                if (m_noOfLoadableBanks == 1 &&
                    LoadType == LoadType.BANK)
                {
                    slotElement.Add(new XElement("StaticBank", m_staticBankPath));
                }
                parentElement.Add(slotElement);
            }
        }

        public void SerializeAsWaveSlotObject(XElement parentElement)
        {
            if (m_noOfLoadableBanks > 0)
            {
                var slotElement = new XElement("Slot",
                                               new XElement("Name", new XAttribute("content", "ascii"), Name),
                                               new XElement("LoadType",
                                                            new XAttribute("content", "ascii"),
                                                            LoadType.ToString()),
                                               new XElement("MaxHeaderSize", new XAttribute("value", MaxHeaderSize)),
                                               new XElement("MaxMetadataSize", new XAttribute("value", MaxMetadataSize)),
                                               new XElement("MaxDataSize", new XAttribute("value", MaxDataSize)),
                                               new XElement("Size", new XAttribute("value", Size)));

                if (m_noOfLoadableBanks == 1 &&
                    LoadType == LoadType.BANK)
                {
                    slotElement.Add(new XElement("StaticBank", new XAttribute("content", "ascii"), m_staticBankPath));
                }
                parentElement.Add(slotElement);
            }
        }

        private void Build(ILog log)
        {
            BankHeaders = new List<KeyValuePair<string, int>>();
            Elements = new List<WaveSlotEntry>();

            m_staticBankPath = string.Empty;

            var banks = new List<Bank>();
            foreach (var loadableObject in m_loadableElements)
            {
                try
                {
                    banks.AddRange(loadableObject.GetLoadableBanks());
                }
                catch (WaveSlotAssociationException e) 
                {
                    log.Warning("WaveSlot association exception occurred: "+e.Message);
                }
            }
            banks = banks.Distinct(new BankComparer()).ToList();

            if (banks.Count() == 0) throw new EmptyWaveslotException();

            m_noOfLoadableBanks = banks.Count;
            if (m_noOfLoadableBanks == 1)
            {
                m_staticBankPath = banks[0].Path;
            }

            foreach (var bank in banks)
            {
                BankHeaders.Add(new KeyValuePair<string, int>(bank.Name, bank.HeaderSize));
                if (bank.HeaderSize > MaxHeaderSize)
                {
                    MaxHeaderSize = bank.HeaderSize;
                }
            }

            var largestName = string.Empty;
            var largestSize = 0;
            switch (LoadType)
            {
                case LoadType.STREAM:
                    {
                        uint maxMetadataSize = 0;
                        int maxStreamingBlockBytes = 0;
                        foreach (var bank in banks)
                        {
                            if (bank.StreamingBlockBytes > maxStreamingBlockBytes)
                                maxStreamingBlockBytes = bank.StreamingBlockBytes;
                        }

                        foreach (var bank in banks)
                        {
                            if (bank.StreamingBlockBytes == 0 ||
                                bank.StreamingBlockBytes % Bank.BLOCK_SIZE != 0)
                            {
                                throw new ApplicationException(
                                    string.Format("Bank - \"{0}\" contains an invalid StreamingBlockByteTag.", bank.Path));
                            }

                            var streamSize = (maxStreamingBlockBytes * 2) + bank.HeaderSize;

                            if (streamSize > largestSize)
                            {
                                largestName = bank.Name;
                                largestSize = streamSize;
                            }

                            if (bank.MetadataSize > maxMetadataSize)
                            {
                                maxMetadataSize = bank.MetadataSize;
                            }

                            m_totalSize += streamSize;
                            m_numElements += 1;

                            var entry = new WaveSlotEntry { Path = bank.Name, FullSize = streamSize };
                            Elements.Add(entry);
                        }
                        Size = largestSize;
                        MaxMetadataSize = maxMetadataSize;
                        break;
                    }
                case LoadType.BANK:
                    {
                        var maxBuiltSize = 0;
                        uint maxMetadataSize = 0;
                        foreach (var bank in banks)
                        {
                            var bankPath = string.Concat(bank.Pack, "/", bank.Name);

                            if (bank.BuiltSize > maxBuiltSize)
                            {
                                maxBuiltSize = bank.BuiltSize;
                            }

                            var bankSize = bank.BuiltSize + bank.HeaderSize;
                            if (bankSize > largestSize)
                            {
                                largestName = bankPath;
                                largestSize = bankSize;
                            }

                            if (bank.MetadataSize > maxMetadataSize)
                            {
                                maxMetadataSize = bank.MetadataSize;
                            }

                            m_totalSize += bankSize;
                            m_numElements += 1;

                            var entry = new WaveSlotEntry { Path = bankPath, FullSize = bankSize };
                            Elements.Add(entry);
                        }
                        Size = maxBuiltSize;
                        MaxMetadataSize = maxMetadataSize;
                        break;
                    }
                case LoadType.WAVE:
                    {
                        uint maxMetadataSize = 0;
                        uint maxDataSize = 0;
                        foreach (var bank in banks)
                        {
                            m_totalSize += bank.HeaderSize;
                            foreach (var wave in bank.Waves)
                            {
                                var wavePath = string.Concat(bank.Pack, "/", bank.Name, "/", wave.Name);

                                var loadedWaveSize = wave.Size + bank.HeaderSize;
                                if (loadedWaveSize > largestSize)
                                {
                                    largestName = wavePath;
                                    largestSize = loadedWaveSize;
                                }

                                if (wave.MetadataSize > maxMetadataSize)
                                {
                                    maxMetadataSize = wave.MetadataSize;
                                }

                                if (wave.DataSize > maxDataSize)
                                {
                                    maxDataSize = wave.DataSize;
                                }

                                m_totalSize += wave.Size;
                                m_numElements += 1;

                                var entry = new WaveSlotEntry
                                {
                                    Path = wavePath,
                                    FullSize = loadedWaveSize,
                                    WaveSize = wave.Size,
                                    HeaderSize = bank.HeaderSize
                                };
                                Elements.Add(entry);
                            }
                        }
                        Size = largestSize;
                        MaxMetadataSize = maxMetadataSize;
                        MaxDataSize = maxDataSize;
                        break;
                    }
            }

            Elements.Sort((x, y) => x.FullSize < y.FullSize ? 1 : (x.FullSize > y.FullSize ? -1 : 0));
            if (Elements.Count > 50)
            {
                Elements = new List<WaveSlotEntry>(Elements.Take(50));
            }
            LargestElement = new KeyValuePair<string, int>(largestName, largestSize);
        }
    }

    public struct WaveSlotEntry
    {
        public string Path { get; set; }

        public int FullSize { get; set; }

        public int? HeaderSize { get; set; }

        public int? WaveSize { get; set; }
    }
}
