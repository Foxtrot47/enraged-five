namespace rage
{
    using System;
    using System.Linq;
    using System.Xml.Linq;

    using rage.Generator;
    using rage.ToolLib.Logging;
    using rage.Types;

    public class WaveSlotGenerator : IGenerator
    {
        private audProjectSettings m_projectSettings;
        private string m_waveSlotSettingsPath;

        #region IGenerator Members

        public bool Init(ILog log, string workingPath, audProjectSettings projectSettings, ITypeDefinitionsManager typeDefManager, params string[] args)
        {
            m_projectSettings = projectSettings;
            m_waveSlotSettingsPath = string.Concat(workingPath, m_projectSettings.GetWaveSlotSettings());
            return true;
        }

        public XDocument Generate(ILog log, XDocument inputDoc)
        {
            try
            {
                var waveSlotSettings = new WaveSlotSettings(log, XDocument.Load(m_waveSlotSettingsPath), inputDoc, m_projectSettings);
                var totalSize = waveSlotSettings.Slots.Sum(waveSlot => waveSlot.Size);
                if (totalSize <= m_projectSettings.GetCurrentPlatform().MaxWaveMemory)
                {
                    var root = new XElement("Objects");
                    foreach (var waveSlot in waveSlotSettings.Slots)
                    {
                        waveSlot.SerializeAsMetadataObject(root);
                    }
                    return new XDocument(root);
                }

                log.Error("Wave memory limit exceeded! Memory limit is: {0} bytes. Memory used: {1} bytes.",
                          m_projectSettings.GetCurrentPlatform().MaxWaveMemory,
                          totalSize);
            }
            catch (Exception ex)
            {
                log.Exception(ex);
            }
            return null;
        }

        public void Shutdown()
        {
            m_projectSettings = null;
            m_waveSlotSettingsPath = null;
        }

        #endregion
    }
}