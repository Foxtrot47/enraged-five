using System.Collections.Generic;

namespace rage
{
    public class BankComparer : IEqualityComparer<Bank>
    {
        #region IEqualityComparer<Bank> Members

        public bool Equals(Bank x, Bank y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null) ||
                ReferenceEquals(y, null))
            {
                return false;
            }

            return x.Path == y.Path;
        }

        public int GetHashCode(Bank obj)
        {
            if (ReferenceEquals(obj, null))
            {
                return 0;
            }

            return obj.Path == null ? 0 : obj.Path.GetHashCode();
        }

        #endregion
    }
}