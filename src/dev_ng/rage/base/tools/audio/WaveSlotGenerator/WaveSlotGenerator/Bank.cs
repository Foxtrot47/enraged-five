using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace rage
{
    public class Bank
    {
        internal const int BLOCK_SIZE = 2048;

        public Bank(XElement bankElement)
        {
            if (bankElement == null)
            {
                throw new ArgumentNullException("bankElement");
            }

            ParseWaves(bankElement);
            ParseBankInfo(bankElement);
            StreamingBlockBytes = FindStreamingBlockByteTag(bankElement);
        }

        public string Name { get; private set; }
        public string Pack { get; private set; }
        public int HeaderSize { get; private set; }
        public uint MetadataSize { get; private set; }
        public int BuiltSize { get; private set; }
        public string Path { get; private set; }
        public int StreamingBlockBytes { get; private set; }
        public List<Wave> Waves { get; private set; }

        private void ParseBankInfo(XElement bankElement)
        {
            var nameAttrib = bankElement.Attribute("name");
            if (nameAttrib == null)
            {
                throw new FormatException("Bank does not have a \"name\" attribute");
            }
            Name = nameAttrib.Value;

            var packElement = bankElement.Ancestors("Pack").FirstOrDefault();
            if (packElement == null)
            {
                throw new FormatException(string.Format("Bank - \"{0}\" does not have a Pack ancestor", Name));
            }
            nameAttrib = packElement.Attribute("name");
            if (nameAttrib == null)
            {
                throw new FormatException("Pack does not have a \"name\" attribute");
            }
            Path = string.Concat(nameAttrib.Value, "\\", Name);

            var headerSizeAttrib = bankElement.Attribute("headerSize");
            if (headerSizeAttrib == null)
            {
                throw new FormatException(string.Format("Bank - \"{0}\" does not have a \"headerSize\" attribute", Path));
            }
            var headerSize = int.Parse(headerSizeAttrib.Value);
            HeaderSize = (headerSize % BLOCK_SIZE) == 0
                             ? headerSize
                             : (headerSize + (BLOCK_SIZE - (headerSize % BLOCK_SIZE)));

            var builtSizeAttrib = bankElement.Attribute("builtSize");
            if (builtSizeAttrib == null)
            {
                throw new FormatException(string.Format("Bank - \"{0}\" does not have a \"builtSize\" attribute", Path));
            }
            BuiltSize = int.Parse(builtSizeAttrib.Value);

            MetadataSize = (uint)(ToolLib.Utility.CalculateMetadataSize(bankElement) + Waves.Sum(wave => wave.MetadataSize));

            this.SetPackName(bankElement);
        }

        private void ParseWaves(XContainer bankElement)
        {
            Waves = new List<Wave>();
            foreach (var waveNode in bankElement.Descendants("Wave"))
            {
                try
                {
                    Waves.Add(new Wave(waveNode));
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Failed to parse Wave in Bank - \"{0}\"", Path), ex);
                }
            }
        }

        private void SetPackName(XElement bankElement)
        {
            this.Pack = string.Empty;

            var parent = bankElement.Parent;
            while (null != parent)
            {
                if (parent.Name.LocalName.ToLower() == "pack")
                {
                    var nameAttr = parent.Attribute("name");
                    if (null != nameAttr)
                    {
                        this.Pack = nameAttr.Value;
                    }

                    break;
                }

                parent = parent.Parent;
            }
        }

        private static int FindStreamingBlockByteTag(XElement bankElement)
        {
            foreach (var element in bankElement.AncestorsAndSelf())
            {
                foreach (var tag in element.Elements("Tag"))
                {
                    var nameAttrib = tag.Attribute("name");
                    if (nameAttrib != null &&
                        string.Compare(nameAttrib.Value, "streamingBlockBytes", true) == 0)
                    {
                        var valueAttrib = tag.Attribute("value");
                        if (valueAttrib != null)
                        {
                            return int.Parse(valueAttrib.Value);
                        }
                    }
                }
            }
            return 0;
        }
    }
}