using System;
using System.Collections.Generic;
using System.Xml.Linq;
using rage.ToolLib.Logging;

namespace rage
{
    public class WaveSlotSettings
    {
        public WaveSlotSettings(ILog log, XContainer slotSettings, XDocument builtWaves, audProjectSettings projectSettings)
        {
            var slots = new List<WaveSlot>();
            var slotNames = new HashSet<string>();
            foreach (var slotElement in slotSettings.Descendants("Slot"))
            {
                var nameAttrib = slotElement.Attribute("name");
                if (nameAttrib == null)
                {
                    throw new FormatException("Slot element doesn't have a \"name\" attribute.");
                }

                var slotName = nameAttrib.Value;
                if (slotNames.Contains(slotName))
                {
                    throw new ApplicationException(string.Format("Found more than one Slot with the name - \"{0}\".",
                                                                 slotName));
                }
                try
                {
                    WaveSlot slot = new WaveSlot(log, slotElement, builtWaves, projectSettings);
                    slots.Add(slot);
                    slotNames.Add(slotName);
                }
                catch (EmptyWaveslotException ex) 
                {
                    log.Warning("WaveSlog " + slotName + " has no associations - slot will be ignored.");    
                }
            }
            Slots = slots;
        }

        public List<WaveSlot> Slots { get; private set; }
    }
}