using System;
using System.Xml.Linq;

namespace rage
{
    public class Wave
    {
        private readonly bool m_hasBuiltSizeAttrib;
        private readonly int m_size;

        public Wave(XElement waveElement)
        {
            if (waveElement == null)
            {
                throw new ArgumentNullException("waveElement");
            }

            var nameAttrib = waveElement.Attribute("name");
            if (nameAttrib == null ||
                string.IsNullOrWhiteSpace(nameAttrib.Value))
            {
                throw new FormatException("Wave element does not have a \"name\" attribute.");
            }
            Name = nameAttrib.Value;

            var builtSizeAttrib = waveElement.Attribute("builtSize");
            m_hasBuiltSizeAttrib = builtSizeAttrib != null;
            if (m_hasBuiltSizeAttrib)
            {
                m_size = int.Parse(builtSizeAttrib.Value);
            }

            MetadataSize = ToolLib.Utility.CalculateMetadataSize(waveElement);

            // Find the data chunk and extract the size
            const string NAME = "name";
            const string SIZE = "size";
            foreach (var chunkElement in waveElement.Elements("Chunk"))
            {
                var chunkNameAttrib = chunkElement.Attribute(NAME);
                if (chunkNameAttrib == null)
                {
                    throw new FormatException(String.Format(
                        "{0} \"{1}\" contains a chunk without a \"{2}\" attribute.", waveElement.Name, waveElement.Attribute(NAME).Value, NAME));
                }

                if (String.Compare("DATA", chunkNameAttrib.Value, true) == 0)
                {
                    var chunkSizeAttrib = chunkElement.Attribute(SIZE);
                    if (chunkSizeAttrib == null)
                    {
                        throw new FormatException(String.Format("{0} \"{1}\" contains a chunk without a \"{2}\" attribute.", waveElement.Name, waveElement.Attribute(NAME).Value, SIZE));
                    }
                    DataSize = uint.Parse(chunkSizeAttrib.Value);
                    break;
                }
            }
        }

        public string Name { get; private set; }

        public int Size
        {
            get
            {
                if (!m_hasBuiltSizeAttrib)
                {
                    throw new ApplicationException(
                        string.Format("Querying built size of wave: {0} with no \"builtSize\" attribute.", Name));
                }
                return m_size;
            }
        }

        public uint MetadataSize { get; private set; }
        public uint DataSize { get; private set; }
    }
}