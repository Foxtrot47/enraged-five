﻿// -----------------------------------------------------------------------
// <copyright file="SlotTag.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace rage
{
    using System.Xml.Linq;

    /// <summary>
    /// Slot tag.
    /// </summary>
    public class SlotTag
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SlotTag"/> class.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public SlotTag(XElement element)
        {
            this.Key = element.Attribute("key").Value;
            this.Value = element.Attribute("value").Value;
            this.Platform = element.Attribute("platform").Value;
        }

        /// <summary>
        /// Gets the key.
        /// </summary>
        public string Key { get; private set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// Gets the platform.
        /// </summary>
        public string Platform { get; private set; }
    }
}
