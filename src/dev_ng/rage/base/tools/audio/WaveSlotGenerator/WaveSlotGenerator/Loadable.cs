using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using rage.ToolLib;

namespace rage
{
    public class Loadable
    {
        private readonly XDocument m_builtWaves;
        private readonly string m_slotName;

        public Loadable(XElement loadableElement, XDocument builtWaves, string slotName)
        {
            if (loadableElement == null)
            {
                throw new ArgumentNullException("loadableElement");
            }
            if (builtWaves == null)
            {
                throw new ArgumentNullException("builtWaves");
            }
            if (string.IsNullOrEmpty(slotName))
            {
                throw new ArgumentNullException("slotName");
            }

            m_builtWaves = builtWaves;
            m_slotName = slotName;

            var nameAttrib = loadableElement.Attribute("name");
            if (nameAttrib == null)
            {
                throw new FormatException(string.Format("Loadable in Slot - \"{0}\" doesn't have a \"name\" attribute."));
            }
            Name = nameAttrib.Value;

            var typeAttrib = loadableElement.Attribute("type");
            if (typeAttrib == null)
            {
                throw new FormatException(
                    string.Format("Loadable - \"{0}\" in Slot - \"{1}\" doesn't have a \"type\" attribute.",
                                  Name,
                                  m_slotName));
            }
            Type = Enum<LoadableType>.Parse(typeAttrib.Value);
        }

        public string Name { get; private set; }
        public LoadableType Type { get; private set; }

        public List<Bank> GetLoadableBanks()
        {
            var banks = new List<Bank>();
            switch (Type)
            {
                case LoadableType.BANK:
                    {
                        var tokens = Name.Split('\\');
                        var packElement = GetPackElement(tokens[0]);
                        var bankElement = GetBankElement(packElement, tokens[1]);
                        banks.Add(new Bank(bankElement));
                        break;
                    }
                case LoadableType.BANKFOLDER:
                    {
                        var tokens = Name.Split('\\');
                        var packElement = GetPackElement(tokens[0]);
                        var bankFolderElement = GetBankFolderElement(packElement, tokens[1]);
                        banks.AddRange(bankFolderElement.Descendants("Bank").Select(bankElement => new Bank(bankElement)));
                        break;
                    }
                case LoadableType.PACK:
                    {
                        var packElement = GetPackElement(Name);
                        banks.AddRange(packElement.Descendants("Bank").Select(bankElement => new Bank(bankElement)));
                        break;
                    }
            }
            return banks;
        }

        private XElement GetPackElement(string packName)
        {
            XElement packElement;
            try
            {
                packElement =
                    (from pack in m_builtWaves.Root.Descendants("Pack")
                     where pack.Attribute("name").Value == packName
                     select pack).FirstOrDefault();
            }
            catch
            {
                packElement = null;
            }

            if (packElement == null)
            {
                throw new WaveSlotAssociationException(
                    string.Format("Waveslot - \"{0}\" references a bank in pack - \"{1}\". This Pack doesn't exist.",
                                  m_slotName,
                                  packName));
            }
            return packElement;
        }

        private XElement GetBankElement(XContainer packElement, string bankName)
        {
            XElement bankElement;
            try
            {
                bankElement =
                    (from bank in packElement.Descendants("Bank")
                     where bank.Attribute("name").Value == bankName
                     select bank).FirstOrDefault();
            }
            catch
            {
                bankElement = null;
            }

            if (bankElement == null)
            {
                throw new WaveSlotAssociationException(
                    string.Format("Waveslot \"{0}\" references bank \"{1}\" which doesn't exist.", m_slotName, bankName));
            }
            return bankElement;
        }

        private XElement GetBankFolderElement(XContainer packElement, string bankFolderName)
        {
            XElement bankFolderElement;
            try
            {
                bankFolderElement = (from bankFolder in packElement.Descendants("BankFolder")
                                     where bankFolder.Attribute("name").Value == bankFolderName
                                     select bankFolder).FirstOrDefault();
            }
            catch
            {
                bankFolderElement = null;
            }

            if (bankFolderElement == null)
            {
                throw new WaveSlotAssociationException(
                    string.Format("Waveslot \"{0}\" references bank folder \"{1}\" which doesn't exist.",
                                  m_slotName,
                                  bankFolderName));
            }
            return bankFolderElement;
        }
    }
}