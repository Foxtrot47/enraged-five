namespace rage
{
    public enum LoadType
    {
        BANK,
        WAVE,
        STREAM,
    }
}