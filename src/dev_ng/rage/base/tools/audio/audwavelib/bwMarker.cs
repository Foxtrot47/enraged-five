using System;
using System.Collections.Generic;
using System.Text;

namespace Wavelib
{
    public class bwMarker
    {
        private uint m_Position;
        private string m_Name;

        public bwMarker(string name, uint position)
        {
            m_Name = name;
            m_Position = position;
        }

        public uint Position
        {
            get { return m_Position; }
            set { m_Position = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
    }
}
