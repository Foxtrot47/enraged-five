﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Wavelib
{
    public class bwWaveVisualizer
    {
        #region Fields

        private bwWaveFile m_waveFile;
        private bwFormatChunk m_format;
        private bwSampleChunk m_sample;
        private bwDataChunk m_data;
        private int m_cursor;

        #endregion

        #region Properties

        public int XOffset { get; set; }
        public float XZoom { get; set; }
        public float YZoom { get; set; }
        public float XScale { get; set; }
        public float YScale { get; set; }

        #endregion

        #region Constructors

        public bwWaveVisualizer(bwWaveFile WaveFile)
        {
            m_waveFile = WaveFile;
            m_format = m_waveFile.Format;
            m_sample = m_waveFile.Sample;
            m_data = m_waveFile.Data;
            XZoom = 1.0f;
            YZoom = 1.0f;
        }

        #endregion

        #region Public Methods

        public void UpdateCursor(int numBytes)
        {
            // convert to samples
            m_cursor = (numBytes / ((m_format.SignificantBitsPerSample / 8) * m_format.NumChannels));
        }

        public void DrawLoop(PaintEventArgs pea)
        {
            var grfx = pea.Graphics;
            var visBounds = grfx.VisibleClipBounds;

            if (m_format.NumChannels == 1)
            {
                RenderMonoLoop(grfx, visBounds);
            }
        }

        public void DrawWave(PaintEventArgs pea)
        {
            var grfx = pea.Graphics;
            var visBounds = grfx.VisibleClipBounds;

            YScale = 1.0f * YZoom;
            // scale x so that the entire sample fits in the visbounds
            XScale = (visBounds.Width / m_data.NumSamples) * XZoom;

            System.Diagnostics.Debug.Assert(m_format.NumChannels == 1);
            RenderMonoWave(grfx, visBounds);
        }

        #endregion

        #region Private Methods

        private void RenderMonoLoop(Graphics g, RectangleF visBounds)
        {
            // number of samples to show at each end of the loop
            var numSamples = (int)((visBounds.Width / 2.0f));

            var rightStart = m_sample.Loops[0].Start;
            var rightEnd = m_sample.Loops[0].Start + numSamples;

            var leftStart = m_sample.Loops[0].End - numSamples;
            var leftEnd = m_sample.Loops[0].End;

            var xscale = 1.0f; //visBounds.Width / (numSamples*2);

            g.FillRectangle(new SolidBrush(Color.FromArgb(240, 240, 255)), 0, 0, visBounds.Width, visBounds.Height);

            var path = new GraphicsPath();

            long lastx = 0, lasty = 0;

            // render left samples (end of loop)
            for (var i = leftStart; i <= leftEnd; i++)
            {
                int startByte = (int)(i * 2);
                int sample = BitConverter.ToInt16(m_data.RawData, startByte);
                sample += 32768;

                var y = (Int32)((sample / 65536.0f) * visBounds.Height);
                var x = i;

                x = (Int32)((x - leftStart) * xscale) + (Int32)visBounds.Left;

                if (i - leftStart >= 1)
                {
                    path.AddLine(lastx, lasty, x, y);
                }

                lastx = x;
                lasty = y;
            }

            // render left samples (end of loop)
            for (var i = rightStart; i <= rightEnd; i++)
            {
                int startByte = (int)(i * 2);
                int sample = BitConverter.ToInt16(m_data.RawData, startByte);
                sample += 32768;

                var y = (Int32)((sample / 65536.0f) * visBounds.Height);
                var x = (long)i;

                x = (long)((x - rightStart) * xscale);
                x += (int)(visBounds.Width / 2.0f);

                if (i - rightStart >= 1)
                {
                    path.AddLine(lastx, lasty, x, y);
                }

                lastx = x;
                lasty = y;
            }

            var p = new Pen(Color.LightGray, 1.1f);
            p.DashStyle = DashStyle.Dash;

            g.DrawLine(p, visBounds.Width / 2, 0, visBounds.Width / 2, visBounds.Height);
            g.DrawLine(p, 0, visBounds.Height / 2, visBounds.Width, visBounds.Height / 2);

            g.DrawPath(Pens.CornflowerBlue, path);

            g.DrawString("Loop point (showing " + (numSamples * 2) + " samples)",
                         new Font("Tahoma", 8.0f),
                         Brushes.Gray,
                         5,
                         visBounds.Height - 20);
        }

        private void RenderMonoWave(Graphics g, RectangleF visBounds)
        {
            Int32 lastx = 0, lasty = 0;
            var gp = new GraphicsPath();
            var p = Pens.CornflowerBlue;

            g.FillRectangle(new SolidBrush(Color.FromArgb(240, 240, 255)),
                            0,
                            0,
                            (Int32)((m_data.NumSamples - XOffset) * XScale),
                            visBounds.Height);

            // draw wave
            for (var i = XOffset; i < m_data.NumSamples; i += 10)
            {
                int startByte = i*2;
                int sample = BitConverter.ToInt16(m_data.RawData, startByte);
                sample += 32768;

                var y = (Int32)((sample / 65536.0f) * visBounds.Height);
                var x = i;

                x = (Int32)((x - XOffset) * XScale);

                y = (Int32)(y * YScale);

                if (i >= 1)
                {
                    gp.AddLine(lastx, lasty, x, y);
                }

                if (YZoom > 20.0f && XZoom > 20.0f)
                {
                    // zoomed in a lot - draw individual samples clearly
                    //g.FillEllipse(new SolidBrush(Color.Black), x, y, 3, 3);	
                }

                lastx = x;
                lasty = y;
            }

            // draw path
            g.DrawPath(p, gp);

            // draw x axis
            g.DrawLine(p, 0, visBounds.Height / 2, visBounds.Width, visBounds.Height / 2);

            // draw cursor position
            g.DrawLine(Pens.Black,
                       (m_cursor - XOffset) * XScale,
                       0,
                       (m_cursor - XOffset) * XScale,
                       visBounds.Height);

            // draw a line to show the end of the wave
            g.DrawLine(p, lastx, 0, lastx, visBounds.Height);

            // show loop points
            if (null != m_sample)
            {
                for (var i = 0; i < m_sample.Loops.Count; i++)
                {
                    g.DrawLine(Pens.Coral,
                               (m_sample.Loops[i].Start - XOffset) * XScale,
                               0,
                               (m_sample.Loops[i].Start - XOffset) * XScale,
                               visBounds.Height);
                    g.DrawLine(Pens.Coral,
                               (m_sample.Loops[i].End - XOffset) * XScale,
                               0,
                               (m_sample.Loops[i].End - XOffset) * XScale,
                               visBounds.Height);
                }

                // add text
                var zoom = XZoom * 100;
                g.DrawString(zoom + "%", new Font("Tahoma", 8.0f), 
                    new SolidBrush(Color.Gray), 5, visBounds.Height - 20);
                g.DrawString(m_waveFile.GetDescription(),
                             new Font("Tahoma", 8.0f),
                             new SolidBrush(Color.Gray),
                             50,
                             visBounds.Height - 20);
            }
        }

        #endregion
    }
}
