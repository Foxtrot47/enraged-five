using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using rage.ToolLib;

namespace Wavelib
{

    public interface IRiffChunk
    {
        void Serialise(BinaryWriter bw);
    }

    public class bwXMAFormatChunk: bwFormatChunk
    {
        public readonly UInt16 NumStreams;
        public readonly UInt32 ChannelMask;
        public readonly UInt32 SamplesEncoded;
        public readonly UInt32 BytesPerBlock;
        public readonly UInt32 PlayBegin;
        public readonly UInt32 PlayLength;
        public readonly UInt32 LoopBegin;
        public readonly UInt32 LoopLength;
        public readonly byte LoopCount;
        public readonly byte EncoderVersion;
        public readonly UInt16 BlockCount;

        public bwXMAFormatChunk(BinaryReader br, int length):base(br,length)
        {
            br.BaseStream.Position -= base.ExtraFormatBytes.Length;
            NumStreams = br.ReadUInt16();
            ChannelMask = br.ReadUInt32();
            SamplesEncoded = br.ReadUInt32();
            BytesPerBlock = br.ReadUInt32();
            PlayBegin = br.ReadUInt32();
            PlayLength = br.ReadUInt32();
            LoopBegin = br.ReadUInt32();
            LoopLength = br.ReadUInt32();
            LoopCount = br.ReadByte();
            EncoderVersion = br.ReadByte();
            BlockCount = br.ReadUInt16();
        }

    }

    public class bwFormatChunk : IRiffChunk
    {
        public enum CompressionFormat
        {
            Pcm = 1,
            Float = 3,
            Extensible = 0xFFFE
        };

        public readonly UInt16 CompressionCode;
        public readonly UInt16 NumChannels;
        public readonly uint SampleRate;
        public readonly uint AverageBytesPerSecond;
        public readonly UInt16 BlockAlign;
        public readonly UInt16 SignificantBitsPerSample;
        public readonly byte[] ExtraFormatBytes;        

        public bwFormatChunk(UInt16 CompressionCode, UInt16 NumChannels, uint SampleRate,uint AverageBytesPerSecond,
                                UInt16 BlockAlign,UInt16 SignificantBitsPerSample,byte[]ExtraFormatBytes)
        {
            this.CompressionCode = CompressionCode;
            this.NumChannels = NumChannels;
            this.SampleRate = SampleRate;
            this.AverageBytesPerSecond = AverageBytesPerSecond;
            this.BlockAlign = BlockAlign;
            this.SignificantBitsPerSample = SignificantBitsPerSample;
            this.ExtraFormatBytes = ExtraFormatBytes;
        }

        

        public bwFormatChunk(BinaryReader br, Int32 length)
        {
            // Some files from ProTools have inconsistent fmt chunk size vs extra bytes (ie chunk size of 40 but extra bytes = 0)
            // so ensure we seek to the end of the full chunk, regardless of contents.
            long endPosition = br.BaseStream.Position + length;

            CompressionCode = br.ReadUInt16();
            NumChannels = br.ReadUInt16();
            SampleRate = br.ReadUInt32();
            AverageBytesPerSecond = br.ReadUInt32();
            BlockAlign = br.ReadUInt16();
            SignificantBitsPerSample = br.ReadUInt16();
            if (length > 16)
            {
                UInt16 numBytes = br.ReadUInt16();
                ExtraFormatBytes = br.ReadBytes(numBytes);
            }
            else
            {
                ExtraFormatBytes = new byte[0];
            }

            br.BaseStream.Position = endPosition;
        }

        public void Serialise(BinaryWriter bw)
        {
            bw.Write(ASCIIEncoding.ASCII.GetBytes("fmt "));
            bw.Write((UInt32)(16 + ExtraFormatBytes.Length));
            bw.Write(CompressionCode);
            bw.Write(NumChannels);
            bw.Write(SampleRate);
            bw.Write(AverageBytesPerSecond);
            bw.Write(BlockAlign);
            bw.Write(SignificantBitsPerSample);
            if (ExtraFormatBytes.Length > 0)
            {
                bw.Write((UInt16)ExtraFormatBytes.Length);
                bw.Write(ExtraFormatBytes);
            }
        }
    }

    public class bwRaveChunk : IRiffChunk
    {
        public readonly byte[] Bytes;
        public readonly string ChunkId;

        public bwRaveChunk(string ChunkId, BinaryReader br, Int32 length)
        {
            Bytes = br.ReadBytes(length);
            this.ChunkId = ChunkId;
        }

        public bwRaveChunk(string ChunkId, byte[] Bytes)
        {
            this.ChunkId = ChunkId;
            this.Bytes = Bytes;
        }

        public void Serialise(BinaryWriter bw)
        {
            byte[] buf = ASCIIEncoding.ASCII.GetBytes(ChunkId);
            if (buf.Length != 4)
            {
                throw new InvalidDataException("ChunkId length != 4");
            }

            bw.Write(buf);
            bw.Write((UInt32)Bytes.Length);
            bw.Write(Bytes);
        }
    }
    public class bwGenericChunk : IRiffChunk
    {
        public readonly byte[] Bytes;
        public readonly string ChunkId;

        public bwGenericChunk(string ChunkId, BinaryReader br, Int32 length)
        {
            Bytes = br.ReadBytes(length);
            this.ChunkId = ChunkId;
        }

        public void Serialise(BinaryWriter bw)
        {
            byte[] buf = ASCIIEncoding.ASCII.GetBytes(ChunkId);
            if (buf.Length != 4)
            {
                throw new InvalidDataException("ChunkId length != 4");
            }

            bw.Write(buf);
            bw.Write((UInt32)Bytes.Length);
            bw.Write(Bytes);
        }
    }

    public class bwDataChunk : IRiffChunk
    {
        public byte[] RawData;
        public float[,] ChannelData;
        public uint NumSamples;
        public uint NumChannels;

        public enum DataFormat
        {
            S16,
            S24,
            F32,
            F64
        }
        public DataFormat Format {get;set;}
        public bwDataChunk(byte[] RawData, uint NumSamples, DataFormat format)
        {
            this.RawData = RawData;
            this.ChannelData = null;
            this.NumSamples = NumSamples;
            this.NumChannels = 1;
            this.Format = format;
        }

        public bwDataChunk(byte[] RawData, uint NumSamples, uint NumChannels, DataFormat format)
        {
            this.RawData = RawData;
            this.ChannelData = null;
            this.NumSamples = NumSamples;
            this.NumChannels = NumChannels;
            this.Format = format;
        }

        public bwDataChunk(bwFormatChunk fmt, BinaryReader br, Int32 length, bool createFloatArray)
        {
            if (fmt.CompressionCode == (uint)bwFormatChunk.CompressionFormat.Pcm)
            {
                if (fmt.SignificantBitsPerSample == 16)
                {
                    Format = DataFormat.S16;
                }
                else if (fmt.SignificantBitsPerSample == 24)
                {
                    Format = DataFormat.S24;
                }
                else
                {
                    throw new FormatException(string.Format("Invalid wave format; only 16 or 24bit integer waveforms are supported - this is {0} bit", fmt.SignificantBitsPerSample));
                }
            }
            else if (fmt.CompressionCode == (uint)bwFormatChunk.CompressionFormat.Float)
            {
                if (fmt.SignificantBitsPerSample == 32)
                {
                    Format = DataFormat.F32;
                }
                else if (fmt.SignificantBitsPerSample == 64)
                {
                    Format = DataFormat.F64;
                }
                else
                {
                    throw new FormatException(string.Format("Invalid wave format; only 32 or 64bit float waveforms are supported - this is {0} bit", fmt.SignificantBitsPerSample));
                }
            }
            else
            {
               // throw new FormatException(string.Format("Invalid wave format; only PCM or IEEE compression codes are supported - this is {0}", fmt.CompressionCode));
            }
            NumChannels = fmt.NumChannels;
            NumSamples = (uint)length / fmt.BlockAlign;
            //int bufferLength = (int)(fmt.NumChannels * NumSamples * (fmt.SignificantBitsPerSample/8)); 
            //use length instead of calculating it new
            RawData = new byte[length];
            int currentByte = 0;
            while((currentByte+250000)<(length-1))
            {
                br.Read(RawData, currentByte, 250000);
                currentByte += 250000;
            }
            if (currentByte < (length - 1))
            {
                int toRead = length - currentByte;
                br.Read(RawData, currentByte, toRead);
                currentByte += toRead;
            }

            // TODO: remove
            if (createFloatArray)
            {
                ChannelData = new float[fmt.NumChannels, NumSamples];
                var doubleData = CreateFloatArray();                
            }
        }

        public double[][] CreateDoubleArray()
        {
            var sampleData = new double[NumChannels][];
            for (int i = 0; i < NumChannels; i++)
            {
                sampleData[i] = new double[NumSamples];
            }
            var br = new BinaryReader(new MemoryStream(RawData));

            for (int sampleIndex = 0; sampleIndex < NumSamples; sampleIndex++)
            {
                for (int channelIndex = 0; channelIndex < NumChannels; channelIndex++)
                {
                    sampleData[channelIndex][sampleIndex] = readSampleFromBinaryReader(br);
                }
            }
            br.Close();
            return sampleData;
        }

        private double readSampleFromBinaryReader(BinaryReader br)
        {
            double sampleValue = 0.0;
            if (Format == DataFormat.S16)
            {
                sampleValue = br.ReadInt16() / (double)Int16.MaxValue;
            }
            else if (Format == DataFormat.S24)
            {
                var tempBytes = br.ReadBytes(3);
                var padded = new byte[4];
                tempBytes.CopyTo(padded, 0);
                padded[3] = 0;
                var intVal = BitConverter.ToInt32(padded, 0);
                double divider = (double)((1 << 24) - 1);
                sampleValue = intVal / divider;
            }
            else if (Format == DataFormat.F32)
            {
                sampleValue = br.ReadSingle();
            }
            else if (Format == DataFormat.F64)
            {
                sampleValue = br.ReadDouble();
            }
            return sampleValue;
        }

        public float[][] CreateFloatArray()
        {
            var sampleData = new float[NumChannels][];
            for (int i = 0; i < NumChannels; i++)
            {
                sampleData[i] = new float[NumSamples];
            }
            var br = new BinaryReader(new MemoryStream(RawData));

            for (int sampleIndex = 0; sampleIndex < NumSamples; sampleIndex++)
            {
                for (int channelIndex = 0; channelIndex < NumChannels; channelIndex++)
                {
                    sampleData[channelIndex][sampleIndex] = (float)readSampleFromBinaryReader(br);
                }
            }
            br.Close();
            return sampleData;
        }


        public void Serialise(BinaryWriter bw)
        {
            bw.Write(ASCIIEncoding.ASCII.GetBytes("data"));
            bw.Write((UInt32)RawData.Length);
            bw.Write(RawData);
        }
    }
    public class bwCuePoint
    {
        public uint Id;
        public uint Position;
        public string DataChunkId;
        public uint ChunkStart;
        public uint CueStart;
        public uint BlockStart;
        public uint SampleOffset;
    }
    public class bwCueChunk : IRiffChunk
    {
        public readonly bwCuePoint[] CuePoints;
        public bwCueChunk(BinaryReader br, Int32 length)
        {
            uint numCues = br.ReadUInt32();
            CuePoints = new bwCuePoint[numCues];
            for (uint i = 0; i < numCues; i++)
            {
                CuePoints[i] = new bwCuePoint();

                CuePoints[i].Id = br.ReadUInt32();
                CuePoints[i].Position = br.ReadUInt32();
                byte[] id = br.ReadBytes(4);
                CuePoints[i].DataChunkId = ASCIIEncoding.ASCII.GetString(id);
                CuePoints[i].ChunkStart = br.ReadUInt32();
                CuePoints[i].BlockStart = br.ReadUInt32();
                CuePoints[i].SampleOffset = br.ReadUInt32();
            }
        }

        public bwCueChunk(bwCuePoint[] cuePoints)
        {
            CuePoints = cuePoints;
        }

        public void Serialise(BinaryWriter bw)
        {
            bw.Write(ASCIIEncoding.ASCII.GetBytes("cue "));
            bw.Write((UInt32)(24 * CuePoints.Length) + 4);
            bw.Write((UInt32)CuePoints.Length);
            foreach (bwCuePoint point in CuePoints)
            {
                bw.Write(point.Id);
                bw.Write(point.Position);
                bw.Write(ASCIIEncoding.ASCII.GetBytes(point.DataChunkId));
                bw.Write(point.ChunkStart);
                bw.Write(point.BlockStart);
                bw.Write(point.SampleOffset);
            }
        }
    }

    public class bwLabeledTextChunk : IRiffChunk
    {
        public readonly uint CuePointId;
        public readonly uint SampleLength;
        public readonly uint PurposeId;
        public readonly UInt16 Country;
        public readonly UInt16 Language;
        public readonly UInt16 Dialect;
        public readonly UInt16 CodePage;
        public readonly string Label;

        public bwLabeledTextChunk(BinaryReader br, Int32 length)
        {
            CuePointId = br.ReadUInt32();
            SampleLength = br.ReadUInt32();
            PurposeId = br.ReadUInt32();
            Country = br.ReadUInt16();
            Language = br.ReadUInt16();
            Dialect = br.ReadUInt16();
            CodePage = br.ReadUInt16();
            Label = ASCIIEncoding.ASCII.GetString(br.ReadBytes(length - 20)).TrimEnd('\0');
        }

        public bwLabeledTextChunk(uint cuePointId, uint sampleLength, string label)
        {
            PurposeId = 0x206E6772;
            Country = 0;
            Language = 0;
            Dialect = 0;
            CodePage = 0;

            CuePointId = cuePointId;
            SampleLength = sampleLength;
            Label = label;
        }

        public void Serialise(BinaryWriter bw)
        {
            byte[] bytes = ASCIIEncoding.ASCII.GetBytes(Label);
            bw.Write(ASCIIEncoding.ASCII.GetBytes("ltxt"));
            bw.Write((UInt32)(20 + bytes.Length));
            bw.Write(CuePointId);
            bw.Write(SampleLength);
            bw.Write(PurposeId);
            bw.Write(Country);
            bw.Write(Language);
            bw.Write(Dialect);
            bw.Write(CodePage);
            bw.Write(bytes);
        }
    }


    public class bwLabelChunk : IRiffChunk
    {
        public readonly uint CuePointId;
        public readonly string Label;
        public bwLabelChunk(BinaryReader br, Int32 length)
        {
            CuePointId = br.ReadUInt32();
            Label = ASCIIEncoding.ASCII.GetString(br.ReadBytes(length - 4)).TrimEnd('\0');
            if (length % 2 != 0)
            {
                br.ReadByte();
            }
        }

        public bwLabelChunk(uint cuePointId, string label)
        {
            CuePointId = cuePointId;
            Label = label;
        }

        public void Serialise(BinaryWriter bw)
        {
            bw.Write(ASCIIEncoding.ASCII.GetBytes("labl"));
            bw.Write((UInt32)Label.Length + 5);
            bw.Write(CuePointId);
            bw.Write(ASCIIEncoding.ASCII.GetBytes(Label));
            bw.Write((byte)0);
            if ((Label.Length+1) % 2 != 0)
            {
                bw.Write((byte)0);
            }
        }
    }

    public class bwNoteChunk : IRiffChunk
    { 
        public readonly uint CuePointId;
        public readonly string Label;
        public bwNoteChunk(BinaryReader br, Int32 length)
        {
            CuePointId = br.ReadUInt32();
            Label = ASCIIEncoding.ASCII.GetString(br.ReadBytes(length - 4)).TrimEnd('\0');
            if (length % 2 != 0)
            {
                br.ReadByte();
            }
        }

        public void Serialise(BinaryWriter bw)
        {
            bw.Write(ASCIIEncoding.ASCII.GetBytes("note"));
            bw.Write((UInt32)Label.Length + 5);//+1 for \0
            bw.Write(CuePointId);
            bw.Write(ASCIIEncoding.ASCII.GetBytes(Label));
            bw.Write((byte)0);
            if ((Label.Length + 1) % 2 != 0)
            {
                bw.Write((byte)0);
            }
        }
    }

    public class bwListChunk : IRiffChunk
    {
        public readonly List<bwLabeledTextChunk> LabeledText;
        public readonly List<bwNoteChunk> Notes;
        public readonly List<bwLabelChunk> Labels;
        public readonly string ListType;
        
        public bwListChunk(BinaryReader br, Int32 length)
        {
            LabeledText = new List<bwLabeledTextChunk>();
            Notes = new List<bwNoteChunk>();
            Labels = new List<bwLabelChunk>();
            length -= 4;
            string chunk = ASCIIEncoding.ASCII.GetString(br.ReadBytes(4)).ToLower();
            ListType = chunk;

            if (ListType == "adtl")
            {
                while (length > 0)
                {
                    chunk = ASCIIEncoding.ASCII.GetString(br.ReadBytes(4)).ToLower();
                    Int32 chunkLen = br.ReadInt32();
                    length -= (chunkLen + 8);//add four for chunk header and four for chunk size
                    switch (chunk)
                    {
                        case "labl":
                            Labels.Add(new bwLabelChunk(br, chunkLen));
                            break;
                        case "note":
                            Notes.Add(new bwNoteChunk(br, chunkLen));
                            break;
                        case "ltxt":
                            LabeledText.Add(new bwLabeledTextChunk(br, chunkLen));
                            break;
                        default:
                            throw new InvalidDataException("Expected labl, note or ltxt chunk - got " + chunk);
                    }
                    if (length % 2 != 0)
                    {
                        length--;
                    }
                }
            }
            else
            {
                throw new InvalidDataException("Expected adtl list type");
            }
        
        }

        public bwListChunk(string listType)
        {
            ListType = listType;
            LabeledText = new List<bwLabeledTextChunk>();
            Notes = new List<bwNoteChunk>();
            Labels = new List<bwLabelChunk>();
        }

        public void Serialise(BinaryWriter bw)
        {
            bw.Write(ASCIIEncoding.ASCII.GetBytes("LIST"));
            // chunk size is dependent on child chunks
            long pos = bw.BaseStream.Position;
            bw.Write((UInt32)0);
            bw.Write(ASCIIEncoding.ASCII.GetBytes(ListType));
            foreach(bwLabelChunk l in Labels)
            {
                l.Serialise(bw);
            }
            foreach (bwNoteChunk n in Notes)
            {
                n.Serialise(bw);
            }
            foreach (bwLabeledTextChunk lt in LabeledText)
            {
                lt.Serialise(bw);
            }
            // go back and write chunk size
            long size = bw.BaseStream.Position - pos;
            bw.Seek((int)pos, SeekOrigin.Begin);
            bw.Write((UInt32)(size - 4));
            bw.Seek(0, SeekOrigin.End);
        }
    }

    public class SampleLoop
    {
        public UInt32 Identifier;
        public UInt32 Type;
        public UInt32 Start;
        public UInt32 End;
        public UInt32 Fraction;
        public UInt32 PlayCount;

        public SampleLoop()
        {

        }

        public SampleLoop(BinaryReader br)
        {
            Identifier= br.ReadUInt32();
            Type = br.ReadUInt32();
            Start = br.ReadUInt32();
            End = br.ReadUInt32();
            Fraction = br.ReadUInt32();
            PlayCount = br.ReadUInt32();
        }
    }

    public class bwSampleChunk : IRiffChunk
    {
        public readonly UInt32 Manufacturer;
        public readonly UInt32 Product;
        public readonly UInt32 SamplePeriod;
        public readonly UInt32 MIDIUnityNote;
        public readonly UInt32 MIDIPitchFraction;
        public readonly UInt32 SMPTEFormat;
        public readonly UInt32 SMPTEOffset;
        public UInt32 SampleLoops {get;set;}
        public readonly UInt32 SamplerData;
        public List<SampleLoop> Loops;
        public readonly byte[] Data;

        public bwSampleChunk()
        {
            Loops = new List<SampleLoop>();
        }

        public bwSampleChunk(BinaryReader br, Int32 length)
        {
            Manufacturer = br.ReadUInt32();
            Product = br.ReadUInt32();
            SamplePeriod = br.ReadUInt32();
            MIDIUnityNote = br.ReadUInt32();
            MIDIPitchFraction = br.ReadUInt32();
            SMPTEFormat = br.ReadUInt32();
            SMPTEOffset = br.ReadUInt32();
            SampleLoops = br.ReadUInt32();
            SamplerData = br.ReadUInt32();
            if (SamplerData > 0)
            {
                Data = new byte[SamplerData];
            }
            Loops = new List<SampleLoop>();

            for(uint i=0; i<SampleLoops; i++)
            {
                Loops.Add(new SampleLoop(br));
            }

            for (int i = 0; i < SamplerData; i++)
            {
                Data[i] = br.ReadByte();
            }
        }

        #region IRiffChunk Members

        public void Serialise(BinaryWriter bw)
        {

            bw.Write(ASCIIEncoding.ASCII.GetBytes("smpl"));
            bw.Write((UInt32)(36 + (Loops.Count * 24)));
            bw.Write(Manufacturer);
            bw.Write(Product);
            bw.Write(SamplePeriod);
            bw.Write(MIDIUnityNote);
            bw.Write(MIDIPitchFraction);
            bw.Write(SMPTEFormat);
            bw.Write(SMPTEOffset);
            bw.Write(SampleLoops);
            bw.Write(SamplerData);
            foreach (SampleLoop loop in Loops)
            {
                bw.Write(loop.Identifier);
                bw.Write(loop.Type);
                bw.Write(loop.Start);
                bw.Write(loop.End);
                bw.Write(loop.Fraction);
                bw.Write(loop.PlayCount);
            }
            if (Data != null)
            {
                bw.Write(Data);
            }
        
        }

        #endregion
    }

}
