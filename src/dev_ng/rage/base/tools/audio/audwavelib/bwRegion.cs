using System;
using System.Collections.Generic;
using System.Text;

namespace Wavelib
{
    public class bwRegion : bwMarker
    {
        private uint m_Length;
        private uint m_OriginalLength;

        public bwRegion(string name, uint position, uint length) : base(name, position)
        {
            m_Length = length;
            m_OriginalLength = Length;
        }

        public uint Length
        {
            get { return m_Length; }
            set { m_Length = value; }
        }

        public void Reset()
        {
            Length = m_OriginalLength;            
        }
    }
}
