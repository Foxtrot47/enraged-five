using System;
using System.Collections;
using System.Text;
using System.IO;

namespace Wavelib
{
    public class bwWaveFile
    {
        #region Fields

        private bwMarkerList m_Markers;
        private ArrayList m_Chunks;
        private Hashtable m_ChunkLookup;
        private string m_FileName;

        #endregion

        #region Properties

        public bwMarkerList Markers
        {
            get { return m_Markers; }
            set { m_Markers = value; }
        }

        public ArrayList Chunks
        {
            get { return m_Chunks; }
        }

        public bwFormatChunk Format
        {
            get { return (bwFormatChunk)m_ChunkLookup["fmt "]; }
            set
            {
                m_Chunks.Remove(m_ChunkLookup["fmt "]);
                m_Chunks.Insert(0, value);
                m_ChunkLookup["fmt "] = value;
            }
        }

        public bwSampleChunk Sample
        {
            get { return (bwSampleChunk)m_ChunkLookup["smpl"]; }
            set
            {
                m_Chunks.Remove(m_ChunkLookup["smpl"]);
                m_ChunkLookup["smpl"] = value;
                m_Chunks.Add(value);
            }
        }

        public bwDataChunk Data
        {
            get { return (bwDataChunk)m_ChunkLookup["data"]; }
            set
            {
                m_Chunks.Remove(m_ChunkLookup["data"]);
                
                //To follow best practice we place the data chunk behind the format chunk.
                //Many programs expect the chunks to be stored in this order and it enables
                //playback of streamed waves before all data has been streamed
                int fmtIndex = m_Chunks.IndexOf(m_ChunkLookup["fmt "]);
                if (fmtIndex == -1)
                {
                    m_Chunks.Insert(0, value);
                }
                else
                {
                    m_Chunks.Insert(fmtIndex+1, value);
                }

                m_ChunkLookup["data"] = value;
            }
        }

        #endregion

        #region Constructors

        public bwWaveFile(string fileName, bool createFloatArray, bool headerOnly)
        {
            m_Chunks = new ArrayList();
            m_Markers = new bwMarkerList();
            m_ChunkLookup = new Hashtable();

            if (!string.IsNullOrEmpty(fileName))
            {
                Load(fileName, createFloatArray, headerOnly);
            }
        }

        public bwWaveFile(string fileName) : this(fileName, true, false)
        {
        }

        public bwWaveFile() : this(null, true, false)
        {
        }

        public bwWaveFile(string fileName, bool createFloatArray) : this(fileName, createFloatArray, false)
        {

        }

        #endregion

        #region Public Methods

        public void RemoveRaveChunks()
        {
            if (m_ChunkLookup.ContainsKey("XARB"))
            {
                m_Chunks.Remove(m_ChunkLookup["XARB"]);
                m_ChunkLookup.Remove("XARB");
            }
            if (m_ChunkLookup.ContainsKey("PARB"))
            {
                m_Chunks.Remove(m_ChunkLookup["PARB"]);
                m_ChunkLookup.Remove("PARB");
            }
            if (m_ChunkLookup.ContainsKey("WARB"))
            {
                m_Chunks.Remove(m_ChunkLookup["WARB"]);
                m_ChunkLookup.Remove("WARB");
            }
        }

        public void AddRaveChunk(bwRaveChunk chunk)
        {
            var toReplace = (bwRaveChunk)m_ChunkLookup[chunk.ChunkId];
            if (toReplace != null)
            {
                m_Chunks.Remove(toReplace);
                m_ChunkLookup.Remove(chunk.ChunkId);
            }
            m_Chunks.Add(chunk);
            m_ChunkLookup.Add(chunk.ChunkId, chunk);
        }

        public string FileName
        {
            set { m_FileName = value; }
            get { return m_FileName; }
        }

        public void AddChunk(IRiffChunk chunk)
        {
            m_Chunks.Add(chunk);
        }

        public IRiffChunk GetChunk(string name)
        {
            if (m_ChunkLookup.ContainsKey(name))
            {
                return (IRiffChunk)m_ChunkLookup[name];
            }

            return null;
        }

        public string GetDescription()
        {
            var desc = Format.SampleRate + "Hz " + Format.SignificantBitsPerSample + "bit ";
            switch (Format.NumChannels)
            {
                case 1:
                    desc += "mono";
                    break;
                case 2:
                    desc += "stereo";
                    break;
                default:
                    desc += Format.NumChannels + " channels";
                    break;
            }
            return desc;
        }

        public IRiffChunk GetChunkByName(string name)
        {
            return (IRiffChunk)m_ChunkLookup[name];
        }

        public bool IsLooping()
        {
            return null != Sample && Sample.Loops.Count >= 1;
        }

        public void Save()
        {
            Save(m_FileName);
        }

        public void Save(string fileName)
        {
            using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                Save(fs);
            }
        }
           
        public void Save(Stream fs)
        {
            ArrayList toRemove = new ArrayList();

            //check for markers outwith the wave
            foreach (bwMarker marker in m_Markers)
            {
                if (marker.Position >= this.Data.NumSamples)
                {
                    toRemove.Add(marker);
                }
            }

            foreach (bwMarker marker in toRemove)
            {
                m_Markers.Remove(marker);
            }

            using (var bw = new BinaryWriter(fs, Encoding.ASCII, true))
            {

                // write RIFF header
                bw.Write(ASCIIEncoding.ASCII.GetBytes("RIFF"));
                // filesize - 8...
                bw.Write((UInt32)0);
                bw.Write(ASCIIEncoding.ASCII.GetBytes("WAVE"));
                foreach (IRiffChunk chunk in m_Chunks)
                {
                    chunk.Serialise(bw);
                    // align chunksize to 2 bytes
                    if ((bw.BaseStream.Position % 2) != 0)
                    {
                        bw.Write((byte)0);
                    }
                }

                if (Markers.Count > 0)
                {
                    bwCueChunk cue;
                    bwListChunk list;
                    CreateChunksFromMarkers(out cue, out list);
                    cue.Serialise(bw);
                    // align chunksize to 2 bytes
                    if ((bw.BaseStream.Position % 2) != 0)
                    {
                        bw.Write((byte)0);
                    }
                    list.Serialise(bw);

                }
                // align chunksize to 2 bytes
                if ((bw.BaseStream.Position % 2) != 0)
                {
                    bw.Write((byte)0);
                }

                // seek back and write the total RIFF size
                bw.Seek(4, SeekOrigin.Begin);
                bw.Write((UInt32)(bw.BaseStream.Length - 8));
            }            
        }

        public uint ConvertSamplesToMs(uint samples)
        {
            return (uint)((samples / (float)Format.SampleRate) * 1000.0f);
        }

        public uint ConvertMsToSamples(uint ms)
        {
            return (uint)((ms / 1000.0f) * (float)Format.SampleRate);
        }

        public void PopulateMarkerInfo()
        {
            var chunksToDelete = new ArrayList();
            bool listChunkFound = false;
            foreach (object o in m_Chunks)
            {
                var list = o as bwListChunk;
                if (list != null && list.ListType == "adtl")
                {
                    listChunkFound = true;
                    chunksToDelete.Add(list);
                    foreach (bwLabelChunk label in list.Labels)
                    {
                        bwCuePoint cuePoint = FindCuePoint(label.CuePointId);
                        if (cuePoint != null)
                        {
                            bool isRegion = false;
                            uint regionLength = 0;
                            foreach (bwLabeledTextChunk ltxt in list.LabeledText)
                            {
                                if (ltxt.CuePointId == label.CuePointId)
                                {
                                    isRegion = true;
                                    regionLength = ltxt.SampleLength;
                                    break;
                                }
                            }
                            if (cuePoint != null)
                            {
                                if (isRegion)
                                {
                                    Markers.Add(new bwRegion(label.Label, cuePoint.SampleOffset, regionLength));
                                }
                                else
                                {
                                    Markers.Add(new bwMarker(label.Label, cuePoint.SampleOffset));
                                }
                            }

                        }
                    }
                }
            }
            if (!listChunkFound)
            {
                var cueChunk = (bwCueChunk)m_ChunkLookup["cue "];
                if (cueChunk != null)
                {
                    foreach (bwCuePoint point in cueChunk.CuePoints)
                    {
                        Markers.Add(new bwMarker(point.Id.ToString(), point.SampleOffset));
                    }
                }
            }
            chunksToDelete.Add(m_ChunkLookup["cue "]);
            foreach (object o in chunksToDelete)
            {
                m_Chunks.Remove(o);
            }
            m_ChunkLookup.Remove("cue ");
        }

        #endregion

        #region Private Methods

        private void Load(string fileName, bool createFloatArray, bool headerOnly)
        {
            BinaryReader br = new BinaryReader(new FileStream(fileName, FileMode.Open, FileAccess.Read));
            string hdr = Encoding.ASCII.GetString(br.ReadBytes(4));
            if (hdr != "RIFF")
            {
                throw new InvalidDataException("Not a valid wave file - missing RIFF header");
            }
            // skip riff chunk size
            br.ReadUInt32();
            hdr = Encoding.ASCII.GetString(br.ReadBytes(4));
            if (hdr != "WAVE")
            {
                throw new InvalidDataException("Not a valid wave file - missing WAVE RIFF type");
            }
            bool finished = false;
            long dataPosition = -1;
            while (!finished && br.BaseStream.Length - br.BaseStream.Position >= 8)
            {
                hdr = Encoding.ASCII.GetString(br.ReadBytes(4));
                Int32 length = br.ReadInt32();

                if (m_ChunkLookup[hdr] != null)
                {
                    br.BaseStream.Seek(br.BaseStream.Position + length, SeekOrigin.Begin);
                }
                else
                {
                    switch (hdr.ToLower())
                    {
                        case "fmt ":
                            {
                                UInt16 compressionCode = br.ReadUInt16();
                                br.BaseStream.Position -= 2;

                                bwFormatChunk chunk = null;
                                if (compressionCode == 358)
                                {
                                    chunk = new bwXMAFormatChunk(br, length);
                                }
                                else
                                {
                                    chunk = new bwFormatChunk(br, length);
                                }
                                m_Chunks.Add(chunk);
                                m_ChunkLookup.Add(hdr, chunk);
                            }

                            if (!headerOnly && m_ChunkLookup["data"] == null && dataPosition != -1)
                            {
                                br.BaseStream.Seek(dataPosition, SeekOrigin.Begin);
                            }

                            break;

                        case "data":
                            {
                                if (m_ChunkLookup["fmt "] == null || headerOnly)
                                {
                                    //store data position first and find fmt chunk
                                    dataPosition = br.BaseStream.Position - 8;
                                    br.BaseStream.Seek(br.BaseStream.Position + length, SeekOrigin.Begin);
                                }
                                else
                                {
                                    bwDataChunk chunk = new bwDataChunk((bwFormatChunk)m_ChunkLookup["fmt "], br, length, createFloatArray);
                                    m_ChunkLookup.Add(hdr, chunk);
                                    m_Chunks.Add(chunk);
                                }

                            }
                            break;
                        case "cue ":
                            {

                                bwCueChunk chunk = new bwCueChunk(br, length);
                                m_Chunks.Add(chunk);
                                m_ChunkLookup.Add(hdr, chunk);

                            }
                            break;
                        case "list":

                            // we only care about adtl lists...lets hope there aren't any other types starting with a...
                            if (br.PeekChar() == 'a')
                            {
                                m_Chunks.Add(new bwListChunk(br, length));
                            }
                            else
                            {
                                m_Chunks.Add(new bwGenericChunk(hdr, br, length));
                            }

                            break;
                        case "smpl":
                            {
                                bwSampleChunk chunk = new bwSampleChunk(br, length);
                                m_Chunks.Add(chunk);
                                m_ChunkLookup.Add(hdr, chunk);

                            }
                            break;
                        case "rave":
                            {
                                bwRaveChunk chunk = new bwRaveChunk(hdr, br, length);
                            }
                            break;
                        case "xarb":
                        case "parb":
                        case "warb":
                            {
                                bwRaveChunk chunk = new bwRaveChunk(hdr, br, length);
                                m_Chunks.Add(chunk);
                                m_ChunkLookup.Add(hdr, chunk);
                                break;
                            }
                        default:
                            {
                                bwGenericChunk chunk = new bwGenericChunk(hdr, br, length);
                                m_Chunks.Add(chunk);
                                m_ChunkLookup.Add(hdr, chunk);
                            }
                            break;
                    }
                    // skip pad byte (RIFF chunks are word aligned)
                    if (length % 2 != 0)
                    {
                        if (br.BaseStream.Position != br.BaseStream.Length)
                        {
                            br.ReadByte();
                        }
                        else
                        {
                            Console.Out.WriteLine("Warning: File " + m_FileName + " has an uneven number of bytes and no padding");
                        }
                    }

                    if (br.BaseStream.Position >= br.BaseStream.Length - 1)
                    {
                        finished = true;
                    }
                }
            }
            br.Close();
            m_FileName = fileName;

            PopulateMarkerInfo();
        }
        
        bwCuePoint FindCuePoint(uint cuePointId)
        {
            var cueChunk = (bwCueChunk)m_ChunkLookup["cue "];

            if (cueChunk != null)
            {
                foreach (bwCuePoint point in cueChunk.CuePoints)
                {
                    if (point.Id == cuePointId)
                    {
                        return point;
                    }
                }
            }
            return null;
        }

        void CreateChunksFromMarkers(out bwCueChunk cue, out bwListChunk list)
        {
            list = new bwListChunk("adtl");
            ArrayList points = new ArrayList();
            uint id = 0;
            foreach (bwMarker m in m_Markers)
            {
                list.Labels.Add(new bwLabelChunk(id, m.Name));
                if (m is bwRegion)
                {
                    bwRegion r = m as bwRegion;
                    list.LabeledText.Add(new bwLabeledTextChunk(id, r.Length, r.Name));
                }
                var p = new bwCuePoint();
                p.Position = m.Position;
                p.Id = id++;
                p.DataChunkId = "data";
                p.BlockStart = 0;
                p.ChunkStart = 0;
                p.SampleOffset = m.Position;
                points.Add(p);
            }

            cue = new bwCueChunk((bwCuePoint[])points.ToArray(typeof(bwCuePoint)));
        }

        #endregion

        public static bwWaveFile CreateFromChannelData(double[][] sampleData, uint sampleRate, int loopStart, int loopEnd, bwMarkerList markers)
        {
            var waveFile = new bwWaveFile();
            var numChannels = sampleData.Length;
            var fmt = new bwFormatChunk((ushort)bwFormatChunk.CompressionFormat.Float, // IEEE float
                                        (ushort)numChannels,
                                        sampleRate,
                                        (uint)(sampleRate * numChannels * 8),
                                        8, // 8 bytes per sample
                                        64, // 64bits per sample
                                        new byte[0]);
            waveFile.Format = fmt;

            var numSamples = sampleData[0].Length;
            var rawData = new byte[numChannels * 8 * numSamples];
            var bw = new BinaryWriter(new MemoryStream(rawData));
            
            // Interleave the channel data
            for (int sampleIndex = 0; sampleIndex < numSamples; sampleIndex++)
            {
                for (int channelIndex = 0; channelIndex < numChannels; channelIndex++)
                {
                    bw.Write(sampleData[channelIndex][sampleIndex]);
                }
            }
            bw.Close();
            waveFile.Data = new bwDataChunk(rawData, (uint)numSamples, (uint)numChannels, bwDataChunk.DataFormat.F64);

            if (loopStart != -1)
            {
                var sampleChunk = new bwSampleChunk();
                sampleChunk.Loops.Add(new SampleLoop() { Start = (uint)loopStart, End = (uint)loopEnd });
                sampleChunk.SampleLoops = 1;
                waveFile.Sample = sampleChunk;
            }

            waveFile.Markers = markers;

            return waveFile;
        }

        public static bwWaveFile CreateFromChannelData(float[][] sampleData, uint sampleRate, int loopStart, int loopEnd, bwMarkerList markers)
        {
            var waveFile = new bwWaveFile();
            var numChannels = sampleData.Length;
            var fmt = new bwFormatChunk((ushort)bwFormatChunk.CompressionFormat.Float, // IEEE float
                                        (ushort)numChannels,
                                        sampleRate,
                                        (uint)(sampleRate * numChannels * 4),
                                        4, // 4 bytes per sample
                                        32, // 32bits per sample
                                        new byte[0]);
            waveFile.Format = fmt;

            var numSamples = sampleData[0].Length;
            var rawData = new byte[numChannels * 4 * numSamples];
            var bw = new BinaryWriter(new MemoryStream(rawData));

            // Interleave the channel data
            for (int sampleIndex = 0; sampleIndex < numSamples; sampleIndex++)
            {
                for (int channelIndex = 0; channelIndex < numChannels; channelIndex++)
                {
                    bw.Write(sampleData[channelIndex][sampleIndex]);
                }
            }
            bw.Close();
            waveFile.Data = new bwDataChunk(rawData, (uint)numSamples, (uint)numChannels, bwDataChunk.DataFormat.F32);

            if (loopStart != -1)
            {
                var sampleChunk = new bwSampleChunk();
                sampleChunk.Loops.Add(new SampleLoop() { Start = (uint)loopStart, End = (uint)loopEnd });
                sampleChunk.SampleLoops = 1;
                waveFile.Sample = sampleChunk;
            }

            waveFile.Markers = markers;

            return waveFile;
        }

        public static bwWaveFile Create16BitFromChannelData(float[][] sampleData, uint sampleRate, int loopStart, int loopEnd, bwMarkerList markers)
        {
            var waveFile = new bwWaveFile();
            var numChannels = sampleData.Length;
            var fmt = new bwFormatChunk((ushort)bwFormatChunk.CompressionFormat.Pcm,
                                        (ushort)numChannels,
                                        sampleRate,
                                        (uint)(sampleRate * numChannels * 2),
                                        2, // bytes per sample
                                        16, // bits per sample
                                        new byte[0]);
            waveFile.Format = fmt;

            var numSamples = sampleData[0].Length;
            var rawData = new byte[numChannels * 2 * numSamples];
            var bw = new BinaryWriter(new MemoryStream(rawData));

            // Interleave the channel data
            for (int sampleIndex = 0; sampleIndex < numSamples; sampleIndex++)
            {
                for (int channelIndex = 0; channelIndex < numChannels; channelIndex++)
                {
                    short sample = (short)rage.ToolLib.Utility.DitherAndRoundSample16Bit(short.MaxValue * sampleData[channelIndex][sampleIndex]);
                    bw.Write(sample);
                }
            }
            bw.Close();
            waveFile.Data = new bwDataChunk(rawData, (uint)numSamples, (uint)numChannels, bwDataChunk.DataFormat.S16);

            if (loopStart != -1)
            {
                var sampleChunk = new bwSampleChunk();
                sampleChunk.Loops.Add(new SampleLoop() { Start = (uint)loopStart, End = (uint)loopEnd });
                sampleChunk.SampleLoops = 1;
                waveFile.Sample = sampleChunk;
            }

            waveFile.Markers = markers;

            return waveFile;
        }
    }
}
