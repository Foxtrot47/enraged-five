﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Xsl;
using audSpeechBuilder;
using rage.Compiler;
using rage.ToolLib;
using rage.ToolLib.Logging;

namespace rage {
    public class SpeechData {

        public readonly Dictionary<string, uint> ContextLookup = new Dictionary<string, uint>();
        public readonly List<VoiceEntry> SpeechEntries = new List<VoiceEntry>();
        public readonly StringTable StringTable = new StringTable();
        public readonly List<string> BankLookupTable = new List<string>();
        public readonly Dictionary<string, uint> VoiceLookup = new Dictionary<string, uint>();
        public uint TotalNumberOfContexts=0;
        public uint TotalNumberVariationElements=0;

        public SpeechData(XDocument inputDoc, string xslFile, ILog log)
        {
            inputDoc = PerformXsl(inputDoc, xslFile);

            if(!ErrorCheckAllNodes(inputDoc, log)) {
                throw new Exception("Errors detected in one or more speech nodes.");
            }
            ParseNodes(inputDoc, log);


        }

        private XDocument PerformXsl(XDocument inputDoc, string xslFile) {
            if(!String.IsNullOrEmpty(xslFile)) {
                var xmlTransform = new XslCompiledTransform();
                var buffer = new StringBuilder();
                var writer = new StringWriter(buffer);
                xmlTransform.Load(xslFile);
                xmlTransform.Transform(inputDoc.CreateReader(), null, writer);
                TextReader tr = new StringReader(buffer.ToString());
                return XDocument.Load(tr);
            }
            else return inputDoc;
        }


        private bool ErrorCheckAllNodes(XDocument inputDoc, ILog log) {
            var bSuccess = true;

            foreach(var voiceTag in
                (from tag in inputDoc.Descendants("Tag")
                 where String.Compare(tag.Attribute("name").Value, "voice", true) == 0
                 select tag).ToList()) {
                var pack = voiceTag.Ancestors("Pack").FirstOrDefault();
                var bank = voiceTag.Ancestors("Bank").FirstOrDefault();
                var parent = voiceTag.Parent;
                if(bank != null && pack != null) {
                    bSuccess &= ErrorCheckVoiceContext(pack.Attribute("name").Value + "\\" + bank.Attribute("name").Value, parent, log);
                }
            }
            return bSuccess;
        }

        private bool ErrorCheckVoiceContext(string bankName, XElement parentNode, ILog log) {
            var bSuccess = true;

            foreach(var waveNode in (from wave in parentNode.Descendants("Wave") select wave).ToList()) {
                var name = waveNode.Attribute("name").Value;
                var index = name.LastIndexOf("_");
                if (name.EndsWith("Wav", true, CultureInfo.InvariantCulture))
                {
                    try
                    {
                        var variationString = name.Substring(index + 1, ((name.IndexOf('.') - index) - 1));
                        Byte.Parse(variationString);
                    }
                    catch (Exception)
                    {
                        log.Error("Invalid variation number for wave: " + name + " bank: " + bankName);
                        bSuccess = false;
                    }
                }
            }
            return bSuccess;
        }


        private void ParseNodes(XDocument inputDoc, ILog log) {
            foreach(XElement voiceTag in
                (from tag in inputDoc.Descendants("Tag")
                 where String.Compare(tag.Attribute("name").Value, "voice", true) == 0
                 select tag).ToList()) {
                XElement pack = voiceTag.Ancestors("Pack").FirstOrDefault();
                XElement bank = voiceTag.Ancestors("Bank").FirstOrDefault();
                XElement parent = voiceTag.Parent;

                if(pack != null && bank != null) {
                    var voiceName = voiceTag.Attribute("value").Value;
                    var voiceNameHash = GenerateHash(voiceName);
                    //Add hash/voice key value pair to dictionary
                    if(!VoiceLookup.ContainsKey(voiceName)) {
                        VoiceLookup.Add(voiceName, voiceNameHash);
                    }

                    var speechEntry =
                        (from se in SpeechEntries where se.NameHash == voiceNameHash select se).FirstOrDefault();
                    if(speechEntry == null) {
                        speechEntry = new VoiceEntry();
                        speechEntry.NameHash = voiceNameHash;
                        speechEntry.Name = voiceName;
                        String painType = "";
                        foreach(XElement descendant in parent.Descendants("Tag")) {
                            if(String.Compare(descendant.Attribute("name").Value, "pain", true) == 0) {
                                painType = descendant.Attribute("value").Value;
                                break;
                            }
                        }

                        speechEntry.PainHash = (byte)(GenerateHash(painType) & 0xff);
                        speechEntry.PainType = painType;
                        speechEntry.Contexts = new List<VoiceContextEntry>();
                        if(SpeechEntries.Contains(speechEntry)) {
                            log.Error("Duplicate speechEntry {0}", speechEntry.Name);
                        }
                        SpeechEntries.Add(speechEntry);
                    }
                    else {
                        String painType = "";
                        foreach(XElement descendant in parent.Descendants("Tag")) {
                            if(String.Compare(descendant.Attribute("name").Value, "pain", true) == 0) {
                                painType = descendant.Attribute("value").Value;
                                break;
                            }
                        }
                        if(!painType.Equals("")) {
                            if(speechEntry.PainHash == 0)
                                speechEntry.PainHash = (byte)(GenerateHash(painType) & 0xff);
                            else if((byte)(GenerateHash(painType) & 0xff) != speechEntry.PainHash) {
                                log.Error("Mismatched pain tags for voice {0}", voiceTag.Value);
                                throw new Exception("Mismatched pain tags for voice.");
                            }
                        }
                    }

                    var bankName = (pack.Attribute("name").Value + "\\" + bank.Attribute("name").Value).ToUpper();
                    //create the relative bank path
                    List<string> relativeBankPath = new List<string>();
                    XElement bankPathElement = bank.Parent;
                    while (!bankPathElement.Name.LocalName.Equals("Pack"))
                    {
                        relativeBankPath.Insert(0, bankPathElement.Attribute("name").Value);
                        bankPathElement = bankPathElement.Parent;
                    }
                    relativeBankPath.Insert(0, bankPathElement.Attribute("name").Value);
                    AddVoiceContext(bankName, relativeBankPath.ToArray(), parent, speechEntry);
                }
                else {
                    if(pack == null && bank == null)
                        log.Warning("Ignoring voice tag pack and bank is null {0}", voiceTag.Value);
                    else if(pack == null)
                        log.Warning("Ignoring voice tag pack is null {0}", voiceTag.Value);
                    else if(bank == null)
                        log.Warning("Ignoring voice tag bank is null {0} {1}", voiceTag.Value, pack.FirstAttribute.Value);
                }
            }
        }

        private void AddVoiceContext(string bankName, string[] relativeBankPath, XElement parentNode, VoiceEntry speechEntry) {
            foreach(var waveNode in (from wave in parentNode.Descendants("Wave") select wave).ToList()) {
                if (FindTag(waveNode, "DoNotBuild") != null || !waveNode.Attribute("name").Value.EndsWith(".wav", StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }
                var name = waveNode.Attribute("name").Value;
                var index = name.LastIndexOf("_");
                var contextName = name.Substring(0, index);
                var variationString = name.Substring(index + 1, ((name.IndexOf('.') - index) - 1));
                byte variationNum;

                try {
                    variationNum = Byte.Parse(variationString);
                }
                catch(FormatException) {
                    throw new Exception("Invalid variation number for wave: " + name + " bank: " + bankName);
                }

                var nameHash = GenerateHash(contextName);

                var speechContext = (from sc in speechEntry.Contexts where sc.NameHash == nameHash select sc).FirstOrDefault();

                // if this voice doesn't already have an entry for this context we need to create one
                if(speechContext == null) {
                    if(!ContextLookup.ContainsKey(contextName)) {
                        ContextLookup.Add(contextName, nameHash);
                    }

                    speechContext = new VoiceContextEntry();
                    speechContext.NameHash = nameHash;
                    speechContext.Name = contextName;
                    speechContext.VoiceName = speechEntry.Name;
                    speechContext.VoiceHash = speechEntry.NameHash;
                    speechContext.VariationData = new List<VariationDataEntry>();
                    speechContext.VariationDataName = new List<string>();

                    //variation and context data produced by xsl. (This only applies to radio speech atm)
                    speechContext.ContextData = FindContextData(waveNode);

                    var hash = new Hash { Value = bankName };
                    StringTable.Add(hash);
                    var bankNameIndex = StringTable.IndexOf(bankName);
                    speechContext.BankNameIndex = (uint)bankNameIndex;
                    speechContext.BankName = bankName;

                    string relativeBankDepotPath = "";
                    foreach (string pathElement in relativeBankPath)
                    {
                        if (relativeBankDepotPath == "") relativeBankDepotPath = pathElement;
                        else relativeBankDepotPath += "\\" + pathElement;
                    }

                    string relativeFileLocalPath = "";
                    XElement wavePathElement = waveNode.Parent;
                    while (!wavePathElement.Name.LocalName.Equals("Pack"))
                    {
                        if (relativeFileLocalPath == "") relativeFileLocalPath = wavePathElement.Attribute("name").Value;
                        else relativeFileLocalPath = wavePathElement.Attribute("name").Value + "\\" + relativeFileLocalPath;
                        wavePathElement = wavePathElement.Parent;
                    }
                    relativeFileLocalPath = wavePathElement.Attribute("name").Value + "\\" + relativeFileLocalPath;

                    speechContext.RelativeBankPath = relativeBankDepotPath;
                    speechContext.RelativeFilePath = relativeFileLocalPath;
                    //Console.WriteLine("speechContext.BankName {0}", speechContext.BankName);

                    if(!BankLookupTable.Contains(bankName))
                        BankLookupTable.Add(bankName);

                    speechEntry.Contexts.Add(speechContext);
                    TotalNumberOfContexts++;
                }

                // search for optional per-variation data (generated by Xsl)
                byte data;
                if(FindVariationData(waveNode, out data)) {
                    //Console.WriteLine("Found variation info data {0}", data);
                    TotalNumberVariationElements++;
                    speechContext.VariationData.Add(new VariationDataEntry { Data = data, Number = variationNum });
                }

                speechContext.NumVariations++;
            }
        }

        private static byte FindContextData(XElement node) {
            var contextTag =
                (from el in node.Descendants("Tag") where el.Attribute("name").Value == "contextData" select el).
                    FirstOrDefault();

            return contextTag == null ? (byte)0 : Byte.Parse(contextTag.Attribute("value").Value);
        }

        private static bool FindVariationData(XElement node, out byte ret) {
            var varTag =
                (from el in node.Descendants("Tag") where el.Attribute("name").Value == "variationData" select el).
                    FirstOrDefault();
            ret = 0;
            if(varTag == null) {
                foreach(var ancestor in node.Ancestors()) {
                    foreach(var el in ancestor.Elements("Tag")) {
                        if(el.Attribute("name").Value == "variationData") {
                            varTag = el;
                            break;
                        }
                    }
                }
            }

            if(varTag != null) {
                ret = Byte.Parse(varTag.Attribute("value").Value);
                return true;
            }

            return false;
        }

        private static XElement FindTag(XElement element, string tagName) {
            foreach(var e in element.AncestorsAndSelf()) {
                foreach(var tag in e.Elements("Tag")) {
                    if(String.Compare(tag.Attribute("name").Value, tagName, true) == 0) {
                        return tag;
                    }
                }
            }
            return null;
        }

        public static UInt32 GenerateHash(string str) {
            if(String.IsNullOrEmpty(str)) {
                return 0;
            }

            byte[] ut8f = Encoding.UTF8.GetBytes(str);
            uint key = 0;

            for(int i = 0; i < str.Length; i++) {
                byte character = ut8f[i];
                if(character >= 'A' && character <= 'Z') {
                    character += 'a' - 'A';
                }
                else if(character == '\\') {
                    character = (byte)'/';
                }

                key += character;
                key += (key << 10);
                key ^= (key >> 6);
            }

            key += (key << 3);
            key ^= (key >> 11);
            key += (key << 15);

            return key;

        }
    }
}
