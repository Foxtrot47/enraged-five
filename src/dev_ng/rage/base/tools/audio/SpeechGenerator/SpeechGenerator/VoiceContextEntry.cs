﻿using System;
using System.Collections.Generic;

namespace audSpeechBuilder
{
    public class VoiceContextEntry
    {
        public UInt32 BankNameIndex { get; set; }
        public string BankName { get; set; }
        //path to the bank including pack, separated by '/'
        public string VoiceName { get; set; }
        public string RelativeBankPath { get; set; }
        public string RelativeFilePath { get; set; }
        public Int32 VariationDataOffsetBytes { get; set; }

        public bool Used { get; set; }
        public bool GroupUsed { get; set; }
       
        public UInt32 NameHash { get; set; }
        public UInt32 VoiceHash { get; set; }
        public string DebugVcdiInfoRef {
            get { return (NameHash ^ VoiceHash).ToString("X8") + "_VCDI"; }
        }
        public string DebugCdiInfoRef
        {
            get { return NameHash.ToString("X8") + "_CDI"; }
        }
        public string Name { get; set; }
        public Byte ContextData { get; set; }
        public Byte NumVariations;
        public List<VariationDataEntry> VariationData { get; set; }
        public List<string> VariationDataName { get; set; }
    }
}
