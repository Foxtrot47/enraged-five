﻿using System;
using System.Collections.Generic;

namespace audSpeechBuilder
{
    public class VoiceEntry
    {
        public string Name { get; set; }
        public UInt32 NameHash { get; set; }
        public string DebugVdiInfoRef {
            get { return NameHash.ToString("X8") + "_VDI"; }
        }
        public byte PainHash { get; set; }
        public string PainType { get; set; }
        public UInt32 ContextOffsetBytes { get; set; }
        public List<VoiceContextEntry> Contexts { get; set; }
    }
}   
