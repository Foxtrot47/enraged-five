﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Xsl;
//using audAssetBuilderCommon;
using rage;
using rage.Compiler;
using rage.ToolLib.Writer;
using rage.ToolLib.Logging;
using rage.Generator;


namespace audSpeechBuilder
{
    public class SpeechBuilder
    {
        private SpeechData data=null;
        public string XSLFile { get; private set; }
        public string DumpXMLFile { get; private set; }

        public SpeechBuilder(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                Console.WriteLine("Arg[]: " + args[i]);
            }

            if(args.Length >= 1)
            {
                XSLFile = args[0];
                Console.WriteLine("XSLFile: " + XSLFile);
                if (args.Length == 2)
                {
                    DumpXMLFile = args[1];
                    Console.WriteLine("DumpXMLFile: " + DumpXMLFile);
                }
            }
        }

        public XDocument Generate(XDocument inputDoc, ILog log)
        {
            if(data==null) data = new SpeechData(inputDoc, XSLFile, log);
            var root = new XElement("Objects");
            SerializeVariationGroupInfoData(root, data.SpeechEntries);
            SerializeVoiceContextVariationXMLData(root, data.SpeechEntries, log);
            SerializePainData(root, data.SpeechEntries);
            SerializeBankLookupData(root, data.BankLookupTable);

            var doc = new XDocument(root);
            if(!string.IsNullOrEmpty(DumpXMLFile)) {
                Save(doc, DumpXMLFile);
            }
            return doc;
        }

        public XDocument GenerateDebugInfoLookup(XDocument inputDoc, ILog log)
        {
            if(data==null) data = new SpeechData(inputDoc, XSLFile, log);
            var root = new XElement("Objects");
            SerializeContextDebugInfo(root, data.SpeechEntries);
            SerializeVoiceDebugInfo(root, data.SpeechEntries);
            var doc = new XDocument(root);
            if(!string.IsNullOrEmpty(DumpXMLFile)) {
                Save(doc, DumpXMLFile);
            }
            return doc;
        }

        private void SerializeContextDebugInfo(XElement parentElement, List<VoiceEntry> speechEntries)
        {
            List<string> addedContextDebugInfos = new List<string>();
            List<string> addedVoiceContextDebugInfos = new List<string>();

            foreach (VoiceEntry voiceEntry in speechEntries)
            {
                foreach (VoiceContextEntry context in voiceEntry.Contexts)
                {
                    if(!addedContextDebugInfos.Contains(context.DebugCdiInfoRef))
                    {
                        addedContextDebugInfos.Add(context.DebugCdiInfoRef);
                        parentElement.Add(
                            new XElement("ContextDebugInfo", new XAttribute("name", context.DebugCdiInfoRef),
                                                        new XElement("ContextNameString", context.Name))
                                                        );
                    }
                    if (!addedVoiceContextDebugInfos.Contains(context.DebugVcdiInfoRef))
                    {
                        addedVoiceContextDebugInfos.Add(context.DebugVcdiInfoRef);
                        parentElement.Add(
                            new XElement("VoiceContextDebugInfo", new XAttribute("name", context.DebugVcdiInfoRef),
                                                        new XElement("ContextFilePath", context.RelativeFilePath),
                                                        new XElement("ContextBankPath", context.RelativeBankPath))
                                                        );
                    }
                }
            }
        }

        private void SerializeVoiceDebugInfo(XElement parentElement, List<VoiceEntry> speechEntries)
        {
            foreach(VoiceEntry voiceEntry in speechEntries) {
                XElement voiceDebugInfoElement = new XElement("VoiceDebugInfo", new XAttribute("name", voiceEntry.DebugVdiInfoRef),
                                                        new XElement("VoiceNameString", voiceEntry.Name));

                foreach(VoiceContextEntry context in voiceEntry.Contexts) {
                    XElement contextInfoRef = new XElement("ContextList", new XElement("Context", context.DebugCdiInfoRef));
                    voiceDebugInfoElement.Add(contextInfoRef);
                }
                parentElement.Add(voiceDebugInfoElement);
            }
        }

        private void SerializeBankLookupData(XElement parentElement, List<string> bankLookupTable)
        {
            foreach(string bankName in bankLookupTable)
            {
                var hash = SpeechData.GenerateHash(bankName);
                string indexString = String.Format("{0}", bankLookupTable.IndexOf(bankName));
                var bankLookup = new XElement("BankNameTable", new XAttribute("name", indexString),
                                                        new XElement("bankName", bankName)
                                             );
                parentElement.Add(bankLookup);
            }
        }

        private void SerializePainData(XElement parentElement, List<VoiceEntry> speechEntries)
        {
            List<VoiceEntry> speechEntriesSet = new List<VoiceEntry>();

            foreach (var voiceEntry in speechEntries)
            {
                if (!speechEntriesSet.Contains(voiceEntry))
                    speechEntriesSet.Add(voiceEntry);
            }


            foreach (var voiceEntry in speechEntriesSet)
            {
                if (voiceEntry.PainHash != 0)
                {
                    //var painVoiceEntry = new XElement("Voice", new XAttribute("name", voiceEntry.Name));
                    var painVoiceEntry = new XElement("Voice", new XAttribute("name", voiceEntry.Name),
                                            new XElement("painType", (byte)(SpeechData.GenerateHash(voiceEntry.PainType) & 0xff))
                                         );
                    parentElement.Add(painVoiceEntry);
                }
                else
                {
                    var painVoiceEntry = new XElement("Voice", new XAttribute("name", voiceEntry.Name));
                    parentElement.Add(painVoiceEntry);
                }

            }
        }

        private void SerializeVariationGroupInfoData(XElement parentElement, List<VoiceEntry> speechEntries)
        {
            foreach (var voiceEntry in speechEntries)
            {
                var voiceHash = voiceEntry.NameHash;
                foreach (var voiceContext in voiceEntry.Contexts)
                {
                    if (voiceContext.NumVariations > 0 && !voiceContext.Used)
                    {
                        //var hash = voiceHash ^ voiceContext.NameHash;
                        var groupNumber = 0;
                        foreach (var similarVoiceContext in voiceEntry.Contexts)
                        {
                            // look for the same context hash that hasn't already been added
                            if (voiceContext.NameHash == similarVoiceContext.NameHash && !similarVoiceContext.GroupUsed)
                            {
                                string vgidName = voiceEntry.Name.ToUpper() + "_" + voiceContext.Name.ToUpper() + "_" + groupNumber;
                                var varGroupInfoData = new XElement("VariationGroupInfoData", new XAttribute("name", vgidName));
                                bool valid = false;
                                foreach (var varData in similarVoiceContext.VariationData)
                                {
                                    var variationDataEntry = new XElement("VariationData",
                                                                new XElement("val", varData.Data)
                                                             );
                                    varGroupInfoData.Add(variationDataEntry);
                                    valid = true;
                                }
                                similarVoiceContext.GroupUsed = true;
                                groupNumber++;
                                if (valid)
                                {
                                    similarVoiceContext.VariationDataName.Add(vgidName);
                                    parentElement.Add(varGroupInfoData);
                                }
                             
                            }
                        }
                    }
                }
            }
        }


        private void SerializeVoiceContextVariationXMLData(XElement parentElement, List<VoiceEntry> speechEntries, ILog log)
        {

            var hashCheck = new Dictionary<uint, string>();

            foreach (var voiceEntry in speechEntries)
            {
                var voiceHash = voiceEntry.NameHash;
                foreach (var voiceContext in voiceEntry.Contexts)
                {
                    if (voiceContext.NumVariations > 0 && !voiceContext.Used)
                    {
                        var hash = voiceHash ^ voiceContext.NameHash;
                        if (voiceHash == voiceContext.NameHash) // the above produces a hash of 0 in this case
                            hash = voiceHash;
                        if (hashCheck.ContainsKey(hash))
                        {
                            log.Error("Duplicate Hash: " + voiceEntry.Name + " context: " + voiceContext.Name + " (clashes with: " + hashCheck[hash] + ")");
                        }
                        else
                        {
                            hashCheck.Add(hash, string.Format("{0} - {1}", voiceEntry.Name, voiceContext.Name));
                        }
                    }
                }
            }

            foreach (var voiceEntry in speechEntries)
            {
                var voiceHash = voiceEntry.NameHash;
                foreach (var voiceContext in voiceEntry.Contexts)
                {
                    if (voiceContext.NumVariations > 0 && !voiceContext.Used)
                    {
                        var hash = voiceHash ^ voiceContext.NameHash;
                        if (voiceHash == voiceContext.NameHash) // the above produces a hash of 0 in this case
                            hash = voiceHash;

                        string hashString = String.Format("{0:X}", hash);
                        var voiceContextInfo = new XElement("VoiceContextInfo", new XAttribute("name", hashString));

                        var description = new XElement("Description", voiceEntry.Name.ToUpper() + " - " + voiceContext.Name.ToUpper());
                        voiceContextInfo.Add(description);

                        bool foundVarGroupData = false;
                        string vdHashString = String.Format("{0:X}_VD", hash);
                        var voiceContextInfoVarGroupData = new XElement("VoiceContextInfoVarGroupData", new XAttribute("name", vdHashString));

                        foreach (var similarVoiceContext in voiceEntry.Contexts)
                        {
                            // look for the first context hash that hasn't already been added
                            if (voiceContext.NameHash == similarVoiceContext.NameHash && !similarVoiceContext.Used)
                            {
                                //var variationGroupInfo = new XElement("VariationGroupInfo",
                                //                            new XElement("numVariations", voiceContext.NumVariations),
                                //                            new XElement("bankNameTableIndex", voiceContext.BankNameIndex)
                                //                        );
                                //voiceContextInfo.Add(variationGroupInfo);

                                var numVariationsElement = new XElement("numVariations", voiceContext.NumVariations);
                                var bankNameTableIndexElement = new XElement("bankNameTableIndex", voiceContext.BankNameIndex);
                                voiceContextInfo.Add(numVariationsElement);
                                voiceContextInfo.Add(bankNameTableIndexElement);

                                similarVoiceContext.Used = true;

                                //if (similarVoiceContext.VariationDataName.Count > 0)
                                //{
                                //    foreach (var variationDataName in similarVoiceContext.VariationDataName)
                                //    {
                                //        var variationGroupInfoData = new XElement("VariationGroupInfoData");
                                //        var variationDataRef = new XElement("ref", variationDataName);
                                //        variationGroupInfoData.Add(variationDataRef);
                                //        voiceContextInfo.Add(variationGroupInfoData);
                                //    }
                                //}
                                if (similarVoiceContext.VariationDataName.Count > 0)
                                {
                                    //var vdDescription = new XElement("Description", voiceEntry.Name.ToUpper() + " - " + voiceContext.Name.ToUpper());

                                    var variationDataRef = new XElement("ref", similarVoiceContext.VariationDataName[0]);
                                    voiceContextInfoVarGroupData.Add(variationDataRef);
                                    foundVarGroupData = true;
                                }

                                break; // we don't support a voice spanning banks, so only add the first entry found.
                            }
                        }

                        parentElement.Add(voiceContextInfo);
                        if (foundVarGroupData == true)
                            parentElement.Add(voiceContextInfoVarGroupData);
                           
                    }
                }
            }
        }      

        public bool Save(XDocument doc, string filePath)
        {
            try
            {
                doc.Save(filePath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}