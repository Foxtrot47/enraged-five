namespace rage
{

    using System;
    using System.Linq;
    using System.Xml.Linq;
    using rage.Generator;
    using rage.ToolLib.Logging;
    using audSpeechBuilder;
    using rage.Types;

    public class SpeechGenerator : IGenerator
    {
        private SpeechBuilder speechBuilder;

        #region IGenerator Members

        public bool Init(ILog log, string workingPath, audProjectSettings projectSettings, ITypeDefinitionsManager typeDefManager, params string[] args)
        {
            log.Information("Speech Generator :");
            for (int i = 0; i < args.Length; i++)
            {
                log.Information("     {0}", args[i]);
            }

            speechBuilder = new SpeechBuilder(args);

            return true;
        }

        public XDocument Generate(ILog log, XDocument inputDoc)
        {
            try
            {
                return speechBuilder.Generate(inputDoc, log);
            }
            catch (Exception ex)
            {
                log.Exception(ex);
            }
            return null;
        }

        public void Shutdown()
        {
        }

        #endregion
    }
}