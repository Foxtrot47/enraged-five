﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using audSpeechBuilder;
using rage.Generator;

namespace rage {
    public class SpeechLookupGenerator: IGenerator {

        private SpeechBuilder speechBuilder;

        public bool Init(ToolLib.Logging.ILog log, string workingPath, audProjectSettings projectSettings, Types.ITypeDefinitionsManager typeDefManager, params string[] args) {

            log.Information("Speech Lookup Generator :");
            for(int i = 0; i < args.Length; i++) {
                log.Information("     {0}", args[i]);
            }

            speechBuilder = new SpeechBuilder(args);

            return true;
        }

        public System.Xml.Linq.XDocument Generate(ToolLib.Logging.ILog log, System.Xml.Linq.XDocument inputDoc) {
            try {
                return speechBuilder.GenerateDebugInfoLookup(inputDoc, log);
            }
            catch(Exception ex) {
                log.Exception(ex);
            }
            return null;
        }

        public void Shutdown() {
        }
    }
}
