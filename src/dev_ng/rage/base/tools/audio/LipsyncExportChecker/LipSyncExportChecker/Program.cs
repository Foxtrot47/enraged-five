﻿using rage.ToolLib.CmdLine;
using Rockstar.AssetManager.Interfaces;
using Rockstar.AssetManager.Main;
using System;
using System.Collections.Generic;
using System.IO;

namespace LipsyncBuiltWavesChecker
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length < 4)
            {
                Console.WriteLine("Please use arguments -animFolder -latestDate -exportForAudio -csvFile to run this script");
                return;
            }

            IAssetManager assetManager = new AutoAssetManager().AssetManager;

            List<string> log = new List<string>();

            CmdLineParser cmdParser = new CmdLineParser(args);

            string animFolder = cmdParser.Arguments["animFolder"];
            DateTime latestDate = Convert.ToDateTime(cmdParser.Arguments["latestDate"]);
            string exportForAudio = cmdParser.Arguments["exportForAudioPath"];
            string csvFile = cmdParser.Arguments["csvFile"];

            if (animFolder == null || latestDate == null || exportForAudio == null || csvFile == null)
            {
                Console.WriteLine("Please use arguments -animFolder -latestDate -exportForAudio -csvFile to run this script");
                return;
            }

            csvFile = Path.ChangeExtension(csvFile, ".csv");
            Console.WriteLine("Starting custom lipsync check");
            log.Add("Missing custom lipsync");

            List<string> customLipsyncFiles = new List<string>();
            RecursiveFileSearchHelper.WalkDirectoryTreeNames(new DirectoryInfo(animFolder), "*.CUSTOM_LIPSYNC", customLipsyncFiles);
            List<FileInfo> customAnimFiles = new List<FileInfo>();
            RecursiveFileSearchHelper.WalkDirectoryTree(new DirectoryInfo(exportForAudio), "*.anim", customAnimFiles);

            List<FileInfo> missingCustomLipsyncFiles = new List<FileInfo>();

            for (int i = 0; i < customAnimFiles.Count; i++)
            {
                Console.Write("\r{1} % Checking file: {0} ........................", customAnimFiles[i].Name, (((float)i / (float)customAnimFiles.Count) * 100).ToString("0.00"));

                string equivalent = Path.GetFileNameWithoutExtension(customAnimFiles[i].FullName) + ".CUSTOM_LIPSYNC";
                if (!customLipsyncFiles.Contains(equivalent))
                {
                    missingCustomLipsyncFiles.Add(customAnimFiles[i]);
                    log.Add(customAnimFiles[i].FullName);
                }
            }

            Console.WriteLine("Starting ycd time check on {0} for files dated older than {1}", animFolder, latestDate.ToString());


            //OUTDATED FILES:
            log.Add("Outdated ycds");
            List<FileInfo> animFiles = new List<FileInfo>();
            RecursiveFileSearchHelper.WalkDirectoryTree(new DirectoryInfo(animFolder), "*.ycd", animFiles);

            for (int i = 0; i < animFiles.Count; i++)
            {
                Console.Write("\r{1} % Checking file: {0} ........................", animFiles[i].Name, (((float)i / (float)animFiles.Count) * 100).ToString("0.00"));
                FileInfo anim = animFiles[i];
                DateTime animDateCreation = assetManager.GetLatestCheckinTime(anim.FullName);

                if (animDateCreation.CompareTo(latestDate) < 0)
                {
                    if (animFiles[i].Directory.GetFiles(animFiles[i].Name.Replace(".", ".PROCESSED.")).Length == 0)
                    {
                        if (assetManager.ExistsAsAsset(animFiles[i].FullName.Replace("\\LIPSYNCANIMS\\", "\\WAVES\\").Replace(".ycd", ".wav")))
                        {
                            bool isCustomAnim = assetManager.ExistsAsAsset(Path.ChangeExtension(animFiles[i].FullName, ".CUSTOM_LIPSYNC"));
                            string customAnim = isCustomAnim ? "Custom Anim" : "Auto Generated";                      
                            log.Add(animFiles[i].FullName + "," + (animDateCreation)+"," + customAnim );
                        }
                    }
                }
            }

            Console.WriteLine("\nWriting to {0}", csvFile);
            string joined = string.Join("\n", log.ToArray());

            try
            {
                File.WriteAllText(csvFile, joined);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
         }
    }
}