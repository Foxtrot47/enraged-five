// audrpfbuilder.h
#pragma once

using namespace audAssetManagement;
using namespace audBuildCommon;
using namespace rage;
using namespace System;
using namespace System::Xml;
using namespace System::Collections;
using namespace System::IO;

namespace audRPFBuild
{
	public __gc class audRPFBuilder : public audPostBuilderBase
	{
	public:
		audRPFBuilder();

		void Build(audAssetManager *assetManager, audProjectSettings *projectSettings, String *buildPlatform,
				   audBuildClient *buildClient, XmlDocument *builtWavesXml, XmlDocument *pendingWavesXml, bool shouldBuildLocally,
				   audBuildComponent *buildComponent, bool m_IsDeferredBuild);

	private:
		void Init();
		void ParsePendingWavesXml();
		void CreateRpfXml();
		void PrepareAssets();
		void GenerateRPFs();
		void CommitAssets();
		void Shutdown();

		void ReleaseAssets();
		String *FindTag(XmlNode *parentNode);
		bool ShouldBuild(XmlNode *parentNode);
		bool FindOperation(XmlNode *parentNode, bool checkRemove);
		bool FindBankWaveOperation(XmlNode *bank);
		void GetBanks(XmlNode *node, ArrayList *banks);

		audBuildClient *m_BuildClient;
		audAssetManager *m_AssetManager;
		audProjectSettings *m_ProjectSettings;
		String *m_BuildPlatform;
		String *m_AssetBuildDataPath;
		String *m_LocalBuildDataPath;
		String *m_AssetBuildInfoPath;
		String *m_LocalBuildInfoPath;
		XmlDocument *m_PendingWavesXml;
		XmlDocument *m_BuiltWavesXml;
		ArrayList *m_PackListRPF;
		bool m_IsDeferredBuild;
		bool m_EnableEncryption;
		ArrayList *m_IncludeList;
		ArrayList *m_ExcludeList;
		String *m_OutputPath;
	};
};


