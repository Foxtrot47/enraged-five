// This is the main DLL file.

#include "stdafx.h"
#include "audrpfbuilder.h"
#include "audassetmanager/assetmanagertypes.h"
#include "audbuildmanager/builddefs.h"
#include "audrpf/audrpf.h"

using namespace System::Runtime::InteropServices;

namespace audRPFBuild
{
	static CAudioRpf *m_AudRpfBuilder = 0;

	audRPFBuilder::audRPFBuilder()
	{
	}

	void audRPFBuilder::Build(audAssetManager* assetManager, 
		audProjectSettings* projectSettings, 
		String* buildPlatform, 
		audBuildClient* buildClient,
		XmlDocument* builtWavesXml, 
		XmlDocument* pendingWavesXml, 
		bool /*shouldBuildLocally*/, 
		audBuildComponent* buildComponent,
		bool isDeferredBuild)
	{
		m_EnableEncryption = projectSettings->ShouldEncrypt();
		m_BuildClient = buildClient;
		m_AssetManager = assetManager;
		m_ProjectSettings = projectSettings;
		m_BuildPlatform = buildPlatform;
		m_PendingWavesXml = pendingWavesXml;
		m_BuiltWavesXml = builtWavesXml;
		m_IsDeferredBuild = isDeferredBuild;
		m_ExcludeList = new ArrayList();
		m_IncludeList = new ArrayList();

		if(buildComponent->m_OutputPath)
		{
			m_OutputPath = buildComponent->m_OutputPath;
		}
		else
		{
			m_OutputPath = "rpf.xml";
		}
		if(buildComponent->m_InputPath)
		{
			try
			{
				XmlDocument *doc = new XmlDocument();
				doc->Load(buildComponent->m_InputPath);

				for( int i =0; i<doc->DocumentElement->ChildNodes->Count; i++)
				{
					XmlNode *childNode = doc->DocumentElement->ChildNodes->Item(i);
					if(childNode->Name->Equals(S"Include"))
					{
						m_IncludeList->Add(childNode->InnerText);
					}
					else if(childNode->Name->Equals(S"Exclude"))
					{
						m_ExcludeList->Add(childNode->InnerText);
					}
				}
			}

			catch(Exception *e)
			{
				throw new audBuildException(e->ToString());
			}
		}

		try
		{
			m_BuildClient->ReportProgress(-1, -1, S"Starting RPF build", true);

			Init();
			ParsePendingWavesXml();
			CreateRpfXml();
			PrepareAssets();
			GenerateRPFs();
			CommitAssets();
			Shutdown();

			m_BuildClient->ReportProgress(-1, -1, S"Finished RPF build", true);
		}
		catch (audBuildException *e)
		{
			ReleaseAssets();
			Shutdown();

			// pass upwards
			throw(e);
		}
		catch (Exception* e)
		{
			ReleaseAssets();
			Shutdown();

			// pass upwards
			throw(e);	
		}
	}

	void audRPFBuilder::Init()
	{
		CErrorManager::InitClass(false);
		audPlatformSpecific::InitClass(m_BuildPlatform);

		m_AudRpfBuilder = new CAudioRpf();
		m_AudRpfBuilder->Init();

		m_PackListRPF = new ArrayList();

		//Generate commonly-used paths.
		m_AssetBuildDataPath = m_ProjectSettings->GetBuildOutputPath();
		m_LocalBuildDataPath = m_AssetManager->GetWorkingPath(m_AssetBuildDataPath);

		m_AssetBuildInfoPath = m_ProjectSettings->GetBuildInfoPath();
		m_LocalBuildInfoPath = m_AssetManager->GetWorkingPath(m_AssetBuildInfoPath);

		if (m_LocalBuildDataPath == 0)
		{
			audBuildException *e = new audBuildException(S"Failed to find working path for Build Data folder");
			throw(e);
		}
	}


	void audRPFBuilder::ParsePendingWavesXml()
	{
		XmlNode *pendingNode = m_PendingWavesXml->DocumentElement->FirstChild;

		while (pendingNode)
		{
			bool isPackRPF = false;

			if (pendingNode->Name->Equals(S"Pack"))
			{
				//This pack is in the pending wave list, so it must be being modified in some way,
				//all we need to determine now is whether or not it requires an RPF to be built.
				//Eposidic files are mounted diffrently we need to create the rpf but not add to 
				//rpf.xml file;
				XmlAttribute * nameAttribute = pendingNode->Attributes->get_ItemOf(S"name");

					String *name = 0;
					if (nameAttribute)
					{
						name = nameAttribute->get_Value();
					}


					XmlNode *builtNode = m_BuiltWavesXml->DocumentElement->FirstChild;
					XmlNode *ExistingBuiltNode;

					XmlAttribute *builtNameAttribute;
					String *builtName;
					while(builtNode)
					{
						//Find corresponding built node
						builtNameAttribute = builtNode->Attributes->get_ItemOf(S"name");
						builtName = 0;
						if(builtNameAttribute)
						{
							builtName = builtNameAttribute->get_Value();
						}
						if(builtNode->Name->Equals(S"Pack")&& builtName->Equals(name))
						{
							//builtNodeFound
							ExistingBuiltNode = builtNode;
							break;
						}
						builtNode = builtNode->NextSibling;
					}

					// does this pack have an rpf tag?
					String *rpfValue = FindTag(pendingNode);
					if (!rpfValue)
					{
						if(ExistingBuiltNode)
						{
							rpfValue = FindTag(ExistingBuiltNode);
						}
					}

					// is the tag valid?
					if (rpfValue)
					{
						if (!rpfValue->Equals("0") || !rpfValue->Equals(""))
						{
							isPackRPF = true;
						}
					}


					//at this point built and pending should be merged therefore the
					//ExistingBuiltNode should exist
					bool bShouldBuild = true;
					if(ExistingBuiltNode)
					{
						bShouldBuild = ShouldBuild(ExistingBuiltNode);
					}



					if(name)
					{
						if(m_IsDeferredBuild || bShouldBuild)
						{
							if(isPackRPF)
							{
								// add this pack to the list to generate
								System::Diagnostics::Debug::WriteLine(String::Concat("**** Adding RPF ",name));
								///check that all files in the bank directory are valid
								DirectoryInfo *dir = new DirectoryInfo(String::Concat(m_LocalBuildDataPath, g_WavePaksPath, name));
								FileInfo *files []= dir->GetFiles();
								ArrayList *banks = new ArrayList();
								GetBanks(builtNode,banks);
								for(int i=0;i<files->Count;i++)
								{
									int index = files[i]->FullName->LastIndexOf('\\');
									String *bankName = files[i]->FullName->Substring(index+1);
									if(!banks->Contains(bankName))
									{
										throw new audBuildException(String::Concat(name," bank directory has an extra file: ",bankName));
									}
								}
								m_PackListRPF->Add(name);
							}
							else
							{
								//This pack should not have an RPF, so ensure that we don't have one in under asset management.
								String *localWavePackPath = String::Concat(m_LocalBuildDataPath, g_WavePaksPath);
								String *localPackPath = String::Concat(localWavePackPath, name, ".rpf");
								String *assetWavePackPath = String::Concat(m_AssetBuildDataPath, g_WavePaksPath);
								String *assetPackPath = String::Concat(assetWavePackPath, name, ".rpf");

								m_AssetManager->DeleteAsset(localPackPath, assetPackPath, true);
							}
						}
					}
				}
			

			pendingNode = pendingNode->NextSibling;
		}
	}

	void audRPFBuilder::GetBanks(XmlNode *node, ArrayList *banks)
	{
		XmlNode *childNode = node->FirstChild;
		while (childNode)
		{
			if (childNode->Name->Equals(S"Bank"))
			{
				banks->Add(childNode->Attributes->GetNamedItem(S"name")->Value);
			}

			childNode = childNode->NextSibling;
		}


		for(int i =0; i<node->ChildNodes->Count; i++)
		{
			if(!node->ChildNodes->Item(i)->Name->Equals(S"Bank"))
			{
				GetBanks(node->ChildNodes->Item(i),banks);
			}
		}
	}


	void audRPFBuilder::CreateRpfXml()
	{
		XmlNode *builtNode = m_BuiltWavesXml->DocumentElement->FirstChild;

		//create new xml document
		XmlDocument *rpfXml = new XmlDocument();
		rpfXml->AppendChild(rpfXml->CreateElement("audRpf"));
		while (builtNode)
		{
			bool isPackRPF = false;

			if (builtNode->Name->Equals(S"Pack"))
			{

				//add episodic check in here
				//We don't care if the pack has an RPF tag if it's excluded or not included
				bool bAllow = true;
				XmlAttribute *nameAttribute = builtNode->Attributes->get_ItemOf(S"name");
				//check include List
				if(!m_IncludeList->Count==0)
				{
					bool found = false;
					IEnumerator* includeEnum = m_IncludeList->GetEnumerator();
					while(includeEnum->MoveNext())
					{
						if(nameAttribute->Value->StartsWith(__try_cast<String*>(includeEnum->Current)))
						{
							found = true;
							break;
						}
					}
					if(!found)
					{
						bAllow=false;
					}
				}
				//Check Exlude List
				if(bAllow && m_ExcludeList->Count !=0)
				{
					bool found = false;
					IEnumerator* excludeEnum = m_ExcludeList->GetEnumerator();
					while(excludeEnum->MoveNext())
					{
						if(nameAttribute->Value->StartsWith(__try_cast<String*>(excludeEnum->Current)))
						{
							found = true;
							break;
						}
					}
					if(found)
					{
						bAllow=false;
					}
				}
				if(bAllow)
				{
					//get name attribute
					XmlAttribute *nameAttribute = builtNode->Attributes->get_ItemOf(S"name");
					String *name = 0;
					if (nameAttribute)
					{
						name = nameAttribute->get_Value();
					}

					// does this pack have an rpf tag?
					String *rpfValue = FindTag(builtNode);

					// is the tag valid?
					if (rpfValue)
					{
						if (!rpfValue->Equals("0") || !rpfValue->Equals(""))
						{
							isPackRPF = true;
						}
					}



					if(name)
					{
						if(isPackRPF)
						{
							System::Diagnostics::Debug::WriteLine(String::Concat("*** Adding RPF XML for ",name));
							// add this to the xml document
							XmlNode *rpfNode = rpfXml->CreateElement(S"Rpf");
							//name
							XmlNode *nodeName = rpfXml->CreateElement(S"Name");
							nodeName->InnerText = name->ToLower();
							rpfNode->AppendChild(nodeName);
							//type
							XmlNode *nodeType = rpfXml->CreateElement(S"Type");
							nodeType->InnerText = "rpf";
							rpfNode->AppendChild(nodeType);
							//alias
							XmlNode *nodeAlias = rpfXml->CreateElement(S"Alias");
							nodeAlias->InnerText = name->ToLower();
							rpfNode->AppendChild(nodeAlias);
							//add rpf node to root element
							rpfXml->DocumentElement->AppendChild(rpfNode);
						}
					}

				}
			}
			builtNode = builtNode->NextSibling;
		}

		String *fullRpfXmlPath = String::Concat(m_AssetBuildDataPath,g_ConfigPath,m_OutputPath);
		m_AssetManager->SimpleCheckOut(fullRpfXmlPath, "RPF builder");

		XmlTextWriter *writer = new XmlTextWriter(fullRpfXmlPath,System::Text::Encoding::ASCII);
		writer->Formatting = Formatting::Indented;
		rpfXml->WriteContentTo(writer);
		writer->Close();

		m_AssetManager->CheckInOrImport(0, fullRpfXmlPath, "RPF builder");

	}

	void audRPFBuilder::PrepareAssets()
	{
		String *fullWaveRootPath = String::Concat(m_LocalBuildDataPath, g_WavePaksPath);
		for (int i=0; i<m_PackListRPF->Count; ++i)
		{
			String *packName = __try_cast<String*>(m_PackListRPF->get_Item(i));

			// need to build a path to the rpf file
			String *fullPackPath = String::Concat(fullWaveRootPath, packName, ".rpf");
			m_AssetManager->SimpleCheckOut(fullPackPath, "RPF build");
		}
	}

	void audRPFBuilder::GenerateRPFs()
	{
		String *fullWaveRootPath = String::Concat(m_LocalBuildDataPath, g_WavePaksPath);
		for (int i=0; i<m_PackListRPF->Count; ++i)
		{
			String *packName = __try_cast<String*>(m_PackListRPF->get_Item(i));

			
				char *fullWaveRootPtr = 0;
				char *packNamePtr = 0;
				IntPtr fullWaveRootIntPointer = Marshal::StringToHGlobalAnsi(fullWaveRootPath);
				if(fullWaveRootIntPointer != 0)
				{
					fullWaveRootPtr = (char *)fullWaveRootIntPointer.ToPointer();
				}
				IntPtr packNameIntPointer = Marshal::StringToHGlobalAnsi(packName);
				if (packNameIntPointer != 0)
				{
					packNamePtr = (char *)packNameIntPointer.ToPointer();
				}

				if (packNamePtr && fullWaveRootPtr)
				{
					String *status = String::Concat(S"Building RPF for: ", packName);
					m_BuildClient->ReportProgress(-1, -1, status, true);

					try{
						m_AudRpfBuilder->Build(fullWaveRootPtr, packNamePtr, fullWaveRootPtr, m_EnableEncryption);
					}
					catch(Exception *)
					{
						throw(new audBuildException(String::Concat("Error building rpf for",__try_cast<String*>(m_PackListRPF->get_Item(i)))));
					}
				}

				Marshal::FreeHGlobal(fullWaveRootIntPointer);
				Marshal::FreeHGlobal(packNameIntPointer);
			}
		
	}

	void audRPFBuilder::CommitAssets()
	{
		String *fullWaveRootPath = String::Concat(m_LocalBuildDataPath, g_WavePaksPath);
		for (int i=0; i<m_PackListRPF->Count; ++i)
		{
			String *packName = __try_cast<String*>(m_PackListRPF->get_Item(i));

			// need to build a path to the rpf file
			String *path = m_AssetManager->GetWorkingPath(fullWaveRootPath);
			String *fullPackPath = String::Concat(path, packName, ".rpf");	
	
			if(!m_AssetManager->CheckInOrImport(0, fullPackPath->ToLower(), "RPF build"))
			{
				throw new audBuildException(String::Concat(S"Failed to check in ",fullPackPath));
			}
		}
	}

	void audRPFBuilder::ReleaseAssets()
	{
		if(m_AssetManager->GetAssetManagementType() == audAssetManagement::ASSET_MANAGER_PERFORCE)
		{
			//Don't attempt to undo the checkout of individual assets here as the build thread clean-up code
			//should revert and delete the changelist.
			return;
		}

		String *fullWaveRootPath = String::Concat(m_LocalBuildDataPath, g_WavePaksPath);
		for (int i=0; i<m_PackListRPF->Count; ++i)
		{
			String *packName = __try_cast<String*>(m_PackListRPF->get_Item(i));

			// need to build a path to the rpf file
			String *fullPackPath = String::Concat(fullWaveRootPath, packName, ".rpf");
			m_AssetManager->SimpleUndoCheckOut(fullPackPath);
		}
	}

	void audRPFBuilder::Shutdown()
	{
		delete m_AudRpfBuilder;
		m_AudRpfBuilder = 0;
	}

	bool audRPFBuilder::FindBankWaveOperation(XmlNode *bank)
	{
		bool retval = false;

		XmlNode *bankNode = bank;
		while (bankNode && !retval)
		{
			if (bankNode->Name->Equals(S"Bank"))
			{
				// check operation on this bank
				retval = FindOperation(bankNode, true);

				if (!retval)
				{
					XmlNode *waveNode = bankNode->FirstChild;
					while (waveNode && !retval)
					{
						if (waveNode->Name->Equals(S"Wave"))
						{
							// check operation on this wave
							retval = FindOperation(waveNode, true);
						}

						waveNode = waveNode->NextSibling;
					}
				}
			}

			bankNode = bankNode->NextSibling;
		}

		return retval;
	}



	bool audRPFBuilder::ShouldBuild(XmlNode *parentNode)
	{

		XmlNode *child = parentNode->FirstChild;
		//check if any children are tags before going deeper
		while(child){
			if(child->Name->Equals(S"Tag"))
			{
				XmlAttribute *nameAttribute = child->Attributes->get_ItemOf(S"name");
				if (nameAttribute)
				{
					if(nameAttribute->Value->Equals(S"deferredBuild"))
					{
						return false;
					}
				}
			}
			child=child->NextSibling;
		}

		return true;
	}



	String * audRPFBuilder::FindTag(XmlNode *parentNode)
	{
		String *buildValue = 0;

		XmlNode *childNode = parentNode->FirstChild;
		while (childNode)
		{
			if (childNode->Name->Equals(S"Tag"))
			{
				XmlAttribute *nameAttribute = childNode->Attributes->get_ItemOf(S"name");
				if (nameAttribute)
				{
					if (nameAttribute->Value->Equals(S"buildRPF"))
					{
						XmlAttribute *valAttribute = childNode->Attributes->get_ItemOf(S"value");
						if (valAttribute)
						{
							buildValue = valAttribute->Value;
							break;
						}

					}
				}
			}

			childNode = childNode->NextSibling;
		}

		if(buildValue==0)
		{
			for(int i =0; i<parentNode->ChildNodes->Count; i++)
			{
				if(!parentNode->ChildNodes->Item(i)->Name->Equals(S"Wave"))
				{
					buildValue = FindTag(parentNode->ChildNodes->Item(i));
					if(buildValue!=0)
					{
						break;
					}
				}
			}
		}

		return buildValue;
	}


	bool audRPFBuilder::FindOperation(XmlNode *parentNode, bool checkRemove)
	{
		XmlAttribute *opAttribute = parentNode->Attributes->get_ItemOf(S"operation");
		String *operation = 0;
		if (opAttribute)
		{
			operation = opAttribute->get_Value();
			if (operation)
			{
				if (operation->Equals(S"add"))
				{
					return true;
				}
				if (operation->Equals(S"modify"))
				{
					return true;
				}
				if (checkRemove && operation->Equals(S"remove"))
				{
					return true;
				}
			}
		}

		return false;
	}

};

