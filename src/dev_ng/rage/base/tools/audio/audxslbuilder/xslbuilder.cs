using System;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;

using audBuildCommon;
using audAssetManagement;
using rage;
using System.Collections.Generic;

namespace audXslBuild
{
	/// <summary>
	/// Summary description for xslbuilder.
	/// </summary>
	public class audXslBuilder : audPostBuilderBase
	{
		public audXslBuilder()
		{
		}

		public override void Build(audAssetManagement.audAssetManager assetMgr, rage.audProjectSettings projectSettings,
			string buildPlatform, audBuildClient buildClient, XmlDocument builtWavesXml, XmlDocument pendingWavesXml, bool shouldBuildLocally,
            audBuildComponent buildComponent, bool isDeferredBuild)
		{
            List<string> xmlPaths = buildComponent.m_XmlPaths;
			string xslPath = buildComponent.m_InputPath;
			string xslFilename = xslPath.Substring(xslPath.LastIndexOf('\\') + 1);
			string outputAssetPath = buildComponent.m_OutputPath;

			buildClient.ReportProgress(-1, -1, "Starting XSL Build - " + xslFilename, true);

            XslCompiledTransform xslt = new XslCompiledTransform();
            //Load the stylesheet.
            XsltSettings settings = new XsltSettings();
            settings.EnableScript = true;
            settings.EnableDocumentFunction = true;

            xslt.Load(xslPath,settings,null);

            XmlDocument doc = new XmlDocument();

            if(xmlPaths.Count == 0 || xmlPaths[0].ToUpper().EndsWith("BUILTWAVES.XML"))
            {
                doc = builtWavesXml;
            }
            else if (xmlPaths[0].ToUpper().EndsWith("PENDINGWAVES.XML"))
            {
                doc = pendingWavesXml;
            }
            else
            {
                try
                {
                    foreach (string path in xmlPaths)
                    {
                        if (!assetMgr.GetLatest(path))
                        {
                            throw new Exception("Failed to get latest on " + xmlPaths);
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    throw new audBuildException(e.ToString());
                }

                doc.Load(xmlPaths[0]);
            }

            //Create a new XPathNavigator for the XML data to be transformed.
            XPathNavigator builtWavesNav = doc.CreateNavigator();

            if (outputAssetPath != null)
            {
                if (assetMgr.ExistsAsAsset(outputAssetPath))
                {
                    if (!assetMgr.SimpleCheckOut(outputAssetPath, "Wave Build"))
                    {
                        throw new audBuildException("Failed to check-out output file: " + outputAssetPath);
                    }
                }

                string outputLocalPath = assetMgr.GetWorkingPath(outputAssetPath);
                if (outputLocalPath == null)
                {
                    throw new audBuildException("Failed to determine local working path for XSL output - Check project settings");
                }

                //Transform the data and send the output to the specified file.

                //Ensure that output folder exists locally.
                int folderIndex = outputLocalPath.LastIndexOf('\\');
                if(folderIndex < 0)
                {
                    folderIndex = outputLocalPath.LastIndexOf('/');
                }

                Directory.CreateDirectory(outputLocalPath.Substring(0, folderIndex));

                try
                {
                    XmlWriter outputXmlWriter = new XmlTextWriter(outputLocalPath, null);
                    xslt.Transform(builtWavesNav, outputXmlWriter);
                    outputXmlWriter.Close();
                }
                catch (Exception e)
                {
                    throw new audBuildException(e.ToString());
                }

                assetMgr.CheckInOrImport(outputLocalPath, outputAssetPath, "Wave Build");
            }
            else
            {
                //Transform the data back into the in-memory built waves XML.
                MemoryStream memStream = new MemoryStream();
                xslt.Transform(builtWavesNav, null, memStream);
                memStream.Seek(0, SeekOrigin.Begin);
                builtWavesXml.Load(memStream);
            }

			buildClient.ReportProgress(-1, -1, "Finished XSL Build", true);
		}
	}
}
