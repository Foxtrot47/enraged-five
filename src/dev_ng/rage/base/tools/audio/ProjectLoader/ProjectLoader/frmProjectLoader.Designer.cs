namespace audProjectLoader
{
    partial class frmProjectLoader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_ListBox = new System.Windows.Forms.ListBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_ListBox
            // 
            this.m_ListBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_ListBox.FormattingEnabled = true;
            this.m_ListBox.Location = new System.Drawing.Point(0, 0);
            this.m_ListBox.Name = "m_ListBox";
            this.m_ListBox.Size = new System.Drawing.Size(528, 225);
            this.m_ListBox.TabIndex = 0;
            // 
            // btnLoad
            // 
            this.btnLoad.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnLoad.Location = new System.Drawing.Point(226, 231);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 1;
            this.btnLoad.Text = "Load Project";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_OnClick);
            // 
            // frmProjectLoader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 266);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.m_ListBox);
            this.Name = "frmProjectLoader";
            this.Text = "Open Project...";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox m_ListBox;
        private System.Windows.Forms.Button btnLoad;
    }
}