using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using rage;
using System.Text.RegularExpressions;

namespace audProjectLoader
{
	/// <summary>
	/// Summary description for frmPreferences.
	/// </summary>
	public class frmProjectLoader: System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox m_AssetManagerType;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox m_ServerName;
		private System.Windows.Forms.TextBox m_AssetProject;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox m_ProjectSettings;
		private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox m_Password;
		private System.Windows.Forms.TextBox m_UserName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox m_MetadataCompiler;
        private System.Windows.Forms.Label label9;
        private TextBox m_DepotRoot;
        private Label lblDepotPath;
        private ComboBox m_Project;
        private Label label8;
        private Button btnNew;

        private audProjectEntry m_CurrentProject;
        private audProjectList m_ProjectList;
        private TextBox m_WaveEditorPath;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

        public audProjectEntry Project
        {
            get { return m_CurrentProject; }
        }
        
		public frmProjectLoader(string path)
		{
			InitializeComponent();
            m_ProjectList = new audProjectList(path);

            if (m_ProjectList.Projects.Count == 0)
            {
                EnableInput(false);
            }
            else
            {
                m_Project.Items.AddRange(m_ProjectList.Projects.ToArray());
                m_Project.SelectedItem = m_Project.Items[0];
                LoadProject((audProjectEntry)m_Project.Items[0]);
            }

		}

        private void EnableInput(bool enable)
        {
            m_Project.Enabled = m_UserName.Enabled = m_AssetManagerType.Enabled = m_AssetProject.Enabled
            = m_DepotRoot.Enabled = m_MetadataCompiler.Enabled = m_Password.Enabled
            = m_Project.Enabled = m_ProjectSettings.Enabled = m_ServerName.Enabled 
            = m_WaveEditorPath.Enabled = btnOk.Enabled = enable;
        }

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProjectLoader));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_DepotRoot = new System.Windows.Forms.TextBox();
            this.lblDepotPath = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_Password = new System.Windows.Forms.TextBox();
            this.m_UserName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_ProjectSettings = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.m_AssetProject = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_ServerName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_AssetManagerType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_MetadataCompiler = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.m_Project = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.m_WaveEditorPath = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.m_DepotRoot);
            this.groupBox1.Controls.Add(this.lblDepotPath);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.m_Password);
            this.groupBox1.Controls.Add(this.m_UserName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.m_ProjectSettings);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.m_AssetProject);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.m_ServerName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.m_AssetManagerType);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(8, 60);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 210);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Asset Manager Settings";
            // 
            // m_DepotRoot
            // 
            this.m_DepotRoot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_DepotRoot.Location = new System.Drawing.Point(112, 182);
            this.m_DepotRoot.Name = "m_DepotRoot";
            this.m_DepotRoot.Size = new System.Drawing.Size(248, 20);
            this.m_DepotRoot.TabIndex = 13;
            this.m_DepotRoot.Validating += new System.ComponentModel.CancelEventHandler(this.m_DepotRoot_Validating);
            // 
            // lblDepotPath
            // 
            this.lblDepotPath.Location = new System.Drawing.Point(8, 186);
            this.lblDepotPath.Name = "lblDepotPath";
            this.lblDepotPath.Size = new System.Drawing.Size(104, 16);
            this.lblDepotPath.TabIndex = 12;
            this.lblDepotPath.Text = "Depot Root";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "Password";
            // 
            // m_Password
            // 
            this.m_Password.Font = new System.Drawing.Font("Wingdings", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.m_Password.Location = new System.Drawing.Point(112, 44);
            this.m_Password.Name = "m_Password";
            this.m_Password.PasswordChar = 'l';
            this.m_Password.Size = new System.Drawing.Size(248, 20);
            this.m_Password.TabIndex = 3;
            this.m_Password.Validating += new System.ComponentModel.CancelEventHandler(this.m_Password_Validating);
            // 
            // m_UserName
            // 
            this.m_UserName.Location = new System.Drawing.Point(112, 20);
            this.m_UserName.Name = "m_UserName";
            this.m_UserName.Size = new System.Drawing.Size(248, 20);
            this.m_UserName.TabIndex = 1;
            this.m_UserName.Validating += new System.ComponentModel.CancelEventHandler(this.m_UserName_Validating);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "User name";
            // 
            // m_ProjectSettings
            // 
            this.m_ProjectSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ProjectSettings.Location = new System.Drawing.Point(112, 156);
            this.m_ProjectSettings.Name = "m_ProjectSettings";
            this.m_ProjectSettings.Size = new System.Drawing.Size(248, 20);
            this.m_ProjectSettings.TabIndex = 11;
            this.m_ProjectSettings.Validating += new System.ComponentModel.CancelEventHandler(this.m_ProjectSettings_Validating);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "Project Settings";
            // 
            // m_AssetProject
            // 
            this.m_AssetProject.Location = new System.Drawing.Point(112, 132);
            this.m_AssetProject.Name = "m_AssetProject";
            this.m_AssetProject.Size = new System.Drawing.Size(248, 20);
            this.m_AssetProject.TabIndex = 9;
            this.m_AssetProject.Validating += new System.ComponentModel.CancelEventHandler(this.m_AssetProject_Validate);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Project/Workspace";
            // 
            // m_ServerName
            // 
            this.m_ServerName.Location = new System.Drawing.Point(112, 108);
            this.m_ServerName.Name = "m_ServerName";
            this.m_ServerName.Size = new System.Drawing.Size(248, 20);
            this.m_ServerName.TabIndex = 7;
            this.m_ServerName.Validating += new System.ComponentModel.CancelEventHandler(this.m_ServerName_Validating);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Server";
            // 
            // m_AssetManagerType
            // 
            this.m_AssetManagerType.Items.AddRange(new object[] {
            "Perforce",
            "Local"});
            this.m_AssetManagerType.Location = new System.Drawing.Point(112, 83);
            this.m_AssetManagerType.Name = "m_AssetManagerType";
            this.m_AssetManagerType.Size = new System.Drawing.Size(248, 21);
            this.m_AssetManagerType.TabIndex = 5;
            this.m_AssetManagerType.Text = "Perforce";
            this.m_AssetManagerType.ValueMemberChanged += new System.EventHandler(this.m_AssetManagerType_Validating);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Asset Manager";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(301, 377);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.m_MetadataCompiler);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.m_WaveEditorPath);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(8, 288);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 83);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "RAVE Settings";
            // 
            // m_MetadataCompiler
            // 
            this.m_MetadataCompiler.Location = new System.Drawing.Point(112, 52);
            this.m_MetadataCompiler.Name = "m_MetadataCompiler";
            this.m_MetadataCompiler.Size = new System.Drawing.Size(248, 20);
            this.m_MetadataCompiler.TabIndex = 3;
            this.m_MetadataCompiler.Validating += new System.ComponentModel.CancelEventHandler(this.m_MetadataCompiler_Validating);
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(8, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 16);
            this.label9.TabIndex = 2;
            this.label9.Text = "Metadata Compiler";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Wave Editor";
            // 
            // m_Project
            // 
            this.m_Project.FormattingEnabled = true;
            this.m_Project.Location = new System.Drawing.Point(62, 12);
            this.m_Project.Name = "m_Project";
            this.m_Project.Size = new System.Drawing.Size(257, 21);
            this.m_Project.TabIndex = 4;
            this.m_Project.Validating += new System.ComponentModel.CancelEventHandler(this.m_ProjectValidating);
            this.m_Project.SelectedIndexChanged += new System.EventHandler(this.m_Project_Changed);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Project";
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(325, 10);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(51, 23);
            this.btnNew.TabIndex = 6;
            this.btnNew.Text = "New..";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // m_WaveEditorPath
            // 
            this.m_WaveEditorPath.Location = new System.Drawing.Point(112, 19);
            this.m_WaveEditorPath.Name = "m_WaveEditorPath";
            this.m_WaveEditorPath.Size = new System.Drawing.Size(248, 20);
            this.m_WaveEditorPath.TabIndex = 1;
            this.m_WaveEditorPath.Validating += new System.ComponentModel.CancelEventHandler(this.m_WaveEditor_Validating);
            // 
            // frmProjectLoader
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(386, 412);
            this.ControlBox = false;
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.m_Project);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProjectLoader";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Project Preferences";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void btnOk_Click(object sender, System.EventArgs e)
		{
            if(this.ValidateChildren())
            {
                m_ProjectList.Save();
                Close();
            }
		}

        private void btnNew_Click(object sender, EventArgs e)
        {
            EnableInput(true);
            LoadProject(m_ProjectList.CreateProject());
        }

        private void m_ProjectValidating(object sender, CancelEventArgs e)
        {
            if (m_Project.Text == "")
            {
                MessageBox.Show("You must provide a name for the project");
                e.Cancel = true;
            }
            else
            {
                m_CurrentProject.Name = m_Project.Text;
            }
        }

        private void LoadProject(audProjectEntry projectEntry)
        {
            m_CurrentProject = projectEntry;            
            m_AssetProject.Text = projectEntry.AssetProject;
            m_DepotRoot.Text = projectEntry.DepotRoot;
            m_MetadataCompiler.Text = projectEntry.MetadatCompiler;
            m_Project.Text = projectEntry.AssetProject;
            m_ProjectSettings.Text = projectEntry.ProjectSettings;
            m_ServerName.Text = projectEntry.AssetServer;
            m_UserName.Text = projectEntry.UserName;
            m_WaveEditorPath.Text = projectEntry.WaveEditor;
            switch (projectEntry.AssetManager)
            {
                case "3":
                    m_AssetManagerType.Text = "Local";
                    break;
                default:
                    m_AssetManagerType.Text = "Perforce";
                    break;
            }
        }

        private void m_ServerName_Validating(object sender, CancelEventArgs e)
        {
            if (m_ServerName.Text == "" || !Regex.Match(m_ServerName.Text, "[a-zA-Z0-9]:[0-9]").Success)
            {
                 MessageBox.Show("You must provide a valid asset server e.g RSGEDIP4D1:1666");
                 e.Cancel = true;
            }
            else
            {
                m_CurrentProject.AssetServer = m_ServerName.Text;
            }
        }

        private void m_AssetProject_Validate(object sender, CancelEventArgs e)
        {
            if (m_AssetProject.Text == "")
            {
                MessageBox.Show("You must provide a name for the project");
                e.Cancel = true;
            }
            else
            {
                m_CurrentProject.AssetProject = m_AssetProject.Text;
            }
        }

        private void m_Password_Validating(object sender, CancelEventArgs e)
        {
            m_CurrentProject.Password = m_Password.Text;
        }

        private void m_ProjectSettings_Validating(object sender, CancelEventArgs e)
        {
            if (m_ProjectSettings.Text == "" || !m_ProjectSettings.Text.ToUpper().EndsWith(".XML"))
            {
                MessageBox.Show("Please provide a valid project settings path");
                e.Cancel = true;
            }

            m_CurrentProject.ProjectSettings = m_ProjectSettings.Text;
        }

        private void m_DepotRoot_Validating(object sender, CancelEventArgs e)
        {
            if (m_DepotRoot.Text == "")
            {
                MessageBox.Show("Please provide a valid asset manager depot root");
                e.Cancel = true;
            }

            m_CurrentProject.DepotRoot = m_DepotRoot.Text;
        }

        private void m_WaveEditor_Validating(object sender, CancelEventArgs e)
        {
            if (m_WaveEditorPath.Text == "" || !m_WaveEditorPath.Text.ToUpper().EndsWith(".EXE"))
            {
                MessageBox.Show("Please provide a valid wave editor");
                e.Cancel = true;
            }

            m_CurrentProject.WaveEditor = m_WaveEditorPath.Text;
        }

        private void m_MetadataCompiler_Validating(object sender, CancelEventArgs e)
        {
            if (m_MetadataCompiler.Text == "" || !m_MetadataCompiler.Text.ToUpper().EndsWith(".EXE"))
            {
                MessageBox.Show("Please provide a valid path to the metadatacompiler.exe");
                e.Cancel = true;
            }

            m_CurrentProject.MetadatCompiler = m_MetadataCompiler.Text;
        }

        private void m_Project_Changed(object sender, EventArgs e)
        {
            LoadProject((audProjectEntry)m_Project.SelectedItem);
        }

        private void m_UserName_Validating(object sender, CancelEventArgs e)
        {
            if (m_UserName.Text == "")
            {
                MessageBox.Show("Please provide a valid user name");
                e.Cancel = true;
            }

            m_CurrentProject.UserName = m_UserName.Text;

        }

        private void m_AssetManagerType_Validating(object sender, EventArgs e)
        {
            switch (m_AssetManagerType.Text)
            {
                case "Local":
                    m_CurrentProject.AssetManager = "3";
                    break;
                default:
                    m_AssetManagerType.Text = "2";
                    break;
            }
        }

	}
}
