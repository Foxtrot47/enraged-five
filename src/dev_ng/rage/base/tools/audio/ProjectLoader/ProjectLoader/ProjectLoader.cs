using System;
using System.Collections.Generic;
using System.Text;
using rage;
using Microsoft.Win32;
using System.Windows.Forms;
using audAssetManagement;

namespace audProjectLoader
{
    public class ProjectLoader
    {
        private string m_AssetUser;
        private string m_AssetPassword;
        private string m_AssetProject;
        private string m_AssetServer;
        private string m_ProjectListPath;
        private string m_DepotRoot;
        private int m_AssetManagerType;

        private audProjectEntry m_ProjectEntry;
        private audProjectSettings m_ProjectSettings;
        private audAssetManager m_AssetManager;


        public ProjectLoader()
        {
            m_ProjectSettings = null;
            m_AssetManager = null;
        }


        public bool Run()
        {
            try
            {
                LoadProjectList();
                LoadAssetManager();
                LoadProjectSettings();
                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error Loading Project", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void LoadProjectList()
        {
            frmProjectLoader projectLoader = new frmProjectLoader(String.Concat(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),@"\Rave\ProjectList.xml"));
            projectLoader.ShowDialog();
            m_ProjectEntry = projectLoader.Project;

            m_AssetUser = m_ProjectEntry.UserName;
            m_AssetPassword = m_ProjectEntry.Password;
            m_AssetProject = m_ProjectEntry.AssetProject;
            m_AssetServer = m_ProjectEntry.AssetServer;
            m_AssetManagerType = 2;
        }

        private void LoadProjectSettings()
        {
            if (m_ProjectEntry == null)
            {
                throw new Exception("Failed to find project");
            }
            else
            {
                m_ProjectSettings = new audProjectSettings(m_AssetManager.GetWorkingPath(m_ProjectEntry.ProjectSettings));
                if (m_ProjectSettings == null)
                {
                    throw new Exception("Failed to Load Project Settings");
                }
            }
        }

        private void LoadAssetManager()
        {
            m_AssetManager = audAssetManager.CreateInstance(m_AssetManagerType);
            m_AssetManager.Init(m_AssetServer, m_AssetProject, m_AssetUser, m_AssetPassword, false);
        }

        public audProjectSettings ProjectSettings
        {
            get{return m_ProjectSettings;}
        }

        public audAssetManager AssetManager
        {
            get { return m_AssetManager; }
        }

        public audProjectEntry ProjectEntry
        {
            get { return m_ProjectEntry; }
        }
    }
}
