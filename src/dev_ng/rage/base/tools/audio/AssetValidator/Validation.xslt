<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/2001/XMLSchema">
  <xsl:output method="xml" omit-xml-declaration="no" indent="yes"/>

  <xsl:template match="/TypeDefinitions">
    <xsl:element name="xs:schema" namespace="http://www.w3.org/2001/XMLSchema">
      <!--root tag Objects-->
      <xsl:element name="xs:element" namespace="http://www.w3.org/2001/XMLSchema">
        <xsl:attribute name="name">Objects</xsl:attribute>
        <xsl:element name="xs:complexType" namespace="http://www.w3.org/2001/XMLSchema">
          <xsl:element name="xs:choice" namespace="http://www.w3.org/2001/XMLSchema">
            <xsl:attribute name="maxOccurs">unbounded</xsl:attribute>
            <!--hard coded template object-->
            <xsl:element name="xs:element" namespace="http://www.w3.org/2001/XMLSchema">
              <xsl:attribute name="name">TemplateInstance</xsl:attribute>
              <xsl:element name="xs:complexType" namespace="http://www.w3.org/2001/XMLSchema">
                <xsl:element name="xs:sequence" namespace="http://www.w3.org/2001/XMLSchema">
                  <xsl:element name="xs:any" namespace="http://www.w3.org/2001/XMLSchema">
                    <xsl:attribute name="minOccurs">0</xsl:attribute>
                  </xsl:element>
                </xsl:element>
                <xsl:element name="xs:attribute" namespace="http://www.w3.org/2001/XMLSchema">
                  <xsl:attribute name="name">name</xsl:attribute>
                </xsl:element>
                <xsl:element name="xs:attribute" namespace="http://www.w3.org/2001/XMLSchema">
                  <xsl:attribute name="name">template</xsl:attribute>
                </xsl:element>
              </xsl:element>
            </xsl:element>
             <!--end of template object-->           
            <xsl:for-each select="child::TypeDefinition">
              <!--Ojbect Type -->
              <xsl:element name="xs:element" namespace="http://www.w3.org/2001/XMLSchema">
                <xsl:attribute name="name">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:element name="xs:complexType" namespace="http://www.w3.org/2001/XMLSchema">
                  <xsl:element name="xs:all" namespace="http://www.w3.org/2001/XMLSchema">
                    <xsl:if test="@inheritsFrom">
                      <xsl:variable name="inheritsFrom" select="@inheritsFrom"/>
                      <xsl:call-template name="FieldSchema">
                        <xsl:with-param name="node" select="//TypeDefinition[@name = $inheritsFrom]"/>
                      </xsl:call-template>
                    </xsl:if>
                    <xsl:call-template name="FieldSchema">
                      <xsl:with-param name="node" select="current()"/>
                    </xsl:call-template>
                    <!--end of Object Type-->
                  </xsl:element>
                  <xsl:element name="xs:attribute" namespace="http://www.w3.org/2001/XMLSchema">
                    <xsl:attribute name="name">name</xsl:attribute>
                    <xsl:attribute name="type">xs:string</xsl:attribute>
                  </xsl:element>
                  <xsl:element name="xs:attribute" namespace="http://www.w3.org/2001/XMLSchema">
                    <xsl:attribute name="name">folder</xsl:attribute>
                    <xsl:attribute name="type">xs:string</xsl:attribute>
                  </xsl:element>
                </xsl:element>
              </xsl:element>
            </xsl:for-each>
            <!--end of Objects-->
          </xsl:element>       
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template name="FieldSchema">
    <xsl:param name="node"/>
    <xsl:for-each select="$node[1]/*">
      <xsl:choose>
        <xsl:when test="not(self::CompositeField)">
          <xsl:element name="xs:element" namespace="http://www.w3.org/2001/XMLSchema">
            <xsl:attribute name="name">
              <xsl:value-of select="@name"/>
            </xsl:attribute>
            <xsl:attribute name="minOccurs">0</xsl:attribute>
            <xsl:attribute name="maxOccurs">1</xsl:attribute>
          </xsl:element>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="CompositeSchema">
            <xsl:with-param name="node" select="current()"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name ="CompositeSchema">
    <xsl:param name="node"/>
    <xsl:element name="xs:element"  namespace="http://www.w3.org/2001/XMLSchema">
      <xsl:attribute name="name">
        <xsl:value-of select="$node[1]/@name"/>
      </xsl:attribute>     
      <xsl:attribute name="minOccurs">0</xsl:attribute>
      <xsl:attribute name="maxOccurs">1</xsl:attribute>
      <xsl:element name="xs:complexType" namespace="http://www.w3.org/2001/XMLSchema">
        <xsl:choose>
          <!--composite is non-repeating-->
          <xsl:when test="not(@maxOccurs)">
            <xsl:element name="xs:all" namespace="http://www.w3.org/2001/XMLSchema">
              <xsl:call-template name="FieldSchema">
                <xsl:with-param name="node" select="$node[1]"/>
              </xsl:call-template>
            </xsl:element>
          </xsl:when>
          <xsl:otherwise>
            <!--composite is repeating-->
            <!--allow for an additional parent node-->
            <xsl:element name="xsl:sequence" namespace="http://www.w3.org/2001/XMLSchema">
              <xsl:element name="xsl:element" namespace="http://www.w3.org/2001/XMLSchema">
                <xsl:attribute name="name">
                  <xsl:value-of select="$node[1]/@name"/>
                </xsl:attribute>
                <xsl:attribute name="minOccurs">0</xsl:attribute>
                <xsl:attribute name="maxOccurs">
                  <xsl:value-of select="$node[1]/@maxOccurs"/>
                </xsl:attribute>
                <xsl:element name="xsl:complexType" namespace="http://www.w3.org/2001/XMLSchema">
                  <xsl:element name="xsl:all" namespace="http://www.w3.org/2001/XMLSchema">
                    <xsl:call-template name="FieldSchema">
                      <xsl:with-param name="node" select="$node[1]"/>
                    </xsl:call-template>
                  </xsl:element>
                </xsl:element>
              </xsl:element>
            </xsl:element>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="$node[1]/@allowOverrideControl">
          <xsl:element name="xs:attribute" namespace="http://www.w3.org/2001/XMLSchema">
            <xsl:attribute name="name">overrideParent</xsl:attribute>
            <xsl:attribute name="use">optional</xsl:attribute>
          </xsl:element>
        </xsl:if>
      </xsl:element>      
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
