using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ProjectLoader2;
using audAssetManagement2;
using rage;
using Rave.Plugins;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Xsl;
using System.Reflection;


namespace AssetValidator
{
    public partial class frmMain : Form
    {
        private Dictionary<string, List<string>> m_Banks;
        private StringBuilder m_Output;
        private int m_NumErrors;

        public frmMain(string[] args)
        {
            InitializeComponent();
            m_Output = new StringBuilder();
            m_NumErrors = 0;

            try
            {
                string path = "ProjectList.xml";
                if (args.Length > 0)
                {
                    path = args[0];
                }
                frmProjectLoader pl = new frmProjectLoader(path);
                pl.ShowDialog();

                Rave.Plugins.Configuration.InitProject(pl.Project);
                AssetManager.InitClass(Rave.Plugins.Configuration.AssetManagementType);
                Rave.Plugins.Configuration.LoadProjectSettings();
                LoadTypeDefinitions();
                LoadBanks();

                pl.Dispose();
             
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            
        }

        private void ValidateBanks(BackgroundWorker worker)
        {
            foreach (KeyValuePair<string, List<string>> kvp in m_Banks)
            {
                foreach (string file in kvp.Value)
                {
                    worker.ReportProgress(0, "Processing: " + file);
                    Validate(kvp.Key, file);
                }
            }
        }

        private string m_CurrentObjectName;
        int m_ErrorsCount;
        StringBuilder m_ErrorMessage;
        
        public bool Validate(string type, string fullPath)
        {
            XmlDocument m_Document = new XmlDocument();
            m_Document.Load(fullPath);

            string definitionsPath;
            string schemaPath = System.IO.Path.GetTempPath() + "schema.xsd";
            m_ErrorMessage = new StringBuilder();
            m_ErrorsCount = 0;

            XmlDocument temp = new XmlDocument();
            XmlElement root = temp.CreateElement("Objects");
            temp.AppendChild(root);

            ParseXml(type, root, m_Document.DocumentElement);

            //get type def paths
            if (TypeDefinitions.AllTypeDefinitions[type].FilePaths.Length == 1)
            {
                definitionsPath = TypeDefinitions.AllTypeDefinitions[type].FilePaths[0];
            }
            else
            {
                //need to concatenate the Type definitions so that we can run
                //the xsl to produce the schema
                XmlDocument doc = new XmlDocument();
                //create the common root
                XmlElement docRoot = doc.CreateElement("TypeDefinitions");
                doc.AppendChild(docRoot);
                //copy over all the Type definitions
                foreach (string filePath in TypeDefinitions.AllTypeDefinitions[type].FilePaths)
                {
                    XmlDocument typeDef = new XmlDocument();
                    typeDef.Load(filePath);
                    foreach (XmlNode node in typeDef.DocumentElement.ChildNodes)
                    {
                        docRoot.AppendChild(doc.ImportNode(node, true));
                    }
                }

                //write concatenated xml to a temporary file
                XmlWriterSettings ws = new XmlWriterSettings();
                ws.Indent = true;
                definitionsPath = System.IO.Path.GetTempPath() + "TypeDefinitions.xml";
                XmlTextWriter writer = new XmlTextWriter(definitionsPath, null);
                writer.Formatting = Formatting.Indented;
                doc.WriteContentTo(writer);
                writer.Flush();
                writer.Close();
            }
            try
            {
                //create schema via xsl
                XslCompiledTransform transform = new XslCompiledTransform();
                XmlTextReader xsltReader = new XmlTextReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("AssetValidator.Validation.xslt"));
                transform.Load(xsltReader);
                transform.Transform(definitionsPath, schemaPath);
                xsltReader.Close();
                //Validate the temp (corrected) xml. The temp xml includes a fake parent for repeating fields
                // this fake node allows the xml to comply with a schema (as you can have any number of children in any order)
                XmlSchemaSet schemaSet = new XmlSchemaSet();
                XmlTextReader schemaReader = new XmlTextReader(schemaPath);
                schemaSet.Add(null, schemaReader);
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ValidationType = ValidationType.Schema;
                settings.Schemas = schemaSet;
                settings.ValidationEventHandler += new ValidationEventHandler(vr_ValidationEventHandler);
                settings.ConformanceLevel = ConformanceLevel.Auto;

                foreach (XmlNode obj in temp.DocumentElement.ChildNodes)
                {
                    XmlDocument frag = new XmlDocument();
                    frag.AppendChild(frag.CreateElement("Objects"));
                    m_CurrentObjectName = obj.Attributes["name"].Value;
                    frag.DocumentElement.AppendChild(frag.ImportNode(obj,true));

                    string fragString = frag.OuterXml;

                    XmlParserContext context = new XmlParserContext(null, null, "", XmlSpace.None);
                    XmlTextReader textreader = new XmlTextReader(fragString, XmlNodeType.Element, context);
                    XmlReader reader = XmlReader.Create(textreader,settings);
                    while (reader.Read()) ;
                    reader.Close();
          
                }

                schemaReader.Close();
                settings.ValidationEventHandler -= new ValidationEventHandler(vr_ValidationEventHandler);

                // Raise exception, if XML validation fails
                if (m_ErrorsCount > 0)
                {
                    m_Output.AppendLine(fullPath);
                    m_Output.AppendLine(m_ErrorMessage.ToString());
                    m_Output.AppendLine();
                    m_NumErrors++;
                    return false;
                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine("No Errors");
                    return true;
                }


            }
            catch (Exception e)
            {
                m_Output.AppendLine(fullPath);
                m_Output.AppendLine(e.Message);
                m_Output.AppendLine();
                return false;
            }
        }

        private void ParseXml(string type, XmlElement root, XmlElement xmlElement)
        {
            foreach (XmlNode child in xmlElement.ChildNodes)
            {
                XmlNode typeNode = root.OwnerDocument.ImportNode(child, false);
                root.AppendChild(typeNode);
                TypeDefinition typeDef = TypeDefinitions.AllTypeDefinitions[type].FindTypeDefinition(child.Name);
                Dictionary<string, List<XmlNode>> repeatingFields = new Dictionary<string, List<XmlNode>>();
                List<XmlNode> nodesToDelete = new List<XmlNode>();
                if (typeDef != null)
                {
                    ParseFields(typeNode, child, typeDef.Fields);
                }
            }
        }

        private void ParseFields(XmlNode parentNew, XmlNode parentOriginal, FieldDefinition[] fields)
        {
            Dictionary<string, List<XmlNode>> repeatingFields = new Dictionary<string, List<XmlNode>>();
            List<XmlNode> nodesToDelete = new List<XmlNode>();

            foreach (XmlNode node in parentOriginal.ChildNodes)
            {
                //check if exists
                FieldDefinition fieldDef = null;
                foreach (FieldDefinition f in fields)
                {
                    if (f.Name == node.Name)
                    {
                        fieldDef = f;
                        break;
                    }
                }

                if (fieldDef != null)
                {
                    CompositeFieldDefinition compFieldDef = fieldDef as CompositeFieldDefinition;
                    if (compFieldDef != null)
                    {
                        XmlNode newCompField = parentNew.OwnerDocument.ImportNode(node, false);
                        if (compFieldDef.MaxOccurs == 1)
                        {
                            parentNew.AppendChild(newCompField);
                        }
                        else
                        {
                            if (!repeatingFields.ContainsKey(node.Name))
                            {
                                repeatingFields.Add(node.Name, new List<XmlNode>());
                            }
                            repeatingFields[node.Name].Add(newCompField);
                        }

                        ParseFields(newCompField, node, compFieldDef.Fields);
                    }
                    else
                    {
                        parentNew.AppendChild(parentNew.OwnerDocument.ImportNode(node, true));
                    }
                }
                else
                {
                    nodesToDelete.Add(node);
                }
            }
            //add repeating fiels under common parent
            foreach (KeyValuePair<string, List<XmlNode>> kvp in repeatingFields)
            {
                XmlElement repeatingRoot = parentNew.OwnerDocument.CreateElement(kvp.Key);
                foreach (XmlNode node in kvp.Value)
                {
                    repeatingRoot.AppendChild(node);
                }
                parentNew.AppendChild(repeatingRoot);
            }
            //strip out no longer used nodes
            foreach (XmlNode nodeToDelete in nodesToDelete)
            {
                parentOriginal.RemoveChild(nodeToDelete);
            }
        }

        public void vr_ValidationEventHandler(object sender,ValidationEventArgs args)
        {
            
            m_ErrorMessage.AppendLine(args.Message);
            m_ErrorMessage.Append("Error in object: ");
            m_ErrorMessage.AppendLine(m_CurrentObjectName);
            m_ErrorMessage.AppendLine("");
            m_ErrorsCount++;
        }

        private void LoadBanks()
        {            
            m_Banks = new Dictionary<string, List<string>>();
            foreach (audMetadataFile file in Rave.Plugins.Configuration.GetProjectSettings().GetMetadataSettings())
            {
                string type = file.Type.ToUpper();
                if (!m_Banks.ContainsKey(type))
                {
                    m_Banks.Add(type, new List<string>());
                }

                GetAllBanks(m_Banks[type], new DirectoryInfo(AssetManager.GetAssetManager().GetWorkingPath(Rave.Plugins.Configuration.ObjectXmlPath + @"\" + file.DataPath)));
            }
        }

        private void LoadTypeDefinitions()
        {

            foreach (PlatformSetting ps in Rave.Plugins.Configuration.GetPlatformSettings())
            {

                foreach (audMetadataType metadataType in Rave.Plugins.Configuration.GetProjectSettings().GetMetadataTypes())
                {
                    ArrayList al = new ArrayList();
                    foreach (audObjectDefinition objDef in metadataType.ObjectDefinitions)
                    {
                        al.Add(AssetManager.GetAssetManager().GetWorkingPath(Rave.Plugins.Configuration.ObjectXmlPath + objDef.DefinitionsFile));
                    }
                    if (TypeDefinitions.AllTypeDefinitions.ContainsKey(metadataType.Type))
                    {
                        TypeDefinitions.AllTypeDefinitions[metadataType.Type].Load((string[])al.ToArray(typeof(string)));
                    }
                    else
                    {
                        TypeDefinitions typedefs = new TypeDefinitions((string[])al.ToArray(typeof(string)), metadataType.SchemaPath);
                        TypeDefinitions.AllTypeDefinitions.Add(metadataType.Type, typedefs);
                    }

                }

            }

        }

        private static void GetAllBanks(List<string> banks, DirectoryInfo dir)
        {
            foreach (FileInfo file in dir.GetFiles())
            {
                banks.Add(file.FullName);
            }

            foreach (DirectoryInfo childDir in dir.GetDirectories())
            {
                GetAllBanks(banks, childDir);
            }
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            ValidateBanks(worker);
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            m_ValidationOutput.AppendText(e.UserState.ToString() + "\r\n");
        }

        private void worker_Run(object sender, RunWorkerCompletedEventArgs e)
        {
            m_Output.AppendLine("Done");
            m_ValidationOutput.AppendText(m_Output.ToString());
            btnGo.Enabled = true;
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            btnGo.Enabled = false;
            m_ValidationOutput.AppendText("Starting Validation...\r\n");
            backgroundWorker1.RunWorkerAsync();
        }

        private void worker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            m_ValidationOutput.AppendText("********** Errors *************\r\n");
            m_Output.Append("Total:");
            m_Output.AppendLine(m_NumErrors.ToString());
            m_ValidationOutput.AppendText(m_Output.ToString());
        }

        
    }
}