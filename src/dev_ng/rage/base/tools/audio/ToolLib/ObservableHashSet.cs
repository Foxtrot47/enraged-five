﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Threading;

namespace rage.ToolLib
{
    /// <summary>
    /// An observable hashset which can be used with WPF content
    /// </summary>
    /// <typeparam name="T">The type of elements in the hash set.</typeparam>    
    public sealed class ObservableHashSet<T> : ISet<T>, INotifyCollectionChanged, INotifyPropertyChanged, IDisposable
    {

        private HashSet<T> hashset;

        private readonly object locker = new object();

        public ObservableHashSet()
        {
            this.hashset = new HashSet<T>();
        }

        public ObservableHashSet(IEnumerable<T> collection)
        {
            this.hashset = new HashSet<T>(collection);
        }

        public bool Add(T item)
        {
            if (Monitor.TryEnter(locker, 100))
            {
                bool added = false;
                try
                {
                    added = this.hashset.Add(item);
                    if (added)
                    {
                        this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
                        this.OnPropertyChanged("Count");
                    }
                }
                finally
                {
                    Monitor.Exit(locker);
                }
                return added;
            }
            else
                return false;
        }

        public void ExceptWith(IEnumerable<T> other)
        {
            if (Monitor.TryEnter(locker, 100))
            {
                try
                {
                    OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, other));
                    OnPropertyChanged("Count");
                    this.hashset.ExceptWith(other);
                }
                finally
                {
                    Monitor.Exit(locker);
                }
            }
        }

        public void IntersectWith(IEnumerable<T> other)
        {
            if (Monitor.TryEnter(locker, 100))
            {
                try
                {
                    var removedItems = this.hashset.Where(x => !other.Contains(x)).ToList();
                    this.hashset.IntersectWith(other);

                    if (removedItems.Count > 0)
                    {
                        this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                        this.OnPropertyChanged("Count");
                    }
                }
                finally
                {
                    Monitor.Exit(locker);
                }
            }

        }

        public bool IsProperSubsetOf(IEnumerable<T> other)
        {
            return this.hashset.IsProperSubsetOf(other);
        }

        public bool IsProperSupersetOf(IEnumerable<T> other)
        {
            return this.hashset.IsProperSupersetOf(other);
        }

        public bool IsSubsetOf(IEnumerable<T> other)
        {
            return this.hashset.IsSubsetOf(other);
        }

        public bool IsSupersetOf(IEnumerable<T> other)
        {
            return this.hashset.IsSupersetOf(other);
        }

        public bool Overlaps(IEnumerable<T> other)
        {
            return this.hashset.Overlaps(other);
        }

        public bool SetEquals(IEnumerable<T> other)
        {
            return this.hashset.SetEquals(other);
        }

        public void SymmetricExceptWith(IEnumerable<T> other)
        {
            if (Monitor.TryEnter(locker, 100))
            {
                try
                {
                    OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, other));
                    OnPropertyChanged("Count");
                    this.hashset.SymmetricExceptWith(other);
                }
                finally
                {
                    Monitor.Exit(locker);
                }
            }
        }

        public void UnionWith(IEnumerable<T> other)
        {
            if (Monitor.TryEnter(locker, 100))
            {
                try
                {
                    OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, other));
                    OnPropertyChanged("Count");
                    this.hashset.UnionWith(other);
                }
                finally
                {
                    Monitor.Exit(locker);
                }
            }
        }

        void ICollection<T>.Add(T item)
        {
            this.Add(item);
        }

        public bool AddRange(IEnumerable<T> items)
        {
            if (Monitor.TryEnter(locker, 100))
            {
                bool allAdded = false;
                try
                {


                    allAdded = true;
                    foreach (T item in items)
                    {
                        if (!Add(item))
                        {
                            allAdded = false;
                        }
                        else
                        {
                            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, items));
                        }
                    }


                    OnPropertyChanged("Count");
                }
                finally
                {
                    Monitor.Exit(locker);
                }

                return allAdded;
            }
            else
            {
                return false;
            }
        }

        public void Clear()
        {
            if (Monitor.TryEnter(locker, 100))
            {
                try
                {
                    if (this.hashset.Count > 0)
                    {
                        List<T> removedItems = this.hashset.ToList<T>();
                        this.hashset.Clear();

                        this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, new List<T>(), removedItems));
                        this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset)); 

                        this.OnPropertyChanged("Count");
                    }
                }
                finally
                {
                    Monitor.Exit(locker);
                }
            }
        }


        public bool Contains(T item)
        {
            return this.hashset.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            this.hashset.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.hashset.Count; }
        }

        public bool IsReadOnly
        {
            get { return ((ICollection<T>)this.hashset).IsReadOnly; }
        }

        public bool Remove(T item)
        {
            if (Monitor.TryEnter(locker, 100))
            {
                bool removed = this.hashset.Remove(item);
                if (removed)
                {
                    List<T> oldList = new List<T>() { item };
                    this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, new List<T>(), oldList));
                    this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                    this.OnPropertyChanged("Count");
                }
                return removed;
            }
            else
            {
                return false;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            List<T> list = new List<T>();

            if (Monitor.TryEnter(locker, 100))
            {
                try
                {
                    list = this.hashset.ToList<T>();
                }
                finally
                {
                    Monitor.Exit(locker);

                }
                return list.GetEnumerator();
            }
            else return list.GetEnumerator();

        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (Monitor.TryEnter(locker, 100))
            {
                try
                {
                    PropertyChangedEventHandler PropertyChanged = this.PropertyChanged;

                    if (PropertyChanged != null)
                        foreach (PropertyChangedEventHandler nh in PropertyChanged.GetInvocationList().ToList())
                        {
                            DispatcherObject dispObj = nh.Target as DispatcherObject;
                            if (dispObj != null)
                            {
                                Dispatcher dispatcher = dispObj.Dispatcher;
                                if (dispatcher != null && !dispatcher.CheckAccess())
                                {
                                    dispatcher.BeginInvoke(
                                        (Action)(() => nh.Invoke(this, new PropertyChangedEventArgs(property))),
                                        DispatcherPriority.DataBind);
                                    continue;
                                }
                            }
                            nh.Invoke(this, new PropertyChangedEventArgs(property));
                        }

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs(property));
                    }
                }
                finally
                {
                    Monitor.Exit(locker);
                }


            }

        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        private void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (Monitor.TryEnter(locker, 100))
            {
                try
                {

                    var eventHandler = CollectionChanged;
                    if (eventHandler != null)
                    {
                        Delegate[] delegates = eventHandler.GetInvocationList();
                        // Walk thru invocation list
                        foreach (NotifyCollectionChangedEventHandler handler in delegates)
                        {
                            var dispatcherObject = handler.Target as DispatcherObject;
                            // If the subscriber is a DispatcherObject and different thread
                            if (dispatcherObject != null && dispatcherObject.CheckAccess() == false)
                                // Invoke handler in the target dispatcher's thread
                                dispatcherObject.Dispatcher.Invoke(DispatcherPriority.DataBind,
                                              handler, this, e);
                            else // Execute handler as is
                                handler(this, e);
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(locker);
                }
            }
        }

        public void Dispose()
        {
            if (locker != null)
            {
                Monitor.Exit(locker);
            }
        }


    }
}

