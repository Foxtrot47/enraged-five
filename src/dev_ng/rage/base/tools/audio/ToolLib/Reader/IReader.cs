﻿using System;
using System.IO;

namespace rage.ToolLib.Reader
{
    public interface IReader : IDisposable
    {
        System.IO.BinaryReader BinReader { get; }

        bool IsBigEndian { get; }

        void Close();
        
        uint Seek(uint offset, SeekOrigin origin);

        uint Tell();

        sbyte ReadSByte();

        byte ReadByte();

        byte[] ReadBytes(int count, bool needsEndian);

        short ReadShort();

        ushort ReadUShort();

        int ReadInt();

        uint ReadUInt();

        long ReadLong();

        ulong ReadULong();

        float ReadFloat();

        string ReadCString();
    }
}