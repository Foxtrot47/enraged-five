﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace rage.ToolLib.Reader
{
    public class BinaryReader : IReader
    {
        public BinaryReader(Stream input, bool isBigEndian) : this(input, Encoding.Default, isBigEndian)
        {
        }

        public BinaryReader(Stream input, Encoding encoding, bool isBigEndian)
        {
            BinReader = new System.IO.BinaryReader(input, encoding);
            IsBigEndian = isBigEndian;
        } 

        #region IReader Members

        public System.IO.BinaryReader BinReader { get; private set; }

        public bool IsBigEndian
        {
            get; private set;
        }

        public void Close()
        {
            BinReader.Close();
        }

        public uint Seek(uint offset, System.IO.SeekOrigin origin)
        {
            return (uint)BinReader.BaseStream.Seek(offset, origin);
        }

        public uint Tell()
        {
            return (uint)BinReader.BaseStream.Position;
        }

        public sbyte ReadSByte()
        {
            return BinReader.ReadSByte();
        }

        public byte ReadByte()
        {
            return BinReader.ReadByte();
        }

        public byte[] ReadBytes(int count, bool needsEndian)
        {
            var data = BinReader.ReadBytes(count);
            if (needsEndian && IsBigEndian)
            {
                data = Utility.Reverse(data);
            }
            return data;
        }

        public short ReadShort()
        {
            var data = ReadBytes(2, true);
            return BitConverter.ToInt16(data, 0);
        }

        public ushort ReadUShort()
        {
            var data = ReadBytes(2, true);
            return BitConverter.ToUInt16(data, 0);
        }

        public int ReadInt()
        {
            var data = ReadBytes(4, true);
            return BitConverter.ToInt32(data, 0);
        }

        public uint ReadUInt()
        {
            var data = ReadBytes(4, true);
            return BitConverter.ToUInt32(data, 0);
        }

        public long ReadLong()
        {
            var data = ReadBytes(8, true);
            return BitConverter.ToInt64(data, 0);
        }

        public ulong ReadULong()
        {
            var data = ReadBytes(8, true);
            return BitConverter.ToUInt64(data, 0);
        }

        public float ReadFloat()
        {
            var data = ReadBytes(4, true);
            return BitConverter.ToSingle(data, 0);
        }

        public string ReadCString()
        {
            List<byte> data = new List<byte>();
            do 
            {
                byte b = BinReader.ReadByte();
                data.Add(b);

                if (b == 0)
                {
                    break;
                }
            } while (true);

            return Encoding.ASCII.GetString(data.ToArray());
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (BinReader != null)
            {
                BinReader.Close();
                BinReader = null;
            }
        }

        #endregion
    }
}
