﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace rage.ToolLib.ProcessTimer
{
    public class ProcessTimer
    {
        private static readonly Dictionary<string, Stopwatch> Stopwatches = new Dictionary<string, Stopwatch>();
        private static readonly Dictionary<string, double> Timings = new Dictionary<string, double>();

        // Explicitly set to true if timings required.
        public static bool RecordTimings;

        // Default output path for timings.
        public static string OutputPath = "timings.csv";

        public static void StartTimer(string timerName)
        {
            if (!RecordTimings) return;
            if (!Stopwatches.ContainsKey(timerName))
            {
                Stopwatches[timerName] = new Stopwatch();
            }
            if (!Stopwatches[timerName].IsRunning)
            {
                Stopwatches[timerName].Start();
            }
        }

        public static void StopTimer(string timerName)
        {
            if (!RecordTimings) return;
            if (Stopwatches.ContainsKey(timerName))
            {
                if (!Timings.ContainsKey(timerName))
                {
                    Timings[timerName] = 0;
                }
                var elapsed = Stopwatches[timerName].Elapsed;
                Timings[timerName] += elapsed.TotalMilliseconds;
                Stopwatches[timerName].Reset();
            }
        }

        public static void SaveTimings()
        {
            if (!RecordTimings) return;
            using (var writer = new StreamWriter(OutputPath))
            {
                writer.WriteLine("Timer Name,Total Duration (ms)");
                foreach (var timing in Timings)
                {
                    // Truncate duration to two decimal places
                    var duration = Math.Truncate(timing.Value * 100) / 100;
                    writer.WriteLine(timing.Key + "," + duration);
                }
            }
        }
    }
}
