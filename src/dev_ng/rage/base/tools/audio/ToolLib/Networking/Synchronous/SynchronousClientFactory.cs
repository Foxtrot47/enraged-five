using System.Collections.Generic;
using System.Net.Sockets;

namespace rage.ToolLib.Networking.Synchronous
{
    public class SynchronousClientFactory : IClientFactory
    {
        private readonly HashSet<IClient> m_clients;

        public SynchronousClientFactory()
        {
            m_clients = new HashSet<IClient>();
        }

        #region IClientFactory Members

        public IEnumerable<IClient> Clients
        {
            get { return m_clients; }
        }

        public IClient Create(TcpClient tcpClient)
        {
            var client = new SynchronousTcpClient(tcpClient);
            m_clients.Add(client);
            return client;
        }

        public void Destroy(IClient client)
        {
            if (m_clients.Contains(client))
            {
                client.Dispose();
            }
            m_clients.Remove(client);
        }

        public void DestroyAll()
        {
            foreach (var client in m_clients)
            {
                client.Dispose();
            }
            m_clients.Clear();
        }

        public void Dispose()
        {
            DestroyAll();
        }

        #endregion
    }
}