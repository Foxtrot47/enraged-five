using System;

namespace rage.ToolLib.Networking
{
    public interface IClient : IDisposable
    {
        bool IsConnected { get; }

        byte[] Read(int numBytes);

        bool Write(byte[] data, int startIndex, int size);
    }
}