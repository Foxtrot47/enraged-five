namespace rage.ToolLib.Networking
{
    public interface INetworkTiming
    {
        uint GetTimestamp();
    }
}