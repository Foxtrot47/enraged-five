using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace rage.ToolLib.Networking
{
    public interface IClientFactory : IDisposable
    {
        IEnumerable<IClient> Clients { get; }

        IClient Create(TcpClient tcpClient);

        void Destroy(IClient client);

        void DestroyAll();
    }
}