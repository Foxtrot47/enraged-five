using System;
using System.Net.Sockets;

namespace rage.ToolLib.Networking.Synchronous
{
    public class SynchronousTcpClient : IClient
    {
        private const int READ_BUFFER_SIZE = 1024 * 1024;
        private readonly TcpClient m_client;
        private readonly byte[] m_readBuffer;

        public SynchronousTcpClient(TcpClient client)
        {
            if (client == null)
            {
                throw new ArgumentNullException("client");
            }
            m_client = client;
            IsConnected = true;
            Timeout = TimeSpan.FromSeconds(10);
            TimeOfLastOperation = DateTime.Now;
            m_readBuffer = new byte[READ_BUFFER_SIZE];
            client.NoDelay = true;
        }

        private TimeSpan Timeout { get; set; }

        private DateTime TimeOfLastOperation { get; set; }

        #region IClient Members

        public bool IsConnected { get; private set; }

        public byte[] Read(int numBytes)
        {
            if (numBytes > READ_BUFFER_SIZE)
            {
                throw new ArgumentException("numBytes requested is > 1 MB");
            }

            if (IsConnected)
            {
                if (numBytes > 0)
                {
                    var stream = m_client.GetStream();
                    if (stream.DataAvailable)
                    {
                        try
                        {
                            var totalBytesRead = 0;
                            var bytesRead = 0;
                            while ((bytesRead = stream.Read(m_readBuffer, bytesRead, numBytes - bytesRead)) != 0)
                            {
                                TimeOfLastOperation = DateTime.Now;
                                totalBytesRead += bytesRead;
                                if (totalBytesRead == numBytes)
                                {
                                    break;
                                }
                            }

                            if (totalBytesRead != numBytes)
                            {
                                IsConnected = false;
                            }

                            var buffer = new byte[numBytes];
                            Array.Copy(m_readBuffer, 0, buffer, 0, numBytes);
                            return buffer;
                        }
                        catch (SocketException)
                        {
                            IsConnected = false;
                        }
                    }

                    var timePeriod = DateTime.Now - TimeOfLastOperation;
                    if (timePeriod > Timeout)
                    {
                        IsConnected = false;
                    }
                }
            }
            return null;
        }

        public bool Write(byte[] data, int startIndex, int size)
        {
            if (IsConnected)
            {
                try
                {
                    m_client.GetStream().Write(data, startIndex, size);
                    TimeOfLastOperation = DateTime.Now;
                    return true;
                }
                catch (SocketException)
                {
                    IsConnected = false;
                }
            }
            return false;
        }

        public void Dispose()
        {
            m_client.GetStream().Flush();
            m_client.Client.Close();
            m_client.Close();
        }

        #endregion
    }
}