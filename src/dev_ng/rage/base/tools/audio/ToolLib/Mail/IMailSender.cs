using System.Net.Mail;

namespace rage.ToolLib.Mail
{
    public interface IMailSender
    {
        bool Send(MailMessage message);
    }
}