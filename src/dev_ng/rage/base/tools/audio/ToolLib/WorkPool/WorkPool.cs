using System;
using System.Collections.Generic;
using System.Threading;
using rage.ToolLib.Logging;

namespace rage.ToolLib.WorkPool
{
    public abstract class WorkPool : IWorkPool
    {
        private const int ONE_SECOND = 1000;
        private readonly ILog m_log;
        private readonly int m_sleepTime;
        private readonly int m_workItemsPerProcessor;
        protected ReaderWriterLockSlim m_lock;

        protected WorkPool(ILog log, int sleepTime, int workItemsPerProcessor)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
            m_sleepTime = sleepTime <= 0 ? ONE_SECOND : sleepTime;
            m_workItemsPerProcessor = workItemsPerProcessor < 1 ? 1 : workItemsPerProcessor;
            m_lock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        }

        #region IWorkPool Members

        public IWorkItem ErrorItem { get; private set; }

        public bool Process(List<IWorkItem> workItems)
        {
            ErrorItem = null;

            var activeIndex = 0;
            var inactiveIndex = 1;
            var workLists = new[] {new List<IWorkItem>(), new List<IWorkItem>()};
            var maxItemCount = Environment.ProcessorCount * m_workItemsPerProcessor;

            while (workItems.Count > 0)
            {
                if (workLists[activeIndex].Count < maxItemCount)
                {
                    QueueWork(workItems, workLists[activeIndex], maxItemCount - workLists[activeIndex].Count, DoWork);
                }

                Thread.Sleep(m_sleepTime);

                workLists[inactiveIndex].Clear();

                var activeWorkCount = workLists[activeIndex].Count;
                for (var i = 0; i < activeWorkCount; ++i)
                {
                    var workItem = workLists[activeIndex][i];

                    m_lock.TryEnterReadLock(Timeout.Infinite);
                    var error = workItem.Error;
                    m_lock.ExitReadLock();
                    if (error != null)
                    {
                        ErrorItem = workItem;
                        return false;
                    }

                    m_lock.TryEnterReadLock(Timeout.Infinite);
                    var isFinished = workItem.IsFinished;
                    m_lock.ExitReadLock();
                    if (!isFinished)
                    {
                        workLists[inactiveIndex].Add(workItem);
                    }
                    else
                    {
                        LogOutput(workItem);
                    }
                }

                activeIndex ^= 1;
                inactiveIndex ^= 1;
            }
            return true;
        }

        #endregion

        protected abstract void DoWork(object state);

        private void LogOutput(IWorkItem workItem)
        {
            if (workItem.Output != null &&
                !string.IsNullOrEmpty(workItem.Output.Message))
            {
                switch (workItem.Output.Type)
                {
                    case WorkItemOutputTypes.Error:
                        {
                            m_log.Error(workItem.Output.Message);
                            break;
                        }
                    case WorkItemOutputTypes.Warning:
                        {
                            m_log.Warning(workItem.Output.Message);
                            break;
                        }
                    case WorkItemOutputTypes.Information:
                        {
                            m_log.Information(workItem.Output.Message);
                            break;
                        }
                }
            }
        }

        private static void QueueWork(List<IWorkItem> workItems,
                                      ICollection<IWorkItem> activeList,
                                      int maxItemCount,
                                      WaitCallback func)
        {
            var numToProcess = Math.Min(maxItemCount, workItems.Count);
            for (var i = 0; i < numToProcess; ++i)
            {
                activeList.Add(workItems[i]);
                ThreadPool.QueueUserWorkItem(func, workItems[i]);
            }
            workItems.RemoveRange(0, numToProcess);
        }
    }
}