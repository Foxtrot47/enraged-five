using System;

namespace rage.ToolLib.WorkPool
{
    public interface IWorkItem
    {
        bool IsFinished { get; }
        Exception Error { get; }
        WorkItemOutput Output { get; }
    }
}