using System.Collections.Generic;

namespace rage.ToolLib.Repository
{
    public class Repository<TKey, TValue> : IRepository<TKey, TValue> where TValue : IRepositoryItem<TKey>
    {
        protected IDictionary<TKey, TValue> m_itemLookup;

        public Repository()
        {
            m_itemLookup = new Dictionary<TKey, TValue>();
        }

        #region IRepository<TKey,TValue> Members

        public IEnumerable<TValue> Items
        {
            get { return m_itemLookup.Values; }
        }

        public bool TryGetItem(TKey id, out TValue item)
        {
            return m_itemLookup.TryGetValue(id, out item);
        }

        public void Add(TValue item)
        {
            m_itemLookup[item.Id] = item;
        }

        public void Remove(TValue item)
        {
            m_itemLookup.Remove(item.Id);
        }

        #endregion
    }
}