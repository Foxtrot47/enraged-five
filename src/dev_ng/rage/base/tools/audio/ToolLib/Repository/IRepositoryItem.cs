namespace rage.ToolLib.Repository
{
    public interface IRepositoryItem<T>
    {
        T Id { get; }
    }
}