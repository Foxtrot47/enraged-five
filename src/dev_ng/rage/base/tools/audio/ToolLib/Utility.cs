using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using rage.ToolLib.Logging;

namespace rage.ToolLib
{
    using System.Security.Cryptography;

    public static class Utility
    {
        public static byte[] Reverse(byte[] val)
        {
            var reversed = new byte[val.Length];
            for (int forwardIndex = 0, backIndex = val.Length - 1;
                 forwardIndex < val.Length;
                 ++forwardIndex, --backIndex)
            {
                reversed[forwardIndex] = val[backIndex];
            }
            return reversed;
        }

        public static XDocument LoadDocument(ILog log, string file)
        {
            XDocument doc = null;
            try
            {
                doc = XDocument.Load(file, LoadOptions.None);
            }
            catch (Exception ex)
            {
                log.Exception(ex);
            }
            return doc;
        }

        public static XDocument ToXDoc(XmlDocument xmlDoc)
        {
            var stream = new MemoryStream();
            var writer = XmlWriter.Create(stream);
            xmlDoc.Save(writer);
            writer.Flush();
            stream.Seek(0, SeekOrigin.Begin);

            var readerSettings = new XmlReaderSettings {IgnoreComments = true, IgnoreWhitespace = true};
            var reader = XmlReader.Create(stream, readerSettings);
            return XDocument.Load(reader);
        }

        public static XmlDocument ToXmlDoc(XDocument xDoc)
        {
            var stream = new MemoryStream();
            var writer = XmlWriter.Create(stream);
            xDoc.Save(writer);
            writer.Flush();
            stream.Seek(0, SeekOrigin.Begin);

            var readerSettings = new XmlReaderSettings {IgnoreComments = true, IgnoreWhitespace = true};
            var reader = XmlReader.Create(stream, readerSettings);
            var doc = new XmlDocument();
            doc.Load(reader);
            return doc;
        }

        public static XDocument ToXDocRooted(XmlNode node, string rootNodeName)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(node.OuterXml);
            var temp = ToXDoc(xmlDoc);
            var rootElement = new XElement(rootNodeName, temp.Root);
            var xDoc = new XDocument(rootElement);
            return xDoc;
        }

        public static XElement ToXElement(XmlNode node)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(node.OuterXml);
            var temp = ToXDoc(xmlDoc);
            return temp.Root;
        }

        public static byte[] ReadData(Stream stream, bool isBigEndian, byte[] buffer)
        {
            stream.Read(buffer, 0, buffer.Length);
            if (isBigEndian)
            {
                buffer = Reverse(buffer);
            }
            return buffer;
        }

        public static bool SafeReadData(Stream stream, byte[] buffer)
        {
            int totalBytesRead = 0, bytesRead = -1;
            var size = buffer.Length;
            while (totalBytesRead < size &&
                   bytesRead != 0)
            {
                bytesRead = stream.Read(buffer, totalBytesRead, size - totalBytesRead);
                totalBytesRead += bytesRead;
            }

            if (bytesRead == 0)
            {
                return false;
            }
            return true;
        }

        public static uint CalculateMetadataSize(XElement element)
        {
            const string NAME = "name";
            const string SIZE = "size";

            uint metadataSize = 0;
            foreach (var chunkElement in element.Elements("Chunk"))
            {
                var chunkNameAttrib = chunkElement.Attribute(NAME);
                if (chunkNameAttrib == null)
                {
                    throw new FormatException(String.Format(
                        "{0} \"{1}\" contains a chunk without a \"{2}\" attribute.", element.Name,
                        element.Attribute(NAME).Value, NAME));
                }

                if (String.Compare("DATA", chunkNameAttrib.Value, true) == 0)
                {
                    continue;
                }

                var chunkSizeAttrib = chunkElement.Attribute(SIZE);
                if (chunkSizeAttrib == null)
                {
                    throw new FormatException(String.Format(
                        "{0} \"{1}\" contains a chunk without a \"{2}\" attribute.", element.Name,
                        element.Attribute(NAME).Value, SIZE));
                }

                metadataSize += uint.Parse(chunkSizeAttrib.Value);
            }
            return metadataSize;
        }

        public static double RoundSample16Bit(double fSample)
        {
            fSample = Math.Floor(0.5 + fSample);
            fSample = Math.Min(+32767.0, Math.Max(fSample, -32768.0));
            return fSample;
        }

        static Random s_DitherRand = new Random();
        public static double DitherAndRoundSample16Bit(double fSample)
        {
            double fDither = s_DitherRand.NextDouble() - s_DitherRand.NextDouble();
            fSample += fDither;
            fSample = Math.Floor(0.5 + fSample);
            fSample = Math.Min(+32767.0, Math.Max(fSample, -32768.0));
            return fSample;
        }

        public static bool FilenameMatchesMask(string filename, string fileMask)
        {
            if (string.IsNullOrEmpty(fileMask)) return true;
            var result = Regex.Match(filename, fileMask, RegexOptions.IgnoreCase);
            return result.Value.Equals(filename);
        }

        public static Dictionary<string, string> ParseKeyValueString(string inputString)
        {
            Regex splitPair = new Regex( @"\s*(\w+)=([^\s]+)\s*");

            var dict = new Dictionary<string, string>();
            foreach (var m in inputString.Split(','))
            {
                var kv = splitPair.Split(m.Trim());
                dict[kv[1]] = kv[2];
            }
            return dict;
        }

		public static void ForceDeleteDirectory(string path)
        {
            var directory = new DirectoryInfo(path) { Attributes = FileAttributes.Normal };

            foreach (var info in directory.GetFileSystemInfos("*", SearchOption.AllDirectories))
            {
                info.Attributes = FileAttributes.Normal;
            }

			directory.Delete(true);
		}

        public static string FileMd5(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException(path);
            }
        

            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(path))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream));
                }
            }
		}
    }
}
