﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage.ToolLib.Encryption
{
    public class EncrypterFactory
    {
        public static IEncrypter Create(string encryptionKey)
        {
            if (string.IsNullOrEmpty(encryptionKey))
            {
                return new NullEncrypter();
            }
            return new TEAEncrypter(encryptionKey);
        }
    }
}
