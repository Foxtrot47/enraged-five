﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage.ToolLib.Encryption
{
    public class NullEncrypter : IEncrypter
    {
        public byte[] Encrypt(byte[] data)
        {
            return Encrypt(data, false);
        }

        public byte[] Encrypt(byte[] data, bool padAsRequired)
        {
            var copiedData = new byte[data.Length];
            data.CopyTo(copiedData, 0);
            return data;
        }

        public bool IsEncrypting
        {
            get 
            { 
                return false;
            }
        }
    }
}
