﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace rage.ToolLib.Encryption
{
    public class XXTEA
    {
        public static byte[] Encrypt(byte[] v, uint[] key)
        {
            if (v.Length % 4 != 0)
            {
                throw new ArgumentException("Input data length must be 4-byte aligned", "v");
            }
            uint[] words = new uint[v.Length >> 2];
            for (int i = 0; i < words.Length; i++)
            {
                words[i] = BitConverter.ToUInt32(v, i * 4);
            }

            Encrypt(words, key);

            byte[] e = new byte[v.Length];
            for (int i = 0; i < words.Length; i++)
            {
                byte[] w = BitConverter.GetBytes(words[i]);
                for (int j = 0; j < 4; j++)
                {
                    e[i * 4 + j] = w[j];
                }
            }

            return e;
        }

        public static byte[] Decrypt(byte[] v, uint[] key)
        {
            if (v.Length % 4 != 0)
            {
                throw new ArgumentException("Input data length must be 4-byte aligned", "v");
            }

            uint[] words = new uint[v.Length >> 2];
            for (int i = 0; i < words.Length; i++)
            {
                words[i] = BitConverter.ToUInt32(v, i * 4);
            }

            Decrypt(words, key);

            byte[] e = new byte[v.Length];
            for (int i = 0; i < words.Length; i++)
            {
                byte[] w = BitConverter.GetBytes(words[i]);
                for (int j = 0; j < 4; j++)
                {
                    e[i * 4 + j] = w[j];
                }
            }

            return e;
        }

        public static void Encrypt(uint[] v, uint[] key)
        {
            uint DELTA = 0x9e3779b9;

            int n = v.Length;

            uint y, z, sum;
            uint p, e;
            int rounds;
            rounds = 6 + 52 / n;
            sum = 0;
            z = v[n - 1];
            do
            {
                sum += DELTA;
                e = (sum >> 2) & 3;
                for (p = 0; p < n - 1; p++)
                {
                    y = v[p + 1];
                    z = v[p] += (((z >> 5 ^ y << 2) + (y >> 3 ^ z << 4)) ^ ((sum ^ y) + (key[(p & 3) ^ e] ^ z)));
                }
                y = v[0];
                z = v[n - 1] += (((z >> 5 ^ y << 2) + (y >> 3 ^ z << 4)) ^ ((sum ^ y) + (key[(p & 3) ^ e] ^ z)));
            } while (--rounds > 0);
            
        }

        public static void Decrypt(uint[] v, uint[] key)
        {
            uint DELTA = 0x9e3779b9;

            int n = v.Length;

            uint y, z, sum;
            uint p, e;
            int rounds;

            rounds = 6 + 52 / n;
            sum = (uint)(rounds * DELTA);
            y = v[0];
            do
            {
                e = (sum >> 2) & 3;
                for (p = (uint)(n - 1); p > 0; p--)
                {
                    z = v[p - 1];
                    y = v[p] -= (((z >> 5 ^ y << 2) + (y >> 3 ^ z << 4)) ^ ((sum ^ y) + (key[(p & 3) ^ e] ^ z)));
                }
                z = v[n - 1];
                y = v[0] -= (((z >> 5 ^ y << 2) + (y >> 3 ^ z << 4)) ^ ((sum ^ y) + (key[(p & 3) ^ e] ^ z)));
            } while ((sum -= DELTA) != 0);
        }

        public static uint[] LoadKey(string keyFile)
        {
            var bytes = new byte[4 * 4];
            using (var f = new FileStream(keyFile, FileMode.Open, FileAccess.Read))
            {
                f.Read(bytes, 0, bytes.Length);
                f.Close();
            }
            var ret = new uint[4];

            for (int i = 0; i < 4; i++)
            {
                ret[i] = BitConverter.ToUInt32(bytes, 4 * i);
            }
            return ret;
        }

        public static uint[] ParseKey(string key)
        {
            var elems = key.Split(',');
            if (elems.Length != 4)
            {
                throw new ArgumentException("Invalid key - Expected four words separated by commas.", "key");
            }
            var words = new uint[4];
            for (int i = 0; i < 4; i++)
            {
                words[i] = Convert.ToUInt32(elems[i], 16);
            }
            return words;
        }
    }
}
