﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage.ToolLib.Encryption
{
    public class TEAEncrypter : IEncrypter
    {
        public uint[] Key { get; set; }

        #region IEncrypter Members
        
        public TEAEncrypter(uint[] key)
        {
            Key = key;
        }

        public TEAEncrypter(string key)
        {
            Key = XXTEA.ParseKey(key);
        }

        public byte[] Encrypt(byte[] data)
        {
            return Encrypt(data, false);
        }

        public byte[] Encrypt(byte[] data, bool padAsRequired)
        {
            // DWORD align for the encryption
            if (padAsRequired)
            {
                if ((data.Length & 3) != 0)
                {
                    var alignedLen = data.Length + (4 - (data.Length & 3));
                    var newByteBuffer = new byte[alignedLen];
                    data.CopyTo(newByteBuffer, 0);
                    for (int i = data.Length; i < newByteBuffer.Length; i++)
                    {
                        newByteBuffer[i] = 0;
                    }
                    data = newByteBuffer;
                }
            }
            return XXTEA.Encrypt(data, Key);
        }

        public bool IsEncrypting
        {
            get { return true; }
        }

        #endregion
    }
}
