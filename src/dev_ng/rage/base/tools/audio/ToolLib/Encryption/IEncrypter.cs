﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage.ToolLib.Encryption
{
    public interface IEncrypter
    {
        byte[] Encrypt(byte[] data);
        byte[] Encrypt(byte[] data, bool padAsRequired);

        // PURPOSE: returns true if the encrypter is active, ie encrypting the data passed to it rather than passing it back unchanged.
        bool IsEncrypting { get; }
    }
}
