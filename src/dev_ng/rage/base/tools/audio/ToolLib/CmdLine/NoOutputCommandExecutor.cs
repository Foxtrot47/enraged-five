using System;
using System.Diagnostics;

namespace rage.ToolLib.CmdLine
{
    public class NoOutputCommandExecutor : ICommandExecutor
    {
        #region ICommandExecutor Members

        public Exception Error { get; private set; }

        public string Output
        {
            get { return string.Empty; }
        }

        public bool Execute(string command)
        {
            return Execute(new ProcessStartInfo(command));
        }

        public bool Execute(string command, string args)
        {
            return Execute(new ProcessStartInfo(command, args));
        }

        #endregion

        private bool Execute(ProcessStartInfo psi)
        {
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;

            try
            {
                var process = new Process {StartInfo = psi};
                var result = process.Start();
                return result;
            }
            catch (Exception e)
            {
                Error = e;
                return false;
            }
        }
    }
}