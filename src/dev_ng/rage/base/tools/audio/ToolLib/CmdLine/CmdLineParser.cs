using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace rage.ToolLib.CmdLine
{
    /// <summary>
    ///   Command line option parser class
    /// </summary>
    /// This class will construct a Dictionary of options and their values
    /// (if applicable) to be read by the application.  To ease this classes
    /// use we have constructors for both a string array and a regular string.
    /// 
    /// Valid parameter forms:
    /// {-,/,--}param_name[ ,=]value
    /// 
    /// E.g.
    /// --param=value
    /// -param value
    /// /param=value
    /// --param
    public class CmdLineParser
    {
        public CmdLineParser(IEnumerable<string> args)
        {
            Process(args);
        }

        public IList<string> TrailingArguments { get; private set; }

        public StringDictionary Arguments { get; private set; }

        public string this[string arg]
        {
            get { return (Arguments[arg]); }
        }

        private void Process(IEnumerable<string> args)
        {
            Arguments = new StringDictionary();
            TrailingArguments = new List<String>();
            var splitter = new Regex(@"^-{1,2}|=", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            var remover = new Regex(@"^['""]?(.*?)['""]?$", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            string parameter = null;
            string[] parts;

            foreach (var sArg in args)
            {
                // Look for new parameter characters
                parts = splitter.Split(sArg, 3);

                switch (parts.Length)
                {
                    case 1:
                        // Found a value for the last parameter found
                        // using a space separator
                        if (null != parameter)
                        {
                            if (!Arguments.ContainsKey(parameter))
                            {
                                parts[0] = remover.Replace(parts[0], "$1");
                                Arguments.Add(parameter, parts[0]);
                            }
                            parameter = null;
                        }
                        else
                        {
                            // No waiting parameter so we assume its for our trailing
                            // args list.
                            TrailingArguments.Add(parts[0]);
                        }
                        break;
                    case 2:
                        // Found just a parameter
                        if (null != parameter)
                        {
                            // Last parameter is still waiting, with no 
                            // associated value so set it to "true"
                            if (!Arguments.ContainsKey(parameter))
                            {
                                Arguments.Add(parameter, "true");
                            }
                        }
                        parameter = parts[1];
                        break;
                    case 3:
                        // Parameter with enclosed value
                        if (null != parameter)
                        {
                            // Last parameter is still waiting, with no
                            // associated value so set it to "true"
                            if (!Arguments.ContainsKey(parameter))
                            {
                                Arguments.Add(parameter, "true");
                            }
                        }
                        parameter = parts[1];

                        // Remove possible enclosing characters (",')
                        if (!Arguments.ContainsKey(parameter))
                        {
                            parts[2] = remover.Replace(parts[2], "$1");
                            Arguments.Add(parameter, parts[2]);
                        }

                        parameter = null;
                        break;
                }
            } // End of switch
            // Mop up the case that there is a parameter still waiting
            // Has no associated value so set it to "true"
            if (null != parameter)
            {
                if (!Arguments.ContainsKey(parameter))
                {
                    Arguments.Add(parameter, "true");
                }
            }
        }
    }
}