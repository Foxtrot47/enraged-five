﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace rage.ToolLib.CmdLine
{
    public abstract class CommandLineConfiguration
    {
        /// <summary>
        /// This attribute can be used to mark a property as an optional parameter
        /// </summary>
        public class OptionalParameterAttribute : Attribute {}

        /// <summary>
        /// This attribute can be used to mark a property to not be treated as a parameter
        /// </summary>
        public class IsNotParameterAttribute: Attribute {}

        public class ParameterException: Exception {
            public ParameterException(string message, string usage) : base(message) { Usage = usage; }
            public String Usage { get; private set; } 
        }

        public class UnrecognisedParameterException : ParameterException
        {
            public UnrecognisedParameterException(List<string> unrecognisedParameters, string usage) 
                : base("Parameter" + (unrecognisedParameters.Count > 1 ? "s -" : " ") + String.Join(" -", unrecognisedParameters) + (unrecognisedParameters.Count > 1 ? " were " : " was ") + "not recognised.", usage) 
            {
            }
        }

        public class RequiredParameterException : ParameterException
        {
            public RequiredParameterException(string parameterName, string usage) 
                : base("Parameter " + parameterName + " has not been provided.", usage) 
            {
            }
        }

        public CommandLineConfiguration(string[] args)
        {
            CmdLineParser argsParser = new CmdLineParser(args);
            PropertyInfo[] configurationProperties = this.GetType().GetProperties();
            foreach (PropertyInfo parameterProperty in configurationProperties)
            {
                IsNotParameterAttribute noParameterValAttr = parameterProperty.GetCustomAttributes(typeof(IsNotParameterAttribute), false).FirstOrDefault() as IsNotParameterAttribute;
                if (noParameterValAttr != null) continue; //ignore properties marked with the IsNotParameter attribute

                DefaultValueAttribute defaultValAttr = parameterProperty.GetCustomAttributes(typeof(DefaultValueAttribute), false).FirstOrDefault() as DefaultValueAttribute;
                if (defaultValAttr != null) parameterProperty.SetValue(this, defaultValAttr.Value, null);
                if (argsParser[parameterProperty.Name] != null)
                {
                    setValue(parameterProperty, argsParser[parameterProperty.Name]);
                    argsParser.Arguments.Remove(parameterProperty.Name);
                }
                else
                {
                    OptionalParameterAttribute optionalAttr = parameterProperty.GetCustomAttributes(typeof(OptionalParameterAttribute), false).FirstOrDefault() as OptionalParameterAttribute;
                    if (optionalAttr == null) throw new RequiredParameterException(parameterProperty.Name, GetUsage());
                }
            }
            if (argsParser.Arguments.Keys.Count != 0)
            {
                throw new UnrecognisedParameterException(argsParser.Arguments.Keys.Cast<string>().ToList(), GetUsage());
            }
        }

        private void setValue(PropertyInfo parameterProperty, string value) 
        {
            if (typeof(Enum).IsAssignableFrom(parameterProperty.PropertyType)) //enum type
            {
                parameterProperty.SetValue(this,
                    Enum.Parse(parameterProperty.PropertyType, value), null);
            }
            else if (typeof(bool).IsAssignableFrom(parameterProperty.PropertyType))
            {
                parameterProperty.SetValue(this, bool.Parse(value), null);
            }
            else if (typeof(int).IsAssignableFrom(parameterProperty.PropertyType))
            {
                parameterProperty.SetValue(this, int.Parse(value), null);
            }
            else if (typeof(float).IsAssignableFrom(parameterProperty.PropertyType))
            {
                parameterProperty.SetValue(this, float.Parse(value), null);
            }
            else
            {
                parameterProperty.SetValue(this, value, null);
            }
        }

        public string GetUsage()
        {
            StringBuilder result = new StringBuilder();
            result.Append("Parameters are:"+Environment.NewLine);

            PropertyInfo[] configurationProperties = GetType().GetProperties();
            foreach (PropertyInfo parameterProperty in configurationProperties)
            {
                DescriptionAttribute descriptionAttr = parameterProperty.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault() as DescriptionAttribute;
                if (descriptionAttr == null) continue;
                string line = "-" + parameterProperty.Name + "\t" + descriptionAttr.Description;
                DefaultValueAttribute defaultValAttr = parameterProperty.GetCustomAttributes(typeof(DefaultValueAttribute), false).FirstOrDefault() as DefaultValueAttribute;
                if (defaultValAttr != null) line+=" (default "+defaultValAttr.Value+")";
                OptionalParameterAttribute optionalAttr = parameterProperty.GetCustomAttributes(typeof(OptionalParameterAttribute), false).FirstOrDefault() as OptionalParameterAttribute;
                if (optionalAttr != null) line += " [optional]";
                result.Append(line+Environment.NewLine);
            }
            return result.ToString();
        }
    }
}
