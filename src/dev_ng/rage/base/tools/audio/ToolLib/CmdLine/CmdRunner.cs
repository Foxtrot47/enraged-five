﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rage.ToolLib.CmdLine
{
    public class CmdRunner
    {
        public class CmdRunnerException : Exception
        {
            public CmdRunnerException(string program, string arguments, int returncode, string stdOut, string stdErr)
                : base(program + " " + arguments + " returned " + returncode)
            {
                this.StdOut = stdOut;
                this.StdErr = stdErr;
            }
            public string StdOut { get; private set; }
            public string StdErr { get; private set; }
        }

        public static string run(string programPath, string arguments, string workingDir = null)
        {
            ProcessStartInfo psi = new ProcessStartInfo(programPath);
            psi.Arguments = arguments;
            if (workingDir != null) psi.WorkingDirectory = workingDir;
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            psi.RedirectStandardError = true;
            psi.RedirectStandardOutput = true;

            Process p = new Process();
            p.StartInfo = psi;
            p.Start();

            var outTask = p.StandardOutput.ReadToEndAsync();
            var errTask = p.StandardError.ReadToEndAsync();
            p.WaitForExit();
            string stdout = outTask.Result;
            string stderr = errTask.Result;
            if (p.ExitCode != 0)
            {
                throw new CmdRunnerException(programPath, arguments, p.ExitCode, stdout, stderr);
            }
            return stdout;
        }
    }
}
