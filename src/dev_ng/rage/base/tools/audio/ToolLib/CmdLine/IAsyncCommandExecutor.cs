using System;

namespace rage.ToolLib.CmdLine
{
    public interface IAsyncCommandExecutor
    {
        event Action<Exception> Error;
        event Action<bool> Finished;
        
        void Execute(string command);

        void Execute(string command, string args);
    }
}