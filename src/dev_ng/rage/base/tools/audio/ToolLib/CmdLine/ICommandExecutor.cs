using System;

namespace rage.ToolLib.CmdLine
{
    public interface ICommandExecutor
    {
        Exception Error { get; }

        string Output { get; }

        bool Execute(string command);

        bool Execute(string command, string args);
    }
}