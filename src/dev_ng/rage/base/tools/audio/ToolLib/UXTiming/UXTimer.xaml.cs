﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;
using AvalonDock;

namespace rage.ToolLib.UXTiming
{

    public class UXTimerHelper
    {
        private Dictionary<int, int> m_NumStatesToPop = new Dictionary<int, int>();

        public void Enter(string identifier)
        {
            var threadId = Thread.CurrentThread.ManagedThreadId;
            UXTimer.Instance.EnterState(threadId, identifier);
            if (!m_NumStatesToPop.ContainsKey(threadId))
            {
                m_NumStatesToPop.Add(threadId, 0);
            }
            m_NumStatesToPop[threadId]++;
        }

        public void Exit()
        {
            var threadId = Thread.CurrentThread.ManagedThreadId;
            UXTimer.Instance.ExitState(threadId);
            try
            {
                m_NumStatesToPop[threadId]--;
            }
            catch(KeyNotFoundException ex)
            {
            }
        }

        public void ExitAll()
        {
            foreach (var threadId in m_NumStatesToPop.Keys)
            {
                while (m_NumStatesToPop[threadId]-- > 0)
                {
                    UXTimer.Instance.ExitState(threadId);
                }
            }
        }
    }

    /// <summary>
    /// Interaction logic for UXTimer.xaml
    /// </summary>
    public partial class UXTimer : UserControl
    {
        private readonly Dictionary<int, Stack<State>> m_StateStack;
        private DockableContent m_Content;
        private static UXTimer sm_Instance;

        public UXTimer()
        {
            sm_Instance = this;
            m_StateStack = new Dictionary<int, Stack<State>>();
            InitializeComponent();
        }

        public DockableContent DockableContent
        {
            get
            {
                if (m_Content == null)
                {
                    m_Content = new DockableContent { Title = "Activity Log", Content = this };
                }
                return m_Content;
            }
        }

        public static UXTimer Instance
        {
            get
            {
                return sm_Instance;
            }
        }

        struct State
        {
            public string Identifier;
            public DateTime EntryTime;
            public bool HasPrinted;
        }
        
        public void EnterState(int threadId, string stateIdentifier)
        {
            if (!m_StateStack.ContainsKey(threadId))
            {
                m_StateStack[threadId] = new Stack<State>();
            }
            var stackState = m_StateStack[threadId];

            if (stackState.Count > 0)
            {
                var s = stackState.Peek();
                if (!s.HasPrinted)
                {
                    AddLogEntry(m_StateStack.Count - 1, s.Identifier, null);
                    s.HasPrinted = true;
                }
            }
            stackState.Push(new State { Identifier = stateIdentifier, EntryTime = DateTime.Now });
        }

        public void ExitState(int threadId)
        {
            if (!m_StateStack.ContainsKey(threadId))
            {
                throw new Exception("No stack state for thread ID " + threadId);
            }

            var stackState = m_StateStack[threadId];
            var s = stackState.Pop();
            var duration = DateTime.Now - s.EntryTime;

            AddLogEntry(m_StateStack.Count, s.Identifier, duration.TotalSeconds);
        }

        public void AppendNewLine(string text)
        {
            var newLine = string.Format("{0}\r\n", text);

            if (!textLog.Dispatcher.CheckAccess())
            {
                textLog.Dispatcher.Invoke(
                    System.Windows.Threading.DispatcherPriority.Normal,
                    new Action(delegate
                    {
                        textLog.AppendText(newLine);
                        textLog.ScrollToEnd();
                    })
                    );
            }
            else
            {
                textLog.AppendText(newLine);
                textLog.ScrollToEnd();
            }
        }

        void AddLogEntry(int tabLevel, string identifier, double? durationSeconds)
        {
            var tabs = "";
            if (tabLevel > 0)
            {
                for (int i = 0; i < tabLevel; i++)
                {
                    tabs += "-";
                }
                tabs += "> ";
            }

            string newLine;
            if (durationSeconds != null)
            {
                newLine = string.Format("{0}{1}: {2}", tabs, identifier, durationSeconds);
            }
            else
            {
                newLine = string.Format("{0}{1}", tabs, identifier);
            }
            AppendNewLine(newLine);
        }
    }
}
