﻿using System;

namespace rage.ToolLib.Extensions
{
	public static class FloatExtensions
	{
		public static float Clamp(this float val, float min, float max)
		{
			if (val.CompareTo(min) < 0) return min;
			if (val.CompareTo(max) > 0) return max;
			return val;
		}

		public static float Clamp01(this float val)
		{
			return Clamp(val, 0.0f, 1.0f);
		}
	}

	public static class GenericExtensions
	{
		public static T Clamp<T>(this T val, T min, T max) where T : IComparable
		{
			if (val.CompareTo(min) < 0) return min;
			if (val.CompareTo(max) > 0) return max;
			return val;
		}

	}
}
