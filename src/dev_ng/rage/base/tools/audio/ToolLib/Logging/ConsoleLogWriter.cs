using System;

namespace rage.ToolLib.Logging
{
    public class ConsoleLogWriter : ILogWriter
    {
        #region ILogWriter Members

        public void WriteLine(object content)
        {
            Console.WriteLine(content);
        }

        public void WriteLine(string format, params object[] args)
        {
            Console.WriteLine(format, args);
        }

        public void Dispose()
        {
            // do nothing
        }

        #endregion
    }
}