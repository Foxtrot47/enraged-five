namespace rage.ToolLib.Logging
{
    public class NullLogWriter : ILogWriter
    {
        #region ILogWriter Members

        public void Dispose()
        {
            // does nothing
        }

        public void WriteLine(object content)
        {
            // does nothing
        }

        public void WriteLine(string format, params object[] args)
        {
            // does nothing
        }

        #endregion
    }
}