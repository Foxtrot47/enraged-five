﻿using System;
using System.Collections.Generic;

namespace rage.ToolLib.Logging
{
    public class ListLogWriter : ILogWriter
    {
        private List<string> _logs;

        public ListLogWriter(out List<string> log)
        {
            _logs = new List<string>();
            log = _logs;
        }

        public void WriteLine(object content)
        {
            _logs.Add((string)content);
        }

        public void WriteLine(string format, params object[] args)
        {
            _logs.Add(String.Format(format, args));
        }

        public void Dispose()
        {
            this._logs.Clear();
        }
    }
}
