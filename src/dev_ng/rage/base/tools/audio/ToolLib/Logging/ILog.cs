using System;

namespace rage.ToolLib.Logging
{
    /// <summary>
    ///   Interface to log information
    /// </summary>
    public interface ILog : IDisposable
    {
        LogVerbosity Verbosity { get; set; }

        uint NumExceptionsLogged { get; }

        uint NumErrorsLogged { get; }

        uint NumWarningsLogged { get; }

        event Action<string, Exception> ExceptionLogged;

        event Action<string, string, bool> ErrorLogged;

        event Action<string, string> WarningLogged;

        event Action<string, string> InformationLogged;

        void Exception(Exception exception);

        void Error(string error, params object[] args);

        void Warning(string warning, params object[] args);

        void Information(string info, params object[] args);

        void WriteFormatted(string data, params object[] args);

        void Write(object data);

        void ResetLogCounts();

        void PushContext(ContextType type, string name);

        ContextDescriptor PeekContext();

        ContextDescriptor PopContext();
    }
}