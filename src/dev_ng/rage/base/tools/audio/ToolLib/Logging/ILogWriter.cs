using System;

namespace rage.ToolLib.Logging
{
    public interface ILogWriter : IDisposable
    {
        void WriteLine(object content);

        void WriteLine(string format, params object[] args);
    }
}