namespace rage.ToolLib.Logging
{
    public enum LogVerbosity
    {
        WarningsAndErrors,
        Everything
    }
}