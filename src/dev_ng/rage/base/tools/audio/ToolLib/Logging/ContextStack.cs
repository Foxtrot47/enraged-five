using System.Collections.Generic;
using System.Text;

namespace rage.ToolLib.Logging
{
    public class ContextStack : IContextStack
    {
        private readonly List<ContextDescriptor> m_stack = new List<ContextDescriptor>();

        #region IContextStack Members

        public string CurrentContext
        {
            get
            {
                lock (m_stack)
                {
                    var builder = new StringBuilder();
                    foreach (var descriptor in m_stack)
                    {
                        builder.Append(string.Format("\\[{0}]", descriptor));
                    }

                    return builder.ToString();
                }
            }
        }

        public void Push(ContextType type, string name)
        {
            lock (m_stack)
            {
                m_stack.Add(new ContextDescriptor(type, name));
            }
        }

        public ContextDescriptor Peek()
        {
            lock (m_stack)
            {
                var count = m_stack.Count;
                if (count > 0)
                {
                    return m_stack[count - 1];
                }
            }

            return null;
        }

        public ContextDescriptor Pop()
        {
            lock (m_stack)
            {
                var count = m_stack.Count;
                if (count > 0)
                {
                    var descriptor = m_stack[count - 1];
                    m_stack.RemoveAt(count - 1);
                    return descriptor;
                }
            }

            return null;
        }

        public void Clear()
        {
            lock (m_stack)
            {
                m_stack.Clear();
            }
        }

        #endregion
    }
}