using System;

namespace rage.ToolLib
{
    public class BitField
    {
        private const string FORMAT_BIT_INDEX_OUT_OF_RANGE =
            "bitIndex: {0} is out of range. The number of bits in this field is: {1}.";

        public BitField(int numBits)
        {
            if (numBits <= 0 ||
                numBits > 64)
            {
                throw new ArgumentException("A bit field must contain between 1 and 64 bits");
            }

            var maskSize = numBits / 8;
            if (numBits % 8 > 0)
            {
                maskSize += 1;
            }

            if (maskSize % 2 > 0 &&
                maskSize != 1)
            {
                var shiftCount = 2;
                while (maskSize > (1 << shiftCount))
                {
                    shiftCount += 1;
                }
                maskSize = 1 << shiftCount;
            }
            Field = new byte[maskSize];
            NumBits = numBits;
        }

        public int NumBits { get; private set; }

        public byte[] Field { get; private set; }

        public string DataType
        {
            get
            {
                var numBytes = Field.Length;
                switch (numBytes)
                {
                    case 1:
                        {
                            return "rage::u8";
                        }
                    case 2:
                        {
                            return "rage::u16";
                        }
                    case 4:
                        {
                            return "rage::u32";
                        }
                    case 8:
                        {
                            return "rage::u64";
                        }
                    default:
                        {
                            throw new Exception(string.Format("Invalid bit field length of {0} bytes.", numBytes));
                        }
                }
            }
        }

        public void Set(int bitIndex, bool value)
        {
            if (bitIndex < 0 ||
                bitIndex >= NumBits)
            {
                throw new ArgumentException(string.Format(FORMAT_BIT_INDEX_OUT_OF_RANGE, bitIndex, NumBits));
            }
            var index = bitIndex / 8;
            var bit = bitIndex % 8;
            if (value)
            {
                // set the bit
                Field[index] = (byte) (Field[index] | (1 << bit));
            }
            else
            {
                // clear the bit
                Field[index] = (byte) (Field[index] & (~(1 << bit)));
            }
        }

        public void Clear()
        {
            for (var i = 0; i < Field.Length; ++i)
            {
                Field[i] = 0;
            }
        }
    }
}