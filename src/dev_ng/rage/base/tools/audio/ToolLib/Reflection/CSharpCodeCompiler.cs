using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Microsoft.CSharp;
using rage.ToolLib.Logging;

namespace rage.ToolLib.Reflection
{
    public class CSharpCodeCompiler : ICSharpCodeCompiler
    {
        private const string URI_FILE = "file:///";
        private const string COMPILING_CS_FILES = "Compiling C# files into assembly {0} ...";
        private const string CS_COMPILATION_SUCCEEDED = "Compilation of C# files succeeded.";
        private const string CS_COMPILATION_FAILED = "Compilation of C# files failed!";
        private const string CS_FILTER = "*.cs";
        private const string DEFAULT_GENERATED_ASSEMBLY_NAME = "CodeCompilerGeneratedAssembly.dll";
        private const string FORMAT_FOUND_CS_FILE = "Found C# file: {0}";
        private readonly ILog m_log;

        public CSharpCodeCompiler(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
        }

        #region ICSharpCodeCompiler Members

        public Assembly Compile(string[] paths,
                                string generatedAssemblyFileName,
                                IEnumerable<Assembly> assembliesToReference)
        {
            if (paths != null)
            {
                var codeFiles = GetCodeFiles(paths);
                if (codeFiles.Count > 0)
                {
                    return CompileFiles(codeFiles,
                                        GetGeneratedAssemblyFileName(generatedAssemblyFileName),
                                        assembliesToReference);
                }
            }
            return null;
        }

        #endregion

        public Assembly CompileFiles(List<string> codeFiles,
                                      string generatedAssemblyFileName,
                                      IEnumerable<Assembly> assembliesToReference)
        {
            m_log.WriteFormatted(COMPILING_CS_FILES, generatedAssemblyFileName);
            foreach (var codeFile in codeFiles)
            {
                m_log.Information(FORMAT_FOUND_CS_FILE, codeFile);
            }

            var compilerParams = new CompilerParameters
                                     {
                                         GenerateInMemory = true,
                                         OutputAssembly = generatedAssemblyFileName,
                                         TempFiles = new TempFileCollection(),
                                         WarningLevel = 4,
                                         TreatWarningsAsErrors = false
                                     };

            foreach (var assembly in assembliesToReference)
            {
                if(assembly.IsDynamic==false)compilerParams.ReferencedAssemblies.Add(assembly.CodeBase.Replace(URI_FILE, string.Empty));
            }

            using (var codeProvider = new CSharpCodeProvider())
            {
                var compilerResults = codeProvider.CompileAssemblyFromFile(compilerParams, codeFiles.ToArray());
                if (compilerResults.Errors.HasErrors)
                {
                    LogCompileErrors(compilerResults);
                    m_log.WriteFormatted(CS_COMPILATION_FAILED);
                }
                else
                {
                    m_log.WriteFormatted(CS_COMPILATION_SUCCEEDED);
                    return compilerResults.CompiledAssembly;
                }
            }
            return null;
        }

        private static string GetGeneratedAssemblyFileName(string generatedAssemblyFileName)
        {
            if (string.IsNullOrEmpty(generatedAssemblyFileName))
            {
                generatedAssemblyFileName = DEFAULT_GENERATED_ASSEMBLY_NAME;
            }
            return generatedAssemblyFileName;
        }

        public static List<string> GetCodeFiles(IEnumerable<string> paths)
        {
            var codeFiles = new List<string>();
            foreach (var path in paths)
            {
                codeFiles.AddRange(Directory.GetFiles(path, CS_FILTER, SearchOption.AllDirectories));
            }
            return codeFiles;
        }

        private void LogCompileErrors(CompilerResults compilerResults)
        {
            var builder = new StringBuilder();
            foreach (var error in compilerResults.Errors)
            {
                builder.Append(error);
                builder.Append(Environment.NewLine);
            }
            m_log.Error(builder.ToString());
        }
    }
}