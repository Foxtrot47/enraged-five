using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using rage.ToolLib.Logging;

namespace rage.ToolLib.Reflection
{
    public class AssemblyLoader : IAssembyLoader
    {
        private const string FORMAT_LOADED_ASSEMBLY = "Loaded assembly: {0}";
        private const string LOADING_ASSEMBLIES = "Loading assemblies...";
        private const string FINISHED_LOADING_ASSEMBLIES = "Finished loading assemblies.";
        private const string DLL_FILTER = "*.dll";
        private const string MICROSOFT = "Microsoft";
        private const string SYSTEM = "System";
        private const string VSHOST = "vshost";
        private const string MSCORLIB = "mscorlib";
        private const string MSVC = "msvc";
        private readonly ILog m_log;

        public AssemblyLoader(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
        }

        #region IAssembyLoader Members

        public ICollection<Assembly> Load(string[] paths)
        {
            var assemblies = new List<Assembly>(FilterSystemAssemblies(AppDomain.CurrentDomain.GetAssemblies()));
            if (paths != null)
            {
                m_log.WriteFormatted(LOADING_ASSEMBLIES);
                foreach (var path in paths)
                {
                    foreach (var assembly in
                             LoadAssemblyFiles(Directory.GetFiles(path, DLL_FILTER, SearchOption.AllDirectories)))
                    {
                        m_log.Information(FORMAT_LOADED_ASSEMBLY, assembly.FullName);
                        assemblies.Add(assembly);
                    }
                }
                m_log.WriteFormatted(FINISHED_LOADING_ASSEMBLIES);
            }
            return assemblies;
        }

        #endregion

        private static IEnumerable<Assembly> LoadAssemblyFiles(IEnumerable<string> assemblyFiles)
        {
            return FilterSystemAssemblies(assemblyFiles.Select(assemblyFile => Assembly.LoadFile(assemblyFile)));
        }

        private static IEnumerable<Assembly> FilterSystemAssemblies(IEnumerable<Assembly> assemblies)
        {
            return assemblies.Where(x => !IsSystemAssembly(x.FullName));
        }

        private static bool IsSystemAssembly(string name)
        {
            return (name.StartsWith(MICROSOFT) || name.StartsWith(SYSTEM) || name.StartsWith(MSCORLIB) ||
                    name.StartsWith(VSHOST) || name.StartsWith(MSVC));
        }
    }
}