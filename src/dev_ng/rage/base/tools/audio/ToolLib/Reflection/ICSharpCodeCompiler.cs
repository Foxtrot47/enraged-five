using System.Collections.Generic;
using System.Reflection;

namespace rage.ToolLib.Reflection
{
    public interface ICSharpCodeCompiler
    {
        Assembly Compile(string[] paths, string generatedAssemblyFileName, IEnumerable<Assembly> assembliesToReference);
    }
}