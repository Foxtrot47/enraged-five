using System;
using System.IO;
using System.Text;

namespace rage.ToolLib.Writer
{
	public class BinaryWriter : IWriter
	{
		public BinaryWriter(Stream output, bool isBigEndian)
			: this(output, Encoding.Default, isBigEndian, false)
		{

		}

		public BinaryWriter(Stream output, bool isBigEndian, bool leaveOpen = false)
			: this(output, Encoding.Default, isBigEndian, leaveOpen)
		{
		}

		public BinaryWriter(Stream output, Encoding encoding, bool isBigEndian, bool leaveOpen = false)
		{
			BinWriter = new System.IO.BinaryWriter(output, encoding, leaveOpen);
			IsBigEndian = isBigEndian;
			Encoding = encoding;
		}

		#region IWriter Members

		public System.IO.BinaryWriter BinWriter { get; private set; }

		public bool IsBigEndian { get; private set; }

		public Encoding Encoding { get; private set; }

		public void Close()
		{
			BinWriter.Close();
		}

		public void Flush()
		{
			BinWriter.Flush();
		}

		public uint Seek(uint offset, SeekOrigin origin)
		{
			return (uint)BinWriter.BaseStream.Seek(offset, origin);
		}

		public uint Tell()
		{
			return (uint)BinWriter.BaseStream.Position;
		}

		public void Write(sbyte val)
		{
			BinWriter.Write(val);
		}

		public void Write(byte val)
		{
			BinWriter.Write(val);
		}

		public void Write(byte[] val, bool needsEndian)
		{
			if (needsEndian && IsBigEndian)
			{
				val = Utility.Reverse(val);
			}
			BinWriter.Write(val);
		}

		public void Write(short val)
		{
			if (IsBigEndian)
			{
				var bytes = Utility.Reverse(BitConverter.GetBytes(val));
				val = BitConverter.ToInt16(bytes, 0);
			}
			BinWriter.Write(val);
		}

		public void Write(ushort val)
		{
			if (IsBigEndian)
			{
				var bytes = Utility.Reverse(BitConverter.GetBytes(val));
				val = BitConverter.ToUInt16(bytes, 0);
			}
			BinWriter.Write(val);
		}

		public void Write(int val)
		{
			if (IsBigEndian)
			{
				var bytes = Utility.Reverse(BitConverter.GetBytes(val));
				val = BitConverter.ToInt32(bytes, 0);
			}
			BinWriter.Write(val);
		}

		public void Write(uint val)
		{
			if (IsBigEndian)
			{
				var bytes = Utility.Reverse(BitConverter.GetBytes(val));
				val = BitConverter.ToUInt32(bytes, 0);
			}
			BinWriter.Write(val);
		}

		public void Write(long val)
		{
			if (IsBigEndian)
			{
				var bytes = Utility.Reverse(BitConverter.GetBytes(val));
				val = BitConverter.ToInt64(bytes, 0);
			}
			BinWriter.Write(val);
		}

		public void Write(ulong val)
		{
			if (IsBigEndian)
			{
				var bytes = Utility.Reverse(BitConverter.GetBytes(val));
				val = BitConverter.ToUInt64(bytes, 0);
			}
			BinWriter.Write(val);
		}

		public void Write(float val)
		{
			if (IsBigEndian)
			{
				var bytes = Utility.Reverse(BitConverter.GetBytes(val));
				val = BitConverter.ToSingle(bytes, 0);
			}
			BinWriter.Write(val);
		}

		public void Write(string val)
		{
			BinWriter.Write(val);
		}

		public void Dispose()
		{
			if (BinWriter != null)
			{
				BinWriter.Flush();
				BinWriter.Close();
				BinWriter = null;
			}
		}

		#endregion
	}
}