using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace rage.ToolLib.Writer
{
    public class DebugBinaryWriter : IWriter
    {
        private readonly uint m_breakAt;

        private readonly bool m_shouldBreak;

        public DebugBinaryWriter(Stream output, bool isBigEndian, uint breakAt)
            : this(output, Encoding.Default, isBigEndian)
        {
            m_breakAt = breakAt;
            m_shouldBreak = m_breakAt != 0;
        }

        public DebugBinaryWriter(Stream output, Encoding encoding, bool isBigEndian)
        {
            BinWriter = new System.IO.BinaryWriter(output, encoding);
            IsBigEndian = isBigEndian;
            Encoding = encoding;
        }

        #region IWriter Members

        public System.IO.BinaryWriter BinWriter { get; private set; }

        public bool IsBigEndian { get; private set; }

        public Encoding Encoding { get; private set; }

        public void Close()
        {
            BinWriter.Close();
        }

        public void Flush()
        {
            BinWriter.Flush();
        }

        public uint Seek(uint offset, SeekOrigin origin)
        {
            return (uint) BinWriter.BaseStream.Seek(offset, origin);
        }

        public uint Tell()
        {
            return (uint) BinWriter.BaseStream.Position;
        }

        public void Write(sbyte val)
        {
            var lastTell = Tell();
            BinWriter.Write(val);
            CheckAndBreak(lastTell);
        }

        public void Write(byte val)
        {
            var lastTell = Tell();
            BinWriter.Write(val);
            CheckAndBreak(lastTell);
        }

        public void Write(byte[] val, bool needsEndian)
        {
            var lastTell = Tell();
            if (needsEndian && IsBigEndian)
            {
                val = Utility.Reverse(val);
            }
            BinWriter.Write(val);
            CheckAndBreak(lastTell);
        }

        public void Write(short val)
        {
            var lastTell = Tell();
            if (IsBigEndian)
            {
                var bytes = Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToInt16(bytes, 0);
            }
            BinWriter.Write(val);
            CheckAndBreak(lastTell);
        }

        public void Write(ushort val)
        {
            var lastTell = Tell();
            if (IsBigEndian)
            {
                var bytes = Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToUInt16(bytes, 0);
            }
            BinWriter.Write(val);
            CheckAndBreak(lastTell);
        }

        public void Write(int val)
        {
            var lastTell = Tell();
            if (IsBigEndian)
            {
                var bytes = Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToInt32(bytes, 0);
            }
            BinWriter.Write(val);
            CheckAndBreak(lastTell);
        }

        public void Write(uint val)
        {
            var lastTell = Tell();
            if (IsBigEndian)
            {
                var bytes = Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToUInt32(bytes, 0);
            }
            BinWriter.Write(val);
            CheckAndBreak(lastTell);
        }

        public void Write(long val)
        {
            var lastTell = Tell();
            if (IsBigEndian)
            {
                var bytes = Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToInt64(bytes, 0);
            }
            BinWriter.Write(val);
            CheckAndBreak(lastTell);
        }

        public void Write(ulong val)
        {
            var lastTell = Tell();
            if (IsBigEndian)
            {
                var bytes = Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToUInt64(bytes, 0);
            }
            BinWriter.Write(val);
            CheckAndBreak(lastTell);
        }

        public void Write(float val)
        {
            var lastTell = Tell();
            if (IsBigEndian)
            {
                var bytes = Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToSingle(bytes, 0);
            }
            BinWriter.Write(val);
            CheckAndBreak(lastTell);
        }

        public void Write(string val)
        {
            var lastTell = Tell();
            BinWriter.Write(val);
            CheckAndBreak(lastTell);
        }

        #endregion

        private void CheckAndBreak(uint lastTell)
        {
            if (m_shouldBreak && lastTell <= m_breakAt &&
                Tell() > m_breakAt)
            {
                Debugger.Break();
            }
        }

        public void Dispose()
        {
            if (BinWriter != null)
            {
                BinWriter.Flush();
                BinWriter.Close();
                BinWriter = null;
            }
        }
    }
}