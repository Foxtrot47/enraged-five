namespace rage.ToolLib.DataStructures
{
    public class Tree<T>
    {
        public TreeNode<T> Root { get; set; }
    }
}