using System.Xml.Linq;

namespace rage.ToolLib.WavesFiles
{
    public interface IWavesFileCombiner
    {
        string Run(string packListFile, string destinationDirectory, string fileMask);

        XDocument Run(string packListFile, string fileMask);
    }
}