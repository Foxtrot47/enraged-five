using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace rage.ToolLib.WavesFiles
{
    public class WavesFileSplitter : IWavesFileSplitter
    {
        private const string PACK = "Pack";
        private const string PACK_FILE = "PackFile";
        private const string NAME = "name";
        private const string FORMAT_PACK_FILENAME = "{0}_PACK_FILE.xml";
        private const string FORMAT_MASTER_FILENAME = "{0}_PACK_LIST.xml";

        #region IWavesFileSplitter Members

        public IEnumerable<string> Run(string sourceFile, string outputDirectory)
        {
            if (!File.Exists(sourceFile))
            {
                throw new ApplicationException(string.Format("{0} waves file does not exist", sourceFile));
            }

            if (!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }

            return Process(sourceFile, outputDirectory);
        }

        #endregion

        private static IEnumerable<string> Process(string sourceFile, string outputDirectory)
        {
            var wavesDocument = LoadWavesFile(sourceFile);
            var file = Path.Combine(outputDirectory,
                                        string.Format(FORMAT_MASTER_FILENAME,
                                                      Path.GetFileNameWithoutExtension(sourceFile)));
            return Split(file, outputDirectory, wavesDocument);
        }

        private static IEnumerable<string> Split(string packListFile, string outputDir, XDocument wavesDocument)
        {
            var fileList = new List<string> {packListFile};

            var root = new XElement(wavesDocument.Root.Name);
            var doc = new XDocument(root);
            bool shouldSave;
            foreach (var pack in wavesDocument.Root.Descendants(PACK))
            {
                var packFileName = string.Format(FORMAT_PACK_FILENAME, pack.Attribute(NAME).Value.ToUpper());
                var packFilePath = Path.Combine(outputDir, packFileName);

                shouldSave = true;
                if (File.Exists(packFilePath) && ((File.GetAttributes(packFilePath) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly))
                {
                    shouldSave = false;
                }

                if (shouldSave)
                {
                    fileList.Add(packFilePath);

                    var packDoc = new XDocument(pack);
                    packDoc.Save(packFilePath);
                }

                root.Add(new XElement(PACK_FILE, packFileName));
            }

            shouldSave = true;
            if (File.Exists(packListFile) && ((File.GetAttributes(packListFile) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly))
            {
                shouldSave = false;
            }

            if (shouldSave)
            {
                doc.Save(packListFile);
            }

            return fileList;
        }

        private static XDocument LoadWavesFile(string fileName)
        {
            var readerSettings = new XmlReaderSettings
                                     {
                                         IgnoreComments = true,
                                         IgnoreWhitespace = true,
                                         IgnoreProcessingInstructions = true,
                                         CloseInput = true
                                     };
            using (var reader = XmlReader.Create(File.OpenRead(fileName), readerSettings))
            {
                return XDocument.Load(reader);
            }
        }
    }
}