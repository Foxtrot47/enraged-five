using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace rage.ToolLib.WavesFiles
{
    public class WavesFileCombiner : IWavesFileCombiner
    {
        private const string PACK = "Pack";
        private const string PACK_FILE = "PackFile";
        private const string XML_EXTENSION = ".xml";

        #region IWavesFileCombiner Members

        /**
         * Combine a list of XML pack files into a single XML file
         * and save to a specified destination directory.
         */
        public string Run(string packListFile, string destinationDirectory, string fileMask)
        {
            if (!File.Exists(packListFile))
            {
                throw new ApplicationException(string.Format("Pack list file: {0} does not exist", packListFile));
            }

            return Combine(packListFile, destinationDirectory, fileMask);
        }

        /**
         * Combine a list of XML pack files in memory.
         */
        public XDocument Run(string packListFile, string fileMask)
        {
            if (!File.Exists(packListFile))
            {
                throw new ApplicationException(string.Format("Pack list file: {0} does not exist", packListFile));
            }

            return Combine(packListFile, fileMask);
        }

        #endregion

        private static string Combine(string packListFile, string destinationDirectory, string fileMask)
        {
            if (!File.Exists(packListFile))
            {
                throw new ApplicationException(string.Format("Pack List file: {0} does not exist", packListFile));
            }

            if (!Directory.Exists(destinationDirectory))
            {
                Directory.CreateDirectory(destinationDirectory);
            }

            var packListDoc = XDocument.Load(packListFile);
            var rootElementName = packListDoc.Root.Name;
            var destinationDoc = Combine(packListFile, fileMask);
            var outputFile = Path.Combine(destinationDirectory, string.Concat(rootElementName, XML_EXTENSION));
            destinationDoc.Save(outputFile);
            return outputFile;
        }

        private static XDocument Combine(string packListFile, string fileMask)
        {
            var packListDirectory = Path.GetDirectoryName(packListFile);
            var packListDoc = XDocument.Load(packListFile);

            // get pack files
            var packFiles =
                from pack in packListDoc.Root.Descendants(PACK_FILE)
                select Path.Combine(packListDirectory, pack.Value);

            // filter by any masks specified to only load required files
            if (!string.IsNullOrEmpty(fileMask))
            {
                packFiles = packFiles.Where(packFile => Utility.FilenameMatchesMask(Path.GetFileName(packFile), fileMask));
            }

            var rootElementName = packListDoc.Root.Name;
            var root = new XElement(rootElementName);

            foreach (var packFile in packFiles)
            {
                var doc = LoadWavesFile(packFile);
                if (doc.Root != null && doc.Root.Name == PACK)
                {
                    root.Add(doc.Root);
                }
            }

            return new XDocument(root);
        }

        private static XDocument LoadWavesFile(string fileName)
        {
            var readerSettings = new XmlReaderSettings
                                     {
                                         IgnoreComments = true,
                                         IgnoreWhitespace = true,
                                         IgnoreProcessingInstructions = true,
                                         CloseInput = true
                                     };
            using (var reader = XmlReader.Create(File.OpenRead(fileName), readerSettings))
            {
                return XDocument.Load(reader);
            }
        }
    }
}