﻿using System;
using System.Collections.Generic;
using System.Text;
//Based on rage\base\src\math\float16.h

namespace rage.ToolLib
{
    public class Float16
    {   
        public Float16(ushort u) { m_data = u; }
        public static implicit operator Float16(float f) 
        { 
            return new Float16(ConvertFloatToF16Data(f)); 
        }

        public static ushort ConvertFloatToF16Data(float f)
        {
            uint i, e, l, o;

            i = BitConverter.ToUInt32(BitConverter.GetBytes(f), 0); // [LHS]
            e = 0x7fffffffu & i;
            e = 0x38000000u - e;
            l = (uint)((int)(0x00000000u + e) >> 31); // underflow
            o = (uint)((int)(0x0fffffffu + e) >> 31); // overflow
            e = (uint)(-(int)e) >> 13;
            e = e | o;
            e = e & ((0x00007fffu & l));
            e = e | ((0x80000000u & i) >> 16);

            return (ushort)e;
        }

        public ushort GetU16() { return m_data; }
        ushort m_data;
    }
}
