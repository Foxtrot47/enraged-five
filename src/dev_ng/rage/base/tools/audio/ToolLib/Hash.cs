// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Hash.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The hash.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace rage.ToolLib
{
    using System.Text;

    /// <summary>
    /// The hash.
    /// </summary>
    public class Hash
    {
        #region Fields

        /// <summary>
        /// The m_value.
        /// </summary>
        private string m_value;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the key.
        /// </summary>
        public uint Key { get; private set; }

        /// <summary>
        /// Gets the partial key.
        /// </summary>
        public uint PartialKey { get; private set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this.m_value;
            }

            set
            {
                this.Key = 0;
                if (!string.IsNullOrEmpty(value))
                {
                    this.m_value = value;
                    this.GenerateHash();
                }
                else
                {
                    this.m_value = string.Empty;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The generate hash.
        /// </summary>
        private void GenerateHash()
        {
            // This is the one-at-a-time hash from this page:
            // http://burtleburtle.net/bob/hash/doobs.html
            // (borrowed from /soft/swat/src/swcore/string2key.cpp)
            var str = Encoding.UTF8.GetBytes(this.Value.Replace("\"", string.Empty).Replace("\\", "/").ToLower());
            var length = str.Length;
            for (var index = 0; index < length; ++index)
            {
                this.Key += str[index];
                this.Key += this.Key << 10; // lint !e701
                this.Key ^= this.Key >> 6; // lint !e702
            }

            // Set partial key at this point
            this.PartialKey = this.Key;

            // Continue to generate full key
            this.Key += this.Key << 3; // lint !e701
            this.Key ^= this.Key >> 11; // lint !e702
            this.Key += this.Key << 15; // lint !e701

            // The original swat code did several tests at this point to make
            // sure that the resulting value was representable as a valid
            // floating-point number (not a negative zero, or a NaN or Inf)
            // and also reserved a nonzero value to indicate a bad key.
            // We don't do this here for generality but the caller could
            // obviously add such checks again in higher-level code.
        }

        #endregion
    }
}