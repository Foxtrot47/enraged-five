using namespace System;

namespace audAssetManagement2
{
public __gc struct lockedStatus
{
public:
	bool IsOpen;
	bool IsLocked;
	bool IsLockedByMe;
	String *Owner;

 lockedStatus(bool isOpen, bool isLocked, bool isLockedByMe, String *owner)
	{
		IsOpen = isOpen;
		IsLocked = isLocked;
		IsLockedByMe = isLockedByMe;
		Owner = owner;
	}

};
}