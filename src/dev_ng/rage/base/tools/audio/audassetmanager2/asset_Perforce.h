#ifndef ASSET_PERFORCE_H
#define ASSET_PERFORCE_H

#include "asset.h"
#include "customclientuser_perforce.h"
#include "changelist.h"
#include "clientWrapper.h"

using namespace System;

namespace audAssetManagement2
{
	public __gc class assetPerforce : public asset
	{	

	public:
		assetPerforce(clientWrapper *client, CustomClientUser *clientUser, changelist *changelist, String *assetPath);
		~assetPerforce();
		String *Revert();
		String *GetDepotPath();
		String *GetLocalPath();
		changelist *GetChangeList();

	public private:
		void SetChangelist(changelist *c);

	private:
		clientWrapper *m_Client;
		CustomClientUser *m_ClientUser;
		changelist *m_Changelist;
		String *m_ClientPath;
		String *m_DepotPath;
	};
}

#endif //ASSET_PERFORCE_H
