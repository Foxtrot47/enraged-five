#include "stdafx.h"

#include "assetManager_Local.h"
#include "changelist_Local.h"
#include "asset_local.h"
#include <strsafe.h>

using namespace audAssetManagement2;
using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Collections;
using namespace System::IO;

assetManagerLocal::assetManagerLocal()
{
	return;
}

assetManagerLocal::~assetManagerLocal()
{
	return;
}

String *assetManagerLocal::Init(String *, String *, String *, String *, String *)
{
	m_Changelists = new Hashtable();
	m_Changelists->Add(__gc new String("default"),__gc new changelistLocal(this,"default",""));
	m_CurrentNo = 0;
	return "";
}

String *assetManagerLocal::Reconnect()
{	
	return"";
}

void assetManagerLocal::ShutDown()
{
	return;
}

void assetManagerLocal::RefreshView()
{
	return;
}

changelist *assetManagerLocal::CreateNewChangelist(String * /*description*/)
{	
	String *changelistNum = (m_CurrentNo++).ToString();
	changelistLocal *local = __gc new changelistLocal(this,changelistNum,"");
	m_Changelists->Add(changelistNum, local);
	return local;
}

void assetManagerLocal::RemoveChangelist(changelist *changelist)
{
	m_Changelists->Remove(changelist->GetNumber());
}

ArrayList *assetManagerLocal::GetChangelists()
{
	ArrayList *changelists = __gc new ArrayList();

	IDictionaryEnumerator *changelistEnum = m_Changelists->GetEnumerator();
	while(changelistEnum->MoveNext())
	{
		DictionaryEntry *de = __try_cast<DictionaryEntry*>(changelistEnum->Current);
		changelists->Add(__try_cast<changelist*>(de->Value));
	}

	return changelists;
}

changelist *assetManagerLocal::GetChangelist(String *changeNo)
{
	if(m_Changelists->ContainsKey(changeNo->ToLower()))
	{
		return __try_cast<changelist*>(m_Changelists->get_Item(changeNo));
	}

	return NULL;

}

//NB this function can take either local or depot path
String *assetManagerLocal::MoveAsset(changelist *oldChangelist, changelist *newChangelist, asset *asset)
{	
	oldChangelist->RemoveAsset(asset);
	newChangelist->AddAsset(asset);
	return "";
}

String *assetManagerLocal::GetLatest(String * /*path*/, bool)
{
	return "";
}

String *assetManagerLocal::GetLatest(String * /*path*/, bool, String* /*changelistNumber*/)
{
	return "";
}

String *assetManagerLocal::GetLatestOnFileType(String * /*path*/, String* /*type*/, bool /*force*/)
{
	return "";
}

//NB this function can take either local or depot path
String *assetManagerLocal::GetPrevious(String * /*path*/, bool)
{
	return "";
}

//NB this function can take either local or depot path
bool assetManagerLocal::ExistsAsAsset(String *path)
{
	return File::Exists(path);		
}

String *assetManagerLocal::GetWorkingPath(String *path)
{
	if(!path->Contains(":"))
	{
		path = String::Concat("d:\\",path);	
	}
	path = path->Replace( "\\\\", "\\" );	// remove doubles
	return path;
}

bool assetManagerLocal::IsCheckedOut(String *path)
{
	path = path->Replace("\\","/");
	if(File::GetAttributes(path)  & FileAttributes::ReadOnly)
	{
		return false;
	}
	return true;
}

bool assetManagerLocal::IsMarkedForAdd(String *)
{
	return true;
}

lockedStatus *assetManagerLocal::GetFileLockInfo(String *path)
{
	path = path->Replace("\\","/");
	if(File::GetAttributes(path)  & FileAttributes::ReadOnly)
	{
		return __gc new lockedStatus(false,true,true,"");
	}

	return __gc new lockedStatus(false,false,false,"");	
}

bool assetManagerLocal::IsResolved(System::String __gc *)
{
	return true;
}

bool assetManagerLocal::HaveLatest(String*)
{
	return true;
}

String* assetManagerLocal::GetLatestChangelistNumber()
{
	return String::Empty;
}


