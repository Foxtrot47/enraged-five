#include "stdafx.h"
#include "changelist_Local.h"
#include "asset_Local.h"
#include <strsafe.h>

using namespace audAssetManagement2;
using namespace System::Runtime::InteropServices;
using namespace System::Collections;
using namespace System::IO;
using namespace System::Text;

changelistLocal::changelistLocal(assetManager * /*assetManager*/, String *change, String *desc)
{
	m_Assets = __gc new ArrayList();
	m_Change = change;
	m_Desc = desc;
}

changelistLocal::~changelistLocal()
{
}

String *changelistLocal::GetDescription()
{
	return m_Desc;
}

void changelistLocal::SetDescription(String *description)
{
	m_Desc = description;
}

String *changelistLocal::GetNumber()
{
	return m_Change;
}

String *changelistLocal::GetAllFiles()
{
	StringBuilder* output = new StringBuilder();

	IEnumerator *iter = m_Assets->GetEnumerator();
	while(iter->MoveNext())
	{
		asset *a = __try_cast<asset*>(iter->Current);
		output->AppendLine(a->GetDepotPath());
	}

	return output->ToString();
}

String *changelistLocal::Submit(String * /*description*/)
{
	IEnumerator *assetsEnum = m_Assets->GetEnumerator();
	while(assetsEnum->MoveNext())
	{
		asset *a = __try_cast<asset*>(assetsEnum->Current);
		String *aPath = a->GetLocalPath();
		//check file was not marked for delete
		if(File::Exists(aPath))
		{
			File::SetAttributes(aPath,FileAttributes::ReadOnly);
		}
	}
	m_Assets->Clear();
	m_AssetManager->RemoveChangelist(this);
	return "";
}

String *changelistLocal::Submit()
{
	return Submit(GetDescription());
}

String *changelistLocal::Revert(bool /*bDelete*/)
{
	IEnumerator *assetsEnum = m_Assets->GetEnumerator();
	while(assetsEnum->MoveNext())
	{
		asset *a = __try_cast<asset*>(assetsEnum->Current);
		String *aPath = a->GetLocalPath();
		//check file was not marked for delete
		if(File::Exists(aPath))
		{
			File::SetAttributes(aPath,FileAttributes::ReadOnly);
		}
	}
	m_Assets->Clear();
	m_AssetManager->RemoveChangelist(this);
	return "";
}

String *changelistLocal::RevertUnchanged()
{
	//	Should we actually do anything??
	return "";
}



asset *changelistLocal::CheckoutAsset(String *path, bool /*lock*/)
{
	asset *a = __gc new assetLocal(this,path);
	m_Assets->Add(a);
	return a;
}

ArrayList *changelistLocal::GetAssets()
{
	return m_Assets;
}

asset *changelistLocal::GetAsset(String *path)
{
	IEnumerator *assetEnum = m_Assets->GetEnumerator();
	while(assetEnum->MoveNext())
	{
		asset *a = __try_cast<asset *>(assetEnum->Current);
		if(a->GetDepotPath()->ToUpper()->Equals(path->ToUpper()) ||
			a->GetLocalPath()->ToUpper()->Equals(path->ToUpper()))
		{
			return a;
		}
	}
	return NULL;
}

void changelistLocal::RemoveAsset(asset *a)
{
	m_Assets->Remove(a);
}

void changelistLocal::AddAsset(asset *asset)
{
	m_Assets->Add(asset);
}

String *changelistLocal::Delete()
{
	m_Assets->Clear();
	m_AssetManager->RemoveChangelist(this);
	return "";
}

asset *changelistLocal::MarkAssetForDelete(String *path)
{
	asset *a = __gc new assetLocal(this,path);
	m_Assets->Add(a);
	return a;
}

asset *changelistLocal::MarkAssetForAdd(String *path)
{
	return changelistLocal::MarkAssetForAdd(path,"");
}

asset *changelistLocal::MarkAssetForAdd(String *path, String * /*type*/)
{
	asset *a = __gc new assetLocal(this,path);
	m_Assets->Add(a);
	return a;
}


