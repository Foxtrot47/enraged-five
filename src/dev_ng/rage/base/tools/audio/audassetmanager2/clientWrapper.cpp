#include "clientWrapper.h"

namespace audAssetManagement2 
{

	clientWrapper::clientWrapper(CustomClientUser *clientUser)
	{
		m_Client = new ClientApi();
		m_ClientUser = clientUser;
	}

	clientWrapper::~clientWrapper()
	{
		delete m_Client;
	}

	void clientWrapper::SetProtocol( const char *p, const char *v )
	{
		m_Client->SetProtocol(p,v);
	}

	void clientWrapper::Init( Error *e )
	{
		m_Client->Init(e);
	}

	void clientWrapper::SetClient( const char *c )
	{
		m_Client->SetClient(c);
	}

	void clientWrapper::SetPort( const char *c )
	{
		m_Client->SetPort(c);
	}

	void clientWrapper::SetUser( const char *c )
	{
		m_Client->SetUser(c);
	}

	void clientWrapper::SetArgv( int argc, char *const *argv )
	{
		m_Client->SetArgv(argc, argv);
	}

	bool clientWrapper::Run(const char *func, ClientUser *ui)
	{
		m_Client->Run(func,ui);
		
		//Connection has dropped
		if(m_Client->Dropped())
		{
			Error error;
			Final(&error);
	
			error.Clear();
			Init(&error);
			if(error.Test())
			{
				return false;
			}

			Run(func,ui);	
		}

		return true;
	}

	int clientWrapper::Final( Error *e )
	{
		return m_Client->Final(e);
	}
	
}