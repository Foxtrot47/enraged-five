#include "stdafx.h"

#include "assetManager.h"
#include "assetManager_Perforce.h"

using namespace audAssetManagement2;
using namespace System::IO;

assetManager *assetManager::CreateInstance(audAssetManagerTypes /*type*/)
{
	assetManager __gc *assetManager = NULL;

	assetManager =  new assetManagerPerforce();

	return assetManager;
}

bool assetManager::IsReadOnly(String *localPath)
{
	FileInfo *file = __gc new FileInfo(localPath);
	return file->IsReadOnly;
}

void assetManager::MakeReadOnly(String *localPath)
{
	FileInfo *file = __gc new FileInfo(localPath);
	file->IsReadOnly = true;
}

void assetManager::MakeWriteable(String *localPath)
{
	FileInfo *file = __gc new FileInfo(localPath);
	file->IsReadOnly = false;
}

