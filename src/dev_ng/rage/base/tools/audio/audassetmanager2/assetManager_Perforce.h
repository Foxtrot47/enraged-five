#ifndef ASSET_MANAGER_PERFORCE_H
#define ASSET_MANAGER_PERFORCE_H

#include "assetManager.h"
#include "customclientuser_perforce.h"
#include "clientWrapper.h"

using namespace System;
using namespace System::Collections;

namespace audAssetManagement2
{
	public __gc class assetManagerPerforce : public assetManager
	{

	public:
		assetManagerPerforce();
		~assetManagerPerforce();
		String *Init(String *serverName, String *projectName, String *userName, String *passwordStr, String *depotRoot);
		String *Reconnect();
		void ShutDown();
		ArrayList *GetChangelists();
		changelist *CreateNewChangelist(String *comment);
		void RemoveChangelist(changelist *changelist);
		changelist *GetChangelist(String *changeNo);
		String *MoveAsset(changelist *oldChangelist, changelist *newChangelist, asset *asset);
		String *GetLatest(String *path, bool force);
		String *GetLatest(String *path, bool force, String* changelistNumber);
		String *GetLatestOnFileType(String *path, String* type, bool force);
		String *GetPrevious(String *path, bool force);
		String *Integrate(String *szFrom, String *szTo, changelist *pChangeList);
		String *Resolve(String *path, enResolveType resolveType);
		bool ExistsAsAsset(String *path);
		String *GetWorkingPath(String *path);
		bool IsCheckedOut(String *path);
		bool IsMarkedForAdd(String *path);
		lockedStatus *GetFileLockInfo(String *path);
		void RefreshView();
		bool IsResolved(String *path);
		bool HaveLatest(String* path);
		String *GetLatestChangelistNumber();
		String *SafeCheckPath(String *path);

	private:
		String *CreateChangelists();
		String *GetDepotPath(String *path);

		//Perforce API
		CustomClientUser *m_ClientUser;
		clientWrapper *m_Client;

		//Perforce connection settings
		String *m_HostName;
		String *m_ClientName;
		String *m_UserName;
		String *m_Password;
		String *m_DepotRoot;
		String *m_Root;
		String *m_fixedRoot;

		//changelist Hashtable
		Hashtable* m_Changelists;

	};

}

#endif //ASSET_MANAGER_PERFORCE_H
