#include "stdafx.h"
#include "asset_Perforce.h"

using namespace audAssetManagement2;
using namespace System;
using namespace System::IO;
using namespace System::Runtime::InteropServices;

assetPerforce::assetPerforce(clientWrapper *client, CustomClientUser *clientUser, changelist *changelist, String *assetPath)
{
	m_Client = client;
	m_ClientUser = clientUser;
	m_Changelist = changelist;

	//to allow us to use either depot or local path to create an asset
	//we will now run fstat and get the info we need

	char *assetPathPtr = (char *)(void *)Marshal::StringToHGlobalAnsi(assetPath);

	if(assetPathPtr)
	{
		m_ClientUser->SetCommand(p4Fstat);
		//set command arguments and run command
		m_Client->SetArgv(1, &assetPathPtr);
		if(!m_Client->Run("fstat", (ClientUser *)m_ClientUser))
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception("Perforce connection failure");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception(String::Concat("Error creating asset object: ",m_ClientUser->GetErrorMessage().c_str()));
		}

		m_DepotPath = __gc new String(m_ClientUser->GetFileInfo().depotFile);
		m_ClientPath = __gc new String(m_ClientUser->GetFileInfo().clientFile);
	}
	else
	{
		Marshal::FreeHGlobal(assetPathPtr);
		throw new Exception(String::Concat("Error creating asset object: ","Failed to get asset path"));
	}
	
	Marshal::FreeHGlobal(assetPathPtr);
}

assetPerforce::~assetPerforce()
{

}

String *assetPerforce::Revert()
{
	char *assetPathPtr = (char *)(void *)Marshal::StringToHGlobalAnsi(m_DepotPath);

	if(assetPathPtr)
	{
		//clear reults and set command used to process output
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand(p4Revert);
		//set command arguements and run command
		m_Client->SetArgv(1, &assetPathPtr);
		if(!m_Client->Run("revert", (ClientUser *)m_ClientUser))
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception("Perforce connection failure");
		}

		Marshal::FreeHGlobal(assetPathPtr);

		if(m_ClientUser->HasErrorOccurred())
		{
			throw new Exception(String::Concat("Failed to revert asset: ",m_ClientUser->GetErrorMessage().c_str()));
		}
		m_Changelist->RemoveAsset(this);
		return(m_ClientUser->GetInfoMessage().c_str());
	}
	else
	{
		throw new Exception("Failed to get asset path");
	}
}


String *assetPerforce::GetDepotPath()
{
	return m_DepotPath;
}

String *assetPerforce::GetLocalPath()
{
	return m_ClientPath;
}

changelist *assetPerforce::GetChangeList()
{
	return m_Changelist;
}

void assetPerforce::SetChangelist(changelist *c)
{
	m_Changelist = c;
}


