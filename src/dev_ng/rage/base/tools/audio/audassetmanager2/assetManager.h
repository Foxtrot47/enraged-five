// AssetManager2.h


#ifndef ASSET_MANAGER_H
#define ASSET_MANAGER_H

#include "changeList.h"
#include "assetmanagertypes.h"
#include "lockedStatus.h"

using namespace System;

namespace audAssetManagement2 {

	public __value enum enResolveType
	{
		kAcceptYours,
		kAcceptThiers,
		kAcceptMerged,
		kAcceptAuto
	};


	public __delegate void OnViewUpdatedDelegate();

	public __gc class assetManager abstract
	{

	public:
		__event OnViewUpdatedDelegate* OnViewUpdated;
		
		//PURPOSE
		//Returns an instance of assetManager depending on the assetManager type supplied
		static assetManager *CreateInstance(audAssetManagerTypes type);
		//PURPOSE
		//Return true if file is readonly
		static bool IsReadOnly(String *localPath);
		//PURPOSE
		//Set readonly file attribute
		static void MakeReadOnly(String *localPath);
		//PURPOSE
		//Removes readonly file attribute
		static void MakeWriteable(String *localPath);
		//PURPOSE
		//Initialises the manager by logging in to the source control system
		virtual String *Init(String *serverName, String *projectName, String *userName, String *passwordStr, String *depotRoot)=0;
		//PURPOSE
		//Reconnect to the source control
		virtual String *Reconnect()=0;
		//PURPOSE
		//Close connection to source control
		virtual void ShutDown()=0;
		//PURPOSE 
		//Refresh changelist View
		virtual void RefreshView()=0;
		//PURPOSE
		//return a list of all changelists
		virtual ArrayList *GetChangelists()=0;
		//PURPOSE
		//Creates a new numbered changelist
		virtual changelist *CreateNewChangelist(String *description)=0;
		//PUPROSE
		//Remove changelist from hadhtable
		virtual void RemoveChangelist(changelist *changelist)=0;
		//PURPOSE
		//Returns the changelist that corresponds to the supplied number
		virtual changelist *GetChangelist(String *changeNo)=0;
		//PURPOSE
		//Moves asset from one changelist to another
		virtual String *MoveAsset(changelist *oldChangelist, changelist *newChangelist, asset *asset)=0;
		//PURPOSE
		//Retrieves the latest revision of the specified file from the source control
		virtual String *GetLatest(String *path, bool force)=0;
		// PURPOSE
		// Retrieves the revision of the specified path at a specific changelist number
		virtual String *GetLatest(String *path, bool force, String* changelistNumber)=0;
		//Retrieves the latest revision of the specified file from the source control
		virtual String *GetLatestOnFileType(String *path, String* type, bool force)=0;
		//PURPOSE
		//Retrieves the previous revision of the current revision of the file
		virtual String *GetPrevious(String *path, bool force)=0;
		virtual String *Integrate(String *szFrom, String *szTo, changelist *pChangeList)=0;
		virtual String *Resolve(String *path, enResolveType resolveType)=0;
		//PURPOSE
		//Given a filepath, the function determines if it exists as an asset in the source control
		virtual bool ExistsAsAsset(String *path)=0;
		//PURPOSE
		//Get Working Path
		virtual String *GetWorkingPath(String *path)=0;
		//PURPOSE
		//checks if given asset is checked out
		virtual bool IsCheckedOut(String *path)=0;
		//PURPOSE
		//checks if given asset is marked to be added
		virtual bool IsMarkedForAdd(String *path)=0;
		//PURPOSE
		//Returns locked info
		virtual lockedStatus *GetFileLockInfo(String *path)=0;
		//PURPOSE
		//Checks if file requires a resolve
		virtual bool IsResolved(String* path)=0;
		// PURPOSE
		// Checks if the client has the latest version of the specified file(s)
		virtual bool HaveLatest(String* path)=0;
		// PURPOSE
		// Gets the latest changelist number from the server
		virtual String* GetLatestChangelistNumber()=0;
	protected:
		virtual void ViewUpdated()
		{
			if(OnViewUpdated != 0)
			{
				OnViewUpdated();
			}
		}
	
	};
}

#endif //ASSET_MANAGER_H
