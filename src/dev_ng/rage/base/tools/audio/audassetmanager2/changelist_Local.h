
#ifndef CHANGELIST_PERFORCE_H
#define CHANGELIST_PERFORCE_H

#include "changelist.h"
#include "asset_Local.h"
#include "assetManager.h"

using namespace System;

namespace audAssetManagement2 
{
	public __gc class changelistLocal: public changelist 
	{
	public:
		changelistLocal(assetManager *parent, String *change, String *desc);
		~changelistLocal();
		String *GetDescription();
		void SetDescription(String *description);
		String* GetNumber();
		String *Submit(String *description);	
		String *Submit();	
		String *Revert(bool bDelete);
		String *RevertUnchanged();
		String *Delete();
		asset *CheckoutAsset(String *asset, bool lock);		
		asset *MarkAssetForAdd(String *asset, String *type);
		asset *MarkAssetForAdd(String *asset);
		asset *MarkAssetForDelete(String *asset);
		ArrayList *GetAssets();
		asset *GetAsset(String *path);

	public private:
		void RemoveAsset(asset *asset);
		void AddAsset(asset *asset);

	private:
		ArrayList *m_Changlists;
		String *GetAllFiles();
		String *m_Change;
		String *m_Desc;
		assetManager *m_AssetManager;
		ArrayList *m_Assets;
	};

}

#endif 
