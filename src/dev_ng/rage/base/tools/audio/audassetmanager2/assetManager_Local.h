#ifndef ASSET_MANAGER_LOCAL_H
#define ASSET_MANAGER_LOCAL_H

#include "assetManager.h"

using namespace System;
using namespace System::Collections;

namespace audAssetManagement2
{
	public __gc class assetManagerLocal : public assetManager
	{

	public:
		 assetManagerLocal();
		 ~assetManagerLocal();
		String *Init(String *serverName, String *projectName, String *userName, String *passwordStr, String *depotRoot);
		String *Reconnect();
		void ShutDown();
		void RefreshView();
		ArrayList *GetChangelists();
		changelist *CreateNewChangelist(String *description);
		void RemoveChangelist(changelist *changelist);
		changelist *GetChangelist(String *changeNo);
		
		String *MoveAsset(changelist *oldChangelist, changelist *newChangelist, asset *asset);		
		
		String *Integrate(String * /*szFrom*/, String * /*szTo*/, changelist * /*pChangeList*/) { throw new NotImplementedException(); }
		String *Resolve(String * /*path*/, enResolveType /*resolveType*/) { throw new NotImplementedException(); }

		String *GetLatest(String *path, bool force);
		String *GetLatest(String *path, bool force, String* changelistNumber);
		String *GetLatestOnFileType(String *path, String* type, bool force);
		String *GetPrevious(String *path, bool force);
		bool ExistsAsAsset(String *path);
		String *GetWorkingPath(String *path);
		bool IsCheckedOut(String *path);
		bool IsMarkedForAdd(String *path);
		lockedStatus *GetFileLockInfo(String *path);
		bool IsResolved(String* path);
		bool HaveLatest(String* path);
		String* GetLatestChangelistNumber();

	private:
		//changelist Hashtable
		Hashtable* m_Changelists;
		int m_CurrentNo;
	};

}

#endif //ASSET_MANAGER_PERFORCE_H
