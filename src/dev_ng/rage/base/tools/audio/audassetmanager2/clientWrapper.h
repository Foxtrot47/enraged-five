#ifndef CLIENTWRAPPER_H
#define CLIENTWRAPPER_H

#include "customclientuser_perforce.h"

namespace audAssetManagement2 
{
	public class clientWrapper
	{
		public:
			clientWrapper(CustomClientUser *clientUser);
			~clientWrapper();
			void SetProtocol( const char *p, const char *v );
			void Init( Error *e );

			void SetClient( const char *c );
			void SetPort( const char *c );
			void SetUser( const char *c );

			void SetArgv( int argc, char *const *argv );
			bool Run( const char *func, ClientUser *ui );
			int Final( Error *e );

		private:
			ClientApi *m_Client;
			CustomClientUser *m_ClientUser;
	};
}

#endif