//
// tools/audassetmanager/customclientuser_perforce.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//
#ifndef CUSTOM_CLIENT_USER_PERFORCE_H
#define CUSTOM_CLIENT_USER_PERFORCE_H

#include "p4api/clientapi.h"
#include <string>
#include <hash_map>
#include <iostream>
#include <xutility>

using namespace System;

//////////////////////////////////////////////////////////////////////////
// struct to cache fstat info from perforce
struct P4FileStatInfo
{
	P4FileStatInfo();

	void Reset();
	bool IsFileUpToDate() const;
	bool IsAsset() const;
	bool IsOpen() const;

	bool m_OurLock;
	bool IsResolved;

	enum { kMaxString=256 };

	char action[kMaxString];
	char actionOwner[kMaxString];
	char change[kMaxString];
	char clientFile[kMaxString];
	char depotFile[kMaxString];
	char haveRev[kMaxString];
	char headAction[kMaxString];
	char headRev[kMaxString];
};

struct P4ServerInfo
{
	P4ServerInfo();
	void Reset();

	enum { kMaxString=256 };

	char clientRoot[kMaxString];
	char serverAddress[kMaxString];

};

///////////////////////////////
// struct for changeLists
struct P4Changelist
{
	P4Changelist();
	enum
	{
		MaxNumString=20,
		MaxString=1000
	};
	//changeeList number
	char change[MaxNumString];
	//changeList description
	char desc[MaxString];

};

//////////////////////////////////////////////////////////////////////////
// bunch of perforce operations
enum P4Command 
{
	p4None = 0,
	p4Info,
	p4Sync,
	p4IsLocked,
	p4ExistsAsAsset,				//headAction
	p4ActionOwner,
	p4Edit,
	p4Revert,
	p4Submit,
	p4Add,
	p4Change,
	p4Dir,
	p4Lock,
	p4Delete,
	p4Fstat,
	p4Describe,
	p4Changes,
	p4ChangesNext,
	P4Reopen,
	p4Opened,
	p4OpenedNext,
	p4Where,
	p4Login,
	p4LatestChangelistNumber
};

// PURPOSE
//  A custom implementation of the Perforce client user for the Asset Manager.
// NOTES
//  Overrides the OutputStat method to enable parsing of data returned by the Perforce server.
//
class CustomClientUser : ClientUser
{
public:
	CustomClientUser();

	//PURPOSE
	//used to collect the changelist info after changes -s peding is called
	std::list<P4Changelist> GetChangelists();
	//PURPOSE
	//used to collect files after opened -c changelist# is called
	std::list<std::string> GetFilesInChangelist();
	//PURPOSE
	//Get string result of query
	std::string GetResult();
	//PURPOSE
	//returns the error message from the error caused by the last command
	std::string GetErrorMessage();
	//PURPOSE
	//returns the information message from the lat command
	std::string GetInfoMessage();
	//PURPOSE
	//Used to set the input parameters for submit and change commands
	//This is only needed if the -i argument is set
	void SetChangelistInputParams(char *changelistNumber, char *comment, char *files);
	//PURPOSE
	//Clears Error messages, info messages, changelist info and file info
	void ClearResults();
	// PURPOSE
	//  sets the current command to process, after invoking m_Client->Run()
	void SetCommand(P4Command eCommand);
	// PURPOSE
	//  Returns true if an error occurred during the processing of the last Perforce server command.
	// RETURNS
	//	True if an error occurred during the processing of the last Perforce server command.
	bool HasErrorOccurred();
	// PURPOSE
	//  init routines
	void SetClient(const char* clientName);
	void SetUser(const char* userName);
	void SetPassword(const char* password);
	// PURPOSE
	//  routines for gathering status on a particular file after running a p4FStat command
	const P4FileStatInfo & GetFileInfo() const	{ return m_FileInfo; }
	const P4ServerInfo & GetServerInfo() const	{ return m_ServerInfo; }


protected:
	enum 
	{ 
		g_MaxTagNameLength = 100,
		g_MaxValueStringLength = 100,
		g_MaxNameStringLength = 100,
		g_MaxComment = 1024,
		g_MaxFiles = 1024,
	};

	// PURPOSE
	//  Overrides the base class implementation to support extraction of the variable associated with the name set by a
	//	previous call to SetTagNameToExtract. Called automatically by the Perforce server in response to the running of a
	//	server command.
	// PARAMS
	//	varList	- A Perforce API StrDict object containing a list of tagged variables returned by the last command.
	virtual void OutputStat(StrDict *varList);

	// PURPOSE
	//  Overrides the base class implementation to inhibit printing to stdout and to flag that no error has occurred.
	virtual void OutputBinary(const char *data, int length);
	virtual void OutputInfo(char level, const char *data);
	virtual void OutputText(const char *data, int length);
	virtual void OutputError(const char *error);

	// PURPOSE
	//  Overrides for error cases and status information
	void Prompt( const StrPtr &msg, StrBuf &rsp, int noEcho, Error *e);
	void InputData( StrBuf* pMyData, Error* pError );
	void ErrorPause(char *errBuf, Error *e);
	void HandleError( Error *err );
	void Message( Error *err );


	//////////////////////////////////////////////////////////////////////////
	// data members
	P4Command m_Command;

	bool m_HasErrorOccurred;

	// result storage
	int  m_IntResult;
	char m_StringResult[g_MaxTagNameLength];
	char m_FindResult[g_MaxTagNameLength];

	// settings
	char m_ClientName[g_MaxNameStringLength];
	char m_UserName[g_MaxNameStringLength];
	char m_Password[g_MaxNameStringLength];

	// change lists
	char *m_ChangelistNumber;//[g_MaxValueStringLength];
	char *m_Comment;//[g_MaxComment];
	char *m_Files;//[g_MaxFiles];
	int m_InputLength;

	// struct to cache fstat info
	P4FileStatInfo m_FileInfo;

	//struct to cahce p4info ino
	P4ServerInfo m_ServerInfo; 

	// list of all pending change lists
	std::list<P4Changelist> m_Changelists;	
	// list of all files found in pending change lists
	std::list<std::string> m_ChangelistFiles;		

	std::string m_ErrorMessage;
	std::string m_InfoMessage;

};


#endif // CUSTOM_CLIENT_USER_PERFORCE_H
