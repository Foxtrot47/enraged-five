#include "stdafx.h"
#include "asset_Local.h"

using namespace audAssetManagement2;
using namespace System;
using namespace System::IO;
using namespace System::Runtime::InteropServices;

assetLocal::assetLocal(changelist *changelist, String *assetPath)
{
	m_Changelist=changelist;
	m_DepotPath = assetPath;
	m_ClientPath = assetPath;
}

assetLocal::~assetLocal()
{

}

String *assetLocal::Revert()
{
	File::SetAttributes(m_ClientPath,FileAttributes::ReadOnly);
	return"";
}

String *assetLocal::GetDepotPath()
{
	return m_DepotPath;
}

String *assetLocal::GetLocalPath()
{
	return m_ClientPath;
}

changelist *assetLocal::GetChangeList()
{
	return m_Changelist;
}

void assetLocal::SetChangelist(changelist *c)
{
	m_Changelist = c;
}


