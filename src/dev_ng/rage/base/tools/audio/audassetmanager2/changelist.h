
#ifndef CHANGELIST_H
#define CHANGELIST_H

#include "asset.h"

using namespace System;
using namespace System::Collections;


namespace audAssetManagement2
{

	public __delegate void OnChangelistUpdatedDelegate();

	public __gc class changelist abstract
	{

	public:
		virtual __event OnChangelistUpdatedDelegate* OnChangelistUpdated;
		//PURPOSE
		//return the changelist description/comment
		virtual String *GetDescription()=0;
		//PURPOSE
		//set the changelist description/comment
		virtual void SetDescription(String *description)=0;
		//PURPOSE
		//returns the changelist number
		virtual String *GetNumber()=0;
		// PURPOSE
		// Commits all files in the changelist
		virtual String *Submit(String* description)=0;
		virtual String *Submit()=0;
		//PURPOSE
		//Reverts all files in the changelist
		virtual String *Revert(bool bDelete)=0;
		//PUPOSE
		//Reverts only checked out files in the changelist that haven't been modified
		virtual String *RevertUnchanged() = 0;
		//PURPOSE
		//Deletes the changelist
		virtual String *Delete()=0;
		//PURPOSE
		//Adds an asset object to the list of asset objects managed in the changelist
		virtual asset *CheckoutAsset(String *asset, bool lock)=0;
		//PURPOSE
		//Adds an asset object to the list of asset objects managed in the changelist
		//marking it to be added to the source control
		virtual asset *MarkAssetForAdd(String *asset)=0;
		//PURPOSE
		//Adds an asset object to the list of asset objects managed in the changelist
		//marking it to be added to the source control
		virtual asset *MarkAssetForAdd(String *asset, String *type)=0;
		//PURPOSE
		//Adds an asset object to the list of asset objects managed in the changelist
		//marking it to be removed from the source control
		virtual asset *MarkAssetForDelete(String *asset)=0;
		//PURPOSE
		//Returns a list of all asset objects managed by the changelist
		virtual ArrayList *GetAssets()=0;
		//PUPROSE
		//Returns the asset object that corresponds to the supplied string
		virtual asset *GetAsset(String *path)=0;
	public private:
	    //PURPOSE
		//Remove an asset object from the list of asset objects
		virtual void RemoveAsset(asset *asset)=0;
		//PURPOSE
		//Add an asset to the list of asset objects
		virtual void AddAsset(asset *asset)=0;

	};
}

#endif //CHANGELIST_H
