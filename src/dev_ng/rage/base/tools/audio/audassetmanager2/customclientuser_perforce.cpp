//
// tools/audassetmanager/customclientuser_perforce.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "stdafx.h"

#include "customclientuser_perforce.h"

#define STRSAFE_LIB
#include <strsafe.h>
#include <string.h>
#include <ctype.h>

using namespace System;
using namespace System::Diagnostics;

///P4FileStatInfo
P4FileStatInfo::P4FileStatInfo()
{
	Reset();
}

void P4FileStatInfo::Reset()
{
	action[0] = '\0';
	actionOwner[0] = '\0';
	change[0] = '\0';
	clientFile[0] = '\0';
	depotFile[0] = '\0';
	haveRev[0] = '\0';
	headAction[0] = '\0';
	headRev[0] = '\0';
	
	m_OurLock = false;
	IsResolved = true;
}

P4ServerInfo::P4ServerInfo()
{
	Reset();
}

void P4ServerInfo::Reset()
{
	serverAddress[0] = '\0';
	clientRoot[0] = '\0';
}

bool P4FileStatInfo::IsFileUpToDate() const
{
	// file is up to date if head/have revs are same
	if (!strcmp(headRev,haveRev))
		return true;

	// file is up to date if we're adding it
	if (!_stricmp(action, "add"))
		return true;

	return false;
}

bool P4FileStatInfo::IsAsset() const
{
	// valid asset if the head action isn't 'delete' or something other than 'add'
	if (strlen(headAction))
	{
		if (0==_stricmp(headAction,"delete")) 
			return false;

		if ((0==_stricmp(headAction,"add"))		|| (0==_stricmp(headAction,"edit")) ||
			(0==_stricmp(headAction,"branch"))	|| (0==_stricmp(headAction,"integrate")))
			return true;
	}

	return false;
}

bool P4FileStatInfo::IsOpen() const
{
	if (strlen(action))
	{
		if ((0==_stricmp(action,"delete")) || (0==_stricmp(action,"add")) | (0==_stricmp(action,"edit")))
			return true;
	}

	return false;
}
///////////////////////////////////

///P4Changelist
P4Changelist::P4Changelist()
{
	change[0] = '\0';
	desc[0] = '\0';
}

//////////////////////////////////

///CustomClientUser
CustomClientUser::CustomClientUser()
{
	m_StringResult[0] = '\0';
	m_IntResult = 0;
	m_HasErrorOccurred = false;
	m_Command = p4None;
	m_ChangelistNumber=0;
	m_Comment = 0;
	m_Files=0;
	m_InputLength=0;

	// reset data structures
	m_Changelists.empty();
	m_ChangelistFiles.empty();

	::ClientUser();
}

void CustomClientUser::ClearResults()
{
	m_Changelists.clear();
	m_ChangelistFiles.clear();
	m_ErrorMessage.clear();
	m_InfoMessage.clear();
	m_StringResult[0] = '\0';
}

void CustomClientUser::OutputStat(StrDict *varList)
{
	StrPtr *varStrPtr;

	switch( m_Command )
	{	
	case p4Where:
		{
			varStrPtr = varList->GetVar( "clientRoot" );
			if( varStrPtr )
			{
				StringCbCopyN( m_StringResult, sizeof( m_StringResult ), varStrPtr->Text(), varStrPtr->Length() );
			}
		}
	case p4Info:
		{
			m_ServerInfo.Reset();

			varStrPtr = varList->GetVar( "clientRoot" );
			if( varStrPtr )
			{
				StringCbCopyN( m_ServerInfo.clientRoot, sizeof( m_ServerInfo.serverAddress ), varStrPtr->Text(), varStrPtr->Length() );
			}
			varStrPtr = varList->GetVar( "serverAddress" );
			if( varStrPtr )
			{
				StringCbCopyN( m_ServerInfo.serverAddress, sizeof( m_ServerInfo.serverAddress ), varStrPtr->Text(), varStrPtr->Length() );
			}
		}
		break;
	case p4IsLocked:
		{
			varStrPtr = varList->GetVar( "otherLock" );
			if(varStrPtr)
			{
				varStrPtr = varList->GetVar( "otherOpen" );
				if( varStrPtr )
				{
					// loop through otherOpen count to see who has it locked
					int sNumberOfTimesOpened = atol( varStrPtr->Text() );
					int sCount = 0;
					char szTageName[g_MaxTagNameLength];
					while( sCount < sNumberOfTimesOpened )
					{
						StringCbPrintfA( szTageName, sizeof(szTageName), "otherLock%d", sCount );
						varStrPtr = varList->GetVar( szTageName );

						if( varStrPtr )
						{
							char *pszClientName = varStrPtr->Text();
							if( pszClientName[0] != '\0' )
							{
								System::Diagnostics::Debug::Write(S"File Locked by : ");
								System::Diagnostics::Debug::WriteLine( pszClientName );
								StringCbCopyN( m_StringResult, sizeof( m_StringResult ), varStrPtr->Text(), varStrPtr->Length() );
							}
						}
						sCount++;
					}
				}
			}
		}
		break;
	case p4Fstat:
		{
			m_FileInfo.Reset();

			StrRef field;
			StrRef result;
			int idx = 0;

			while (varList->GetVar(idx, field, result))
			{
				const char *text = field.Text();

				// store off result of fstat
				if (!strcmp(text, "action"))
					StringCbCopyN(m_FileInfo.action, sizeof(m_FileInfo.action), result.Text(), result.Length());
				else if (!strcmp(text, "actionOwner"))
					StringCbCopyN(m_FileInfo.actionOwner, sizeof(m_FileInfo.actionOwner), result.Text(), result.Length());
				else if (!strcmp(text, "change"))
					StringCbCopyN(m_FileInfo.change, sizeof(m_FileInfo.change), result.Text(), result.Length());
				else if (!strcmp(text, "clientFile"))
				{
					StringCbCopyN(m_FileInfo.clientFile, sizeof(m_FileInfo.clientFile), result.Text(), result.Length());

					// need to fixup the client file to sync with the asset manager's path changes or the hash won't match
					for (unsigned int i=0; i<strlen(m_FileInfo.clientFile); ++i)
					{
						m_FileInfo.clientFile[i] = (char)toupper(m_FileInfo.clientFile[i]);
						if (m_FileInfo.clientFile[i] == '\\')
							m_FileInfo.clientFile[i] = '/';
					}
				}
				else if (!strcmp(text, "depotFile"))
					StringCbCopyN(m_FileInfo.depotFile, sizeof(m_FileInfo.depotFile), result.Text(), result.Length());
				else if (!strcmp(text, "haveRev"))
					StringCbCopyN(m_FileInfo.haveRev, sizeof(m_FileInfo.haveRev), result.Text(), result.Length());
				else if (!strcmp(text, "headAction"))
					StringCbCopyN(m_FileInfo.headAction, sizeof(m_FileInfo.headAction), result.Text(), result.Length());
				else if (!strcmp(text, "headRev"))
					StringCbCopyN(m_FileInfo.headRev, sizeof(m_FileInfo.headRev), result.Text(), result.Length());
				else if (!strcmp(text, "ourLock"))
					m_FileInfo.m_OurLock = true;
				else if (!strcmp(text, "unresolved"))
					m_FileInfo.IsResolved = false;

				++idx;
			}
		}
		break;
	case p4Opened:
		{
			ClearResults();
			m_ChangelistFiles.clear();
		}
	case p4OpenedNext:
		{
			varStrPtr = varList->GetVar( "depotFile" );
			if(varStrPtr)
			{
				m_ChangelistFiles.push_back(varStrPtr->Text());
			}
			m_Command = p4OpenedNext;
			break;
		}
	case p4Changes:
		{
			ClearResults();
		}
	case p4ChangesNext:
		{			
			P4Changelist changelist;

			varStrPtr = varList->GetVar( "change" );
			if(varStrPtr)
			{
				StringCbCopyN( changelist.change, sizeof( changelist.change ), varStrPtr->Text(), varStrPtr->Length() );
			}

			varStrPtr = varList->GetVar("desc");
			if(varStrPtr)
			{
				//ensure description isn't too long. Capped at 256
				int i;
				if(varStrPtr->Length() < P4Changelist::MaxString)
				{
					i=varStrPtr->Length();
				}
				else
				{
					i=P4Changelist::MaxString;
				}
				StringCbCopyN(changelist.desc, sizeof(changelist.desc), varStrPtr->Text(),i);
			}			
			
			m_Changelists.push_back(changelist);
			m_Command = p4ChangesNext;
		}
		break;
	case p4LatestChangelistNumber:
		{
			varStrPtr = varList->GetVar( "change" );
			if(varStrPtr)
			{
				StringCbCopyN( m_StringResult, sizeof( m_StringResult ), varStrPtr->Text(), varStrPtr->Length() );
			}
			break;
		}
	default:
		ClearResults();
	}
}

void CustomClientUser::OutputBinary(const char * /*data*/, int /*length*/)
{
	m_HasErrorOccurred = false;
}

void CustomClientUser::OutputInfo(char /*level*/, const char *data)
{
	m_InfoMessage = data;
	m_HasErrorOccurred = false;
}

void CustomClientUser::OutputText(const char *data, int /*length*/)
{
	m_InfoMessage = data;
	m_HasErrorOccurred = false;
}


void CustomClientUser::OutputError(const char *error)
{
	m_ErrorMessage = error;
	m_HasErrorOccurred = true;
}

void CustomClientUser::Prompt( const StrPtr & /*msg*/, StrBuf &rsp, int /*noEcho*/, Error * /*e*/ ) {
	rsp.Set( m_Password );
}

void CustomClientUser::InputData( StrBuf* pMyData, Error* /*pError*/ )
{
	switch(m_Command)
	{
		case p4Change:
		{
			int size = m_InputLength + 1 + 1024;
			char *szClientForm = new char[size];
			StringCbPrintfA( szClientForm, size, "Change: %s\nClient: %s\nUser: %s\nStatus: new\nDescription: %s\n",m_ChangelistNumber, m_ClientName, m_UserName, m_Comment );
			pMyData->Set( szClientForm );
			delete[] szClientForm;
		}
		break;
		//This is used to allow the changelist description to be changed on submitting
		case p4Submit:
		{
			int size = m_InputLength + 1 + 1024;
			char *szClientForm = new char[size];
			StringCbPrintfA( szClientForm, size, "Change: %s\nClient: %s\nUser: %s\nStatus: pending\nDescription: %s\nFiles: %s\n",m_ChangelistNumber, m_ClientName, m_UserName, m_Comment, m_Files );
			pMyData->Set( szClientForm );
			delete[] szClientForm;
		}
		break;
		case p4Login:
		{
			pMyData->Set( m_Password );
		}
		break;
	}
}

void CustomClientUser::ErrorPause(char *errBuf, Error * /*e*/ )
{
	m_ErrorMessage = errBuf;
	m_HasErrorOccurred = true;
}

void CustomClientUser::HandleError( Error *pError )
{
	StrBuf strBuffer;
	pError->Fmt( strBuffer, EF_NEWLINE );

	if( strstr( strBuffer.Text(), "up-to-date." ) )
	{
		OutputText( strBuffer.Text(), strBuffer.Length() );
		return;
	}
	OutputError( strBuffer.Text() );
	m_HasErrorOccurred = true;
}

void CustomClientUser::Message( Error *pError )
{
	if( pError->IsInfo() )
	{
		// Info
		StrBuf strBuffer;
		pError->Fmt( strBuffer, EF_PLAIN );
		OutputInfo( (char)pError->GetGeneric() + '0', strBuffer.Text() );
	}
	else
	{
		// warn, failed, fatal
		HandleError( pError );
	}
}

void CustomClientUser::SetClient( const char* clientName )
{
	StringCbCopyA(m_ClientName, g_MaxNameStringLength, clientName);
}

void CustomClientUser::SetUser( const char* userName )
{
	StringCbCopyA(m_UserName, g_MaxNameStringLength, userName);
}

void CustomClientUser::SetPassword( const char* password )
{
	StringCbCopyA(m_Password, g_MaxNameStringLength, password);
}

bool CustomClientUser::HasErrorOccurred()
{
	bool bTemp = m_HasErrorOccurred; 
	m_HasErrorOccurred = false; 
	return bTemp;
}

std::string CustomClientUser::GetErrorMessage()
{
	std::string temp = m_ErrorMessage;
	//clear message
	m_ErrorMessage = "";
	return temp;
}

std::string CustomClientUser::GetInfoMessage()
{
	std::string temp = m_InfoMessage;
	//clear message
	m_InfoMessage = "";
	return temp;
}

void CustomClientUser::SetCommand(P4Command eCommand)
{
	// remove any status from the last operation
	ClearResults();

	m_Command = eCommand;
}

std::list<P4Changelist> CustomClientUser::GetChangelists()
{
	return m_Changelists;
}

std::list<std::string> CustomClientUser::GetFilesInChangelist()
{
	return m_ChangelistFiles;
}


void CustomClientUser::SetChangelistInputParams(char *changelistNumber, char *comment, char *files)
{
	m_InputLength=0;

	//Note:
	//although the destination size is in bytes sizeof(char)=1
	//The size+1 is allow for the terminating null character

	size_t size = strlen(changelistNumber);
	if(m_ChangelistNumber)
	{
		delete[] m_ChangelistNumber;
		m_ChangelistNumber=0;
	}
	m_ChangelistNumber = new char[size+1];	
	StringCbCopyA(m_ChangelistNumber,size+1,changelistNumber);
	m_InputLength+=size;

	if(m_Comment)
	{
		delete[] m_Comment;
		m_Comment=0;
	}	
	size = strlen(comment);
	m_Comment = new char[size+1];
	StringCbCopyA(m_Comment,size+1,comment);
	m_InputLength+=size;


	if(m_Files)
	{
		delete[] m_Files;
		m_Files=0;
	}
	if(files)
	{
		size = strlen(files);
		m_Files = new char[size+1];
		StringCbCopyA(m_Files,size+1,files);
		m_InputLength+=size;
	}
	
}

std::string CustomClientUser::GetResult()
{
	return m_StringResult;
}


///////////////////////////////////////
