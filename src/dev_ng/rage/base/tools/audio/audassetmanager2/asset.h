
#ifndef ASSET_H
#define ASSET_H

using namespace System;

namespace audAssetManagement2
{
	public __gc class changelist;

	public __gc class asset abstract
	{
	public:
		//PUPOSE
		//Revert changes to asset
		virtual String *Revert()=0;
		//PURPOSE
		//Returns the depot Path
		virtual String *GetDepotPath()=0;
		//PURPOSE
		//Returns local file path
		virtual String *GetLocalPath()=0;	
		//PURPOSE
		//Get Changelist
		virtual changelist *GetChangeList()=0;

	public private:
		virtual void SetChangelist(changelist *c)=0;

	};
}

#endif // ASSET_H
