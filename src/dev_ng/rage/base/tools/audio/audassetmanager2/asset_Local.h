#ifndef ASSET_LOCAL_H
#define ASSET_LOCAL_H

#include "asset.h"
#include "changelist.h"

using namespace System;

namespace audAssetManagement2
{
	public __gc class assetLocal : public asset
	{	

	public:
		assetLocal(changelist *changelist, String *assetPath);
		~assetLocal();
		String *Revert();
		String *GetDepotPath();
		String *GetLocalPath();
		changelist *GetChangeList();

	public private:
		void SetChangelist(changelist *c);

	private:
		changelist *m_Changelist;
		String *m_ClientPath;
		String *m_DepotPath;
	};
}

#endif //ASSET_LOCAL_H
