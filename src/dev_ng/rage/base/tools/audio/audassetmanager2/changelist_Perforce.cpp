#include "stdafx.h"
//#include "assetManager.h"
#include "changelist_Perforce.h"
#include "asset_Perforce.h"
#include <strsafe.h>

using namespace audAssetManagement2;
using namespace System::Runtime::InteropServices;
using namespace System::Collections;
using namespace System::IO;
using namespace System::Text;

changelistPerforce::changelistPerforce(clientWrapper *client, CustomClientUser *clientUser, assetManagerPerforce *assetManager, String *change, String *desc)
{
	m_Assets = __gc new ArrayList();
	m_Client = client;
	m_ClientUser = clientUser;
	m_AssetManager = assetManager;
	m_Change = change;
	m_Desc = desc;
}

changelistPerforce::~changelistPerforce()
{

}

String *changelistPerforce::RebuildChangeList()
{
	// setup the arg list
	const int kMaxCmd = 4;
	char argv[kMaxCmd][P4FileStatInfo::kMaxString];
	char *pArgv[kMaxCmd];

	// setup the arg list
	for (int i=0; i<kMaxCmd; ++i)
	{
		pArgv[i] = &argv[i][0];
	}

	//clear reults and set command used to process output
	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand(p4Opened);
	//Set arguments and run command
	StringCbPrintfA(argv[0],sizeof(argv[0]),"-c%s", m_Change);
	m_Client->SetArgv(1, pArgv);

	if(!m_Client->Run("opened", (ClientUser *)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	//check if error has occurred
	if(m_ClientUser->HasErrorOccurred())
	{
		throw new Exception(m_ClientUser->GetErrorMessage().c_str());
	}

	//	Make sure we're empty of all assets in our list
	m_Assets->Clear();

	//No error, carry on...
	std::list<std::string> files = m_ClientUser->GetFilesInChangelist();
	while(!files.empty())
	{
		std::string filepath = files.front();
		System::Diagnostics::Debug::WriteLine(String::Concat("  ....file: ",__gc new String(filepath.c_str())));
		//create new asset
		assetPerforce *asset = __gc new assetPerforce(m_Client, m_ClientUser, this, __gc new String(filepath.c_str()));
		//add it to changelist
		AddAsset(asset);
		//remove curent filepath from list
		files.pop_front();
	}

	String *infoMessage = m_ClientUser->GetInfoMessage().c_str();
	return infoMessage;
}

String *changelistPerforce::GetDescription()
{
	return m_Desc;
}

void changelistPerforce::SetDescription(String *description)
{
	m_Desc = description;
}

String *changelistPerforce::GetNumber()
{
	return m_Change;
}

String *changelistPerforce::GetAllFiles()
{
	StringBuilder* sb = __gc new StringBuilder();
 
	IEnumerator *iter = m_Assets->GetEnumerator();
	
	ArrayList* strings = __gc new ArrayList();

	while(iter->MoveNext())
	{
		asset *a = __try_cast<asset*>(iter->Current);
		String* s = a->GetDepotPath();

		if(!strings->Contains(s))
		{			
			sb->Append(s);
			sb->Append("\n ");
			strings->Add(s);
		}
	}

	return sb->ToString();
}

String *changelistPerforce::Submit(String *description)
{
	char arg[256];
	char* pArg = &arg[0];

	char *changeNo = (char*)(void*)Marshal::StringToHGlobalAnsi(m_Change);
	char *desc = (char*)(void*)Marshal::StringToHGlobalAnsi(description);
	char *files = (char*)(void*)Marshal::StringToHGlobalAnsi(GetAllFiles());

	if(changeNo && desc)
	{
		//Clear previous results and set command for output processing
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand(p4Submit);
		//set input data
		//input method invoked by using -i
		m_ClientUser->SetChangelistInputParams(changeNo,desc,files);
		//set arguments and run command
		StringCbPrintfA( arg, sizeof(arg), "-i" );
		m_Client->SetArgv(1, &pArg);

		if(!m_Client->Run("submit", (ClientUser *)m_ClientUser))
		{
			Marshal::FreeHGlobal(changeNo);
			Marshal::FreeHGlobal(desc);
			Marshal::FreeHGlobal(files);
			throw new Exception("Perforce connection error");

		}

		if(m_ClientUser->HasErrorOccurred())
		{
			Marshal::FreeHGlobal(changeNo);
			Marshal::FreeHGlobal(desc);
			Marshal::FreeHGlobal(files);
			throw new Exception(String::Concat("Error submitting changelist: ",m_ClientUser->GetErrorMessage().c_str()));
		}
	}
	else
	{
		Marshal::FreeHGlobal(changeNo);
		Marshal::FreeHGlobal(desc);
		Marshal::FreeHGlobal(files);
		throw new Exception(String::Concat("Error submitting changelist: ","Could not get changelist number or description"));
	}

	Marshal::FreeHGlobal(changeNo);
	Marshal::FreeHGlobal(desc);
	Marshal::FreeHGlobal(files);

	IEnumerator *assetsEnum = m_Assets->GetEnumerator();
	while(assetsEnum->MoveNext())
	{
		asset *a = __try_cast<asset*>(assetsEnum->Current);
		String *aPath = a->GetLocalPath();
		//check file was not marked for delete
		if(File::Exists(aPath))
		{
			if(!assetManager::IsReadOnly(aPath))
			{
				assetManager::MakeReadOnly(aPath);
			}
		}
	}
	m_Assets->Clear();
	m_AssetManager->RemoveChangelist(this);
	return m_ClientUser->GetInfoMessage().c_str();
}

String *changelistPerforce::Submit()
{
	return Submit(GetDescription());
}
String *changelistPerforce::Revert(bool bDelete)
{
	const int kNumArgs = 2;
	
	char argv[kNumArgs][P4FileStatInfo::kMaxString];
	char *pArgv[kNumArgs];

	// setup the arg list
	for (int i=0; i<kNumArgs; ++i)
	{
		pArgv[i] = &argv[i][0];
	}

	//Clear previous results and set command for output processing
	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand(p4Revert);
	//Set up arguments and run command
	StringCbPrintfA(argv[0],sizeof(argv[0]),"-c%s", m_Change);
	StringCbPrintfA(argv[1],sizeof(argv[1]), "//...");
	m_Client->SetArgv(kNumArgs,pArgv);

	if(!m_Client->Run("revert",(ClientUser*)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		String* szError = m_ClientUser->GetErrorMessage().c_str();
		//	This error just means the changelist is empty
		if (!szError->ToLower()->Contains("file(s) not opened on this client") &&
			!szError->ToLower()->Contains("file(s) not opened in that changelist"))
			throw new Exception(String::Concat("Error reverting changelist: ", szError));
	}

	if(m_Assets->Count > 0)
	{
		IEnumerator *assetsEnum = m_Assets->GetEnumerator();
		while(assetsEnum->MoveNext())
		{
			asset *a = __try_cast<asset*>(assetsEnum->Current);
			String *aPath = a->GetLocalPath();
			if(File::Exists(aPath))
			{
				if(!assetManager::IsReadOnly(aPath))
				{
					assetManager::MakeReadOnly(aPath);
				}
			}
		}
		m_Assets->Clear();
	}

	if(bDelete)
	{
		m_AssetManager->RemoveChangelist(this);
		Delete();
	}
	return m_ClientUser->GetInfoMessage().c_str();
}

String *changelistPerforce::RevertUnchanged()
{
	const int kNumArgs = 3;

	char argv[kNumArgs][P4FileStatInfo::kMaxString];
	char *pArgv[kNumArgs];

	// setup the arg list
	for (int i=0; i<kNumArgs; ++i)
	{
		pArgv[i] = &argv[i][0];
	}

	//Clear previous results and set command for output processing
	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand(p4Revert);
	//Set up arguments and run command
	StringCbPrintfA(argv[0],sizeof(argv[0]),"-c%s", m_Change);
	StringCbPrintfA(argv[1],sizeof(argv[1]),"-a");
	StringCbPrintfA(argv[2],sizeof(argv[2]), "//...");
	m_Client->SetArgv(kNumArgs,pArgv);

	if(!m_Client->Run("revert",(ClientUser*)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		String* szError = m_ClientUser->GetErrorMessage().c_str();
		//	This error just means the changelist is empty
		if (!szError->ToLower()->Contains("file(s) not opened on this client") &&
			!szError->ToLower()->Contains("file(s) not opened in that changelist") &&
			!szError->ToLower()->Contains("file(s) not opened for edit"))
			throw new Exception(String::Concat("Error reverting changelist: ", szError));
	}

	String *infoMessage = m_ClientUser->GetInfoMessage().c_str();
	if(!infoMessage->Equals(""))
	{
		infoMessage = String::Concat(infoMessage,"\n");
	}

	//	Finally repopulate the changelist asset list
	return String::Concat(infoMessage, RebuildChangeList());
}

String *changelistPerforce::Delete()
{
	char argv[2][P4FileStatInfo::kMaxString];
	char *pArgv[2];

	// setup the arg list
	for (int i=0; i<2; ++i)
	{
		pArgv[i] = &argv[i][0];
	}

	//Clear previous results and set command for output processing
	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand(p4Change);
	//Set up arguments and run command
	StringCbPrintfA(argv[0],sizeof(argv[0]), "-d");
	StringCbPrintfA(argv[1],sizeof(argv[1]), "%s", m_Change);	
	m_Client->SetArgv(2,pArgv);

	if(!m_Client->Run("change",(ClientUser*)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		throw new Exception(m_ClientUser->GetErrorMessage().c_str());
	}

	m_Assets->Clear();
	m_AssetManager->RemoveChangelist(this);
	return m_ClientUser->GetInfoMessage().c_str();
}

asset *changelistPerforce::CheckoutAsset(String *path, bool lock)
{
	path = m_AssetManager->SafeCheckPath(path);

	//need to get latest if we have revision 0 or checkout fails
	try
	{
		m_AssetManager->GetLatest(path, true);
	}
	catch(Exception *e)
	{
		throw new Exception(e->ToString());
	}

	char argv[2][P4FileStatInfo::kMaxString];
	char *pArgv[2];

	// setup the arg list
	for (int i=0; i<2; ++i)
	{
		pArgv[i] = &argv[i][0];
	}

	//Clear previous results and set command for output processing
	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand(p4Edit);
	//Set up arguments and run command
	StringCbPrintfA(argv[0],sizeof(argv[0]),"-c%s", m_Change);
	StringCbPrintfA(argv[1],sizeof(argv[1]),"%s", path);
	m_Client->SetArgv(2,pArgv);

	if(!m_Client->Run("edit",(ClientUser*)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		throw new Exception(String::Concat("Error checking out file: ",m_ClientUser->GetErrorMessage().c_str()));
	}

	if(lock)
	{
		char *pArg[1];
		pArg[0] = &argv[1][0];

		//Clear previous results and set command for output processing
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand(p4Lock);
		//Set arguments and run command
		m_Client->SetArgv(1,pArg);

		if(!m_Client->Run("lock",(ClientUser*)m_ClientUser))
		{
			throw new Exception("Perforce connection error");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			throw new Exception(String::Concat("Error locking file: ",m_ClientUser->GetErrorMessage().c_str()));
		}
	}

	assetPerforce *asset = __gc new assetPerforce(m_Client,m_ClientUser,this,path);
	//need to do a bit of extra checking here if we can't lock the file perforce helpfully doesn't give us an error
	//it gives us an info message

	if(lock)
	{
		lockedStatus *status;
		try
		{
			status = m_AssetManager->GetFileLockInfo(path);
		}
		catch(Exception *e)
		{
			throw new Exception (e->ToString());
		}

		if(status->IsLocked && !status->IsLockedByMe)
		{
			try
			{
				asset->Revert();
			}
			catch(Exception *e)
			{
				throw new Exception(e->ToString());
			}

			throw new Exception(String::Concat(path," currently locked by ",status->Owner));
		}
	}
	//Everything has gone swimmingly add asset to list
	
    m_Assets->Add(asset);

	return asset;
}

asset *changelistPerforce::MarkAssetForAdd(String *path)
{
	path = m_AssetManager->SafeCheckPath(path);
	char argv[2][P4FileStatInfo::kMaxString];
	char *pArgv[2];

	// setup the arg list
	for (int i=0; i<2; ++i)
	{
		pArgv[i] = &argv[i][0];
	}

	//Clear previous results and set command for output processing
	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand(p4Add);
	//Set arguments and run command
	StringCbPrintfA(argv[0],sizeof(argv[0]),"-c%s", m_Change);
	StringCbPrintfA(argv[1],sizeof(argv[1]),"%s", path);

	m_Client->SetArgv(2,pArgv);

	if(!m_Client->Run("add",(ClientUser*)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		throw new Exception(String::Concat("Error marking asset to be added: ",m_ClientUser->GetErrorMessage().c_str()));
	}

	//create asset and add it to list
	asset *a = __gc new assetPerforce(m_Client,m_ClientUser,this,path);
	m_Assets->Add(a);
	return a;

}

asset *changelistPerforce::MarkAssetForAdd(String *path, String *type)
{
	path = m_AssetManager->SafeCheckPath(path);
	char argv[3][P4FileStatInfo::kMaxString];
	char *pArgv[3];

	// setup the arg list
	for (int i=0; i<3; ++i)
	{
		pArgv[i] = &argv[i][0];
	}

	//Clear previous results and set command for output processing
	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand(p4Add);
	//Set arguments and run command
	StringCbPrintfA(argv[0],sizeof(argv[0]),"-c%s", m_Change);
	StringCbPrintfA(argv[1],sizeof(argv[1]),"-t%s", type);
	StringCbPrintfA(argv[2],sizeof(argv[2]),"%s", path);
	
	m_Client->SetArgv(3,pArgv);

	if(!m_Client->Run("add",(ClientUser*)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{

		throw new Exception(String::Concat("Error marking asset to be added: ",m_ClientUser->GetErrorMessage().c_str()));
	}
	//create asset and add it to list
	asset *a = __gc new assetPerforce(m_Client,m_ClientUser,this,path);
	m_Assets->Add(a);
	return a;

}

asset *changelistPerforce::MarkAssetForDelete(String *path)
{
	path = m_AssetManager->SafeCheckPath(path);

	try
	{
		m_AssetManager->GetLatest(path, true);
	}
	catch(Exception *e)
	{
		if (e->Message->Contains("Failed to sync file"))
		{
			String* szError = String::Concat("Error: ", e->Message, " File may already have been deleted. Allowing program to continue.");
			Console::WriteLine(szError);
			return NULL;
		}
		else
			throw e;
	}

	char argv[2][P4FileStatInfo::kMaxString];
	char *pArgv[2];

	// setup the arg list
	for (int i=0; i<2; ++i)
	{
		pArgv[i] = &argv[i][0];
	}

	// run delete command
	m_ClientUser->SetCommand(p4Delete);
	//ensures list of pending changes only
	StringCbPrintfA(argv[0],sizeof(argv[0]),"-c%s", m_Change);
	StringCbPrintfA(argv[1],sizeof(argv[1]), "%s", path);
	m_Client->SetArgv(2,pArgv);

	if(!m_Client->Run("delete",(ClientUser*)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		throw new Exception(String::Concat("Error marking asset to be deleted: ",m_ClientUser->GetErrorMessage().c_str()));
	}
	//create asset and add it to list
	asset *a = __gc new assetPerforce(m_Client,m_ClientUser,this,path);

	//lock asset
	char *pArg[1];
	pArg[0] = &argv[1][0];

	//Clear previous results and set command for output processing
	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand(p4Lock);
	//Set arguments and run command
	m_Client->SetArgv(1,pArg);

	if(!m_Client->Run("lock",(ClientUser*)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		throw new Exception(String::Concat("Error locking file: ",m_ClientUser->GetErrorMessage().c_str()));
	}

	assetPerforce *asset = __gc new assetPerforce(m_Client,m_ClientUser,this,path);
	//need to do a bit of extra checking here if we can't lock the file perforce helpfully doesn't give us an error
	//it gives us an info message

	lockedStatus *status;
	try
	{
		status = m_AssetManager->GetFileLockInfo(path);
	}
	catch(Exception *e)
	{
		throw new Exception (e->ToString());
	}


	if(status->IsLocked && !status->IsLockedByMe)
	{
		try
		{
			asset->Revert();
		}
		catch(Exception *e)
		{
			throw new Exception(e->ToString());
		}

		throw new Exception(String::Concat(path," currently locked by ",status->Owner));
	}


	m_Assets->Add(a);

	return a;
}

ArrayList *changelistPerforce::GetAssets()
{
	return m_Assets;
}

asset *changelistPerforce::GetAsset(String *path)
{
	path = m_AssetManager->SafeCheckPath(path);
	IEnumerator *assetEnum = m_Assets->GetEnumerator();
	while(assetEnum->MoveNext())
	{
		path = path->Replace("\\","/");
		asset *a = __try_cast<asset *>(assetEnum->Current);
		if(a->GetDepotPath()->ToUpper()->Equals(path->ToUpper()) ||
			a->GetLocalPath()->ToUpper()->Equals(path->ToUpper()))
		{
			return a;
		}
	}
	return NULL;
}

void changelistPerforce::RemoveAsset(asset *a)
{
	m_Assets->Remove(a);
}

void changelistPerforce::AddAsset(asset *asset)
{
	m_Assets->Add(asset);
}