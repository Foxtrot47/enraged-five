
#ifndef CHANGELIST_PERFORCE_H
#define CHANGELIST_PERFORCE_H

#include "changelist.h"
#include "customclientuser_perforce.h"
#include "asset_Perforce.h"
#include "assetManager_Perforce.h"
#include "clientWrapper.h"

using namespace System;

namespace audAssetManagement2 
{
	public __gc class changelistPerforce: public changelist 
	{
	public:
		changelistPerforce(clientWrapper *client, CustomClientUser *clientUser, assetManagerPerforce *parent, String *change, String *desc);
		~changelistPerforce();
		String *RebuildChangeList();
		String *GetDescription();
		void SetDescription(String *description);
		String *GetNumber();
		String *Submit(String *description);	
		String *Submit();	
		String *Revert(bool bDelete);
		String *RevertUnchanged();
		String *Delete();
		asset *CheckoutAsset(String *asset, bool lock);	
		asset *MarkAssetForAdd(String *asset);
		asset *MarkAssetForAdd(String *asset, String *type);
		asset *MarkAssetForDelete(String *asset);
		ArrayList *GetAssets();
		asset *GetAsset(String *path);

	public private:
		void RemoveAsset(asset *asset);
		void AddAsset(asset *asset);

	private:
		String *GetAllFiles();
		String *m_Change;
		String *m_Desc;
		clientWrapper *m_Client;
		CustomClientUser *m_ClientUser;
		assetManagerPerforce *m_AssetManager;
		ArrayList *m_Assets;
	};

}

#endif //CHANGELIST_PERFORCE_H
