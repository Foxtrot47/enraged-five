#include "stdafx.h"

#include "assetManager_Perforce.h"
#include "changelist_Perforce.h"
#include "asset_Perforce.h"
#include <strsafe.h>

using namespace audAssetManagement2;
using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Collections;
using namespace System::Diagnostics;
using namespace System::IO;

assetManagerPerforce::assetManagerPerforce() : m_HostName(0), m_ClientName(0), m_UserName(0), m_Password(0)
{
	m_ClientUser = new CustomClientUser();
	m_Client = new clientWrapper(m_ClientUser);
}

assetManagerPerforce::~assetManagerPerforce()
{
	delete m_ClientUser;
	delete m_Client;
}

String *assetManagerPerforce::Init(String *hostName, String *clientName, String *userName, String *passwordStr, String *depotRoot)
{
	m_HostName = hostName;
	m_ClientName = clientName;
	m_UserName = userName;
	m_Password = passwordStr;
	m_DepotRoot = depotRoot;
	m_Changelists = __gc new Hashtable();
	Error error;

	//Set the protocols:
	//Tag protocol causes data to appear using ClientUser::OuputStat() rather than OuputInfo()
	m_Client->SetProtocol("tag", "");
	//set api version
	m_Client->SetProtocol("api", "56");

	IntPtr clientPtr = Marshal::StringToHGlobalAnsi(clientName);
	if(clientPtr != 0)
	{
		const char *clientString = (char *)clientPtr.ToPointer();
		m_Client->SetClient(clientString); 
		m_ClientUser->SetClient(clientString);
		Marshal::FreeHGlobal(clientPtr);
	}
	
	//explicitly set hostName
	IntPtr hostPtr = Marshal::StringToHGlobalAnsi(hostName);
	if(hostPtr != 0)
	{
		const char *hostString = (char *)hostPtr.ToPointer();
		m_Client->SetPort(hostString); //Expected format: "myserver:myport".
		Marshal::FreeHGlobal(hostPtr);
	}

		//Explicitly set userName
	IntPtr userPtr = Marshal::StringToHGlobalAnsi(userName);
	if(userPtr != 0)
	{
		const char *userString = (char *)userPtr.ToPointer();
		m_Client->SetUser(userString);
		m_ClientUser->SetUser(userString);
		Marshal::FreeHGlobal(userPtr);
	}

	//Connect to server
	m_Client->Init(&error);

	//Test for error
	if(error.Test())
	{
		//Failed to connect
		StrBuf msg;
		error.Fmt(&msg);
		throw new Exception(String::Concat("Error connecting to server: ",msg.Text()));
	}

	if(passwordStr->Length > 0)
	{
		IntPtr passPtr = Marshal::StringToHGlobalAnsi(passwordStr);
		if (passPtr != 0)
		{
			const char *passString = (char *)passPtr.ToPointer();
			m_ClientUser->SetPassword(passString);
		}

		//clear reults and set command used to process output
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand(p4Login);		
		if(!m_Client->Run("login", (ClientUser *)m_ClientUser))
		{
			throw new Exception("Perforce connection error");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			throw new Exception(m_ClientUser->GetErrorMessage().c_str());
		}
	}

	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand( p4Info);

	if(!m_Client->Run("info", (ClientUser *)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		throw new Exception(String::Concat("Failed to get workspace root: ",m_ClientUser->GetErrorMessage().c_str()));
	}
	
	m_Root =  m_ClientUser->GetServerInfo().clientRoot;
	m_fixedRoot = m_Root->Replace("/", "\\");
	if (m_fixedRoot->EndsWith("\\"))
	{
		m_fixedRoot = m_fixedRoot->Substring(0, m_fixedRoot->Length - 1);
	}

	String *returnVal = 0;
	//Get List of changelists
	try
	{
		returnVal = CreateChangelists();
	}
	catch(Exception *e)
	{
		throw new Exception(String::Concat("Error Building changlist table: ",e->ToString()));
	}

	return returnVal;
}

void assetManagerPerforce::RefreshView()
{
	CreateChangelists();
	ViewUpdated();
}

String *assetManagerPerforce::CreateChangelists()
{
	String *returnVal = 0;
	const int kMaxCmd = 4;
	char argv[kMaxCmd][P4FileStatInfo::kMaxString];
	char *pArgv[kMaxCmd];

	// setup the arg list
	for (int i=0; i<kMaxCmd; ++i)
	{
		pArgv[i] = &argv[i][0];
	}

	//clear reults and set command used to process output
	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand(p4Changes);
	//Set arguments and run command
	//ensures list of pending changes only
	StringCbPrintfA(argv[0],sizeof(argv[0]),"-spending");
	StringCbPrintfA(argv[1],sizeof(argv[1]),"-u%s", m_UserName);
	StringCbPrintfA(argv[2],sizeof(argv[2]),"-c%s", m_ClientName);
	StringCbPrintfA(argv[3],sizeof(argv[3]),"-l", "");
	m_Client->SetArgv(kMaxCmd, pArgv);

	if(!m_Client->Run("changes", (ClientUser *)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		throw new Exception(m_ClientUser->GetErrorMessage().c_str());
	}

	returnVal = m_ClientUser->GetInfoMessage().c_str();

	//No error, carry on....
	//get list of p4changelists from user client
	std::list<P4Changelist> changelists = m_ClientUser->GetChangelists();

	//add default changelist manually, not listed under changes
	P4Changelist defaultList;
	StringCbPrintfA(defaultList.change,sizeof(defaultList.change),"default");
	changelists.push_back(defaultList);

	// delete changelists
	System::Collections::Generic::List<String*>* changelistsToDelete = __gc new System::Collections::Generic::List<String*>();
	IDictionaryEnumerator* enumerator = m_Changelists->GetEnumerator();
	while (enumerator->MoveNext())
	{
		String* key = __try_cast<String*>(enumerator->Key);
		bool found = false;
		for (std::list<P4Changelist>::const_iterator i = changelists.begin(); i != changelists.end(); ++i)
		{
			String* changelistNumber = __gc new String((*i).change);
			if (key->Equals(changelistNumber))
			{
				found = true;
				break;
			}
		}

		if (!found)
		{
			changelistsToDelete->Add(key);
		}
	}

	System::Collections::Generic::List<String*>::Enumerator listEnumerator = changelistsToDelete->GetEnumerator();
	while (listEnumerator.MoveNext())
	{
		m_Changelists->Remove(listEnumerator.Current);
	}

	// create/update changelists
	while(!changelists.empty())
	{
		P4Changelist p4Changelist = changelists.back();
		std::string change = p4Changelist.change;
		String *changeNumber = __gc new String(p4Changelist.change);
		String *descPtr = __gc new String(p4Changelist.desc);
		//remove any new line characters
		descPtr = descPtr->Replace("\n","");

		changelistPerforce *changelist = NULL;
		IDictionaryEnumerator* enumerator = m_Changelists->GetEnumerator();
		while(enumerator->MoveNext())
		{
			if (__try_cast<String*>(enumerator->Key)->Equals(changeNumber))
			{
				changelist = __try_cast<changelistPerforce*>(enumerator->Value);
				break;
			}
		}

		if (changelist == NULL)
		{
			//create new changelist
			changelist = __gc new changelistPerforce(m_Client, m_ClientUser, this, changeNumber, descPtr);
			//add changelist to hashtable
			m_Changelists->Add(changeNumber, changelist);
		}

		System::Diagnostics::Debug::WriteLine(String::Concat("Change: ",changeNumber));
		System::Diagnostics::Debug::WriteLine(String::Concat("Desc: ",descPtr));

		String *infoMessage = changelist->RebuildChangeList();
		if(!infoMessage->Equals(""))
		{
			returnVal = String::Concat(returnVal,"\n",infoMessage);
		}

		// remove current changelist from list
		changelists.pop_back();
	}
	return returnVal;	
}

String *assetManagerPerforce::Reconnect()
{	
	try
	{
		ShutDown();
	}
	catch(Exception *e)
	{
		EventLog::WriteEntry("Asset Manager",e->Message);
	}

	return Init(m_HostName, m_ClientName, m_UserName, m_Password, m_DepotRoot);

}

void assetManagerPerforce::ShutDown()
{
	Error error;

	//Close connection.
	m_Client->Final( &error );

	if(error.Test())
	{
		StrBuf msg;
		error.Fmt(&msg);
		throw new Exception(msg.Text());
	}
}

changelist *assetManagerPerforce::CreateNewChangelist(String *description)
{	
	char * desc = (char*)(void*)Marshal::StringToHGlobalAnsi(description);

	char argv[256];
	char *pArgv = &argv[0];
	//clear results and set command used to process output
	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand(p4Change);

	//set parameter for inputdata function in clientuser
	//input data method invoked by use of -i argument
	m_ClientUser->SetChangelistInputParams("new",desc,NULL);

	//Set arguments and run command
	StringCbPrintfA(argv,sizeof(argv),"-i");
	m_Client->SetArgv(1, &pArgv);

	if(!m_Client->Run("change", (ClientUser *)m_ClientUser))
	{
		Marshal::FreeHGlobal(desc);
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		Marshal::FreeHGlobal(desc);
		throw new Exception(m_ClientUser->GetErrorMessage().c_str());
	}

	//parse info message to get changelist number
	String *msg = m_ClientUser->GetInfoMessage().c_str();
	String *tokens[]=0;
	String *split = " ";
	tokens = msg->Split(split->ToCharArray());

	try
	{
		//ensure second token is a number
		Int32::Parse(tokens[1]);

	}
	catch(Exception *e)
	{
		Marshal::FreeHGlobal(desc);
		throw new Exception(String::Concat("Error creating new changelist: ",e->ToString()));
	}

	changelistPerforce *changelist = new changelistPerforce(m_Client, m_ClientUser, this, tokens[1],desc);
	//add to hashtable
	m_Changelists->Add(tokens[1],changelist);
	Marshal::FreeHGlobal(desc);
	return changelist;
}

void assetManagerPerforce::RemoveChangelist(changelist *changelist)
{
	m_Changelists->Remove(changelist->GetNumber());
}
ArrayList *assetManagerPerforce::GetChangelists()
{
	ArrayList *changelists = __gc new ArrayList();

	IDictionaryEnumerator *changelistEnum = m_Changelists->GetEnumerator();
	while(changelistEnum->MoveNext())
	{
		DictionaryEntry *de = __try_cast<DictionaryEntry*>(changelistEnum->Current);
		changelists->Add(__try_cast<changelist*>(de->Value));
	}

	return changelists;
}

changelist *assetManagerPerforce::GetChangelist(String *changeNo)
{
	if(m_Changelists->ContainsKey(changeNo->ToLower()))
	{
		return __try_cast<changelist*>(m_Changelists->get_Item(changeNo));
	}

	return NULL;

}

//NB this function can take either local or depot path
String *assetManagerPerforce::MoveAsset(changelist *oldChangelist, changelist *newChangelist, asset *asset)
{
	char *assetPathPtr = (char*)(void*)Marshal::StringToHGlobalAnsi(asset->GetLocalPath());
	if(assetPathPtr)
	{
		char argv[2][P4FileStatInfo::kMaxString];
		char *pArgv[2];

		// setup the arg list
		for (int i=0; i<2; ++i)
		{
			pArgv[i] = &argv[i][0];
		}

		//Clear previous results and set command for output processing
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand(P4Reopen);

		//Set arguments and run command
		StringCbPrintfA(argv[0],sizeof(argv[0]),"-c%s", newChangelist->GetNumber());
		StringCbPrintfA(argv[1],sizeof(argv[1]), assetPathPtr);
		m_Client->SetArgv(2, pArgv);

		if(!m_Client->Run("reopen", (ClientUser *)m_ClientUser))
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception("Perforce connection error");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception(String::Concat("Error moving asset: ", m_ClientUser->GetErrorMessage().c_str()));
		}

		oldChangelist->RemoveAsset(asset);
		newChangelist->AddAsset(asset);
		//update the changelist in the asset object
		asset->SetChangelist(newChangelist);
		Marshal::FreeHGlobal(assetPathPtr);

		return m_ClientUser->GetInfoMessage().c_str();
	}
	else
	{
		throw new Exception("Error moving asset: path error");
	}
}

String *assetManagerPerforce::GetLatest(String *path, bool force)
{
	path = SafeCheckPath(path);
	return GetLatest(path, force, String::Empty);
}

//NB this function can take either local or depot path
String *assetManagerPerforce::GetLatest(String *path, bool force, String* changelistNumber)
{
	path = SafeCheckPath(path);

	if (!String::IsNullOrEmpty(changelistNumber))
	{
		changelistNumber = String::Format("@{0}", changelistNumber);
	}

	path = GetDepotPath(path);

	char *assetPathPtr = (char*)(void*)Marshal::StringToHGlobalAnsi(path);
	if(assetPathPtr != 0)
	{
		char argv[2][P4FileStatInfo::kMaxString];
		char *pArgv[2];

		// setup the arg list
		for (int i=0; i<2; ++i)
		{
			pArgv[i] = &argv[i][0];
		}

		//Clear previous results and set command for output processing
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand( p4Sync );

		//Set arguments and run command
		StringCbPrintfA(argv[0],sizeof(argv[0]), "%s", force ? "-f" : "");
		StringCbPrintfA(argv[1],sizeof(argv[1]), "%s%s", assetPathPtr, changelistNumber);
		m_Client->SetArgv(2, pArgv);

		if(!m_Client->Run("sync", (ClientUser *)m_ClientUser))
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception("Perforce connection error");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception(String::Concat("Failed to sync file: ",m_ClientUser->GetErrorMessage().c_str()));
		}

		Marshal::FreeHGlobal(assetPathPtr);
		return m_ClientUser->GetInfoMessage().c_str();
	}
	else
	{
		throw new Exception("Failed to get path");
	}
}

String *assetManagerPerforce::GetLatestOnFileType(String *path, String* type, bool force)
{
	if(!path || !type)
		throw new Exception("Null path or type argument.");

	path = SafeCheckPath(path);
	path = GetDepotPath(path);

	char *assetPathPtr = (char*)(void*)Marshal::StringToHGlobalAnsi(String::Concat(path, type));
	if(assetPathPtr != 0)
	{
		char argv[2][P4FileStatInfo::kMaxString];
		char *pArgv[2];

		// setup the arg list
		for (int i=0; i<2; ++i)
		{
			pArgv[i] = &argv[i][0];
		}

		//Clear previous results and set command for output processing
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand( p4Sync );

		//Set arguments and run command
		StringCbPrintfA(argv[0],sizeof(argv[0]),"%s", force ? "-f" : "");
		StringCbPrintfA(argv[1],sizeof(argv[1]), assetPathPtr);
		m_Client->SetArgv(2, pArgv);

		if(!m_Client->Run("sync", (ClientUser *)m_ClientUser))
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception("Perforce connection error");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception(String::Concat("Failed to sync file: ",m_ClientUser->GetErrorMessage().c_str()));
		}

		Marshal::FreeHGlobal(assetPathPtr);
		return m_ClientUser->GetInfoMessage().c_str();
	}
	else
	{
		throw new Exception("Failed to get path");
	}
}

String *assetManagerPerforce::GetPrevious(String *path, bool force)
{
	path = SafeCheckPath(path);
	if (IsCheckedOut(path))
	{
		throw new Exception("We already have this file checked out, so this won't do us any good");
	}

	//if not passed a local path get the correct depot path
	path = GetDepotPath(path);

	char *assetPathPtr = (char*)(void*)Marshal::StringToHGlobalAnsi(path);
	if(assetPathPtr != 0)
	{
		//clear reults and set command used to process output
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand(p4Fstat);
		//set command arguments and run command
		m_Client->SetArgv(1, &assetPathPtr);
		
		if(!m_Client->Run("fstat", (ClientUser *)m_ClientUser))
		{
			throw new Exception("Perforce connection error");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			throw new Exception(String::Concat("Failed to get file info: ",m_ClientUser->GetErrorMessage().c_str()));
		}

		UInt32 uCurrentRev = (UInt32)atoi(m_ClientUser->GetFileInfo().haveRev);
		if (uCurrentRev <= 1)
		{
			throw new Exception("There is no previous revision of this file");
		}


		char argv[2][P4FileStatInfo::kMaxString];
		char *pArgv[2];

		// setup the arg list
		for (int i=0; i<2; ++i)
		{
			pArgv[i] = &argv[i][0];
		}

		//Clear previous results and set command for output processing
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand( p4Sync );

		//Set arguments and run command
		StringCbPrintfA(argv[0],sizeof(argv[0]),"%s", force ? "-f" : "");
		StringCbPrintfA(argv[1],sizeof(argv[1]), "%s#%u", assetPathPtr, uCurrentRev-1);
		m_Client->SetArgv(2, pArgv);

		if(!m_Client->Run("sync", (ClientUser *)m_ClientUser))
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception("Perforce connection error");	
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception(String::Concat("Failed to sync file: ",m_ClientUser->GetErrorMessage().c_str()));
		}

		Marshal::FreeHGlobal(assetPathPtr);
		return m_ClientUser->GetInfoMessage().c_str();
	}
	else
	{
		throw new Exception("Failed to get path");
	}
}

String *assetManagerPerforce::Integrate(String *szFrom, String *szTo, changelist *pChangeList)
{
	szFrom = SafeCheckPath(szFrom);
	szTo = SafeCheckPath(szTo);

	szFrom = GetDepotPath(szFrom);
	szTo = GetDepotPath(szTo);

	char *fromPathPtr = (char*)(void*)Marshal::StringToHGlobalAnsi(szFrom);
	char *toPathPtr = (char*)(void*)Marshal::StringToHGlobalAnsi(szTo);
	if(fromPathPtr != 0 && toPathPtr != 0)
	{
		const int numArgs = 3;

		char argv[numArgs][P4FileStatInfo::kMaxString];
		char *pArgv[numArgs];

		// setup the arg list
		for (int i=0; i<numArgs; ++i)
		{
			pArgv[i] = &argv[i][0];
		}

		//Clear previous results and set command for output processing
		m_ClientUser->ClearResults();
		//m_ClientUser->SetCommand( p4Sync );

		//Set arguments and run command
		if (pChangeList)
			StringCbPrintfA(argv[0],sizeof(argv[0]),"-c%s", pChangeList->GetNumber());
		else
			StringCbPrintfA(argv[0],sizeof(argv[0]),"");

		//StringCbPrintfA(argv[1],sizeof(argv[1]),"-i");
		//StringCbPrintfA(argv[2],sizeof(argv[2]),"-d");
		
		StringCbPrintfA(argv[1],sizeof(argv[1]), fromPathPtr);
		StringCbPrintfA(argv[2],sizeof(argv[2]), toPathPtr);
		m_Client->SetArgv(numArgs, pArgv);

		if(!m_Client->Run("integrate", (ClientUser *)m_ClientUser))
		{
			Marshal::FreeHGlobal(fromPathPtr);
			throw new Exception("Perforce connection error");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			Marshal::FreeHGlobal(fromPathPtr);
			throw new Exception(String::Concat("Failed to integrate: ", m_ClientUser->GetErrorMessage().c_str()));
		}

		Marshal::FreeHGlobal(fromPathPtr);
		RefreshView();
		return m_ClientUser->GetInfoMessage().c_str();
	}
	else
	{
		throw new Exception("Failed to integrate");
	}
}

String *assetManagerPerforce::Resolve(String *path, enResolveType resolveType)
{
	path = SafeCheckPath(path);
	path = GetDepotPath(path);

	char *pathPtr = (char*)(void*)Marshal::StringToHGlobalAnsi(path);
	if(pathPtr != 0)
	{
		const int numArgs = 2;

		char argv[numArgs][P4FileStatInfo::kMaxString];
		char *pArgv[numArgs];

		// setup the arg list
		for (int i=0; i<numArgs; ++i)
		{
			pArgv[i] = &argv[i][0];
		}

		//Clear previous results and set command for output processing
		m_ClientUser->ClearResults();
		//m_ClientUser->SetCommand( p4Sync );

		//Set arguments and run command
		const char* pszResolveType = NULL;
		switch (resolveType)
		{
		case kAcceptYours: pszResolveType = "-ay"; break;
		case kAcceptThiers: pszResolveType = "-at"; break;
		case kAcceptMerged: pszResolveType = "-am"; break;
		case kAcceptAuto: pszResolveType = "-a"; break;
		}

		StringCbPrintfA(argv[0],sizeof(argv[0]), pszResolveType);
		StringCbPrintfA(argv[1],sizeof(argv[1]), pathPtr);
		m_Client->SetArgv(numArgs, pArgv);

		if(!m_Client->Run("resolve", (ClientUser *)m_ClientUser))
		{
			Marshal::FreeHGlobal(pathPtr);
			throw new Exception("Perforce connection error");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			Marshal::FreeHGlobal(pathPtr);
			throw new Exception(String::Concat("Failed to resolve: ", m_ClientUser->GetErrorMessage().c_str()));
		}

		Marshal::FreeHGlobal(pathPtr);
		RefreshView();
		return m_ClientUser->GetInfoMessage().c_str();
	}
	else
	{
		throw new Exception("Failed to resolve");
	}
}

//NB this function can take either local or depot path
bool assetManagerPerforce::ExistsAsAsset(String *path)
{
	path = SafeCheckPath(path);
	path = GetDepotPath(path);

	char * assetPathPtr = (char *)(void *)Marshal::StringToHGlobalAnsi(path);
	if(assetPathPtr != 0)
	{
		//Clear previous results and set command for output processing
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand( p4Fstat );
		//Set arguments and run command
		m_Client->SetArgv(1, (char* const *)&assetPathPtr);

		if(!m_Client->Run("fstat", (ClientUser *)m_ClientUser))
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception("Perforce connection error");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			Marshal::FreeHGlobal(assetPathPtr);
			return false;
		}

		Marshal::FreeHGlobal(assetPathPtr);
		return m_ClientUser->GetFileInfo().IsAsset();
	}
	else
	{
		throw new Exception("Failed to get path");
	}		
}

String *assetManagerPerforce::GetDepotPath(String *path)
{
	path = SafeCheckPath(path);

	if(!path->Contains(":"))
	{
		//fixup path to represent depot path
		path = path->Replace("\\","/");
		path = path->Concat("/",path);
		path = path->Replace("//","/");
		path = String::Concat(m_DepotRoot,path);


		//directory path
		if(!path->Contains("."))
		{
			if(!path->EndsWith("/"))
			{
				path = String::Concat(path,"/");
			}
			path=String::Concat(path,"...");
		}
	}
	else if(path->EndsWith("\\"))
	{
		path=String::Concat(path,"...");
	}

	return path;
}

String *assetManagerPerforce::GetWorkingPath(String *path)
{
	path = SafeCheckPath(path);

	if(path->Equals(""))
	{
		return m_fixedRoot;
	}

	path = GetDepotPath(path);
	
	char *assetPathPtr = (char *)(void *)Marshal::StringToHGlobalAnsi(path);

	if(assetPathPtr)
	{
		//clear reults and set command used to process output
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand(p4Where);
		//set command arguments and run command
		m_Client->SetArgv(1, &assetPathPtr);

		if(!m_Client->Run("where", (ClientUser *)m_ClientUser))
		{
			throw new Exception("Perforce connection error");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			throw new Exception(String::Concat("Error getting working path: ",m_ClientUser->GetErrorMessage().c_str()));
		}

		Marshal::FreeHGlobal(assetPathPtr);
		String *stringPaths = __gc new String(m_ClientUser->GetInfoMessage().c_str());
		Char sep[] = {' '};
		String *paths[] = stringPaths->Split(sep);

		//remove ellipses
		if(paths->Length !=3)
		{
			throw new Exception(String::Format("Unexpected output from p4 where: input={0} ouput={1}. It is not safe to continue.",path,stringPaths));
		}

		String* workingPath = paths[2]->Replace("...","")->Trim();
		if (String::IsNullOrEmpty(workingPath))
		{
			throw new Exception(String::Format("Unexpected output from p4 where: input={0} ouput={1}. It is not safe to continue.",path,stringPaths));
		}
		return workingPath;
	}
	else
	{
		Marshal::FreeHGlobal(assetPathPtr);
		throw new Exception(String::Concat("Error getting working path: ","Failed to get asset path"));
	}	
}

bool assetManagerPerforce::IsCheckedOut(String *path)
{
	path = SafeCheckPath(path);
	path = path->Replace("\\","/");

	IEnumerator *changelistEnum =  m_Changelists->Values->GetEnumerator();
	String* inPathUpper = path->ToUpper();
	while(changelistEnum->MoveNext())
	{
		changelistPerforce *changelist = __try_cast<changelistPerforce*>(changelistEnum->Current);
		if(changelist)
		{
			IEnumerator *assetEnum = changelist->GetAssets()->GetEnumerator();
			while(assetEnum->MoveNext())
			{
				assetPerforce * a = __try_cast<assetPerforce*>(assetEnum->Current);
				if(a && (a->GetDepotPath()->ToUpper()->Equals(inPathUpper) || a->GetLocalPath()->Equals(inPathUpper)) )
				{
					return true;
				}
			}
		}
	}
	return false;
}

bool assetManagerPerforce::IsMarkedForAdd(String *path)
{
	path = SafeCheckPath(path);
	path = GetDepotPath(path);

	IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(path);

	if(assetPathPtr == 0)
	{
		throw new Exception("Failed to get path");
	}

	char *assetPathString = (char *)assetPathPtr.ToPointer();

	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand( p4Fstat );
	m_Client->SetArgv(1, &assetPathString);
	
	if(!m_Client->Run("fstat", (ClientUser *)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		return false;
	}

	String *action = new String(m_ClientUser->GetFileInfo().action);
	return action->Equals("add");
}

lockedStatus *assetManagerPerforce::GetFileLockInfo(String *path)
{
	path = SafeCheckPath(path);
	path = GetDepotPath(path);
	
	IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(path);

	if(assetPathPtr == 0)
	{
		throw new Exception("Failed to get path");
	}
//	bool isLocked=false;
	char *assetPathString = (char *)assetPathPtr.ToPointer();

	bool isOpen = false;
	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand( p4Fstat );
	m_Client->SetArgv(1, &assetPathString);
	if(!m_Client->Run("fstat", (ClientUser *)m_ClientUser))
	{
		throw new Exception("Perforce connection failure");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		Marshal::FreeHGlobal(assetPathPtr);	
		throw new Exception(String::Concat("Failed to determine if files is locked by me: ",
			m_ClientUser->GetErrorMessage().c_str()));
	}
	isOpen = m_ClientUser->GetFileInfo().IsOpen();
	if( isOpen && m_ClientUser->GetFileInfo().m_OurLock )
	{
		Marshal::FreeHGlobal(assetPathPtr);
		return new lockedStatus(isOpen,true,true,m_ClientName);
	}

	//Not locked by me, check if locked by other
	String *owner=S"";

	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand( p4IsLocked );
	m_Client->SetArgv(1, &assetPathString);
	
	if(!m_Client->Run("fstat", (ClientUser *)m_ClientUser))
	{
		Marshal::FreeHGlobal(assetPathPtr);
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{
		Marshal::FreeHGlobal(assetPathPtr);
		throw new Exception(String::Concat("Failed to determine if files is locked by other: ",
			m_ClientUser->GetErrorMessage().c_str()));

	}

	owner = m_ClientUser->GetResult().c_str();

	//locked by other
	if(!owner->Empty || !owner->Equals(""))
	{
		Marshal::FreeHGlobal(assetPathPtr);
		return new lockedStatus(isOpen,true,false,owner);
	}

	//Not locked open
	Marshal::FreeHGlobal(assetPathPtr);
	return new lockedStatus(isOpen,false,false,owner);

}

bool assetManagerPerforce::IsResolved(String *path)
{
	path = SafeCheckPath(path);
	path = GetDepotPath(path);

	IntPtr assetPathPtr = Marshal::StringToHGlobalAnsi(path);

	if(assetPathPtr == 0)
	{
		throw new Exception("Failed to get path");
	}

	char *assetPathString = (char *)assetPathPtr.ToPointer();

	m_ClientUser->ClearResults();
	m_ClientUser->SetCommand( p4Fstat );
	m_Client->SetArgv(1, &assetPathString);

	if(!m_Client->Run("fstat", (ClientUser *)m_ClientUser))
	{
		Marshal::FreeHGlobal(assetPathPtr);	
		throw new Exception("Perforce connection error");
	}

	Marshal::FreeHGlobal(assetPathPtr);	
	if(m_ClientUser->HasErrorOccurred())
	{		
		throw new Exception(String::Concat("Epic fail: ",
			m_ClientUser->GetErrorMessage().c_str()));
	}

	return m_ClientUser->GetFileInfo().IsResolved;
}

bool assetManagerPerforce::HaveLatest(String* path)
{
	path = SafeCheckPath(path);
	path = GetDepotPath(path);

	char *assetPathPtr = (char*)(void*)Marshal::StringToHGlobalAnsi(path);
	if(assetPathPtr != 0)
	{
		char argv[2][P4FileStatInfo::kMaxString];
		char *pArgv[2];

		// setup the arg list
		for (int i=0; i<2; ++i)
		{
			pArgv[i] = &argv[i][0];
		}

		//Clear previous results and set command for output processing
		m_ClientUser->ClearResults();
		m_ClientUser->SetCommand( p4Sync );

		//Set arguments and run command
		StringCbPrintfA(argv[0],sizeof(argv[0]),"%s", "-n");
		StringCbPrintfA(argv[1],sizeof(argv[1]), assetPathPtr);
		m_Client->SetArgv(2, pArgv);

		if(!m_Client->Run("sync", (ClientUser *)m_ClientUser))
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception("Perforce connection error");
		}

		if(m_ClientUser->HasErrorOccurred())
		{
			Marshal::FreeHGlobal(assetPathPtr);
			throw new Exception(String::Concat("Failed to check status of file: ",m_ClientUser->GetErrorMessage().c_str()));
		}

		Marshal::FreeHGlobal(assetPathPtr);
		String* output = m_ClientUser->GetInfoMessage().c_str();
		return output->Contains("up-to-date");
	}
	else
	{
		throw new Exception("Failed to get path");
	}
}

String *assetManagerPerforce::GetLatestChangelistNumber()
{
	char argv[2][P4FileStatInfo::kMaxString];
	char *pArgv[2];

	for (int i = 0; i < 2; ++i)
	{
		pArgv[i] = &argv[i][0];	
	}
	

	//Clear previous results and set command for output processing
	m_ClientUser->SetCommand( p4LatestChangelistNumber );

	//Set arguments and run command
	StringCbPrintfA(argv[0],sizeof(argv[0]),"-ssubmitted");
	StringCbPrintfA(argv[1],sizeof(argv[1]),"-m1");
	m_Client->SetArgv(2, pArgv);

	if(!m_Client->Run("changes", (ClientUser *)m_ClientUser))
	{
		throw new Exception("Perforce connection error");
	}

	if(m_ClientUser->HasErrorOccurred())
	{	
		throw new Exception(String::Concat("Failed to get latest changelist number: ",m_ClientUser->GetErrorMessage().c_str()));
	}

	return m_ClientUser->GetResult().c_str();
}

String *assetManagerPerforce::SafeCheckPath(String *path)
{
	path = path->Replace("@", "%40");
	path = path->Replace("#", "%23");
	path = path->Replace("*", "%2A");
	return path;
}