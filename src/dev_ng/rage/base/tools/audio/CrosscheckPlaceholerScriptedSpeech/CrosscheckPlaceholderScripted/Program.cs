﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.CmdLine;
using System.IO;
using System.Xml;

namespace CrosscheckPlaceholderScripted
{
    class Program
    {
        static void Main(string[] args)
        {

            CmdLineParser cmdParser = new CmdLineParser(args);
            string placeHolderFolder = cmdParser.Arguments["placeHolderFolder"];
            string scriptedSpeechFolder = cmdParser.Arguments["scriptedSpeechFolder"];
            string csvOutput = cmdParser.Arguments["csvOutput"];
            string dStarFolder = cmdParser.Arguments["dStarFolder"];

            if (csvOutput == null || scriptedSpeechFolder == null || placeHolderFolder == null || dStarFolder == null)
            {
                Console.WriteLine("Please use arguments -placeHolderFolder -scriptedSpeechFoler -csvOutput -dStarFolder to run this script");
                return;
            }


            csvOutput = Path.ChangeExtension(csvOutput, ".csv");
            Console.WriteLine("Getting latest for placeholder folder, this might take a while...");

            AutoAssetManager.Instance.AssetManager.GetLatest(placeHolderFolder, false);
            if (!Directory.Exists(placeHolderFolder))
            {
                throw new DirectoryNotFoundException(placeHolderFolder + " does not exist, please check for typos.");
            }
            List<string> placeHolderNames = new List<string>();
            RecursiveFileSearchHelper.WalkDirectoryTreeNames(new DirectoryInfo(placeHolderFolder), "*.wav", placeHolderNames);

            Console.WriteLine("Getting latest for scripted speech folder, this might take a while..");

            AutoAssetManager.Instance.AssetManager.GetLatest(scriptedSpeechFolder, false);
            if (!Directory.Exists(scriptedSpeechFolder))
            {
                throw new DirectoryNotFoundException(scriptedSpeechFolder + " does not exist, please check for typos.");
            }
            List<string> scriptedSpeechNames = new List<string>();
            RecursiveFileSearchHelper.WalkDirectoryTreeNames(new DirectoryInfo(scriptedSpeechFolder), "*.wav", scriptedSpeechNames);

            List<string> nonMatching = placeHolderNames.Except(scriptedSpeechNames, StringComparer.OrdinalIgnoreCase).ToList<string>();

            List<FileInfo> dstarFiles = new List<FileInfo>();
            RecursiveFileSearchHelper.WalkDirectoryTree(new DirectoryInfo(dStarFolder), "*.dstar", dstarFiles);

            List<string> usedDialouge = new List<string>();
            foreach (FileInfo dstarFile in dstarFiles)
            {
               XmlDocument xmlDoc = DstarHelper.GetXMLFromFile(dstarFile);
               usedDialouge.AddRange(DstarHelper.GetAllFileNamesValues(xmlDoc));
            }

            

            StringBuilder sb = new StringBuilder();
            
            foreach (string name in nonMatching)
            {
                if (usedDialouge.Contains(name.Substring(0,name.Length-7)))
                {
                    sb.Append(name);
                    sb.Append(",");
                    sb.Append(Directory.GetFiles(placeHolderFolder, name, SearchOption.AllDirectories)[0]);
                    sb.Append("\n");
                }
            }
            
            try
            {
                File.WriteAllText(csvOutput, sb.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }



        }
    }
}
