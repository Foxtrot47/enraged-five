﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CrosscheckPlaceholderScripted
{
    class DstarHelper
    {

        public static XmlDocument GetXMLFromFile(FileInfo file)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(file.FullName);
            return doc;
        }

        public static List<string> GetAllFileNamesValues(XmlDocument doc)
        {
            List<string> listOfFileNames = new List<string>();
            XmlNodeList list = doc.DocumentElement.SelectNodes("//Line");
            foreach (XmlNode xmlNode in list)
            {
                listOfFileNames.Add(xmlNode.Attributes["filename"].Value);
            }
            return listOfFileNames;
        }

    }
}
