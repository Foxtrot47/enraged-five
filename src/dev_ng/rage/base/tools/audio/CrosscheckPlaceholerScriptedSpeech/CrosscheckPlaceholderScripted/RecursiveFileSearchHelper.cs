﻿using System;
using System.Collections.Generic;
using System.IO;

public class RecursiveFileSearchHelper
{
    static System.Collections.Specialized.StringCollection log = new System.Collections.Specialized.StringCollection();

    public static void WalkDirectoryTree(System.IO.DirectoryInfo root, string type, List<FileInfo> filesInDirectory)
    {
        System.IO.FileInfo[] files = null;
        System.IO.DirectoryInfo[] subDirs = null;

        try
        {
            files = root.GetFiles(type);
        }
        catch (UnauthorizedAccessException e)
        {
            log.Add(e.Message);
        }

        catch (System.IO.DirectoryNotFoundException e)
        {
            Console.WriteLine(e.Message);
        }

        if (files != null)
        {
            foreach (System.IO.FileInfo fi in files)
            {
                filesInDirectory.Add(fi);
            }

            subDirs = root.GetDirectories();

            foreach (System.IO.DirectoryInfo dirInfo in subDirs)
            {
                WalkDirectoryTree(dirInfo, type, filesInDirectory);
            }


        }
    }

    public static void WalkDirectoryTreeNames(System.IO.DirectoryInfo root, string type, List<string> filesInDirectory)
    {
        System.IO.FileInfo[] files = null;
        System.IO.DirectoryInfo[] subDirs = null;

        try
        {
            files = root.GetFiles(type);
        }
        catch (UnauthorizedAccessException e)
        {
            log.Add(e.Message);
        }

        catch (System.IO.DirectoryNotFoundException e)
        {
            Console.WriteLine(e.Message);
        }

        if (files != null)
        {
            foreach (System.IO.FileInfo fi in files)
            {
                filesInDirectory.Add(fi.Name);
            }

            subDirs = root.GetDirectories();

            foreach (System.IO.DirectoryInfo dirInfo in subDirs)
            {
                WalkDirectoryTreeNames(dirInfo, type, filesInDirectory);
            }


        }
    }
}
