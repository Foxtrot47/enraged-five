using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using audVoiceCapture;
using rage.ToolLib;

namespace audVC2CSV
{
    class Program
    {
        static Hashtable LoadWaveNameHashes(string builtWaves)
        {
            Hashtable waveHashes = new Hashtable();
            foreach (string p in Directory.GetFiles(builtWaves))
            {
                XmlDocument builtWavesDoc = new XmlDocument();
                builtWavesDoc.Load(p);              

                ParseWaveNameHashes(builtWavesDoc.DocumentElement, waveHashes);
            }
            return waveHashes;
        }

        static void ParseWaveNameHashes(XmlNode node, Hashtable waveHashes)
        {
            if(node.Name == "Wave")
            {
                string name = node.Attributes["name"].Value;
                uint hash = uint.Parse(node.Attributes["nameHash"].Value);
                if (!waveHashes.ContainsKey(hash))
                {
                    waveHashes.Add(hash, name);
                }
            }
            foreach(XmlNode n in node.ChildNodes)
            {
                ParseWaveNameHashes(n, waveHashes);
            }
        }

        static Hashtable LoadObjectNames(string assetPath)
        {
            Hashtable h = new Hashtable();
            LoadObjectNames(assetPath, h);
            return h;
        }

        static void LoadObjectNames(string assetPath, Hashtable h)
        {
            foreach (var d in Directory.GetDirectories(assetPath))
            {
                LoadObjectNames(d, h);
            }

            foreach(var f in Directory.GetFiles(assetPath, "*.xml"))
            {
                var doc = XDocument.Load(f);
                var root = doc.Element("Objects");
                if (root != null)
                {
                    foreach (var o in root.Elements())
                    {
                        if (o.Attribute("name") != null)
                        {
                            Hash hash = new Hash();
                            hash.Value = o.Attribute("name").Value;
                            if (!h.ContainsKey(hash.Key))
                            {
                                h.Add(hash.Key, hash.Value);
                            }
                        }
                    }
                }
            }
        }

        static string FindAssetName(uint assetNameHash, Hashtable objectNames, Hashtable waveNameHashes)
        {
            if (waveNameHashes.ContainsKey(assetNameHash))
            {
                return (string)waveNameHashes[assetNameHash];
            }
            else if (objectNames.ContainsKey(assetNameHash))
            {
                return objectNames[assetNameHash] as string;
            }
            return string.Format("{0}", assetNameHash);
        }

        static void Main(string[] args)
        {
            var includeVirtualVoices = true;
            var includePhysicalVoices = false;

            audVoiceCaptureParser parser = new audVoiceCaptureParser();
            Console.WriteLine("Loading wave name hashes...");
            Hashtable waveNameHashes = LoadWaveNameHashes(@"x:\gta5\audio\dev\build\ps3\builtwaves\");
            Console.WriteLine("Loading object names...");
            Hashtable objectNames = LoadObjectNames(@"X:\gta5\audio\dev\assets\Objects\Core\Audio\MODULARSYNTH\");
            Console.WriteLine("Loading capture file...");
            List<audVoiceCapturePacket> packets = parser.ParseFile(@"c:\voiceCapture.dmp");
            Console.WriteLine("Writing csv...");
            int frameNum = 0;
            uint curTime = 0;

            var typeNames = new string[]{   "WavePlayer",
		                                    "GrainPlayer",
		                                    "ModularSynth",
		                                    "Submix",
		                                    "SynthCore",
		                                    "ExternalStream"};

            var states = new string[] {
                                        "PlayingVirt",
	                                    "StartingPhys",
	                                    "PlayingPhys",
	                                    "Virtualising",
	                                    "StoppingPhys",
                                                };

            StreamWriter sw = new StreamWriter(@"c:\voiceCapture.csv",false);
            sw.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}", "frameNum", "curTime", "frameVoiceId", "voice.bucketId", "voice.voiceId", "voice.groupId", "assetName", "slotName", "voice.virtualisationScore", "voice.hpfCutoff", "voice.lpfCutoff", "voice.peakLevel", "voice.state", "voice.playtime", "voice.costEstimate", "voice.pcmSourceType", "voice.pcmSourceChannelId");
            List<string> waveSlots = null;
            int voiceId = 0;
            foreach(audVoiceCapturePacket packet in packets)
            {
                switch(packet.packetHeader)
                {
                    case audVoiceCapturePackets.AUD_VOICE_CAPTURE_START:
                        waveSlots = ((audVoiceCaptureStart)packet).waveSlotContents;
                        break;
                    case audVoiceCapturePackets.AUD_VOICE_CAPTURE_FRAME_START:
                        curTime = ((audVoiceCaptureFrameStart)packet).timeInMs;
                        voiceId = 0;
                        break;
                    case audVoiceCapturePackets.AUD_VOICE_CAPTURE_FRAME_END:
                        frameNum++;
                        break;
                    case audVoiceCapturePackets.AUD_VOICE_CAPTURE_VIRTUAL_VOICE:
                        if(includeVirtualVoices)
                        {
                            audVoiceCaptureVirtualVoice voice = (audVoiceCaptureVirtualVoice)packet;
                            string assetName = FindAssetName(voice.assetNameHash, objectNames, waveNameHashes);
                            string slotName = voice.pcmSourceType == 0 && voice.slotId < waveSlots.Count ? waveSlots[voice.slotId] : "none";

                            string typeName = voice.pcmSourceType < typeNames.Length ? typeNames[voice.pcmSourceType] : voice.pcmSourceType.ToString();
                            sw.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}", frameNum, curTime, voiceId++, voice.bucketId, voice.voiceId, voice.groupId, assetName, slotName, voice.virtualisationScore, voice.hpfCutoff, voice.lpfCutoff, voice.peakLevel, states[voice.state], voice.playtime, voice.costEstimate, typeName, voice.pcmSourceChannelId);
                        }
                        break;
                    case audVoiceCapturePackets.AUD_VOICE_CAPTURE_PHYSICAL_VOICE:
                        if (includePhysicalVoices)
                        {
                            var voice = packet as audVoiceCapturePhysicalVoice;
                            string assetName = FindAssetName(voice.assetNameHash, objectNames, waveNameHashes);
                                
                            string slotName = voice.pcmSourceType == 0 && voice.slotId < waveSlots.Count ? waveSlots[voice.slotId] : "none";

                            string typeName = voice.pcmSourceType < typeNames.Length ? typeNames[voice.pcmSourceType] : voice.pcmSourceType.ToString();
                            sw.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}", frameNum, curTime, voiceId++, voice.bucketId, voice.voiceId, 0/*voice.groupId*/, assetName, slotName, voice.virtualisationScore, 0/*voice.hpfCutoff*/, 0/*voice.lpfCutoff*/, 0/*voice.peakLevel*/, "Physical", 0/*voice.playtime*/, voice.costEstimate, typeName, voice.pcmSourceChannelId);
                        }
                        break;
                }
            }
            sw.Close();
        }
    }
}
