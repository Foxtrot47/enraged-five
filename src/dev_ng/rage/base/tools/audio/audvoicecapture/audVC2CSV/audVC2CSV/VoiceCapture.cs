using System;
using System.IO;
using System.Collections.Generic;

namespace audVoiceCapture
{

public enum audVoiceCapturePackets
{
	AUD_VOICE_CAPTURE_START = 0,
	AUD_VOICE_CAPTURE_FRAME_START,
	AUD_VOICE_CAPTURE_VIRTUAL_VOICE,
	AUD_VOICE_CAPTURE_PHYSICAL_VOICE,
	AUD_VOICE_CAPTURE_FRAME_END,
	AUD_VOICE_CAPTURE_END,
};


public class audVoiceCapturePacket
{
    public audVoiceCapturePackets packetHeader;
    public byte packetSize;
};

public class audVoiceCaptureStart : audVoiceCapturePacket
{
    public audVoiceCaptureStart(BinaryReader br)
    {
        packetHeader = audVoiceCapturePackets.AUD_VOICE_CAPTURE_START;
        packetSize = br.ReadByte();

        numWaveSlots = audVoiceCaptureParser.EndianSwap(br.ReadUInt32());
        waveSlotTableSize = audVoiceCaptureParser.EndianSwap(br.ReadUInt32());
        numVirtualVoices = audVoiceCaptureParser.EndianSwap(br.ReadUInt32());

        waveSlotContents = new List<string>((int)numWaveSlots);
        for(UInt32 i = 0; i < numWaveSlots; i++)
        {
            byte[] buf = new byte[256];
            int k = 0;
            bool gotNul = false;
            while(!gotNul)
            {
                buf[k] = br.ReadByte();
                if(buf[k] == 0)
                {
                    gotNul = true;
                }
                k++;                
            }

            waveSlotContents.Add(System.Text.Encoding.ASCII.GetString(buf,0,k-1));
        }
     }
    public UInt32 numWaveSlots;
    public UInt32 waveSlotTableSize;
    public UInt32 numVirtualVoices;
    public List<string> waveSlotContents;
};

public class audVoiceCaptureEnd : audVoiceCapturePacket
{
    public audVoiceCaptureEnd(BinaryReader br)
    {
        packetHeader = audVoiceCapturePackets.AUD_VOICE_CAPTURE_END;
        packetSize = br.ReadByte();
    }
};

public class audVoiceCaptureFrameStart : audVoiceCapturePacket
{
    public audVoiceCaptureFrameStart(BinaryReader br)
    {
        packetHeader = audVoiceCapturePackets.AUD_VOICE_CAPTURE_FRAME_START;
        packetSize = br.ReadByte();
        timeInMs = audVoiceCaptureParser.EndianSwap(br.ReadUInt32());
    }
    public UInt32 timeInMs;
};

public class audVoiceCaptureFrameEnd : audVoiceCapturePacket
{
    public audVoiceCaptureFrameEnd(BinaryReader br)
    {
        packetHeader = audVoiceCapturePackets.AUD_VOICE_CAPTURE_FRAME_END;
        packetSize = br.ReadByte();
    }
};

public class audVoiceCaptureVirtualVoice : audVoiceCapturePacket
{
    public audVoiceCaptureVirtualVoice(BinaryReader br)
    {
        packetHeader = audVoiceCapturePackets.AUD_VOICE_CAPTURE_VIRTUAL_VOICE;
        packetSize = br.ReadByte();

        virtualisationScore = audVoiceCaptureParser.EndianSwap(br.ReadUInt32());
        playtime = (Int32)(audVoiceCaptureParser.EndianSwap(br.ReadUInt32()));
        state = audVoiceCaptureParser.EndianSwap(br.ReadUInt32());
        assetNameHash = audVoiceCaptureParser.EndianSwap(br.ReadUInt32());
        hpfCutoff = audVoiceCaptureParser.EndianSwap(br.ReadUInt16());
        lpfCutoff = audVoiceCaptureParser.EndianSwap(br.ReadUInt16());
        peakLevel = audVoiceCaptureParser.EndianSwap(br.ReadUInt16());
        
        groupId = audVoiceCaptureParser.EndianSwap(br.ReadUInt16());
        costEstimate = audVoiceCaptureParser.EndianSwap(br.ReadUInt16());
        slotId = br.ReadByte();
        bucketId = br.ReadByte();
        voiceId = br.ReadByte();
        pcmSourceType = br.ReadByte();
        pcmSourceChannelId = br.ReadByte();

        // Skip padding byte
        br.ReadByte();
       
    }
    public UInt32 virtualisationScore;
    public Int32 playtime;
    public UInt32 state;
    public UInt32 assetNameHash;
    public UInt16 hpfCutoff;
    public UInt16 lpfCutoff;
    public UInt16 peakLevel;
    
    public UInt16 groupId;
    public UInt16 costEstimate;
    public byte slotId;
    public byte bucketId;
    public byte voiceId;
    public byte pcmSourceType;
    public byte pcmSourceChannelId;
};

public class audVoiceCapturePhysicalVoice : audVoiceCapturePacket
{
    public audVoiceCapturePhysicalVoice(BinaryReader br)
    {
        packetHeader = audVoiceCapturePackets.AUD_VOICE_CAPTURE_PHYSICAL_VOICE;
        packetSize = br.ReadByte();

        assetNameHash = audVoiceCaptureParser.EndianSwap(br.ReadUInt32());
        virtualisationScore = audVoiceCaptureParser.EndianSwap(br.ReadUInt32());
        costEstimate = audVoiceCaptureParser.EndianSwap(br.ReadUInt16());
        physVoiceId = br.ReadByte();
        bucketId = br.ReadByte();
        voiceId = br.ReadByte();
        slotId = br.ReadByte();
        pcmSourceType = br.ReadByte();
        pcmSourceChannelId = br.ReadByte();
     }
    public UInt32 assetNameHash;
    public UInt32 virtualisationScore;
    public UInt16 costEstimate;
    public byte physVoiceId;
    public byte bucketId;
    public byte voiceId;
    public byte slotId;
    public byte pcmSourceType;
    public byte pcmSourceChannelId;
};

    public class audVoiceCaptureParser
    {
        public List<audVoiceCapturePacket> ParseFile(string fileName)
        {
            List<audVoiceCapturePacket> packets = new List<audVoiceCapturePacket>();

            BinaryReader br = new BinaryReader(new FileStream(fileName, FileMode.Open), System.Text.Encoding.ASCII);
            bool finished = false;
            while(!finished)
            {               
                if(br.BaseStream.Position >= br.BaseStream.Length)
                {
                    finished = true;
                    break;
                }
                byte header = br.ReadByte();
                switch((audVoiceCapturePackets)header)
                {
                    case audVoiceCapturePackets.AUD_VOICE_CAPTURE_START:
                        packets.Add(new audVoiceCaptureStart(br));
                        break;
                    case audVoiceCapturePackets.AUD_VOICE_CAPTURE_END:
                        packets.Add(new audVoiceCaptureEnd(br));
                        finished = true;
                        break;
                    case audVoiceCapturePackets.AUD_VOICE_CAPTURE_FRAME_START:
                        packets.Add(new audVoiceCaptureFrameStart(br));
                        break;
                    case audVoiceCapturePackets.AUD_VOICE_CAPTURE_FRAME_END:
                        packets.Add(new audVoiceCaptureFrameEnd(br));
                        break;
                    case audVoiceCapturePackets.AUD_VOICE_CAPTURE_VIRTUAL_VOICE:
                        packets.Add(new audVoiceCaptureVirtualVoice(br));
                        break;
                    case audVoiceCapturePackets.AUD_VOICE_CAPTURE_PHYSICAL_VOICE:
                        packets.Add(new audVoiceCapturePhysicalVoice(br));
                        break;
                }
            }
            return packets;
        }

        static public uint EndianSwap(uint val)
        {
            return (((val & 0xff) << 24) | ((val & 0xff00) << 8) | ((val & 0xff0000) >> 8) | ((val & 0xff000000) >> 24));
        }

        static public UInt16 EndianSwap(UInt16 val)
        {
            return (UInt16)(((val & 0xff) << 8) | ((val & 0xff00) >> 8));
        }

        unsafe static public float EndianSwap(float val)
        {
            float ret = val;
            uint *p = (uint*)&ret;
            *p = EndianSwap(*p);
            return ret;
        }
    };

}