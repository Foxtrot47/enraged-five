using System;
using System.Collections;
using System.Xml;

namespace audWaveSlotBuild
{
	public enum audLoadableType
	{
		BANK,
		PACK,
        BANKFOLDER
	}
	/// <summary>
	/// Summary description for audLoadable.
	/// </summary>
	public class audLoadable
	{
		public readonly audLoadableType LoadableType;
		public readonly string Name;

		private XmlDocument m_BuiltWaves;

		public audLoadable(XmlNode node, XmlDocument builtWaves)
		{
			m_BuiltWaves = builtWaves;
			Name = node.Attributes["name"].Value;
			LoadableType = (audLoadableType)Enum.Parse(typeof(audLoadableType), node.Attributes["type"].Value, true);
		}

		public audWave[] GetLoadableWaves()
		{
			ArrayList al = new ArrayList();
			audBank[] loadableBanks = GetLoadableBanks();
			foreach(audBank b in loadableBanks)
			{
				al.AddRange(b.Waves);
			}

			return (audWave[])al.ToArray(typeof(audWave));
		}

        private audBank[] GetBanksFromFolder(XmlNode parent, string packName, string packFolderName)
        {
            ArrayList al = new ArrayList();
            foreach (XmlNode child in parent.ChildNodes)
            {
                System.Diagnostics.Debug.WriteLine(child.Attributes["name"].Value);
                if (child.Attributes["name"].Value == packFolderName)
                {
                    al.AddRange(GetBanksFromParentNode(child, packName));
                }
            }
            return (audBank[])al.ToArray(typeof(audBank));
        }

		private audBank[] GetBanksFromParentNode(XmlNode parent, string packName)
		{
			ArrayList al = new ArrayList();
			if(parent != null)
			{
				foreach(XmlNode child in parent.ChildNodes)
				{
					if(child.Name == "Bank")
					{
						al.Add(new audBank(child, packName));
					}
					else if(child.Name == "BankFolder")
					{
						al.AddRange(GetBanksFromParentNode(child, packName));
					}
				}
			}
			return (audBank[])al.ToArray(typeof(audBank));
		}

		public audBank[] GetLoadableBanks()
		{
			
           if (LoadableType == audLoadableType.PACK)
            {
                XmlNode packNode = FindChildXmlNode(m_BuiltWaves.DocumentElement, "Pack", Name);
                return GetBanksFromParentNode(packNode, Name);
            }

            else
            {
               //split the name to get pack name and element name
                string[] pathElems = Name.Split('\\');
                XmlNode packNode = FindChildXmlNode(m_BuiltWaves.DocumentElement, "Pack", pathElems[0]);

                if (LoadableType == audLoadableType.BANK)
                {
                    audBank[] banks = GetBanksFromParentNode(packNode, pathElems[0]);

                    foreach (audBank bank in banks)
                    {
                        if (bank.Name == pathElems[1])
                        {
                            return new audBank[] { bank };
                        }
                    }
                }
                else
                {
                    return GetBanksFromFolder(packNode, pathElems[0], pathElems[1]);
                }
            }
            //no banks to return
            return new audBank[] { };
		}

		private XmlNode FindChildXmlNode(XmlNode parent, string nodeType, string nodeName)
		{
			if(parent != null)
			{
				foreach(XmlNode child in parent.ChildNodes)
				{
					if(child.Name == nodeType && child.Attributes["name"].Value == nodeName)
					{
						return child;
					}
				}
			}
			return null;
		}
	}
}
