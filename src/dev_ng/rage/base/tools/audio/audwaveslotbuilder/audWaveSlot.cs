using System;
using System.Collections;
using System.Xml;
using System.Collections.Generic;

namespace audWaveSlotBuild
{
    public enum audLoadType
    {
        BANK,
        WAVE,
        STREAM,
        BUFFER,
    }

	/// <summary>
	/// Summary description for audWaveSlot.
	/// </summary>
	public class audWaveSlot
	{
		public readonly string Name;
		public readonly audLoadType LoadType;
		public readonly audLoadable[] LoadableElements;
		public readonly int Size;

        //used if manually sized
        private int m_MaxSlotSize = 0;
        private int m_MaxHeaderSize = 0;

        private int m_NoOfElements = 0;
        private int m_TotalSize = 0;
        private int m_MaxElementSize = 0;
        private string m_LargestElementName = "";
        private ArrayList m_Elements;
        private List<KeyValuePair<string, int>> m_BankHeaders;
        
        private int m_SlotSize = 0;
        private int m_HeaderSize = 0;
        private audBank m_StaticBank;
        private int m_NoOfLoadableBanks = 0;
        private bool m_bIsValid = true;
        private static int sm_MaxLoadWaveHeaderSize;

        static public void InitClass()
        {
            sm_MaxLoadWaveHeaderSize = 0;
        }

		public audWaveSlot(XmlNode node, XmlDocument builtWaves, string buildPlatform)
		{
            m_Elements = new ArrayList();
            m_BankHeaders = new List<KeyValuePair<string, int>>();
			Name = node.Attributes["name"].Value;
			LoadType = (audLoadType)Enum.Parse(typeof(audLoadType), node.Attributes["loadType"].Value, true);
            
            if (LoadType == audLoadType.BUFFER)
            {
                Size = Int32.Parse(node.Attributes["size"].Value);
            }

            ArrayList al = new ArrayList();
			foreach(XmlNode n in node.ChildNodes)
			{
				if(n.Name == "Loadable")
				{
					al.Add(new audLoadable(n, builtWaves));
				}
                //check if wave slot has been manually sized
                else if (n.Name == "Size")
                {
                    //ensure platforms match
                    if (n.Attributes["platform"].Value.ToUpper() == buildPlatform.ToUpper())
                    {
                        //Set max header size
                        try
                        {
                            m_MaxHeaderSize = Int32.Parse(n.Attributes["maxHeaderSize"].Value);
                        }
                        catch
                        {
                            throw new audBuildCommon.audBuildException("The MaxHeaderSize attribute in slot " + Name + " is NAN");
                        }

                        //Set max wave slot size
                        try
                        {
                            m_MaxSlotSize = Int32.Parse(n.Attributes["maxSlotSize"].Value);
                        }
                        catch
                        {
                            throw new audBuildCommon.audBuildException("The MaxSize attribute in slot " + Name + " is NAN");
                        }
                    }
                }
			}
			LoadableElements = (audLoadable[])al.ToArray(typeof(audLoadable));
		}

        public audWaveSlot(string name, int slotSize, audLoadType loadType)
        {
            //A hacky constructor for runtime generation of wave slots (scratch header wave slot for example.)
            Name = name;
            m_SlotSize = slotSize;
            LoadType = loadType;
        }

		public audWave[] GetLoadableWaves()
		{
			ArrayList al = new ArrayList();
			foreach(audLoadable loadable in LoadableElements)
			{
				al.Add(loadable.GetLoadableWaves());
			}

			return (audWave[])al.ToArray(typeof(audWave));
		}

		public audBank[] GetLoadableBanks()
		{
			ArrayList al = new ArrayList();
			foreach(audLoadable loadable in LoadableElements)
			{
				al.AddRange(loadable.GetLoadableBanks());
			}

			return(audBank[])al.ToArray(typeof(audBank));
		}

        public int SlotSize
        {
            get { return m_SlotSize; }
        }

        public ArrayList Elements
        {
            get { return m_Elements; }
        }

        public int AverageSize
        {
            get
            {
                if (m_TotalSize != 0 && m_NoOfElements != 0)
                {
                    return m_TotalSize / m_NoOfElements;
                }
                return 0;
            }
        }

        public int LargestElementSize
        {
            get { return m_MaxElementSize; }
        }

        public string LargestElement
        {
            get { return m_LargestElementName; }
        }

        public List<KeyValuePair<string, int>> BankHeaders
        {
            get
            {
                return m_BankHeaders;
            }
        }

		public void ComputeSize()
		{
			int headerSize;

			audBank[] loadableBanks = GetLoadableBanks();

			// MaxHeaderSize should be the largest header size from the set of
			// loadable banks for this slot
			foreach(audBank bank in loadableBanks)
			{
				headerSize = bank.GetHeaderSize();
                m_BankHeaders.Add(new KeyValuePair<string,int>(bank.Name, headerSize));

				if(headerSize > m_HeaderSize)
				{
					m_HeaderSize = headerSize;                    
				}
			}
            //check if manually sized
            if (m_MaxHeaderSize != 0)
            {
                if (m_MaxHeaderSize < m_HeaderSize)
                {
                    throw new audBuildCommon.audBuildException("Header size is too large for manually sized wave slot " + Name +
                                                                ". Max Size:" + m_MaxHeaderSize + " Actual Size:" + m_HeaderSize);
                }
                else
                {
                    m_HeaderSize = m_MaxHeaderSize;
                }
            }

            m_NoOfLoadableBanks = loadableBanks.Length;

			if(LoadType == audLoadType.BANK)
			{
               if (m_NoOfLoadableBanks == 1)
				{
					// this is a static bank
                    m_StaticBank = loadableBanks[0];
                    m_MaxElementSize = m_TotalSize  =  m_SlotSize = m_StaticBank.GetRequiredSizeForBankLoad();
                    m_LargestElementName = loadableBanks[0].Name;
                    m_NoOfElements = 1;
                    m_Elements.Add(new StringIntItem(m_LargestElementName, m_MaxElementSize));
				}
                else if (m_NoOfLoadableBanks > 1)
				{
					// this is a dynamic bank
					int bankSize;

					foreach(audBank bank in loadableBanks)
					{
						bankSize = bank.GetRequiredSizeForBankLoad();
                        m_NoOfElements++;
                        m_TotalSize += bankSize;
                        m_Elements.Add(new StringIntItem(bank.Name, bankSize));
						if(bankSize > m_SlotSize)
						{
                           m_SlotSize = bankSize;
                           m_MaxElementSize = bankSize;
                           m_LargestElementName = bank.Name;
						}
					}                

				}
				else
				{
					// this slot has no banks associated with it - don't write it out
					m_bIsValid = false;
				}
			}
			else if(LoadType == audLoadType.WAVE)
			{
               	// find the largest loaded wave size for each bank and use that
				int waveSize;
                string waveName;
                ArrayList largestElements = new ArrayList();
                foreach (audBank bank in loadableBanks)
                {

                    ArrayList waves = bank.GetWaves();
                    foreach (StringIntItem si in waves)
                    {
                        if (largestElements.Count == 0)
                        {
                            largestElements.Add(si);
                        }
                        else
                        {
                            int i = 0;
                            bool bInserted = false;
                            //create list of largest waves
                            while(!bInserted)
                            {
                                //if larger than what is currently at i, insert at i
                                //elements should be in order
                                if (((StringIntItem)si).Value > ((StringIntItem)largestElements[i]).Value)
                                {
                                    largestElements.Insert(i, si);
                                    bInserted = true;
                                }
                                //if its not greater than the last item and the number of item
                                //is less that 50.. add the item on
                                else if (i == largestElements.Count - 1 && largestElements.Count<=50)
                                {
                                    largestElements.Add(si);
                                    bInserted = true;
                                }
                                //if it's not greater than the last one and there are 50 item
                                //Do not bother to add
                                else if (i == largestElements.Count - 1 && largestElements.Count > 50)
                                {
                                    bInserted = true;
                                }
                                i++;
                               
                            }
                            //remove any elements not in the top 50
                            if (largestElements.Count > 50)
                            {
                                largestElements.RemoveRange(50, largestElements.Count - 50);
                            }
                        }
                    }
                    waveSize = bank.GetLargestLoadedWaveSize();
                    waveName = bank.GetLargestLoadedWaveName();
                    m_NoOfElements++;
                    m_TotalSize += waveSize;
                   // m_Elements.Add(new StringInt(bank.Name + "/" + waveName, waveSize));
                    if (waveSize > m_SlotSize)
                    {
                        m_SlotSize = waveSize;
                        m_MaxElementSize = waveSize;
                        m_LargestElementName = bank.Name + "/" + waveName;
                    }
                }

                m_Elements = largestElements;
                m_SlotSize = (m_SlotSize + 2048 - (m_SlotSize % 2048));

                //NOTE: This header size clamp was removed as we are now using the scratch header wave slot.
                // In the case of large numbers of wave files within a bank, it may 
                // happen that the sum of all the metadata will exceed the largest single
                // wave file.  If this happens, we'll size to the header.
                //if (m_HeaderSize > m_SlotSize)
                //    m_SlotSize = m_HeaderSize;

                
                //Store the max header size for across all audLoadType.WAVE slots so we can size the scratch header wave slot.
                if (m_HeaderSize > sm_MaxLoadWaveHeaderSize)
                {
                    sm_MaxLoadWaveHeaderSize = m_HeaderSize;
                }
			}
			else if(LoadType == audLoadType.STREAM)
			{
                int streamSize;
				foreach(audBank bank in loadableBanks)
				{
					streamSize = bank.GetRequiredSizeForStreamBank();
					if(streamSize % 2048 != 0)
					{
						throw new audBuildCommon.audBuildException("streamingBlockBytes must be a multiple of 2048!");
					}
                    m_NoOfElements++;
                    m_TotalSize += streamSize;
                    m_Elements.Add(new StringIntItem(bank.Name, streamSize));
					if(streamSize > m_SlotSize)
					{
						m_SlotSize = streamSize;
                        m_MaxElementSize = streamSize;
					}
				}
			}
			else if(LoadType == audLoadType.BUFFER)
			{
                m_MaxElementSize = m_TotalSize = m_SlotSize = Size;
                m_NoOfElements = 1;
			}

            if (m_MaxSlotSize != 0)
            {
                if (m_MaxSlotSize < m_SlotSize)
                {
                    throw new audBuildCommon.audBuildException("Wave slot size is too large for manually sized wave slot " + Name +
                                                        ". Max Size:" + m_MaxSlotSize + " Actual Size:" + Size);
                }
                else
                {
                    m_SlotSize = m_MaxSlotSize;
                }
            }
		}


        public void Serialise(XmlNode parentNode)
        {
            XmlNode slotNode = parentNode.OwnerDocument.CreateElement("Slot");

            AppendStringNode(slotNode, "Name", Name);
                      
            AppendIntNode(slotNode, "MaxHeaderSize", m_HeaderSize);

            AppendStringNode(slotNode, "LoadType", LoadType.ToString());

            if (LoadType == audLoadType.BANK)
            {
                if (m_NoOfLoadableBanks == 1)
                {
                    AppendStringNode(slotNode, "StaticBank", m_StaticBank.GetPath());
                    AppendIntNode(slotNode, "Size", m_SlotSize);
                }
                else if (m_NoOfLoadableBanks > 1)
                {
                    AppendIntNode(slotNode, "Size", m_SlotSize);
                }
            }
            else
            {
                AppendIntNode(slotNode, "Size", m_SlotSize);
            }

            if (m_bIsValid)
            {
                parentNode.AppendChild(slotNode);
            }
        }

		private void AppendStringNode(XmlNode parentNode, string nodeName, string nodeValue)
		{
			XmlNode n = parentNode.OwnerDocument.CreateElement(nodeName);
			n.Attributes.Append(parentNode.OwnerDocument.CreateAttribute("content"));
			n.Attributes["content"].Value = "ascii";
			n.InnerText = nodeValue;
			parentNode.AppendChild(n);
		}

		private void AppendIntNode(XmlNode parentNode, string nodeName, int nodeValue)
		{
			XmlNode n = parentNode.OwnerDocument.CreateElement(nodeName);
			n.Attributes.Append(parentNode.OwnerDocument.CreateAttribute("value"));
			n.Attributes["value"].Value = nodeValue.ToString();
			parentNode.AppendChild(n);
		}

        public static void SerialiseScratchWaveSlotHeader(XmlNode parentNode)
        {
            audWaveSlot waveSlot = new audWaveSlot("SCRATCH_HEADER_WAVE_SLOT", sm_MaxLoadWaveHeaderSize, audLoadType.WAVE);
            waveSlot.Serialise(parentNode);
        }
	}

    public class StringIntItem
    {
        private string m_Text;
        private int m_Value;

        public StringIntItem(string text, int value)
        {
            m_Text = text;
            m_Value = value;
        }

        public String Text
        {
       
           get{ return m_Text;}
        }

        public int Value
        {
            get{return m_Value;}
        }


    }
}
