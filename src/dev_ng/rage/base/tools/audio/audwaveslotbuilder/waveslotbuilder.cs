using System;
using System.Xml;

using audBuildCommon;
using audAssetManagement;
using rage;

namespace audWaveSlotBuild
{
	/// <summary>
	/// Summary description for waveslotbuilder.
	/// </summary>
	public class audWaveSlotBuilder : audPostBuilderBase
	{
		public audWaveSlotBuilder()
		{
			
		}

		public override void Build(audAssetManagement.audAssetManager assetMgr, rage.audProjectSettings projectSettings,
			string buildPlatform, audBuildClient buildClient, XmlDocument builtWavesXml, XmlDocument pendingWavesXml, bool shouldBuildLocally,
            audBuildComponent buildComponent, bool isDeferredBuild)
		{
			string waveSlotsAssetPath = projectSettings.GetBuildOutputPath() + "config\\waveslots.xml";
			string waveSlotsWorkingPath = assetMgr.GetWorkingPath(waveSlotsAssetPath);
			buildClient.ReportProgress(-1,-1,"Starting Wave Slot Build", true);

			if(!assetMgr.GetLatest(projectSettings.GetWaveSlotSettings()))
			{
				throw new audBuildException("Failed to get latest wave slot settings Xml file.  Check project settings.");
			}

            try
            {
                audWaveSlot.InitClass();

                audWaveSlotSettings waveSlotSettings = new audWaveSlotSettings(assetMgr.GetWorkingPath(projectSettings.GetWaveSlotSettings()), builtWavesXml, buildPlatform);
            
			    assetMgr.SimpleCheckOut(waveSlotsAssetPath, "audWaveSlotBuilder");

			
				XmlDocument waveSlotsDoc = new XmlDocument();

				waveSlotsDoc.AppendChild(waveSlotsDoc.CreateElement("WaveSlots"));

                int totalSlotSize = 0;
                foreach (audWaveSlot slot in waveSlotSettings.WaveSlots)
                {
                    slot.ComputeSize();
                    if (slot.Name != "TEST")
                    {
                        totalSlotSize += slot.SlotSize;
                    }
                }

                //if total slot size isn't specified or if its less than the specified max
                if (projectSettings.GetCurrentPlatform().MaxWaveMemory == 0 || totalSlotSize <= projectSettings.GetCurrentPlatform().MaxWaveMemory)
                {
                    foreach (audWaveSlot slot in waveSlotSettings.WaveSlots)
                    {
                        slot.Serialise(waveSlotsDoc.DocumentElement);
                    }

                    //Auto-generate the custom wave slot used as scratch space for storing the bank header when loading an individual wave.
                    audWaveSlot.SerialiseScratchWaveSlotHeader(waveSlotsDoc.DocumentElement);
                }
                else
                {
                    throw new audBuildException("Wave Memory Limit Exceeded! Memory Limit: " + 
                        projectSettings.GetCurrentPlatform().MaxWaveMemory + "   " + "Memory Used: " + totalSlotSize);
                }

				waveSlotsDoc.Save(waveSlotsWorkingPath);
				assetMgr.CheckInOrImport(waveSlotsWorkingPath, waveSlotsAssetPath, "audWaveSlotBuilder");
				buildClient.ReportProgress(-1,-1,"Finished Wave Slot Build", true);
			}
			catch(Exception)
			{
				assetMgr.UndoCheckOut(waveSlotsAssetPath);
				throw;
			}
		}
	}
}
