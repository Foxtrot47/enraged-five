using System;
using System.Collections;
using System.Xml;
using System.Collections.Generic;

namespace audWaveSlotBuild
{
	/// <summary>
	/// Summary description for audBank.
	/// </summary>
	public class audBank
	{
		public readonly string Name;
		public readonly int MetadataSize, WaveMetadataLookupSize, CustomMetadataLookupSize;
		public readonly audWave[] Waves;
		public readonly int StreamingBlockBytes;

		private string m_PackName;

		private audWave[] GetWavesFromNode(XmlNode bank)
		{
			ArrayList al = new ArrayList();
			if(bank != null)
			{
				foreach(XmlNode child in bank.ChildNodes)
				{
					if(child.Name == "Wave")
					{
						al.Add(new audWave(child));
					}
					else if(child.Name == "WaveFolder")
					{
						al.AddRange(GetWavesFromNode(child));
					}
				}
			}
			return (audWave[])al.ToArray(typeof(audWave));
		}

		private int FindStreamingBlockBytesTag(XmlNode parentNode)
		{
			if(parentNode!=null)
			{
				foreach(XmlNode child in parentNode.ChildNodes)
				{
					if(child.Name == "Tag" && child.Attributes["name"].Value == "streamingBlockBytes")
					{
						return Int32.Parse(child.Attributes["value"].Value);
					}
				}

				// no match found, try searching up
				if(parentNode.ParentNode != null)
				{
					return FindStreamingBlockBytesTag(parentNode.ParentNode);
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;
			}
		}

		public audBank(XmlNode node, string packName)
		{
			Name = node.Attributes["name"].Value;

			MetadataSize = Int32.Parse(node.Attributes["metadataSize"].Value);
			WaveMetadataLookupSize = Int32.Parse(node.Attributes["waveMetadataLookupSize"].Value);
            if (node.Attributes["customMetadataLookupSize"] != null)
            {
                CustomMetadataLookupSize = Int32.Parse(node.Attributes["customMetadataLookupSize"].Value);
            }
            else
            {
                CustomMetadataLookupSize = 0;
            }
			Waves = GetWavesFromNode(node);
			m_PackName = packName;

			StreamingBlockBytes = FindStreamingBlockBytesTag(node);
		}

		// PURPOSE
		//	Returns the size in bytes required to load this entire bank
		//	This is defined as:
		//		bankHeaderSize + the sum of each waves built size
		public int GetRequiredSizeForBankLoad()
		{
			int waveBuiltSizeSum = 0;

			foreach(audWave wave in Waves)
			{
				waveBuiltSizeSum += wave.BuiltSize;
			}

			return waveBuiltSizeSum + GetHeaderSize();
		}

		// PURPOSE
		//	Returns the bank header size
		//	bankHeaderSize = waveMetadataLookupSize + customMetadataLookupSize + MetadataSize + 
		//							the sum of each waves metadata size
		public int GetHeaderSize()
		{
			int waveMetadataSizeSum = 0, headerSize;
            
			foreach(audWave wave in Waves)
			{
				waveMetadataSizeSum += wave.MetadataSize;
			}

            //System.Diagnostics.Debug.WriteLine(this.Name+": "+ waveMetadataSizeSum);
			headerSize = 
				waveMetadataSizeSum + WaveMetadataLookupSize + CustomMetadataLookupSize + MetadataSize;

			// align header size to 2k
			return (headerSize + 2048 - (headerSize%2048));
		}


        public string GetLargestLoadedWaveName()
        {
            string name = "";
            int largestSize = 0, waveSize;
            foreach (audWave w in Waves)
            {
                waveSize = GetLoadedWaveSize(w);
                if (waveSize > largestSize)
                {
                    largestSize = waveSize;
                    name = w.Name;
                }
            }
            return name;

        }

        public ArrayList GetWaves()
        {
            ArrayList WaveList = new ArrayList();
            foreach(audWave wave in Waves)
            {
                WaveList.Add(new StringIntItem(this.Name+"/"+wave.Name,GetLoadedWaveSize(wave)));
            }
            return WaveList;
        }

		public int GetLargestLoadedWaveSize()
		{
			int largestSize = 0, waveSize;
			foreach(audWave w in Waves)
			{	
				waveSize = GetLoadedWaveSize(w);
				if(waveSize > largestSize)
				{
					largestSize = waveSize;
				}
			}
			return largestSize;
		}

		// PURPOSE
		//	Returns the path of this bank in the form PACK\BANK
		public string GetPath()
		{
			return m_PackName + "\\" + Name;
		}

		private int GetLoadedWaveSize(audWave wave)
		{
			return wave.BuiltSize + wave.MetadataSize + ((WaveMetadataLookupSize/Waves.Length) + CustomMetadataLookupSize + MetadataSize);
		}

		public int GetRequiredSizeForStreamBank()
		{
            return (StreamingBlockBytes * 2) + GetHeaderSize();
		}
	}
}
