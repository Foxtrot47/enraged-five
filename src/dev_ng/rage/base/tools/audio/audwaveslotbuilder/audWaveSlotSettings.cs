using System;
using System.Xml;
using System.Collections;
using audBuildCommon;

namespace audWaveSlotBuild
{
	/// <summary>
	/// Summary description for audWaveSlotSettings.
	/// </summary>
	public class audWaveSlotSettings
	{
		public readonly audWaveSlot[] WaveSlots;

		public audWaveSlotSettings(string path, XmlDocument builtWaves, string buildPlatform)
		{
			XmlDocument doc = new XmlDocument();
			doc.Load(path);

			ArrayList al = new ArrayList();
			foreach(XmlNode n in doc.DocumentElement.ChildNodes)
			{
				if(n.Name == "Slot")
				{
                    audWaveSlot temp = new audWaveSlot(n, builtWaves, buildPlatform);
                    bool duplicateName = false;
                    foreach (audWaveSlot aws in al)
                    {
                        if (aws.Name == temp.Name)
                        {
                            duplicateName = true;
                        }
                    }

                    if (!duplicateName)
                    {
                        al.Add(temp);
                    }
                    else
                    {
                        throw new audBuildException("Duplicate Wave Slot Name Detected: " + temp.Name);
                    }					
				}
			}

			WaveSlots = (audWaveSlot[])al.ToArray(typeof(audWaveSlot));
		}
	}
}
