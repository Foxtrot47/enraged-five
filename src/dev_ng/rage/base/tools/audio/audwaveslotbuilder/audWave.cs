using System;
using System.Collections;
using System.Xml;

namespace audWaveSlotBuild
{
	/// <summary>
	/// Summary description for audWave.
	/// </summary>
	public class audWave
	{
		public readonly string Name;
		public readonly int RawSize;
		public readonly int BuiltSize;
		public readonly int MetadataSize;
		
		public audWave(XmlNode node)
		{
			Name = node.Attributes["name"].Value;
			RawSize = Int32.Parse(node.Attributes["rawSize"].Value);
			BuiltSize = Int32.Parse(node.Attributes["builtSize"].Value);
			MetadataSize = Int32.Parse(node.Attributes["metadataSize"].Value);
		}
	}
}
