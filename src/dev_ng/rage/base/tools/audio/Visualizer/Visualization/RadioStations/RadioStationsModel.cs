using System;
using System.Collections.Generic;
using rage.ToolLib;
using Visualization.Core;

namespace Visualization.RadioStations
{
    public class RadioStationsModel : IRadioStationsModel
    {
        public List<RadioStationData> m_testData = new List<RadioStationData>
                                                       {
                                                           new RadioStationData
                                                               {
                                                                   CurrentTrack = "TrackA",
                                                                   PlayTime = TimeSpan.FromSeconds(10),
                                                                   State = "Virtual"
                                                               },
                                                           new RadioStationData
                                                               {
                                                                   CurrentTrack = "TrackB",
                                                                   PlayTime = TimeSpan.FromSeconds(6),
                                                                   State = "Virtual"
                                                               },
                                                           new RadioStationData
                                                               {
                                                                   CurrentTrack = "TrackC",
                                                                   PlayTime = TimeSpan.FromSeconds(1),
                                                                   State = "Virtual"
                                                               },
                                                           new RadioStationData
                                                               {
                                                                   CurrentTrack = "TrackD",
                                                                   PlayTime = TimeSpan.FromSeconds(4.7),
                                                                   State = "Physical"
                                                               },
                                                       };

        #region IRadioStationsModel Members

        public event Action<IList<RadioStationData>> DataChanged;

        public bool Init()
        {
            DataChanged.Raise(m_testData);
            return true;
        }

        public void Reset()
        {
            //DataChanged.Raise(null);
            DataChanged.Raise(m_testData);
        }

        public void Visualize(VisualizerPacket packet)
        {
            DataChanged.Raise(m_testData);
        }

        #endregion
    }
}