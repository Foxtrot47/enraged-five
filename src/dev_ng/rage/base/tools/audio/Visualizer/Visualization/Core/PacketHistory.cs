using System;
using System.Collections.Generic;
using System.Linq;

namespace Visualization.Core
{
    public class PacketHistory : IPacketHistory
    {
        private List<VisualizerPacket> m_historyList;
        private bool m_needsSort;

        public PacketHistory()
        {
            Clear();
        }

        private TimeSpan ZeroTime
        {
            get
            {
                if (m_historyList.Count > 0)
                {
                    Sort();
                    return m_historyList[0].TimeStamp;
                }
                return TimeSpan.Zero;
            }
        }

        #region IPacketHistory Members

        public bool IsEmpty
        {
            get { return m_historyList.Count == 0; }
        }

        public TimeSpan EndTime
        {
            get
            {
                if (m_historyList.Count > 0)
                {
                    Sort();
                    return m_historyList[m_historyList.Count - 1].TimeStamp - ZeroTime;
                }
                return TimeSpan.Zero;
            }
        }

        public IEnumerable<VisualizerPacket> Packets
        {
            get
            {
                Sort();
                return m_historyList;
            }
        }

        public IEnumerable<VisualizerPacket> GetPacketsBetween(TimeSpan start, TimeSpan end)
        {
            end += ZeroTime;
            start += ZeroTime;
            return m_historyList.Where(packet => packet.TimeStamp >= start && packet.TimeStamp <= end);
        }

        public void Add(VisualizerPacket packet)
        {
            m_historyList.Add(packet);
            m_needsSort = true;
        }

        public void Clear()
        {
            m_historyList = new List<VisualizerPacket>();
            m_needsSort = false;
        }

        #endregion

        private void Sort()
        {
            if (m_needsSort)
            {
                m_historyList.Sort(SortComparer);
                m_needsSort = false;
            }
        }

        private static int SortComparer(VisualizerPacket a, VisualizerPacket b)
        {
            if (a.TimeStamp <
                b.TimeStamp)
            {
                return -1;
            }
            if (a.TimeStamp >
                b.TimeStamp)
            {
                return 1;
            }
            return 0;
        }
    }
}