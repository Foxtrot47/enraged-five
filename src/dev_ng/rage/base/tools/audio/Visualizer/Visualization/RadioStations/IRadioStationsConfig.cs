namespace Visualization.RadioStations
{
    public interface IRadioStationsConfig
    {
        string Root { get; set; }
    }
}