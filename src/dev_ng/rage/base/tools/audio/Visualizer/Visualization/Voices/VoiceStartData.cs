namespace Visualization.Voices
{
    public class VoiceStartData
    {
        public uint NumWaveSlots { get; set; }
        public uint WaveSlotTableSize { get; set; }
        public uint NumVirtualVoices { get; set; }
    }
}