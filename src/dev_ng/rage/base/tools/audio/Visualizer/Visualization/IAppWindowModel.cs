using System;
using Visualization.Core;

namespace Visualization
{
    public interface IAppWindowModel : IDisposable
    {
        bool RequiresSave { get; }
        event Action<AppWindowMode> ModeChanged;
        event Action<IVisualizer> VisualizerActivated;
        event Action Connected;
        event Action Disconnected;
        event Action Exit;

        bool Start();

        bool SwitchToReplay(string file);

        void SwitchToLive();

        void ActivateVisualizer(string visualizerId);

        bool SaveHistory(string fileName);

        void Stop();
    }
}