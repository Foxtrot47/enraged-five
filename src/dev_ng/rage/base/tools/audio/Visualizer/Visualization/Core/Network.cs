using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using rage;
using rage.ToolLib;
using rage.ToolLib.Networking;
using Utility = rage.ToolLib.Utility;

namespace Visualization.Core
{
    public class Network : INetwork
    {
        private readonly INetworkTiming m_networkTiming;
        private readonly audProjectSettings m_projectSettings;
        private readonly byte[] m_receiptMsg;
        private readonly Queue<VisualizerPacket> m_receiveQueue;
        private readonly Queue<byte[]> m_sendQueue;
        private readonly IServer m_server;
        private int m_connectedCount;
        private Thread m_processingThread;

        public Network(IServer server, INetworkTiming networkTiming, audProjectSettings projectSettings)
        {
            if (server == null)
            {
                throw new ArgumentNullException("server");
            }
            if (networkTiming == null)
            {
                throw new ArgumentNullException("networkTiming");
            }
            if (projectSettings == null)
            {
                throw new ArgumentNullException("projectSettings");
            }
            m_server = server;
            m_networkTiming = networkTiming;
            m_projectSettings = projectSettings;

            m_receiveQueue = new Queue<VisualizerPacket>();
            m_sendQueue = new Queue<byte[]>();

            m_receiptMsg = new byte[VisualizerPacket.PacketHeaderSize];
            m_receiptMsg[0] = (byte) VisualizationType.MessageReceipt;
        }

        #region INetwork Members

        public event Action EndianChanged;
        public event Action Connected;
        public event Action Disconnected;
        public event Action DataArrived;
        public bool IsBigEndian { get; private set; }

        public bool IsConnected
        {
            get { return m_connectedCount > 0; }
        }

        public void Start()
        {
            if (m_processingThread == null ||
                !m_processingThread.IsAlive)
            {
                m_server.ClientConnected += OnClientConnected;
                m_server.ClientDisconnected += OnClientDisconnected;
                m_server.Start();

                m_processingThread = new Thread(Process) {Name = "Visualizer - Network"};
                m_processingThread.Start();
            }
        }

        public void Send(VisualizationType type, byte[] data, ushort length)
        {
            var dataLength = data == null ? 0 : data.Length;
            if (length > dataLength)
            {
                throw new ArgumentException(
                    string.Format(
                        "The length specified ({0} bytes} is greater than the size of the provided buffer ({1} bytes).",
                        length,
                        dataLength));
            }

            var packet = CreateOutgoingPacket(type, data, length);
            lock (m_sendQueue)
            {
                m_sendQueue.Enqueue(packet);
            }
        }

        public VisualizerPacket Receive()
        {
            VisualizerPacket nextPacket = null;
            lock (m_receiveQueue)
            {
                if (m_receiveQueue.Count > 0)
                {
                    nextPacket = m_receiveQueue.Dequeue();
                }
            }
            return nextPacket;
        }

        public void Stop()
        {
            if (m_processingThread != null &&
                m_processingThread.IsAlive)
            {
                m_processingThread.Abort();
                m_processingThread = null;

                m_server.Stop();
                m_server.ClientConnected -= OnClientConnected;
                m_server.ClientDisconnected -= OnClientDisconnected;
            }
        }

        public void Dispose()
        {
            Stop();
        }

        #endregion

        private void OnClientDisconnected(IClient client)
        {
            m_connectedCount -= 1;
            if (!IsConnected)
            {
                Disconnected.Raise();
            }
        }

        private void OnClientConnected(IClient client)
        {
            var wasConnected = IsConnected;
            m_connectedCount += 1;
            if (!wasConnected && IsConnected)
            {
                Connected.Raise();
            }
        }

        private void Process()
        {
            while (true)
            {
                m_server.Update();
                if (IsConnected)
                {
                    SendData();
                    ReceiveData();
                }
                Thread.Sleep(100);
            }
        }

        private void ReceiveData()
        {
            foreach (var client in m_server.Clients)
            {
                byte[] buffer;
                while ((buffer = client.Read(VisualizerPacket.PacketHeaderSize)) != null)
                {
                    var visualizerPacket = new VisualizerPacket(buffer, 0, IsBigEndian);
                    if (visualizerPacket.Type ==
                        VisualizationType.SetRuntimePlatform)
                    {
                        HandleSetRuntimePlatformCommand(client, visualizerPacket);
                    }
                    else if (visualizerPacket.Type != VisualizationType.Ping &&
                             visualizerPacket.Type != VisualizationType.MessageReceipt)
                    {
                        HandleVisualizerPacket(client, visualizerPacket);
                    }
                }
            }
        }

        private void SendData()
        {
            int count;
            lock (m_sendQueue)
            {
                count = m_sendQueue.Count;
            }

            while (count > 0)
            {
                byte[] packetData;
                lock (m_sendQueue)
                {
                    packetData = m_sendQueue.Dequeue();
                }
                foreach (var client in m_server.Clients)
                {
                    client.Write(packetData, 0, packetData.Length);
                }
                count -= 1;
            }
        }

        private void HandleVisualizerPacket(IClient client, VisualizerPacket visualizerPacket)
        {
            if (ReadPacketContent(client, visualizerPacket))
            {
                lock (m_receiveQueue)
                {
                    m_receiveQueue.Enqueue(visualizerPacket);
                    DataArrived.Raise();
                }
            }
        }

        private static bool ReadPacketContent(IClient client, VisualizerPacket visualizerPacket)
        {
            var buffer = client.Read(visualizerPacket.Size);
            if (buffer != null)
            {
                visualizerPacket.SetData(buffer);
                return true;
            }
            return false;
        }

        private void HandleSetRuntimePlatformCommand(IClient client, VisualizerPacket visualizerPacket)
        {
            if (ReadPacketContent(client, visualizerPacket))
            {
                var builder = new StringBuilder();
                var data = visualizerPacket.Data;
                for (ushort i = 0; i < visualizerPacket.Size; ++i)
                {
                    builder.Append((char) data.ReadByte());
                }
                m_projectSettings.SetCurrentPlatformByTag(builder.ToString());
                var isBigEndian = m_projectSettings.IsBigEndian();
                if (isBigEndian != IsBigEndian)
                {
                    IsBigEndian = isBigEndian;
                    EndianChanged.Raise();
                }
                client.Write(WriteTimestamp(m_receiptMsg), 0, VisualizerPacket.PacketHeaderSize);
            }
        }

        private byte[] WriteTimestamp(byte[] packetData)
        {
            var timeStamp = m_networkTiming.GetTimestamp();
            var timeStampBytes = BitConverter.GetBytes(timeStamp);
            if (IsBigEndian)
            {
                timeStampBytes = Utility.Reverse(timeStampBytes);
            }
            Array.Copy(timeStampBytes, 0, packetData, 1, 4);
            return packetData;
        }

        private byte[] CreateOutgoingPacket(VisualizationType type, byte[] data, ushort length)
        {
            var packet = new byte[VisualizerPacket.PacketHeaderSize + length];
            packet[0] = (byte) type;

            WriteTimestamp(packet);

            var lengthBytes = BitConverter.GetBytes(length);
            if (IsBigEndian)
            {
                lengthBytes = Utility.Reverse(lengthBytes);
            }
            Array.Copy(lengthBytes, 0, packet, 5, 2);

            if (length > 0)
            {
                Array.Copy(data, 0, packet, 8, length);
            }
            return packet;
        }
    }
}