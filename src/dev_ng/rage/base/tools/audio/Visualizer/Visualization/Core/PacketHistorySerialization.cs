using System;
using System.IO;
using rage.ToolLib;
using rage.ToolLib.Logging;

namespace Visualization.Core
{
    public class PacketHistorySerialization : IPacketHistorySerialization
    {
        private static readonly byte[] ms_visFileHeader = new byte[4];
        private readonly ILog m_log;

        static PacketHistorySerialization()
        {
            ms_visFileHeader[0] = (byte) '.';
            ms_visFileHeader[1] = (byte) 'V';
            ms_visFileHeader[2] = (byte) 'I';
            ms_visFileHeader[3] = (byte) 'S';
        }

        public PacketHistorySerialization(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
        }

        #region IPacketHistorySerialization Members

        public bool Serialize(string fileName, IPacketHistory packetHistory)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                m_log.Exception(new ArgumentNullException("fileName"));
                return false;
            }

            if (packetHistory == null)
            {
                m_log.Exception(new ArgumentNullException("packetHistory"));
                return false;
            }

            if (!packetHistory.IsEmpty)
            {
                try
                {
                    using (var stream = File.Open(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        stream.Write(ms_visFileHeader, 0, 4);
                        foreach (var packet in packetHistory.Packets)
                        {
                            // Write packet header
                            stream.WriteByte((byte) packet.Type);
                            var milliseconds = (uint) packet.TimeStamp.TotalMilliseconds;
                            stream.Write(BitConverter.GetBytes(milliseconds), 0, 4);
                            stream.Write(BitConverter.GetBytes(packet.Size), 0, 2);
                            // using the unused byte to store endian flag
                            stream.WriteByte(packet.IsBigEndian ? (byte) 1 : (byte) 0);

                            // Write data
                            var buffer = new byte[packet.Size];
                            packet.Data.Read(buffer, 0, packet.Size);
                            stream.Write(buffer, 0, packet.Size);
                        }
                        stream.Flush();
                        stream.Close();
                    }
                }
                catch (Exception ex)
                {
                    m_log.Exception(ex);
                    return false;
                }
            }
            return true;
        }

        public bool Deserialize(string fileName, IPacketHistory packetHistory)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                m_log.Exception(new ArgumentNullException("fileName"));
                return false;
            }
            if (!File.Exists(fileName))
            {
                m_log.Exception(new ArgumentException(string.Format("File: {0} does not exist.", fileName)));
                return false;
            }
            if (packetHistory == null)
            {
                m_log.Exception(new ArgumentNullException("packetHistory"));
                return false;
            }

            packetHistory.Clear();

            var retVal = true;
            try
            {
                using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    var fileHeaderBuffer = new byte[4];
                    if ((!Utility.SafeReadData(stream, fileHeaderBuffer)) ||
                        (fileHeaderBuffer[0] != ms_visFileHeader[0] || fileHeaderBuffer[1] != ms_visFileHeader[1] ||
                         fileHeaderBuffer[2] != ms_visFileHeader[2] || fileHeaderBuffer[3] != ms_visFileHeader[3]))
                    {
                        m_log.Error("File: {0} is not a valid .VIS file.", fileName);
                        retVal = false;
                    }

                    if (retVal)
                    {
                        var headerBuffer = new byte[8];
                        while (Utility.SafeReadData(stream, headerBuffer))
                        {
                            // using the unused byte in the packet header to store endian flag
                            var visualizerPacket = new VisualizerPacket(headerBuffer, 0);
                            var buffer = new byte[visualizerPacket.Size];

                            if (!Utility.SafeReadData(stream, buffer))
                            {
                                m_log.Error("File: {0} is not a valid .VIS file.", fileName);
                                retVal = false;
                                break;
                            }

                            visualizerPacket.SetData(buffer);
                            packetHistory.Add(visualizerPacket);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                m_log.Exception(ex);
                retVal = false;
            }
            return retVal;
        }

        #endregion
    }
}