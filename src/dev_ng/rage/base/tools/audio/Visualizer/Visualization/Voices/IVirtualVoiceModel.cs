using System;
using System.Collections.Generic;
using Visualization.Core;

namespace Visualization.Voices
{
    public interface IVirtualVoiceModel
    {
        event Action<IList<VirtualVoiceData>> DataChanged;

        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets);
    }
}