using System;
using System.Collections.Generic;
using System.Linq;
using rage.ToolLib;
using Visualization.Core;

namespace Visualization.Voices
{
    public class PhysicalVoiceModel : IPhysicalVoiceModel
    {
        private readonly List<PhysicalVoiceData> m_data = new List<PhysicalVoiceData>();

        #region IPhysicalVoiceModel Members

        public event Action<IList<PhysicalVoiceData>> DataChanged;

        public bool Init()
        {
            return true;
        }

        public void Reset()
        {
            m_data.Clear();
            DataChanged.Raise(m_data);
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets)
        {
            m_data.Clear();
            CreateData(packets.Last());
            DataChanged.Raise(m_data);
        }

        #endregion

        private void CreateData(VisualizerPacket packet)
        {
            var data = packet.Data;
            while (data.Position <
                   packet.Size)
            {
                var voiceData = new PhysicalVoiceData();

                var buffer = new byte[4];
                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                voiceData.WaveNameHash = BitConverter.ToUInt32(buffer, 0);

                voiceData.WaveSlotIndex = (byte) data.ReadByte();
                voiceData.BucketId = (byte) data.ReadByte();
                voiceData.VoiceId = (byte) data.ReadByte();
                voiceData.PhysicalVoiceId = (byte) data.ReadByte();

                m_data.Add(voiceData);
            }
        }
    }
}