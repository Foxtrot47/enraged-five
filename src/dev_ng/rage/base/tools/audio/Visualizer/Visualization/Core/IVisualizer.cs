using System.Collections.Generic;
using rage.ToolLib.Repository;

namespace Visualization.Core
{
    public interface IVisualizer : IRepositoryItem<string>
    {
        /// <summary>
        ///   The type that this object can visualize.
        /// </summary>
        IEnumerable<VisualizationType> Types { get; }

        /// <summary>
        ///   Initialize.
        /// </summary>
        /// <returns>true if successful, false otherwise.</returns>
        bool Init();

        /// <summary>
        ///   Instructs the visualizer to reset the state of visualization to the initial state.
        /// </summary>
        void Reset();

        /// <summary>
        ///   Parse and update with new data.
        /// </summary>
        /// <param name = "packets">Timestamp ordered collection of packets to visualize.</param>
        /// <param name = "shouldReset">True if the visualizer should clear out its internal state before visualizing the packets, false otherwise.</param>
        void Visualize(IEnumerable<VisualizerPacket> packets, bool shouldReset);

        /// <summary>
        ///   Shutdown
        /// </summary>
        void Shutdown();
    }
}