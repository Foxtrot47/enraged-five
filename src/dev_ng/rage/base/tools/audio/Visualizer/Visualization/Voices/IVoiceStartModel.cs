using System;
using Visualization.Core;

namespace Visualization.Voices
{
    public interface IVoiceStartModel
    {
        event Action<VoiceStartData> DataChanged;

        void Reset();

        void Visualize(VisualizerPacket packet);
    }
}