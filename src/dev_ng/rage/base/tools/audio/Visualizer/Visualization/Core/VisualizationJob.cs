using System.Collections.Generic;

namespace Visualization.Core
{
    public class VisualizationJob
    {
        public bool ShouldReset { get; set; }
        public IVisualizer Visualizer { get; set; }
        public IList<VisualizerPacket> Packets { get; set; }
    }
}