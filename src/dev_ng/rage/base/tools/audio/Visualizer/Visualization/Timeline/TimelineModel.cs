using System;
using rage.ToolLib;

namespace Visualization.Timeline
{
    public class TimelineModel : ITimelineModel
    {
        private TimeSpan m_currentTime;
        private TimeSpan m_endTime;
        private TimeSpan m_startTime;

        public TimelineModel()
        {
            m_startTime = TimeSpan.Zero;
            m_currentTime = TimeSpan.Zero;
            m_endTime = TimeSpan.Zero;
        }

        #region ITimelineModel Members

        public event Action<TimeSpan> StartTimeChanged;
        public event Action<TimeSpan> CurrentTimeChanged;
        public event Action<TimeSpan> EndTimeChanged;
        public event Action Reset;

        public void ResetModel(TimeSpan endTime)
        {
            StartTime = TimeSpan.Zero;
            CurrentTime = TimeSpan.Zero;
            EndTime = endTime;
            Reset.Raise();
        }

        public TimeSpan StartTime
        {
            get { return m_startTime; }
            set
            {
                m_startTime = value;
                StartTimeChanged.Raise(m_startTime);
                if (m_startTime > m_endTime)
                {
                    EndTime = m_startTime;
                }
                if (m_startTime > m_currentTime)
                {
                    CurrentTime = m_startTime;
                }
            }
        }

        public TimeSpan CurrentTime
        {
            get { return m_currentTime; }
            set
            {
                m_currentTime = value;
                if (m_currentTime < m_startTime)
                {
                    m_currentTime = m_startTime;
                }
                else if (m_currentTime > m_endTime)
                {
                    m_currentTime = m_endTime;
                }
                CurrentTimeChanged.Raise(m_currentTime);
            }
        }

        public TimeSpan EndTime
        {
            get { return m_endTime; }
            set
            {
                m_endTime = value;
                EndTimeChanged.Raise(m_endTime);
                if (m_endTime < m_startTime)
                {
                    StartTime = m_endTime;
                }
                if (m_endTime < m_currentTime)
                {
                    CurrentTime = m_endTime;
                }
            }
        }

        #endregion
    }
}