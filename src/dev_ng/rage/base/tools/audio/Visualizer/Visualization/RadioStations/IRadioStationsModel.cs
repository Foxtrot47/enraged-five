using System;
using System.Collections.Generic;
using Visualization.Core;

namespace Visualization.RadioStations
{
    public interface IRadioStationsModel
    {
        event Action<IList<RadioStationData>> DataChanged;

        bool Init();

        void Reset();

        void Visualize(VisualizerPacket packet);
    }
}