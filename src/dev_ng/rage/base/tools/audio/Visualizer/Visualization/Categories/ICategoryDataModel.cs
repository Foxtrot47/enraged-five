using System;
using System.Collections.Generic;
using rage.ToolLib.DataStructures;
using Visualization.Core;

namespace Visualization.Categories
{
    public interface ICategoryDataModel
    {
        event Action<Tree<CategoryData>> DataChanged;

        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets);
    }
}