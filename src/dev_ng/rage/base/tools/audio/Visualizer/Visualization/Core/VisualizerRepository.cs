using System;
using System.Collections.Generic;
using rage.ToolLib.Repository;

namespace Visualization.Core
{
    public class VisualizerRepository : IVisualizerRepository
    {
        private readonly IRepository<string, IVisualizer> m_visualizers;

        public VisualizerRepository(IEnumerable<IVisualizer> visualizers)
        {
            if (visualizers == null)
            {
                throw new ArgumentNullException("visualizers");
            }

            m_visualizers = new Repository<string, IVisualizer>();
            foreach (var visualizer in visualizers)
            {
                m_visualizers.Add(visualizer);
            }
        }

        #region IVisualizerRepository Members

        public IEnumerable<IVisualizer> Visualizers
        {
            get { return m_visualizers.Items; }
        }

        public IVisualizer Get(string id)
        {
            IVisualizer visualizer;
            m_visualizers.TryGetItem(id, out visualizer);
            return visualizer;
        }

        public IVisualizer Get(VisualizationType type)
        {
            foreach (var visualizer in Visualizers)
            {
                foreach (var visualizableType in visualizer.Types)
                {
                    if (visualizableType == type)
                    {
                        return visualizer;
                    }
                }
            }
            return null;
        }

        #endregion
    }
}