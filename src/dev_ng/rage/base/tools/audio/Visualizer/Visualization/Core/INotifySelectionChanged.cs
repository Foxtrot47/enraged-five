using System;

namespace Visualization.Core
{
    public interface INotifySelectionChanged
    {
        /// <summary>
        ///   Set to true by the visualizer system if outgoing data should be in big endian format, false otherwise.
        /// </summary>
        bool OutgoingDataIsBigEndian { set; }

        /// <summary>
        ///   Raised when an item is selected or deselected.
        ///   The parameters are:
        ///   The type of message to send to the game.
        ///   The data for the message.
        ///   The length of the message data.
        /// </summary>
        event Action<VisualizationType, byte[], ushort> SelectionChanged;
    }
}