using System;

namespace Visualization.RadioStations
{
    public class RadioStationData
    {
        public string CurrentTrack { get; set; }
        public string State { get; set; }
        public TimeSpan PlayTime { get; set; }
    }
}