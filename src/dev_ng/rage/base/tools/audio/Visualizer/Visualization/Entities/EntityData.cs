namespace Visualization.Entities
{
    public class EntityData
    {
        public uint Id { get; set; }
        public string Type { get; set; }
    }
}