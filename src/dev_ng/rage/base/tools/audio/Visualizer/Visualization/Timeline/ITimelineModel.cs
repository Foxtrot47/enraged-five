using System;

namespace Visualization.Timeline
{
    public interface ITimelineModel
    {
        TimeSpan StartTime { get; set; }
        TimeSpan CurrentTime { get; set; }
        TimeSpan EndTime { get; set; }
        event Action<TimeSpan> StartTimeChanged;
        event Action<TimeSpan> CurrentTimeChanged;
        event Action<TimeSpan> EndTimeChanged;
        event Action Reset;

        void ResetModel(TimeSpan endTime);
    }
}