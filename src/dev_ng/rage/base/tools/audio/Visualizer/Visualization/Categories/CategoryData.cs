namespace Visualization.Categories
{
    public class CategoryData
    {
        private float m_distanceReverbDamping;
        private float m_distanceRollOffScale;
        private float m_environmentalFilterDamping;
        private float m_environmentalLoudness;
        private ushort m_hpfCutoff;
        private ushort m_lpfCutoff;
        private float m_occlusionDamping;
        private short m_pitch;
        private float m_sourceReverbDamping;
        private float m_volume;

        public string Name { get; set; }

        public float OldVolume { get; private set; }

        public float Volume
        {
            get { return m_volume; }
            set
            {
                if (m_volume != value)
                {
                    OldVolume = m_volume;
                    m_volume = value;
                }
            }
        }

        public float OldDistanceRollOffScale { get; private set; }

        public float DistanceRollOffScale
        {
            get { return m_distanceRollOffScale; }
            set
            {
                if (m_distanceRollOffScale != value)
                {
                    OldDistanceRollOffScale = m_distanceRollOffScale;
                    m_distanceRollOffScale = value;
                }
            }
        }

        public float OldOcclusionDamping { get; private set; }

        public float OcclusionDamping
        {
            get { return m_occlusionDamping; }
            set
            {
                if (m_occlusionDamping != value)
                {
                    OldOcclusionDamping = m_occlusionDamping;
                    m_occlusionDamping = value;
                }
            }
        }

        public float OldEnvironmentalFilterDamping { get; private set; }

        public float EnvironmentalFilterDamping
        {
            get { return m_environmentalFilterDamping; }
            set
            {
                if (m_environmentalFilterDamping != value)
                {
                    OldEnvironmentalFilterDamping = m_environmentalFilterDamping;
                    m_environmentalFilterDamping = value;
                }
            }
        }

        public float OldSourceReverbDamping { get; private set; }

        public float SourceReverbDamping
        {
            get { return m_sourceReverbDamping; }
            set
            {
                if (m_sourceReverbDamping != value)
                {
                    OldSourceReverbDamping = m_sourceReverbDamping;
                    m_sourceReverbDamping = value;
                }
            }
        }

        public float OldDistanceReverbDamping { get; private set; }

        public float DistanceReverbDamping
        {
            get { return m_distanceReverbDamping; }
            set
            {
                if (m_distanceReverbDamping != value)
                {
                    OldDistanceReverbDamping = m_distanceReverbDamping;
                    m_distanceReverbDamping = value;
                }
            }
        }

        public float OldInteriorReverbDamping { get; private set; }

        public float InteriorReverbDamping
        {
            get { return m_interiorReverbDamping; }
            set
            {
                if (m_interiorReverbDamping != value)
                {
                    OldInteriorReverbDamping = m_interiorReverbDamping;
                    m_interiorReverbDamping = value;
                }
            }
        }

        public float OldEnvironmentalLoudness { get; private set; }

        public float EnvironmentalLoudness
        {
            get { return m_environmentalLoudness; }
            set
            {
                if (m_environmentalLoudness != value)
                {
                    OldEnvironmentalLoudness = m_environmentalLoudness;
                    m_environmentalLoudness = value;
                }
            }
        }

        public short OldPitch { get; private set; }

        public short Pitch
        {
            get { return m_pitch; }
            set
            {
                if (m_pitch != value)
                {
                    OldPitch = m_pitch;
                    m_pitch = value;
                }
            }
        }

        public ushort OldLpfCutoff { get; private set; }

        public ushort LpfCutoff
        {
            get { return m_lpfCutoff; }
            set
            {
                if (m_lpfCutoff != value)
                {
                    OldLpfCutoff = m_lpfCutoff;
                    m_lpfCutoff = value;
                }
            }
        }

        public ushort OldHpfCutoff { get; private set; }

        public ushort HpfCutoff
        {
            get { return m_hpfCutoff; }
            set
            {
                if (m_hpfCutoff != value)
                {
                    OldHpfCutoff = m_hpfCutoff;
                    m_hpfCutoff = value;
                }
            }
        }
    }
}