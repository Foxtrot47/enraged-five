namespace Visualization.Entities
{
    public interface IEntityConfig
    {
        string Root { get; set; }

        string EntityTypesFile { get; }
    }
}