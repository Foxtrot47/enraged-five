using System;
using System.Collections.Generic;

namespace Visualization.Core
{
    public interface IPacketHistory
    {
        bool IsEmpty { get; }

        TimeSpan EndTime { get; }

        IEnumerable<VisualizerPacket> Packets { get; }

        IEnumerable<VisualizerPacket> GetPacketsBetween(TimeSpan start, TimeSpan end);

        void Add(VisualizerPacket packet);

        void Clear();
    }
}