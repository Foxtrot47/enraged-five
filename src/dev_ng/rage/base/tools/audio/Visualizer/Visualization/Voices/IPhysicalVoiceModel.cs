using System;
using System.Collections.Generic;
using Visualization.Core;

namespace Visualization.Voices
{
    public interface IPhysicalVoiceModel
    {
        event Action<IList<PhysicalVoiceData>> DataChanged;

        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets);
    }
}