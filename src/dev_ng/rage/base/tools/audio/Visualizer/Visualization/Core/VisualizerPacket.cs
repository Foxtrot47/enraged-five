using System;
using System.IO;
using rage.ToolLib;

namespace Visualization.Core
{
    public class VisualizerPacket
    {
        private const int PACKET_HEADER_SIZE = 8;
        private byte[] m_data;

        public VisualizerPacket(byte[] data, int startIndex, bool isBigEndian)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            if (data.Length - startIndex < 8)
            {
                throw new ArgumentException("data must have at least 8 bytes available after startIndex");
            }

            // 8 byte packet header with format:
            // 1 byte � type
            // 4 bytes � timestamp
            // 2 bytes � size of data which follows the header
            // 1 byte � padding

            // first byte is the type
            Type = (VisualizationType) data[startIndex];
            if (Type == VisualizationType.SetRuntimePlatform)
            {
                isBigEndian = true;
            }
            startIndex += 1;

            IsBigEndian = isBigEndian;

            // next 4 bytes are timeStamp
            var temp = new byte[4];
            Array.Copy(data, startIndex, temp, 0, 4);
            if (isBigEndian)
            {
                temp = Utility.Reverse(temp);
            }
            TimeStamp = TimeSpan.FromMilliseconds(BitConverter.ToUInt32(temp, 0));
            startIndex += 4;

            // next 2 bytes are the size of the data followed by 1 unused byte
            temp = new byte[2];
            Array.Copy(data, startIndex, temp, 0, 2);
            if (isBigEndian)
            {
                temp = Utility.Reverse(temp);
            }
            Size = BitConverter.ToUInt16(temp, 0);
        }

        public VisualizerPacket(byte[] data, int startIndex)
        {
            // this constructor is used for loading data from .vis files and assumes little endian
            // format for the packet header
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            if (data.Length - startIndex < 8)
            {
                throw new ArgumentException("data must have at least 8 bytes available after startIndex");
            }

            // 8 byte packet header with format:
            // 1 byte � type
            // 4 bytes � timestamp
            // 2 bytes � size of data which follows the header
            // 1 byte � padding

            // first byte is the type
            Type = (VisualizationType) data[startIndex];
            startIndex += 1;

            // next 4 bytes are timeStamp
            var temp = new byte[4];
            Array.Copy(data, startIndex, temp, 0, 4);
            TimeStamp = TimeSpan.FromMilliseconds(BitConverter.ToUInt32(temp, 0));
            startIndex += 4;

            // next 2 bytes are the size of the data
            temp = new byte[2];
            Array.Copy(data, startIndex, temp, 0, 2);
            Size = BitConverter.ToUInt16(temp, 0);
            startIndex += 2;

            // last byte is endianness of the data that follows the packet header
            IsBigEndian = data[startIndex] == 1;
        }

        public static int PacketHeaderSize
        {
            get { return PACKET_HEADER_SIZE; }
        }

        public bool IsBigEndian { get; private set; }
        public TimeSpan TimeStamp { get; private set; }
        public VisualizationType Type { get; private set; }
        public ushort Size { get; private set; }

        public Stream Data
        {
            get { return new MemoryStream(m_data, false); }
        }

        public void SetData(byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            if (data.Length < Size)
            {
                throw new ArgumentException(
                    String.Format("The provided data doesn't have enough bytes. Size = {0}, Data Length = {1}.",
                                  Size,
                                  data.Length));
            }
            m_data = data;
        }
    }
}