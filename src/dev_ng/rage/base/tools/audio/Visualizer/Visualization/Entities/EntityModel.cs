using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;
using rage.ToolLib;
using Visualization.Core;

namespace Visualization.Entities
{
    public class EntityModel : IEntityModel
    {
        private readonly IEntityConfig m_config;
        private readonly Dictionary<uint, EntityData> m_entityLookup;
        private readonly Dictionary<uint, string> m_entityTypesLookup;

        public EntityModel(IEntityConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            m_config = config;
            m_entityTypesLookup = new Dictionary<uint, string>();
            m_entityLookup = new Dictionary<uint, EntityData>();
        }

        #region IEntityModel Members

        public event Action<List<EntityData>> DataChanged;

        public bool Init()
        {
            return LoadEntityTypes();
        }

        public void Reset()
        {
            m_entityLookup.Clear();
            DataChanged.Raise(null);
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets, bool shouldReset)
        {
            DebugCheckPacketValidity(packets);

            if (shouldReset)
            {
                m_entityLookup.Clear();
            }

            foreach (var packet in packets)
            {
                switch (packet.Type)
                {
                    case VisualizationType.AddEntity:
                        {
                            AddEntity(packet);
                            break;
                        }
                    case VisualizationType.RemoveEntity:
                        {
                            RemoveEntity(packet);
                            break;
                        }
                }
            }

            DataChanged.Raise(m_entityLookup.Values.ToList());
        }

        #endregion

        [Conditional("DEBUG")]
        private static void DebugCheckPacketValidity(IEnumerable<VisualizerPacket> packets)
        {
            foreach (var packet in packets)
            {
                if ((!(packet.Type == VisualizationType.AddEntity || packet.Type == VisualizationType.RemoveEntity)) ||
                    packet.Size == 0)
                {
                    throw new ArgumentException(
                        string.Format("EntityVisualizer: Invalid packet with type: {0} and size: {1}.",
                                      packet.Type,
                                      packet.Size));
                }
            }
        }

        private void AddEntity(VisualizerPacket packet)
        {
            var data = packet.Data;
            var buffer = new byte[4];
            buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
            var entityData = new EntityData {Id = BitConverter.ToUInt32(buffer, 0)};

            buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
            var entityTypeNameHash = BitConverter.ToUInt32(buffer, 0);
            string entityTypeName;
            if (!m_entityTypesLookup.TryGetValue(entityTypeNameHash, out entityTypeName))
            {
                entityTypeName = entityTypeNameHash.ToString();
            }
            entityData.Type = entityTypeName;
            m_entityLookup.Remove(entityData.Id);
            m_entityLookup.Add(entityData.Id, entityData);
        }

        private void RemoveEntity(VisualizerPacket packet)
        {
            var buffer = new byte[4];
            buffer = Utility.ReadData(packet.Data, packet.IsBigEndian, buffer);
            m_entityLookup.Remove(BitConverter.ToUInt32(buffer, 0));
        }

        private bool LoadEntityTypes()
        {
            XDocument doc;
            try
            {
                doc = XDocument.Load(string.Concat(m_config.Root, m_config.EntityTypesFile));

                var entityTypesElement = doc.Root.Element("EntityTypes");
                if (entityTypesElement == null)
                {
                    return false;
                }

                var entityTypes = from entityType in entityTypesElement.Elements("EntityType") select entityType.Value;
                var hash = new Hash();
                foreach (var entityType in entityTypes)
                {
                    hash.Value = entityType;
                    m_entityTypesLookup.Add(hash.Key, entityType);
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}