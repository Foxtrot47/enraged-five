using System.Collections.Generic;
using Visualization.Core;

namespace Visualization.WaveSlots
{
    public interface IWaveSlotsModel
    {
        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets);
    }
}