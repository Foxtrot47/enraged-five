using System;
using System.Diagnostics;
using rage.ToolLib;
using Visualization.Core;

namespace Visualization.Voices
{
    public class VoiceStartModel : IVoiceStartModel
    {
        private VoiceStartData m_data;

        #region IVoiceStartModel Members

        public event Action<VoiceStartData> DataChanged;

        public void Reset()
        {
            m_data = null;
            DataChanged.Raise(m_data);
        }

        public void Visualize(VisualizerPacket packet)
        {
            m_data = null;

            if (packet != null)
            {
                DebugCheckPacketValidity(packet);

                var data = packet.Data;
                m_data = new VoiceStartData();

                // 4 bytes long
                var buffer = new byte[4];
                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                m_data.NumWaveSlots = BitConverter.ToUInt32(buffer, 0);

                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                m_data.WaveSlotTableSize = BitConverter.ToUInt32(buffer, 0);

                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                m_data.NumVirtualVoices = BitConverter.ToUInt32(buffer, 0);
            }

            DataChanged.Raise(m_data);
        }

        #endregion

        [Conditional("DEBUG")]
        private static void DebugCheckPacketValidity(VisualizerPacket packet)
        {
            if (packet.Type != VisualizationType.VoiceStart ||
                packet.Size != 12)
            {
                throw new ArgumentException(
                    string.Format("VoicesVisualizer - Start: Invalid packet with type: {0} and size: {1}.",
                                  packet.Type,
                                  packet.Size));
            }
        }
    }
}