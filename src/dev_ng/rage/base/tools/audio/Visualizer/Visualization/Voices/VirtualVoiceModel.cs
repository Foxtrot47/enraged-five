using System;
using System.Collections.Generic;
using System.Linq;
using rage.ToolLib;
using Visualization.Core;

namespace Visualization.Voices
{
    public class VirtualVoiceModel : IVirtualVoiceModel
    {
        private readonly List<VirtualVoiceData> m_data = new List<VirtualVoiceData>();

        #region IVirtualVoiceModel Members

        public event Action<IList<VirtualVoiceData>> DataChanged;

        public bool Init()
        {
            return true;
        }

        public void Reset()
        {
            m_data.Clear();
            DataChanged.Raise(m_data);
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets)
        {
            m_data.Clear();
            CreateData(packets.Last());
            DataChanged.Raise(m_data);
        }

        #endregion

        private void CreateData(VisualizerPacket packet)
        {
            var data = packet.Data;
            while (data.Position <
                   packet.Size)
            {
                var voiceData = new VirtualVoiceData();

                var buffer = new byte[8];
                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                voiceData.VirtualizationScore = BitConverter.ToUInt64(buffer, 0);

                buffer = new byte[4];
                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                voiceData.Playtime = BitConverter.ToInt32(buffer, 0);

                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                voiceData.State = BitConverter.ToUInt32(buffer, 0);

                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                voiceData.WaveNameHash = BitConverter.ToUInt32(buffer, 0);

                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                voiceData.SourceEffectWetMix = BitConverter.ToSingle(buffer, 0);

                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                voiceData.SourceEffecDryMix = BitConverter.ToSingle(buffer, 0);

                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                voiceData.RequestedVolume = BitConverter.ToSingle(buffer, 0);

                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                voiceData.PostSubmixVolume = BitConverter.ToSingle(buffer, 0);

                buffer = new byte[2];
                buffer = Utility.ReadData(data, packet.IsBigEndian, buffer);
                voiceData.GroupId = BitConverter.ToUInt16(buffer, 0);

                voiceData.WaveSlotIndex = (byte) data.ReadByte();
                voiceData.BucketId = (byte) data.ReadByte();
                voiceData.VoiceId = (byte) data.ReadByte();

                var boolFlags = (byte) data.ReadByte();
                voiceData.NeedsSampleData = (boolFlags & 1) != 0;
                voiceData.ShouldControlSubmix = (boolFlags & (1 << 1)) != 0;
                voiceData.HasEffectChain = (boolFlags & (1 << 2)) != 0;

                m_data.Add(voiceData);
            }
        }
    }
}