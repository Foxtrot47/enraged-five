using System;

namespace Visualization.Core
{
    /// <summary>
    ///   This enumeration should have the same values defined (except Ping = 0) as audRemoteDebugCommands defines in
    ///   rage\base\src\audioengine\remotedebug.h
    /// </summary>
    [Flags]
    public enum VisualizationType : byte
    {
        // System Messages
        Ping,
        MessageReceipt,
        SetRuntimePlatform,

        // Category data message
        ResolvedCategoryData,

        // Entity messages
        AddEntity,
        RemoveEntity,
        EntitySelected,

        // Voice messages
        VoiceStart,
        VirtualVoiceData,
        PhysicalVoiceData,

        // Radio Station messages
        RadioStationData,

        // Wave Slot messages
        WaveSlotLoadRequested,
        WaveSlotLoadCompleted,
        WaveSlotLoadDenied,
    }
}