using System.Collections.Generic;

namespace Visualization.Core
{
    public interface IVisualizerRepository
    {
        /// <summary>
        ///   Retrieves all of the visualizers.
        /// </summary>
        IEnumerable<IVisualizer> Visualizers { get; }

        /// <summary>
        ///   Retrieve the visualizer with the specified id.
        /// </summary>
        /// <param name = "id">The id of the visualizer.</param>
        /// <returns>The visualizer if found, null otherwise.</returns>
        IVisualizer Get(string id);

        /// <summary>
        ///   Attempt to retrieve a visualizer that can visualize the specified type.
        /// </summary>
        /// <param name = "type">The type of data to visualize.</param>
        /// <returns>An appropriate visualizer if found, null otherwise.</returns>
        IVisualizer Get(VisualizationType type);
    }
}