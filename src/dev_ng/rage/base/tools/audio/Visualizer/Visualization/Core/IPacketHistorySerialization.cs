namespace Visualization.Core
{
    public interface IPacketHistorySerialization
    {
        bool Serialize(string fileName, IPacketHistory packetHistory);

        bool Deserialize(string fileName, IPacketHistory packetHistory);
    }
}