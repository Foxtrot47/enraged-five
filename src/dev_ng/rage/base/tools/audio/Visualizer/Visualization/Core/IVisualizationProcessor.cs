using System;

namespace Visualization.Core
{
    public interface IVisualizationProcessor : IDisposable
    {
        void Start();

        void Add(VisualizationJob job);

        void ClearJobs();

        void Stop();
    }
}