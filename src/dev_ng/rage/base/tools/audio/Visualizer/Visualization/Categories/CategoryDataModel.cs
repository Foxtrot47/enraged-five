using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using rage.Compiler;
using rage.ToolLib;
using rage.ToolLib.DataStructures;
using Visualization.Core;

namespace Visualization.Categories
{
    public class CategoryDataModel : ICategoryDataModel
    {
        private readonly Tree<string> m_categoryHierarchyTree;
        private readonly ICategoryDataConfig m_config;
        private readonly Tree<CategoryData> m_data;
        private readonly ICompiledObjectLookup m_objectLookup;

        public CategoryDataModel(ICategoryDataConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            m_config = config;
            m_data = new Tree<CategoryData>();
            m_categoryHierarchyTree = new Tree<string>();
            m_objectLookup = new CompiledObjectLookup(true);
        }

        #region ICategoryDataModel Members

        public event Action<Tree<CategoryData>> DataChanged;

        public bool Init()
        {
            BuildObjectLookup();
            BuildCategoryHierarchy();
            return true;
        }

        public void Reset()
        {
            m_data.Root = null;
            DataChanged.Raise(m_data);
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets)
        {
            DebugCheckPacketValidity(packets);

            m_data.Root = null;

            var packet = packets.Last();
            var data = packet.Data;
            var numCategories = GetNumCategories(data, packet.IsBigEndian);
            var categoryNames = GetCategoryNames(data, packet.IsBigEndian, numCategories);
            CreateData(data, packet.IsBigEndian, categoryNames, numCategories);

            DataChanged.Raise(m_data);
        }

        #endregion

        [Conditional("DEBUG")]
        private static void DebugCheckPacketValidity(IEnumerable<VisualizerPacket> packets)
        {
            foreach (var packet in packets)
            {
                if (packet.Type != VisualizationType.ResolvedCategoryData ||
                    packet.Size == 0)
                {
                    throw new ArgumentException(
                        string.Format("CategoryDataVisualizer: Invalid packet with type: {0} and size: {1}.",
                                      packet.Type,
                                      packet.Size));
                }
            }
        }

        private void CreateData(Stream data, bool isBigEndian, IList<string> categoryNames, ushort numCategories)
        {
            var categoryLookup = new Dictionary<string, CategoryData>();
            for (var i = 0; i < numCategories; ++i)
            {
                var categoryData = new CategoryData {Name = categoryNames[i]};

                // 4 bytes long
                var buffer = new byte[4];
                buffer = Utility.ReadData(data, isBigEndian, buffer);
                categoryData.Volume = BitConverter.ToSingle(buffer, 0);

                buffer = Utility.ReadData(data, isBigEndian, buffer);
                categoryData.DistanceRollOffScale = BitConverter.ToSingle(buffer, 0);

                buffer = Utility.ReadData(data, isBigEndian, buffer);
                categoryData.OcclusionDamping = BitConverter.ToSingle(buffer, 0);

                buffer = Utility.ReadData(data, isBigEndian, buffer);
                categoryData.EnvironmentalFilterDamping = BitConverter.ToSingle(buffer, 0);

                buffer = Utility.ReadData(data, isBigEndian, buffer);
                categoryData.SourceReverbDamping = BitConverter.ToSingle(buffer, 0);

                buffer = Utility.ReadData(data, isBigEndian, buffer);
                categoryData.DistanceReverbDamping = BitConverter.ToSingle(buffer, 0);

                buffer = Utility.ReadData(data, isBigEndian, buffer);
                categoryData.InteriorReverbDamping = BitConverter.ToSingle(buffer, 0);

                buffer = Utility.ReadData(data, isBigEndian, buffer);
                categoryData.EnvironmentalLoudness = BitConverter.ToSingle(buffer, 0);

                // 2 bytes long
                buffer = new byte[2];
                buffer = Utility.ReadData(data, isBigEndian, buffer);
                categoryData.Pitch = BitConverter.ToInt16(buffer, 0);

                buffer = Utility.ReadData(data, isBigEndian, buffer);
                categoryData.LpfCutoff = BitConverter.ToUInt16(buffer, 0);

                buffer = Utility.ReadData(data, isBigEndian, buffer);
                categoryData.HpfCutoff = BitConverter.ToUInt16(buffer, 0);

                categoryLookup.Add(categoryData.Name, categoryData);
            }

            BuildCategoryDataTree(categoryLookup);
        }

        private void BuildCategoryDataTree(IDictionary<string, CategoryData> categoryDataLookup)
        {
            m_data.Root = new TreeNode<CategoryData>(null)
                              {Data = categoryDataLookup[m_categoryHierarchyTree.Root.Data]};
            BuildCategoryDataTree(m_data.Root, m_categoryHierarchyTree.Root, categoryDataLookup);
        }

        private static void BuildCategoryDataTree(TreeNode<CategoryData> categoryNode,
                                                  TreeNode<string> categoryHierarchyNode,
                                                  IDictionary<string, CategoryData> categoryDataLookup)
        {
            foreach (var categoryHierarchyChildNode in categoryHierarchyNode.Children)
            {
                // If we don't find the key we are probably playing back an older version of a saved file 
                // that is missing some categories
                CategoryData value;
                if (categoryDataLookup.TryGetValue(categoryHierarchyChildNode.Data, out value))
                {
                    var categoryChildNode = new TreeNode<CategoryData>(categoryNode) {Data = value};
                    categoryNode.Children.Add(categoryChildNode);
                    BuildCategoryDataTree(categoryChildNode, categoryHierarchyChildNode, categoryDataLookup);
                }
            }
        }

        private IList<string> GetCategoryNames(Stream data, bool isBigEndian, ushort numCategories)
        {
            var categoryNames = new List<string>();
            var buffer = new byte[4];
            for (var i = 0; i < numCategories; ++i)
            {
                buffer = Utility.ReadData(data, isBigEndian, buffer);
                var offset = BitConverter.ToUInt32(buffer, 0);
                var name = m_objectLookup.GetNameFromOffset(offset);
                if (string.IsNullOrEmpty(name))
                {
                    name = offset.ToString();
                }
                categoryNames.Add(name);
            }
            return categoryNames;
        }

        private static ushort GetNumCategories(Stream data, bool isBigEndian)
        {
            var buffer = new byte[2];
            buffer = Utility.ReadData(data, isBigEndian, buffer);
            return BitConverter.ToUInt16(buffer, 0);
        }

        private void BuildObjectLookup()
        {
            var categoriesMetadataFileName = string.Concat(m_config.Root, m_config.CategoryMetaDataFile);
            if (!File.Exists(categoriesMetadataFileName))
            {
                throw new ApplicationException(
                    string.Concat("CategoryDataVisualizer: Could not load \"Categories\" metadata from: ",
                                  categoriesMetadataFileName));
            }
            var data = File.OpenRead(categoriesMetadataFileName);

            // first 4 bytes is metadata version so skip it
            var buffer = new byte[4];
            data.Read(buffer, 0, 4);

            // skip metadata chunk
            var metaDataChunkSize = GetChunkSize(data);
            data.Seek(metaDataChunkSize + 8, SeekOrigin.Begin);

            // skip string table chunk
            var stringTableChunkSize = GetChunkSize(data);
            data.Seek(metaDataChunkSize + stringTableChunkSize + 12, SeekOrigin.Begin);

            // deserialize the compiled object lookup
            m_objectLookup.Clear();
            m_objectLookup.Deserialize(data, m_config.IsBigEndian);
        }

        private void BuildCategoryHierarchy()
        {
            var categoryDataFile = string.Concat(m_config.Root, m_config.CategoryDataFile);
            if (!File.Exists(categoryDataFile))
            {
                throw new ApplicationException(
                    string.Concat("CategoryDataVisualizer: Could not load \"Categories\" data from: ", categoryDataFile));
            }

            var doc = XDocument.Load(categoryDataFile);
            var categoryElements = doc.Root.Elements("Category");
            var categoryTreeNodeLookup =
                categoryElements.Select(category => category.Attribute("name").Value).ToDictionary(
                    categoryName => categoryName, categoryName => new TreeNode<string>(null) {Data = categoryName});

            foreach (var categoryElement in categoryElements)
            {
                var categoryNode = categoryTreeNodeLookup[categoryElement.Attribute("name").Value];
                var childCategoryNames = from categoryRef in categoryElement.Elements("CategoryRef")
                                         select categoryRef.Element("CategoryId").Value;
                foreach (var childCategoryName in childCategoryNames)
                {
                    var childCategory = categoryTreeNodeLookup[childCategoryName];
                    if (childCategory.Parent == null)
                    {
                        childCategory.Parent = categoryNode;
                        categoryNode.Children.Add(childCategory);
                    }
                    else
                    {
                        throw new ApplicationException(
                            string.Format(
                                "CategoryDataVisualizer: Error parsing category hierarchy data. Category: \"{0}\" already has parent category: \"{1}\" but is being claimed by category: \"{2}\"",
                                childCategoryName,
                                childCategory.Parent.Data,
                                categoryNode.Data));
                    }
                }
            }

            m_categoryHierarchyTree.Root =
                (from categoryNode in categoryTreeNodeLookup where categoryNode.Value.Parent == null select categoryNode)
                    .First().Value;
        }

        private uint GetChunkSize(Stream data)
        {
            var buffer = new byte[4];
            data.Read(buffer, 0, 4);
            if (m_config.IsBigEndian)
            {
                buffer = Utility.Reverse(buffer);
            }
            return BitConverter.ToUInt32(buffer, 0);
        }
    }
}