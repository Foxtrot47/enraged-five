using System;

namespace Visualization.Core
{
    public interface INetwork : IDisposable
    {
        bool IsBigEndian { get; }
        bool IsConnected { get; }
        event Action EndianChanged;
        event Action Connected;
        event Action Disconnected;
        event Action DataArrived;

        void Start();

        void Send(VisualizationType type, byte[] data, ushort length);

        VisualizerPacket Receive();

        void Stop();
    }
}