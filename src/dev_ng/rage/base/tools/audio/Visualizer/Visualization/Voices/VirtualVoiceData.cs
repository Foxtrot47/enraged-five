namespace Visualization.Voices
{
    public class VirtualVoiceData
    {
        public ulong VirtualizationScore { get; set; }
        public int Playtime { get; set; }
        public uint State { get; set; }
        public uint WaveNameHash { get; set; }
        public float SourceEffectWetMix { get; set; }
        public float SourceEffecDryMix { get; set; }
        public float RequestedVolume { get; set; }
        public float PostSubmixVolume { get; set; }
        public ushort GroupId { get; set; }
        public byte WaveSlotIndex { get; set; }
        public byte BucketId { get; set; }
        public byte VoiceId { get; set; }
        public bool NeedsSampleData { get; set; }
        public bool ShouldControlSubmix { get; set; }
        public bool HasEffectChain { get; set; }
    }
}