namespace Visualization.Voices
{
    public class PhysicalVoiceData
    {
        public uint WaveNameHash { get; set; }
        public byte WaveSlotIndex { get; set; }
        public byte BucketId { get; set; }
        public byte VoiceId { get; set; }
        public byte PhysicalVoiceId { get; set; }
    }
}