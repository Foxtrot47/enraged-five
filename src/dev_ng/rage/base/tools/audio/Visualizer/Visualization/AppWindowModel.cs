using System;
using System.Collections.Generic;
using System.Linq;
using rage.ToolLib;
using rage.ToolLib.Logging;
using Visualization.Core;
using Visualization.Timeline;

namespace Visualization
{
    public class AppWindowModel : IAppWindowModel
    {
        private readonly IPacketHistory m_livePacketHistory;
        private readonly ILog m_log;
        private readonly INetwork m_network;
        private readonly IPacketHistorySerialization m_packetHistorySerialization;
        private readonly IPacketHistory m_replayPacketHistory;
        private readonly IPacketHistory m_tempPacketHistory;
        private readonly ITimelineModel m_timelineModel;
        private readonly IVisualizationProcessor m_visualizationProcessor;
        private readonly IVisualizerRepository m_visualizerRepository;
        private IVisualizer m_currentVisualizer;
        private AppWindowMode m_mode;
        private string m_replayFile;

        public AppWindowModel(ILog log,
                              INetwork network,
                              IVisualizerRepository visualizerRepository,
                              IVisualizationProcessor visualizationProcessor,
                              IPacketHistory livePacketHistory,
                              IPacketHistory replayPacketHistory,
                              IPacketHistory tempPacketHistory,
                              IPacketHistorySerialization packetHistorySerialization,
                              ITimelineModel timelineModel)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (network == null)
            {
                throw new ArgumentNullException("network");
            }
            if (visualizerRepository == null)
            {
                throw new ArgumentNullException("visualizerRepository");
            }
            if (visualizationProcessor == null)
            {
                throw new ArgumentNullException("visualizationProcessor");
            }
            if (livePacketHistory == null)
            {
                throw new ArgumentNullException("livePacketHistory");
            }
            if (replayPacketHistory == null)
            {
                throw new ArgumentNullException("replayPacketHistory");
            }
            if (tempPacketHistory == null)
            {
                throw new ArgumentNullException("tempPacketHistory");
            }
            if (packetHistorySerialization == null)
            {
                throw new ArgumentNullException("packetHistorySerialization");
            }
            if (timelineModel == null)
            {
                throw new ArgumentNullException("timelineModel");
            }
            m_log = log;
            m_network = network;
            m_visualizerRepository = visualizerRepository;
            m_visualizationProcessor = visualizationProcessor;
            m_livePacketHistory = livePacketHistory;
            m_replayPacketHistory = replayPacketHistory;
            m_tempPacketHistory = tempPacketHistory;
            m_packetHistorySerialization = packetHistorySerialization;
            m_timelineModel = timelineModel;
            m_replayFile = string.Empty;
        }

        #region IAppWindowModel Members

        public event Action<IVisualizer> VisualizerActivated;
        public event Action Connected;
        public event Action Disconnected;
        public event Action Exit;

        public bool RequiresSave
        {
            get { return !m_livePacketHistory.IsEmpty; }
        }

        public event Action<AppWindowMode> ModeChanged;

        public void SwitchToLive()
        {
            m_mode = AppWindowMode.Live;
            ModeSwitched();
        }

        public bool Start()
        {
            if (InitVisualizers())
            {
                m_network.EndianChanged += OnEndianChanged;
                m_network.Connected += OnConnected;
                m_network.Disconnected += OnDisconnected;
                m_network.DataArrived += OnDataArrived;
                m_network.Start();

                m_timelineModel.CurrentTimeChanged += OnCurrentTimeChanged;
                m_visualizationProcessor.Start();
                return true;
            }
            return false;
        }

        public bool SwitchToReplay(string file)
        {
            var valid = true;
            if (string.Compare(m_replayFile, file, true) != 0)
            {
                m_replayFile = file;
                valid = m_packetHistorySerialization.Deserialize(m_replayFile, m_replayPacketHistory);
            }

            if (valid)
            {
                m_mode = AppWindowMode.Replay;
                ModeSwitched();
                m_timelineModel.ResetModel(m_replayPacketHistory.EndTime);
            }
            else
            {
                m_replayFile = null;
            }
            return valid;
        }

        public void ActivateVisualizer(string visualizerId)
        {
            DeactivateCurrentVisualizer();
            m_currentVisualizer = m_visualizerRepository.Get(visualizerId);
            ActivateCurrentVisualizer();
        }

        public bool SaveHistory(string fileName)
        {
            if (RequiresSave)
            {
                if (m_packetHistorySerialization.Serialize(fileName, m_livePacketHistory))
                {
                    m_livePacketHistory.Clear();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public void Stop()
        {
            m_timelineModel.CurrentTimeChanged -= OnCurrentTimeChanged;
            m_visualizationProcessor.Stop();

            m_network.Stop();
            m_network.DataArrived -= OnDataArrived;
            m_network.Disconnected -= OnDisconnected;
            m_network.Connected -= OnConnected;
            m_network.EndianChanged -= OnEndianChanged;

            ShutdownVisualizers();
        }

        public void Dispose()
        {
            Stop();
        }

        #endregion

        private void OnEndianChanged()
        {
            var notifySelectionChanged = m_currentVisualizer as INotifySelectionChanged;
            if (notifySelectionChanged != null)
            {
                notifySelectionChanged.OutgoingDataIsBigEndian = m_network.IsBigEndian;
            }
        }

        private void OnConnected()
        {
            Connected.Raise();
        }

        private void OnDisconnected()
        {
            Disconnected.Raise();
        }

        private void OnDataArrived()
        {
            if (m_network.IsConnected)
            {
                m_tempPacketHistory.Clear();
                VisualizerPacket packet;
                while ((packet = m_network.Receive()) != null)
                {
                    m_livePacketHistory.Add(packet);
                    m_tempPacketHistory.Add(packet);
                }

                if (m_mode == AppWindowMode.Live)
                {
                    Visualize(m_tempPacketHistory.Packets, false);
                }
            }
        }

        private void ModeSwitched()
        {
            ModeChanged.Raise(m_mode);
            m_visualizationProcessor.ClearJobs();
            ResetVisualizers();
            if (m_currentVisualizer != null)
            {
                ActivateVisualizer(m_currentVisualizer.Id);
            }
        }

        private void ActivateCurrentVisualizer()
        {
            if (m_currentVisualizer != null)
            {
                var notifySelectionChanged = m_currentVisualizer as INotifySelectionChanged;
                if (notifySelectionChanged != null)
                {
                    notifySelectionChanged.SelectionChanged += OnSelectionChanged;
                    notifySelectionChanged.OutgoingDataIsBigEndian = m_network.IsBigEndian;
                }

                switch (m_mode)
                {
                    case AppWindowMode.Live:
                        {
                            lock (m_livePacketHistory)
                            {
                                Visualize(m_livePacketHistory.Packets, true);
                            }
                            break;
                        }
                    case AppWindowMode.Replay:
                        {
                            Visualize(
                                m_replayPacketHistory.GetPacketsBetween(TimeSpan.Zero, m_timelineModel.CurrentTime),
                                true);
                            break;
                        }
                }
            }
            VisualizerActivated.Raise(m_currentVisualizer);
        }

        private void DeactivateCurrentVisualizer()
        {
            if (m_currentVisualizer != null)
            {
                m_currentVisualizer.Reset();
                var notifySelectionChanged = m_currentVisualizer as INotifySelectionChanged;
                if (notifySelectionChanged != null)
                {
                    notifySelectionChanged.SelectionChanged -= OnSelectionChanged;
                }
            }
        }

        private void OnSelectionChanged(VisualizationType type, byte[] data, ushort length)
        {
            if (m_network.IsConnected)
            {
                m_network.Send(type, data, length);
            }
        }

        private void OnCurrentTimeChanged(TimeSpan currentTime)
        {
            Visualize(m_replayPacketHistory.GetPacketsBetween(TimeSpan.Zero, currentTime), true);
        }

        private void Visualize(IEnumerable<VisualizerPacket> packets, bool shouldReset)
        {
            var visualizerPackets = (from packet in packets
                                     from visualizationType in m_currentVisualizer.Types
                                     where visualizationType == packet.Type
                                     select packet).ToList();
            if (visualizerPackets.Count > 0)
            {
                m_visualizationProcessor.Add(new VisualizationJob
                                                 {
                                                     Visualizer = m_currentVisualizer,
                                                     Packets = visualizerPackets,
                                                     ShouldReset = shouldReset
                                                 });
            }
        }

        private bool InitVisualizers()
        {
            foreach (var visualizer in m_visualizerRepository.Visualizers)
            {
                if (!visualizer.Init())
                {
                    m_log.Error("Visualizer: {0} failed to initialize!", visualizer.Id);
                    Exit.Raise();
                    return false;
                }
            }
            return true;
        }

        private void ResetVisualizers()
        {
            foreach (var visualizer in m_visualizerRepository.Visualizers)
            {
                visualizer.Reset();
            }
        }

        private void ShutdownVisualizers()
        {
            foreach (var visualizer in m_visualizerRepository.Visualizers)
            {
                visualizer.Shutdown();
            }
        }
    }
}