using System.Collections.Generic;
using System.Threading;

namespace Visualization.Core
{
    public class VisualizationProcessor : IVisualizationProcessor
    {
        private readonly Queue<VisualizationJob> m_jobQueue;
        private Thread m_workerThread;

        public VisualizationProcessor()
        {
            m_jobQueue = new Queue<VisualizationJob>();
        }

        #region IVisualizationProcessor Members

        public void Start()
        {
            if (m_workerThread == null ||
                !m_workerThread.IsAlive)
            {
                m_workerThread = new Thread(Process) {Name = "Visualizer - Processing"};
                m_workerThread.Start(m_jobQueue);
            }
        }

        public void Add(VisualizationJob job)
        {
            lock (m_jobQueue)
            {
                m_jobQueue.Enqueue(job);
            }
        }

        public void ClearJobs()
        {
            Stop();
            m_jobQueue.Clear();
            Start();
        }

        public void Stop()
        {
            if (m_workerThread != null &&
                m_workerThread.IsAlive)
            {
                m_workerThread.Abort();
                m_workerThread = null;
            }
        }

        public void Dispose()
        {
            Stop();
        }

        #endregion

        private static void Process(object obj)
        {
            var jobQueue = obj as Queue<VisualizationJob>;
            if (jobQueue != null)
            {
                while (true)
                {
                    VisualizationJob job = null;
                    lock (jobQueue)
                    {
                        var count = jobQueue.Count;
                        if (count > 0)
                        {
                            // only need to do the latest one
                            while (count > 0)
                            {
                                job = jobQueue.Dequeue();
                                count -= 1;
                            }
                        }
                    }

                    if (job != null)
                    {
                        job.Visualizer.Visualize(job.Packets, job.ShouldReset);
                    }
                    Thread.Sleep(100);
                }
            }
        }
    }
}