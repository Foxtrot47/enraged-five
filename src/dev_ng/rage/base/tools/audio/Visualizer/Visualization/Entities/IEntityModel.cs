using System;
using System.Collections.Generic;
using Visualization.Core;

namespace Visualization.Entities
{
    public interface IEntityModel
    {
        event Action<List<EntityData>> DataChanged;

        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets, bool shouldReset);
    }
}