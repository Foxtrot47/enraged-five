namespace Visualization.Categories
{
    public interface ICategoryDataConfig
    {
        string Root { get; set; }

        bool IsBigEndian { get; }

        string CategoryMetaDataFile { get; }

        string CategoryDataFile { get; }

        string VolumeLimits { get; }

        string DistanceRollOffScaleLimits { get; }

        string OcclusionDampingLimits { get; }

        string EnvironmentalFilterDampingLimits { get; }

        string SourceReverbDampingLimits { get; }

        string DistanceReverbDampingLimits { get; }

        string EnvironmentalLoudnessLimits { get; }

        string PitchLimits { get; }

        string LpfCutoffLimits { get; }

        string HpfCutoffLimits { get; }
    }
}