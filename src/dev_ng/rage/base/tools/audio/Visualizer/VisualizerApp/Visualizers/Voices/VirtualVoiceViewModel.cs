using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Threading;
using Visualization.Core;
using Visualization.Voices;
using WPFToolLib.Extensions;

namespace VisualizerApp.Visualizers.Voices
{
    public class VirtualVoiceViewModel : IVirtualVoiceViewModel
    {
        private readonly IVirtualVoiceModel m_model;

        public VirtualVoiceViewModel(IVirtualVoiceModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            m_model.DataChanged += OnDataChanged;

            Data = new BindingList<VirtualVoiceData>();
        }

        #region IVirtualVoiceViewModel Members

        public event PropertyChangedEventHandler PropertyChanged;

        public BindingList<VirtualVoiceData> Data { get; private set; }

        public int Count
        {
            get { return Data.Count; }
        }

        public bool Init()
        {
            return m_model.Init();
        }

        public void Reset()
        {
            m_model.Reset();
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets)
        {
            DebugCheckPacketValidity(packets);
            m_model.Visualize(packets);
        }

        public void Dispose()
        {
            m_model.DataChanged -= OnDataChanged;
        }

        #endregion

        private void OnDataChanged(IList<VirtualVoiceData> virtualVoiceData)
        {
            if (virtualVoiceData != null)
            {
                Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal,
                                                                new Action<IList<VirtualVoiceData>>(UpdateData),
                                                                virtualVoiceData);
            }
            else
            {
                Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal, new Action(ClearData));
            }
        }

        private void ClearData()
        {
            Data.Clear();
            PropertyChanged.Raise(this, "Count");
        }

        private void UpdateData(IList<VirtualVoiceData> virtualVoiceData)
        {
            Data.RaiseListChangedEvents = false;
            Data.Clear();

            foreach (var item in virtualVoiceData)
            {
                Data.Add(item);
            }

            Data.RaiseListChangedEvents = true;
            Data.ResetBindings();
            PropertyChanged.Raise(this, "Count");
        }

        [Conditional("DEBUG")]
        private static void DebugCheckPacketValidity(IEnumerable<VisualizerPacket> packets)
        {
            foreach (var packet in packets)
            {
                if (packet.Type != VisualizationType.VirtualVoiceData ||
                    packet.Size == 0)
                {
                    throw new ArgumentException(
                        string.Format("VoicesVisualizer - Virtual: Invalid packet with type: {0} and size: {1}.",
                                      packet.Type,
                                      packet.Size));
                }
            }
        }
    }
}