using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Threading;
using rage.ToolLib;
using Visualization.Core;
using Visualization.Entities;
using WPFToolLib.Extensions;

namespace VisualizerApp.Visualizers.Entities
{
    public class EntityViewModel : IEntityViewModel
    {
        private readonly IEntityModel m_model;
        private uint? m_selectedEntityId;

        public EntityViewModel(IEntityModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            m_model.DataChanged += OnDataChanged;
            Data = new BindingList<EntityData>();
        }

        #region IEntityViewModel Members

        public int Count
        {
            get { return Data.Count; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event Action<int> SelectItem;
        public event Action<VisualizationType, byte[], ushort> SelectionChanged;
        public bool OutgoingDataIsBigEndian { private get; set; }
        public BindingList<EntityData> Data { get; private set; }

        public bool Init()
        {
            return m_model.Init();
        }

        public void Reset()
        {
            m_model.Reset();
            m_selectedEntityId = null;
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets, bool shouldReset)
        {
            m_model.Visualize(packets, shouldReset);
        }

        public void Select(uint entityId, int index)
        {
            var selectedEntityChanged = false;
            if (m_selectedEntityId != null)
            {
                if (m_selectedEntityId.Value != entityId)
                {
                    m_selectedEntityId = entityId;
                    selectedEntityChanged = true;
                }
            }
            else
            {
                m_selectedEntityId = entityId;
                selectedEntityChanged = true;
            }

            if (selectedEntityChanged)
            {
                var entityIdBytes = BitConverter.GetBytes(entityId);
                if (OutgoingDataIsBigEndian)
                {
                    entityIdBytes = Utility.Reverse(entityIdBytes);
                }
                SelectionChanged.Raise<VisualizationType, byte[], ushort>(VisualizationType.EntitySelected,
                                                                          entityIdBytes,
                                                                          4);
            }
        }

        public void Dispose()
        {
            m_model.DataChanged -= OnDataChanged;
        }

        #endregion

        private void OnDataChanged(List<EntityData> entityData)
        {
            if (entityData != null)
            {
                Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal,
                                                                new Action<List<EntityData>>(UpdateData),
                                                                entityData);
            }
            else
            {
                Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal, new Action(ClearData));
            }
        }

        private void ClearData()
        {
            Data.Clear();
            PropertyChanged.Raise(this, "Count");
        }

        private void UpdateData(List<EntityData> entityData)
        {
            Data.RaiseListChangedEvents = false;
            Data.Clear();

            var selectedEntityIndex = -1;
            for (var i = 0; i < entityData.Count; ++i)
            {
                Data.Add(entityData[i]);
                if (m_selectedEntityId != null &&
                    entityData[i].Id == m_selectedEntityId.Value)
                {
                    selectedEntityIndex = i;
                }
            }

            Data.RaiseListChangedEvents = true;
            Data.ResetBindings();

            PropertyChanged.Raise(this, "Count");

            if (selectedEntityIndex >= 0)
            {
                SelectItem.Raise(selectedEntityIndex);
            }
            else
            {
                m_selectedEntityId = null;
            }
        }
    }
}