using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Microsoft.Win32;
using rage.ToolLib;
using Visualization;
using Visualization.Core;
using VisualizerApp.Timeline;
using WPFToolLib.Extensions;

namespace VisualizerApp
{
    public class AppWindowViewModel : IAppWindowViewModel
    {
        private readonly IAppWindowModel m_model;
        private readonly IVisualizerRepository m_visualizerRepository;

        public AppWindowViewModel(IAppWindowModel model,
                                  IVisualizerRepository visualizerRepository,
                                  ITimelineViewModel timelineViewModel,
                                  AppConfig config)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            if (visualizerRepository == null)
            {
                throw new ArgumentNullException("visualizerRepository");
            }
            if (timelineViewModel == null)
            {
                throw new ArgumentNullException("timelineViewModel");
            }
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            m_model = model;
            m_visualizerRepository = visualizerRepository;
            TimelineViewModel = timelineViewModel;

            m_model.ModeChanged += OnModeChanged;
            m_model.VisualizerActivated += OnVisualizerActivated;
            m_model.Connected += OnConnected;
            m_model.Disconnected += OnDisconnected;
            m_model.Exit += OnExit;
        }

        #region IAppWindowViewModel Members

        public event Action<IVisualizer> VisualizerActivated;
        public event Action<string> StatusMessageChanged;
        public event Action<string> ModeMessageChanged;
        public event Action Disconnected;
        public event Action Exit;
        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsConnected { get; private set; }

        public AppWindowMode Mode { get; private set; }

        public IEnumerable<string> VisualizerIds
        {
            get { return from visualizer in m_visualizerRepository.Visualizers orderby visualizer.Id select visualizer.Id; }
        }

        public ITimelineViewModel TimelineViewModel { get; private set; }

        public bool Start()
        {
            return m_model.Start();
        }

        public bool SetMode(AppWindowMode mode)
        {
            ModeMessageChanged.Raise(string.Empty);
            var modeChanged = true;
            switch (mode)
            {
                case AppWindowMode.Replay:
                    {
                        modeChanged = SetReplayMode();
                        break;
                    }
                case AppWindowMode.Live:
                    {
                        m_model.SwitchToLive();
                        break;
                    }
            }
            return modeChanged;
        }

        public void SelectVisualizer(string visualizerId)
        {
            if (!string.IsNullOrEmpty(visualizerId))
            {
                m_model.ActivateVisualizer(visualizerId);
            }
        }

        public void Closing()
        {
            if (m_model.RequiresSave)
            {
                var saved = false;
                while (!saved)
                {
                    var result = MessageBox.Show(App.Window,
                                                 "Would you like to save this session's history?",
                                                 "Save session history",
                                                 MessageBoxButton.YesNo,
                                                 MessageBoxImage.Exclamation,
                                                 MessageBoxResult.Yes);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            {
                                saved = Save();
                                break;
                            }
                        case MessageBoxResult.No:
                            {
                                saved = true;
                                break;
                            }
                    }
                }
            }
        }

        public void Stop()
        {
            m_model.Stop();
        }

        public void Dispose()
        {
            m_model.Exit -= OnExit;
            m_model.Disconnected -= OnDisconnected;
            m_model.Connected -= OnConnected;
            m_model.VisualizerActivated -= OnVisualizerActivated;
            m_model.ModeChanged -= OnModeChanged;
        }

        #endregion

        private void OnExit()
        {
            Exit.Raise();
        }

        private void OnConnected()
        {
            IsConnected = true;
            PropertyChanged.Raise(this, "IsConnected");
        }

        private void OnVisualizerActivated(IVisualizer visualizer)
        {
            VisualizerActivated.Raise(visualizer);
            StatusMessageChanged.Raise(visualizer != null
                                           ? string.Format("Visualizing: {0}", visualizer.Id)
                                           : "No visualizer selected");
        }

        private void OnModeChanged(AppWindowMode mode)
        {
            Mode = mode;
            PropertyChanged.Raise(this, "Mode");
        }

        private void OnDisconnected()
        {
            IsConnected = false;
            PropertyChanged.Raise(this, "IsConnected");
            Disconnected.Raise();
        }

        private bool Save()
        {
            var saveFileDlg = new SaveFileDialog
                                  {
                                      AddExtension = true,
                                      CheckPathExists = true,
                                      DefaultExt = ".vis",
                                      Filter = "Visualizer History Files|*.vis",
                                      ValidateNames = true
                                  };
            var result = saveFileDlg.ShowDialog();
            if (result.Value)
            {
                return m_model.SaveHistory(saveFileDlg.FileName);
            }
            return false;
        }

        private bool SetReplayMode()
        {
            var fileOpenDlg = new OpenFileDialog
                                  {
                                      AddExtension = true,
                                      CheckFileExists = true,
                                      CheckPathExists = true,
                                      DefaultExt = ".vis",
                                      Filter = "Visualizer History Files|*.vis",
                                      Multiselect = false,
                                      ValidateNames = true
                                  };

            var result = fileOpenDlg.ShowDialog();
            if (result.Value &&
                m_model.SwitchToReplay(fileOpenDlg.FileName))
            {
                ModeMessageChanged.Raise(string.Format("Replaying: {0}", fileOpenDlg.FileName));
                return true;
            }
            return false;
        }
    }
}