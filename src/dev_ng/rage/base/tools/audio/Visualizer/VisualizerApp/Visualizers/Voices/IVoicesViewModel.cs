using System;
using System.Collections.Generic;
using System.ComponentModel;
using Visualization.Core;
using Visualization.Voices;

namespace VisualizerApp.Visualizers.Voices
{
    public interface IVoicesViewModel : INotifyPropertyChanged,
                                        IDisposable
    {
        VoiceStartData VoiceStart { get; }

        IVirtualVoiceViewModel VirtualVoices { get; }

        IPhysicalVoiceViewModel PhysicalVoices { get; }

        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets);
    }
}