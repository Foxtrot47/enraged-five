using System;
using System.Collections.Generic;
using System.ComponentModel;
using Visualization.Core;
using Visualization.WaveSlots;

namespace VisualizerApp.Visualizers.WaveSlots
{
    public class WaveSlotsViewModel : IWaveSlotsViewModel
    {
        private readonly IWaveSlotsModel m_model;

        public WaveSlotsViewModel(IWaveSlotsModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
        }

        #region IWaveSlotsViewModel Members

        public event PropertyChangedEventHandler PropertyChanged;

        public bool Init()
        {
            return m_model.Init();
        }

        public void Reset()
        {
            m_model.Reset();
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets)
        {
            m_model.Visualize(packets);
        }

        public void Dispose()
        {
        }

        #endregion
    }
}