﻿using System.Collections.Generic;
using System.Windows;
using Visualization.Core;

namespace VisualizerApp.Visualizers.Voices
{
    /// <summary>
    ///   Interaction logic for VoicesView.xaml
    /// </summary>
    public partial class VoicesView : IVisualizer
    {
        private IVoicesViewModel m_viewModel;

        private readonly VisualizationType[] m_types = new[]
                                                           {
                                                               VisualizationType.VoiceStart,
                                                               VisualizationType.VirtualVoiceData,
                                                               VisualizationType.PhysicalVoiceData
                                                           };

        public VoicesView()
        {
            InitializeComponent();
        }

        #region IVisualizer Members

        public string Id
        {
            get { return "Voices"; }
        }

        public IEnumerable<VisualizationType> Types
        {
            get { return m_types; }
        }

        public bool Init()
        {
            m_viewModel = this.ViewModel<IVoicesViewModel>();
            return m_viewModel.Init();
        }

        public void Reset()
        {
            m_viewModel.Reset();
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets, bool shouldReset)
        {
            m_viewModel.Visualize(packets);
        }

        public void Shutdown()
        {
            if (m_viewModel != null)
            {
                m_viewModel.Reset();
                m_viewModel = null;
            }
        }

        #endregion
    }
}