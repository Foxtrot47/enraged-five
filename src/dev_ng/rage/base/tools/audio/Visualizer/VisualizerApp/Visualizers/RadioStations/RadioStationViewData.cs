namespace VisualizerApp.Visualizers.RadioStations
{
    public class RadioStationViewData
    {
        public string CurrentTrack { get; set; }
        public string State { get; set; }
        public string PlayTime { get; set; }
    }
}