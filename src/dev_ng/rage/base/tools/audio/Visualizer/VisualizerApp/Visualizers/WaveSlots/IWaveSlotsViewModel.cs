using System;
using System.Collections.Generic;
using System.ComponentModel;
using Visualization.Core;

namespace VisualizerApp.Visualizers.WaveSlots
{
    public interface IWaveSlotsViewModel : IDisposable,
                                           INotifyPropertyChanged
    {
        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets);
    }
}