using System;
using rage.ToolLib;
using Visualization.Timeline;

namespace VisualizerApp.Timeline
{
    public class TimelineViewModel : ITimelineViewModel
    {
        private readonly ITimelineModel m_model;

        public TimelineViewModel(ITimelineModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            m_model.StartTimeChanged += OnStartTimeChanged;
            m_model.CurrentTimeChanged += OnCurrentTimeChanged;
            m_model.EndTimeChanged += OnEndTimeChanged;
            m_model.Reset += OnReset;
        }

        #region ITimelineViewModel Members

        public event Action<string> StartTimeChanged;
        public event Action<string> CurrentTimeChanged;
        public event Action<string> EndTimeChanged;
        public event Action Reset;

        public bool IsValidTime(string text)
        {
            TimeSpan temp;
            return TimeSpan.TryParse(text, out temp);
        }

        public void SetCurrentTime(string text)
        {
            TimeSpan time;
            if (TimeSpan.TryParse(text, out time))
            {
                m_model.CurrentTime = time;
            }
        }

        public void SetCurrentTimeAsT(double value)
        {
            var range = (m_model.EndTime - m_model.StartTime).TotalMilliseconds;
            m_model.CurrentTime = TimeSpan.FromMilliseconds(m_model.StartTime.TotalMilliseconds + range * value);
        }

        public double GetCurrentTimeAsT()
        {
            var range = (m_model.EndTime - m_model.StartTime).TotalMilliseconds;
            var offset = (m_model.CurrentTime - m_model.StartTime).TotalMilliseconds;
            var value = range == 0 ? 0 : offset / range;
            return value;
        }

        public void Dispose()
        {
            m_model.EndTimeChanged -= OnEndTimeChanged;
            m_model.CurrentTimeChanged -= OnCurrentTimeChanged;
            m_model.StartTimeChanged -= OnStartTimeChanged;
            m_model.Reset -= OnReset;
        }

        #endregion

        private void OnReset()
        {
            Reset.Raise();
        }

        private void OnStartTimeChanged(TimeSpan time)
        {
            StartTimeChanged.Raise(time.ToString());
        }

        private void OnCurrentTimeChanged(TimeSpan time)
        {
            CurrentTimeChanged.Raise(time.ToString());
        }

        private void OnEndTimeChanged(TimeSpan time)
        {
            EndTimeChanged.Raise(time.ToString());
        }
    }
}