using System.ComponentModel;
using WPFToolLib.Extensions;

namespace VisualizerApp.TreeGrid
{
    public class TreeGridItemViewModel<T> : INotifyPropertyChanged where T : class
    {
        private T m_data;

        public TreeGridItemViewModel(TreeGridItemViewModel<T> parent)
        {
            Parent = parent;
            Children = new BindingList<TreeGridItemViewModel<T>>();
        }

        public TreeGridItemViewModel<T> Parent { get; private set; }

        public BindingList<TreeGridItemViewModel<T>> Children { get; private set; }

        public T Data
        {
            get { return m_data; }
            set
            {
                m_data = value;
                if (m_data != null)
                {
                    PropertyChanged.Raise(this, "Data");
                }
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}