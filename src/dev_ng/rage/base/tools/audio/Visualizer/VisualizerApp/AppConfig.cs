using System.Configuration;

namespace VisualizerApp
{
    public class AppConfig : ConfigurationSection
    {
        private const string ROOT = "root";
        private const string PROJECT_SETTINGS_FILE = "projectSettingsFile";

        [ConfigurationProperty(ROOT, IsKey = false, IsRequired = true)]
        public string Root
        {
            get { return (string) this[ROOT]; }
        }

        [ConfigurationProperty(PROJECT_SETTINGS_FILE, IsKey = false, IsRequired = true)]
        public string ProjectSettingsFile
        {
            get { return (string) this[PROJECT_SETTINGS_FILE]; }
        }
    }
}