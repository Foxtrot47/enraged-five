using System;
using System.Collections.Generic;
using System.ComponentModel;
using Visualization;
using Visualization.Core;
using VisualizerApp.Timeline;

namespace VisualizerApp
{
    public interface IAppWindowViewModel : INotifyPropertyChanged,
                                           IDisposable
    {
        bool IsConnected { get; }
        AppWindowMode Mode { get; }
        IEnumerable<string> VisualizerIds { get; }
        ITimelineViewModel TimelineViewModel { get; }
        event Action<IVisualizer> VisualizerActivated;
        event Action<string> StatusMessageChanged;
        event Action<string> ModeMessageChanged;
        event Action Disconnected;
        event Action Exit;

        bool Start();

        bool SetMode(AppWindowMode mode);

        void SelectVisualizer(string visualizerId);

        void Closing();

        void Stop();
    }
}