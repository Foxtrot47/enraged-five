using System.Configuration;
using Visualization.RadioStations;

namespace VisualizerApp.Visualizers.RadioStations
{
    public class RadioStationsConfig : ConfigurationSection,
                                       IRadioStationsConfig
    {
        #region IRadioStationsConfig Members

        public string Root { get; set; }

        #endregion
    }
}