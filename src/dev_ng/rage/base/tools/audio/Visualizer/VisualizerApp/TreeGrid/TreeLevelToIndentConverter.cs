using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace VisualizerApp.TreeGrid
{
    public class TreeLevelToIndentConverter : IValueConverter
    {
        public TreeLevelToIndentConverter()
        {
            IndentSize = 10;
            Direction = IndentDirection.Left;
            Margin = new Thickness(0);
        }

        public double IndentSize { get; set; }

        public IndentDirection Direction { get; set; }

        public Thickness Margin { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var left = Margin.Left;
            var top = Margin.Top;
            var right = Margin.Right;
            var bottom = Margin.Bottom;

            var level = (int) value;
            switch (Direction)
            {
                case IndentDirection.Left:
                    {
                        left += (level * IndentSize);
                        break;
                    }
                case IndentDirection.Top:
                    {
                        top += (level * IndentSize);
                        break;
                    }
                case IndentDirection.Right:
                    {
                        right += (level * IndentSize);
                        break;
                    }
                case IndentDirection.Bottom:
                    {
                        bottom += (level * IndentSize);
                        break;
                    }
            }
            return new Thickness(left, top, right, bottom);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}