using System;
using System.Collections.Generic;
using System.ComponentModel;
using Visualization.Core;
using Visualization.Voices;

namespace VisualizerApp.Visualizers.Voices
{
    public interface IVirtualVoiceViewModel : INotifyPropertyChanged,
                                              IDisposable
    {
        BindingList<VirtualVoiceData> Data { get; }

        int Count { get; }

        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets);
    }
}