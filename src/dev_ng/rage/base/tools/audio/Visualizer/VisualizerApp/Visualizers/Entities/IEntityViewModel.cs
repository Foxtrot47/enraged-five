using System;
using System.Collections.Generic;
using System.ComponentModel;
using Visualization.Core;
using Visualization.Entities;

namespace VisualizerApp.Visualizers.Entities
{
    public interface IEntityViewModel : INotifyPropertyChanged,
                                        INotifySelectionChanged,
                                        IDisposable
    {
        BindingList<EntityData> Data { get; }
        int Count { get; }
        event Action<int> SelectItem;

        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets, bool shouldReset);

        void Select(uint entityId, int index);
    }
}