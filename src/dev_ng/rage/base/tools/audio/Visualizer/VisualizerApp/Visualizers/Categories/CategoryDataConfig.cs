using System.Configuration;
using Visualization.Categories;

namespace VisualizerApp.Visualizers.Categories
{
    public class CategoryDataConfig : ConfigurationSection,
                                      ICategoryDataConfig
    {
        private const string VOLUME_LIMITS = "volumeLimits";
        private const string IS_BIG_ENDIAN = "isBigEndian";
        private const string CATEGORY_METADATA_FILE = "categoryMetaDataFile";
        private const string CATEGORY_DATA_FILE = "categoryDataFile";
        private const string DISTANCE_ROLLOFF_SCALE_LIMITS = "distanceRollOffScaleLimits";
        private const string OCCLUSION_DAMPING_LIMITS = "occlusionDampingLimits";
        private const string ENVIRONMENTAL_FILTER_DAMPING_LIMITS = "environmentalFilterDampingLimits";
        private const string SOURCE_REVERB_DAMPING_LIMITS = "sourceReverbDampingLimits";
        private const string DISTANCE_REVERB_DAMPING_LIMITS = "distanceReverbDampingLimits";
        private const string ENVIRONMENTAL_LOUDNESS_LIMITS = "environmentalLoudnessLimits";
        private const string PITCH_LIMITS = "pitchLimits";
        private const string LPF_CUTOFF_LIMITS = "lpfCutoffLimits";
        private const string HPF_CUTOFF_LIMITS = "hpfCutoffLimits";

        #region ICategoryDataConfig Members

        public string Root { get; set; }

        [ConfigurationProperty(IS_BIG_ENDIAN, IsRequired = true, IsKey = false)]
        public bool IsBigEndian
        {
            get { return (bool) this[IS_BIG_ENDIAN]; }
        }

        [ConfigurationProperty(CATEGORY_METADATA_FILE, IsRequired = true, IsKey = false)]
        public string CategoryMetaDataFile
        {
            get { return (string) this[CATEGORY_METADATA_FILE]; }
        }

        [ConfigurationProperty(CATEGORY_DATA_FILE, IsRequired = true, IsKey = false)]
        public string CategoryDataFile
        {
            get { return (string) this[CATEGORY_DATA_FILE]; }
        }

        [ConfigurationProperty(VOLUME_LIMITS, IsRequired = true, IsKey = false)]
        public string VolumeLimits
        {
            get { return (string) this[VOLUME_LIMITS]; }
        }

        [ConfigurationProperty(DISTANCE_ROLLOFF_SCALE_LIMITS, IsRequired = true, IsKey = false)]
        public string DistanceRollOffScaleLimits
        {
            get { return (string) this[DISTANCE_ROLLOFF_SCALE_LIMITS]; }
        }

        [ConfigurationProperty(OCCLUSION_DAMPING_LIMITS, IsRequired = true, IsKey = false)]
        public string OcclusionDampingLimits
        {
            get { return (string) this[OCCLUSION_DAMPING_LIMITS]; }
        }

        [ConfigurationProperty(ENVIRONMENTAL_FILTER_DAMPING_LIMITS, IsRequired = true, IsKey = false)]
        public string EnvironmentalFilterDampingLimits
        {
            get { return (string) this[ENVIRONMENTAL_FILTER_DAMPING_LIMITS]; }
        }

        [ConfigurationProperty(SOURCE_REVERB_DAMPING_LIMITS, IsRequired = true, IsKey = false)]
        public string SourceReverbDampingLimits
        {
            get { return (string) this[SOURCE_REVERB_DAMPING_LIMITS]; }
        }

        [ConfigurationProperty(DISTANCE_REVERB_DAMPING_LIMITS, IsRequired = true, IsKey = false)]
        public string DistanceReverbDampingLimits
        {
            get { return (string) this[DISTANCE_REVERB_DAMPING_LIMITS]; }
        }

        [ConfigurationProperty(ENVIRONMENTAL_LOUDNESS_LIMITS, IsRequired = true, IsKey = false)]
        public string EnvironmentalLoudnessLimits
        {
            get { return (string) this[ENVIRONMENTAL_LOUDNESS_LIMITS]; }
        }

        [ConfigurationProperty(PITCH_LIMITS, IsRequired = true, IsKey = false)]
        public string PitchLimits
        {
            get { return (string) this[PITCH_LIMITS]; }
        }

        [ConfigurationProperty(LPF_CUTOFF_LIMITS, IsRequired = true, IsKey = false)]
        public string LpfCutoffLimits
        {
            get { return (string) this[LPF_CUTOFF_LIMITS]; }
        }

        [ConfigurationProperty(HPF_CUTOFF_LIMITS, IsRequired = true, IsKey = false)]
        public string HpfCutoffLimits
        {
            get { return (string) this[HPF_CUTOFF_LIMITS]; }
        }

        #endregion
    }
}