using System;
using System.Collections.Generic;
using System.ComponentModel;
using Visualization.Core;
using Visualization.Voices;

namespace VisualizerApp.Visualizers.Voices
{
    public interface IPhysicalVoiceViewModel : INotifyPropertyChanged,
                                               IDisposable
    {
        BindingList<PhysicalVoiceData> Data { get; }

        int Count { get; }

        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets);
    }
}