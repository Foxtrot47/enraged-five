﻿using System.Collections.Generic;
using System.Windows;
using Visualization.Core;

namespace VisualizerApp.Visualizers.RadioStations
{
    /// <summary>
    ///   Interaction logic for RadioStationsView.xaml
    /// </summary>
    public partial class RadioStationsView : IVisualizer
    {
        private IRadioStationsViewModel m_viewModel;

        public RadioStationsView()
        {
            InitializeComponent();
            Types = new List<VisualizationType> {VisualizationType.RadioStationData};
        }

        #region IVisualizer Members

        public string Id
        {
            get { return "Radio Stations"; }
        }

        public IEnumerable<VisualizationType> Types { get; private set; }

        public bool Init()
        {
            m_viewModel = this.ViewModel<IRadioStationsViewModel>();
            return m_viewModel.Init();
        }

        public void Reset()
        {
            m_viewModel.Reset();
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets, bool shouldReset)
        {
            m_viewModel.Visualize(packets);
        }

        public void Shutdown()
        {
            if (m_viewModel != null)
            {
                m_viewModel.Reset();
                m_viewModel = null;
            }
        }

        #endregion
    }
}