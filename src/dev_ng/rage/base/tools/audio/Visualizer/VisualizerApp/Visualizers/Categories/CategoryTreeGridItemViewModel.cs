using Visualization.Categories;
using VisualizerApp.TreeGrid;

namespace VisualizerApp.Visualizers.Categories
{
    public class CategoryTreeGridItemViewModel : TreeGridItemViewModel<CategoryData>
    {
        public CategoryTreeGridItemViewModel() : base(null)
        {
        }

        public CategoryTreeGridItemViewModel(TreeGridItemViewModel<CategoryData> parent) : base(parent)
        {
        }
    }
}