﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Autofac;
using rage.ToolLib.Logging;
using VisualizerApp.Logging;

namespace VisualizerApp
{
    /// <summary>
    ///   Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private IContainer m_container;

        public static Thread UiThread { get; private set; }

        public static Window Window { get; private set; }

        private ILog Log { get; set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);           

            Log = new MessageBoxLog(new ContextStack()) {Verbosity = LogVerbosity.Everything};
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
            Current.DispatcherUnhandledException += OnDispatcherUnhandledException;

            m_container = ContainerConfiguration.Build(Log);
            UiThread = Thread.CurrentThread;
            Window = m_container.Resolve<AppWindowView>();
            Window.Show();
        }

        private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            try
            {
                Log.Exception(e.Exception);
                e.Handled = true;
            }
            finally
            {
                Current.Shutdown(-1);
            }
        }

        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Log.Exception(e.ExceptionObject as Exception);
            }
            finally
            {
                Current.Shutdown(-1);
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            m_container.Dispose();

            AppDomain.CurrentDomain.UnhandledException -= OnUnhandledException;
            Current.DispatcherUnhandledException -= OnDispatcherUnhandledException;

            base.OnExit(e);
        }
    }
}