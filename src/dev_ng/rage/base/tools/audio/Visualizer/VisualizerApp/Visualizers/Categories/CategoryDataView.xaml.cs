﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using Visualization.Core;

namespace VisualizerApp.Visualizers.Categories
{
    /// <summary>
    ///   Interaction logic for CategoryDataView.xaml
    /// </summary>
    public partial class CategoryDataView : IVisualizer
    {
        private readonly VisualizationType[] m_types = new[] {VisualizationType.ResolvedCategoryData};
        private Storyboard m_maxChangeColourAnimation;
        private Storyboard m_medChangeColourAnimation;
        private Storyboard m_minChangeColourAnimation;
        private ICategoryDataViewModel m_viewModel;

        public CategoryDataView()
        {
            InitializeComponent();
        }

        #region IVisualizer Members

        public string Id
        {
            get { return "Categories"; }
        }

        public IEnumerable<VisualizationType> Types
        {
            get { return m_types; }
        }

        public bool Init()
        {
            m_minChangeColourAnimation = Resources["MinChangeColourAnimation"] as Storyboard;
            m_medChangeColourAnimation = Resources["MedChangeColourAnimation"] as Storyboard;
            m_maxChangeColourAnimation = Resources["MaxChangeColourAnimation"] as Storyboard;

            m_viewModel = this.ViewModel<ICategoryDataViewModel>();
            return m_viewModel.Init();
        }

        public void Reset()
        {
            m_viewModel.Reset();
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets, bool shouldReset)
        {
            m_viewModel.Visualize(packets);
        }

        public void Shutdown()
        {
            m_minChangeColourAnimation = null;
            m_medChangeColourAnimation = null;
            m_maxChangeColourAnimation = null;

            if (m_viewModel != null)
            {
                m_viewModel.Reset();
                m_viewModel = null;
            }
        }

        #endregion

        private void OnVolumeTextChanged(object sender, TextChangedEventArgs e)
        {
            var control = sender as TextBox;
            var categoryData = (control.DataContext as CategoryTreeGridItemViewModel).Data;
            Animate(control, "Volume", categoryData.OldVolume, categoryData.Volume);
        }

        private void OnEnvironmentalFilterDampingTextChanged(object sender, TextChangedEventArgs e)
        {
            var control = sender as TextBox;
            var categoryData = (control.DataContext as CategoryTreeGridItemViewModel).Data;
            Animate(control,
                    "EnvironmentalFilterDamping",
                    categoryData.OldEnvironmentalFilterDamping,
                    categoryData.EnvironmentalFilterDamping);
        }

        private void OnOcclusionDampingTextChanged(object sender, TextChangedEventArgs e)
        {
            var control = sender as TextBox;
            var categoryData = (control.DataContext as CategoryTreeGridItemViewModel).Data;
            Animate(control, "OcclusionDamping", categoryData.OldOcclusionDamping, categoryData.OcclusionDamping);
            e.Handled = true;
        }

        private void OnSourceReverbDampingTextChanged(object sender, TextChangedEventArgs e)
        {
            var control = sender as TextBox;
            var categoryData = (control.DataContext as CategoryTreeGridItemViewModel).Data;
            Animate(control,
                    "SourceReverbDamping",
                    categoryData.OldSourceReverbDamping,
                    categoryData.SourceReverbDamping);
            e.Handled = true;
        }

        private void OnDistanceReverbDampingTextChanged(object sender, TextChangedEventArgs e)
        {
            var control = sender as TextBox;
            var categoryData = (control.DataContext as CategoryTreeGridItemViewModel).Data;
            Animate(control,
                    "DistanceReverbDamping",
                    categoryData.OldDistanceReverbDamping,
                    categoryData.DistanceReverbDamping);
            e.Handled = true;
        }

        private void OnDistanceRollOffScaleTextChanged(object sender, TextChangedEventArgs e)
        {
            var control = sender as TextBox;
            var categoryData = (control.DataContext as CategoryTreeGridItemViewModel).Data;
            Animate(control,
                    "DistanceRollOffScale",
                    categoryData.OldDistanceRollOffScale,
                    categoryData.DistanceRollOffScale);
            e.Handled = true;
        }

        private void OnEnvironmentalLoudnessTextChanged(object sender, TextChangedEventArgs e)
        {
            var control = sender as TextBox;
            var categoryData = (control.DataContext as CategoryTreeGridItemViewModel).Data;
            Animate(control,
                    "EnvironmentalLoudness",
                    categoryData.OldEnvironmentalLoudness,
                    categoryData.EnvironmentalLoudness);
            e.Handled = true;
        }

        private void OnPitchTextChanged(object sender, TextChangedEventArgs e)
        {
            var control = sender as TextBox;
            var categoryData = (control.DataContext as CategoryTreeGridItemViewModel).Data;
            Animate(control, "Pitch", categoryData.OldPitch, categoryData.Pitch);
            e.Handled = true;
        }

        private void OnLpfCutoffTextChanged(object sender, TextChangedEventArgs e)
        {
            var control = sender as TextBox;
            var categoryData = (control.DataContext as CategoryTreeGridItemViewModel).Data;
            Animate(control, "LpfCutoff", categoryData.OldLpfCutoff, categoryData.LpfCutoff);
            e.Handled = true;
        }

        private void OnHpfCutoffTextChanged(object sender, TextChangedEventArgs e)
        {
            var control = sender as TextBox;
            var categoryData = (control.DataContext as CategoryTreeGridItemViewModel).Data;
            Animate(control, "HpfCutoff", categoryData.OldHpfCutoff, categoryData.HpfCutoff);
        }

        private void Animate(FrameworkElement element, string property, float previousValue, float currentValue)
        {
            CategoryDataLimitInfo info;
            if (m_viewModel.TryGetLimitInfo(property, out info))
            {
                var min = info.Min;
                var max = info.Max;
                var delta = Math.Abs(currentValue - previousValue);
                if (info.IsPercentage)
                {
                    delta = Math.Abs(delta / previousValue) * 100;
                }

                if (delta >= max)
                {
                    m_maxChangeColourAnimation.Begin(element, true);
                }
                else if (delta > min)
                {
                    if (!IsAnimationPlaying(element, m_maxChangeColourAnimation))
                    {
                        m_medChangeColourAnimation.Begin(element, true);
                    }
                }
                else
                {
                    if (!IsAnimationPlaying(element, m_maxChangeColourAnimation) &&
                        !IsAnimationPlaying(element, m_medChangeColourAnimation))
                    {
                        m_minChangeColourAnimation.Begin(element, true);
                    }
                }
            }
        }

        private static bool IsAnimationPlaying(FrameworkElement element, Storyboard storyboard)
        {
            try
            {
                if (storyboard.GetCurrentState(element) !=
                    ClockState.Active)
                {
                    return false;
                }
            }
            catch (InvalidOperationException)
            {
                return false;
            }
            return true;
        }

        private void OnTreeExpanded(object sender, RoutedEventArgs e)
        {
            treeGridView.Traverse(item => item.IsExpanded = true);
            e.Handled = true;
        }

        private void OnTreeCollapsed(object sender, RoutedEventArgs e)
        {
            treeGridView.Traverse(item => item.IsExpanded = false);
            e.Handled = true;
        }
    }
}