﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Visualization;
using Visualization.Core;
using VisualizerApp.Timeline;

namespace VisualizerApp
{
    /// <summary>
    ///   Interaction logic for AppWindowView.xaml
    /// </summary>
    public partial class AppWindowView
    {
        private IAppWindowViewModel m_viewModel;

        public AppWindowView()
        {
            InitializeComponent();
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            m_viewModel = this.ViewModel<IAppWindowViewModel>();
            m_viewModel.VisualizerActivated += OnVisualizerActivated;
            m_viewModel.StatusMessageChanged += OnStatusMessageChanged;
            m_viewModel.ModeMessageChanged += OnModeMessageChanged;
            m_viewModel.Disconnected += OnDisconnected;
            m_viewModel.Exit += OnExit;

            (timelineExpander.Content as TimelineView).DataContext = m_viewModel.TimelineViewModel;

            if (m_viewModel.Start())
            {
                visualizerIds.DataContext = m_viewModel;

                visualizerIds.SelectedIndex = 0;
                OnVisualizersSelectionChanged(null, null);
                visualizerIds.Focus();

                OnSwitchToLiveClicked(null, null);
            }
        }

        private void OnExit()
        {
            Close();
        }

        private void OnDisconnected()
        {
            Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal, new Action(Close));
        }

        private void OnModeMessageChanged(string modeMsg)
        {
            modeMessage.Text = modeMsg;
        }

        private void OnStatusMessageChanged(string statusMsg)
        {
            statusMessage.Text = statusMsg;
        }

        private void OnClosed(object sender, EventArgs e)
        {
            if (m_viewModel != null)
            {
                m_viewModel.Exit -= OnExit;
                m_viewModel.Disconnected -= OnDisconnected;
                m_viewModel.ModeMessageChanged -= OnModeMessageChanged;
                m_viewModel.StatusMessageChanged -= OnStatusMessageChanged;
                m_viewModel.VisualizerActivated -= OnVisualizerActivated;
            }
        }

        private void OnExitClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnVisualizersSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedVisualizer = visualizerIds.SelectedItem as string;
            if (!string.IsNullOrEmpty(selectedVisualizer))
            {
                m_viewModel.SelectVisualizer(selectedVisualizer);
            }
        }

        private void OnSwitchToLiveClicked(object sender, RoutedEventArgs e)
        {
            if (m_viewModel.SetMode(AppWindowMode.Live))
            {
                timelineExpander.Visibility = Visibility.Collapsed;
            }
        }

        private void OnSwitchToReplayClicked(object sender, RoutedEventArgs e)
        {
            if (m_viewModel.SetMode(AppWindowMode.Replay))
            {
                timelineExpander.Visibility = Visibility.Visible;
            }
        }

        private void OnVisualizerActivated(IVisualizer visualizer)
        {
            rootContainer.Children.Clear();
            if (visualizer != null)
            {
                rootContainer.Children.Add(visualizer as UIElement);
            }
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            if (m_viewModel != null)
            {
                m_viewModel.Stop();
                m_viewModel.Closing();
            }
        }
    }
}