using System;
using System.Collections.Generic;
using rage.ToolLib.Logging;
using Visualization.Categories;

namespace VisualizerApp.Visualizers.Categories
{
    public class LimitsParser : ILimitsParser
    {
        private readonly ICategoryDataConfig m_config;
        private readonly IDictionary<string, CategoryDataLimitInfo> m_limitsLookup;
        private readonly ILog m_log;

        public LimitsParser(ILog log, ICategoryDataConfig config)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            m_log = log;
            m_config = config;
            m_limitsLookup = new Dictionary<string, CategoryDataLimitInfo>();
        }

        #region ILimitsParser Members

        public bool Parse()
        {
            return ParseInfo(m_config.VolumeLimits, "Volume") &&
                   ParseInfo(m_config.DistanceRollOffScaleLimits, "DistanceRollOffScale") &&
                   ParseInfo(m_config.OcclusionDampingLimits, "OcclusionDamping") &&
                   ParseInfo(m_config.EnvironmentalFilterDampingLimits, "EnvironmentalFilterDamping") &&
                   ParseInfo(m_config.SourceReverbDampingLimits, "SourceReverbDamping") &&
                   ParseInfo(m_config.DistanceReverbDampingLimits, "DistanceReverbDamping") &&
                   ParseInfo(m_config.EnvironmentalLoudnessLimits, "EnvironmentalLoudness") &&
                   ParseInfo(m_config.PitchLimits, "Pitch") && ParseInfo(m_config.LpfCutoffLimits, "LpfCutoff") &&
                   ParseInfo(m_config.HpfCutoffLimits, "HpfCutoff");
        }

        public bool TryGetLimitInfo(string property, out CategoryDataLimitInfo info)
        {
            return m_limitsLookup.TryGetValue(property, out info);
        }

        #endregion

        private bool ParseInfo(string values, string property)
        {
            var valueComponents = values.Split(',');
            if (valueComponents.Length == 2)
            {
                var isPercentage = valueComponents[0].Contains("%") || valueComponents[1].Contains("%");
                if (isPercentage)
                {
                    valueComponents[0] = valueComponents[0].Replace("%", string.Empty);
                    valueComponents[1] = valueComponents[1].Replace("%", string.Empty);
                }

                float min, max;
                if (float.TryParse(valueComponents[0], out min) &&
                    float.TryParse(valueComponents[1], out max))
                {
                    if (min < 0 ||
                        max < 0)
                    {
                        m_log.Error(
                            "\"min\" and \"max\" limits must be greater than or equal to 0 for property \"{0}\"",
                            property);
                    }
                    else if (min > max)
                    {
                        m_log.Error("\"min\" limit must be less than or equal to \"max\" limit for property \"{0}\"",
                                    property);
                    }
                    else
                    {
                        m_limitsLookup.Add(property, new CategoryDataLimitInfo(min, max, isPercentage));
                        return true;
                    }
                }
                else
                {
                    m_log.Error("Failed to parse limits for property \"{0}\".", property);
                }
            }
            return false;
        }
    }
}