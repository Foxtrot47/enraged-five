using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Threading;
using Visualization.Core;
using Visualization.Voices;
using WPFToolLib.Extensions;

namespace VisualizerApp.Visualizers.Voices
{
    public class VoicesViewModel : IVoicesViewModel
    {
        private readonly IVoiceStartModel m_voiceStartModel;

        public VoicesViewModel(IVoiceStartModel voiceStartModel,
                               IVirtualVoiceViewModel virtualVoiceViewModel,
                               IPhysicalVoiceViewModel physicalVoiceViewModel)
        {
            if (voiceStartModel == null)
            {
                throw new ArgumentNullException("voiceStartModel");
            }
            if (virtualVoiceViewModel == null)
            {
                throw new ArgumentNullException("virtualVoiceViewModel");
            }
            if (physicalVoiceViewModel == null)
            {
                throw new ArgumentNullException("physicalVoiceViewModel");
            }
            m_voiceStartModel = voiceStartModel;
            VirtualVoices = virtualVoiceViewModel;
            PhysicalVoices = physicalVoiceViewModel;

            m_voiceStartModel.DataChanged += OnDataChanged;
        }

        #region IVoicesViewModel Members

        public event PropertyChangedEventHandler PropertyChanged;

        public VoiceStartData VoiceStart { get; private set; }
        public IVirtualVoiceViewModel VirtualVoices { get; private set; }
        public IPhysicalVoiceViewModel PhysicalVoices { get; private set; }

        public bool Init()
        {
            return VirtualVoices.Init() && PhysicalVoices.Init();
        }

        public void Reset()
        {
            m_voiceStartModel.Reset();
            VirtualVoices.Reset();
            PhysicalVoices.Reset();
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets)
        {
            DebugCheckPacketValidity(packets);

            VisualizerPacket startPacket = null;
            var virtualVoicePackets = new List<VisualizerPacket>();
            var physicalVoicePackets = new List<VisualizerPacket>();
            foreach (var packet in packets)
            {
                switch (packet.Type)
                {
                    case VisualizationType.VoiceStart:
                        {
                            startPacket = packet;
                            virtualVoicePackets.Clear();
                            physicalVoicePackets.Clear();
                            VirtualVoices.Reset();
                            PhysicalVoices.Reset();
                            break;
                        }
                    case VisualizationType.VirtualVoiceData:
                        {
                            virtualVoicePackets.Add(packet);
                            break;
                        }
                    case VisualizationType.PhysicalVoiceData:
                        {
                            physicalVoicePackets.Add(packet);
                            break;
                        }
                }
            }

            if (startPacket != null)
            {
                m_voiceStartModel.Visualize(startPacket);
            }

            if (virtualVoicePackets.Count > 0)
            {
                VirtualVoices.Visualize(virtualVoicePackets);
            }

            if (physicalVoicePackets.Count > 0)
            {
                PhysicalVoices.Visualize(physicalVoicePackets);
            }
        }

        public void Dispose()
        {
            m_voiceStartModel.DataChanged -= OnDataChanged;
        }

        #endregion

        private void OnDataChanged(VoiceStartData voiceStartData)
        {
            Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal,
                                                            new Action<VoiceStartData>(UpdateVoiceStartData),
                                                            voiceStartData);
        }

        private void UpdateVoiceStartData(VoiceStartData startData)
        {
            VoiceStart = startData;
            PropertyChanged.Raise(this, "VoiceStart");
        }

        [Conditional("DEBUG")]
        private static void DebugCheckPacketValidity(IEnumerable<VisualizerPacket> packets)
        {
            foreach (var packet in packets)
            {
                if (
                    (!(packet.Type == VisualizationType.VoiceStart || packet.Type == VisualizationType.VirtualVoiceData ||
                       packet.Type == VisualizationType.PhysicalVoiceData)) ||
                    packet.Size == 0)
                {
                    throw new ArgumentException(
                        string.Format("VoicesVisualizer: Invalid packet with type: {0} and size: {1}.",
                                      packet.Type,
                                      packet.Size));
                }
            }
        }
    }
}