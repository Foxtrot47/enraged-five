using System.Windows;

namespace VisualizerApp
{
    public static class Extensions
    {
        public static T ViewModel<T>(this FrameworkElement element) where T : class
        {
            return element.DataContext as T;
        }
    }
}