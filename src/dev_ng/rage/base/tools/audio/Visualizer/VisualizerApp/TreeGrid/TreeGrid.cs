using System;
using System.Windows;
using System.Windows.Controls;

namespace VisualizerApp.TreeGrid
{
    public class TreeGrid : TreeView
    {
        private readonly GridViewColumnCollection m_columns;

        public TreeGrid()
        {
            m_columns = new GridViewColumnCollection();
        }

        public GridViewColumnCollection Columns
        {
            get { return m_columns; }
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeGridItem();
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeGridItem;
        }

        public void Traverse(Action<TreeGridItem> itemAction)
        {
            Traverse(this, itemAction);
        }

        private static void Traverse(ItemsControl control, Action<TreeGridItem> itemAction)
        {
            foreach (var item in control.Items)
            {
                var container = control.ItemContainerGenerator.ContainerFromItem(item);
                var childControl = container as ItemsControl;
                if (childControl != null)
                {
                    Traverse(childControl, itemAction);
                }

                var treeGridItem = container as TreeGridItem;
                if (treeGridItem != null &&
                    itemAction != null)
                {
                    itemAction(treeGridItem);
                }
            }
        }
    }
}