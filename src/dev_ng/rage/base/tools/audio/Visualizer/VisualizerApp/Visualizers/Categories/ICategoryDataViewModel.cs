using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Visualization.Core;

namespace VisualizerApp.Visualizers.Categories
{
    public interface ICategoryDataViewModel : IDisposable
    {
        ObservableCollection<CategoryTreeGridItemViewModel> Data { get; }

        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets);

        bool TryGetLimitInfo(string property, out CategoryDataLimitInfo info);
    }
}