namespace VisualizerApp.Visualizers.Categories
{
    public interface ILimitsParser
    {
        bool Parse();

        bool TryGetLimitInfo(string property, out CategoryDataLimitInfo info);
    }
}