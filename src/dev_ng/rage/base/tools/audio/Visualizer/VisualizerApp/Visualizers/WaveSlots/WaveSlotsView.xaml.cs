﻿using System.Collections.Generic;
using Visualization.Core;

namespace VisualizerApp.Visualizers.WaveSlots
{
    /// <summary>
    ///   Interaction logic for WaveSlotsView.xaml
    /// </summary>
    public partial class WaveSlotsView : IVisualizer
    {
        private readonly VisualizationType[] m_types = new[]
                                                           {
                                                               VisualizationType.WaveSlotLoadRequested,
                                                               VisualizationType.WaveSlotLoadCompleted,
                                                               VisualizationType.WaveSlotLoadDenied
                                                           };

        private IWaveSlotsViewModel m_viewModel;

        public WaveSlotsView()
        {
            InitializeComponent();
        }

        #region IVisualizer Members

        public string Id
        {
            get { return "Wave Slots"; }
        }

        public IEnumerable<VisualizationType> Types
        {
            get { return m_types; }
        }

        public bool Init()
        {
            m_viewModel = this.ViewModel<IWaveSlotsViewModel>();
            return m_viewModel.Init();
        }

        public void Reset()
        {
            m_viewModel.Reset();
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets, bool shouldReset)
        {
            m_viewModel.Visualize(packets);
        }

        public void Shutdown()
        {
            if (m_viewModel != null)
            {
                m_viewModel.Reset();
                m_viewModel = null;
            }
        }

        #endregion
    }
}