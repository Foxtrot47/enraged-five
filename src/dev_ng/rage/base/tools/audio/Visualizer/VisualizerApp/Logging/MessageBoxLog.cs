using System;
using rage.ToolLib;
using rage.ToolLib.Logging;

namespace VisualizerApp.Logging
{
    public class MessageBoxLog : ILog
    {
        private readonly IContextStack m_contextStack;

        public MessageBoxLog(IContextStack contextStack)
        {
            if (contextStack == null)
            {
                throw new ArgumentNullException("contextStack");
            }
            m_contextStack = contextStack;
        }

        #region ILog Members

        public LogVerbosity Verbosity { get; set; }

        public uint NumExceptionsLogged { get; private set; }

        public uint NumErrorsLogged { get; private set; }

        public uint NumWarningsLogged { get; private set; }

        public event Action<string, Exception> ExceptionLogged;

        public event Action<string, string> ErrorLogged;

        public event Action<string, string> WarningLogged;

        public event Action<string, string> InformationLogged;

        public void Exception(Exception exception)
        {
            NumExceptionsLogged++;
            var messageBox = new LogWindow(LogIcon.Exception) {DataContext = exception.ToString()};
            messageBox.ShowDialog();
            ExceptionLogged.Raise(m_contextStack.CurrentContext, exception);
        }

        public void Error(string error, params object[] args)
        {
            NumErrorsLogged++;
            var errorMsg = string.Format(error, args);
            var messageBox = new LogWindow(LogIcon.Error) {DataContext = errorMsg};
            messageBox.ShowDialog();
            ErrorLogged.Raise(m_contextStack.CurrentContext, errorMsg);
        }

        public void Warning(string warning, params object[] args)
        {
            NumWarningsLogged++;
            var warningMsg = string.Format(warning, args);
            var messageBox = new LogWindow(LogIcon.Warning) {DataContext = warningMsg};
            messageBox.ShowDialog();
            WarningLogged.Raise(m_contextStack.CurrentContext, warningMsg);
        }

        public void Information(string info, params object[] args)
        {
            if (Verbosity == LogVerbosity.Everything)
            {
                var infoMsg = string.Format(info, args);
                var messageBox = new LogWindow(LogIcon.Information) {DataContext = infoMsg};
                messageBox.ShowDialog();
                InformationLogged.Raise(m_contextStack.CurrentContext, infoMsg);
            }
        }

        public void WriteFormatted(string data, params object[] args)
        {
            var msg = string.Format(data, args);
            var messageBox = new LogWindow(LogIcon.Information) {DataContext = msg};
            messageBox.ShowDialog();
        }

        public void Write(object data)
        {
            var messageBox = new LogWindow(LogIcon.Information) {DataContext = data.ToString()};
            messageBox.ShowDialog();
        }

        public void ResetLogCounts()
        {
            NumErrorsLogged = NumWarningsLogged = NumExceptionsLogged = 0;
        }

        public void PushContext(ContextType type, string name)
        {
            m_contextStack.Push(type, name);
        }

        public ContextDescriptor PeekContext()
        {
            return m_contextStack.Peek();
        }

        public ContextDescriptor PopContext()
        {
            return m_contextStack.Pop();
        }

        public void Dispose()
        {
            // do nothing
        }

        #endregion
    }
}