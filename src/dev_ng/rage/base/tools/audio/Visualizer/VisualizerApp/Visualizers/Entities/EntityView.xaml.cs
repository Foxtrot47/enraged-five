﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using rage.ToolLib;
using Visualization.Core;
using Visualization.Entities;

namespace VisualizerApp.Visualizers.Entities
{
    /// <summary>
    ///   Interaction logic for EntityView.xaml
    /// </summary>
    public partial class EntityView : IVisualizer,
                                      INotifySelectionChanged
    {
        private readonly VisualizationType[] m_types = new[]
                                                           {VisualizationType.AddEntity, VisualizationType.RemoveEntity};

        private IEntityViewModel m_viewModel;

        public EntityView()
        {
            InitializeComponent();
        }

        #region INotifySelectionChanged Members

        public bool OutgoingDataIsBigEndian
        {
            set { m_viewModel.OutgoingDataIsBigEndian = value; }
        }

        public event Action<VisualizationType, byte[], ushort> SelectionChanged;

        #endregion

        #region IVisualizer Members

        public string Id
        {
            get { return "Entities"; }
        }

        public IEnumerable<VisualizationType> Types
        {
            get { return m_types; }
        }

        public bool Init()
        {
            m_viewModel = this.ViewModel<IEntityViewModel>();
            m_viewModel.SelectionChanged += OnSelectionChanged;
            m_viewModel.SelectItem += OnSelectItem;
            return m_viewModel.Init();
        }

        public void Reset()
        {
            m_viewModel.Reset();
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets, bool shouldReset)
        {
            m_viewModel.Visualize(packets, shouldReset);
        }

        public void Shutdown()
        {
            if (m_viewModel != null)
            {
                m_viewModel.SelectionChanged -= OnSelectionChanged;
                m_viewModel.SelectItem -= OnSelectItem;
                m_viewModel.Reset();
                m_viewModel = null;
            }
        }

        #endregion

        private void OnSelectItem(int index)
        {
            entityListView.SelectedIndex = index;
        }

        private void OnSelectionChanged(VisualizationType type, byte[] data, ushort length)
        {
            SelectionChanged.Raise(type, data, length);
        }

        private void OnEntityListViewSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (entityListView.SelectedItem != null)
            {
                var selectedEntity = entityListView.SelectedItem as EntityData;
                var selectedEntityIndex = entityListView.SelectedIndex;
                m_viewModel.Select(selectedEntity.Id, selectedEntityIndex);
            }
        }
    }
}