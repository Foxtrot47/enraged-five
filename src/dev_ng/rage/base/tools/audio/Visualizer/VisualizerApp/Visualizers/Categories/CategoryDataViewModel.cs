using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using rage.ToolLib.DataStructures;
using Visualization.Categories;
using Visualization.Core;
using VisualizerApp.TreeGrid;

namespace VisualizerApp.Visualizers.Categories
{
    public class CategoryDataViewModel : ICategoryDataViewModel
    {
        private readonly ILimitsParser m_limitsParser;
        private readonly ICategoryDataModel m_model;

        public CategoryDataViewModel(ICategoryDataModel model, ILimitsParser limitsParser)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            if (limitsParser == null)
            {
                throw new ArgumentNullException("limitsParser");
            }
            m_model = model;
            m_limitsParser = limitsParser;
            m_model.DataChanged += OnDataChanged;

            Data = new ObservableCollection<CategoryTreeGridItemViewModel>();
        }

        #region ICategoryDataViewModel Members

        public ObservableCollection<CategoryTreeGridItemViewModel> Data { get; private set; }

        public bool Init()
        {
            return m_limitsParser.Parse() && m_model.Init();
        }

        public void Reset()
        {
            m_model.Reset();
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets)
        {
            m_model.Visualize(packets);
        }

        public bool TryGetLimitInfo(string property, out CategoryDataLimitInfo info)
        {
            return m_limitsParser.TryGetLimitInfo(property, out info);
        }

        public void Dispose()
        {
            m_model.DataChanged -= OnDataChanged;
        }

        #endregion

        private void OnDataChanged(Tree<CategoryData> categoryDataTree)
        {
            if (categoryDataTree == null ||
                categoryDataTree.Root == null)
            {
                Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal, new Action(Clear));
            }
            else
            {
                if (Data.Count == 0)
                {
                    Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal,
                                                                    new Action<CategoryTreeGridItemViewModel>(Initialize),
                                                                    BuildTree(categoryDataTree.Root));
                }
                else
                {
                    UpdateTree(Data[0], categoryDataTree.Root);
                    Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal,
                                                                    new Action<TreeGridItemViewModel<CategoryData>>(Update),
                                                                    Data[0]);
                }
            }
        }

        private static TreeGridItemViewModel<CategoryData> BuildTree(TreeNode<CategoryData> modelNode)
        {
            var root = new CategoryTreeGridItemViewModel(null);
            BuildTree(root, modelNode);
            return root;
        }

        private static void BuildTree(TreeGridItemViewModel<CategoryData> viewModelNode,
                                      TreeNode<CategoryData> modelNode)
        {
            if (modelNode != null)
            {
                viewModelNode.Data = modelNode.Data;
                foreach (var childNode in modelNode.Children)
                {
                    var viewModelChildNode = new CategoryTreeGridItemViewModel(viewModelNode);
                    BuildTree(viewModelChildNode, childNode);
                    viewModelNode.Children.Add(viewModelChildNode);
                }
            }
        }

        private void Clear()
        {
            Data.Clear();
        }

        private void Initialize(CategoryTreeGridItemViewModel root)
        {
            Clear();
            if (root.Data == null)
            {
                return;
            }
            Data.Add(root);
        }

        private static void Update(TreeGridItemViewModel<CategoryData> viewModelNode)
        {
            var viewModelData = viewModelNode.Data;
            viewModelNode.Data = null;
            viewModelNode.Data = viewModelData;
            foreach (var viewModelChildNode in viewModelNode.Children)
            {
                Update(viewModelChildNode);
            }
        }

        private static void UpdateTree(TreeGridItemViewModel<CategoryData> viewModelNode,
                                       TreeNode<CategoryData> modelNode)
        {
            if (modelNode != null)
            {
                CategoryData viewModelData = viewModelNode.Data;
                CategoryData modelData = modelNode.Data;

                DebugCheckForMismatchedNodeNames(viewModelData, modelData);
                viewModelData.Volume = modelData.Volume;
                viewModelData.DistanceRollOffScale = modelData.DistanceRollOffScale;
                viewModelData.OcclusionDamping = modelData.OcclusionDamping;
                viewModelData.EnvironmentalFilterDamping = modelData.EnvironmentalFilterDamping;
                viewModelData.SourceReverbDamping = modelData.SourceReverbDamping;
                viewModelData.DistanceReverbDamping = modelData.DistanceReverbDamping;
                viewModelData.EnvironmentalLoudness = modelData.EnvironmentalLoudness;
                viewModelData.Pitch = modelData.Pitch;
                viewModelData.LpfCutoff = modelData.LpfCutoff;
                viewModelData.HpfCutoff = modelData.HpfCutoff;

                DebugCheckForMismatchedChildCount(viewModelNode, modelNode);
                for (int i = 0; i < modelNode.Children.Count; ++i)
                {
                    UpdateTree(viewModelNode.Children[i], modelNode.Children[i]);
                }
            }
        }

        private static void DebugCheckForMismatchedNodeNames(CategoryData viewModelData,
                                                             CategoryData modelData)
        {
            if (viewModelData.Name !=
                modelData.Name)
            {
                throw new ApplicationException("CategoryDataVisualizer: Error updating view - mismatched node names.");
            }
        }

        private static void DebugCheckForMismatchedChildCount(TreeGridItemViewModel<CategoryData> viewModelNode,
                                                              TreeNode<CategoryData> modelNode)
        {
            if (viewModelNode.Children.Count !=
                modelNode.Children.Count)
            {
                throw new ApplicationException(
                    "CategoryDataVisualizer: Error updating view - mismatched child node count.");
            }
        }
    }
}