﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;

namespace VisualizerApp.Timeline
{
    /// <summary>
    ///   Interaction logic for TimelineView.xaml
    /// </summary>
    public partial class TimelineView
    {
        private const int DELAY = 300;
        private readonly DispatcherTimer m_timer;
        private double? m_lastUpdateValue;
        private ITimelineViewModel m_viewModel;

        public TimelineView()
        {
            InitializeComponent();
            m_timer = new DispatcherTimer();
        }

        private void OnGoClicked(object sender, RoutedEventArgs e)
        {
            m_viewModel.SetCurrentTime(currentTime.Text);
            var value = m_viewModel.GetCurrentTimeAsT();
            var range = currentTimeSlider.Maximum - currentTimeSlider.Minimum;
            currentTimeSlider.Value = currentTimeSlider.Minimum + range * value;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            m_viewModel = this.ViewModel<ITimelineViewModel>();
            m_viewModel.StartTimeChanged += OnStartTimeChanged;
            m_viewModel.CurrentTimeChanged += OnCurrentTimeChanged;
            m_viewModel.EndTimeChanged += OnEndTimeChanged;
            m_viewModel.Reset += OnReset;
            m_timer.Interval = TimeSpan.FromMilliseconds(DELAY);
            m_timer.Tick += TimerTick;
        }

        private void OnReset()
        {
            currentTimeSlider.Value = 0;
        }

        private void TimerTick(object sender, EventArgs e)
        {
            if (m_lastUpdateValue != null)
            {
                m_viewModel.SetCurrentTimeAsT(m_lastUpdateValue.Value);
                m_lastUpdateValue = null;
            }
        }

        private void OnEndTimeChanged(string timeStr)
        {
            endTime.Text = timeStr;
        }

        private void OnCurrentTimeChanged(string timeStr)
        {
            currentTime.Text = timeStr;
        }

        private void OnStartTimeChanged(string timeStr)
        {
            startTime.Text = timeStr;
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            m_viewModel.EndTimeChanged -= OnEndTimeChanged;
            m_viewModel.CurrentTimeChanged -= OnCurrentTimeChanged;
            m_viewModel.StartTimeChanged -= OnStartTimeChanged;
            m_viewModel.Reset -= OnReset;
            m_timer.Tick -= TimerTick;
        }

        private void OnCurrentTimeKeyUp(object sender, KeyEventArgs keyEventArgs)
        {
            go.IsEnabled = m_viewModel.IsValidTime(currentTime.Text);
        }

        private void OnCurrentTimeSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            m_lastUpdateValue = (e.NewValue - currentTimeSlider.Minimum) /
                                (currentTimeSlider.Maximum - currentTimeSlider.Minimum);
            if (!m_timer.IsEnabled)
            {
                TimerTick(sender, e);
            }
        }

        private void OnCurrentTimeSliderDragCompleted(object sender, DragCompletedEventArgs e)
        {
            m_timer.Stop();
            TimerTick(sender, e);
        }

        private void OnCurrentTimeSliderDragStarted(object sender, DragStartedEventArgs e)
        {
            m_timer.Start();
        }

        private void OnCurrentTimeKeyDown(object sender, KeyEventArgs e)
        {
            if (go.IsEnabled &&
                e.Key == Key.Enter)
            {
                OnGoClicked(null, null);
            }
        }
    }
}