using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Threading;
using Visualization.Core;
using Visualization.RadioStations;
using WPFToolLib.Extensions;

namespace VisualizerApp.Visualizers.RadioStations
{
    public class RadioStationsViewModel : IRadioStationsViewModel
    {
        private readonly IRadioStationsModel m_model;

        public RadioStationsViewModel(IRadioStationsModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            m_model.DataChanged += OnDataChanged;

            Data = new BindingList<RadioStationViewData>();
        }

        #region IRadioStationsViewModel Members

        public BindingList<RadioStationViewData> Data { get; private set; }

        public int Count
        {
            get { return Data.Count; }
        }

        public bool Init()
        {
            return m_model.Init();
        }

        public void Reset()
        {
            m_model.Reset();
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets)
        {
            DebugCheckPacketValidity(packets);
            m_model.Visualize(packets.Last());
        }

        public void Dispose()
        {
            m_model.DataChanged -= OnDataChanged;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void OnDataChanged(IList<RadioStationData> data)
        {
            if (data == null)
            {
                Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal, new Action(ClearData));
            }
            else
            {
                Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal,
                                                                new Action<IList<RadioStationData>>(UpdateData),
                                                                data);
            }
        }

        private void ClearData()
        {
            Data.Clear();
            PropertyChanged.Raise(this, "Count");
        }

        private void UpdateData(IList<RadioStationData> radioStationData)
        {
            Data.RaiseListChangedEvents = false;
            Data.Clear();

            foreach (var item in radioStationData)
            {
                var playTime = item.PlayTime - TimeSpan.FromMilliseconds(item.PlayTime.Milliseconds);
                Data.Add(new RadioStationViewData {CurrentTrack = item.CurrentTrack, State = item.State, PlayTime = playTime.ToString()});
            }

            Data.RaiseListChangedEvents = true;
            Data.ResetBindings();
            PropertyChanged.Raise(this, "Count");
        }

        [Conditional("DEBUG")]
        private static void DebugCheckPacketValidity(IEnumerable<VisualizerPacket> packets)
        {
            foreach (var packet in packets)
            {
                if (packet.Type != VisualizationType.RadioStationData ||
                    packet.Size == 0)
                {
                    throw new ArgumentException(
                        string.Format("RadioStationVisualizer - Invalid packet with type: {0} and size: {1}.",
                                      packet.Type,
                                      packet.Size));
                }
            }
        }
    }
}