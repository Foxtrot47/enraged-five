using System;

namespace VisualizerApp.Visualizers.Categories
{
    public class CategoryDataLimitInfo
    {
        public CategoryDataLimitInfo(float min, float max, bool isPercentage)
        {
            if (min > max)
            {
                throw new ApplicationException("min should always be less than or equal to max");
            }

            if (isPercentage && min < 0)
            {
                throw new ApplicationException("Percentages should have min >= 0");
            }
            Min = min;
            Max = max;
            IsPercentage = isPercentage;
        }

        public float Min { get; private set; }
        public float Max { get; private set; }
        public bool IsPercentage { get; private set; }
    }
}