﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace VisualizerApp.Logging
{
    public enum LogIcon
    {
        Exception,
        Error,
        Warning,
        Information
    }

    /// <summary>
    ///   Interaction logic for LogWindow.xaml
    /// </summary>
    public partial class LogWindow
    {
        public LogWindow(LogIcon iconType)
        {
            InitializeComponent();
            Title = iconType.ToString();
            
            switch (iconType)
            {
                case LogIcon.Exception:
                case LogIcon.Error:
                    {
                        iconImage.Source = new BitmapImage(new Uri("../Images/Error.png", UriKind.Relative));
                        break;
                    }
                case LogIcon.Warning:
                    {
                        iconImage.Source = new BitmapImage(new Uri("../Images/Warning.png", UriKind.Relative));
                        break;
                    }
                case LogIcon.Information:
                    {
                        iconImage.Source = new BitmapImage(new Uri("../Images/Information.png", UriKind.Relative));
                        break;
                    }
            }
        }

        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}