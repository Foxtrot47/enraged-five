namespace VisualizerApp.TreeGrid
{
    public enum IndentDirection
    {
        Left,
        Top,
        Right,
        Bottom
    }
}