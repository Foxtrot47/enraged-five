using System;

namespace VisualizerApp.Timeline
{
    public interface ITimelineViewModel : IDisposable
    {
        event Action<string> StartTimeChanged;
        event Action<string> CurrentTimeChanged;
        event Action<string> EndTimeChanged;
        event Action Reset;

        bool IsValidTime(string text);

        void SetCurrentTime(string text);

        void SetCurrentTimeAsT(double value);

        double GetCurrentTimeAsT();
    }
}