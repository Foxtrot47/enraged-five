using System.Windows;
using System.Windows.Controls;

namespace VisualizerApp.TreeGrid
{
    public class TreeGridItem : TreeViewItem
    {
        private int m_level;

        public TreeGridItem()
        {
            m_level = -1;
        }

        public int Level
        {
            get
            {
                if (m_level == -1)
                {
                    var parent = ItemsControlFromItemContainer(this) as TreeGridItem;
                    m_level = parent == null ? 0 : parent.Level + 1;
                }
                return m_level;
            }
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeGridItem();
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeGridItem;
        }
    }
}