using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Threading;
using Visualization.Core;
using Visualization.Voices;
using WPFToolLib.Extensions;

namespace VisualizerApp.Visualizers.Voices
{
    public class PhysicalVoiceViewModel : IPhysicalVoiceViewModel
    {
        private readonly IPhysicalVoiceModel m_model;

        public PhysicalVoiceViewModel(IPhysicalVoiceModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            m_model.DataChanged += OnDataChanged;

            Data = new BindingList<PhysicalVoiceData>();
        }

        #region IPhysicalVoiceViewModel Members

        public event PropertyChangedEventHandler PropertyChanged;

        public BindingList<PhysicalVoiceData> Data { get; private set; }

        public int Count
        {
            get { return Data.Count; }
        }

        public bool Init()
        {
            return m_model.Init();
        }

        public void Reset()
        {
            m_model.Reset();
        }

        public void Visualize(IEnumerable<VisualizerPacket> packets)
        {
            DebugCheckPacketValidity(packets);
            m_model.Visualize(packets);
        }

        public void Dispose()
        {
            m_model.DataChanged -= OnDataChanged;
        }

        #endregion

        private void OnDataChanged(IList<PhysicalVoiceData> physicalVoiceData)
        {
            if (physicalVoiceData != null)
            {
                Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal,
                                                                new Action<IList<PhysicalVoiceData>>(UpdateData),
                                                                physicalVoiceData);
            }
            else
            {
                Dispatcher.FromThread(App.UiThread).BeginInvoke(DispatcherPriority.Normal, new Action(ClearData));
            }
        }

        private void ClearData()
        {
            Data.Clear();
            PropertyChanged.Raise(this, "Count");
        }

        private void UpdateData(IList<PhysicalVoiceData> physicalVoiceData)
        {
            Data.RaiseListChangedEvents = false;
            Data.Clear();

            foreach (var item in physicalVoiceData)
            {
                Data.Add(item);
            }

            Data.RaiseListChangedEvents = true;
            Data.ResetBindings();
            PropertyChanged.Raise(this, "Count");
        }

        [Conditional("DEBUG")]
        private static void DebugCheckPacketValidity(IEnumerable<VisualizerPacket> packets)
        {
            foreach (var packet in packets)
            {
                if (packet.Type != VisualizationType.PhysicalVoiceData ||
                    packet.Size == 0)
                {
                    throw new ArgumentException(
                        string.Format("VoicesVisualizer - Physical: Invalid packet with type: {0} and size: {1}.",
                                      packet.Type,
                                      packet.Size));
                }
            }
        }
    }
}