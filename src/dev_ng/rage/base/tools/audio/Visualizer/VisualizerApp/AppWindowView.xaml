﻿<Window x:Class="VisualizerApp.AppWindowView"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:Visualizers="clr-namespace:VisualizerApp.Visualizers"
        xmlns:Timeline="clr-namespace:VisualizerApp.Timeline"
        Title="Visualizer"
        Loaded="OnLoaded"
        Closing="OnClosing"
        Closed="OnClosed"
        MinWidth="640"
        MinHeight="480"
        Icon="App.ico">

	<Window.Resources>
		<BitmapImage x:Key="connectedImg"
		             UriSource="Images\Connected.png"/>
		<BitmapImage x:Key="disconnectedImg"
		             UriSource="Images\Disconnected.png"/>
		<BitmapImage x:Key="liveImg"
		             UriSource="Images\Live.png"/>
		<BitmapImage x:Key="replayImg"
		             UriSource="Images\Replay.png"/>
	</Window.Resources>

	<DockPanel VerticalAlignment="Stretch"
	           HorizontalAlignment="Stretch"
	           Background="WhiteSmoke"
	           LastChildFill="True">

		<Menu DockPanel.Dock="Top">
			<MenuItem Header="_File">
				<MenuItem Header="_Exit"
				          Click="OnExitClicked"/>
			</MenuItem>
			<MenuItem Header="_Switch To">
				<MenuItem Click="OnSwitchToLiveClicked">
					<MenuItem.HeaderTemplate>
						<DataTemplate>
							<StackPanel Orientation="Horizontal">
								<Image Width="16"
								       Height="16"
								       Margin="5,5,5,5"
								       HorizontalAlignment="Center"
								       VerticalAlignment="Center"
								       Source="{StaticResource liveImg}"/>
								<TextBlock HorizontalAlignment="Center"
								           VerticalAlignment="Center">
									Live
								</TextBlock>
							</StackPanel>
						</DataTemplate>
					</MenuItem.HeaderTemplate>
				</MenuItem>
				<MenuItem Click="OnSwitchToReplayClicked">
					<MenuItem.HeaderTemplate>
						<DataTemplate>
							<StackPanel Orientation="Horizontal">
								<Image Width="16"
								       Height="16"
								       Margin="5,5,5,5"
								       HorizontalAlignment="Center"
								       VerticalAlignment="Center"
								       Source="{StaticResource replayImg}"/>
								<TextBlock HorizontalAlignment="Center"
								           VerticalAlignment="Center">
									Replay
								</TextBlock>
							</StackPanel>
						</DataTemplate>
					</MenuItem.HeaderTemplate>
				</MenuItem>
			</MenuItem>
		</Menu>

		<StatusBar DockPanel.Dock="Bottom">
			<StatusBarItem Background="Transparent"
			               Focusable="False">
				<Image Width="32"
				       Height="32">
					<Image.Style>
						<Style>
							<Style.Triggers>
								<DataTrigger Binding="{Binding Path=IsConnected}"
								             Value="True">
									<Setter Property="Image.Source"
									        Value="{StaticResource connectedImg}"/>
									<Setter Property="Image.ToolTip"
									        Value="Connected to game"/>
								</DataTrigger>
								<DataTrigger Binding="{Binding Path=IsConnected}"
								             Value="False">
									<Setter Property="Image.Source"
									        Value="{StaticResource disconnectedImg}"/>
									<Setter Property="Image.ToolTip"
									        Value="Not connected to game"/>
								</DataTrigger>
							</Style.Triggers>
						</Style>
					</Image.Style>
				</Image>
			</StatusBarItem>
			<StatusBarItem Background="Transparent"
			               Focusable="False">
				<TextBlock x:Name="statusMessage">Unable to visualize data</TextBlock>
			</StatusBarItem>
			<StatusBarItem Background="Transparent"
			               Focusable="False"
			               HorizontalAlignment="Right">
				<StackPanel Orientation="Horizontal">
					<TextBlock x:Name="modeMessage"
					           VerticalAlignment="Center"
					           Margin="5,5,5,5"/>
					<Image Width="20"
					       Height="20"
					       Margin="5,5,5,5">
						<Image.Style>
							<Style>
								<Style.Triggers>
									<DataTrigger Binding="{Binding Path=Mode}"
									             Value="Live">
										<Setter Property="Image.Source"
										        Value="{StaticResource liveImg}"/>
										<Setter Property="Image.ToolTip"
										        Value="Live mode"/>
									</DataTrigger>
									<DataTrigger Binding="{Binding Path=Mode}"
									             Value="Replay">
										<Setter Property="Image.Source"
										        Value="{StaticResource replayImg}"/>
										<Setter Property="Image.ToolTip"
										        Value="Replay mode"/>
									</DataTrigger>
								</Style.Triggers>
							</Style>
						</Image.Style>
					</Image>
				</StackPanel>
			</StatusBarItem>
		</StatusBar>

		<Expander x:Name="timelineExpander"
		          DockPanel.Dock="Bottom"
		          Header="Replay Timeline"
		          FontSize="18"
		          FontWeight="Bold"
		          Background="LightGray"
		          ExpandDirection="Down"
		          BorderBrush="Black"
		          IsExpanded="True"
		          Margin="10,10,10,10">
			<Timeline:TimelineView/>
		</Expander>

		<Grid HorizontalAlignment="Stretch"
		      VerticalAlignment="Stretch"
		      Background="WhiteSmoke">

			<Grid.ColumnDefinitions>
				<ColumnDefinition MinWidth="130"
				                  Width="130"/>
				<ColumnDefinition/>
			</Grid.ColumnDefinitions>

			<DockPanel Grid.Column="0"
			           HorizontalAlignment="Left"
			           VerticalAlignment="Stretch"
			           LastChildFill="True">
				<GroupBox Header="Visualizers"
				          BorderThickness="1"
				          Margin="5,5,5,5">
					<ListBox x:Name="visualizerIds"
					         DockPanel.Dock="Left"
					         Padding="5,5,5,5"
					         Margin="5,5,5,5"
					         ItemsSource="{Binding Path=VisualizerIds, Mode=OneWay}"
					         SelectionChanged="OnVisualizersSelectionChanged">
					</ListBox>
				</GroupBox>
			</DockPanel>

			<GridSplitter Grid.Column="1"
			              BorderBrush="DarkGray"
			              ShowsPreview="True"
			              IsTabStop="False"
			              HorizontalAlignment="Left"
			              BorderThickness="2">
			</GridSplitter>

			<DockPanel Grid.Column="1"
			           VerticalAlignment="Stretch"
			           LastChildFill="True">
				<Grid x:Name="rootContainer"
				      Margin="15,5,5,5"
				      HorizontalAlignment="Stretch"
				      VerticalAlignment="Stretch">
					<Visualizers:DefaultView/>
				</Grid>
			</DockPanel>
		</Grid>
	</DockPanel>
</Window>