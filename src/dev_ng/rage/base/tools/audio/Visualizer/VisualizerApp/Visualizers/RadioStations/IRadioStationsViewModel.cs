using System;
using System.Collections.Generic;
using System.ComponentModel;
using Visualization.Core;

namespace VisualizerApp.Visualizers.RadioStations
{
    public interface IRadioStationsViewModel : IDisposable,
                                               INotifyPropertyChanged
    {
        BindingList<RadioStationViewData> Data { get; }

        int Count { get; }

        bool Init();

        void Reset();

        void Visualize(IEnumerable<VisualizerPacket> packets);
    }
}