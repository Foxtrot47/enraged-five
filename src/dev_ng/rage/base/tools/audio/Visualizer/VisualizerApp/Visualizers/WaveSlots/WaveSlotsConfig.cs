using System.Configuration;
using Visualization.WaveSlots;

namespace VisualizerApp.Visualizers.WaveSlots
{
    public class WaveSlotsConfig : ConfigurationSection,
                                   IWaveSlotsConfig
    {
        #region IWaveSlotsConfig Members

        public string Root { get; set; }

        #endregion
    }
}