using System;
using System.Configuration;
using Autofac;
using rage;
using rage.ToolLib.Logging;
using rage.ToolLib.Networking;
using rage.ToolLib.Networking.Synchronous;
using Visualization;
using Visualization.Categories;
using Visualization.Core;
using Visualization.Entities;
using Visualization.RadioStations;
using Visualization.Timeline;
using Visualization.Voices;
using Visualization.WaveSlots;
using VisualizerApp.Timeline;
using VisualizerApp.Visualizers.Categories;
using VisualizerApp.Visualizers.Entities;
using VisualizerApp.Visualizers.RadioStations;
using VisualizerApp.Visualizers.Voices;
using VisualizerApp.Visualizers.WaveSlots;

namespace VisualizerApp
{
    public class ContainerConfiguration
    {
        private const int PORT_NUMBER = 48001;

        public static IContainer Build(ILog log)
        {
            var builder = new ContainerBuilder();

            ConfigureApp(builder, log);
            ConfigureNetworking(builder);
            ConfigureCategoryDataVisualizer(builder);
            ConfigureEntityVisualizer(builder);
            ConfigureVoicesVisualizer(builder);
            ConfigureRadioStationVisualizer(builder);
            ConfigureWaveSlotVisualizer(builder);

            return builder.Build();
        }

        private static void ConfigureApp(ContainerBuilder builder, ILog log)
        {
            builder.RegisterInstance(log).As<ILog>().ExternallyOwned();

            builder.RegisterType<VisualizerRepository>().As<IVisualizerRepository>().SingleInstance();
            builder.RegisterType<VisualizationProcessor>().As<IVisualizationProcessor>().SingleInstance();

            builder.Register(x => (AppConfig) ConfigurationManager.GetSection("AppConfig")).SingleInstance();
            builder.Register(
                x =>
                new audProjectSettings(string.Concat(x.Resolve<AppConfig>().Root,
                                                     x.Resolve<AppConfig>().ProjectSettingsFile))).SingleInstance();

            builder.RegisterType<PacketHistory>().As<IPacketHistory>();
            builder.RegisterType<PacketHistorySerialization>().As<IPacketHistorySerialization>().SingleInstance();

            builder.RegisterType<TimelineViewModel>().As<ITimelineViewModel>().SingleInstance();
            builder.RegisterType<TimelineModel>().As<ITimelineModel>().SingleInstance();

            builder.Register(x => new AppWindowView {DataContext = x.Resolve<IAppWindowViewModel>()}).SingleInstance();
            builder.RegisterType<AppWindowViewModel>().As<IAppWindowViewModel>().SingleInstance();
            builder.RegisterType<AppWindowModel>().As<IAppWindowModel>().SingleInstance();
        }

        private static void ConfigureNetworking(ContainerBuilder builder)
        {
            builder.RegisterType<NetworkTiming>().As<INetworkTiming>().SingleInstance();
            builder.RegisterType<SynchronousClientFactory>().As<IClientFactory>().SingleInstance();
            builder.Register(x => new SynchronousTcpServer(x.Resolve<IClientFactory>(), PORT_NUMBER)).As<IServer>().
                SingleInstance();
            builder.RegisterType<Network>().As<INetwork>().SingleInstance();
        }

        private static void ConfigureCategoryDataVisualizer(ContainerBuilder builder)
        {
            builder.Register(x => new CategoryDataView {DataContext = x.Resolve<ICategoryDataViewModel>()}).As
                <IVisualizer>().SingleInstance();
            builder.RegisterType<CategoryDataViewModel>().As<ICategoryDataViewModel>().SingleInstance();
            builder.RegisterType<CategoryDataModel>().As<ICategoryDataModel>().SingleInstance();
            builder.RegisterType<LimitsParser>().As<ILimitsParser>().SingleInstance();
            builder.Register(x =>
                                 {
                                     var catDataConfig =
                                         (CategoryDataConfig) ConfigurationManager.GetSection("CategoryDataConfig");
                                     catDataConfig.Root = x.Resolve<AppConfig>().Root;
                                     return catDataConfig;
                                 }).As<ICategoryDataConfig>().SingleInstance();
        }

        private static void ConfigureEntityVisualizer(ContainerBuilder builder)
        {
            builder.Register(x => new EntityView {DataContext = x.Resolve<IEntityViewModel>()}).As<IVisualizer>().
                SingleInstance();
            builder.RegisterType<EntityViewModel>().As<IEntityViewModel>().SingleInstance();
            builder.RegisterType<EntityModel>().As<IEntityModel>().SingleInstance();
            builder.Register(x =>
                                 {
                                     var entityConfig = (EntityConfig) ConfigurationManager.GetSection("EntityConfig");
                                     entityConfig.Root = x.Resolve<AppConfig>().Root;
                                     return entityConfig;
                                 }).As<IEntityConfig>().SingleInstance();
        }

        private static void ConfigureVoicesVisualizer(ContainerBuilder builder)
        {
            builder.Register(x => new VoicesView {DataContext = x.Resolve<IVoicesViewModel>()}).As<IVisualizer>().
                SingleInstance();
            builder.RegisterType<VoicesViewModel>().As<IVoicesViewModel>().SingleInstance();
            builder.RegisterType<VirtualVoiceViewModel>().As<IVirtualVoiceViewModel>().SingleInstance();
            builder.RegisterType<PhysicalVoiceViewModel>().As<IPhysicalVoiceViewModel>().SingleInstance();
            builder.RegisterType<VoiceStartModel>().As<IVoiceStartModel>().SingleInstance();
            builder.RegisterType<VirtualVoiceModel>().As<IVirtualVoiceModel>().SingleInstance();
            builder.RegisterType<PhysicalVoiceModel>().As<IPhysicalVoiceModel>().SingleInstance();
        }

        private static void ConfigureRadioStationVisualizer(ContainerBuilder builder)
        {
            builder.Register(x => new RadioStationsView {DataContext = x.Resolve<IRadioStationsViewModel>()}).As
                <IVisualizer>().SingleInstance();
            builder.RegisterType<RadioStationsViewModel>().As<IRadioStationsViewModel>().SingleInstance();
            builder.RegisterType<RadioStationsModel>().As<IRadioStationsModel>().SingleInstance();
            builder.Register(x =>
                                 {
                                     var radioStationsConfig =
                                         (RadioStationsConfig) ConfigurationManager.GetSection("RadioStationsConfig");
                                     radioStationsConfig.Root = x.Resolve<AppConfig>().Root;
                                     return radioStationsConfig;
                                 }).As<IRadioStationsConfig>().SingleInstance();
        }

        private static void ConfigureWaveSlotVisualizer(ContainerBuilder builder)
        {
            builder.Register(x => new WaveSlotsView {DataContext = x.Resolve<IWaveSlotsViewModel>()}).As<IVisualizer>().
                SingleInstance();
            builder.RegisterType<WaveSlotsViewModel>().As<IWaveSlotsViewModel>().SingleInstance();
            builder.RegisterType<WaveSlotsModel>().As<IWaveSlotsModel>().SingleInstance();
            builder.Register(x =>
                                 {
                                     var waveSlotsConfig =
                                         (WaveSlotsConfig) ConfigurationManager.GetSection("WaveSlotsConfig");
                                     waveSlotsConfig.Root = x.Resolve<AppConfig>().Root;
                                     return waveSlotsConfig;
                                 }).As<IWaveSlotsConfig>().SingleInstance();
        }
    }
}