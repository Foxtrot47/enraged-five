using System.Configuration;
using Visualization.Entities;

namespace VisualizerApp.Visualizers.Entities
{
    public class EntityConfig : ConfigurationSection,
                                IEntityConfig
    {
        private const string ENTITY_TYPES_FILE = "entityTypesFile";

        #region IEntityConfig Members

        public string Root { get; set; }

        [ConfigurationProperty(ENTITY_TYPES_FILE, IsRequired = true, IsKey = false)]
        public string EntityTypesFile
        {
            get { return (string) this[ENTITY_TYPES_FILE]; }
        }

        #endregion
    }
}