using System;
using System.Collections.Generic;
using System.Text;

namespace audPlaceholderSpeechGenerator
{
    public abstract class SpeechObject
    {
        private string m_Voice;
        private string m_FileName;
        private string m_FilePath;

        protected SpeechObject(string voice, string fileName)
        {
            m_Voice = voice.Replace(" ", "_");
            m_FileName = string.Concat(fileName);
        }

        public string Voice
        {
            get { return m_Voice; }
        }

        public abstract string Episode
        {
            get;
        }

        public string FileName
        {
            get { return m_FileName; }
        }

        public string FilePath
        {
            get
            {
                if (string.IsNullOrEmpty(m_FilePath))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(Episode);
                    sb.Append("\\");
                    sb.Append(m_Voice);
                    sb.Append("\\");
                    sb.Append(m_FileName);
                    m_FilePath = sb.ToString();
                }

                return m_FilePath;
            }
        }
    }
}
