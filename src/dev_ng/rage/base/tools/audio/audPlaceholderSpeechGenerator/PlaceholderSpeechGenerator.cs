using System;
using System.Collections.Generic;
using System.Text;
using audAssetManagement2;
using System.Xml;
using Wavelib;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using System.Diagnostics;
using rage;
using Microsoft.Office.Interop.Excel;

namespace audPlaceholderSpeechGenerator
{
    public class PlaceholderSpeechGenerator
    {
        private assetManager m_AssetManager;
        private audProjectSettings m_ProjectSettings;
        private changelist m_Changelist;

        private string m_ExcelDoc;
        private string m_VoiceXml;
        private string m_XSLPathPlaceholder;
        private string m_XSLPathLive;
        private string m_XPath;
        private string m_SoxPath;

        private string m_PlaceholderOutputPath;      
        private string m_UnrecordedLine;
        private string m_LinesNotInExcel;
        private string m_IncorrectNameTag;

        private Dictionary<string, string> m_Voices;
        private Dictionary<string, XmlDocument> m_BuiltWaves = new Dictionary<string, XmlDocument>();
        private Dictionary<string, XmlDocument> m_PendingWaves = new Dictionary<string, XmlDocument>();

        public PlaceholderSpeechGenerator(assetManager assetManager, 
                                          audProjectSettings projectSettings,
                                          changelist changelist,
                                          string excelDoc,
                                          string voiceXML,                                         
                                          string XSLPathPlaceholder,
                                          string XSLPathLive, 
                                          string XPath, 
                                          string soxPath,
                                          string placeholderOutputPath,
                                          string reportOutput)
        {
            m_AssetManager = assetManager;
            m_ProjectSettings = projectSettings;
            m_Changelist = changelist;
            m_ExcelDoc = excelDoc;
            m_VoiceXml = voiceXML;
            m_XSLPathPlaceholder = XSLPathPlaceholder;
            m_XSLPathLive = XSLPathLive;
            m_XPath = XPath;
            m_SoxPath = soxPath;
            m_PlaceholderOutputPath = placeholderOutputPath;            
           
            m_Voices = new Dictionary<string, string>();
            m_UnrecordedLine = String.Concat(reportOutput, "Scripted_Speech_Unrecorded_Lines.txt");
            m_LinesNotInExcel = String.Concat(reportOutput, "Scripted_Speech_Not_In_Excel.txt");
            m_IncorrectNameTag = String.Concat(reportOutput, "Scripted_Speech_Incorrect_Voice.txt");
        }


        public void Run()
        {
            CheckoutAssets();
            ParseVoices();
            
            SpeechObjectExcel.ComputeExcelSpeechObjects(m_ExcelDoc);
            SpeechObjectRave.ComputeRaveSpeechObjects(m_ProjectSettings, 
                                                      m_PendingWaves,
                                                      m_BuiltWaves, 
                                                      m_XSLPathPlaceholder, 
                                                      m_XSLPathLive);
            
            GenerateVoices();
            UpdateXML();
            WriteReports();
        }

        private void CheckoutAssets()
        {
            EventLog.WriteEntry("Paceholder Builder", "Checking out Wave Lists");
            foreach (PlatformSetting ps in m_ProjectSettings.GetPlatformSettings())
            {
                string outputPath = m_AssetManager.GetWorkingPath(ps.buildInfo);
                string builtwaves = String.Concat(outputPath, "BuiltWaves.Xml");
                string pendingwaves = String.Concat(outputPath, "PendingWaves.Xml");

                m_AssetManager.GetLatest(pendingwaves, false);
                m_AssetManager.GetLatest(builtwaves, false);
                
                m_Changelist.CheckoutAsset(pendingwaves, true);

                XmlDocument pendingDoc = new XmlDocument();
                pendingDoc.Load(pendingwaves);
                m_PendingWaves.Add(ps.PlatformTag, pendingDoc);

                XmlDocument builtDoc = new XmlDocument();
                builtDoc.Load(builtwaves);
                m_BuiltWaves.Add(ps.PlatformTag, builtDoc);
            }

            m_AssetManager.GetLatest(m_ExcelDoc, false);
            m_Changelist.CheckoutAsset(m_LinesNotInExcel, true);
            m_Changelist.CheckoutAsset(m_UnrecordedLine, true);
            m_Changelist.CheckoutAsset(m_IncorrectNameTag, true);
        }

        private void ParseVoices()
        {
            EventLog.WriteEntry("Placeholder Speech Builder", "Parsing Voice XML");
            XmlDocument doc = new XmlDocument();
            doc.Load(m_VoiceXml);
            foreach (XmlNode voice in doc.DocumentElement.ChildNodes)
            {
                m_Voices.Add(voice.Attributes["name"].Value, voice.Attributes["id"].Value);
            }
        }
        
        public void GenerateVoices()
        {
            string tempPath = Path.GetTempPath() + "Placeholder";
            Directory.CreateDirectory(tempPath);

            bool found = false;
            int current = 0;
            foreach (SpeechObjectExcel excelObject in SpeechObjectExcel.ExcelSpeechObjects)
            {
                current++;
                found = false;
                foreach (SpeechObjectRave raveObject in SpeechObjectRave.RaveSpeechObjects)
                {
                    if (String.Compare(excelObject.FileName, raveObject.FileName, true) == 0)
                    {
                        found = true;
                    }
                }

                if (!found)//create wave
                {
                    string tempPlaceholderPath = tempPath + "\\" + excelObject.FilePath;

                    string placeholderFilePath = string.Concat(m_PlaceholderOutputPath, "\\", excelObject.FilePath);
                    string dirPath = Path.GetDirectoryName(placeholderFilePath);
                    string tempDir = Path.GetDirectoryName(tempPlaceholderPath);
                    
                    Directory.CreateDirectory(dirPath);
                    Directory.CreateDirectory(tempDir);
                    

                    //doesn't exist in placeholder 
                    if (!m_AssetManager.ExistsAsAsset(placeholderFilePath))
                    {
                        EventLog.WriteEntry("Placeholder Speech Builder", String.Concat("Generating voice ", placeholderFilePath));
                        if (File.Exists(placeholderFilePath))
                        {
                            //ensure files is not readonly, causes generator to fail
                            File.SetAttributes(placeholderFilePath, FileAttributes.Normal);
                        }

                        Console.Out.WriteLine(String.Format("Processing {0} of {1}", current, SpeechObjectExcel.ExcelSpeechObjects.Count));
                        ProcessStartInfo psi = new ProcessStartInfo("ttscmd");
                        StringBuilder args = new StringBuilder();
                        args.Append("/ttw ");
                        args.Append("\"");
                        args.Append(excelObject.Text);
                        args.Append("\" ");
                        args.Append("\"");
                        args.Append(tempPlaceholderPath);
                        args.Append("\"");
                        args.Append(" -e ");
                        if (m_Voices.ContainsKey(excelObject.Voice))
                        {
                            args.Append(m_Voices[excelObject.Voice]);
                        }
                        else
                        {
                            args.Append(m_Voices["DEFAULT"]);
                        }
                        psi.Arguments = args.ToString();
                        psi.UseShellExecute = false;
                        psi.RedirectStandardOutput = false;
                        psi.RedirectStandardError = true;
                        Process p = new Process();
                        p.StartInfo = psi;
                        p.Start();
                        string er = p.StandardError.ReadToEnd();
                        p.WaitForExit();
                        if (p.ExitCode != 0)
                        {
                            throw new Exception(String.Concat("Failed to generate placeholder file for ", placeholderFilePath));
                        }

                        Trim(tempPlaceholderPath, placeholderFilePath);
                        RMSNormalize(placeholderFilePath);
                        m_Changelist.MarkAssetForAdd(placeholderFilePath);
                    }
                }
            }

            Directory.Delete(tempPath, true);
        }

        private void Trim(string tempPath, string path)
        {
            ProcessStartInfo psi = new ProcessStartInfo(m_SoxPath);
            StringBuilder args = new StringBuilder();
            args.Append("\"");
            args.Append(tempPath);
            args.Append("\"");
            args.Append(" ");
            args.Append(path);
            args.Append(" trim 0 6");

            psi.Arguments = args.ToString();
            psi.UseShellExecute = false;
            psi.RedirectStandardOutput = false;
            psi.RedirectStandardError = true;
            Process p = new Process();
            p.StartInfo = psi;
            p.Start();
            string er = p.StandardError.ReadToEnd();
            p.WaitForExit();
            if (p.ExitCode != 0)
            {
                throw new Exception(String.Concat("Failed to trim file ", path));
            }
        }

        private void RMSNormalize(string path)
        {
            bwWaveFile wave = new bwWaveFile(path, false);

            int dataSize = wave.Data.RawData.Length;
            double sum = 0;

            for (int i = 0; i < dataSize; i += 2)
            {
                short currentSample = BitConverter.ToInt16(wave.Data.RawData, i);
                sum += Math.Pow((double)currentSample, 2.0);
            }

            short rms = (short)Math.Floor(Math.Sqrt(sum / (dataSize / 2)));

            //Calculate scaling factor
            short desiredRMSSample = (short)Math.Floor((double)0x7FFF * Math.Pow(10.0, -20.0 / 20.0));
            double scalingFactor = (double)desiredRMSSample / (double)rms;

            //Scale audio data
            for (int i = 0; i < dataSize; i += 2)
            {
                short currentSample = BitConverter.ToInt16(wave.Data.RawData, i);
                short adjustedSample = (short)Math.Floor((double)currentSample * scalingFactor);

                byte[] temp = BitConverter.GetBytes(adjustedSample);
                wave.Data.RawData[i] = temp[0];
                wave.Data.RawData[i + 1] = temp[1];
            }

            wave.Save();
        }

        private void UpdateXML()
        {
            EventLog.WriteEntry("Placeholder Speech Builder", "Updating XML");

            List<SpeechObjectExcel> toAdd = new List<SpeechObjectExcel>();
            List<SpeechObjectRave> toRemove = new List<SpeechObjectRave>();

            foreach (SpeechObjectExcel excelSpeechObject in SpeechObjectExcel.ExcelSpeechObjects)
            {
                bool found = false;

                foreach (SpeechObjectRave raveSpeechObject in SpeechObjectRave.RaveSpeechObjects)
                {
                    if (String.Compare(raveSpeechObject.FileName, excelSpeechObject.FileName,true)==0)
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    toAdd.Add(excelSpeechObject);
                }
            }

            foreach (SpeechObjectRave raveSpeechObject in SpeechObjectRave.RaveSpeechObjects)
            {
                bool found = false;
                foreach (SpeechObjectExcel excelSpeechObject in SpeechObjectExcel.ExcelSpeechObjects)
                {
                    if (String.Compare(raveSpeechObject.FileName, excelSpeechObject.FileName,true)==0)
                    {
                        found = true;
                    }
                }

                if (!found && raveSpeechObject.FilePath.ToUpper().Contains("PLACEHOLDER"))
                {
                    toRemove.Add(raveSpeechObject);
                }
            }

            if (toAdd.Count>0 || toRemove.Count>0)
            {
                foreach (PlatformSetting ps in m_ProjectSettings.GetPlatformSettings())
                {
                    string outputPath = m_AssetManager.GetWorkingPath(ps.buildInfo);

                    XmlNode pendingSpeechPack = m_PendingWaves[ps.PlatformTag].DocumentElement.SelectSingleNode(m_XPath);
                    XmlNode builtSpeechPack = m_BuiltWaves[ps.PlatformTag].DocumentElement.SelectSingleNode(m_XPath);

                    if (pendingSpeechPack == null)
                    {
                        if (builtSpeechPack == null)
                        {
                            throw new Exception("Could not find speech pack");
                        }
                        else
                        {
                            pendingSpeechPack = m_PendingWaves[ps.PlatformTag].ImportNode(builtSpeechPack, false);
                            m_PendingWaves[ps.PlatformTag].DocumentElement.AppendChild(pendingSpeechPack);
                        }
                    }

                    XmlNode pPlaceHolderSpeech = pendingSpeechPack.SelectSingleNode("child::BankFolder[@name='PLACEHOLDER_SPEECH']");
                    XmlNode bPlaceHolderSpeech = builtSpeechPack == null? null:builtSpeechPack.SelectSingleNode("child::BankFolder[@name='PLACEHOLDER_SPEECH']");

                    if (pPlaceHolderSpeech == null)
                    {
                        if (bPlaceHolderSpeech == null)
                        {
                            throw new Exception("Could not find placeholder folder");
                        }
                        else
                        {
                            pPlaceHolderSpeech = m_PendingWaves[ps.PlatformTag].ImportNode(bPlaceHolderSpeech, false);
                            pendingSpeechPack.AppendChild(pPlaceHolderSpeech);
                        }
                    }

                    foreach (SpeechObject so in toAdd)
                    {
                        AddSpeechObject(m_PendingWaves[ps.PlatformTag], pPlaceHolderSpeech, bPlaceHolderSpeech,so, "add");
                    }

                    foreach (SpeechObject so in toRemove)
                    {
                        AddSpeechObject(m_PendingWaves[ps.PlatformTag], pPlaceHolderSpeech, bPlaceHolderSpeech, so, "remove");
                    }

                    m_PendingWaves[ps.PlatformTag].Save(String.Concat(outputPath, "PendingWaves.Xml"));
                }
            }
        }

        private void AddSpeechObject(XmlDocument pendingWaves,                                     
                                     XmlNode pendingPlaceholderFolder, 
                                     XmlNode builtPlaceholderFolder,
                                     SpeechObject speechObject, 
                                     string action)
        {
            string bankPath = String.Format("child::Bank[@name='{0}']", speechObject.Episode);
            XmlNode pendingBank = pendingPlaceholderFolder.SelectSingleNode(bankPath);
            XmlNode builtBank = builtPlaceholderFolder == null? null: builtPlaceholderFolder.SelectSingleNode(bankPath);

            //Create Bank
            if (pendingBank == null)
            {
                if (builtBank == null)
                {
                    XmlElement el = pendingWaves.CreateElement("Bank");
                    el.SetAttribute("name", speechObject.Episode);
                    el.SetAttribute("operation", "add");
                    pendingBank = el;
                }
                else
                {
                    pendingBank = pendingWaves.ImportNode(builtBank, false);
                }

                pendingPlaceholderFolder.AppendChild(pendingBank);
            }

            //Create Wave Folder
            string waveFolderPath = String.Format("child::WaveFolder[@name='{0}']", speechObject.Voice);
            XmlNode pWaveFolder = pendingBank.SelectSingleNode(waveFolderPath);
            XmlNode bWaveFolder = null;

            if (pWaveFolder == null)
            {
                if (builtBank == null || builtBank.SelectSingleNode(waveFolderPath) == null)
                {
                    XmlElement el = pendingWaves.CreateElement("WaveFolder");
                    el.SetAttribute("name", speechObject.Voice);
                    el.SetAttribute("operation", "add");

                    XmlElement tag = pendingWaves.CreateElement("Tag");
                    tag.SetAttribute("name", "voice");
                    tag.SetAttribute("value", string.Concat(speechObject.Voice, "_PLACEHOLDER"));
                    tag.SetAttribute("operation", "add");

                    el.AppendChild(tag);

                    pWaveFolder = el;
                }
                else
                {
                    bWaveFolder = builtBank.SelectSingleNode(waveFolderPath);
                    pWaveFolder = pendingWaves.ImportNode(bWaveFolder, false);
                }

                pendingBank.AppendChild(pWaveFolder);
            }
           
            XmlElement waveEl = pendingWaves.CreateElement("Wave");
            waveEl.SetAttribute("operation", action == "add"? "add":"remove");
            waveEl.SetAttribute("name", speechObject.FileName);
            waveEl.SetAttribute("builtName", speechObject.FileName);

            pWaveFolder.AppendChild(waveEl);
        }

        private void WriteReports()
        {
           string platform = m_ProjectSettings.GetCurrentPlatform().PlatformTag;

            Dictionary<string,List<SpeechObjectExcel>> missing = new Dictionary<string,List<SpeechObjectExcel>>();
            List<string> csvWaves = new List<string>();

            foreach (SpeechObjectExcel excelObject in SpeechObjectExcel.ExcelSpeechObjects)
            {
                bool found = false;

                foreach (SpeechObjectRave raveObject in SpeechObjectRave.RaveSpeechObjects)
                {
                    if (String.Compare(excelObject.FileName, raveObject.FileName, true) == 0
                        && String.Compare(excelObject.FilePath, raveObject.FilePath,true) != 0)
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    if (!missing.ContainsKey(excelObject.Voice))
                    {
                        missing.Add(excelObject.Voice, new List<SpeechObjectExcel>());
                    }

                    missing[excelObject.Voice].Add(excelObject);
                }                
            }

            if (missing.Count > 0)
            {
                FileStream fs = new FileStream(m_UnrecordedLine, FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);

                foreach (KeyValuePair<string, List<SpeechObjectExcel>> kvp in missing)
                {
                    sw.WriteLine(kvp.Key);
                    sw.WriteLine("---------------------------------------------");
                    foreach (SpeechObjectExcel so in kvp.Value)
                    {
                        sw.Write(so.FileName);
                        sw.Write(" ");
                        sw.WriteLine(so.Text);
                    }
                    sw.WriteLine();
                }

                
                sw.Close();
                fs.Close();
            }

        }
        
    }
}
