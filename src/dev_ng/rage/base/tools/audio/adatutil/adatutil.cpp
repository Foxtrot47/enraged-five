// 
// adatutil/adatutil.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#if __TOOL

#include "audiodata/container.h"
#include "audiodata/containerbuilder.h"
#include "audiohardware/mp3util.h"
#include "audiohardware/waveref.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/stream.h"
#include "math/amath.h"
#include "string/stringhash.h"
#include "system/param.h"
#include "system/platform.h"
#include "system/timer.h"

using namespace rage;

PARAM(platform, "The target platform");
PARAM(show, "Show the contents of the container");
PARAM(file, "Filename of the container to operate on");
PARAM(create, "Create an empty container/object/chunk");
PARAM(object, "Specify object name");
PARAM(objecthash, "Specify object name hash");
PARAM(objectid, "Specify object id");
PARAM(chunk, "Specify chunk name");
PARAM(extract, "Extract the specified chunk");
PARAM(decodeformat, "Decode and display any format chunks");
PARAM(testcontiguous, "Scan specified directory and check all containers are packed for contiguous loading");
PARAM(computepadding, "Compute total MP3 frame padding");

PARAM(keyplatform, "The platform to use to select decryption key");

void FileScanCallback(const fiFindData &data, void *userArg)
{	
	if(strstr(data.m_Name, ".awc"))
	{
		char path[RAGE_MAX_PATH];
		const char *inputPath = (const char*)userArg;
		formatf(path, "%s\\%s", inputPath, data.m_Name);

		u8 *buf = rage_aligned_new(16) u8[32*1024];

		fiStream *stream = ASSET.Open(path, NULL);
		if(!stream)
		{
			Quitf("Failed to open file %s", path);
		}
		
		stream->Read(buf, Min(32*1024,stream->Size()));
		stream->Close();

		adatContainer container((adatContainerHeader*)buf);

		Assert(container.IsValid());
		Assert(container.GetHeaderSize() <= 32*1024);

		Displayf("%s - %d objects", data.m_Name, container.GetNumObjects());
		//Displayf("Header reports %s", container.GetPackingType() == adatContainer::OPTIMISED_PACKING ? "non-contiguous packing" : "contiguous packing");

		for(adatObjectId waveId = 0; waveId < (adatObjectId)container.GetNumObjects(); waveId++)
		{		
			// issue the data load request for all of this wave's chunks
			const u32 numWaveChunks = container.GetNumDataChunksForObject(waveId);
			Assertf(numWaveChunks > 0, "Wave with no data: %s/%u", data.m_Name, waveId);

			// all chunks are stored next to each other so calculate the total size and starting offset
			u32 sumOfChunkDataSize = 0;
			u32 minOffset = ~0U;
			u32 maxOffset = 0;
			u32 sizeOfChunkAtMaxOffset = 0;
			for(u32 i = 0; i < numWaveChunks; i++)
			{
				u32 chunkHash,offsetBytes,sizeBytes;
				container.GetObjectDataChunk(waveId, (adatObjectDataChunkId)i, chunkHash, offsetBytes, sizeBytes);
				sumOfChunkDataSize += sizeBytes;
				Assert(offsetBytes != minOffset);
				Assert(offsetBytes != maxOffset);
				if(offsetBytes < minOffset)
				{
					minOffset = offsetBytes;
				}
				if(offsetBytes > maxOffset)
				{
					maxOffset = offsetBytes;
					sizeOfChunkAtMaxOffset = sizeBytes;
				}
			}

			const u32 totalBytesToLoad = (maxOffset+sizeOfChunkAtMaxOffset) - minOffset;

			// check that the chunks are in fact contiguous, allowing for alignment
			if(sumOfChunkDataSize >= totalBytesToLoad-16 && sumOfChunkDataSize <= totalBytesToLoad+16)
			{
				//Displayf("%d is contiguous", waveId);
			}
			else
			{
				Errorf("%d is not contiguous", waveId);
			}
		}

		delete[] buf;
	}
}

void RunContiguousScan(const char *path)
{
	ASSET.EnumFiles(path, FileScanCallback, (void*)path);
}

const char *GetChunkTypeName(const u32 typeNameHash)
{
#define CHUNK_TYPE_MAP(x) if(typeNameHash == (atStringHash(x) & adatContainerObjectDataTableEntry::kDataTypeHashMask)) return x
	
	CHUNK_TYPE_MAP("DATA");
	CHUNK_TYPE_MAP("VISEME");
	CHUNK_TYPE_MAP("FORMAT");
	CHUNK_TYPE_MAP("SEEKTABLE");
	CHUNK_TYPE_MAP("STREAMFORMAT");
	CHUNK_TYPE_MAP("MARKERS");
	CHUNK_TYPE_MAP("PEAK");
	CHUNK_TYPE_MAP("LIPSYNC");
	CHUNK_TYPE_MAP("CUSTOM_LIPSYNC");

	return "unknown";
}

u32 ComputeMP3Padding(const adatContainer &container)
{
	u32 numObjects = container.GetNumObjects();
	u32 paddingBytes = 0;
	for(adatObjectId objectId = 0; objectId < (adatObjectId)numObjects; objectId++)
	{
		u32 offsetBytes = 0;
		u32 sizeBytes = 0;
		u32 typeHash;
		adatObjectDataChunkId chunkId = container.FindObjectData(objectId, "FORMAT");
		if(chunkId == adatContainer::InvalidId)
		{
			continue;
		}
		container.GetObjectDataChunk(objectId, chunkId, typeHash, offsetBytes, sizeBytes);
		const audWaveFormat *waveFormat = (const audWaveFormat*)(container.GetDataPointer() + offsetBytes);

		if(waveFormat->Format != audWaveFormat::kMP3)
		{
			continue;
		}

		chunkId = container.FindObjectData(objectId, "DATA");
		if(chunkId == adatContainer::InvalidId)
		{
			continue;
		}

		container.GetObjectDataChunk(objectId, chunkId, typeHash, offsetBytes, sizeBytes);
		
		const u8 *const mp3Data = container.GetDataPointer() + offsetBytes;
		u32 byteOffset = 0;
				
		while(byteOffset < sizeBytes)
		{
			const u32 frameSizeBytes = rage::audMp3Util::ComputeSizeOfFrame(mp3Data + byteOffset);
			const u8 *paddingPtr = mp3Data + byteOffset + frameSizeBytes - 1;
			while(*paddingPtr)
			{
				paddingBytes++;
				paddingPtr--;
			}
			byteOffset += frameSizeBytes;
		}

		paddingBytes += 2; // 2 bytes saved from frame sync
	}

	return paddingBytes;
}
#pragma warning(disable: 4748)

int main(int argc,char **argv)
{
	sysParam::Init(argc,argv);

	// Get target platform (default to ps3 to avoid breaking existing scripts)
	const char *platform = "ps3";
	PARAM_platform.Get(platform);
	g_sysPlatform = sysGetPlatform(platform);
	
	const bool isBigEndian = (g_sysPlatform == platform::XENON || g_sysPlatform == platform::PS3);

	if(PARAM_testcontiguous.Get())
	{
		const char *path;
		PARAM_testcontiguous.Get(path);
		RunContiguousScan(path);
		return 0;
	}

	
	adatContainerBuilder containerBuilder(adatContainer::OPTIMISED_PACKING);

	const char *fileName = "testcontainer";
	PARAM_file.Get(fileName);

	if(PARAM_create.Get() && !ASSET.Exists(fileName,NULL))
	{
		// create an empty container
		Displayf("Creating container %s", fileName);
		fiStream *stream = ASSET.Create(fileName, NULL);
		Assert(stream);
		containerBuilder.Serialize(stream, isBigEndian);
		stream->Close();
	}
	//else 
	{
		fiStream *stream = ASSET.Open(fileName, NULL);
		if(!stream)
		{
			Quitf("Failed to open file %s", fileName);
		}
		const u32 containerSizeBytes = stream->Size();
		u8 *buf = rage_new u8[stream->Size()];
		stream->Read(buf, stream->Size());
		stream->Close();

		if(!adatContainer::IsValidHeader(buf))
		{
			// try decrypting
			const u32 *key = NULL;

			static const u32 s_PS4Key[] = AUD_WAVESLOT_KEY_PS4;
			static const u32 s_XBONEKey[] = AUD_WAVESLOT_KEY_XBONE;
			static const u32 s_PCKey[] = AUD_WAVESLOT_KEY_PC;

			const char *keyPlatform = "";
			PARAM_keyplatform.Get(keyPlatform);
			if(!stricmp(keyPlatform, "PS4"))
			{
				key = &s_PS4Key[0];				
			}
			else if(!stricmp(keyPlatform, "XBONE"))
			{
				key = &s_XBONEKey[0];
			}
			else
			{
				key = &s_PCKey[0];
			}
			audDisplayf("Attempting decryption...");
			btea((u32*)buf, -int(containerSizeBytes>>2), key);
		}

		adatContainer container((adatContainerHeader*)buf);
		Assert(container.IsValid());

		if(PARAM_show.Get())
		{		
			static const u32 formatTypeHash = (atStringHash("format") & adatContainerObjectDataTableEntry::kDataTypeHashMask);

			Displayf("%u objects", container.GetNumObjects());
			Displayf("Header size: %u bytes", container.GetHeaderSize());
			Displayf("N%sative endian", container.IsNativeEndian() ? "" : "on-n");
			Displayf("%s fast lookup table", container.HasFastLookupTable() ? "Has" : "No");

			const char *packingTypes[] = {
				"Padding-Optimised (alignment sorted: bank-loaded)",
				"Object-Contiguous (wave loaded banks)",
				"Stream"
			};
			Displayf("%s packing", packingTypes[container.GetPackingType()]);

			if(PARAM_objecthash.Get() || PARAM_object.Get() || PARAM_objectid.Get())
			{
				const char *name = "";
				adatObjectId objectId;
				u32 nameHashRequested = 0;
				if(PARAM_object.Get())
				{
					
					PARAM_object.Get(name);
					objectId = container.FindObject(name);
					if(objectId == adatContainer::InvalidId)
					{
						Errorf("Failed to find object with name \'%s\'", name);
						return -1;
					}
				}
				else if(PARAM_objecthash.Get(nameHashRequested))
				{
					objectId = container.FindObject(nameHashRequested);
					if(objectId == adatContainer::InvalidId)
					{
						Errorf("Failed to find object with name hash \'%u\'", nameHashRequested);
						return -1;
					}
				}
				else
				{
					s32 objectIdInt;
					PARAM_objectid.Get(objectIdInt);
					objectId = (adatObjectId)objectIdInt;
				}
				
			
				const u32 numChunks = container.GetNumDataChunksForObject(objectId);
				Displayf("Object %d (%s:%u) %u chunks", objectId, name, container.GetObjectNameHash(objectId), numChunks);
				Displayf("Contiguous size %u", container.ComputeContiguousObjectSize(objectId));
				for(adatObjectDataChunkId chunkId = 0; chunkId < (adatObjectDataChunkId)numChunks; chunkId++)
				{
					u32 typeHash, offsetBytes, sizeBytes;
					container.GetObjectDataChunk(objectId, chunkId, typeHash, offsetBytes, sizeBytes);
					Displayf("Chunk %d type: %s (%02X) offset: %u size: %u first dword: %08X", chunkId, GetChunkTypeName(typeHash), typeHash, offsetBytes, sizeBytes, *(u32*)(container.GetDataPointer() + offsetBytes));

					
					if(typeHash == formatTypeHash && sizeBytes == sizeof(audWaveFormat))
					{
						audWaveFormat *waveFormat = (audWaveFormat*)((u8*)container.GetDataPointer() + offsetBytes);
						
						if(waveFormat->Format >= audWaveFormat::kNumFormats)
						{
							Errorf("Invalid wave format: %u", waveFormat->Format);
						}

						Displayf("Format chunk:");
						const char *formatTable[] = {
							"16bit PCM (LE)",
							"16bit PCM (BE)",
							"32bit IEEE Float (LE)",
							"32bit IEEE Float (BE)",
							"ADPCM",
							"XMA2",
							"xWMA",
							"MP3",
							"Ogg Vorbis",
							"AAC",
							"WMA"};
						
						CompileTimeAssert(sizeof(waveFormat->Format) == 1);
						Displayf("Format: %s (%u)", formatTable[waveFormat->Format], waveFormat->Format);

						if(container.IsNativeEndian())
						{
							Displayf("LengthSamples: %u", waveFormat->LengthSamples);
							Displayf("LoopPointSamples: %d", waveFormat->LoopPointSamples);
							Displayf("Sample rate: %u", waveFormat->SampleRate);
							Displayf("Headroom: %d", waveFormat->Headroom);
							Displayf("LoopBegin: %u", waveFormat->LoopBegin);
							Displayf("LoopEnd: %u", waveFormat->LoopEnd);
							Displayf("PlayEnd: %u", waveFormat->PlayEnd);

							if(waveFormat->Format == audWaveFormat::kMP3)
							{
								// decode post-encoder gain info
								if(waveFormat->LoopEnd != 0)
								{
									Displayf("Post-encoder headroom: %umB", waveFormat->LoopBegin);
								}
								else
								{
									Displayf("No post-encoder headroom info");
								}
							}
						}
						else
						{
							Displayf("LengthSamples: %u", sysEndian::Swap(waveFormat->LengthSamples));
							Displayf("LoopPointSamples: %d", sysEndian::Swap(waveFormat->LoopPointSamples));
							Displayf("Sample rate: %u", sysEndian::Swap(waveFormat->SampleRate));
							Displayf("Headroom: %d", sysEndian::Swap(waveFormat->Headroom));
							Displayf("LoopBegin: %u", sysEndian::Swap(waveFormat->LoopBegin));
							Displayf("LoopEnd: %u", sysEndian::Swap(waveFormat->LoopEnd));
							Displayf("PlayEnd: %u", sysEndian::Swap(waveFormat->PlayEnd));
							
							if(waveFormat->Format == audWaveFormat::kMP3)
							{
								// decode post-encoder gain info
								if(waveFormat->LoopEnd != 0)
								{
									Displayf("Post-encoder headroom: %umB", sysEndian::Swap(waveFormat->LoopBegin));
								}
								else
								{
									Displayf("No post-encoder headroom info");
								}
							}

						}

						CompileTimeAssert(sizeof(waveFormat->PlayBegin) == 1);
						Displayf("PlayBegin: %u", waveFormat->PlayBegin);
					}
				}					
			}
			else
			{
				// list all objects/chunks
				for(adatObjectId objectId = 0; objectId < (adatObjectId)container.GetNumObjects(); objectId++)
				{
					adatObjectDataChunkId numDataChunks = (adatObjectDataChunkId)container.GetNumDataChunksForObject(objectId);
					Displayf("Object %d (hash: %u) has %d data chunks:", objectId, container.GetObjectNameHash(objectId), numDataChunks);
					Displayf("Data size: %u Contiguous size: %u", container.ComputeObjectDataSize(objectId), container.ComputeContiguousObjectSize(objectId));
					for(adatObjectDataChunkId chunkId = 0; chunkId < numDataChunks; chunkId++)
					{
						u32 typeHash, offsetBytes, sizeBytes;
						container.GetObjectDataChunk(objectId, chunkId, typeHash, offsetBytes, sizeBytes);
						Displayf("Chunk %d type: %s (%02X) offset: %u size: %u first dword: %08X", chunkId, GetChunkTypeName(typeHash), typeHash, offsetBytes, sizeBytes, *(u32*)(container.GetDataPointer() + offsetBytes));
						if(PARAM_decodeformat.Get() && typeHash == formatTypeHash && sizeBytes == sizeof(audWaveFormat))
						{
							audWaveFormat *waveFormat = (audWaveFormat*)((u8*)container.GetDataPointer() + offsetBytes);

							if(waveFormat->Format >= audWaveFormat::kNumFormats)
							{
								Errorf("Invalid wave format: %u", waveFormat->Format);
							}

							Displayf("Format chunk:");
							const char *formatTable[] = {
								"16bit PCM (LE)",
								"16bit PCM (BE)",
								"32bit IEEE Float (LE)",
								"32bit IEEE Float (BE)",
								"ADPCM",
								"XMA2",
								"xWMA",
								"MP3",
								"Ogg Vorbis",
								"AAC",
								"WMA"};

								CompileTimeAssert(sizeof(waveFormat->Format) == 1);
								Displayf("Format: %s (%u)", formatTable[waveFormat->Format], waveFormat->Format);

								if(container.IsNativeEndian())
								{
									Displayf("LengthSamples: %u", waveFormat->LengthSamples);
									Displayf("LoopPointSamples: %d", waveFormat->LoopPointSamples);
									Displayf("Sample rate: %u", waveFormat->SampleRate);
									Displayf("Headroom: %d", waveFormat->Headroom);
									Displayf("LoopBegin: %u", waveFormat->LoopBegin);
									Displayf("LoopEnd: %u", waveFormat->LoopEnd);
									Displayf("PlayEnd: %u", waveFormat->PlayEnd);

									if(waveFormat->Format == audWaveFormat::kMP3)
									{
										// decode post-encoder gain info
										if(waveFormat->LoopEnd != 0)
										{
											Displayf("Post-encoder headroom: %umB", waveFormat->LoopBegin);
										}
										else
										{
											Displayf("No post-encoder headroom info");
										}
									}
								}
								else
								{
									Displayf("LengthSamples: %u", sysEndian::Swap(waveFormat->LengthSamples));
									Displayf("LoopPointSamples: %d", sysEndian::Swap(waveFormat->LoopPointSamples));
									Displayf("Sample rate: %u", sysEndian::Swap(waveFormat->SampleRate));
									Displayf("Headroom: %d", sysEndian::Swap(waveFormat->Headroom));
									Displayf("LoopBegin: %u", sysEndian::Swap(waveFormat->LoopBegin));
									Displayf("LoopEnd: %u", sysEndian::Swap(waveFormat->LoopEnd));
									Displayf("PlayEnd: %u", sysEndian::Swap(waveFormat->PlayEnd));

									if(waveFormat->Format == audWaveFormat::kMP3)
									{
										// decode post-encoder gain info
										if(waveFormat->LoopEnd != 0)
										{
											Displayf("Post-encoder headroom: %umB", sysEndian::Swap(waveFormat->LoopBegin));
										}
										else
										{
											Displayf("No post-encoder headroom info");
										}
									}

								}

								CompileTimeAssert(sizeof(waveFormat->PlayBegin) == 1);
								Displayf("PlayBegin: %u", waveFormat->PlayBegin);
						}
					}
				}
			}
		}
		else if(PARAM_extract.Get())
		{
			const char *outputFile = "output.bin";
			PARAM_extract.Get(outputFile);
			const char *chunkTypeName = "";
			PARAM_chunk.Get(chunkTypeName);
			const char *objectName = "";
			PARAM_object.Get(objectName);

			adatObjectId objectId = container.FindObject(objectName);
			if(objectId == adatContainer::InvalidId)
			{
				Errorf("Failed to find object %s in container", objectName);
				return -1;
			}

			adatObjectDataChunkId chunkId = container.FindObjectData(objectId, chunkTypeName);
			if(chunkId == adatContainer::InvalidId)
			{
				Errorf("Failed to find chunk %s in object %s (id %d) in container", chunkTypeName, objectName, objectId);
				return -1;
			}

			u32 typeHash, sizeBytes, offsetBytes;
			container.GetObjectDataChunk(objectId, chunkId, typeHash, offsetBytes, sizeBytes);

			Displayf("Found chunk id %d:%d (type %08X) size %u at offset %u - extracting to %s", objectId, chunkId, typeHash, sizeBytes, offsetBytes, outputFile);
			fiStream *outputStream = ASSET.Create(outputFile, NULL);
			outputStream->Write(buf + offsetBytes, sizeBytes);
			outputStream->Close();
		}
		else if(PARAM_create.Get())
		{
			adatContainerBuilder containerBuilder(adatContainer::OPTIMISED_PACKING);
			if(!containerBuilder.PopulateFromPrebuiltContainer(&container))
			{
				Errorf("Error parsing container");
				return -1;
			}

			// set all chunks to 16-byte alignment
			const u32 numExistingObjects = containerBuilder.GetNumObjects();
			for(u32 i = 0; i < numExistingObjects; i++)
			{
				const u32 numChunks = containerBuilder.GetNumDataChunks((adatObjectId)i);
				for(u32 k = 0; k < numChunks; k++)
				{
					containerBuilder.SetDataChunkAlignment((adatObjectId)i, (adatObjectDataChunkId)k, 16);
				}
			}
			
			if(PARAM_chunk.Get())
			{
				const char *chunkTypeName = "";
				PARAM_chunk.Get(chunkTypeName);

				u8 chunkData[] = {0xaa,0xbb,0xcc,0xdd,0xee,0xff};

				const char *objectName = "";
				PARAM_object.Get(objectName);

				adatObjectId objectId = containerBuilder.FindObject(atStringHash(objectName));
				if(objectId == adatContainer::InvalidId)
				{
					Displayf("Creating new object: %s", objectName);
					objectId = containerBuilder.AddObject(atStringHash(objectName));
				}

				containerBuilder.AddDataChunk(objectId, atStringHash(chunkTypeName), chunkData, sizeof(chunkData), 16);

			}
			else if(PARAM_object.Get())
			{
				const char *name = "";
				PARAM_object.Get(name);
				containerBuilder.AddObject(atStringHash(name));
			}

			fiStream *stream = ASSET.Create(fileName, NULL);
			Assert(stream);
			containerBuilder.Serialize(stream, isBigEndian);
			stream->Close();
		}
		else if(PARAM_computepadding.Get())
		{
			u32 padding = ComputeMP3Padding(container);
			Displayf("Total MP3 padding: %u (%.2f%)", padding, 100.f * (padding / (float)containerSizeBytes));
		}
	
		delete[] buf;
	}

	return 0;	
}
#else
int main(int,char **)
{
	return 0;
}
#endif // __TOOL
