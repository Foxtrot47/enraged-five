top: bottom

include ..\..\..\..\rage\build\Makefile.template

.PHONY: FORCE
TARGET = adatutil
ELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.elf
SELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.self

bottom: $(SELF)

$(SELF): $(ELF)
	make_fself $(ELF) $(SELF)

LIBS += ..\..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib

..\..\..\..\..\rage\base\src\vcproj\RageCore\$(INTDIR)\RageCore.lib: FORCE
	$(MAKE) -C ..\..\..\..\..\rage\base\src\vcproj\RageCore -f RageCore.mk

LIBS += ..\..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib

..\..\..\..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib: FORCE
	$(MAKE) -C ..\..\..\..\..\rage\stlport\STLport-5.0RC5\src -f stlport.mk

LIBS += ..\..\..\..\..\rage\base\src\audiodata/$(INTDIR)/audiodata.lib

..\..\..\..\..\rage\base\src\audiodata/$(INTDIR)/audiodata.lib: FORCE
	$(MAKE) -C ..\..\..\..\..\rage\base\src\audiodata -f audiodata.mk

INCLUDES = -I ..\..\..\..\rage\stlport\STLport-5.0RC5\stlport -I ..\..\..\..\..\rage\base\src -I ..\..\..\..\..\rage\stlport\STLport-5.0RC5\src -I ..\..\..\..\..\rage\base\tools\cli -I ..\..\..\..\..\rage\base\tools\dcc\libs -I ..\..\..\..\..\rage\base\tools\libs

$(INTDIR)\adatutil.obj: adatutil.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(ELF): $(LIBS) $(INTDIR)\adatutil.obj
	ppu-lv2-gcc -o $(ELF) $(INTDIR)\adatutil.obj -Wl,--start-group $(LIBS) -Wl,--end-group $(LLIBS)
