﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Perforce integrate program.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PerforceIntegrate
{
    using System;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// The program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The branch.
        /// </summary>
        private static string branch;

        /// <summary>
        /// The resolve action.
        /// </summary>
        private static ResolveAction resolveAction;

        /// <summary>
        /// The P4 port.
        /// </summary>
        private static string p4Port;

        /// <summary>
        /// The P4 workspace.
        /// </summary>
        private static string p4Workspace;

        /// <summary>
        /// The P4 user.
        /// </summary>
        private static string p4User;

        /// <summary>
        /// The P4 password.
        /// </summary>
        private static string p4Password;

        /// <summary>
        /// The P4 depot root.
        /// </summary>
        private static string p4DepotRoot;

        /// <summary>
        /// The main method.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            ParseArgs(args);

            var assetManager = AssetManagerFactory.GetInstance(
                AssetManagerType.Perforce, null, p4Port, p4Workspace, p4User, p4Password, p4DepotRoot);
            
            // Check we can connect to asset manager
            assetManager.Connect();

            var app = new PerforceIntegrate(assetManager);
            string changeListNumber;
            if (!app.Integrate(branch, resolveAction, out changeListNumber))
            {
                Environment.Exit(-1);
            }
        }

        /// <summary>
        /// Parse the args.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        private static void ParseArgs(string[] args)
        {
            for (var i = 0; i < args.Length; i++)
            {
                if (!args[i].StartsWith("-"))
                {
                    continue;
                }

                if (i + 1 == args.Length)
                {
                    Help();
                    throw new Exception("Invalid args");
                }

                switch (args[i])
                {
                    case "-branch":
                        {
                            branch = args[i + 1];
                            break;
                        }

                    case "-resolve":
                        {
                            if (!Enum.TryParse(args[i + 1], true, out resolveAction))
                            {
                                Help();
                                throw new Exception("Failed to parse resolve action: " + args[i + 1]);
                            }

                            break;
                        }

                    case "-p4host":
                        {
                            p4Port = args[i + 1];
                            break;
                        }

                    case "-p4client":
                        {
                            p4Workspace = args[i + 1];
                            break;
                        }

                    case "-p4user":
                        {
                            p4User = args[i + 1];
                            break;
                        }

                    case "-p4passwd":
                        {
                            p4Password = args[i + 1];
                            break;
                        }

                    case "-p4depotroot":
                        {
                            p4DepotRoot = args[i + 1];
                            break;
                        }
                }
            }

            if (string.IsNullOrEmpty(branch))
            {
                throw new ArgumentException("Branch required");
            }

            if (resolveAction == ResolveAction.Unknown)
            {
                throw new ArgumentException("Resolve action required");
            }
        }

        /// <summary>
        /// Print help info.
        /// </summary>
        private static void Help()
        {
            Console.WriteLine("Usage: perforceintegrate.exe -branch <branch> -resolve <resolve action>");
        }
    }
}
