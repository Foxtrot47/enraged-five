﻿// -----------------------------------------------------------------------
// <copyright file="PerforceIntegrate.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace PerforceIntegrate
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Perforce;

    /// <summary>
    /// Perforce integration tool.
    /// </summary>
    public class PerforceIntegrate
    {
        /// <summary>
        /// The change list description.
        /// </summary>
        private const string ChangeListDescription = "Integrating branch ";

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="PerforceIntegrate"/> class.
        /// </summary>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public PerforceIntegrate(IAssetManager assetManager)
        {
            if (null == assetManager)
            {
                throw new ArgumentNullException("assetManager");
            }

            this.assetManager = assetManager;
        }

        /// <summary>
        /// Perform the P4 integration.
        /// </summary>
        /// <param name="branch">
        /// The branch.
        /// </param>
        /// <param name="resolveAction">
        /// The resolve action.
        /// </param>
        /// <param name="submitChangelist">
        /// If the change list should also be submitted.
        /// </param>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        /// <returns>
        /// The change list number <see cref="string"/>.
        /// </returns>
        public bool Integrate(string branch, ResolveAction resolveAction, bool submitChangelist, out string changeListNumber)
        {
            changeListNumber = null;
            var changeList = this.CreateChangeList(branch);
            if (null == changeList)
            {
                return false;
            }

            if (!this.Integrate(changeList, branch))
            {
                return false;
            }

            if (!this.Resolve(changeList, resolveAction))
            {
                return false;
            }

            // Refresh to load details of files in change list into memory
            changeList.Refresh();

            if (!changeList.Assets.Any())
            {
                // Nothing to submit
                return changeList.Delete();
            }

            if(submitChangelist) {
                if(!changeList.Submit()) {
                    return false;
                }
            }

            changeListNumber = changeList.NumberAsString;
            return true;
        }

        /// <summary>
        /// Perform the P4 integration and submit.
        /// </summary>
        /// <param name="branch">
        /// The branch.
        /// </param>
        /// <param name="resolveAction">
        /// The resolve action.
        /// </param>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        /// <returns>
        /// The change list number <see cref="string"/>. Null if nothing to submit
        /// </returns>
        public bool Integrate(string branch, ResolveAction resolveAction, out string changeListNumber) 
        {
            return Integrate(branch, resolveAction, true, out changeListNumber);
        }


        /// <summary>
        /// Create a new change list for the integration.
        /// </summary>
        /// <param name="branch">
        /// The branch.
        /// </param>
        /// <returns>
        /// The new change list <see cref="IChangeList"/>.
        /// </returns>
        private IChangeList CreateChangeList(string branch)
        {
            return this.assetManager.CreateChangeList(ChangeListDescription + branch);
        }

        /// <summary>
        /// Perform the integration.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="branch">
        /// The branch.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool Integrate(IChangeList changeList, string branch)
        {
            var args = new List<string>
                {
                    "-c" + changeList.NumberAsString,
                    "-b" + branch,
                    "-d"
                };

            var request = new P4RunCmdRequest("integrate", args);
            this.assetManager.RequestHandler.ActionRequest(request);
            if (request.HasErrors())
            {
                Console.WriteLine("Failed to integrate: " + request.FormattedErrors);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Resolve any pending conflicts.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="resolveAction">
        /// The resolve action.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool Resolve(IChangeList changeList, ResolveAction resolveAction)
        {
            string resolveKey;

            switch (resolveAction)
            {
                case ResolveAction.AcceptMerged:
                    resolveKey = "f";
                    break;

                case ResolveAction.AcceptTheirs:
                    resolveKey = "t";
                    break;

                case ResolveAction.AcceptYours:
                    resolveKey = "y";
                    break;

                default:
                    Console.WriteLine("Resolve action not supported in auto-resolve: " + resolveAction.ToString());
                    return false;
            }

            var args = new List<string>
                {
                    "-c" + changeList.NumberAsString,
                    "-a" + resolveKey,
                };

            var request = new P4RunCmdRequest("resolve", args);
            this.assetManager.RequestHandler.ActionRequest(request);
            if (request.HasErrors())
            {
                Console.WriteLine("Failed to integrate: " + request.FormattedErrors);
                return false;
            }

            return true;
        }
    }
}
