﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Perforce.P4;
using rage.ToolLib;
using SpeechAnalyticsModel;

#endregion

namespace SpeechWaveAssets
{
    public class DatabaseUpdater
    {
        private int _updated, _created;

        public void UpdateDatabaseWithSpeechWaves(speech_analyticsEntities database,
            Dictionary<string, List<FileHistory>> fileHistories)
        {
            DbSet<SpeechWaveAsset> speechWaveAssets = database.SpeechWaveAssets;
            try
            {
                database.Configuration.AutoDetectChangesEnabled = false;

                foreach (KeyValuePair<string, List<FileHistory>> fileHistory in fileHistories)
                {
                    SetSpeechFromFileHistory(
                        fileHistory,
                        speechWaveAssets);
                }
                Trace.WriteLine(string.Format(
                    "{0} entries have been _updated, {1} entries have been added.", 
                    _updated,
                    _created));
            }
            finally
            {
                database.ChangeTracker.DetectChanges();
                database.SaveChanges();
            }
        }

        private void SetSpeechFromFileHistory(KeyValuePair<string, List<FileHistory>> fileHistory,
            DbSet<SpeechWaveAsset> speechWaveAssets)
        {
            string waveName = Path.GetFileNameWithoutExtension(fileHistory.Key);

            List<string> splitName = fileHistory.Key.ToUpper().Split('/').ToList();

            string packName = splitName[splitName.IndexOf("WAVES") + 1];

            string voiceName = splitName[splitName.Count - 2];

            FileHistory addedHistory =
                fileHistory.Value.Last(
                    p =>
                        p.Action == FileAction.MoveAdd ||
                        p.Action == FileAction.Add ||
                        p.Action == FileAction.Added ||
                        p.Action == FileAction.Branch ||
                        p.Action == FileAction.BranchInto);

            int variationNumber = -1;
            if (string.IsNullOrEmpty(waveName))
            {
                return;
            }

            try
            {
                int.TryParse(waveName.Substring(waveName.Length - 2, 2), out variationNumber);
            }
            catch
            {
            }


            string context = waveName.Remove(waveName.Length - 3, 3);
            Hash hash = new Hash {Value = context};
            Hash voiceNameHash = new Hash {Value = voiceName};
            Hash wavenameHash = new Hash {Value = waveName};

            FileHistory deletedHistory = fileHistory.Value.FirstOrDefault(p => p.Action == FileAction.Delete);
            DateTime? dateDeleted = null;
            int? deletedCl = null;
            if (deletedHistory != null)
            {
                dateDeleted = deletedHistory.Date;
                deletedCl = deletedHistory.ChangelistId;
            }

            FileHistory modifiedHistory = fileHistory.Value.FirstOrDefault(p => p.Action == FileAction.Edit);
            DateTime? dateModified = null;
            int? modifiedCl = null;
            if (modifiedHistory != null)
            {
                dateModified = modifiedHistory.Date;
                modifiedCl = modifiedHistory.ChangelistId;
            }

            InsertOrUpdateSpeechWaves(speechWaveAssets, fileHistory, waveName, packName, addedHistory, context,
                dateModified, modifiedCl, hash, dateDeleted, deletedCl, voiceName, variationNumber, voiceNameHash,
                wavenameHash);
        }

        private void InsertOrUpdateSpeechWaves(DbSet<SpeechWaveAsset> speechWaveAssets,
            KeyValuePair<string, List<FileHistory>> fileHistory, string waveName,
            string packName, FileHistory addedHistory, string context, DateTime? dateModified, int? modifiedCl,
            Hash hash,
            DateTime? dateDeleted, int? deletedCl, string voiceName, int variationNumber, Hash voiceNameHash,
            Hash wavenameHash)
        {
            SpeechWaveAsset speechWaveAsset =
                speechWaveAssets.FirstOrDefault(p => p.AssetPath.Equals(fileHistory.Key));

            if (speechWaveAsset == null)
            {
                _created++;
                speechWaveAssets.Add(new SpeechWaveAsset
                {
                    AssetPath = fileHistory.Key,
                    WaveName = waveName,
                    PackName = packName,
                    AddedCL = addedHistory.ChangelistId,
                    Context = context,
                    DateAdded = addedHistory.Date,
                    DateModified = dateModified,
                    ModifiedCL = modifiedCl,
                    ContextHash = hash.Key,
                    DateDeleted = dateDeleted,
                    DeletedCL = deletedCl,
                    VoiceName = voiceName,
                    IsPlaceholder = packName.Contains("PS_"),
                    VariationNumber = (long?) variationNumber == -1 ? null : (long?) variationNumber,
                    VoiceNameHash = (int?) voiceNameHash.Key,
                    WaveNameHash = wavenameHash.Key
                });
            }
            else
            {
                _updated++;
                speechWaveAsset.AssetPath = fileHistory.Key;
                speechWaveAsset.WaveName = waveName;
                speechWaveAsset.PackName = packName;
                speechWaveAsset.AddedCL = addedHistory.ChangelistId;
                speechWaveAsset.Context = context;
                speechWaveAsset.DateAdded = addedHistory.Date;
                speechWaveAsset.DateModified = dateModified;
                speechWaveAsset.ModifiedCL = modifiedCl;
                speechWaveAsset.ContextHash = hash.Key;
                speechWaveAsset.DateDeleted = dateDeleted;
                speechWaveAsset.DeletedCL = deletedCl;
                speechWaveAsset.VoiceName = voiceName;
                speechWaveAsset.IsPlaceholder = packName.Contains("PS_");
                speechWaveAsset.VariationNumber = (long?) variationNumber == -1 ? null : (long?) variationNumber;
                speechWaveAsset.VoiceNameHash = (int?) voiceNameHash.Key;
                speechWaveAsset.WaveNameHash = wavenameHash.Key;
            }
        }
    }
}