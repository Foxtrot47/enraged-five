﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Perforce.P4;

#endregion

namespace SpeechWaves
{
    public class SpeechWavesGenerator
    {
        private const string WavesLocation = @"//rdr3/audio/dev/assets/Waves/*";
        private Connection _perforceConnection;
        private Repository _repository;
        public PerforceConnection ConnectionSettings = new PerforceConnection();

        public List<string> GetAllSpeechFolders()
        {
            GetDepotDirsCmdOptions opts =
                new GetDepotDirsCmdOptions(GetDepotDirsCmdFlags.IncludeDeletedFilesDirs, null);
            IList<String> depotDirs = _repository.GetDepotDirs(opts, WavesLocation);
            List<string> speechDirs = depotDirs.Where(
                dir =>
                    dir.ToUpper().Contains("/PAIN") ||
                    dir.ToUpper().Contains("/SS_") ||
                    dir.ToUpper().Contains("/S_") ||
                    dir.ToUpper().Contains("/PEDS") ||
                    dir.ToUpper().Contains("/PS_")).ToList();
            return speechDirs;
        }

        public Dictionary<string, List<FileHistory>> GetWavFileHistory(List<string> speechDirs)
        {
            GetFileHistoryCmdOptions fileopts = new GetFileHistoryCmdOptions(GetFileHistoryCmdFlags.FullDescription, 0, 100);

            Dictionary<string, List<FileHistory>> waveFiles = new Dictionary<string, List<FileHistory>>();

            foreach (string depotPath in speechDirs)
            {
                FileSpec fs = new FileSpec(new DepotPath(depotPath + "/..."), null);
                IList<FileHistory> files = _repository.GetFileHistory(fileopts, fs);
                foreach (FileHistory wavfile in files)
                {
                    if (!wavfile.DepotPath.Path.EndsWith(".WAV", StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }


                    if (!waveFiles.ContainsKey(wavfile.DepotPath.Path) || waveFiles[wavfile.DepotPath.Path] == null)
                    {
                        waveFiles[wavfile.DepotPath.Path] = new List<FileHistory>();
                    }
                    waveFiles[wavfile.DepotPath.Path].Add(wavfile);
                }
            }
            RemoveEntriesDeletedBeforeDate(new DateTime(2015, 1, 1), waveFiles);
            return waveFiles;
        }


        public void RemoveEntriesDeletedBeforeDate(DateTime dateTime, Dictionary<string, List<FileHistory>> dictionary)
        {
            List<string> filesToRemove = new List<string>();
            foreach (KeyValuePair<string, List<FileHistory>> filehistory in dictionary)
            {
                if (filehistory.Value.Count <= 1)
                {
                    continue;
                }
                filehistory.Value.Sort((history1, history2) => history2.Date.CompareTo(history1.Date));
                var latestHistory = filehistory.Value[0];

                if (latestHistory.Action == FileAction.Delete || latestHistory.Action == FileAction.Purge ||
                    latestHistory.Action == FileAction.MoveDelete)
                {
                    if (latestHistory.Date < dateTime)
                    {
                        filesToRemove.Add(filehistory.Key);
                    }
                }

            }

            foreach (string file in filesToRemove)
            {
                dictionary.Remove(file);
            }

        }

        public void ConnectToPerforce()
        {
            ServerAddress serverAddress = new ServerAddress(ConnectionSettings.P4Server);
            Server server = new Server(serverAddress);
            _repository = new Repository(server);
            _perforceConnection = _repository.Connection;

            if (!string.IsNullOrEmpty(ConnectionSettings.UserName))
            {
                // use the connection varaibles for this connection
                _perforceConnection.UserName = ConnectionSettings.UserName;
                _perforceConnection.Client = new Client();
                _perforceConnection.Client.Name = ConnectionSettings.P4Client;
                _perforceConnection.Login(ConnectionSettings.Password);
            }

          
            _perforceConnection.Connect(new Options());
            _perforceConnection.CommandTimeout = new TimeSpan(0, 0, 5, 0);
        }
    }
}