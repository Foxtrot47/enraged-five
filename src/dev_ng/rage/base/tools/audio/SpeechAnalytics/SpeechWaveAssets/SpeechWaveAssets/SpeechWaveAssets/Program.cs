﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Perforce.P4;
using rage.ToolLib;
using rage.ToolLib.CmdLine;
using SpeechAnalyticsModel;
using SpeechWaves;
using File = System.IO.File;

#endregion

namespace SpeechWaveAssets
{
    internal class Program : CommandLineConfiguration
    {

        [Description("Perforce username")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string UserName { get; set; }

        [Description("Perforce password")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string Password { get; set; }

        [Description("Perforce Client")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string P4Client { get; set; }


        [Description("Perforce Server")]
        [CommandLineConfiguration.OptionalParameterAttribute]
        public string P4Server { get; set; }

        private static void Main(string[] args)
        {

            Program config;
            try
            {

                Trace.Listeners.Clear();
                string logFilepath = Path.Combine(Path.GetTempPath(),
                    "SpeechWaveAsset_log_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", string.Empty) + ".txt");
                File.Create(logFilepath);

                TextWriterTraceListener twtl =
                    new TextWriterTraceListener(logFilepath)
                    {
                        Name = "TextLogger",
                        TraceOutputOptions = TraceOptions.ThreadId | TraceOptions.DateTime
                    };

                ConsoleTraceListener ctl = new ConsoleTraceListener(false);
                ctl.TraceOutputOptions = TraceOptions.DateTime;

                Trace.Listeners.Add(twtl);
                Trace.Listeners.Add(ctl);
                Trace.AutoFlush = true;
                config = new Program(args);
            }
            catch (rage.ToolLib.CmdLine.CommandLineConfiguration.ParameterException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Usage);
            }
        }


        public Program(string[] args)
            : base(args)
        {
            try
            {

                Trace.WriteLine("Starting speech waves update.");
                SpeechWavesGenerator speechWaves = new SpeechWavesGenerator();
                speechWaves.ConnectionSettings = new PerforceConnection
                {
                    P4Client = P4Client,
                    P4Server = P4Server,
                    Password = Password,
                    UserName = UserName
                };
                Trace.WriteLine("Connecting to perforce...");

                speechWaves.ConnectToPerforce();
                Trace.WriteLine("Getting all speech folders...");
                List<string> speechfolders = speechWaves.GetAllSpeechFolders();
                Trace.WriteLine("Getting all speech history files...");
                Dictionary<string, List<FileHistory>> fileHistories = speechWaves.GetWavFileHistory(speechfolders);
                speech_analyticsEntities database = new speech_analyticsEntities();
                DatabaseUpdater dbUpdater = new DatabaseUpdater();

                Trace.WriteLine(string.Format("{0} entries found, updating database ", fileHistories.Count));
                dbUpdater.UpdateDatabaseWithSpeechWaves(database, fileHistories);
                Trace.WriteLine("Exiting with exit code " + Environment.ExitCode);
            }
            catch (Exception e)
            {
                ReportMailer.ErrorMessageSender(e);
                Trace.WriteLine(string.Format("Errors encountered: {0}\nStackTrace: {1}", e.Message, e.StackTrace));
            }

        }
    }
}