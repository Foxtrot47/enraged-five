﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SpeechWaveAssets
{
    class ReportMailer
    {
        public static void ErrorMessageSender(Exception exception)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.Subject = "Speech Waves Update Report - errors encountered";
            mailMessage.From = new MailAddress("speechwaveserrorreporter@rockstarnorth.com");
            mailMessage.To.Add("Danjeli.schembri@rockstarnorth.com");
            mailMessage.To.Add("ben.durrenberger@rockstarnorth.com");
            mailMessage.To.Add("Alastair.MacGregor@rockstarnorth.com");
            mailMessage.ReplyToList.Add("Danjeli.schembri@rockstarnorth.com");
            mailMessage.Body = string.Format("Error message: {0} \n\n Error source: {1} \n\n Error Stack trace: {2} ",
                exception.Message, exception.Source, exception.StackTrace);

            File.WriteAllText(Path.GetTempFileName() + "log.txt", mailMessage.Body);
            SmtpClient client = new SmtpClient("smtp.rockstar.t2.corp") { Credentials = CredentialCache.DefaultNetworkCredentials };
            client.Send(mailMessage);
        }
    }
}
