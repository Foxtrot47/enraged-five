﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummaryEmailReportCreator.Templates
{
    partial class HtmlReportTemplate
    {
        private string title;
        private List<string> reportSummaryLines = new List<string>();
        private List<string> repeatingTimeLines = new List<string>();
        private bool printLineBold = true;

        public HtmlReportTemplate(string title, List<string> reportSummaryLines, List<string> repeatingTimeLines)
        {
            this.title = title;
            this.reportSummaryLines = reportSummaryLines;
            this.repeatingTimeLines = repeatingTimeLines;
        }
    }
}
