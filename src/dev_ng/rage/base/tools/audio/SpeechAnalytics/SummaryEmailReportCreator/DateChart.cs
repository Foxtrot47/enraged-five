﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace SummaryEmailReportCreator
{
    public class DateChart
    {
        private Chart chartPlotter;

        public DateChart(int width, int height)
        {
            chartPlotter = new Chart();
            chartPlotter.Size = new Size(width, height);

            ChartArea chartArea = new ChartArea();
            chartArea.AxisX.LabelStyle.Format = "dd/MMM/yyyy\nhh:mm";
            chartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
            chartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
            chartPlotter.ChartAreas.Add(chartArea);
            Legend legend = new Legend();
            legend.Docking = Docking.Bottom;
            chartPlotter.Legends.Add(legend);
        }

        public void AddDataLine(IEnumerable<ReportData.GraphEntry> graphData, string name)
        {
            var series = new Series();
            series.Name = name;
            series.ChartType = SeriesChartType.FastLine;
            series.XValueType = ChartValueType.DateTime;
            chartPlotter.Series.Add(series);
            List<DateTime> xValues = new List<DateTime>();
            List<double> yValues = new List<double>();
            foreach (ReportData.GraphEntry entry in graphData)
            {
                xValues.Add(entry.Date);
                yValues.Add(entry.Value);
            }
            chartPlotter.Series[name].Points.DataBindXY(xValues, yValues);
        }

        public void SaveChartImageTo(string path)
        {
            // draw the chart
            chartPlotter.Invalidate();
            chartPlotter.SaveImage(path, ChartImageFormat.Png);
        }
    }
}
