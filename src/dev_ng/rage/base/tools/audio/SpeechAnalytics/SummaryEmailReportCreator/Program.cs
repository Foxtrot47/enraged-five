﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace SummaryEmailReportCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] voicesForRepeatingVariations = ConfigurationManager.AppSettings["VoicesForRepeatingVariations"].Split(new[] { ';', ',', ' ' });

            string[] recipients = ConfigurationManager.AppSettings["ReportRecipients"].Split(new[]{';', ',', ' '});
            string sender = ConfigurationManager.AppSettings["Sender"];
            string smtp = ConfigurationManager.AppSettings["SMTP"];

            ReportEmail mail = new ReportEmail(createDummyData());
            mail.Send(recipients, sender, smtp);
        }

        private static ReportData createDummyData()
        {
            List<ReportData.GraphEntry> dialogueTriggered = new List<ReportData.GraphEntry>();
            List<ReportData.GraphEntry> dialogueTriggeredPercentage = new List<ReportData.GraphEntry>();
            List<ReportData.GraphEntry> placeholderTriggered = new List<ReportData.GraphEntry>();
            List<ReportData.GraphEntry> missingLines = new List<ReportData.GraphEntry>();

            DateTime d1 = new DateTime(2015, 9, 16);
            dialogueTriggered.Add(new ReportData.GraphEntry(d1, 7103));
            dialogueTriggeredPercentage.Add(new ReportData.GraphEntry(d1, 100 * 7103 / 14000.0));
            placeholderTriggered.Add(new ReportData.GraphEntry(d1, 3103));
            missingLines.Add(new ReportData.GraphEntry(d1, 54));
            DateTime d2 = new DateTime(2015, 9, 23);
            dialogueTriggered.Add(new ReportData.GraphEntry(d2, 5438));
            dialogueTriggeredPercentage.Add(new ReportData.GraphEntry(d2, 100 * 5438 / 14000.0));
            placeholderTriggered.Add(new ReportData.GraphEntry(d2, 2500));
            missingLines.Add(new ReportData.GraphEntry(d2, 124));
            DateTime d3 = new DateTime(2015, 9, 30);
            dialogueTriggered.Add(new ReportData.GraphEntry(d3, 7621));
            dialogueTriggeredPercentage.Add(new ReportData.GraphEntry(d3, 100 * 7621 / 14000.0));
            placeholderTriggered.Add(new ReportData.GraphEntry(d3, 3000));
            missingLines.Add(new ReportData.GraphEntry(d3, 68));
            DateTime d4 = new DateTime(2015, 10, 7);
            dialogueTriggered.Add(new ReportData.GraphEntry(d4, 4456));
            dialogueTriggeredPercentage.Add(new ReportData.GraphEntry(d4, 100 * 4456 / 14000.0));
            placeholderTriggered.Add(new ReportData.GraphEntry(d4, 3500));
            missingLines.Add(new ReportData.GraphEntry(d4, 163));
            DateTime d5 = new DateTime(2015, 10, 14);
            dialogueTriggered.Add(new ReportData.GraphEntry(d5, 7539));
            dialogueTriggeredPercentage.Add(new ReportData.GraphEntry(d5, 100 * 7539 / 14000.0));
            placeholderTriggered.Add(new ReportData.GraphEntry(d5, 4309));
            missingLines.Add(new ReportData.GraphEntry(d5, 290));

            List<ReportData.RepeatingEntry> topRepeatingContexts = new List<ReportData.RepeatingEntry>();
            topRepeatingContexts.Add(new ReportData.RepeatingEntry("EXERT_LOW", new TimeSpan(0, 0, 45)));
            topRepeatingContexts.Add(new ReportData.RepeatingEntry("BREATH_HEAVY_MEDIUM", new TimeSpan(0, 3, 2)));
            topRepeatingContexts.Add(new ReportData.RepeatingEntry("BUMP", new TimeSpan(0, 17, 5)));
            topRepeatingContexts.Add(new ReportData.RepeatingEntry("PAIN_SHOVE", new TimeSpan(0, 18, 45)));
            List<ReportData.RepeatingEntry> topRepeatingVariations = new List<ReportData.RepeatingEntry>();
            topRepeatingVariations.Add(new ReportData.RepeatingEntry("ARTHUR - EXERT_LOW_07", new TimeSpan(0, 8, 0)));
            topRepeatingVariations.Add(new ReportData.RepeatingEntry("ARTHUR - EXERT_LOW_01", new TimeSpan(0, 9, 32)));
            topRepeatingVariations.Add(new ReportData.RepeatingEntry("ARTHUR - EXERT_LOW_06", new TimeSpan(0, 10, 55)));
            topRepeatingVariations.Add(new ReportData.RepeatingEntry("JAVIER - BUMP", new TimeSpan(0, 14, 0)));
            List<ReportData.RepeatingVariationsPerVoice> topRepeatingVariationsPerVoice = new List<ReportData.RepeatingVariationsPerVoice>();
            topRepeatingVariationsPerVoice.Add(new ReportData.RepeatingVariationsPerVoice("ARTHUR", new ReadOnlyCollection<ReportData.RepeatingEntry>(new List<ReportData.RepeatingEntry>(topRepeatingVariations.Take(3)))));


            ReportData data = new ReportData(DateTime.Now, "696.0", "8713021", "RDR3", 2328, new TimeSpan(776, 30, 0), 7539, 14000, 4209, 290, 89, 98, 101, 96, 120, dialogueTriggered, dialogueTriggeredPercentage, placeholderTriggered, missingLines, topRepeatingContexts, topRepeatingVariations, topRepeatingVariationsPerVoice);
            return data;
        }

    }
}
