﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SummaryEmailReportCreator
{
    public class ReportData
    {
        public ReportData(
            DateTime creationTime,
            string buildVersion,
            string speechDataCL,
            string projectName,
            int numSessionLogs,
            TimeSpan sessionPlayingTime,
            int dialogueLinesTriggered,
            int totalBuildDialogueLines,
            int placeholderLinesTriggered,
            int missingLinesTriggered,
            int numVoicesAssigned,
            int numVoicesWithBuiltSpeech,
            int numVoicesAssingedToPVGs,
            int numVoicesTriggered,
            int totalVoices,
            List<GraphEntry> dialogueTriggered,
            List<GraphEntry> dialogueTriggeredPercentage,
            List<GraphEntry> placeholderTriggered,
            List<GraphEntry> missingLines,
            List<RepeatingEntry> topRepeatingContexts,
            List<RepeatingEntry> topRepeatingVariations,
            List<RepeatingVariationsPerVoice> topRepeatingVariationsPerVoice)
        {
            CreationTime = creationTime;
            BuildVersion = buildVersion;
            SpeechDataCL = speechDataCL;
            ProjectName = projectName;
            NumSessionLogs = numSessionLogs;
            SessionPlayingTime = sessionPlayingTime;
            DialogueLinesTriggered = dialogueLinesTriggered;
            TotalBuildDialogueLines = totalBuildDialogueLines;
            PlaceholderLinesTriggered = placeholderLinesTriggered;
            MissingLinesTriggered = missingLinesTriggered;
            NumVoicesAssigned = numVoicesAssigned;
            NumVoicesWithBuiltSpeech = numVoicesWithBuiltSpeech;
            NumVoicesAssingedToPVGs = numVoicesAssingedToPVGs;
            NumVoicesTriggered = numVoicesTriggered;
            TotalVoices = totalVoices;
            DialogueTriggered = dialogueTriggered.AsReadOnly();
            DialogueTriggeredPercentage = dialogueTriggeredPercentage.AsReadOnly();
            PlaceholderTriggered = placeholderTriggered.AsReadOnly();
            MissingLines = missingLines.AsReadOnly();
            TopRepeatingContexts = topRepeatingContexts.AsReadOnly();
            TopRepeatingVariations = topRepeatingVariations.AsReadOnly();
            TopRepeatingVariationsPerVoiceList = topRepeatingVariationsPerVoice.AsReadOnly();
        }

        public DateTime CreationTime { get; private set; }

        public string BuildVersion { get; private set; }

        public string SpeechDataCL { get; private set; }

        public string ProjectName { get; private set; }

        public int NumSessionLogs { get; private set; }

        public TimeSpan SessionPlayingTime { get; private set; }

        public int DialogueLinesTriggered { get; private set; }

        public int TotalBuildDialogueLines { get; private set; }

        public int PlaceholderLinesTriggered { get; private set; }

        public int MissingLinesTriggered { get; private set; }

        public int NumVoicesAssigned { get; private set; }

        public int NumVoicesWithBuiltSpeech { get; private set; }

        public int NumVoicesAssingedToPVGs { get; private set; }

        public int NumVoicesTriggered { get; private set; }

        public int TotalVoices { get; private set; }

        public class GraphEntry
        {
            public GraphEntry(DateTime date, double value)
            {
                Date = date;
                Value = value;
            }
            public DateTime Date { get; private set; }
            public double Value { get; private set; }
        }

        public IReadOnlyCollection<GraphEntry> DialogueTriggered;

        public IReadOnlyCollection<GraphEntry> DialogueTriggeredPercentage;

        public IReadOnlyCollection<GraphEntry> PlaceholderTriggered;

        public IReadOnlyCollection<GraphEntry> MissingLines;

        public class RepeatingEntry
        {
            public RepeatingEntry(string name, TimeSpan repeatTime)
            {
                Name = name;
                RepeatTime = repeatTime;
            }
            public string Name { get; private set; }
            public TimeSpan RepeatTime { get; private set; }
        }

        public IReadOnlyCollection<RepeatingEntry> TopRepeatingContexts;

        public IReadOnlyCollection<RepeatingEntry> TopRepeatingVariations;

        public class RepeatingVariationsPerVoice
        {
            public RepeatingVariationsPerVoice(string voice, IReadOnlyCollection<RepeatingEntry> topRepeatingVariations)
            {
                Voice = voice;
                TopRepeatingVariations = topRepeatingVariations;
            }
            public string Voice;
            public IReadOnlyCollection<RepeatingEntry> TopRepeatingVariations;
        }

        public IReadOnlyCollection<RepeatingVariationsPerVoice> TopRepeatingVariationsPerVoiceList;

    }
}
