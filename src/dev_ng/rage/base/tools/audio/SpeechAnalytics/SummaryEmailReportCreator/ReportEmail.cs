﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using SummaryEmailReportCreator.Templates;

namespace SummaryEmailReportCreator
{
    public class ReportEmail
    {
        MailMessage reportEmail = new MailMessage();

        public ReportEmail(ReportData data)
        {
            string title = "Daily speech tracking for " + data.ProjectName + " - " + data.CreationTime;
            reportEmail.Subject = title;

            HtmlReportTemplate htmTemplateInstance = new HtmlReportTemplate(title, getReportSummaryLines(data), getRepeatingTimeLines(data));
            string htmlContent = htmTemplateInstance.TransformText();
            PlaintextReportTemplate textTemplateInstance = new PlaintextReportTemplate(title, getReportSummaryLines(data), getRepeatingTimeLines(data));
            string textContent = textTemplateInstance.TransformText();
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlContent, null, "text/html");
            AlternateView textView = AlternateView.CreateAlternateViewFromString(textContent, null, "text/plain");

            DateChart triggeredLinesChart = new DateChart(600, 350);
            triggeredLinesChart.AddDataLine(data.DialogueTriggered, "Lines of dialogue triggered");
            triggeredLinesChart.AddDataLine(data.PlaceholderTriggered, "Lines of placeholders triggered");
            triggeredLinesChart.AddDataLine(data.MissingLines, "Missing lines triggered");

            DateChart linesOfDialoguePercentage = new DateChart(600, 200);
            linesOfDialoguePercentage.AddDataLine(data.DialogueTriggeredPercentage, "Percentage of dialogue triggered");

            string triggeredLinesChartPath = Path.Combine(Path.GetTempPath(), "triggeredLinesChart.png");
            triggeredLinesChart.SaveChartImageTo(triggeredLinesChartPath);
            LinkedResource triggeredLinesHtmlRessource = new LinkedResource(triggeredLinesChartPath);
            triggeredLinesHtmlRessource.ContentId = "triggeredLinesChart";
            htmlView.LinkedResources.Add(triggeredLinesHtmlRessource);

            string linesOfDialoguePercentagePath = Path.Combine(Path.GetTempPath(), "linesOfDialoguePercentage.png");
            linesOfDialoguePercentage.SaveChartImageTo(linesOfDialoguePercentagePath);
            LinkedResource linesOfDialoguePercentageHtmlRessource = new LinkedResource(linesOfDialoguePercentagePath);
            linesOfDialoguePercentageHtmlRessource.ContentId = "linesOfDialoguePercentage";
            htmlView.LinkedResources.Add(linesOfDialoguePercentageHtmlRessource);

            reportEmail.AlternateViews.Add(htmlView);
            reportEmail.AlternateViews.Add(textView);

        }

        private List<string> getReportSummaryLines(ReportData data)
        {
            List<string> reportSummaryLines = new List<string>();
            reportSummaryLines.Add("Latest build " + data.BuildVersion + " (Speech CL " + data.SpeechDataCL + ").");
            reportSummaryLines.Add("");
            reportSummaryLines.Add("This report is based on " + data.NumSessionLogs +
                                   " session logs representing a total of " + data.SessionPlayingTime.TotalHours+ " hours (" +
                                   data.SessionPlayingTime.Days + " days) of playing time.");
            reportSummaryLines.Add("");
            reportSummaryLines.Add("Lines of dialogue triggered: " + data.DialogueLinesTriggered + " (" +
                                   (data.DialogueLinesTriggered / (double)data.TotalBuildDialogueLines).ToString("P2") +
                                   " of built speech waves)");
            reportSummaryLines.Add("Lines of placeholder dialogue triggered: " + data.PlaceholderLinesTriggered);
            reportSummaryLines.Add("Missing lines triggered from script: " + data.MissingLinesTriggered);
            reportSummaryLines.Add("Number of voices assigned: " + data.NumVoicesAssigned + " (" +
                                   (data.NumVoicesAssigned / (double)data.NumVoicesWithBuiltSpeech).ToString("P2") +
                                   " of voices with built speech; " +
                                   (data.NumVoicesAssigned / (double)data.NumVoicesAssingedToPVGs).ToString("P2") +
                                   " of voices assigned to PVGs)");
            reportSummaryLines.Add("Percentage of voices triggered: " +
                                   (data.NumVoicesTriggered / (double)data.TotalVoices).ToString("P2"));

            reportSummaryLines.Add("");
            return reportSummaryLines;
        }

        private List<string> getRepeatingTimeLines(ReportData data)
        {
            List<string> repeatingTimeLines = new List<string>();
            
            repeatingTimeLines.Add("");
            repeatingTimeLines.Add("Top "+data.TopRepeatingContexts.Count+" repeating contexts by Averange Repeat Time");
            foreach(ReportData.RepeatingEntry entry in data.TopRepeatingContexts) repeatingTimeLines.Add(entry.Name+" - "+entry.RepeatTime.ToString(@"hh\:mm\:ss"));
            repeatingTimeLines.Add("");
            repeatingTimeLines.Add("Top "+data.TopRepeatingVariations.Count+" repeating variations by Averange Repeat Time");
            foreach (ReportData.RepeatingEntry entry in data.TopRepeatingVariations) repeatingTimeLines.Add(entry.Name + " - " + entry.RepeatTime.ToString(@"hh\:mm\:ss"));
            repeatingTimeLines.Add("");
            foreach (ReportData.RepeatingVariationsPerVoice variationsPerVoice in data.TopRepeatingVariationsPerVoiceList)
            {
                repeatingTimeLines.Add("Top "+variationsPerVoice.TopRepeatingVariations.Count+" repeating variations by Averange Repeat Time ("+variationsPerVoice.Voice+")");
                foreach(ReportData.RepeatingEntry entry in variationsPerVoice.TopRepeatingVariations) repeatingTimeLines.Add(entry.Name+" - "+entry.RepeatTime.ToString(@"hh\:mm\:ss"));
            }

            return repeatingTimeLines;
        }

        public void Send(string[] mailAddresses, string sender, string smtp)
        {
            reportEmail.From = new System.Net.Mail.MailAddress(sender);
            foreach (string recipient in mailAddresses) reportEmail.To.Add(recipient);
            
            using (SmtpClient smtpClient = new SmtpClient(smtp) { Credentials = CredentialCache.DefaultNetworkCredentials })
            {
                smtpClient.Send(reportEmail);
            }
        }
    }
}
