﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.Reader;
using SpeechAnalyticsModel;

namespace LogImportService
{
    public class LogFileReader
    {
        public LogFileReader(BinaryReader logFileReader)
        {
            ushort version = logFileReader.ReadUShort();
            uint dateTimeSessionStarted = logFileReader.ReadUInt();
            uint consoleIdentifier = logFileReader.ReadUInt();
            uint speechDataCL = logFileReader.ReadUInt();
            uint platform = logFileReader.ReadUInt();
            uint buildType = logFileReader.ReadUInt();
            uint commandLineStringLength = logFileReader.ReadUInt();
            uint buildVersionStringLength = logFileReader.ReadUInt();
            uint gamerTagStringLength = logFileReader.ReadUInt();
            string commandLine = readStringFromStream(commandLineStringLength, logFileReader.BinReader);
            string buildVersion = readStringFromStream(buildVersionStringLength, logFileReader.BinReader);
            string gamerTag = readStringFromStream(gamerTagStringLength, logFileReader.BinReader);

            LogFile = new LogFile
            {
                FileFormatVersionID = version,
                DateTimeSessionStarted = new DateTime(dateTimeSessionStarted),
                ConsoleID = consoleIdentifier,
                SpeechDataCL = speechDataCL,
                Platform = platform,
                BuildType = buildType,
                BuildVersion = buildVersion,
                CommandLine = commandLine,
                Gamertag = gamerTag,
            };

            SpeechLogEntries = new List<SpeechLogEntry>();
            VoiceAssignments = new List<VoiceAssignment>();

            while (logFileReader.BinReader.BaseStream.Position < logFileReader.BinReader.BaseStream.Length)
            {
                ushort recordTypeId = logFileReader.ReadUShort();
                switch (recordTypeId)
                {
                    case 0:
                        uint prepareTime = logFileReader.ReadUInt();
                        uint playTime = logFileReader.ReadUInt();
                        uint modelNameHash = logFileReader.ReadUInt();
                        uint voiceNameHash = logFileReader.ReadUInt();
                        uint requestContextHash = logFileReader.ReadUInt();
                        uint contextHash = logFileReader.ReadUInt();
                        ushort prepareDuration = logFileReader.ReadUShort();
                        ushort distanceFromMic = logFileReader.ReadUShort();
                        ushort angleToMic = logFileReader.ReadUShort();
                        byte variationNumber = logFileReader.ReadByte();
                        byte speechType = logFileReader.ReadByte();
                        byte audioType = logFileReader.ReadByte();
                        byte audibility = logFileReader.ReadByte();
                        byte stopReason = logFileReader.ReadByte();
                        byte percentagePlayed = logFileReader.ReadByte();
                        byte startOffset = logFileReader.ReadByte();
                        sbyte gameLoudnessRMS = logFileReader.ReadSByte();
                        sbyte gameLoudnessLUFS = logFileReader.ReadSByte();
                        byte flags = logFileReader.ReadByte();
                        bool placeholder = getBit(flags, 1);
                        bool debugTriggered = getBit(flags, 2);
                        bool succeeded = getBit(flags, 3);
                        bool hasLipsync = getBit(flags, 4);
                        bool hasGestures = getBit(flags, 5);
                        bool wasForced = getBit(flags, 6);
                        bool wasBackupContext = getBit(flags, 7);
                        bool wasContextModifiedBySelector = getBit(flags, 8);

                        SpeechLogEntry speechLogEntry = new SpeechLogEntry
                        {
                            PrepareTime = prepareTime,
                            PlayTime = playTime,
                            ModelNameHash = modelNameHash,
                            VoiceNameHash = voiceNameHash,
                            RequestedContextHash = requestContextHash,
                            ContextHash = contextHash,
                            PrepareDuration = prepareDuration,
                            DistanceFromMic = distanceFromMic,
                            AngleToMic = angleToMic,
                            VariationNumber = variationNumber,
                            SpeechType = speechType,
                            AudioType = audioType,
                            Audibility = audibility,
                            StopReason = stopReason,
                            PercentagePlayed = percentagePlayed,
                            StartOffset = startOffset,
                            GameLoudnessLevelRMS = gameLoudnessRMS,
                            GameLoudnessLevelLUFS = gameLoudnessLUFS,
                            Placeholder = placeholder,
                            DebugTriggered = debugTriggered,
                            Succeeded = succeeded,
                            HasLipsync = hasLipsync,
                            HasGestures = hasGestures,
                            WasForced = wasForced,
                            WasBackupContext = wasBackupContext,
                            WasContextModifiedBySelector = wasContextModifiedBySelector,
                            LogFile = this.LogFile
                        };
                        SpeechLogEntries.Add(speechLogEntry);
                        break;
                    case 1:
                        VoiceAssignment voiceAssignment = new VoiceAssignment
                        {
                            PedModelNameHash = logFileReader.ReadUInt(),
                            R2PVGNameHash = logFileReader.ReadUInt(),
                            PVGNameHash =  logFileReader.ReadUInt(),
                            VoiceNameHash = logFileReader.ReadUInt(),
                            ContextHash = logFileReader.ReadUInt(),
                            DistanceFromMic = logFileReader.ReadUShort(),
                            AngleToMic = logFileReader.ReadUShort(),
                            PositionX = logFileReader.ReadShort(),
                            PositionY = logFileReader.ReadShort(),
                            PositionZ = logFileReader.ReadShort(),
                            Ethnicity = logFileReader.ReadByte(),
                            EthnicitySource = logFileReader.ReadByte(),
                            PedVoiceGroupSource = logFileReader.ReadByte(),
                            PedVoiceSource = logFileReader.ReadByte(),
                            LogFile = this.LogFile
                        };
                        VoiceAssignments.Add(voiceAssignment);
                        break;
                    default:
                        throw new Exception("Record type ID "+recordTypeId+" unknown");
                }
            }
        }

         private string readStringFromStream(uint length, System.IO.BinaryReader stream)
         {
             List<byte> data = new List<byte>();
             for (int i = 0; i < length; i++)
             {
                 byte b = stream.ReadByte();
                 data.Add(b);
             }

             return Encoding.ASCII.GetString(data.ToArray());
         }

         private bool getBit(byte b, int bitNumber)
         {
             var bit = (b & (1 << bitNumber - 1)) != 0;
             return bit;
         }

        public LogFile LogFile;
        public List<VoiceAssignment> VoiceAssignments;
        public List<SpeechLogEntry> SpeechLogEntries;
    }
}
