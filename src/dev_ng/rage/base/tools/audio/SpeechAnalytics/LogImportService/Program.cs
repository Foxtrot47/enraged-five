﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NLog;
using SpeechAnalyticsModel;
using BinaryReader = rage.ToolLib.Reader.BinaryReader;
using System.Configuration;

namespace LogImportService
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            string logDir = ConfigurationManager.AppSettings["LogDir"];
            if(logDir==null || !Directory.Exists(logDir)) throw new Exception("No valid configuration for LogDir variable found!");
            string processedLogDir = ConfigurationManager.AppSettings["ProcessedLogDir"];
            if (processedLogDir == null || !Directory.Exists(processedLogDir)) throw new Exception("No valid configuration for ProcessedLogDir variable found!");
            string errorLogDir = ConfigurationManager.AppSettings["ErrorLogDir"];
            if (errorLogDir == null || !Directory.Exists(errorLogDir)) throw new Exception("No valid configuration for ErrorLogDir variable found!");

            DateTime lastProcessedLogDirSizeCheck = DateTime.MinValue;
            int maxProcessedLogDirSizeMB = 10;
            if (!int.TryParse(ConfigurationManager.AppSettings["MaxProcessedLogDirSizeMB"], out maxProcessedLogDirSizeMB))
            {
                logger.Warn("No valid configuration for MaxProcessedLogDirSizeMB found, using "+maxProcessedLogDirSizeMB+" MB");
            }

            DateTime lastErrorDirSizeCheck = DateTime.MinValue;
            int maxErrordLogDirSizeMB = 10;
            if (!int.TryParse(ConfigurationManager.AppSettings["MaxErrordLogDirSizeMB"], out maxErrordLogDirSizeMB))
            {
                logger.Warn("No valid configuration for MaxErrordLogDirSizeMB found, using " + maxErrordLogDirSizeMB + " MB");
            }

            using (var dataBaseContext = new speech_analyticsEntities())
            {
                while (true) // service loop
                {
                    foreach (string logFilePath in Directory.GetFiles(logDir))
                    {
                        logger.Info("Processing log file " + Path.GetFileName(logFilePath));
                        using (var dbContextTransaction = dataBaseContext.Database.BeginTransaction())
                        {

                            try
                            {
                                using (
                                    BinaryReader binaryLogFileReader =
                                        new BinaryReader(new FileStream(logFilePath, FileMode.Open), false))
                                {
                                    LogFileReader logFileReader = new LogFileReader(binaryLogFileReader);

                                    dataBaseContext.LogFiles.Add(logFileReader.LogFile);
                                    dataBaseContext.SpeechLogEntries.AddRange(logFileReader.SpeechLogEntries);
                                    dataBaseContext.VoiceAssignments.AddRange(logFileReader.VoiceAssignments);

                                    dataBaseContext.SaveChanges();

                                    dbContextTransaction.Commit();
                                }
                                try
                                {
                                    string destPath = Path.Combine(processedLogDir, Path.GetFileName(logFilePath));
                                    if (File.Exists(destPath))
                                    {
                                        logger.Warn("File " + destPath + " already exists - it will be overwritten");
                                        File.Delete(destPath);
                                    }
                                    File.Move(logFilePath, destPath);
                                    lastProcessedLogDirSizeCheck = CheckDirectorySize(lastProcessedLogDirSizeCheck,
                                        processedLogDir,
                                        maxProcessedLogDirSizeMB);
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex.Message);
                                }

                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                string errorMessage = "";
                                if (ex is System.Data.Entity.Validation.DbEntityValidationException)
                                {
                                    foreach (var validationErrors in ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors)
                                    {
                                        foreach (var validationError in validationErrors.ValidationErrors)
                                        {
                                            if (errorMessage != "") errorMessage += ", ";
                                            errorMessage += validationErrors.Entry.Entity.ToString() +":"+validationError.ErrorMessage;
                                        }
                                    }
                                }
                                if (errorMessage == "") errorMessage = ex.Message;
                                logger.Error("Could not process file " + logDir + ": " + errorMessage);
                                string destPath = Path.Combine(errorLogDir, Path.GetFileName(logFilePath));
                                if (File.Exists(destPath))
                                {
                                    logger.Warn("File "+destPath+" already exists - it will be overwritten");
                                    File.Delete(destPath);
                                }
                                File.Move(logFilePath, destPath);
                                lastErrorDirSizeCheck = CheckDirectorySize(lastErrorDirSizeCheck, errorLogDir, maxErrordLogDirSizeMB);
                            }
                        }
                    }
                    System.IO.FileSystemWatcher watcher = new System.IO.FileSystemWatcher(logDir);
                    watcher.WaitForChanged(WatcherChangeTypes.All);
                }
            }
        }

        private static DateTime CheckDirectorySize(DateTime lastSizeCheck, string directory, int maxSizeInMB)
        {
            if (lastSizeCheck < (DateTime.Now.AddDays(-1)))
            {
                lastSizeCheck = DateTime.Now;
                double size = 0;
                foreach (string file in Directory.GetFiles(directory)) size += (new FileInfo(file)).Length;
                if (size > (maxSizeInMB * Math.Pow(1024, 2)))
                {
                    logger.Error("Directory " + directory + " exceeds " + maxSizeInMB + " MB");
                }
            }
            return lastSizeCheck;
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            logger.Fatal(e.ExceptionObject.ToString());
            Environment.Exit(-1);
        }
    }
}
