using System;
using System.Collections.Generic;
using System.Text;

using audBuildCommon;
using audAssetManagement;
using rage;

using System.IO;
using System.Collections;
using System.Xml;

namespace audspeechprebuild
{
    struct NodePair
    {
        public string NodeType;
        public string NodeName;

        public NodePair(string nodeType, string nodeName)
        {
            NodeType = nodeType;
            NodeName = nodeName;
        }
    }

    class audspeechprebuilder : audPreBuilderBase
    {
        audAssetManager m_AssetManager;
        Dictionary<string,List<string>> m_BankFileLists;
        Dictionary<string, List<string>> m_WaveFileLists;
        XmlDocument m_PendingWaves;
        XmlDocument m_BuiltWaves;
        List<XmlNode> m_NodesToDelete;
        const string m_Tag = "DoNotBuild";
        string m_OutputPath;
        string m_Platform;
        bool m_IsDeferred = false;
        audBuildClient m_BuildClient;
        bool b_AddTags;

        public audspeechprebuilder()
        {
            m_BankFileLists = new Dictionary<string, List<string>>();
            m_WaveFileLists = new Dictionary<string, List<string>>();
            m_NodesToDelete = new List<XmlNode>();
        }

        public override void Build(audAssetManager assetManager, audProjectSettings projectSettings, 
            string buildPlatform, audBuildClient buildClient, XmlDocument pendingWavesXml, 
            XmlDocument builtWavesXml, bool shouldBuildLocally, audBuildComponent buildComponent, 
            bool isDeferredBuild)
        {

            m_AssetManager = assetManager;
            m_PendingWaves = pendingWavesXml;
            m_BuiltWaves = builtWavesXml;
            m_OutputPath = buildComponent.m_OutputPath;
            m_IsDeferred = isDeferredBuild;
            m_Platform = buildPlatform;
            m_BuildClient = buildClient;
            b_AddTags = buildComponent.m_EnabledStrictMode;

            m_BuildClient.ReportProgress(-1, -1, "Starting Speech preBuild Step", true);
          
            if (m_OutputPath == null || m_OutputPath == "")
            {
                throw new audBuildException("Error: No output path specified for the speech pre-builder");
            }

            try
            {
                if (b_AddTags)
                {
                    LoadXML(buildComponent.m_InputPath);
                    ParseBuiltWaves();
                    RemoveNodes();
                    ParsePendingWaves();
                }
                else
                {
                    StripTags(m_PendingWaves.DocumentElement);
                }
            }
            catch (Exception e)
            {
                throw new audBuildException(e.ToString());
            }

            m_BuildClient.ReportProgress(-1, -1, "Speech preBuild step complete", true);
        }

        private static void StripTags(XmlNode node)
        {
            bool removed = false;
            if(node.Name == "Tag" && node.Attributes["name"] !=null && node.Attributes["name"].Value == m_Tag)
            {
                node.ParentNode.RemoveChild(node);
                removed=true;
            }
            if (!removed)
            {
                foreach (XmlNode child in node.ChildNodes)
                {
                    StripTags(child);
                }
            }
        }

        private void RemoveNodes()
        {
            foreach (XmlNode node in m_NodesToDelete)
            {
                node.ParentNode.RemoveChild(node);
            }
        }

        private void ParsePendingWaves()
        {
            foreach (XmlNode node in m_PendingWaves.DocumentElement.ChildNodes)
            {
                string packName = node.Attributes["name"].Value;
                if(m_WaveFileLists.ContainsKey(packName) && (m_IsDeferred ||!HasDeferredTag(packName)))
                {
                    ProcessPendingNodes(node,parseFileList(m_WaveFileLists[packName]));
                }
            }
        }

        public bool HasDeferredTag(string packName)
        {
            bool hasTag = false;

            foreach (XmlNode pack in m_BuiltWaves.DocumentElement.ChildNodes)
            {
                if (pack.Attributes["name"].Value == packName)
                {
                    hasTag = FindTag(pack);
                    break;
                }
            }
            if (!hasTag)
            {
                foreach (XmlNode pack in m_PendingWaves.DocumentElement.ChildNodes)
                {
                    if (pack.Attributes["name"].Value == packName)
                    {
                        hasTag = FindTag(pack);
                        break;
                    }
                }
            }
            return hasTag;
        }


        public static bool FindTag(XmlNode parentNode)
        {
            bool hasTag = false;
            foreach (XmlNode childNode in parentNode.ChildNodes)
            {
                if (childNode.Attributes["name"].Value == "deferredBuild")
                {
                    hasTag = true;
                    break;
                }
            }

            if (hasTag == false)
            {

                foreach (XmlNode childNode in parentNode.ChildNodes)
                {
                    hasTag = FindTag(childNode);
                    if (hasTag == true)
                    {
                        break;
                    }
                }
            }

            return hasTag;
        }


        private void ParseBuiltWaves()
        {
            ParseBuiltWaves(m_BuiltWaves.DocumentElement);
        }

        private void ParseBuiltWaves(XmlNode buildNode)
        {
            foreach (XmlNode pack in m_BuiltWaves.DocumentElement.ChildNodes)
            {
                string packName = pack.Attributes["name"].Value;
                if (m_WaveFileLists.ContainsKey(packName)&& (m_IsDeferred || !HasDeferredTag(packName)))
                {
                    ProcessBuiltNodes(pack,new List<NodePair>(), parseFileList(m_WaveFileLists[packName]));
                }
             }
        }

        private static bool NodeDeletedInPendingWaves(XmlNode builtNode, XmlNode pendingNode, List<NodePair> path)
        {
            if (path.Count == 0)
            {
                foreach (XmlNode child in pendingNode.ChildNodes)
                {
                    if (child.Attributes["name"].Value == builtNode.Attributes["name"].Value &&
                        child.Name == builtNode.Name)
                    {
                        if (child.Attributes["operation"] != null && child.Attributes["operation"].Value == "remove")
                        {
                            XmlNode parent = child.ParentNode;
                            while (parent!=null)
                            {
                                if (parent.Name == "Bank")
                                {
                                    AddRebuildTag(parent);
                                    break;
                                }

                                parent = parent.ParentNode;
                            }
                            pendingNode.RemoveChild(child);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                //node not found
                return false;
            }
            else
            {
                foreach (XmlNode child in pendingNode.ChildNodes)
                {
                    if (child.Attributes["name"].Value == path[0].NodeName &&
                        child.Name == path[0].NodeType)
                    {
                        path.RemoveAt(0);
                        return NodeDeletedInPendingWaves(builtNode, child, path);
                    }
                }
                //node not found
                return false;
            }        
            
        }

        private void RemoveBuiltBank(XmlNode node, List<NodePair> path)
        {
            string filePath = m_OutputPath + "\\" + m_Platform + "\\BUILDDATA\\Sfx\\" + path[0].NodeName + "\\" + node.Attributes["name"].Value;
            //remove bank from sfx
            if (!m_AssetManager.DeleteAsset(m_AssetManager.GetWorkingPath(filePath), filePath, true))
            {
                m_BuildClient.ReportProgress(-1, -1, "Failed to delete " + filePath, true);
                throw new audBuildException("Failed to delete " + filePath);
            }
            m_BuildClient.ReportProgress(-1, -1, "PreBuild deleting " + filePath, true);
        }
        
        private bool ProcessBuiltNodes(XmlNode node, List<NodePair> path, ArrayList fileNames)
        {
            NodePair[] temp = new NodePair[path.Count];
            path.CopyTo(temp);
            List<NodePair> tempPath = new List<NodePair>(temp);

            //check if node is marked for delete in the pending waves. If so delete it here and mark the bank to be
            //rebuilt. If bank is to be deleted delete here but don't mark parent to rebuild as the pack will remain in the pending
            //waves and therefore the RPF for the pack will be rebuilt. 
            if (NodeDeletedInPendingWaves(node, m_PendingWaves.DocumentElement, tempPath))
            {
                if (node.Name == "Bank")
                {
                    RemoveBuiltBank(node, path);
                }
                m_NodesToDelete.Add(node);
                return true;
            }

            //Check Node to see if it is invalid and needs removed from the builtWaves
            else
            {
                //If node is a wave then we need to check it against the list of valid contexts
                if (node.Name == "Wave")
                {
                    string nodeName = node.Attributes["name"].Value;
                    //get name without variation
                    int end = nodeName.LastIndexOf("_");
                    nodeName = nodeName.Substring(0, end);
                    //check list of valid names
                    if (!fileNames.Contains(nodeName))
                    {
                        m_NodesToDelete.Add(node);
                        AddToPending(path, node as XmlElement, m_PendingWaves.DocumentElement as XmlElement);
                        return true;
                    }
                    return false;
                }
                // current node a wave but this ensures that no empty banks etc are built
                else
                {
                    bool removeNode = true;
                    if (node.ChildNodes.Count != 0)
                    {
                        //This ensures that we do not end up with empty banks. If all children are removed (excluding tags)
                        //then the current node should also be removed
                        foreach (XmlNode child in node.ChildNodes)
                        {
                            NodePair[] tempNewPath = new NodePair[path.Count];
                            path.CopyTo(tempNewPath);
                            List<NodePair> newPath = new List<NodePair>(tempNewPath);
                            newPath.Add(new NodePair(node.Name, node.Attributes["name"].Value));
                            if (!ProcessBuiltNodes(child, newPath, fileNames)
                                && child.Name != "Tag")
                            {
                                removeNode = false;
                            }
                        }
                    }
                    else
                    {
                        //Waves may not have children, there should never be a abuilt bank with no children
                        return false;
                    }

                    //  If the node is to be removed from the pending waves we need to add it to the pending waves.
                    //  We need to also check if the node is a bank and delete the corresponding bank in perforce                    
                    if (removeNode)
                    {
                        m_NodesToDelete.Add(node);
                        if (node.Name == "Bank")
                        {
                            string filePath = m_OutputPath + "\\" + m_Platform + "\\BUILDDATA\\Sfx\\" + path[0].NodeName + "\\" + node.Attributes["name"].Value;

                            if (!m_AssetManager.DeleteAsset(m_AssetManager.GetWorkingPath(filePath), filePath, true))
                            {
                                m_BuildClient.ReportProgress(-1, -1, "Failed to delete " + filePath, true);
                                throw new audBuildException("Failed to delete " + filePath);
                            }

                            m_BuildClient.ReportProgress(-1, -1, "PreBuild deleting " + filePath, true);

                        }
                        AddToPending(path, node as XmlElement, m_PendingWaves.DocumentElement as XmlElement);
                        return true;
                    }

                    else
                    {
                        return false;
                    }
                }

            }
        }
    

        private void AddToPending(List<NodePair> path, XmlElement nodeToAdd, XmlNode parentNode)
        {

            string nodeType = "";
            string nodeName ="";
            
            int ancestorsLength = path.Count;
            if (ancestorsLength > 0)
            {
                nodeType = path[0].NodeType;
                nodeName = path[0].NodeName;
            }

            bool found = false;

            foreach (XmlElement node in parentNode.ChildNodes)
            {
                //we are looking for the node to add in the pending waves
                if (node.Attributes["name"].Value == nodeToAdd.Attributes["name"].Value && ancestorsLength == 0)
                {
                    //ensure that the node has an add operation attribute
                    node.SetAttribute("operation", "add");

                    //ensures if bank to be added then it does not contain a rebuild tag
                    RemoveRebuildTag(node);

                    //ensure bank is set to be modified
                    XmlNode parent = parentNode;
                    while (parent!=null)
                    {
                        if (parent.Name == "Bank")
                        {
                            AddRebuildTag(parent);
                            break;
                        }
                        parent = parent.ParentNode;
                    }

                    /// As this function is called from the waves up, banks etc will only be added if all children
                    /// have been added. Therefore we only need to look for missing tag nodes here.
                    foreach(XmlElement builtChild in nodeToAdd.ChildNodes)
                    {
                        bool foundChild = false;
                        foreach (XmlElement pendingChild in node.ChildNodes)
                        {
                            if (pendingChild.Name == builtChild.Name
                                && pendingChild.Attributes["name"].Value == builtChild.Attributes["name"].Value)
                            {
                                foundChild = true;
                            }
                        }
                        //ensures voice tags etc are added
                        if (!foundChild && builtChild.Name == "Tag")
                        {
                            AddNode(node, builtChild);
                        }
                    }
                    found = true;
                }
                //found ancestor
                else if (node.Attributes["name"].Value == nodeName)
                {
                    found = true;
                    //find next node          
                    path.RemoveAt(0);
                    AddToPending(path, nodeToAdd, node);
                }
            }
            //if not found create the node
            if (!found)
            {
                //create child
                if (ancestorsLength == 0)
                {
                    //set opertaion to modify so parent is rebuilt
                    //no need to set donotbuild tag here as this is added when pending waves is parsed.
                    XmlNode parent = parentNode;
                    while (parent!=null)
                    {
                        if (parent.Name == "Bank" && !HasRebuildTag(parent))
                        {
                            AddRebuildTag(parent);
                            break;
                        }
                        parent = parent.ParentNode;
                    }
                  AddNode(parentNode,nodeToAdd);
                }
                //create ancestor
                else
                {
                    XmlElement newNode = m_PendingWaves.CreateElement(nodeType);
                    newNode.SetAttribute("name", nodeName);
                    parentNode.AppendChild(newNode);
                    //find next node
                    path.RemoveAt(0);
                    AddToPending(path, nodeToAdd, newNode);
                }              

            }
        }

        static void RemoveRebuildTag(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name == "Tag" && child.Attributes["name"].Value == "rebuild")
                {
                    node.RemoveChild(child);
                    return ;
                }
            }
        }

        private static void AddRebuildTag(XmlNode node)
        {
            if (!HasRebuildTag(node))
            {
                XmlElement rebuildTag = node.OwnerDocument.CreateElement("Tag");
                rebuildTag.SetAttribute("name", "rebuild");
                rebuildTag.SetAttribute("operation", "add");
                node.AppendChild(rebuildTag);
             }
        }

        static bool HasRebuildTag(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name == "Tag" && child.Attributes["name"].Value == "rebuild")
                {
                    return true;
                }
            }
            return false;
        }

        private static void AddNode(XmlNode parent, XmlNode child)
        {
            ///This function should only be called for waves as any wavefolders
            ///and banks should have already been added
            
            XmlElement newNode = parent.OwnerDocument.CreateElement(child.Name);
            newNode.SetAttribute("operation", "add");
            newNode.SetAttribute("name", child.Attributes["name"].Value);
            if (child.Name == "Tag")
            {
                newNode.SetAttribute("value", child.Attributes["value"].Value);
            }

            foreach (XmlNode childNode in child.ChildNodes)
            {
                if (childNode.Name != "Marker")
                {
                    AddNode(newNode, childNode);
                }
            }

            parent.AppendChild(newNode);
        }

        //returns a bool indicating if a tag has been added
        private static bool ProcessPendingNodes(XmlNode node, ArrayList fileNames)
        {

            if (node.Name == "Wave")
            {
                string nodeName = node.Attributes["name"].Value;
                //get name without variation
                if (nodeName.Contains("_"))
                {
                    int end = nodeName.LastIndexOf("_");
                    nodeName = nodeName.Substring(0, end);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine(nodeName);
                }
                if (fileNames.Contains(nodeName))
                {
                    addBuildTag(node, false);
                    return false;
                }
                else
                {
                    addBuildTag(node, true);
                    return true;
                }
            }

            else
            {
                bool addTag = true;
            
                if (node.ChildNodes.Count > 0)
                {
                    foreach (XmlNode child in node.ChildNodes)
                    {
                        if(!ProcessPendingNodes(child, fileNames) 
                            && child.Name!= "Tag")
                        {
                            addTag = false;
                        }
                    }
                }
                else
                {
                    addTag = false;
                }

                if (addTag && node.Attributes["operation"] != null 
                    && node.Attributes["operation"].Value == "add")
                {
                    //ensure node has a tag
                    addBuildTag(node, true);
                    return true;
                }
                else
                {
                    //ensure a tag does not exist
                    addBuildTag(node, false);
                    return false;
                }
            }
        }

        public static void addBuildTag(XmlNode node, bool shouldHaveTag)
        {
            if(!shouldHaveTag)
            {
                //Remove tag node
                foreach (XmlNode child in node.ChildNodes)
                {
                    if (child.Name == "Tag" && child.Attributes["name"].Value == m_Tag)
                    {
                        node.RemoveChild(child);
                        break;
                    }
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Adding tag to " + node.Attributes["name"].Value);
                //Add tag node if one is needed
                if (node.ChildNodes.Count == 0)
                {
                    XmlElement tag = node.OwnerDocument.CreateElement("Tag");
                    tag.SetAttribute("name", m_Tag);
                    tag.SetAttribute("operation", "add");
                    node.AppendChild(tag);
                }
                else
                {
                    bool hasTag = false;
                    foreach (XmlNode child in node.ChildNodes)
                    {

                        if (child.Name == "Tag" && child.Attributes["name"].Value == m_Tag)
                        {
                            hasTag = true;
                            break;
                        }
                    }
                    if (!hasTag)
                    {
                        XmlElement tag = node.OwnerDocument.CreateElement("Tag");
                        tag.SetAttribute("name", m_Tag);
                        tag.SetAttribute("operation", "add");
                        node.AppendChild(tag);
                    }
                }

            }
        }


        public void LoadXML(string fileName)
        {
            m_AssetManager.GetLatest(fileName);
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                string packName = node.Attributes["packName"].Value.ToUpper();
                List<string> files = new List<string>();
                foreach (XmlNode child in node.ChildNodes)
                {
                    files.Add(child.InnerText);
                }
                m_WaveFileLists.Add(packName, files);
            }
        }

        public ArrayList parseFileList(List<string> fileNames)
        {
            ArrayList result = new ArrayList();
            try
            {
                foreach (string fileName in fileNames)
                {
                    if (Directory.Exists(fileName))
                    {
                        DirectoryInfo dir = new DirectoryInfo(fileName);
                        foreach (FileInfo file in dir.GetFiles())
                        {
                            AddFileContentsToList(file.FullName, result);
                        }
                    }
                    else
                    {
                        AddFileContentsToList(fileName, result);
                    }

                }
            }
            catch (Exception e)
            {
                throw new audBuildException(e.ToString());
            }


            return result;
        }
    

        public void AddFileContentsToList(string fileName, ArrayList result)
        {
            m_AssetManager.GetLatest(fileName);
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            while (!sr.EndOfStream)
            {
                result.Add(sr.ReadLine().ToUpper());
            }
            sr.Close();
            fs.Close();
        }



    }
}
