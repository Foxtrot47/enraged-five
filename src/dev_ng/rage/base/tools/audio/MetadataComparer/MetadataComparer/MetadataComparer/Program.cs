﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace MetadataComparer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// Metadata comparer.
    /// </summary>
    public class Program
    {
        private static Dictionary<string, IList<string>> objs = new Dictionary<string, IList<string>>(); 

        /// <summary>
        /// Main entry point.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            var outputPath = Path.Combine(Path.GetTempPath(), "MetadataComparerResults.txt");
            var writer = new StreamWriter(outputPath);

            var metadataObjectsPaths = new List<string>
            {
                "X:\\gta5\\audio\\dev\\assets\\Objects\\Core\\Audio\\GAMEOBJECTS",
            };

            foreach (var objsPath in metadataObjectsPaths)
            {
                var filePaths = Directory.GetFiles(objsPath, "*.xml", SearchOption.AllDirectories);
                foreach (var filePath in filePaths)
                {
                    try
                    {
                        var doc = XDocument.Load(filePath);
                        foreach (var childElem in doc.Element("Objects").Elements())
                        {
                            var nameAttr = childElem.Attribute("name");
                            if (null == nameAttr || string.IsNullOrEmpty(nameAttr.Value))
                            {
                                Console.WriteLine("Object with no name attribute detected in " + filePath);
                                continue;
                            }

                            var name = nameAttr.Value;
                            
                            // Remove name attribute from object
                            nameAttr.Remove();

                            var objStr = childElem.ToString();
                            if (!objs.ContainsKey(objStr))
                            {
                                objs[objStr] = new List<string>();
                            }

                            objs[objStr].Add(name);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Failed to load script data from " + filePath + ": " + ex.Message);
                        return;
                    }
                }

                foreach (var obj in objs.Where(obj => obj.Value.Count > 1))
                {
                    // Display details of duplicates
                    Console.WriteLine(string.Join(Environment.NewLine, obj.Value));
                    Console.WriteLine();

                    // Write details to file
                    writer.WriteLine(string.Join(Environment.NewLine, obj.Value));
                    writer.WriteLine();
                }
            }

            Console.WriteLine("Results saved to " + outputPath);

            writer.Close();
            Console.ReadKey();
        }
    }
}
