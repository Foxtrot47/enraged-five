//
// tools/audbuildcommon/buildexception.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef BUILD_EXCEPTION_H
#define BUILD_EXCEPTION_H

using namespace System;

namespace audBuildCommon {

//
// PURPOSE
//  An application-specific Exception implementation that carries debug information suitable for
//	writing to the event log and passing to the client via a socket connection.
//
public __gc class audBuildException : public ApplicationException
{
public:
	//
	// PURPOSE
	//  Formats the specified error message ready for event logging and communication to the Client.
	// PARAMS
	//  errorMsg	- The debug error message associated with the exception. Can contain a dump of
	//					the .NET exception string if available.
	//
	audBuildException(String *errorMsg)
	{
		m_logErrorMsg = errorMsg;
		m_clientErrorMsg = String::Concat(S"ERROR \"", errorMsg, S"\"\n");
	}

	String *m_logErrorMsg, *m_clientErrorMsg;
};

} // namespace audBuildCommon

#endif // BUILD_EXCEPTION_H
