//
// tools/audbuildcommon/buildclient.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//
#include "stdafx.h"

#include "buildclient.h"

#include "buildexception.h"

using namespace System::Text;

const int g_MaxClientResponseBytes	= 1024;
const int g_ClientPollIntervalMs	= 500;

namespace audBuildCommon {

audBuildClient::audBuildClient(NetworkStream *clientStream, EventLog *eventLog) :
	m_ClientStream(clientStream), m_EventLog(eventLog), m_IsListeningToClient(false)
{
	//Allocate memory for buffer to receive client responses.
	m_ReceivedBytes = new Byte[g_MaxClientResponseBytes];
}

void audBuildClient::SendMessage(String *messageStr)
{
	try
	{
		if(m_ClientStream)
		{
			Byte messageBytes[] = Text::Encoding::ASCII->GetBytes(messageStr);
			m_ClientStream->Write(messageBytes, 0, messageBytes->Length);
		}
	}
	catch(Exception *) {} //These are optional info messages so don't worry about exceptions.
}

String *audBuildClient::WaitForReponse(void)
{
	bool gotResponse=false;
	int bytesRead, totalBytesRead=0;
	String *receivedString=0;

	try
	{
		m_IsListeningToClient = true;

		if(m_ClientStream)
		{
			//Clear receive buffer.
			System::Array::Clear(m_ReceivedBytes, 0, g_MaxClientResponseBytes);

			m_EventLog->WriteEntry(S"Waiting for user response");

			do
			{
				//Blocks until at least one byte is read.
				bytesRead = m_ClientStream->Read(m_ReceivedBytes, totalBytesRead,
					g_MaxClientResponseBytes);

				if(bytesRead > 0)
				{
					//Convert received bytes to a string.
					receivedString = Encoding::UTF8->GetString(m_ReceivedBytes);
					//Check for terminating newline character.
					if(receivedString->Trim()->IndexOf(L'\n') != -1)
					{
						gotResponse = true;
					}

					totalBytesRead += bytesRead;
				}
				else
				{
					//Client has disconnected, so throw a useful exception via this function's
					//catch wrapper.
					throw(new Exception());
				}

			} while(!gotResponse);
		}

		m_IsListeningToClient = false;

		return receivedString;
	}
	catch(Exception *)
	{
		m_IsListeningToClient = false;
		audBuildException *exception = new audBuildException(
			S"Invalid or no response from client");
		throw(exception);
	}
}

void audBuildClient::ReportProgress(int percentageComplete, int totalEstimatedTimeRemainingSecs,
	String *progressMessage, bool shouldLogEvent)
{
	//Send progress message to client.
	String *message = String::Format(S"PROGRESS {0,3} {1,5} {2}\n", __box(percentageComplete),
		__box(totalEstimatedTimeRemainingSecs), progressMessage);
	SendMessage(message);

	if(shouldLogEvent)
	{
		//Write to event log.
		m_EventLog->WriteEntry(progressMessage, EventLogEntryType::Information);
	}
}

} // namespace audBuildCommon
