//
// tools/audbuildcommon/bytearraylist.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//
#include "stdafx.h"

#include "bytearraylist.h"

namespace audBuildCommon {


audByteArrayList::audByteArrayList()
{
	m_Array = 0;
	m_LengthBytes = 0;
	m_Count = 0;
}

audByteArrayList::audByteArrayList(Array *array)
{
	m_Array = static_cast<Byte __gc[]>(array->Clone());
	m_Count = m_Array->get_Length();
}

audByteArrayList::~audByteArrayList()
{
	Clear();
}

int audByteArrayList::get_Count(void)
{
	return m_Count;
}

void audByteArrayList::AddRange(audByteArrayList *arrayList)
{
	AddRange(static_cast<Array *>(arrayList->ToArray(__typeof(Byte))));
}

void audByteArrayList::AddRange(Array *array)
{
	int lengthBytesRequired = m_Count + array->get_Count();

	if(m_LengthBytes < lengthBytesRequired)
	{
		//(Re)allocate twice the amount we need to provide some headroom to grow in.
		m_LengthBytes = lengthBytesRequired << 1;

		Byte newArray __gc[] = new Byte[m_LengthBytes];

		if(m_Array != 0)
		{
			Array::Copy(m_Array, newArray, m_Count);
		}

    	m_Array = newArray;
	}

	Array::Copy(array, 0, m_Array, m_Count, array->get_Count());
	m_Count = lengthBytesRequired;
}

audByteArrayList *audByteArrayList::GetRange(int index, int count)
{
	Byte newArray __gc[] = new Byte[count];

	if(m_Array != 0)
	{
		Array::Copy(m_Array, index, newArray, 0, count);
	}

	return new audByteArrayList(newArray);
}

void audByteArrayList::SetRange(int index, Array *array)
{
	if(m_Array != 0)
	{
		array->CopyTo(m_Array, index);
	}
}

void audByteArrayList::RemoveRange(int index, int count)
{
	if(m_Array == 0)
	{
		return;
	}

	Byte copyArray __gc[] = static_cast<Byte __gc[]>(m_Array->Clone());

	if(index + count < m_Count)
	{
		Array::Copy(copyArray, index + count, m_Array, index, m_Count - count - index);
	}

	m_Count -= count;
}

Byte audByteArrayList::ToArray(Type *type) __gc[]
{
	(type); //Ignore type - only included for interface consistency with ArrayList.

	if(m_Array == 0)
	{
		return 0;
	}
	else
	{
		Byte newArray __gc[] = new Byte[m_Count];
		Array::Copy(m_Array, 0, newArray, 0, m_Count);

		return newArray;
	}
}

void audByteArrayList::Clear(void)
{
	m_Array = 0;
	m_LengthBytes = 0;
	m_Count = 0;
}

} // namespace audBuildCommon
