//
// tools/audbuildcommon/bytearraylist.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//
#ifndef BYTE_ARRAY_LIST_H
#define BYTE_ARRAY_LIST_H

using namespace System;

namespace audBuildCommon {

//
// PURPOSE
//  A custom replacement for the Byte array usage of the .NET managed ArrayList class. Emulates just enough of the
//	ArrayList interface to enable drop-in replacement within the Wave Builder tool.
// NOTES
//	See .NET ArrayList documentation.
//
public __gc class audByteArrayList
{
public:
	audByteArrayList();
	audByteArrayList(Array *array);
	~audByteArrayList();

	int get_Count(void);

	void AddRange(audByteArrayList *arrayList);
	void AddRange(Array *array);
	audByteArrayList *GetRange(int index, int count);
	void SetRange(int index, Array *array);
	void RemoveRange(int index, int count);

	Byte ToArray(Type *type) __gc[];

	void Clear(void);

private:
	Byte m_Array __gc[];
	int m_LengthBytes;
	int m_Count;
};

} // namespace audBuildCommon

#endif // BYTE_ARRAY_LIST_H
