//
// tools/audbuildcommon/builderbase.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef BUILDER_BASE_H
#define BUILDER_BASE_H

using namespace audAssetManagement;
using namespace rage;
using namespace System;
using namespace System::Xml;

namespace audBuildCommon
{
	//
	// PURPOSE
	//  Defines the interface that the Audio Build Manager uses to execute a pre-build step.
	// SEE ALSO
	//	audBuildManager
	//
	public __gc class audPreBuilderBase abstract
	{
	public:
		// PURPOSE
		//  Performs a pre-build step for the Wave assets listed in the Pending Waves List.
		// PARAMS
		//  assetManager		- The Asset Manager instance to be used for accessing the requisite audio assets.
		//  projectSettings		- Defines the project for which audio assets are to be built.
		//	buildPlatform		- The name of the target build platform.
		//	buildClient			- An encapsulation of the connection to the build client.
		//	pendingWavesXml		- An XML document containing a list of all Waves assets to be built (with associated
		//							metadata tags.)
		//	shouldBuildLocally	- If true, the build is to be performed locally, which may require special handling.
		//	buildComponent		- An encapsulation of this build step that includes custom settings.
		//
		virtual void Build(audAssetManager *assetManager, audProjectSettings *projectSettings, String *buildPlatform,
			audBuildClient *buildClient, XmlDocument *pendingWavesXml, XmlDocument *builtWaves, bool shouldBuildLocally,
			audBuildComponent *buildComponent, bool isDeferredBuild) = 0;
	};
	//
	// PURPOSE
	//  Defines the interface that the Audio Build Manager uses to execute a build step.
	// SEE ALSO
	//	audBuildManager
	//
	public __gc class audBuilderBase abstract
	{
	public:
		// PURPOSE
		//  Performs a build step for the Wave assets listed in the Pending Waves List, recording the built assets in the
		//	Built Waves List.
		// PARAMS
		//  assetManager		- The Asset Manager instance to be used for accessing the requisite audio assets.
		//  projectSettings		- Defines the project for which audio assets are to be built.
		//	buildPlatform		- The name of the target build platform.
		//	buildClient			- An encapsulation of the connection to the build client.
		//	pendingWavesXml		- An XML document containing a list of all Waves assets to be built (with associated
		//							metadata tags.)
		//  builtWavesXml		- An XML document containing a list of all currently built Waves assets (with associated
		//							metadata tags.)
		//	shouldBuildLocally	- If true, the build is to be performed locally, which may require special handling.
		//	buildComponent		- An encapsulation of this build step that includes custom settings.
		//
		virtual void Build(audAssetManager *assetManager, audProjectSettings *projectSettings, String *buildPlatform,
			audBuildClient *buildClient, XmlDocument *pendingWavesXml, XmlDocument *builtWavesXml, bool shouldBuildLocally,
			audBuildComponent *buildComponent, bool isDeferredBuild) = 0;
	};
	//
	// PURPOSE
	//  Defines the interface that the Audio Build Manager uses to execute a post-build step.
	// SEE ALSO
	//	audBuildManager
	//
	public __gc class audPostBuilderBase abstract
	{
	public:
		// PURPOSE
		//  Performs a post-build step for the Wave assets listed in the Built Waves List.
		// PARAMS
		//  assetManager		- The Asset Manager instance to be used for accessing the requisite audio assets.
		//  projectSettings		- Defines the project for which audio assets are to be built.
		//	buildPlatform		- The name of the target build platform.
		//	buildClient			- An encapsulation of the connection to the build client.
		//  builtWavesXml		- An XML document containing a list of all currently built Waves assets (with associated
		//							metadata tags.)
		//  pendingWavesXml     - An XML document containing a list of all pending Wave assets (with associated 
		//                          metadata tags.)
		//	shouldBuildLocally	- If true, the build is to be performed locally, which may require special handling.
		//	buildComponent		- An encapsulation of this build step that includes custom settings.
		//
		//  isDeferredBuild		- Used to determine whether we are performing a deferred build. Deferred build used to speed up build 

		virtual void Build(audAssetManager *assetManager, audProjectSettings *projectSettings, String *buildPlatform,
			audBuildClient *buildClient, XmlDocument *builtWavesXml, XmlDocument *pendingWavesXml, bool shouldBuildLocally,
			audBuildComponent *buildComponent, bool isDeferredBuild) = 0;
	};

} // namespace audBuildCommon

#endif // BUILDER_BASE_H
