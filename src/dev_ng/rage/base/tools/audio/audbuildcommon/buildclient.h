//
// tools/audbuildcommon/buildclient.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef BUILD_CLIENT_H
#define BUILD_CLIENT_H

using namespace System;
using namespace System::Diagnostics;
using namespace System::Net::Sockets;

namespace audBuildCommon {

//
// PURPOSE
//  Encapsulates the connection to, and aids communication with, a build client.
// SEE ALSO
//  audBuildThread, audBuildException
//
public __gc class audBuildClient
{
public:
	//
	// PURPOSE
	//  Initializes class and allocates memory for buffer to receive client responses.
	// PARAMS
	//  clientStream	- A NetworkStream associated with the build client to be wrapped.
	//	eventLog		- The application event log.
	//
	audBuildClient(NetworkStream *clientStream, EventLog *eventLog);
	//
	// PURPOSE
	//  Sends a specified text message to the build client via the internal client stream.
	// PARAMS
	//  messageStr	- The message to be sent to the client.
	//
	void SendMessage(String *messageStr);
	//
	// PURPOSE
	//  Waits indefinitely for a new line ('\n') terminated text message from the build client via
	//	the internal client stream. The blocking read can be interrupted by a socket exception.
	// RETURNS
	//	The message received from the client (an audBuildException is thrown if an error
	//	occurs.)
	//
	String *WaitForReponse(void);
	//
	// PURPOSE
	//  A helper function that passes a specified progress message to the client along with a
	//	specified completion percentage. The message is also added to the application event log if
	//	requested.
	// PARAMS
	//  percentageComplete				- The percentage of the build process completed. Pass -1 if
	//										a percentage is not derivable.
	//	totalEstimatedTimeRemainingSecs	- An estimate of the total build time remaining, in seconds.
	//										Pass -1 if a time is not derivable.
	//	progressMessage					- A human-readable progress message to be passed to the
	//										client.
	//	shouldLogEvent					- Specifies whether or not the progress message should be
	//										added to the event log.
	// SEE ALSO
	//  SendMessage
	//
	void ReportProgress(int percentageComplete, int totalEstimatedTimeRemainingSecs,
		String *progressMessage, bool shouldLogEvent);

	bool m_IsListeningToClient;

private:
	Byte m_ReceivedBytes[];
	NetworkStream *m_ClientStream;
	EventLog *m_EventLog;
};

} // namespace audBuildCommon

#endif // BUILD_CLIENT_H
