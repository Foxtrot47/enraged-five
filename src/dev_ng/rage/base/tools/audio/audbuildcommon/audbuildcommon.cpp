//
// tools/audbuildcommon/audbuildcommon.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

// This is the main DLL file.

#include "stdafx.h"

#include "audbuildcommon.h"

#include "buildclient.h"
#include "builderbase.h"
#include "platformspecific.h"
