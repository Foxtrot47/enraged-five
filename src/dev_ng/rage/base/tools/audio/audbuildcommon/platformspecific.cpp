//
// tools/audbuildcommon/platformspecific.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//
#include "stdafx.h"

#include "platformspecific.h"

#include "buildexception.h"

namespace audBuildCommon {

union FLOATCONVERT
{
	Single floating;
	UInt32 fixed;
};

void audPlatformSpecific::InitClass(String *buildPlatform)
{
	sm_BigEndianPlatforms = new bool __gc[NUM_BUILD_PLATFORMS];

	sm_BigEndianPlatforms[BUILD_PLATFORM_PC] = false;
	sm_BigEndianPlatforms[BUILD_PLATFORM_XENON] = true;
	sm_BigEndianPlatforms[BUILD_PLATFORM_PS3] = true;
	//Add new platforms here.

	if(buildPlatform->StartsWith(S"PC"))
	{
		sm_BuildPlatform = BUILD_PLATFORM_PC;
	}
	else if(buildPlatform->StartsWith(S"XENON") || buildPlatform->StartsWith(S"X360") || buildPlatform->Equals(S"XBOX360"))
	{
		sm_BuildPlatform = BUILD_PLATFORM_XENON;
	}
	else if(buildPlatform->StartsWith(S"PS3"))
	{
		sm_BuildPlatform = BUILD_PLATFORM_PS3;
	}
	//Add new platforms here.
	else
	{
		audBuildException *exception = new audBuildException(String::Concat(S"Invalid build platform - ",
			buildPlatform));
		throw(exception);
	}
}

UInt16 audPlatformSpecific::FixEndian(UInt16 value)
{
	if(sm_BigEndianPlatforms[sm_BuildPlatform])
	{
		value = FlipEndian(value);
	}

	return value;
}

UInt32 audPlatformSpecific::FixEndian(UInt32 value)
{
	if(sm_BigEndianPlatforms[sm_BuildPlatform])
	{
		value = FlipEndian(value);
	}

	return value;
}

Single audPlatformSpecific::FixEndian(Single value)
{
	if(sm_BigEndianPlatforms[sm_BuildPlatform])
	{
		value = FlipEndian(value);
	}

	return value;
}

unsigned __int64 audPlatformSpecific::FixEndian(unsigned __int64 value)
{
	if(sm_BigEndianPlatforms[sm_BuildPlatform])
	{
		value = FlipEndian(value);
	}

	return value;
}

UInt16 audPlatformSpecific::FlipEndian(UInt16 value)
{
	Byte b1, b2;

	b1 = (Byte)(value & 255);
	b2 = (Byte)((value >> 8) & 255);
	value = (b1 << 8) + b2;

	return value;
}

UInt32 audPlatformSpecific::FlipEndian(UInt32 value)
{
	Byte b1, b2, b3, b4;

	b1 = (Byte)(value & 255);
	b2 = (Byte)((value >> 8 ) & 255);
	b3 = (Byte)((value >> 16 ) & 255);
	b4 = (Byte)((value >> 24 ) & 255);

	value = ((UInt32)b1 << 24) + ((UInt32)b2 << 16) + ((UInt32)b3 << 8) + b4;

	return value;
}

Single audPlatformSpecific::FlipEndian(Single value)
{
	FLOATCONVERT converter;

	converter.floating = value;
	converter.fixed = FlipEndian(converter.fixed);

	return converter.floating;
}

unsigned __int64 audPlatformSpecific::FlipEndian(unsigned __int64 value)
{
  union
  {
    unsigned __int64 i;
    unsigned char b[8];
  } dat1, dat2;

  dat1.i = value;
  dat2.b[0] = dat1.b[7];
  dat2.b[1] = dat1.b[6];
  dat2.b[2] = dat1.b[5];
  dat2.b[3] = dat1.b[4];
  dat2.b[4] = dat1.b[3];
  dat2.b[5] = dat1.b[2];
  dat2.b[6] = dat1.b[1];
  dat2.b[7] = dat1.b[0];

  return dat2.i;
}

} // namespace audBuildCommon
