========================================================================
    DYNAMIC LINK LIBRARY : audbuildcommon Project Overview
========================================================================

AppWizard has created this audbuildcommon DLL for you.  

This file contains a summary of what you will find in each of the files that
make up your audbuildcommon application.

audbuildcommon.vcproj
    This is the main project file for VC++ projects generated using an Application Wizard. 
    It contains information about the version of Visual C++ that generated the file, and 
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

audbuildcommon.cpp
    This is the main DLL source file.

audbuildcommon.h
    This file contains a class declaration.

AssemblyInfo.cpp
	Contains custom attributes for modifying assembly metadata.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
