﻿// -----------------------------------------------------------------------
// <copyright file="MetadataFile.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace MetadataLib
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Xml.Linq;

    using MetadataViewer;

    using BinaryReader = rage.ToolLib.Reader.BinaryReader;

    /// <summary>
    /// Metadata file.
    /// </summary>
    public class MetadataFile
    {
        /// <summary>
        /// The name table extension.
        /// </summary>
        private const string NameTableExt = ".nametable";

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataFile"/> class.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <param name="isBigEndian">
        /// The is big endian.
        /// </param>
        /// <param name="schemaPath">
        /// The schema path.
        /// </param>
        public MetadataFile(string filePath, bool isBigEndian, string schemaPath)
        {
            this.IsBigEndian = isBigEndian;
            this.SchemaPath = schemaPath;
            this.FilePath = filePath;
            this.NameTableFileName = this.FilePath + NameTableExt;

            // Mode 0 DAT files don't have a corresponding name table file
            this.ModeZero = !File.Exists(this.NameTableFileName);

            this.Init(new FileStream(filePath, FileMode.Open, FileAccess.Read), this.IsBigEndian, this.SchemaPath);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataFile"/> class.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <param name="isBigEndian">
        /// The is big endian.
        /// </param>
        /// <param name="schemaPath">
        /// The schema path.
        /// </param>
        public MetadataFile(Stream stream, bool isBigEndian, string schemaPath)
        {
            this.IsBigEndian = isBigEndian;
            this.SchemaPath = schemaPath;
            this.ModeZero = true;

            this.Init(stream, this.IsBigEndian, this.SchemaPath);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the file path.
        /// </summary>
        public string FilePath { get; private set; }

        /// <summary>
        /// Gets the data size.
        /// </summary>
        public uint DataSize { get; private set; }

        /// <summary>
        /// Gets the name table file name.
        /// </summary>
        public string NameTableFileName { get; private set; }

        /// <summary>
        /// Gets a value indicating whether is big endian.
        /// </summary>
        public bool IsBigEndian { get; private set; }

        /// <summary>
        /// Gets the schema path.
        /// </summary>
        public string SchemaPath { get; private set; }

        /// <summary>
        /// Gets a value indicating whether mode zero.
        /// </summary>
        public bool ModeZero { get; private set; }

        /// <summary>
        /// Gets the name table data.
        /// </summary>
        public byte[] NameTableData { get; private set; } 

        /// <summary>
        /// Gets the object lookup.
        /// </summary>
        public Dictionary<string, MetadataObject> ObjectLookup { get; private set; }

        /// <summary>
        /// Gets the objects.
        /// </summary>
        public List<MetadataObject> Objects { get; private set; }

        /// <summary>
        /// Gets the schema version.
        /// </summary>
        public uint SchemaVersion { get; private set; }

        /// <summary>
        /// Gets the string table.
        /// </summary>
        public List<string> StringTable { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Validate the current metadata file.
        /// This is only applicable to release file versions, which are compared
        /// to the development version.
        /// </summary>
        /// <returns>
        /// Value indicating whether metadata file is valid <see cref="bool"/>.
        /// </returns>
        public bool Validate()
        {
            if (string.IsNullOrEmpty(this.FilePath))
            {
                // Running in-memory - no file to validate against
                return true;
            }

            var ext = Path.GetExtension(this.FilePath);
            if (null == ext || !ext.ToLower().EndsWith(".rel"))
            {
                // Only want to validate release file versions
                return true;
            }

            // Remove the .rel extension
            var devFilePath = this.FilePath.Substring(0, this.FilePath.Length - 4);

            if (!File.Exists(devFilePath))
            {
                MessageBox.Show(
                    "Failed to find dev version of file: " + devFilePath,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }

            // Check for any objects
            if (!this.Objects.Any())
            {
                MessageBox.Show(
                    "No objects found in metadata file",
                    "Validation Warning",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return true;
            }

            var devFile = new MetadataFile(devFilePath, this.IsBigEndian, this.SchemaPath);

            // Check both files have same number of objects
            if (this.Objects.Count != devFile.Objects.Count)
            {
                MessageBox.Show(
                    "Release version of file has " + this.Objects.Count + " object(s) but the dev version has "
                    + devFile.Objects.Count + " object(s) - these should match",
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }

            // Check file sizes
            var relFileSize = new FileInfo(this.FilePath).Length;
            var devFileSize = new FileInfo(devFilePath).Length;

            // We know that the release file doesn't include 3 byte name table offset for each object
            var knownDiff = 3 * this.Objects.Count;

            if (devFileSize != relFileSize + knownDiff)
            {
                MessageBox.Show(
                    "Release file size (" + relFileSize + " bytes) does not match expected size (" + (devFileSize - knownDiff)
                    + " bytes)",
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }

            // Compare objects
            for (var i = 0; i < this.Objects.Count; i++)
            {
                var relData = this.Objects[i].Data;
                var devData = devFile.Objects[i].Data;

                // Remove 3 byte name table offset from dev data
                var tmp = devData.ToList();
                tmp.RemoveRange(1, 3);
                devData = tmp.ToArray();

                // Check data lengths are now equal
                if (relData.Length != devData.Length)
                {
                    MessageBox.Show(
                        "Object data lengths do not match: " + this.Objects[i].Name,
                        "Validation Failed",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return false;
                }

                // Compare byte by byte
                var comparer = EqualityComparer<byte>.Default;
                for (var j = 0; j < relData.Length; j++)
                {
                    if (!comparer.Equals(relData[j], devData[j]))
                    {
                        MessageBox.Show(
                        "Byte " + j + " of object data does not match dev version: " + this.Objects[i].Name,
                        "Validation Failed",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                        return false;
                    }
                }
            }

            return true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initialize the metadata file.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool Init(Stream stream, bool isBigEndian, string schemaPath)
        {
            stream.Seek(0, SeekOrigin.Begin);

            try
            {
                using (BinaryReader br = new BinaryReader(stream, isBigEndian))
                {
                    this.SchemaVersion = br.ReadUInt();
                    this.DataSize = br.ReadUInt();

                    // Skip over the data
                    br.Seek(this.DataSize, SeekOrigin.Current);

                    this.LoadStringTable(br);
                    this.LoadObjects(br);

                    if (!this.ModeZero)
                    {
                        this.LoadNameTable();
                    }

                    this.LoadObjectData(br, schemaPath);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to open file: " + ex.Message);
            }

            return true;
        }

        /// <summary>
        /// The load object data.
        /// </summary>
        /// <param name="br">
        /// The br.
        /// </param>
        /// <param name="schemaPath">
        /// The schema path.
        /// </param>
        private void LoadObjectData(BinaryReader br, string schemaPath)
        {
            var typeNames = new List<string>();
            if (schemaPath != null)
            {
                var schemaFile = XDocument.Load(schemaPath);
                typeNames.AddRange(schemaFile.Root.Elements("TypeDefinition").Select(e => e.Attribute("name").Value));
            }

            foreach (var obj in this.Objects)
            {
                br.Seek(obj.Offset + 8, SeekOrigin.Begin);
                obj.Data = br.ReadBytes((int)obj.Size, false);
                if (typeNames.Count > obj.TypeId)
                {
                    obj.TypeName = typeNames[obj.TypeId];
                }

                if (!this.ModeZero)
                {
                    obj.Name = string.Empty;

                    var classId = obj.Data[0];

                    var offsetBytes = new List<byte> { obj.Data[1], obj.Data[2], obj.Data[3] };
                    if (this.IsBigEndian)
                    {
                        offsetBytes.Reverse();
                    }

                    offsetBytes.Add(new byte());
                    var offset = BitConverter.ToUInt32(offsetBytes.ToArray(), 0);

                    var currIndex = offset;
                    byte currByte;
                    var objNameBytes = new List<byte>();
                    while ((currByte = this.NameTableData[currIndex]) != 0)
                    {
                        objNameBytes.Add(currByte);
                        currIndex++;
                    }

                    obj.Name = System.Text.Encoding.ASCII.GetString(objNameBytes.ToArray());
                    this.ObjectLookup.Add(obj.Name, obj);
                }
            }
        }

        /// <summary>
        /// The load objects.
        /// </summary>
        /// <param name="br">
        /// The DAT file reader.
        /// </param>
        private void LoadObjects(BinaryReader br)
        {
            // Load name table from start of file
            var numObjects = br.ReadUInt();

            if (this.ModeZero)
            {
                var stringSize = br.ReadUInt();
            }

            this.Objects = new List<MetadataObject>((int)numObjects);
            this.ObjectLookup = new Dictionary<string, MetadataObject>((int)numObjects);

            for (uint i = 0; i < numObjects; i++)
            {
                if (this.ModeZero)
                {
                    var nameLength = br.ReadByte();
                    var nameBytes = br.ReadBytes(nameLength, false);
                    var obj = new MetadataObject
                    {
                        Name = System.Text.Encoding.ASCII.GetString(nameBytes),
                        Offset = br.ReadUInt(),
                        Size = br.ReadUInt()
                    };

                    this.Objects.Add(obj);
                    this.ObjectLookup.Add(obj.Name, obj);
                }
                else
                {
                    var nameHash = br.ReadUInt();
                    var obj = new MetadataObject
                    {
                        Offset = br.ReadUInt(),
                        Size = br.ReadUInt()
                    };

                    this.Objects.Add(obj);
                }
            }
        }

        /// <summary>
        /// The load string table.
        /// </summary>
        /// <param name="datReader">
        /// The DAT file reader.
        /// </param>
        private void LoadStringTable(BinaryReader datReader)
        {
            var stringTableSize = datReader.ReadUInt();
            var numStrings = datReader.ReadUInt();

            datReader.Seek(numStrings * 4, SeekOrigin.Current);

            this.StringTable = new List<string>((int)numStrings);
            for (uint i = 0; i < numStrings; i++)
            {
                this.StringTable.Add(datReader.ReadCString());
            }
        }

        /// <summary>
        /// Load the name table file.
        /// </summary>
        private void LoadNameTable()
        {
            if (!File.Exists(this.NameTableFileName))
            {
                throw new Exception("Failed to find name table file: " + this.NameTableFileName);
            }

            var fileStream = new FileStream(this.NameTableFileName, FileMode.Open, FileAccess.Read);

            this.NameTableData = new byte[fileStream.Length];
            fileStream.Read(this.NameTableData, 0, this.NameTableData.Length);

            fileStream.Close();
        }

        #endregion
    }
}
