﻿// -----------------------------------------------------------------------
// <copyright file="MetadataObject.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace MetadataLib
{
    /// <summary>
    /// Metadata object.
    /// </summary>
    public class MetadataObject
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the offset.
        /// </summary>
        public uint Offset { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        public uint Size { get; set; }

        /// <summary>
        /// Gets the type id.
        /// </summary>
        public byte TypeId
        {
            get
            {
                if (this.Data != null)
                {
                    return this.Data[0];
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets or sets the type name.
        /// </summary>
        public string TypeName { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
