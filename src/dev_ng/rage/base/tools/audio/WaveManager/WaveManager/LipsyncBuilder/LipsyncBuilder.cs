﻿// -----------------------------------------------------------------------
// <copyright file="LipsyncBuilder.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace LipsyncBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Lipsync Builder.
    /// </summary>
    public class LipsyncBuilder
    {
        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The local watch path.
        /// </summary>
        private readonly string watchLocalPath;

        /// <summary>
        /// The pending bank build path.
        /// </summary>
        private readonly string pendingBankBuildPath;

        /// <summary>
        /// The project settings local path.
        /// </summary>
        private readonly string projectSettingsLocalPath;

        /// <summary>
        /// The module exe local path.
        /// </summary>
        private readonly string moduleExeLocalPath;

        /// <summary>
        /// The errors.
        /// </summary>
        private StringBuilder errors;

        /// <summary>
        /// The is running flag.
        /// </summary>
        private bool isRunning;

        /// <summary>
        /// Initializes a new instance of the <see cref="LipsyncBuilder"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public LipsyncBuilder(Settings settings, IAssetManager assetManager)
        {
            this.settings = settings;
            this.assetManager = assetManager;

            this.watchLocalPath = this.assetManager.GetLocalPath(this.settings.WatchDepotPath);
            this.pendingBankBuildPath = this.assetManager.GetLocalPath(this.settings.PendingBankBuildPath);
            this.projectSettingsLocalPath = this.assetManager.GetLocalPath(this.settings.ProjectSettingsDepotPath);
            this.moduleExeLocalPath = this.assetManager.GetLocalPath(this.settings.ModuleExeDepotPath);
        }

        /// <summary>
        /// Run the lipsync builder.
        /// </summary>
        public void Run()
        {
            while (true)
            {
                if (!this.isRunning)
                {
                    try
                    {
                        this.isRunning = true;
                        this.errors = new StringBuilder();

                        if (!this.CheckForPendingFiles())
                        {
                            throw new Exception("Failed to check for pending files");
                        }

                        this.CleanupPendingFiles();

                        this.ProcessPendingFiles();
                        this.isRunning = false;
                    }
                    catch (Exception ex)
                    {
                        Utils.EmailErrors(this.settings, this.errors);

                        var message = "** The service may require restarting **";
                        message += ex.Message + Environment.NewLine;
                        message += ex.StackTrace + Environment.NewLine;
                        if (ex.InnerException != null)
                        {
                            message += "Inner exception: " + Environment.NewLine;
                            message += ex.InnerException.Message + Environment.NewLine;
                        }
                        Utils.SendEmail(this.settings, message);
                    }
                    finally 
                    {
                        this.isRunning = false;
                    }
                }

                Thread.Sleep(this.settings.RefreshFrequency);
            }
        }

        /// <summary>
        /// Check for pending files.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool CheckForPendingFiles()
        {
            if (!this.assetManager.GetLatest(this.settings.WatchDepotPath, false))
            {
                throw new Exception("Failed to get latest data: " + this.settings.WatchDepotPath);
            }

            return true;
        }

        /// <summary>
        /// Cleanup the pending files by removing duplicate lines within files
        /// or deleting files if all lines within them are duplicates.
        /// </summary>
        private void CleanupPendingFiles()
        {
            var updateChangeList =
                this.assetManager.CreateChangeList("[Lipsync Builder] Removing pending lipsync regen duplicate lines");

            // Keep track of the pending paths to rebuild and the files which we can delete
            var pendingPaths = new HashSet<string>();
            var filesToDelete = new List<string>();

            foreach (var file in Directory.GetFiles(this.watchLocalPath, "*.txt"))
            {
                var reader = new StreamReader(file);
                var lines = new List<string>();

                var changesFound = false;

                // Check each pending line in current pending file
                string line;
                while (null != (line = reader.ReadLine()))
                {
                    line = line.Trim();

                    if (string.IsNullOrEmpty(line))
                    {
                        continue;
                    }

                    line = line.ToUpper();

                    if (pendingPaths.Contains(line))
                    {
                        // This line exists in another file so can
                        // skip this one
                        changesFound = true;
                        continue;
                    }

                    // This line is not found so still needs to be processed
                    lines.Add(line);
                    pendingPaths.Add(line);
                }

                reader.Close();

                if (lines.Any())
                {
                    if (changesFound)
                    {
                        // Some lines in the file are already due to be processed,
                        // so can remove these lines from the current file
                        updateChangeList.CheckoutAsset(file, true);
                        using (var writer = new StreamWriter(file))
                        {
                            foreach (var currLine in lines)
                            {
                                writer.WriteLine(currLine);
                            }
                        }
                    }
                }
                else
                {
                    // All lines in file are already due to be processed,
                    // so it's safe to delete this file to avoid duplicate
                    // processing
                    filesToDelete.Add(file);
                }
            }

            // Commit any pending changes to files
            updateChangeList.RevertUnchanged();
            if (updateChangeList.Assets.Any())
            {
                updateChangeList.Submit();
            }
            else
            {
                updateChangeList.Delete();
            }

            // Delete any files that we no longer require due to
            // them containing only duplicate lines
            if (filesToDelete.Any())
            {
                var changeList =
                    this.assetManager.CreateChangeList("[Lipsync Builder] Removing pending lipsync regen duplicate files");

                foreach (var file in filesToDelete)
                {
                    changeList.MarkAssetForDelete(file);
                }

                changeList.RevertUnchanged();
                changeList.Submit();
            }
        }

        /// <summary>
        /// Process any pending files.
        /// </summary>
        private void ProcessPendingFiles()
        {
            if (!this.assetManager.GetLatest(this.settings.ProjectSettingsDepotPath, false))
            {
                throw new Exception("Failed to get latest project settings: " + this.settings.ProjectSettingsDepotPath);
            }

            if (!Directory.Exists(this.watchLocalPath))
            {
                throw new Exception("Failed to find local watch path: " + this.watchLocalPath);
            }

            if (!Directory.Exists(this.settings.ProcessedPath))
            {
                Directory.CreateDirectory(this.settings.ProcessedPath);
            }

            foreach (var file in Directory.GetFiles(this.watchLocalPath, "*.txt"))
            {
                ISet<string> banksToProcess = new HashSet<string>();

                var reader = new StreamReader(file);
                string line;
                while (null != (line = reader.ReadLine()))
                {
                    line = line.Trim();

                    if (string.IsNullOrEmpty(line))
                    {
                        continue;
                    }

                    banksToProcess.Add(line);
                }

                reader.Close();

                banksToProcess = this.RemoveInvalidBanks(banksToProcess);

                // Copy current file to processed folder.
                var fileName = Path.GetFileName(file);
                var destPath = Path.Combine(this.settings.ProcessedPath, fileName);
                if (File.Exists(destPath)) new FileInfo(destPath) { IsReadOnly = false };
                File.Copy(file, destPath, true);
                new FileInfo(destPath) { IsReadOnly = false };

                if (banksToProcess.Any())
                {
                    if (!this.GenerateLipsyncData(banksToProcess))
                    {
                        this.errors.AppendLine("Failed to generate lipsync data for bank(s): " + string.Join(", ", banksToProcess));
                    }
                }

                if (!this.MoveProcessedFile(file))
                {
                    this.errors.AppendLine("Failed to move processed file: " + file);
                }

                Utils.EmailErrors(this.settings, this.errors);
            }
        }

        /// <summary>
        /// Remove invalid banks.
        /// </summary>
        /// <param name="bankToProcess">
        /// The bank to process.
        /// </param>
        /// <returns>
        /// The updated bank list <see cref="ISet"/>.
        /// </returns>
        private ISet<string> RemoveInvalidBanks(IEnumerable<string> bankToProcess)
        {
            var updatedBankList = new HashSet<string>();
            
            foreach (var bank in bankToProcess)
            {
                // Check that bank path exists in P4 (may have changed since
                // the pending file was created).
                var bankPath = Path.Combine(this.settings.WavesFolderLocalPath, bank + "\\");
                try
                {
                    this.assetManager.GetLatest(bankPath, true);

                    if (!Directory.Exists(bankPath))
                    {
                        continue;
                    }
                }
                catch (Exception ex)
                {
                    if (!ex.Message.ToLower().Contains("no such file(s)"))
                    {
                        this.errors.AppendLine("Failed to check for invalid banks: " + ex.Message);
                        throw ex;
                    }

                    this.errors.AppendLine("bank path not found in P4 - skipping bank path: " + bankPath);
                    continue;
                }

                updatedBankList.Add(bank);
            }

            return updatedBankList;
        }

        /// <summary>
        /// Generate lipsync data.
        /// </summary>
        /// <param name="banksToProcess">
        /// The banks to be processed.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GenerateLipsyncData(IEnumerable<string> banksToProcess)
        {
            foreach (var bank in banksToProcess)
            {
                var args = new List<string>
                {
                    string.Format("-assetserver {0}", this.settings.P4Port),
                    string.Format("-assetproject {0}", this.settings.P4Workspace),
                    string.Format("-assetusername {0}", this.settings.P4User),
                    string.Format("-assetpassword {0}", this.settings.P4Password),
                    string.Format("-depotroot {0}", this.settings.P4DepotRoot),
                    string.Format("-projectsettings {0}", this.projectSettingsLocalPath),
                    string.Format("-wavesfolder {0}", this.settings.WavesFolderLocalPath),
                    string.Format("-moduleexepath {0}", this.moduleExeLocalPath),
                    string.Format("-facefxexepath {0}", this.settings.FaceFxExeLocalPath),
                    string.Format("-speechfolder {0}", bank)
                };

                if (!string.IsNullOrEmpty(this.settings.LogfilePath)) args.Add("-logfile "+this.settings.LogfilePath);

                var argsStr = string.Join(" ", args);
                string visemeBuilderLocalPath = this.assetManager.GetLocalPath(this.settings.VisemeBuilderDepotPath);
                string visemeBuilderWorkingLocalPath = this.assetManager.GetLocalPath(this.settings.VisemeBuilderWorkingDepotPath);

                var attempts = 0;
                var successful = false;
                VisemeBuilderInvoker visemeBuilder = null;
                while (attempts <= this.settings.MaxRetryAttempts && !successful)
                {
                    visemeBuilder = new VisemeBuilderInvoker(visemeBuilderLocalPath, visemeBuilderWorkingLocalPath, argsStr);
                    successful = visemeBuilder.Invoke();
                    attempts++;
                }

                if (!successful)
                {
                    this.errors.AppendLine(string.Format(
                        "Failed to generate lipsync data after {0} attempt(s): {1}", this.settings.MaxRetryAttempts, bank));
                    this.errors.Append(visemeBuilder.Errors.ToString());
                }
            }

            return true;
        }

        /// <summary>
        /// Move the processed file.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool MoveProcessedFile(string file)
        {
            // Move source file to bank builder pending folder.
            var changeList = this.assetManager.CreateChangeList(
                "[Lipsync Builder] Moving processed bank list file");

            try
            {
                var asset = changeList.CheckoutAsset(file, true);
                var fileName = Path.GetFileName(file);
                var newPath = Path.Combine(this.pendingBankBuildPath, fileName);

                if (!changeList.MoveAsset(asset, newPath))
                {
                    this.errors.AppendLine("Failed to move asset: " + file);
                }
            }
            catch (Exception ex)
            {
                this.errors.AppendLine(string.Format("Failed to move asset: {0} - {1}", file, ex.Message));
            }

            if (changeList.Assets.Any())
            {
                if (!changeList.Submit())
                {
                    this.errors.AppendLine("Failed to submit change list with pending file move(s)");
                }
            }
            else
            {
                changeList.Delete();
            }

            return true;
        }
    }
}
