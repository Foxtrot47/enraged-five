﻿// -----------------------------------------------------------------------
// <copyright file="BankBuilder.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace LipsyncBuilder
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Xml.Linq;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Bank builder.
    /// </summary>
    public class BankBuilder
    {
        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The pending bank build path.
        /// </summary>
        private string pendingBankBuildPath;

        /// <summary>
        /// The pending waves paths.
        /// </summary>
        private IDictionary<string, string> pendingWavesPaths;

        /// <summary>
        /// The built waves paths.
        /// </summary>
        private IDictionary<string, string> builtWavesPaths;

        /// <summary>
        /// The errors.
        /// </summary>
        private StringBuilder errors;

        /// <summary>
        /// The is running flag.
        /// </summary>
        private bool isRunning;

        /// <summary>
        /// Initializes a new instance of the <see cref="BankBuilder"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public BankBuilder(Settings settings, IAssetManager assetManager)
        {
            this.settings = settings;
            this.assetManager = assetManager;
            this.InitPaths();
        }

        /// <summary>
        /// Run the bank builder.
        /// </summary>
        public void Run()
        {
            while (true)
            {
                if (!this.isRunning)
                {
                    try
                    {
                        this.isRunning = true;
                        this.errors = new StringBuilder();
                        if (!this.CheckForPendingFiles())
                        {
                            throw new Exception("Failed to check for pending files");
                        }

                        this.ProcessPendingFiles();
                        this.isRunning = false;
                    }
                    catch (Exception ex)
                    {
                        Utils.EmailErrors(this.settings, this.errors);

                        var message = string.Format(
                            "{0}{1}{2}** The service may require restarting **",
                            ex.Message,
                            Environment.NewLine,
                            Environment.NewLine);
                        Utils.SendEmail(this.settings, message);

                        this.isRunning = false;
                    }
                }

                Thread.Sleep(this.settings.RefreshFrequency);
            }
        }

        /// <summary>
        /// Initialize the paths.
        /// </summary>
        private void InitPaths()
        {
            this.pendingBankBuildPath = this.assetManager.GetLocalPath(this.settings.PendingBankBuildPath);

            this.pendingWavesPaths = new Dictionary<string, string>();
            this.builtWavesPaths = new Dictionary<string, string>();

            foreach (var platform in this.settings.Platforms)
            {
                var pendingWavesPath = Regex.Replace(
                    this.settings.PendingWavesDepotPath, "{platform}", platform, RegexOptions.IgnoreCase);

                this.pendingWavesPaths.Add(platform, this.assetManager.GetLocalPath(pendingWavesPath));

                var builtWavesPath = Regex.Replace(
                    this.settings.BuiltWavesDepotPath, "{platform}", platform, RegexOptions.IgnoreCase);

                this.builtWavesPaths.Add(platform, this.assetManager.GetLocalPath(builtWavesPath));
            }
        }

        /// <summary>
        /// Check for pending files.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool CheckForPendingFiles()
        {
            if (!this.assetManager.GetLatest(this.settings.PendingBankBuildPath, false))
            {
                throw new Exception("Failed to get latest data: " + this.settings.PendingBankBuildPath);
            }

            foreach (var platform in this.settings.Platforms)
            {
                if (!this.assetManager.GetLatest(this.pendingWavesPaths[platform], false))
                {
                    return false;
                }

                if (!this.assetManager.GetLatest(this.builtWavesPaths[platform], false))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Process any pending files.
        /// </summary>
        private void ProcessPendingFiles()
        {
            foreach (var file in Directory.GetFiles(this.pendingBankBuildPath, "*.txt"))
            {
                var banksToProcess = new HashSet<string>();
                var reader = new StreamReader(file);
                string line;
                while (null != (line = reader.ReadLine()))
                {
                    line = line.Trim();

                    if (string.IsNullOrEmpty(line))
                    {
                        continue;
                    }

                    banksToProcess.Add(line);
                }

                reader.Close();

                if (banksToProcess.Any() && !this.MarkBanksForRebuildHelper(file, banksToProcess))
                {
                    this.errors.AppendLine("Failed to mark bank(s) for rebuild: " + string.Join(", ", banksToProcess));
                }
            }

            Utils.EmailErrors(this.settings, this.errors);
        }

        /// <summary>
        /// Mark banks for rebuild.
        /// </summary>
        /// <param name="pendingFilePath">
        /// The pending file path.
        /// </param>
        /// <param name="bankPaths">
        /// The banks.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool MarkBanksForRebuildHelper(string pendingFilePath, IEnumerable<string> bankPaths)
        {
            var banksInPacks = new Dictionary<string, IList<string>>();

            // Group banks by pack
            foreach (var bank in bankPaths)
            {
                var split = bank.Split('\\');
                var packName = split[0].ToUpper();

                if (!banksInPacks.ContainsKey(packName))
                {
                    banksInPacks[packName] = new List<string>();
                }

                banksInPacks[packName].Add(bank);
            }

            var filesLocked = false;

            // Check if any pending waves files are currently locked
            // If so, we will just leave the file and try again later
            foreach (var banksInPack in banksInPacks)
            {
                if (filesLocked)
                {
                    break;
                }

                var pack = banksInPack.Key;
                foreach (var platform in this.settings.Platforms)
                {
                    var pendingWavesPath = Path.Combine(this.pendingWavesPaths[platform], pack + ".xml");
                    if (this.assetManager.ExistsAsAsset(pendingWavesPath))
                    {
                        var fileInfo = this.assetManager.GetFileStatus(pendingWavesPath);
                        if (fileInfo.IsLocked && !fileInfo.IsLockedByMe)
                        {
                            // At least one file we need is currently locked
                            // so will leave file to retry later
                            filesLocked = true;
                            break;
                        }
                    }
                }
            }

            if (filesLocked)
            {
                return true;
            }

            // Mark up banks for rebuild
            foreach (var banksInPack in banksInPacks)
            {
                var pack = banksInPack.Key;
                var banks = banksInPack.Value;

                foreach (var platform in this.settings.Platforms)
                {
                    // Create new change list
                    var changeList =
                        this.assetManager.CreateChangeList("Wave Rebuild of " + pack + " pack [" + platform + "]");

                    // Mark next batch of banks for rebuild
                    if (!this.MarkBanksForRebuildHelper(changeList, platform, pack, banks))
                    {
                        this.errors.AppendLine("Failed to mark banks for rebuild: " + pack);
                        changeList.Revert(true);
                        return false;
                    }

                    changeList.RevertUnchanged();

                    if (changeList.Assets.Any())
                    {
                        // Submit pending wave changes
                        if (!changeList.Submit())
                        {
                            this.errors.AppendLine("Failed to submit change list: " + changeList.NumberAsString);
                            return false;
                        }
                    }
                    else
                    {
                        changeList.Delete();
                    }
                }
            }

            // We've successfully processed the pending file so now delete it
            if (!this.DeleteProcessedFile(pendingFilePath))
            {
                this.errors.AppendLine("Failed to delete processed file: " + pendingFilePath);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Mark banks in a pack for rebuild.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <param name="paths">
        /// The paths to be marked for rebuild.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool MarkBanksForRebuildHelper(IChangeList changeList, string platform, string packName, IEnumerable<string> paths)
        {
            var pendingWavesPath = Path.Combine(this.pendingWavesPaths[platform], packName + ".xml");
            var builtWavesPath = Path.Combine(this.builtWavesPaths[platform], packName + "_pack_file.xml");

            if (!File.Exists(builtWavesPath))
            {
                this.errors.AppendLine("Failed to find built waves file: " + builtWavesPath);
                return false;
            }

            var builtWavesDoc = XDocument.Load(builtWavesPath);
            var banksElemsToRebuild = new List<XElement>();

            foreach (var path in paths)
            {
                var split = path.Split('\\');
                var currElem = builtWavesDoc.Root;
                for (var i = 1; i < split.Length; i++)
                {
                    var upperName = split[i].ToUpper();
                    currElem = currElem.Elements().FirstOrDefault(e => e.Attribute("name").Value.ToUpper() == upperName);
                    if (null == currElem)
                    {
                        this.errors.AppendLine("Failed to find element to mark for rebuild: " + path);
                        break;
                    }
                }

                if (null != currElem)
                {
                    banksElemsToRebuild.Add(currElem);
                }
            }

            if (banksElemsToRebuild.Any())
            {
                if (this.assetManager.ExistsAsAsset(pendingWavesPath))
                {
                    try
                    {
                        var asset = changeList.CheckoutAsset(pendingWavesPath, true);
                        if (null == asset)
                        {
                            this.errors.AppendLine(
                                "Failed to check out and lock pending waves file: " + pendingWavesPath);
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        this.errors.AppendLine(ex.Message);
                        return false;
                    }
                }
                else
                {
                    changeList.MarkAssetForAdd(pendingWavesPath);
                }

                if (!this.WritePendingWaves(banksElemsToRebuild, pendingWavesPath))
                {
                    this.errors.AppendLine("Failed to write pending wave changes to file: " + pendingWavesPath);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Write bank rebuild entries into pending waves.
        /// </summary>
        /// <param name="bankElems">
        /// The bank elements.
        /// </param>
        /// <param name="pendingWavesPath">
        /// The pending waves path.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool WritePendingWaves(IEnumerable<XElement> bankElems, string pendingWavesPath)
        {
            var doc = new XDocument();
            if (File.Exists(pendingWavesPath))
            {
                doc = XDocument.Load(pendingWavesPath);
            }
            else
            {
                var pendingWavesElem = new XElement("PendingWaves");
                doc.Add(pendingWavesElem);
            }

            foreach (var bankElem in bankElems)
            {
                // Get hierarchy structure of bank
                var parentList = new List<XElement> { bankElem };
                var currParent = bankElem.Parent;
                while (null != currParent)
                {
                    // Stop when we hit PendingWaves element
                    if (currParent.Name.LocalName != "PendingWaves")
                    {
                        parentList.Insert(0, currParent);
                    }

                    currParent = currParent.Parent;
                }

                // Insert cloned bank element into pending waves
                var currElem = doc.Root;
                foreach (var elem in parentList)
                {
                    var existingElem = this.FindExistingElement(currElem, elem);
                    if (null != existingElem)
                    {
                        // Check that existing element not marked for remove
                        var opAttr = existingElem.Attribute("operation");
                        if (null != opAttr && opAttr.Value.ToLower() == "remove")
                        {
                            // We don't want to mark an element for rebuild if it is
                            // in a path that contains an element marked for remove
                            continue;
                        }
                    }
                    else 
                    {
                        existingElem = new XElement(elem);
                        existingElem.RemoveNodes();

                        var isBankElem = false;

                        if (existingElem.Name.LocalName == "Bank")
                        {
                            isBankElem = true;

                            // Add rebuild tag to Bank
                            var tagElem = new XElement("Tag");
                            tagElem.SetAttributeValue("name", "rebuild");
                            tagElem.SetAttributeValue("operation", "add");
                            existingElem.Add(tagElem);
                        }

                        currElem.Add(existingElem);

                        if (isBankElem)
                        {
                            // We've found the bank and marked for rebuild,
                            // no need to continue any further
                            break;
                        }
                    }

                    currElem = existingElem;
                }
            }

            doc.Save(pendingWavesPath);
            return true;
        }

        /// <summary>
        /// Determine whether element already exists in new structure.
        /// (based on element type and name).
        /// </summary>
        /// <param name="parentElem">
        /// The parent element.
        /// </param>
        /// <param name="elem">
        /// The element to find.
        /// </param>
        /// <returns>
        /// Existing element <see cref="bool"/>.
        /// </returns>
        private XElement FindExistingElement(XElement parentElem, XElement elem)
        {
            foreach (var childElem in parentElem.Elements())
            {
                if (childElem.Name.LocalName != elem.Name.LocalName)
                {
                    continue;
                }

                var childNameAttr = childElem.Attribute("name");
                var elemNameAttr = elem.Attribute("name");

                if (null == childNameAttr && null == elemNameAttr)
                {
                    return childElem;
                }

                if (null == childNameAttr || null == elemNameAttr)
                {
                    continue;
                }

                var childName = childNameAttr.Value;
                var elemName = elemNameAttr.Value;

                if (childName == elemName)
                {
                    return childElem;
                }
            }

            return null;
        }

        /// <summary>
        /// Delete a processed file.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool DeleteProcessedFile(string file)
        {
            // Delete source file from asset manager.
            var changeList = this.assetManager.CreateChangeList(
                "[Lipsync Bank Builder] Removing processed bank list file");

            try
            {
                var asset = changeList.MarkAssetForDelete(file);
                if (null == asset)
                {
                    this.errors.AppendLine("Failed to mark asset for delete: " + file);
                }
            }
            catch (Exception ex)
            {
                this.errors.AppendLine(string.Format("Failed to mark asset for delete: {0} - {1}", file, ex.Message));
            }

            if (changeList.Assets.Any())
            {
                if (!changeList.Submit())
                {
                    this.errors.AppendLine("Failed to submit change list with pending file deletion(s)");
                }
            }
            else
            {
                changeList.Delete();
            }

            return true;
        }
    }
}
