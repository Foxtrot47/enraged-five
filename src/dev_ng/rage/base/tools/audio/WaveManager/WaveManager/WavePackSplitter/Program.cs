﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Wave pack splitter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WavePackSplitter
{
    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// <para>
    /// Wave pack splitter.
    /// </para>
    /// <para>
    /// Designed to run as a one-off task to split large packs into
    /// multiple smaller packs.
    /// </para>
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main entry point.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            var settings = new Settings(args);
            var assetManager = AssetManagerFactory.GetInstance(
                AssetManagerType.Perforce,
                settings.P4Host,
                settings.P4Port,
                settings.P4Workspace,
                settings.P4User,
                settings.P4Password,
                settings.P4DepotRoot);

            //var path = @"x:\gta5\audio\dev\build\ps3\BuiltWaves\SPEECH_PACK_FILE.xml";
            //var splitSpeech = new SplitSpeech(settings, assetManager, path, "SPEECH", "PS3");
            //splitSpeech.Run();

            //var path = @"x:\gta5\audio\dev\build\xbox360\BuiltWaves\SPEECH_PACK_FILE.xml";
            //var splitSpeech = new SplitSpeech(settings, assetManager, path, "SPEECH", "XBOX360");
            //splitSpeech.Run();
            
            var path = @"x:\gta5\audio\dev\build\pc\BuiltWaves\SPEECH_PACK_FILE.xml";
            var splitSpeech = new SplitSpeech(settings, assetManager, path, "SPEECH", "PC");
            splitSpeech.Run();
        }
    }
}
