﻿// -----------------------------------------------------------------------
// <copyright file="Console.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace WaveManager
{
    using System;

    using WaveManager.WaveTools;

    /// <summary>
    /// Wave manager console.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The main entry point.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            Console.WriteLine("Wave Manager started...");
            var settings = new Settings(args);

            if (settings.WaveRebuild || settings.WaveRebuildContinue)
            {
                Console.WriteLine("Starting Bank Rebuilder process...");
                var bankRebuilder = new BankRebuilder(settings);

                if (!bankRebuilder.Run())
                {
                    Console.WriteLine("Wave Manager bank rebuild failed - press any key to continue...");
                    Console.ReadKey();
                }
            }
        }
    }
}
