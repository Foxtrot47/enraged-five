﻿// -----------------------------------------------------------------------
// <copyright file="WaveActions.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace WaveManager.Enums
{
    /// <summary>
    /// Wave actions enum.
    /// </summary>
    public enum WaveActions
    {
        /// <summary>
        /// The bank rebuild.
        /// </summary>
        BankRebuild,

        /// <summary>
        /// The bank rebuild continue.
        /// </summary>
        BankRebuildContinue
    }
}
