﻿// -----------------------------------------------------------------------
// <copyright file="Utils.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace LipsyncBuilder
{
    using System.Linq;
    using System.Net.Mail;
    using System.Text;

    /// <summary>
    /// Lipsync builder utilities.
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// Email errors.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        public static void EmailErrors(Settings settings, StringBuilder errors)
        {
            string errorText = errors.ToString().Trim();
            if (errorText.Length == 0)
            {
                return;
            }

            SendEmail(settings, errorText.ToString());
            errors.Clear();
        }

        /// <summary>
        /// Send email.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void SendEmail(Settings settings, string message)
        {
            if (!settings.EmailRecipients.Any())
            {
                return;
            }

            var email = new MailMessage
            {
                From = new MailAddress(settings.EmailFrom),
                Subject = "Lipsync Builder Failed",
                IsBodyHtml = false,
                Body = message
            };

            foreach (var recipient in settings.EmailRecipients)
            {
                email.To.Add(new MailAddress(recipient));
            }

            settings.SmtpClient.Send(email);
        }

    }
}
