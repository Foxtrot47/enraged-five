﻿namespace BuiltWavesStats
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine();
            Console.WriteLine("--- Built Waves Stats Generator ---\n");
            Console.WriteLine(@"Make sure you have grabbed latest on //depot/gta5/audio/dev/build/");
            Console.WriteLine();
            Console.WriteLine("Press Enter to continue...");
            Console.ReadKey();

            Console.WriteLine("\n> Process started");

            var platforms = new[] { "PS3", "XBOX360", "PC" };

            const string BuiltWavesPath = @"x:\gta5\audio\dev\build\{platform}\BuiltWaves\";
            const string OutputPath = @"C:\Temp\BuiltWavesStats\";

            foreach (var platform in platforms)
            {
                var path = Regex.Replace(BuiltWavesPath, "{platform}", platform, RegexOptions.IgnoreCase);

                if (!Directory.Exists(path))
                {
                    Console.WriteLine("Error: Failed to find path: " + path);
                    Console.WriteLine();
                    Console.WriteLine("Press Enter to Continue...");
                    Console.ReadKey();
                    continue;
                }

                var platformOutputPath = Path.Combine(OutputPath, platform);
                if (!Directory.Exists(platformOutputPath))
                {
                    Directory.CreateDirectory(platformOutputPath);
                }

                foreach (var file in Directory.GetFiles(path, "*.xml"))
                {
                    Console.WriteLine("> Processing " + file);
                    var doc = XDocument.Load(file);
                    var builtBankLists = new SortedDictionary<DateTime, IList<string>>();
                    var totalBanks = 0;

                    foreach (var bankElem in doc.Descendants("Bank"))
                    {
                        var timestampStr = bankElem.Attribute("timeStamp").Value;
                        var timestamp = DateTime.Parse(timestampStr);
                        var dateTimestamp = timestamp.Date;
                        if (!builtBankLists.ContainsKey(dateTimestamp))
                        {
                            builtBankLists[dateTimestamp] = new List<string>();
                        }

                        var bankName = bankElem.Attribute("name").Value;
                        builtBankLists[dateTimestamp].Add(bankName);
                        totalBanks++;
                    }

                    var outputFile = Path.Combine(platformOutputPath, Path.GetFileNameWithoutExtension(file) + "_overview.csv");
                    var writer = new StreamWriter(outputFile);
                    writer.WriteLine("Date,Banks Built,Percentage of Banks in File");

                    foreach (var builtBank in builtBankLists)
                    {
                        var percentage = (((float)builtBank.Value.Count) / totalBanks) * 100;
                        writer.WriteLine("{0},{1},{2:0.00}", builtBank.Key.ToShortDateString(), builtBank.Value.Count, percentage);
                    }

                    writer.Close();

                    outputFile = Path.Combine(platformOutputPath, Path.GetFileNameWithoutExtension(file) + "_banks.csv");
                    writer = new StreamWriter(outputFile);

                    writer.WriteLine("Bank,Last Rebuilt");

                    foreach (var builtBankList in builtBankLists)
                    {
                        foreach (var builtBank in builtBankList.Value)
                        {
                            writer.WriteLine("{0},{1}", builtBank, builtBankList.Key.ToShortDateString());
                        }
                    }

                    writer.Close();
                }

                Console.WriteLine();
            }

            Console.WriteLine();
            Console.WriteLine("Built wave stats saved to " + OutputPath);
        }
    }
}
