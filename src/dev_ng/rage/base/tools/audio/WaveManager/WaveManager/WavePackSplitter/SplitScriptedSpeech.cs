﻿// -----------------------------------------------------------------------
// <copyright file="SplitScriptedSpeech.cs" company="Rockstar North">
// Split Scripted speech.
// </copyright>
// -----------------------------------------------------------------------

namespace WavePackSplitter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Split scripted speech pack.
    /// </summary>
    public class SplitScriptedSpeech
    {
        /// <summary>
        /// Flag indicating whether to lock assets when checked out (false for testing).
        /// </summary>
        private const bool Lock = true;

        /// <summary>
        /// The ranges.
        /// </summary>
        private readonly IList<IList<string>> ranges;

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The pack name.
        /// </summary>
        private readonly string packName;

        /// <summary>
        /// The platform.
        /// </summary>
        private readonly string platform;

        /// <summary>
        /// The input file.
        /// </summary>
        private readonly string inputFile;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The change list.
        /// </summary>
        private IChangeList changeList;

        /// <summary>
        /// Initializes a new instance of the <see cref="SplitScriptedSpeech"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        /// <param name="inputFile">
        /// The input file.
        /// </param>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        public SplitScriptedSpeech(Settings settings, IAssetManager assetManager, string inputFile, string packName, string platform)
        {
            this.settings = settings;
            this.assetManager = assetManager;
            this.inputFile = inputFile;
            this.packName = packName;
            this.platform = platform;

            this.ranges = new List<IList<string>>();
            //ranges.Add(new List<string> { "a", "b", "c" });
            //ranges.Add(new List<string> { "d", "e" });
            //ranges.Add(new List<string> { "f" });
            //ranges.Add(new List<string> { "g", "h", "i", "j", "k", "l", "m" });
            //ranges.Add(new List<string> { "n", "o", "p" });
            //ranges.Add(new List<string> { "q", "r" });
            ranges.Add(new List<string> { "s", "t" });
            //ranges.Add(new List<string> { "u", "v", "w", "x", "y", "z" });
        }

        /// <summary>
        /// Write stats of scripted speech pack to temp file.
        /// </summary>
        public void Stats()
        {
            var inputDoc = XDocument.Load(this.inputFile);

            Console.WriteLine("Loaded file: " + this.inputFile);

            var packElems =
                inputDoc.Root.Elements("BankFolder")
                        .Where(n => n.Attribute("name").Value.ToUpper() == "PLACEHOLDER_SPEECH" || n.Attribute("name").Value.ToUpper() == "SCRIPTED_SPEECH");

            var banks = new Dictionary<string, IList<XElement>>();

            foreach (var packElem in packElems)
            {
                var bankElems = packElem.Elements("Bank").ToList();

                Console.WriteLine("Number of banks: " + bankElems.Count());

                foreach (var bankElem in bankElems)
                {
                    var name = bankElem.Attribute("name").Value;
                    var key = name[0].ToString().ToUpper();
                    if (!banks.ContainsKey(key))
                    {
                        banks[key] = new List<XElement>();
                    }

                    banks[key].Add(bankElem);
                }
            }


            var sorted = banks.ToList().OrderBy(bank => bank.Key);

            var writer = new StreamWriter(@"C:\temp\scripted_speech_split.txt");
            foreach (var bank in sorted)
            {
                var line = string.Format("{0}: {1}", bank.Key, bank.Value.Count);
                Console.WriteLine(line);
                writer.WriteLine(line);
            }

            writer.WriteLine("Total keys: " + banks.Count);
            writer.WriteLine("Total banks: " + banks.Sum(b => b.Value.Count));
            writer.Close();
        }

        /// <summary>
        /// Run the pack splitting process.
        /// </summary>
        public void Run()
        {
            /*if (this.assetManager.IsCheckedOut(this.inputFile))
            {
                throw new Exception("File is already checked out: " + this.inputFile);
            }*/

            /*if (!this.GetLatestData())
            {
                Console.WriteLine("Failed to get latest data");
                Console.ReadKey();
            }*/

            this.changeList = this.assetManager.CreateChangeList("Split " + this.packName + " wave pack");
            //this.changeList.CheckoutAsset(this.inputFile, Lock);

            var inputDoc = XDocument.Load(this.inputFile);
            //var packListDir = Path.GetDirectoryName(this.inputFile);

            //var packListFilePath = Path.Combine(packListDir, "BUILTWAVES_PACK_LIST.XML");
            //this.changeList.CheckoutAsset(packListFilePath, Lock);
            //var packListDoc = XDocument.Load(packListFilePath);

            var newPacks = new Dictionary<string, IList<XElement>>();

            var bankFolderElems = inputDoc.Root.Elements("BankFolder");

            foreach (var bankFolderElem in bankFolderElems)
            {
                // Create new pack lists
                foreach (var bankElem in bankFolderElem.Elements("Bank"))
                {
                    var bankName = bankElem.Attribute("name").Value;
                    var newPackName = SplitUtils.GetNewPackName(this.ranges, bankName);

                    if (null == newPackName)
                    {
                        continue;
                    }

                    if (!newPacks.ContainsKey(newPackName))
                    {
                        newPacks[newPackName] = new List<XElement>();
                    }

                    newPacks[newPackName].Add(bankElem);
                }
            }

            // Create new pack files
            foreach (var kvp in newPacks)
            {
                var packName = kvp.Key;
                var bankElems = kvp.Value;

                /*var outputDoc = new XDocument();
                var rootElem = new XElement("Pack");
                rootElem.SetAttributeValue("name", packName);
                outputDoc.Add(rootElem);

                foreach (var bankElem in bankElems)
                {
                    // Initialize parent element structure up to the bank folder
                    var hierarchy = new List<XElement>();
                    var currElem = bankElem.Parent;
                    while (null != currElem)
                    {
                        if (null != currElem && currElem.Name == "Pack" && currElem.Attribute("name").Value == "SCRIPTED_SPEECH")
                        {
                            // Reached the top of required parent hierarchy
                            break;
                        }

                        hierarchy.Add(currElem);
                        currElem = currElem.Parent;
                    }

                    var parent = SplitUtils.CloneHierarchy(rootElem, hierarchy);

                    // Clone bank element and children and add to new document
                    var newElem = new XElement(bankElem);
                    parent.Add(newElem);
                }

                // Save new pack file
                var packFileName = packName + "_PACK_FILE.xml";
                var filePath = Path.Combine(packListDir, packFileName);
                outputDoc.Save(filePath);
                this.changeList.MarkAssetForAdd(filePath);

                // Add pack file name to pack list
                var packFileElem = new XElement("PackFile") { Value = packFileName };
                packListDoc.Root.Add(packFileElem);*/

                // Move waves and AWC files
                foreach (var bankElem in bankElems)
                {
                    var name = bankElem.Attribute("name").Value.ToUpper();

                    // Slight hack because it slows things down when checking whether to move files
                    // and only need to it onces per file (instead of for each platform)
                    if (this.platform.ToUpper() == "PC")
                    {
                        SplitUtils.MoveWaves(
                            this.settings,
                            this.assetManager,
                            this.changeList,
                            bankElem,
                            packName,
                            Lock,
                            new List<string> { ".gesture" });
                    }

                    //SplitUtils.MoveAwcFiles(this.settings, this.changeList, this.packName, this.platform, bankElem, packName, Lock);
                }
            }

            // Save changes to pack list file
            //packListDoc.Save(packListFilePath);

            // Delete banks from source file
            /*foreach (var bankElems in newPacks.Values)
            {
                foreach (var bankElem in bankElems)
                {
                    var parent = bankElem.Parent;
                    bankElem.Remove();

                    // Delete parent element(s) if no longer contain child elements
                    SplitUtils.CleanupNode(parent);
                }
            }*/

            /*var fileInfo = new FileInfo(this.inputFile);
            fileInfo.IsReadOnly = false;

            if (null == inputDoc.Root)
            {
                // Delete source file if all banks split out
                var srcAsset = this.changeList.GetAsset(inputFile);
                srcAsset.MarkForDelete();
            }
            else
            {
                // Save changes to source file
                inputDoc.Save(this.inputFile);
            }*/
        }

        /// <summary>
        /// Get latest data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GetLatestData()
        {
            return this.assetManager.GetLatest(this.settings.WaveRootPath, false) && 
                this.assetManager.GetLatest(this.settings.BuiltWavesPath, false) && 
                this.assetManager.GetLatest(this.settings.AwcRootPath, false);
        }
    }
}
