﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LipsyncBuilder
{
    using System;
    using System.IO;
    using System.ServiceProcess;

    /// <summary>
    /// The lipsync builder program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The settings path.
        /// </summary>
        private const string SettingsPath = "lipsync_builder_settings.xml";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            Settings settings;
            if (!InitSettings(out settings))
            {
                Console.WriteLine("Failed to initialize the settings");
                return;
            }

            if (args.Length > 0 && string.Compare(args[0], "-CommandLine", StringComparison.OrdinalIgnoreCase) == 0)
            {
                Console.Out.WriteLine("Running in command line mode");
                var cw = new LipsyncBuilderService(settings);
                cw.Run();

                while (true)
                {
                    System.Threading.Thread.Sleep(100);
                }
            }
            else
            {
                var location = System.Reflection.Assembly.GetExecutingAssembly().Location;
                var path = Path.GetDirectoryName(location);
                Directory.SetCurrentDirectory(path);

                var servicesToRun = new ServiceBase[] { new LipsyncBuilderService(settings) };

                ServiceBase.Run(servicesToRun);
            }
        }

        /// <summary>
        /// Load the settings.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private static bool InitSettings(out Settings settings)
        {
            if (!File.Exists(SettingsPath))
            {
                Console.WriteLine("Failed to find settings file: " + SettingsPath);
                settings = null;
                return false;
            }

            settings = new Settings(SettingsPath);
            return true;
        }
    }
}
