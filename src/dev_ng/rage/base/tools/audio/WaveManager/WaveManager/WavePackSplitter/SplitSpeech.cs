﻿// -----------------------------------------------------------------------
// <copyright file="SplitSpeech.cs" company="Rockstar North">
// Split SPEECH pack.
// </copyright>
// -----------------------------------------------------------------------

namespace WavePackSplitter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Split SPEECH pack.
    /// </summary>
    public class SplitSpeech
    {
        /// <summary>
        /// Flag indicating whether to lock assets when checked out (false for testing).
        /// </summary>
        private const bool Lock = true;

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The pack name.
        /// </summary>
        private readonly string packName;

        /// <summary>
        /// The platform.
        /// </summary>
        private readonly string platform;

        /// <summary>
        /// The input file.
        /// </summary>
        private readonly string inputFile;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The new packs.
        /// </summary>
        private readonly IDictionary<string, IList<XElement>> newPacks;

        /// <summary>
        /// The change list.
        /// </summary>
        private IChangeList changeList;

        /// <summary>
        /// Initializes a new instance of the <see cref="SplitSpeech"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        /// <param name="inputFile">
        /// The input file.
        /// </param>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        public SplitSpeech(Settings settings, IAssetManager assetManager, string inputFile, string packName, string platform)
        {
            this.settings = settings;
            this.assetManager = assetManager;
            this.inputFile = inputFile;
            this.packName = packName;
            this.platform = platform;
            this.newPacks = new Dictionary<string, IList<XElement>>
            {
                { "S_FULL_AMB_M", new List<XElement>() },
                { "S_FULL_AMB_F", new List<XElement>() },
                { "S_FULL_GAN", new List<XElement>() },
                { "S_FULL_SER", new List<XElement>() },
                { "S_MINI_AMB", new List<XElement>() },
                { "S_MINI_GAN", new List<XElement>() },
                { "S_MINI_SER", new List<XElement>() },
                { "S_MISC", new List<XElement>() }
            };
        }

        /// <summary>
        /// The run.
        /// </summary>
        public void Run()
        {
            if (this.assetManager.IsCheckedOut(this.inputFile))
            {
                throw new Exception("File is already checked out: " + this.inputFile);
            }

            if (!this.GetLatestData())
            {
                Console.WriteLine("Failed to get latest data");
                Console.ReadKey();
                return;
            }

            var packListDir = Path.GetDirectoryName(this.inputFile);
            //var packListFilePath = Path.Combine(packListDir, "BUILTWAVES_PACK_LIST.XML");
            //this.changeList.CheckoutAsset(packListFilePath, Lock);
            //var packListDoc = XDocument.Load(packListFilePath);

            var inputDoc = XDocument.Load(this.inputFile);


            // Move bank elements to new packs
            var bankElems = inputDoc.Descendants("Bank");
            foreach (var bankElem in bankElems)
            {
                var bankName = bankElem.Attribute("name").Value.ToUpper();
                var bankFolderElem = bankElem.Ancestors("BankFolder").First();
                var bankFolderName = bankFolderElem.Attribute("name").Value.ToUpper();

                string newPackName = null;

                switch (bankFolderName)
                {
                    case "EMERGENCY_SERVICES":
                        {
                            if (bankName.StartsWith("S_"))
                            {
                                if (bankName.Contains("_MINI_"))
                                {
                                    newPackName = "S_MINI_SER";
                                }
                                else if (bankName.Contains("_FULL_"))
                                {
                                    newPackName = "S_FULL_SER";
                                }
                            }

                            break;
                        }

                    case "FULLPEDS":
                        {
                            if (bankName.StartsWith("A_M"))
                            {
                                newPackName = "S_FULL_AMB_M";
                            }
                            else if (bankName.StartsWith("A_F"))
                            {
                                newPackName = "S_FULL_AMB_F";
                            }
                            else if (bankName.StartsWith("S_")/* && bankName.Contains("_FULL_")*/)
                            {
                                newPackName = "S_FULL_SER";
                            }
                            else if (bankName == "TIME_OF_DEATH_TEST" || bankName == "PRISON_ANNOUNCER")
                            {
                                newPackName = "S_MISC";
                            }

                            break;
                        }

                    case "GANGS":
                        {
                            if (bankName.StartsWith("G_"))
                            {
                                if (bankName.Contains("_MINI_"))
                                {
                                    newPackName = "S_MINI_GAN";
                                }
                                else if (bankName.Contains("_FULL_"))
                                {
                                    newPackName = "S_FULL_GAN";
                                }
                            }

                            break;
                        }

                    case "MINIPEDS":
                        {
                            if (bankName.StartsWith("A_") && bankName.Contains("_MINI_"))
                            {
                                newPackName = "S_MINI_AMB";
                            }
                            else if (bankName.StartsWith("S_") && bankName.Contains("_MINI_"))
                            {
                                newPackName = "S_MINI_SER";
                            }
                            else if (bankName.StartsWith("AIR"))
                            {
                                newPackName = "S_MISC";
                            }

                            break;
                        }

                    default:
                        {
                            newPackName = "S_MISC";
                            break;
                        }
                }

                if (string.IsNullOrEmpty(newPackName))
                {
                    Console.WriteLine("Failed to find new pack for bank '{0}' in bank folder '{1}'", bankName, bankFolderName);
                    Console.ReadKey();
                    return;
                }

                this.newPacks[newPackName].Add(bankElem);
            }

            // Create new pack files
            foreach (var kvp in this.newPacks)
            {
                var newPackName = kvp.Key;
                var currBankElems = kvp.Value;

                this.changeList = this.assetManager.CreateChangeList("Split " + this.packName + " wave pack (new pack: " + newPackName + ")");
                this.changeList.CheckoutAsset(this.inputFile, Lock);

                var outputDoc = new XDocument();
                var rootElem = new XElement("Pack");
                rootElem.SetAttributeValue("name", newPackName);
                outputDoc.Add(rootElem);

                foreach (var bankElem in currBankElems)
                {
                    // Initialize parent element structure up to the bank folder
                    var hierarchy = new List<XElement>();
                    var currElem = bankElem.Parent;
                    while (null != currElem)
                    {
                        if (null != currElem && currElem.Name == "Pack")
                        {
                            // Reached the top of required parent hierarchy
                            break;
                        }

                        hierarchy.Add(currElem);
                        currElem = currElem.Parent;
                    }

                    var parent = SplitUtils.CloneHierarchy(rootElem, hierarchy);

                    // Clone bank element and children and add to new document
                    var newElem = new XElement(bankElem);
                    parent.Add(newElem);
                }

                // Save new pack file
                var packFileName = newPackName + "_PACK_FILE.xml";
                var filePath = Path.Combine(packListDir, packFileName);
                outputDoc.Save(filePath);
                this.changeList.MarkAssetForAdd(filePath);

                // Add pack file name to pack list
                //var packFileElem = new XElement("PackFile") { Value = packFileName };
                //packListDoc.Root.Add(packFileElem);
                //packListDoc.Save(packListFilePath);

                // Move waves and AWC files
                foreach (var bankElem in currBankElems)
                {
                    // Slight hack because it slows things down when checking whether to move files
                    // and only need to it onces per file (instead of for each platform)
                    if (this.platform.ToUpper() == "PC")
                    {
                        SplitUtils.MoveWaves(
                            this.settings,
                            this.assetManager,
                            this.changeList,
                            bankElem,
                            newPackName,
                            Lock,
                            new List<string> { ".gesture" });
                    }

                    SplitUtils.MoveAwcFiles(this.settings, this.changeList, this.packName, this.platform, bankElem, newPackName, Lock);
                }

                if (!this.changeList.Submit())
                {
                    Console.WriteLine("Failed to submit change list");
                }
            }

            // Delete banks from source file
            foreach (var currBankElem in this.newPacks.Values)
            {
                foreach (var bankElem in currBankElem)
                {
                    var parent = bankElem.Parent;
                    bankElem.Remove();

                    // Delete parent element(s) if no longer contain child elements
                    SplitUtils.CleanupNode(parent);
                }
            }

            var fileInfo = new FileInfo(this.inputFile);
            fileInfo.IsReadOnly = false;

            if (null == inputDoc.Root)
            {
                // Delete source file if all banks split out
                //var srcAsset = this.changeList.GetAsset(this.inputFile);
                //srcAsset.MarkForDelete();
            }
            else
            {
                // Save changes to source file
                inputDoc.Save(this.inputFile);
            }
        }

        /// <summary>
        /// Get latest data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GetLatestData()
        {
            var builtWavesPath = Regex.Replace(
                this.settings.BuiltWavesPath, "{platform}", this.platform, RegexOptions.IgnoreCase);

            var awcRootPath = Regex.Replace(
                this.settings.AwcRootPath, "{platform}", this.platform, RegexOptions.IgnoreCase);

            return this.assetManager.GetLatest(this.settings.WaveRootPath, false) &&
                this.assetManager.GetLatest(builtWavesPath, false) &&
                this.assetManager.GetLatest(awcRootPath, false);
        }
    }
}
