﻿// -----------------------------------------------------------------------
// <copyright file="BankRebuild.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace WaveManager.WaveTools
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// Bank rebuilder.
    /// </summary>
    public class BankRebuilder
    {
        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The list of rebuilt banks per platform.
        /// </summary>
        private readonly IDictionary<string, ISet<string>> rebuiltBanks;

        /// <summary>
        /// The list of processing banks per platform.
        /// </summary>
        private readonly IDictionary<string, ISet<string>> processingBanks;

        /// <summary>
        /// The data save path.
        /// </summary>
        private readonly string dataSavePath;

        /// <summary>
        /// The process started date/time.
        /// </summary>
        private DateTime processStartTime;

        /// <summary>
        /// The sync change list number.
        /// </summary>
        private string syncChangeListNumber;

        /// <summary>
        /// The pending waves paths.
        /// </summary>
        private IDictionary<string, string> pendingWavesPaths;

        /// <summary>
        /// The built waves paths.
        /// </summary>
        private IDictionary<string, string> builtWavesPaths;

        /// <summary>
        /// Initializes a new instance of the <see cref="BankRebuilder"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        public BankRebuilder(Settings settings)
        {
            this.settings = settings;
            
            this.dataSavePath = Path.Combine(Path.GetTempPath(), "BankRebuilder_{pack}_{platform}.txt");

            this.assetManager = AssetManagerFactory.GetInstance(
                AssetManagerType.Perforce,
                settings.P4User,
                settings.P4Port,
                settings.P4Workspace,
                settings.P4User,
                settings.P4Password,
                settings.P4DepotRoot);

            this.rebuiltBanks = new Dictionary<string, ISet<string>>();
            this.processingBanks = new Dictionary<string, ISet<string>>();

            foreach (var platform in this.settings.Platforms)
            {
                this.rebuiltBanks[platform] = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                this.processingBanks[platform] = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            }
        }

        /// <summary>
        /// Run the bank builder
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Run()
        {
            this.processStartTime = DateTime.Now;

            if (string.IsNullOrEmpty(this.settings.Pack))
            {
                Console.WriteLine("Pack name required");
                return false;
            }

            this.InitPaths();

            if (!this.LoadData())
            {
                Console.WriteLine("Failed to load saved data: " + this.dataSavePath);
                return false;
            }

            foreach (var platform in this.settings.Platforms)
            {
                var banksRemaining = true;

                while (banksRemaining)
                {
                    // Check max duration has not been exceeded
                    if (null != this.settings.BankRebuilderMaxRunMins)
                    {
                        var now = DateTime.Now;
                        var duration = now - this.processStartTime;

                        if (duration.TotalMinutes >= this.settings.BankRebuilderMaxRunMins)
                        {
                            Console.WriteLine(
                                "Process has exceeded max runtime duration of {0} minute(s) - saving current state and exiting", 
                                this.settings.BankRebuilderMaxRunMins);

                            return true;
                        }
                    }

                    // Get latest data before each update/build
                    if (!this.GetLatestData())
                    {
                        Console.WriteLine("Failed to get latest data");
                        return false;
                    }

                    // Build any current pending changes to pack
                    if (!this.BuildPack(this.settings.Pack, platform))
                    {
                        Console.WriteLine("Failed to build pack: " + this.settings.Pack);
                        return false;
                    }

                    // Create new change list
                    var changeList =
                        this.assetManager.CreateChangeList(
                            "Wave Rebuild of " + this.settings.Pack + " pack [" + platform + "]");

                    // Mark next batch of banks for rebuild
                    if (!this.MarkBanksForRebuild(changeList, platform, this.settings.Pack, this.settings.MaxBanksPerBatch, out banksRemaining))
                    {
                        Console.WriteLine("Failed to mark banks for rebuild: " + this.settings.Pack);
                        return false;
                    }

                    changeList.RevertUnchanged();

                    if (changeList.Assets.Any())
                    {
                        // Submit pending wave changes
                        if (!changeList.Submit())
                        {
                            Console.WriteLine("Failed to submit change list: " + changeList.NumberAsString);
                            return false;
                        }
                    }
                    else
                    {
                        changeList.Delete();
                    }

                    // Build pending changes to pack
                    if (!this.BuildPack(this.settings.Pack, platform))
                    {
                        Console.WriteLine("Failed to build pack: " + this.settings.Pack);
                        return false;
                    }

                    // Move banks from processing list to rebuilt list
                    foreach (var bank in this.processingBanks[platform])
                    {
                        this.rebuiltBanks[platform].Add(bank);
                    }

                    this.processingBanks[platform].Clear();

                    if (!this.SaveData())
                    {
                        Console.WriteLine("Failed to save data to file: " + this.dataSavePath);
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// The get latest data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GetLatestData()
        {
            this.syncChangeListNumber = this.assetManager.GetLatestChangeListNumber().ToString(CultureInfo.InvariantCulture);

            foreach (var platform in this.settings.Platforms)
            {
                if (!this.assetManager.GetLatest(this.pendingWavesPaths[platform], false))
                {
                    return false;
                }

                if (!this.assetManager.GetLatest(this.builtWavesPaths[platform], false))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Initialize the paths.
        /// </summary>
        private void InitPaths()
        {
            this.pendingWavesPaths = new Dictionary<string, string>();
            this.builtWavesPaths = new Dictionary<string, string>();

            foreach (var platform in this.settings.Platforms)
            {
                var pendingWavesPath = Regex.Replace(
                    this.settings.PendingWavesPath, "{platform}", platform, RegexOptions.IgnoreCase);

                this.pendingWavesPaths.Add(platform, this.assetManager.GetLocalPath(pendingWavesPath));

                var builtWavesPath = Regex.Replace(
                    this.settings.BuiltWavesPath, "{platform}", platform, RegexOptions.IgnoreCase);

                this.builtWavesPaths.Add(platform, this.assetManager.GetLocalPath(builtWavesPath));
            }
        }

        /// <summary>
        /// Load any saved data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadData()
        {
            // Load any saved data from previous run if continuing wave build
            if (this.settings.WaveRebuildContinue)
            {
                foreach (var platform in this.settings.Platforms)
                {
                    var platformDataPath = Regex.Replace(
                        this.dataSavePath, "{pack}", this.settings.Pack, RegexOptions.IgnoreCase);

                    platformDataPath = Regex.Replace(
                        platformDataPath, "{platform}", platform, RegexOptions.IgnoreCase);

                    if (File.Exists(platformDataPath))
                    {
                        // Load most recently updated data file
                        var file = new StreamReader(platformDataPath);
                        string line;
                        while (null != (line = file.ReadLine()))
                        {
                            if (!string.IsNullOrEmpty(line.Trim()))
                            {
                                if (!this.rebuiltBanks.ContainsKey(platform))
                                {
                                    this.rebuiltBanks.Add(platform, new HashSet<string>());
                                }

                                this.rebuiltBanks[platform].Add(line.Trim());
                            }
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Save current state to continue from this point on next run.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool SaveData()
        {
            foreach (var platform in this.settings.Platforms)
            {
                var platformDataPath = Regex.Replace(
                         this.dataSavePath, "{pack}", this.settings.Pack, RegexOptions.IgnoreCase);

                platformDataPath = Regex.Replace(
                    platformDataPath, "{platform}", platform, RegexOptions.IgnoreCase);

                var writer = new StreamWriter(platformDataPath);
                foreach (var bank in this.rebuiltBanks[platform])
                {
                    writer.WriteLine(bank);
                }

                writer.Close();
            }

            return true;
        }

        /// <summary>
        /// Build pack.
        /// </summary>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool BuildPack(string packName, string platform)
        {
            var pendingWavesPath = Path.Combine(this.pendingWavesPaths[platform], packName + ".xml");
            if (File.Exists(pendingWavesPath))
            {
                var changeList =
                    this.assetManager.CreateChangeList("[WaveManager] Wave Build - " + 
                    platform + " (" + packName + ") @ CL " + this.syncChangeListNumber);

                var args = this.GetWaveBuildArgs(changeList, platform, packName);

                var builderPath = this.assetManager.GetLocalPath(this.settings.AudioDataBuilderPath);

                var process =
                   Process.Start(
                       new ProcessStartInfo(builderPath, args)
                       {
                           UseShellExecute = false,
                           CreateNoWindow = false,
                           RedirectStandardOutput = false
                       });
                process.WaitForExit();

                if (process.ExitCode != 0)
                {
                    Console.WriteLine("Failed to build pack " + packName + " for platform " + platform);
                    return false;
                }

                changeList.RevertUnchanged();
                if (changeList.Assets.Any())
                {
                    return changeList.Submit();
                }
                
                changeList.Delete();
            }

            return true;
        }

        /// <summary>
        /// The get wave build args.
        /// </summary>
        /// <param name="changeList">
        /// The change list to add the pending changes to.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="pack">
        /// The pack.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetWaveBuildArgs(IChangeList changeList, string platform, string pack)
        {
            var builder = new StringBuilder();
            builder.Append("-ASSETSERVER ");
            builder.Append(this.settings.P4Port);
            builder.Append(" -ASSETPROJECT ");
            builder.Append(this.settings.P4Workspace);
            builder.Append(" -ASSETUSERNAME ");
            builder.Append(this.settings.P4User);
            if (!string.IsNullOrEmpty(this.settings.P4Password))
            {
                builder.Append(" -ASSETPASSWORD ");
                builder.Append(this.settings.P4Password);
            }

            builder.Append(" -DEPOTROOT ");
            builder.Append(this.settings.P4DepotRoot);
            builder.Append(" -PROJECTSETTINGS ");
            builder.Append(this.assetManager.GetLocalPath(this.settings.ProjectSettingsPath));
            builder.Append(" -PLATFORM ");
            builder.Append(platform);
            builder.Append(" -CHANGE ");
            builder.Append(changeList.NumberAsString);
            builder.Append(" -SYNCNUMBER ");
            builder.Append(this.syncChangeListNumber);
            builder.Append(" -STAY_OPEN_ON_ERROR true");

            if (!string.IsNullOrWhiteSpace(pack))
            {
                builder.Append(" -PACK ");
                builder.Append(pack);
            }

            return builder.ToString();
        }

        /// <summary>
        /// Mark banks in a pack for rebuild.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <param name="maxBanks">
        /// The max banks to rebuild in each batch.
        /// </param>
        /// <param name="banksRemaining">
        /// Value indicating if there are banks remaining to be processed after this run.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool MarkBanksForRebuild(IChangeList changeList, string platform, string packName, uint? maxBanks, out bool banksRemaining)
        {
            banksRemaining = true;

            var pendingWavesPath = Path.Combine(this.pendingWavesPaths[platform], packName + ".xml");
            var builtWavesPath = Path.Combine(this.builtWavesPaths[platform], packName + "_pack_file.xml");

            if (File.Exists(pendingWavesPath))
            {
                Console.WriteLine("Pending waves file already exists - must be built before banks marked for rebuild: " + pendingWavesPath);
                return false;
            }

            if (!File.Exists(builtWavesPath))
            {
                Console.WriteLine("Failed to find built waves file: " + builtWavesPath);
                return false;
            }

            var builtWavesDoc = XDocument.Load(builtWavesPath);
            var bankElems = builtWavesDoc.Descendants(XName.Get("Bank"));
            var banksElemsToRebuild = new List<XElement>();

            foreach (var bankElem in bankElems)
            {
                if (null != maxBanks && banksElemsToRebuild.Count >= maxBanks)
                {
                    // Reached the limit for this batch of banks to rebuild
                    break;
                }
                    
                var nameAttr = bankElem.Attribute("name");
                var name = nameAttr.Value;

                if (this.rebuiltBanks[platform].Contains(name) || this.processingBanks[platform].Contains(name))
                {
                    // Bank has already been rebuilt, or is being processed
                    continue;
                }

                this.processingBanks[platform].Add(name);
                banksElemsToRebuild.Add(bankElem);
            }

            if (banksElemsToRebuild.Any())
            {
                changeList.MarkAssetForAdd(pendingWavesPath);

                if (!this.WritePendingWaves(banksElemsToRebuild, pendingWavesPath))
                {
                    Console.WriteLine("Failed to write pending wave changes to file: " + pendingWavesPath);
                    return false;
                }
            }
            else
            {
                banksRemaining = false;
            }

            return true;
        }

        /// <summary>
        /// Write bank rebuild entries into pending waves.
        /// </summary>
        /// <param name="bankElems">
        /// The bank elements.
        /// </param>
        /// <param name="pendingWavesPath">
        /// The pending waves path.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool WritePendingWaves(IEnumerable<XElement> bankElems, string pendingWavesPath)
        {
            var doc = new XDocument();
            var pendingWavesElem = new XElement("PendingWaves");
            doc.Add(pendingWavesElem);

            foreach (var bankElem in bankElems)
            {
                // Get hierarchy structure of bank
                var parentList = new List<XElement> { bankElem };
                var currParent = bankElem.Parent;
                while (null != currParent)
                {
                    // Stop when we hit PendingWaves element
                    if (currParent.Name.LocalName != "PendingWaves")
                    {
                        parentList.Insert(0, currParent);
                    }

                    currParent = currParent.Parent;
                }

                // Insert cloned bank element into pending waves
                var currElem = doc.Root;
                foreach (var elem in parentList)
                {
                    var existingElem = this.FindExistingElement(currElem, elem);
                    if (null == existingElem)
                    {
                        existingElem = new XElement(elem);
                        existingElem.RemoveNodes();

                        if (existingElem.Name.LocalName == "Bank")
                        {
                            // Add rebuild tag to Bank
                            var tagElem = new XElement("Tag");
                            tagElem.SetAttributeValue("name", "rebuild");
                            tagElem.SetAttributeValue("operation", "add");
                            existingElem.Add(tagElem);
                        }

                        currElem.Add(existingElem);
                    }

                    currElem = existingElem;
                }
            }
            
            doc.Save(pendingWavesPath);
            return true;
        }

        /// <summary>
        /// Determine whether element already exists in new structure.
        /// (based on element type and name).
        /// </summary>
        /// <param name="parentElem">
        /// The parent element.
        /// </param>
        /// <param name="elem">
        /// The element to find.
        /// </param>
        /// <returns>
        /// Existing element <see cref="bool"/>.
        /// </returns>
        private XElement FindExistingElement(XElement parentElem, XElement elem)
        {
            foreach (var childElem in parentElem.Elements())
            {
                if (childElem.Name.LocalName != elem.Name.LocalName)
                {
                    continue;
                }

                var childNameAttr = childElem.Attribute("name");
                var elemNameAttr = elem.Attribute("name");

                if (null == childNameAttr && null == elemNameAttr)
                {
                    return childElem;
                }

                if (null == childNameAttr || null == elemNameAttr)
                {
                    continue;
                }

                var childName = childNameAttr.Value;
                var elemName = elemNameAttr.Value;

                if (childName == elemName)
                {
                    return childElem;
                }
            }

            return null;
        }
    }
}
