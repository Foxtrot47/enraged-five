﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace LipsyncBuilder
{
    public class VisemeBuilderInvoker
    {
        private readonly string visemeBuilderLocalPath;
        private readonly string visemeBuilderWorkingLocalPath;

        private StringBuilder errors = new StringBuilder();
        public StringBuilder Errors
        {
            get { return errors; }
        }

        private string args;
        public string Args
        {
            get{ return args; }
            private set{ args = value; }
        }

        public VisemeBuilderInvoker(string visemeBuilderLocalPath, string visemeBuilderWorkingLocalPath, string args) {
            this.visemeBuilderLocalPath = visemeBuilderLocalPath;
            this.visemeBuilderWorkingLocalPath = visemeBuilderWorkingLocalPath;
            this.Args = args;
        }

        public bool Invoke()
        {
            var processInfo = new ProcessStartInfo(this.visemeBuilderLocalPath, args)
            {
                UseShellExecute = false,
                CreateNoWindow = true,
                WorkingDirectory = this.visemeBuilderWorkingLocalPath,
                RedirectStandardError = true,
                RedirectStandardOutput = true,
            };

            Process lipsyncProcess = new Process();
            lipsyncProcess.StartInfo = processInfo;
            lipsyncProcess.OutputDataReceived += new DataReceivedEventHandler(lipsyncProcess_OutputDataReceived);
            lipsyncProcess.ErrorDataReceived += new DataReceivedEventHandler(lipsyncProcess_ErrorDataReceived);

            lipsyncProcess.Start();
            lipsyncProcess.EnableRaisingEvents = true;
            lipsyncProcess.BeginOutputReadLine();
            lipsyncProcess.BeginErrorReadLine();


            lipsyncProcess.WaitForExit();

            if (lipsyncProcess.ExitCode != 0)
            {
                this.errors.AppendLine(string.Format(this.visemeBuilderLocalPath + " failed with exit code " + lipsyncProcess.ExitCode + ". Args were " + string.Join(" ", args)));
            }

            return lipsyncProcess.ExitCode == 0;
        }

        void lipsyncProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data == null) return;
            string error = e.Data;
            error = error.Trim();
            Console.WriteLine(error);
            this.errors.AppendLine(error);
        }

        void lipsyncProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data == null) return;
            Console.WriteLine(e.Data);
        }
    }
}
