﻿// -----------------------------------------------------------------------
// <copyright file="Settings.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace LipsyncBuilder
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Mail;
    using System.Xml.Linq;

    /// <summary>
    /// Lipsync builder settings.
    /// </summary>
    public class Settings
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        /// <param name="settingsPath">
        /// The path to the settings file.
        /// </param>
        public Settings(string settingsPath)
        {
            if (!File.Exists(settingsPath))
            {
                throw new Exception("Failed to find settings file: " + settingsPath);
            }
            
            this.Platforms = new List<string>();
            this.EmailRecipients = new List<string>();

            var doc = XDocument.Load(settingsPath);

            foreach (var elem in doc.Root.Elements())
            {
                switch (elem.Name.LocalName)
                {
                    case "AssetManager":
                        {
                            this.ParseAssetManagerSettings(elem);
                            break;
                        }

                    case "Platform":
                        {
                            this.Platforms.Add(elem.Value);
                            break;
                        }

                    case "PendingWavesDepotPath":
                        {
                            this.PendingWavesDepotPath = elem.Value;
                            break;
                        }

                    case "BuiltWavesDepotPath":
                        {
                            this.BuiltWavesDepotPath = elem.Value;
                            break;
                        }

                    case "AudioDataBuilderDepotPath":
                        {
                            this.AudioDataBuilderDepotPath = elem.Value;
                            break;
                        }

                    case "VisemeBuilderWorkingDepotPath":
                        {
                            this.VisemeBuilderWorkingDepotPath = elem.Value;
                            break;
                        }

                    case "VisemeBuilderDepotPath":
                        {
                            this.VisemeBuilderDepotPath = elem.Value;
                            break;
                        }

                    case "WatchDepotPath":
                        {
                            this.WatchDepotPath = elem.Value;
                            break;
                        }

                    case "ProcessedPath":
                        {
                            this.ProcessedPath = elem.Value;
                            break;
                        }

                    case "LogfilePath":
                        {
                            this.LogfilePath = elem.Value;
                            break;
                        }

                    case "WavesFolderLocalPath":
                        {
                            this.WavesFolderLocalPath = elem.Value;
                            break;
                        }

                    case "FaceFxExeLocalPath":
                        {
                            this.FaceFxExeLocalPath = elem.Value;
                            break;
                        }

                    case "PendingBankBuildPath":
                        {
                            this.PendingBankBuildPath = elem.Value;
                            break;
                        }

                    case "ProjectSettingsDepotPath":
                        {
                            this.ProjectSettingsDepotPath = elem.Value;
                            break;
                        }

                    case "ModuleExeDepotPath":
                        {
                            this.ModuleExeDepotPath = elem.Value;
                            break;
                        }

                    case "MaxRetryAttempts":
                        {
                            int max;
                            if (!int.TryParse(elem.Value, out max))
                            {
                                throw new Exception("Failed to parse MaxRetryAttempts: " + elem.Value);
                            }

                            this.MaxRetryAttempts = max;
                            break;
                        }

                    case "RefreshFrequency":
                        {
                            int frequency;
                            if (!int.TryParse(elem.Value, out frequency))
                            {
                                throw new Exception("Failed to parse RefreshFrequency: " + elem.Value);
                            }

                            this.RefreshFrequency = frequency;
                            break;
                        }

                    case "SmtpClient":
                        {
                            this.SmtpClient = new SmtpClient(elem.Value);
                            break;
                        }

                    case "EmailFrom":
                        {
                            this.EmailFrom = elem.Value;
                            break;
                        }

                    case "EmailRecipient":
                        {
                            this.EmailRecipients.Add(elem.Value);
                            break;
                        }
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the platforms.
        /// </summary>
        public IList<string> Platforms { get; private set; }

        /// <summary>
        /// Gets the pending waves depot path.
        /// </summary>
        public string PendingWavesDepotPath { get; private set; }

        /// <summary>
        /// Gets the built waves depot path.
        /// </summary>
        public string BuiltWavesDepotPath { get; private set; }

        /// <summary>
        /// Gets the audio data builder depot path.
        /// </summary>
        public string AudioDataBuilderDepotPath { get; private set; }

        /// <summary>
        /// Gets the Viseme builder working path.
        /// </summary>
        public string VisemeBuilderWorkingDepotPath { get; private set; }

        /// <summary>
        /// Gets the Viseme builder path.
        /// </summary>
        public string VisemeBuilderDepotPath { get; private set; }

        /// <summary>
        /// Gets the watch depot path.
        /// </summary>
        public string WatchDepotPath { get; private set; }

        /// <summary>
        /// Gets the processed path.
        /// </summary>
        public string ProcessedPath { get; private set; }

        /// <summary>
        /// Gets the logfile path.
        /// </summary>
        public string LogfilePath { get; private set; }

        /// <summary>
        /// Gets the waves folder local path.
        /// </summary>
        public string WavesFolderLocalPath { get; private set; }

        /// <summary>
        /// Gets the faceFX exe local path.
        /// </summary>
        public string FaceFxExeLocalPath { get; private set; }

        /// <summary>
        /// Gets the pending bank build path.
        /// </summary>
        public string PendingBankBuildPath { get; private set; }

        /// <summary>
        /// Gets the project settings depot path.
        /// </summary>
        public string ProjectSettingsDepotPath { get; private set; }

        /// <summary>
        /// Gets the module exe depot path.
        /// </summary>
        public string ModuleExeDepotPath { get; private set; }

        /// <summary>
        /// Gets the max retry attempts.
        /// </summary>
        public int MaxRetryAttempts { get; private set; }

        /// <summary>
        /// Gets the refresh frequency.
        /// </summary>
        public int RefreshFrequency { get; private set; }

        /// <summary>
        /// Gets the SMTP client.
        /// </summary>
        public SmtpClient SmtpClient { get; private set; }

        /// <summary>
        /// Gets the SMTP from.
        /// </summary>
        public string EmailFrom { get; private set; }

        /// <summary>
        /// Gets the email recipients.
        /// </summary>
        public IList<string> EmailRecipients { get; private set; }

        /// <summary>
        /// Gets the P4 host.
        /// </summary>
        public string P4Host { get; private set; }

        /// <summary>
        /// Gets the P4 port.
        /// </summary>
        public string P4Port { get; private set; }

        /// <summary>
        /// Gets the P4 workspace.
        /// </summary>
        public string P4Workspace { get; private set; }

        /// <summary>
        /// Gets the P4 user.
        /// </summary>
        public string P4User { get; private set; }

        /// <summary>
        /// Gets the P4 password.
        /// </summary>
        public string P4Password { get; private set; }

        /// <summary>
        /// Gets the P4 depot root.
        /// </summary>
        public string P4DepotRoot { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Parse the asset manager settings.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private void ParseAssetManagerSettings(XElement node)
        {
            foreach (var elem in node.Elements())
            {
                switch (elem.Name.LocalName)
                {
                    case "P4Host":
                        this.P4Host = elem.Value;
                        break;
                    case "P4Port":
                        this.P4Port = elem.Value;
                        break;
                    case "P4Workspace":
                        this.P4Workspace = elem.Value;
                        break;
                    case "P4User":
                        this.P4User = elem.Value;
                        break;
                    case "P4Password":
                        this.P4Password = elem.Value;
                        break;
                    case "P4DepotRoot":
                        this.P4DepotRoot = elem.Value;
                        break;
                }
            }
        }

        #endregion
    }
}
