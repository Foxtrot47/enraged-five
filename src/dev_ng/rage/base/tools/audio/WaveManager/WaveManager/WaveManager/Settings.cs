﻿// -----------------------------------------------------------------------
// <copyright file="Settings.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace WaveManager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using WaveManager.Enums;

    /// <summary>
    /// Wave manager settings.
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public Settings(string[] args)
        {
            this.Platforms = new List<string>();
            this.LoadSettings();
            this.ParseArgs(args);

            // Add all platforms if no platform specified in args
            if (!this.Platforms.Any())
            {
                this.Platforms.Add("PS3");
                this.Platforms.Add("XBOX360");
                this.Platforms.Add("PC");
            }
        }

        /// <summary>
        /// Gets the platforms.
        /// </summary>
        public IList<string> Platforms { get; private set; }

        /// <summary>
        /// Gets the pack.
        /// </summary>
        public string Pack { get; private set; }

        /// <summary>
        /// Gets the bank rebuilder max run mins.
        /// </summary>
        public uint? BankRebuilderMaxRunMins { get; private set; }

        /// <summary>
        /// Gets the max banks per batch.
        /// </summary>
        public uint? MaxBanksPerBatch { get; private set; }

        /// <summary>
        /// Gets the P4 host.
        /// </summary>
        public string P4Host { get; private set; }

        /// <summary>
        /// Gets the P4 port.
        /// </summary>
        public string P4Port { get; private set; }

        /// <summary>
        /// Gets the P4 workspace.
        /// </summary>
        public string P4Workspace { get; private set; }

        /// <summary>
        /// Gets the P4 user.
        /// </summary>
        public string P4User { get; private set; }

        /// <summary>
        /// Gets the P4 password.
        /// </summary>
        public string P4Password { get; private set; }

        /// <summary>
        /// Gets the P4 depot root.
        /// </summary>
        public string P4DepotRoot { get; private set; }

        /// <summary>
        /// Gets the pending waves path.
        /// </summary>
        public string PendingWavesPath { get; private set; }

        /// <summary>
        /// Gets the built waves path.
        /// </summary>
        public string BuiltWavesPath { get; private set; }

        /// <summary>
        /// Gets the audio data builder path.
        /// </summary>
        public string AudioDataBuilderPath { get; private set; }

        /// <summary>
        /// Gets the project settings path.
        /// </summary>
        public string ProjectSettingsPath { get; private set; }

        /// <summary>
        /// Gets a value indicating whether wave rebuild is required.
        /// </summary>
        public bool WaveRebuild { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to continue wave rebuild from previous point.
        /// </summary>
        public bool WaveRebuildContinue { get; private set; }

        /// <summary>
        /// The load settings.
        /// </summary>
        private void LoadSettings()
        {
            var doc = XDocument.Load("settings.xml");
            var settingsElem = doc.Element("Settings");
            foreach (var elem in settingsElem.Elements())
            {
                switch (elem.Name.LocalName)
                {
                    case "AssetManager":
                        {
                            this.LoadAssetManagerSettings(elem);
                            break;
                        }

                    case "PendingWavesPath":
                        {
                            this.PendingWavesPath = elem.Value;
                            break;
                        }

                    case "BuiltWavesPath":
                        {
                            this.BuiltWavesPath = elem.Value;
                            break;
                        }

                    case "AudioDataBuilderPath":
                        {
                            this.AudioDataBuilderPath = elem.Value;
                            break;
                        }

                    case "ProjectSettingsPath":
                        {
                            this.ProjectSettingsPath = elem.Value;
                            break;
                        }

                    case "BankRebuilderMaxRunMins":
                        {
                            uint value;
                            if (!uint.TryParse(elem.Value, out value))
                            {
                                throw new Exception("Invalid value for BankRebuilderMaxRunMins: " + elem.Value);
                            }

                            this.BankRebuilderMaxRunMins = value;

                            break;
                        }

                    case "MaxBanksPerBatch":
                        {
                            uint value;
                            if (!uint.TryParse(elem.Value, out value))
                            {
                                throw new Exception("Invalid value for MaxBanksPerBatch: " + elem.Value);
                            }

                            this.MaxBanksPerBatch = value;

                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Load the asset manager settings.
        /// </summary>
        /// <param name="assetManagerElem">
        /// The asset manager element.
        /// </param>
        private void LoadAssetManagerSettings(XElement assetManagerElem)
        {
            foreach (var elem in assetManagerElem.Elements())
            {
                switch (elem.Name.LocalName)
                {
                    case "P4User":
                        {
                            this.P4User = elem.Value;
                            break;
                        }

                    case "P4Workspace":
                        {
                            this.P4Workspace = elem.Value;
                            break;
                        }

                    case "P4Host":
                        {
                            this.P4Host = elem.Value;
                            break;
                        }

                    case "P4Port":
                        {
                            this.P4Port = elem.Value;
                            break;
                        }

                    case "P4Password":
                        {
                            this.P4Password = elem.Value;
                            break;
                        }

                    case "P4DepotRoot":
                        {
                            this.P4DepotRoot = elem.Value;
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Parse the runtime args.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        private void ParseArgs(string[] args)
        {
            for (var i = 0; i < args.Length; i++)
            {
                var arg = args[i].ToLower();
                switch (arg)
                {
                    case "-action":
                        {
                            WaveActions action;
                            if (!Enum.TryParse(args[++i], true, out action))
                            {
                                throw new Exception("Invalid wave action: " + args[i]);
                            }

                            switch (action)
                            {
                                case WaveActions.BankRebuild:
                                    {
                                        this.WaveRebuild = true;
                                        break;
                                    }

                                case WaveActions.BankRebuildContinue:
                                    {
                                        this.WaveRebuildContinue = true;
                                        break;
                                    }
                            }

                            break;
                        }

                    case "-pack":
                        {
                            this.Pack = args[++i].ToUpper();
                            break;
                        }

                    case "-platform":
                        {
                            this.Platforms.Add(args[++i].ToUpper());
                            break;
                        }

                    default:
                        {
                            throw new Exception("Invalid arg: " + arg);
                        }
                }
            }
        }
    }
}
