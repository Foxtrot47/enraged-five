﻿// -----------------------------------------------------------------------
// <copyright file="SplitUtils.cs" company="Rockstar North">
// Utilities for packs splitting.
// </copyright>
// -----------------------------------------------------------------------

namespace WavePackSplitter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Utilities for packs splitting.
    /// </summary>
    public class SplitUtils
    {
        /// <summary>
        /// Get new pack name.
        /// </summary>
        /// <param name="ranges">
        /// The ranges.
        /// </param>
        /// <param name="bankName">
        /// The bank name.
        /// </param>
        /// <returns>
        /// The new pack name <see cref="string"/>.
        /// </returns>
        public static string GetNewPackName(IList<IList<string>> ranges, string bankName)
        {
            var prefix = bankName.ToUpper().Contains("PLACEHOLDER") ? "PS_" : "SS_";

            var initial = bankName[0].ToString().ToLower();
            foreach (var range in ranges)
            {
                if (range.Contains(initial))
                {
                    return (prefix + range[0] + range[range.Count - 1]).ToUpper();
                }
            }

            return null;
        }

        /// <summary>
        /// Clone the hierarchy.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="hierarchy">
        /// The hierarchy.
        /// </param>
        /// <returns>
        /// The last element in cloned hierarchy <see cref="XElement"/>.
        /// </returns>
        public static XElement CloneHierarchy(XElement parent, IList<XElement> hierarchy)
        {
            if (!hierarchy.Any())
            {
                return parent;
            }

            var currElem = hierarchy.Last();
            var currElemName = currElem.Attribute("name").Value;
            XElement foundElem = null;
            foreach (var childElem in parent.Elements(currElem.Name))
            {
                var childElemName = childElem.Attribute("name").Value;
                if (childElemName == currElemName)
                {
                    foundElem = childElem;
                }
            }

            if (null == foundElem)
            {
                foundElem = new XElement(currElem);

                // Remove all non-tag children
                foundElem.Elements().Where(childElem => childElem.Name != "Tag").Remove();
                parent.Add(foundElem);
            }

            hierarchy.Remove(currElem);
            return CloneHierarchy(foundElem, hierarchy);
        }

        /// <summary>
        /// Cleanup nodes.
        /// </summary>
        /// <param name="elem">
        /// The current element.
        /// </param>
        public static void CleanupNode(XElement elem)
        {
            if (null == elem)
            {
                return;
            }

            if (elem.Elements().All(e => e.Name == "Tag"))
            {
                var parent = elem.Parent;
                elem.Remove();
                CleanupNode(parent);
            }
        }

        /// <summary>
        /// Move waves to their new location.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="bankElem">
        /// The bank element.
        /// </param>
        /// <param name="newPackName">
        /// The new pack name.
        /// </param>
        /// <param name="lockAssets">
        /// The lock assets flag.
        /// </param>
        /// <param name="moveOtherExtFiles">
        /// List of extensions of files to move along with wave in same location, if file exists.
        /// </param>
        public static void MoveWaves(Settings settings, IAssetManager assetManager, IChangeList changeList, XElement bankElem, string newPackName, bool lockAssets = true, IList<string> moveOtherExtFiles = null)
        {
            foreach (var waveElem in bankElem.Descendants("Wave"))
            {
                var srcPath = string.Empty;
                var destPath = string.Empty;
                var currElem = waveElem;
                while (null != currElem)
                {
                    var name = currElem.Attribute("name").Value;
                    srcPath = Path.Combine(name, srcPath);

                    var destElemName = currElem.Name == "Pack" ? newPackName : name;
                    destPath = Path.Combine(destElemName, destPath);
                    currElem = currElem.Parent;
                }

                srcPath = Path.Combine(settings.WaveRootPath, srcPath);
                destPath = Path.Combine(settings.WaveRootPath, destPath);

                // May have already moved the wave file for another platform
                if (assetManager.ExistsAsAsset(srcPath))
                {
                    var waveAsset = changeList.CheckoutAsset(srcPath, lockAssets);
                    if (!changeList.MoveAsset(waveAsset, destPath))
                    {
                        Console.WriteLine("Failed to move wave asset: " + srcPath);
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                    }
                }

                if (null != moveOtherExtFiles)
                {
                    var srcDir = Path.GetDirectoryName(srcPath);
                    var srcFileNoExt = Path.GetFileNameWithoutExtension(srcPath);
                    var noExtSrcPath = Path.Combine(srcDir, srcFileNoExt).Replace('\\', '/'); ;

                    var destDir = Path.GetDirectoryName(destPath);
                    var destFileNoExt = Path.GetFileNameWithoutExtension(destPath);
                    var noExtDestPath = Path.Combine(destDir, destFileNoExt).Replace('\\', '/');

                    foreach (var ext in moveOtherExtFiles)
                    {
                        var extFilePath = noExtSrcPath + ext;
                        if (assetManager.ExistsAsAsset(extFilePath))
                        {
                            var extDestFilePath = noExtDestPath + ext;
                            var extAsset = changeList.CheckoutAsset(extFilePath, lockAssets);
                            if (!changeList.MoveAsset(extAsset, extDestFilePath))
                            {
                                Console.WriteLine("Failed to move asset: " + extFilePath);
                                Console.WriteLine("Press any key to continue...");
                                Console.ReadKey();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Move the AWC files.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="bankElem">
        /// The bank element.
        /// </param>
        /// <param name="newPackName">
        /// The new pack name.
        /// </param>
        /// <param name="lockAssets">
        /// The lock assets flag.
        /// </param>
        public static void MoveAwcFiles(Settings settings, IChangeList changeList, string packName, string platform, XElement bankElem, string newPackName, bool lockAssets = true)
        {
            var bankName = bankElem.Attribute("name").Value;
            var rootPath = Regex.Replace(settings.AwcRootPath, "{platform}", platform, RegexOptions.IgnoreCase);
            var srcPath = Path.Combine(rootPath, packName, bankName + ".awc");
            var destPath = Path.Combine(rootPath, newPackName, bankName + ".awc");

            if (!changeList.AssetManager.ExistsAsAsset(srcPath))
            {
                Console.WriteLine("ASSET NOT FOUND: " + srcPath);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
            else
            {
                var awcAsset = changeList.CheckoutAsset(srcPath, lockAssets);
                changeList.MoveAsset(awcAsset, destPath);
            }
        }
    }
}
