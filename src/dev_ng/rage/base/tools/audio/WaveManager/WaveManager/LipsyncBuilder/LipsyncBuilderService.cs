﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LipsyncBuilderService.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the LipsyncBuilderService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LipsyncBuilder
{
    using System.ServiceProcess;
    using System.Threading;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// The lipsync builder service.
    /// </summary>
    partial class LipsyncBuilderService : ServiceBase
    {
        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The lipsync builder.
        /// </summary>
        private readonly LipsyncBuilder lipsyncBuilder;

        /// <summary>
        /// The bank builder.
        /// </summary>
        private readonly BankBuilder bankBuilder;

        /// <summary>
        /// The lipsync builder thread.
        /// </summary>
        private Thread lipsyncBuilderThread;

        /// <summary>
        /// The bank builder thread.
        /// </summary>
        private Thread bankBuilderThread;

        /// <summary>
        /// Initializes a new instance of the <see cref="LipsyncBuilderService"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        public LipsyncBuilderService(Settings settings)
        {
            this.settings = settings;
            this.InitializeComponent();

            this.assetManager = AssetManagerFactory.GetInstance(
                AssetManagerType.Perforce,
                this.settings.P4Host,
                this.settings.P4Port,
                this.settings.P4Workspace,
                this.settings.P4User,
                this.settings.P4Password,
                this.settings.P4DepotRoot);

            this.lipsyncBuilder = new LipsyncBuilder(this.settings, this.assetManager);
            this.bankBuilder = new BankBuilder(this.settings, this.assetManager);
        }

        /// <summary>
        /// Run the service.
        /// </summary>
        public void Run()
        {
            this.lipsyncBuilderThread = new Thread(this.lipsyncBuilder.Run);
            this.bankBuilderThread = new Thread(this.bankBuilder.Run);

            this.lipsyncBuilderThread.Start();
            this.bankBuilderThread.Start();
        }

        /// <summary>
        /// Start tasks.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnStart(string[] args)
        {
            this.Run();
        }

        /// <summary>
        /// Stop tasks.
        /// </summary>
        protected override void OnStop()
        {
            if (null != this.lipsyncBuilderThread)
            {
                this.lipsyncBuilderThread.Abort();
            }

            if (null != this.bankBuilderThread)
            {
                this.bankBuilderThread.Abort();
            }
        }
    }
}
