﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace MixerQueryEditor.Data {

    public class MixerQuery {

        private List<Clauses> clauseGroups = new List<Clauses>();
        public List<Clauses> ClauseGroups {
            get { return clauseGroups; }
            set { clauseGroups = value; }
        }

    }
}

