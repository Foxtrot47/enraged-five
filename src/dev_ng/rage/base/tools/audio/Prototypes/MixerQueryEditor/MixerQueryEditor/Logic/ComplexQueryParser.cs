﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MixerQueryEditor.Logic {
    public class ComplexQueryParser {

        public const String TAGGEDWITH = "TaggedWith";
        public const String INCATEGORY = "InCategory";
        public const String INMIXGROUP = "InMixGroup";
        public const String AND = "AND";
        public const String OR = "OR";

        private String raveMixerQuery = "";
        public ComplexQueryParser(String raveMixerQuery) {
            this.raveMixerQuery = raveMixerQuery;
        }


        /**
         * Recursive descent implementation of a lexical analyser and parser for the following grammar:
         * 
         * complexGroupingExpr      ->  complexTagExpr complexGroupingRestExpr |
         *                              complexCategoryExpr complexGroupingRestExpr |
         *                              complexMixGroupExpr complexGroupingRestExpr|
         *                              (complexGroupingExpr) complexGroupingRestExpr
         *                 
         * complexGroupingRestExpr  ->  AND  complexGroupingExpr |
         *                              OR complexGroupingExpr |
         *                              ε
         * 
         * complexTagExpr           ->  TAGNAME complexTagRestExpr |
         *                              (complexTagExpr) complexTagRestExpr
         * 
         * complexTagRestExpr       ->  AND  complexTagExpr |
         *                              OR complexTagExpr |
         *                              ε
         *                             
         * complexCategoryExpr      ->  CATEGORYNAME complexCategoryRestExpr |
         *                              (complexCategoryExpr) complexCategoryRestExpr
         * 
         * complexCategoryRestExpr  ->  AND  complexCategoryExpr |
         *                              OR complexCategoryExpr |
         *                              ε
         *                              
         * complexMixGroupExpr      ->  CATEGORYNAME complexMixGroupRestExpr |
         *                              (complexMixGroupExpr) complexMixGroupRestExpr
         * 
         * complexMixGroupRestExpr  ->  AND  complexMixGroupExpr |
         *                              OR complexMixGroupExpr |
         *                              ε
         *                              
         * 
         * This allows parsing queries like this:
         * (TaggedWith[AmbientAudioEntity or Ambience] and InCategory [Collision]) or 
         * (TaggedWith[(Mic3 or Mic1)and Mic2] and InCategory[Collision])
         * 
         **/

        public void parseQuery() {
            String result = complexGroupingExpr(this.raveMixerQuery);
            result.TrimStart();
            if(!result.Equals("")) throw new QueryParserException("unexpected input: " + result, this.raveMixerQuery.Length - result.Length);
        }

        private String complexGroupingExpr(String queryComponent) {
            queryComponent=queryComponent.TrimStart();
            if(queryComponent.StartsWith("(")) {
                queryComponent = complexGroupingExpr(queryComponent.Substring(1));
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith(")")) {
                    throw new QueryParserException("Closing bracket expected", this.raveMixerQuery.Length-queryComponent.Length);
                }
                queryComponent = queryComponent.Substring(1);
                queryComponent = queryComponent.TrimStart();
                return complexGroupingRestExpr(queryComponent);
            }
            else if(queryComponent.StartsWith(TAGGEDWITH, true, null)) {
                queryComponent = queryComponent.Substring(TAGGEDWITH.Length);
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("[")) {
                    throw new QueryParserException("[ expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                queryComponent = complexTagExpr(queryComponent.Substring(1));
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("]")) {
                    throw new QueryParserException("] expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                return complexGroupingRestExpr(queryComponent.Substring(1));
            }
            else if(queryComponent.StartsWith(INCATEGORY, true, null)) {
                queryComponent = queryComponent.Substring(INCATEGORY.Length);
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("[")) {
                    throw new QueryParserException("[ expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                queryComponent = complexCategoryExpr(queryComponent.Substring(1));
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("]")) {
                    throw new QueryParserException("] expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                return complexGroupingRestExpr(queryComponent.Substring(1));
            }
            else if(queryComponent.StartsWith(INMIXGROUP, true, null)) {
                queryComponent = queryComponent.Substring(INMIXGROUP.Length);
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("[")) {
                    throw new QueryParserException("[ expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                queryComponent = complexMixGroupExpr(queryComponent.Substring(1));
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("]")) {
                    throw new QueryParserException("] expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                return complexGroupingRestExpr(queryComponent.Substring(1));
            }
            else {
                throw new QueryParserException("keyword expected", this.raveMixerQuery.Length - queryComponent.Length);
            }
        }

        private String complexGroupingRestExpr(String queryComponent) {
            return andOrParser(queryComponent, new exprCallback(complexGroupingExpr));
        }

        private delegate String exprCallback(String queryComponent);
        private String andOrParser(String queryComponent, exprCallback expr) {
            queryComponent = queryComponent.TrimStart();
            if(queryComponent.StartsWith(AND+" ", true, null)) {
                queryComponent = queryComponent.Substring(AND.Length+1);
                queryComponent = queryComponent.TrimStart();
                return expr(queryComponent);
            }
            else if(queryComponent.StartsWith(OR+" ", true, null)) {
                queryComponent = queryComponent.Substring(OR.Length+1);
                queryComponent = queryComponent.TrimStart();
                return expr(queryComponent);
            }
            else {
                return queryComponent;
            }
        }


        /**
         * implementation of complexTagExpr, complexCategoryExpr, and complexMixGroupExpr may be refactored
         * into a common complexVariableExpr method similar to the RestExpressions depending on how the 
         * parsed query's gonna be implemented. 
         **/

        private String complexTagRestExpr(String queryComponent) {
            return andOrParser(queryComponent, new exprCallback(complexTagExpr));
        }

        private String complexTagExpr(String queryComponent) {
            queryComponent=queryComponent.TrimStart();
            if(queryComponent.StartsWith("(")) {
                queryComponent = complexTagExpr(queryComponent.Substring(1));
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith(")")) {
                    throw new QueryParserException("Closing bracket expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                queryComponent = queryComponent.Substring(1);
                queryComponent = queryComponent.TrimStart();
                return complexTagRestExpr(queryComponent);
            }
            else {
                //determine the end of the tag name
                int endIndex = getEndIndexOfStartedWord(queryComponent);
                if(endIndex==0 || endIndex==-1){
                    throw new QueryParserException("Tag expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                queryComponent = queryComponent.Substring(endIndex);
                return complexTagRestExpr(queryComponent);
            }
        }


        private String complexCategoryRestExpr(String queryComponent) {
            return andOrParser(queryComponent, new exprCallback(complexCategoryExpr));
        }

        private String complexCategoryExpr(String queryComponent) {
            queryComponent = queryComponent.TrimStart();
            if(queryComponent.StartsWith("(")) {
                queryComponent = complexCategoryExpr(queryComponent.Substring(1));
                queryComponent = queryComponent.TrimStart();
                if(!queryComponent.StartsWith(")")) {
                    throw new QueryParserException("Closing bracket expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                queryComponent = queryComponent.Substring(1);
                queryComponent = queryComponent.TrimStart();
                return complexCategoryRestExpr(queryComponent);
            }
            else {
                //determine the end of the category name
                int endIndex = getEndIndexOfStartedWord(queryComponent);
                if(endIndex == 0 || endIndex == -1) {
                    throw new QueryParserException("Category expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                queryComponent = queryComponent.Substring(endIndex);
                return complexCategoryRestExpr(queryComponent);
            }
        }

        private String complexMixGroupRestExpr(String queryComponent) {
            return andOrParser(queryComponent, new exprCallback(complexMixGroupExpr));
        }

        private String complexMixGroupExpr(String queryComponent) {
            queryComponent = queryComponent.TrimStart();
            if(queryComponent.StartsWith("(")) {
                queryComponent = complexMixGroupExpr(queryComponent.Substring(1));
                queryComponent = queryComponent.TrimStart();
                if(!queryComponent.StartsWith(")")) {
                    throw new QueryParserException("Closing bracket expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                queryComponent = queryComponent.Substring(1);
                queryComponent = queryComponent.TrimStart();
                return complexMixGroupRestExpr(queryComponent);
            }
            else {
                //determine the end of the mixgroup name
                int endIndex = getEndIndexOfStartedWord(queryComponent);
                if(endIndex == 0 || endIndex == -1) {
                    throw new QueryParserException("MixGroup expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                queryComponent = queryComponent.Substring(endIndex);
                return complexMixGroupRestExpr(queryComponent);
            }
        }

        private int getEndIndexOfStartedWord(String queryComponent) {
            int endIndex = queryComponent.Length - 1;
            if(queryComponent.IndexOf(" ") != -1 && (endIndex > queryComponent.IndexOf(" ") || endIndex == -1)) endIndex = queryComponent.IndexOf(" ");
            if(queryComponent.IndexOf("]") != -1 && (endIndex > queryComponent.IndexOf("]") || endIndex == -1)) endIndex = queryComponent.IndexOf("]");
            if(queryComponent.IndexOf(")") != -1 && (endIndex > queryComponent.IndexOf(")") || endIndex == -1)) endIndex = queryComponent.IndexOf(")");
            return endIndex;
        }


        public enum ExpectedInput {
            LOGICALOPERATOR,
            GROUPING,
            CATEGORY,
            TAG,
            MIXGROUP,
            INPUT_MALFORMED
        }

        public static ExpectedInput detectExpectedInputType(String raveMixerQuery) {
            //Search for the last opening position of a square bracket
            int lastGroupingOpeningPosition = 0;
            int closedSquareBracketsCounter = 0;
            for(int i = raveMixerQuery.Length - 1; i > 0; i--) {
                char c = raveMixerQuery[i];
                if(c == ']') closedSquareBracketsCounter++;
                if(c == '[') {
                    if(closedSquareBracketsCounter == 0) {
                        lastGroupingOpeningPosition = i;
                        break;
                    }
                    else {
                        closedSquareBracketsCounter--;
                    }
                }
            }

            //cut off unfinished input 
            //if none of the symbols are found (-1) the query gets reduced to ""
            int startPositionOfunfinishedInput = raveMixerQuery.LastIndexOf(" ");
            if(raveMixerQuery.LastIndexOf("(")>startPositionOfunfinishedInput) startPositionOfunfinishedInput=raveMixerQuery.LastIndexOf("(");
            if(raveMixerQuery.LastIndexOf(")")>startPositionOfunfinishedInput) startPositionOfunfinishedInput=raveMixerQuery.LastIndexOf(")");
            if(raveMixerQuery.LastIndexOf("[")>startPositionOfunfinishedInput) startPositionOfunfinishedInput=raveMixerQuery.LastIndexOf("[");
            if(raveMixerQuery.LastIndexOf("]")>startPositionOfunfinishedInput) startPositionOfunfinishedInput=raveMixerQuery.LastIndexOf("]");
            if(startPositionOfunfinishedInput<raveMixerQuery.Length) raveMixerQuery=raveMixerQuery.Substring(0, startPositionOfunfinishedInput+1);

            //grouping, tag, mixgroup, or category expected
            if(raveMixerQuery.EndsWith(AND + " ", true, null) ||
                raveMixerQuery.EndsWith(OR + " ", true, null) ||
                raveMixerQuery.TrimEnd().EndsWith("(") ||
                raveMixerQuery.TrimEnd().EndsWith("[") ||
                raveMixerQuery.TrimEnd().Equals("")) {

                //not inside a grouping -> grouping keyword expected
                if(lastGroupingOpeningPosition == 0) {
                    return ExpectedInput.GROUPING;
                }

                //in a grouping -> determine the type of expected input
                String searchSubpart = raveMixerQuery.Substring(0, lastGroupingOpeningPosition);
                searchSubpart = searchSubpart.TrimEnd();
                if(searchSubpart.EndsWith(TAGGEDWITH, true, null)) return ExpectedInput.TAG;
                if(searchSubpart.EndsWith(INMIXGROUP, true, null)) return ExpectedInput.MIXGROUP;
                if(searchSubpart.EndsWith(INCATEGORY, true, null)) return ExpectedInput.CATEGORY;
                else return ExpectedInput.INPUT_MALFORMED;
            }
            else {
                //logical operator expected
                return ExpectedInput.LOGICALOPERATOR;
            }

        }

    }
}
