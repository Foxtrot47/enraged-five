﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MixerQueryEditor.Logic {
    class QueryParserException : Exception {

        int position = 0;
        public QueryParserException(String message, int position): base(message) {
            this.position = position;
        }

        public int getPosition() {
            return position;
        }
    }
}
