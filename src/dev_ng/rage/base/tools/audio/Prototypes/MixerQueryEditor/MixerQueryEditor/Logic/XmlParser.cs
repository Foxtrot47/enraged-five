﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MixerQueryEditor.Data;

namespace MixerQueryEditor.Logic {
    public class XmlParser {


        private const String MATCH_CATEGORY_ANY = "MATCH_CATEGORY_ANY";
        private const String MATCH_TAGS_ALL = "MATCH_TAGS_ALL";
        private const String MATCH_MIX_GROUP_ANY = "MATCH_MIX_GROUP_ANY";
        private const String MATCH_TAGS_ANY = "MATCH_TAGS_ANY";

        private MixerQuery model = null;

        public MixerQuery getModel(){
            return model;
        }

        public XmlParser(String xmlFileContent) { 

            model = new MixerQuery();

            XElement root = XElement.Parse(xmlFileContent);
            foreach(XElement xmlClauseGroups in root.Elements("ClauseGroups")) { 
                Clauses clausesModel = new Clauses();
                model.ClauseGroups.Add(clausesModel);

                foreach(XElement xmlClauses in xmlClauseGroups.Elements("Clauses")) {

                    MatchCondition matchConditionModel = null;
                    switch(xmlClauses.Element("MatchType").Value) { 
                        case MATCH_CATEGORY_ANY:
                            matchConditionModel = new MatchCondition(MatchCondition.MatchType.InAnyCategory);
                            break;
                        case MATCH_MIX_GROUP_ANY:
                            matchConditionModel = new MatchCondition(MatchCondition.MatchType.InAnyMixGroup);
                            break;
                        case MATCH_TAGS_ALL:
                            matchConditionModel = new MatchCondition(MatchCondition.MatchType.TaggedWithAll);
                            break;
                        case MATCH_TAGS_ANY:
                            matchConditionModel = new MatchCondition(MatchCondition.MatchType.TaggedWithAny);
                            break;
                    }
                    if(matchConditionModel==null) {
                        throw new Exception("MatchType not found in "+xmlClauses.Name);
                    }
                    clausesModel.MatchConditions.Add(matchConditionModel);
                    foreach(XElement xmlTags in xmlClauses.Elements("Tags")) {
                        matchConditionModel.Tags.Add(xmlTags.Element("TagName").Value);
                    }
                }
            }
        }

        public static String toXml(MixerQuery model) {
            XElement xmlRoot = new XElement("PatchQuery");
            foreach(Clauses clauses in model.ClauseGroups) {
                XElement xmlClauseGroups = new XElement("ClauseGroups");
                xmlRoot.Add(xmlClauseGroups);
                foreach(MatchCondition matchCondition in clauses.MatchConditions) {
                    XElement xmlClauses = new XElement("Clauses");
                    xmlClauseGroups.Add(xmlClauses);
                    XElement xmlMatchType = new XElement("MatchType");
                    xmlClauses.Add(xmlMatchType);

                    switch(matchCondition.Type){
                        case MatchCondition.MatchType.InAnyCategory:
                            xmlMatchType.SetValue(MATCH_CATEGORY_ANY);
                            break;
                        case MatchCondition.MatchType.InAnyMixGroup:
                            xmlMatchType.SetValue(MATCH_MIX_GROUP_ANY);
                            break;
                        case MatchCondition.MatchType.TaggedWithAll:
                            xmlMatchType.SetValue(MATCH_TAGS_ALL);
                            break;
                        case MatchCondition.MatchType.TaggedWithAny:
                            xmlMatchType.SetValue(MATCH_TAGS_ANY);
                            break;
                    }


                    foreach(String tag in matchCondition.Tags) {
                        XElement xmlTags = new XElement("Tags");
                        xmlClauses.Add(xmlTags);

                        XElement xmlTag = new XElement("TagName", tag);
                        xmlTags.Add(xmlTag);
                    }  

                }
                
            }
            return xmlRoot.ToString();
        }
    }
}
