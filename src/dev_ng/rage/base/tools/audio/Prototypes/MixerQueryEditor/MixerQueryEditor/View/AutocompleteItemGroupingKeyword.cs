﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Document;

namespace MixerQueryEditor.View {
    class AutocompleteItemGroupingKeyword: AutoCompleteItem {

        public AutocompleteItemGroupingKeyword(string keyword, string description) : base(keyword, description) { }
        public AutocompleteItemGroupingKeyword(string keyword) : base(keyword) { }

        public override void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs) {
            textArea.Document.Replace(completionSegment, this.Text+"[]");
            textArea.Caret.Offset--;
        }
    }
}
