﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Document;

namespace MixerQueryEditor.View {
    public class AutoCompleteGroupingElement: AutoCompleteItem {
        public delegate void insertionCallback();

        private insertionCallback callback = null;

        public AutoCompleteGroupingElement(string keyword, string description, insertionCallback callback) : base(keyword, description) {
            this.callback = callback;
        }
        public AutoCompleteGroupingElement(string keyword, insertionCallback callback) : base(keyword) {
            this.callback = callback;
        }

        public override void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs) {
            textArea.Document.Replace(completionSegment, this.Text+" ");
            if(callback!=null) callback();
        }
    }
}
