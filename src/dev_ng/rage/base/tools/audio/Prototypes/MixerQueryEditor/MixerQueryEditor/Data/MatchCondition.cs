﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MixerQueryEditor.Data{

    /// <summary>
    /// This class represents a keyword and its associated tags. Tags can be category names, mixgroup names or tag names.
    /// </summary>
    public class MatchCondition {

        public enum MatchType {
            TaggedWithAll,
            TaggedWithAny,
            InAnyCategory,
            InAnyMixGroup
        }

        public MatchCondition(MatchType type) {
            this.type = type;
        }

        private MatchType type;
        public MatchType Type {
            get { return type; }
            set { type=value; }
        }

        private List<String> tags = new List<String>();
        public List<String> Tags {
            get { return tags; }
            set { tags = value; }
        }
    }
}
