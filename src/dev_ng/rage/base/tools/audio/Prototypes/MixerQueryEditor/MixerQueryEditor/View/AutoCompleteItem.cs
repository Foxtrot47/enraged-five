﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.CodeCompletion;

namespace MixerQueryEditor.View {
    public class AutoCompleteItem : ICompletionData {
        public AutoCompleteItem(string keyword)
            : this(keyword, null) {
        }

        public AutoCompleteItem(string keyword, string description) {
            this.Text = keyword;
            this.Description = description;
        }

        public System.Windows.Media.ImageSource Image {
            get { return null; }
        }

        public string Text { get; private set; }

        // Use this property if you want to show a fancy UIElement in the drop down list.
        public object Content {
            get { return this.Text; }
        }

        public object Description { get; private set; }

        public double Priority { get { return 0; } }

        public virtual void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs) {
            textArea.Document.Replace(completionSegment, this.Text);
        }
    }
}
