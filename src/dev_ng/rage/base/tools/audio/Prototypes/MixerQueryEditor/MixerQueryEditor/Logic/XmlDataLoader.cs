﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSharpCode.AvalonEdit.CodeCompletion;
using System.Xml.Linq;

namespace MixerQueryEditor.Logic {
    public class XmlDataLoader {

        public static IList<ICompletionData> loadCategories(View.AutoCompleteGroupingElement.insertionCallback insertionCallback) {
            XDocument categoriesFile = XDocument.Load(@"X:\rdr3\audio\dev\ASSETS\OBJECTS\Core\Audio\Categories\Categories.xml");
            IList<ICompletionData> result = new List<ICompletionData>();
            foreach(XElement xmlCategory in categoriesFile.Root.Elements()){
                StringBuilder description = new StringBuilder();
                foreach(XElement xmlCategorySubelement in xmlCategory.Elements()) {
                    description.AppendLine(xmlCategorySubelement.Name+": "+xmlCategorySubelement.Value);
                }
                result.Add(new View.AutoCompleteGroupingElement(xmlCategory.Attribute("name").Value, description.ToString(), insertionCallback));
            }
            return result;
        }

        public static IList<ICompletionData> loadTags(View.AutoCompleteGroupingElement.insertionCallback insertionCallback) {
            XDocument categoriesFile = XDocument.Load(@"X:\rdr3\audio\dev\ASSETS\OBJECTS\Definitions\AudioDefinitions.xml");
            IList<ICompletionData> result = new List<ICompletionData>();
            foreach(XElement xmlTag in categoriesFile.Root.Elements().Where(x=>x.Name.ToString().Equals("TypeDefinition"))) {
                String gameObjectName = xmlTag.Attribute("name").Value;
                foreach(XElement xmlSound in xmlTag.Elements().Where(
                    x => x.Attribute("allowedType") !=null && x.Attribute("allowedType").Value!=null && x.Attribute("allowedType").Value.Equals("Sound") && x.Attribute("units")!=null && x.Attribute("units").Value!=null && x.Attribute("units").Value.Equals("ObjectRef"))
                    ){
                    String soundName = xmlSound.Attribute("name").Value;
                    result.Add(new View.AutoCompleteGroupingElement(gameObjectName+"."+soundName, insertionCallback));
                }
            }
            return result;
        }

    }
}
