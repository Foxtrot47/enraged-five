﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ICSharpCode.AvalonEdit.Highlighting;
using System.IO;
using System.Xml;
using ICSharpCode.AvalonEdit.CodeCompletion;
using Microsoft.Win32;
using MixerQueryEditor.Logic;
using MixerQueryEditor.View;

namespace MixerQueryEditor {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {


            // Load RaveMixer highlighting definition
            IHighlightingDefinition customHighlighting;
            using (Stream s = typeof(MainWindow).Assembly.GetManifestResourceStream("MixerQueryEditor.View.RaveMixerHighlighting.xshd")) {
                if (s == null)
                    throw new InvalidOperationException("Could not find embedded resource");
                using (XmlReader reader = new XmlTextReader(s)) {
                    customHighlighting = ICSharpCode.AvalonEdit.Highlighting.Xshd.
                        HighlightingLoader.Load(reader, HighlightingManager.Instance);
                }
            }
            // and register it in the HighlightingManager
            HighlightingManager.Instance.RegisterHighlighting("RaveMixer Highlighting", new string[] { }, customHighlighting);

            InitializeComponent();

            textEditor.WordWrap = true;
            textEditor.TextArea.TextEntering += textEditor_TextArea_TextEntering;


            groupingAutocompletionentries = new List<ICompletionData>();
            groupingAutocompletionentries.Add(new AutocompleteItemGroupingKeyword(QueryParser.TAGGEDWITHANY, "Selection based on tags"));
            groupingAutocompletionentries.Add(new AutocompleteItemGroupingKeyword(QueryParser.TAGGEDWITHALL, "Selection based on tags"));
            groupingAutocompletionentries.Add(new AutocompleteItemGroupingKeyword(QueryParser.INANYCATEGORY, "Selection based on categories"));
            groupingAutocompletionentries.Add(new AutocompleteItemGroupingKeyword(QueryParser.INANYMIXGROUP, "Selection based on mix groups"));

            logicOperatorAutocompletionentries = new List<ICompletionData>();
            logicOperatorAutocompletionentries.Add(new AutoCompleteItem(QueryParser.AND));
            logicOperatorAutocompletionentries.Add(new AutoCompleteItem(QueryParser.OR));

            categoryAutocompletionentries = XmlDataLoader.loadCategories(new AutoCompleteGroupingElement.insertionCallback(afterGroupElementInsertion));
            tagAutocompletionentries = XmlDataLoader.loadTags(new AutoCompleteGroupingElement.insertionCallback(afterGroupElementInsertion));
            //todo fill mixgroups
            mixGroupAutocompletionentries = new List<ICompletionData>();

        }

        private void afterGroupElementInsertion() {
            CommandBinding_ShowAutocomplete(null, null);
        }

        IList<ICompletionData> groupingAutocompletionentries;
        IList<ICompletionData> logicOperatorAutocompletionentries;
        IList<ICompletionData> categoryAutocompletionentries;
        IList<ICompletionData> tagAutocompletionentries;
        IList<ICompletionData> mixGroupAutocompletionentries;

        CompletionWindow completionWindow;

        void CommandBinding_ShowAutocomplete(object sender, ExecutedRoutedEventArgs e) {
            // open code completion after the user has pressed dot:
            completionWindow = new CompletionWindow(textEditor.TextArea);
            // find the start of the word
            completionWindow.StartOffset = getOffsetToStartOfTheWord();
            // provide AvalonEdit with the data:
            IList<ICompletionData> data = completionWindow.CompletionList.CompletionData;
            switch (QueryParser.detectExpectedInputType(textEditor.Text.Substring(0, textEditor.CaretOffset))) {
                case QueryParser.ExpectedInput.CATEGORY:
                    foreach(ICompletionData entry in categoryAutocompletionentries) data.Add(entry);
                    break;
                case QueryParser.ExpectedInput.GROUPING:
                    foreach(ICompletionData entry in groupingAutocompletionentries) data.Add(entry);
                    break;
                case QueryParser.ExpectedInput.TAG:
                    foreach(ICompletionData entry in tagAutocompletionentries) data.Add(entry);
                    break;
                case QueryParser.ExpectedInput.MIXGROUP:
                    foreach(ICompletionData entry in mixGroupAutocompletionentries) data.Add(entry);
                    break;
                case QueryParser.ExpectedInput.LOGICALOPERATOR:
                    foreach(ICompletionData entry in logicOperatorAutocompletionentries) data.Add(entry);
                    break;
            }
            completionWindow.MinWidth = 175;
            completionWindow.SizeToContent = SizeToContent.WidthAndHeight;

            completionWindow.Show();
            completionWindow.Closed += delegate {
                completionWindow = null;
            };
        }

        int getOffsetToStartOfTheWord() {
            int offsetToStartOfTheWord = -1;
            String searchString = textEditor.Text.Substring(0, textEditor.CaretOffset);
            if(offsetToStartOfTheWord <searchString.LastIndexOf(" ")) offsetToStartOfTheWord = searchString.LastIndexOf(" ");
            if(offsetToStartOfTheWord <searchString.LastIndexOf("\n")) offsetToStartOfTheWord = searchString.LastIndexOf("\n");
            if(offsetToStartOfTheWord <searchString.LastIndexOf("\t")) offsetToStartOfTheWord = searchString.LastIndexOf("\t");
            if(offsetToStartOfTheWord <searchString.LastIndexOf("[")) offsetToStartOfTheWord = searchString.LastIndexOf("[");
            if(offsetToStartOfTheWord <searchString.LastIndexOf("]")) offsetToStartOfTheWord = searchString.LastIndexOf("]");
            if(offsetToStartOfTheWord <searchString.LastIndexOf("(")) offsetToStartOfTheWord = searchString.LastIndexOf("(");
            if(offsetToStartOfTheWord <searchString.LastIndexOf(")")) offsetToStartOfTheWord = searchString.LastIndexOf(")");
            return offsetToStartOfTheWord+1;
        }

        void textEditor_TextArea_TextEntering(object sender, TextCompositionEventArgs e) {
            if (e.Text.Length > 0 && completionWindow != null) {
                if (e.Text[0]=='\n' || e.Text[0]=='\t') {
                    // Whenever a non-letter is typed while the completion window is open,
                    // insert the currently selected element.
                    bool isGroupingElement = completionWindow.CompletionList.SelectedItem is AutoCompleteGroupingElement;
                    completionWindow.CompletionList.RequestInsertion(e);
                    if(isGroupingElement) CommandBinding_ShowAutocomplete(null, null);
                }
                else if(e.Text[e.Text.Length-1] == ' ') {
                    completionWindow.Hide();
                }
            }
            // do not set e.Handled=true - we still want to insert the character that was typed
        }

        void runFileClick(object sender, EventArgs e) {
            try {
                new QueryParser(textEditor.Text);
                outputTextBox.Text="Query parsed successfully";
            }
            catch(QueryParserException ex) {
                if(textEditor.Text.Length>0) {
                    this.textEditor.Select(ex.getPosition(), 1);
                }
                outputTextBox.Text = ex.Message;
            }
        }
        
        string currentFileName;

        void openFileClick(object sender, RoutedEventArgs e) {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Rave Mixer Query | *.xml";
            openDialog.CheckFileExists = true;
            if(openDialog.ShowDialog() ?? false) {
                currentFileName = openDialog.FileName;
                try {

                    using(StreamReader inFile = new StreamReader(currentFileName)) {
                        textEditor.Text=QueryParser.toQueryString((new XmlParser(inFile.ReadToEnd())).getModel());
                    }
                }
                catch(Exception ex) {
                    outputTextBox.Text = ex.Message;
                }
                addOrLineBreaks();
            }
        }

        void saveFileClick(object sender, EventArgs e) {
            if(currentFileName == null) {
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Rave Mixer Query | *.xml";
                saveDialog.DefaultExt = ".xml";
                if(saveDialog.ShowDialog() ?? false) {
                    currentFileName = saveDialog.FileName;
                }
                else {
                    return;
                }
            }

            try {
                QueryParser parser = new QueryParser(textEditor.Text);

                using(StreamWriter outfile = new StreamWriter(currentFileName)) {
                    outfile.Write(XmlParser.toXml(parser.getModel()));
                }
            }
            catch(Exception ex) {
                outputTextBox.Text = ex.Message;
            }
            addOrLineBreaks();
        }

        private void addAndToEnd() {
            textEditor.Focus();
            if(textEditor.Text.Length!=0) {
                if(!textEditor.Text.EndsWith(Environment.NewLine)) {
                    if(!textEditor.Text.EndsWith(" ")) textEditor.Text+=" ";
                    textEditor.Text+=QueryParser.AND+" ";
                }
            }
            textEditor.CaretOffset = textEditor.Text.Length;
        }

        private void taggedWithAllClick(object sender, RoutedEventArgs e) {
            addAndToEnd();
            keywordButtonAction(QueryParser.TAGGEDWITHALL);
        }

        private void taggedWithAnyClick(object sender, RoutedEventArgs e) {
            addAndToEnd();
            keywordButtonAction(QueryParser.TAGGEDWITHANY);
        }

        private void inCategoryClick(object sender, RoutedEventArgs e) {
            addAndToEnd();
            keywordButtonAction(QueryParser.INANYCATEGORY);
        }

        private void inMixGroupClick(object sender, RoutedEventArgs e) {
            addAndToEnd();
            keywordButtonAction(QueryParser.INANYMIXGROUP);
        }

        private void keywordButtonAction(String keyword) {
            textEditor.Document.Replace(getOffsetToStartOfTheWord(), textEditor.CaretOffset-getOffsetToStartOfTheWord(), keyword+"[]");
            textEditor.CaretOffset--;
            Command.ShowAutocompleteWindow.Execute(null, null);
        }

        private void addOrLineBreaks() {
            
            String newText = "";
            char[] charsToTrim = {' ', '\t'};
            for(int index = 0; ; index=index+QueryParser.OR.Length) {
                int oldIndex = index;
                index = textEditor.Text.IndexOf(QueryParser.OR, index, StringComparison.CurrentCultureIgnoreCase);
                if(index==-1) {
                    newText+=(textEditor.Text.Substring(oldIndex, textEditor.Text.Length-oldIndex).TrimStart(charsToTrim));
                    break;
                }
                newText+=(textEditor.Text.Substring(oldIndex, index-oldIndex).TrimStart(charsToTrim));
                
                //check if newline after OR is needed
                bool orIsPartOfWord = false;
                bool newLineAfterOrNeeded = false;
                if(index+2 < textEditor.Text.Length) {
                    string rest = textEditor.Text.Substring(index+2);
                    if(rest.StartsWith(" ") || rest.StartsWith("\t") || rest.StartsWith("]")) newLineAfterOrNeeded = true;
                    else if(!rest.StartsWith(Environment.NewLine)) orIsPartOfWord = true;
                }
                //check if newline before OR is needed
                if((newText.EndsWith(" ") || newText.EndsWith("\t") || newText.EndsWith("]")) && !orIsPartOfWord) {
                    newText+=Environment.NewLine;
                }
                else if(!orIsPartOfWord) {
                    orIsPartOfWord = true;
                }
                newText+=textEditor.Text.Substring(index, 2);
                if(!orIsPartOfWord && newLineAfterOrNeeded) {
                    newText+=Environment.NewLine;
                }

            }
            textEditor.Text = newText;
        }

        private void orClick(object sender, RoutedEventArgs e) {
            if(!textEditor.Text.EndsWith(Environment.NewLine)) textEditor.Text+=Environment.NewLine;
            textEditor.Text+=QueryParser.OR+Environment.NewLine;
            textEditor.CaretOffset = textEditor.Text.Length;
        }
    }

    public static class Command {
        public static readonly RoutedUICommand ShowAutocompleteWindow = new RoutedUICommand("Show Autocomplete Window", "ShowAutocompleteWindow", typeof(MainWindow));
    }
}
