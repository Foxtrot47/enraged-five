﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using MixerQueryEditor.Data;

namespace MixerQueryEditor.Logic {
    public class QueryParser {

        public const String TAGGEDWITHANY = "TaggedWithAny";
        public const String TAGGEDWITHALL = "TaggedWithAll";
        public const String INANYCATEGORY = "InAnyCategory";
        public const String INANYMIXGROUP = "InAnyMixGroup";
        public const String AND = "AND";
        public const String OR = "OR";

        private String raveMixerQuery = "";
        private MixerQuery model = null;


        public MixerQuery getModel(){
            return model;
        }

        /**
         * Recursive descent implementation of a lexical analyser and parser for the following grammar:
         * 
         * complexGroupingExpr      ->  complexTagAllExpr complexGroupingRestExpr |
         *                              complexTagAnyExpr complexGroupingRestExpr |
         *                              complexCategoryExpr complexGroupingRestExpr |
         *                              complexMixGroupExpr complexGroupingRestExpr
         *                 
         * complexGroupingRestExpr  ->  AND  complexGroupingExpr |
         *                              OR complexGroupingExpr |
         *                              ε
         * 
         * complexTagAnyExpr        ->  TAGNAME complexTagAnyRestExpr
         * 
         * complexTagAnyRestExpr    ->  TAGNAME |
         *                              ε
         *                             
         * complexTagAllExpr        ->  TAGNAME complexTagAllRestExpr
         * 
         * complexTagAllRestExpr    ->  TAGNAME |
         *                              ε
         *                              
         * complexCategoryExpr      ->  CATEGORYNAME complexCategoryRestExpr
         * 
         * complexCategoryRestExpr  ->  CATEGORYNAME |
         *                              ε
         *                              
         * complexMixGroupExpr      ->  MIXGROUPNAME complexMixGroupRestExpr
         * 
         * complexMixGroupRestExpr  ->  MIXGROUPNAME |
         *                              ε
         *                              
         * 
         * This allows parsing queries like this:
         * TaggedWithAny[AmbientAudioEntity Ambience] and InAnyCategory [Collision] 
         * or TaggedWithAll[Mic3 Mic1 Mic2] and InAnyCategory[Collision]
         * 
         **/
        
        
        
        
        
        public QueryParser(String raveMixerQuery) {
            this.raveMixerQuery = raveMixerQuery;
            this.model = new MixerQuery();
            Clauses initialClauses = new Clauses();
            this.model.ClauseGroups.Add(initialClauses);
            String result = complexGroupingExpr(Regex.Replace(this.raveMixerQuery, "[\n\r\t]", " "), initialClauses);
            result.TrimStart();
            if(!result.Equals("")) throw new QueryParserException("unexpected input: " + result, this.raveMixerQuery.Length - result.Length);
        }

        private String complexGroupingExpr(String queryComponent, Clauses actualClauses) {
            queryComponent=queryComponent.TrimStart();
            if(queryComponent.StartsWith(TAGGEDWITHALL, true, null)) {
                queryComponent = queryComponent.Substring(TAGGEDWITHALL.Length);
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("[")) {
                    throw new QueryParserException("[ expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                MatchCondition modelClass = new MatchCondition(MatchCondition.MatchType.TaggedWithAll);
                actualClauses.MatchConditions.Add(modelClass);
                queryComponent = complexTagAllExpr(queryComponent.Substring(1), modelClass);
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("]")) {
                    throw new QueryParserException("] expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                return complexGroupingRestExpr(queryComponent.Substring(1), actualClauses);
            }
            else if(queryComponent.StartsWith(TAGGEDWITHANY, true, null)) {
                queryComponent = queryComponent.Substring(TAGGEDWITHANY.Length);
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("[")) {
                    throw new QueryParserException("[ expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                MatchCondition modelClass = new MatchCondition(MatchCondition.MatchType.TaggedWithAny);
                actualClauses.MatchConditions.Add(modelClass);
                queryComponent = complexTagAnyExpr(queryComponent.Substring(1), modelClass);
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("]")) {
                    throw new QueryParserException("] expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                return complexGroupingRestExpr(queryComponent.Substring(1), actualClauses);
            }
            else if(queryComponent.StartsWith(INANYCATEGORY, true, null)) {
                queryComponent = queryComponent.Substring(INANYCATEGORY.Length);
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("[")) {
                    throw new QueryParserException("[ expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                MatchCondition modelClass = new MatchCondition(MatchCondition.MatchType.InAnyCategory);
                actualClauses.MatchConditions.Add(modelClass);
                queryComponent = complexCategoryExpr(queryComponent.Substring(1), modelClass);
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("]")) {
                    throw new QueryParserException("] expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                return complexGroupingRestExpr(queryComponent.Substring(1), actualClauses);
            }
            else if(queryComponent.StartsWith(INANYMIXGROUP, true, null)) {
                queryComponent = queryComponent.Substring(INANYMIXGROUP.Length);
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("[")) {
                    throw new QueryParserException("[ expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                MatchCondition modelClass = new MatchCondition(MatchCondition.MatchType.InAnyMixGroup);
                actualClauses.MatchConditions.Add(modelClass);
                queryComponent = complexMixGroupExpr(queryComponent.Substring(1), modelClass);
                queryComponent=queryComponent.TrimStart();
                if(!queryComponent.StartsWith("]")) {
                    throw new QueryParserException("] expected", this.raveMixerQuery.Length - queryComponent.Length);
                }
                return complexGroupingRestExpr(queryComponent.Substring(1), actualClauses);
            }
            else {
                throw new QueryParserException("keyword expected", this.raveMixerQuery.Length - queryComponent.Length);
            }
        }

        private String complexGroupingRestExpr(String queryComponent, Clauses actualClauses) {
            queryComponent = queryComponent.TrimStart();
            if(queryComponent.StartsWith(AND+" ", true, null)) {
                queryComponent = queryComponent.Substring(AND.Length+1);
                queryComponent = queryComponent.TrimStart();
                return complexGroupingExpr(queryComponent, actualClauses);
            }
            else if(queryComponent.StartsWith(OR+" ", true, null)) {
                queryComponent = queryComponent.Substring(OR.Length+1);
                queryComponent = queryComponent.TrimStart();
                Clauses grouping = new Clauses();
                this.model.ClauseGroups.Add(grouping);
                return complexGroupingExpr(queryComponent, grouping);
            }
            else {
                return queryComponent;
            }
        }

        private String complexTagAllRestExpr(String queryComponent, MatchCondition actualMatchGroup) {
            queryComponent=queryComponent.TrimStart();
            //determine the end of the tag name
            int endIndex = getEndIndexOfStartedWord(queryComponent);
            if(endIndex==0 || endIndex==-1) {
                return queryComponent;
            }
            actualMatchGroup.Tags.Add(queryComponent.Substring(0,endIndex));
            queryComponent = queryComponent.Substring(endIndex);
            return complexTagAllRestExpr(queryComponent, actualMatchGroup);
        }

        private String complexTagAllExpr(String queryComponent, MatchCondition actualMatchGroup) {
            queryComponent=queryComponent.TrimStart();
            //determine the end of the tag name
            int endIndex = getEndIndexOfStartedWord(queryComponent);
            if(endIndex==0 || endIndex==-1){
                throw new QueryParserException("Tag expected", this.raveMixerQuery.Length - queryComponent.Length);
            }
            actualMatchGroup.Tags.Add(queryComponent.Substring(0, endIndex));
            queryComponent = queryComponent.Substring(endIndex);
            return complexTagAllRestExpr(queryComponent, actualMatchGroup);
        }

        private String complexTagAnyRestExpr(String queryComponent, MatchCondition actualMatchGroup) {
            queryComponent=queryComponent.TrimStart();
            //determine the end of the tag name
            int endIndex = getEndIndexOfStartedWord(queryComponent);
            if(endIndex==0 || endIndex==-1) {
                return queryComponent;
            }
            actualMatchGroup.Tags.Add(queryComponent.Substring(0, endIndex));
            queryComponent = queryComponent.Substring(endIndex);
            return complexTagAnyRestExpr(queryComponent, actualMatchGroup);
        }

        private String complexTagAnyExpr(String queryComponent, MatchCondition actualMatchGroup) {
            queryComponent=queryComponent.TrimStart();
            //determine the end of the tag name
            int endIndex = getEndIndexOfStartedWord(queryComponent);
            if(endIndex==0 || endIndex==-1) {
                throw new QueryParserException("Tag expected", this.raveMixerQuery.Length - queryComponent.Length);
            }
            actualMatchGroup.Tags.Add(queryComponent.Substring(0, endIndex));
            queryComponent = queryComponent.Substring(endIndex);
            return complexTagAnyRestExpr(queryComponent, actualMatchGroup);
        }

        private String complexCategoryRestExpr(String queryComponent, MatchCondition actualMatchGroup) {
            queryComponent=queryComponent.TrimStart();
            //determine the end of the category name
            int endIndex = getEndIndexOfStartedWord(queryComponent);
            if(endIndex==0 || endIndex==-1) {
                return queryComponent;
            }
            actualMatchGroup.Tags.Add(queryComponent.Substring(0, endIndex));
            queryComponent = queryComponent.Substring(endIndex);
            return complexCategoryRestExpr(queryComponent, actualMatchGroup);
        }

        private String complexCategoryExpr(String queryComponent, MatchCondition actualMatchGroup) {
            queryComponent=queryComponent.TrimStart();
            //determine the end of the category name
            int endIndex = getEndIndexOfStartedWord(queryComponent);
            if(endIndex==0 || endIndex==-1) {
                throw new QueryParserException("Category expected", this.raveMixerQuery.Length - queryComponent.Length);
            }
            actualMatchGroup.Tags.Add(queryComponent.Substring(0, endIndex));
            queryComponent = queryComponent.Substring(endIndex);
            return complexCategoryRestExpr(queryComponent, actualMatchGroup);
        }

        private String complexMixGroupRestExpr(String queryComponent, MatchCondition actualMatchGroup) {
            queryComponent=queryComponent.TrimStart();
            //determine the end of the mixgroup name
            int endIndex = getEndIndexOfStartedWord(queryComponent);
            if(endIndex==0 || endIndex==-1) {
                return queryComponent;
            }
            actualMatchGroup.Tags.Add(queryComponent.Substring(0, endIndex));
            queryComponent = queryComponent.Substring(endIndex);
            return complexMixGroupRestExpr(queryComponent, actualMatchGroup);
        }

        private String complexMixGroupExpr(String queryComponent, MatchCondition actualMatchGroup) {
            queryComponent=queryComponent.TrimStart();
            //determine the end of the mixgroup name
            int endIndex = getEndIndexOfStartedWord(queryComponent);
            if(endIndex==0 || endIndex==-1) {
                throw new QueryParserException("MixGroup expected", this.raveMixerQuery.Length - queryComponent.Length);
            }
            actualMatchGroup.Tags.Add(queryComponent.Substring(0, endIndex));
            queryComponent = queryComponent.Substring(endIndex);
            return complexMixGroupRestExpr(queryComponent, actualMatchGroup);
        }

        

        private int getEndIndexOfStartedWord(String queryComponent) {
            int endIndex = queryComponent.Length - 1;
            if(queryComponent.IndexOf(" ") != -1 && (endIndex > queryComponent.IndexOf(" ") || endIndex == -1)) endIndex = queryComponent.IndexOf(" ");
            if(queryComponent.IndexOf("]") != -1 && (endIndex > queryComponent.IndexOf("]") || endIndex == -1)) endIndex = queryComponent.IndexOf("]");
            return endIndex;
        }


        public enum ExpectedInput {
            LOGICALOPERATOR,
            GROUPING,
            CATEGORY,
            TAG,
            MIXGROUP,
            INPUT_MALFORMED
        }

        public static ExpectedInput detectExpectedInputType(String raveMixerQuery) {
            //Search for the last opening position of a square bracket
            int lastGroupingOpeningPosition = 0;
            int closedSquareBracketsCounter = 0;
            for(int i = raveMixerQuery.Length - 1; i > 0; i--) {
                char c = raveMixerQuery[i];
                if(c == ']') closedSquareBracketsCounter++;
                if(c == '[') {
                    if(closedSquareBracketsCounter == 0) {
                        lastGroupingOpeningPosition = i;
                        break;
                    }
                    else {
                        closedSquareBracketsCounter--;
                    }
                }
            }

            //cut off unfinished input 
            //if none of the symbols are found (-1) the query gets reduced to ""
            int startPositionOfunfinishedInput = raveMixerQuery.LastIndexOf(" ");
            if(raveMixerQuery.LastIndexOf("[")>startPositionOfunfinishedInput) startPositionOfunfinishedInput=raveMixerQuery.LastIndexOf("[");
            if(raveMixerQuery.LastIndexOf("]")>startPositionOfunfinishedInput) startPositionOfunfinishedInput=raveMixerQuery.LastIndexOf("]");
            if(startPositionOfunfinishedInput<raveMixerQuery.Length) raveMixerQuery=raveMixerQuery.Substring(0, startPositionOfunfinishedInput+1);

            
            if(lastGroupingOpeningPosition==0){
                if(raveMixerQuery.EndsWith(AND + " ", true, null) ||
                   raveMixerQuery.EndsWith(OR + " ", true, null) ||
                   raveMixerQuery.TrimEnd().Equals("")) {
                    //grouping expected
                    return ExpectedInput.GROUPING;
                }
                else{
                    //logical operator expected
                    return ExpectedInput.LOGICALOPERATOR;
                }  
            }
            else {
                //tag, mixgroup, or category expected
                String searchSubpart = raveMixerQuery.Substring(0, lastGroupingOpeningPosition);
                searchSubpart = searchSubpart.TrimEnd();
                if(searchSubpart.EndsWith(TAGGEDWITHALL, true, null)) return ExpectedInput.TAG;
                if(searchSubpart.EndsWith(TAGGEDWITHANY, true, null)) return ExpectedInput.TAG;
                if(searchSubpart.EndsWith(INANYMIXGROUP, true, null)) return ExpectedInput.MIXGROUP;
                if(searchSubpart.EndsWith(INANYCATEGORY, true, null)) return ExpectedInput.CATEGORY;
                else return ExpectedInput.INPUT_MALFORMED;
            }
        }


        public static String toQueryString(MixerQuery model){
            StringBuilder result = new StringBuilder();
            bool firstClauseGroup = true;
            foreach(Clauses clauseGroup in model.ClauseGroups) {
                if(!firstClauseGroup) result.Append(QueryParser.OR+" ");
                else firstClauseGroup = false;
                bool firstMatchCondition = true;
                foreach(MatchCondition matchCondition in clauseGroup.MatchConditions) {
                    if(!firstMatchCondition) result.Append(QueryParser.AND+" ");
                    else firstMatchCondition = false;

                    switch(matchCondition.Type) {
                        case MatchCondition.MatchType.InAnyCategory:
                            result.Append(QueryParser.INANYCATEGORY+"[");
                            break;
                        case MatchCondition.MatchType.InAnyMixGroup:
                            result.Append(QueryParser.INANYMIXGROUP+"[");
                            break;
                        case MatchCondition.MatchType.TaggedWithAll:
                            result.Append(QueryParser.TAGGEDWITHALL+"[");
                            break;
                        case MatchCondition.MatchType.TaggedWithAny:
                            result.Append(QueryParser.TAGGEDWITHANY+"[");
                            break;
                    }


                    foreach(String tag in matchCondition.Tags) {
                        result.Append(tag+" ");
                    }
                    result.Append("] ");
                }

            }
            return result.ToString();
        }

    }
}
