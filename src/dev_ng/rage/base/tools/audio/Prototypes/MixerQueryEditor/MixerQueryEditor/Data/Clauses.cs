﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MixerQueryEditor.Data {

    public class Clauses {


        private List<MatchCondition> matchConditions = new List<MatchCondition>();
        public List<MatchCondition> MatchConditions {
            get { return matchConditions; }
            set { matchConditions = value; }
        }
    }
}