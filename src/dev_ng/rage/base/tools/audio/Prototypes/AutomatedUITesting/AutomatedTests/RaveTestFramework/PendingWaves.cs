﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace AutomatedRaveTests.RaveTestFramework {
    public class PendingWaves {

        public string PendingWavesFolderPC { get; set; }
        public string PendingWavesFolderXBOX360 { get; set; }
        public string PendingWavesFolderPS3 { get; set; }

        public PendingWaves() {
            PendingWavesFolderPC = @"X:\gta5\audio\dev\build\pc\PendingWaves\";
            PendingWavesFolderXBOX360 = @"X:\gta5\audio\dev\build\xbox360\PendingWaves\";
            PendingWavesFolderPS3 = @"X:\gta5\audio\dev\build\ps3\PendingWaves\";
        }

        public enum Operation {
            add,
            remove,
            modify
        }

        public enum Type {
            Pack,
            Bank,
            Tag
        }

        private XDocument getPendingWavesXmlDocument(Rave.Platform platform, string packName) {
            string path = "";

            switch (platform) {
                case Rave.Platform.PC:
                    path = PendingWavesFolderPC + packName + ".XML";
                    break;
                case Rave.Platform.PS3:
                    path = PendingWavesFolderPS3 + packName + ".XML";
                    break;
                case Rave.Platform.XBOX360:
                    path = PendingWavesFolderXBOX360 + packName + ".XML";
                    break;
                default:
                    throw new Exception("Platform not recognised ("+platform.ToString()+")");
            }
            if(!File.Exists(path)) throw new FileNotFoundException("Pending waves xml not found ("+path+")");
            return XDocument.Load(path);
        }

        private XElement findNode(IEnumerable<XElement> xmlElements, List<string> nodePath) {
            foreach (XElement element in xmlElements) {
                if (element.Attribute("name") != null && element.Attribute("name").Value.Equals(nodePath.First())) {
                    nodePath.RemoveRange(0, 1);
                    if (nodePath.Count > 0) {
                        return findNode(element.Elements(), nodePath);
                    }
                    else {
                        return element;
                    }
                }
            }
            return null;
        }

        public bool isTagValue(string value, Rave.Platform platform, params string[] tagPath) {
            XDocument doc = getPendingWavesXmlDocument(platform, tagPath[0]);
            List<string> tagPathList = tagPath.ToList();
            XElement result = findNode(doc.Root.Elements(), tagPathList);
            if (result.Attribute("value") != null) {
                return result.Attribute("value").Value.Equals(value);
            }
            else {
                return value.Equals("");
            }
        }

        public bool isThereAnyPendingWaveFor(Rave.Platform platform, string packName) {
            XDocument doc;
            try {
                doc = getPendingWavesXmlDocument(platform, packName);
            }
            catch(FileNotFoundException) {
                return false;
            }
            return doc.Root.Elements().Any();
        }

        public bool nodeExists(Rave.Platform platform, params string[] nodePath) {
            XDocument doc;
            try {
               doc = getPendingWavesXmlDocument(platform, nodePath[0]);
            }
            catch (FileNotFoundException) {
                return false;
            }
            List<string> nodePathList = nodePath.ToList();
            XElement result = findNode(doc.Root.Elements(), nodePathList);
            return result!=null;
        }

        public bool isOperation(Operation operation, Rave.Platform platform, params string[] nodePath) {
            XDocument doc = getPendingWavesXmlDocument(platform, nodePath[0]);
            List<string> nodePathList = nodePath.ToList();
            XElement result = findNode(doc.Root.Elements(), nodePathList);
            if (result == null) throw new Exception("Node not found");
            if(result.Attribute("operation")!=null && result.Attribute("operation").Value.Equals(operation.ToString())) {
                return true;
            }
            else {
                return false;
            }
        }


    }
}
