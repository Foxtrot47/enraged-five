﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Text;
using TestStack.White.Configuration;
using TestStack.White.Factory;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.TreeItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.MenuItems;
using TestStack.White;

namespace AutomatedRaveTests.RaveTestFramework {
    public class WaveBrowser {
        internal WaveBrowser(Window mainWindow) {
            this.mainWindow = mainWindow;
        }

        private Window mainWindow;

        public void open() {
            mainWindow.MenuBar.MenuItem("Window", "Show", "Wave Browser").Click();
        }

        public void setActivePlatformToPC() {
            ComboBox activePlatform = mainWindow.Get<ComboBox>("m_activePlatform");
            activePlatform.EditableText = "PC";
        }

        public void setActivePlatformToPS3() {
            ComboBox activePlatform = mainWindow.Get<ComboBox>("m_activePlatform");
            activePlatform.EditableText = "PS3";
        }

        public void setActivePlatformToXBOX360() {
            ComboBox activePlatform = mainWindow.Get<ComboBox>("m_activePlatform");
            activePlatform.EditableText = "XBOX360";
        }

        public void setActivePlatformToAll() {
            ComboBox activePlatform = mainWindow.Get<ComboBox>("m_activePlatform");
            activePlatform.EditableText = "All";
        }

        public void setImportPluginToNoPlugin() {
            ComboBox activePlatform = mainWindow.Get<ComboBox>("m_pluginChooser");
            activePlatform.EditableText = "No Plugin";
        }

        public void setImportPluginToSpeechImport() {
            ComboBox activePlatform = mainWindow.Get<ComboBox>("m_pluginChooser");
            activePlatform.EditableText = "Speech Import";
        }

        public void setImportPluginToScriptedSpeechImport() {
            ComboBox activePlatform = mainWindow.Get<ComboBox>("m_pluginChooser");
            activePlatform.EditableText = "Scripted Speech Import";
        }

        public void setImportPluginToRadioImportTool() {
            ComboBox activePlatform = mainWindow.Get<ComboBox>("m_pluginChooser");
            activePlatform.EditableText = "Radio Import Tool";
        }

        public void setImportPluginToCutsceneImport() {
            ComboBox activePlatform = mainWindow.Get<ComboBox>("m_pluginChooser");
            activePlatform.EditableText = "Cutscene Import";
        }

        public void loadPack(string name) {
            mainWindow.Focus();
            Tree waveTree = mainWindow.Get<Tree>("m_treeView");
            TreeNode selectedNode = waveTree.Node(name);
            if(selectedNode==null) throw new Exception("Node "+name+" not found in tree");
            selectedNode.RightClick();
            foreach(Menu menu in mainWindow.Popup.Items){
                if(menu.Name.Equals("Load Pack")) {
                    menu.Click();
                    return;
                }
            }
            //pack already loaded
            //throw new Exception("Menu for loading pack not found");
        }

        public enum Tag {
            notForGirls,
            compression,
            sampleRate,
            voice,
            pain,
            streamingBlockBytes,
            rename,
            rebuild,
            granular,
            nongranular,
            preserveTransient,
            disableLooping,
            ambientSlotType,
            blockSize,
            markSilence,
            DoNotBuild,
            cutsceneStrip,
            compressionType,
            noLipsync,
            iterativeEncoding,
            forceInternalEncoder
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="platforms">Platforms can be combined with binary or</param>
        /// <param name="nodePath"></param>
        public void addTag(Tag tag, Rave.Platform platforms, params string[] nodePath) {
            Tree waveTree = mainWindow.Get<Tree>("m_treeView");
            TreeNode selectedNode = waveTree.Node(nodePath);
            if(selectedNode==null) throw new Exception("Node "+nodePath.Last()+" not found in tree");
            selectedNode.Expand();
            selectedNode.RightClick();
            mainWindow.Popup.Item("New Tag").Click();
            Window tagEditor = mainWindow.ModalWindow("Tag Editor");
            ComboBox tagComboBox = tagEditor.Get<ComboBox>("cbTagName");
            tagComboBox.Select(tag.ToString());
            if (platforms.HasFlag(Rave.Platform.PC)) {
                tagEditor.Get<CheckBox>(SearchCriteria.ByText(Rave.Platform.PC.ToString())).Checked = false;
            }
            if (platforms.HasFlag(Rave.Platform.XBOX360)) {
                tagEditor.Get<CheckBox>(SearchCriteria.ByText(Rave.Platform.XBOX360.ToString())).Checked = false;
            }
            if (platforms.HasFlag(Rave.Platform.PS3)) {
                tagEditor.Get<CheckBox>(SearchCriteria.ByText(Rave.Platform.PS3.ToString())).Checked = false;
            }
            tagEditor.Get<Button>(SearchCriteria.ByText("Ok")).Click();
        }

        public void changeTag(string value, Rave.Platform platform, params string[] nodePath) {
            Tree waveTree = mainWindow.Get<Tree>("m_treeView");
            TreeNode selectedNode = waveTree.Node(nodePath);
            if(selectedNode==null) throw new Exception("Node "+nodePath.Last()+" not found in tree");
            selectedNode.Expand();
            selectedNode.DoubleClick();
            Window tagEditor = mainWindow.ModalWindow("Tag Editor");
            if(platform==Rave.Platform.PC) {
                tagEditor.Get<TextBox>(SearchCriteria.ByText(Rave.Platform.PC.ToString())).Text = value;
            }
            if(platform==Rave.Platform.XBOX360) {
                tagEditor.Get<TextBox>(SearchCriteria.ByText(Rave.Platform.XBOX360.ToString())).Text = value;
            }
            if(platform==Rave.Platform.PS3) {
                tagEditor.Get<TextBox>(SearchCriteria.ByText(Rave.Platform.PS3.ToString())).Text = value;
            }
            tagEditor.Get<Button>(SearchCriteria.ByText("Ok")).Click();
        }


        public void deleteNode(params string[] nodePath) {
            Tree waveTree = mainWindow.Get<Tree>("m_treeView");
            TreeNode selectedNode = waveTree.Node(nodePath);
            if(selectedNode==null) throw new Exception("Node "+nodePath.Last()+" not found in tree");
            selectedNode.RightClick();
            mainWindow.Popup.Item("Delete").Click();
        }

        public bool nodeExists(params string[] nodePath) {
            Tree waveTree = mainWindow.Get<Tree>("m_treeView");
            TreeNode selectedNode = waveTree.Node(nodePath);
            return selectedNode != null;
        }

        public void commitChanges() {
            mainWindow.MenuBar.MenuItem("Wave Browser", "Commit Changes").Click();
            Window confirmCommit = mainWindow.ModalWindow("Confirm Commit ...");
            confirmCommit.Get<Button>(SearchCriteria.ByText("Commit")).Click();
            mainWindow.WaitWhileBusy();
            CoreAppXmlConfiguration.Instance.FindWindowTimeout=90000;
            Window confirmWindow = mainWindow.ModalWindow("Information");
            //reset timeout
            CoreAppXmlConfiguration.Instance.FindWindowTimeout=30000;
            confirmWindow.Get<Button>(SearchCriteria.ByText("OK")).Click();
        }

        public void buildWavesForAllPlatforms(string packName) {
            buildWaves("All Platforms", packName);
        }

        public void buildWaves(Rave.Platform platform, string packName) {
            buildWaves(platform.ToString(), packName);
        }

        private void buildWaves(string platformName, string packName) {
            Tree waveTree = mainWindow.Get<Tree>("m_treeView");
            TreeNode selectedNode = waveTree.Node(packName);
            if(selectedNode==null) throw new Exception("Pack "+packName+" not found in tree");
            selectedNode.Expand();
            selectedNode.RightClick();
            mainWindow.Popup.Item("Build Waves", platformName).Click();
            Window askCommitWindow = mainWindow.ModalWindow("Build");
            askCommitWindow.Get<Button>(SearchCriteria.ByText("Yes")).Click();

            CoreAppXmlConfiguration.Instance.FindWindowTimeout=180000;
            Window confirmWindow = mainWindow.ModalWindow("Information");
            //reset timeout
            CoreAppXmlConfiguration.Instance.FindWindowTimeout=30000;
            confirmWindow.Get<Button>(SearchCriteria.ByText("OK")).Click();
        }


    }
}
