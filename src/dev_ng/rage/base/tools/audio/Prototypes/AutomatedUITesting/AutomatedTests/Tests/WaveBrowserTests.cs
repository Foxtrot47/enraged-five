﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using AutomatedRaveTests.RaveTestFramework;

namespace AutomatedRaveTests.Tests {

    [TestFixture]
    public class WaveBrowserTests {

        [SetUpFixture]
        public class SetupRaveInstance {
            
            [SetUp]
            public void StartRave() {
                Rave rave = Rave.loginWithWorkspace1();
                Assert.IsTrue(rave.isMainWindowsVisible());
            }

            [TearDown]
            public void CloseRave() {
                //TODO: automatically close Rave
            }

        }

        //TODO: move all concurrency tests into a namespace with a SetUpFixture starting two instances of Rave 
        //[Test]
        public void TestLoadPack() {
            //Rave rave = Rave.attach();
            Rave rave = Rave.loginWithWorkspace1();
            Rave rave2 = Rave.loginWithWorkspace2();
            rave.WaveBrowser.open();
            rave2.WaveBrowser.open();
            rave.WaveBrowser.loadPack("TEST");
            rave2.WaveBrowser.loadPack("TEST");
            Assert.IsTrue(true);
        }


        [Test]
        public void TestDeleteAndAddResolveToModify() {
            Rave rave = Rave.attach();
            rave.WaveBrowser.open();
            rave.WaveBrowser.loadPack("TEST");
            //precondition
            //Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.PC, "TEST"));
            Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.PS3, "TEST"));
            Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.XBOX360, "TEST"));
            Assert.IsTrue(rave.WaveBrowser.nodeExists("TEST", "BANK", "sampleRate"));
            //test
            rave.WaveBrowser.deleteNode("TEST", "BANK", "sampleRate");
            rave.WaveBrowser.addTag(WaveBrowser.Tag.sampleRate, /*Rave.Platform.PC|*/Rave.Platform.PS3|Rave.Platform.XBOX360, "TEST", "BANK");
            rave.WaveBrowser.commitChanges();
            //Assert.IsTrue(rave.PendingWaves.isOperation(PendingWaves.Operation.modify, Rave.Platform.PC, "TEST", "BANK", "sampleRate"));
            Assert.IsTrue(rave.PendingWaves.isOperation(PendingWaves.Operation.modify, Rave.Platform.PS3, "TEST", "BANK", "sampleRate"));
            Assert.IsTrue(rave.PendingWaves.isOperation(PendingWaves.Operation.modify, Rave.Platform.XBOX360, "TEST", "BANK", "sampleRate"));
            //revert changes
            rave.WaveBrowser.buildWavesForAllPlatforms("TEST");
            //Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.PC, "TEST"));
            Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.PS3, "TEST"));
            Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.XBOX360, "TEST"));
            
        }


        [Test]
        public void TestAddAndDeleteResolveToNothing() {
            Rave rave = Rave.attach();
            rave.WaveBrowser.open();
            rave.WaveBrowser.loadPack("TEST");
            
            rave.WaveBrowser.addTag(WaveBrowser.Tag.compression, Rave.Platform.PS3, "TEST");
            rave.WaveBrowser.deleteNode("TEST", "compression (PS3)");
            rave.WaveBrowser.commitChanges();
            Assert.IsFalse(rave.PendingWaves.nodeExists(Rave.Platform.PS3, "TEST", "compression"));
        }

        [Test]
        public void TestChangeAndDeleteResolveToDelete() {
            Rave rave = Rave.attach();
            rave.WaveBrowser.open();
            rave.WaveBrowser.loadPack("TEST");
            //precondition
            //Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.PC, "TEST"));
            Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.PS3, "TEST"));
            Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.XBOX360, "TEST"));
            Assert.IsTrue(rave.WaveBrowser.nodeExists("TEST", "BANK", "sampleRate"));
            //test
            rave.WaveBrowser.changeTag("testtext", Rave.Platform.PS3, "TEST", "BANK", "sampleRate");
            rave.WaveBrowser.deleteNode("TEST", "BANK", "sampleRate");
            rave.WaveBrowser.commitChanges();
            Assert.IsTrue(rave.PendingWaves.isOperation(PendingWaves.Operation.remove, Rave.Platform.PS3, "TEST", "BANK", "sampleRate"));
            //revert changes
            rave.WaveBrowser.addTag(WaveBrowser.Tag.sampleRate,  /*Rave.Platform.PC|*/Rave.Platform.PS3|Rave.Platform.XBOX360, "TEST", "BANK");
            rave.WaveBrowser.commitChanges();
            rave.WaveBrowser.buildWavesForAllPlatforms("TEST");
            //Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.PC, "TEST"));
            Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.PS3, "TEST"));
            Assert.IsFalse(rave.PendingWaves.isThereAnyPendingWaveFor(Rave.Platform.XBOX360, "TEST"));
            Assert.IsTrue(rave.PendingWaves.isTagValue("", Rave.Platform.PS3, "TEST", "BANK", "sampleRate"));
        }

        [Test]
        public void TestAddAndChangeResolveToAdd() {
            Rave rave = Rave.attach();
            rave.WaveBrowser.open();
            rave.WaveBrowser.loadPack("TEST");

            rave.WaveBrowser.addTag(WaveBrowser.Tag.pain, /*Rave.Platform.PC|*/Rave.Platform.PS3|Rave.Platform.XBOX360, "TEST");
            rave.WaveBrowser.changeTag("testtext", Rave.Platform.PS3, "TEST", "pain");
            rave.WaveBrowser.commitChanges();
            Assert.IsTrue(rave.PendingWaves.isOperation(PendingWaves.Operation.add, Rave.Platform.PS3, "TEST", "pain"));
            //revert changes
            rave.WaveBrowser.deleteNode("TEST", "pain");
            rave.WaveBrowser.commitChanges();
            //Assert.IsFalse(rave.PendingWaves.nodeExists(Rave.Platform.PC, "TEST", "pain"));
            Assert.IsFalse(rave.PendingWaves.nodeExists(Rave.Platform.PS3, "TEST", "pain"));
            Assert.IsFalse(rave.PendingWaves.nodeExists(Rave.Platform.XBOX360, "TEST", "pain"));
        }


    }
}
