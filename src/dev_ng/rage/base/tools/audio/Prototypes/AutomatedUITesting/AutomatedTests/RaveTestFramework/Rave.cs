﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.MenuItems;
using TestStack.White.UIItems.TreeItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WindowStripControls;
using TestStack.White.Configuration;
using System.Diagnostics;

namespace AutomatedRaveTests.RaveTestFramework {
    public class Rave {

        static private KeyValueConfigurationCollection settings;
        private Window mainWindow;
        private WaveBrowser waveBrowser;
        private PendingWaves pendingWaves;

        static Rave() {
            string assemblyPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
            Configuration cfg = ConfigurationManager.OpenExeConfiguration(assemblyPath);
            settings = cfg.AppSettings.Settings;
        }

        private Rave(Window mainWindow) {
            this.mainWindow = mainWindow;
            this.waveBrowser = new WaveBrowser(mainWindow);
            this.pendingWaves = new PendingWaves();
        }

        

        /// <summary>
        /// use the binary or to combine values (i.e. PC | PS3)
        /// use the binary and to test (i.e. (value & PC) != 0)
        /// or use the HasFlag method to test (i.e. value.HasFlag(PC))
        /// </summary>
        [Flags]
        public enum Platform {
            XBOX360=1,
            PC=2,
            PS3=4
        }

        private static Rave login(string workspace, string pathToRaveExe, string arguments) {
            ProcessStartInfo raveProcessInfo = new ProcessStartInfo(pathToRaveExe, arguments);
            Application application = Application.Launch(raveProcessInfo);
            application.WaitWhileBusy();
            Window login = application.GetWindow("Project Preferences", InitializeOption.NoCache);
            TextBox passwordTB = login.Get<TextBox>("m_password");
            passwordTB.Text = settings["Password"].Value;
            TextBox workspaceTB = login.Get<TextBox>("m_assetProject");
            workspaceTB.Text = workspace;
            Button okBtn = login.Get<Button>("btnOk");
            okBtn.Click();
            application.WaitWhileBusy();
            CoreAppXmlConfiguration.Instance.FindWindowTimeout=90000;
            Window window = application.GetWindow(settings["ProjectTitleBar"].Value, InitializeOption.NoCache);
            //reset timeout
            CoreAppXmlConfiguration.Instance.FindWindowTimeout=30000;
            window.MenuBar.MenuItem("Window", "Load Layout", "default.xml").Click();
            return new Rave(window);
        }

        public static Rave loginWithWorkspace1() {

            return login(settings["Workspace1"].Value, settings["RavePath"].Value, "-noremotecontrol");
        }

        public static Rave loginWithWorkspace2() {
            return login(settings["Workspace2"].Value, settings["RavePath"].Value, "-noremotecontrol");
        }

        public static Rave attach() {
            System.Diagnostics.Process[] allProcesses = System.Diagnostics.Process.GetProcesses();
            System.Diagnostics.Process[] foundProcesses = System.Diagnostics.Process.GetProcessesByName("rave");
            if(foundProcesses.Length<1) throw new Exception("Rave not running");

            Application application = Application.Attach("rave");
            Window window = application.GetWindow(settings["ProjectTitleBar"].Value, InitializeOption.NoCache);
            return new Rave(window);
        }

        public bool isMainWindowsVisible() {
            return this.mainWindow.Visible;
        }

        public WaveBrowser WaveBrowser {
            get {
                return waveBrowser;
            }
        }

        public PendingWaves PendingWaves {
            get {
                return pendingWaves;
            }
        }
    }
}
