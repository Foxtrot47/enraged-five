﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickGraph;

namespace HierarchyBrowser.Model {

    /// <summary>
    /// Graph representing a Sound hierarchy
    /// </summary>
    public class SoundHierarchy: BidirectionalGraph<Sound, IEdge<Sound>> {

        public SoundHierarchy(): base(false) { }
    }
}
