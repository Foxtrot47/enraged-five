﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GraphSharp.Controls;
using GraphSharp;
using QuickGraph;
using HierarchyBrowser.Model;

namespace HierarchyBrowser {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// Selection and moving logic of vertices is handled here
    /// </summary>
    public partial class MainWindow: Window {

        public MainWindow()
        {
            InitializeComponent();
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            graphLayout.Relayout();
        }


        bool mouseDownRubberband = false;
        Point mouseDownPosRubberband;

        /// <summary>
        /// Create rubberband selection
        /// </summary>
        private void graphGrid_MouseDown(object sender, MouseButtonEventArgs e) {
            if(Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) {
                mouseDownRubberband = true;
                mouseDownPosRubberband = e.GetPosition(graphGrid);
                graphGrid.CaptureMouse();
      
                Canvas.SetLeft(selectionBox, mouseDownPosRubberband.X);
                Canvas.SetTop(selectionBox, mouseDownPosRubberband.Y);
                selectionBox.Width = 0;
                selectionBox.Height = 0;

                selectionBox.Visibility = Visibility.Visible;
            }

        }

        /// <summary>
        /// Hide rubberband selection or stop moving selected vertices
        /// </summary>
        private void graphGrid_MouseUp(object sender, MouseButtonEventArgs e) {
            if(Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)){
                mouseDownRubberband = false;
                graphGrid.ReleaseMouseCapture();

                selectionBox.Visibility = Visibility.Collapsed;

                Point mouseUpPos = e.GetPosition(graphGrid);

                graphLayout.setIntersectingVerticesSelected(selectionBox, graphGrid);
            }
            if(mouseDownOnVertex) {
                mouseDownOnVertex=false;
            }
        }

        /// <summary>
        /// Resize rubberband selection or move selected vertices
        /// </summary>
        private void graphGrid_MouseMove(object sender, MouseEventArgs e) {
            if(Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) {
                if(mouseDownRubberband) {
                    Point mousePos = e.GetPosition(graphGrid);

                    if(mouseDownPosRubberband.X < mousePos.X) {
                        Canvas.SetLeft(selectionBox, mouseDownPosRubberband.X);
                        selectionBox.Width = mousePos.X - mouseDownPosRubberband.X;
                    }
                    else {
                        Canvas.SetLeft(selectionBox, mousePos.X);
                        selectionBox.Width = mouseDownPosRubberband.X - mousePos.X;
                    }

                    if(mouseDownPosRubberband.Y < mousePos.Y) {
                        Canvas.SetTop(selectionBox, mouseDownPosRubberband.Y);
                        selectionBox.Height = mousePos.Y - mouseDownPosRubberband.Y;
                    }
                    else {
                        Canvas.SetTop(selectionBox, mousePos.Y);
                        selectionBox.Height = mouseDownPosRubberband.Y - mousePos.Y;
                    }
                }
            }
            if(mouseDownOnVertex) {
                Point currentMousePosition = e.GetPosition(graphLayout);
                double deltaX = currentMousePosition.X - initialVertexPosition.X;
                double deltaY = currentMousePosition.Y - initialVertexPosition.Y;
                graphLayout.moveSelectedVertices(deltaX, deltaY);
                initialVertexPosition = currentMousePosition;
            }

        }

        private void EdgeControl_MouseDown(object sender, MouseEventArgs e) {
            //Remove edge could be implemented here
        }

        /// <summary>
        /// Unselect selection
        /// </summary>
        private void Window_KeyDown(object sender, KeyEventArgs e) {
            if(e.Key==Key.Escape) graphLayout.unselectAllVertices();
        }

        bool mouseDownOnVertex = false;
        Point initialVertexPosition = new Point();

        /// <summary>
        /// Add vertex to selection if a crtl key is pressed. Otherwise select only this vertex
        /// </summary>
        private void vertexBorder_MouseDown(object sender, MouseButtonEventArgs e) {
            mouseDownOnVertex=true;
            initialVertexPosition = e.GetPosition(graphLayout);

            Sound pressedSound = (Sound)((VertexControl)(((System.Windows.Controls.Border)sender).TemplatedParent)).Vertex;
            if(!pressedSound.IsSelected) {
                if(!Keyboard.IsKeyDown(Key.LeftCtrl) && !Keyboard.IsKeyDown(Key.RightCtrl)) {
                    graphLayout.unselectAllVertices();
                }
                pressedSound.IsSelected = true;
            }
            e.Handled = true;
        }

        private void vertexBorder_MouseUp(object sender, MouseButtonEventArgs e) {
            mouseDownOnVertex = false;
        }

        /// <summary>
        /// Test handler for showing resizing of an UI element in a vertex
        /// TODO: Remove
        /// </summary>
        private void Button_Click(object sender, RoutedEventArgs e) {
            if(((Button)sender).Height!=200) {
                ((Button)sender).Height=200;
                ((Button)sender).Content="SimpleSound                              \nCollapsed Overview\nChanged properties: ";
            }
            else {
                ((Button)sender).Height=500;
                ((Button)sender).Content="SimpleSound                              \nExpanded View\nSound: \nBank: \nDescription: \nTimestamp: ";
            }
        }

    }

}
