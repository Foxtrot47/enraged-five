﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace HierarchyBrowser.Model {
    public class SimpleSound: Sound {


        public String Wave { get; set; }
        public String Bank { get; set; }

        public SimpleSound(String wave, String bank) {
            this.Wave = wave;
            this.Bank = bank;
        }
    }
}
