﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraphSharp.Controls;
using QuickGraph;
using System.Windows.Input;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Controls;

namespace HierarchyBrowser.Model {
    public class SoundHierarchyLayout: GraphLayout<Sound, IEdge<Sound>, SoundHierarchy> {

        /// <summary>
        /// Sets the IsSelected property of all Sounds intersected by the given selectionRubberBand to true
        /// </summary>
        public void setIntersectingVerticesSelected(Rectangle selectionRubberBand, Visual relativeVisual){
            foreach(KeyValuePair<Sound, VertexControl> entry in base._vertexControls){
                Rect rect1= entry.Value.TransformToVisual(relativeVisual).TransformBounds(new Rect(0, 0, entry.Value.ActualWidth, entry.Value.ActualHeight));
                Rect rect2 = selectionRubberBand.TransformToVisual(relativeVisual).TransformBounds(new Rect(0, 0, selectionRubberBand.Width, selectionRubberBand.Height));
                if(rect1.IntersectsWith(rect2)) entry.Key.IsSelected = true;
            }
        }

        /// <summary>
        /// Returns true if a Sound is intersected by the given Point
        /// </summary>
        public bool isIntersectingVertex(Point p, Visual relativeVisual) {
            foreach(KeyValuePair<Sound, VertexControl> entry in base._vertexControls) {
                Rect rect1= entry.Value.TransformToVisual(relativeVisual).TransformBounds(new Rect(0, 0, entry.Value.ActualWidth, entry.Value.ActualHeight));
                if(rect1.Contains(p)) return true;
            }
            return false;
        }

        public void unselectAllVertices() {
            foreach(KeyValuePair<Sound, VertexControl> entry in base._vertexControls) {
                entry.Key.IsSelected = false;
            }
        }

        public void moveSelectedVertices(double deltaX, double deltaY) {
            foreach(KeyValuePair<Sound, VertexControl> entry in base._vertexControls) {
                if(entry.Key.IsSelected) {
                    GraphCanvas.SetX(entry.Value, GraphCanvas.GetX(entry.Value)+deltaX);
                    GraphCanvas.SetY(entry.Value, GraphCanvas.GetY(entry.Value)+deltaY);
                }
            }
        }

    }
}
