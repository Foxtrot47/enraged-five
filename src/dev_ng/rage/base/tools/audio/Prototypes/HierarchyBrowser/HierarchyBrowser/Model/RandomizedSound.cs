﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HierarchyBrowser.Model {
    public class RandomizedSound: Sound {

        public String Description { get; set; }
        public int HistorySpace { get; set; }

        public RandomizedSound(String description, int historyspace) {
            this.Description = description;
            this.HistorySpace = historyspace;
        }
    }
}
