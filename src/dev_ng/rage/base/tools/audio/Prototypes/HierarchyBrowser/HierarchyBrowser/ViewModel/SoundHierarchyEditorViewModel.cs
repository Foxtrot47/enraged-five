﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using HierarchyBrowser.Model;
using QuickGraph;
using System.Windows.Input;

namespace HierarchyBrowser.ViewModel {
    class SoundHierarchyEditorViewModel: INotifyPropertyChanged {

        private string layoutAlgorithmType;
        private SoundHierarchy graph;
        private List<string> layoutAlgorithmTypes = new List<string>();


        public SoundHierarchyEditorViewModel(){

            generateRandomGraph();

            //Add Layout Algorithm Types
            layoutAlgorithmTypes.Add("BoundedFR");
            layoutAlgorithmTypes.Add("Circular");
            layoutAlgorithmTypes.Add("CompoundFDP");
            layoutAlgorithmTypes.Add("EfficientSugiyama");
            layoutAlgorithmTypes.Add("FR");
            layoutAlgorithmTypes.Add("ISOM");
            layoutAlgorithmTypes.Add("KK");
            layoutAlgorithmTypes.Add("LinLog");
            layoutAlgorithmTypes.Add("Tree");
            
            //Pick a default Layout Algorithm Type
            LayoutAlgorithmType = "Tree";

        }


        /// <summary>
        /// Generates a dummy SoundHierarchy for testing
        /// </summary>
        public void generateRandomGraph() {
            SoundHierarchy graph = new SoundHierarchy();

            int parent = 0;
            var random = new Random(DateTime.Now.Millisecond);
            for(int i = 0; i < 100; i++) {
                if(random.Next()%2==0)graph.AddVertex(new SimpleSound("WAVE_NO_"+i.ToString(), "STANDARD_BANK"));
                else graph.AddVertex(new RandomizedSound("Random Sound no "+i, 4));
                graph.AddEdge(new Edge<Sound>(graph.Vertices.ElementAt(parent), graph.Vertices.ElementAt(i)));
                if(random.Next()%4==0) {
                    Random selector = new Random();

                    parent = selector.Next(0, graph.OutEdges(graph.Vertices.ElementAt(parent)).Count()-1);
                }
            }

            this.Graph = graph;
        }


        public List<String> LayoutAlgorithmTypes {
            get { 
                return layoutAlgorithmTypes; 
            }
        }

        public string LayoutAlgorithmType{
            get { 
                return layoutAlgorithmType; 
            }
            set {
                layoutAlgorithmType = value;
                NotifyPropertyChanged("LayoutAlgorithmType");
            }
        }

        public SoundHierarchy Graph {
            get { 
                return graph; 
            }
            set {
                graph = value;
                NotifyPropertyChanged("Graph");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info) {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public ICommand GenerateGraph { get { return new RelayCommand(generateRandomGraph); } }


    }
}
