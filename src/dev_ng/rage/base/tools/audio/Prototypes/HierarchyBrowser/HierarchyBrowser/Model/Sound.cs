﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace HierarchyBrowser.Model {

    /// <summary>
    /// Base class for all types of Sounds in a SoundHierarchy 
    /// </summary>
    public abstract class Sound: DependencyObject {

        public static readonly DependencyProperty IsSelectedProperty = 
            DependencyProperty.Register(
            "IsSelected", typeof(Boolean), typeof(Sound));

        public bool IsSelected {
            get { return (bool) GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
    }
}
