﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ModelLib
{
    public class ObjectRef: ModelBase, IXmlSerializable
    {
        private System.String _Reference;
        public System.String Reference
        {
            get { return _Reference; }
            set { SetField(ref _Reference, value, () => Reference); }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            _Reference = reader.ReadElementContentAsString();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteString(_Reference);
        }
    }
}
