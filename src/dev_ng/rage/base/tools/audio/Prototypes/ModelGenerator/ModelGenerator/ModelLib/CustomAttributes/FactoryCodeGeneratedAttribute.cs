﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    //This custom attribute represents the IsFactoryCodeGenerated xml element
    [AttributeUsage(AttributeTargets.Class)]
    public class FactoryCodeGeneratedAttribute : Attribute
    {
        public FactoryCodeGeneratedAttribute(bool value)
        {
            this.value = value;
        }

        private readonly bool value;
        public bool Value
        {
            get
            {
                return this.value;
            }
        }
    }
}
