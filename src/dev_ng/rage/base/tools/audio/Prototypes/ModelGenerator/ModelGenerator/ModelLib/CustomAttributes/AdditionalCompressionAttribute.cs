﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    //the presence of this custom attribute is equivalent to AdditionalCompression="yes" in the xml
    [AttributeUsage(AttributeTargets.Class)]
    public class AdditionalCompressionAttribute: Attribute
    {
    }
}
