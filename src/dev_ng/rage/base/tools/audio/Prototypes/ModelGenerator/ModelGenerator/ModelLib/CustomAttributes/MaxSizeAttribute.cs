﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    //to be applied on a collection
    [AttributeUsage(AttributeTargets.Property)]
    public class MaxSizeAttribute: Attribute
    {
        public MaxSizeAttribute(ulong maxSize, Type maxSizeType) : this(maxSize)
        {
            this.numType = maxSizeType;
        }

        public MaxSizeAttribute(ulong maxSize)
        {
            this.value = maxSize;
        }

        //represents the value of the maxOccurs attribute
        private readonly ulong value;
        public ulong Value
        {
            get
            {
                return this.value;
            }
        }

        //represents the value of the numType attribute
        private readonly Type numType=null;
        public Type NumType
        {
            get
            {
                return this.numType;
            }
        }
    }
}
