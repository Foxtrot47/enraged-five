﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using ModelGeneratorLib.SchemaModel;
using ModelLib;
using ModelLib.Types;

namespace ModelGeneratorLib.Templates
{
    partial class ModelClassTemplate
    {
        private List<AttributeModel> attributes = new List<AttributeModel>(); 
        private bool isAbstract;
        private bool isInheriting;
        private string inheritsFrom;
        private string name;

        private List<FieldModel> fields = new List<FieldModel>(); 
        private List<CompositeFieldModel> compositeFields = new List<CompositeFieldModel>(); 
        
        public static bool checkForYesContent(String stringToCheck)
        {
            return !string.IsNullOrEmpty(stringToCheck) && stringToCheck.Equals("yes", StringComparison.CurrentCultureIgnoreCase);
        }

        public ModelClassTemplate(TypeDefinitionsTypeDefinition typeDefinition)
        {
            name = typeDefinition.name;

            isAbstract = (checkForYesContent(typeDefinition.isAbstract));

            isInheriting = !string.IsNullOrEmpty(typeDefinition.inheritsFrom);
            if (isInheriting) inheritsFrom = typeDefinition.inheritsFrom;

            if (checkForYesContent(typeDefinition.AdditionalCompression))
                attributes.Add(new AttributeModel("ModelLib.CustomAttributes.AdditionalCompression"));

            if (!string.IsNullOrEmpty(typeDefinition.IsFactoryCodeGenerated))
            {
                if (typeDefinition.IsFactoryCodeGenerated.Equals("yes", StringComparison.CurrentCultureIgnoreCase))
                    attributes.Add(new AttributeModel("ModelLib.CustomAttributes.FactoryCodeGenerated", "true"));
                if (typeDefinition.IsFactoryCodeGenerated.Equals("no", StringComparison.CurrentCultureIgnoreCase))
                    attributes.Add(new AttributeModel("ModelLib.CustomAttributes.FactoryCodeGenerated", "false"));
            }

            if(checkForYesContent(typeDefinition.RequiresHeader))
                attributes.Add(new AttributeModel("ModelLib.CustomAttributes.RequiresHeader"));

            if(!string.IsNullOrEmpty(typeDefinition.Transformer))
                attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Transformer", "\"" + typeDefinition.Transformer.Replace("\\", "\\\\").Replace("\"", "\\\"") + "\""));

            if(!string.IsNullOrEmpty(typeDefinition.align))
                attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Align", typeDefinition.align));

            if(!string.IsNullOrEmpty(typeDefinition.group)) 
                attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Group", "\""+typeDefinition.group+"\""));
            
            if(checkForYesContent(typeDefinition.isCompressed))
                attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Compressed"));

            if (!string.IsNullOrEmpty(typeDefinition.packed))
            {
                if (typeDefinition.packed.Equals("yes", StringComparison.CurrentCultureIgnoreCase))
                    attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Packed", "true"));
                if (typeDefinition.packed.Equals("no", StringComparison.CurrentCultureIgnoreCase))
                    attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Packed", "false"));
            }

            if(checkForYesContent(typeDefinition.skipDeleteContainerOnRefDelete))
				attributes.Add(new AttributeModel("ModelLib.CustomAttributes.SkipDeleteContainerOnRefDelete"));

            if(!string.IsNullOrEmpty(typeDefinition.validator))
                attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Validator", "\"" + typeDefinition.validator + "\""));

            addFieldStructure(typeDefinition.Items, this.fields, this.compositeFields);
        }

        private Type getFieldType(string typeString)
        {
            if (typeString.Equals("u8", StringComparison.CurrentCultureIgnoreCase)) return typeof(byte);
            if (typeString.Equals("s8", StringComparison.CurrentCultureIgnoreCase)) return typeof(sbyte);
            if (typeString.Equals("u16", StringComparison.CurrentCultureIgnoreCase)) return typeof(ushort);
            if (typeString.Equals("s16", StringComparison.CurrentCultureIgnoreCase)) return typeof(short);
            if (typeString.Equals("u32", StringComparison.CurrentCultureIgnoreCase)) return typeof(uint);
            if (typeString.Equals("s32", StringComparison.CurrentCultureIgnoreCase)) return typeof(int);
            if (typeString.Equals("u64", StringComparison.CurrentCultureIgnoreCase)) return typeof(ulong);
            if (typeString.Equals("s64", StringComparison.CurrentCultureIgnoreCase)) return typeof(long);
            if (typeString.Equals("f32", StringComparison.CurrentCultureIgnoreCase)) return typeof(float);
            if (typeString.Equals("cstring", StringComparison.CurrentCultureIgnoreCase)) return typeof(string);
            if (typeString.Equals("string", StringComparison.CurrentCultureIgnoreCase)) return typeof(string);
            if (typeString.Equals("tristate", StringComparison.CurrentCultureIgnoreCase)) return typeof(TriState);
            if (typeString.Equals("bit", StringComparison.CurrentCultureIgnoreCase)) return typeof(Bit);
            if (typeString.Equals("hash", StringComparison.CurrentCultureIgnoreCase)) return typeof(string);
            if (typeString.Equals("partialhash", StringComparison.CurrentCultureIgnoreCase)) return typeof(string);
            if (typeString.Equals("enum", StringComparison.CurrentCultureIgnoreCase)) return typeof(Enum);
            return null;
        }


        private void addFieldStructure(object[] items, List<FieldModel> fields, List<CompositeFieldModel> compositeFields)
        {
            if (items == null) return;
            foreach (object item in items)
            {
                if (item is Field)
                {
                    Field fieldDefinition = (Field)item;

                    string type = getFieldType(fieldDefinition.type).ToString();
                    if (type.Equals(typeof(Enum).ToString()) && !string.IsNullOrEmpty(fieldDefinition.@enum))
                        type = fieldDefinition.@enum;

                    FieldModel fieldModel = new FieldModel(fieldDefinition.name, type);

                    if (!string.IsNullOrEmpty(fieldDefinition.@default))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Default", "\"" + fieldDefinition.@default + "\""));

                    if (checkForYesContent(fieldDefinition.allowOverrideControl))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.AllowOverrideControl"));

                    if (!string.IsNullOrEmpty(fieldDefinition.allowedType))
                    {
                        string allowedTypesArray="";
                        foreach (string allowedType in fieldDefinition.allowedType.Split(','))
                        {
                            if (string.IsNullOrEmpty(allowedTypesArray))
                                allowedTypesArray = "typeof(" + allowedType.Trim() + ")";
                            else allowedTypesArray += ", typeof(" + allowedType.Trim() + ")";
                        }
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.AllowedType", allowedTypesArray));
                    }

                    if (checkForYesContent(fieldDefinition.defaultDropField))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.DefaultDropField"));

                    if (!string.IsNullOrEmpty(fieldDefinition.description))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Description", "\"" + fieldDefinition.description + "\""));

                    if (!string.IsNullOrEmpty(fieldDefinition.display))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Display", "\"" + fieldDefinition.display + "\""));

                    if (!string.IsNullOrEmpty(fieldDefinition.displayGroup))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.DisplayGroup", "\"" + fieldDefinition.displayGroup + "\""));

                    if (checkForYesContent(fieldDefinition.ignore))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Ignore"));

                    if (!string.IsNullOrEmpty(fieldDefinition.length))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Length", fieldDefinition.length));

                    if (!string.IsNullOrEmpty(fieldDefinition.max))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Max", fieldDefinition.max));

                    if (!string.IsNullOrEmpty(fieldDefinition.min))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Min", fieldDefinition.min));

                    if (!string.IsNullOrEmpty(fieldDefinition.outputVariable))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.OutputVariable"));

                    if (checkForYesContent(fieldDefinition.requiresValue))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.RequiresValue"));

                    if (!string.IsNullOrEmpty(fieldDefinition.units))
                    {
                        if (fieldDefinition.units.Equals("ObjectRef", StringComparison.OrdinalIgnoreCase))
                        {
                            fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.SerializeAs",
                                "typeof(" + fieldModel.type + ")"));
                            fieldModel.type = typeof(ModelLib.ObjectRef).ToString();
                        }
                        else if (fieldDefinition.units.Equals("stringtableindex", StringComparison.OrdinalIgnoreCase))
                        {
                            fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.SerializeAs",
                                "typeof(" + fieldModel.type + ")"));
                            fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Unit",
                                "\"" + fieldDefinition.units + "\""));
                            fieldModel.type = typeof(string).ToString();
                        }
                        else
                        {
                            fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Unit",
                                "\"" + fieldDefinition.units + "\""));
                        }
                    }

                    if (!string.IsNullOrEmpty(fieldDefinition.visible))
                        if (fieldDefinition.visible.Equals("false", StringComparison.CurrentCultureIgnoreCase) || fieldDefinition.visible.Equals("no", StringComparison.CurrentCultureIgnoreCase))
                            fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Hidden"));

                    fields.Add(fieldModel);
                }

                if (item is TypeDefinitionsTypeDefinitionAllocatedSpaceField)
                {
                    TypeDefinitionsTypeDefinitionAllocatedSpaceField fieldDefinition =
                        (TypeDefinitionsTypeDefinitionAllocatedSpaceField)item;

                    string type = getFieldType(fieldDefinition.type).ToString();

                    FieldModel fieldModel = new FieldModel(fieldDefinition.name, type);

                    if (!string.IsNullOrEmpty(fieldDefinition.@default))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Default", "\"" + fieldDefinition.@default + "\""));

                    if (!string.IsNullOrEmpty(fieldDefinition.max))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Max", fieldDefinition.max));

                    if (!string.IsNullOrEmpty(fieldDefinition.min))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Min", fieldDefinition.min));

                    if (!string.IsNullOrEmpty(fieldDefinition.visible))
                        if (fieldDefinition.visible.Equals("false", StringComparison.CurrentCultureIgnoreCase) || fieldDefinition.visible.Equals("no", StringComparison.CurrentCultureIgnoreCase))
                            fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Hidden"));

                    if (!string.IsNullOrEmpty(fieldDefinition.initialValue))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.InitialValue", fieldDefinition.initialValue));

                    fields.Add(fieldModel);
                }

                if (item is CompositeField)
                {
                    CompositeField fieldDefinition = (CompositeField) item;

                    CompositeFieldModel fieldModel = new CompositeFieldModel(fieldDefinition.name, getCompositeClassName(fieldDefinition.name));

                    if (checkForYesContent(fieldDefinition.allowOverrideControl))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.AllowOverrideControl"));

                    if (!string.IsNullOrEmpty(fieldDefinition.displayGroup))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.DisplayGroup", "\"" + fieldDefinition.displayGroup + "\""));

                    if (checkForYesContent(fieldDefinition.fixedSize))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.FixedSize"));

                    if (!string.IsNullOrEmpty(fieldDefinition.maxOccurs))
                    {
						string parameters = fieldDefinition.maxOccurs + (!string.IsNullOrEmpty(fieldDefinition.numType) ? ", typeof(" + getFieldType(fieldDefinition.numType) + ")" : string.Empty);
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.MaxSize", parameters));
                        fieldModel.isArray = true;
                    }

                    if (!string.IsNullOrEmpty(fieldDefinition.orderBy))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.OrderBy", "\"" + fieldDefinition.orderBy + "\""));

                    if (!string.IsNullOrEmpty(fieldDefinition.units))
                        fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Unit", "\"" + fieldDefinition.units + "\""));

                    if (!string.IsNullOrEmpty(fieldDefinition.visible))
                        if (fieldDefinition.visible.Equals("false", StringComparison.CurrentCultureIgnoreCase) || fieldDefinition.visible.Equals("no", StringComparison.CurrentCultureIgnoreCase))
                            fieldModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Hidden"));

                    addFieldStructure(fieldDefinition.Items, fieldModel.fields, fieldModel.compositeFields);

                    compositeFields.Add(fieldModel);
                }


            }
        }

        private HashSet<string> compositeDefNametable = new HashSet<string>();

        private string getCompositeClassName(string compositeName)
        {
            int index = 1;
            string compositeClassName = compositeName + "Definition";
            while (compositeDefNametable.Contains(compositeClassName))
            {
                index++;
                compositeClassName = compositeName + "Definition" + index;
            }
            compositeDefNametable.Add(compositeClassName);
            return compositeClassName;
        }

    }
}
