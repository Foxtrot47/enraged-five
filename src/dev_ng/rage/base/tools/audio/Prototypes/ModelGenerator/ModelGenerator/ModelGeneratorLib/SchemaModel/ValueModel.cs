﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelGeneratorLib.SchemaModel
{
    public class ValueModel
    {
        public ValueModel(string value)
        {
            this.value = value;
            this.attributes = new List<AttributeModel>();
        }

        public List<AttributeModel> attributes;
        public String value;
    }
}
