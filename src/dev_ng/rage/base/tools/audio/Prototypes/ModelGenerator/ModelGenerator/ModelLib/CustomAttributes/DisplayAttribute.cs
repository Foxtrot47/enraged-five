﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class DisplayAttribute : Attribute
    {
        public DisplayAttribute(string value)
        {
            this.value = value;
        }

        private readonly String value;
        public String Value
        {
            get
            {
                return this.value;
            }
        }
    }
}
