﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    //to be applied on a collection
    [AttributeUsage(AttributeTargets.Property)]
    public class OrderByAttribute : Attribute
    {
        public OrderByAttribute(string value)
        {
            this.value = value;
        }

        //the name of the field the elements of the collection are ordered by
        private readonly String value;
        public String Value
        {
            get
            {
                return this.value;
            }
        }
    }
}
