﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PackedAttribute: Attribute
    {
        public PackedAttribute(bool value)
        {
            this.value = value;
        }

        private readonly bool value;
        public bool Value
        {
            get
            {
                return this.value;
            }
        }
    }
}
