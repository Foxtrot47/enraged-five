using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using System.Windows.Forms;

namespace ModelGenerator.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            GenerateModel = new RelayCommand(() => GenerateModelExecute(), () => checkParameters());
            SelectXml = new RelayCommand(() => SelectXmlExecute(), () => true);
            SelectOutput = new RelayCommand(() => SelectOutputExecute(), () => true);
        }
        
        public RelayCommand GenerateModel { get; set; }

        public ICommand SelectXml { get; private set; }

        public ICommand SelectOutput { get; private set; }

        public String SourceXml { get; private set; }

        public String outputdir { get; private set; }

        private void GenerateModelExecute()
        {
            ModelGeneratorLib.ModelGenerator.generateModel(SourceXml, outputdir);
        }

        private bool checkParameters()
        {
            bool fileExists = File.Exists(SourceXml);
            bool dirExists = Directory.Exists(outputdir);
            return File.Exists(SourceXml) && Directory.Exists(outputdir);
        }

        private void SelectXmlExecute()
        {
            Microsoft.Win32.OpenFileDialog openFileDialog1 = new Microsoft.Win32.OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "Xml files (*.xml)|*.xml";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            bool? userClickedOK = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (userClickedOK == true)
            {
                // Open the selected file to read.
                SourceXml = openFileDialog1.FileName;
                RaisePropertyChanged("SourceXml");
                GenerateModel.RaiseCanExecuteChanged();
            }
        }

        private void SelectOutputExecute()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                outputdir = dialog.SelectedPath;
                RaisePropertyChanged("outputdir");
                GenerateModel.RaiseCanExecuteChanged();
            }
        }
    }
}