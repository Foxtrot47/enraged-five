﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ModelLib.Types
{
    public enum TriState
    {
        yes,
        no,
        [XmlEnum(Name = "")]
        unspecified = 0
    }
}
