﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class AllowedTypeAttribute : Attribute
    {
        public AllowedTypeAttribute(params Type[] value)
        {
            this.value = new List<Type>(value);
        }

        private readonly List<Type> value;
        public List<Type> Value
        {
            get
            {
                return this.value;
            }
        }
    }
}
