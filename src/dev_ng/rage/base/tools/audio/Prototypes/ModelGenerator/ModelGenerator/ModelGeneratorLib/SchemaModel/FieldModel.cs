﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelGeneratorLib.SchemaModel
{
    public class FieldModel
    {
        public FieldModel(string name, string type)
        {
            this.name = name;
            this.type = type;
            this.attributes = new List<AttributeModel>();
        }

        public List<AttributeModel> attributes;
        public String name;
        public String type;
    }
}
