﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelGeneratorLib.SchemaModel
{
    public class AttributeModel
    {
        public AttributeModel(string name, string value)
            : this(name)
        {
            this.value = value;
        }

        public AttributeModel(string name)
        {
            this.name = name;
        }

        public String name;
        public String value;
    }
}
