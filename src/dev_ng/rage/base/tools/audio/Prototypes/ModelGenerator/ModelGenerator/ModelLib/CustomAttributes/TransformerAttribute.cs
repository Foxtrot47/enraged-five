﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TransformerAttribute: Attribute
    {
        public TransformerAttribute(String value)
        {
            this.value = value;
        }

        private readonly String value;
        public String Value
        {
            get
            {
                return this.value;
            }
        }
    }
}
