﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelGeneratorLib.SchemaModel
{
    public class CompositeFieldModel
    {
        public CompositeFieldModel(string name, string className)
        {
            this.name = name;
            this.className = className;
            this.attributes = new List<AttributeModel>();
            this.fields = new List<FieldModel>();
            this.compositeFields = new List<CompositeFieldModel>();
        }

        public List<AttributeModel> attributes;
        public List<CompositeFieldModel> compositeFields;
        public List<FieldModel> fields;
        public String name;
        public String className;
        public bool isArray = false;
    }
}
