﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    public class SerializeAsAttribute: Attribute
    {
        public SerializeAsAttribute(Type value)
        {
            this.value = value;
        }

        private readonly Type value;
        public Type Value
        {
            get
            {
                return this.value;
            }
        }
    }
}
