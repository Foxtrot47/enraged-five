﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Enum)]
    public class InitialValueAttribute : Attribute
    {
        public InitialValueAttribute(double value)
        {
            this.value = value;
        }

        private readonly double value;
        public double Value
        {
            get
            {
                return this.value;
            }
        }
    }
}
