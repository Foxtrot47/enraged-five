﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ModelLib.Extentions
{
	public static class Serialization
	{

		public static string SerialializeToString<T>(this T toserialize) where T : ModelBase
		{
			XmlSerializer serializer = new XmlSerializer(typeof(T));

			using (StringWriter textWriter = new StringWriter())
			{
				serializer.Serialize(textWriter, toserialize);

				XmlDocument doc = new XmlDocument();
				doc.LoadXml(textWriter.ToString().Trim());
				XmlNode newNode = doc.DocumentElement;
				
				if (newNode != null)
				{
					string innerXml = newNode.InnerXml;

					return innerXml;
				}
				return string.Empty;
			}
		}

		public static XmlNode SerialializeToNode<T>(this T toserialize) where T : ModelBase
		{
			XmlSerializer serializer = new XmlSerializer(typeof(T));

			using (StringWriter textWriter = new StringWriter())
			{
				serializer.Serialize(textWriter, toserialize);

				XmlDocument doc = new XmlDocument();
				doc.LoadXml(textWriter.ToString().Trim());
				XmlNode newNode = doc.DocumentElement;


				return newNode;
			}
		}

		public static T DeserializeToModel<T>(this XmlNode objectNode) where T : ModelBase
		{
			XmlSerializer serializer = new XmlSerializer(typeof (T));
			XmlNodeReader reader = new XmlNodeReader(objectNode);
			
			return (T) serializer.Deserialize(reader);
		}
	}
}
