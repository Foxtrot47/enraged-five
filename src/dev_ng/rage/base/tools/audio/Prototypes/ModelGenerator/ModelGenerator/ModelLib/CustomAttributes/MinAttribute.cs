﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MinAttribute: Attribute
    {
        public MinAttribute(double value)
        {
            this.value = value;
        }

        private readonly double value;
        public double Value
        {
            get
            {
                return this.value;
            }
        }
    }
}
