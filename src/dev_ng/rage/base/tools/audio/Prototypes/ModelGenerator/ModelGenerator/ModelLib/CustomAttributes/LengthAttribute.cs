﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LengthAttribute : Attribute
    {
        public LengthAttribute(int value)
        {
            this.value = value;
        }

        private readonly int value;
        public int Value
        {
            get
            {
                return this.value;
            }
        }
    }
}
