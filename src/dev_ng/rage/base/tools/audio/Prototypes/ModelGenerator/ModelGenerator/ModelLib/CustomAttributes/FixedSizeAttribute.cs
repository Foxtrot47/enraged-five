﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    //to be applied on a collection
    //the presence of this custom attribute is equivalent to fixedSize="yes" in the xml
    [AttributeUsage(AttributeTargets.Property)]
    public class FixedSizeAttribute: Attribute
    {
    }
}
