﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLib.CustomAttributes
{
    //the presence of this custom attribute is equivalent to visible="no" or visible="false" in the xml
    [AttributeUsage(AttributeTargets.Property)]
    public class HiddenAttribute: Attribute
    {
    }
}
