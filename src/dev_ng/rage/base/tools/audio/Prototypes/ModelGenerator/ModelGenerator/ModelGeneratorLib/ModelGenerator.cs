﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using ModelGeneratorLib.Templates;

namespace ModelGeneratorLib
{
    public class ModelGenerator
    {
        public static void generateModel(string inputXML, string outputDirectory)
        {
            //Parse XML containing typedefinitions
            TextReader reader = new StreamReader(inputXML);
            XmlSerializer serializer = new XmlSerializer(typeof(TypeDefinitions));
            TypeDefinitions typeDefinitions = (TypeDefinitions)serializer.Deserialize(reader);
            reader.Close();

            
            foreach (object typeDefinitionsItem in typeDefinitions.Items)
            {
                //Generate Enums
                if (typeDefinitionsItem is TypeDefinitionsEnum)
                {
                    TypeDefinitionsEnum enumDefinition = (TypeDefinitionsEnum)typeDefinitionsItem;
                    ModelEnumTemplate templateInstance = new ModelEnumTemplate(enumDefinition);

                    String modelEnumContent = templateInstance.TransformText();
                    File.WriteAllText(Path.Combine(outputDirectory, enumDefinition.name + ".cs"), modelEnumContent);

                }

                //Generate Classes
                if (typeDefinitionsItem is TypeDefinitionsTypeDefinition)
                {
                    TypeDefinitionsTypeDefinition typeDefinition = (TypeDefinitionsTypeDefinition) typeDefinitionsItem;
                    ModelClassTemplate templateInstance = new ModelClassTemplate(typeDefinition);

                    String modelClassContent = templateInstance.TransformText();
                    System.IO.File.WriteAllText(Path.Combine(outputDirectory, typeDefinition.name+".cs"), modelClassContent);
                }
            }

        }
    }
}
