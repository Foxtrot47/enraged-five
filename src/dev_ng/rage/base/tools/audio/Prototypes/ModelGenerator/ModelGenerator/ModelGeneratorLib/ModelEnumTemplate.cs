﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelGeneratorLib.SchemaModel;

namespace ModelGeneratorLib.Templates
{
    partial class ModelEnumTemplate
    {
        private string name;
        private List<AttributeModel> attributes = new List<AttributeModel>();
        private List<ValueModel> values = new List<ValueModel>();

        public ModelEnumTemplate(TypeDefinitionsEnum enumDefinition)
        {
            name = enumDefinition.name;

            if (!string.IsNullOrEmpty(enumDefinition.initialValue))
                attributes.Add(new AttributeModel("ModelLib.CustomAttributes.InitialValue", enumDefinition.initialValue));

            if (ModelClassTemplate.checkForYesContent(enumDefinition.bitset))
                attributes.Add(new AttributeModel("ModelLib.CustomAttributes.BitSet"));

            foreach (TypeDefinitionsEnumValue value in enumDefinition.Value)
            {
                ValueModel valueModel = new ValueModel(value.Value);

                if (!string.IsNullOrEmpty(value.display))
                    valueModel.attributes.Add(new AttributeModel("ModelLib.CustomAttributes.Display", "\"" + value.display + "\""));

                values.Add(valueModel);
            }
        }
    }
}
