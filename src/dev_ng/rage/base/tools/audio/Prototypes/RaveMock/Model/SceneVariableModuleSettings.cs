using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	public class SceneVariableModuleSettings:  ModelBase {

		private System.String _SceneVariable;
		[ModelLib.CustomAttributes.Default("apply")]
		public System.String SceneVariable {
			get { return _SceneVariable; }
			set { SetField(ref _SceneVariable, value, () => SceneVariable); }
		}

		private ModelLib.ObjectRef _InputOutputCurve;
		[ModelLib.CustomAttributes.Default("LINEAR_RISE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef InputOutputCurve {
			get { return _InputOutputCurve; }
			set { SetField(ref _InputOutputCurve, value, () => InputOutputCurve); }
		}

		private MixModuleInput _Input;
		[ModelLib.CustomAttributes.Default("INPUT_NONE")]
		public MixModuleInput Input {
			get { return _Input; }
			set { SetField(ref _Input, value, () => Input); }
		}

		private System.Single _ScaleMin;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single ScaleMin {
			get { return _ScaleMin; }
			set { SetField(ref _ScaleMin, value, () => ScaleMin); }
		}

		private System.Single _ScaleMax;
		[ModelLib.CustomAttributes.Default("10.0")]
		public System.Single ScaleMax {
			get { return _ScaleMax; }
			set { SetField(ref _ScaleMax, value, () => ScaleMax); }
		}

	}
}


