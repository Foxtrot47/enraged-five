using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	public class DynamicMixModuleSettings:  ModelBase {

		private System.UInt16 _FadeIn;
		public System.UInt16 FadeIn {
			get { return _FadeIn; }
			set { SetField(ref _FadeIn, value, () => FadeIn); }
		}

		private System.UInt16 _FadeOut;
		public System.UInt16 FadeOut {
			get { return _FadeOut; }
			set { SetField(ref _FadeOut, value, () => FadeOut); }
		}

		private System.String _ApplyVariable;
		[ModelLib.CustomAttributes.Default("apply")]
		public System.String ApplyVariable {
			get { return _ApplyVariable; }
			set { SetField(ref _ApplyVariable, value, () => ApplyVariable); }
		}

		private System.Single _Duration;
		public System.Single Duration {
			get { return _Duration; }
			set { SetField(ref _Duration, value, () => Duration); }
		}

		private ModelLib.ObjectRef _ModuleTypeSettings;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ModuleTypeSettings {
			get { return _ModuleTypeSettings; }
			set { SetField(ref _ModuleTypeSettings, value, () => ModuleTypeSettings); }
		}

	}
}


