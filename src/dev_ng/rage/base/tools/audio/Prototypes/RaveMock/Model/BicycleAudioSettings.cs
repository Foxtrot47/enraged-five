using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class BicycleAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _ChainLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ChainLoop {
			get { return _ChainLoop; }
			set { SetField(ref _ChainLoop, value, () => ChainLoop); }
		}

		private ModelLib.ObjectRef _SprocketSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SprocketSound {
			get { return _SprocketSound; }
			set { SetField(ref _SprocketSound, value, () => SprocketSound); }
		}

		private ModelLib.ObjectRef _RePedalSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RePedalSound {
			get { return _RePedalSound; }
			set { SetField(ref _RePedalSound, value, () => RePedalSound); }
		}

		private ModelLib.ObjectRef _GearChangeSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GearChangeSound {
			get { return _GearChangeSound; }
			set { SetField(ref _GearChangeSound, value, () => GearChangeSound); }
		}

		private System.Single _RoadNoiseVolumeScale;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single RoadNoiseVolumeScale {
			get { return _RoadNoiseVolumeScale; }
			set { SetField(ref _RoadNoiseVolumeScale, value, () => RoadNoiseVolumeScale); }
		}

		private System.Single _SkidVolumeScale;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single SkidVolumeScale {
			get { return _SkidVolumeScale; }
			set { SetField(ref _SkidVolumeScale, value, () => SkidVolumeScale); }
		}

		private ModelLib.ObjectRef _SuspensionUp;
		[ModelLib.CustomAttributes.Default("SUSPENSION_UP")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.Display("Up Sound")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SuspensionUp {
			get { return _SuspensionUp; }
			set { SetField(ref _SuspensionUp, value, () => SuspensionUp); }
		}

		private ModelLib.ObjectRef _SuspensionDown;
		[ModelLib.CustomAttributes.Default("SUSPENSION_DOWN")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.Display("Down Sound")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SuspensionDown {
			get { return _SuspensionDown; }
			set { SetField(ref _SuspensionDown, value, () => SuspensionDown); }
		}

		private System.Single _MinSuspCompThresh;
		[ModelLib.CustomAttributes.Default("0.45")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		public System.Single MinSuspCompThresh {
			get { return _MinSuspCompThresh; }
			set { SetField(ref _MinSuspCompThresh, value, () => MinSuspCompThresh); }
		}

		private System.Single _MaxSuspCompThres;
		[ModelLib.CustomAttributes.Default("6.9")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		public System.Single MaxSuspCompThres {
			get { return _MaxSuspCompThres; }
			set { SetField(ref _MaxSuspCompThres, value, () => MaxSuspCompThres); }
		}

		private ModelLib.ObjectRef _JumpLandSound;
		[ModelLib.CustomAttributes.Default("JUMP_LAND_INTACT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef JumpLandSound {
			get { return _JumpLandSound; }
			set { SetField(ref _JumpLandSound, value, () => JumpLandSound); }
		}

		private ModelLib.ObjectRef _DamagedJumpLandSound;
		[ModelLib.CustomAttributes.Default("JUMP_LAND_LOOSE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DamagedJumpLandSound {
			get { return _DamagedJumpLandSound; }
			set { SetField(ref _DamagedJumpLandSound, value, () => DamagedJumpLandSound); }
		}

		private System.UInt32 _JumpLandMinThresh;
		[ModelLib.CustomAttributes.Default("31")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt32 JumpLandMinThresh {
			get { return _JumpLandMinThresh; }
			set { SetField(ref _JumpLandMinThresh, value, () => JumpLandMinThresh); }
		}

		private System.UInt32 _JumpLandMaxThresh;
		[ModelLib.CustomAttributes.Default("36")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt32 JumpLandMaxThresh {
			get { return _JumpLandMaxThresh; }
			set { SetField(ref _JumpLandMaxThresh, value, () => JumpLandMaxThresh); }
		}

		private ModelLib.ObjectRef _VehicleCollisions;
		[ModelLib.CustomAttributes.Default("VEHICLE_COLLISION_BICYCLE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(VehicleCollisionSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehicleCollisions {
			get { return _VehicleCollisions; }
			set { SetField(ref _VehicleCollisions, value, () => VehicleCollisions); }
		}

		private ModelLib.ObjectRef _BellSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BellSound {
			get { return _BellSound; }
			set { SetField(ref _BellSound, value, () => BellSound); }
		}

		private ModelLib.ObjectRef _BrakeBlock;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BrakeBlock {
			get { return _BrakeBlock; }
			set { SetField(ref _BrakeBlock, value, () => BrakeBlock); }
		}

		private ModelLib.ObjectRef _BrakeBlockWet;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BrakeBlockWet {
			get { return _BrakeBlockWet; }
			set { SetField(ref _BrakeBlockWet, value, () => BrakeBlockWet); }
		}

	}
}


