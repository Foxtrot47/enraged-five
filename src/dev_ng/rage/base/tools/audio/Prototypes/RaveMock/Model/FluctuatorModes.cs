using ModelLib.CustomAttributes;

namespace model {

	public enum FluctuatorModes {
		[ModelLib.CustomAttributes.Display("ProbabilityBased")]
		PROBABILITY_BASED,
		[ModelLib.CustomAttributes.Display("TimeBased")]
		TIME_BASED
	}

}