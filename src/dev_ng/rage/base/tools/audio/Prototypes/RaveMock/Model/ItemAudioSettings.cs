using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class ItemAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _WeaponSettings;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef WeaponSettings {
			get { return _WeaponSettings; }
			set { SetField(ref _WeaponSettings, value, () => WeaponSettings); }
		}

		public class ContextSettingsDefinition: ModelBase {
			private System.String _Context;
			public System.String Context {
				get { return _Context; }
				set { SetField(ref _Context, value, () => Context); }
			}

			private ModelLib.ObjectRef _WeaponSettings;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef WeaponSettings {
				get { return _WeaponSettings; }
				set { SetField(ref _WeaponSettings, value, () => WeaponSettings); }
			}

		}
		private ItemsObservableCollection<ContextSettingsDefinition> _ContextSettings = new ItemsObservableCollection<ContextSettingsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(64)]
		[XmlElement("ContextSettings")]
		public ItemsObservableCollection<ContextSettingsDefinition> ContextSettings {
			get { return _ContextSettings; }
			set { SetField(ref _ContextSettings, value, () => ContextSettings); }
		}

	}
}


