using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class StemMix:  ModelBase {

		private System.Int16 _Stem1Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Stem1Volume {
			get { return _Stem1Volume; }
			set { SetField(ref _Stem1Volume, value, () => Stem1Volume); }
		}

		private System.Int16 _Stem2Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Stem2Volume {
			get { return _Stem2Volume; }
			set { SetField(ref _Stem2Volume, value, () => Stem2Volume); }
		}

		private System.Int16 _Stem3Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Stem3Volume {
			get { return _Stem3Volume; }
			set { SetField(ref _Stem3Volume, value, () => Stem3Volume); }
		}

		private System.Int16 _Stem4Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Stem4Volume {
			get { return _Stem4Volume; }
			set { SetField(ref _Stem4Volume, value, () => Stem4Volume); }
		}

		private System.Int16 _Stem5Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Stem5Volume {
			get { return _Stem5Volume; }
			set { SetField(ref _Stem5Volume, value, () => Stem5Volume); }
		}

		private System.Int16 _Stem6Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Stem6Volume {
			get { return _Stem6Volume; }
			set { SetField(ref _Stem6Volume, value, () => Stem6Volume); }
		}

		private System.Int16 _Stem7Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Stem7Volume {
			get { return _Stem7Volume; }
			set { SetField(ref _Stem7Volume, value, () => Stem7Volume); }
		}

		private System.Int16 _Stem8Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Stem8Volume {
			get { return _Stem8Volume; }
			set { SetField(ref _Stem8Volume, value, () => Stem8Volume); }
		}

	}
}


