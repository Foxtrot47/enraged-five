using ModelLib.CustomAttributes;

namespace model {

	public enum ShoeTypes {
		[ModelLib.CustomAttributes.Display("Bare")]
		SHOE_TYPE_BARE,
		[ModelLib.CustomAttributes.Display("Boots")]
		SHOE_TYPE_BOOTS,
		[ModelLib.CustomAttributes.Display("Shoes")]
		SHOE_TYPE_SHOES,
		[ModelLib.CustomAttributes.Display("High Heels")]
		SHOE_TYPE_HIGH_HEELS,
		[ModelLib.CustomAttributes.Display("FlipFlops")]
		SHOE_TYPE_FLIPFLOPS,
		[ModelLib.CustomAttributes.Display("Flippers")]
		SHOE_TYPE_FLIPPERS,
		[ModelLib.CustomAttributes.Display("Trainers")]
		SHOE_TYPE_TRAINERS,
		[ModelLib.CustomAttributes.Display("Clown")]
		SHOE_TYPE_CLOWN,
		[ModelLib.CustomAttributes.Display("Golf")]
		SHOE_TYPE_GOLF
	}

}