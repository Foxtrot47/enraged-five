using ModelLib.CustomAttributes;

namespace model {

	public enum AnimalType {
		[ModelLib.CustomAttributes.Display("None")]
		kAnimalNone,
		[ModelLib.CustomAttributes.Display("Boar")]
		kAnimalBoar,
		[ModelLib.CustomAttributes.Display("Chicken")]
		kAnimalChicken,
		[ModelLib.CustomAttributes.Display("Dog")]
		kAnimalDog,
		[ModelLib.CustomAttributes.Display("Rottweiler")]
		kAnimalRottweiler,
		[ModelLib.CustomAttributes.Display("Horse")]
		kAnimalHorse,
		[ModelLib.CustomAttributes.Display("Cow")]
		kAnimalCow,
		[ModelLib.CustomAttributes.Display("Coyote")]
		kAnimalCoyote,
		[ModelLib.CustomAttributes.Display("Deer")]
		kAnimalDeer,
		[ModelLib.CustomAttributes.Display("Eagle")]
		kAnimalEagle,
		[ModelLib.CustomAttributes.Display("Fish")]
		kAnimalFish,
		[ModelLib.CustomAttributes.Display("Hen")]
		kAnimalHen,
		[ModelLib.CustomAttributes.Display("Monkey")]
		kAnimalMonkey,
		[ModelLib.CustomAttributes.Display("Mountain Lion")]
		kAnimalMountainLion,
		[ModelLib.CustomAttributes.Display("Pigeon")]
		kAnimalPigeon,
		[ModelLib.CustomAttributes.Display("Rat")]
		kAnimalRat,
		[ModelLib.CustomAttributes.Display("Seagull")]
		kAnimalSeagull,
		[ModelLib.CustomAttributes.Display("Crow")]
		kAnimalCrow,
		[ModelLib.CustomAttributes.Display("Pig")]
		kAnimalPig,
		[ModelLib.CustomAttributes.Display("Chickenhawk")]
		kAnimalChickenhawk,
		[ModelLib.CustomAttributes.Display("Cormorant")]
		kAnimalCormorant,
		[ModelLib.CustomAttributes.Display("Rhesus")]
		kAnimalRhesus,
		[ModelLib.CustomAttributes.Display("Retriever")]
		kAnimalRetriever,
		[ModelLib.CustomAttributes.Display("Chimp")]
		kAnimalChimp,
		[ModelLib.CustomAttributes.Display("Husky")]
		kAnimalHusky,
		[ModelLib.CustomAttributes.Display("Shepherd")]
		kAnimalShepherd
	}

}