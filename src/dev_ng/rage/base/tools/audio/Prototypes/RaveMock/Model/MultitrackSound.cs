using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class MultitrackSound:  Sound{

		private ModelLib.Types.TriState _StopWhenChildStops;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("If set to yes then the multitrack sound will stop itself when any one of its child sounds finish, otherwise it will continue playing until all child sounds have finished")]
		public ModelLib.Types.TriState StopWhenChildStops {
			get { return _StopWhenChildStops; }
			set { SetField(ref _StopWhenChildStops, value, () => StopWhenChildStops); }
		}

		private ModelLib.Types.TriState _SyncChildren;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("If set to yes then all children will start playback on the same sample.  Do not use if any children have predelays")]
		public ModelLib.Types.TriState SyncChildren {
			get { return _SyncChildren; }
			set { SetField(ref _SyncChildren, value, () => SyncChildren); }
		}

		public class SoundRefDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundId;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.RequiresValue]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef SoundId {
				get { return _SoundId; }
				set { SetField(ref _SoundId, value, () => SoundId); }
			}

			private System.String _ReferenceName;
			[ModelLib.CustomAttributes.Ignore]
			[ModelLib.CustomAttributes.Hidden]
			public System.String ReferenceName {
				get { return _ReferenceName; }
				set { SetField(ref _ReferenceName, value, () => ReferenceName); }
			}

		}
		private ItemsObservableCollection<SoundRefDefinition> _SoundRef = new ItemsObservableCollection<SoundRefDefinition>();
		[ModelLib.CustomAttributes.MaxSize(8)]
		[XmlElement("SoundRef")]
		public ItemsObservableCollection<SoundRefDefinition> SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

	}
}


