using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Math Sound Types")]
	public class MathOperationSound:  Sound{

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		private ModelLib.Types.TriState _OnlyRunOnce;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState OnlyRunOnce {
			get { return _OnlyRunOnce; }
			set { SetField(ref _OnlyRunOnce, value, () => OnlyRunOnce); }
		}

		public class OperationDefinition: ModelBase {
			private MathOperations _Operation;
			[ModelLib.CustomAttributes.Default("MATH_OPERATION_ADD")]
			public MathOperations Operation {
				get { return _Operation; }
				set { SetField(ref _Operation, value, () => Operation); }
			}

			private System.String _Result;
			[ModelLib.CustomAttributes.Unit("variable")]
			public System.String Result {
				get { return _Result; }
				set { SetField(ref _Result, value, () => Result); }
			}

			public class param1Definition: ModelBase {
				private System.Single _Value;
				[ModelLib.CustomAttributes.Default("0")]
				public System.Single Value {
					get { return _Value; }
					set { SetField(ref _Value, value, () => Value); }
				}

				private System.String _Variable;
				[ModelLib.CustomAttributes.Unit("variable")]
				public System.String Variable {
					get { return _Variable; }
					set { SetField(ref _Variable, value, () => Variable); }
				}

			}
			private param1Definition _param1;
			public param1Definition param1 {
				get { return _param1; }
				set { SetField(ref _param1, value, () => param1); }
			}

			public class param2Definition: ModelBase {
				private System.Single _Value;
				[ModelLib.CustomAttributes.Default("0")]
				public System.Single Value {
					get { return _Value; }
					set { SetField(ref _Value, value, () => Value); }
				}

				private System.String _Variable;
				[ModelLib.CustomAttributes.Unit("variable")]
				public System.String Variable {
					get { return _Variable; }
					set { SetField(ref _Variable, value, () => Variable); }
				}

			}
			private param2Definition _param2;
			public param2Definition param2 {
				get { return _param2; }
				set { SetField(ref _param2, value, () => param2); }
			}

			public class param3Definition: ModelBase {
				private System.Single _Value;
				[ModelLib.CustomAttributes.Default("0")]
				public System.Single Value {
					get { return _Value; }
					set { SetField(ref _Value, value, () => Value); }
				}

				private System.String _Variable;
				[ModelLib.CustomAttributes.Unit("variable")]
				public System.String Variable {
					get { return _Variable; }
					set { SetField(ref _Variable, value, () => Variable); }
				}

			}
			private param3Definition _param3;
			public param3Definition param3 {
				get { return _param3; }
				set { SetField(ref _param3, value, () => param3); }
			}

		}
		private ItemsObservableCollection<OperationDefinition> _Operation = new ItemsObservableCollection<OperationDefinition>();
		[ModelLib.CustomAttributes.MaxSize(10)]
		[XmlElement("Operation")]
		public ItemsObservableCollection<OperationDefinition> Operation {
			get { return _Operation; }
			set { SetField(ref _Operation, value, () => Operation); }
		}

	}
}


