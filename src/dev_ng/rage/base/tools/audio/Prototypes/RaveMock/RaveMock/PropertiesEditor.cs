﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.Integration;
using System.Windows.Forms;

namespace RaveMock
{
     public class PropertiesEditor : ContainerControl
    {
        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_DockableContent;

         TextBox content = new TextBox();

         public PropertiesEditor()
         {
             content.Height = 300;
             content.Width = 100;
             content.Text = "Content of the dummy PropertiesEditor control";
         }

         public Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable Content
        {
            get
            {
                return this.m_DockableContent
                       ?? (this.m_DockableContent =
                           new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
                               {
                                   Title = "PropertiesEditor",
                                   Content = this,
                                   ContentId = this.Name
                               });
            }
        }

        public void Activate()
        {
            this.Content.Show();
        }
    }
}
