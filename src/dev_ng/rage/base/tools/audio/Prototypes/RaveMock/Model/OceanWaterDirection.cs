using ModelLib.CustomAttributes;

namespace model {

	public enum OceanWaterDirection {
		[ModelLib.CustomAttributes.Display("North")]
		AUD_OCEAN_DIR_NORTH,
		[ModelLib.CustomAttributes.Display("NorthEast")]
		AUD_OCEAN_DIR_NORTH_EAST,
		[ModelLib.CustomAttributes.Display("East")]
		AUD_OCEAN_DIR_EAST,
		[ModelLib.CustomAttributes.Display("SouthEast")]
		AUD_OCEAN_DIR_SOUTH_EAST,
		[ModelLib.CustomAttributes.Display("South")]
		AUD_OCEAN_DIR_SOUTH,
		[ModelLib.CustomAttributes.Display("SouthWest")]
		AUD_OCEAN_DIR_SOUTH_WEST,
		[ModelLib.CustomAttributes.Display("West")]
		AUD_OCEAN_DIR_WEST,
		[ModelLib.CustomAttributes.Display("NorthWest")]
		AUD_OCEAN_DIR_NORTH_WEST
	}

}