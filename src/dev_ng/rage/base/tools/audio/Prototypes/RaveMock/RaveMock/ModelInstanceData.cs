﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLib;

namespace RaveMock
{
    public class ModelInstanceData
    {
        public string Name { get; private set; }
        public Type InstanceType { get; private set; }
        public ModelBase Instance { get; private set; }
        public string FilePath { get; private set; }
        public string Folder { get; private set; }

        public ModelInstanceData(string name, Type instanceType, ModelBase instance, string filePath, string folder)
        {
            InstanceType = instanceType;
            Instance = instance;
            FilePath = filePath;
            Folder = folder;
            Name = name;
        }
    }
}
