using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Scanner")]
	public class ScannerVoiceParams:  ModelBase {

		public class NoPrefixDefinition: ModelBase {
			private ModelLib.ObjectRef _BlackSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BlackSound {
				get { return _BlackSound; }
				set { SetField(ref _BlackSound, value, () => BlackSound); }
			}

			private ModelLib.ObjectRef _BlueSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BlueSound {
				get { return _BlueSound; }
				set { SetField(ref _BlueSound, value, () => BlueSound); }
			}

			private ModelLib.ObjectRef _BrownSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BrownSound {
				get { return _BrownSound; }
				set { SetField(ref _BrownSound, value, () => BrownSound); }
			}

			private ModelLib.ObjectRef _BeigeSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BeigeSound {
				get { return _BeigeSound; }
				set { SetField(ref _BeigeSound, value, () => BeigeSound); }
			}

			private ModelLib.ObjectRef _GraphiteSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GraphiteSound {
				get { return _GraphiteSound; }
				set { SetField(ref _GraphiteSound, value, () => GraphiteSound); }
			}

			private ModelLib.ObjectRef _GreenSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GreenSound {
				get { return _GreenSound; }
				set { SetField(ref _GreenSound, value, () => GreenSound); }
			}

			private ModelLib.ObjectRef _GreySound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GreySound {
				get { return _GreySound; }
				set { SetField(ref _GreySound, value, () => GreySound); }
			}

			private ModelLib.ObjectRef _OrangeSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef OrangeSound {
				get { return _OrangeSound; }
				set { SetField(ref _OrangeSound, value, () => OrangeSound); }
			}

			private ModelLib.ObjectRef _PinkSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef PinkSound {
				get { return _PinkSound; }
				set { SetField(ref _PinkSound, value, () => PinkSound); }
			}

			private ModelLib.ObjectRef _RedSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef RedSound {
				get { return _RedSound; }
				set { SetField(ref _RedSound, value, () => RedSound); }
			}

			private ModelLib.ObjectRef _SilverSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef SilverSound {
				get { return _SilverSound; }
				set { SetField(ref _SilverSound, value, () => SilverSound); }
			}

			private ModelLib.ObjectRef _WhiteSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef WhiteSound {
				get { return _WhiteSound; }
				set { SetField(ref _WhiteSound, value, () => WhiteSound); }
			}

			private ModelLib.ObjectRef _YellowSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef YellowSound {
				get { return _YellowSound; }
				set { SetField(ref _YellowSound, value, () => YellowSound); }
			}

		}
		private NoPrefixDefinition _NoPrefix;
		public NoPrefixDefinition NoPrefix {
			get { return _NoPrefix; }
			set { SetField(ref _NoPrefix, value, () => NoPrefix); }
		}

		public class BrightDefinition: ModelBase {
			private ModelLib.ObjectRef _BlackSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BlackSound {
				get { return _BlackSound; }
				set { SetField(ref _BlackSound, value, () => BlackSound); }
			}

			private ModelLib.ObjectRef _BlueSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BlueSound {
				get { return _BlueSound; }
				set { SetField(ref _BlueSound, value, () => BlueSound); }
			}

			private ModelLib.ObjectRef _BrownSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BrownSound {
				get { return _BrownSound; }
				set { SetField(ref _BrownSound, value, () => BrownSound); }
			}

			private ModelLib.ObjectRef _BeigeSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BeigeSound {
				get { return _BeigeSound; }
				set { SetField(ref _BeigeSound, value, () => BeigeSound); }
			}

			private ModelLib.ObjectRef _GraphiteSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GraphiteSound {
				get { return _GraphiteSound; }
				set { SetField(ref _GraphiteSound, value, () => GraphiteSound); }
			}

			private ModelLib.ObjectRef _GreenSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GreenSound {
				get { return _GreenSound; }
				set { SetField(ref _GreenSound, value, () => GreenSound); }
			}

			private ModelLib.ObjectRef _GreySound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GreySound {
				get { return _GreySound; }
				set { SetField(ref _GreySound, value, () => GreySound); }
			}

			private ModelLib.ObjectRef _OrangeSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef OrangeSound {
				get { return _OrangeSound; }
				set { SetField(ref _OrangeSound, value, () => OrangeSound); }
			}

			private ModelLib.ObjectRef _PinkSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef PinkSound {
				get { return _PinkSound; }
				set { SetField(ref _PinkSound, value, () => PinkSound); }
			}

			private ModelLib.ObjectRef _RedSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef RedSound {
				get { return _RedSound; }
				set { SetField(ref _RedSound, value, () => RedSound); }
			}

			private ModelLib.ObjectRef _SilverSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef SilverSound {
				get { return _SilverSound; }
				set { SetField(ref _SilverSound, value, () => SilverSound); }
			}

			private ModelLib.ObjectRef _WhiteSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef WhiteSound {
				get { return _WhiteSound; }
				set { SetField(ref _WhiteSound, value, () => WhiteSound); }
			}

			private ModelLib.ObjectRef _YellowSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef YellowSound {
				get { return _YellowSound; }
				set { SetField(ref _YellowSound, value, () => YellowSound); }
			}

		}
		private BrightDefinition _Bright;
		public BrightDefinition Bright {
			get { return _Bright; }
			set { SetField(ref _Bright, value, () => Bright); }
		}

		public class LightDefinition: ModelBase {
			private ModelLib.ObjectRef _BlackSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BlackSound {
				get { return _BlackSound; }
				set { SetField(ref _BlackSound, value, () => BlackSound); }
			}

			private ModelLib.ObjectRef _BlueSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BlueSound {
				get { return _BlueSound; }
				set { SetField(ref _BlueSound, value, () => BlueSound); }
			}

			private ModelLib.ObjectRef _BrownSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BrownSound {
				get { return _BrownSound; }
				set { SetField(ref _BrownSound, value, () => BrownSound); }
			}

			private ModelLib.ObjectRef _BeigeSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BeigeSound {
				get { return _BeigeSound; }
				set { SetField(ref _BeigeSound, value, () => BeigeSound); }
			}

			private ModelLib.ObjectRef _GraphiteSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GraphiteSound {
				get { return _GraphiteSound; }
				set { SetField(ref _GraphiteSound, value, () => GraphiteSound); }
			}

			private ModelLib.ObjectRef _GreenSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GreenSound {
				get { return _GreenSound; }
				set { SetField(ref _GreenSound, value, () => GreenSound); }
			}

			private ModelLib.ObjectRef _GreySound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GreySound {
				get { return _GreySound; }
				set { SetField(ref _GreySound, value, () => GreySound); }
			}

			private ModelLib.ObjectRef _OrangeSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef OrangeSound {
				get { return _OrangeSound; }
				set { SetField(ref _OrangeSound, value, () => OrangeSound); }
			}

			private ModelLib.ObjectRef _PinkSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef PinkSound {
				get { return _PinkSound; }
				set { SetField(ref _PinkSound, value, () => PinkSound); }
			}

			private ModelLib.ObjectRef _RedSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef RedSound {
				get { return _RedSound; }
				set { SetField(ref _RedSound, value, () => RedSound); }
			}

			private ModelLib.ObjectRef _SilverSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef SilverSound {
				get { return _SilverSound; }
				set { SetField(ref _SilverSound, value, () => SilverSound); }
			}

			private ModelLib.ObjectRef _WhiteSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef WhiteSound {
				get { return _WhiteSound; }
				set { SetField(ref _WhiteSound, value, () => WhiteSound); }
			}

			private ModelLib.ObjectRef _YellowSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef YellowSound {
				get { return _YellowSound; }
				set { SetField(ref _YellowSound, value, () => YellowSound); }
			}

		}
		private LightDefinition _Light;
		public LightDefinition Light {
			get { return _Light; }
			set { SetField(ref _Light, value, () => Light); }
		}

		public class DarkDefinition: ModelBase {
			private ModelLib.ObjectRef _BlackSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BlackSound {
				get { return _BlackSound; }
				set { SetField(ref _BlackSound, value, () => BlackSound); }
			}

			private ModelLib.ObjectRef _BlueSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BlueSound {
				get { return _BlueSound; }
				set { SetField(ref _BlueSound, value, () => BlueSound); }
			}

			private ModelLib.ObjectRef _BrownSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BrownSound {
				get { return _BrownSound; }
				set { SetField(ref _BrownSound, value, () => BrownSound); }
			}

			private ModelLib.ObjectRef _BeigeSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BeigeSound {
				get { return _BeigeSound; }
				set { SetField(ref _BeigeSound, value, () => BeigeSound); }
			}

			private ModelLib.ObjectRef _GraphiteSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GraphiteSound {
				get { return _GraphiteSound; }
				set { SetField(ref _GraphiteSound, value, () => GraphiteSound); }
			}

			private ModelLib.ObjectRef _GreenSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GreenSound {
				get { return _GreenSound; }
				set { SetField(ref _GreenSound, value, () => GreenSound); }
			}

			private ModelLib.ObjectRef _GreySound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GreySound {
				get { return _GreySound; }
				set { SetField(ref _GreySound, value, () => GreySound); }
			}

			private ModelLib.ObjectRef _OrangeSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef OrangeSound {
				get { return _OrangeSound; }
				set { SetField(ref _OrangeSound, value, () => OrangeSound); }
			}

			private ModelLib.ObjectRef _PinkSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef PinkSound {
				get { return _PinkSound; }
				set { SetField(ref _PinkSound, value, () => PinkSound); }
			}

			private ModelLib.ObjectRef _RedSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef RedSound {
				get { return _RedSound; }
				set { SetField(ref _RedSound, value, () => RedSound); }
			}

			private ModelLib.ObjectRef _SilverSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef SilverSound {
				get { return _SilverSound; }
				set { SetField(ref _SilverSound, value, () => SilverSound); }
			}

			private ModelLib.ObjectRef _WhiteSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef WhiteSound {
				get { return _WhiteSound; }
				set { SetField(ref _WhiteSound, value, () => WhiteSound); }
			}

			private ModelLib.ObjectRef _YellowSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef YellowSound {
				get { return _YellowSound; }
				set { SetField(ref _YellowSound, value, () => YellowSound); }
			}

		}
		private DarkDefinition _Dark;
		public DarkDefinition Dark {
			get { return _Dark; }
			set { SetField(ref _Dark, value, () => Dark); }
		}

		public class ExtraPrefixDefinition: ModelBase {
			private ModelLib.ObjectRef _Battered;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Battered {
				get { return _Battered; }
				set { SetField(ref _Battered, value, () => Battered); }
			}

			private ModelLib.ObjectRef _BeatUp;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef BeatUp {
				get { return _BeatUp; }
				set { SetField(ref _BeatUp, value, () => BeatUp); }
			}

			private ModelLib.ObjectRef _Chopped;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Chopped {
				get { return _Chopped; }
				set { SetField(ref _Chopped, value, () => Chopped); }
			}

			private ModelLib.ObjectRef _Custom;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Custom {
				get { return _Custom; }
				set { SetField(ref _Custom, value, () => Custom); }
			}

			private ModelLib.ObjectRef _Customized;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Customized {
				get { return _Customized; }
				set { SetField(ref _Customized, value, () => Customized); }
			}

			private ModelLib.ObjectRef _Damaged;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Damaged {
				get { return _Damaged; }
				set { SetField(ref _Damaged, value, () => Damaged); }
			}

			private ModelLib.ObjectRef _Dented;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Dented {
				get { return _Dented; }
				set { SetField(ref _Dented, value, () => Dented); }
			}

			private ModelLib.ObjectRef _Dirty;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Dirty {
				get { return _Dirty; }
				set { SetField(ref _Dirty, value, () => Dirty); }
			}

			private ModelLib.ObjectRef _Distressed;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Distressed {
				get { return _Distressed; }
				set { SetField(ref _Distressed, value, () => Distressed); }
			}

			private ModelLib.ObjectRef _Mint;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Mint {
				get { return _Mint; }
				set { SetField(ref _Mint, value, () => Mint); }
			}

			private ModelLib.ObjectRef _Modified;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Modified {
				get { return _Modified; }
				set { SetField(ref _Modified, value, () => Modified); }
			}

			private ModelLib.ObjectRef _RunDown1;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef RunDown1 {
				get { return _RunDown1; }
				set { SetField(ref _RunDown1, value, () => RunDown1); }
			}

			private ModelLib.ObjectRef _RunDown2;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef RunDown2 {
				get { return _RunDown2; }
				set { SetField(ref _RunDown2, value, () => RunDown2); }
			}

			private ModelLib.ObjectRef _Rusty;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Rusty {
				get { return _Rusty; }
				set { SetField(ref _Rusty, value, () => Rusty); }
			}

		}
		private ExtraPrefixDefinition _ExtraPrefix;
		public ExtraPrefixDefinition ExtraPrefix {
			get { return _ExtraPrefix; }
			set { SetField(ref _ExtraPrefix, value, () => ExtraPrefix); }
		}

	}
}


