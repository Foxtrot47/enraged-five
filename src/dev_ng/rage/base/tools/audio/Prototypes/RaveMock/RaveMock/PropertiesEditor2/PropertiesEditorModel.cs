﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ModelLib;
using WPFToolLib.Extensions;

namespace RaveMock
{
    public class PropertiesEditorModel : INotifyPropertyChanged
    {
        public PropertiesEditorModel()
        {
            propertyEntries = new ItemsObservableCollection<PropertyModel>();
            propertyEntries.CollectionChanged += propertyEntries_CollectionChanged;
        }

        void propertyEntries_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if(!isLoading)Console.Out.WriteLine("---------------- collection changed fired");
        }

        private ItemsObservableCollection<PropertyModel> propertyEntries;
        public ItemsObservableCollection<PropertyModel> Items
        {
            get
            {
                return this.propertyEntries;
            }
        }

        private bool m_canAddPropertyEntry;
        public bool CanAddPropertyEntry
        {
            get { return this.m_canAddPropertyEntry; }

            private set
            {
                if (this.m_canAddPropertyEntry != value)
                {
                    this.m_canAddPropertyEntry = value;
                    this.PropertyChanged.Raise(this, "CanAddPropertyEntry");
                }
            }
        }

        private bool m_isCheckedOut;
        public bool IsCheckedOut
        {
            get
            {
                return this.m_isCheckedOut;
            }

            set
            {
                if (this.m_isCheckedOut != value)
                {
                    this.m_isCheckedOut = value;
                    this.PropertyChanged.Raise(this, "IsCheckedOut");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private bool isLoading = false;
        public void EditObject()
        {
            isLoading = true;

            PropertyModel model = new PropertyModel();
            model.Name = "name1";
            model.Value = "value1";
            this.Items.Add(model);

            PropertyModel model2 = new PropertyModel();
            model2.Name = "name 2";
            model2.Value = "value 2";
            this.Items.Add(model2);

            this.CanAddPropertyEntry = true;
            this.IsCheckedOut = true;

            isLoading = false;
        }
    }
}
