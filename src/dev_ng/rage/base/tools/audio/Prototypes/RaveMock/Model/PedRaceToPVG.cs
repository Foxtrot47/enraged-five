using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class PedRaceToPVG:  AudBaseObject{

		private ModelLib.ObjectRef _Universal;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Universal {
			get { return _Universal; }
			set { SetField(ref _Universal, value, () => Universal); }
		}

		private ModelLib.ObjectRef _White;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef White {
			get { return _White; }
			set { SetField(ref _White, value, () => White); }
		}

		private ModelLib.ObjectRef _Black;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Black {
			get { return _Black; }
			set { SetField(ref _Black, value, () => Black); }
		}

		private ModelLib.ObjectRef _Chinese;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Chinese {
			get { return _Chinese; }
			set { SetField(ref _Chinese, value, () => Chinese); }
		}

		private ModelLib.ObjectRef _Latino;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Latino {
			get { return _Latino; }
			set { SetField(ref _Latino, value, () => Latino); }
		}

		private ModelLib.ObjectRef _Arab;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Arab {
			get { return _Arab; }
			set { SetField(ref _Arab, value, () => Arab); }
		}

		private ModelLib.ObjectRef _Balkan;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Balkan {
			get { return _Balkan; }
			set { SetField(ref _Balkan, value, () => Balkan); }
		}

		private ModelLib.ObjectRef _Jamaican;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Jamaican {
			get { return _Jamaican; }
			set { SetField(ref _Jamaican, value, () => Jamaican); }
		}

		private ModelLib.ObjectRef _Korean;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Korean {
			get { return _Korean; }
			set { SetField(ref _Korean, value, () => Korean); }
		}

		private ModelLib.ObjectRef _Italian;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Italian {
			get { return _Italian; }
			set { SetField(ref _Italian, value, () => Italian); }
		}

		private ModelLib.ObjectRef _Pakistani;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Pakistani {
			get { return _Pakistani; }
			set { SetField(ref _Pakistani, value, () => Pakistani); }
		}

		public class FriendPVGsDefinition: ModelBase {
			private ModelLib.ObjectRef _PVG;
			[ModelLib.CustomAttributes.AllowedType(typeof(FriendGroup))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef PVG {
				get { return _PVG; }
				set { SetField(ref _PVG, value, () => PVG); }
			}

		}
		private ItemsObservableCollection<FriendPVGsDefinition> _FriendPVGs = new ItemsObservableCollection<FriendPVGsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("FriendPVGs")]
		public ItemsObservableCollection<FriendPVGsDefinition> FriendPVGs {
			get { return _FriendPVGs; }
			set { SetField(ref _FriendPVGs, value, () => FriendPVGs); }
		}

	}
}


