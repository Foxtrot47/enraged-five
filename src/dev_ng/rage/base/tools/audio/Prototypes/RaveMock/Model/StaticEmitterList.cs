using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class StaticEmitterList:  ModelBase {

		public class EmitterDefinition: ModelBase {
			private ModelLib.ObjectRef _StaticEmitter;
			[ModelLib.CustomAttributes.AllowedType(typeof(StaticEmitter))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef StaticEmitter {
				get { return _StaticEmitter; }
				set { SetField(ref _StaticEmitter, value, () => StaticEmitter); }
			}

		}
		private ItemsObservableCollection<EmitterDefinition> _Emitter = new ItemsObservableCollection<EmitterDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1024)]
		[XmlElement("Emitter")]
		public ItemsObservableCollection<EmitterDefinition> Emitter {
			get { return _Emitter; }
			set { SetField(ref _Emitter, value, () => Emitter); }
		}

	}
}


