using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Radio")]
	public class RadioStationSettings:  ModelBase {

		private System.Int32 _Order;
		[ModelLib.CustomAttributes.Default("1000")]
		public System.Int32 Order {
			get { return _Order; }
			set { SetField(ref _Order, value, () => Order); }
		}

		private System.UInt32 _NextStationSettingsPtr;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 NextStationSettingsPtr {
			get { return _NextStationSettingsPtr; }
			set { SetField(ref _NextStationSettingsPtr, value, () => NextStationSettingsPtr); }
		}

		private RadioGenre _Genre;
		[ModelLib.CustomAttributes.Default("RADIO_GENRE_UNSPECIFIED")]
		public RadioGenre Genre {
			get { return _Genre; }
			set { SetField(ref _Genre, value, () => Genre); }
		}

		private System.SByte _AmbientRadioVol;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(6)]
		[ModelLib.CustomAttributes.Min(-24)]
		public System.SByte AmbientRadioVol {
			get { return _AmbientRadioVol; }
			set { SetField(ref _AmbientRadioVol, value, () => AmbientRadioVol); }
		}

		private System.String _Name;
		[ModelLib.CustomAttributes.Length(32)]
		public System.String Name {
			get { return _Name; }
			set { SetField(ref _Name, value, () => Name); }
		}

		private ModelLib.Types.TriState _NoBack2BackMusic;
		public ModelLib.Types.TriState NoBack2BackMusic {
			get { return _NoBack2BackMusic; }
			set { SetField(ref _NoBack2BackMusic, value, () => NoBack2BackMusic); }
		}

		private ModelLib.Types.TriState _Back2BackAds;
		public ModelLib.Types.TriState Back2BackAds {
			get { return _Back2BackAds; }
			set { SetField(ref _Back2BackAds, value, () => Back2BackAds); }
		}

		private ModelLib.Types.TriState _PlayWeather;
		public ModelLib.Types.TriState PlayWeather {
			get { return _PlayWeather; }
			set { SetField(ref _PlayWeather, value, () => PlayWeather); }
		}

		private ModelLib.Types.TriState _PlayNews;
		public ModelLib.Types.TriState PlayNews {
			get { return _PlayNews; }
			set { SetField(ref _PlayNews, value, () => PlayNews); }
		}

		private ModelLib.Types.TriState _SequentialMusic;
		public ModelLib.Types.TriState SequentialMusic {
			get { return _SequentialMusic; }
			set { SetField(ref _SequentialMusic, value, () => SequentialMusic); }
		}

		private ModelLib.Types.TriState _IdentsInsteadOfAds;
		public ModelLib.Types.TriState IdentsInsteadOfAds {
			get { return _IdentsInsteadOfAds; }
			set { SetField(ref _IdentsInsteadOfAds, value, () => IdentsInsteadOfAds); }
		}

		private ModelLib.Types.TriState _Locked;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState Locked {
			get { return _Locked; }
			set { SetField(ref _Locked, value, () => Locked); }
		}

		private ModelLib.Types.TriState _Hidden;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState Hidden {
			get { return _Hidden; }
			set { SetField(ref _Hidden, value, () => Hidden); }
		}

		public class TrackListDefinition: ModelBase {
			private ModelLib.ObjectRef _TrackListRef;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef TrackListRef {
				get { return _TrackListRef; }
				set { SetField(ref _TrackListRef, value, () => TrackListRef); }
			}

		}
		private ItemsObservableCollection<TrackListDefinition> _TrackList = new ItemsObservableCollection<TrackListDefinition>();
		[ModelLib.CustomAttributes.MaxSize(30, typeof(System.UInt32))]
		[XmlElement("TrackList")]
		public ItemsObservableCollection<TrackListDefinition> TrackList {
			get { return _TrackList; }
			set { SetField(ref _TrackList, value, () => TrackList); }
		}

	}
}


