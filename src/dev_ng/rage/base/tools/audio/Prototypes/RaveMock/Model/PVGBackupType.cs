using ModelLib.CustomAttributes;

namespace model {

	public enum PVGBackupType {
		[ModelLib.CustomAttributes.Display("MaleCowardly")]
		kMaleCowardly,
		[ModelLib.CustomAttributes.Display("MaleBrave")]
		kMaleBrave,
		[ModelLib.CustomAttributes.Display("MaleGang")]
		kMaleGang,
		[ModelLib.CustomAttributes.Display("Female")]
		kFemale,
		[ModelLib.CustomAttributes.Display("Cop")]
		kCop
	}

}