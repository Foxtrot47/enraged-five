using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class RandomisedRadioEmitterSettings:  ModelBase {

		private System.Single _VehicleEmitterBias;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single VehicleEmitterBias {
			get { return _VehicleEmitterBias; }
			set { SetField(ref _VehicleEmitterBias, value, () => VehicleEmitterBias); }
		}

		private ModelLib.Types.TriState _UseOcclusionOnStaticEmitters;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState UseOcclusionOnStaticEmitters {
			get { return _UseOcclusionOnStaticEmitters; }
			set { SetField(ref _UseOcclusionOnStaticEmitters, value, () => UseOcclusionOnStaticEmitters); }
		}

		private ModelLib.Types.TriState _UseOcclusionOnVehicleEmitters;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState UseOcclusionOnVehicleEmitters {
			get { return _UseOcclusionOnVehicleEmitters; }
			set { SetField(ref _UseOcclusionOnVehicleEmitters, value, () => UseOcclusionOnVehicleEmitters); }
		}

		public class StaticEmitterConfigDefinition: ModelBase {
			private ModelLib.ObjectRef _StaticEmitter;
			[ModelLib.CustomAttributes.Default("RANDOMISED_STATIC_RADIO_EMITTER")]
			[ModelLib.CustomAttributes.AllowedType(typeof(StaticEmitter))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef StaticEmitter {
				get { return _StaticEmitter; }
				set { SetField(ref _StaticEmitter, value, () => StaticEmitter); }
			}

			private System.Single _MinTime;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("hours")]
			public System.Single MinTime {
				get { return _MinTime; }
				set { SetField(ref _MinTime, value, () => MinTime); }
			}

			private System.Single _MaxTime;
			[ModelLib.CustomAttributes.Default("24")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("hours")]
			public System.Single MaxTime {
				get { return _MaxTime; }
				set { SetField(ref _MaxTime, value, () => MaxTime); }
			}

			private System.Single _MinFadeRadius;
			[ModelLib.CustomAttributes.Default("25")]
			[ModelLib.CustomAttributes.Max(10000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single MinFadeRadius {
				get { return _MinFadeRadius; }
				set { SetField(ref _MinFadeRadius, value, () => MinFadeRadius); }
			}

			private System.Single _MaxFadeRadius;
			[ModelLib.CustomAttributes.Default("25")]
			[ModelLib.CustomAttributes.Max(10000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single MaxFadeRadius {
				get { return _MaxFadeRadius; }
				set { SetField(ref _MaxFadeRadius, value, () => MaxFadeRadius); }
			}

			private System.UInt32 _RetriggerTimeMin;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(1000000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 RetriggerTimeMin {
				get { return _RetriggerTimeMin; }
				set { SetField(ref _RetriggerTimeMin, value, () => RetriggerTimeMin); }
			}

			private System.UInt32 _RetriggerTimeMax;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(1000000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 RetriggerTimeMax {
				get { return _RetriggerTimeMax; }
				set { SetField(ref _RetriggerTimeMax, value, () => RetriggerTimeMax); }
			}

		}
		private StaticEmitterConfigDefinition _StaticEmitterConfig;
		public StaticEmitterConfigDefinition StaticEmitterConfig {
			get { return _StaticEmitterConfig; }
			set { SetField(ref _StaticEmitterConfig, value, () => StaticEmitterConfig); }
		}

		public class VehicleEmitterConfigDefinition: ModelBase {
			private ModelLib.ObjectRef _StaticEmitter;
			[ModelLib.CustomAttributes.Default("RANDOMISED_VEHICLE_RADIO_EMITTER")]
			[ModelLib.CustomAttributes.AllowedType(typeof(StaticEmitter))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef StaticEmitter {
				get { return _StaticEmitter; }
				set { SetField(ref _StaticEmitter, value, () => StaticEmitter); }
			}

			private System.Single _MinTime;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("hours")]
			public System.Single MinTime {
				get { return _MinTime; }
				set { SetField(ref _MinTime, value, () => MinTime); }
			}

			private System.Single _MaxTime;
			[ModelLib.CustomAttributes.Default("24")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("hours")]
			public System.Single MaxTime {
				get { return _MaxTime; }
				set { SetField(ref _MaxTime, value, () => MaxTime); }
			}

			private System.UInt32 _MinAttackTime;
			[ModelLib.CustomAttributes.Default("2000")]
			[ModelLib.CustomAttributes.Max(10000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 MinAttackTime {
				get { return _MinAttackTime; }
				set { SetField(ref _MinAttackTime, value, () => MinAttackTime); }
			}

			private System.UInt32 _MaxAttackTime;
			[ModelLib.CustomAttributes.Default("2000")]
			[ModelLib.CustomAttributes.Max(10000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 MaxAttackTime {
				get { return _MaxAttackTime; }
				set { SetField(ref _MaxAttackTime, value, () => MaxAttackTime); }
			}

			private System.UInt32 _MinHoldTime;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(10000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 MinHoldTime {
				get { return _MinHoldTime; }
				set { SetField(ref _MinHoldTime, value, () => MinHoldTime); }
			}

			private System.UInt32 _MaxHoldTime;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(10000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 MaxHoldTime {
				get { return _MaxHoldTime; }
				set { SetField(ref _MaxHoldTime, value, () => MaxHoldTime); }
			}

			private System.UInt32 _MinReleaseTime;
			[ModelLib.CustomAttributes.Default("2000")]
			[ModelLib.CustomAttributes.Max(10000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 MinReleaseTime {
				get { return _MinReleaseTime; }
				set { SetField(ref _MinReleaseTime, value, () => MinReleaseTime); }
			}

			private System.UInt32 _MaxReleaseTime;
			[ModelLib.CustomAttributes.Default("2000")]
			[ModelLib.CustomAttributes.Max(10000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 MaxReleaseTime {
				get { return _MaxReleaseTime; }
				set { SetField(ref _MaxReleaseTime, value, () => MaxReleaseTime); }
			}

			private System.Single _MinPanAngleChange;
			[ModelLib.CustomAttributes.Default("20")]
			[ModelLib.CustomAttributes.Max(360)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single MinPanAngleChange {
				get { return _MinPanAngleChange; }
				set { SetField(ref _MinPanAngleChange, value, () => MinPanAngleChange); }
			}

			private System.Single _MaxPanAngleChange;
			[ModelLib.CustomAttributes.Default("20")]
			[ModelLib.CustomAttributes.Max(360)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single MaxPanAngleChange {
				get { return _MaxPanAngleChange; }
				set { SetField(ref _MaxPanAngleChange, value, () => MaxPanAngleChange); }
			}

			private System.UInt32 _RetriggerTimeMin;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(1000000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 RetriggerTimeMin {
				get { return _RetriggerTimeMin; }
				set { SetField(ref _RetriggerTimeMin, value, () => RetriggerTimeMin); }
			}

			private System.UInt32 _RetriggerTimeMax;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(1000000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 RetriggerTimeMax {
				get { return _RetriggerTimeMax; }
				set { SetField(ref _RetriggerTimeMax, value, () => RetriggerTimeMax); }
			}

		}
		private VehicleEmitterConfigDefinition _VehicleEmitterConfig;
		public VehicleEmitterConfigDefinition VehicleEmitterConfig {
			get { return _VehicleEmitterConfig; }
			set { SetField(ref _VehicleEmitterConfig, value, () => VehicleEmitterConfig); }
		}

	}
}


