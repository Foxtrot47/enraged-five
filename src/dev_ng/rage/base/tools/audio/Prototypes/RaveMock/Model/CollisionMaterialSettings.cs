using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Physics")]
	public class CollisionMaterialSettings:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private ModelLib.ObjectRef _HardImpact;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HardImpact {
			get { return _HardImpact; }
			set { SetField(ref _HardImpact, value, () => HardImpact); }
		}

		private ModelLib.ObjectRef _SolidImpact;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SolidImpact {
			get { return _SolidImpact; }
			set { SetField(ref _SolidImpact, value, () => SolidImpact); }
		}

		private ModelLib.ObjectRef _SoftImpact;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SoftImpact {
			get { return _SoftImpact; }
			set { SetField(ref _SoftImpact, value, () => SoftImpact); }
		}

		private ModelLib.Types.TriState _IsSoftForLightProps;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("should the material only have soft sound for light and very light objects")]
		public ModelLib.Types.TriState IsSoftForLightProps {
			get { return _IsSoftForLightProps; }
			set { SetField(ref _IsSoftForLightProps, value, () => IsSoftForLightProps); }
		}

		private ModelLib.ObjectRef _ScrapeSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScrapeSound {
			get { return _ScrapeSound; }
			set { SetField(ref _ScrapeSound, value, () => ScrapeSound); }
		}

		private ModelLib.ObjectRef _PedScrapeSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedScrapeSound {
			get { return _PedScrapeSound; }
			set { SetField(ref _PedScrapeSound, value, () => PedScrapeSound); }
		}

		private ModelLib.ObjectRef _BreakSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BreakSound {
			get { return _BreakSound; }
			set { SetField(ref _BreakSound, value, () => BreakSound); }
		}

		private ModelLib.ObjectRef _DestroySound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DestroySound {
			get { return _DestroySound; }
			set { SetField(ref _DestroySound, value, () => DestroySound); }
		}

		private ModelLib.ObjectRef _SettleSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SettleSound {
			get { return _SettleSound; }
			set { SetField(ref _SettleSound, value, () => SettleSound); }
		}

		private ModelLib.ObjectRef _BulletImpactSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BulletImpactSound {
			get { return _BulletImpactSound; }
			set { SetField(ref _BulletImpactSound, value, () => BulletImpactSound); }
		}

		private ModelLib.ObjectRef _AutomaticBulletImpactSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AutomaticBulletImpactSound {
			get { return _AutomaticBulletImpactSound; }
			set { SetField(ref _AutomaticBulletImpactSound, value, () => AutomaticBulletImpactSound); }
		}

		private ModelLib.ObjectRef _ShotgunBulletImpactSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ShotgunBulletImpactSound {
			get { return _ShotgunBulletImpactSound; }
			set { SetField(ref _ShotgunBulletImpactSound, value, () => ShotgunBulletImpactSound); }
		}

		private ModelLib.ObjectRef _BigVehicleImpactSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BigVehicleImpactSound {
			get { return _BigVehicleImpactSound; }
			set { SetField(ref _BigVehicleImpactSound, value, () => BigVehicleImpactSound); }
		}

		private ModelLib.ObjectRef _PedPunch;
		[ModelLib.CustomAttributes.Default("PED_PUNCH_HARD_MESH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedPunch {
			get { return _PedPunch; }
			set { SetField(ref _PedPunch, value, () => PedPunch); }
		}

		private ModelLib.ObjectRef _PedKick;
		[ModelLib.CustomAttributes.Default("PED_KICK_HARD_MESH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedKick {
			get { return _PedKick; }
			set { SetField(ref _PedKick, value, () => PedKick); }
		}

		private ModelLib.ObjectRef _MediumIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("FakeBulletImpactSoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef MediumIntensity {
			get { return _MediumIntensity; }
			set { SetField(ref _MediumIntensity, value, () => MediumIntensity); }
		}

		private ModelLib.ObjectRef _HighIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("FakeBulletImpactSoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HighIntensity {
			get { return _HighIntensity; }
			set { SetField(ref _HighIntensity, value, () => HighIntensity); }
		}

		private System.Byte _Hardness;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("%")]
		public System.Byte Hardness {
			get { return _Hardness; }
			set { SetField(ref _Hardness, value, () => Hardness); }
		}

		private System.Single _MinImpulseMag;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single MinImpulseMag {
			get { return _MinImpulseMag; }
			set { SetField(ref _MinImpulseMag, value, () => MinImpulseMag); }
		}

		private System.Single _MaxImpulseMag;
		[ModelLib.CustomAttributes.Default("6.0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single MaxImpulseMag {
			get { return _MaxImpulseMag; }
			set { SetField(ref _MaxImpulseMag, value, () => MaxImpulseMag); }
		}

		private System.UInt16 _ImpulseMagScalar;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 ImpulseMagScalar {
			get { return _ImpulseMagScalar; }
			set { SetField(ref _ImpulseMagScalar, value, () => ImpulseMagScalar); }
		}

		private System.Single _MaxScrapeSpeed;
		[ModelLib.CustomAttributes.Default("24.0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("m/s")]
		public System.Single MaxScrapeSpeed {
			get { return _MaxScrapeSpeed; }
			set { SetField(ref _MaxScrapeSpeed, value, () => MaxScrapeSpeed); }
		}

		private System.Single _MinScrapeSpeed;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("m/s")]
		public System.Single MinScrapeSpeed {
			get { return _MinScrapeSpeed; }
			set { SetField(ref _MinScrapeSpeed, value, () => MinScrapeSpeed); }
		}

		private System.Single _ScrapeImpactMag;
		[ModelLib.CustomAttributes.Default("3.0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("m/s")]
		public System.Single ScrapeImpactMag {
			get { return _ScrapeImpactMag; }
			set { SetField(ref _ScrapeImpactMag, value, () => ScrapeImpactMag); }
		}

		private System.Single _MaxRollSpeed;
		[ModelLib.CustomAttributes.Default("3.0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("m/s")]
		public System.Single MaxRollSpeed {
			get { return _MaxRollSpeed; }
			set { SetField(ref _MaxRollSpeed, value, () => MaxRollSpeed); }
		}

		private System.Single _MinRollSpeed;
		[ModelLib.CustomAttributes.Default("0.1")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("m/s")]
		public System.Single MinRollSpeed {
			get { return _MinRollSpeed; }
			set { SetField(ref _MinRollSpeed, value, () => MinRollSpeed); }
		}

		private System.Single _RollImpactMag;
		[ModelLib.CustomAttributes.Default("3.0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("m/s")]
		public System.Single RollImpactMag {
			get { return _RollImpactMag; }
			set { SetField(ref _RollImpactMag, value, () => RollImpactMag); }
		}

		private System.Byte _BulletCollisionScaling;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.Max(200)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("%")]
		public System.Byte BulletCollisionScaling {
			get { return _BulletCollisionScaling; }
			set { SetField(ref _BulletCollisionScaling, value, () => BulletCollisionScaling); }
		}

		private ModelLib.ObjectRef _FootstepCustomImpactSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FootstepCustomImpactSound {
			get { return _FootstepCustomImpactSound; }
			set { SetField(ref _FootstepCustomImpactSound, value, () => FootstepCustomImpactSound); }
		}

		private System.Single _FootstepMaterialHardness;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		[ModelLib.CustomAttributes.Unit("%")]
		public System.Single FootstepMaterialHardness {
			get { return _FootstepMaterialHardness; }
			set { SetField(ref _FootstepMaterialHardness, value, () => FootstepMaterialHardness); }
		}

		private System.Single _FootstepMaterialLoudness;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		[ModelLib.CustomAttributes.Unit("%")]
		public System.Single FootstepMaterialLoudness {
			get { return _FootstepMaterialLoudness; }
			set { SetField(ref _FootstepMaterialLoudness, value, () => FootstepMaterialLoudness); }
		}

		private ModelLib.ObjectRef _FootstepSettings;
		[ModelLib.CustomAttributes.Default("FOOTSTEPS_GENERIC")]
		[ModelLib.CustomAttributes.AllowedType(typeof(FootstepSurfaceSounds))]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		[ModelLib.CustomAttributes.Hidden]
		public ModelLib.ObjectRef FootstepSettings {
			get { return _FootstepSettings; }
			set { SetField(ref _FootstepSettings, value, () => FootstepSettings); }
		}

		private System.Byte _FootstepScaling;
		[ModelLib.CustomAttributes.Default("50")]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("%")]
		public System.Byte FootstepScaling {
			get { return _FootstepScaling; }
			set { SetField(ref _FootstepScaling, value, () => FootstepScaling); }
		}

		private System.Byte _ScuffstepScaling;
		[ModelLib.CustomAttributes.Default("20")]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("%")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Byte ScuffstepScaling {
			get { return _ScuffstepScaling; }
			set { SetField(ref _ScuffstepScaling, value, () => ScuffstepScaling); }
		}

		private System.Byte _FootstepImpactScaling;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("%")]
		public System.Byte FootstepImpactScaling {
			get { return _FootstepImpactScaling; }
			set { SetField(ref _FootstepImpactScaling, value, () => FootstepImpactScaling); }
		}

		private System.Byte _ScuffImpactScaling;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("%")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Byte ScuffImpactScaling {
			get { return _ScuffImpactScaling; }
			set { SetField(ref _ScuffImpactScaling, value, () => ScuffImpactScaling); }
		}

		private ModelLib.ObjectRef _SkiSettings;
		[ModelLib.CustomAttributes.Default("SKIS_GENERIC")]
		[ModelLib.CustomAttributes.AllowedType(typeof(SkiAudioSettings))]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		[ModelLib.CustomAttributes.Hidden]
		public ModelLib.ObjectRef SkiSettings {
			get { return _SkiSettings; }
			set { SetField(ref _SkiSettings, value, () => SkiSettings); }
		}

		private ModelLib.ObjectRef _AnimalReference;
		[ModelLib.CustomAttributes.Default("ANIMAL_GENERIC")]
		[ModelLib.CustomAttributes.AllowedType(typeof(AnimalFootstepReference))]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef AnimalReference {
			get { return _AnimalReference; }
			set { SetField(ref _AnimalReference, value, () => AnimalReference); }
		}

		private ModelLib.Types.TriState _DryMaterial;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		public ModelLib.Types.TriState DryMaterial {
			get { return _DryMaterial; }
			set { SetField(ref _DryMaterial, value, () => DryMaterial); }
		}

		private ModelLib.ObjectRef _WetMaterialReference;
		[ModelLib.CustomAttributes.AllowedType(typeof(CollisionMaterialSettings))]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef WetMaterialReference {
			get { return _WetMaterialReference; }
			set { SetField(ref _WetMaterialReference, value, () => WetMaterialReference); }
		}

		private ModelLib.Types.TriState _ShouldPlayRicochet;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("if true the material will play a ricochet for acute angles of incidence")]
		public ModelLib.Types.TriState ShouldPlayRicochet {
			get { return _ShouldPlayRicochet; }
			set { SetField(ref _ShouldPlayRicochet, value, () => ShouldPlayRicochet); }
		}

		private ModelLib.Types.TriState _IsResonant;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("if true the material's impact sound will be treated specially as it contains a resonance loop")]
		public ModelLib.Types.TriState IsResonant {
			get { return _IsResonant; }
			set { SetField(ref _IsResonant, value, () => IsResonant); }
		}

		private ModelLib.Types.TriState _TreatAsFixed;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("Treat as fixed when computing impulse magnitude")]
		public ModelLib.Types.TriState TreatAsFixed {
			get { return _TreatAsFixed; }
			set { SetField(ref _TreatAsFixed, value, () => TreatAsFixed); }
		}

		private ModelLib.Types.TriState _LooseSurface;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("Only this material's scrape sound plays when scraping")]
		public ModelLib.Types.TriState LooseSurface {
			get { return _LooseSurface; }
			set { SetField(ref _LooseSurface, value, () => LooseSurface); }
		}

		private ModelLib.Types.TriState _ScrapesforPeds;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("Allows this material to make scrape sounds with peds even if it normally wouldn't")]
		public ModelLib.Types.TriState ScrapesforPeds {
			get { return _ScrapesforPeds; }
			set { SetField(ref _ScrapesforPeds, value, () => ScrapesforPeds); }
		}

		private ModelLib.Types.TriState _PedsAreSolid;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("Ped collisions count as solid")]
		public ModelLib.Types.TriState PedsAreSolid {
			get { return _PedsAreSolid; }
			set { SetField(ref _PedsAreSolid, value, () => PedsAreSolid); }
		}

		private ModelLib.Types.TriState _UseCustomCurves;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("Uses gameobject collision curves")]
		public ModelLib.Types.TriState UseCustomCurves {
			get { return _UseCustomCurves; }
			set { SetField(ref _UseCustomCurves, value, () => UseCustomCurves); }
		}

		private ModelLib.ObjectRef _ImpactStartOffsetCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_IMPACT_START_OFFSET")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ImpactStartOffsetCurve {
			get { return _ImpactStartOffsetCurve; }
			set { SetField(ref _ImpactStartOffsetCurve, value, () => ImpactStartOffsetCurve); }
		}

		private ModelLib.ObjectRef _ImpactVolCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_IMPACT_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ImpactVolCurve {
			get { return _ImpactVolCurve; }
			set { SetField(ref _ImpactVolCurve, value, () => ImpactVolCurve); }
		}

		private ModelLib.ObjectRef _ScrapePitchCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_SCRAPE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScrapePitchCurve {
			get { return _ScrapePitchCurve; }
			set { SetField(ref _ScrapePitchCurve, value, () => ScrapePitchCurve); }
		}

		private ModelLib.ObjectRef _ScrapeVolCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_SCRAPE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScrapeVolCurve {
			get { return _ScrapeVolCurve; }
			set { SetField(ref _ScrapeVolCurve, value, () => ScrapeVolCurve); }
		}

		private ModelLib.ObjectRef _FastTyreRoll;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FastTyreRoll {
			get { return _FastTyreRoll; }
			set { SetField(ref _FastTyreRoll, value, () => FastTyreRoll); }
		}

		private ModelLib.ObjectRef _DetailTyreRoll;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DetailTyreRoll {
			get { return _DetailTyreRoll; }
			set { SetField(ref _DetailTyreRoll, value, () => DetailTyreRoll); }
		}

		private ModelLib.ObjectRef _MainSkid;
		[ModelLib.CustomAttributes.Default("VEHICLES_WHEEL_LOOPS_MAIN_TARMAC_SKID")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef MainSkid {
			get { return _MainSkid; }
			set { SetField(ref _MainSkid, value, () => MainSkid); }
		}

		private ModelLib.ObjectRef _SideSkid;
		[ModelLib.CustomAttributes.Default("VEHICLES_WHEEL_LOOPS_SIDE_TARMAC_SKID")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SideSkid {
			get { return _SideSkid; }
			set { SetField(ref _SideSkid, value, () => SideSkid); }
		}

		private ModelLib.ObjectRef _MetalShellCasingSmall;
		[ModelLib.CustomAttributes.Default("SHELL_CASINGS_METAL_BOUNCE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef MetalShellCasingSmall {
			get { return _MetalShellCasingSmall; }
			set { SetField(ref _MetalShellCasingSmall, value, () => MetalShellCasingSmall); }
		}

		private ModelLib.ObjectRef _MetalShellCasingMedium;
		[ModelLib.CustomAttributes.Default("SHELL_CASINGS_METAL_BOUNCE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef MetalShellCasingMedium {
			get { return _MetalShellCasingMedium; }
			set { SetField(ref _MetalShellCasingMedium, value, () => MetalShellCasingMedium); }
		}

		private ModelLib.ObjectRef _MetalShellCasingLarge;
		[ModelLib.CustomAttributes.Default("SHELL_CASINGS_METAL_BOUNCE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef MetalShellCasingLarge {
			get { return _MetalShellCasingLarge; }
			set { SetField(ref _MetalShellCasingLarge, value, () => MetalShellCasingLarge); }
		}

		private ModelLib.ObjectRef _MetalShellCasingSmallSlow;
		[ModelLib.CustomAttributes.Default("SHELL_CASINGS_METAL_SLOWMO")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef MetalShellCasingSmallSlow {
			get { return _MetalShellCasingSmallSlow; }
			set { SetField(ref _MetalShellCasingSmallSlow, value, () => MetalShellCasingSmallSlow); }
		}

		private ModelLib.ObjectRef _MetalShellCasingMediumSlow;
		[ModelLib.CustomAttributes.Default("SHELL_CASINGS_METAL_SLOWMO")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef MetalShellCasingMediumSlow {
			get { return _MetalShellCasingMediumSlow; }
			set { SetField(ref _MetalShellCasingMediumSlow, value, () => MetalShellCasingMediumSlow); }
		}

		private ModelLib.ObjectRef _MetalShellCasingLargeSlow;
		[ModelLib.CustomAttributes.Default("SHELL_CASINGS_METAL_SLOWMO")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef MetalShellCasingLargeSlow {
			get { return _MetalShellCasingLargeSlow; }
			set { SetField(ref _MetalShellCasingLargeSlow, value, () => MetalShellCasingLargeSlow); }
		}

		private ModelLib.ObjectRef _PlasticShellCasing;
		[ModelLib.CustomAttributes.Default("SHELL_CASINGS_PLASTIC_BOUNCE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PlasticShellCasing {
			get { return _PlasticShellCasing; }
			set { SetField(ref _PlasticShellCasing, value, () => PlasticShellCasing); }
		}

		private ModelLib.ObjectRef _PlasticShellCasingSlow;
		[ModelLib.CustomAttributes.Default("SHELL_CASINGS_PLASTIC_BOUNCE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PlasticShellCasingSlow {
			get { return _PlasticShellCasingSlow; }
			set { SetField(ref _PlasticShellCasingSlow, value, () => PlasticShellCasingSlow); }
		}

		private ModelLib.Types.TriState _ShellCasingsIgnoreHardness;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("Shell casing volume will ignore material hardness")]
		public ModelLib.Types.TriState ShellCasingsIgnoreHardness {
			get { return _ShellCasingsIgnoreHardness; }
			set { SetField(ref _ShellCasingsIgnoreHardness, value, () => ShellCasingsIgnoreHardness); }
		}

		private ModelLib.ObjectRef _RollSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RollSound {
			get { return _RollSound; }
			set { SetField(ref _RollSound, value, () => RollSound); }
		}

		private ModelLib.ObjectRef _RainLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RainLoop {
			get { return _RainLoop; }
			set { SetField(ref _RainLoop, value, () => RainLoop); }
		}

		private ModelLib.ObjectRef _TyreBump;
		[ModelLib.CustomAttributes.Default("TYRE_BUMPS_CONCRETE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TyreBump {
			get { return _TyreBump; }
			set { SetField(ref _TyreBump, value, () => TyreBump); }
		}

		private ModelLib.ObjectRef _ShockwaveSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ShockwaveSound {
			get { return _ShockwaveSound; }
			set { SetField(ref _ShockwaveSound, value, () => ShockwaveSound); }
		}

		private ModelLib.ObjectRef _RandomAmbient;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RandomAmbient {
			get { return _RandomAmbient; }
			set { SetField(ref _RandomAmbient, value, () => RandomAmbient); }
		}

		private ModelLib.ObjectRef _ClimbSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(ClimbingAudioSettings))]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ClimbSettings {
			get { return _ClimbSettings; }
			set { SetField(ref _ClimbSettings, value, () => ClimbSettings); }
		}

		private System.Single _Dirtiness;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		[ModelLib.CustomAttributes.Unit("%")]
		public System.Single Dirtiness {
			get { return _Dirtiness; }
			set { SetField(ref _Dirtiness, value, () => Dirtiness); }
		}

		private ModelLib.ObjectRef _SurfaceSettle;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SurfaceSettle {
			get { return _SurfaceSettle; }
			set { SetField(ref _SurfaceSettle, value, () => SurfaceSettle); }
		}

		private ModelLib.Types.TriState _SurfaceSettleIsLoop;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		public ModelLib.Types.TriState SurfaceSettleIsLoop {
			get { return _SurfaceSettleIsLoop; }
			set { SetField(ref _SurfaceSettleIsLoop, value, () => SurfaceSettleIsLoop); }
		}

		private ModelLib.ObjectRef _RidgedSurfaceLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RidgedSurfaceLoop {
			get { return _RidgedSurfaceLoop; }
			set { SetField(ref _RidgedSurfaceLoop, value, () => RidgedSurfaceLoop); }
		}

		private System.UInt32 _CollisionCount;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("code")]
		public System.UInt32 CollisionCount {
			get { return _CollisionCount; }
			set { SetField(ref _CollisionCount, value, () => CollisionCount); }
		}

		private System.UInt32 _CollisionCountThreshold;
		[ModelLib.CustomAttributes.Default("3")]
		[ModelLib.CustomAttributes.DisplayGroup("code")]
		public System.UInt32 CollisionCountThreshold {
			get { return _CollisionCountThreshold; }
			set { SetField(ref _CollisionCountThreshold, value, () => CollisionCountThreshold); }
		}

		private System.Single _VolumeThreshold;
		[ModelLib.CustomAttributes.Default("-100.0")]
		[ModelLib.CustomAttributes.DisplayGroup("code")]
		public System.Single VolumeThreshold {
			get { return _VolumeThreshold; }
			set { SetField(ref _VolumeThreshold, value, () => VolumeThreshold); }
		}

		private System.Int32 _MaterialID;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Int32 MaterialID {
			get { return _MaterialID; }
			set { SetField(ref _MaterialID, value, () => MaterialID); }
		}

		private ModelLib.ObjectRef _DebrisLaunch;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Explosions")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DebrisLaunch {
			get { return _DebrisLaunch; }
			set { SetField(ref _DebrisLaunch, value, () => DebrisLaunch); }
		}

		private ModelLib.ObjectRef _DebrisLand;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Explosions")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DebrisLand {
			get { return _DebrisLand; }
			set { SetField(ref _DebrisLand, value, () => DebrisLand); }
		}

		private ModelLib.ObjectRef _OffRoadSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef OffRoadSound {
			get { return _OffRoadSound; }
			set { SetField(ref _OffRoadSound, value, () => OffRoadSound); }
		}

		private System.Single _Roughness;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		[ModelLib.CustomAttributes.Unit("%")]
		public System.Single Roughness {
			get { return _Roughness; }
			set { SetField(ref _Roughness, value, () => Roughness); }
		}

		private ModelLib.ObjectRef _DebrisOnSlope;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DebrisOnSlope {
			get { return _DebrisOnSlope; }
			set { SetField(ref _DebrisOnSlope, value, () => DebrisOnSlope); }
		}

		private ModelLib.ObjectRef _BicycleTyreRoll;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BicycleTyreRoll {
			get { return _BicycleTyreRoll; }
			set { SetField(ref _BicycleTyreRoll, value, () => BicycleTyreRoll); }
		}

		private ModelLib.ObjectRef _OffRoadRumbleSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef OffRoadRumbleSound {
			get { return _OffRoadRumbleSound; }
			set { SetField(ref _OffRoadRumbleSound, value, () => OffRoadRumbleSound); }
		}

		private ModelLib.Types.TriState _GeneratePostShootOutDebris;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("when true, the material will play a post shoot-out debris sound")]
		[ModelLib.CustomAttributes.DisplayGroup("Bullets")]
		public ModelLib.Types.TriState GeneratePostShootOutDebris {
			get { return _GeneratePostShootOutDebris; }
			set { SetField(ref _GeneratePostShootOutDebris, value, () => GeneratePostShootOutDebris); }
		}

		private ModelLib.ObjectRef _StealthSweetener;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef StealthSweetener {
			get { return _StealthSweetener; }
			set { SetField(ref _StealthSweetener, value, () => StealthSweetener); }
		}

		private ModelLib.ObjectRef _Scuff;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Scuff {
			get { return _Scuff; }
			set { SetField(ref _Scuff, value, () => Scuff); }
		}

		private MaterialType _MaterialType;
		[ModelLib.CustomAttributes.Default("OTHER")]
		public MaterialType MaterialType {
			get { return _MaterialType; }
			set { SetField(ref _MaterialType, value, () => MaterialType); }
		}

		private SurfacePriority _SurfacePriority;
		[ModelLib.CustomAttributes.Default("PRIORITY_OTHER")]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		public SurfacePriority SurfacePriority {
			get { return _SurfacePriority; }
			set { SetField(ref _SurfacePriority, value, () => SurfacePriority); }
		}

		private ModelLib.ObjectRef _WheelSpinLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WheelSpinLoop {
			get { return _WheelSpinLoop; }
			set { SetField(ref _WheelSpinLoop, value, () => WheelSpinLoop); }
		}

		private ModelLib.ObjectRef _BicycleTyreGritSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BicycleTyreGritSound {
			get { return _BicycleTyreGritSound; }
			set { SetField(ref _BicycleTyreGritSound, value, () => BicycleTyreGritSound); }
		}

		private ModelLib.ObjectRef _PedSlideSound;
		[ModelLib.CustomAttributes.Default("BONNET_SLIDE_DEFAULT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedSlideSound {
			get { return _PedSlideSound; }
			set { SetField(ref _PedSlideSound, value, () => PedSlideSound); }
		}

		private ModelLib.ObjectRef _PedRollSound;
		[ModelLib.CustomAttributes.Default("BONNET_SLIDE_DEFAULT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedRollSound {
			get { return _PedRollSound; }
			set { SetField(ref _PedRollSound, value, () => PedRollSound); }
		}

		private System.Single _TimeInAirToTriggerBigLand;
		[ModelLib.CustomAttributes.Default("0.8")]
		[ModelLib.CustomAttributes.DisplayGroup("Footsteps")]
		public System.Single TimeInAirToTriggerBigLand {
			get { return _TimeInAirToTriggerBigLand; }
			set { SetField(ref _TimeInAirToTriggerBigLand, value, () => TimeInAirToTriggerBigLand); }
		}

		private ModelLib.ObjectRef _MeleeOverideMaterial;
		[ModelLib.CustomAttributes.AllowedType(typeof(CollisionMaterialSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef MeleeOverideMaterial {
			get { return _MeleeOverideMaterial; }
			set { SetField(ref _MeleeOverideMaterial, value, () => MeleeOverideMaterial); }
		}

		private ModelLib.Types.TriState _TriggerClatterOnTouch;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("WheelLoops")]
		public ModelLib.Types.TriState TriggerClatterOnTouch {
			get { return _TriggerClatterOnTouch; }
			set { SetField(ref _TriggerClatterOnTouch, value, () => TriggerClatterOnTouch); }
		}

		public class SoundSetDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundSetRef;
			[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef SoundSetRef {
				get { return _SoundSetRef; }
				set { SetField(ref _SoundSetRef, value, () => SoundSetRef); }
			}

		}
		private SoundSetDefinition _SoundSet;
		public SoundSetDefinition SoundSet {
			get { return _SoundSet; }
			set { SetField(ref _SoundSet, value, () => SoundSet); }
		}

	}
}


