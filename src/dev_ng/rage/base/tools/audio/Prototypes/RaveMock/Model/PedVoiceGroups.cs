using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	[ModelLib.CustomAttributes.Packed(true)]
	public class PedVoiceGroups:  ModelBase {

		private PVGBackupType _PVGType;
		[ModelLib.CustomAttributes.Default("kMaleCowardly")]
		public PVGBackupType PVGType {
			get { return _PVGType; }
			set { SetField(ref _PVGType, value, () => PVGType); }
		}

		private System.Byte _VoicePriority;
		[ModelLib.CustomAttributes.Default("3")]
		[ModelLib.CustomAttributes.Max(10)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Byte VoicePriority {
			get { return _VoicePriority; }
			set { SetField(ref _VoicePriority, value, () => VoicePriority); }
		}

		private ModelLib.ObjectRef _RingtoneSounds;
		[ModelLib.CustomAttributes.Default("RINGTONE_LIST_DEFAULT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundHashList))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RingtoneSounds {
			get { return _RingtoneSounds; }
			set { SetField(ref _RingtoneSounds, value, () => RingtoneSounds); }
		}

		public class PVGBitsDefinition: ModelBase {
			private ModelLib.Types.Bit _Populated;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit Populated {
				get { return _Populated; }
				set { SetField(ref _Populated, value, () => Populated); }
			}

		}
		private PVGBitsDefinition _PVGBits;
		public PVGBitsDefinition PVGBits {
			get { return _PVGBits; }
			set { SetField(ref _PVGBits, value, () => PVGBits); }
		}

		public class PrimaryVoiceDefinition: ModelBase {
			private System.String _VoiceName;
			public System.String VoiceName {
				get { return _VoiceName; }
				set { SetField(ref _VoiceName, value, () => VoiceName); }
			}

			private System.UInt32 _ReferenceCount;
			[ModelLib.CustomAttributes.Default("0")]
			public System.UInt32 ReferenceCount {
				get { return _ReferenceCount; }
				set { SetField(ref _ReferenceCount, value, () => ReferenceCount); }
			}

			private System.UInt32 _RunningTab;
			[ModelLib.CustomAttributes.Default("0")]
			public System.UInt32 RunningTab {
				get { return _RunningTab; }
				set { SetField(ref _RunningTab, value, () => RunningTab); }
			}

		}
		private ItemsObservableCollection<PrimaryVoiceDefinition> _PrimaryVoice = new ItemsObservableCollection<PrimaryVoiceDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16)]
		[XmlElement("PrimaryVoice")]
		public ItemsObservableCollection<PrimaryVoiceDefinition> PrimaryVoice {
			get { return _PrimaryVoice; }
			set { SetField(ref _PrimaryVoice, value, () => PrimaryVoice); }
		}

		public class MiniVoiceDefinition: ModelBase {
			private System.String _VoiceName;
			public System.String VoiceName {
				get { return _VoiceName; }
				set { SetField(ref _VoiceName, value, () => VoiceName); }
			}

			private System.UInt32 _ReferenceCount;
			[ModelLib.CustomAttributes.Default("0")]
			public System.UInt32 ReferenceCount {
				get { return _ReferenceCount; }
				set { SetField(ref _ReferenceCount, value, () => ReferenceCount); }
			}

			private System.UInt32 _RunningTab;
			[ModelLib.CustomAttributes.Default("0")]
			public System.UInt32 RunningTab {
				get { return _RunningTab; }
				set { SetField(ref _RunningTab, value, () => RunningTab); }
			}

		}
		private ItemsObservableCollection<MiniVoiceDefinition> _MiniVoice = new ItemsObservableCollection<MiniVoiceDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16)]
		[XmlElement("MiniVoice")]
		public ItemsObservableCollection<MiniVoiceDefinition> MiniVoice {
			get { return _MiniVoice; }
			set { SetField(ref _MiniVoice, value, () => MiniVoice); }
		}

		public class GangVoiceDefinition: ModelBase {
			private System.String _VoiceName;
			public System.String VoiceName {
				get { return _VoiceName; }
				set { SetField(ref _VoiceName, value, () => VoiceName); }
			}

			private System.UInt32 _ReferenceCount;
			[ModelLib.CustomAttributes.Default("0")]
			public System.UInt32 ReferenceCount {
				get { return _ReferenceCount; }
				set { SetField(ref _ReferenceCount, value, () => ReferenceCount); }
			}

			private System.UInt32 _RunningTab;
			[ModelLib.CustomAttributes.Default("0")]
			public System.UInt32 RunningTab {
				get { return _RunningTab; }
				set { SetField(ref _RunningTab, value, () => RunningTab); }
			}

		}
		private ItemsObservableCollection<GangVoiceDefinition> _GangVoice = new ItemsObservableCollection<GangVoiceDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16)]
		[XmlElement("GangVoice")]
		public ItemsObservableCollection<GangVoiceDefinition> GangVoice {
			get { return _GangVoice; }
			set { SetField(ref _GangVoice, value, () => GangVoice); }
		}

		public class BackupPVGDefinition: ModelBase {
			private ModelLib.ObjectRef _PVG;
			[ModelLib.CustomAttributes.AllowedType(typeof(PedVoiceGroups))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef PVG {
				get { return _PVG; }
				set { SetField(ref _PVG, value, () => PVG); }
			}

		}
		private ItemsObservableCollection<BackupPVGDefinition> _BackupPVG = new ItemsObservableCollection<BackupPVGDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16)]
		[XmlElement("BackupPVG")]
		public ItemsObservableCollection<BackupPVGDefinition> BackupPVG {
			get { return _BackupPVG; }
			set { SetField(ref _BackupPVG, value, () => BackupPVG); }
		}

	}
}


