using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Transformer("{Type=\"rage.CarRecordingTransformer.CarRecordingTransformer, CarRecordingTransformer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\" Args=\"$\\gta5\\audio\\dev\\assets\\Objects\\CORE\\AUDIO\\DYNAMIX\\$\"}")]
	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class CarRecordingAudioSettings:  ModelBase {

		private System.String _Mixgroup;
		[ModelLib.CustomAttributes.Hidden]
		public System.String Mixgroup {
			get { return _Mixgroup; }
			set { SetField(ref _Mixgroup, value, () => Mixgroup); }
		}

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private System.String _VehicleModel;
		[ModelLib.CustomAttributes.Ignore]
		public System.String VehicleModel {
			get { return _VehicleModel; }
			set { SetField(ref _VehicleModel, value, () => VehicleModel); }
		}

		private System.UInt32 _VehicleModelId;
		[ModelLib.CustomAttributes.Default("65535")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 VehicleModelId {
			get { return _VehicleModelId; }
			set { SetField(ref _VehicleModelId, value, () => VehicleModelId); }
		}

		public class EventDefinition: ModelBase {
			private System.Single _Time;
			public System.Single Time {
				get { return _Time; }
				set { SetField(ref _Time, value, () => Time); }
			}

			private ModelLib.ObjectRef _Sound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Sound {
				get { return _Sound; }
				set { SetField(ref _Sound, value, () => Sound); }
			}

			private ModelLib.ObjectRef _OneShotScene;
			[ModelLib.CustomAttributes.AllowedType(typeof(MixerScene))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef OneShotScene {
				get { return _OneShotScene; }
				set { SetField(ref _OneShotScene, value, () => OneShotScene); }
			}

		}
		private ItemsObservableCollection<EventDefinition> _Event = new ItemsObservableCollection<EventDefinition>();
		[ModelLib.CustomAttributes.MaxSize(128)]
		[XmlElement("Event")]
		public ItemsObservableCollection<EventDefinition> Event {
			get { return _Event; }
			set { SetField(ref _Event, value, () => Event); }
		}

		public class PersistentMixerScenesDefinition: ModelBase {
			private ModelLib.ObjectRef _Scene;
			[ModelLib.CustomAttributes.AllowedType(typeof(MixerScene))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Scene {
				get { return _Scene; }
				set { SetField(ref _Scene, value, () => Scene); }
			}

			private System.Single _StartTime;
			public System.Single StartTime {
				get { return _StartTime; }
				set { SetField(ref _StartTime, value, () => StartTime); }
			}

			private System.Single _EndTime;
			public System.Single EndTime {
				get { return _EndTime; }
				set { SetField(ref _EndTime, value, () => EndTime); }
			}

		}
		private ItemsObservableCollection<PersistentMixerScenesDefinition> _PersistentMixerScenes = new ItemsObservableCollection<PersistentMixerScenesDefinition>();
		[ModelLib.CustomAttributes.MaxSize(3)]
		[XmlElement("PersistentMixerScenes")]
		public ItemsObservableCollection<PersistentMixerScenesDefinition> PersistentMixerScenes {
			get { return _PersistentMixerScenes; }
			set { SetField(ref _PersistentMixerScenes, value, () => PersistentMixerScenes); }
		}

	}
}


