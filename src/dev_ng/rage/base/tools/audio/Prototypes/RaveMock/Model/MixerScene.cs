using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class MixerScene:  ModelBase {

		private System.Int32 _ReferenceCount;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Int32 ReferenceCount {
			get { return _ReferenceCount; }
			set { SetField(ref _ReferenceCount, value, () => ReferenceCount); }
		}

		private ModelLib.Types.TriState _PlayerCarDialogueFrontend;
		[ModelLib.CustomAttributes.Default("true")]
		public ModelLib.Types.TriState PlayerCarDialogueFrontend {
			get { return _PlayerCarDialogueFrontend; }
			set { SetField(ref _PlayerCarDialogueFrontend, value, () => PlayerCarDialogueFrontend); }
		}

		private ModelLib.Types.TriState _StealthScene;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState StealthScene {
			get { return _StealthScene; }
			set { SetField(ref _StealthScene, value, () => StealthScene); }
		}

		private ModelLib.Types.TriState _MuteUserMusic;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState MuteUserMusic {
			get { return _MuteUserMusic; }
			set { SetField(ref _MuteUserMusic, value, () => MuteUserMusic); }
		}

		private ModelLib.Types.TriState _OverrideFrontendScene;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState OverrideFrontendScene {
			get { return _OverrideFrontendScene; }
			set { SetField(ref _OverrideFrontendScene, value, () => OverrideFrontendScene); }
		}

		private ModelLib.Types.TriState _CusceneAudioOverruns;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState CusceneAudioOverruns {
			get { return _CusceneAudioOverruns; }
			set { SetField(ref _CusceneAudioOverruns, value, () => CusceneAudioOverruns); }
		}

		private ModelLib.Types.TriState _CutsceneQuickRelease;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState CutsceneQuickRelease {
			get { return _CutsceneQuickRelease; }
			set { SetField(ref _CutsceneQuickRelease, value, () => CutsceneQuickRelease); }
		}

		private ModelLib.Types.TriState _CutsceneTrimmedAsset;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState CutsceneTrimmedAsset {
			get { return _CutsceneTrimmedAsset; }
			set { SetField(ref _CutsceneTrimmedAsset, value, () => CutsceneTrimmedAsset); }
		}

		private ModelLib.Types.TriState _KeepMobileRadioActive;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState KeepMobileRadioActive {
			get { return _KeepMobileRadioActive; }
			set { SetField(ref _KeepMobileRadioActive, value, () => KeepMobileRadioActive); }
		}

		public class PatchGroupDefinition: ModelBase {
			private ModelLib.ObjectRef _Patch;
			[ModelLib.CustomAttributes.AllowedType(typeof(MixerPatch))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Patch {
				get { return _Patch; }
				set { SetField(ref _Patch, value, () => Patch); }
			}

			private ModelLib.ObjectRef _MixGroup;
			[ModelLib.CustomAttributes.AllowedType(typeof(MixGroup))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef MixGroup {
				get { return _MixGroup; }
				set { SetField(ref _MixGroup, value, () => MixGroup); }
			}

		}
		private ItemsObservableCollection<PatchGroupDefinition> _PatchGroup = new ItemsObservableCollection<PatchGroupDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16)]
		[XmlElement("PatchGroup")]
		public ItemsObservableCollection<PatchGroupDefinition> PatchGroup {
			get { return _PatchGroup; }
			set { SetField(ref _PatchGroup, value, () => PatchGroup); }
		}

	}
}


