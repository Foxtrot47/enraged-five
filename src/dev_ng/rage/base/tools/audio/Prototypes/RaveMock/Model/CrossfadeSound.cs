using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class CrossfadeSound:  Sound{

		private ModelLib.ObjectRef _NearSoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef NearSoundRef {
			get { return _NearSoundRef; }
			set { SetField(ref _NearSoundRef, value, () => NearSoundRef); }
		}

		private ModelLib.ObjectRef _FarSoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef FarSoundRef {
			get { return _FarSoundRef; }
			set { SetField(ref _FarSoundRef, value, () => FarSoundRef); }
		}

		private CrossfadeMode _Mode;
		[ModelLib.CustomAttributes.Default("CROSSFADE_MODE_BOTH")]
		public CrossfadeMode Mode {
			get { return _Mode; }
			set { SetField(ref _Mode, value, () => Mode); }
		}

		private System.Single _MinDistance;
		[ModelLib.CustomAttributes.Default("-1.0")]
		public System.Single MinDistance {
			get { return _MinDistance; }
			set { SetField(ref _MinDistance, value, () => MinDistance); }
		}

		private System.Single _MaxDistance;
		[ModelLib.CustomAttributes.Default("-1.0")]
		public System.Single MaxDistance {
			get { return _MaxDistance; }
			set { SetField(ref _MaxDistance, value, () => MaxDistance); }
		}

		private System.Single _Hysteresis;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single Hysteresis {
			get { return _Hysteresis; }
			set { SetField(ref _Hysteresis, value, () => Hysteresis); }
		}

		private ModelLib.ObjectRef _CrossfadeCurve;
		[ModelLib.CustomAttributes.Default("EQUAL_POWER_FALL_CROSSFADE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CrossfadeCurve {
			get { return _CrossfadeCurve; }
			set { SetField(ref _CrossfadeCurve, value, () => CrossfadeCurve); }
		}

		private System.String _DistanceVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String DistanceVariable {
			get { return _DistanceVariable; }
			set { SetField(ref _DistanceVariable, value, () => DistanceVariable); }
		}

		private System.String _MinDistanceVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String MinDistanceVariable {
			get { return _MinDistanceVariable; }
			set { SetField(ref _MinDistanceVariable, value, () => MinDistanceVariable); }
		}

		private System.String _MaxDistanceVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String MaxDistanceVariable {
			get { return _MaxDistanceVariable; }
			set { SetField(ref _MaxDistanceVariable, value, () => MaxDistanceVariable); }
		}

		private System.String _HysteresisVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String HysteresisVariable {
			get { return _HysteresisVariable; }
			set { SetField(ref _HysteresisVariable, value, () => HysteresisVariable); }
		}

		private System.String _CrossfadeVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String CrossfadeVariable {
			get { return _CrossfadeVariable; }
			set { SetField(ref _CrossfadeVariable, value, () => CrossfadeVariable); }
		}

	}
}


