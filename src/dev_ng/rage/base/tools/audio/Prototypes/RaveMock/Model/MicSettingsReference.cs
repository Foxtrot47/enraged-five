using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Environment")]
	public class MicSettingsReference:  ModelBase {

		public class ReferenceDefinition: ModelBase {
			private System.String _CameraType;
			public System.String CameraType {
				get { return _CameraType; }
				set { SetField(ref _CameraType, value, () => CameraType); }
			}

			private ModelLib.ObjectRef _MicSettings;
			[ModelLib.CustomAttributes.AllowedType(typeof(MicrophoneSettings))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef MicSettings {
				get { return _MicSettings; }
				set { SetField(ref _MicSettings, value, () => MicSettings); }
			}

		}
		private ItemsObservableCollection<ReferenceDefinition> _Reference = new ItemsObservableCollection<ReferenceDefinition>();
		[ModelLib.CustomAttributes.MaxSize(64)]
		[XmlElement("Reference")]
		public ItemsObservableCollection<ReferenceDefinition> Reference {
			get { return _Reference; }
			set { SetField(ref _Reference, value, () => Reference); }
		}

	}
}


