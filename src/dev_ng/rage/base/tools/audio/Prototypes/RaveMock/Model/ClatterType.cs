using ModelLib.CustomAttributes;

namespace model {

	public enum ClatterType {
		[ModelLib.CustomAttributes.Display("Truck Cab")]
		AUD_CLATTER_TRUCK_CAB,
		[ModelLib.CustomAttributes.Display("Truck Cages")]
		AUD_CLATTER_TRUCK_CAGES,
		[ModelLib.CustomAttributes.Display("Truck Chains")]
		AUD_CLATTER_TRUCK_TRAILER_CHAINS,
		[ModelLib.CustomAttributes.Display("Taxi")]
		AUD_CLATTER_TAXI,
		[ModelLib.CustomAttributes.Display("Bus")]
		AUD_CLATTER_BUS,
		[ModelLib.CustomAttributes.Display("Transit Van")]
		AUD_CLATTER_TRANSIT,
		[ModelLib.CustomAttributes.Display("Clown Transit Van")]
		AUD_CLATTER_TRANSIT_CLOWN,
		[ModelLib.CustomAttributes.Display("Detailed")]
		AUD_CLATTER_DETAIL,
		[ModelLib.CustomAttributes.Display("None")]
		AUD_CLATTER_NONE
	}

}