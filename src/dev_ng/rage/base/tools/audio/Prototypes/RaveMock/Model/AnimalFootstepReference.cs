using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class AnimalFootstepReference:  ModelBase {

		public class AnimalReferenceDefinition: ModelBase {
			private System.String _Name;
			public System.String Name {
				get { return _Name; }
				set { SetField(ref _Name, value, () => Name); }
			}

			private ModelLib.ObjectRef _AnimalFootstepSettings;
			[ModelLib.CustomAttributes.Default("ANIMALS_GENERIC")]
			[ModelLib.CustomAttributes.AllowedType(typeof(AnimalFootstepSettings))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef AnimalFootstepSettings {
				get { return _AnimalFootstepSettings; }
				set { SetField(ref _AnimalFootstepSettings, value, () => AnimalFootstepSettings); }
			}

		}
		private ItemsObservableCollection<AnimalReferenceDefinition> _AnimalReference = new ItemsObservableCollection<AnimalReferenceDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1000)]
		[XmlElement("AnimalReference")]
		public ItemsObservableCollection<AnimalReferenceDefinition> AnimalReference {
			get { return _AnimalReference; }
			set { SetField(ref _AnimalReference, value, () => AnimalReference); }
		}

	}
}


