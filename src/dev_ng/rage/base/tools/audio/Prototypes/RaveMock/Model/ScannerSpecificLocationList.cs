using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Scanner")]
	public class ScannerSpecificLocationList:  ModelBase {

		public class LocationDefinition: ModelBase {
			private ModelLib.ObjectRef _Ref;
			[ModelLib.CustomAttributes.AllowedType(typeof(ScannerSpecificLocation))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Ref {
				get { return _Ref; }
				set { SetField(ref _Ref, value, () => Ref); }
			}

		}
		private ItemsObservableCollection<LocationDefinition> _Location = new ItemsObservableCollection<LocationDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1000)]
		[XmlElement("Location")]
		public ItemsObservableCollection<LocationDefinition> Location {
			get { return _Location; }
			set { SetField(ref _Location, value, () => Location); }
		}

	}
}


