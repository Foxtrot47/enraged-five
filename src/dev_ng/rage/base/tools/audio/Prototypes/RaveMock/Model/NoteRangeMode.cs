using ModelLib.CustomAttributes;

namespace model {

	public enum NoteRangeMode {
		[ModelLib.CustomAttributes.Display("TrackPitch")]
		NOTE_RANGE_TRACK_PITCH,
		[ModelLib.CustomAttributes.Display("UnityPitch")]
		NOTE_RANGE_UNITY_PITCH
	}

}