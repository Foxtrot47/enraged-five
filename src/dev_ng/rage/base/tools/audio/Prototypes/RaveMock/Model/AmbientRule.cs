using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Transformer("{Type=\"rage.AmbientRuleCompiler.AmbientRuleTransformer, AmbientRuleCompiler, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\"}")]
	[ModelLib.CustomAttributes.Group("Ambience")]
	[ModelLib.CustomAttributes.Packed(false)]
	public class AmbientRule:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private System.String _Notes;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Notes {
			get { return _Notes; }
			set { SetField(ref _Notes, value, () => Notes); }
		}

		private ModelLib.ObjectRef _Sound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Sound {
			get { return _Sound; }
			set { SetField(ref _Sound, value, () => Sound); }
		}

		private System.String _Category;
		[ModelLib.CustomAttributes.Default("BASE")]
		[ModelLib.CustomAttributes.Unit("CategoryRef")]
		public System.String Category {
			get { return _Category; }
			set { SetField(ref _Category, value, () => Category); }
		}

		private System.UInt32 _LastPlayTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 LastPlayTime {
			get { return _LastPlayTime; }
			set { SetField(ref _LastPlayTime, value, () => LastPlayTime); }
		}

		private System.Int32 _DynamicBankID;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Int32 DynamicBankID {
			get { return _DynamicBankID; }
			set { SetField(ref _DynamicBankID, value, () => DynamicBankID); }
		}

		private System.String _DynamicSlotType;
		[ModelLib.CustomAttributes.Hidden]
		public System.String DynamicSlotType {
			get { return _DynamicSlotType; }
			set { SetField(ref _DynamicSlotType, value, () => DynamicSlotType); }
		}

		private System.Single _Weight;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single Weight {
			get { return _Weight; }
			set { SetField(ref _Weight, value, () => Weight); }
		}

		private System.Single _MinDist;
		[ModelLib.CustomAttributes.Default("70.0")]
		[ModelLib.CustomAttributes.Unit("m")]
		public System.Single MinDist {
			get { return _MinDist; }
			set { SetField(ref _MinDist, value, () => MinDist); }
		}

		private System.Single _MaxDist;
		[ModelLib.CustomAttributes.Default("250.0")]
		[ModelLib.CustomAttributes.Unit("m")]
		public System.Single MaxDist {
			get { return _MaxDist; }
			set { SetField(ref _MaxDist, value, () => MaxDist); }
		}

		private System.Single _MinTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Ignore]
		[ModelLib.CustomAttributes.Max(24)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("hours")]
		public System.Single MinTime {
			get { return _MinTime; }
			set { SetField(ref _MinTime, value, () => MinTime); }
		}

		private System.Single _MaxTime;
		[ModelLib.CustomAttributes.Default("24")]
		[ModelLib.CustomAttributes.Ignore]
		[ModelLib.CustomAttributes.Max(24)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("hours")]
		public System.Single MaxTime {
			get { return _MaxTime; }
			set { SetField(ref _MaxTime, value, () => MaxTime); }
		}

		private System.UInt16 _MinTimeMinutes;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt16 MinTimeMinutes {
			get { return _MinTimeMinutes; }
			set { SetField(ref _MinTimeMinutes, value, () => MinTimeMinutes); }
		}

		private System.UInt16 _MaxTimeMinutes;
		[ModelLib.CustomAttributes.Default("1440")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt16 MaxTimeMinutes {
			get { return _MaxTimeMinutes; }
			set { SetField(ref _MaxTimeMinutes, value, () => MaxTimeMinutes); }
		}

		private System.UInt16 _MinRepeatTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(65536)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("s")]
		public System.UInt16 MinRepeatTime {
			get { return _MinRepeatTime; }
			set { SetField(ref _MinRepeatTime, value, () => MinRepeatTime); }
		}

		private System.UInt16 _MinRepeatTimeVariance;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(65536)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("s")]
		public System.UInt16 MinRepeatTimeVariance {
			get { return _MinRepeatTimeVariance; }
			set { SetField(ref _MinRepeatTimeVariance, value, () => MinRepeatTimeVariance); }
		}

		private ModelLib.Types.TriState _IgnoreInitialMinRepeatTime;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState IgnoreInitialMinRepeatTime {
			get { return _IgnoreInitialMinRepeatTime; }
			set { SetField(ref _IgnoreInitialMinRepeatTime, value, () => IgnoreInitialMinRepeatTime); }
		}

		private ModelLib.Types.TriState _StopWhenRaining;
		public ModelLib.Types.TriState StopWhenRaining {
			get { return _StopWhenRaining; }
			set { SetField(ref _StopWhenRaining, value, () => StopWhenRaining); }
		}

		private ModelLib.Types.TriState _StopOnLoudSound;
		public ModelLib.Types.TriState StopOnLoudSound {
			get { return _StopOnLoudSound; }
			set { SetField(ref _StopOnLoudSound, value, () => StopOnLoudSound); }
		}

		private ModelLib.Types.TriState _FollowListener;
		public ModelLib.Types.TriState FollowListener {
			get { return _FollowListener; }
			set { SetField(ref _FollowListener, value, () => FollowListener); }
		}

		private AmbientRuleHeight _SpawnHeight;
		[ModelLib.CustomAttributes.Default("AMBIENCE_HEIGHT_RANDOM")]
		public AmbientRuleHeight SpawnHeight {
			get { return _SpawnHeight; }
			set { SetField(ref _SpawnHeight, value, () => SpawnHeight); }
		}

		private AmbientRuleExplicitPositionType _ExplicitSpawnPositionUsage;
		[ModelLib.CustomAttributes.Default("RULE_EXPLICIT_SPAWN_POS_DISABLED")]
		public AmbientRuleExplicitPositionType ExplicitSpawnPositionUsage {
			get { return _ExplicitSpawnPositionUsage; }
			set { SetField(ref _ExplicitSpawnPositionUsage, value, () => ExplicitSpawnPositionUsage); }
		}

		private System.SByte _MaxLocalInstances;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Max(127)]
		[ModelLib.CustomAttributes.Min(-1)]
		public System.SByte MaxLocalInstances {
			get { return _MaxLocalInstances; }
			set { SetField(ref _MaxLocalInstances, value, () => MaxLocalInstances); }
		}

		private System.SByte _MaxGlobalInstances;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Max(127)]
		[ModelLib.CustomAttributes.Min(-1)]
		public System.SByte MaxGlobalInstances {
			get { return _MaxGlobalInstances; }
			set { SetField(ref _MaxGlobalInstances, value, () => MaxGlobalInstances); }
		}

		private System.Byte _BlockabilityFactor;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("%")]
		public System.Byte BlockabilityFactor {
			get { return _BlockabilityFactor; }
			set { SetField(ref _BlockabilityFactor, value, () => BlockabilityFactor); }
		}

		private ModelLib.Types.TriState _UseOcclusion;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState UseOcclusion {
			get { return _UseOcclusion; }
			set { SetField(ref _UseOcclusion, value, () => UseOcclusion); }
		}

		private ModelLib.Types.TriState _StreamingSound;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState StreamingSound {
			get { return _StreamingSound; }
			set { SetField(ref _StreamingSound, value, () => StreamingSound); }
		}

		private ModelLib.Types.TriState _StopOnZoneDeactivation;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState StopOnZoneDeactivation {
			get { return _StopOnZoneDeactivation; }
			set { SetField(ref _StopOnZoneDeactivation, value, () => StopOnZoneDeactivation); }
		}

		private ModelLib.Types.TriState _TriggerOnLoudSound;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState TriggerOnLoudSound {
			get { return _TriggerOnLoudSound; }
			set { SetField(ref _TriggerOnLoudSound, value, () => TriggerOnLoudSound); }
		}

		private ModelLib.Types.TriState _CanTriggerOverWater;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState CanTriggerOverWater {
			get { return _CanTriggerOverWater; }
			set { SetField(ref _CanTriggerOverWater, value, () => CanTriggerOverWater); }
		}

		private ModelLib.Types.TriState _CheckConditionsEachFrame;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState CheckConditionsEachFrame {
			get { return _CheckConditionsEachFrame; }
			set { SetField(ref _CheckConditionsEachFrame, value, () => CheckConditionsEachFrame); }
		}

		private System.Byte _MaxPathDepth;
		[ModelLib.CustomAttributes.Default("3")]
		[ModelLib.CustomAttributes.Max(5)]
		[ModelLib.CustomAttributes.Min(1)]
		public System.Byte MaxPathDepth {
			get { return _MaxPathDepth; }
			set { SetField(ref _MaxPathDepth, value, () => MaxPathDepth); }
		}

		private ModelLib.Types.TriState _UsePlayerPosition;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState UsePlayerPosition {
			get { return _UsePlayerPosition; }
			set { SetField(ref _UsePlayerPosition, value, () => UsePlayerPosition); }
		}

		private ModelLib.Types.TriState _DisableInMultiplayer;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState DisableInMultiplayer {
			get { return _DisableInMultiplayer; }
			set { SetField(ref _DisableInMultiplayer, value, () => DisableInMultiplayer); }
		}

		public class ExplicitSpawnPositionDefinition: ModelBase {
			private System.Single _x;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single x {
				get { return _x; }
				set { SetField(ref _x, value, () => x); }
			}

			private System.Single _y;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

			private System.Single _z;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single z {
				get { return _z; }
				set { SetField(ref _z, value, () => z); }
			}

		}
		private ExplicitSpawnPositionDefinition _ExplicitSpawnPosition;
		[ModelLib.CustomAttributes.Unit("vector3")]
		public ExplicitSpawnPositionDefinition ExplicitSpawnPosition {
			get { return _ExplicitSpawnPosition; }
			set { SetField(ref _ExplicitSpawnPosition, value, () => ExplicitSpawnPosition); }
		}

		public class ConditionDefinition: ModelBase {
			private System.String _ConditionVariable;
			[ModelLib.CustomAttributes.Unit("variable")]
			public System.String ConditionVariable {
				get { return _ConditionVariable; }
				set { SetField(ref _ConditionVariable, value, () => ConditionVariable); }
			}

			private System.Single _ConditionValue;
			[ModelLib.CustomAttributes.Default("0")]
			public System.Single ConditionValue {
				get { return _ConditionValue; }
				set { SetField(ref _ConditionValue, value, () => ConditionValue); }
			}

			private AmbientRuleConditionTypes _ConditionType;
			[ModelLib.CustomAttributes.Default("RULE_IF_CONDITION_GREATER_THAN")]
			public AmbientRuleConditionTypes ConditionType {
				get { return _ConditionType; }
				set { SetField(ref _ConditionType, value, () => ConditionType); }
			}

			private AmbientRuleBankLoad _BankLoading;
			[ModelLib.CustomAttributes.Default("DONT_INFLUENCE_BANK_LOAD")]
			public AmbientRuleBankLoad BankLoading {
				get { return _BankLoading; }
				set { SetField(ref _BankLoading, value, () => BankLoading); }
			}

		}
		private ItemsObservableCollection<ConditionDefinition> _Condition = new ItemsObservableCollection<ConditionDefinition>();
		[ModelLib.CustomAttributes.MaxSize(5)]
		[XmlElement("Condition")]
		public ItemsObservableCollection<ConditionDefinition> Condition {
			get { return _Condition; }
			set { SetField(ref _Condition, value, () => Condition); }
		}

	}
}


