using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Radio")]
	public class RadioStationTrackList:  ModelBase {

		private TrackCats _Category;
		[ModelLib.CustomAttributes.Default("RADIO_TRACK_CAT_MUSIC")]
		public TrackCats Category {
			get { return _Category; }
			set { SetField(ref _Category, value, () => Category); }
		}

		private ModelLib.Types.TriState _Locked;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState Locked {
			get { return _Locked; }
			set { SetField(ref _Locked, value, () => Locked); }
		}

		private ModelLib.Types.TriState _Shuffle;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState Shuffle {
			get { return _Shuffle; }
			set { SetField(ref _Shuffle, value, () => Shuffle); }
		}

		private System.UInt32 _NextValidSelectionTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 NextValidSelectionTime {
			get { return _NextValidSelectionTime; }
			set { SetField(ref _NextValidSelectionTime, value, () => NextValidSelectionTime); }
		}

		private System.Byte _HistoryWriteIndex;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Byte HistoryWriteIndex {
			get { return _HistoryWriteIndex; }
			set { SetField(ref _HistoryWriteIndex, value, () => HistoryWriteIndex); }
		}

		private System.UInt32 _HistorySpace;
		[ModelLib.CustomAttributes.Default("10")]
		[ModelLib.CustomAttributes.Max(10)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Hidden]
		[ModelLib.CustomAttributes.InitialValue(0)]
		public System.UInt32 HistorySpace {
			get { return _HistorySpace; }
			set { SetField(ref _HistorySpace, value, () => HistorySpace); }
		}

		private System.UInt16 _TotalNumTracks;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt16 TotalNumTracks {
			get { return _TotalNumTracks; }
			set { SetField(ref _TotalNumTracks, value, () => TotalNumTracks); }
		}

		private System.UInt32 _NextTrackListPtr;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 NextTrackListPtr {
			get { return _NextTrackListPtr; }
			set { SetField(ref _NextTrackListPtr, value, () => NextTrackListPtr); }
		}

		public class TrackDefinition: ModelBase {
			private System.String _Context;
			public System.String Context {
				get { return _Context; }
				set { SetField(ref _Context, value, () => Context); }
			}

			private ModelLib.ObjectRef _SoundRef;
			[ModelLib.CustomAttributes.Default("NULL_SOUND")]
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef SoundRef {
				get { return _SoundRef; }
				set { SetField(ref _SoundRef, value, () => SoundRef); }
			}

		}
		private ItemsObservableCollection<TrackDefinition> _Track = new ItemsObservableCollection<TrackDefinition>();
		[ModelLib.CustomAttributes.MaxSize(250)]
		[XmlElement("Track")]
		public ItemsObservableCollection<TrackDefinition> Track {
			get { return _Track; }
			set { SetField(ref _Track, value, () => Track); }
		}

	}
}


