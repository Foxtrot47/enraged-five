using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Environment")]
	public class MicrophoneSettings:  ModelBase {

		private MicrophoneType _MicType;
		[ModelLib.CustomAttributes.Default("MIC_FOLLOW_PED")]
		public MicrophoneType MicType {
			get { return _MicType; }
			set { SetField(ref _MicType, value, () => MicType); }
		}

		private ModelLib.Types.TriState _PlayerFrontend;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState PlayerFrontend {
			get { return _PlayerFrontend; }
			set { SetField(ref _PlayerFrontend, value, () => PlayerFrontend); }
		}

		private ModelLib.Types.TriState _DistantPlayerWeapons;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState DistantPlayerWeapons {
			get { return _DistantPlayerWeapons; }
			set { SetField(ref _DistantPlayerWeapons, value, () => DistantPlayerWeapons); }
		}

		private ModelLib.Types.TriState _DisableBulletsBy;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState DisableBulletsBy {
			get { return _DisableBulletsBy; }
			set { SetField(ref _DisableBulletsBy, value, () => DisableBulletsBy); }
		}

		public class ListenerParametersDefinition: ModelBase {
			private System.Single _ListenerContribution;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single ListenerContribution {
				get { return _ListenerContribution; }
				set { SetField(ref _ListenerContribution, value, () => ListenerContribution); }
			}

			private System.Single _RearAttenuationFrontConeAngle;
			[ModelLib.CustomAttributes.Default("45.0")]
			public System.Single RearAttenuationFrontConeAngle {
				get { return _RearAttenuationFrontConeAngle; }
				set { SetField(ref _RearAttenuationFrontConeAngle, value, () => RearAttenuationFrontConeAngle); }
			}

			private System.Single _RearAttenuationRearConeAngle;
			[ModelLib.CustomAttributes.Default("135.0")]
			public System.Single RearAttenuationRearConeAngle {
				get { return _RearAttenuationRearConeAngle; }
				set { SetField(ref _RearAttenuationRearConeAngle, value, () => RearAttenuationRearConeAngle); }
			}

			private System.Single _CloseRearAttenuation;
			[ModelLib.CustomAttributes.Default("-3.0")]
			public System.Single CloseRearAttenuation {
				get { return _CloseRearAttenuation; }
				set { SetField(ref _CloseRearAttenuation, value, () => CloseRearAttenuation); }
			}

			private System.Single _FarRearAttenuation;
			[ModelLib.CustomAttributes.Default("-3.0")]
			public System.Single FarRearAttenuation {
				get { return _FarRearAttenuation; }
				set { SetField(ref _FarRearAttenuation, value, () => FarRearAttenuation); }
			}

			private System.Single _RollOff;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single RollOff {
				get { return _RollOff; }
				set { SetField(ref _RollOff, value, () => RollOff); }
			}

			private MicAttenuationType _RearAttenuationType;
			[ModelLib.CustomAttributes.Default("REAR_ATTENUATION_DEFAULT")]
			public MicAttenuationType RearAttenuationType {
				get { return _RearAttenuationType; }
				set { SetField(ref _RearAttenuationType, value, () => RearAttenuationType); }
			}

			private System.Single _MicLength;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single MicLength {
				get { return _MicLength; }
				set { SetField(ref _MicLength, value, () => MicLength); }
			}

			private System.Single _MicToPlayerLocalEnvironmentRatio;
			[ModelLib.CustomAttributes.Default("0.5")]
			public System.Single MicToPlayerLocalEnvironmentRatio {
				get { return _MicToPlayerLocalEnvironmentRatio; }
				set { SetField(ref _MicToPlayerLocalEnvironmentRatio, value, () => MicToPlayerLocalEnvironmentRatio); }
			}

		}
		private ItemsObservableCollection<ListenerParametersDefinition> _ListenerParameters = new ItemsObservableCollection<ListenerParametersDefinition>();
		[ModelLib.CustomAttributes.DisplayGroup("Params")]
		[ModelLib.CustomAttributes.MaxSize(2)]
		[XmlElement("ListenerParameters")]
		public ItemsObservableCollection<ListenerParametersDefinition> ListenerParameters {
			get { return _ListenerParameters; }
			set { SetField(ref _ListenerParameters, value, () => ListenerParameters); }
		}

	}
}


