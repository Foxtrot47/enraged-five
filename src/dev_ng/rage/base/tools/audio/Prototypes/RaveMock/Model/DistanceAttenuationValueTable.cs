using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class DistanceAttenuationValueTable:  BaseCurve{

		public class ValueDefinition: ModelBase {
			private System.Single _y;
			public System.Single y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

		}
		private ItemsObservableCollection<ValueDefinition> _Value = new ItemsObservableCollection<ValueDefinition>();
		[ModelLib.CustomAttributes.MaxSize(65535)]
		[XmlElement("Value")]
		public ItemsObservableCollection<ValueDefinition> Value {
			get { return _Value; }
			set { SetField(ref _Value, value, () => Value); }
		}

	}
}


