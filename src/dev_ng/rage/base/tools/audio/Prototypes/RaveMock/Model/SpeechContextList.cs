using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class SpeechContextList:  ModelBase {

		public class SpeechContextsDefinition: ModelBase {
			private ModelLib.ObjectRef _SpeechContext;
			[ModelLib.CustomAttributes.AllowedType(typeof(TriggeredSpeechContext))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef SpeechContext {
				get { return _SpeechContext; }
				set { SetField(ref _SpeechContext, value, () => SpeechContext); }
			}

		}
		private ItemsObservableCollection<SpeechContextsDefinition> _SpeechContexts = new ItemsObservableCollection<SpeechContextsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(64)]
		[XmlElement("SpeechContexts")]
		public ItemsObservableCollection<SpeechContextsDefinition> SpeechContexts {
			get { return _SpeechContexts; }
			set { SetField(ref _SpeechContexts, value, () => SpeechContexts); }
		}

	}
}


