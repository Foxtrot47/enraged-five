using ModelLib.CustomAttributes;

namespace model {

	public enum FootstepLoudness {
		[ModelLib.CustomAttributes.Display("Silent")]
		FOOTSTEP_LOUDNESS_SILENT,
		[ModelLib.CustomAttributes.Display("Quiet")]
		FOOTSTEP_LOUDNESS_QUIET,
		[ModelLib.CustomAttributes.Display("Medium")]
		FOOTSTEP_LOUDNESS_MEDIUM,
		[ModelLib.CustomAttributes.Display("Loud")]
		FOOTSTEP_LOUDNESS_LOUD
	}

}