using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class Voice:  ModelBase {

		private System.Byte _painType;
		[ModelLib.CustomAttributes.Default("0")]
		public System.Byte painType {
			get { return _painType; }
			set { SetField(ref _painType, value, () => painType); }
		}

	}
}


