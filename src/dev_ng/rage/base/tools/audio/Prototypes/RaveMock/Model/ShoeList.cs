using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class ShoeList:  ModelBase {

		public class ShoeDefinition: ModelBase {
			private System.String _ShoeName;
			public System.String ShoeName {
				get { return _ShoeName; }
				set { SetField(ref _ShoeName, value, () => ShoeName); }
			}

			private ModelLib.ObjectRef _ShoeAudioSettings;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef ShoeAudioSettings {
				get { return _ShoeAudioSettings; }
				set { SetField(ref _ShoeAudioSettings, value, () => ShoeAudioSettings); }
			}

		}
		private ItemsObservableCollection<ShoeDefinition> _Shoe = new ItemsObservableCollection<ShoeDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1000)]
		[XmlElement("Shoe")]
		public ItemsObservableCollection<ShoeDefinition> Shoe {
			get { return _Shoe; }
			set { SetField(ref _Shoe, value, () => Shoe); }
		}

	}
}


