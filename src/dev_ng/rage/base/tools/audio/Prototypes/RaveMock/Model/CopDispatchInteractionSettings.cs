using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class CopDispatchInteractionSettings:  ModelBase {

		private System.Int32 _MinTimeBetweenInteractions;
		[ModelLib.CustomAttributes.Default("60000")]
		[ModelLib.CustomAttributes.Max(120000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinTimeBetweenInteractions {
			get { return _MinTimeBetweenInteractions; }
			set { SetField(ref _MinTimeBetweenInteractions, value, () => MinTimeBetweenInteractions); }
		}

		private System.Int32 _MinTimeBetweenInteractionsVariance;
		[ModelLib.CustomAttributes.Default("15000")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinTimeBetweenInteractionsVariance {
			get { return _MinTimeBetweenInteractionsVariance; }
			set { SetField(ref _MinTimeBetweenInteractionsVariance, value, () => MinTimeBetweenInteractionsVariance); }
		}

		private System.Int32 _FirstLinePredelay;
		[ModelLib.CustomAttributes.Default("1000")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 FirstLinePredelay {
			get { return _FirstLinePredelay; }
			set { SetField(ref _FirstLinePredelay, value, () => FirstLinePredelay); }
		}

		private System.Int32 _FirstLinePredelayVariance;
		[ModelLib.CustomAttributes.Default("250")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 FirstLinePredelayVariance {
			get { return _FirstLinePredelayVariance; }
			set { SetField(ref _FirstLinePredelayVariance, value, () => FirstLinePredelayVariance); }
		}

		private System.Int32 _SecondLinePredelay;
		[ModelLib.CustomAttributes.Default("1000")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 SecondLinePredelay {
			get { return _SecondLinePredelay; }
			set { SetField(ref _SecondLinePredelay, value, () => SecondLinePredelay); }
		}

		private System.Int32 _SecondLinePredelayVariance;
		[ModelLib.CustomAttributes.Default("250")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 SecondLinePredelayVariance {
			get { return _SecondLinePredelayVariance; }
			set { SetField(ref _SecondLinePredelayVariance, value, () => SecondLinePredelayVariance); }
		}

		private System.Int32 _ScannerPredelay;
		[ModelLib.CustomAttributes.Default("6000")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 ScannerPredelay {
			get { return _ScannerPredelay; }
			set { SetField(ref _ScannerPredelay, value, () => ScannerPredelay); }
		}

		private System.Int32 _ScannerPredelayVariance;
		[ModelLib.CustomAttributes.Default("1000")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 ScannerPredelayVariance {
			get { return _ScannerPredelayVariance; }
			set { SetField(ref _ScannerPredelayVariance, value, () => ScannerPredelayVariance); }
		}

		private System.Int32 _MinTimeBetweenSpottedAndVehicleLinePlays;
		[ModelLib.CustomAttributes.Default("1500")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinTimeBetweenSpottedAndVehicleLinePlays {
			get { return _MinTimeBetweenSpottedAndVehicleLinePlays; }
			set { SetField(ref _MinTimeBetweenSpottedAndVehicleLinePlays, value, () => MinTimeBetweenSpottedAndVehicleLinePlays); }
		}

		private System.Int32 _MinTimeBetweenSpottedAndVehicleLinePlaysVariance;
		[ModelLib.CustomAttributes.Default("500")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinTimeBetweenSpottedAndVehicleLinePlaysVariance {
			get { return _MinTimeBetweenSpottedAndVehicleLinePlaysVariance; }
			set { SetField(ref _MinTimeBetweenSpottedAndVehicleLinePlaysVariance, value, () => MinTimeBetweenSpottedAndVehicleLinePlaysVariance); }
		}

		private System.Int32 _MaxTimeAfterVehicleChangeSpottedLineCanPlay;
		[ModelLib.CustomAttributes.Default("5000")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MaxTimeAfterVehicleChangeSpottedLineCanPlay {
			get { return _MaxTimeAfterVehicleChangeSpottedLineCanPlay; }
			set { SetField(ref _MaxTimeAfterVehicleChangeSpottedLineCanPlay, value, () => MaxTimeAfterVehicleChangeSpottedLineCanPlay); }
		}

		private System.Int32 _TimePassedSinceLastLineToForceVoiceChange;
		[ModelLib.CustomAttributes.Default("60000")]
		[ModelLib.CustomAttributes.Max(300000)]
		[ModelLib.CustomAttributes.Min(30000)]
		public System.Int32 TimePassedSinceLastLineToForceVoiceChange {
			get { return _TimePassedSinceLastLineToForceVoiceChange; }
			set { SetField(ref _TimePassedSinceLastLineToForceVoiceChange, value, () => TimePassedSinceLastLineToForceVoiceChange); }
		}

		private System.UInt32 _NumCopsKilledToForceVoiceChange;
		[ModelLib.CustomAttributes.Default("4")]
		[ModelLib.CustomAttributes.Max(20)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 NumCopsKilledToForceVoiceChange {
			get { return _NumCopsKilledToForceVoiceChange; }
			set { SetField(ref _NumCopsKilledToForceVoiceChange, value, () => NumCopsKilledToForceVoiceChange); }
		}

		public class SuspectCrashedVehicleDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

		}
		private ItemsObservableCollection<SuspectCrashedVehicleDefinition> _SuspectCrashedVehicle = new ItemsObservableCollection<SuspectCrashedVehicleDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("SuspectCrashedVehicle")]
		public ItemsObservableCollection<SuspectCrashedVehicleDefinition> SuspectCrashedVehicle {
			get { return _SuspectCrashedVehicle; }
			set { SetField(ref _SuspectCrashedVehicle, value, () => SuspectCrashedVehicle); }
		}

		public class SuspectEnteredFreewayDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

		}
		private ItemsObservableCollection<SuspectEnteredFreewayDefinition> _SuspectEnteredFreeway = new ItemsObservableCollection<SuspectEnteredFreewayDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("SuspectEnteredFreeway")]
		public ItemsObservableCollection<SuspectEnteredFreewayDefinition> SuspectEnteredFreeway {
			get { return _SuspectEnteredFreeway; }
			set { SetField(ref _SuspectEnteredFreeway, value, () => SuspectEnteredFreeway); }
		}

		public class SuspectEnteredMetroDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

		}
		private ItemsObservableCollection<SuspectEnteredMetroDefinition> _SuspectEnteredMetro = new ItemsObservableCollection<SuspectEnteredMetroDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("SuspectEnteredMetro")]
		public ItemsObservableCollection<SuspectEnteredMetroDefinition> SuspectEnteredMetro {
			get { return _SuspectEnteredMetro; }
			set { SetField(ref _SuspectEnteredMetro, value, () => SuspectEnteredMetro); }
		}

		public class SuspectIsInCarDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

		}
		private ItemsObservableCollection<SuspectIsInCarDefinition> _SuspectIsInCar = new ItemsObservableCollection<SuspectIsInCarDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("SuspectIsInCar")]
		public ItemsObservableCollection<SuspectIsInCarDefinition> SuspectIsInCar {
			get { return _SuspectIsInCar; }
			set { SetField(ref _SuspectIsInCar, value, () => SuspectIsInCar); }
		}

		public class SuspectIsOnFootDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

		}
		private ItemsObservableCollection<SuspectIsOnFootDefinition> _SuspectIsOnFoot = new ItemsObservableCollection<SuspectIsOnFootDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("SuspectIsOnFoot")]
		public ItemsObservableCollection<SuspectIsOnFootDefinition> SuspectIsOnFoot {
			get { return _SuspectIsOnFoot; }
			set { SetField(ref _SuspectIsOnFoot, value, () => SuspectIsOnFoot); }
		}

		public class SuspectIsOnMotorcycleDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

		}
		private ItemsObservableCollection<SuspectIsOnMotorcycleDefinition> _SuspectIsOnMotorcycle = new ItemsObservableCollection<SuspectIsOnMotorcycleDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("SuspectIsOnMotorcycle")]
		public ItemsObservableCollection<SuspectIsOnMotorcycleDefinition> SuspectIsOnMotorcycle {
			get { return _SuspectIsOnMotorcycle; }
			set { SetField(ref _SuspectIsOnMotorcycle, value, () => SuspectIsOnMotorcycle); }
		}

		public class SuspectLeftFreewayDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

		}
		private ItemsObservableCollection<SuspectLeftFreewayDefinition> _SuspectLeftFreeway = new ItemsObservableCollection<SuspectLeftFreewayDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("SuspectLeftFreeway")]
		public ItemsObservableCollection<SuspectLeftFreewayDefinition> SuspectLeftFreeway {
			get { return _SuspectLeftFreeway; }
			set { SetField(ref _SuspectLeftFreeway, value, () => SuspectLeftFreeway); }
		}

		public class RequestBackupDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

		}
		private ItemsObservableCollection<RequestBackupDefinition> _RequestBackup = new ItemsObservableCollection<RequestBackupDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("RequestBackup")]
		public ItemsObservableCollection<RequestBackupDefinition> RequestBackup {
			get { return _RequestBackup; }
			set { SetField(ref _RequestBackup, value, () => RequestBackup); }
		}

		public class AcknowledgeSituationDefinition: ModelBase {
			private System.Int32 _MinTimeAfterCrime;
			[ModelLib.CustomAttributes.Default("2000")]
			[ModelLib.CustomAttributes.Max(15000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeAfterCrime {
				get { return _MinTimeAfterCrime; }
				set { SetField(ref _MinTimeAfterCrime, value, () => MinTimeAfterCrime); }
			}

			private System.Int32 _MaxTimeAfterCrime;
			[ModelLib.CustomAttributes.Default("4000")]
			[ModelLib.CustomAttributes.Max(15000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeAfterCrime {
				get { return _MaxTimeAfterCrime; }
				set { SetField(ref _MaxTimeAfterCrime, value, () => MaxTimeAfterCrime); }
			}

		}
		private ItemsObservableCollection<AcknowledgeSituationDefinition> _AcknowledgeSituation = new ItemsObservableCollection<AcknowledgeSituationDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("AcknowledgeSituation")]
		public ItemsObservableCollection<AcknowledgeSituationDefinition> AcknowledgeSituation {
			get { return _AcknowledgeSituation; }
			set { SetField(ref _AcknowledgeSituation, value, () => AcknowledgeSituation); }
		}

		public class UnitRespondingDispatchDefinition: ModelBase {
			private System.Single _Probability;
			[ModelLib.CustomAttributes.Default("0.5")]
			[ModelLib.CustomAttributes.Max(1.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single Probability {
				get { return _Probability; }
				set { SetField(ref _Probability, value, () => Probability); }
			}

		}
		private ItemsObservableCollection<UnitRespondingDispatchDefinition> _UnitRespondingDispatch = new ItemsObservableCollection<UnitRespondingDispatchDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("UnitRespondingDispatch")]
		public ItemsObservableCollection<UnitRespondingDispatchDefinition> UnitRespondingDispatch {
			get { return _UnitRespondingDispatch; }
			set { SetField(ref _UnitRespondingDispatch, value, () => UnitRespondingDispatch); }
		}

		public class RequestGuidanceDispatchDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

			private System.Int32 _TimeNotSpottedToTrigger;
			[ModelLib.CustomAttributes.Default("2000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 TimeNotSpottedToTrigger {
				get { return _TimeNotSpottedToTrigger; }
				set { SetField(ref _TimeNotSpottedToTrigger, value, () => TimeNotSpottedToTrigger); }
			}

			private System.Single _ProbabilityOfMegaphoneLine;
			[ModelLib.CustomAttributes.Default("0.5")]
			[ModelLib.CustomAttributes.Max(1.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single ProbabilityOfMegaphoneLine {
				get { return _ProbabilityOfMegaphoneLine; }
				set { SetField(ref _ProbabilityOfMegaphoneLine, value, () => ProbabilityOfMegaphoneLine); }
			}

		}
		private ItemsObservableCollection<RequestGuidanceDispatchDefinition> _RequestGuidanceDispatch = new ItemsObservableCollection<RequestGuidanceDispatchDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("RequestGuidanceDispatch")]
		public ItemsObservableCollection<RequestGuidanceDispatchDefinition> RequestGuidanceDispatch {
			get { return _RequestGuidanceDispatch; }
			set { SetField(ref _RequestGuidanceDispatch, value, () => RequestGuidanceDispatch); }
		}

		public class HeliMaydayDispatchDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

		}
		private ItemsObservableCollection<HeliMaydayDispatchDefinition> _HeliMaydayDispatch = new ItemsObservableCollection<HeliMaydayDispatchDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("HeliMaydayDispatch")]
		public ItemsObservableCollection<HeliMaydayDispatchDefinition> HeliMaydayDispatch {
			get { return _HeliMaydayDispatch; }
			set { SetField(ref _HeliMaydayDispatch, value, () => HeliMaydayDispatch); }
		}

		public class ShotAtHeliDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

		}
		private ItemsObservableCollection<ShotAtHeliDefinition> _ShotAtHeli = new ItemsObservableCollection<ShotAtHeliDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("ShotAtHeli")]
		public ItemsObservableCollection<ShotAtHeliDefinition> ShotAtHeli {
			get { return _ShotAtHeli; }
			set { SetField(ref _ShotAtHeli, value, () => ShotAtHeli); }
		}

		public class HeliApproachingDispatchDefinition: ModelBase {
			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("60000")]
			[ModelLib.CustomAttributes.Max(120000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MinTimeBetweenVariance;
			[ModelLib.CustomAttributes.Default("15000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenVariance {
				get { return _MinTimeBetweenVariance; }
				set { SetField(ref _MinTimeBetweenVariance, value, () => MinTimeBetweenVariance); }
			}

		}
		private ItemsObservableCollection<HeliApproachingDispatchDefinition> _HeliApproachingDispatch = new ItemsObservableCollection<HeliApproachingDispatchDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("HeliApproachingDispatch")]
		public ItemsObservableCollection<HeliApproachingDispatchDefinition> HeliApproachingDispatch {
			get { return _HeliApproachingDispatch; }
			set { SetField(ref _HeliApproachingDispatch, value, () => HeliApproachingDispatch); }
		}

	}
}


