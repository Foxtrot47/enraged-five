using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class ForceRadioTrackAction:  MusicAction{

		private ModelLib.ObjectRef _Station;
		[ModelLib.CustomAttributes.AllowedType(typeof(RadioStationSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Station {
			get { return _Station; }
			set { SetField(ref _Station, value, () => Station); }
		}

		private System.UInt32 _NextIndex;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 NextIndex {
			get { return _NextIndex; }
			set { SetField(ref _NextIndex, value, () => NextIndex); }
		}

		public class TrackListDefinition: ModelBase {
			private ModelLib.ObjectRef _Track;
			[ModelLib.CustomAttributes.AllowedType(typeof(RadioTrackSettings))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef Track {
				get { return _Track; }
				set { SetField(ref _Track, value, () => Track); }
			}

		}
		private ItemsObservableCollection<TrackListDefinition> _TrackList = new ItemsObservableCollection<TrackListDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16)]
		[XmlElement("TrackList")]
		public ItemsObservableCollection<TrackListDefinition> TrackList {
			get { return _TrackList; }
			set { SetField(ref _TrackList, value, () => TrackList); }
		}

	}
}


