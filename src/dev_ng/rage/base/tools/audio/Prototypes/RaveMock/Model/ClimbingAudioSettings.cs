using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class ClimbingAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _climbLaunch;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef climbLaunch {
			get { return _climbLaunch; }
			set { SetField(ref _climbLaunch, value, () => climbLaunch); }
		}

		private ModelLib.ObjectRef _climbFoot;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef climbFoot {
			get { return _climbFoot; }
			set { SetField(ref _climbFoot, value, () => climbFoot); }
		}

		private ModelLib.ObjectRef _climbKnee;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef climbKnee {
			get { return _climbKnee; }
			set { SetField(ref _climbKnee, value, () => climbKnee); }
		}

		private ModelLib.ObjectRef _climbScrape;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef climbScrape {
			get { return _climbScrape; }
			set { SetField(ref _climbScrape, value, () => climbScrape); }
		}

		private ModelLib.ObjectRef _climbHand;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef climbHand {
			get { return _climbHand; }
			set { SetField(ref _climbHand, value, () => climbHand); }
		}

	}
}


