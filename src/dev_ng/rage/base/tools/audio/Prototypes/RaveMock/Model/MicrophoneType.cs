using ModelLib.CustomAttributes;

namespace model {

	public enum MicrophoneType {
		[ModelLib.CustomAttributes.Display("FollowPed")]
		MIC_FOLLOW_PED,
		[ModelLib.CustomAttributes.Display("FollowVeh")]
		MIC_FOLLOW_VEHICLE,
		[ModelLib.CustomAttributes.Display("CinematicChaseHeli")]
		MIC_C_CHASE_HELI,
		[ModelLib.CustomAttributes.Display("CinematicIdle")]
		MIC_C_IDLE,
		[ModelLib.CustomAttributes.Display("CinematicTrainTrack")]
		MIC_C_TRAIN_TRACK,
		[ModelLib.CustomAttributes.Display("SniperRifle")]
		MIC_SNIPER_RIFLE,
		[ModelLib.CustomAttributes.Display("DebugMic")]
		MIC_DEBUG,
		[ModelLib.CustomAttributes.Display("PlayerHead")]
		MIC_PLAYER_HEAD,
		[ModelLib.CustomAttributes.Display("CinematicDefault")]
		MIC_C_DEFAULT,
		[ModelLib.CustomAttributes.Display("CinematicCamManCam")]
		MIC_C_MANCAM,
		[ModelLib.CustomAttributes.Display("VehBonnetMic")]
		MIC_VEH_BONNET
	}

}