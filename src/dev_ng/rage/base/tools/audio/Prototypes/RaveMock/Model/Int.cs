using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class Int:  ModelBase {

		private System.Int32 _Value;
		[ModelLib.CustomAttributes.Default("0")]
		public System.Int32 Value {
			get { return _Value; }
			set { SetField(ref _Value, value, () => Value); }
		}

	}
}


