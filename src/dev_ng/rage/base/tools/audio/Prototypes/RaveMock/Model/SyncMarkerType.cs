using ModelLib.CustomAttributes;

namespace model {

	public enum SyncMarkerType {
		[ModelLib.CustomAttributes.Display("None")]
		kNoSyncMarker,
		[ModelLib.CustomAttributes.Display("AnyMarker")]
		kAnyMarker,
		[ModelLib.CustomAttributes.Display("BeatMarker")]
		kBeatMarker,
		[ModelLib.CustomAttributes.Display("BarMarker")]
		kBarMarker
	}

}