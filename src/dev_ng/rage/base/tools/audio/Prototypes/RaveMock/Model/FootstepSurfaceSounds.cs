using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class FootstepSurfaceSounds:  ModelBase {

		private ModelLib.ObjectRef _Normal;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef Normal {
			get { return _Normal; }
			set { SetField(ref _Normal, value, () => Normal); }
		}

	}
}


