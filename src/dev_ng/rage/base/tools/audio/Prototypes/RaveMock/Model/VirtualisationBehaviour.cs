using ModelLib.CustomAttributes;

namespace model {

	public enum VirtualisationBehaviour {
		[ModelLib.CustomAttributes.Display("Normal")]
		kVirtualisationNormal,
		[ModelLib.CustomAttributes.Display("StopWhenVirtualised")]
		kVirtualisationStop,
		[ModelLib.CustomAttributes.Display("Uncancellable")]
		kVirtualisationUcancellable
	}

}