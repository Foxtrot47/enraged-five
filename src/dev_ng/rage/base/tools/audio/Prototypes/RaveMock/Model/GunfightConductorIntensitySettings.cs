using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Conductors")]
	public class GunfightConductorIntensitySettings:  ModelBase {

		private ModelLib.ObjectRef _ObjectsMediumIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("DynamicFakeBISoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ObjectsMediumIntensity {
			get { return _ObjectsMediumIntensity; }
			set { SetField(ref _ObjectsMediumIntensity, value, () => ObjectsMediumIntensity); }
		}

		private ModelLib.ObjectRef _ObjectsHighIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("DynamicFakeBISoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ObjectsHighIntensity {
			get { return _ObjectsHighIntensity; }
			set { SetField(ref _ObjectsHighIntensity, value, () => ObjectsHighIntensity); }
		}

		private ModelLib.ObjectRef _VehiclesMediumIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("DynamicFakeBISoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehiclesMediumIntensity {
			get { return _VehiclesMediumIntensity; }
			set { SetField(ref _VehiclesMediumIntensity, value, () => VehiclesMediumIntensity); }
		}

		private ModelLib.ObjectRef _VehiclesHighIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("DynamicFakeBISoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehiclesHighIntensity {
			get { return _VehiclesHighIntensity; }
			set { SetField(ref _VehiclesHighIntensity, value, () => VehiclesHighIntensity); }
		}

		private ModelLib.ObjectRef _GroundMediumIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("DynamicFakeBISoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GroundMediumIntensity {
			get { return _GroundMediumIntensity; }
			set { SetField(ref _GroundMediumIntensity, value, () => GroundMediumIntensity); }
		}

		private ModelLib.ObjectRef _GroundHighIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("DynamicFakeBISoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GroundHighIntensity {
			get { return _GroundHighIntensity; }
			set { SetField(ref _GroundHighIntensity, value, () => GroundHighIntensity); }
		}

		public class CommonDefinition: ModelBase {
			private System.UInt32 _MaxTimeAfterLastShot;
			[ModelLib.CustomAttributes.Default("500")]
			[ModelLib.CustomAttributes.Max(5000)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("ms")]
			[ModelLib.CustomAttributes.Hidden]
			public System.UInt32 MaxTimeAfterLastShot {
				get { return _MaxTimeAfterLastShot; }
				set { SetField(ref _MaxTimeAfterLastShot, value, () => MaxTimeAfterLastShot); }
			}

		}
		private CommonDefinition _Common;
		[ModelLib.CustomAttributes.Hidden]
		public CommonDefinition Common {
			get { return _Common; }
			set { SetField(ref _Common, value, () => Common); }
		}

		public class GoingIntoCoverDefinition: ModelBase {
			private System.Single _TimeToFakeBulletImpacts;
			[ModelLib.CustomAttributes.Default("0.5")]
			[ModelLib.CustomAttributes.Max(5.0)]
			[ModelLib.CustomAttributes.Min(0.01)]
			[ModelLib.CustomAttributes.Unit("seconds")]
			public System.Single TimeToFakeBulletImpacts {
				get { return _TimeToFakeBulletImpacts; }
				set { SetField(ref _TimeToFakeBulletImpacts, value, () => TimeToFakeBulletImpacts); }
			}

			private System.Single _MinTimeToReTriggerFakeBI;
			[ModelLib.CustomAttributes.Default("0.5")]
			[ModelLib.CustomAttributes.Max(50.0)]
			[ModelLib.CustomAttributes.Min(0.01)]
			[ModelLib.CustomAttributes.Unit("seconds")]
			public System.Single MinTimeToReTriggerFakeBI {
				get { return _MinTimeToReTriggerFakeBI; }
				set { SetField(ref _MinTimeToReTriggerFakeBI, value, () => MinTimeToReTriggerFakeBI); }
			}

			private System.Single _MaxTimeToReTriggerFakeBI;
			[ModelLib.CustomAttributes.Default("0.5")]
			[ModelLib.CustomAttributes.Max(50.0)]
			[ModelLib.CustomAttributes.Min(0.01)]
			[ModelLib.CustomAttributes.Unit("seconds")]
			public System.Single MaxTimeToReTriggerFakeBI {
				get { return _MaxTimeToReTriggerFakeBI; }
				set { SetField(ref _MaxTimeToReTriggerFakeBI, value, () => MaxTimeToReTriggerFakeBI); }
			}

		}
		private GoingIntoCoverDefinition _GoingIntoCover;
		public GoingIntoCoverDefinition GoingIntoCover {
			get { return _GoingIntoCover; }
			set { SetField(ref _GoingIntoCover, value, () => GoingIntoCover); }
		}

		public class RunningAwayDefinition: ModelBase {
			private System.Single _MinTimeToFakeBulletImpacts;
			[ModelLib.CustomAttributes.Default("3.0")]
			[ModelLib.CustomAttributes.Max(5.0)]
			[ModelLib.CustomAttributes.Min(0.01)]
			[ModelLib.CustomAttributes.Unit("seconds")]
			public System.Single MinTimeToFakeBulletImpacts {
				get { return _MinTimeToFakeBulletImpacts; }
				set { SetField(ref _MinTimeToFakeBulletImpacts, value, () => MinTimeToFakeBulletImpacts); }
			}

			private System.Single _MaxTimeToFakeBulletImpacts;
			[ModelLib.CustomAttributes.Default("10.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(5.0)]
			[ModelLib.CustomAttributes.Unit("seconds")]
			public System.Single MaxTimeToFakeBulletImpacts {
				get { return _MaxTimeToFakeBulletImpacts; }
				set { SetField(ref _MaxTimeToFakeBulletImpacts, value, () => MaxTimeToFakeBulletImpacts); }
			}

			private System.Single _VfxProbability;
			[ModelLib.CustomAttributes.Default("0.5")]
			[ModelLib.CustomAttributes.Max(1.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single VfxProbability {
				get { return _VfxProbability; }
				set { SetField(ref _VfxProbability, value, () => VfxProbability); }
			}

		}
		private RunningAwayDefinition _RunningAway;
		public RunningAwayDefinition RunningAway {
			get { return _RunningAway; }
			set { SetField(ref _RunningAway, value, () => RunningAway); }
		}

		public class OpenSpaceDefinition: ModelBase {
			private System.Single _MinTimeToFakeBulletImpacts;
			[ModelLib.CustomAttributes.Default("3.0")]
			[ModelLib.CustomAttributes.Max(5.0)]
			[ModelLib.CustomAttributes.Min(0.01)]
			[ModelLib.CustomAttributes.Unit("seconds")]
			public System.Single MinTimeToFakeBulletImpacts {
				get { return _MinTimeToFakeBulletImpacts; }
				set { SetField(ref _MinTimeToFakeBulletImpacts, value, () => MinTimeToFakeBulletImpacts); }
			}

			private System.Single _MaxTimeToFakeBulletImpacts;
			[ModelLib.CustomAttributes.Default("10.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(5.0)]
			[ModelLib.CustomAttributes.Unit("seconds")]
			public System.Single MaxTimeToFakeBulletImpacts {
				get { return _MaxTimeToFakeBulletImpacts; }
				set { SetField(ref _MaxTimeToFakeBulletImpacts, value, () => MaxTimeToFakeBulletImpacts); }
			}

		}
		private OpenSpaceDefinition _OpenSpace;
		public OpenSpaceDefinition OpenSpace {
			get { return _OpenSpace; }
			set { SetField(ref _OpenSpace, value, () => OpenSpace); }
		}

	}
}


