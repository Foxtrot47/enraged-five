using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Scanner")]
	public class ScannerSpecificLocation:  ModelBase {

		private System.Single _Radius;
		[ModelLib.CustomAttributes.Default("100.0")]
		public System.Single Radius {
			get { return _Radius; }
			set { SetField(ref _Radius, value, () => Radius); }
		}

		private System.Single _ProbOfPlaying;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single ProbOfPlaying {
			get { return _ProbOfPlaying; }
			set { SetField(ref _ProbOfPlaying, value, () => ProbOfPlaying); }
		}

		public class PositionDefinition: ModelBase {
			private System.Single _x;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single x {
				get { return _x; }
				set { SetField(ref _x, value, () => x); }
			}

			private System.Single _y;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

			private System.Single _z;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single z {
				get { return _z; }
				set { SetField(ref _z, value, () => z); }
			}

		}
		private PositionDefinition _Position;
		[ModelLib.CustomAttributes.Unit("vector3")]
		public PositionDefinition Position {
			get { return _Position; }
			set { SetField(ref _Position, value, () => Position); }
		}

		public class SoundsDefinition: ModelBase {
			private ModelLib.ObjectRef _Sound;
			[ModelLib.CustomAttributes.Default("NULL_SOUND")]
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Sound {
				get { return _Sound; }
				set { SetField(ref _Sound, value, () => Sound); }
			}

		}
		private ItemsObservableCollection<SoundsDefinition> _Sounds = new ItemsObservableCollection<SoundsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16)]
		[XmlElement("Sounds")]
		public ItemsObservableCollection<SoundsDefinition> Sounds {
			get { return _Sounds; }
			set { SetField(ref _Sounds, value, () => Sounds); }
		}

	}
}


