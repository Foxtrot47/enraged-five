using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	[ModelLib.CustomAttributes.Validator("validators/RandomizedSoundValidator.xsl")]
	public class RandomizedSound:  Sound{

		private System.String _OrderedDirection;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Ignore]
		[ModelLib.CustomAttributes.Unit("variable")]
		[ModelLib.CustomAttributes.Hidden]
		public System.String OrderedDirection {
			get { return _OrderedDirection; }
			set { SetField(ref _OrderedDirection, value, () => OrderedDirection); }
		}

		private System.Byte _HistoryIndex;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Byte HistoryIndex {
			get { return _HistoryIndex; }
			set { SetField(ref _HistoryIndex, value, () => HistoryIndex); }
		}

		private ModelLib.Types.TriState _OrderedPlaylist;
		public ModelLib.Types.TriState OrderedPlaylist {
			get { return _OrderedPlaylist; }
			set { SetField(ref _OrderedPlaylist, value, () => OrderedPlaylist); }
		}

		private System.SByte _HistorySpace;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(254)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.InitialValue(-1)]
		public System.SByte HistorySpace {
			get { return _HistorySpace; }
			set { SetField(ref _HistorySpace, value, () => HistorySpace); }
		}

		public class VariationsDefinition: ModelBase {
			private ModelLib.ObjectRef _Variation;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.RequiresValue]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef Variation {
				get { return _Variation; }
				set { SetField(ref _Variation, value, () => Variation); }
			}

			private System.Single _Weight;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single Weight {
				get { return _Weight; }
				set { SetField(ref _Weight, value, () => Weight); }
			}

		}
		private ItemsObservableCollection<VariationsDefinition> _Variations = new ItemsObservableCollection<VariationsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(254)]
		[XmlElement("Variations")]
		public ItemsObservableCollection<VariationsDefinition> Variations {
			get { return _Variations; }
			set { SetField(ref _Variations, value, () => Variations); }
		}

	}
}


