using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Ambience")]
	public class WeatherAudioSettings:  ModelBase {

		private System.Single _Strength;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("WindParams")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single Strength {
			get { return _Strength; }
			set { SetField(ref _Strength, value, () => Strength); }
		}

		private System.Single _Blustery;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("WindParams")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single Blustery {
			get { return _Blustery; }
			set { SetField(ref _Blustery, value, () => Blustery); }
		}

		private System.Single _Temperature;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("WindParams")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single Temperature {
			get { return _Temperature; }
			set { SetField(ref _Temperature, value, () => Temperature); }
		}

		private System.Single _TimeOfDayAffectsTemperature;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("WindParams")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single TimeOfDayAffectsTemperature {
			get { return _TimeOfDayAffectsTemperature; }
			set { SetField(ref _TimeOfDayAffectsTemperature, value, () => TimeOfDayAffectsTemperature); }
		}

		private System.Single _WhistleVolumeOffset;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("WindParams")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single WhistleVolumeOffset {
			get { return _WhistleVolumeOffset; }
			set { SetField(ref _WhistleVolumeOffset, value, () => WhistleVolumeOffset); }
		}

		private ModelLib.ObjectRef _WindGust;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WindGust {
			get { return _WindGust; }
			set { SetField(ref _WindGust, value, () => WindGust); }
		}

		private ModelLib.ObjectRef _AudioScene;
		[ModelLib.CustomAttributes.AllowedType(typeof(MixerScene))]
		[ModelLib.CustomAttributes.DisplayGroup("DynamicMixing")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AudioScene {
			get { return _AudioScene; }
			set { SetField(ref _AudioScene, value, () => AudioScene); }
		}

		private ModelLib.ObjectRef _WindGustEnd;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WindGustEnd {
			get { return _WindGustEnd; }
			set { SetField(ref _WindGustEnd, value, () => WindGustEnd); }
		}

		public class WindSoundDefinition: ModelBase {
			private ModelLib.ObjectRef _WindSoundComponent;
			[ModelLib.CustomAttributes.Default("NULL_SOUND")]
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef WindSoundComponent {
				get { return _WindSoundComponent; }
				set { SetField(ref _WindSoundComponent, value, () => WindSoundComponent); }
			}

		}
		private ItemsObservableCollection<WindSoundDefinition> _WindSound = new ItemsObservableCollection<WindSoundDefinition>();
		[ModelLib.CustomAttributes.MaxSize(6)]
		[XmlElement("WindSound")]
		public ItemsObservableCollection<WindSoundDefinition> WindSound {
			get { return _WindSound; }
			set { SetField(ref _WindSound, value, () => WindSound); }
		}

	}
}


