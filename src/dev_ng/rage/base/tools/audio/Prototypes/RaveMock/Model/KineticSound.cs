using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class KineticSound:  Sound{

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		private System.Single _Mass;
		[ModelLib.CustomAttributes.Default("100.0")]
		[ModelLib.CustomAttributes.Max(10000.0)]
		[ModelLib.CustomAttributes.Min(0.001)]
		public System.Single Mass {
			get { return _Mass; }
			set { SetField(ref _Mass, value, () => Mass); }
		}

		private ModelLib.Types.TriState _VelocityOrientation;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("If set to yes the velocity will be speed in the direction of orientation, otherwise it will be the velocity magnitude")]
		public ModelLib.Types.TriState VelocityOrientation {
			get { return _VelocityOrientation; }
			set { SetField(ref _VelocityOrientation, value, () => VelocityOrientation); }
		}

		private System.Single _YawAngle;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single YawAngle {
			get { return _YawAngle; }
			set { SetField(ref _YawAngle, value, () => YawAngle); }
		}

		private System.Single _PitchAngle;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single PitchAngle {
			get { return _PitchAngle; }
			set { SetField(ref _PitchAngle, value, () => PitchAngle); }
		}

	}
}


