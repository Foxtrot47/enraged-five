using ModelLib.CustomAttributes;

namespace model {

	public enum CarEngineTypes {
		[ModelLib.CustomAttributes.Display("Combustion")]
		COMBUSTION,
		[ModelLib.CustomAttributes.Display("Electric")]
		ELECTRIC,
		[ModelLib.CustomAttributes.Display("Hybrid")]
		HYBRID,
		[ModelLib.CustomAttributes.Display("Blend")]
		BLEND
	}

}