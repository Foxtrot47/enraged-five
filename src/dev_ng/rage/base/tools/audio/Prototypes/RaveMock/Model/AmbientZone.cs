using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	[ModelLib.CustomAttributes.Packed(false)]
	public class AmbientZone:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private System.String _Notes;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Notes {
			get { return _Notes; }
			set { SetField(ref _Notes, value, () => Notes); }
		}

		private AmbientZoneShape _Shape;
		[ModelLib.CustomAttributes.Default("kAmbientZoneCuboid")]
		public AmbientZoneShape Shape {
			get { return _Shape; }
			set { SetField(ref _Shape, value, () => Shape); }
		}

		private System.Single _BuiltUpFactor;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single BuiltUpFactor {
			get { return _BuiltUpFactor; }
			set { SetField(ref _BuiltUpFactor, value, () => BuiltUpFactor); }
		}

		private ModelLib.Types.TriState _InteriorZone;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState InteriorZone {
			get { return _InteriorZone; }
			set { SetField(ref _InteriorZone, value, () => InteriorZone); }
		}

		private System.Single _MinPedDensity;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Walla")]
		public System.Single MinPedDensity {
			get { return _MinPedDensity; }
			set { SetField(ref _MinPedDensity, value, () => MinPedDensity); }
		}

		private System.Single _MaxPedDensity;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Walla")]
		public System.Single MaxPedDensity {
			get { return _MaxPedDensity; }
			set { SetField(ref _MaxPedDensity, value, () => MaxPedDensity); }
		}

		private ModelLib.ObjectRef _PedDensityTOD;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Walla")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedDensityTOD {
			get { return _PedDensityTOD; }
			set { SetField(ref _PedDensityTOD, value, () => PedDensityTOD); }
		}

		private ModelLib.Types.TriState _ZoneEnabledStatePersistent;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState ZoneEnabledStatePersistent {
			get { return _ZoneEnabledStatePersistent; }
			set { SetField(ref _ZoneEnabledStatePersistent, value, () => ZoneEnabledStatePersistent); }
		}

		private ModelLib.Types.TriState _ZoneEnabledStateNonPersistent;
		[ModelLib.CustomAttributes.Hidden]
		public ModelLib.Types.TriState ZoneEnabledStateNonPersistent {
			get { return _ZoneEnabledStateNonPersistent; }
			set { SetField(ref _ZoneEnabledStateNonPersistent, value, () => ZoneEnabledStateNonPersistent); }
		}

		private System.Single _PedDensityScalar;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Walla")]
		public System.Single PedDensityScalar {
			get { return _PedDensityScalar; }
			set { SetField(ref _PedDensityScalar, value, () => PedDensityScalar); }
		}

		private ModelLib.Types.TriState _UsePlayerPosition;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState UsePlayerPosition {
			get { return _UsePlayerPosition; }
			set { SetField(ref _UsePlayerPosition, value, () => UsePlayerPosition); }
		}

		private ModelLib.Types.TriState _DisableInMultiplayer;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState DisableInMultiplayer {
			get { return _DisableInMultiplayer; }
			set { SetField(ref _DisableInMultiplayer, value, () => DisableInMultiplayer); }
		}

		private System.Single _MaxWindInfluence;
		[ModelLib.CustomAttributes.Default("-1.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(-1.0)]
		public System.Single MaxWindInfluence {
			get { return _MaxWindInfluence; }
			set { SetField(ref _MaxWindInfluence, value, () => MaxWindInfluence); }
		}

		private System.Single _MinWindInfluence;
		[ModelLib.CustomAttributes.Default("-1.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(-1.0)]
		public System.Single MinWindInfluence {
			get { return _MinWindInfluence; }
			set { SetField(ref _MinWindInfluence, value, () => MinWindInfluence); }
		}

		private ModelLib.Types.TriState _ScaleMaxWorldHeightWithZoneInfluence;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState ScaleMaxWorldHeightWithZoneInfluence {
			get { return _ScaleMaxWorldHeightWithZoneInfluence; }
			set { SetField(ref _ScaleMaxWorldHeightWithZoneInfluence, value, () => ScaleMaxWorldHeightWithZoneInfluence); }
		}

		private ModelLib.ObjectRef _WindElevationSounds;
		[ModelLib.CustomAttributes.AllowedType(typeof(WeatherAudioSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WindElevationSounds {
			get { return _WindElevationSounds; }
			set { SetField(ref _WindElevationSounds, value, () => WindElevationSounds); }
		}

		private ModelLib.ObjectRef _EnvironmentRule;
		[ModelLib.CustomAttributes.AllowedType(typeof(EnvironmentRule))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EnvironmentRule {
			get { return _EnvironmentRule; }
			set { SetField(ref _EnvironmentRule, value, () => EnvironmentRule); }
		}

		private ModelLib.Types.TriState _HasTunnelReflections;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState HasTunnelReflections {
			get { return _HasTunnelReflections; }
			set { SetField(ref _HasTunnelReflections, value, () => HasTunnelReflections); }
		}

		private ModelLib.Types.TriState _HasReverbLinkedReflections;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState HasReverbLinkedReflections {
			get { return _HasReverbLinkedReflections; }
			set { SetField(ref _HasReverbLinkedReflections, value, () => HasReverbLinkedReflections); }
		}

		private ModelLib.ObjectRef _AudioScene;
		[ModelLib.CustomAttributes.AllowedType(typeof(MixerScene))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AudioScene {
			get { return _AudioScene; }
			set { SetField(ref _AudioScene, value, () => AudioScene); }
		}

		private System.Single _UnderwaterCreakFactor;
		[ModelLib.CustomAttributes.Default("-1.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(-1.0)]
		public System.Single UnderwaterCreakFactor {
			get { return _UnderwaterCreakFactor; }
			set { SetField(ref _UnderwaterCreakFactor, value, () => UnderwaterCreakFactor); }
		}

		private ModelLib.Types.TriState _HasRandomStaticRadioEmitters;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState HasRandomStaticRadioEmitters {
			get { return _HasRandomStaticRadioEmitters; }
			set { SetField(ref _HasRandomStaticRadioEmitters, value, () => HasRandomStaticRadioEmitters); }
		}

		private ModelLib.Types.TriState _HasRandomVehicleRadioEmitters;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState HasRandomVehicleRadioEmitters {
			get { return _HasRandomVehicleRadioEmitters; }
			set { SetField(ref _HasRandomVehicleRadioEmitters, value, () => HasRandomVehicleRadioEmitters); }
		}

		private ModelLib.ObjectRef _PedWallaSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(PedWallaSettingsList))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedWallaSettings {
			get { return _PedWallaSettings; }
			set { SetField(ref _PedWallaSettings, value, () => PedWallaSettings); }
		}

		private ModelLib.ObjectRef _RandomisedRadioSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(RandomisedRadioEmitterSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RandomisedRadioSettings {
			get { return _RandomisedRadioSettings; }
			set { SetField(ref _RandomisedRadioSettings, value, () => RandomisedRadioSettings); }
		}

		private System.Byte _NumRulesToPlay;
		[ModelLib.CustomAttributes.Default("4")]
		[ModelLib.CustomAttributes.Max(16)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Byte NumRulesToPlay {
			get { return _NumRulesToPlay; }
			set { SetField(ref _NumRulesToPlay, value, () => NumRulesToPlay); }
		}

		private AmbientZoneWaterType _ZoneWaterCalculation;
		[ModelLib.CustomAttributes.Default("AMBIENT_ZONE_FORCE_OVER_LAND")]
		public AmbientZoneWaterType ZoneWaterCalculation {
			get { return _ZoneWaterCalculation; }
			set { SetField(ref _ZoneWaterCalculation, value, () => ZoneWaterCalculation); }
		}

		public class ActivationZoneDefinition: ModelBase {
			private System.UInt16 _RotationAngle;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(360)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("degrees")]
			public System.UInt16 RotationAngle {
				get { return _RotationAngle; }
				set { SetField(ref _RotationAngle, value, () => RotationAngle); }
			}

			public class CentreDefinition: ModelBase {
				private System.Single _x;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single x {
					get { return _x; }
					set { SetField(ref _x, value, () => x); }
				}

				private System.Single _y;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single y {
					get { return _y; }
					set { SetField(ref _y, value, () => y); }
				}

				private System.Single _z;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single z {
					get { return _z; }
					set { SetField(ref _z, value, () => z); }
				}

			}
			private CentreDefinition _Centre;
			[ModelLib.CustomAttributes.Unit("vector3")]
			public CentreDefinition Centre {
				get { return _Centre; }
				set { SetField(ref _Centre, value, () => Centre); }
			}

			public class SizeDefinition: ModelBase {
				private System.Single _x;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single x {
					get { return _x; }
					set { SetField(ref _x, value, () => x); }
				}

				private System.Single _y;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single y {
					get { return _y; }
					set { SetField(ref _y, value, () => y); }
				}

				private System.Single _z;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single z {
					get { return _z; }
					set { SetField(ref _z, value, () => z); }
				}

			}
			private SizeDefinition _Size;
			[ModelLib.CustomAttributes.Unit("vector3")]
			public SizeDefinition Size {
				get { return _Size; }
				set { SetField(ref _Size, value, () => Size); }
			}

			public class PostRotationOffsetDefinition: ModelBase {
				private System.Single _x;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single x {
					get { return _x; }
					set { SetField(ref _x, value, () => x); }
				}

				private System.Single _y;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single y {
					get { return _y; }
					set { SetField(ref _y, value, () => y); }
				}

				private System.Single _z;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single z {
					get { return _z; }
					set { SetField(ref _z, value, () => z); }
				}

			}
			private PostRotationOffsetDefinition _PostRotationOffset;
			[ModelLib.CustomAttributes.Unit("vector3")]
			public PostRotationOffsetDefinition PostRotationOffset {
				get { return _PostRotationOffset; }
				set { SetField(ref _PostRotationOffset, value, () => PostRotationOffset); }
			}

			public class SizeScaleDefinition: ModelBase {
				private System.Single _x;
				[ModelLib.CustomAttributes.Default("1.0")]
				public System.Single x {
					get { return _x; }
					set { SetField(ref _x, value, () => x); }
				}

				private System.Single _y;
				[ModelLib.CustomAttributes.Default("1.0")]
				public System.Single y {
					get { return _y; }
					set { SetField(ref _y, value, () => y); }
				}

				private System.Single _z;
				[ModelLib.CustomAttributes.Default("1.0")]
				public System.Single z {
					get { return _z; }
					set { SetField(ref _z, value, () => z); }
				}

			}
			private SizeScaleDefinition _SizeScale;
			[ModelLib.CustomAttributes.Unit("vector3")]
			public SizeScaleDefinition SizeScale {
				get { return _SizeScale; }
				set { SetField(ref _SizeScale, value, () => SizeScale); }
			}

		}
		private ActivationZoneDefinition _ActivationZone;
		public ActivationZoneDefinition ActivationZone {
			get { return _ActivationZone; }
			set { SetField(ref _ActivationZone, value, () => ActivationZone); }
		}

		public class PositioningZoneDefinition: ModelBase {
			private System.UInt16 _RotationAngle;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(360)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("degrees")]
			public System.UInt16 RotationAngle {
				get { return _RotationAngle; }
				set { SetField(ref _RotationAngle, value, () => RotationAngle); }
			}

			public class CentreDefinition2: ModelBase {
				private System.Single _x;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single x {
					get { return _x; }
					set { SetField(ref _x, value, () => x); }
				}

				private System.Single _y;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single y {
					get { return _y; }
					set { SetField(ref _y, value, () => y); }
				}

				private System.Single _z;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single z {
					get { return _z; }
					set { SetField(ref _z, value, () => z); }
				}

			}
			private CentreDefinition2 _Centre;
			[ModelLib.CustomAttributes.Unit("vector3")]
			public CentreDefinition2 Centre {
				get { return _Centre; }
				set { SetField(ref _Centre, value, () => Centre); }
			}

			public class SizeDefinition2: ModelBase {
				private System.Single _x;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single x {
					get { return _x; }
					set { SetField(ref _x, value, () => x); }
				}

				private System.Single _y;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single y {
					get { return _y; }
					set { SetField(ref _y, value, () => y); }
				}

				private System.Single _z;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single z {
					get { return _z; }
					set { SetField(ref _z, value, () => z); }
				}

			}
			private SizeDefinition2 _Size;
			[ModelLib.CustomAttributes.Unit("vector3")]
			public SizeDefinition2 Size {
				get { return _Size; }
				set { SetField(ref _Size, value, () => Size); }
			}

			public class PostRotationOffsetDefinition2: ModelBase {
				private System.Single _x;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single x {
					get { return _x; }
					set { SetField(ref _x, value, () => x); }
				}

				private System.Single _y;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single y {
					get { return _y; }
					set { SetField(ref _y, value, () => y); }
				}

				private System.Single _z;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single z {
					get { return _z; }
					set { SetField(ref _z, value, () => z); }
				}

			}
			private PostRotationOffsetDefinition2 _PostRotationOffset;
			[ModelLib.CustomAttributes.Unit("vector3")]
			public PostRotationOffsetDefinition2 PostRotationOffset {
				get { return _PostRotationOffset; }
				set { SetField(ref _PostRotationOffset, value, () => PostRotationOffset); }
			}

			public class SizeScaleDefinition2: ModelBase {
				private System.Single _x;
				[ModelLib.CustomAttributes.Default("1.0")]
				public System.Single x {
					get { return _x; }
					set { SetField(ref _x, value, () => x); }
				}

				private System.Single _y;
				[ModelLib.CustomAttributes.Default("1.0")]
				public System.Single y {
					get { return _y; }
					set { SetField(ref _y, value, () => y); }
				}

				private System.Single _z;
				[ModelLib.CustomAttributes.Default("1.0")]
				public System.Single z {
					get { return _z; }
					set { SetField(ref _z, value, () => z); }
				}

			}
			private SizeScaleDefinition2 _SizeScale;
			[ModelLib.CustomAttributes.Unit("vector3")]
			public SizeScaleDefinition2 SizeScale {
				get { return _SizeScale; }
				set { SetField(ref _SizeScale, value, () => SizeScale); }
			}

		}
		private PositioningZoneDefinition _PositioningZone;
		public PositioningZoneDefinition PositioningZone {
			get { return _PositioningZone; }
			set { SetField(ref _PositioningZone, value, () => PositioningZone); }
		}

		public class RuleDefinition: ModelBase {
			private ModelLib.ObjectRef _Ref;
			[ModelLib.CustomAttributes.AllowedType(typeof(AmbientRule))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Ref {
				get { return _Ref; }
				set { SetField(ref _Ref, value, () => Ref); }
			}

		}
		private ItemsObservableCollection<RuleDefinition> _Rule = new ItemsObservableCollection<RuleDefinition>();
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("Rule")]
		public ItemsObservableCollection<RuleDefinition> Rule {
			get { return _Rule; }
			set { SetField(ref _Rule, value, () => Rule); }
		}

		public class DirAmbienceDefinition: ModelBase {
			private ModelLib.ObjectRef _Ref;
			[ModelLib.CustomAttributes.AllowedType(typeof(DirectionalAmbience))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Ref {
				get { return _Ref; }
				set { SetField(ref _Ref, value, () => Ref); }
			}

			private System.Single _Volume;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single Volume {
				get { return _Volume; }
				set { SetField(ref _Volume, value, () => Volume); }
			}

		}
		private ItemsObservableCollection<DirAmbienceDefinition> _DirAmbience = new ItemsObservableCollection<DirAmbienceDefinition>();
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("DirAmbience")]
		public ItemsObservableCollection<DirAmbienceDefinition> DirAmbience {
			get { return _DirAmbience; }
			set { SetField(ref _DirAmbience, value, () => DirAmbience); }
		}

	}
}


