using ModelLib.CustomAttributes;

namespace model {

	public enum ParameterDestinations {
		[ModelLib.CustomAttributes.Display("Volume")]
		PARAM_DESTINATION_VOLUME,
		[ModelLib.CustomAttributes.Display("Pitch")]
		PARAM_DESTINATION_PITCH,
		[ModelLib.CustomAttributes.Display("Pan")]
		PARAM_DESTINATION_PAN,
		[ModelLib.CustomAttributes.Display("StartOffset")]
		PARAM_DESTINATION_STARTOFFSET,
		[ModelLib.CustomAttributes.Display("Predelay")]
		PARAM_DESTINATION_PREDELAY,
		[ModelLib.CustomAttributes.Display("LPF Cutoff")]
		PARAM_DESTINATION_LPF,
		[ModelLib.CustomAttributes.Display("HPF Cutoff")]
		PARAM_DESTINATION_HPF,
		[ModelLib.CustomAttributes.Display("Custom variable")]
		PARAM_DESTINATION_VARIABLE,
		[ModelLib.CustomAttributes.Display("VolumeCurveScale")]
		PARAM_DESTINATION_ROLLOFF
	}

}