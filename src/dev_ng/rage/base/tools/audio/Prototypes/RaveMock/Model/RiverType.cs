using ModelLib.CustomAttributes;

namespace model {

	public enum RiverType {
		[ModelLib.CustomAttributes.Display("BrookWeak")]
		AUD_RIVER_BROOK_WEAK,
		[ModelLib.CustomAttributes.Display("BrookStrong")]
		AUD_RIVER_BROOK_STRONG,
		[ModelLib.CustomAttributes.Display("LAWeak")]
		AUD_RIVER_LA_WEAK,
		[ModelLib.CustomAttributes.Display("LAStrong")]
		AUD_RIVER_LA_STRONG,
		[ModelLib.CustomAttributes.Display("Weak")]
		AUD_RIVER_WEAK,
		[ModelLib.CustomAttributes.Display("Medium")]
		AUD_RIVER_MEDIUM,
		[ModelLib.CustomAttributes.Display("Strong")]
		AUD_RIVER_STRONG,
		[ModelLib.CustomAttributes.Display("RapidsWeak")]
		AUD_RIVER_RAPIDS_WEAK,
		[ModelLib.CustomAttributes.Display("RapidsStrong")]
		AUD_RIVER_RAPIDS_STRONG
	}

}