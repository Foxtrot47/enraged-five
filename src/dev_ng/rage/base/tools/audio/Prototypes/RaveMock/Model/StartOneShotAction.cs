using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class StartOneShotAction:  MusicAction{

		private ModelLib.ObjectRef _Sound;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Sound {
			get { return _Sound; }
			set { SetField(ref _Sound, value, () => Sound); }
		}

		private System.Int32 _Predelay;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Min(-1)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.Int32 Predelay {
			get { return _Predelay; }
			set { SetField(ref _Predelay, value, () => Predelay); }
		}

		private System.Int32 _FadeInTime;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(-1)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.Int32 FadeInTime {
			get { return _FadeInTime; }
			set { SetField(ref _FadeInTime, value, () => FadeInTime); }
		}

		private System.Int32 _FadeOutTime;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(-1)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.Int32 FadeOutTime {
			get { return _FadeOutTime; }
			set { SetField(ref _FadeOutTime, value, () => FadeOutTime); }
		}

		private SyncMarkerType _SyncMarker;
		[ModelLib.CustomAttributes.Default("kNoSyncMarker")]
		public SyncMarkerType SyncMarker {
			get { return _SyncMarker; }
			set { SetField(ref _SyncMarker, value, () => SyncMarker); }
		}

		private ModelLib.Types.TriState _UnfreezeRadio;
		public ModelLib.Types.TriState UnfreezeRadio {
			get { return _UnfreezeRadio; }
			set { SetField(ref _UnfreezeRadio, value, () => UnfreezeRadio); }
		}

		private ModelLib.Types.TriState _OverrideRadio;
		public ModelLib.Types.TriState OverrideRadio {
			get { return _OverrideRadio; }
			set { SetField(ref _OverrideRadio, value, () => OverrideRadio); }
		}

	}
}


