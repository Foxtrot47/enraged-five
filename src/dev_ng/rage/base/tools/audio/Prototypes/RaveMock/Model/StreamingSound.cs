using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class StreamingSound:  Sound{

		private ModelLib.Types.TriState _StopWhenChildStops;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("If set to yes then the streaming sound will stop itself when any one of its child sounds finish, otherwise it will continue playing until all child sounds have finished")]
		public ModelLib.Types.TriState StopWhenChildStops {
			get { return _StopWhenChildStops; }
			set { SetField(ref _StopWhenChildStops, value, () => StopWhenChildStops); }
		}

		private ModelLib.Types.TriState _Looping;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("If set to yes then the streaming sound will ensure that its child sounds loop the full extent of the Wave data contained in the referenced streaming Wave Bank")]
		public ModelLib.Types.TriState Looping {
			get { return _Looping; }
			set { SetField(ref _Looping, value, () => Looping); }
		}

		private ModelLib.Types.TriState _StreamPhysically;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.Description("If set to yes then the streaming sound will physically stream (and physically or virtually play back) the Wave data contained in the referenced streaming Wave Bank")]
		public ModelLib.Types.TriState StreamPhysically {
			get { return _StreamPhysically; }
			set { SetField(ref _StreamPhysically, value, () => StreamPhysically); }
		}

		private System.UInt32 _Duration;
		[ModelLib.CustomAttributes.Default("0")]
		public System.UInt32 Duration {
			get { return _Duration; }
			set { SetField(ref _Duration, value, () => Duration); }
		}

		public class SoundRefDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundId;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef SoundId {
				get { return _SoundId; }
				set { SetField(ref _SoundId, value, () => SoundId); }
			}

			private System.String _ReferenceName;
			[ModelLib.CustomAttributes.Ignore]
			[ModelLib.CustomAttributes.Hidden]
			public System.String ReferenceName {
				get { return _ReferenceName; }
				set { SetField(ref _ReferenceName, value, () => ReferenceName); }
			}

		}
		private ItemsObservableCollection<SoundRefDefinition> _SoundRef = new ItemsObservableCollection<SoundRefDefinition>();
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("SoundRef")]
		public ItemsObservableCollection<SoundRefDefinition> SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

	}
}


