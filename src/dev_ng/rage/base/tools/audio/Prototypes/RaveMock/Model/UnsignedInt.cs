using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class UnsignedInt:  ModelBase {

		private System.UInt32 _Value;
		[ModelLib.CustomAttributes.Default("0")]
		public System.UInt32 Value {
			get { return _Value; }
			set { SetField(ref _Value, value, () => Value); }
		}

	}
}


