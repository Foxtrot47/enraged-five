using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class SilenceConstraint:  MusicTimingConstraint{

		private System.Single _MinimumDuration;
		[ModelLib.CustomAttributes.Default("0.15")]
		[ModelLib.CustomAttributes.Max(30)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("seconds")]
		public System.Single MinimumDuration {
			get { return _MinimumDuration; }
			set { SetField(ref _MinimumDuration, value, () => MinimumDuration); }
		}

	}
}


