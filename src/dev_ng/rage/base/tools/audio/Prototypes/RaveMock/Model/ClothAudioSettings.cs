using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class ClothAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _ImpactSound;
		[ModelLib.CustomAttributes.Default("BODY_FALL_BODY")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ImpactSound {
			get { return _ImpactSound; }
			set { SetField(ref _ImpactSound, value, () => ImpactSound); }
		}

		private ModelLib.ObjectRef _WalkSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WalkSound {
			get { return _WalkSound; }
			set { SetField(ref _WalkSound, value, () => WalkSound); }
		}

		private ModelLib.ObjectRef _RunSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RunSound {
			get { return _RunSound; }
			set { SetField(ref _RunSound, value, () => RunSound); }
		}

		private ModelLib.ObjectRef _SprintSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SprintSound {
			get { return _SprintSound; }
			set { SetField(ref _SprintSound, value, () => SprintSound); }
		}

		private ModelLib.ObjectRef _IntoCoverSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IntoCoverSound {
			get { return _IntoCoverSound; }
			set { SetField(ref _IntoCoverSound, value, () => IntoCoverSound); }
		}

		private ModelLib.ObjectRef _OutOfCoverSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef OutOfCoverSound {
			get { return _OutOfCoverSound; }
			set { SetField(ref _OutOfCoverSound, value, () => OutOfCoverSound); }
		}

		private ModelLib.ObjectRef _WindSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WindSound {
			get { return _WindSound; }
			set { SetField(ref _WindSound, value, () => WindSound); }
		}

		private System.Single _Intensity;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("ClothParams")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single Intensity {
			get { return _Intensity; }
			set { SetField(ref _Intensity, value, () => Intensity); }
		}

		private ModelLib.Types.TriState _HavePockets;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState HavePockets {
			get { return _HavePockets; }
			set { SetField(ref _HavePockets, value, () => HavePockets); }
		}

		private ModelLib.ObjectRef _PlayerVersion;
		[ModelLib.CustomAttributes.AllowedType(typeof(ClothAudioSettings))]
		[ModelLib.CustomAttributes.DisplayGroup("Player")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PlayerVersion {
			get { return _PlayerVersion; }
			set { SetField(ref _PlayerVersion, value, () => PlayerVersion); }
		}

		private ModelLib.ObjectRef _BulletImpacts;
		[ModelLib.CustomAttributes.Default("GENERIC_PED_BULLETIMPACTS")]
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BulletImpacts {
			get { return _BulletImpacts; }
			set { SetField(ref _BulletImpacts, value, () => BulletImpacts); }
		}

		private ModelLib.ObjectRef _PedRollSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedRollSound {
			get { return _PedRollSound; }
			set { SetField(ref _PedRollSound, value, () => PedRollSound); }
		}

		private ModelLib.ObjectRef _ScrapeMaterialSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(CollisionMaterialSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScrapeMaterialSettings {
			get { return _ScrapeMaterialSettings; }
			set { SetField(ref _ScrapeMaterialSettings, value, () => ScrapeMaterialSettings); }
		}

		private ModelLib.ObjectRef _JumpLandSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef JumpLandSound {
			get { return _JumpLandSound; }
			set { SetField(ref _JumpLandSound, value, () => JumpLandSound); }
		}

		private ModelLib.Types.TriState _FlapsInWind;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState FlapsInWind {
			get { return _FlapsInWind; }
			set { SetField(ref _FlapsInWind, value, () => FlapsInWind); }
		}

	}
}


