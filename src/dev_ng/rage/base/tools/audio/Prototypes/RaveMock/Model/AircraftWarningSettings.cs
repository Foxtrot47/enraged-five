using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class AircraftWarningSettings:  ModelBase {

		private System.Int32 _MinTimeBetweenDamageReports;
		[ModelLib.CustomAttributes.Default("1000")]
		[ModelLib.CustomAttributes.Max(500000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinTimeBetweenDamageReports {
			get { return _MinTimeBetweenDamageReports; }
			set { SetField(ref _MinTimeBetweenDamageReports, value, () => MinTimeBetweenDamageReports); }
		}

		public class TargetedLockDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<TargetedLockDefinition> _TargetedLock = new ItemsObservableCollection<TargetedLockDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("TargetedLock")]
		public ItemsObservableCollection<TargetedLockDefinition> TargetedLock {
			get { return _TargetedLock; }
			set { SetField(ref _TargetedLock, value, () => TargetedLock); }
		}

		public class MissleFiredDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<MissleFiredDefinition> _MissleFired = new ItemsObservableCollection<MissleFiredDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("MissleFired")]
		public ItemsObservableCollection<MissleFiredDefinition> MissleFired {
			get { return _MissleFired; }
			set { SetField(ref _MissleFired, value, () => MissleFired); }
		}

		public class AcquiringTargetDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<AcquiringTargetDefinition> _AcquiringTarget = new ItemsObservableCollection<AcquiringTargetDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("AcquiringTarget")]
		public ItemsObservableCollection<AcquiringTargetDefinition> AcquiringTarget {
			get { return _AcquiringTarget; }
			set { SetField(ref _AcquiringTarget, value, () => AcquiringTarget); }
		}

		public class TargetAcquiredDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<TargetAcquiredDefinition> _TargetAcquired = new ItemsObservableCollection<TargetAcquiredDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("TargetAcquired")]
		public ItemsObservableCollection<TargetAcquiredDefinition> TargetAcquired {
			get { return _TargetAcquired; }
			set { SetField(ref _TargetAcquired, value, () => TargetAcquired); }
		}

		public class AllClearDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<AllClearDefinition> _AllClear = new ItemsObservableCollection<AllClearDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("AllClear")]
		public ItemsObservableCollection<AllClearDefinition> AllClear {
			get { return _AllClear; }
			set { SetField(ref _AllClear, value, () => AllClear); }
		}

		public class PlaneWarningStallDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<PlaneWarningStallDefinition> _PlaneWarningStall = new ItemsObservableCollection<PlaneWarningStallDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("PlaneWarningStall")]
		public ItemsObservableCollection<PlaneWarningStallDefinition> PlaneWarningStall {
			get { return _PlaneWarningStall; }
			set { SetField(ref _PlaneWarningStall, value, () => PlaneWarningStall); }
		}

		public class AltitudeWarningLowDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

			private System.Single _DownProbeLength;
			[ModelLib.CustomAttributes.Default("80.0")]
			[ModelLib.CustomAttributes.Max(200.0)]
			[ModelLib.CustomAttributes.Min(10.0)]
			public System.Single DownProbeLength {
				get { return _DownProbeLength; }
				set { SetField(ref _DownProbeLength, value, () => DownProbeLength); }
			}

		}
		private ItemsObservableCollection<AltitudeWarningLowDefinition> _AltitudeWarningLow = new ItemsObservableCollection<AltitudeWarningLowDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("AltitudeWarningLow")]
		public ItemsObservableCollection<AltitudeWarningLowDefinition> AltitudeWarningLow {
			get { return _AltitudeWarningLow; }
			set { SetField(ref _AltitudeWarningLow, value, () => AltitudeWarningLow); }
		}

		public class AltitudeWarningHighDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<AltitudeWarningHighDefinition> _AltitudeWarningHigh = new ItemsObservableCollection<AltitudeWarningHighDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("AltitudeWarningHigh")]
		public ItemsObservableCollection<AltitudeWarningHighDefinition> AltitudeWarningHigh {
			get { return _AltitudeWarningHigh; }
			set { SetField(ref _AltitudeWarningHigh, value, () => AltitudeWarningHigh); }
		}

		public class Engine_1_FireDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<Engine_1_FireDefinition> _Engine_1_Fire = new ItemsObservableCollection<Engine_1_FireDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("Engine_1_Fire")]
		public ItemsObservableCollection<Engine_1_FireDefinition> Engine_1_Fire {
			get { return _Engine_1_Fire; }
			set { SetField(ref _Engine_1_Fire, value, () => Engine_1_Fire); }
		}

		public class Engine_2_FireDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<Engine_2_FireDefinition> _Engine_2_Fire = new ItemsObservableCollection<Engine_2_FireDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("Engine_2_Fire")]
		public ItemsObservableCollection<Engine_2_FireDefinition> Engine_2_Fire {
			get { return _Engine_2_Fire; }
			set { SetField(ref _Engine_2_Fire, value, () => Engine_2_Fire); }
		}

		public class Engine_3_FireDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<Engine_3_FireDefinition> _Engine_3_Fire = new ItemsObservableCollection<Engine_3_FireDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("Engine_3_Fire")]
		public ItemsObservableCollection<Engine_3_FireDefinition> Engine_3_Fire {
			get { return _Engine_3_Fire; }
			set { SetField(ref _Engine_3_Fire, value, () => Engine_3_Fire); }
		}

		public class Engine_4_FireDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<Engine_4_FireDefinition> _Engine_4_Fire = new ItemsObservableCollection<Engine_4_FireDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("Engine_4_Fire")]
		public ItemsObservableCollection<Engine_4_FireDefinition> Engine_4_Fire {
			get { return _Engine_4_Fire; }
			set { SetField(ref _Engine_4_Fire, value, () => Engine_4_Fire); }
		}

		public class DamagedSeriousDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<DamagedSeriousDefinition> _DamagedSerious = new ItemsObservableCollection<DamagedSeriousDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("DamagedSerious")]
		public ItemsObservableCollection<DamagedSeriousDefinition> DamagedSerious {
			get { return _DamagedSerious; }
			set { SetField(ref _DamagedSerious, value, () => DamagedSerious); }
		}

		public class DamagedCriticalDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<DamagedCriticalDefinition> _DamagedCritical = new ItemsObservableCollection<DamagedCriticalDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("DamagedCritical")]
		public ItemsObservableCollection<DamagedCriticalDefinition> DamagedCritical {
			get { return _DamagedCritical; }
			set { SetField(ref _DamagedCritical, value, () => DamagedCritical); }
		}

		public class OverspeedDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<OverspeedDefinition> _Overspeed = new ItemsObservableCollection<OverspeedDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("Overspeed")]
		public ItemsObservableCollection<OverspeedDefinition> Overspeed {
			get { return _Overspeed; }
			set { SetField(ref _Overspeed, value, () => Overspeed); }
		}

		public class TerrainDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

			private System.Single _ForwardProbeLength;
			[ModelLib.CustomAttributes.Default("100.0")]
			[ModelLib.CustomAttributes.Max(300.0)]
			[ModelLib.CustomAttributes.Min(10.0)]
			public System.Single ForwardProbeLength {
				get { return _ForwardProbeLength; }
				set { SetField(ref _ForwardProbeLength, value, () => ForwardProbeLength); }
			}

		}
		private ItemsObservableCollection<TerrainDefinition> _Terrain = new ItemsObservableCollection<TerrainDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("Terrain")]
		public ItemsObservableCollection<TerrainDefinition> Terrain {
			get { return _Terrain; }
			set { SetField(ref _Terrain, value, () => Terrain); }
		}

		public class PullUpDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

			private System.Int32 _MaxTimeSinceTerrainTriggerToPlay;
			[ModelLib.CustomAttributes.Default("10000")]
			[ModelLib.CustomAttributes.Max(60000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeSinceTerrainTriggerToPlay {
				get { return _MaxTimeSinceTerrainTriggerToPlay; }
				set { SetField(ref _MaxTimeSinceTerrainTriggerToPlay, value, () => MaxTimeSinceTerrainTriggerToPlay); }
			}

		}
		private ItemsObservableCollection<PullUpDefinition> _PullUp = new ItemsObservableCollection<PullUpDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("PullUp")]
		public ItemsObservableCollection<PullUpDefinition> PullUp {
			get { return _PullUp; }
			set { SetField(ref _PullUp, value, () => PullUp); }
		}

		public class LowFuelDefinition: ModelBase {
			private System.Int32 _MinTimeInStateToTrigger;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeInStateToTrigger {
				get { return _MinTimeInStateToTrigger; }
				set { SetField(ref _MinTimeInStateToTrigger, value, () => MinTimeInStateToTrigger); }
			}

			private System.Int32 _MaxTimeBetweenTriggerAndPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MaxTimeBetweenTriggerAndPlay {
				get { return _MaxTimeBetweenTriggerAndPlay; }
				set { SetField(ref _MaxTimeBetweenTriggerAndPlay, value, () => MaxTimeBetweenTriggerAndPlay); }
			}

			private System.Int32 _MinTimeBetweenPlay;
			[ModelLib.CustomAttributes.Default("5000")]
			[ModelLib.CustomAttributes.Max(500000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Int32 MinTimeBetweenPlay {
				get { return _MinTimeBetweenPlay; }
				set { SetField(ref _MinTimeBetweenPlay, value, () => MinTimeBetweenPlay); }
			}

		}
		private ItemsObservableCollection<LowFuelDefinition> _LowFuel = new ItemsObservableCollection<LowFuelDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("LowFuel")]
		public ItemsObservableCollection<LowFuelDefinition> LowFuel {
			get { return _LowFuel; }
			set { SetField(ref _LowFuel, value, () => LowFuel); }
		}

	}
}


