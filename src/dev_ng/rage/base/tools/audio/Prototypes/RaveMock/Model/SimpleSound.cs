using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class SimpleSound:  Sound{

		private System.Byte _WaveSlotIndex;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Byte WaveSlotIndex {
			get { return _WaveSlotIndex; }
			set { SetField(ref _WaveSlotIndex, value, () => WaveSlotIndex); }
		}

		private System.UInt32 _WaveSlotPtr;
		[ModelLib.CustomAttributes.Ignore]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 WaveSlotPtr {
			get { return _WaveSlotPtr; }
			set { SetField(ref _WaveSlotPtr, value, () => WaveSlotPtr); }
		}

		public class WaveRefDefinition: ModelBase {
			private System.String _BankName;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			[ModelLib.CustomAttributes.Unit("stringtableindex")]
			public System.String BankName {
				get { return _BankName; }
				set { SetField(ref _BankName, value, () => BankName); }
			}

			private System.String _WaveName;
			public System.String WaveName {
				get { return _WaveName; }
				set { SetField(ref _WaveName, value, () => WaveName); }
			}

		}
		private WaveRefDefinition _WaveRef;
		[ModelLib.CustomAttributes.Unit("WaveRef")]
		public WaveRefDefinition WaveRef {
			get { return _WaveRef; }
			set { SetField(ref _WaveRef, value, () => WaveRef); }
		}

	}
}


