using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Physics")]
	public class DoorAudioSettings:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private ModelLib.ObjectRef _Sounds;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Sounds {
			get { return _Sounds; }
			set { SetField(ref _Sounds, value, () => Sounds); }
		}

		private ModelLib.ObjectRef _TuningParams;
		[ModelLib.CustomAttributes.AllowedType(typeof(DoorTuningParams))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef TuningParams {
			get { return _TuningParams; }
			set { SetField(ref _TuningParams, value, () => TuningParams); }
		}

		private System.Single _MaxOcclusion;
		[ModelLib.CustomAttributes.Default("0.7")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single MaxOcclusion {
			get { return _MaxOcclusion; }
			set { SetField(ref _MaxOcclusion, value, () => MaxOcclusion); }
		}

	}
}


