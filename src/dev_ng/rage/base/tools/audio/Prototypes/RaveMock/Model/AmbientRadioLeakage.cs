using ModelLib.CustomAttributes;

namespace model {

	public enum AmbientRadioLeakage {
		[ModelLib.CustomAttributes.Display("Bassy/Loud")]
		LEAKAGE_BASSY_LOUD,
		[ModelLib.CustomAttributes.Display("Bassy/Medium")]
		LEAKAGE_BASSY_MEDIUM,
		[ModelLib.CustomAttributes.Display("Mids/Loud")]
		LEAKAGE_MIDS_LOUD,
		[ModelLib.CustomAttributes.Display("Mids/Medium")]
		LEAKAGE_MIDS_MEDIUM,
		[ModelLib.CustomAttributes.Display("Mids/Quiet")]
		LEAKAGE_MIDS_QUIET,
		[ModelLib.CustomAttributes.Display("Deafening")]
		LEAKAGE_CRAZY_LOUD
	}

}