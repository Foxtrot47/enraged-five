using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class CollapsingStereoSound:  Sound{

		private ModelLib.ObjectRef _LeftSoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef LeftSoundRef {
			get { return _LeftSoundRef; }
			set { SetField(ref _LeftSoundRef, value, () => LeftSoundRef); }
		}

		private ModelLib.ObjectRef _RightSoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef RightSoundRef {
			get { return _RightSoundRef; }
			set { SetField(ref _RightSoundRef, value, () => RightSoundRef); }
		}

		private System.Single _MinDistance;
		[ModelLib.CustomAttributes.Default("-1.0")]
		public System.Single MinDistance {
			get { return _MinDistance; }
			set { SetField(ref _MinDistance, value, () => MinDistance); }
		}

		private System.Single _MaxDistance;
		[ModelLib.CustomAttributes.Default("-1.0")]
		public System.Single MaxDistance {
			get { return _MaxDistance; }
			set { SetField(ref _MaxDistance, value, () => MaxDistance); }
		}

		private System.String _MinDistanceVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String MinDistanceVariable {
			get { return _MinDistanceVariable; }
			set { SetField(ref _MinDistanceVariable, value, () => MinDistanceVariable); }
		}

		private System.String _MaxDistanceVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String MaxDistanceVariable {
			get { return _MaxDistanceVariable; }
			set { SetField(ref _MaxDistanceVariable, value, () => MaxDistanceVariable); }
		}

		private System.String _CrossfadeOverrideVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String CrossfadeOverrideVariable {
			get { return _CrossfadeOverrideVariable; }
			set { SetField(ref _CrossfadeOverrideVariable, value, () => CrossfadeOverrideVariable); }
		}

		private System.String _FrontendLeftPan;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String FrontendLeftPan {
			get { return _FrontendLeftPan; }
			set { SetField(ref _FrontendLeftPan, value, () => FrontendLeftPan); }
		}

		private System.String _FrontendRightPan;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String FrontendRightPan {
			get { return _FrontendRightPan; }
			set { SetField(ref _FrontendRightPan, value, () => FrontendRightPan); }
		}

		private System.Single _PositionRelativePanDamping;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single PositionRelativePanDamping {
			get { return _PositionRelativePanDamping; }
			set { SetField(ref _PositionRelativePanDamping, value, () => PositionRelativePanDamping); }
		}

		private System.String _PositionRelativePanDampingVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String PositionRelativePanDampingVariable {
			get { return _PositionRelativePanDampingVariable; }
			set { SetField(ref _PositionRelativePanDampingVariable, value, () => PositionRelativePanDampingVariable); }
		}

		private CrossfadeMode _Mode;
		[ModelLib.CustomAttributes.Default("CROSSFADE_MODE_BOTH")]
		public CrossfadeMode Mode {
			get { return _Mode; }
			set { SetField(ref _Mode, value, () => Mode); }
		}

	}
}


