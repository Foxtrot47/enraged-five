using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.FactoryCodeGenerated(false)]
	[ModelLib.CustomAttributes.Group("Util")]
	public class SoundHashList:  ModelBase {

		private System.UInt16 _CurrentSoundIdx;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(65535)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt16 CurrentSoundIdx {
			get { return _CurrentSoundIdx; }
			set { SetField(ref _CurrentSoundIdx, value, () => CurrentSoundIdx); }
		}

		public class SoundHashesDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundHash;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef SoundHash {
				get { return _SoundHash; }
				set { SetField(ref _SoundHash, value, () => SoundHash); }
			}

		}
		private ItemsObservableCollection<SoundHashesDefinition> _SoundHashes = new ItemsObservableCollection<SoundHashesDefinition>();
		[ModelLib.CustomAttributes.MaxSize(65535, typeof(System.UInt32))]
		[XmlElement("SoundHashes")]
		public ItemsObservableCollection<SoundHashesDefinition> SoundHashes {
			get { return _SoundHashes; }
			set { SetField(ref _SoundHashes, value, () => SoundHashes); }
		}

	}
}


