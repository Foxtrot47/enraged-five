using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class GranularEngineSet:  ModelBase {

		public class GranularEnginesDefinition: ModelBase {
			private ModelLib.ObjectRef _GranularEngine;
			[ModelLib.CustomAttributes.AllowedType(typeof(GranularEngineAudioSettings))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef GranularEngine {
				get { return _GranularEngine; }
				set { SetField(ref _GranularEngine, value, () => GranularEngine); }
			}

			private System.Single _Weight;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single Weight {
				get { return _Weight; }
				set { SetField(ref _Weight, value, () => Weight); }
			}

		}
		private ItemsObservableCollection<GranularEnginesDefinition> _GranularEngines = new ItemsObservableCollection<GranularEnginesDefinition>();
		[ModelLib.CustomAttributes.MaxSize(10)]
		[XmlElement("GranularEngines")]
		public ItemsObservableCollection<GranularEnginesDefinition> GranularEngines {
			get { return _GranularEngines; }
			set { SetField(ref _GranularEngines, value, () => GranularEngines); }
		}

	}
}


