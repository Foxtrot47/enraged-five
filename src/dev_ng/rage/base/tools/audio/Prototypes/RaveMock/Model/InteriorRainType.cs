using ModelLib.CustomAttributes;

namespace model {

	public enum InteriorRainType {
		[ModelLib.CustomAttributes.Display("None")]
		RAIN_TYPE_NONE,
		[ModelLib.CustomAttributes.Display("CorrugatedIron")]
		RAIN_TYPE_CORRUGATED_IRON,
		[ModelLib.CustomAttributes.Display("Plastic")]
		RAIN_TYPE_PLASTIC
	}

}