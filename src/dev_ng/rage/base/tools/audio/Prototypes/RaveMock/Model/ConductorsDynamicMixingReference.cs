using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Conductors")]
	public class ConductorsDynamicMixingReference:  ModelBase {

		public class ReferenceDefinition: ModelBase {
			private System.String _Type;
			public System.String Type {
				get { return _Type; }
				set { SetField(ref _Type, value, () => Type); }
			}

			private ModelLib.ObjectRef _Scene;
			[ModelLib.CustomAttributes.AllowedType(typeof(MixerScene))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Scene {
				get { return _Scene; }
				set { SetField(ref _Scene, value, () => Scene); }
			}

			private ModelLib.ObjectRef _MixGroup;
			[ModelLib.CustomAttributes.AllowedType(typeof(MixGroup))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef MixGroup {
				get { return _MixGroup; }
				set { SetField(ref _MixGroup, value, () => MixGroup); }
			}

		}
		private ItemsObservableCollection<ReferenceDefinition> _Reference = new ItemsObservableCollection<ReferenceDefinition>();
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("Reference")]
		public ItemsObservableCollection<ReferenceDefinition> Reference {
			get { return _Reference; }
			set { SetField(ref _Reference, value, () => Reference); }
		}

	}
}


