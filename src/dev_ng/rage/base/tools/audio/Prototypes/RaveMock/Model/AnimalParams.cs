using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Packed(true)]
	public class AnimalParams:  ModelBase {

		private System.Int32 _BasePriority;
		[ModelLib.CustomAttributes.Default("5")]
		[ModelLib.CustomAttributes.Max(10)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 BasePriority {
			get { return _BasePriority; }
			set { SetField(ref _BasePriority, value, () => BasePriority); }
		}

		private System.Single _MinFarDistance;
		[ModelLib.CustomAttributes.Default("30.0")]
		[ModelLib.CustomAttributes.Max(200.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single MinFarDistance {
			get { return _MinFarDistance; }
			set { SetField(ref _MinFarDistance, value, () => MinFarDistance); }
		}

		private System.Single _MaxDistanceToBankLoad;
		[ModelLib.CustomAttributes.Default("60.0")]
		[ModelLib.CustomAttributes.Max(1000.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single MaxDistanceToBankLoad {
			get { return _MaxDistanceToBankLoad; }
			set { SetField(ref _MaxDistanceToBankLoad, value, () => MaxDistanceToBankLoad); }
		}

		private System.UInt32 _MaxSoundInstances;
		[ModelLib.CustomAttributes.Default("4")]
		[ModelLib.CustomAttributes.Max(32)]
		[ModelLib.CustomAttributes.Min(1)]
		public System.UInt32 MaxSoundInstances {
			get { return _MaxSoundInstances; }
			set { SetField(ref _MaxSoundInstances, value, () => MaxSoundInstances); }
		}

		private AnimalType _Type;
		[ModelLib.CustomAttributes.Default("kAnimalNone")]
		public AnimalType Type {
			get { return _Type; }
			set { SetField(ref _Type, value, () => Type); }
		}

		private System.String _ScrapeVolCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_ANIMAL_SCRAPE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		public System.String ScrapeVolCurve {
			get { return _ScrapeVolCurve; }
			set { SetField(ref _ScrapeVolCurve, value, () => ScrapeVolCurve); }
		}

		private System.String _ScrapePitchCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_ANIMAL_SCRAPE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		public System.String ScrapePitchCurve {
			get { return _ScrapePitchCurve; }
			set { SetField(ref _ScrapePitchCurve, value, () => ScrapePitchCurve); }
		}

		private System.String _BigVehicleImpact;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		public System.String BigVehicleImpact {
			get { return _BigVehicleImpact; }
			set { SetField(ref _BigVehicleImpact, value, () => BigVehicleImpact); }
		}

		private System.Single _VehicleSpeedForBigImpact;
		[ModelLib.CustomAttributes.Default("5.0")]
		public System.Single VehicleSpeedForBigImpact {
			get { return _VehicleSpeedForBigImpact; }
			set { SetField(ref _VehicleSpeedForBigImpact, value, () => VehicleSpeedForBigImpact); }
		}

		private System.String _RunOverSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		public System.String RunOverSound {
			get { return _RunOverSound; }
			set { SetField(ref _RunOverSound, value, () => RunOverSound); }
		}

		public class ContextDefinition: ModelBase {
			private System.String _ContextName;
			[ModelLib.CustomAttributes.Length(32)]
			public System.String ContextName {
				get { return _ContextName; }
				set { SetField(ref _ContextName, value, () => ContextName); }
			}

			private System.Single _VolumeOffset;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(20.0)]
			[ModelLib.CustomAttributes.Min(-100.0)]
			public System.Single VolumeOffset {
				get { return _VolumeOffset; }
				set { SetField(ref _VolumeOffset, value, () => VolumeOffset); }
			}

			private System.Single _RollOff;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Max(100.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single RollOff {
				get { return _RollOff; }
				set { SetField(ref _RollOff, value, () => RollOff); }
			}

			private System.UInt32 _Priority;
			[ModelLib.CustomAttributes.Default("3")]
			[ModelLib.CustomAttributes.Max(5)]
			[ModelLib.CustomAttributes.Min(1)]
			public System.UInt32 Priority {
				get { return _Priority; }
				set { SetField(ref _Priority, value, () => Priority); }
			}

			private System.Single _Probability;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Max(1.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single Probability {
				get { return _Probability; }
				set { SetField(ref _Probability, value, () => Probability); }
			}

			private System.Int32 _MinTimeBetween;
			[ModelLib.CustomAttributes.Default("-1")]
			[ModelLib.CustomAttributes.Max(20000)]
			[ModelLib.CustomAttributes.Min(-1)]
			public System.Int32 MinTimeBetween {
				get { return _MinTimeBetween; }
				set { SetField(ref _MinTimeBetween, value, () => MinTimeBetween); }
			}

			private System.Int32 _MaxTimeBetween;
			[ModelLib.CustomAttributes.Default("-1")]
			[ModelLib.CustomAttributes.Max(20000)]
			[ModelLib.CustomAttributes.Min(-1)]
			public System.Int32 MaxTimeBetween {
				get { return _MaxTimeBetween; }
				set { SetField(ref _MaxTimeBetween, value, () => MaxTimeBetween); }
			}

			private System.Single _MaxDistance;
			[ModelLib.CustomAttributes.Default("40.0")]
			[ModelLib.CustomAttributes.Max(200.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single MaxDistance {
				get { return _MaxDistance; }
				set { SetField(ref _MaxDistance, value, () => MaxDistance); }
			}

			public class ContextBitsDefinition: ModelBase {
				private ModelLib.Types.Bit _SwapWhenUsedUp;
				[ModelLib.CustomAttributes.Default("no")]
				public ModelLib.Types.Bit SwapWhenUsedUp {
					get { return _SwapWhenUsedUp; }
					set { SetField(ref _SwapWhenUsedUp, value, () => SwapWhenUsedUp); }
				}

				private ModelLib.Types.Bit _AllowOverlap;
				[ModelLib.CustomAttributes.Default("no")]
				public ModelLib.Types.Bit AllowOverlap {
					get { return _AllowOverlap; }
					set { SetField(ref _AllowOverlap, value, () => AllowOverlap); }
				}

			}
			private ContextBitsDefinition _ContextBits;
			public ContextBitsDefinition ContextBits {
				get { return _ContextBits; }
				set { SetField(ref _ContextBits, value, () => ContextBits); }
			}

		}
		private ItemsObservableCollection<ContextDefinition> _Context = new ItemsObservableCollection<ContextDefinition>();
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("Context")]
		public ItemsObservableCollection<ContextDefinition> Context {
			get { return _Context; }
			set { SetField(ref _Context, value, () => Context); }
		}

	}
}


