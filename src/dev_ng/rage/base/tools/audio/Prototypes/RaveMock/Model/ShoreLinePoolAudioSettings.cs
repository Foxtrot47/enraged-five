using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Transformer("{Type=\"rage.ShorelineCompiler.ShorelineTransformer, ShorelineCompiler, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\"}")]
	[ModelLib.CustomAttributes.Group("Ambience")]
	public class ShoreLinePoolAudioSettings:  ShoreLineAudioSettings{

		private System.Int32 _WaterLappingMinDelay;
		[ModelLib.CustomAttributes.Default("-500")]
		[ModelLib.CustomAttributes.Max(0)]
		[ModelLib.CustomAttributes.Min(-5000)]
		public System.Int32 WaterLappingMinDelay {
			get { return _WaterLappingMinDelay; }
			set { SetField(ref _WaterLappingMinDelay, value, () => WaterLappingMinDelay); }
		}

		private System.Int32 _WaterLappingMaxDelay;
		[ModelLib.CustomAttributes.Default("500")]
		[ModelLib.CustomAttributes.Max(5000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 WaterLappingMaxDelay {
			get { return _WaterLappingMaxDelay; }
			set { SetField(ref _WaterLappingMaxDelay, value, () => WaterLappingMaxDelay); }
		}

		private System.Int32 _WaterSplashMinDelay;
		[ModelLib.CustomAttributes.Default("5000")]
		[ModelLib.CustomAttributes.Max(30000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 WaterSplashMinDelay {
			get { return _WaterSplashMinDelay; }
			set { SetField(ref _WaterSplashMinDelay, value, () => WaterSplashMinDelay); }
		}

		private System.Int32 _WaterSplashMaxDelay;
		[ModelLib.CustomAttributes.Default("14000")]
		[ModelLib.CustomAttributes.Max(30000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 WaterSplashMaxDelay {
			get { return _WaterSplashMaxDelay; }
			set { SetField(ref _WaterSplashMaxDelay, value, () => WaterSplashMaxDelay); }
		}

		private System.Int32 _FirstQuadIndex;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.DisplayGroup("Definition")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Int32 FirstQuadIndex {
			get { return _FirstQuadIndex; }
			set { SetField(ref _FirstQuadIndex, value, () => FirstQuadIndex); }
		}

		private System.Int32 _SecondQuadIndex;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.DisplayGroup("Definition")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Int32 SecondQuadIndex {
			get { return _SecondQuadIndex; }
			set { SetField(ref _SecondQuadIndex, value, () => SecondQuadIndex); }
		}

		private System.Int32 _ThirdQuadIndex;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.DisplayGroup("Definition")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Int32 ThirdQuadIndex {
			get { return _ThirdQuadIndex; }
			set { SetField(ref _ThirdQuadIndex, value, () => ThirdQuadIndex); }
		}

		private System.Int32 _FourthQuadIndex;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.DisplayGroup("Definition")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Int32 FourthQuadIndex {
			get { return _FourthQuadIndex; }
			set { SetField(ref _FourthQuadIndex, value, () => FourthQuadIndex); }
		}

		private System.Single _SmallestDistanceToPoint;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1000000.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single SmallestDistanceToPoint {
			get { return _SmallestDistanceToPoint; }
			set { SetField(ref _SmallestDistanceToPoint, value, () => SmallestDistanceToPoint); }
		}

		private ModelLib.Types.TriState _TreatAsLake;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState TreatAsLake {
			get { return _TreatAsLake; }
			set { SetField(ref _TreatAsLake, value, () => TreatAsLake); }
		}

		public class ShoreLinePointsDefinition: ModelBase {
			private System.Single _X;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single X {
				get { return _X; }
				set { SetField(ref _X, value, () => X); }
			}

			private System.Single _Y;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single Y {
				get { return _Y; }
				set { SetField(ref _Y, value, () => Y); }
			}

		}
		private ItemsObservableCollection<ShoreLinePointsDefinition> _ShoreLinePoints = new ItemsObservableCollection<ShoreLinePointsDefinition>();
		[ModelLib.CustomAttributes.DisplayGroup("Definition")]
		[ModelLib.CustomAttributes.MaxSize(1000)]
		[XmlElement("ShoreLinePoints")]
		public ItemsObservableCollection<ShoreLinePointsDefinition> ShoreLinePoints {
			get { return _ShoreLinePoints; }
			set { SetField(ref _ShoreLinePoints, value, () => ShoreLinePoints); }
		}

	}
}


