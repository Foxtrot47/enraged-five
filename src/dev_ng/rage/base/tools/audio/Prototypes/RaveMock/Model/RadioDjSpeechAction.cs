using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class RadioDjSpeechAction:  MusicAction{

		private ModelLib.ObjectRef _Station;
		[ModelLib.CustomAttributes.AllowedType(typeof(RadioStationSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Station {
			get { return _Station; }
			set { SetField(ref _Station, value, () => Station); }
		}

		private RadioDjSpeechCategories _Category;
		[ModelLib.CustomAttributes.Default("kDjSpeechGeneral")]
		public RadioDjSpeechCategories Category {
			get { return _Category; }
			set { SetField(ref _Category, value, () => Category); }
		}

	}
}


