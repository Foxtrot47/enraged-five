using ModelLib.CustomAttributes;

namespace model {

	public enum SoundTimer {
		[ModelLib.CustomAttributes.Display("Normal Timer")]
		kSoundTimerNormal,
		[ModelLib.CustomAttributes.Display("Unpausable Timer")]
		kSoundTimerUnpausable,
		[ModelLib.CustomAttributes.Display("Radio Timer")]
		kSoundTimerRadio,
		[ModelLib.CustomAttributes.Display("Music Timer")]
		kSoundTimerMusic,
		[ModelLib.CustomAttributes.Display("Unspecified")]
		kSoundTimerUnspecified,
		[ModelLib.CustomAttributes.Display("Scripted Speech")]
		kSoundTimerScriptedSpeech,
		[ModelLib.CustomAttributes.Display("Slow-mo")]
		kSoundTimerSlowMo
	}

}