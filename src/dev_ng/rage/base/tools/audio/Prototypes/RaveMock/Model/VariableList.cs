using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class VariableList:  ModelBase {

		private System.String _DisplayName;
		[ModelLib.CustomAttributes.Ignore]
		public System.String DisplayName {
			get { return _DisplayName; }
			set { SetField(ref _DisplayName, value, () => DisplayName); }
		}

		public class VariableDefinition: ModelBase {
			private System.String _Name;
			public System.String Name {
				get { return _Name; }
				set { SetField(ref _Name, value, () => Name); }
			}

			private System.Single _InitialValue;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single InitialValue {
				get { return _InitialValue; }
				set { SetField(ref _InitialValue, value, () => InitialValue); }
			}

			private System.String _Description;
			[ModelLib.CustomAttributes.Ignore]
			public System.String Description {
				get { return _Description; }
				set { SetField(ref _Description, value, () => Description); }
			}

			private ModelLib.Types.Bit _ValidAsOutput;
			[ModelLib.CustomAttributes.Default("no")]
			[ModelLib.CustomAttributes.Ignore]
			public ModelLib.Types.Bit ValidAsOutput {
				get { return _ValidAsOutput; }
				set { SetField(ref _ValidAsOutput, value, () => ValidAsOutput); }
			}

		}
		private ItemsObservableCollection<VariableDefinition> _Variable = new ItemsObservableCollection<VariableDefinition>();
		[ModelLib.CustomAttributes.MaxSize(255, typeof(System.UInt32))]
		[XmlElement("Variable")]
		public ItemsObservableCollection<VariableDefinition> Variable {
			get { return _Variable; }
			set { SetField(ref _Variable, value, () => Variable); }
		}

	}
}


