using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Environment")]
	public class EnvironmentRule:  AudBaseObject{

		private System.String _Notes;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Notes {
			get { return _Notes; }
			set { SetField(ref _Notes, value, () => Notes); }
		}

		private ModelLib.Types.TriState _OverrideReverb;
		public ModelLib.Types.TriState OverrideReverb {
			get { return _OverrideReverb; }
			set { SetField(ref _OverrideReverb, value, () => OverrideReverb); }
		}

		private ModelLib.Types.TriState _OverrideEchos;
		public ModelLib.Types.TriState OverrideEchos {
			get { return _OverrideEchos; }
			set { SetField(ref _OverrideEchos, value, () => OverrideEchos); }
		}

		private ModelLib.Types.TriState _RandomEchoPositions;
		public ModelLib.Types.TriState RandomEchoPositions {
			get { return _RandomEchoPositions; }
			set { SetField(ref _RandomEchoPositions, value, () => RandomEchoPositions); }
		}

		private ModelLib.Types.TriState _HeightRestrictions;
		public ModelLib.Types.TriState HeightRestrictions {
			get { return _HeightRestrictions; }
			set { SetField(ref _HeightRestrictions, value, () => HeightRestrictions); }
		}

		private System.Single _ReverbSmall;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1)]
		[ModelLib.CustomAttributes.Min(-1)]
		public System.Single ReverbSmall {
			get { return _ReverbSmall; }
			set { SetField(ref _ReverbSmall, value, () => ReverbSmall); }
		}

		private System.Single _ReverbMedium;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1)]
		[ModelLib.CustomAttributes.Min(-1)]
		public System.Single ReverbMedium {
			get { return _ReverbMedium; }
			set { SetField(ref _ReverbMedium, value, () => ReverbMedium); }
		}

		private System.Single _ReverbLarge;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1)]
		[ModelLib.CustomAttributes.Min(-1)]
		public System.Single ReverbLarge {
			get { return _ReverbLarge; }
			set { SetField(ref _ReverbLarge, value, () => ReverbLarge); }
		}

		private System.Single _ReverbDamp;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1)]
		[ModelLib.CustomAttributes.Min(-1)]
		public System.Single ReverbDamp {
			get { return _ReverbDamp; }
			set { SetField(ref _ReverbDamp, value, () => ReverbDamp); }
		}

		private System.Single _EchoDelay;
		[ModelLib.CustomAttributes.Default("250")]
		[ModelLib.CustomAttributes.Max(2000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single EchoDelay {
			get { return _EchoDelay; }
			set { SetField(ref _EchoDelay, value, () => EchoDelay); }
		}

		private System.Single _EchoDelayVariance;
		[ModelLib.CustomAttributes.Default("75")]
		[ModelLib.CustomAttributes.Max(500)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single EchoDelayVariance {
			get { return _EchoDelayVariance; }
			set { SetField(ref _EchoDelayVariance, value, () => EchoDelayVariance); }
		}

		private System.Single _EchoAttenuation;
		[ModelLib.CustomAttributes.Default("-100")]
		[ModelLib.CustomAttributes.Max(40)]
		[ModelLib.CustomAttributes.Min(-100)]
		public System.Single EchoAttenuation {
			get { return _EchoAttenuation; }
			set { SetField(ref _EchoAttenuation, value, () => EchoAttenuation); }
		}

		private System.Byte _EchoNumber;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Byte EchoNumber {
			get { return _EchoNumber; }
			set { SetField(ref _EchoNumber, value, () => EchoNumber); }
		}

		private ModelLib.ObjectRef _EchoSoundList;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EchoSoundList {
			get { return _EchoSoundList; }
			set { SetField(ref _EchoSoundList, value, () => EchoSoundList); }
		}

		private System.Single _BaseEchoVolumeModifier;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(40)]
		[ModelLib.CustomAttributes.Min(-100)]
		public System.Single BaseEchoVolumeModifier {
			get { return _BaseEchoVolumeModifier; }
			set { SetField(ref _BaseEchoVolumeModifier, value, () => BaseEchoVolumeModifier); }
		}

	}
}


