using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class Slot:  ModelBase {

		private SlotLoadType _LoadType;
		[ModelLib.CustomAttributes.Default("SLOT_LOAD_TYPE_BANK")]
		public SlotLoadType LoadType {
			get { return _LoadType; }
			set { SetField(ref _LoadType, value, () => LoadType); }
		}

		private System.Byte _padding0;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Byte padding0 {
			get { return _padding0; }
			set { SetField(ref _padding0, value, () => padding0); }
		}

		private System.Byte _padding1;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Byte padding1 {
			get { return _padding1; }
			set { SetField(ref _padding1, value, () => padding1); }
		}

		private System.Byte _padding2;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Byte padding2 {
			get { return _padding2; }
			set { SetField(ref _padding2, value, () => padding2); }
		}

		private System.UInt32 _MaxHeaderSize;
		[ModelLib.CustomAttributes.Default("0")]
		public System.UInt32 MaxHeaderSize {
			get { return _MaxHeaderSize; }
			set { SetField(ref _MaxHeaderSize, value, () => MaxHeaderSize); }
		}

		private System.UInt32 _Size;
		[ModelLib.CustomAttributes.Default("0")]
		public System.UInt32 Size {
			get { return _Size; }
			set { SetField(ref _Size, value, () => Size); }
		}

		private System.String _StaticBank;
		[ModelLib.CustomAttributes.Default("0")]
		public System.String StaticBank {
			get { return _StaticBank; }
			set { SetField(ref _StaticBank, value, () => StaticBank); }
		}

		private System.UInt32 _MaxMetadataSize;
		[ModelLib.CustomAttributes.Default("0")]
		public System.UInt32 MaxMetadataSize {
			get { return _MaxMetadataSize; }
			set { SetField(ref _MaxMetadataSize, value, () => MaxMetadataSize); }
		}

		private System.UInt32 _MaxDataSize;
		[ModelLib.CustomAttributes.Default("0")]
		public System.UInt32 MaxDataSize {
			get { return _MaxDataSize; }
			set { SetField(ref _MaxDataSize, value, () => MaxDataSize); }
		}

	}
}


