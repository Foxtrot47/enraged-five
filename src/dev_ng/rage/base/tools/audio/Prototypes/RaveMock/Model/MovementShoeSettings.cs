using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class MovementShoeSettings:  ModelBase {

		private ModelLib.ObjectRef _HighHeels;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef HighHeels {
			get { return _HighHeels; }
			set { SetField(ref _HighHeels, value, () => HighHeels); }
		}

		private ModelLib.ObjectRef _Leather;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef Leather {
			get { return _Leather; }
			set { SetField(ref _Leather, value, () => Leather); }
		}

		private ModelLib.ObjectRef _RubberHard;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef RubberHard {
			get { return _RubberHard; }
			set { SetField(ref _RubberHard, value, () => RubberHard); }
		}

		private ModelLib.ObjectRef _RubberSoft;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef RubberSoft {
			get { return _RubberSoft; }
			set { SetField(ref _RubberSoft, value, () => RubberSoft); }
		}

		private ModelLib.ObjectRef _Bare;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef Bare {
			get { return _Bare; }
			set { SetField(ref _Bare, value, () => Bare); }
		}

		private ModelLib.ObjectRef _HeavyBoots;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef HeavyBoots {
			get { return _HeavyBoots; }
			set { SetField(ref _HeavyBoots, value, () => HeavyBoots); }
		}

		private ModelLib.ObjectRef _FlipFlops;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef FlipFlops {
			get { return _FlipFlops; }
			set { SetField(ref _FlipFlops, value, () => FlipFlops); }
		}

		private FootstepLoudness _AudioEventLoudness;
		[ModelLib.CustomAttributes.Default("FOOTSTEP_LOUDNESS_MEDIUM")]
		public FootstepLoudness AudioEventLoudness {
			get { return _AudioEventLoudness; }
			set { SetField(ref _AudioEventLoudness, value, () => AudioEventLoudness); }
		}

	}
}


