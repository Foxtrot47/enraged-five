using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Radio")]
	public class RadioTrackSettings:  ModelBase {

		private ModelLib.ObjectRef _Sound;
		[ModelLib.CustomAttributes.AllowedType(typeof(StreamingSound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Sound {
			get { return _Sound; }
			set { SetField(ref _Sound, value, () => Sound); }
		}

		private System.Single _StartOffset;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(0.8)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single StartOffset {
			get { return _StartOffset; }
			set { SetField(ref _StartOffset, value, () => StartOffset); }
		}

		public class HistoryDefinition: ModelBase {
			private TrackCats _Category;
			[ModelLib.CustomAttributes.Default("RADIO_TRACK_CAT_MUSIC")]
			public TrackCats Category {
				get { return _Category; }
				set { SetField(ref _Category, value, () => Category); }
			}

			private ModelLib.ObjectRef _Sound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Sound {
				get { return _Sound; }
				set { SetField(ref _Sound, value, () => Sound); }
			}

		}
		private HistoryDefinition _History;
		public HistoryDefinition History {
			get { return _History; }
			set { SetField(ref _History, value, () => History); }
		}

	}
}


