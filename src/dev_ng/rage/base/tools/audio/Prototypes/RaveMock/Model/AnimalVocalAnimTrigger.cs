using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("AnimTriggers")]
	[ModelLib.CustomAttributes.Packed(true)]
	public class AnimalVocalAnimTrigger:  ModelBase {

		public class AnimTriggersDefinition: ModelBase {
			private AnimalType _Animal;
			[ModelLib.CustomAttributes.Default("kAnimalNone")]
			public AnimalType Animal {
				get { return _Animal; }
				set { SetField(ref _Animal, value, () => Animal); }
			}

			public class AngryContextsDefinition: ModelBase {
				private System.String _Context;
				public System.String Context {
					get { return _Context; }
					set { SetField(ref _Context, value, () => Context); }
				}

				private System.Single _Weight;
				[ModelLib.CustomAttributes.Default("1.0")]
				[ModelLib.CustomAttributes.Max(10.0)]
				[ModelLib.CustomAttributes.Min(0.0)]
				public System.Single Weight {
					get { return _Weight; }
					set { SetField(ref _Weight, value, () => Weight); }
				}

			}
			private ItemsObservableCollection<AngryContextsDefinition> _AngryContexts = new ItemsObservableCollection<AngryContextsDefinition>();
			[ModelLib.CustomAttributes.FixedSize]
			[ModelLib.CustomAttributes.MaxSize(8)]
			[XmlElement("AngryContexts")]
			public ItemsObservableCollection<AngryContextsDefinition> AngryContexts {
				get { return _AngryContexts; }
				set { SetField(ref _AngryContexts, value, () => AngryContexts); }
			}

			public class PlayfulContextsDefinition: ModelBase {
				private System.String _Context;
				public System.String Context {
					get { return _Context; }
					set { SetField(ref _Context, value, () => Context); }
				}

				private System.Single _Weight;
				[ModelLib.CustomAttributes.Default("1.0")]
				[ModelLib.CustomAttributes.Max(10.0)]
				[ModelLib.CustomAttributes.Min(0.0)]
				public System.Single Weight {
					get { return _Weight; }
					set { SetField(ref _Weight, value, () => Weight); }
				}

			}
			private ItemsObservableCollection<PlayfulContextsDefinition> _PlayfulContexts = new ItemsObservableCollection<PlayfulContextsDefinition>();
			[ModelLib.CustomAttributes.FixedSize]
			[ModelLib.CustomAttributes.MaxSize(8)]
			[XmlElement("PlayfulContexts")]
			public ItemsObservableCollection<PlayfulContextsDefinition> PlayfulContexts {
				get { return _PlayfulContexts; }
				set { SetField(ref _PlayfulContexts, value, () => PlayfulContexts); }
			}

		}
		private ItemsObservableCollection<AnimTriggersDefinition> _AnimTriggers = new ItemsObservableCollection<AnimTriggersDefinition>();
		[ModelLib.CustomAttributes.DisplayGroup("AnimTriggers")]
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("AnimTriggers")]
		public ItemsObservableCollection<AnimTriggersDefinition> AnimTriggers {
			get { return _AnimTriggers; }
			set { SetField(ref _AnimTriggers, value, () => AnimTriggers); }
		}

	}
}


