using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class SportsCarRevsSettings:  ModelBase {

		private System.Int32 _EngineVolumeBoost;
		[ModelLib.CustomAttributes.Default("200")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 EngineVolumeBoost {
			get { return _EngineVolumeBoost; }
			set { SetField(ref _EngineVolumeBoost, value, () => EngineVolumeBoost); }
		}

		private System.Int32 _ExhaustVolumeBoost;
		[ModelLib.CustomAttributes.Default("600")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ExhaustVolumeBoost {
			get { return _ExhaustVolumeBoost; }
			set { SetField(ref _ExhaustVolumeBoost, value, () => ExhaustVolumeBoost); }
		}

		private System.Single _RollOffBoost;
		[ModelLib.CustomAttributes.Default("15.0")]
		[ModelLib.CustomAttributes.Max(100.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single RollOffBoost {
			get { return _RollOffBoost; }
			set { SetField(ref _RollOffBoost, value, () => RollOffBoost); }
		}

		private System.UInt32 _MinTriggerTime;
		[ModelLib.CustomAttributes.Default("60000")]
		[ModelLib.CustomAttributes.Max(500000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 MinTriggerTime {
			get { return _MinTriggerTime; }
			set { SetField(ref _MinTriggerTime, value, () => MinTriggerTime); }
		}

		private System.UInt32 _MinRepeatTime;
		[ModelLib.CustomAttributes.Default("120000")]
		[ModelLib.CustomAttributes.Max(500000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 MinRepeatTime {
			get { return _MinRepeatTime; }
			set { SetField(ref _MinRepeatTime, value, () => MinRepeatTime); }
		}

		private System.Single _AttackTimeScalar;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Max(100.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single AttackTimeScalar {
			get { return _AttackTimeScalar; }
			set { SetField(ref _AttackTimeScalar, value, () => AttackTimeScalar); }
		}

		private System.Single _ReleaseTimeScalar;
		[ModelLib.CustomAttributes.Default("0.25")]
		[ModelLib.CustomAttributes.Max(100.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single ReleaseTimeScalar {
			get { return _ReleaseTimeScalar; }
			set { SetField(ref _ReleaseTimeScalar, value, () => ReleaseTimeScalar); }
		}

		private System.Byte _SmallReverbSend;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.Byte SmallReverbSend {
			get { return _SmallReverbSend; }
			set { SetField(ref _SmallReverbSend, value, () => SmallReverbSend); }
		}

		private System.Byte _MediumReverbSend;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.Byte MediumReverbSend {
			get { return _MediumReverbSend; }
			set { SetField(ref _MediumReverbSend, value, () => MediumReverbSend); }
		}

		private System.Byte _LargeReverbSend;
		[ModelLib.CustomAttributes.Default("30")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.Byte LargeReverbSend {
			get { return _LargeReverbSend; }
			set { SetField(ref _LargeReverbSend, value, () => LargeReverbSend); }
		}

		private System.Single _JunctionTriggerSpeed;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Junctions")]
		[ModelLib.CustomAttributes.Max(100.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single JunctionTriggerSpeed {
			get { return _JunctionTriggerSpeed; }
			set { SetField(ref _JunctionTriggerSpeed, value, () => JunctionTriggerSpeed); }
		}

		private System.Single _JunctionStopSpeed;
		[ModelLib.CustomAttributes.Default("0.5")]
		[ModelLib.CustomAttributes.DisplayGroup("Junctions")]
		[ModelLib.CustomAttributes.Max(100.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single JunctionStopSpeed {
			get { return _JunctionStopSpeed; }
			set { SetField(ref _JunctionStopSpeed, value, () => JunctionStopSpeed); }
		}

		private System.UInt32 _JunctionMinDistance;
		[ModelLib.CustomAttributes.Default("25")]
		[ModelLib.CustomAttributes.DisplayGroup("Junctions")]
		[ModelLib.CustomAttributes.Max(1000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 JunctionMinDistance {
			get { return _JunctionMinDistance; }
			set { SetField(ref _JunctionMinDistance, value, () => JunctionMinDistance); }
		}

		private System.UInt32 _JunctionMaxDistance;
		[ModelLib.CustomAttributes.Default("70")]
		[ModelLib.CustomAttributes.DisplayGroup("Junctions")]
		[ModelLib.CustomAttributes.Max(1000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 JunctionMaxDistance {
			get { return _JunctionMaxDistance; }
			set { SetField(ref _JunctionMaxDistance, value, () => JunctionMaxDistance); }
		}

		private System.Single _PassbyTriggerSpeed;
		[ModelLib.CustomAttributes.Default("10.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Passybys")]
		[ModelLib.CustomAttributes.Max(100.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single PassbyTriggerSpeed {
			get { return _PassbyTriggerSpeed; }
			set { SetField(ref _PassbyTriggerSpeed, value, () => PassbyTriggerSpeed); }
		}

		private System.Single _PassbyStopSpeed;
		[ModelLib.CustomAttributes.Default("5.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Passybys")]
		[ModelLib.CustomAttributes.Max(100.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single PassbyStopSpeed {
			get { return _PassbyStopSpeed; }
			set { SetField(ref _PassbyStopSpeed, value, () => PassbyStopSpeed); }
		}

		private System.UInt32 _PassbyMinDistance;
		[ModelLib.CustomAttributes.Default("10")]
		[ModelLib.CustomAttributes.DisplayGroup("Passybys")]
		[ModelLib.CustomAttributes.Max(1000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 PassbyMinDistance {
			get { return _PassbyMinDistance; }
			set { SetField(ref _PassbyMinDistance, value, () => PassbyMinDistance); }
		}

		private System.UInt32 _PassbyMaxDistance;
		[ModelLib.CustomAttributes.Default("70")]
		[ModelLib.CustomAttributes.DisplayGroup("Passybys")]
		[ModelLib.CustomAttributes.Max(1000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 PassbyMaxDistance {
			get { return _PassbyMaxDistance; }
			set { SetField(ref _PassbyMaxDistance, value, () => PassbyMaxDistance); }
		}

		private System.Single _PassbyLookaheadTime;
		[ModelLib.CustomAttributes.Default("2.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Passybys")]
		[ModelLib.CustomAttributes.Max(60.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single PassbyLookaheadTime {
			get { return _PassbyLookaheadTime; }
			set { SetField(ref _PassbyLookaheadTime, value, () => PassbyLookaheadTime); }
		}

		private System.UInt32 _ClusterTriggerDistance;
		[ModelLib.CustomAttributes.Default("20")]
		[ModelLib.CustomAttributes.DisplayGroup("Clusters")]
		[ModelLib.CustomAttributes.Max(1000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 ClusterTriggerDistance {
			get { return _ClusterTriggerDistance; }
			set { SetField(ref _ClusterTriggerDistance, value, () => ClusterTriggerDistance); }
		}

		private System.Single _ClusterTriggerSpeed;
		[ModelLib.CustomAttributes.Default("10.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Passybys")]
		[ModelLib.CustomAttributes.Max(100.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single ClusterTriggerSpeed {
			get { return _ClusterTriggerSpeed; }
			set { SetField(ref _ClusterTriggerSpeed, value, () => ClusterTriggerSpeed); }
		}

	}
}


