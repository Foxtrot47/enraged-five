using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class EnvironmentSound:  Sound{

		private System.Byte _ChannelId;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Description("Specifies which channel should be used as the source from the PCM generator")]
		[ModelLib.CustomAttributes.Max(3)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Byte ChannelId {
			get { return _ChannelId; }
			set { SetField(ref _ChannelId, value, () => ChannelId); }
		}

	}
}


