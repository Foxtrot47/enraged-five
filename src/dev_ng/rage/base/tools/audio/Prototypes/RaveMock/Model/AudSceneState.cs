using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class AudSceneState:  ModelBase {

		public class StateDefinition: ModelBase {
			private System.String _Name;
			public System.String Name {
				get { return _Name; }
				set { SetField(ref _Name, value, () => Name); }
			}

			private ModelLib.ObjectRef _Scene;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Scene {
				get { return _Scene; }
				set { SetField(ref _Scene, value, () => Scene); }
			}

		}
		private ItemsObservableCollection<StateDefinition> _State = new ItemsObservableCollection<StateDefinition>();
		[ModelLib.CustomAttributes.MaxSize(8)]
		[XmlElement("State")]
		public ItemsObservableCollection<StateDefinition> State {
			get { return _State; }
			set { SetField(ref _State, value, () => State); }
		}

	}
}


