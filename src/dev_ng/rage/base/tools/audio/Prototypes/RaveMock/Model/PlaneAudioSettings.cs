using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class PlaneAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _EngineLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineLoop {
			get { return _EngineLoop; }
			set { SetField(ref _EngineLoop, value, () => EngineLoop); }
		}

		private ModelLib.ObjectRef _ExhaustLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustLoop {
			get { return _ExhaustLoop; }
			set { SetField(ref _ExhaustLoop, value, () => ExhaustLoop); }
		}

		private ModelLib.ObjectRef _IdleLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IdleLoop {
			get { return _IdleLoop; }
			set { SetField(ref _IdleLoop, value, () => IdleLoop); }
		}

		private ModelLib.ObjectRef _DistanceLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DistanceLoop {
			get { return _DistanceLoop; }
			set { SetField(ref _DistanceLoop, value, () => DistanceLoop); }
		}

		private ModelLib.ObjectRef _PropellorLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PropellorLoop {
			get { return _PropellorLoop; }
			set { SetField(ref _PropellorLoop, value, () => PropellorLoop); }
		}

		private ModelLib.ObjectRef _BankingLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankingLoop {
			get { return _BankingLoop; }
			set { SetField(ref _BankingLoop, value, () => BankingLoop); }
		}

		private System.UInt16 _EngineConeFrontAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 EngineConeFrontAngle {
			get { return _EngineConeFrontAngle; }
			set { SetField(ref _EngineConeFrontAngle, value, () => EngineConeFrontAngle); }
		}

		private System.UInt16 _EngineConeRearAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 EngineConeRearAngle {
			get { return _EngineConeRearAngle; }
			set { SetField(ref _EngineConeRearAngle, value, () => EngineConeRearAngle); }
		}

		private System.Int16 _EngineConeAtten;
		[ModelLib.CustomAttributes.Default("-1500")]
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 EngineConeAtten {
			get { return _EngineConeAtten; }
			set { SetField(ref _EngineConeAtten, value, () => EngineConeAtten); }
		}

		private System.UInt16 _ExhaustConeFrontAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 ExhaustConeFrontAngle {
			get { return _ExhaustConeFrontAngle; }
			set { SetField(ref _ExhaustConeFrontAngle, value, () => ExhaustConeFrontAngle); }
		}

		private System.UInt16 _ExhaustConeRearAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 ExhaustConeRearAngle {
			get { return _ExhaustConeRearAngle; }
			set { SetField(ref _ExhaustConeRearAngle, value, () => ExhaustConeRearAngle); }
		}

		private System.Int16 _ExhaustConeAtten;
		[ModelLib.CustomAttributes.Default("-1500")]
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 ExhaustConeAtten {
			get { return _ExhaustConeAtten; }
			set { SetField(ref _ExhaustConeAtten, value, () => ExhaustConeAtten); }
		}

		private System.UInt16 _PropellorConeFrontAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 PropellorConeFrontAngle {
			get { return _PropellorConeFrontAngle; }
			set { SetField(ref _PropellorConeFrontAngle, value, () => PropellorConeFrontAngle); }
		}

		private System.UInt16 _PropellorConeRearAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 PropellorConeRearAngle {
			get { return _PropellorConeRearAngle; }
			set { SetField(ref _PropellorConeRearAngle, value, () => PropellorConeRearAngle); }
		}

		private System.Int16 _PropellorConeAtten;
		[ModelLib.CustomAttributes.Default("-1500")]
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 PropellorConeAtten {
			get { return _PropellorConeAtten; }
			set { SetField(ref _PropellorConeAtten, value, () => PropellorConeAtten); }
		}

		private ModelLib.ObjectRef _EngineThrottleVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineThrottleVolumeCurve {
			get { return _EngineThrottleVolumeCurve; }
			set { SetField(ref _EngineThrottleVolumeCurve, value, () => EngineThrottleVolumeCurve); }
		}

		private ModelLib.ObjectRef _EngineThrottlePitchCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineThrottlePitchCurve {
			get { return _EngineThrottlePitchCurve; }
			set { SetField(ref _EngineThrottlePitchCurve, value, () => EngineThrottlePitchCurve); }
		}

		private ModelLib.ObjectRef _ExhaustThrottleVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustThrottleVolumeCurve {
			get { return _ExhaustThrottleVolumeCurve; }
			set { SetField(ref _ExhaustThrottleVolumeCurve, value, () => ExhaustThrottleVolumeCurve); }
		}

		private ModelLib.ObjectRef _ExhaustThrottlePitchCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustThrottlePitchCurve {
			get { return _ExhaustThrottlePitchCurve; }
			set { SetField(ref _ExhaustThrottlePitchCurve, value, () => ExhaustThrottlePitchCurve); }
		}

		private ModelLib.ObjectRef _PropellorThrottleVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PropellorThrottleVolumeCurve {
			get { return _PropellorThrottleVolumeCurve; }
			set { SetField(ref _PropellorThrottleVolumeCurve, value, () => PropellorThrottleVolumeCurve); }
		}

		private ModelLib.ObjectRef _PropellorThrottlePitchCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PropellorThrottlePitchCurve {
			get { return _PropellorThrottlePitchCurve; }
			set { SetField(ref _PropellorThrottlePitchCurve, value, () => PropellorThrottlePitchCurve); }
		}

		private ModelLib.ObjectRef _IdleThrottleVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IdleThrottleVolumeCurve {
			get { return _IdleThrottleVolumeCurve; }
			set { SetField(ref _IdleThrottleVolumeCurve, value, () => IdleThrottleVolumeCurve); }
		}

		private ModelLib.ObjectRef _IdleThrottlePitchCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IdleThrottlePitchCurve {
			get { return _IdleThrottlePitchCurve; }
			set { SetField(ref _IdleThrottlePitchCurve, value, () => IdleThrottlePitchCurve); }
		}

		private ModelLib.ObjectRef _DistanceThrottleVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DistanceThrottleVolumeCurve {
			get { return _DistanceThrottleVolumeCurve; }
			set { SetField(ref _DistanceThrottleVolumeCurve, value, () => DistanceThrottleVolumeCurve); }
		}

		private ModelLib.ObjectRef _DistanceThrottlePitchCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DistanceThrottlePitchCurve {
			get { return _DistanceThrottlePitchCurve; }
			set { SetField(ref _DistanceThrottlePitchCurve, value, () => DistanceThrottlePitchCurve); }
		}

		private ModelLib.ObjectRef _DistanceVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DistanceVolumeCurve {
			get { return _DistanceVolumeCurve; }
			set { SetField(ref _DistanceVolumeCurve, value, () => DistanceVolumeCurve); }
		}

		private ModelLib.ObjectRef _StallWarning;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef StallWarning {
			get { return _StallWarning; }
			set { SetField(ref _StallWarning, value, () => StallWarning); }
		}

		private ModelLib.ObjectRef _DoorOpen;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Doors")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorOpen {
			get { return _DoorOpen; }
			set { SetField(ref _DoorOpen, value, () => DoorOpen); }
		}

		private ModelLib.ObjectRef _DoorClose;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Doors")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorClose {
			get { return _DoorClose; }
			set { SetField(ref _DoorClose, value, () => DoorClose); }
		}

		private ModelLib.ObjectRef _DoorLimit;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Doors")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorLimit {
			get { return _DoorLimit; }
			set { SetField(ref _DoorLimit, value, () => DoorLimit); }
		}

		private ModelLib.ObjectRef _LandingGearDeploy;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LandingGearDeploy {
			get { return _LandingGearDeploy; }
			set { SetField(ref _LandingGearDeploy, value, () => LandingGearDeploy); }
		}

		private ModelLib.ObjectRef _LandingGearRetract;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LandingGearRetract {
			get { return _LandingGearRetract; }
			set { SetField(ref _LandingGearRetract, value, () => LandingGearRetract); }
		}

		private ModelLib.ObjectRef _Ignition;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Ignition {
			get { return _Ignition; }
			set { SetField(ref _Ignition, value, () => Ignition); }
		}

		private ModelLib.ObjectRef _TyreSqueal;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TyreSqueal {
			get { return _TyreSqueal; }
			set { SetField(ref _TyreSqueal, value, () => TyreSqueal); }
		}

		private ModelLib.ObjectRef _BankingThrottleVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankingThrottleVolumeCurve {
			get { return _BankingThrottleVolumeCurve; }
			set { SetField(ref _BankingThrottleVolumeCurve, value, () => BankingThrottleVolumeCurve); }
		}

		private ModelLib.ObjectRef _BankingThrottlePitchCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankingThrottlePitchCurve {
			get { return _BankingThrottlePitchCurve; }
			set { SetField(ref _BankingThrottlePitchCurve, value, () => BankingThrottlePitchCurve); }
		}

		private ModelLib.ObjectRef _BankingAngleVolCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankingAngleVolCurve {
			get { return _BankingAngleVolCurve; }
			set { SetField(ref _BankingAngleVolCurve, value, () => BankingAngleVolCurve); }
		}

		private ModelLib.ObjectRef _AfterburnerLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AfterburnerLoop {
			get { return _AfterburnerLoop; }
			set { SetField(ref _AfterburnerLoop, value, () => AfterburnerLoop); }
		}

		private BankingStyle _BankingStyle;
		[ModelLib.CustomAttributes.Default("kRotationAngle")]
		public BankingStyle BankingStyle {
			get { return _BankingStyle; }
			set { SetField(ref _BankingStyle, value, () => BankingStyle); }
		}

		private ModelLib.ObjectRef _AfterburnerThrottleVolCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AfterburnerThrottleVolCurve {
			get { return _AfterburnerThrottleVolCurve; }
			set { SetField(ref _AfterburnerThrottleVolCurve, value, () => AfterburnerThrottleVolCurve); }
		}

		private ModelLib.Types.TriState _EnginesAttachedToWings;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public ModelLib.Types.TriState EnginesAttachedToWings {
			get { return _EnginesAttachedToWings; }
			set { SetField(ref _EnginesAttachedToWings, value, () => EnginesAttachedToWings); }
		}

		private ModelLib.Types.TriState _HasMissileLockSystem;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public ModelLib.Types.TriState HasMissileLockSystem {
			get { return _HasMissileLockSystem; }
			set { SetField(ref _HasMissileLockSystem, value, () => HasMissileLockSystem); }
		}

		private ModelLib.ObjectRef _EngineSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSynthDef {
			get { return _EngineSynthDef; }
			set { SetField(ref _EngineSynthDef, value, () => EngineSynthDef); }
		}

		private ModelLib.ObjectRef _EngineSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSynthPreset {
			get { return _EngineSynthPreset; }
			set { SetField(ref _EngineSynthPreset, value, () => EngineSynthPreset); }
		}

		private ModelLib.ObjectRef _ExhaustSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSynthDef {
			get { return _ExhaustSynthDef; }
			set { SetField(ref _ExhaustSynthDef, value, () => ExhaustSynthDef); }
		}

		private ModelLib.ObjectRef _ExhaustSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSynthPreset {
			get { return _ExhaustSynthPreset; }
			set { SetField(ref _ExhaustSynthPreset, value, () => ExhaustSynthPreset); }
		}

		private ModelLib.ObjectRef _EngineSubmixVoice;
		[ModelLib.CustomAttributes.Default("DEFAULT_PLANE_ENGINE_SUBMIX_CONTROL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSubmixVoice {
			get { return _EngineSubmixVoice; }
			set { SetField(ref _EngineSubmixVoice, value, () => EngineSubmixVoice); }
		}

		private ModelLib.ObjectRef _ExhaustSubmixVoice;
		[ModelLib.CustomAttributes.Default("DEFAULT_PLANE_EXHAUST_SUBMIX_CONTROL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSubmixVoice {
			get { return _ExhaustSubmixVoice; }
			set { SetField(ref _ExhaustSubmixVoice, value, () => ExhaustSubmixVoice); }
		}

		private ModelLib.ObjectRef _PropellorBreakOneShot;
		[ModelLib.CustomAttributes.Default("PROP_PLANE_ENGINE_BREAKDOWN")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PropellorBreakOneShot {
			get { return _PropellorBreakOneShot; }
			set { SetField(ref _PropellorBreakOneShot, value, () => PropellorBreakOneShot); }
		}

		private ModelLib.ObjectRef _WindNoise;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WindNoise {
			get { return _WindNoise; }
			set { SetField(ref _WindNoise, value, () => WindNoise); }
		}

		private ModelLib.ObjectRef _PeelingPitchCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PeelingPitchCurve {
			get { return _PeelingPitchCurve; }
			set { SetField(ref _PeelingPitchCurve, value, () => PeelingPitchCurve); }
		}

		private System.Int16 _DivingFactor;
		[ModelLib.CustomAttributes.Default("1")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("m/s")]
		public System.Int16 DivingFactor {
			get { return _DivingFactor; }
			set { SetField(ref _DivingFactor, value, () => DivingFactor); }
		}

		private System.Int16 _MaxDivePitch;
		[ModelLib.CustomAttributes.Default("750")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int16 MaxDivePitch {
			get { return _MaxDivePitch; }
			set { SetField(ref _MaxDivePitch, value, () => MaxDivePitch); }
		}

		private System.Int16 _DiveAirSpeedThreshold;
		[ModelLib.CustomAttributes.Default("50")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(1000)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("m/s")]
		public System.Int16 DiveAirSpeedThreshold {
			get { return _DiveAirSpeedThreshold; }
			set { SetField(ref _DiveAirSpeedThreshold, value, () => DiveAirSpeedThreshold); }
		}

		private System.Int16 _DivingRateApproachingGround;
		[ModelLib.CustomAttributes.Default("-30")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(1000)]
		[ModelLib.CustomAttributes.Min(-1000)]
		[ModelLib.CustomAttributes.Unit("m/s")]
		public System.Int16 DivingRateApproachingGround {
			get { return _DivingRateApproachingGround; }
			set { SetField(ref _DivingRateApproachingGround, value, () => DivingRateApproachingGround); }
		}

		private System.Single _PeelingAfterburnerPitchScalingFactor;
		[ModelLib.CustomAttributes.Default("2")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(10)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single PeelingAfterburnerPitchScalingFactor {
			get { return _PeelingAfterburnerPitchScalingFactor; }
			set { SetField(ref _PeelingAfterburnerPitchScalingFactor, value, () => PeelingAfterburnerPitchScalingFactor); }
		}

		private ModelLib.ObjectRef _Rudder;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Rudder {
			get { return _Rudder; }
			set { SetField(ref _Rudder, value, () => Rudder); }
		}

		private ModelLib.ObjectRef _Aileron;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Aileron {
			get { return _Aileron; }
			set { SetField(ref _Aileron, value, () => Aileron); }
		}

		private ModelLib.ObjectRef _Elevator;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Elevator {
			get { return _Elevator; }
			set { SetField(ref _Elevator, value, () => Elevator); }
		}

		private ModelLib.ObjectRef _DoorStartOpen;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Doors")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorStartOpen {
			get { return _DoorStartOpen; }
			set { SetField(ref _DoorStartOpen, value, () => DoorStartOpen); }
		}

		private ModelLib.ObjectRef _DoorStartClose;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Doors")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorStartClose {
			get { return _DoorStartClose; }
			set { SetField(ref _DoorStartClose, value, () => DoorStartClose); }
		}

		private ModelLib.ObjectRef _VehicleCollisions;
		[ModelLib.CustomAttributes.Default("VEHICLE_COLLISION_PLANE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(VehicleCollisionSettings))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehicleCollisions {
			get { return _VehicleCollisions; }
			set { SetField(ref _VehicleCollisions, value, () => VehicleCollisions); }
		}

		private ModelLib.ObjectRef _FireAudio;
		[ModelLib.CustomAttributes.Default("VEH_FIRE_SOUNDSET")]
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FireAudio {
			get { return _FireAudio; }
			set { SetField(ref _FireAudio, value, () => FireAudio); }
		}

		private ModelLib.ObjectRef _EngineMissFire;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineMissFire {
			get { return _EngineMissFire; }
			set { SetField(ref _EngineMissFire, value, () => EngineMissFire); }
		}

		private System.UInt32 _IsRealLODRange;
		[ModelLib.CustomAttributes.Default("5000000")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(20000000)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("m")]
		public System.UInt32 IsRealLODRange {
			get { return _IsRealLODRange; }
			set { SetField(ref _IsRealLODRange, value, () => IsRealLODRange); }
		}

		private ModelLib.Types.TriState _IsJet;
		[ModelLib.CustomAttributes.DisplayGroup("Main")]
		public ModelLib.Types.TriState IsJet {
			get { return _IsJet; }
			set { SetField(ref _IsJet, value, () => IsJet); }
		}

		private ModelLib.ObjectRef _SuspensionUp;
		[ModelLib.CustomAttributes.Default("SUSPENSION_UP")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.Display("Up Sound")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SuspensionUp {
			get { return _SuspensionUp; }
			set { SetField(ref _SuspensionUp, value, () => SuspensionUp); }
		}

		private ModelLib.ObjectRef _SuspensionDown;
		[ModelLib.CustomAttributes.Default("SUSPENSION_DOWN")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.Display("Down Sound")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SuspensionDown {
			get { return _SuspensionDown; }
			set { SetField(ref _SuspensionDown, value, () => SuspensionDown); }
		}

		private System.Single _MinSuspCompThresh;
		[ModelLib.CustomAttributes.Default("0.4")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		public System.Single MinSuspCompThresh {
			get { return _MinSuspCompThresh; }
			set { SetField(ref _MinSuspCompThresh, value, () => MinSuspCompThresh); }
		}

		private System.Single _MaxSuspCompThres;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		public System.Single MaxSuspCompThres {
			get { return _MaxSuspCompThres; }
			set { SetField(ref _MaxSuspCompThres, value, () => MaxSuspCompThres); }
		}

		private ModelLib.ObjectRef _DamageEngineSpeedVolumeCurve;
		[ModelLib.CustomAttributes.Default("PLANE_JET_DAMAGE_THROTTLE_VOL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DamageEngineSpeedVolumeCurve {
			get { return _DamageEngineSpeedVolumeCurve; }
			set { SetField(ref _DamageEngineSpeedVolumeCurve, value, () => DamageEngineSpeedVolumeCurve); }
		}

		private ModelLib.ObjectRef _DamageEngineSpeedPitchCurve;
		[ModelLib.CustomAttributes.Default("PLANE_JET_DAMAGE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DamageEngineSpeedPitchCurve {
			get { return _DamageEngineSpeedPitchCurve; }
			set { SetField(ref _DamageEngineSpeedPitchCurve, value, () => DamageEngineSpeedPitchCurve); }
		}

		private ModelLib.ObjectRef _DamageHealthVolumeCurve;
		[ModelLib.CustomAttributes.Default("PLANE_JET_DAMAGE_VOL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DamageHealthVolumeCurve {
			get { return _DamageHealthVolumeCurve; }
			set { SetField(ref _DamageHealthVolumeCurve, value, () => DamageHealthVolumeCurve); }
		}

		private ModelLib.ObjectRef _TurbineWindDown;
		[ModelLib.CustomAttributes.Default("JET_PLANE_COOLING_TURBINE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TurbineWindDown {
			get { return _TurbineWindDown; }
			set { SetField(ref _TurbineWindDown, value, () => TurbineWindDown); }
		}

		private ModelLib.ObjectRef _JetDamageLoop;
		[ModelLib.CustomAttributes.Default("JET_PLANE_DAMAGE_LOOP")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef JetDamageLoop {
			get { return _JetDamageLoop; }
			set { SetField(ref _JetDamageLoop, value, () => JetDamageLoop); }
		}

		private ModelLib.ObjectRef _AfterburnerThrottlePitchCurve;
		[ModelLib.CustomAttributes.Default("PLANE_JET_AFTERBURNER_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AfterburnerThrottlePitchCurve {
			get { return _AfterburnerThrottlePitchCurve; }
			set { SetField(ref _AfterburnerThrottlePitchCurve, value, () => AfterburnerThrottlePitchCurve); }
		}

		private System.Single _NPCEngineSmoothAmount;
		[ModelLib.CustomAttributes.Default("0.003")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single NPCEngineSmoothAmount {
			get { return _NPCEngineSmoothAmount; }
			set { SetField(ref _NPCEngineSmoothAmount, value, () => NPCEngineSmoothAmount); }
		}

		private ModelLib.ObjectRef _FlybySound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FlybySound {
			get { return _FlybySound; }
			set { SetField(ref _FlybySound, value, () => FlybySound); }
		}

		private ModelLib.ObjectRef _DivingSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DivingSound {
			get { return _DivingSound; }
			set { SetField(ref _DivingSound, value, () => DivingSound); }
		}

		private System.Int16 _DivingSoundPitchFactor;
		[ModelLib.CustomAttributes.Default("1")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(10)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int16 DivingSoundPitchFactor {
			get { return _DivingSoundPitchFactor; }
			set { SetField(ref _DivingSoundPitchFactor, value, () => DivingSoundPitchFactor); }
		}

		private System.Int16 _DivingSoundVolumeFactor;
		[ModelLib.CustomAttributes.Default("1")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(10)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int16 DivingSoundVolumeFactor {
			get { return _DivingSoundVolumeFactor; }
			set { SetField(ref _DivingSoundVolumeFactor, value, () => DivingSoundVolumeFactor); }
		}

		private System.Int16 _MaxDiveSoundPitch;
		[ModelLib.CustomAttributes.Default("750")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int16 MaxDiveSoundPitch {
			get { return _MaxDiveSoundPitch; }
			set { SetField(ref _MaxDiveSoundPitch, value, () => MaxDiveSoundPitch); }
		}

		private RadioType _RadioType;
		[ModelLib.CustomAttributes.Default("RADIO_TYPE_NORMAL")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public RadioType RadioType {
			get { return _RadioType; }
			set { SetField(ref _RadioType, value, () => RadioType); }
		}

		private RadioGenre _RadioGenre;
		[ModelLib.CustomAttributes.Default("RADIO_GENRE_UNSPECIFIED")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public RadioGenre RadioGenre {
			get { return _RadioGenre; }
			set { SetField(ref _RadioGenre, value, () => RadioGenre); }
		}

		private ModelLib.Types.TriState _DisableAmbientRadio;
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public ModelLib.Types.TriState DisableAmbientRadio {
			get { return _DisableAmbientRadio; }
			set { SetField(ref _DisableAmbientRadio, value, () => DisableAmbientRadio); }
		}

		private ModelLib.Types.TriState _AircraftWarningVoiceIsMale;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public ModelLib.Types.TriState AircraftWarningVoiceIsMale {
			get { return _AircraftWarningVoiceIsMale; }
			set { SetField(ref _AircraftWarningVoiceIsMale, value, () => AircraftWarningVoiceIsMale); }
		}

		private System.Single _AircraftWarningSeriousDamageThresh;
		[ModelLib.CustomAttributes.Default("0.003")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(-1000.0)]
		public System.Single AircraftWarningSeriousDamageThresh {
			get { return _AircraftWarningSeriousDamageThresh; }
			set { SetField(ref _AircraftWarningSeriousDamageThresh, value, () => AircraftWarningSeriousDamageThresh); }
		}

		private System.Single _AircraftWarningCriticalDamageThresh;
		[ModelLib.CustomAttributes.Default("0.003")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(-1000.0)]
		public System.Single AircraftWarningCriticalDamageThresh {
			get { return _AircraftWarningCriticalDamageThresh; }
			set { SetField(ref _AircraftWarningCriticalDamageThresh, value, () => AircraftWarningCriticalDamageThresh); }
		}

		private System.Single _AircraftWarningMaxSpeed;
		[ModelLib.CustomAttributes.Default("100.0")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(500.0)]
		[ModelLib.CustomAttributes.Min(10.0)]
		public System.Single AircraftWarningMaxSpeed {
			get { return _AircraftWarningMaxSpeed; }
			set { SetField(ref _AircraftWarningMaxSpeed, value, () => AircraftWarningMaxSpeed); }
		}

		private ModelLib.ObjectRef _SimpleSoundForLoading;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SimpleSoundForLoading {
			get { return _SimpleSoundForLoading; }
			set { SetField(ref _SimpleSoundForLoading, value, () => SimpleSoundForLoading); }
		}

		private ModelLib.ObjectRef _FlyAwaySound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FlyAwaySound {
			get { return _FlyAwaySound; }
			set { SetField(ref _FlyAwaySound, value, () => FlyAwaySound); }
		}

	}
}


