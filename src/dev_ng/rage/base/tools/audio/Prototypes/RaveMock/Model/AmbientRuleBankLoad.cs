using ModelLib.CustomAttributes;

namespace model {

	public enum AmbientRuleBankLoad {
		[ModelLib.CustomAttributes.Display("Influence Bank Loading")]
		INFLUENCE_BANK_LOAD,
		[ModelLib.CustomAttributes.Display("Don't Influence Bank Loading")]
		DONT_INFLUENCE_BANK_LOAD
	}

}