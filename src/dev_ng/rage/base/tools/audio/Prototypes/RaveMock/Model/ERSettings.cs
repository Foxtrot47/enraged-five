using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Effects.Reverb")]
	public class ERSettings:  ModelBase {

		private System.Single _RoomSize;
		[ModelLib.CustomAttributes.Default("12")]
		[ModelLib.CustomAttributes.Max(20)]
		[ModelLib.CustomAttributes.Min(7)]
		[ModelLib.CustomAttributes.Unit("m")]
		public System.Single RoomSize {
			get { return _RoomSize; }
			set { SetField(ref _RoomSize, value, () => RoomSize); }
		}

		public class RoomDimensionsDefinition: ModelBase {
			private System.Single _widthRatio;
			[ModelLib.CustomAttributes.Default("1.62")]
			[ModelLib.CustomAttributes.Max(2)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single widthRatio {
				get { return _widthRatio; }
				set { SetField(ref _widthRatio, value, () => widthRatio); }
			}

			private System.Single _lengthRatio;
			[ModelLib.CustomAttributes.Default("1")]
			[ModelLib.CustomAttributes.Max(2)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single lengthRatio {
				get { return _lengthRatio; }
				set { SetField(ref _lengthRatio, value, () => lengthRatio); }
			}

			private System.Single _heightRatio;
			[ModelLib.CustomAttributes.Default("0.62")]
			[ModelLib.CustomAttributes.Max(2)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single heightRatio {
				get { return _heightRatio; }
				set { SetField(ref _heightRatio, value, () => heightRatio); }
			}

		}
		private RoomDimensionsDefinition _RoomDimensions;
		public RoomDimensionsDefinition RoomDimensions {
			get { return _RoomDimensions; }
			set { SetField(ref _RoomDimensions, value, () => RoomDimensions); }
		}

		public class ListenerPosDefinition: ModelBase {
			private System.Single _x;
			[ModelLib.CustomAttributes.Default("0.23")]
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single x {
				get { return _x; }
				set { SetField(ref _x, value, () => x); }
			}

			private System.Single _y;
			[ModelLib.CustomAttributes.Default("0.59")]
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

			private System.Single _z;
			[ModelLib.CustomAttributes.Default("0.73")]
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single z {
				get { return _z; }
				set { SetField(ref _z, value, () => z); }
			}

		}
		private ListenerPosDefinition _ListenerPos;
		public ListenerPosDefinition ListenerPos {
			get { return _ListenerPos; }
			set { SetField(ref _ListenerPos, value, () => ListenerPos); }
		}

		public class AllpassesDefinition: ModelBase {
			private System.Single _Diffusion;
			[ModelLib.CustomAttributes.Default("0.7")]
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single Diffusion {
				get { return _Diffusion; }
				set { SetField(ref _Diffusion, value, () => Diffusion); }
			}

			private System.UInt32 _DelayLength;
			[ModelLib.CustomAttributes.Default("327")]
			[ModelLib.CustomAttributes.Max(10000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 DelayLength {
				get { return _DelayLength; }
				set { SetField(ref _DelayLength, value, () => DelayLength); }
			}

		}
		private ItemsObservableCollection<AllpassesDefinition> _Allpasses = new ItemsObservableCollection<AllpassesDefinition>();
		[ModelLib.CustomAttributes.FixedSize]
		[ModelLib.CustomAttributes.MaxSize(4)]
		[XmlElement("Allpasses")]
		public ItemsObservableCollection<AllpassesDefinition> Allpasses {
			get { return _Allpasses; }
			set { SetField(ref _Allpasses, value, () => Allpasses); }
		}

		public class NodeGainMatrixDefinition: ModelBase {
			public class X1Definition: ModelBase {
				private System.Single _FL;
				[ModelLib.CustomAttributes.Default("-1.21")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FL {
					get { return _FL; }
					set { SetField(ref _FL, value, () => FL); }
				}

				private System.Single _FR;
				[ModelLib.CustomAttributes.Default("-1.21")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FR {
					get { return _FR; }
					set { SetField(ref _FR, value, () => FR); }
				}

				private System.Single _RL;
				[ModelLib.CustomAttributes.Default("-1.21")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RL {
					get { return _RL; }
					set { SetField(ref _RL, value, () => RL); }
				}

				private System.Single _RR;
				[ModelLib.CustomAttributes.Default("-1.21")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RR {
					get { return _RR; }
					set { SetField(ref _RR, value, () => RR); }
				}

			}
			private X1Definition _X1;
			public X1Definition X1 {
				get { return _X1; }
				set { SetField(ref _X1, value, () => X1); }
			}

			public class X2Definition: ModelBase {
				private System.Single _FL;
				[ModelLib.CustomAttributes.Default("-0.92")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FL {
					get { return _FL; }
					set { SetField(ref _FL, value, () => FL); }
				}

				private System.Single _FR;
				[ModelLib.CustomAttributes.Default("-0.92")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FR {
					get { return _FR; }
					set { SetField(ref _FR, value, () => FR); }
				}

				private System.Single _RL;
				[ModelLib.CustomAttributes.Default("-0.92")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RL {
					get { return _RL; }
					set { SetField(ref _RL, value, () => RL); }
				}

				private System.Single _RR;
				[ModelLib.CustomAttributes.Default("-0.92")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RR {
					get { return _RR; }
					set { SetField(ref _RR, value, () => RR); }
				}

			}
			private X2Definition _X2;
			public X2Definition X2 {
				get { return _X2; }
				set { SetField(ref _X2, value, () => X2); }
			}

			public class Y1Definition: ModelBase {
				private System.Single _FL;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FL {
					get { return _FL; }
					set { SetField(ref _FL, value, () => FL); }
				}

				private System.Single _FR;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FR {
					get { return _FR; }
					set { SetField(ref _FR, value, () => FR); }
				}

				private System.Single _RL;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RL {
					get { return _RL; }
					set { SetField(ref _RL, value, () => RL); }
				}

				private System.Single _RR;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RR {
					get { return _RR; }
					set { SetField(ref _RR, value, () => RR); }
				}

			}
			private Y1Definition _Y1;
			public Y1Definition Y1 {
				get { return _Y1; }
				set { SetField(ref _Y1, value, () => Y1); }
			}

			public class Y2Definition: ModelBase {
				private System.Single _FL;
				[ModelLib.CustomAttributes.Default("-1.83")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FL {
					get { return _FL; }
					set { SetField(ref _FL, value, () => FL); }
				}

				private System.Single _FR;
				[ModelLib.CustomAttributes.Default("-1.83")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FR {
					get { return _FR; }
					set { SetField(ref _FR, value, () => FR); }
				}

				private System.Single _RL;
				[ModelLib.CustomAttributes.Default("-1.83")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RL {
					get { return _RL; }
					set { SetField(ref _RL, value, () => RL); }
				}

				private System.Single _RR;
				[ModelLib.CustomAttributes.Default("-1.83")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RR {
					get { return _RR; }
					set { SetField(ref _RR, value, () => RR); }
				}

			}
			private Y2Definition _Y2;
			public Y2Definition Y2 {
				get { return _Y2; }
				set { SetField(ref _Y2, value, () => Y2); }
			}

			public class Z1Definition: ModelBase {
				private System.Single _FL;
				[ModelLib.CustomAttributes.Default("-2.62")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FL {
					get { return _FL; }
					set { SetField(ref _FL, value, () => FL); }
				}

				private System.Single _FR;
				[ModelLib.CustomAttributes.Default("-2.62")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FR {
					get { return _FR; }
					set { SetField(ref _FR, value, () => FR); }
				}

				private System.Single _RL;
				[ModelLib.CustomAttributes.Default("-2.62")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RL {
					get { return _RL; }
					set { SetField(ref _RL, value, () => RL); }
				}

				private System.Single _RR;
				[ModelLib.CustomAttributes.Default("-2.62")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RR {
					get { return _RR; }
					set { SetField(ref _RR, value, () => RR); }
				}

			}
			private Z1Definition _Z1;
			public Z1Definition Z1 {
				get { return _Z1; }
				set { SetField(ref _Z1, value, () => Z1); }
			}

			public class Z2Definition: ModelBase {
				private System.Single _FL;
				[ModelLib.CustomAttributes.Default("-1.11")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FL {
					get { return _FL; }
					set { SetField(ref _FL, value, () => FL); }
				}

				private System.Single _FR;
				[ModelLib.CustomAttributes.Default("-1.11")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single FR {
					get { return _FR; }
					set { SetField(ref _FR, value, () => FR); }
				}

				private System.Single _RL;
				[ModelLib.CustomAttributes.Default("-1.11")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RL {
					get { return _RL; }
					set { SetField(ref _RL, value, () => RL); }
				}

				private System.Single _RR;
				[ModelLib.CustomAttributes.Default("-1.11")]
				[ModelLib.CustomAttributes.Max(24)]
				[ModelLib.CustomAttributes.Min(-100)]
				[ModelLib.CustomAttributes.Unit("decibels")]
				public System.Single RR {
					get { return _RR; }
					set { SetField(ref _RR, value, () => RR); }
				}

			}
			private Z2Definition _Z2;
			public Z2Definition Z2 {
				get { return _Z2; }
				set { SetField(ref _Z2, value, () => Z2); }
			}

		}
		private NodeGainMatrixDefinition _NodeGainMatrix;
		public NodeGainMatrixDefinition NodeGainMatrix {
			get { return _NodeGainMatrix; }
			set { SetField(ref _NodeGainMatrix, value, () => NodeGainMatrix); }
		}

		public class Gain_1stOrderDefinition: ModelBase {
			private System.Single _FL;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single FL {
				get { return _FL; }
				set { SetField(ref _FL, value, () => FL); }
			}

			private System.Single _FR;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single FR {
				get { return _FR; }
				set { SetField(ref _FR, value, () => FR); }
			}

			private System.Single _RL;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single RL {
				get { return _RL; }
				set { SetField(ref _RL, value, () => RL); }
			}

			private System.Single _RR;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single RR {
				get { return _RR; }
				set { SetField(ref _RR, value, () => RR); }
			}

		}
		private Gain_1stOrderDefinition _Gain_1stOrder;
		public Gain_1stOrderDefinition Gain_1stOrder {
			get { return _Gain_1stOrder; }
			set { SetField(ref _Gain_1stOrder, value, () => Gain_1stOrder); }
		}

		public class Gain_2ndOrderDefinition: ModelBase {
			private System.Single _FL;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single FL {
				get { return _FL; }
				set { SetField(ref _FL, value, () => FL); }
			}

			private System.Single _FR;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single FR {
				get { return _FR; }
				set { SetField(ref _FR, value, () => FR); }
			}

			private System.Single _RL;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single RL {
				get { return _RL; }
				set { SetField(ref _RL, value, () => RL); }
			}

			private System.Single _RR;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single RR {
				get { return _RR; }
				set { SetField(ref _RR, value, () => RR); }
			}

		}
		private Gain_2ndOrderDefinition _Gain_2ndOrder;
		public Gain_2ndOrderDefinition Gain_2ndOrder {
			get { return _Gain_2ndOrder; }
			set { SetField(ref _Gain_2ndOrder, value, () => Gain_2ndOrder); }
		}

		public class Gain_3rdOrderDefinition: ModelBase {
			private System.Single _FL;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single FL {
				get { return _FL; }
				set { SetField(ref _FL, value, () => FL); }
			}

			private System.Single _FR;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single FR {
				get { return _FR; }
				set { SetField(ref _FR, value, () => FR); }
			}

			private System.Single _RL;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single RL {
				get { return _RL; }
				set { SetField(ref _RL, value, () => RL); }
			}

			private System.Single _RR;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(24)]
			[ModelLib.CustomAttributes.Min(-100)]
			[ModelLib.CustomAttributes.Unit("decibels")]
			public System.Single RR {
				get { return _RR; }
				set { SetField(ref _RR, value, () => RR); }
			}

		}
		private Gain_3rdOrderDefinition _Gain_3rdOrder;
		public Gain_3rdOrderDefinition Gain_3rdOrder {
			get { return _Gain_3rdOrder; }
			set { SetField(ref _Gain_3rdOrder, value, () => Gain_3rdOrder); }
		}

		public class NodeLPF_1stOrderDefinition: ModelBase {
			private System.Single _FL_Cutoff;
			[ModelLib.CustomAttributes.Default("3000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single FL_Cutoff {
				get { return _FL_Cutoff; }
				set { SetField(ref _FL_Cutoff, value, () => FL_Cutoff); }
			}

			private System.Single _FR_Cutoff;
			[ModelLib.CustomAttributes.Default("3000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single FR_Cutoff {
				get { return _FR_Cutoff; }
				set { SetField(ref _FR_Cutoff, value, () => FR_Cutoff); }
			}

			private System.Single _RL_Cutoff;
			[ModelLib.CustomAttributes.Default("3000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single RL_Cutoff {
				get { return _RL_Cutoff; }
				set { SetField(ref _RL_Cutoff, value, () => RL_Cutoff); }
			}

			private System.Single _RR_Cutoff;
			[ModelLib.CustomAttributes.Default("3000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single RR_Cutoff {
				get { return _RR_Cutoff; }
				set { SetField(ref _RR_Cutoff, value, () => RR_Cutoff); }
			}

		}
		private ItemsObservableCollection<NodeLPF_1stOrderDefinition> _NodeLPF_1stOrder = new ItemsObservableCollection<NodeLPF_1stOrderDefinition>();
		[ModelLib.CustomAttributes.FixedSize]
		[ModelLib.CustomAttributes.MaxSize(6, typeof(System.UInt32))]
		[XmlElement("NodeLPF_1stOrder")]
		public ItemsObservableCollection<NodeLPF_1stOrderDefinition> NodeLPF_1stOrder {
			get { return _NodeLPF_1stOrder; }
			set { SetField(ref _NodeLPF_1stOrder, value, () => NodeLPF_1stOrder); }
		}

		public class NodeLPF_2ndOrderDefinition: ModelBase {
			private System.Single _FL_Cutoff;
			[ModelLib.CustomAttributes.Default("2000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single FL_Cutoff {
				get { return _FL_Cutoff; }
				set { SetField(ref _FL_Cutoff, value, () => FL_Cutoff); }
			}

			private System.Single _FR_Cutoff;
			[ModelLib.CustomAttributes.Default("2000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single FR_Cutoff {
				get { return _FR_Cutoff; }
				set { SetField(ref _FR_Cutoff, value, () => FR_Cutoff); }
			}

			private System.Single _RL_Cutoff;
			[ModelLib.CustomAttributes.Default("2000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single RL_Cutoff {
				get { return _RL_Cutoff; }
				set { SetField(ref _RL_Cutoff, value, () => RL_Cutoff); }
			}

			private System.Single _RR_Cutoff;
			[ModelLib.CustomAttributes.Default("2000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single RR_Cutoff {
				get { return _RR_Cutoff; }
				set { SetField(ref _RR_Cutoff, value, () => RR_Cutoff); }
			}

		}
		private ItemsObservableCollection<NodeLPF_2ndOrderDefinition> _NodeLPF_2ndOrder = new ItemsObservableCollection<NodeLPF_2ndOrderDefinition>();
		[ModelLib.CustomAttributes.FixedSize]
		[ModelLib.CustomAttributes.MaxSize(6, typeof(System.UInt32))]
		[XmlElement("NodeLPF_2ndOrder")]
		public ItemsObservableCollection<NodeLPF_2ndOrderDefinition> NodeLPF_2ndOrder {
			get { return _NodeLPF_2ndOrder; }
			set { SetField(ref _NodeLPF_2ndOrder, value, () => NodeLPF_2ndOrder); }
		}

		public class NodeLPF_3rdOrderDefinition: ModelBase {
			private System.Single _FL_Cutoff;
			[ModelLib.CustomAttributes.Default("1000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single FL_Cutoff {
				get { return _FL_Cutoff; }
				set { SetField(ref _FL_Cutoff, value, () => FL_Cutoff); }
			}

			private System.Single _FR_Cutoff;
			[ModelLib.CustomAttributes.Default("1000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single FR_Cutoff {
				get { return _FR_Cutoff; }
				set { SetField(ref _FR_Cutoff, value, () => FR_Cutoff); }
			}

			private System.Single _RL_Cutoff;
			[ModelLib.CustomAttributes.Default("1000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single RL_Cutoff {
				get { return _RL_Cutoff; }
				set { SetField(ref _RL_Cutoff, value, () => RL_Cutoff); }
			}

			private System.Single _RR_Cutoff;
			[ModelLib.CustomAttributes.Default("1000")]
			[ModelLib.CustomAttributes.Max(12000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single RR_Cutoff {
				get { return _RR_Cutoff; }
				set { SetField(ref _RR_Cutoff, value, () => RR_Cutoff); }
			}

		}
		private ItemsObservableCollection<NodeLPF_3rdOrderDefinition> _NodeLPF_3rdOrder = new ItemsObservableCollection<NodeLPF_3rdOrderDefinition>();
		[ModelLib.CustomAttributes.FixedSize]
		[ModelLib.CustomAttributes.MaxSize(6, typeof(System.UInt32))]
		[XmlElement("NodeLPF_3rdOrder")]
		public ItemsObservableCollection<NodeLPF_3rdOrderDefinition> NodeLPF_3rdOrder {
			get { return _NodeLPF_3rdOrder; }
			set { SetField(ref _NodeLPF_3rdOrder, value, () => NodeLPF_3rdOrder); }
		}

	}
}


