using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Transformer("{Type=\"rage.Transform.XmlTransformer, MetadataCompilerLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\" Args=\"$\\gta5\\audio\\dev\\assets\\Objects\\Definitions\\RAGEAudio\\xsl\\MixGroupMap_Generator.xsl$\"}}")]
	public class MixGroup:  ModelBase {

		private System.Int32 _ReferenceCount;
		[ModelLib.CustomAttributes.Default("0")]
		public System.Int32 ReferenceCount {
			get { return _ReferenceCount; }
			set { SetField(ref _ReferenceCount, value, () => ReferenceCount); }
		}

		private System.Single _FadeTime;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single FadeTime {
			get { return _FadeTime; }
			set { SetField(ref _FadeTime, value, () => FadeTime); }
		}

		private ModelLib.Types.TriState _OverrideParent;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState OverrideParent {
			get { return _OverrideParent; }
			set { SetField(ref _OverrideParent, value, () => OverrideParent); }
		}

		private ModelLib.ObjectRef _Map;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		[ModelLib.CustomAttributes.Hidden]
		public ModelLib.ObjectRef Map {
			get { return _Map; }
			set { SetField(ref _Map, value, () => Map); }
		}

	}
}


