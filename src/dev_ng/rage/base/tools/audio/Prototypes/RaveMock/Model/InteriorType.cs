using ModelLib.CustomAttributes;

namespace model {

	public enum InteriorType {
		[ModelLib.CustomAttributes.Display("None")]
		INTERIOR_TYPE_NONE,
		[ModelLib.CustomAttributes.Display("RoadTunnel")]
		INTERIOR_TYPE_ROAD_TUNNEL,
		[ModelLib.CustomAttributes.Display("SubwayTunnel")]
		INTERIOR_TYPE_SUBWAY_TUNNEL,
		[ModelLib.CustomAttributes.Display("SubwayStation")]
		INTERIOR_TYPE_SUBWAY_STATION,
		[ModelLib.CustomAttributes.Display("SubwayEntrance")]
		INTERIOR_TYPE_SUBWAY_ENTRANCE,
		[ModelLib.CustomAttributes.Display("AbandonedWarehouse")]
		INTERIOR_TYPE_ABANDONED_WAREHOUSE
	}

}