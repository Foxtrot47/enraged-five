using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class VoiceContextInfo:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private System.Byte _numVariations;
		public System.Byte numVariations {
			get { return _numVariations; }
			set { SetField(ref _numVariations, value, () => numVariations); }
		}

		private System.UInt16 _bankNameTableIndex;
		public System.UInt16 bankNameTableIndex {
			get { return _bankNameTableIndex; }
			set { SetField(ref _bankNameTableIndex, value, () => bankNameTableIndex); }
		}

	}
}


