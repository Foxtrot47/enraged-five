using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Physics")]
	public class DoorTuningParams:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private System.Single _OpenThresh;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single OpenThresh {
			get { return _OpenThresh; }
			set { SetField(ref _OpenThresh, value, () => OpenThresh); }
		}

		private System.Single _HeadingThresh;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single HeadingThresh {
			get { return _HeadingThresh; }
			set { SetField(ref _HeadingThresh, value, () => HeadingThresh); }
		}

		private System.Single _ClosedThresh;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single ClosedThresh {
			get { return _ClosedThresh; }
			set { SetField(ref _ClosedThresh, value, () => ClosedThresh); }
		}

		private System.Single _SpeedThresh;
		[ModelLib.CustomAttributes.Default("0.1")]
		public System.Single SpeedThresh {
			get { return _SpeedThresh; }
			set { SetField(ref _SpeedThresh, value, () => SpeedThresh); }
		}

		private System.Single _SpeedScale;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single SpeedScale {
			get { return _SpeedScale; }
			set { SetField(ref _SpeedScale, value, () => SpeedScale); }
		}

		private System.Single _HeadingDeltaThreshold;
		[ModelLib.CustomAttributes.Default("0.0001")]
		public System.Single HeadingDeltaThreshold {
			get { return _HeadingDeltaThreshold; }
			set { SetField(ref _HeadingDeltaThreshold, value, () => HeadingDeltaThreshold); }
		}

		private System.Single _AngularVelocityThreshold;
		[ModelLib.CustomAttributes.Default("0.003")]
		public System.Single AngularVelocityThreshold {
			get { return _AngularVelocityThreshold; }
			set { SetField(ref _AngularVelocityThreshold, value, () => AngularVelocityThreshold); }
		}

		private DoorTypes _DoorType;
		[ModelLib.CustomAttributes.Default("AUD_DOOR_TYPE_STD")]
		public DoorTypes DoorType {
			get { return _DoorType; }
			set { SetField(ref _DoorType, value, () => DoorType); }
		}

		private ModelLib.Types.TriState _IsRailLevelCrossing;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState IsRailLevelCrossing {
			get { return _IsRailLevelCrossing; }
			set { SetField(ref _IsRailLevelCrossing, value, () => IsRailLevelCrossing); }
		}

	}
}


