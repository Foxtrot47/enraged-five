using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	public class Preset:  ModelBase {

		public class VariableDefinition: ModelBase {
			private System.String _Name;
			public System.String Name {
				get { return _Name; }
				set { SetField(ref _Name, value, () => Name); }
			}

			private System.Single _Value;
			[ModelLib.CustomAttributes.Default("0")]
			public System.Single Value {
				get { return _Value; }
				set { SetField(ref _Value, value, () => Value); }
			}

			private System.Single _Variance;
			[ModelLib.CustomAttributes.Default("0")]
			public System.Single Variance {
				get { return _Variance; }
				set { SetField(ref _Variance, value, () => Variance); }
			}

		}
		private ItemsObservableCollection<VariableDefinition> _Variable = new ItemsObservableCollection<VariableDefinition>();
		[ModelLib.CustomAttributes.MaxSize(128)]
		[XmlElement("Variable")]
		public ItemsObservableCollection<VariableDefinition> Variable {
			get { return _Variable; }
			set { SetField(ref _Variable, value, () => Variable); }
		}

	}
}


