using ModelLib.CustomAttributes;

namespace model {

	public enum SpeechAudibilityType {
		[ModelLib.CustomAttributes.Display("Normal")]
		CONTEXT_AUDIBILITY_NORMAL,
		[ModelLib.CustomAttributes.Display("Clear")]
		CONTEXT_AUDIBILITY_CLEAR,
		[ModelLib.CustomAttributes.Display("Critical")]
		CONTEXT_AUDIBILITY_CRITICAL,
		[ModelLib.CustomAttributes.Display("LeadIn")]
		CONTEXT_AUDIBILITY_LEAD_IN
	}

}