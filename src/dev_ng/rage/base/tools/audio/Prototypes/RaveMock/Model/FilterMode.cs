using ModelLib.CustomAttributes;

namespace model {

	public enum FilterMode {
		[ModelLib.CustomAttributes.Display("LowPass2Pole")]
		kLowPass2Pole,
		[ModelLib.CustomAttributes.Display("HighPass2Pole")]
		kHighPass2Pole,
		[ModelLib.CustomAttributes.Display("BandPass2Pole")]
		kBandPass2Pole,
		[ModelLib.CustomAttributes.Display("BandStop2Pole")]
		kBandStop2Pole,
		[ModelLib.CustomAttributes.Display("LowPass4Pole")]
		kLowPass4Pole,
		[ModelLib.CustomAttributes.Display("HighPass4Pole")]
		kHighPass4Pole,
		[ModelLib.CustomAttributes.Display("BandPass4Pole")]
		kBandPass4Pole,
		[ModelLib.CustomAttributes.Display("BandStop4Pole")]
		kBandStop4Pole,
		[ModelLib.CustomAttributes.Display("PeakingEQ")]
		kPeakingEQ,
		[ModelLib.CustomAttributes.Display("LowShelf2Pole")]
		kLowShelf2Pole,
		[ModelLib.CustomAttributes.Display("LowShelf4Pole")]
		kLowShelf4Pole,
		[ModelLib.CustomAttributes.Display("HighShelf2Pole")]
		kHighShelf2Pole,
		[ModelLib.CustomAttributes.Display("HighShelf4Pole")]
		kHighShelf4Pole
	}

}