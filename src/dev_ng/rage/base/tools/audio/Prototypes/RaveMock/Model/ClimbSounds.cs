using ModelLib.CustomAttributes;

namespace model {

	public enum ClimbSounds {
		[ModelLib.CustomAttributes.Display("Concrete")]
		CLIMB_SOUNDS_CONCRETE,
		[ModelLib.CustomAttributes.Display("ChainLink")]
		CLIMB_SOUNDS_CHAINLINK,
		[ModelLib.CustomAttributes.Display("Metal")]
		CLIMB_SOUNDS_METAL,
		[ModelLib.CustomAttributes.Display("Wood")]
		CLIMB_SOUNDS_WOOD
	}

}