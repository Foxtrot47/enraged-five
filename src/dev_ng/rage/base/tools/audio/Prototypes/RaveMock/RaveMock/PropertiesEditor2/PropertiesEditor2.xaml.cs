﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModelLib;

namespace RaveMock
{
    /// <summary>
    /// Interaction logic for PropertiesEditor2.xaml
    /// </summary>
    public partial class PropertiesEditor2 : UserControl
    {
        public PropertiesEditor2()
        {
            InitializeComponent();
        }

        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_DockableContent;
        public Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable Content
        {
            get
            {
                return this.m_DockableContent
                       ?? (this.m_DockableContent =
                           new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
                           {
                               Title = "PropertiesEditor2",
                               Content = this,
                               ContentId = this.Name
                           });
            }
        }

        public void Activate()
        {
            this.Content.Show();
        }

        private PropertiesEditorViewModel m_viewModel;
        private void PropertiesEditor2_OnLoaded(object sender, RoutedEventArgs e)
        {
            m_viewModel = new PropertiesEditorViewModel(new PropertiesEditorModel());
            //m_viewModel.PropertyChanged += OnViewModelPropertyChanged;
            this.DataContext = this.m_viewModel;
            //OnViewModelPropertyChanged(null, new PropertyChangedEventArgs("IsCheckedOut"));
            m_viewModel.EditObject();
        }

        private void EventSetter_OnHandler(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void EditObject(ModelInstanceData modelObject)
        {
            Dictionary<string, ReflectedFieldInstanceInfo> modelData = reflectInstance(modelObject);
            //todo set modelData as model for mvvm pattern
            return;
        }

        public class ReflectedFieldInstanceInfo

        {
            private Type Type;
            private object Value;

            public ReflectedFieldInstanceInfo(Type type, object value)
            {
                this.Type = type;
                this.Value = value;
            }
        }

        public Dictionary<string, ReflectedFieldInstanceInfo> reflectInstance(ModelInstanceData modelInstance)
        {
            return getClassInfo(modelInstance.InstanceType, modelInstance.Instance);
        }

        private ReflectedFieldInstanceInfo getClassPropertyInfo(PropertyInfo propertyInfo, object instance)
        {
            Type cpiType = null;
            object cpiObject = null;
            if (propertyInfo.PropertyType.IsSubclassOf(typeof(ModelBase)))
            {
                cpiType = typeof(Dictionary<string, ReflectedFieldInstanceInfo>);
                cpiObject = getClassInfo(propertyInfo.PropertyType, propertyInfo.GetValue(instance));

            }
            else if (typeof(ICollection).IsAssignableFrom(propertyInfo.PropertyType))
            {
                cpiType = typeof(ObservableCollection<Dictionary<string, ReflectedFieldInstanceInfo>>);
                cpiObject = new ObservableCollection<Dictionary<string, ReflectedFieldInstanceInfo>>();
                foreach (object element in ((ICollection)propertyInfo.GetValue(instance)))
                {
                    ((ObservableCollection<Dictionary<string, ReflectedFieldInstanceInfo>>)cpiObject).Add(
                        getClassInfo(element.GetType(), element));
                }

            }
            else
            {
                cpiType = propertyInfo.PropertyType;
                cpiObject = propertyInfo.GetValue(instance);
            }
            ReflectedFieldInstanceInfo cpi = new ReflectedFieldInstanceInfo(cpiType, cpiObject);
            return cpi;
        }

        private Dictionary<string, ReflectedFieldInstanceInfo> getClassInfo(Type classType, object instance)
        {
            Dictionary<string, ReflectedFieldInstanceInfo> classInfo = new Dictionary<string, ReflectedFieldInstanceInfo>();
            foreach (PropertyInfo propertyInfo in classType.GetProperties())
            {
                ReflectedFieldInstanceInfo cpi = getClassPropertyInfo(propertyInfo, instance);
                classInfo.Add(propertyInfo.Name, cpi);
            }

            return classInfo;
        }
    }
}
