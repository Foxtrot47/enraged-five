using ModelLib.CustomAttributes;

namespace model {

	public enum SlotLoadType {
		[ModelLib.CustomAttributes.Display("Bank")]
		SLOT_LOAD_TYPE_BANK,
		[ModelLib.CustomAttributes.Display("Wave")]
		SLOT_LOAD_TYPE_WAVE,
		[ModelLib.CustomAttributes.Display("Stream")]
		SLOT_LOAD_TYPE_STREAM
	}

}