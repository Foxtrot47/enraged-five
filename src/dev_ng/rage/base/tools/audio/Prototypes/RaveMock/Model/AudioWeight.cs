using ModelLib.CustomAttributes;

namespace model {

	public enum AudioWeight {
		[ModelLib.CustomAttributes.Display("Very light")]
		AUDIO_WEIGHT_VL,
		[ModelLib.CustomAttributes.Display("Light")]
		AUDIO_WEIGHT_L,
		[ModelLib.CustomAttributes.Display("Medium")]
		AUDIO_WEIGHT_M,
		[ModelLib.CustomAttributes.Display("Heavy")]
		AUDIO_WEIGHT_H,
		[ModelLib.CustomAttributes.Display("Very heavy")]
		AUDIO_WEIGHT_VH
	}

}