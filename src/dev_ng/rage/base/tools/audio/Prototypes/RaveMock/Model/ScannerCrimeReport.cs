using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Scanner")]
	public class ScannerCrimeReport:  ModelBase {

		private ModelLib.ObjectRef _GenericInstructionSndRef;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GenericInstructionSndRef {
			get { return _GenericInstructionSndRef; }
			set { SetField(ref _GenericInstructionSndRef, value, () => GenericInstructionSndRef); }
		}

		private ModelLib.ObjectRef _PedsAroundInstructionSndRef;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedsAroundInstructionSndRef {
			get { return _PedsAroundInstructionSndRef; }
			set { SetField(ref _PedsAroundInstructionSndRef, value, () => PedsAroundInstructionSndRef); }
		}

		private ModelLib.ObjectRef _PoliceAroundInstructionSndRef;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PoliceAroundInstructionSndRef {
			get { return _PoliceAroundInstructionSndRef; }
			set { SetField(ref _PoliceAroundInstructionSndRef, value, () => PoliceAroundInstructionSndRef); }
		}

		private System.Single _AcknowledgeSituationProbability;
		[ModelLib.CustomAttributes.Default("0.5")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single AcknowledgeSituationProbability {
			get { return _AcknowledgeSituationProbability; }
			set { SetField(ref _AcknowledgeSituationProbability, value, () => AcknowledgeSituationProbability); }
		}

		private ModelLib.ObjectRef _SmallCrimeSoundRef;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SmallCrimeSoundRef {
			get { return _SmallCrimeSoundRef; }
			set { SetField(ref _SmallCrimeSoundRef, value, () => SmallCrimeSoundRef); }
		}

		public class CrimeSetDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundRef;
			[ModelLib.CustomAttributes.Default("NULL_SOUND")]
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef SoundRef {
				get { return _SoundRef; }
				set { SetField(ref _SoundRef, value, () => SoundRef); }
			}

			private System.Single _Weight;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single Weight {
				get { return _Weight; }
				set { SetField(ref _Weight, value, () => Weight); }
			}

		}
		private ItemsObservableCollection<CrimeSetDefinition> _CrimeSet = new ItemsObservableCollection<CrimeSetDefinition>();
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("CrimeSet")]
		public ItemsObservableCollection<CrimeSetDefinition> CrimeSet {
			get { return _CrimeSet; }
			set { SetField(ref _CrimeSet, value, () => CrimeSet); }
		}

	}
}


