using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class OnStopSound:  Sound{

		private ModelLib.ObjectRef _ChildSoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DefaultDropField]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef ChildSoundRef {
			get { return _ChildSoundRef; }
			set { SetField(ref _ChildSoundRef, value, () => ChildSoundRef); }
		}

		private ModelLib.ObjectRef _StopSoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef StopSoundRef {
			get { return _StopSoundRef; }
			set { SetField(ref _StopSoundRef, value, () => StopSoundRef); }
		}

		private ModelLib.ObjectRef _FinishedSoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef FinishedSoundRef {
			get { return _FinishedSoundRef; }
			set { SetField(ref _FinishedSoundRef, value, () => FinishedSoundRef); }
		}

	}
}


