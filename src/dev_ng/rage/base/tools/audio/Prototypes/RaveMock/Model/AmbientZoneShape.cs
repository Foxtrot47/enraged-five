using ModelLib.CustomAttributes;

namespace model {

	public enum AmbientZoneShape {
		[ModelLib.CustomAttributes.Display("Cuboid")]
		kAmbientZoneCuboid,
		[ModelLib.CustomAttributes.Display("Sphere")]
		kAmbientZoneSphere,
		[ModelLib.CustomAttributes.Display("Cuboid/Line Emitter")]
		kAmbientZoneCuboidLineEmitter,
		[ModelLib.CustomAttributes.Display("Sphere/Line Emitter")]
		kAmbientZoneSphereLineEmitter
	}

}