using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class Linear:  BaseCurve{

		public class LeftHandPairDefinition: ModelBase {
			private System.Single _x;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Display("x")]
			public System.Single x {
				get { return _x; }
				set { SetField(ref _x, value, () => x); }
			}

			private System.Single _y;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Display("y")]
			public System.Single y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

		}
		private LeftHandPairDefinition _LeftHandPair;
		public LeftHandPairDefinition LeftHandPair {
			get { return _LeftHandPair; }
			set { SetField(ref _LeftHandPair, value, () => LeftHandPair); }
		}

		public class RightHandPairDefinition: ModelBase {
			private System.Single _x;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Display("x")]
			public System.Single x {
				get { return _x; }
				set { SetField(ref _x, value, () => x); }
			}

			private System.Single _y;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Display("y")]
			public System.Single y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

		}
		private RightHandPairDefinition _RightHandPair;
		public RightHandPairDefinition RightHandPair {
			get { return _RightHandPair; }
			set { SetField(ref _RightHandPair, value, () => RightHandPair); }
		}

	}
}


