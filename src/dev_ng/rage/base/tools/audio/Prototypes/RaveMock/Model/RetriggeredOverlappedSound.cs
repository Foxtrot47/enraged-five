using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class RetriggeredOverlappedSound:  Sound{

		private System.Int16 _LoopCount;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Max(512)]
		[ModelLib.CustomAttributes.Min(-1)]
		public System.Int16 LoopCount {
			get { return _LoopCount; }
			set { SetField(ref _LoopCount, value, () => LoopCount); }
		}

		private System.UInt16 _LoopCountVariance;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Description("Loop count will be set to LoopCount +/- variance")]
		public System.UInt16 LoopCountVariance {
			get { return _LoopCountVariance; }
			set { SetField(ref _LoopCountVariance, value, () => LoopCountVariance); }
		}

		private System.UInt16 _DelayTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(65535)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt16 DelayTime {
			get { return _DelayTime; }
			set { SetField(ref _DelayTime, value, () => DelayTime); }
		}

		private System.UInt16 _DelayTimeVariance;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Description("DelayTime will be set to DelayTime +/- variance.  Note that this variance will be constant during the lifetime of the sound, ie the delay time will not vary each time round.")]
		[ModelLib.CustomAttributes.Max(65535)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt16 DelayTimeVariance {
			get { return _DelayTimeVariance; }
			set { SetField(ref _DelayTimeVariance, value, () => DelayTimeVariance); }
		}

		private System.String _LoopCountVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String LoopCountVariable {
			get { return _LoopCountVariable; }
			set { SetField(ref _LoopCountVariable, value, () => LoopCountVariable); }
		}

		private System.String _DelayTimeVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String DelayTimeVariable {
			get { return _DelayTimeVariable; }
			set { SetField(ref _DelayTimeVariable, value, () => DelayTimeVariable); }
		}

		private ModelLib.ObjectRef _StartSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef StartSound {
			get { return _StartSound; }
			set { SetField(ref _StartSound, value, () => StartSound); }
		}

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DefaultDropField]
		[ModelLib.CustomAttributes.Display("RepeatSound")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		private ModelLib.ObjectRef _StopSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef StopSound {
			get { return _StopSound; }
			set { SetField(ref _StopSound, value, () => StopSound); }
		}

		private ModelLib.Types.TriState _ReleaseChildren;
		[ModelLib.CustomAttributes.Description("Release a child sound when delay time is up (i.e. when the next child is kicked off)")]
		public ModelLib.Types.TriState ReleaseChildren {
			get { return _ReleaseChildren; }
			set { SetField(ref _ReleaseChildren, value, () => ReleaseChildren); }
		}

		private ModelLib.Types.TriState _AllowChildToFinish;
		[ModelLib.CustomAttributes.Description("Allow child to finish playing when told to release")]
		public ModelLib.Types.TriState AllowChildToFinish {
			get { return _AllowChildToFinish; }
			set { SetField(ref _AllowChildToFinish, value, () => AllowChildToFinish); }
		}

		private ModelLib.Types.TriState _SetLoopCountVariableOnce;
		[ModelLib.CustomAttributes.Description("Should be true on sounds with codeset loopcount variable.")]
		public ModelLib.Types.TriState SetLoopCountVariableOnce {
			get { return _SetLoopCountVariableOnce; }
			set { SetField(ref _SetLoopCountVariableOnce, value, () => SetLoopCountVariableOnce); }
		}

	}
}


