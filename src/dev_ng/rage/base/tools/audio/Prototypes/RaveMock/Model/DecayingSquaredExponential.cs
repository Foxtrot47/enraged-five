using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class DecayingSquaredExponential:  BaseCurve{

		private System.Single _HorizontalScaling;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Display("Horizontal Scaling")]
		public System.Single HorizontalScaling {
			get { return _HorizontalScaling; }
			set { SetField(ref _HorizontalScaling, value, () => HorizontalScaling); }
		}

		private ModelLib.Types.TriState _ClampToZeroAtOne;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState ClampToZeroAtOne {
			get { return _ClampToZeroAtOne; }
			set { SetField(ref _ClampToZeroAtOne, value, () => ClampToZeroAtOne); }
		}

	}
}


