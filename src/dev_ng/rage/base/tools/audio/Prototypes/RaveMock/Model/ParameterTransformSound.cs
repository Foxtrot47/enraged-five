using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Logic Sound Types")]
	public class ParameterTransformSound:  Sound{

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		public class ParameterTransformDefinition: ModelBase {
			private System.String _InputVariable;
			[ModelLib.CustomAttributes.Unit("variable")]
			public System.String InputVariable {
				get { return _InputVariable; }
				set { SetField(ref _InputVariable, value, () => InputVariable); }
			}

			public class InputRangeDefinition: ModelBase {
				private System.Single _Min;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single Min {
					get { return _Min; }
					set { SetField(ref _Min, value, () => Min); }
				}

				private System.Single _Max;
				[ModelLib.CustomAttributes.Default("1.0")]
				public System.Single Max {
					get { return _Max; }
					set { SetField(ref _Max, value, () => Max); }
				}

			}
			private InputRangeDefinition _InputRange;
			public InputRangeDefinition InputRange {
				get { return _InputRange; }
				set { SetField(ref _InputRange, value, () => InputRange); }
			}

			public class OutputDefinition: ModelBase {
				private System.Single _InputSmoothRate;
				[ModelLib.CustomAttributes.Default("-1")]
				[ModelLib.CustomAttributes.Description("Time taken for input to traverse entire range, in seconds. -1 means no smoothing will be applied")]
				[ModelLib.CustomAttributes.Max(120)]
				[ModelLib.CustomAttributes.Min(-1)]
				[ModelLib.CustomAttributes.Unit("seconds")]
				public System.Single InputSmoothRate {
					get { return _InputSmoothRate; }
					set { SetField(ref _InputSmoothRate, value, () => InputSmoothRate); }
				}

				private ParameterDestinations _Destination;
				[ModelLib.CustomAttributes.Default("PARAM_DESTINATION_VOLUME")]
				public ParameterDestinations Destination {
					get { return _Destination; }
					set { SetField(ref _Destination, value, () => Destination); }
				}

				private System.Byte _padding0;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Hidden]
				public System.Byte padding0 {
					get { return _padding0; }
					set { SetField(ref _padding0, value, () => padding0); }
				}

				private System.Byte _padding1;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Hidden]
				public System.Byte padding1 {
					get { return _padding1; }
					set { SetField(ref _padding1, value, () => padding1); }
				}

				private System.Byte _padding2;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Hidden]
				public System.Byte padding2 {
					get { return _padding2; }
					set { SetField(ref _padding2, value, () => padding2); }
				}

				private System.String _OutputVariable;
				[ModelLib.CustomAttributes.OutputVariable]
				[ModelLib.CustomAttributes.Unit("variable")]
				public System.String OutputVariable {
					get { return _OutputVariable; }
					set { SetField(ref _OutputVariable, value, () => OutputVariable); }
				}

				public class OutputRangeDefinition: ModelBase {
					private System.Single _Min;
					[ModelLib.CustomAttributes.Default("0.0")]
					public System.Single Min {
						get { return _Min; }
						set { SetField(ref _Min, value, () => Min); }
					}

					private System.Single _Max;
					[ModelLib.CustomAttributes.Default("1.0")]
					public System.Single Max {
						get { return _Max; }
						set { SetField(ref _Max, value, () => Max); }
					}

				}
				private OutputRangeDefinition _OutputRange;
				public OutputRangeDefinition OutputRange {
					get { return _OutputRange; }
					set { SetField(ref _OutputRange, value, () => OutputRange); }
				}

				public class TransformPointsDefinition: ModelBase {
					private System.Single _x;
					[ModelLib.CustomAttributes.Max(1.0)]
					[ModelLib.CustomAttributes.Min(0.0)]
					public System.Single x {
						get { return _x; }
						set { SetField(ref _x, value, () => x); }
					}

					private System.Single _y;
					[ModelLib.CustomAttributes.Max(1.0)]
					[ModelLib.CustomAttributes.Min(0.0)]
					public System.Single y {
						get { return _y; }
						set { SetField(ref _y, value, () => y); }
					}

				}
				private ItemsObservableCollection<TransformPointsDefinition> _TransformPoints = new ItemsObservableCollection<TransformPointsDefinition>();
				[ModelLib.CustomAttributes.MaxSize(16, typeof(System.UInt32))]
				[XmlElement("TransformPoints")]
				public ItemsObservableCollection<TransformPointsDefinition> TransformPoints {
					get { return _TransformPoints; }
					set { SetField(ref _TransformPoints, value, () => TransformPoints); }
				}

			}
			private ItemsObservableCollection<OutputDefinition> _Output = new ItemsObservableCollection<OutputDefinition>();
			[ModelLib.CustomAttributes.MaxSize(4, typeof(System.UInt32))]
			[XmlElement("Output")]
			public ItemsObservableCollection<OutputDefinition> Output {
				get { return _Output; }
				set { SetField(ref _Output, value, () => Output); }
			}

		}
		private ItemsObservableCollection<ParameterTransformDefinition> _ParameterTransform = new ItemsObservableCollection<ParameterTransformDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16, typeof(System.UInt32))]
		[XmlElement("ParameterTransform")]
		public ItemsObservableCollection<ParameterTransformDefinition> ParameterTransform {
			get { return _ParameterTransform; }
			set { SetField(ref _ParameterTransform, value, () => ParameterTransform); }
		}

	}
}


