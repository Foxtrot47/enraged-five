using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class VariationGroupInfoData:  ModelBase {

		public class VariationDataDefinition: ModelBase {
			private System.Byte _val;
			[ModelLib.CustomAttributes.Default("0")]
			public System.Byte val {
				get { return _val; }
				set { SetField(ref _val, value, () => val); }
			}

		}
		private ItemsObservableCollection<VariationDataDefinition> _VariationData = new ItemsObservableCollection<VariationDataDefinition>();
		[ModelLib.CustomAttributes.MaxSize(255)]
		[XmlElement("VariationData")]
		public ItemsObservableCollection<VariationDataDefinition> VariationData {
			get { return _VariationData; }
			set { SetField(ref _VariationData, value, () => VariationData); }
		}

	}
}


