using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class VehicleEngineAudioSettings:  ModelBase {

		private System.Int32 _MasterVolume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 MasterVolume {
			get { return _MasterVolume; }
			set { SetField(ref _MasterVolume, value, () => MasterVolume); }
		}

		private System.Int32 _MaxConeAttenuation;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 MaxConeAttenuation {
			get { return _MaxConeAttenuation; }
			set { SetField(ref _MaxConeAttenuation, value, () => MaxConeAttenuation); }
		}

		private System.Int32 _FXCompensation;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(3600)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 FXCompensation {
			get { return _FXCompensation; }
			set { SetField(ref _FXCompensation, value, () => FXCompensation); }
		}

		private System.Int32 _NonPlayerFXComp;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(3600)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 NonPlayerFXComp {
			get { return _NonPlayerFXComp; }
			set { SetField(ref _NonPlayerFXComp, value, () => NonPlayerFXComp); }
		}

		private ModelLib.ObjectRef _LowEngineLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LowEngineLoop {
			get { return _LowEngineLoop; }
			set { SetField(ref _LowEngineLoop, value, () => LowEngineLoop); }
		}

		private ModelLib.ObjectRef _HighEngineLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HighEngineLoop {
			get { return _HighEngineLoop; }
			set { SetField(ref _HighEngineLoop, value, () => HighEngineLoop); }
		}

		private ModelLib.ObjectRef _LowExhaustLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LowExhaustLoop {
			get { return _LowExhaustLoop; }
			set { SetField(ref _LowExhaustLoop, value, () => LowExhaustLoop); }
		}

		private ModelLib.ObjectRef _HighExhaustLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HighExhaustLoop {
			get { return _HighExhaustLoop; }
			set { SetField(ref _HighExhaustLoop, value, () => HighExhaustLoop); }
		}

		private ModelLib.ObjectRef _RevsOffLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RevsOffLoop {
			get { return _RevsOffLoop; }
			set { SetField(ref _RevsOffLoop, value, () => RevsOffLoop); }
		}

		private System.Int32 _MinPitch;
		[ModelLib.CustomAttributes.Default("-500")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 MinPitch {
			get { return _MinPitch; }
			set { SetField(ref _MinPitch, value, () => MinPitch); }
		}

		private System.Int32 _MaxPitch;
		[ModelLib.CustomAttributes.Default("1000")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 MaxPitch {
			get { return _MaxPitch; }
			set { SetField(ref _MaxPitch, value, () => MaxPitch); }
		}

		private ModelLib.ObjectRef _IdleEngineSimpleLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Idle")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IdleEngineSimpleLoop {
			get { return _IdleEngineSimpleLoop; }
			set { SetField(ref _IdleEngineSimpleLoop, value, () => IdleEngineSimpleLoop); }
		}

		private ModelLib.ObjectRef _IdleExhaustSimpleLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Idle")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IdleExhaustSimpleLoop {
			get { return _IdleExhaustSimpleLoop; }
			set { SetField(ref _IdleExhaustSimpleLoop, value, () => IdleExhaustSimpleLoop); }
		}

		private System.Int32 _IdleMinPitch;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Idle")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 IdleMinPitch {
			get { return _IdleMinPitch; }
			set { SetField(ref _IdleMinPitch, value, () => IdleMinPitch); }
		}

		private System.Int32 _IdleMaxPitch;
		[ModelLib.CustomAttributes.Default("1500")]
		[ModelLib.CustomAttributes.DisplayGroup("Idle")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 IdleMaxPitch {
			get { return _IdleMaxPitch; }
			set { SetField(ref _IdleMaxPitch, value, () => IdleMaxPitch); }
		}

		private ModelLib.ObjectRef _InductionLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Intake")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef InductionLoop {
			get { return _InductionLoop; }
			set { SetField(ref _InductionLoop, value, () => InductionLoop); }
		}

		private System.Int32 _InductionMinPitch;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Intake")]
		[ModelLib.CustomAttributes.Unit("cents")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Int32 InductionMinPitch {
			get { return _InductionMinPitch; }
			set { SetField(ref _InductionMinPitch, value, () => InductionMinPitch); }
		}

		private System.Int32 _InductionMaxPitch;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Intake")]
		[ModelLib.CustomAttributes.Unit("cents")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Int32 InductionMaxPitch {
			get { return _InductionMaxPitch; }
			set { SetField(ref _InductionMaxPitch, value, () => InductionMaxPitch); }
		}

		private ModelLib.ObjectRef _TurboWhine;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Intake")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TurboWhine {
			get { return _TurboWhine; }
			set { SetField(ref _TurboWhine, value, () => TurboWhine); }
		}

		private System.Int32 _TurboMinPitch;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Intake")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 TurboMinPitch {
			get { return _TurboMinPitch; }
			set { SetField(ref _TurboMinPitch, value, () => TurboMinPitch); }
		}

		private System.Int32 _TurboMaxPitch;
		[ModelLib.CustomAttributes.Default("1200")]
		[ModelLib.CustomAttributes.DisplayGroup("Intake")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 TurboMaxPitch {
			get { return _TurboMaxPitch; }
			set { SetField(ref _TurboMaxPitch, value, () => TurboMaxPitch); }
		}

		private ModelLib.ObjectRef _DumpValve;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Intake")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DumpValve {
			get { return _DumpValve; }
			set { SetField(ref _DumpValve, value, () => DumpValve); }
		}

		private System.UInt32 _DumpValveProb;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.DisplayGroup("Intake")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt32 DumpValveProb {
			get { return _DumpValveProb; }
			set { SetField(ref _DumpValveProb, value, () => DumpValveProb); }
		}

		private System.UInt32 _TurboSpinupSpeed;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.DisplayGroup("Intake")]
		[ModelLib.CustomAttributes.Max(1000)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt32 TurboSpinupSpeed {
			get { return _TurboSpinupSpeed; }
			set { SetField(ref _TurboSpinupSpeed, value, () => TurboSpinupSpeed); }
		}

		private ModelLib.ObjectRef _GearTransLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Transmission")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GearTransLoop {
			get { return _GearTransLoop; }
			set { SetField(ref _GearTransLoop, value, () => GearTransLoop); }
		}

		private System.Int32 _GearTransMinPitch;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Transmission")]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 GearTransMinPitch {
			get { return _GearTransMinPitch; }
			set { SetField(ref _GearTransMinPitch, value, () => GearTransMinPitch); }
		}

		private System.Int32 _GearTransMaxPitch;
		[ModelLib.CustomAttributes.Default("600")]
		[ModelLib.CustomAttributes.DisplayGroup("Transmission")]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 GearTransMaxPitch {
			get { return _GearTransMaxPitch; }
			set { SetField(ref _GearTransMaxPitch, value, () => GearTransMaxPitch); }
		}

		private System.UInt32 _GTThrottleVol;
		[ModelLib.CustomAttributes.Default("300")]
		[ModelLib.CustomAttributes.DisplayGroup("Transmission")]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.UInt32 GTThrottleVol {
			get { return _GTThrottleVol; }
			set { SetField(ref _GTThrottleVol, value, () => GTThrottleVol); }
		}

		private ModelLib.ObjectRef _Ignition;
		[ModelLib.CustomAttributes.Default("IGNITION")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Ignition {
			get { return _Ignition; }
			set { SetField(ref _Ignition, value, () => Ignition); }
		}

		private ModelLib.ObjectRef _EngineShutdown;
		[ModelLib.CustomAttributes.Default("VEHICLES_ENGINE_RESIDENT_SHUT_DOWN_1")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineShutdown {
			get { return _EngineShutdown; }
			set { SetField(ref _EngineShutdown, value, () => EngineShutdown); }
		}

		private ModelLib.ObjectRef _CoolingFan;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CoolingFan {
			get { return _CoolingFan; }
			set { SetField(ref _CoolingFan, value, () => CoolingFan); }
		}

		private ModelLib.ObjectRef _ExhaustPops;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustPops {
			get { return _ExhaustPops; }
			set { SetField(ref _ExhaustPops, value, () => ExhaustPops); }
		}

		private ModelLib.ObjectRef _StartLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef StartLoop {
			get { return _StartLoop; }
			set { SetField(ref _StartLoop, value, () => StartLoop); }
		}

		private System.Int32 _MasterTurboVolume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Intake")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 MasterTurboVolume {
			get { return _MasterTurboVolume; }
			set { SetField(ref _MasterTurboVolume, value, () => MasterTurboVolume); }
		}

		private System.Int32 _MasterTransmissionVolume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Transmission")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 MasterTransmissionVolume {
			get { return _MasterTransmissionVolume; }
			set { SetField(ref _MasterTransmissionVolume, value, () => MasterTransmissionVolume); }
		}

		private ModelLib.ObjectRef _EngineStartUp;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineStartUp {
			get { return _EngineStartUp; }
			set { SetField(ref _EngineStartUp, value, () => EngineStartUp); }
		}

		private ModelLib.ObjectRef _EngineSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSynthDef {
			get { return _EngineSynthDef; }
			set { SetField(ref _EngineSynthDef, value, () => EngineSynthDef); }
		}

		private ModelLib.ObjectRef _EngineSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSynthPreset {
			get { return _EngineSynthPreset; }
			set { SetField(ref _EngineSynthPreset, value, () => EngineSynthPreset); }
		}

		private ModelLib.ObjectRef _ExhaustSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSynthDef {
			get { return _ExhaustSynthDef; }
			set { SetField(ref _ExhaustSynthDef, value, () => ExhaustSynthDef); }
		}

		private ModelLib.ObjectRef _ExhaustSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSynthPreset {
			get { return _ExhaustSynthPreset; }
			set { SetField(ref _ExhaustSynthPreset, value, () => ExhaustSynthPreset); }
		}

		private ModelLib.ObjectRef _EngineSubmixVoice;
		[ModelLib.CustomAttributes.Default("DEFAULT_CAR_ENGINE_SUBMIX_CONTROL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSubmixVoice {
			get { return _EngineSubmixVoice; }
			set { SetField(ref _EngineSubmixVoice, value, () => EngineSubmixVoice); }
		}

		private ModelLib.ObjectRef _ExhaustSubmixVoice;
		[ModelLib.CustomAttributes.Default("DEFAULT_CAR_EXHAUST_SUBMIX_CONTROL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSubmixVoice {
			get { return _ExhaustSubmixVoice; }
			set { SetField(ref _ExhaustSubmixVoice, value, () => ExhaustSubmixVoice); }
		}

		private System.Int32 _UpgradedTransmissionVolumeBoost;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 UpgradedTransmissionVolumeBoost {
			get { return _UpgradedTransmissionVolumeBoost; }
			set { SetField(ref _UpgradedTransmissionVolumeBoost, value, () => UpgradedTransmissionVolumeBoost); }
		}

		private ModelLib.ObjectRef _UpgradedGearChangeInt;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedGearChangeInt {
			get { return _UpgradedGearChangeInt; }
			set { SetField(ref _UpgradedGearChangeInt, value, () => UpgradedGearChangeInt); }
		}

		private ModelLib.ObjectRef _UpgradedGearChangeExt;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedGearChangeExt {
			get { return _UpgradedGearChangeExt; }
			set { SetField(ref _UpgradedGearChangeExt, value, () => UpgradedGearChangeExt); }
		}

		private System.Int32 _UpgradedEngineVolumeBoost_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 UpgradedEngineVolumeBoost_PostSubmix {
			get { return _UpgradedEngineVolumeBoost_PostSubmix; }
			set { SetField(ref _UpgradedEngineVolumeBoost_PostSubmix, value, () => UpgradedEngineVolumeBoost_PostSubmix); }
		}

		private ModelLib.ObjectRef _UpgradedEngineSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedEngineSynthDef {
			get { return _UpgradedEngineSynthDef; }
			set { SetField(ref _UpgradedEngineSynthDef, value, () => UpgradedEngineSynthDef); }
		}

		private ModelLib.ObjectRef _UpgradedEngineSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedEngineSynthPreset {
			get { return _UpgradedEngineSynthPreset; }
			set { SetField(ref _UpgradedEngineSynthPreset, value, () => UpgradedEngineSynthPreset); }
		}

		private System.Int32 _UpgradedExhaustVolumeBoost_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 UpgradedExhaustVolumeBoost_PostSubmix {
			get { return _UpgradedExhaustVolumeBoost_PostSubmix; }
			set { SetField(ref _UpgradedExhaustVolumeBoost_PostSubmix, value, () => UpgradedExhaustVolumeBoost_PostSubmix); }
		}

		private ModelLib.ObjectRef _UpgradedExhaustSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedExhaustSynthDef {
			get { return _UpgradedExhaustSynthDef; }
			set { SetField(ref _UpgradedExhaustSynthDef, value, () => UpgradedExhaustSynthDef); }
		}

		private ModelLib.ObjectRef _UpgradedExhaustSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedExhaustSynthPreset {
			get { return _UpgradedExhaustSynthPreset; }
			set { SetField(ref _UpgradedExhaustSynthPreset, value, () => UpgradedExhaustSynthPreset); }
		}

		private ModelLib.ObjectRef _UpgradedDumpValve;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedDumpValve {
			get { return _UpgradedDumpValve; }
			set { SetField(ref _UpgradedDumpValve, value, () => UpgradedDumpValve); }
		}

		private System.Int32 _UpgradedTurboVolumeBoost;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 UpgradedTurboVolumeBoost {
			get { return _UpgradedTurboVolumeBoost; }
			set { SetField(ref _UpgradedTurboVolumeBoost, value, () => UpgradedTurboVolumeBoost); }
		}

		private ModelLib.ObjectRef _UpgradedGearTransLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedGearTransLoop {
			get { return _UpgradedGearTransLoop; }
			set { SetField(ref _UpgradedGearTransLoop, value, () => UpgradedGearTransLoop); }
		}

		private ModelLib.ObjectRef _UpgradedTurboWhine;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedTurboWhine {
			get { return _UpgradedTurboWhine; }
			set { SetField(ref _UpgradedTurboWhine, value, () => UpgradedTurboWhine); }
		}

		private ModelLib.ObjectRef _UpgradedInductionLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedInductionLoop {
			get { return _UpgradedInductionLoop; }
			set { SetField(ref _UpgradedInductionLoop, value, () => UpgradedInductionLoop); }
		}

		private ModelLib.ObjectRef _UpgradedExhaustPops;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedExhaustPops {
			get { return _UpgradedExhaustPops; }
			set { SetField(ref _UpgradedExhaustPops, value, () => UpgradedExhaustPops); }
		}

	}
}


