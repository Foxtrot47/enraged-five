﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation.Peers;
using System.Windows.Forms.Integration;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using ModelLib;
using ModelLib.Types;

namespace RaveMock
{
    public class SoundBrowser : ContainerControl
    {
        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_DockableContent;

        string dataBaseDir = @"X:\rdr3\src\dev\rage\base\tools\audio\Prototypes\RaveMock\RaveMock\data\DLC_MPHeist";
        string pathToModel = @"X:\rdr3\src\dev\rage\base\tools\audio\Prototypes\RaveMock\RaveMock\model\model.dll";

        TreeView content = new TreeView();

        public delegate void editObjectDelegate(ModelInstanceData instance);
        private editObjectDelegate editObject;
        public SoundBrowser(editObjectDelegate editObject)
        {
            this.editObject = editObject;

            content.Height = 300;
            content.Width = 100;

            content.NodeMouseDoubleClick += content_NodeMouseDoubleClick;
            content.ContextMenu = new ContextMenu(new MenuItem[]{new MenuItem("Reload", OnRefreshClick), new MenuItem("Save", OnSaveClick)});

            reloadSoundTree();

            
        }

	    private void OnSaveClick(object sender, EventArgs e)
	    {
		    
	    }

	    private void OnRefreshClick(object sender, EventArgs eventArgs)
        {
            reloadSoundTree();
        }

        void content_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
	        if (e.Node.Tag != null && e.Node.Tag is ModelInstanceData)

	        {
		        editObject(((ModelInstanceData) e.Node.Tag));
				saveObject((ModelInstanceData) e.Node.Tag)
	        }

        }

        private void reloadSoundTree()
        {
            content.BeginUpdate();
            content.Nodes.Clear();

            DateTime startTime = DateTime.Now;

            /**
             * foreach(audMetadata soundMetadata in soundMetaDataEntries){
             *      
             *      Dictionary<string, ModelInstanceData> sounds = loadSounds(soundMetadata.DataPath);
             * }
             */


            //string soundMetadataDataPath = "Sounds";
            //TreeNode sounds = new TreeNode(soundMetadataDataPath);
            //content.Nodes.Add(sounds);

            //createNodeFromPath(new string[]{ dataBaseDir }, content.Nodes.Add("data"));
            
            
            TreeNode rootNode = new TreeNode("data");
            List<Task> taskList = new List<Task>();

            List<string> dataDirectories = new List<string>(Directory.GetDirectories(dataBaseDir));

            if (dataDirectories.Contains(Path.Combine(dataBaseDir, "Core")))
            {
                dataDirectories.Remove(Path.Combine(dataBaseDir, "Core"));
                TreeNode coreNode = rootNode.Nodes.Add("Core");
                TreeNode audioNode = coreNode.Nodes.Add("Audio");
                foreach (string subdir in Directory.GetDirectories(Path.Combine(Path.Combine(dataBaseDir, "Core"), "Audio")))
                {
                    Task t = createTask(Directory.GetDirectories(subdir), audioNode.Nodes.Add(new DirectoryInfo(subdir).Name));
                    t.Start();
                    taskList.Add(t);
                }
            }
            Task t3 = createTask(dataDirectories.ToArray(), rootNode);
            t3.Start();
            taskList.Add(t3);

            foreach (Task t in taskList)
            {
                t.Wait();
            }

            content.Nodes.Add(rootNode);

            content.Sort();
            MessageBox.Show((DateTime.Now - startTime).ToString());

            content.EndUpdate();
        }

        private Task createTask(string[] paths, TreeNode parent)
        {
            Task t = new Task(() => createNodeFromPath(paths, parent));
            return t;
        }

        private void createNodeFromPath(string[] paths, TreeNode parent)
        {
            foreach (string path in paths)
            {
                if (Directory.Exists(path))
                {
                    string directory = path;
                    createNodeFromPath(Directory.GetFileSystemEntries(directory),
                        parent.Nodes.Add(new DirectoryInfo(directory).Name));
                }
                if (File.Exists(path))
                {
                    string file = path;
                    if (file.ToUpper().Contains(".XML"))
                    {
                        loadSounds(file, parent.Nodes.Add(Path.GetFileNameWithoutExtension(file)));

                    }
                }
            }
        }

        public Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable Content
        {
            get
            {
                return this.m_DockableContent
                       ?? (this.m_DockableContent =
                           new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
                               {
                                   Title = "SoundBrowser",
                                   Content = new WindowsFormsHost { Child = content },
                                   ContentId = content.Name
                               });
            }
        }

        public void Activate()
        {
            this.Content.Show();
        }


        public void loadSounds(string filePath, TreeNode parent)
        {
            Assembly modelAssembly = Assembly.LoadFile(pathToModel);

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            settings.ConformanceLevel = ConformanceLevel.Document;
            using (XmlReader reader = XmlTextReader.Create(filePath, settings))
            {
                reader.MoveToContent();
                if (!reader.Name.Equals("Objects")) throw new Exception("Error: Malformed object store XML - " + filePath);

                Dictionary<string, TreeNode> folderLookup = new Dictionary<string, TreeNode>();

                bool skipped = false;
                //prevent read() from executing if skip() has been called
                while (skipped || reader.Read())
                {
                    skipped = false;
                    reader.MoveToContent();
                    if(reader.Name.Equals("Objects")) continue;
                    string name = reader["name"];
                    string modelClassName = "model." + reader.Name;
                    //ignore template instances for mock application
                    if (modelClassName.IndexOf("TemplateInstance", StringComparison.OrdinalIgnoreCase) >= 0) 
                    {
                        reader.Skip();
                        skipped = true;
                        continue;
                    }
                    string folder = null;
                    TreeNode actualParent = parent;
                    if (reader["folder"] != null)
                    {
                        folder = reader["folder"];
                        if (folderLookup.ContainsKey(folder)) actualParent = folderLookup[folder];
                        else
                        {
                            actualParent = new TreeNode(folder);
                            parent.Nodes.Add(actualParent);
                            folderLookup.Add(folder, actualParent);
                        }
                    }

                    Type modelClassType = modelAssembly.GetType(modelClassName);
                    //Console.Out.WriteLine("Deserialize "+name+" "+modelClassName+" ("+filePath+")");
                    if (modelClassType == null)
                    {
                        Console.Out.WriteLine("No type found for " + modelClassName);
                        continue;
                    }
                    XmlSerializer serializer = new XmlSerializer(modelClassType);

                    try
                    {
                        XmlReader reader2 = reader.ReadSubtree();
                        //reader2.MoveToContent();
                        //Console.Out.Write(reader2.ReadInnerXml());
                        ModelBase modelObject = (ModelBase)serializer.Deserialize(reader2);
                        ModelInstanceData data = new ModelInstanceData(name, modelClassType, modelObject, filePath, folder);
                        TreeNode instanceNode = new TreeNode(name);
                        instanceNode.Tag = data;
                        actualParent.Nodes.Add(instanceNode);
                    }
                    catch (Exception e)
                    {
                        string error = "Failed to deserialize instance of " + modelClassName + " from file " + filePath;
                        Console.Out.WriteLine(error);
                        MessageBox.Show(error);
                        continue;
                    }
                }
 
            }
        }

    }

    }
