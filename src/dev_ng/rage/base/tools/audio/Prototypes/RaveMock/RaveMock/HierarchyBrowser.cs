﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.Integration;
using System.Windows.Forms;

namespace RaveMock
{
     public class HierarchyBrowser : ContainerControl
    {
        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_DockableContent;

         public TextBox content = new TextBox();

         public HierarchyBrowser()
         {
             content.Height = 300;
             content.Width = 100;
             content.Text = "Content of the dummy HierarchyBrowser control";
         }

         public Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable Content
        {
            get
            {
                return this.m_DockableContent
                       ?? (this.m_DockableContent =
                           new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
                               {
                                   Title = "HierarchyBrowser",
                                   Content = new WindowsFormsHost { Child = content },
                                   ContentId = content.Name
                               });
            }
        }

        public void Activate()
        {
            this.Content.Show();
        }
    }
}
