using ModelLib.CustomAttributes;

namespace model {

	public enum ConditionTypes {
		[ModelLib.CustomAttributes.Display("Less Than")]
		IF_CONDITION_LESS_THAN,
		[ModelLib.CustomAttributes.Display("Less Than or Equal To")]
		IF_CONDITION_LESS_THAN_OR_EQUAL_TO,
		[ModelLib.CustomAttributes.Display("Greater Than")]
		IF_CONDITION_GREATER_THAN,
		[ModelLib.CustomAttributes.Display("Greater Than or Equal To")]
		IF_CONDITION_GREATER_THAN_OR_EQUAL_TO,
		[ModelLib.CustomAttributes.Display("Equal To")]
		IF_CONDITION_EQUAL_TO,
		[ModelLib.CustomAttributes.Display("Not Equal To")]
		IF_CONDITION_NOT_EQUAL_TO
	}

}