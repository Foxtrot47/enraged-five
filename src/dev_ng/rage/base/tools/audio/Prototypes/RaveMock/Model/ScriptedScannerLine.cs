using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Scanner")]
	public class ScriptedScannerLine:  ModelBase {

		private ModelLib.Types.TriState _DisableScannerSFX;
		public ModelLib.Types.TriState DisableScannerSFX {
			get { return _DisableScannerSFX; }
			set { SetField(ref _DisableScannerSFX, value, () => DisableScannerSFX); }
		}

		private ModelLib.Types.TriState _EnableEcho;
		public ModelLib.Types.TriState EnableEcho {
			get { return _EnableEcho; }
			set { SetField(ref _EnableEcho, value, () => EnableEcho); }
		}

		public class PhraseDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundRef;
			[ModelLib.CustomAttributes.Default("NULL_SOUND")]
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef SoundRef {
				get { return _SoundRef; }
				set { SetField(ref _SoundRef, value, () => SoundRef); }
			}

			private ScannerSlotType _Slot;
			[ModelLib.CustomAttributes.Default("LARGE_SCANNER_SLOT")]
			public ScannerSlotType Slot {
				get { return _Slot; }
				set { SetField(ref _Slot, value, () => Slot); }
			}

			private System.UInt16 _PostDelay;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Unit("ms")]
			public System.UInt16 PostDelay {
				get { return _PostDelay; }
				set { SetField(ref _PostDelay, value, () => PostDelay); }
			}

		}
		private ItemsObservableCollection<PhraseDefinition> _Phrase = new ItemsObservableCollection<PhraseDefinition>();
		[ModelLib.CustomAttributes.MaxSize(256)]
		[XmlElement("Phrase")]
		public ItemsObservableCollection<PhraseDefinition> Phrase {
			get { return _Phrase; }
			set { SetField(ref _Phrase, value, () => Phrase); }
		}

	}
}


