﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.Integration;
using System.Windows.Forms;

namespace RaveMock
{
     public class UXTimer : ContainerControl
    {
        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_DockableContent;

         TextBox content = new TextBox();

         public UXTimer()
         {
             content.Height = 300;
             content.Width = 100;
             content.Text = "Content of the dummy Activity Log control";
         }

         public Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable Content
        {
            get
            {
                return this.m_DockableContent
                       ?? (this.m_DockableContent =
                           new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
                               {
                                   Title = "Activity Log",
                                   Content = new WindowsFormsHost { Child = content },
                                   FloatingHeight = content.Height,
                                   FloatingWidth = content.Width,
                                   ContentId = content.Name
                               });
            }
        }

        public void Activate()
        {
            this.Content.Show();
        }
    }
}
