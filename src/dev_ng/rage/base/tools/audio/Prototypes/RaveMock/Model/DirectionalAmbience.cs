using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class DirectionalAmbience:  ModelBase {

		private ModelLib.ObjectRef _SoundNorth;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SoundNorth {
			get { return _SoundNorth; }
			set { SetField(ref _SoundNorth, value, () => SoundNorth); }
		}

		private ModelLib.ObjectRef _SoundEast;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SoundEast {
			get { return _SoundEast; }
			set { SetField(ref _SoundEast, value, () => SoundEast); }
		}

		private ModelLib.ObjectRef _SoundSouth;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SoundSouth {
			get { return _SoundSouth; }
			set { SetField(ref _SoundSouth, value, () => SoundSouth); }
		}

		private ModelLib.ObjectRef _SoundWest;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SoundWest {
			get { return _SoundWest; }
			set { SetField(ref _SoundWest, value, () => SoundWest); }
		}

		private System.Single _VolumeSmoothing;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single VolumeSmoothing {
			get { return _VolumeSmoothing; }
			set { SetField(ref _VolumeSmoothing, value, () => VolumeSmoothing); }
		}

		private ModelLib.ObjectRef _TimeToVolume;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TimeToVolume {
			get { return _TimeToVolume; }
			set { SetField(ref _TimeToVolume, value, () => TimeToVolume); }
		}

		private ModelLib.ObjectRef _OcclusionToVol;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef OcclusionToVol {
			get { return _OcclusionToVol; }
			set { SetField(ref _OcclusionToVol, value, () => OcclusionToVol); }
		}

		private ModelLib.ObjectRef _HeightToCutOff;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HeightToCutOff {
			get { return _HeightToCutOff; }
			set { SetField(ref _HeightToCutOff, value, () => HeightToCutOff); }
		}

		private ModelLib.ObjectRef _OcclusionToCutOff;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef OcclusionToCutOff {
			get { return _OcclusionToCutOff; }
			set { SetField(ref _OcclusionToCutOff, value, () => OcclusionToCutOff); }
		}

		private ModelLib.ObjectRef _BuiltUpFactorToVol;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BuiltUpFactorToVol {
			get { return _BuiltUpFactorToVol; }
			set { SetField(ref _BuiltUpFactorToVol, value, () => BuiltUpFactorToVol); }
		}

		private ModelLib.ObjectRef _BuildingDensityToVol;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BuildingDensityToVol {
			get { return _BuildingDensityToVol; }
			set { SetField(ref _BuildingDensityToVol, value, () => BuildingDensityToVol); }
		}

		private ModelLib.ObjectRef _TreeDensityToVol;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TreeDensityToVol {
			get { return _TreeDensityToVol; }
			set { SetField(ref _TreeDensityToVol, value, () => TreeDensityToVol); }
		}

		private ModelLib.ObjectRef _WaterFactorToVol;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WaterFactorToVol {
			get { return _WaterFactorToVol; }
			set { SetField(ref _WaterFactorToVol, value, () => WaterFactorToVol); }
		}

		private ModelLib.Types.TriState _MonoPannedSource;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState MonoPannedSource {
			get { return _MonoPannedSource; }
			set { SetField(ref _MonoPannedSource, value, () => MonoPannedSource); }
		}

		private ModelLib.Types.TriState _StopWhenRaining;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState StopWhenRaining {
			get { return _StopWhenRaining; }
			set { SetField(ref _StopWhenRaining, value, () => StopWhenRaining); }
		}

		private System.Single _InstanceVolumeScale;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single InstanceVolumeScale {
			get { return _InstanceVolumeScale; }
			set { SetField(ref _InstanceVolumeScale, value, () => InstanceVolumeScale); }
		}

		private ModelLib.ObjectRef _HeightAboveBlanketToVol;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HeightAboveBlanketToVol {
			get { return _HeightAboveBlanketToVol; }
			set { SetField(ref _HeightAboveBlanketToVol, value, () => HeightAboveBlanketToVol); }
		}

		private ModelLib.ObjectRef _HighwayFactorToVol;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HighwayFactorToVol {
			get { return _HighwayFactorToVol; }
			set { SetField(ref _HighwayFactorToVol, value, () => HighwayFactorToVol); }
		}

		private ModelLib.ObjectRef _VehicleCountToVol;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehicleCountToVol {
			get { return _VehicleCountToVol; }
			set { SetField(ref _VehicleCountToVol, value, () => VehicleCountToVol); }
		}

		private System.Single _MaxDistanceOutToSea;
		[ModelLib.CustomAttributes.Default("-1.0")]
		public System.Single MaxDistanceOutToSea {
			get { return _MaxDistanceOutToSea; }
			set { SetField(ref _MaxDistanceOutToSea, value, () => MaxDistanceOutToSea); }
		}

	}
}


