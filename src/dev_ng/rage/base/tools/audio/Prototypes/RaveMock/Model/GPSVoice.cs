using ModelLib.CustomAttributes;

namespace model {

	public enum GPSVoice {
		[ModelLib.CustomAttributes.Display("Female")]
		GPS_VOICE_FEMALE,
		[ModelLib.CustomAttributes.Display("Male")]
		GPS_VOICE_MALE,
		[ModelLib.CustomAttributes.Display("Extra1")]
		GPS_VOICE_EXTRA1,
		[ModelLib.CustomAttributes.Display("Extra2")]
		GPS_VOICE_EXTRA2,
		[ModelLib.CustomAttributes.Display("Extra3")]
		GPS_VOICE_EXTRA3
	}

}