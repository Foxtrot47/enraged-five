using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class TrainStation:  ModelBase {

		private ModelLib.ObjectRef _StationNameSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef StationNameSound {
			get { return _StationNameSound; }
			set { SetField(ref _StationNameSound, value, () => StationNameSound); }
		}

		private ModelLib.ObjectRef _TransferSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TransferSound {
			get { return _TransferSound; }
			set { SetField(ref _TransferSound, value, () => TransferSound); }
		}

		private ModelLib.ObjectRef _Letter;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Letter {
			get { return _Letter; }
			set { SetField(ref _Letter, value, () => Letter); }
		}

		private ModelLib.ObjectRef _ApproachingStation;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ApproachingStation {
			get { return _ApproachingStation; }
			set { SetField(ref _ApproachingStation, value, () => ApproachingStation); }
		}

		private ModelLib.ObjectRef _SpareSound1;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		[ModelLib.CustomAttributes.Hidden]
		public ModelLib.ObjectRef SpareSound1 {
			get { return _SpareSound1; }
			set { SetField(ref _SpareSound1, value, () => SpareSound1); }
		}

		private ModelLib.ObjectRef _SpareSound2;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		[ModelLib.CustomAttributes.Hidden]
		public ModelLib.ObjectRef SpareSound2 {
			get { return _SpareSound2; }
			set { SetField(ref _SpareSound2, value, () => SpareSound2); }
		}

		private System.String _StationName;
		public System.String StationName {
			get { return _StationName; }
			set { SetField(ref _StationName, value, () => StationName); }
		}

	}
}


