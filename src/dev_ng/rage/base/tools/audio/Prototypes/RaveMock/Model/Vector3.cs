using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class Vector3:  ModelBase {

		public class ValueDefinition: ModelBase {
			private System.Single _x;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single x {
				get { return _x; }
				set { SetField(ref _x, value, () => x); }
			}

			private System.Single _y;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

			private System.Single _z;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single z {
				get { return _z; }
				set { SetField(ref _z, value, () => z); }
			}

		}
		private ValueDefinition _Value;
		[ModelLib.CustomAttributes.Unit("vector3")]
		public ValueDefinition Value {
			get { return _Value; }
			set { SetField(ref _Value, value, () => Value); }
		}

	}
}


