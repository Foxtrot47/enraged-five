using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Logic Sound Types")]
	public class VariableCurveSound:  Sound{

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		private System.String _InputVariable;
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String InputVariable {
			get { return _InputVariable; }
			set { SetField(ref _InputVariable, value, () => InputVariable); }
		}

		private System.String _OutputVariable;
		[ModelLib.CustomAttributes.OutputVariable]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String OutputVariable {
			get { return _OutputVariable; }
			set { SetField(ref _OutputVariable, value, () => OutputVariable); }
		}

		private ModelLib.ObjectRef _Curve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Curve {
			get { return _Curve; }
			set { SetField(ref _Curve, value, () => Curve); }
		}

	}
}


