using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class TwinLoopSound:  Sound{

		private ModelLib.Types.TriState _IgnoreStoppedChild;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState IgnoreStoppedChild {
			get { return _IgnoreStoppedChild; }
			set { SetField(ref _IgnoreStoppedChild, value, () => IgnoreStoppedChild); }
		}

		private System.UInt16 _MinSwapTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt16 MinSwapTime {
			get { return _MinSwapTime; }
			set { SetField(ref _MinSwapTime, value, () => MinSwapTime); }
		}

		private System.UInt16 _MaxSwapTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt16 MaxSwapTime {
			get { return _MaxSwapTime; }
			set { SetField(ref _MaxSwapTime, value, () => MaxSwapTime); }
		}

		private System.UInt16 _MinCrossfadeTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt16 MinCrossfadeTime {
			get { return _MinCrossfadeTime; }
			set { SetField(ref _MinCrossfadeTime, value, () => MinCrossfadeTime); }
		}

		private System.UInt16 _MaxCrossfadeTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt16 MaxCrossfadeTime {
			get { return _MaxCrossfadeTime; }
			set { SetField(ref _MaxCrossfadeTime, value, () => MaxCrossfadeTime); }
		}

		private ModelLib.ObjectRef _CrossfadeCurve;
		[ModelLib.CustomAttributes.Default("EQUAL_POWER_RISE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CrossfadeCurve {
			get { return _CrossfadeCurve; }
			set { SetField(ref _CrossfadeCurve, value, () => CrossfadeCurve); }
		}

		private System.String _MinSwapTimeVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String MinSwapTimeVariable {
			get { return _MinSwapTimeVariable; }
			set { SetField(ref _MinSwapTimeVariable, value, () => MinSwapTimeVariable); }
		}

		private System.String _MaxSwapTimeVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String MaxSwapTimeVariable {
			get { return _MaxSwapTimeVariable; }
			set { SetField(ref _MaxSwapTimeVariable, value, () => MaxSwapTimeVariable); }
		}

		private System.String _MinCrossfadeTimeVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String MinCrossfadeTimeVariable {
			get { return _MinCrossfadeTimeVariable; }
			set { SetField(ref _MinCrossfadeTimeVariable, value, () => MinCrossfadeTimeVariable); }
		}

		private System.String _MaxCrossfadeTimeVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String MaxCrossfadeTimeVariable {
			get { return _MaxCrossfadeTimeVariable; }
			set { SetField(ref _MaxCrossfadeTimeVariable, value, () => MaxCrossfadeTimeVariable); }
		}

		public class SoundRefDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundId;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef SoundId {
				get { return _SoundId; }
				set { SetField(ref _SoundId, value, () => SoundId); }
			}

			private System.String _ReferenceName;
			[ModelLib.CustomAttributes.Ignore]
			[ModelLib.CustomAttributes.Hidden]
			public System.String ReferenceName {
				get { return _ReferenceName; }
				set { SetField(ref _ReferenceName, value, () => ReferenceName); }
			}

		}
		private ItemsObservableCollection<SoundRefDefinition> _SoundRef = new ItemsObservableCollection<SoundRefDefinition>();
		[ModelLib.CustomAttributes.MaxSize(2)]
		[XmlElement("SoundRef")]
		public ItemsObservableCollection<SoundRefDefinition> SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

	}
}


