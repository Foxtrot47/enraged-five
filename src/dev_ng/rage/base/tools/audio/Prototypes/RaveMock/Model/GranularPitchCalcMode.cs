using ModelLib.CustomAttributes;

namespace model {

	public enum GranularPitchCalcMode {
		[ModelLib.CustomAttributes.Display("Channel 0")]
		kGranularPitchMapChannel0,
		[ModelLib.CustomAttributes.Display("Channel 1")]
		kGranularPitchMapChannel1,
		[ModelLib.CustomAttributes.Display("Channel 2")]
		kGranularPitchMapChannel2,
		[ModelLib.CustomAttributes.Display("Channel 3")]
		kGranularPitchMapChannel3,
		[ModelLib.CustomAttributes.Display("Channel 4")]
		kGranularPitchMapChannel4,
		[ModelLib.CustomAttributes.Display("Channel 5")]
		kGranularPitchMapChannel5,
		[ModelLib.CustomAttributes.Display("Clamp")]
		kGranularPitchMapClamp,
		[ModelLib.CustomAttributes.Display("Directional Clamp")]
		kGranularPitchMapDirectionalClamp,
		[ModelLib.CustomAttributes.Display("Directional Min/Max")]
		kGranularPitchMapDirectionalMinMax,
		[ModelLib.CustomAttributes.Display("Average")]
		kGranularPitchMapAverage
	}

}