using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class AmbientZoneList:  ModelBase {

		public class ZoneDefinition: ModelBase {
			private ModelLib.ObjectRef _Ref;
			[ModelLib.CustomAttributes.AllowedType(typeof(AmbientZone))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Ref {
				get { return _Ref; }
				set { SetField(ref _Ref, value, () => Ref); }
			}

		}
		private ItemsObservableCollection<ZoneDefinition> _Zone = new ItemsObservableCollection<ZoneDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1000)]
		[XmlElement("Zone")]
		public ItemsObservableCollection<ZoneDefinition> Zone {
			get { return _Zone; }
			set { SetField(ref _Zone, value, () => Zone); }
		}

	}
}


