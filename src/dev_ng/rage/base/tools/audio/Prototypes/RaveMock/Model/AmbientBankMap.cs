using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class AmbientBankMap:  ModelBase {

		public class BankMapDefinition: ModelBase {
			private System.String _BankName;
			public System.String BankName {
				get { return _BankName; }
				set { SetField(ref _BankName, value, () => BankName); }
			}

			private System.String _SlotType;
			public System.String SlotType {
				get { return _SlotType; }
				set { SetField(ref _SlotType, value, () => SlotType); }
			}

		}
		private ItemsObservableCollection<BankMapDefinition> _BankMap = new ItemsObservableCollection<BankMapDefinition>();
		[ModelLib.CustomAttributes.MaxSize(255)]
		[XmlElement("BankMap")]
		public ItemsObservableCollection<BankMapDefinition> BankMap {
			get { return _BankMap; }
			set { SetField(ref _BankMap, value, () => BankMap); }
		}

	}
}


