using ModelLib.CustomAttributes;

namespace model {

	public enum BankingStyle {
		[ModelLib.CustomAttributes.Display("Rotation Angle")]
		kRotationAngle,
		[ModelLib.CustomAttributes.Display("Rotation Speed")]
		kRotationSpeed
	}

}