using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class CarRecordingList:  ModelBase {

		public class CarRecordingDefinition: ModelBase {
			private System.String _Name;
			public System.String Name {
				get { return _Name; }
				set { SetField(ref _Name, value, () => Name); }
			}

			private ModelLib.ObjectRef _CarRecordingAudioSettings;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef CarRecordingAudioSettings {
				get { return _CarRecordingAudioSettings; }
				set { SetField(ref _CarRecordingAudioSettings, value, () => CarRecordingAudioSettings); }
			}

		}
		private ItemsObservableCollection<CarRecordingDefinition> _CarRecording = new ItemsObservableCollection<CarRecordingDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1000)]
		[XmlElement("CarRecording")]
		public ItemsObservableCollection<CarRecordingDefinition> CarRecording {
			get { return _CarRecording; }
			set { SetField(ref _CarRecording, value, () => CarRecording); }
		}

	}
}


