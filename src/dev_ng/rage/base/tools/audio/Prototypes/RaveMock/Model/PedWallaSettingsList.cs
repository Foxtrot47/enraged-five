using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class PedWallaSettingsList:  ModelBase {

		public class PedWallaSettingsInstanceDefinition: ModelBase {
			private ModelLib.ObjectRef _PedWallaSettings;
			[ModelLib.CustomAttributes.AllowedType(typeof(PedWallaSpeechSettings))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef PedWallaSettings {
				get { return _PedWallaSettings; }
				set { SetField(ref _PedWallaSettings, value, () => PedWallaSettings); }
			}

			private System.Single _Weight;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Max(10)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single Weight {
				get { return _Weight; }
				set { SetField(ref _Weight, value, () => Weight); }
			}

			private System.Single _PedDensityThreshold;
			[ModelLib.CustomAttributes.Default("-1.0")]
			[ModelLib.CustomAttributes.Max(1.0)]
			[ModelLib.CustomAttributes.Min(-1.0)]
			public System.Single PedDensityThreshold {
				get { return _PedDensityThreshold; }
				set { SetField(ref _PedDensityThreshold, value, () => PedDensityThreshold); }
			}

			private System.SByte _IsMale;
			[ModelLib.CustomAttributes.Default("-1")]
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(-1)]
			public System.SByte IsMale {
				get { return _IsMale; }
				set { SetField(ref _IsMale, value, () => IsMale); }
			}

			private System.SByte _IsGang;
			[ModelLib.CustomAttributes.Default("-1")]
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(-1)]
			public System.SByte IsGang {
				get { return _IsGang; }
				set { SetField(ref _IsGang, value, () => IsGang); }
			}

		}
		private ItemsObservableCollection<PedWallaSettingsInstanceDefinition> _PedWallaSettingsInstance = new ItemsObservableCollection<PedWallaSettingsInstanceDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16)]
		[XmlElement("PedWallaSettingsInstance")]
		public ItemsObservableCollection<PedWallaSettingsInstanceDefinition> PedWallaSettingsInstance {
			get { return _PedWallaSettingsInstance; }
			set { SetField(ref _PedWallaSettingsInstance, value, () => PedWallaSettingsInstance); }
		}

	}
}


