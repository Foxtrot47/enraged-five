using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class String:  ModelBase {

		private System.String _Value;
		[ModelLib.CustomAttributes.Length(64)]
		public System.String Value {
			get { return _Value; }
			set { SetField(ref _Value, value, () => Value); }
		}

	}
}


