using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class BeatConstraint:  MusicTimingConstraint{

		public class ValidSixteenthsDefinition: ModelBase {
			private ModelLib.Types.Bit __1;
			[ModelLib.CustomAttributes.Default("yes")]
			public ModelLib.Types.Bit _1 {
				get { return __1; }
				set { SetField(ref __1, value, () => _1); }
			}

			private ModelLib.Types.Bit __2;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _2 {
				get { return __2; }
				set { SetField(ref __2, value, () => _2); }
			}

			private ModelLib.Types.Bit __3;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _3 {
				get { return __3; }
				set { SetField(ref __3, value, () => _3); }
			}

			private ModelLib.Types.Bit __4;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _4 {
				get { return __4; }
				set { SetField(ref __4, value, () => _4); }
			}

			private ModelLib.Types.Bit __5;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _5 {
				get { return __5; }
				set { SetField(ref __5, value, () => _5); }
			}

			private ModelLib.Types.Bit __6;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _6 {
				get { return __6; }
				set { SetField(ref __6, value, () => _6); }
			}

			private ModelLib.Types.Bit __7;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _7 {
				get { return __7; }
				set { SetField(ref __7, value, () => _7); }
			}

			private ModelLib.Types.Bit __8;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _8 {
				get { return __8; }
				set { SetField(ref __8, value, () => _8); }
			}

			private ModelLib.Types.Bit __9;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _9 {
				get { return __9; }
				set { SetField(ref __9, value, () => _9); }
			}

			private ModelLib.Types.Bit __10;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _10 {
				get { return __10; }
				set { SetField(ref __10, value, () => _10); }
			}

			private ModelLib.Types.Bit __11;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _11 {
				get { return __11; }
				set { SetField(ref __11, value, () => _11); }
			}

			private ModelLib.Types.Bit __12;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _12 {
				get { return __12; }
				set { SetField(ref __12, value, () => _12); }
			}

			private ModelLib.Types.Bit __13;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _13 {
				get { return __13; }
				set { SetField(ref __13, value, () => _13); }
			}

			private ModelLib.Types.Bit __14;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _14 {
				get { return __14; }
				set { SetField(ref __14, value, () => _14); }
			}

			private ModelLib.Types.Bit __15;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _15 {
				get { return __15; }
				set { SetField(ref __15, value, () => _15); }
			}

			private ModelLib.Types.Bit __16;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit _16 {
				get { return __16; }
				set { SetField(ref __16, value, () => _16); }
			}

		}
		private ValidSixteenthsDefinition _ValidSixteenths;
		public ValidSixteenthsDefinition ValidSixteenths {
			get { return _ValidSixteenths; }
			set { SetField(ref _ValidSixteenths, value, () => ValidSixteenths); }
		}

	}
}


