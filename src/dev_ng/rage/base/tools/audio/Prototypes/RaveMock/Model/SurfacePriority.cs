using ModelLib.CustomAttributes;

namespace model {

	public enum SurfacePriority {
		[ModelLib.CustomAttributes.Display("Gravel")]
		GRAVEL,
		[ModelLib.CustomAttributes.Display("Sand")]
		SAND,
		[ModelLib.CustomAttributes.Display("Grass")]
		GRASS,
		[ModelLib.CustomAttributes.Display("Other")]
		PRIORITY_OTHER,
		[ModelLib.CustomAttributes.Display("Tarmac")]
		TARMAC
	}

}