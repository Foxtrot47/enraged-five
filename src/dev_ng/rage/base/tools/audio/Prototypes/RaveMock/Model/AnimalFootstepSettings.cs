using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class AnimalFootstepSettings:  ModelBase {

		private ModelLib.ObjectRef _WalkAndTrot;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WalkAndTrot {
			get { return _WalkAndTrot; }
			set { SetField(ref _WalkAndTrot, value, () => WalkAndTrot); }
		}

		private ModelLib.ObjectRef _Gallop1;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Gallop1 {
			get { return _Gallop1; }
			set { SetField(ref _Gallop1, value, () => Gallop1); }
		}

		private ModelLib.ObjectRef _Gallop2;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Gallop2 {
			get { return _Gallop2; }
			set { SetField(ref _Gallop2, value, () => Gallop2); }
		}

		private ModelLib.ObjectRef _Scuff;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Scuff {
			get { return _Scuff; }
			set { SetField(ref _Scuff, value, () => Scuff); }
		}

		private ModelLib.ObjectRef _Jump;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Jump {
			get { return _Jump; }
			set { SetField(ref _Jump, value, () => Jump); }
		}

		private ModelLib.ObjectRef _Land;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Land {
			get { return _Land; }
			set { SetField(ref _Land, value, () => Land); }
		}

		private ModelLib.ObjectRef _LandHard;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LandHard {
			get { return _LandHard; }
			set { SetField(ref _LandHard, value, () => LandHard); }
		}

		private ModelLib.ObjectRef _Clumsy;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Clumsy {
			get { return _Clumsy; }
			set { SetField(ref _Clumsy, value, () => Clumsy); }
		}

		private ModelLib.ObjectRef _SlideLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SlideLoop {
			get { return _SlideLoop; }
			set { SetField(ref _SlideLoop, value, () => SlideLoop); }
		}

		private FootstepLoudness _AudioEventLoudness;
		[ModelLib.CustomAttributes.Default("FOOTSTEP_LOUDNESS_MEDIUM")]
		public FootstepLoudness AudioEventLoudness {
			get { return _AudioEventLoudness; }
			set { SetField(ref _AudioEventLoudness, value, () => AudioEventLoudness); }
		}

	}
}


