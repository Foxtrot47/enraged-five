using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class ModelFootStepTuning:  ModelBase {

		private System.Single _FootstepPitchRatioMin;
		[ModelLib.CustomAttributes.Default("-600.0")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		public System.Single FootstepPitchRatioMin {
			get { return _FootstepPitchRatioMin; }
			set { SetField(ref _FootstepPitchRatioMin, value, () => FootstepPitchRatioMin); }
		}

		private System.Single _FootstepPitchRatioMax;
		[ModelLib.CustomAttributes.Default("400.0")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		public System.Single FootstepPitchRatioMax {
			get { return _FootstepPitchRatioMax; }
			set { SetField(ref _FootstepPitchRatioMax, value, () => FootstepPitchRatioMax); }
		}

		private System.Single _InLeftPocketProbability;
		[ModelLib.CustomAttributes.Default("0.5")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single InLeftPocketProbability {
			get { return _InLeftPocketProbability; }
			set { SetField(ref _InLeftPocketProbability, value, () => InLeftPocketProbability); }
		}

		private System.Single _HasKeysProbability;
		[ModelLib.CustomAttributes.Default("0.1")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single HasKeysProbability {
			get { return _HasKeysProbability; }
			set { SetField(ref _HasKeysProbability, value, () => HasKeysProbability); }
		}

		private System.Single _HasMoneyProbability;
		[ModelLib.CustomAttributes.Default("0.1")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single HasMoneyProbability {
			get { return _HasMoneyProbability; }
			set { SetField(ref _HasMoneyProbability, value, () => HasMoneyProbability); }
		}

		public class ModesDefinition: ModelBase {
			private System.String _Name;
			public System.String Name {
				get { return _Name; }
				set { SetField(ref _Name, value, () => Name); }
			}

			private System.Single _MaterialImpactImpulseScale;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Max(1.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single MaterialImpactImpulseScale {
				get { return _MaterialImpactImpulseScale; }
				set { SetField(ref _MaterialImpactImpulseScale, value, () => MaterialImpactImpulseScale); }
			}

			public class ShoeDefinition: ModelBase {
				private System.Single _VolumeOffset;
				[ModelLib.CustomAttributes.Default("0.0")]
				[ModelLib.CustomAttributes.Max(24.0)]
				[ModelLib.CustomAttributes.Min(-100.0)]
				public System.Single VolumeOffset {
					get { return _VolumeOffset; }
					set { SetField(ref _VolumeOffset, value, () => VolumeOffset); }
				}

				private System.UInt32 _LPFCutoff;
				[ModelLib.CustomAttributes.Default("24000")]
				[ModelLib.CustomAttributes.Max(24000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 LPFCutoff {
					get { return _LPFCutoff; }
					set { SetField(ref _LPFCutoff, value, () => LPFCutoff); }
				}

				private System.UInt32 _AttackTime;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Max(10000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 AttackTime {
					get { return _AttackTime; }
					set { SetField(ref _AttackTime, value, () => AttackTime); }
				}

				private System.Single _DragScuffProbability;
				[ModelLib.CustomAttributes.Default("0.1")]
				[ModelLib.CustomAttributes.Max(1.0)]
				[ModelLib.CustomAttributes.Min(0.0)]
				public System.Single DragScuffProbability {
					get { return _DragScuffProbability; }
					set { SetField(ref _DragScuffProbability, value, () => DragScuffProbability); }
				}

			}
			private ItemsObservableCollection<ShoeDefinition> _Shoe = new ItemsObservableCollection<ShoeDefinition>();
			[ModelLib.CustomAttributes.MaxSize(1)]
			[XmlElement("Shoe")]
			public ItemsObservableCollection<ShoeDefinition> Shoe {
				get { return _Shoe; }
				set { SetField(ref _Shoe, value, () => Shoe); }
			}

			public class DirtLayerDefinition: ModelBase {
				private System.Single _VolumeOffset;
				[ModelLib.CustomAttributes.Default("0.0")]
				[ModelLib.CustomAttributes.Max(24.0)]
				[ModelLib.CustomAttributes.Min(-100.0)]
				public System.Single VolumeOffset {
					get { return _VolumeOffset; }
					set { SetField(ref _VolumeOffset, value, () => VolumeOffset); }
				}

				private System.UInt32 _LPFCutoff;
				[ModelLib.CustomAttributes.Default("24000")]
				[ModelLib.CustomAttributes.Max(24000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 LPFCutoff {
					get { return _LPFCutoff; }
					set { SetField(ref _LPFCutoff, value, () => LPFCutoff); }
				}

				private System.UInt32 _AttackTime;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Max(10000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 AttackTime {
					get { return _AttackTime; }
					set { SetField(ref _AttackTime, value, () => AttackTime); }
				}

				private ModelLib.ObjectRef _SweetenerCurve;
				[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
				[ModelLib.CustomAttributes.DisplayGroup("Curves")]
				[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
				public ModelLib.ObjectRef SweetenerCurve {
					get { return _SweetenerCurve; }
					set { SetField(ref _SweetenerCurve, value, () => SweetenerCurve); }
				}

			}
			private ItemsObservableCollection<DirtLayerDefinition> _DirtLayer = new ItemsObservableCollection<DirtLayerDefinition>();
			[ModelLib.CustomAttributes.MaxSize(1)]
			[XmlElement("DirtLayer")]
			public ItemsObservableCollection<DirtLayerDefinition> DirtLayer {
				get { return _DirtLayer; }
				set { SetField(ref _DirtLayer, value, () => DirtLayer); }
			}

			public class CreakLayerDefinition: ModelBase {
				private System.Single _VolumeOffset;
				[ModelLib.CustomAttributes.Default("0.0")]
				[ModelLib.CustomAttributes.Max(24.0)]
				[ModelLib.CustomAttributes.Min(-100.0)]
				public System.Single VolumeOffset {
					get { return _VolumeOffset; }
					set { SetField(ref _VolumeOffset, value, () => VolumeOffset); }
				}

				private System.UInt32 _LPFCutoff;
				[ModelLib.CustomAttributes.Default("24000")]
				[ModelLib.CustomAttributes.Max(24000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 LPFCutoff {
					get { return _LPFCutoff; }
					set { SetField(ref _LPFCutoff, value, () => LPFCutoff); }
				}

				private System.UInt32 _AttackTime;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Max(10000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 AttackTime {
					get { return _AttackTime; }
					set { SetField(ref _AttackTime, value, () => AttackTime); }
				}

				private ModelLib.ObjectRef _SweetenerCurve;
				[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
				[ModelLib.CustomAttributes.DisplayGroup("Curves")]
				[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
				public ModelLib.ObjectRef SweetenerCurve {
					get { return _SweetenerCurve; }
					set { SetField(ref _SweetenerCurve, value, () => SweetenerCurve); }
				}

			}
			private ItemsObservableCollection<CreakLayerDefinition> _CreakLayer = new ItemsObservableCollection<CreakLayerDefinition>();
			[ModelLib.CustomAttributes.MaxSize(1)]
			[XmlElement("CreakLayer")]
			public ItemsObservableCollection<CreakLayerDefinition> CreakLayer {
				get { return _CreakLayer; }
				set { SetField(ref _CreakLayer, value, () => CreakLayer); }
			}

			public class GlassLayerDefinition: ModelBase {
				private System.Single _VolumeOffset;
				[ModelLib.CustomAttributes.Default("0.0")]
				[ModelLib.CustomAttributes.Max(24.0)]
				[ModelLib.CustomAttributes.Min(-100.0)]
				public System.Single VolumeOffset {
					get { return _VolumeOffset; }
					set { SetField(ref _VolumeOffset, value, () => VolumeOffset); }
				}

				private System.UInt32 _LPFCutoff;
				[ModelLib.CustomAttributes.Default("24000")]
				[ModelLib.CustomAttributes.Max(24000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 LPFCutoff {
					get { return _LPFCutoff; }
					set { SetField(ref _LPFCutoff, value, () => LPFCutoff); }
				}

				private System.UInt32 _AttackTime;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Max(10000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 AttackTime {
					get { return _AttackTime; }
					set { SetField(ref _AttackTime, value, () => AttackTime); }
				}

				private ModelLib.ObjectRef _SweetenerCurve;
				[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
				[ModelLib.CustomAttributes.DisplayGroup("Curves")]
				[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
				public ModelLib.ObjectRef SweetenerCurve {
					get { return _SweetenerCurve; }
					set { SetField(ref _SweetenerCurve, value, () => SweetenerCurve); }
				}

			}
			private ItemsObservableCollection<GlassLayerDefinition> _GlassLayer = new ItemsObservableCollection<GlassLayerDefinition>();
			[ModelLib.CustomAttributes.MaxSize(1)]
			[XmlElement("GlassLayer")]
			public ItemsObservableCollection<GlassLayerDefinition> GlassLayer {
				get { return _GlassLayer; }
				set { SetField(ref _GlassLayer, value, () => GlassLayer); }
			}

			public class WetLayerDefinition: ModelBase {
				private System.Single _VolumeOffset;
				[ModelLib.CustomAttributes.Default("0.0")]
				[ModelLib.CustomAttributes.Max(24.0)]
				[ModelLib.CustomAttributes.Min(-100.0)]
				public System.Single VolumeOffset {
					get { return _VolumeOffset; }
					set { SetField(ref _VolumeOffset, value, () => VolumeOffset); }
				}

				private System.UInt32 _LPFCutoff;
				[ModelLib.CustomAttributes.Default("24000")]
				[ModelLib.CustomAttributes.Max(24000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 LPFCutoff {
					get { return _LPFCutoff; }
					set { SetField(ref _LPFCutoff, value, () => LPFCutoff); }
				}

				private System.UInt32 _AttackTime;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Max(10000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 AttackTime {
					get { return _AttackTime; }
					set { SetField(ref _AttackTime, value, () => AttackTime); }
				}

				private ModelLib.ObjectRef _SweetenerCurve;
				[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
				[ModelLib.CustomAttributes.DisplayGroup("Curves")]
				[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
				public ModelLib.ObjectRef SweetenerCurve {
					get { return _SweetenerCurve; }
					set { SetField(ref _SweetenerCurve, value, () => SweetenerCurve); }
				}

			}
			private ItemsObservableCollection<WetLayerDefinition> _WetLayer = new ItemsObservableCollection<WetLayerDefinition>();
			[ModelLib.CustomAttributes.MaxSize(1)]
			[XmlElement("WetLayer")]
			public ItemsObservableCollection<WetLayerDefinition> WetLayer {
				get { return _WetLayer; }
				set { SetField(ref _WetLayer, value, () => WetLayer); }
			}

			public class CustomImpactDefinition: ModelBase {
				private System.Single _VolumeOffset;
				[ModelLib.CustomAttributes.Default("0.0")]
				[ModelLib.CustomAttributes.Max(24.0)]
				[ModelLib.CustomAttributes.Min(-100.0)]
				public System.Single VolumeOffset {
					get { return _VolumeOffset; }
					set { SetField(ref _VolumeOffset, value, () => VolumeOffset); }
				}

				private System.UInt32 _LPFCutoff;
				[ModelLib.CustomAttributes.Default("24000")]
				[ModelLib.CustomAttributes.Max(24000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 LPFCutoff {
					get { return _LPFCutoff; }
					set { SetField(ref _LPFCutoff, value, () => LPFCutoff); }
				}

				private System.UInt32 _AttackTime;
				[ModelLib.CustomAttributes.Default("0")]
				[ModelLib.CustomAttributes.Max(10000)]
				[ModelLib.CustomAttributes.Min(0)]
				public System.UInt32 AttackTime {
					get { return _AttackTime; }
					set { SetField(ref _AttackTime, value, () => AttackTime); }
				}

			}
			private ItemsObservableCollection<CustomImpactDefinition> _CustomImpact = new ItemsObservableCollection<CustomImpactDefinition>();
			[ModelLib.CustomAttributes.MaxSize(1)]
			[XmlElement("CustomImpact")]
			public ItemsObservableCollection<CustomImpactDefinition> CustomImpact {
				get { return _CustomImpact; }
				set { SetField(ref _CustomImpact, value, () => CustomImpact); }
			}

		}
		private ItemsObservableCollection<ModesDefinition> _Modes = new ItemsObservableCollection<ModesDefinition>();
		[ModelLib.CustomAttributes.MaxSize(10)]
		[XmlElement("Modes")]
		public ItemsObservableCollection<ModesDefinition> Modes {
			get { return _Modes; }
			set { SetField(ref _Modes, value, () => Modes); }
		}

	}
}


