using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class BoatAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _Engine1Loop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Engine1Loop {
			get { return _Engine1Loop; }
			set { SetField(ref _Engine1Loop, value, () => Engine1Loop); }
		}

		private ModelLib.ObjectRef _Engine1Vol;
		[ModelLib.CustomAttributes.Default("BOAT_ENGINE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Engine1Vol {
			get { return _Engine1Vol; }
			set { SetField(ref _Engine1Vol, value, () => Engine1Vol); }
		}

		private ModelLib.ObjectRef _Engine1Pitch;
		[ModelLib.CustomAttributes.Default("BOAT_ENGINE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Engine1Pitch {
			get { return _Engine1Pitch; }
			set { SetField(ref _Engine1Pitch, value, () => Engine1Pitch); }
		}

		private ModelLib.ObjectRef _Engine2Loop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Engine2Loop {
			get { return _Engine2Loop; }
			set { SetField(ref _Engine2Loop, value, () => Engine2Loop); }
		}

		private ModelLib.ObjectRef _Engine2Vol;
		[ModelLib.CustomAttributes.Default("BOAT_ENGINE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Engine2Vol {
			get { return _Engine2Vol; }
			set { SetField(ref _Engine2Vol, value, () => Engine2Vol); }
		}

		private ModelLib.ObjectRef _Engine2Pitch;
		[ModelLib.CustomAttributes.Default("BOAT_ENGINE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Engine2Pitch {
			get { return _Engine2Pitch; }
			set { SetField(ref _Engine2Pitch, value, () => Engine2Pitch); }
		}

		private ModelLib.ObjectRef _LowResoLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LowResoLoop {
			get { return _LowResoLoop; }
			set { SetField(ref _LowResoLoop, value, () => LowResoLoop); }
		}

		private ModelLib.ObjectRef _LowResoLoopVol;
		[ModelLib.CustomAttributes.Default("BOAT_ENGINE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LowResoLoopVol {
			get { return _LowResoLoopVol; }
			set { SetField(ref _LowResoLoopVol, value, () => LowResoLoopVol); }
		}

		private ModelLib.ObjectRef _LowResoPitch;
		[ModelLib.CustomAttributes.Default("BOAT_ENGINE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LowResoPitch {
			get { return _LowResoPitch; }
			set { SetField(ref _LowResoPitch, value, () => LowResoPitch); }
		}

		private ModelLib.ObjectRef _ResoLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ResoLoop {
			get { return _ResoLoop; }
			set { SetField(ref _ResoLoop, value, () => ResoLoop); }
		}

		private ModelLib.ObjectRef _ResoLoopVol;
		[ModelLib.CustomAttributes.Default("BOAT_ENGINE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ResoLoopVol {
			get { return _ResoLoopVol; }
			set { SetField(ref _ResoLoopVol, value, () => ResoLoopVol); }
		}

		private ModelLib.ObjectRef _ResoPitch;
		[ModelLib.CustomAttributes.Default("BOAT_ENGINE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ResoPitch {
			get { return _ResoPitch; }
			set { SetField(ref _ResoPitch, value, () => ResoPitch); }
		}

		private ModelLib.ObjectRef _WaterTurbulance;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Water")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WaterTurbulance {
			get { return _WaterTurbulance; }
			set { SetField(ref _WaterTurbulance, value, () => WaterTurbulance); }
		}

		private ModelLib.ObjectRef _WaterTurbulanceVol;
		[ModelLib.CustomAttributes.Default("BOAT_ENGINE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Water")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WaterTurbulanceVol {
			get { return _WaterTurbulanceVol; }
			set { SetField(ref _WaterTurbulanceVol, value, () => WaterTurbulanceVol); }
		}

		private ModelLib.ObjectRef _WaterTurbulancePitch;
		[ModelLib.CustomAttributes.Default("BOAT_WATER_TURBULANCE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Water")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WaterTurbulancePitch {
			get { return _WaterTurbulancePitch; }
			set { SetField(ref _WaterTurbulancePitch, value, () => WaterTurbulancePitch); }
		}

		private ModelLib.ObjectRef _ScannerMake;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScannerMake {
			get { return _ScannerMake; }
			set { SetField(ref _ScannerMake, value, () => ScannerMake); }
		}

		private ModelLib.ObjectRef _ScannerModel;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScannerModel {
			get { return _ScannerModel; }
			set { SetField(ref _ScannerModel, value, () => ScannerModel); }
		}

		private ModelLib.ObjectRef _ScannerCategory;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScannerCategory {
			get { return _ScannerCategory; }
			set { SetField(ref _ScannerCategory, value, () => ScannerCategory); }
		}

		private ModelLib.ObjectRef _ScannerVehicleSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(ScannerVehicleParams))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScannerVehicleSettings {
			get { return _ScannerVehicleSettings; }
			set { SetField(ref _ScannerVehicleSettings, value, () => ScannerVehicleSettings); }
		}

		private RadioType _RadioType;
		[ModelLib.CustomAttributes.Default("RADIO_TYPE_NORMAL")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public RadioType RadioType {
			get { return _RadioType; }
			set { SetField(ref _RadioType, value, () => RadioType); }
		}

		private RadioGenre _RadioGenre;
		[ModelLib.CustomAttributes.Default("RADIO_GENRE_UNSPECIFIED")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public RadioGenre RadioGenre {
			get { return _RadioGenre; }
			set { SetField(ref _RadioGenre, value, () => RadioGenre); }
		}

		private ModelLib.Types.TriState _DisableAmbientRadio;
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public ModelLib.Types.TriState DisableAmbientRadio {
			get { return _DisableAmbientRadio; }
			set { SetField(ref _DisableAmbientRadio, value, () => DisableAmbientRadio); }
		}

		private ModelLib.ObjectRef _HornLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HornLoop {
			get { return _HornLoop; }
			set { SetField(ref _HornLoop, value, () => HornLoop); }
		}

		private ModelLib.ObjectRef _IgnitionOneShot;
		[ModelLib.CustomAttributes.Default("SPEEDBOAT_IGNITION")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IgnitionOneShot {
			get { return _IgnitionOneShot; }
			set { SetField(ref _IgnitionOneShot, value, () => IgnitionOneShot); }
		}

		private ModelLib.ObjectRef _ShutdownOneShot;
		[ModelLib.CustomAttributes.Default("SPEEDBOAT_SHUTDOWN")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ShutdownOneShot {
			get { return _ShutdownOneShot; }
			set { SetField(ref _ShutdownOneShot, value, () => ShutdownOneShot); }
		}

		private System.Int16 _EngineVolPostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 EngineVolPostSubmix {
			get { return _EngineVolPostSubmix; }
			set { SetField(ref _EngineVolPostSubmix, value, () => EngineVolPostSubmix); }
		}

		private System.Int16 _ExhaustVolPostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Engine")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 ExhaustVolPostSubmix {
			get { return _ExhaustVolPostSubmix; }
			set { SetField(ref _ExhaustVolPostSubmix, value, () => ExhaustVolPostSubmix); }
		}

		private ModelLib.ObjectRef _EngineSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSynthDef {
			get { return _EngineSynthDef; }
			set { SetField(ref _EngineSynthDef, value, () => EngineSynthDef); }
		}

		private ModelLib.ObjectRef _EngineSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSynthPreset {
			get { return _EngineSynthPreset; }
			set { SetField(ref _EngineSynthPreset, value, () => EngineSynthPreset); }
		}

		private ModelLib.ObjectRef _ExhaustSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSynthDef {
			get { return _ExhaustSynthDef; }
			set { SetField(ref _ExhaustSynthDef, value, () => ExhaustSynthDef); }
		}

		private ModelLib.ObjectRef _ExhaustSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSynthPreset {
			get { return _ExhaustSynthPreset; }
			set { SetField(ref _ExhaustSynthPreset, value, () => ExhaustSynthPreset); }
		}

		private ModelLib.ObjectRef _VehicleCollisions;
		[ModelLib.CustomAttributes.Default("VEHICLE_COLLISION_BOAT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(VehicleCollisionSettings))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehicleCollisions {
			get { return _VehicleCollisions; }
			set { SetField(ref _VehicleCollisions, value, () => VehicleCollisions); }
		}

		private ModelLib.ObjectRef _EngineSubmixVoice;
		[ModelLib.CustomAttributes.Default("DEFAULT_BOAT_ENGINE_SUBMIX_CONTROL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSubmixVoice {
			get { return _EngineSubmixVoice; }
			set { SetField(ref _EngineSubmixVoice, value, () => EngineSubmixVoice); }
		}

		private ModelLib.ObjectRef _ExhaustSubmixVoice;
		[ModelLib.CustomAttributes.Default("DEFAULT_BOAT_EXHAUST_SUBMIX_CONTROL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSubmixVoice {
			get { return _ExhaustSubmixVoice; }
			set { SetField(ref _ExhaustSubmixVoice, value, () => ExhaustSubmixVoice); }
		}

		private ModelLib.ObjectRef _WaveHitSound;
		[ModelLib.CustomAttributes.Default("BOAT_WAVE_HIT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Water")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WaveHitSound {
			get { return _WaveHitSound; }
			set { SetField(ref _WaveHitSound, value, () => WaveHitSound); }
		}

		private ModelLib.ObjectRef _LeftWaterSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Water")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LeftWaterSound {
			get { return _LeftWaterSound; }
			set { SetField(ref _LeftWaterSound, value, () => LeftWaterSound); }
		}

		private ModelLib.ObjectRef _IdleHullSlapLoop;
		[ModelLib.CustomAttributes.Default("IDLE_HULL_SLAP")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Water")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IdleHullSlapLoop {
			get { return _IdleHullSlapLoop; }
			set { SetField(ref _IdleHullSlapLoop, value, () => IdleHullSlapLoop); }
		}

		private ModelLib.ObjectRef _IdleHullSlapSpeedToVol;
		[ModelLib.CustomAttributes.Default("BOAT_SPEED_TO_HULL_SLAP_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Water")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IdleHullSlapSpeedToVol {
			get { return _IdleHullSlapSpeedToVol; }
			set { SetField(ref _IdleHullSlapSpeedToVol, value, () => IdleHullSlapSpeedToVol); }
		}

		private ModelLib.ObjectRef _GranularEngine;
		[ModelLib.CustomAttributes.AllowedType(typeof(GranularEngineAudioSettings))]
		[ModelLib.CustomAttributes.DisplayGroup("GranularEngine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GranularEngine {
			get { return _GranularEngine; }
			set { SetField(ref _GranularEngine, value, () => GranularEngine); }
		}

		private ModelLib.ObjectRef _BankSpraySound;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Water")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankSpraySound {
			get { return _BankSpraySound; }
			set { SetField(ref _BankSpraySound, value, () => BankSpraySound); }
		}

		private ModelLib.ObjectRef _IgnitionLoop;
		[ModelLib.CustomAttributes.Default("IGNITION")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("GranularEngine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IgnitionLoop {
			get { return _IgnitionLoop; }
			set { SetField(ref _IgnitionLoop, value, () => IgnitionLoop); }
		}

		private ModelLib.Types.TriState _IsSubmarine;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public ModelLib.Types.TriState IsSubmarine {
			get { return _IsSubmarine; }
			set { SetField(ref _IsSubmarine, value, () => IsSubmarine); }
		}

		private ModelLib.ObjectRef _EngineStartUp;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("GranularEngine")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineStartUp {
			get { return _EngineStartUp; }
			set { SetField(ref _EngineStartUp, value, () => EngineStartUp); }
		}

		private ModelLib.ObjectRef _SubTurningEnginePitchModifier;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Submersible")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SubTurningEnginePitchModifier {
			get { return _SubTurningEnginePitchModifier; }
			set { SetField(ref _SubTurningEnginePitchModifier, value, () => SubTurningEnginePitchModifier); }
		}

		private ModelLib.ObjectRef _SubTurningSweetenerSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Submersible")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SubTurningSweetenerSound {
			get { return _SubTurningSweetenerSound; }
			set { SetField(ref _SubTurningSweetenerSound, value, () => SubTurningSweetenerSound); }
		}

		private ModelLib.ObjectRef _RevsToSweetenerVolume;
		[ModelLib.CustomAttributes.Default("CONSTANT_ONE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Submersible")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RevsToSweetenerVolume {
			get { return _RevsToSweetenerVolume; }
			set { SetField(ref _RevsToSweetenerVolume, value, () => RevsToSweetenerVolume); }
		}

		private ModelLib.ObjectRef _TurningToSweetenerVolume;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Submersible")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TurningToSweetenerVolume {
			get { return _TurningToSweetenerVolume; }
			set { SetField(ref _TurningToSweetenerVolume, value, () => TurningToSweetenerVolume); }
		}

		private ModelLib.ObjectRef _TurningToSweetenerPitch;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Submersible")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TurningToSweetenerPitch {
			get { return _TurningToSweetenerPitch; }
			set { SetField(ref _TurningToSweetenerPitch, value, () => TurningToSweetenerPitch); }
		}

		private ModelLib.ObjectRef _DryLandScrape;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DryLandScrape {
			get { return _DryLandScrape; }
			set { SetField(ref _DryLandScrape, value, () => DryLandScrape); }
		}

		private System.Single _MaxRollOffScalePlayer;
		[ModelLib.CustomAttributes.Default("6.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Rolloff")]
		public System.Single MaxRollOffScalePlayer {
			get { return _MaxRollOffScalePlayer; }
			set { SetField(ref _MaxRollOffScalePlayer, value, () => MaxRollOffScalePlayer); }
		}

		private System.Single _MaxRollOffScaleNPC;
		[ModelLib.CustomAttributes.Default("3.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Rolloff")]
		public System.Single MaxRollOffScaleNPC {
			get { return _MaxRollOffScaleNPC; }
			set { SetField(ref _MaxRollOffScaleNPC, value, () => MaxRollOffScaleNPC); }
		}

		private System.Single _Openness;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single Openness {
			get { return _Openness; }
			set { SetField(ref _Openness, value, () => Openness); }
		}

		private ModelLib.ObjectRef _DryLandHardScrape;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DryLandHardScrape {
			get { return _DryLandHardScrape; }
			set { SetField(ref _DryLandHardScrape, value, () => DryLandHardScrape); }
		}

		private ModelLib.ObjectRef _DryLandHardImpact;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DryLandHardImpact {
			get { return _DryLandHardImpact; }
			set { SetField(ref _DryLandHardImpact, value, () => DryLandHardImpact); }
		}

		private ModelLib.ObjectRef _WindClothSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WindClothSound {
			get { return _WindClothSound; }
			set { SetField(ref _WindClothSound, value, () => WindClothSound); }
		}

		private ModelLib.ObjectRef _FireAudio;
		[ModelLib.CustomAttributes.Default("VEH_FIRE_SOUNDSET")]
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FireAudio {
			get { return _FireAudio; }
			set { SetField(ref _FireAudio, value, () => FireAudio); }
		}

		private ModelLib.ObjectRef _DoorOpen;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorOpen {
			get { return _DoorOpen; }
			set { SetField(ref _DoorOpen, value, () => DoorOpen); }
		}

		private ModelLib.ObjectRef _DoorClose;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorClose {
			get { return _DoorClose; }
			set { SetField(ref _DoorClose, value, () => DoorClose); }
		}

		private ModelLib.ObjectRef _DoorLimit;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorLimit {
			get { return _DoorLimit; }
			set { SetField(ref _DoorLimit, value, () => DoorLimit); }
		}

		private ModelLib.ObjectRef _DoorStartClose;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorStartClose {
			get { return _DoorStartClose; }
			set { SetField(ref _DoorStartClose, value, () => DoorStartClose); }
		}

		private ModelLib.ObjectRef _SubExtrasSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Submersible")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SubExtrasSound {
			get { return _SubExtrasSound; }
			set { SetField(ref _SubExtrasSound, value, () => SubExtrasSound); }
		}

		private ModelLib.ObjectRef _SFXBankSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Submersible")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SFXBankSound {
			get { return _SFXBankSound; }
			set { SetField(ref _SFXBankSound, value, () => SFXBankSound); }
		}

		private ModelLib.ObjectRef _SubmersibleCreaksSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Submersible")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SubmersibleCreaksSound {
			get { return _SubmersibleCreaksSound; }
			set { SetField(ref _SubmersibleCreaksSound, value, () => SubmersibleCreaksSound); }
		}

		private ModelLib.ObjectRef _WaveHitBigAirSound;
		[ModelLib.CustomAttributes.Default("BOAT_WAVE_HIT_BIG_AIR")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Water")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WaveHitBigAirSound {
			get { return _WaveHitBigAirSound; }
			set { SetField(ref _WaveHitBigAirSound, value, () => WaveHitBigAirSound); }
		}

		private System.Single _BigAirMinTime;
		[ModelLib.CustomAttributes.Default("1.2")]
		[ModelLib.CustomAttributes.DisplayGroup("Water")]
		[ModelLib.CustomAttributes.Max(10.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single BigAirMinTime {
			get { return _BigAirMinTime; }
			set { SetField(ref _BigAirMinTime, value, () => BigAirMinTime); }
		}

	}
}


