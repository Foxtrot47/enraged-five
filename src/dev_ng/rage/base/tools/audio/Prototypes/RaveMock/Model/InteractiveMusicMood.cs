using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class InteractiveMusicMood:  AudBaseObject{

		private System.Int16 _FadeInTime;
		[ModelLib.CustomAttributes.Default("400")]
		[ModelLib.CustomAttributes.Max(30000)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.Int16 FadeInTime {
			get { return _FadeInTime; }
			set { SetField(ref _FadeInTime, value, () => FadeInTime); }
		}

		private System.Int16 _FadeOutTime;
		[ModelLib.CustomAttributes.Default("3000")]
		[ModelLib.CustomAttributes.Max(30000)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.Int16 FadeOutTime {
			get { return _FadeOutTime; }
			set { SetField(ref _FadeOutTime, value, () => FadeOutTime); }
		}

		private System.Single _AmbMusicDuckingVol;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(0.0)]
		[ModelLib.CustomAttributes.Min(-100.0)]
		public System.Single AmbMusicDuckingVol {
			get { return _AmbMusicDuckingVol; }
			set { SetField(ref _AmbMusicDuckingVol, value, () => AmbMusicDuckingVol); }
		}

		public class StemMixesDefinition: ModelBase {
			private ModelLib.ObjectRef _StemMix;
			[ModelLib.CustomAttributes.AllowedType(typeof(StemMix))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef StemMix {
				get { return _StemMix; }
				set { SetField(ref _StemMix, value, () => StemMix); }
			}

			private ModelLib.ObjectRef _OnTriggerAction;
			[ModelLib.CustomAttributes.AllowedType(typeof(MusicAction))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef OnTriggerAction {
				get { return _OnTriggerAction; }
				set { SetField(ref _OnTriggerAction, value, () => OnTriggerAction); }
			}

			private System.Single _MinDuration;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Unit("seconds")]
			public System.Single MinDuration {
				get { return _MinDuration; }
				set { SetField(ref _MinDuration, value, () => MinDuration); }
			}

			private System.Single _MaxDuration;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Unit("seconds")]
			public System.Single MaxDuration {
				get { return _MaxDuration; }
				set { SetField(ref _MaxDuration, value, () => MaxDuration); }
			}

			private System.Single _FadeTime;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Unit("seconds")]
			public System.Single FadeTime {
				get { return _FadeTime; }
				set { SetField(ref _FadeTime, value, () => FadeTime); }
			}

			private MusicConstraintTypes _Constrain;
			[ModelLib.CustomAttributes.Default("kConstrainStart")]
			public MusicConstraintTypes Constrain {
				get { return _Constrain; }
				set { SetField(ref _Constrain, value, () => Constrain); }
			}

			public class TimingConstraintsDefinition: ModelBase {
				private ModelLib.ObjectRef _Constraint;
				[ModelLib.CustomAttributes.AllowedType(typeof(MusicTimingConstraint))]
				[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
				public ModelLib.ObjectRef Constraint {
					get { return _Constraint; }
					set { SetField(ref _Constraint, value, () => Constraint); }
				}

			}
			private ItemsObservableCollection<TimingConstraintsDefinition> _TimingConstraints = new ItemsObservableCollection<TimingConstraintsDefinition>();
			[ModelLib.CustomAttributes.FixedSize]
			[ModelLib.CustomAttributes.MaxSize(2)]
			[XmlElement("TimingConstraints")]
			public ItemsObservableCollection<TimingConstraintsDefinition> TimingConstraints {
				get { return _TimingConstraints; }
				set { SetField(ref _TimingConstraints, value, () => TimingConstraints); }
			}

		}
		private ItemsObservableCollection<StemMixesDefinition> _StemMixes = new ItemsObservableCollection<StemMixesDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16)]
		[XmlElement("StemMixes")]
		public ItemsObservableCollection<StemMixesDefinition> StemMixes {
			get { return _StemMixes; }
			set { SetField(ref _StemMixes, value, () => StemMixes); }
		}

	}
}


