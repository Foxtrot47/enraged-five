using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class ClothList:  ModelBase {

		public class ClothDefinition: ModelBase {
			private System.String _ClothName;
			public System.String ClothName {
				get { return _ClothName; }
				set { SetField(ref _ClothName, value, () => ClothName); }
			}

			private ModelLib.ObjectRef _ClothAudioSettings;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef ClothAudioSettings {
				get { return _ClothAudioSettings; }
				set { SetField(ref _ClothAudioSettings, value, () => ClothAudioSettings); }
			}

		}
		private ItemsObservableCollection<ClothDefinition> _Cloth = new ItemsObservableCollection<ClothDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1000)]
		[XmlElement("Cloth")]
		public ItemsObservableCollection<ClothDefinition> Cloth {
			get { return _Cloth; }
			set { SetField(ref _Cloth, value, () => Cloth); }
		}

	}
}


