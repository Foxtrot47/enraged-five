using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class EntityEmitter:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private ModelLib.ObjectRef _Sound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Sound {
			get { return _Sound; }
			set { SetField(ref _Sound, value, () => Sound); }
		}

		private System.Single _MaxDistance;
		[ModelLib.CustomAttributes.Default("100.0")]
		[ModelLib.CustomAttributes.Description("Sound will only be played if distance to emitter is less than MaxDistance")]
		[ModelLib.CustomAttributes.Unit("m")]
		public System.Single MaxDistance {
			get { return _MaxDistance; }
			set { SetField(ref _MaxDistance, value, () => MaxDistance); }
		}

		private System.Single _BusinessHoursProb;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Description("Probability that this is on from 9-6")]
		public System.Single BusinessHoursProb {
			get { return _BusinessHoursProb; }
			set { SetField(ref _BusinessHoursProb, value, () => BusinessHoursProb); }
		}

		private System.Single _EveningProb;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Description("probability that this will be active between 6pm and 11pm")]
		public System.Single EveningProb {
			get { return _EveningProb; }
			set { SetField(ref _EveningProb, value, () => EveningProb); }
		}

		private System.Single _NightProb;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Description("probability that this will be active between 11pm and 9am")]
		public System.Single NightProb {
			get { return _NightProb; }
			set { SetField(ref _NightProb, value, () => NightProb); }
		}

		private ModelLib.Types.TriState _OnlyWhenRaining;
		[ModelLib.CustomAttributes.Description("if true then this will only be active when it is raining")]
		public ModelLib.Types.TriState OnlyWhenRaining {
			get { return _OnlyWhenRaining; }
			set { SetField(ref _OnlyWhenRaining, value, () => OnlyWhenRaining); }
		}

		private ModelLib.Types.TriState _MoreLikelyWhenHot;
		[ModelLib.CustomAttributes.Description("If true then this will be more likely to be active when the ambient temperature is above 20 degrees")]
		public ModelLib.Types.TriState MoreLikelyWhenHot {
			get { return _MoreLikelyWhenHot; }
			set { SetField(ref _MoreLikelyWhenHot, value, () => MoreLikelyWhenHot); }
		}

		private ModelLib.Types.TriState _VolumeCone;
		[ModelLib.CustomAttributes.Description("If true then this emitter will be directional")]
		public ModelLib.Types.TriState VolumeCone {
			get { return _VolumeCone; }
			set { SetField(ref _VolumeCone, value, () => VolumeCone); }
		}

		private System.Single _ConeInnerAngle;
		[ModelLib.CustomAttributes.Description("inner cone angle")]
		public System.Single ConeInnerAngle {
			get { return _ConeInnerAngle; }
			set { SetField(ref _ConeInnerAngle, value, () => ConeInnerAngle); }
		}

		private System.Single _ConeOuterAngle;
		[ModelLib.CustomAttributes.Description("cone outer angle")]
		public System.Single ConeOuterAngle {
			get { return _ConeOuterAngle; }
			set { SetField(ref _ConeOuterAngle, value, () => ConeOuterAngle); }
		}

		private System.Single _ConeMaxAtten;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Description("cone maximum attenuation")]
		public System.Single ConeMaxAtten {
			get { return _ConeMaxAtten; }
			set { SetField(ref _ConeMaxAtten, value, () => ConeMaxAtten); }
		}

		private System.UInt32 _StopAfterLoudSound;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Description("Time after a loud sound in this emitters proximity that the emitter will be stopped")]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt32 StopAfterLoudSound {
			get { return _StopAfterLoudSound; }
			set { SetField(ref _StopAfterLoudSound, value, () => StopAfterLoudSound); }
		}

		private ModelLib.Types.TriState _WindBasedVolume;
		[ModelLib.CustomAttributes.Description("if true then the volume will be scaled by the local wind strength in a pleasing wind-like fashion")]
		public ModelLib.Types.TriState WindBasedVolume {
			get { return _WindBasedVolume; }
			set { SetField(ref _WindBasedVolume, value, () => WindBasedVolume); }
		}

		private ModelLib.Types.TriState _GeneratesTreeRain;
		[ModelLib.CustomAttributes.Description("if true then this emitter will be treated as a tree for the rain computation")]
		public ModelLib.Types.TriState GeneratesTreeRain {
			get { return _GeneratesTreeRain; }
			set { SetField(ref _GeneratesTreeRain, value, () => GeneratesTreeRain); }
		}

		private ModelLib.Types.TriState _StopWhenRaining;
		[ModelLib.CustomAttributes.Description("if true then the emitter will be stopped when the rain starts")]
		public ModelLib.Types.TriState StopWhenRaining {
			get { return _StopWhenRaining; }
			set { SetField(ref _StopWhenRaining, value, () => StopWhenRaining); }
		}

		private ModelLib.Types.TriState _ComputeOcclusion;
		[ModelLib.CustomAttributes.Description("if true then the emitter calculate occlusion")]
		public ModelLib.Types.TriState ComputeOcclusion {
			get { return _ComputeOcclusion; }
			set { SetField(ref _ComputeOcclusion, value, () => ComputeOcclusion); }
		}

		private System.Byte _MaxPathDepth;
		[ModelLib.CustomAttributes.Default("3")]
		[ModelLib.CustomAttributes.Max(5)]
		[ModelLib.CustomAttributes.Min(1)]
		public System.Byte MaxPathDepth {
			get { return _MaxPathDepth; }
			set { SetField(ref _MaxPathDepth, value, () => MaxPathDepth); }
		}

		private System.Single _BrokenHealth;
		[ModelLib.CustomAttributes.Default("-1.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(-1.0)]
		public System.Single BrokenHealth {
			get { return _BrokenHealth; }
			set { SetField(ref _BrokenHealth, value, () => BrokenHealth); }
		}

	}
}


