using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class ReflectionsSettings:  ModelBase {

		private System.Single _MinDelay;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(2.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		[ModelLib.CustomAttributes.Unit("seconds")]
		public System.Single MinDelay {
			get { return _MinDelay; }
			set { SetField(ref _MinDelay, value, () => MinDelay); }
		}

		private System.Single _MaxDelay;
		[ModelLib.CustomAttributes.Default("2.0")]
		[ModelLib.CustomAttributes.Max(2.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		[ModelLib.CustomAttributes.Unit("seconds")]
		public System.Single MaxDelay {
			get { return _MaxDelay; }
			set { SetField(ref _MaxDelay, value, () => MaxDelay); }
		}

		private System.Single _DelayTimeScalar;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Max(20.0)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("seconds")]
		public System.Single DelayTimeScalar {
			get { return _DelayTimeScalar; }
			set { SetField(ref _DelayTimeScalar, value, () => DelayTimeScalar); }
		}

		private System.Single _DelayTimeAddition;
		[ModelLib.CustomAttributes.Default("0.1")]
		[ModelLib.CustomAttributes.Max(2.0)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("seconds")]
		public System.Single DelayTimeAddition {
			get { return _DelayTimeAddition; }
			set { SetField(ref _DelayTimeAddition, value, () => DelayTimeAddition); }
		}

		private ModelLib.ObjectRef _EnterSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EnterSound {
			get { return _EnterSound; }
			set { SetField(ref _EnterSound, value, () => EnterSound); }
		}

		private ModelLib.ObjectRef _ExitSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExitSound {
			get { return _ExitSound; }
			set { SetField(ref _ExitSound, value, () => ExitSound); }
		}

		private ModelLib.ObjectRef _SubmixVoice;
		[ModelLib.CustomAttributes.Default("DEFAULT_REFLECTIONS_SUBMIX_CONTROL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SubmixVoice {
			get { return _SubmixVoice; }
			set { SetField(ref _SubmixVoice, value, () => SubmixVoice); }
		}

		private System.Single _Smoothing;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Max(10.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single Smoothing {
			get { return _Smoothing; }
			set { SetField(ref _Smoothing, value, () => Smoothing); }
		}

		private System.Int16 _PostSubmixVolumeAttenuation;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 PostSubmixVolumeAttenuation {
			get { return _PostSubmixVolumeAttenuation; }
			set { SetField(ref _PostSubmixVolumeAttenuation, value, () => PostSubmixVolumeAttenuation); }
		}

		private System.Single _RollOffScale;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single RollOffScale {
			get { return _RollOffScale; }
			set { SetField(ref _RollOffScale, value, () => RollOffScale); }
		}

		private ModelLib.Types.TriState _FilterEnabled;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Filter")]
		public ModelLib.Types.TriState FilterEnabled {
			get { return _FilterEnabled; }
			set { SetField(ref _FilterEnabled, value, () => FilterEnabled); }
		}

		private FilterMode _FilterMode;
		[ModelLib.CustomAttributes.Default("kPeakingEQ")]
		[ModelLib.CustomAttributes.DisplayGroup("Filter")]
		public FilterMode FilterMode {
			get { return _FilterMode; }
			set { SetField(ref _FilterMode, value, () => FilterMode); }
		}

		private System.Single _FilterFrequencyMin;
		[ModelLib.CustomAttributes.Default("10000.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Filter")]
		[ModelLib.CustomAttributes.Max(20000.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single FilterFrequencyMin {
			get { return _FilterFrequencyMin; }
			set { SetField(ref _FilterFrequencyMin, value, () => FilterFrequencyMin); }
		}

		private System.Single _FilterFrequencyMax;
		[ModelLib.CustomAttributes.Default("10000.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Filter")]
		[ModelLib.CustomAttributes.Max(20000.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single FilterFrequencyMax {
			get { return _FilterFrequencyMax; }
			set { SetField(ref _FilterFrequencyMax, value, () => FilterFrequencyMax); }
		}

		private System.Single _FilterResonanceMin;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Filter")]
		[ModelLib.CustomAttributes.Max(10.0)]
		[ModelLib.CustomAttributes.Min(0.07)]
		public System.Single FilterResonanceMin {
			get { return _FilterResonanceMin; }
			set { SetField(ref _FilterResonanceMin, value, () => FilterResonanceMin); }
		}

		private System.Single _FilterResonanceMax;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Filter")]
		[ModelLib.CustomAttributes.Max(10.0)]
		[ModelLib.CustomAttributes.Min(0.07)]
		public System.Single FilterResonanceMax {
			get { return _FilterResonanceMax; }
			set { SetField(ref _FilterResonanceMax, value, () => FilterResonanceMax); }
		}

		private System.Single _FilterBandwidthMin;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.DisplayGroup("Filter")]
		[ModelLib.CustomAttributes.Max(20000.0)]
		[ModelLib.CustomAttributes.Min(1.0)]
		public System.Single FilterBandwidthMin {
			get { return _FilterBandwidthMin; }
			set { SetField(ref _FilterBandwidthMin, value, () => FilterBandwidthMin); }
		}

		private System.Single _FilterBandwidthMax;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.DisplayGroup("Filter")]
		[ModelLib.CustomAttributes.Max(20000.0)]
		[ModelLib.CustomAttributes.Min(1.0)]
		public System.Single FilterBandwidthMax {
			get { return _FilterBandwidthMax; }
			set { SetField(ref _FilterBandwidthMax, value, () => FilterBandwidthMax); }
		}

		private ModelLib.ObjectRef _DistanceToFilterInput;
		[ModelLib.CustomAttributes.Default("CONSTANT_ZERO")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Filter")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DistanceToFilterInput {
			get { return _DistanceToFilterInput; }
			set { SetField(ref _DistanceToFilterInput, value, () => DistanceToFilterInput); }
		}

		private ModelLib.Types.TriState _StaticReflections;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState StaticReflections {
			get { return _StaticReflections; }
			set { SetField(ref _StaticReflections, value, () => StaticReflections); }
		}

	}
}


