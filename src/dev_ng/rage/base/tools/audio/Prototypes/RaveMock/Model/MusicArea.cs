using ModelLib.CustomAttributes;

namespace model {

	public enum MusicArea {
		[ModelLib.CustomAttributes.Display("Everywhere")]
		kMusicAreaEverywhere,
		[ModelLib.CustomAttributes.Display("City")]
		kMusicAreaCity,
		[ModelLib.CustomAttributes.Display("Countryside / Desert")]
		kMusicAreaCountry
	}

}