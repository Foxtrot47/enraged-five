using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class ModelPhysicsParams:  ModelBase {

		private System.UInt32 _NumFeet;
		[ModelLib.CustomAttributes.Default("2")]
		[ModelLib.CustomAttributes.Max(8)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 NumFeet {
			get { return _NumFeet; }
			set { SetField(ref _NumFeet, value, () => NumFeet); }
		}

		private PedTypes _PedType;
		[ModelLib.CustomAttributes.Default("HUMAN")]
		public PedTypes PedType {
			get { return _PedType; }
			set { SetField(ref _PedType, value, () => PedType); }
		}

		private ModelLib.ObjectRef _FootstepTuningValues;
		[ModelLib.CustomAttributes.Default("DEFAULT_TUNING_VALUES")]
		[ModelLib.CustomAttributes.AllowedType(typeof(ModelFootStepTuning))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FootstepTuningValues {
			get { return _FootstepTuningValues; }
			set { SetField(ref _FootstepTuningValues, value, () => FootstepTuningValues); }
		}

		private System.Single _StopSpeedThreshold;
		[ModelLib.CustomAttributes.Default("0.9")]
		[ModelLib.CustomAttributes.Max(100000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single StopSpeedThreshold {
			get { return _StopSpeedThreshold; }
			set { SetField(ref _StopSpeedThreshold, value, () => StopSpeedThreshold); }
		}

		private System.Single _WalkSpeedThreshold;
		[ModelLib.CustomAttributes.Default("2.0")]
		[ModelLib.CustomAttributes.Max(100000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single WalkSpeedThreshold {
			get { return _WalkSpeedThreshold; }
			set { SetField(ref _WalkSpeedThreshold, value, () => WalkSpeedThreshold); }
		}

		private System.Single _RunSpeedThreshold;
		[ModelLib.CustomAttributes.Default("4.9")]
		[ModelLib.CustomAttributes.Max(100000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single RunSpeedThreshold {
			get { return _RunSpeedThreshold; }
			set { SetField(ref _RunSpeedThreshold, value, () => RunSpeedThreshold); }
		}

		public class ModesDefinition: ModelBase {
			private System.String _Name;
			public System.String Name {
				get { return _Name; }
				set { SetField(ref _Name, value, () => Name); }
			}

			private System.Single _GroundDistanceDownEpsilon;
			[ModelLib.CustomAttributes.Default("0.03")]
			[ModelLib.CustomAttributes.Max(100000)]
			[ModelLib.CustomAttributes.Min(-1.0)]
			public System.Single GroundDistanceDownEpsilon {
				get { return _GroundDistanceDownEpsilon; }
				set { SetField(ref _GroundDistanceDownEpsilon, value, () => GroundDistanceDownEpsilon); }
			}

			private System.Single _GroundDistanceUpEpsilon;
			[ModelLib.CustomAttributes.Default("0.05")]
			[ModelLib.CustomAttributes.Max(100000)]
			[ModelLib.CustomAttributes.Min(-1.0)]
			public System.Single GroundDistanceUpEpsilon {
				get { return _GroundDistanceUpEpsilon; }
				set { SetField(ref _GroundDistanceUpEpsilon, value, () => GroundDistanceUpEpsilon); }
			}

			private System.Single _DownSpeedEpsilon;
			[ModelLib.CustomAttributes.Default("0.5")]
			[ModelLib.CustomAttributes.Max(100000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single DownSpeedEpsilon {
				get { return _DownSpeedEpsilon; }
				set { SetField(ref _DownSpeedEpsilon, value, () => DownSpeedEpsilon); }
			}

			private System.Single _HindGroundDistanceDownEpsilon;
			[ModelLib.CustomAttributes.Default("0.03")]
			[ModelLib.CustomAttributes.Max(100000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single HindGroundDistanceDownEpsilon {
				get { return _HindGroundDistanceDownEpsilon; }
				set { SetField(ref _HindGroundDistanceDownEpsilon, value, () => HindGroundDistanceDownEpsilon); }
			}

			private System.Single _HindGroundDistanceUpEpsilon;
			[ModelLib.CustomAttributes.Default("0.05")]
			[ModelLib.CustomAttributes.Max(100000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single HindGroundDistanceUpEpsilon {
				get { return _HindGroundDistanceUpEpsilon; }
				set { SetField(ref _HindGroundDistanceUpEpsilon, value, () => HindGroundDistanceUpEpsilon); }
			}

			private System.Single _SpeedSmootherIncreaseRate;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Max(100000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single SpeedSmootherIncreaseRate {
				get { return _SpeedSmootherIncreaseRate; }
				set { SetField(ref _SpeedSmootherIncreaseRate, value, () => SpeedSmootherIncreaseRate); }
			}

			private System.Single _SpeedSmootherDecreaseRate;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Max(100000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single SpeedSmootherDecreaseRate {
				get { return _SpeedSmootherDecreaseRate; }
				set { SetField(ref _SpeedSmootherDecreaseRate, value, () => SpeedSmootherDecreaseRate); }
			}

			private System.UInt32 _TimeToRetrigger;
			[ModelLib.CustomAttributes.Default("500")]
			[ModelLib.CustomAttributes.Max(100000)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 TimeToRetrigger {
				get { return _TimeToRetrigger; }
				set { SetField(ref _TimeToRetrigger, value, () => TimeToRetrigger); }
			}

		}
		private ItemsObservableCollection<ModesDefinition> _Modes = new ItemsObservableCollection<ModesDefinition>();
		[ModelLib.CustomAttributes.MaxSize(20)]
		[XmlElement("Modes")]
		public ItemsObservableCollection<ModesDefinition> Modes {
			get { return _Modes; }
			set { SetField(ref _Modes, value, () => Modes); }
		}

	}
}


