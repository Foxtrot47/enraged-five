using ModelLib.CustomAttributes;

namespace model {

	public enum AmbientZoneWaterType {
		[ModelLib.CustomAttributes.Display("Force Over Water")]
		AMBIENT_ZONE_FORCE_OVER_WATER,
		[ModelLib.CustomAttributes.Display("Force Over Land")]
		AMBIENT_ZONE_FORCE_OVER_LAND,
		[ModelLib.CustomAttributes.Display("Calculate Using Shorelines")]
		AMBIENT_ZONE_USE_SHORELINES
	}

}