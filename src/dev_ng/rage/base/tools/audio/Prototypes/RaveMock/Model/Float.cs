using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class Float:  ModelBase {

		private System.Single _Value;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single Value {
			get { return _Value; }
			set { SetField(ref _Value, value, () => Value); }
		}

	}
}


