using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class MusicEvent:  ModelBase {

		public class ActionsDefinition: ModelBase {
			private ModelLib.ObjectRef _Action;
			[ModelLib.CustomAttributes.AllowedType(typeof(MusicAction))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef Action {
				get { return _Action; }
				set { SetField(ref _Action, value, () => Action); }
			}

		}
		private ItemsObservableCollection<ActionsDefinition> _Actions = new ItemsObservableCollection<ActionsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(8)]
		[XmlElement("Actions")]
		public ItemsObservableCollection<ActionsDefinition> Actions {
			get { return _Actions; }
			set { SetField(ref _Actions, value, () => Actions); }
		}

	}
}


