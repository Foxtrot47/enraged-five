﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFToolLib.Extensions;

namespace RaveMock
{
    class PropertiesEditorViewModel : INotifyPropertyChanged, IDisposable
    {
        private PropertiesEditorModel m_model;

        public PropertiesEditorViewModel(PropertiesEditorModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            m_model.PropertyChanged += m_model_PropertyChanged;
        }

        //forward property changed events
        void m_model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChanged.Raise(this, e.PropertyName);
        }

        public bool IsReadOnly
        {
            get { return !m_model.IsCheckedOut; }
        }

        public bool CanAddPropertyEntry
        {
            get { return m_model.CanAddPropertyEntry; }
        }


        public ObservableCollection<PropertyModel> Items
        {
            get { return m_model.Items; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void EditObject()
        {
            m_model.EditObject();
        }

        public void Dispose()
        {
            if (m_model != null)
            {
                m_model.PropertyChanged -= m_model_PropertyChanged;
                m_model = null;
            }
        }

    }
}
