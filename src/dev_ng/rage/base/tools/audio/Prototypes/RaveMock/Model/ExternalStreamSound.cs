using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class ExternalStreamSound:  Sound{

		public class EnvironmentSoundDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundRef;
			[ModelLib.CustomAttributes.AllowedType(typeof(EnvironmentSound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef SoundRef {
				get { return _SoundRef; }
				set { SetField(ref _SoundRef, value, () => SoundRef); }
			}

		}
		private ItemsObservableCollection<EnvironmentSoundDefinition> _EnvironmentSound = new ItemsObservableCollection<EnvironmentSoundDefinition>();
		[ModelLib.CustomAttributes.FixedSize]
		[ModelLib.CustomAttributes.MaxSize(4)]
		[XmlElement("EnvironmentSound")]
		public ItemsObservableCollection<EnvironmentSoundDefinition> EnvironmentSound {
			get { return _EnvironmentSound; }
			set { SetField(ref _EnvironmentSound, value, () => EnvironmentSound); }
		}

	}
}


