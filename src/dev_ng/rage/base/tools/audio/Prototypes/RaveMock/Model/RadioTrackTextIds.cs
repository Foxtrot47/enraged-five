using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class RadioTrackTextIds:  ModelBase {

		public class TextIdDefinition: ModelBase {
			private System.UInt32 _OffsetMs;
			public System.UInt32 OffsetMs {
				get { return _OffsetMs; }
				set { SetField(ref _OffsetMs, value, () => OffsetMs); }
			}

			private System.UInt32 _TextId;
			public System.UInt32 TextId {
				get { return _TextId; }
				set { SetField(ref _TextId, value, () => TextId); }
			}

		}
		private ItemsObservableCollection<TextIdDefinition> _TextId = new ItemsObservableCollection<TextIdDefinition>();
		[ModelLib.CustomAttributes.MaxSize(255, typeof(System.UInt32))]
		[XmlElement("TextId")]
		public ItemsObservableCollection<TextIdDefinition> TextId {
			get { return _TextId; }
			set { SetField(ref _TextId, value, () => TextId); }
		}

	}
}


