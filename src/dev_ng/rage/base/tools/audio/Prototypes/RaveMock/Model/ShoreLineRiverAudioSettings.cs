using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class ShoreLineRiverAudioSettings:  ShoreLineAudioSettings{

		private ModelLib.ObjectRef _NextShoreline;
		[ModelLib.CustomAttributes.AllowedType(typeof(ShoreLineRiverAudioSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef NextShoreline {
			get { return _NextShoreline; }
			set { SetField(ref _NextShoreline, value, () => NextShoreline); }
		}

		private RiverType _RiverType;
		[ModelLib.CustomAttributes.Default("AUD_RIVER_MEDIUM")]
		public RiverType RiverType {
			get { return _RiverType; }
			set { SetField(ref _RiverType, value, () => RiverType); }
		}

		public class ShoreLinePointsDefinition: ModelBase {
			private System.Single _X;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single X {
				get { return _X; }
				set { SetField(ref _X, value, () => X); }
			}

			private System.Single _Y;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single Y {
				get { return _Y; }
				set { SetField(ref _Y, value, () => Y); }
			}

			private System.Single _RiverWidth;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single RiverWidth {
				get { return _RiverWidth; }
				set { SetField(ref _RiverWidth, value, () => RiverWidth); }
			}

		}
		private ItemsObservableCollection<ShoreLinePointsDefinition> _ShoreLinePoints = new ItemsObservableCollection<ShoreLinePointsDefinition>();
		[ModelLib.CustomAttributes.DisplayGroup("Definition")]
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("ShoreLinePoints")]
		public ItemsObservableCollection<ShoreLinePointsDefinition> ShoreLinePoints {
			get { return _ShoreLinePoints; }
			set { SetField(ref _ShoreLinePoints, value, () => ShoreLinePoints); }
		}

	}
}


