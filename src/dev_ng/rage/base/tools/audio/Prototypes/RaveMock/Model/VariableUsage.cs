using ModelLib.CustomAttributes;

namespace model {

	public enum VariableUsage {
		[ModelLib.CustomAttributes.Display("Set from sounds")]
		VARIABLE_USAGE_SOUND,
		[ModelLib.CustomAttributes.Display("Set from game")]
		VARIABLE_USAGE_CODE,
		[ModelLib.CustomAttributes.Display("Constant")]
		VARIABLE_USAGE_CONSTANT
	}

}