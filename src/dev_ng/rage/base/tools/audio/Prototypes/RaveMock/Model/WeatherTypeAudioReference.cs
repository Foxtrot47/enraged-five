using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Ambience")]
	public class WeatherTypeAudioReference:  ModelBase {

		public class ReferenceDefinition: ModelBase {
			private System.String _WeatherName;
			public System.String WeatherName {
				get { return _WeatherName; }
				set { SetField(ref _WeatherName, value, () => WeatherName); }
			}

			private ModelLib.ObjectRef _WeatherType;
			[ModelLib.CustomAttributes.AllowedType(typeof(WeatherAudioSettings))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef WeatherType {
				get { return _WeatherType; }
				set { SetField(ref _WeatherType, value, () => WeatherType); }
			}

		}
		private ItemsObservableCollection<ReferenceDefinition> _Reference = new ItemsObservableCollection<ReferenceDefinition>();
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("Reference")]
		public ItemsObservableCollection<ReferenceDefinition> Reference {
			get { return _Reference; }
			set { SetField(ref _Reference, value, () => Reference); }
		}

	}
}


