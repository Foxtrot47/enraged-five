using ModelLib.CustomAttributes;

namespace model {

	public enum WaterType {
		[ModelLib.CustomAttributes.Display("Pool")]
		AUD_WATER_TYPE_POOL,
		[ModelLib.CustomAttributes.Display("River")]
		AUD_WATER_TYPE_RIVER,
		[ModelLib.CustomAttributes.Display("Lake")]
		AUD_WATER_TYPE_LAKE,
		[ModelLib.CustomAttributes.Display("Ocean")]
		AUD_WATER_TYPE_OCEAN
	}

}