using ModelLib.CustomAttributes;

namespace model {

	public enum GPSType {
		[ModelLib.CustomAttributes.Display("None")]
		GPS_TYPE_NONE,
		[ModelLib.CustomAttributes.Display("Any")]
		GPS_TYPE_ANY
	}

}