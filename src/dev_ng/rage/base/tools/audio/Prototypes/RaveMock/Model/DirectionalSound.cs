using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class DirectionalSound:  Sound{

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		private System.Single _InnerAngle;
		[ModelLib.CustomAttributes.Default("65.0")]
		[ModelLib.CustomAttributes.Max(360.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single InnerAngle {
			get { return _InnerAngle; }
			set { SetField(ref _InnerAngle, value, () => InnerAngle); }
		}

		private System.Single _OuterAngle;
		[ModelLib.CustomAttributes.Default("135.0")]
		[ModelLib.CustomAttributes.Max(360.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single OuterAngle {
			get { return _OuterAngle; }
			set { SetField(ref _OuterAngle, value, () => OuterAngle); }
		}

		private System.Single _RearAttenuation;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single RearAttenuation {
			get { return _RearAttenuation; }
			set { SetField(ref _RearAttenuation, value, () => RearAttenuation); }
		}

		private System.Single _YawAngle;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single YawAngle {
			get { return _YawAngle; }
			set { SetField(ref _YawAngle, value, () => YawAngle); }
		}

		private System.Single _PitchAngle;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single PitchAngle {
			get { return _PitchAngle; }
			set { SetField(ref _PitchAngle, value, () => PitchAngle); }
		}

	}
}


