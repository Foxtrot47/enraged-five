using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class AudioRoadInfo:  ModelBase {

		private System.String _RoadName;
		public System.String RoadName {
			get { return _RoadName; }
			set { SetField(ref _RoadName, value, () => RoadName); }
		}

		private System.Single _TyreBumpDistance;
		[ModelLib.CustomAttributes.Default("12.0")]
		public System.Single TyreBumpDistance {
			get { return _TyreBumpDistance; }
			set { SetField(ref _TyreBumpDistance, value, () => TyreBumpDistance); }
		}

	}
}


