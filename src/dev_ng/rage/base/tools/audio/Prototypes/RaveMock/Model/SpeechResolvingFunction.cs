using ModelLib.CustomAttributes;

namespace model {

	public enum SpeechResolvingFunction {
		[ModelLib.CustomAttributes.Display("Default")]
		SRF_DEFAULT,
		[ModelLib.CustomAttributes.Display("TimeOfDay")]
		SRF_TIME_OF_DAY,
		[ModelLib.CustomAttributes.Display("PedToughness")]
		SRF_PED_TOUGHNESS,
		[ModelLib.CustomAttributes.Display("TargetGender")]
		SRF_TARGET_GENDER
	}

}