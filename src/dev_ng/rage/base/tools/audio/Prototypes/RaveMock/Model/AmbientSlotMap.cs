using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class AmbientSlotMap:  ModelBase {

		public class SlotMapDefinition: ModelBase {
			private System.String _WaveSlotName;
			public System.String WaveSlotName {
				get { return _WaveSlotName; }
				set { SetField(ref _WaveSlotName, value, () => WaveSlotName); }
			}

			private System.String _SlotType;
			public System.String SlotType {
				get { return _SlotType; }
				set { SetField(ref _SlotType, value, () => SlotType); }
			}

			private System.UInt32 _Priority;
			[ModelLib.CustomAttributes.Default("0")]
			public System.UInt32 Priority {
				get { return _Priority; }
				set { SetField(ref _Priority, value, () => Priority); }
			}

		}
		private ItemsObservableCollection<SlotMapDefinition> _SlotMap = new ItemsObservableCollection<SlotMapDefinition>();
		[ModelLib.CustomAttributes.MaxSize(255)]
		[XmlElement("SlotMap")]
		public ItemsObservableCollection<SlotMapDefinition> SlotMap {
			get { return _SlotMap; }
			set { SetField(ref _SlotMap, value, () => SlotMap); }
		}

	}
}


