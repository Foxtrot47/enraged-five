using ModelLib.CustomAttributes;

namespace model {

	public enum PedTypes {
		[ModelLib.CustomAttributes.Display("Human")]
		HUMAN,
		[ModelLib.CustomAttributes.Display("Dog")]
		DOG,
		[ModelLib.CustomAttributes.Display("Deer")]
		DEER,
		[ModelLib.CustomAttributes.Display("Boar")]
		BOAR,
		[ModelLib.CustomAttributes.Display("Coyote")]
		COYOTE,
		[ModelLib.CustomAttributes.Display("MtLion")]
		MTLION,
		[ModelLib.CustomAttributes.Display("Pig")]
		PIG,
		[ModelLib.CustomAttributes.Display("Chimp")]
		CHIMP,
		[ModelLib.CustomAttributes.Display("Cow")]
		COW,
		[ModelLib.CustomAttributes.Display("Rottweiler")]
		ROTTWEILER,
		[ModelLib.CustomAttributes.Display("Elk")]
		ELK,
		[ModelLib.CustomAttributes.Display("Retriever")]
		RETRIEVER,
		[ModelLib.CustomAttributes.Display("Rat")]
		RAT,
		[ModelLib.CustomAttributes.Display("Bird")]
		BIRD,
		[ModelLib.CustomAttributes.Display("Fish")]
		FISH
	}

}