using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Radio")]
	public class RadioStationList:  ModelBase {

		public class StationDefinition: ModelBase {
			private ModelLib.ObjectRef _StationSettings;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef StationSettings {
				get { return _StationSettings; }
				set { SetField(ref _StationSettings, value, () => StationSettings); }
			}

		}
		private ItemsObservableCollection<StationDefinition> _Station = new ItemsObservableCollection<StationDefinition>();
		[ModelLib.CustomAttributes.MaxSize(100)]
		[XmlElement("Station")]
		public ItemsObservableCollection<StationDefinition> Station {
			get { return _Station; }
			set { SetField(ref _Station, value, () => Station); }
		}

	}
}


