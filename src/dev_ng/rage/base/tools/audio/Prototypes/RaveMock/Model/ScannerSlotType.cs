using ModelLib.CustomAttributes;

namespace model {

	public enum ScannerSlotType {
		[ModelLib.CustomAttributes.Display("LargeSlot")]
		LARGE_SCANNER_SLOT,
		[ModelLib.CustomAttributes.Display("SmallSlot")]
		SMALL_SCANNER_SLOT
	}

}