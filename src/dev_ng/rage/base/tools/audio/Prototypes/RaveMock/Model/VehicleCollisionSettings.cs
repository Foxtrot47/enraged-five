using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class VehicleCollisionSettings:  ModelBase {

		private ModelLib.ObjectRef _MediumIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("FakeBulletImpactSoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef MediumIntensity {
			get { return _MediumIntensity; }
			set { SetField(ref _MediumIntensity, value, () => MediumIntensity); }
		}

		private ModelLib.ObjectRef _HighIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("FakeBulletImpactSoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HighIntensity {
			get { return _HighIntensity; }
			set { SetField(ref _HighIntensity, value, () => HighIntensity); }
		}

		private ModelLib.ObjectRef _SmallScrapeImpact;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SmallScrapeImpact {
			get { return _SmallScrapeImpact; }
			set { SetField(ref _SmallScrapeImpact, value, () => SmallScrapeImpact); }
		}

		private ModelLib.ObjectRef _SlowScrapeImpact;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SlowScrapeImpact {
			get { return _SlowScrapeImpact; }
			set { SetField(ref _SlowScrapeImpact, value, () => SlowScrapeImpact); }
		}

		private ModelLib.ObjectRef _SlowScrapeLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SlowScrapeLoop {
			get { return _SlowScrapeLoop; }
			set { SetField(ref _SlowScrapeLoop, value, () => SlowScrapeLoop); }
		}

		private ModelLib.ObjectRef _RollSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RollSound {
			get { return _RollSound; }
			set { SetField(ref _RollSound, value, () => RollSound); }
		}

		private ModelLib.ObjectRef _VehOnVehCrashSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehOnVehCrashSound {
			get { return _VehOnVehCrashSound; }
			set { SetField(ref _VehOnVehCrashSound, value, () => VehOnVehCrashSound); }
		}

		private ModelLib.ObjectRef _HighImpactSweetenerSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HighImpactSweetenerSound {
			get { return _HighImpactSweetenerSound; }
			set { SetField(ref _HighImpactSweetenerSound, value, () => HighImpactSweetenerSound); }
		}

		private System.Single _SweetenerImpactThreshold;
		[ModelLib.CustomAttributes.Default("20.0")]
		public System.Single SweetenerImpactThreshold {
			get { return _SweetenerImpactThreshold; }
			set { SetField(ref _SweetenerImpactThreshold, value, () => SweetenerImpactThreshold); }
		}

		private ModelLib.ObjectRef _ScrapePitchCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_VEHICLE_SCRAPE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScrapePitchCurve {
			get { return _ScrapePitchCurve; }
			set { SetField(ref _ScrapePitchCurve, value, () => ScrapePitchCurve); }
		}

		private ModelLib.ObjectRef _ScrapeVolCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_VEHICLE_SCRAPE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScrapeVolCurve {
			get { return _ScrapeVolCurve; }
			set { SetField(ref _ScrapeVolCurve, value, () => ScrapeVolCurve); }
		}

		private ModelLib.ObjectRef _SlowScrapeVolCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_VEHICLE_SLOW_SCRAPE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SlowScrapeVolCurve {
			get { return _SlowScrapeVolCurve; }
			set { SetField(ref _SlowScrapeVolCurve, value, () => SlowScrapeVolCurve); }
		}

		private ModelLib.ObjectRef _ScrapeImpactVolCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_IMPACT_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScrapeImpactVolCurve {
			get { return _ScrapeImpactVolCurve; }
			set { SetField(ref _ScrapeImpactVolCurve, value, () => ScrapeImpactVolCurve); }
		}

		private ModelLib.ObjectRef _SlowScrapeImpactCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_IMPACT_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SlowScrapeImpactCurve {
			get { return _SlowScrapeImpactCurve; }
			set { SetField(ref _SlowScrapeImpactCurve, value, () => SlowScrapeImpactCurve); }
		}

		private ModelLib.ObjectRef _ImpactStartOffsetCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_IMPACT_START_OFFSET")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ImpactStartOffsetCurve {
			get { return _ImpactStartOffsetCurve; }
			set { SetField(ref _ImpactStartOffsetCurve, value, () => ImpactStartOffsetCurve); }
		}

		private ModelLib.ObjectRef _ImpactVolCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_IMPACT_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ImpactVolCurve {
			get { return _ImpactVolCurve; }
			set { SetField(ref _ImpactVolCurve, value, () => ImpactVolCurve); }
		}

		private ModelLib.ObjectRef _VehicleImpactStartOffsetCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_IMPACT_START_OFFSET")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehicleImpactStartOffsetCurve {
			get { return _VehicleImpactStartOffsetCurve; }
			set { SetField(ref _VehicleImpactStartOffsetCurve, value, () => VehicleImpactStartOffsetCurve); }
		}

		private ModelLib.ObjectRef _VehicleImpactVolCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_IMPACT_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehicleImpactVolCurve {
			get { return _VehicleImpactVolCurve; }
			set { SetField(ref _VehicleImpactVolCurve, value, () => VehicleImpactVolCurve); }
		}

		private ModelLib.ObjectRef _VelocityImpactScalingCurve;
		[ModelLib.CustomAttributes.Default("VEH_VEL_IMPACT_SCALING")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VelocityImpactScalingCurve {
			get { return _VelocityImpactScalingCurve; }
			set { SetField(ref _VelocityImpactScalingCurve, value, () => VelocityImpactScalingCurve); }
		}

		private ModelLib.ObjectRef _FakeImpactStartOffsetCurve;
		[ModelLib.CustomAttributes.Default("VEHICLE_FAKE_IMPACT_START_OFFSET")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FakeImpactStartOffsetCurve {
			get { return _FakeImpactStartOffsetCurve; }
			set { SetField(ref _FakeImpactStartOffsetCurve, value, () => FakeImpactStartOffsetCurve); }
		}

		private ModelLib.ObjectRef _FakeImpactVolCurve;
		[ModelLib.CustomAttributes.Default("VEHICLE_FAKE_IMPACT_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FakeImpactVolCurve {
			get { return _FakeImpactVolCurve; }
			set { SetField(ref _FakeImpactVolCurve, value, () => FakeImpactVolCurve); }
		}

		private System.Single _FakeImpactMin;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single FakeImpactMin {
			get { return _FakeImpactMin; }
			set { SetField(ref _FakeImpactMin, value, () => FakeImpactMin); }
		}

		private System.Single _FakeImpactMax;
		[ModelLib.CustomAttributes.Default("20.0")]
		public System.Single FakeImpactMax {
			get { return _FakeImpactMax; }
			set { SetField(ref _FakeImpactMax, value, () => FakeImpactMax); }
		}

		private System.Single _FakeImpactScale;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single FakeImpactScale {
			get { return _FakeImpactScale; }
			set { SetField(ref _FakeImpactScale, value, () => FakeImpactScale); }
		}

		private System.Single _VehicleSizeScale;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single VehicleSizeScale {
			get { return _VehicleSizeScale; }
			set { SetField(ref _VehicleSizeScale, value, () => VehicleSizeScale); }
		}

		private System.Single _FakeImpactTriggerDelta;
		[ModelLib.CustomAttributes.Default("6.0")]
		public System.Single FakeImpactTriggerDelta {
			get { return _FakeImpactTriggerDelta; }
			set { SetField(ref _FakeImpactTriggerDelta, value, () => FakeImpactTriggerDelta); }
		}

		private System.Single _FakeImpactSweetenerThreshold;
		[ModelLib.CustomAttributes.Default("30.0")]
		public System.Single FakeImpactSweetenerThreshold {
			get { return _FakeImpactSweetenerThreshold; }
			set { SetField(ref _FakeImpactSweetenerThreshold, value, () => FakeImpactSweetenerThreshold); }
		}

		private ModelLib.ObjectRef _DamageVolCurve;
		[ModelLib.CustomAttributes.Default("IMPACT_DAMAGE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DamageVolCurve {
			get { return _DamageVolCurve; }
			set { SetField(ref _DamageVolCurve, value, () => DamageVolCurve); }
		}

		private ModelLib.ObjectRef _JumpLandVolCurve;
		[ModelLib.CustomAttributes.Default("COLLISION_VEHICLE_JUMPLAND_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef JumpLandVolCurve {
			get { return _JumpLandVolCurve; }
			set { SetField(ref _JumpLandVolCurve, value, () => JumpLandVolCurve); }
		}

		private ModelLib.ObjectRef _VehicleMaterialSettings;
		[ModelLib.CustomAttributes.Default("AM_CAR_BASE_METAL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(CollisionMaterialSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehicleMaterialSettings {
			get { return _VehicleMaterialSettings; }
			set { SetField(ref _VehicleMaterialSettings, value, () => VehicleMaterialSettings); }
		}

		private ModelLib.ObjectRef _DeformationSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DeformationSound {
			get { return _DeformationSound; }
			set { SetField(ref _DeformationSound, value, () => DeformationSound); }
		}

		private ModelLib.ObjectRef _ImpactDebris;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ImpactDebris {
			get { return _ImpactDebris; }
			set { SetField(ref _ImpactDebris, value, () => ImpactDebris); }
		}

		private ModelLib.ObjectRef _GlassDebris;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GlassDebris {
			get { return _GlassDebris; }
			set { SetField(ref _GlassDebris, value, () => GlassDebris); }
		}

		private ModelLib.ObjectRef _PostImpactDebris;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PostImpactDebris {
			get { return _PostImpactDebris; }
			set { SetField(ref _PostImpactDebris, value, () => PostImpactDebris); }
		}

		private System.Single _PedCollisionMin;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single PedCollisionMin {
			get { return _PedCollisionMin; }
			set { SetField(ref _PedCollisionMin, value, () => PedCollisionMin); }
		}

		private System.Single _PedCollisionMax;
		[ModelLib.CustomAttributes.Default("20.0")]
		public System.Single PedCollisionMax {
			get { return _PedCollisionMax; }
			set { SetField(ref _PedCollisionMax, value, () => PedCollisionMax); }
		}

		private System.Single _CollisionMin;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single CollisionMin {
			get { return _CollisionMin; }
			set { SetField(ref _CollisionMin, value, () => CollisionMin); }
		}

		private System.Single _CollisionMax;
		[ModelLib.CustomAttributes.Default("20.0")]
		public System.Single CollisionMax {
			get { return _CollisionMax; }
			set { SetField(ref _CollisionMax, value, () => CollisionMax); }
		}

		private System.Single _VehicleCollisionMin;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single VehicleCollisionMin {
			get { return _VehicleCollisionMin; }
			set { SetField(ref _VehicleCollisionMin, value, () => VehicleCollisionMin); }
		}

		private System.Single _VehicleCollisionMax;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single VehicleCollisionMax {
			get { return _VehicleCollisionMax; }
			set { SetField(ref _VehicleCollisionMax, value, () => VehicleCollisionMax); }
		}

		private System.Single _VehicleSweetenerThreshold;
		[ModelLib.CustomAttributes.Default("10")]
		public System.Single VehicleSweetenerThreshold {
			get { return _VehicleSweetenerThreshold; }
			set { SetField(ref _VehicleSweetenerThreshold, value, () => VehicleSweetenerThreshold); }
		}

		private System.Single _ScrapeMin;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single ScrapeMin {
			get { return _ScrapeMin; }
			set { SetField(ref _ScrapeMin, value, () => ScrapeMin); }
		}

		private System.Single _ScrapeMax;
		[ModelLib.CustomAttributes.Default("20.0")]
		public System.Single ScrapeMax {
			get { return _ScrapeMax; }
			set { SetField(ref _ScrapeMax, value, () => ScrapeMax); }
		}

		private System.Single _DamageMin;
		[ModelLib.CustomAttributes.Default("20.0")]
		public System.Single DamageMin {
			get { return _DamageMin; }
			set { SetField(ref _DamageMin, value, () => DamageMin); }
		}

		private System.Single _DamageMax;
		[ModelLib.CustomAttributes.Default("130.0")]
		public System.Single DamageMax {
			get { return _DamageMax; }
			set { SetField(ref _DamageMax, value, () => DamageMax); }
		}

		private ModelLib.ObjectRef _TrainImpact;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TrainImpact {
			get { return _TrainImpact; }
			set { SetField(ref _TrainImpact, value, () => TrainImpact); }
		}

		private ModelLib.ObjectRef _TrainImpactLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TrainImpactLoop {
			get { return _TrainImpactLoop; }
			set { SetField(ref _TrainImpactLoop, value, () => TrainImpactLoop); }
		}

		private ModelLib.ObjectRef _WaveImpactLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WaveImpactLoop {
			get { return _WaveImpactLoop; }
			set { SetField(ref _WaveImpactLoop, value, () => WaveImpactLoop); }
		}

		private ModelLib.ObjectRef _FragMaterial;
		[ModelLib.CustomAttributes.AllowedType(typeof(CollisionMaterialSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FragMaterial {
			get { return _FragMaterial; }
			set { SetField(ref _FragMaterial, value, () => FragMaterial); }
		}

		private ModelLib.ObjectRef _WheelFragMaterial;
		[ModelLib.CustomAttributes.AllowedType(typeof(CollisionMaterialSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WheelFragMaterial {
			get { return _WheelFragMaterial; }
			set { SetField(ref _WheelFragMaterial, value, () => WheelFragMaterial); }
		}

		public class MeleeMaterialOverrideDefinition: ModelBase {
			private ModelLib.ObjectRef _Material;
			[ModelLib.CustomAttributes.AllowedType(typeof(CollisionMaterialSettings))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef Material {
				get { return _Material; }
				set { SetField(ref _Material, value, () => Material); }
			}

			private ModelLib.ObjectRef _Override;
			[ModelLib.CustomAttributes.AllowedType(typeof(CollisionMaterialSettings))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef Override {
				get { return _Override; }
				set { SetField(ref _Override, value, () => Override); }
			}

		}
		private ItemsObservableCollection<MeleeMaterialOverrideDefinition> _MeleeMaterialOverride = new ItemsObservableCollection<MeleeMaterialOverrideDefinition>();
		[ModelLib.CustomAttributes.MaxSize(64)]
		[XmlElement("MeleeMaterialOverride")]
		public ItemsObservableCollection<MeleeMaterialOverrideDefinition> MeleeMaterialOverride {
			get { return _MeleeMaterialOverride; }
			set { SetField(ref _MeleeMaterialOverride, value, () => MeleeMaterialOverride); }
		}

	}
}


