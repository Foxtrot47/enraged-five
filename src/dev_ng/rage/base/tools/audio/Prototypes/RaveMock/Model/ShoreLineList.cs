using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Ambience")]
	public class ShoreLineList:  ModelBase {

		public class ShoreLinesDefinition: ModelBase {
			private System.String _ShoreLine;
			public System.String ShoreLine {
				get { return _ShoreLine; }
				set { SetField(ref _ShoreLine, value, () => ShoreLine); }
			}

		}
		private ItemsObservableCollection<ShoreLinesDefinition> _ShoreLines = new ItemsObservableCollection<ShoreLinesDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1000)]
		[XmlElement("ShoreLines")]
		public ItemsObservableCollection<ShoreLinesDefinition> ShoreLines {
			get { return _ShoreLines; }
			set { SetField(ref _ShoreLines, value, () => ShoreLines); }
		}

	}
}


