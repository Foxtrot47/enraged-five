using ModelLib.CustomAttributes;

namespace model {

	public enum RadioType {
		[ModelLib.CustomAttributes.Display("None")]
		RADIO_TYPE_NONE,
		[ModelLib.CustomAttributes.Display("Normal")]
		RADIO_TYPE_NORMAL,
		[ModelLib.CustomAttributes.Display("Emergency Services")]
		RADIO_TYPE_EMERGENCY_SERVICES,
		[ModelLib.CustomAttributes.Display("None on-mission, Normal otherwise")]
		RADIO_TYPE_NORMAL_OFF_MISSION_AND_MP
	}

}