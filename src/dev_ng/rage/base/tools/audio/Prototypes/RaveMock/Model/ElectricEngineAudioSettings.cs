using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class ElectricEngineAudioSettings:  ModelBase {

		private System.Int32 _MasterVolume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 MasterVolume {
			get { return _MasterVolume; }
			set { SetField(ref _MasterVolume, value, () => MasterVolume); }
		}

		private ModelLib.ObjectRef _SpeedLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SpeedLoop {
			get { return _SpeedLoop; }
			set { SetField(ref _SpeedLoop, value, () => SpeedLoop); }
		}

		private System.Int32 _SpeedLoop_MinPitch;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 SpeedLoop_MinPitch {
			get { return _SpeedLoop_MinPitch; }
			set { SetField(ref _SpeedLoop_MinPitch, value, () => SpeedLoop_MinPitch); }
		}

		private System.Int32 _SpeedLoop_MaxPitch;
		[ModelLib.CustomAttributes.Default("600")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 SpeedLoop_MaxPitch {
			get { return _SpeedLoop_MaxPitch; }
			set { SetField(ref _SpeedLoop_MaxPitch, value, () => SpeedLoop_MaxPitch); }
		}

		private System.Int32 _SpeedLoop_ThrottleVol;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 SpeedLoop_ThrottleVol {
			get { return _SpeedLoop_ThrottleVol; }
			set { SetField(ref _SpeedLoop_ThrottleVol, value, () => SpeedLoop_ThrottleVol); }
		}

		private ModelLib.ObjectRef _BoostLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BoostLoop {
			get { return _BoostLoop; }
			set { SetField(ref _BoostLoop, value, () => BoostLoop); }
		}

		private System.Int32 _BoostLoop_MinPitch;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 BoostLoop_MinPitch {
			get { return _BoostLoop_MinPitch; }
			set { SetField(ref _BoostLoop_MinPitch, value, () => BoostLoop_MinPitch); }
		}

		private System.Int32 _BoostLoop_MaxPitch;
		[ModelLib.CustomAttributes.Default("1200")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 BoostLoop_MaxPitch {
			get { return _BoostLoop_MaxPitch; }
			set { SetField(ref _BoostLoop_MaxPitch, value, () => BoostLoop_MaxPitch); }
		}

		private System.UInt32 _BoostLoop_SpinupSpeed;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.Max(1000)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt32 BoostLoop_SpinupSpeed {
			get { return _BoostLoop_SpinupSpeed; }
			set { SetField(ref _BoostLoop_SpinupSpeed, value, () => BoostLoop_SpinupSpeed); }
		}

		private System.Int32 _BoostLoop_Vol;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 BoostLoop_Vol {
			get { return _BoostLoop_Vol; }
			set { SetField(ref _BoostLoop_Vol, value, () => BoostLoop_Vol); }
		}

		private ModelLib.ObjectRef _RevsOffLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RevsOffLoop {
			get { return _RevsOffLoop; }
			set { SetField(ref _RevsOffLoop, value, () => RevsOffLoop); }
		}

		private System.Int32 _RevsOffLoop_MinPitch;
		[ModelLib.CustomAttributes.Default("-500")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 RevsOffLoop_MinPitch {
			get { return _RevsOffLoop_MinPitch; }
			set { SetField(ref _RevsOffLoop_MinPitch, value, () => RevsOffLoop_MinPitch); }
		}

		private System.Int32 _RevsOffLoop_MaxPitch;
		[ModelLib.CustomAttributes.Default("1000")]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int32 RevsOffLoop_MaxPitch {
			get { return _RevsOffLoop_MaxPitch; }
			set { SetField(ref _RevsOffLoop_MaxPitch, value, () => RevsOffLoop_MaxPitch); }
		}

		private System.Int32 _RevsOffLoop_Vol;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 RevsOffLoop_Vol {
			get { return _RevsOffLoop_Vol; }
			set { SetField(ref _RevsOffLoop_Vol, value, () => RevsOffLoop_Vol); }
		}

		private ModelLib.ObjectRef _BankLoadSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankLoadSound {
			get { return _BankLoadSound; }
			set { SetField(ref _BankLoadSound, value, () => BankLoadSound); }
		}

		private ModelLib.ObjectRef _EngineStartUp;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineStartUp {
			get { return _EngineStartUp; }
			set { SetField(ref _EngineStartUp, value, () => EngineStartUp); }
		}

	}
}


