using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class SlowMoSettings:  ModelBase {

		private ModelLib.ObjectRef _Scene;
		[ModelLib.CustomAttributes.AllowedType(typeof(MixerScene))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Scene {
			get { return _Scene; }
			set { SetField(ref _Scene, value, () => Scene); }
		}

		private SlowMoType _Priority;
		[ModelLib.CustomAttributes.Default("AUD_SLOWMO_GENERAL")]
		public SlowMoType Priority {
			get { return _Priority; }
			set { SetField(ref _Priority, value, () => Priority); }
		}

		private System.Single _Release;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single Release {
			get { return _Release; }
			set { SetField(ref _Release, value, () => Release); }
		}

	}
}


