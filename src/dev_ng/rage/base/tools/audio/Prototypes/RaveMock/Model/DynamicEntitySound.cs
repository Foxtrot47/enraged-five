using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class DynamicEntitySound:  Sound{

		public class ObjectRefsDefinition: ModelBase {
			private System.String _GameObjectHash;
			public System.String GameObjectHash {
				get { return _GameObjectHash; }
				set { SetField(ref _GameObjectHash, value, () => GameObjectHash); }
			}

		}
		private ItemsObservableCollection<ObjectRefsDefinition> _ObjectRefs = new ItemsObservableCollection<ObjectRefsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(8)]
		[XmlElement("ObjectRefs")]
		public ItemsObservableCollection<ObjectRefsDefinition> ObjectRefs {
			get { return _ObjectRefs; }
			set { SetField(ref _ObjectRefs, value, () => ObjectRefs); }
		}

	}
}


