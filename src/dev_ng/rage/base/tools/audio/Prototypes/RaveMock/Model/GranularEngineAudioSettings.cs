using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class GranularEngineAudioSettings:  ModelBase {

		private System.Int32 _MasterVolume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 MasterVolume {
			get { return _MasterVolume; }
			set { SetField(ref _MasterVolume, value, () => MasterVolume); }
		}

		private ModelLib.ObjectRef _EngineAccel;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineAccel {
			get { return _EngineAccel; }
			set { SetField(ref _EngineAccel, value, () => EngineAccel); }
		}

		private ModelLib.ObjectRef _ExhaustAccel;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustAccel {
			get { return _ExhaustAccel; }
			set { SetField(ref _ExhaustAccel, value, () => ExhaustAccel); }
		}

		private System.Int32 _EngineVolume_PreSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Pre Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 EngineVolume_PreSubmix {
			get { return _EngineVolume_PreSubmix; }
			set { SetField(ref _EngineVolume_PreSubmix, value, () => EngineVolume_PreSubmix); }
		}

		private System.Int32 _ExhaustVolume_PreSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Pre Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ExhaustVolume_PreSubmix {
			get { return _ExhaustVolume_PreSubmix; }
			set { SetField(ref _ExhaustVolume_PreSubmix, value, () => ExhaustVolume_PreSubmix); }
		}

		private System.Int32 _AccelVolume_PreSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Pre Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 AccelVolume_PreSubmix {
			get { return _AccelVolume_PreSubmix; }
			set { SetField(ref _AccelVolume_PreSubmix, value, () => AccelVolume_PreSubmix); }
		}

		private System.Int32 _DecelVolume_PreSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Pre Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 DecelVolume_PreSubmix {
			get { return _DecelVolume_PreSubmix; }
			set { SetField(ref _DecelVolume_PreSubmix, value, () => DecelVolume_PreSubmix); }
		}

		private System.Int32 _IdleVolume_PreSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Pre Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 IdleVolume_PreSubmix {
			get { return _IdleVolume_PreSubmix; }
			set { SetField(ref _IdleVolume_PreSubmix, value, () => IdleVolume_PreSubmix); }
		}

		private System.Int32 _EngineRevsVolume_PreSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Pre Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 EngineRevsVolume_PreSubmix {
			get { return _EngineRevsVolume_PreSubmix; }
			set { SetField(ref _EngineRevsVolume_PreSubmix, value, () => EngineRevsVolume_PreSubmix); }
		}

		private System.Int32 _ExhaustRevsVolume_PreSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Pre Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ExhaustRevsVolume_PreSubmix {
			get { return _ExhaustRevsVolume_PreSubmix; }
			set { SetField(ref _ExhaustRevsVolume_PreSubmix, value, () => ExhaustRevsVolume_PreSubmix); }
		}

		private System.Int32 _EngineThrottleVolume_PreSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Pre Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 EngineThrottleVolume_PreSubmix {
			get { return _EngineThrottleVolume_PreSubmix; }
			set { SetField(ref _EngineThrottleVolume_PreSubmix, value, () => EngineThrottleVolume_PreSubmix); }
		}

		private System.Int32 _ExhaustThrottleVolume_PreSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Pre Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ExhaustThrottleVolume_PreSubmix {
			get { return _ExhaustThrottleVolume_PreSubmix; }
			set { SetField(ref _ExhaustThrottleVolume_PreSubmix, value, () => ExhaustThrottleVolume_PreSubmix); }
		}

		private System.Int32 _EngineVolume_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 EngineVolume_PostSubmix {
			get { return _EngineVolume_PostSubmix; }
			set { SetField(ref _EngineVolume_PostSubmix, value, () => EngineVolume_PostSubmix); }
		}

		private System.Int32 _ExhaustVolume_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ExhaustVolume_PostSubmix {
			get { return _ExhaustVolume_PostSubmix; }
			set { SetField(ref _ExhaustVolume_PostSubmix, value, () => ExhaustVolume_PostSubmix); }
		}

		private System.Int32 _EngineMaxConeAttenuation;
		[ModelLib.CustomAttributes.Default("-300")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 EngineMaxConeAttenuation {
			get { return _EngineMaxConeAttenuation; }
			set { SetField(ref _EngineMaxConeAttenuation, value, () => EngineMaxConeAttenuation); }
		}

		private System.Int32 _ExhaustMaxConeAttenuation;
		[ModelLib.CustomAttributes.Default("-300")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ExhaustMaxConeAttenuation {
			get { return _ExhaustMaxConeAttenuation; }
			set { SetField(ref _ExhaustMaxConeAttenuation, value, () => ExhaustMaxConeAttenuation); }
		}

		private System.Int32 _EngineRevsVolume_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 EngineRevsVolume_PostSubmix {
			get { return _EngineRevsVolume_PostSubmix; }
			set { SetField(ref _EngineRevsVolume_PostSubmix, value, () => EngineRevsVolume_PostSubmix); }
		}

		private System.Int32 _ExhaustRevsVolume_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ExhaustRevsVolume_PostSubmix {
			get { return _ExhaustRevsVolume_PostSubmix; }
			set { SetField(ref _ExhaustRevsVolume_PostSubmix, value, () => ExhaustRevsVolume_PostSubmix); }
		}

		private System.Int32 _EngineThrottleVolume_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 EngineThrottleVolume_PostSubmix {
			get { return _EngineThrottleVolume_PostSubmix; }
			set { SetField(ref _EngineThrottleVolume_PostSubmix, value, () => EngineThrottleVolume_PostSubmix); }
		}

		private System.Int32 _ExhaustThrottleVolume_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ExhaustThrottleVolume_PostSubmix {
			get { return _ExhaustThrottleVolume_PostSubmix; }
			set { SetField(ref _ExhaustThrottleVolume_PostSubmix, value, () => ExhaustThrottleVolume_PostSubmix); }
		}

		private ModelLib.Types.TriState _GearChangeWobblesEnabled;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("Wobbles")]
		public ModelLib.Types.TriState GearChangeWobblesEnabled {
			get { return _GearChangeWobblesEnabled; }
			set { SetField(ref _GearChangeWobblesEnabled, value, () => GearChangeWobblesEnabled); }
		}

		private System.UInt32 _GearChangeWobbleLength;
		[ModelLib.CustomAttributes.Default("30")]
		[ModelLib.CustomAttributes.DisplayGroup("Wobbles")]
		[ModelLib.CustomAttributes.Max(1000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 GearChangeWobbleLength {
			get { return _GearChangeWobbleLength; }
			set { SetField(ref _GearChangeWobbleLength, value, () => GearChangeWobbleLength); }
		}

		private System.Single _GearChangeWobbleLengthVariance;
		[ModelLib.CustomAttributes.Default("0.2")]
		[ModelLib.CustomAttributes.DisplayGroup("Wobbles")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single GearChangeWobbleLengthVariance {
			get { return _GearChangeWobbleLengthVariance; }
			set { SetField(ref _GearChangeWobbleLengthVariance, value, () => GearChangeWobbleLengthVariance); }
		}

		private System.Single _GearChangeWobbleSpeed;
		[ModelLib.CustomAttributes.Default("0.26")]
		[ModelLib.CustomAttributes.DisplayGroup("Wobbles")]
		[ModelLib.CustomAttributes.Max(100.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single GearChangeWobbleSpeed {
			get { return _GearChangeWobbleSpeed; }
			set { SetField(ref _GearChangeWobbleSpeed, value, () => GearChangeWobbleSpeed); }
		}

		private System.Single _GearChangeWobbleSpeedVariance;
		[ModelLib.CustomAttributes.Default("0.2")]
		[ModelLib.CustomAttributes.DisplayGroup("Wobbles")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single GearChangeWobbleSpeedVariance {
			get { return _GearChangeWobbleSpeedVariance; }
			set { SetField(ref _GearChangeWobbleSpeedVariance, value, () => GearChangeWobbleSpeedVariance); }
		}

		private System.Single _GearChangeWobblePitch;
		[ModelLib.CustomAttributes.Default("0.15")]
		[ModelLib.CustomAttributes.DisplayGroup("Wobbles")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single GearChangeWobblePitch {
			get { return _GearChangeWobblePitch; }
			set { SetField(ref _GearChangeWobblePitch, value, () => GearChangeWobblePitch); }
		}

		private System.Single _GearChangeWobblePitchVariance;
		[ModelLib.CustomAttributes.Default("0.2")]
		[ModelLib.CustomAttributes.DisplayGroup("Wobbles")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single GearChangeWobblePitchVariance {
			get { return _GearChangeWobblePitchVariance; }
			set { SetField(ref _GearChangeWobblePitchVariance, value, () => GearChangeWobblePitchVariance); }
		}

		private System.Single _GearChangeWobbleVolume;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Wobbles")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single GearChangeWobbleVolume {
			get { return _GearChangeWobbleVolume; }
			set { SetField(ref _GearChangeWobbleVolume, value, () => GearChangeWobbleVolume); }
		}

		private System.Single _GearChangeWobbleVolumeVariance;
		[ModelLib.CustomAttributes.Default("0.2")]
		[ModelLib.CustomAttributes.DisplayGroup("Wobbles")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single GearChangeWobbleVolumeVariance {
			get { return _GearChangeWobbleVolumeVariance; }
			set { SetField(ref _GearChangeWobbleVolumeVariance, value, () => GearChangeWobbleVolumeVariance); }
		}

		private System.Int32 _EngineClutchAttenuation_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 EngineClutchAttenuation_PostSubmix {
			get { return _EngineClutchAttenuation_PostSubmix; }
			set { SetField(ref _EngineClutchAttenuation_PostSubmix, value, () => EngineClutchAttenuation_PostSubmix); }
		}

		private System.Int32 _ExhaustClutchAttenuation_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ExhaustClutchAttenuation_PostSubmix {
			get { return _ExhaustClutchAttenuation_PostSubmix; }
			set { SetField(ref _ExhaustClutchAttenuation_PostSubmix, value, () => ExhaustClutchAttenuation_PostSubmix); }
		}

		private ModelLib.Types.TriState _Publish;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
		public ModelLib.Types.TriState Publish {
			get { return _Publish; }
			set { SetField(ref _Publish, value, () => Publish); }
		}

		private ModelLib.ObjectRef _EngineSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSynthDef {
			get { return _EngineSynthDef; }
			set { SetField(ref _EngineSynthDef, value, () => EngineSynthDef); }
		}

		private ModelLib.ObjectRef _EngineSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSynthPreset {
			get { return _EngineSynthPreset; }
			set { SetField(ref _EngineSynthPreset, value, () => EngineSynthPreset); }
		}

		private ModelLib.ObjectRef _ExhaustSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSynthDef {
			get { return _ExhaustSynthDef; }
			set { SetField(ref _ExhaustSynthDef, value, () => ExhaustSynthDef); }
		}

		private ModelLib.ObjectRef _ExhaustSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSynthPreset {
			get { return _ExhaustSynthPreset; }
			set { SetField(ref _ExhaustSynthPreset, value, () => ExhaustSynthPreset); }
		}

		private ModelLib.ObjectRef _NPCEngineAccel;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("NPC")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef NPCEngineAccel {
			get { return _NPCEngineAccel; }
			set { SetField(ref _NPCEngineAccel, value, () => NPCEngineAccel); }
		}

		private ModelLib.ObjectRef _NPCExhaustAccel;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("NPC")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef NPCExhaustAccel {
			get { return _NPCExhaustAccel; }
			set { SetField(ref _NPCExhaustAccel, value, () => NPCExhaustAccel); }
		}

		private ModelLib.Types.TriState _HasRevLimiter;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Rev Limiter")]
		public ModelLib.Types.TriState HasRevLimiter {
			get { return _HasRevLimiter; }
			set { SetField(ref _HasRevLimiter, value, () => HasRevLimiter); }
		}

		private ModelLib.ObjectRef _RevLimiterPopSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Rev Limiter")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RevLimiterPopSound {
			get { return _RevLimiterPopSound; }
			set { SetField(ref _RevLimiterPopSound, value, () => RevLimiterPopSound); }
		}

		private ModelLib.Types.TriState _VisualFXOnLimiter;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Rev Limiter")]
		public ModelLib.Types.TriState VisualFXOnLimiter {
			get { return _VisualFXOnLimiter; }
			set { SetField(ref _VisualFXOnLimiter, value, () => VisualFXOnLimiter); }
		}

		private System.UInt32 _MinRPMOverride;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
		[ModelLib.CustomAttributes.Max(15000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 MinRPMOverride {
			get { return _MinRPMOverride; }
			set { SetField(ref _MinRPMOverride, value, () => MinRPMOverride); }
		}

		private System.UInt32 _MaxRPMOverride;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
		[ModelLib.CustomAttributes.Max(15000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 MaxRPMOverride {
			get { return _MaxRPMOverride; }
			set { SetField(ref _MaxRPMOverride, value, () => MaxRPMOverride); }
		}

		private ModelLib.ObjectRef _EngineSubmixVoice;
		[ModelLib.CustomAttributes.Default("DEFAULT_CAR_ENGINE_SUBMIX_CONTROL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSubmixVoice {
			get { return _EngineSubmixVoice; }
			set { SetField(ref _EngineSubmixVoice, value, () => EngineSubmixVoice); }
		}

		private ModelLib.ObjectRef _ExhaustSubmixVoice;
		[ModelLib.CustomAttributes.Default("DEFAULT_CAR_EXHAUST_SUBMIX_CONTROL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSubmixVoice {
			get { return _ExhaustSubmixVoice; }
			set { SetField(ref _ExhaustSubmixVoice, value, () => ExhaustSubmixVoice); }
		}

		private System.Int32 _ExhaustProximityVolume_PostSubmix;
		[ModelLib.CustomAttributes.Default("600")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ExhaustProximityVolume_PostSubmix {
			get { return _ExhaustProximityVolume_PostSubmix; }
			set { SetField(ref _ExhaustProximityVolume_PostSubmix, value, () => ExhaustProximityVolume_PostSubmix); }
		}

		private ModelLib.Types.TriState _ExhaustProximityMixerSceneEnabled;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		public ModelLib.Types.TriState ExhaustProximityMixerSceneEnabled {
			get { return _ExhaustProximityMixerSceneEnabled; }
			set { SetField(ref _ExhaustProximityMixerSceneEnabled, value, () => ExhaustProximityMixerSceneEnabled); }
		}

		private System.UInt32 _RevLimiterGrainsToPlay;
		[ModelLib.CustomAttributes.Default("5")]
		[ModelLib.CustomAttributes.DisplayGroup("Rev Limiter")]
		[ModelLib.CustomAttributes.Max(30)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 RevLimiterGrainsToPlay {
			get { return _RevLimiterGrainsToPlay; }
			set { SetField(ref _RevLimiterGrainsToPlay, value, () => RevLimiterGrainsToPlay); }
		}

		private System.UInt32 _RevLimiterGrainsToSkip;
		[ModelLib.CustomAttributes.Default("2")]
		[ModelLib.CustomAttributes.DisplayGroup("Rev Limiter")]
		[ModelLib.CustomAttributes.Max(30)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 RevLimiterGrainsToSkip {
			get { return _RevLimiterGrainsToSkip; }
			set { SetField(ref _RevLimiterGrainsToSkip, value, () => RevLimiterGrainsToSkip); }
		}

		private ModelLib.ObjectRef _SynchronisedSynth;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SynchronisedSynth {
			get { return _SynchronisedSynth; }
			set { SetField(ref _SynchronisedSynth, value, () => SynchronisedSynth); }
		}

		private System.Int32 _UpgradedEngineVolumeBoost_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 UpgradedEngineVolumeBoost_PostSubmix {
			get { return _UpgradedEngineVolumeBoost_PostSubmix; }
			set { SetField(ref _UpgradedEngineVolumeBoost_PostSubmix, value, () => UpgradedEngineVolumeBoost_PostSubmix); }
		}

		private ModelLib.ObjectRef _UpgradedEngineSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedEngineSynthDef {
			get { return _UpgradedEngineSynthDef; }
			set { SetField(ref _UpgradedEngineSynthDef, value, () => UpgradedEngineSynthDef); }
		}

		private ModelLib.ObjectRef _UpgradedEngineSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedEngineSynthPreset {
			get { return _UpgradedEngineSynthPreset; }
			set { SetField(ref _UpgradedEngineSynthPreset, value, () => UpgradedEngineSynthPreset); }
		}

		private System.Int32 _UpgradedExhaustVolumeBoost_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 UpgradedExhaustVolumeBoost_PostSubmix {
			get { return _UpgradedExhaustVolumeBoost_PostSubmix; }
			set { SetField(ref _UpgradedExhaustVolumeBoost_PostSubmix, value, () => UpgradedExhaustVolumeBoost_PostSubmix); }
		}

		private ModelLib.ObjectRef _UpgradedExhaustSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedExhaustSynthDef {
			get { return _UpgradedExhaustSynthDef; }
			set { SetField(ref _UpgradedExhaustSynthDef, value, () => UpgradedExhaustSynthDef); }
		}

		private ModelLib.ObjectRef _UpgradedExhaustSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedExhaustSynthPreset {
			get { return _UpgradedExhaustSynthPreset; }
			set { SetField(ref _UpgradedExhaustSynthPreset, value, () => UpgradedExhaustSynthPreset); }
		}

		private ModelLib.ObjectRef _DamageSynthHashList;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundHashList))]
		[ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DamageSynthHashList {
			get { return _DamageSynthHashList; }
			set { SetField(ref _DamageSynthHashList, value, () => DamageSynthHashList); }
		}

		private ModelLib.ObjectRef _UpgradedRevLimiterPop;
		[ModelLib.CustomAttributes.Default("UPGRADE_POPS_MULTI")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Upgrades")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UpgradedRevLimiterPop {
			get { return _UpgradedRevLimiterPop; }
			set { SetField(ref _UpgradedRevLimiterPop, value, () => UpgradedRevLimiterPop); }
		}

		private System.Int32 _EngineIdleVolume_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 EngineIdleVolume_PostSubmix {
			get { return _EngineIdleVolume_PostSubmix; }
			set { SetField(ref _EngineIdleVolume_PostSubmix, value, () => EngineIdleVolume_PostSubmix); }
		}

		private System.Int32 _ExhaustIdleVolume_PostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ExhaustIdleVolume_PostSubmix {
			get { return _ExhaustIdleVolume_PostSubmix; }
			set { SetField(ref _ExhaustIdleVolume_PostSubmix, value, () => ExhaustIdleVolume_PostSubmix); }
		}

		private System.Int32 _StartupRevsVolumeBoostEngine_PostSubmix;
		[ModelLib.CustomAttributes.Default("500")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 StartupRevsVolumeBoostEngine_PostSubmix {
			get { return _StartupRevsVolumeBoostEngine_PostSubmix; }
			set { SetField(ref _StartupRevsVolumeBoostEngine_PostSubmix, value, () => StartupRevsVolumeBoostEngine_PostSubmix); }
		}

		private System.Int32 _StartupRevsVolumeBoostExhaust_PostSubmix;
		[ModelLib.CustomAttributes.Default("500")]
		[ModelLib.CustomAttributes.DisplayGroup("Post Submix")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 StartupRevsVolumeBoostExhaust_PostSubmix {
			get { return _StartupRevsVolumeBoostExhaust_PostSubmix; }
			set { SetField(ref _StartupRevsVolumeBoostExhaust_PostSubmix, value, () => StartupRevsVolumeBoostExhaust_PostSubmix); }
		}

	}
}


