using ModelLib.CustomAttributes;

namespace model {

	public enum HumanPedTypes {
		[ModelLib.CustomAttributes.Display("Michael")]
		AUD_PEDTYPE_MICHAEL,
		[ModelLib.CustomAttributes.Display("Franklin")]
		AUD_PEDTYPE_FRANKLIN,
		[ModelLib.CustomAttributes.Display("Trevor")]
		AUD_PEDTYPE_TREVOR,
		[ModelLib.CustomAttributes.Display("Man")]
		AUD_PEDTYPE_MAN,
		[ModelLib.CustomAttributes.Display("Woman")]
		AUD_PEDTYPE_WOMAN,
		[ModelLib.CustomAttributes.Display("Gang")]
		AUD_PEDTYPE_GANG,
		[ModelLib.CustomAttributes.Display("Cop")]
		AUD_PEDTYPE_COP
	}

}