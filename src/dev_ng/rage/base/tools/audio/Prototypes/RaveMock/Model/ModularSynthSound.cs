using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Synth Sound Types")]
	public class ModularSynthSound:  Sound{

		private ModelLib.ObjectRef _SynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SynthDef {
			get { return _SynthDef; }
			set { SetField(ref _SynthDef, value, () => SynthDef); }
		}

		private ModelLib.ObjectRef _Preset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Preset {
			get { return _Preset; }
			set { SetField(ref _Preset, value, () => Preset); }
		}

		private System.Single _PlayBackTimeLimit;
		[ModelLib.CustomAttributes.Default("-1.0")]
		[ModelLib.CustomAttributes.Max(120.0)]
		[ModelLib.CustomAttributes.Min(-1.0)]
		[ModelLib.CustomAttributes.Unit("seconds")]
		public System.Single PlayBackTimeLimit {
			get { return _PlayBackTimeLimit; }
			set { SetField(ref _PlayBackTimeLimit, value, () => PlayBackTimeLimit); }
		}

		private VirtualisationBehaviour _VirtualisationMode;
		[ModelLib.CustomAttributes.Default("kVirtualisationNormal")]
		public VirtualisationBehaviour VirtualisationMode {
			get { return _VirtualisationMode; }
			set { SetField(ref _VirtualisationMode, value, () => VirtualisationMode); }
		}

		private System.Byte _pad1;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Byte pad1 {
			get { return _pad1; }
			set { SetField(ref _pad1, value, () => pad1); }
		}

		private System.UInt16 _pad0;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt16 pad0 {
			get { return _pad0; }
			set { SetField(ref _pad0, value, () => pad0); }
		}

		private ModelLib.Types.TriState _ReleaseThroughSynth;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState ReleaseThroughSynth {
			get { return _ReleaseThroughSynth; }
			set { SetField(ref _ReleaseThroughSynth, value, () => ReleaseThroughSynth); }
		}

		public class EnvironmentSoundDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundRef;
			[ModelLib.CustomAttributes.AllowedType(typeof(EnvironmentSound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef SoundRef {
				get { return _SoundRef; }
				set { SetField(ref _SoundRef, value, () => SoundRef); }
			}

		}
		private ItemsObservableCollection<EnvironmentSoundDefinition> _EnvironmentSound = new ItemsObservableCollection<EnvironmentSoundDefinition>();
		[ModelLib.CustomAttributes.FixedSize]
		[ModelLib.CustomAttributes.MaxSize(4, typeof(System.UInt32))]
		[XmlElement("EnvironmentSound")]
		public ItemsObservableCollection<EnvironmentSoundDefinition> EnvironmentSound {
			get { return _EnvironmentSound; }
			set { SetField(ref _EnvironmentSound, value, () => EnvironmentSound); }
		}

		public class ExposedVariableDefinition: ModelBase {
			private System.String _SynthKey;
			public System.String SynthKey {
				get { return _SynthKey; }
				set { SetField(ref _SynthKey, value, () => SynthKey); }
			}

			private System.String _Variable;
			[ModelLib.CustomAttributes.Unit("variable")]
			public System.String Variable {
				get { return _Variable; }
				set { SetField(ref _Variable, value, () => Variable); }
			}

			private System.Single _Value;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single Value {
				get { return _Value; }
				set { SetField(ref _Value, value, () => Value); }
			}

		}
		private ItemsObservableCollection<ExposedVariableDefinition> _ExposedVariable = new ItemsObservableCollection<ExposedVariableDefinition>();
		[ModelLib.CustomAttributes.MaxSize(64, typeof(System.UInt32))]
		[XmlElement("ExposedVariable")]
		public ItemsObservableCollection<ExposedVariableDefinition> ExposedVariable {
			get { return _ExposedVariable; }
			set { SetField(ref _ExposedVariable, value, () => ExposedVariable); }
		}

	}
}


