using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class VoiceContextVariationMask:  ModelBase {

		private System.UInt16 _variationMask;
		public System.UInt16 variationMask {
			get { return _variationMask; }
			set { SetField(ref _variationMask, value, () => variationMask); }
		}

	}
}


