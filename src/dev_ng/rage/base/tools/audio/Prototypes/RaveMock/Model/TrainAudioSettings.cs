using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class TrainAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _DriveTone;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DriveTone {
			get { return _DriveTone; }
			set { SetField(ref _DriveTone, value, () => DriveTone); }
		}

		private ModelLib.ObjectRef _DriveToneSynth;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DriveToneSynth {
			get { return _DriveToneSynth; }
			set { SetField(ref _DriveToneSynth, value, () => DriveToneSynth); }
		}

		private ModelLib.ObjectRef _IdleLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IdleLoop {
			get { return _IdleLoop; }
			set { SetField(ref _IdleLoop, value, () => IdleLoop); }
		}

		private ModelLib.ObjectRef _Brakes;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Brakes {
			get { return _Brakes; }
			set { SetField(ref _Brakes, value, () => Brakes); }
		}

		private ModelLib.ObjectRef _BigBrakeRelease;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BigBrakeRelease {
			get { return _BigBrakeRelease; }
			set { SetField(ref _BigBrakeRelease, value, () => BigBrakeRelease); }
		}

		private ModelLib.ObjectRef _BrakeRelease;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BrakeRelease {
			get { return _BrakeRelease; }
			set { SetField(ref _BrakeRelease, value, () => BrakeRelease); }
		}

		private ModelLib.ObjectRef _WheelDry;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WheelDry {
			get { return _WheelDry; }
			set { SetField(ref _WheelDry, value, () => WheelDry); }
		}

		private ModelLib.ObjectRef _AirHornOneShot;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AirHornOneShot {
			get { return _AirHornOneShot; }
			set { SetField(ref _AirHornOneShot, value, () => AirHornOneShot); }
		}

		private ModelLib.ObjectRef _BellLoopCrosing;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BellLoopCrosing {
			get { return _BellLoopCrosing; }
			set { SetField(ref _BellLoopCrosing, value, () => BellLoopCrosing); }
		}

		private ModelLib.ObjectRef _BellLoopEngine;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BellLoopEngine {
			get { return _BellLoopEngine; }
			set { SetField(ref _BellLoopEngine, value, () => BellLoopEngine); }
		}

		private ModelLib.ObjectRef _AmbientCarriage;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AmbientCarriage {
			get { return _AmbientCarriage; }
			set { SetField(ref _AmbientCarriage, value, () => AmbientCarriage); }
		}

		private ModelLib.ObjectRef _AmbientRumble;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AmbientRumble {
			get { return _AmbientRumble; }
			set { SetField(ref _AmbientRumble, value, () => AmbientRumble); }
		}

		private ModelLib.ObjectRef _AmbientGrind;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AmbientGrind {
			get { return _AmbientGrind; }
			set { SetField(ref _AmbientGrind, value, () => AmbientGrind); }
		}

		private ModelLib.ObjectRef _AmbientSqueal;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AmbientSqueal {
			get { return _AmbientSqueal; }
			set { SetField(ref _AmbientSqueal, value, () => AmbientSqueal); }
		}

		private ModelLib.ObjectRef _CarriagePitchCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_CARRIAGE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CarriagePitchCurve {
			get { return _CarriagePitchCurve; }
			set { SetField(ref _CarriagePitchCurve, value, () => CarriagePitchCurve); }
		}

		private ModelLib.ObjectRef _CarriageVolumeCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_CARRIAGE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CarriageVolumeCurve {
			get { return _CarriageVolumeCurve; }
			set { SetField(ref _CarriageVolumeCurve, value, () => CarriageVolumeCurve); }
		}

		private ModelLib.ObjectRef _DriveTonePitchCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_DRIVE_TONE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DriveTonePitchCurve {
			get { return _DriveTonePitchCurve; }
			set { SetField(ref _DriveTonePitchCurve, value, () => DriveTonePitchCurve); }
		}

		private ModelLib.ObjectRef _DriveToneVolumeCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_DRIVE_TONE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DriveToneVolumeCurve {
			get { return _DriveToneVolumeCurve; }
			set { SetField(ref _DriveToneVolumeCurve, value, () => DriveToneVolumeCurve); }
		}

		private ModelLib.ObjectRef _DriveToneSynthPitchCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_DRIVE_TONE_SYNTH_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DriveToneSynthPitchCurve {
			get { return _DriveToneSynthPitchCurve; }
			set { SetField(ref _DriveToneSynthPitchCurve, value, () => DriveToneSynthPitchCurve); }
		}

		private ModelLib.ObjectRef _DriveToneSynthVolumeCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_DRIVE_TONE_SYNTH_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DriveToneSynthVolumeCurve {
			get { return _DriveToneSynthVolumeCurve; }
			set { SetField(ref _DriveToneSynthVolumeCurve, value, () => DriveToneSynthVolumeCurve); }
		}

		private ModelLib.ObjectRef _GrindPitchCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_GRIND_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GrindPitchCurve {
			get { return _GrindPitchCurve; }
			set { SetField(ref _GrindPitchCurve, value, () => GrindPitchCurve); }
		}

		private ModelLib.ObjectRef _GrindVolumeCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_GRIND_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GrindVolumeCurve {
			get { return _GrindVolumeCurve; }
			set { SetField(ref _GrindVolumeCurve, value, () => GrindVolumeCurve); }
		}

		private ModelLib.ObjectRef _TrainIdlePitchCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_IDLE_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TrainIdlePitchCurve {
			get { return _TrainIdlePitchCurve; }
			set { SetField(ref _TrainIdlePitchCurve, value, () => TrainIdlePitchCurve); }
		}

		private ModelLib.ObjectRef _TrainIdleVolumeCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_IDLE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TrainIdleVolumeCurve {
			get { return _TrainIdleVolumeCurve; }
			set { SetField(ref _TrainIdleVolumeCurve, value, () => TrainIdleVolumeCurve); }
		}

		private ModelLib.ObjectRef _SquealPitchCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_SQUEAL_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SquealPitchCurve {
			get { return _SquealPitchCurve; }
			set { SetField(ref _SquealPitchCurve, value, () => SquealPitchCurve); }
		}

		private ModelLib.ObjectRef _SquealVolumeCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_SQUEAL_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SquealVolumeCurve {
			get { return _SquealVolumeCurve; }
			set { SetField(ref _SquealVolumeCurve, value, () => SquealVolumeCurve); }
		}

		private ModelLib.ObjectRef _ScrapeSpeedVolumeCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_SCRAPE_SPEED_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScrapeSpeedVolumeCurve {
			get { return _ScrapeSpeedVolumeCurve; }
			set { SetField(ref _ScrapeSpeedVolumeCurve, value, () => ScrapeSpeedVolumeCurve); }
		}

		private ModelLib.ObjectRef _WheelVolumeCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_WHEEL_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WheelVolumeCurve {
			get { return _WheelVolumeCurve; }
			set { SetField(ref _WheelVolumeCurve, value, () => WheelVolumeCurve); }
		}

		private ModelLib.ObjectRef _WheelDelayCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_WHEEL_DELAY")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WheelDelayCurve {
			get { return _WheelDelayCurve; }
			set { SetField(ref _WheelDelayCurve, value, () => WheelDelayCurve); }
		}

		private ModelLib.ObjectRef _RumbleVolumeCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_RUMBLE_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RumbleVolumeCurve {
			get { return _RumbleVolumeCurve; }
			set { SetField(ref _RumbleVolumeCurve, value, () => RumbleVolumeCurve); }
		}

		private ModelLib.ObjectRef _BrakeVelocityPitchCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_BRAKE_VELOCITY_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BrakeVelocityPitchCurve {
			get { return _BrakeVelocityPitchCurve; }
			set { SetField(ref _BrakeVelocityPitchCurve, value, () => BrakeVelocityPitchCurve); }
		}

		private ModelLib.ObjectRef _BrakeVelocityVolumeCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_BRAKE_VELOCITY_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BrakeVelocityVolumeCurve {
			get { return _BrakeVelocityVolumeCurve; }
			set { SetField(ref _BrakeVelocityVolumeCurve, value, () => BrakeVelocityVolumeCurve); }
		}

		private ModelLib.ObjectRef _BrakeAccelerationPitchCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_BRAKE_ACCELERATION_PITCH")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BrakeAccelerationPitchCurve {
			get { return _BrakeAccelerationPitchCurve; }
			set { SetField(ref _BrakeAccelerationPitchCurve, value, () => BrakeAccelerationPitchCurve); }
		}

		private ModelLib.ObjectRef _BrakeAccelerationVolumeCurve;
		[ModelLib.CustomAttributes.Default("SUBWAY_BRAKE_ACCELERATION_VOLUME")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BrakeAccelerationVolumeCurve {
			get { return _BrakeAccelerationVolumeCurve; }
			set { SetField(ref _BrakeAccelerationVolumeCurve, value, () => BrakeAccelerationVolumeCurve); }
		}

		private ModelLib.ObjectRef _TrainApproachTrackRumble;
		[ModelLib.CustomAttributes.Default("TRAIN_APPROACH_TRACK_RUMBLE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TrainApproachTrackRumble {
			get { return _TrainApproachTrackRumble; }
			set { SetField(ref _TrainApproachTrackRumble, value, () => TrainApproachTrackRumble); }
		}

		private ModelLib.ObjectRef _TrackRumbleDistanceToIntensity;
		[ModelLib.CustomAttributes.Default("SUBWAY_RUMBLE_DISTANCE_TO_INTENSITY")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TrackRumbleDistanceToIntensity {
			get { return _TrackRumbleDistanceToIntensity; }
			set { SetField(ref _TrackRumbleDistanceToIntensity, value, () => TrackRumbleDistanceToIntensity); }
		}

		private ModelLib.ObjectRef _TrainDistanceToRollOffScale;
		[ModelLib.CustomAttributes.Default("CONSTANT_ONE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TrainDistanceToRollOffScale {
			get { return _TrainDistanceToRollOffScale; }
			set { SetField(ref _TrainDistanceToRollOffScale, value, () => TrainDistanceToRollOffScale); }
		}

		private ModelLib.ObjectRef _VehicleCollisions;
		[ModelLib.CustomAttributes.Default("VEHICLE_COLLISION_TRAIN")]
		[ModelLib.CustomAttributes.AllowedType(typeof(VehicleCollisionSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehicleCollisions {
			get { return _VehicleCollisions; }
			set { SetField(ref _VehicleCollisions, value, () => VehicleCollisions); }
		}

	}
}


