using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class SpeechContext:  AudBaseObject{

		private System.String _ContextNamePHash;
		public System.String ContextNamePHash {
			get { return _ContextNamePHash; }
			set { SetField(ref _ContextNamePHash, value, () => ContextNamePHash); }
		}

		private ModelLib.Types.TriState _AllowRepeat;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState AllowRepeat {
			get { return _AllowRepeat; }
			set { SetField(ref _AllowRepeat, value, () => AllowRepeat); }
		}

		private ModelLib.Types.TriState _ForcePlay;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState ForcePlay {
			get { return _ForcePlay; }
			set { SetField(ref _ForcePlay, value, () => ForcePlay); }
		}

		private System.Int32 _RepeatTime;
		[ModelLib.CustomAttributes.Default("0")]
		public System.Int32 RepeatTime {
			get { return _RepeatTime; }
			set { SetField(ref _RepeatTime, value, () => RepeatTime); }
		}

		private System.Int32 _RepeatTimeOnSameVoice;
		[ModelLib.CustomAttributes.Default("-1")]
		public System.Int32 RepeatTimeOnSameVoice {
			get { return _RepeatTimeOnSameVoice; }
			set { SetField(ref _RepeatTimeOnSameVoice, value, () => RepeatTimeOnSameVoice); }
		}

		private SpeechVolumeType _VolumeType;
		[ModelLib.CustomAttributes.Default("CONTEXT_VOLUME_NORMAL")]
		public SpeechVolumeType VolumeType {
			get { return _VolumeType; }
			set { SetField(ref _VolumeType, value, () => VolumeType); }
		}

		private SpeechAudibilityType _Audibility;
		[ModelLib.CustomAttributes.Default("CONTEXT_AUDIBILITY_NORMAL")]
		public SpeechAudibilityType Audibility {
			get { return _Audibility; }
			set { SetField(ref _Audibility, value, () => Audibility); }
		}

		private ModelLib.Types.TriState _IsConversation;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState IsConversation {
			get { return _IsConversation; }
			set { SetField(ref _IsConversation, value, () => IsConversation); }
		}

		private ModelLib.Types.TriState _IsPain;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState IsPain {
			get { return _IsPain; }
			set { SetField(ref _IsPain, value, () => IsPain); }
		}

		private ModelLib.Types.TriState _IsCombat;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState IsCombat {
			get { return _IsCombat; }
			set { SetField(ref _IsCombat, value, () => IsCombat); }
		}

		private ModelLib.Types.TriState _IsOkayOnMission;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState IsOkayOnMission {
			get { return _IsOkayOnMission; }
			set { SetField(ref _IsOkayOnMission, value, () => IsOkayOnMission); }
		}

		private ModelLib.Types.TriState _IsUrgent;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState IsUrgent {
			get { return _IsUrgent; }
			set { SetField(ref _IsUrgent, value, () => IsUrgent); }
		}

		private System.String _GenderNonSpecificVersion;
		public System.String GenderNonSpecificVersion {
			get { return _GenderNonSpecificVersion; }
			set { SetField(ref _GenderNonSpecificVersion, value, () => GenderNonSpecificVersion); }
		}

		private System.UInt32 _TimeCanNextPlay;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 TimeCanNextPlay {
			get { return _TimeCanNextPlay; }
			set { SetField(ref _TimeCanNextPlay, value, () => TimeCanNextPlay); }
		}

		private System.Byte _Priority;
		[ModelLib.CustomAttributes.Default("3")]
		[ModelLib.CustomAttributes.Max(10)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Byte Priority {
			get { return _Priority; }
			set { SetField(ref _Priority, value, () => Priority); }
		}

		private ModelLib.Types.TriState _IsMutedDuringSlowMo;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState IsMutedDuringSlowMo {
			get { return _IsMutedDuringSlowMo; }
			set { SetField(ref _IsMutedDuringSlowMo, value, () => IsMutedDuringSlowMo); }
		}

		private ModelLib.Types.TriState _IsPitchedDuringSlowMo;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState IsPitchedDuringSlowMo {
			get { return _IsPitchedDuringSlowMo; }
			set { SetField(ref _IsPitchedDuringSlowMo, value, () => IsPitchedDuringSlowMo); }
		}

		public class FakeGestureDefinition: ModelBase {
			private FakeGesture _GestureEnum;
			[ModelLib.CustomAttributes.Default("kAbsolutelyGesture")]
			public FakeGesture GestureEnum {
				get { return _GestureEnum; }
				set { SetField(ref _GestureEnum, value, () => GestureEnum); }
			}

		}
		private ItemsObservableCollection<FakeGestureDefinition> _FakeGesture = new ItemsObservableCollection<FakeGestureDefinition>();
		[ModelLib.CustomAttributes.MaxSize(4)]
		[XmlElement("FakeGesture")]
		public ItemsObservableCollection<FakeGestureDefinition> FakeGesture {
			get { return _FakeGesture; }
			set { SetField(ref _FakeGesture, value, () => FakeGesture); }
		}

	}
}


