using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class Constant:  BaseCurve{

		private System.Single _Value;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Display("Value")]
		public System.Single Value {
			get { return _Value; }
			set { SetField(ref _Value, value, () => Value); }
		}

	}
}


