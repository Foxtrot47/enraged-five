using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Physics")]
	public class ModelAudioCollisionSettings:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private System.UInt32 _LastFragTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 LastFragTime {
			get { return _LastFragTime; }
			set { SetField(ref _LastFragTime, value, () => LastFragTime); }
		}

		private ModelLib.ObjectRef _MediumIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("FakeBulletImpactSoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef MediumIntensity {
			get { return _MediumIntensity; }
			set { SetField(ref _MediumIntensity, value, () => MediumIntensity); }
		}

		private ModelLib.ObjectRef _HighIntensity;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("FakeBulletImpactSoundsets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HighIntensity {
			get { return _HighIntensity; }
			set { SetField(ref _HighIntensity, value, () => HighIntensity); }
		}

		private ModelLib.ObjectRef _BreakSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BreakSound {
			get { return _BreakSound; }
			set { SetField(ref _BreakSound, value, () => BreakSound); }
		}

		private ModelLib.ObjectRef _DestroySound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DestroySound {
			get { return _DestroySound; }
			set { SetField(ref _DestroySound, value, () => DestroySound); }
		}

		private ModelLib.ObjectRef _UprootSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef UprootSound {
			get { return _UprootSound; }
			set { SetField(ref _UprootSound, value, () => UprootSound); }
		}

		private ModelLib.ObjectRef _WindSounds;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WindSounds {
			get { return _WindSounds; }
			set { SetField(ref _WindSounds, value, () => WindSounds); }
		}

		private ModelLib.ObjectRef _SwingSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SwingSound {
			get { return _SwingSound; }
			set { SetField(ref _SwingSound, value, () => SwingSound); }
		}

		private System.Single _MinSwingVel;
		[ModelLib.CustomAttributes.Default("0.1")]
		public System.Single MinSwingVel {
			get { return _MinSwingVel; }
			set { SetField(ref _MinSwingVel, value, () => MinSwingVel); }
		}

		private System.Single _MaxswingVel;
		[ModelLib.CustomAttributes.Default("5.0")]
		public System.Single MaxswingVel {
			get { return _MaxswingVel; }
			set { SetField(ref _MaxswingVel, value, () => MaxswingVel); }
		}

		private ModelLib.ObjectRef _RainLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RainLoop {
			get { return _RainLoop; }
			set { SetField(ref _RainLoop, value, () => RainLoop); }
		}

		private ModelLib.ObjectRef _ShockwaveSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ShockwaveSound {
			get { return _ShockwaveSound; }
			set { SetField(ref _ShockwaveSound, value, () => ShockwaveSound); }
		}

		private ModelLib.ObjectRef _RandomAmbient;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RandomAmbient {
			get { return _RandomAmbient; }
			set { SetField(ref _RandomAmbient, value, () => RandomAmbient); }
		}

		private ModelLib.ObjectRef _EntityResonance;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EntityResonance {
			get { return _EntityResonance; }
			set { SetField(ref _EntityResonance, value, () => EntityResonance); }
		}

		private AudioWeight _Weight;
		[ModelLib.CustomAttributes.Default("AUDIO_WEIGHT_M")]
		public AudioWeight Weight {
			get { return _Weight; }
			set { SetField(ref _Weight, value, () => Weight); }
		}

		private ModelLib.Types.TriState _IsResonant;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("Specifies whether the model will count as resonant for environment resonant impulses")]
		public ModelLib.Types.TriState IsResonant {
			get { return _IsResonant; }
			set { SetField(ref _IsResonant, value, () => IsResonant); }
		}

		private ModelLib.Types.TriState _TurnOffRainVolOnProps;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("Set to true stop the code from modulating the RainLoop volume based on the rain level.")]
		public ModelLib.Types.TriState TurnOffRainVolOnProps {
			get { return _TurnOffRainVolOnProps; }
			set { SetField(ref _TurnOffRainVolOnProps, value, () => TurnOffRainVolOnProps); }
		}

		private ModelLib.Types.TriState _SwingingProp;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("Prop will use swinging behaviour instead of rolling")]
		public ModelLib.Types.TriState SwingingProp {
			get { return _SwingingProp; }
			set { SetField(ref _SwingingProp, value, () => SwingingProp); }
		}

		public class MaterialOverrideDefinition: ModelBase {
			private ModelLib.ObjectRef _Material;
			[ModelLib.CustomAttributes.AllowedType(typeof(CollisionMaterialSettings))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef Material {
				get { return _Material; }
				set { SetField(ref _Material, value, () => Material); }
			}

			private ModelLib.ObjectRef _Override;
			[ModelLib.CustomAttributes.AllowedType(typeof(CollisionMaterialSettings))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef Override {
				get { return _Override; }
				set { SetField(ref _Override, value, () => Override); }
			}

		}
		private ItemsObservableCollection<MaterialOverrideDefinition> _MaterialOverride = new ItemsObservableCollection<MaterialOverrideDefinition>();
		[ModelLib.CustomAttributes.MaxSize(64)]
		[XmlElement("MaterialOverride")]
		public ItemsObservableCollection<MaterialOverrideDefinition> MaterialOverride {
			get { return _MaterialOverride; }
			set { SetField(ref _MaterialOverride, value, () => MaterialOverride); }
		}

		public class FragComponentSettingDefinition: ModelBase {
			private ModelLib.ObjectRef _ModelSettings;
			[ModelLib.CustomAttributes.AllowedType(typeof(ModelAudioCollisionSettings))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef ModelSettings {
				get { return _ModelSettings; }
				set { SetField(ref _ModelSettings, value, () => ModelSettings); }
			}

		}
		private ItemsObservableCollection<FragComponentSettingDefinition> _FragComponentSetting = new ItemsObservableCollection<FragComponentSettingDefinition>();
		[ModelLib.CustomAttributes.MaxSize(64, typeof(System.UInt32))]
		[XmlElement("FragComponentSetting")]
		public ItemsObservableCollection<FragComponentSettingDefinition> FragComponentSetting {
			get { return _FragComponentSetting; }
			set { SetField(ref _FragComponentSetting, value, () => FragComponentSetting); }
		}

	}
}


