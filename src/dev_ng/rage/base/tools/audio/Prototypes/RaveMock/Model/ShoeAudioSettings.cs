using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class ShoeAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _Walk;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Walk {
			get { return _Walk; }
			set { SetField(ref _Walk, value, () => Walk); }
		}

		private ModelLib.ObjectRef _DirtyWalk;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DirtyWalk {
			get { return _DirtyWalk; }
			set { SetField(ref _DirtyWalk, value, () => DirtyWalk); }
		}

		private ModelLib.ObjectRef _CreakyWalk;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CreakyWalk {
			get { return _CreakyWalk; }
			set { SetField(ref _CreakyWalk, value, () => CreakyWalk); }
		}

		private ModelLib.ObjectRef _GlassyWalk;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GlassyWalk {
			get { return _GlassyWalk; }
			set { SetField(ref _GlassyWalk, value, () => GlassyWalk); }
		}

		private ModelLib.ObjectRef _Run;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Run {
			get { return _Run; }
			set { SetField(ref _Run, value, () => Run); }
		}

		private ModelLib.ObjectRef _DirtyRun;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DirtyRun {
			get { return _DirtyRun; }
			set { SetField(ref _DirtyRun, value, () => DirtyRun); }
		}

		private ModelLib.ObjectRef _CreakyRun;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CreakyRun {
			get { return _CreakyRun; }
			set { SetField(ref _CreakyRun, value, () => CreakyRun); }
		}

		private ModelLib.ObjectRef _GlassyRun;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GlassyRun {
			get { return _GlassyRun; }
			set { SetField(ref _GlassyRun, value, () => GlassyRun); }
		}

		private ModelLib.ObjectRef _WetWalk;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WetWalk {
			get { return _WetWalk; }
			set { SetField(ref _WetWalk, value, () => WetWalk); }
		}

		private ModelLib.ObjectRef _WetRun;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WetRun {
			get { return _WetRun; }
			set { SetField(ref _WetRun, value, () => WetRun); }
		}

		private ModelLib.ObjectRef _SoftWalk;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SoftWalk {
			get { return _SoftWalk; }
			set { SetField(ref _SoftWalk, value, () => SoftWalk); }
		}

		private ModelLib.ObjectRef _Scuff;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Scuff {
			get { return _Scuff; }
			set { SetField(ref _Scuff, value, () => Scuff); }
		}

		private ModelLib.ObjectRef _Land;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Land {
			get { return _Land; }
			set { SetField(ref _Land, value, () => Land); }
		}

		private System.Single _ShoeHardness;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		[ModelLib.CustomAttributes.Unit("%")]
		public System.Single ShoeHardness {
			get { return _ShoeHardness; }
			set { SetField(ref _ShoeHardness, value, () => ShoeHardness); }
		}

		private System.Single _ShoeCreakiness;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		[ModelLib.CustomAttributes.Unit("%")]
		public System.Single ShoeCreakiness {
			get { return _ShoeCreakiness; }
			set { SetField(ref _ShoeCreakiness, value, () => ShoeCreakiness); }
		}

		private ShoeTypes _ShoeType;
		[ModelLib.CustomAttributes.Default("SHOE_TYPE_SHOES")]
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		public ShoeTypes ShoeType {
			get { return _ShoeType; }
			set { SetField(ref _ShoeType, value, () => ShoeType); }
		}

		private System.Single _ShoeFitness;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single ShoeFitness {
			get { return _ShoeFitness; }
			set { SetField(ref _ShoeFitness, value, () => ShoeFitness); }
		}

		private ModelLib.ObjectRef _LadderShoeDown;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LadderShoeDown {
			get { return _LadderShoeDown; }
			set { SetField(ref _LadderShoeDown, value, () => LadderShoeDown); }
		}

		private ModelLib.ObjectRef _LadderShoeUp;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LadderShoeUp {
			get { return _LadderShoeUp; }
			set { SetField(ref _LadderShoeUp, value, () => LadderShoeUp); }
		}

		private ModelLib.ObjectRef _Kick;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Sounds")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Kick {
			get { return _Kick; }
			set { SetField(ref _Kick, value, () => Kick); }
		}

	}
}


