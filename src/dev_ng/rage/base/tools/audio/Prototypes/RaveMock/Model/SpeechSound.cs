using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class SpeechSound:  Sound{

		private System.UInt32 _LastVariation;
		[ModelLib.CustomAttributes.Default("1")]
		public System.UInt32 LastVariation {
			get { return _LastVariation; }
			set { SetField(ref _LastVariation, value, () => LastVariation); }
		}

		private System.String _DynamicFieldName;
		public System.String DynamicFieldName {
			get { return _DynamicFieldName; }
			set { SetField(ref _DynamicFieldName, value, () => DynamicFieldName); }
		}

		private System.String _VoiceName;
		public System.String VoiceName {
			get { return _VoiceName; }
			set { SetField(ref _VoiceName, value, () => VoiceName); }
		}

		private System.String _ContextName;
		public System.String ContextName {
			get { return _ContextName; }
			set { SetField(ref _ContextName, value, () => ContextName); }
		}

	}
}


