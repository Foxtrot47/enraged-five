using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.FactoryCodeGenerated(false)]
	public class NoteMap:  ModelBase {

		public class RangeDefinition: ModelBase {
			private System.Byte _FirstNoteId;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(128)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte FirstNoteId {
				get { return _FirstNoteId; }
				set { SetField(ref _FirstNoteId, value, () => FirstNoteId); }
			}

			private System.Byte _LastNoteId;
			[ModelLib.CustomAttributes.Default("128")]
			[ModelLib.CustomAttributes.Max(128)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte LastNoteId {
				get { return _LastNoteId; }
				set { SetField(ref _LastNoteId, value, () => LastNoteId); }
			}

			private NoteRangeMode _Mode;
			[ModelLib.CustomAttributes.Default("NOTE_RANGE_TRACK_PITCH")]
			public NoteRangeMode Mode {
				get { return _Mode; }
				set { SetField(ref _Mode, value, () => Mode); }
			}

			private ModelLib.ObjectRef _SoundRef;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef SoundRef {
				get { return _SoundRef; }
				set { SetField(ref _SoundRef, value, () => SoundRef); }
			}

		}
		private ItemsObservableCollection<RangeDefinition> _Range = new ItemsObservableCollection<RangeDefinition>();
		[ModelLib.CustomAttributes.MaxSize(128)]
		[XmlElement("Range")]
		public ItemsObservableCollection<RangeDefinition> Range {
			get { return _Range; }
			set { SetField(ref _Range, value, () => Range); }
		}

	}
}


