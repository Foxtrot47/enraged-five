using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.FactoryCodeGenerated(false)]
	[ModelLib.CustomAttributes.SkipDeleteContainerOnRefDelete]
	public class SoundSet:  ModelBase {

		public class SoundsDefinition: ModelBase {
			private System.String _Name;
			public System.String Name {
				get { return _Name; }
				set { SetField(ref _Name, value, () => Name); }
			}

			private ModelLib.ObjectRef _Sound;
			[ModelLib.CustomAttributes.Default("NULL_SOUND")]
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef Sound {
				get { return _Sound; }
				set { SetField(ref _Sound, value, () => Sound); }
			}

			private System.String _Comments;
			[ModelLib.CustomAttributes.Ignore]
			public System.String Comments {
				get { return _Comments; }
				set { SetField(ref _Comments, value, () => Comments); }
			}

		}
		private ItemsObservableCollection<SoundsDefinition> _Sounds = new ItemsObservableCollection<SoundsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1000, typeof(System.UInt32))]
		[ModelLib.CustomAttributes.OrderBy("Name")]
		[XmlElement("Sounds")]
		public ItemsObservableCollection<SoundsDefinition> Sounds {
			get { return _Sounds; }
			set { SetField(ref _Sounds, value, () => Sounds); }
		}

	}
}


