using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class AutomationSound:  Sound{

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		private System.Single _PlaybackRate;
		[ModelLib.CustomAttributes.Default("1")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single PlaybackRate {
			get { return _PlaybackRate; }
			set { SetField(ref _PlaybackRate, value, () => PlaybackRate); }
		}

		private System.Single _PlaybackRateVariance;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single PlaybackRateVariance {
			get { return _PlaybackRateVariance; }
			set { SetField(ref _PlaybackRateVariance, value, () => PlaybackRateVariance); }
		}

		private System.String _PlaybackRateVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String PlaybackRateVariable {
			get { return _PlaybackRateVariable; }
			set { SetField(ref _PlaybackRateVariable, value, () => PlaybackRateVariable); }
		}

		private ModelLib.ObjectRef _NoteMap;
		[ModelLib.CustomAttributes.AllowedType(typeof(NoteMap))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef NoteMap {
			get { return _NoteMap; }
			set { SetField(ref _NoteMap, value, () => NoteMap); }
		}

		private ModelLib.Types.TriState _LoopSequence;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState LoopSequence {
			get { return _LoopSequence; }
			set { SetField(ref _LoopSequence, value, () => LoopSequence); }
		}

		public class DataRefDefinition: ModelBase {
			private System.String _BankName;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			[ModelLib.CustomAttributes.Unit("stringtableindex")]
			public System.String BankName {
				get { return _BankName; }
				set { SetField(ref _BankName, value, () => BankName); }
			}

			private System.String _WaveName;
			public System.String WaveName {
				get { return _WaveName; }
				set { SetField(ref _WaveName, value, () => WaveName); }
			}

		}
		private DataRefDefinition _DataRef;
		[ModelLib.CustomAttributes.Unit("WaveRef")]
		public DataRefDefinition DataRef {
			get { return _DataRef; }
			set { SetField(ref _DataRef, value, () => DataRef); }
		}

		public class VariableOutputsDefinition: ModelBase {
			private System.UInt32 _ChannelId;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(16)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.UInt32 ChannelId {
				get { return _ChannelId; }
				set { SetField(ref _ChannelId, value, () => ChannelId); }
			}

			private System.String _OutputVariable;
			[ModelLib.CustomAttributes.OutputVariable]
			[ModelLib.CustomAttributes.Unit("variable")]
			public System.String OutputVariable {
				get { return _OutputVariable; }
				set { SetField(ref _OutputVariable, value, () => OutputVariable); }
			}

		}
		private ItemsObservableCollection<VariableOutputsDefinition> _VariableOutputs = new ItemsObservableCollection<VariableOutputsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(8, typeof(System.UInt32))]
		[XmlElement("VariableOutputs")]
		public ItemsObservableCollection<VariableOutputsDefinition> VariableOutputs {
			get { return _VariableOutputs; }
			set { SetField(ref _VariableOutputs, value, () => VariableOutputs); }
		}

	}
}


