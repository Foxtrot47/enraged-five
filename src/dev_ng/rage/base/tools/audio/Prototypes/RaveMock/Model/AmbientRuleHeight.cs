using ModelLib.CustomAttributes;

namespace model {

	public enum AmbientRuleHeight {
		[ModelLib.CustomAttributes.Display("Random Within Zone")]
		AMBIENCE_HEIGHT_RANDOM,
		[ModelLib.CustomAttributes.Display("Listener Height")]
		AMBIENCE_LISTENER_HEIGHT,
		[ModelLib.CustomAttributes.Display("World Blanket Height")]
		AMBIENCE_WORLD_BLANKET_HEIGHT
	}

}