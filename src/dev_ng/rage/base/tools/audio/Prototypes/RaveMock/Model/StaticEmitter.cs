using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Transformer("{Type=\"rage.AmbientRuleCompiler.AmbientRuleTransformer, AmbientRuleCompiler, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\"}")]
	[ModelLib.CustomAttributes.Group("Ambience")]
	public class StaticEmitter:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private ModelLib.ObjectRef _Sound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Sound {
			get { return _Sound; }
			set { SetField(ref _Sound, value, () => Sound); }
		}

		private ModelLib.ObjectRef _RadioStation;
		[ModelLib.CustomAttributes.AllowedType(typeof(RadioStationSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RadioStation {
			get { return _RadioStation; }
			set { SetField(ref _RadioStation, value, () => RadioStation); }
		}

		private System.Single _MinDistance;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Description("Sound will only be played if distance to emitter is greater than MinDistance")]
		[ModelLib.CustomAttributes.Unit("m")]
		public System.Single MinDistance {
			get { return _MinDistance; }
			set { SetField(ref _MinDistance, value, () => MinDistance); }
		}

		private System.Single _MaxDistance;
		[ModelLib.CustomAttributes.Default("20.0")]
		[ModelLib.CustomAttributes.Description("Sound will only be played if distance to emitter is less than MaxDistance")]
		[ModelLib.CustomAttributes.Unit("m")]
		public System.Single MaxDistance {
			get { return _MaxDistance; }
			set { SetField(ref _MaxDistance, value, () => MaxDistance); }
		}

		private System.Int32 _EmittedVolume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(2000)]
		[ModelLib.CustomAttributes.Min(-6000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 EmittedVolume {
			get { return _EmittedVolume; }
			set { SetField(ref _EmittedVolume, value, () => EmittedVolume); }
		}

		private System.UInt16 _FilterCutoff;
		[ModelLib.CustomAttributes.Default("23900")]
		[ModelLib.CustomAttributes.Max(23900)]
		[ModelLib.CustomAttributes.Min(100)]
		[ModelLib.CustomAttributes.Unit("Hz")]
		public System.UInt16 FilterCutoff {
			get { return _FilterCutoff; }
			set { SetField(ref _FilterCutoff, value, () => FilterCutoff); }
		}

		private System.UInt16 _HPFCutoff;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(9000)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("Hz")]
		public System.UInt16 HPFCutoff {
			get { return _HPFCutoff; }
			set { SetField(ref _HPFCutoff, value, () => HPFCutoff); }
		}

		private System.UInt16 _RolloffFactor;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.Max(1000)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 RolloffFactor {
			get { return _RolloffFactor; }
			set { SetField(ref _RolloffFactor, value, () => RolloffFactor); }
		}

		private ModelLib.ObjectRef _InteriorSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(InteriorSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef InteriorSettings {
			get { return _InteriorSettings; }
			set { SetField(ref _InteriorSettings, value, () => InteriorSettings); }
		}

		private ModelLib.ObjectRef _InteriorRoom;
		[ModelLib.CustomAttributes.AllowedType(typeof(InteriorRoom))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef InteriorRoom {
			get { return _InteriorRoom; }
			set { SetField(ref _InteriorRoom, value, () => InteriorRoom); }
		}

		private ModelLib.ObjectRef _RadioStationForScore;
		[ModelLib.CustomAttributes.AllowedType(typeof(RadioStationSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RadioStationForScore {
			get { return _RadioStationForScore; }
			set { SetField(ref _RadioStationForScore, value, () => RadioStationForScore); }
		}

		private ModelLib.Types.TriState _Enabled;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState Enabled {
			get { return _Enabled; }
			set { SetField(ref _Enabled, value, () => Enabled); }
		}

		private ModelLib.Types.TriState _GeneratesBeats;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState GeneratesBeats {
			get { return _GeneratesBeats; }
			set { SetField(ref _GeneratesBeats, value, () => GeneratesBeats); }
		}

		private ModelLib.Types.TriState _LinkedToProp;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState LinkedToProp {
			get { return _LinkedToProp; }
			set { SetField(ref _LinkedToProp, value, () => LinkedToProp); }
		}

		private ModelLib.Types.TriState _ConedFront;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState ConedFront {
			get { return _ConedFront; }
			set { SetField(ref _ConedFront, value, () => ConedFront); }
		}

		private ModelLib.Types.TriState _FillsInteriorSpace;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState FillsInteriorSpace {
			get { return _FillsInteriorSpace; }
			set { SetField(ref _FillsInteriorSpace, value, () => FillsInteriorSpace); }
		}

		private System.Single _MaxLeakage;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single MaxLeakage {
			get { return _MaxLeakage; }
			set { SetField(ref _MaxLeakage, value, () => MaxLeakage); }
		}

		private System.UInt16 _MinLeakageDistance;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("m")]
		public System.UInt16 MinLeakageDistance {
			get { return _MinLeakageDistance; }
			set { SetField(ref _MinLeakageDistance, value, () => MinLeakageDistance); }
		}

		private System.UInt16 _MaxLeakageDistance;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("m")]
		public System.UInt16 MaxLeakageDistance {
			get { return _MaxLeakageDistance; }
			set { SetField(ref _MaxLeakageDistance, value, () => MaxLeakageDistance); }
		}

		private ModelLib.Types.TriState _PlayFullRadio;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState PlayFullRadio {
			get { return _PlayFullRadio; }
			set { SetField(ref _PlayFullRadio, value, () => PlayFullRadio); }
		}

		private ModelLib.Types.TriState _MuteForScore;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState MuteForScore {
			get { return _MuteForScore; }
			set { SetField(ref _MuteForScore, value, () => MuteForScore); }
		}

		private ModelLib.ObjectRef _AlarmSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(AlarmSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AlarmSettings {
			get { return _AlarmSettings; }
			set { SetField(ref _AlarmSettings, value, () => AlarmSettings); }
		}

		private ModelLib.Types.TriState _DisabledByScript;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState DisabledByScript {
			get { return _DisabledByScript; }
			set { SetField(ref _DisabledByScript, value, () => DisabledByScript); }
		}

		private ModelLib.ObjectRef _OnBreakOneShot;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef OnBreakOneShot {
			get { return _OnBreakOneShot; }
			set { SetField(ref _OnBreakOneShot, value, () => OnBreakOneShot); }
		}

		private System.Byte _MaxPathDepth;
		[ModelLib.CustomAttributes.Default("3")]
		[ModelLib.CustomAttributes.Max(5)]
		[ModelLib.CustomAttributes.Min(1)]
		public System.Byte MaxPathDepth {
			get { return _MaxPathDepth; }
			set { SetField(ref _MaxPathDepth, value, () => MaxPathDepth); }
		}

		private System.Byte _SmallReverbSend;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.Byte SmallReverbSend {
			get { return _SmallReverbSend; }
			set { SetField(ref _SmallReverbSend, value, () => SmallReverbSend); }
		}

		private System.Byte _MediumReverbSend;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.Byte MediumReverbSend {
			get { return _MediumReverbSend; }
			set { SetField(ref _MediumReverbSend, value, () => MediumReverbSend); }
		}

		private System.Byte _LargeReverbSend;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.Byte LargeReverbSend {
			get { return _LargeReverbSend; }
			set { SetField(ref _LargeReverbSend, value, () => LargeReverbSend); }
		}

		private System.Single _MinTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(24)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("hours")]
		public System.Single MinTime {
			get { return _MinTime; }
			set { SetField(ref _MinTime, value, () => MinTime); }
		}

		private System.Single _MaxTime;
		[ModelLib.CustomAttributes.Default("24")]
		[ModelLib.CustomAttributes.Max(24)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("hours")]
		public System.Single MaxTime {
			get { return _MaxTime; }
			set { SetField(ref _MaxTime, value, () => MaxTime); }
		}

		private System.UInt16 _MinTimeMinutes;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt16 MinTimeMinutes {
			get { return _MinTimeMinutes; }
			set { SetField(ref _MinTimeMinutes, value, () => MinTimeMinutes); }
		}

		private System.UInt16 _MaxTimeMinutes;
		[ModelLib.CustomAttributes.Default("1440")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt16 MaxTimeMinutes {
			get { return _MaxTimeMinutes; }
			set { SetField(ref _MaxTimeMinutes, value, () => MaxTimeMinutes); }
		}

		private ModelLib.Types.TriState _StartStopImmediatelyAtTimeLimits;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("If set to no, the emitter will wait until the player is outside of the max distance before changing state")]
		public ModelLib.Types.TriState StartStopImmediatelyAtTimeLimits {
			get { return _StartStopImmediatelyAtTimeLimits; }
			set { SetField(ref _StartStopImmediatelyAtTimeLimits, value, () => StartStopImmediatelyAtTimeLimits); }
		}

		private System.Single _BrokenHealth;
		[ModelLib.CustomAttributes.Default("0.94")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single BrokenHealth {
			get { return _BrokenHealth; }
			set { SetField(ref _BrokenHealth, value, () => BrokenHealth); }
		}

		private System.Single _UndamagedHealth;
		[ModelLib.CustomAttributes.Default("0.99")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single UndamagedHealth {
			get { return _UndamagedHealth; }
			set { SetField(ref _UndamagedHealth, value, () => UndamagedHealth); }
		}

		private ModelLib.Types.TriState _MuteForFrontendRadio;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState MuteForFrontendRadio {
			get { return _MuteForFrontendRadio; }
			set { SetField(ref _MuteForFrontendRadio, value, () => MuteForFrontendRadio); }
		}

		private ModelLib.Types.TriState _ForceMaxPathDepth;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState ForceMaxPathDepth {
			get { return _ForceMaxPathDepth; }
			set { SetField(ref _ForceMaxPathDepth, value, () => ForceMaxPathDepth); }
		}

		private ModelLib.Types.TriState _IgnoreSniper;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState IgnoreSniper {
			get { return _IgnoreSniper; }
			set { SetField(ref _IgnoreSniper, value, () => IgnoreSniper); }
		}

		public class PositionDefinition: ModelBase {
			private System.Single _x;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single x {
				get { return _x; }
				set { SetField(ref _x, value, () => x); }
			}

			private System.Single _y;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

			private System.Single _z;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single z {
				get { return _z; }
				set { SetField(ref _z, value, () => z); }
			}

		}
		private PositionDefinition _Position;
		public PositionDefinition Position {
			get { return _Position; }
			set { SetField(ref _Position, value, () => Position); }
		}

	}
}


