using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class SpeechParams:  AudBaseObject{

		private ModelLib.Types.TriState _AllowRepeat;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState AllowRepeat {
			get { return _AllowRepeat; }
			set { SetField(ref _AllowRepeat, value, () => AllowRepeat); }
		}

		private ModelLib.Types.TriState _ForcePlay;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState ForcePlay {
			get { return _ForcePlay; }
			set { SetField(ref _ForcePlay, value, () => ForcePlay); }
		}

		private ModelLib.Types.TriState _SayOverPain;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState SayOverPain {
			get { return _SayOverPain; }
			set { SetField(ref _SayOverPain, value, () => SayOverPain); }
		}

		private ModelLib.Types.TriState _PreloadOnly;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState PreloadOnly {
			get { return _PreloadOnly; }
			set { SetField(ref _PreloadOnly, value, () => PreloadOnly); }
		}

		private System.UInt32 _PreloadTimeoutInMs;
		[ModelLib.CustomAttributes.Default("30000")]
		[ModelLib.CustomAttributes.Max(120000)]
		[ModelLib.CustomAttributes.Min(30)]
		public System.UInt32 PreloadTimeoutInMs {
			get { return _PreloadTimeoutInMs; }
			set { SetField(ref _PreloadTimeoutInMs, value, () => PreloadTimeoutInMs); }
		}

		private SpeechRequestedVolumeType _RequestedVolume;
		[ModelLib.CustomAttributes.Default("SPEECH_VOLUME_UNSPECIFIED")]
		public SpeechRequestedVolumeType RequestedVolume {
			get { return _RequestedVolume; }
			set { SetField(ref _RequestedVolume, value, () => RequestedVolume); }
		}

		private SpeechAudibilityType _Audibility;
		[ModelLib.CustomAttributes.Default("CONTEXT_AUDIBILITY_NORMAL")]
		public SpeechAudibilityType Audibility {
			get { return _Audibility; }
			set { SetField(ref _Audibility, value, () => Audibility); }
		}

		private System.Int32 _RepeatTime;
		[ModelLib.CustomAttributes.Default("0")]
		public System.Int32 RepeatTime {
			get { return _RepeatTime; }
			set { SetField(ref _RepeatTime, value, () => RepeatTime); }
		}

		private System.Int32 _RepeatTimeOnSameVoice;
		[ModelLib.CustomAttributes.Default("-1")]
		public System.Int32 RepeatTimeOnSameVoice {
			get { return _RepeatTimeOnSameVoice; }
			set { SetField(ref _RepeatTimeOnSameVoice, value, () => RepeatTimeOnSameVoice); }
		}

		private ModelLib.Types.TriState _IsConversationInterrupt;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState IsConversationInterrupt {
			get { return _IsConversationInterrupt; }
			set { SetField(ref _IsConversationInterrupt, value, () => IsConversationInterrupt); }
		}

		private ModelLib.Types.TriState _AddBlip;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState AddBlip {
			get { return _AddBlip; }
			set { SetField(ref _AddBlip, value, () => AddBlip); }
		}

		private ModelLib.Types.TriState _IsUrgent;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState IsUrgent {
			get { return _IsUrgent; }
			set { SetField(ref _IsUrgent, value, () => IsUrgent); }
		}

		private ModelLib.Types.TriState _InterruptAmbientSpeech;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState InterruptAmbientSpeech {
			get { return _InterruptAmbientSpeech; }
			set { SetField(ref _InterruptAmbientSpeech, value, () => InterruptAmbientSpeech); }
		}

		private ModelLib.Types.TriState _IsMutedDuringSlowMo;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState IsMutedDuringSlowMo {
			get { return _IsMutedDuringSlowMo; }
			set { SetField(ref _IsMutedDuringSlowMo, value, () => IsMutedDuringSlowMo); }
		}

		private ModelLib.Types.TriState _IsPitchedDuringSlowMo;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState IsPitchedDuringSlowMo {
			get { return _IsPitchedDuringSlowMo; }
			set { SetField(ref _IsPitchedDuringSlowMo, value, () => IsPitchedDuringSlowMo); }
		}

		public class OverrideContextSettingsDefinition: ModelBase {
			private ModelLib.Types.Bit _OverrideAllowRepeat;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit OverrideAllowRepeat {
				get { return _OverrideAllowRepeat; }
				set { SetField(ref _OverrideAllowRepeat, value, () => OverrideAllowRepeat); }
			}

			private ModelLib.Types.Bit _OverrideForcePlay;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit OverrideForcePlay {
				get { return _OverrideForcePlay; }
				set { SetField(ref _OverrideForcePlay, value, () => OverrideForcePlay); }
			}

			private ModelLib.Types.Bit _OverrideRepeatTime;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit OverrideRepeatTime {
				get { return _OverrideRepeatTime; }
				set { SetField(ref _OverrideRepeatTime, value, () => OverrideRepeatTime); }
			}

			private ModelLib.Types.Bit _OverrideRepeatTimeOnSameVoice;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit OverrideRepeatTimeOnSameVoice {
				get { return _OverrideRepeatTimeOnSameVoice; }
				set { SetField(ref _OverrideRepeatTimeOnSameVoice, value, () => OverrideRepeatTimeOnSameVoice); }
			}

		}
		private OverrideContextSettingsDefinition _OverrideContextSettings;
		public OverrideContextSettingsDefinition OverrideContextSettings {
			get { return _OverrideContextSettings; }
			set { SetField(ref _OverrideContextSettings, value, () => OverrideContextSettings); }
		}

	}
}


