using ModelLib.CustomAttributes;

namespace model {

	public enum DoorTypes {
		[ModelLib.CustomAttributes.Display("Sliding horizontal")]
		AUD_DOOR_TYPE_SLIDING_HORIZONTAL,
		[ModelLib.CustomAttributes.Display("Sliding vertical")]
		AUD_DOOR_TYPE_SLIDING_VERTICAL,
		[ModelLib.CustomAttributes.Display("Garage")]
		AUD_DOOR_TYPE_GARAGE,
		[ModelLib.CustomAttributes.Display("Barrier arm")]
		AUD_DOOR_TYPE_BARRIER_ARM,
		[ModelLib.CustomAttributes.Display("Standard swing door")]
		AUD_DOOR_TYPE_STD
	}

}