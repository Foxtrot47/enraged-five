using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class OldShoeSettings:  ModelBase {

		private ModelLib.ObjectRef _ScuffHard;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScuffHard {
			get { return _ScuffHard; }
			set { SetField(ref _ScuffHard, value, () => ScuffHard); }
		}

		private ModelLib.ObjectRef _Scuff;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Scuff {
			get { return _Scuff; }
			set { SetField(ref _Scuff, value, () => Scuff); }
		}

		private ModelLib.ObjectRef _Ladder;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Ladder {
			get { return _Ladder; }
			set { SetField(ref _Ladder, value, () => Ladder); }
		}

		private ModelLib.ObjectRef _Jump;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Jump {
			get { return _Jump; }
			set { SetField(ref _Jump, value, () => Jump); }
		}

		private ModelLib.ObjectRef _Run;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Run {
			get { return _Run; }
			set { SetField(ref _Run, value, () => Run); }
		}

		private ModelLib.ObjectRef _Walk;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Walk {
			get { return _Walk; }
			set { SetField(ref _Walk, value, () => Walk); }
		}

		private ModelLib.ObjectRef _Sprint;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Sprint {
			get { return _Sprint; }
			set { SetField(ref _Sprint, value, () => Sprint); }
		}

	}
}


