using ModelLib.CustomAttributes;

namespace model {

	public enum TrackCats {
		[ModelLib.CustomAttributes.Display("Adverts")]
		RADIO_TRACK_CAT_ADVERTS,
		[ModelLib.CustomAttributes.Display("Idents")]
		RADIO_TRACK_CAT_IDENTS,
		[ModelLib.CustomAttributes.Display("Music")]
		RADIO_TRACK_CAT_MUSIC,
		[ModelLib.CustomAttributes.Display("News")]
		RADIO_TRACK_CAT_NEWS,
		[ModelLib.CustomAttributes.Display("Weather")]
		RADIO_TRACK_CAT_WEATHER,
		[ModelLib.CustomAttributes.Display("DjSolo")]
		RADIO_TRACK_CAT_DJSOLO,
		[ModelLib.CustomAttributes.Display("Null")]
		NUM_RADIO_TRACK_CATS
	}

}