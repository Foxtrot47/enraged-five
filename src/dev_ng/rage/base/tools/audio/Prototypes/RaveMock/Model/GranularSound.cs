using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class GranularSound:  Sound{

		private System.UInt32 _WaveSlotPtr;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 WaveSlotPtr {
			get { return _WaveSlotPtr; }
			set { SetField(ref _WaveSlotPtr, value, () => WaveSlotPtr); }
		}

		private ModelLib.Types.TriState _LoopRandomisationEnabled;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ModelLib.Types.TriState LoopRandomisationEnabled {
			get { return _LoopRandomisationEnabled; }
			set { SetField(ref _LoopRandomisationEnabled, value, () => LoopRandomisationEnabled); }
		}

		private System.Single _LoopRandomisationChangeRate;
		[ModelLib.CustomAttributes.Default("0.01")]
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single LoopRandomisationChangeRate {
			get { return _LoopRandomisationChangeRate; }
			set { SetField(ref _LoopRandomisationChangeRate, value, () => LoopRandomisationChangeRate); }
		}

		private System.Single _LoopRandomisationPitchFraction;
		[ModelLib.CustomAttributes.Default("0.05")]
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single LoopRandomisationPitchFraction {
			get { return _LoopRandomisationPitchFraction; }
			set { SetField(ref _LoopRandomisationPitchFraction, value, () => LoopRandomisationPitchFraction); }
		}

		private System.Int16 _Channel0_Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Volumes")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Channel0_Volume {
			get { return _Channel0_Volume; }
			set { SetField(ref _Channel0_Volume, value, () => Channel0_Volume); }
		}

		private System.Int16 _Channel1_Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Volumes")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Channel1_Volume {
			get { return _Channel1_Volume; }
			set { SetField(ref _Channel1_Volume, value, () => Channel1_Volume); }
		}

		private System.Int16 _Channel2_Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Volumes")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Channel2_Volume {
			get { return _Channel2_Volume; }
			set { SetField(ref _Channel2_Volume, value, () => Channel2_Volume); }
		}

		private System.Int16 _Channel3_Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Volumes")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Channel3_Volume {
			get { return _Channel3_Volume; }
			set { SetField(ref _Channel3_Volume, value, () => Channel3_Volume); }
		}

		private System.Int16 _Channel4_Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Volumes")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Channel4_Volume {
			get { return _Channel4_Volume; }
			set { SetField(ref _Channel4_Volume, value, () => Channel4_Volume); }
		}

		private System.Int16 _Channel5_Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Volumes")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Channel5_Volume {
			get { return _Channel5_Volume; }
			set { SetField(ref _Channel5_Volume, value, () => Channel5_Volume); }
		}

		private ModelLib.ObjectRef _ParentSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef ParentSound {
			get { return _ParentSound; }
			set { SetField(ref _ParentSound, value, () => ParentSound); }
		}

		private ModelLib.Types.TriState _AllowLoopGrainCrossoverChannel0;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ModelLib.Types.TriState AllowLoopGrainCrossoverChannel0 {
			get { return _AllowLoopGrainCrossoverChannel0; }
			set { SetField(ref _AllowLoopGrainCrossoverChannel0, value, () => AllowLoopGrainCrossoverChannel0); }
		}

		private ModelLib.Types.TriState _AllowLoopGrainCrossoverChannel1;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ModelLib.Types.TriState AllowLoopGrainCrossoverChannel1 {
			get { return _AllowLoopGrainCrossoverChannel1; }
			set { SetField(ref _AllowLoopGrainCrossoverChannel1, value, () => AllowLoopGrainCrossoverChannel1); }
		}

		private ModelLib.Types.TriState _AllowLoopGrainCrossoverChannel2;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ModelLib.Types.TriState AllowLoopGrainCrossoverChannel2 {
			get { return _AllowLoopGrainCrossoverChannel2; }
			set { SetField(ref _AllowLoopGrainCrossoverChannel2, value, () => AllowLoopGrainCrossoverChannel2); }
		}

		private ModelLib.Types.TriState _AllowLoopGrainCrossoverChannel3;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ModelLib.Types.TriState AllowLoopGrainCrossoverChannel3 {
			get { return _AllowLoopGrainCrossoverChannel3; }
			set { SetField(ref _AllowLoopGrainCrossoverChannel3, value, () => AllowLoopGrainCrossoverChannel3); }
		}

		private ModelLib.Types.TriState _AllowLoopGrainCrossoverChannel4;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ModelLib.Types.TriState AllowLoopGrainCrossoverChannel4 {
			get { return _AllowLoopGrainCrossoverChannel4; }
			set { SetField(ref _AllowLoopGrainCrossoverChannel4, value, () => AllowLoopGrainCrossoverChannel4); }
		}

		private ModelLib.Types.TriState _AllowLoopGrainCrossoverChannel5;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ModelLib.Types.TriState AllowLoopGrainCrossoverChannel5 {
			get { return _AllowLoopGrainCrossoverChannel5; }
			set { SetField(ref _AllowLoopGrainCrossoverChannel5, value, () => AllowLoopGrainCrossoverChannel5); }
		}

		public class Channel0Definition: ModelBase {
			private System.String _BankName;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			[ModelLib.CustomAttributes.Unit("stringtableindex")]
			public System.String BankName {
				get { return _BankName; }
				set { SetField(ref _BankName, value, () => BankName); }
			}

			private System.String _WaveName;
			public System.String WaveName {
				get { return _WaveName; }
				set { SetField(ref _WaveName, value, () => WaveName); }
			}

		}
		private Channel0Definition _Channel0;
		[ModelLib.CustomAttributes.Unit("WaveRef")]
		public Channel0Definition Channel0 {
			get { return _Channel0; }
			set { SetField(ref _Channel0, value, () => Channel0); }
		}

		public class Channel1Definition: ModelBase {
			private System.String _BankName;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			[ModelLib.CustomAttributes.Unit("stringtableindex")]
			public System.String BankName {
				get { return _BankName; }
				set { SetField(ref _BankName, value, () => BankName); }
			}

			private System.String _WaveName;
			public System.String WaveName {
				get { return _WaveName; }
				set { SetField(ref _WaveName, value, () => WaveName); }
			}

		}
		private Channel1Definition _Channel1;
		[ModelLib.CustomAttributes.Unit("WaveRef")]
		public Channel1Definition Channel1 {
			get { return _Channel1; }
			set { SetField(ref _Channel1, value, () => Channel1); }
		}

		public class Channel2Definition: ModelBase {
			private System.String _BankName;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			[ModelLib.CustomAttributes.Unit("stringtableindex")]
			public System.String BankName {
				get { return _BankName; }
				set { SetField(ref _BankName, value, () => BankName); }
			}

			private System.String _WaveName;
			public System.String WaveName {
				get { return _WaveName; }
				set { SetField(ref _WaveName, value, () => WaveName); }
			}

		}
		private Channel2Definition _Channel2;
		[ModelLib.CustomAttributes.Unit("WaveRef")]
		public Channel2Definition Channel2 {
			get { return _Channel2; }
			set { SetField(ref _Channel2, value, () => Channel2); }
		}

		public class Channel3Definition: ModelBase {
			private System.String _BankName;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			[ModelLib.CustomAttributes.Unit("stringtableindex")]
			public System.String BankName {
				get { return _BankName; }
				set { SetField(ref _BankName, value, () => BankName); }
			}

			private System.String _WaveName;
			public System.String WaveName {
				get { return _WaveName; }
				set { SetField(ref _WaveName, value, () => WaveName); }
			}

		}
		private Channel3Definition _Channel3;
		[ModelLib.CustomAttributes.Unit("WaveRef")]
		public Channel3Definition Channel3 {
			get { return _Channel3; }
			set { SetField(ref _Channel3, value, () => Channel3); }
		}

		public class Channel4Definition: ModelBase {
			private System.String _BankName;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			[ModelLib.CustomAttributes.Unit("stringtableindex")]
			public System.String BankName {
				get { return _BankName; }
				set { SetField(ref _BankName, value, () => BankName); }
			}

			private System.String _WaveName;
			public System.String WaveName {
				get { return _WaveName; }
				set { SetField(ref _WaveName, value, () => WaveName); }
			}

		}
		private Channel4Definition _Channel4;
		[ModelLib.CustomAttributes.Unit("WaveRef")]
		public Channel4Definition Channel4 {
			get { return _Channel4; }
			set { SetField(ref _Channel4, value, () => Channel4); }
		}

		public class Channel5Definition: ModelBase {
			private System.String _BankName;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			[ModelLib.CustomAttributes.Unit("stringtableindex")]
			public System.String BankName {
				get { return _BankName; }
				set { SetField(ref _BankName, value, () => BankName); }
			}

			private System.String _WaveName;
			public System.String WaveName {
				get { return _WaveName; }
				set { SetField(ref _WaveName, value, () => WaveName); }
			}

		}
		private Channel5Definition _Channel5;
		[ModelLib.CustomAttributes.Unit("WaveRef")]
		public Channel5Definition Channel5 {
			get { return _Channel5; }
			set { SetField(ref _Channel5, value, () => Channel5); }
		}

		public class ChannelSettings0Definition: ModelBase {
			private System.Byte _OutputBuffer;
			[ModelLib.CustomAttributes.Default("0")]
			public System.Byte OutputBuffer {
				get { return _OutputBuffer; }
				set { SetField(ref _OutputBuffer, value, () => OutputBuffer); }
			}

			private System.Byte _GranularClockIndex;
			public System.Byte GranularClockIndex {
				get { return _GranularClockIndex; }
				set { SetField(ref _GranularClockIndex, value, () => GranularClockIndex); }
			}

			private System.Byte _StretchToMinPitch0;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMinPitch0 {
				get { return _StretchToMinPitch0; }
				set { SetField(ref _StretchToMinPitch0, value, () => StretchToMinPitch0); }
			}

			private System.Byte _StretchToMaxPitch0;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMaxPitch0 {
				get { return _StretchToMaxPitch0; }
				set { SetField(ref _StretchToMaxPitch0, value, () => StretchToMaxPitch0); }
			}

			private System.Single _MaxLoopProportion;
			[ModelLib.CustomAttributes.Default("0.5")]
			public System.Single MaxLoopProportion {
				get { return _MaxLoopProportion; }
				set { SetField(ref _MaxLoopProportion, value, () => MaxLoopProportion); }
			}

		}
		private ChannelSettings0Definition _ChannelSettings0;
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ChannelSettings0Definition ChannelSettings0 {
			get { return _ChannelSettings0; }
			set { SetField(ref _ChannelSettings0, value, () => ChannelSettings0); }
		}

		public class ChannelSettings1Definition: ModelBase {
			private System.Byte _OutputBuffer;
			[ModelLib.CustomAttributes.Default("1")]
			public System.Byte OutputBuffer {
				get { return _OutputBuffer; }
				set { SetField(ref _OutputBuffer, value, () => OutputBuffer); }
			}

			private System.Byte _GranularClockIndex;
			public System.Byte GranularClockIndex {
				get { return _GranularClockIndex; }
				set { SetField(ref _GranularClockIndex, value, () => GranularClockIndex); }
			}

			private System.Byte _StretchToMinPitch1;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMinPitch1 {
				get { return _StretchToMinPitch1; }
				set { SetField(ref _StretchToMinPitch1, value, () => StretchToMinPitch1); }
			}

			private System.Byte _StretchToMaxPitch1;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMaxPitch1 {
				get { return _StretchToMaxPitch1; }
				set { SetField(ref _StretchToMaxPitch1, value, () => StretchToMaxPitch1); }
			}

			private System.Single _MaxLoopProportion;
			[ModelLib.CustomAttributes.Default("0.5")]
			public System.Single MaxLoopProportion {
				get { return _MaxLoopProportion; }
				set { SetField(ref _MaxLoopProportion, value, () => MaxLoopProportion); }
			}

		}
		private ChannelSettings1Definition _ChannelSettings1;
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ChannelSettings1Definition ChannelSettings1 {
			get { return _ChannelSettings1; }
			set { SetField(ref _ChannelSettings1, value, () => ChannelSettings1); }
		}

		public class ChannelSettings2Definition: ModelBase {
			private System.Byte _OutputBuffer;
			[ModelLib.CustomAttributes.Default("0")]
			public System.Byte OutputBuffer {
				get { return _OutputBuffer; }
				set { SetField(ref _OutputBuffer, value, () => OutputBuffer); }
			}

			private System.Byte _GranularClockIndex;
			public System.Byte GranularClockIndex {
				get { return _GranularClockIndex; }
				set { SetField(ref _GranularClockIndex, value, () => GranularClockIndex); }
			}

			private System.Byte _StretchToMinPitch2;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMinPitch2 {
				get { return _StretchToMinPitch2; }
				set { SetField(ref _StretchToMinPitch2, value, () => StretchToMinPitch2); }
			}

			private System.Byte _StretchToMaxPitch2;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMaxPitch2 {
				get { return _StretchToMaxPitch2; }
				set { SetField(ref _StretchToMaxPitch2, value, () => StretchToMaxPitch2); }
			}

			private System.Single _MaxLoopProportion;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single MaxLoopProportion {
				get { return _MaxLoopProportion; }
				set { SetField(ref _MaxLoopProportion, value, () => MaxLoopProportion); }
			}

		}
		private ChannelSettings2Definition _ChannelSettings2;
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ChannelSettings2Definition ChannelSettings2 {
			get { return _ChannelSettings2; }
			set { SetField(ref _ChannelSettings2, value, () => ChannelSettings2); }
		}

		public class ChannelSettings3Definition: ModelBase {
			private System.Byte _OutputBuffer;
			[ModelLib.CustomAttributes.Default("1")]
			public System.Byte OutputBuffer {
				get { return _OutputBuffer; }
				set { SetField(ref _OutputBuffer, value, () => OutputBuffer); }
			}

			private System.Byte _GranularClockIndex;
			public System.Byte GranularClockIndex {
				get { return _GranularClockIndex; }
				set { SetField(ref _GranularClockIndex, value, () => GranularClockIndex); }
			}

			private System.Byte _StretchToMinPitch3;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMinPitch3 {
				get { return _StretchToMinPitch3; }
				set { SetField(ref _StretchToMinPitch3, value, () => StretchToMinPitch3); }
			}

			private System.Byte _StretchToMaxPitch3;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMaxPitch3 {
				get { return _StretchToMaxPitch3; }
				set { SetField(ref _StretchToMaxPitch3, value, () => StretchToMaxPitch3); }
			}

			private System.Single _MaxLoopProportion;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single MaxLoopProportion {
				get { return _MaxLoopProportion; }
				set { SetField(ref _MaxLoopProportion, value, () => MaxLoopProportion); }
			}

		}
		private ChannelSettings3Definition _ChannelSettings3;
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ChannelSettings3Definition ChannelSettings3 {
			get { return _ChannelSettings3; }
			set { SetField(ref _ChannelSettings3, value, () => ChannelSettings3); }
		}

		public class ChannelSettings4Definition: ModelBase {
			private System.Byte _OutputBuffer;
			[ModelLib.CustomAttributes.Default("0")]
			public System.Byte OutputBuffer {
				get { return _OutputBuffer; }
				set { SetField(ref _OutputBuffer, value, () => OutputBuffer); }
			}

			private System.Byte _GranularClockIndex;
			public System.Byte GranularClockIndex {
				get { return _GranularClockIndex; }
				set { SetField(ref _GranularClockIndex, value, () => GranularClockIndex); }
			}

			private System.Byte _StretchToMinPitch4;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMinPitch4 {
				get { return _StretchToMinPitch4; }
				set { SetField(ref _StretchToMinPitch4, value, () => StretchToMinPitch4); }
			}

			private System.Byte _StretchToMaxPitch4;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMaxPitch4 {
				get { return _StretchToMaxPitch4; }
				set { SetField(ref _StretchToMaxPitch4, value, () => StretchToMaxPitch4); }
			}

			private System.Single _MaxLoopProportion;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single MaxLoopProportion {
				get { return _MaxLoopProportion; }
				set { SetField(ref _MaxLoopProportion, value, () => MaxLoopProportion); }
			}

		}
		private ChannelSettings4Definition _ChannelSettings4;
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ChannelSettings4Definition ChannelSettings4 {
			get { return _ChannelSettings4; }
			set { SetField(ref _ChannelSettings4, value, () => ChannelSettings4); }
		}

		public class ChannelSettings5Definition: ModelBase {
			private System.Byte _OutputBuffer;
			[ModelLib.CustomAttributes.Default("1")]
			public System.Byte OutputBuffer {
				get { return _OutputBuffer; }
				set { SetField(ref _OutputBuffer, value, () => OutputBuffer); }
			}

			private System.Byte _GranularClockIndex;
			public System.Byte GranularClockIndex {
				get { return _GranularClockIndex; }
				set { SetField(ref _GranularClockIndex, value, () => GranularClockIndex); }
			}

			private System.Byte _StretchToMinPitch5;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMinPitch5 {
				get { return _StretchToMinPitch5; }
				set { SetField(ref _StretchToMinPitch5, value, () => StretchToMinPitch5); }
			}

			private System.Byte _StretchToMaxPitch5;
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Byte StretchToMaxPitch5 {
				get { return _StretchToMaxPitch5; }
				set { SetField(ref _StretchToMaxPitch5, value, () => StretchToMaxPitch5); }
			}

			private System.Single _MaxLoopProportion;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single MaxLoopProportion {
				get { return _MaxLoopProportion; }
				set { SetField(ref _MaxLoopProportion, value, () => MaxLoopProportion); }
			}

		}
		private ChannelSettings5Definition _ChannelSettings5;
		[ModelLib.CustomAttributes.DisplayGroup("Advanced")]
		public ChannelSettings5Definition ChannelSettings5 {
			get { return _ChannelSettings5; }
			set { SetField(ref _ChannelSettings5, value, () => ChannelSettings5); }
		}

		public class GranularClockDefinition: ModelBase {
			private System.Single _MinPitch;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(200.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single MinPitch {
				get { return _MinPitch; }
				set { SetField(ref _MinPitch, value, () => MinPitch); }
			}

			private System.Single _MaxPitch;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(200.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single MaxPitch {
				get { return _MaxPitch; }
				set { SetField(ref _MaxPitch, value, () => MaxPitch); }
			}

		}
		private ItemsObservableCollection<GranularClockDefinition> _GranularClock = new ItemsObservableCollection<GranularClockDefinition>();
		[ModelLib.CustomAttributes.MaxSize(2)]
		[XmlElement("GranularClock")]
		public ItemsObservableCollection<GranularClockDefinition> GranularClock {
			get { return _GranularClock; }
			set { SetField(ref _GranularClock, value, () => GranularClock); }
		}

	}
}


