using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Scanner")]
	public class ScannerVehicleParams:  ModelBase {

		private ModelLib.Types.TriState _BlockColorReporting;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState BlockColorReporting {
			get { return _BlockColorReporting; }
			set { SetField(ref _BlockColorReporting, value, () => BlockColorReporting); }
		}

		public class VoiceDefinition: ModelBase {
			private ModelLib.ObjectRef _ManufacturerSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef ManufacturerSound {
				get { return _ManufacturerSound; }
				set { SetField(ref _ManufacturerSound, value, () => ManufacturerSound); }
			}

			private ModelLib.ObjectRef _ModelSound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef ModelSound {
				get { return _ModelSound; }
				set { SetField(ref _ModelSound, value, () => ModelSound); }
			}

			private ModelLib.ObjectRef _CategorySound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef CategorySound {
				get { return _CategorySound; }
				set { SetField(ref _CategorySound, value, () => CategorySound); }
			}

			private ModelLib.ObjectRef _ScannerColorOverride;
			[ModelLib.CustomAttributes.Default("NULL_SOUND")]
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.DisplayGroup("Extras")]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef ScannerColorOverride {
				get { return _ScannerColorOverride; }
				set { SetField(ref _ScannerColorOverride, value, () => ScannerColorOverride); }
			}

		}
		private ItemsObservableCollection<VoiceDefinition> _Voice = new ItemsObservableCollection<VoiceDefinition>();
		[ModelLib.CustomAttributes.MaxSize(8)]
		[XmlElement("Voice")]
		public ItemsObservableCollection<VoiceDefinition> Voice {
			get { return _Voice; }
			set { SetField(ref _Voice, value, () => Voice); }
		}

	}
}


