using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	public class VehicleCollisionModuleSettings:  ModelBase {

		private MixModuleInput _Input;
		[ModelLib.CustomAttributes.Default("INPUT_NONE")]
		public MixModuleInput Input {
			get { return _Input; }
			set { SetField(ref _Input, value, () => Input); }
		}

		private System.String _Transition;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		public System.String Transition {
			get { return _Transition; }
			set { SetField(ref _Transition, value, () => Transition); }
		}

	}
}


