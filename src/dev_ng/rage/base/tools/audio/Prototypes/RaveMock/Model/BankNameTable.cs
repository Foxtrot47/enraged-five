using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class BankNameTable:  ModelBase {

		private System.String _bankName;
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		[ModelLib.CustomAttributes.Unit("stringtableindex")]
		public System.String bankName {
			get { return _bankName; }
			set { SetField(ref _bankName, value, () => bankName); }
		}

	}
}


