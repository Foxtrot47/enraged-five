using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Physics")]
	public class DoorAudioSettingsLink:  ModelBase {

		private ModelLib.ObjectRef _DoorAudioSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(DoorAudioSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef DoorAudioSettings {
			get { return _DoorAudioSettings; }
			set { SetField(ref _DoorAudioSettings, value, () => DoorAudioSettings); }
		}

	}
}


