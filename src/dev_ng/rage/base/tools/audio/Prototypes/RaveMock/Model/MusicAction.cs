using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public abstract class MusicAction:  ModelBase {

		private MusicConstraintTypes _Constrain;
		[ModelLib.CustomAttributes.Default("kConstrainStart")]
		public MusicConstraintTypes Constrain {
			get { return _Constrain; }
			set { SetField(ref _Constrain, value, () => Constrain); }
		}

		private System.Single _DelayTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Unit("seconds")]
		public System.Single DelayTime {
			get { return _DelayTime; }
			set { SetField(ref _DelayTime, value, () => DelayTime); }
		}

		public class TimingConstraintsDefinition: ModelBase {
			private ModelLib.ObjectRef _Constraint;
			[ModelLib.CustomAttributes.AllowedType(typeof(MusicTimingConstraint))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef Constraint {
				get { return _Constraint; }
				set { SetField(ref _Constraint, value, () => Constraint); }
			}

		}
		private ItemsObservableCollection<TimingConstraintsDefinition> _TimingConstraints = new ItemsObservableCollection<TimingConstraintsDefinition>();
		[ModelLib.CustomAttributes.FixedSize]
		[ModelLib.CustomAttributes.MaxSize(2, typeof(System.UInt32))]
		[XmlElement("TimingConstraints")]
		public ItemsObservableCollection<TimingConstraintsDefinition> TimingConstraints {
			get { return _TimingConstraints; }
			set { SetField(ref _TimingConstraints, value, () => TimingConstraints); }
		}

	}
}


