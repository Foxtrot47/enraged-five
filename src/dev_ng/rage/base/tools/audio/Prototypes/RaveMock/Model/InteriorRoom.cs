using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class InteriorRoom:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private System.String _RoomName;
		public System.String RoomName {
			get { return _RoomName; }
			set { SetField(ref _RoomName, value, () => RoomName); }
		}

		private ModelLib.ObjectRef _AmbientZone;
		[ModelLib.CustomAttributes.AllowedType(typeof(AmbientZone))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef AmbientZone {
			get { return _AmbientZone; }
			set { SetField(ref _AmbientZone, value, () => AmbientZone); }
		}

		private InteriorType _InteriorType;
		[ModelLib.CustomAttributes.Default("INTERIOR_TYPE_NONE")]
		public InteriorType InteriorType {
			get { return _InteriorType; }
			set { SetField(ref _InteriorType, value, () => InteriorType); }
		}

		private System.Single _ReverbSmall;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single ReverbSmall {
			get { return _ReverbSmall; }
			set { SetField(ref _ReverbSmall, value, () => ReverbSmall); }
		}

		private System.Single _ReverbMedium;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single ReverbMedium {
			get { return _ReverbMedium; }
			set { SetField(ref _ReverbMedium, value, () => ReverbMedium); }
		}

		private System.Single _ReverbLarge;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single ReverbLarge {
			get { return _ReverbLarge; }
			set { SetField(ref _ReverbLarge, value, () => ReverbLarge); }
		}

		private ModelLib.ObjectRef _RoomToneSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RoomToneSound {
			get { return _RoomToneSound; }
			set { SetField(ref _RoomToneSound, value, () => RoomToneSound); }
		}

		private InteriorRainType _RainType;
		[ModelLib.CustomAttributes.Default("RAIN_TYPE_NONE")]
		public InteriorRainType RainType {
			get { return _RainType; }
			set { SetField(ref _RainType, value, () => RainType); }
		}

		private System.Single _ExteriorAudibility;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single ExteriorAudibility {
			get { return _ExteriorAudibility; }
			set { SetField(ref _ExteriorAudibility, value, () => ExteriorAudibility); }
		}

		private System.Single _RoomOcclusionDamping;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single RoomOcclusionDamping {
			get { return _RoomOcclusionDamping; }
			set { SetField(ref _RoomOcclusionDamping, value, () => RoomOcclusionDamping); }
		}

		private System.Single _NonMarkedPortalOcclusion;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single NonMarkedPortalOcclusion {
			get { return _NonMarkedPortalOcclusion; }
			set { SetField(ref _NonMarkedPortalOcclusion, value, () => NonMarkedPortalOcclusion); }
		}

		private System.Single _DistanceFromPortalForOcclusion;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single DistanceFromPortalForOcclusion {
			get { return _DistanceFromPortalForOcclusion; }
			set { SetField(ref _DistanceFromPortalForOcclusion, value, () => DistanceFromPortalForOcclusion); }
		}

		private System.Single _DistanceFromPortalFadeDistance;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single DistanceFromPortalFadeDistance {
			get { return _DistanceFromPortalFadeDistance; }
			set { SetField(ref _DistanceFromPortalFadeDistance, value, () => DistanceFromPortalFadeDistance); }
		}

		private ModelLib.ObjectRef _WeaponMetrics;
		[ModelLib.CustomAttributes.AllowedType(typeof(InteriorWeaponMetrics))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WeaponMetrics {
			get { return _WeaponMetrics; }
			set { SetField(ref _WeaponMetrics, value, () => WeaponMetrics); }
		}

		private ModelLib.Types.TriState _HasInteriorWalla;
		public ModelLib.Types.TriState HasInteriorWalla {
			get { return _HasInteriorWalla; }
			set { SetField(ref _HasInteriorWalla, value, () => HasInteriorWalla); }
		}

		private ModelLib.ObjectRef _InteriorWallaSoundSet;
		[ModelLib.CustomAttributes.Default("INTERIOR_WALLA_STREAMED_DEFAULT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef InteriorWallaSoundSet {
			get { return _InteriorWallaSoundSet; }
			set { SetField(ref _InteriorWallaSoundSet, value, () => InteriorWallaSoundSet); }
		}

	}
}


