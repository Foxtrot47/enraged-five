using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class EqualPower:  BaseCurve{

		public class FlipDefinition: ModelBase {
			private ModelLib.Types.Bit _x;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit x {
				get { return _x; }
				set { SetField(ref _x, value, () => x); }
			}

			private ModelLib.Types.Bit _y;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

		}
		private FlipDefinition _Flip;
		public FlipDefinition Flip {
			get { return _Flip; }
			set { SetField(ref _Flip, value, () => Flip); }
		}

	}
}


