using ModelLib.CustomAttributes;

namespace model {

	public enum CrossfadeMode {
		[ModelLib.CustomAttributes.Display("Fixed")]
		CROSSFADE_MODE_FIXED,
		[ModelLib.CustomAttributes.Display("Both")]
		CROSSFADE_MODE_BOTH,
		[ModelLib.CustomAttributes.Display("Retrigger")]
		CROSSFADE_MODE_RETRIGGER,
		[ModelLib.CustomAttributes.Display("PositionRelativePan")]
		CROSSFADE_MODE_POSITION_RELATIVE_PAN
	}

}