using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class InteriorWeaponMetrics:  ModelBase {

		private System.Single _Wetness;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single Wetness {
			get { return _Wetness; }
			set { SetField(ref _Wetness, value, () => Wetness); }
		}

		private System.Single _Visability;
		[ModelLib.CustomAttributes.Default("0.2")]
		public System.Single Visability {
			get { return _Visability; }
			set { SetField(ref _Visability, value, () => Visability); }
		}

		private System.UInt32 _LPF;
		[ModelLib.CustomAttributes.Default("24000")]
		public System.UInt32 LPF {
			get { return _LPF; }
			set { SetField(ref _LPF, value, () => LPF); }
		}

		private System.UInt32 _Predelay;
		[ModelLib.CustomAttributes.Default("0")]
		public System.UInt32 Predelay {
			get { return _Predelay; }
			set { SetField(ref _Predelay, value, () => Predelay); }
		}

		private System.Single _Hold;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single Hold {
			get { return _Hold; }
			set { SetField(ref _Hold, value, () => Hold); }
		}

	}
}


