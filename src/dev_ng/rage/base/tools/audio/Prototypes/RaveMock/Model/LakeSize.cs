using ModelLib.CustomAttributes;

namespace model {

	public enum LakeSize {
		[ModelLib.CustomAttributes.Display("Small")]
		AUD_LAKE_SMALL,
		[ModelLib.CustomAttributes.Display("Medium")]
		AUD_LAKE_MEDIUM,
		[ModelLib.CustomAttributes.Display("Large")]
		AUD_LAKE_LARGE
	}

}