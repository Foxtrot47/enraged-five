using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class PiecewiseLinear:  BaseCurve{

		public class PointDefinition: ModelBase {
			private System.Single _x;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Display("x")]
			public System.Single x {
				get { return _x; }
				set { SetField(ref _x, value, () => x); }
			}

			private System.Single _y;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Display("y")]
			public System.Single y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

		}
		private ItemsObservableCollection<PointDefinition> _Point = new ItemsObservableCollection<PointDefinition>();
		[ModelLib.CustomAttributes.MaxSize(65537)]
		[XmlElement("Point")]
		public ItemsObservableCollection<PointDefinition> Point {
			get { return _Point; }
			set { SetField(ref _Point, value, () => Point); }
		}

	}
}


