using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class PedScenarioAudioSettings:  ModelBase {

		private System.UInt32 _MaxInstances;
		[ModelLib.CustomAttributes.Default("1")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 MaxInstances {
			get { return _MaxInstances; }
			set { SetField(ref _MaxInstances, value, () => MaxInstances); }
		}

		private ModelLib.ObjectRef _Sound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Sound {
			get { return _Sound; }
			set { SetField(ref _Sound, value, () => Sound); }
		}

		private ModelLib.Types.TriState _IsStreaming;
		public ModelLib.Types.TriState IsStreaming {
			get { return _IsStreaming; }
			set { SetField(ref _IsStreaming, value, () => IsStreaming); }
		}

		private ModelLib.Types.TriState _AllowSharedOwnership;
		public ModelLib.Types.TriState AllowSharedOwnership {
			get { return _AllowSharedOwnership; }
			set { SetField(ref _AllowSharedOwnership, value, () => AllowSharedOwnership); }
		}

		private System.Single _SharedOwnershipRadius;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.Max(2000.0)]
		[ModelLib.CustomAttributes.Min(1.0)]
		public System.Single SharedOwnershipRadius {
			get { return _SharedOwnershipRadius; }
			set { SetField(ref _SharedOwnershipRadius, value, () => SharedOwnershipRadius); }
		}

		public class PropOverrideDefinition: ModelBase {
			private System.String _PropName;
			public System.String PropName {
				get { return _PropName; }
				set { SetField(ref _PropName, value, () => PropName); }
			}

			private ModelLib.ObjectRef _Sound;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Sound {
				get { return _Sound; }
				set { SetField(ref _Sound, value, () => Sound); }
			}

		}
		private ItemsObservableCollection<PropOverrideDefinition> _PropOverride = new ItemsObservableCollection<PropOverrideDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16)]
		[XmlElement("PropOverride")]
		public ItemsObservableCollection<PropOverrideDefinition> PropOverride {
			get { return _PropOverride; }
			set { SetField(ref _PropOverride, value, () => PropOverride); }
		}

	}
}


