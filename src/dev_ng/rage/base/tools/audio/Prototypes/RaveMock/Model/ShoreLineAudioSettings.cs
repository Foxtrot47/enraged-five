using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public abstract class ShoreLineAudioSettings:  ModelBase {

		private ModelLib.Types.TriState _IsInInterior;
		public ModelLib.Types.TriState IsInInterior {
			get { return _IsInInterior; }
			set { SetField(ref _IsInInterior, value, () => IsInInterior); }
		}

		public class ActivationBoxDefinition: ModelBase {
			private System.Single _RotationAngle;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single RotationAngle {
				get { return _RotationAngle; }
				set { SetField(ref _RotationAngle, value, () => RotationAngle); }
			}

			public class CenterDefinition: ModelBase {
				private System.Single _X;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single X {
					get { return _X; }
					set { SetField(ref _X, value, () => X); }
				}

				private System.Single _Y;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single Y {
					get { return _Y; }
					set { SetField(ref _Y, value, () => Y); }
				}

			}
			private CenterDefinition _Center;
			[ModelLib.CustomAttributes.DisplayGroup("Definition")]
			public CenterDefinition Center {
				get { return _Center; }
				set { SetField(ref _Center, value, () => Center); }
			}

			public class SizeDefinition: ModelBase {
				private System.Single _Width;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single Width {
					get { return _Width; }
					set { SetField(ref _Width, value, () => Width); }
				}

				private System.Single _Height;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single Height {
					get { return _Height; }
					set { SetField(ref _Height, value, () => Height); }
				}

			}
			private SizeDefinition _Size;
			[ModelLib.CustomAttributes.DisplayGroup("Definition")]
			public SizeDefinition Size {
				get { return _Size; }
				set { SetField(ref _Size, value, () => Size); }
			}

		}
		private ActivationBoxDefinition _ActivationBox;
		[ModelLib.CustomAttributes.DisplayGroup("Definition")]
		public ActivationBoxDefinition ActivationBox {
			get { return _ActivationBox; }
			set { SetField(ref _ActivationBox, value, () => ActivationBox); }
		}

	}
}


