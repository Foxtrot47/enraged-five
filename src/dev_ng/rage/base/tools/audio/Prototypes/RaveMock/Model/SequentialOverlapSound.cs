using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class SequentialOverlapSound:  Sound{

		private ModelLib.Types.TriState _Loop;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState Loop {
			get { return _Loop; }
			set { SetField(ref _Loop, value, () => Loop); }
		}

		private ModelLib.Types.TriState _ReleaseChildren;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState ReleaseChildren {
			get { return _ReleaseChildren; }
			set { SetField(ref _ReleaseChildren, value, () => ReleaseChildren); }
		}

		private System.UInt16 _DelayTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(65535)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt16 DelayTime {
			get { return _DelayTime; }
			set { SetField(ref _DelayTime, value, () => DelayTime); }
		}

		private System.String _DelayTimeVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String DelayTimeVariable {
			get { return _DelayTimeVariable; }
			set { SetField(ref _DelayTimeVariable, value, () => DelayTimeVariable); }
		}

		private ModelLib.Types.TriState _DelayTimeIsPercentage;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState DelayTimeIsPercentage {
			get { return _DelayTimeIsPercentage; }
			set { SetField(ref _DelayTimeIsPercentage, value, () => DelayTimeIsPercentage); }
		}

		private ModelLib.Types.TriState _DelayTimeIsRemaining;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState DelayTimeIsRemaining {
			get { return _DelayTimeIsRemaining; }
			set { SetField(ref _DelayTimeIsRemaining, value, () => DelayTimeIsRemaining); }
		}

		private ModelLib.Types.TriState _TriggerOnChildRelease;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("If set to 'yes' then delay time is ignored, and the next child will trigger when the previous child enters its release phase")]
		public ModelLib.Types.TriState TriggerOnChildRelease {
			get { return _TriggerOnChildRelease; }
			set { SetField(ref _TriggerOnChildRelease, value, () => TriggerOnChildRelease); }
		}

		private System.String _SequenceDirection;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String SequenceDirection {
			get { return _SequenceDirection; }
			set { SetField(ref _SequenceDirection, value, () => SequenceDirection); }
		}

		private ModelLib.Types.TriState _SpliceMode;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState SpliceMode {
			get { return _SpliceMode; }
			set { SetField(ref _SpliceMode, value, () => SpliceMode); }
		}

		public class ChildSoundDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundRef;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef SoundRef {
				get { return _SoundRef; }
				set { SetField(ref _SoundRef, value, () => SoundRef); }
			}

		}
		private ItemsObservableCollection<ChildSoundDefinition> _ChildSound = new ItemsObservableCollection<ChildSoundDefinition>();
		[ModelLib.CustomAttributes.MaxSize(254)]
		[XmlElement("ChildSound")]
		public ItemsObservableCollection<ChildSoundDefinition> ChildSound {
			get { return _ChildSound; }
			set { SetField(ref _ChildSound, value, () => ChildSound); }
		}

	}
}


