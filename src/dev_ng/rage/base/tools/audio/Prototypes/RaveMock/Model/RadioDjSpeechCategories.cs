using ModelLib.CustomAttributes;

namespace model {

	public enum RadioDjSpeechCategories {
		[ModelLib.CustomAttributes.Display("Intro")]
		kDjSpeechIntro,
		[ModelLib.CustomAttributes.Display("Outro")]
		kDjSpeechOutro,
		[ModelLib.CustomAttributes.Display("General")]
		kDjSpeechGeneral,
		[ModelLib.CustomAttributes.Display("Time")]
		kDjSpeechTime,
		[ModelLib.CustomAttributes.Display("To")]
		kDjSpeechTo
	}

}