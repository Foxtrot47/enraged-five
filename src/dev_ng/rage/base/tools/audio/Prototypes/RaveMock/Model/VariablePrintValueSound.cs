using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Debug Sound Types")]
	public class VariablePrintValueSound:  Sound{

		private System.String _Variable;
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String Variable {
			get { return _Variable; }
			set { SetField(ref _Variable, value, () => Variable); }
		}

		private System.String _Message;
		[ModelLib.CustomAttributes.Default("Variable value")]
		public System.String Message {
			get { return _Message; }
			set { SetField(ref _Message, value, () => Message); }
		}

	}
}


