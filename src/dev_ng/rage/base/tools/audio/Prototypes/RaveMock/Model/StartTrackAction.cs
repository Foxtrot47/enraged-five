using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class StartTrackAction:  MusicAction{

		private ModelLib.ObjectRef _Song;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Song {
			get { return _Song; }
			set { SetField(ref _Song, value, () => Song); }
		}

		private ModelLib.ObjectRef _Mood;
		[ModelLib.CustomAttributes.AllowedType(typeof(InteractiveMusicMood))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef Mood {
			get { return _Mood; }
			set { SetField(ref _Mood, value, () => Mood); }
		}

		private System.Single _VolumeOffset;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(12)]
		[ModelLib.CustomAttributes.Min(-100)]
		[ModelLib.CustomAttributes.Unit("decibels")]
		public System.Single VolumeOffset {
			get { return _VolumeOffset; }
			set { SetField(ref _VolumeOffset, value, () => VolumeOffset); }
		}

		private System.Int32 _FadeInTime;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(-1)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.Int32 FadeInTime {
			get { return _FadeInTime; }
			set { SetField(ref _FadeInTime, value, () => FadeInTime); }
		}

		private System.Int32 _FadeOutTime;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(-1)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.Int32 FadeOutTime {
			get { return _FadeOutTime; }
			set { SetField(ref _FadeOutTime, value, () => FadeOutTime); }
		}

		private System.Single _StartOffsetScalar;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single StartOffsetScalar {
			get { return _StartOffsetScalar; }
			set { SetField(ref _StartOffsetScalar, value, () => StartOffsetScalar); }
		}

		private System.UInt32 _LastSong;
		[ModelLib.CustomAttributes.Default("1000")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 LastSong {
			get { return _LastSong; }
			set { SetField(ref _LastSong, value, () => LastSong); }
		}

		private ModelLib.Types.TriState _OverrideRadio;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState OverrideRadio {
			get { return _OverrideRadio; }
			set { SetField(ref _OverrideRadio, value, () => OverrideRadio); }
		}

		private ModelLib.Types.TriState _RandomStartOffset;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState RandomStartOffset {
			get { return _RandomStartOffset; }
			set { SetField(ref _RandomStartOffset, value, () => RandomStartOffset); }
		}

		public class AltSongsDefinition: ModelBase {
			private ModelLib.ObjectRef _Song;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Song {
				get { return _Song; }
				set { SetField(ref _Song, value, () => Song); }
			}

			private MusicArea _ValidArea;
			[ModelLib.CustomAttributes.Default("kMusicAreaEverywhere")]
			public MusicArea ValidArea {
				get { return _ValidArea; }
				set { SetField(ref _ValidArea, value, () => ValidArea); }
			}

		}
		private ItemsObservableCollection<AltSongsDefinition> _AltSongs = new ItemsObservableCollection<AltSongsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(64)]
		[XmlElement("AltSongs")]
		public ItemsObservableCollection<AltSongsDefinition> AltSongs {
			get { return _AltSongs; }
			set { SetField(ref _AltSongs, value, () => AltSongs); }
		}

	}
}


