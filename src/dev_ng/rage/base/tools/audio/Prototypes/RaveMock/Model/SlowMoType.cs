using ModelLib.CustomAttributes;

namespace model {

	public enum SlowMoType {
		[ModelLib.CustomAttributes.Display("General")]
		AUD_SLOWMO_GENERAL,
		[ModelLib.CustomAttributes.Display("CinematicCamera")]
		AUD_SLOWMO_CINEMATIC,
		[ModelLib.CustomAttributes.Display("StuntJump")]
		AUD_SLOWMO_STUNT,
		[ModelLib.CustomAttributes.Display("WeaponWheel")]
		AUD_SLOWMO_WEAPON,
		[ModelLib.CustomAttributes.Display("PlayerSwitch")]
		AUD_SLOWMO_SWITCH,
		[ModelLib.CustomAttributes.Display("SpecialAbility")]
		AUD_SLOWMO_SPECIAL,
		[ModelLib.CustomAttributes.Display("Custom")]
		AUD_SLOWMO_CUSTOM,
		[ModelLib.CustomAttributes.Display("Radio wheel")]
		AUD_SLOWMO_RADIOWHEEL,
		[ModelLib.CustomAttributes.Display("Pause menu")]
		AUD_SLOWMO_PAUSEMENU
	}

}