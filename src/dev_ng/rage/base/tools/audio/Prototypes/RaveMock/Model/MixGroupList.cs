using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class MixGroupList:  ModelBase {

		public class MixGroupDefinition: ModelBase {
			private ModelLib.ObjectRef _MixGroupHash;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef MixGroupHash {
				get { return _MixGroupHash; }
				set { SetField(ref _MixGroupHash, value, () => MixGroupHash); }
			}

		}
		private ItemsObservableCollection<MixGroupDefinition> _MixGroup = new ItemsObservableCollection<MixGroupDefinition>();
		[ModelLib.CustomAttributes.MaxSize(255)]
		[XmlElement("MixGroup")]
		public ItemsObservableCollection<MixGroupDefinition> MixGroup {
			get { return _MixGroup; }
			set { SetField(ref _MixGroup, value, () => MixGroup); }
		}

	}
}


