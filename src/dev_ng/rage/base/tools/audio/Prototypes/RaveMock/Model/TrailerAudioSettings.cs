using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class TrailerAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _BumpSound;
		[ModelLib.CustomAttributes.Default("SUSPENSION_UP_COLLISION")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BumpSound {
			get { return _BumpSound; }
			set { SetField(ref _BumpSound, value, () => BumpSound); }
		}

		private ClatterType _ClatterType;
		[ModelLib.CustomAttributes.Default("AUD_CLATTER_NONE")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		public ClatterType ClatterType {
			get { return _ClatterType; }
			set { SetField(ref _ClatterType, value, () => ClatterType); }
		}

		private ModelLib.ObjectRef _LinkStressSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LinkStressSound {
			get { return _LinkStressSound; }
			set { SetField(ref _LinkStressSound, value, () => LinkStressSound); }
		}

		private ModelLib.ObjectRef _ModelCollisionSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModelAudioCollisionSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ModelCollisionSettings {
			get { return _ModelCollisionSettings; }
			set { SetField(ref _ModelCollisionSettings, value, () => ModelCollisionSettings); }
		}

		private ModelLib.ObjectRef _FireAudio;
		[ModelLib.CustomAttributes.Default("VEH_FIRE_SOUNDSET")]
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FireAudio {
			get { return _FireAudio; }
			set { SetField(ref _FireAudio, value, () => FireAudio); }
		}

		private System.Int32 _TrailerBumpVolumeBoost;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 TrailerBumpVolumeBoost {
			get { return _TrailerBumpVolumeBoost; }
			set { SetField(ref _TrailerBumpVolumeBoost, value, () => TrailerBumpVolumeBoost); }
		}

		private System.Single _ClatterSensitivityScalar;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		public System.Single ClatterSensitivityScalar {
			get { return _ClatterSensitivityScalar; }
			set { SetField(ref _ClatterSensitivityScalar, value, () => ClatterSensitivityScalar); }
		}

		private System.Int32 _ClatterVolumeBoost;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ClatterVolumeBoost {
			get { return _ClatterVolumeBoost; }
			set { SetField(ref _ClatterVolumeBoost, value, () => ClatterVolumeBoost); }
		}

		private System.Single _ChassisStressSensitivityScalar;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		public System.Single ChassisStressSensitivityScalar {
			get { return _ChassisStressSensitivityScalar; }
			set { SetField(ref _ChassisStressSensitivityScalar, value, () => ChassisStressSensitivityScalar); }
		}

		private System.Int32 _ChassisStressVolumeBoost;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ChassisStressVolumeBoost {
			get { return _ChassisStressVolumeBoost; }
			set { SetField(ref _ChassisStressVolumeBoost, value, () => ChassisStressVolumeBoost); }
		}

		private System.Int32 _LinkStressVolumeBoost;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 LinkStressVolumeBoost {
			get { return _LinkStressVolumeBoost; }
			set { SetField(ref _LinkStressVolumeBoost, value, () => LinkStressVolumeBoost); }
		}

		private System.Single _LinkStressSensitivityScalar;
		[ModelLib.CustomAttributes.Default("1.0")]
		public System.Single LinkStressSensitivityScalar {
			get { return _LinkStressSensitivityScalar; }
			set { SetField(ref _LinkStressSensitivityScalar, value, () => LinkStressSensitivityScalar); }
		}

	}
}


