using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class SpeechContextSettings:  ModelBase {

		public class ContextsDefinition: ModelBase {
			private ModelLib.ObjectRef _Context;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef Context {
				get { return _Context; }
				set { SetField(ref _Context, value, () => Context); }
			}

		}
		private ItemsObservableCollection<ContextsDefinition> _Contexts = new ItemsObservableCollection<ContextsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(400)]
		[XmlElement("Contexts")]
		public ItemsObservableCollection<ContextsDefinition> Contexts {
			get { return _Contexts; }
			set { SetField(ref _Contexts, value, () => Contexts); }
		}

	}
}


