using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class SetMoodAction:  MusicAction{

		private ModelLib.ObjectRef _Mood;
		[ModelLib.CustomAttributes.AllowedType(typeof(InteractiveMusicMood))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
		public ModelLib.ObjectRef Mood {
			get { return _Mood; }
			set { SetField(ref _Mood, value, () => Mood); }
		}

		private System.Single _VolumeOffset;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(12.0)]
		[ModelLib.CustomAttributes.Min(-100.0)]
		[ModelLib.CustomAttributes.Unit("decibels")]
		public System.Single VolumeOffset {
			get { return _VolumeOffset; }
			set { SetField(ref _VolumeOffset, value, () => VolumeOffset); }
		}

		private System.Int32 _FadeInTime;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(-1)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.Int32 FadeInTime {
			get { return _FadeInTime; }
			set { SetField(ref _FadeInTime, value, () => FadeInTime); }
		}

		private System.Int32 _FadeOutTime;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(-1)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.Int32 FadeOutTime {
			get { return _FadeOutTime; }
			set { SetField(ref _FadeOutTime, value, () => FadeOutTime); }
		}

	}
}


