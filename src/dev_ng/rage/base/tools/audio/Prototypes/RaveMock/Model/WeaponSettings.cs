using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Weapons")]
	public class WeaponSettings:  ModelBase {

		private ModelLib.ObjectRef _FireSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FireSound {
			get { return _FireSound; }
			set { SetField(ref _FireSound, value, () => FireSound); }
		}

		private ModelLib.ObjectRef _SuppressedFireSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SuppressedFireSound {
			get { return _SuppressedFireSound; }
			set { SetField(ref _SuppressedFireSound, value, () => SuppressedFireSound); }
		}

		private ModelLib.ObjectRef _AutoSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AutoSound {
			get { return _AutoSound; }
			set { SetField(ref _AutoSound, value, () => AutoSound); }
		}

		private ModelLib.ObjectRef _ReportSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ReportSound {
			get { return _ReportSound; }
			set { SetField(ref _ReportSound, value, () => ReportSound); }
		}

		private System.Single _ReportVolumeDelta;
		[ModelLib.CustomAttributes.Default("-2.0")]
		[ModelLib.CustomAttributes.Unit("dB")]
		public System.Single ReportVolumeDelta {
			get { return _ReportVolumeDelta; }
			set { SetField(ref _ReportVolumeDelta, value, () => ReportVolumeDelta); }
		}

		private System.UInt32 _ReportPredelayDelta;
		[ModelLib.CustomAttributes.Default("50")]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt32 ReportPredelayDelta {
			get { return _ReportPredelayDelta; }
			set { SetField(ref _ReportPredelayDelta, value, () => ReportPredelayDelta); }
		}

		private System.Single _TailEnergyValue;
		[ModelLib.CustomAttributes.Default("0.2")]
		public System.Single TailEnergyValue {
			get { return _TailEnergyValue; }
			set { SetField(ref _TailEnergyValue, value, () => TailEnergyValue); }
		}

		private ModelLib.ObjectRef _EchoSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EchoSound {
			get { return _EchoSound; }
			set { SetField(ref _EchoSound, value, () => EchoSound); }
		}

		private ModelLib.ObjectRef _SuppressedEchoSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SuppressedEchoSound {
			get { return _SuppressedEchoSound; }
			set { SetField(ref _SuppressedEchoSound, value, () => SuppressedEchoSound); }
		}

		private ModelLib.ObjectRef _ShellCasingSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ShellCasingSound {
			get { return _ShellCasingSound; }
			set { SetField(ref _ShellCasingSound, value, () => ShellCasingSound); }
		}

		private ModelLib.ObjectRef _SwipeSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SwipeSound {
			get { return _SwipeSound; }
			set { SetField(ref _SwipeSound, value, () => SwipeSound); }
		}

		private ModelLib.ObjectRef _GeneralStrikeSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GeneralStrikeSound {
			get { return _GeneralStrikeSound; }
			set { SetField(ref _GeneralStrikeSound, value, () => GeneralStrikeSound); }
		}

		private ModelLib.ObjectRef _PedStrikeSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedStrikeSound {
			get { return _PedStrikeSound; }
			set { SetField(ref _PedStrikeSound, value, () => PedStrikeSound); }
		}

		private ModelLib.ObjectRef _HeftSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HeftSound {
			get { return _HeftSound; }
			set { SetField(ref _HeftSound, value, () => HeftSound); }
		}

		private ModelLib.ObjectRef _PutDownSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PutDownSound {
			get { return _PutDownSound; }
			set { SetField(ref _PutDownSound, value, () => PutDownSound); }
		}

		private ModelLib.ObjectRef _RattleSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RattleSound {
			get { return _RattleSound; }
			set { SetField(ref _RattleSound, value, () => RattleSound); }
		}

		private ModelLib.ObjectRef _RattleLandSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RattleLandSound {
			get { return _RattleLandSound; }
			set { SetField(ref _RattleLandSound, value, () => RattleLandSound); }
		}

		private ModelLib.ObjectRef _PickupSound;
		[ModelLib.CustomAttributes.Default("FRONTEND_GAME_PICKUP_WEAPON")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PickupSound {
			get { return _PickupSound; }
			set { SetField(ref _PickupSound, value, () => PickupSound); }
		}

		private ShellCasingType _ShellCasing;
		[ModelLib.CustomAttributes.Default("SHELLCASING_METAL")]
		public ShellCasingType ShellCasing {
			get { return _ShellCasing; }
			set { SetField(ref _ShellCasing, value, () => ShellCasing); }
		}

		private ModelLib.ObjectRef _SafetyOn;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SafetyOn {
			get { return _SafetyOn; }
			set { SetField(ref _SafetyOn, value, () => SafetyOn); }
		}

		private ModelLib.ObjectRef _SafetyOff;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SafetyOff {
			get { return _SafetyOff; }
			set { SetField(ref _SafetyOff, value, () => SafetyOff); }
		}

		private ModelLib.Types.TriState _IsPlayerAutoFire;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState IsPlayerAutoFire {
			get { return _IsPlayerAutoFire; }
			set { SetField(ref _IsPlayerAutoFire, value, () => IsPlayerAutoFire); }
		}

		private ModelLib.Types.TriState _IsNpcAutoFire;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState IsNpcAutoFire {
			get { return _IsNpcAutoFire; }
			set { SetField(ref _IsNpcAutoFire, value, () => IsNpcAutoFire); }
		}

		private ModelLib.Types.TriState _IsNonLethal;
		[ModelLib.CustomAttributes.Default("false")]
		public ModelLib.Types.TriState IsNonLethal {
			get { return _IsNonLethal; }
			set { SetField(ref _IsNonLethal, value, () => IsNonLethal); }
		}

		private ModelLib.ObjectRef _SpecialWeaponSoundSet;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SpecialWeaponSoundSet {
			get { return _SpecialWeaponSoundSet; }
			set { SetField(ref _SpecialWeaponSoundSet, value, () => SpecialWeaponSoundSet); }
		}

		private ModelLib.ObjectRef _BankSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankSound {
			get { return _BankSound; }
			set { SetField(ref _BankSound, value, () => BankSound); }
		}

		private ModelLib.ObjectRef _InteriorShotSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef InteriorShotSound {
			get { return _InteriorShotSound; }
			set { SetField(ref _InteriorShotSound, value, () => InteriorShotSound); }
		}

		private ModelLib.ObjectRef _ReloadSounds;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ReloadSounds {
			get { return _ReloadSounds; }
			set { SetField(ref _ReloadSounds, value, () => ReloadSounds); }
		}

		private ModelLib.ObjectRef _IntoCoverSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IntoCoverSound {
			get { return _IntoCoverSound; }
			set { SetField(ref _IntoCoverSound, value, () => IntoCoverSound); }
		}

		private ModelLib.ObjectRef _OutOfCoverSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef OutOfCoverSound {
			get { return _OutOfCoverSound; }
			set { SetField(ref _OutOfCoverSound, value, () => OutOfCoverSound); }
		}

		private System.UInt32 _BulletImpactTimeFilter;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt32 BulletImpactTimeFilter {
			get { return _BulletImpactTimeFilter; }
			set { SetField(ref _BulletImpactTimeFilter, value, () => BulletImpactTimeFilter); }
		}

		private System.UInt32 _LastBulletImpactTime;
		[ModelLib.CustomAttributes.Default("0")]
		public System.UInt32 LastBulletImpactTime {
			get { return _LastBulletImpactTime; }
			set { SetField(ref _LastBulletImpactTime, value, () => LastBulletImpactTime); }
		}

		private ModelLib.Types.TriState _ReCockWhenSniping;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState ReCockWhenSniping {
			get { return _ReCockWhenSniping; }
			set { SetField(ref _ReCockWhenSniping, value, () => ReCockWhenSniping); }
		}

		private ModelLib.ObjectRef _RattleAimSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RattleAimSound {
			get { return _RattleAimSound; }
			set { SetField(ref _RattleAimSound, value, () => RattleAimSound); }
		}

		private ModelLib.ObjectRef _SmallAnimalStrikeSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SmallAnimalStrikeSound {
			get { return _SmallAnimalStrikeSound; }
			set { SetField(ref _SmallAnimalStrikeSound, value, () => SmallAnimalStrikeSound); }
		}

		private ModelLib.ObjectRef _BigAnimalStrikeSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BigAnimalStrikeSound {
			get { return _BigAnimalStrikeSound; }
			set { SetField(ref _BigAnimalStrikeSound, value, () => BigAnimalStrikeSound); }
		}

		private ModelLib.ObjectRef _SlowMoFireSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SlowMoFireSound {
			get { return _SlowMoFireSound; }
			set { SetField(ref _SlowMoFireSound, value, () => SlowMoFireSound); }
		}

		private ModelLib.ObjectRef _HitPedSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HitPedSound {
			get { return _HitPedSound; }
			set { SetField(ref _HitPedSound, value, () => HitPedSound); }
		}

	}
}


