using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class Category:  ModelBase {

		private System.String _Comments1;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Comments1 {
			get { return _Comments1; }
			set { SetField(ref _Comments1, value, () => Comments1); }
		}

		private System.String _Comments2;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Comments2 {
			get { return _Comments2; }
			set { SetField(ref _Comments2, value, () => Comments2); }
		}

		private System.String _Comments3;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Comments3 {
			get { return _Comments3; }
			set { SetField(ref _Comments3, value, () => Comments3); }
		}

		private System.Int16 _Volume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 Volume {
			get { return _Volume; }
			set { SetField(ref _Volume, value, () => Volume); }
		}

		private System.Int16 _Pitch;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Max(2400)]
		[ModelLib.CustomAttributes.Min(-2400)]
		[ModelLib.CustomAttributes.Unit("cents")]
		public System.Int16 Pitch {
			get { return _Pitch; }
			set { SetField(ref _Pitch, value, () => Pitch); }
		}

		private System.UInt16 _LPFCutoff;
		[ModelLib.CustomAttributes.Default("23900")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Max(23900)]
		[ModelLib.CustomAttributes.Min(50)]
		[ModelLib.CustomAttributes.Unit("Hz")]
		public System.UInt16 LPFCutoff {
			get { return _LPFCutoff; }
			set { SetField(ref _LPFCutoff, value, () => LPFCutoff); }
		}

		private ModelLib.ObjectRef _LPFDistanceCurve;
		[ModelLib.CustomAttributes.Default("DEFAULT_LPF_CURVE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef LPFDistanceCurve {
			get { return _LPFDistanceCurve; }
			set { SetField(ref _LPFDistanceCurve, value, () => LPFDistanceCurve); }
		}

		private System.UInt16 _HPFCutoff;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Max(23900)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("Hz")]
		public System.UInt16 HPFCutoff {
			get { return _HPFCutoff; }
			set { SetField(ref _HPFCutoff, value, () => HPFCutoff); }
		}

		private ModelLib.ObjectRef _HPFDistanceCurve;
		[ModelLib.CustomAttributes.Default("DEFAULT_HPF_CURVE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HPFDistanceCurve {
			get { return _HPFDistanceCurve; }
			set { SetField(ref _HPFDistanceCurve, value, () => HPFDistanceCurve); }
		}

		private System.UInt16 _DistanceRollOffScale;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 DistanceRollOffScale {
			get { return _DistanceRollOffScale; }
			set { SetField(ref _DistanceRollOffScale, value, () => DistanceRollOffScale); }
		}

		private System.UInt16 _PlateauRollOffScale;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 PlateauRollOffScale {
			get { return _PlateauRollOffScale; }
			set { SetField(ref _PlateauRollOffScale, value, () => PlateauRollOffScale); }
		}

		private System.UInt16 _OcclusionDamping;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 OcclusionDamping {
			get { return _OcclusionDamping; }
			set { SetField(ref _OcclusionDamping, value, () => OcclusionDamping); }
		}

		private System.UInt16 _EnvironmentalFilterDamping;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 EnvironmentalFilterDamping {
			get { return _EnvironmentalFilterDamping; }
			set { SetField(ref _EnvironmentalFilterDamping, value, () => EnvironmentalFilterDamping); }
		}

		private System.UInt16 _SourceReverbDamping;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 SourceReverbDamping {
			get { return _SourceReverbDamping; }
			set { SetField(ref _SourceReverbDamping, value, () => SourceReverbDamping); }
		}

		private System.UInt16 _DistanceReverbDamping;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 DistanceReverbDamping {
			get { return _DistanceReverbDamping; }
			set { SetField(ref _DistanceReverbDamping, value, () => DistanceReverbDamping); }
		}

		private System.UInt16 _InteriorReverbDamping;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 InteriorReverbDamping {
			get { return _InteriorReverbDamping; }
			set { SetField(ref _InteriorReverbDamping, value, () => InteriorReverbDamping); }
		}

		private System.UInt16 _EnvironmentalLoudness;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 EnvironmentalLoudness {
			get { return _EnvironmentalLoudness; }
			set { SetField(ref _EnvironmentalLoudness, value, () => EnvironmentalLoudness); }
		}

		private System.UInt16 _UnderwaterWetLevel;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 UnderwaterWetLevel {
			get { return _UnderwaterWetLevel; }
			set { SetField(ref _UnderwaterWetLevel, value, () => UnderwaterWetLevel); }
		}

		private System.UInt16 _StonedWetLevel;
		[ModelLib.CustomAttributes.Default("100")]
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt16 StonedWetLevel {
			get { return _StonedWetLevel; }
			set { SetField(ref _StonedWetLevel, value, () => StonedWetLevel); }
		}

		private SoundTimer _Timer;
		[ModelLib.CustomAttributes.Default("kSoundTimerUnspecified")]
		public SoundTimer Timer {
			get { return _Timer; }
			set { SetField(ref _Timer, value, () => Timer); }
		}

		private ModelLib.Types.TriState _MuteOnUserMusic;
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Description("If true then sounds withing this category will mute when the user is playing their music (Xbox360 only)")]
		public ModelLib.Types.TriState MuteOnUserMusic {
			get { return _MuteOnUserMusic; }
			set { SetField(ref _MuteOnUserMusic, value, () => MuteOnUserMusic); }
		}

		private ModelLib.Types.TriState _Unpausable;
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Description("if true then sounds withing this category will not be paused when the game is")]
		public ModelLib.Types.TriState Unpausable {
			get { return _Unpausable; }
			set { SetField(ref _Unpausable, value, () => Unpausable); }
		}

		private ModelLib.Types.TriState _DisableRearAtten;
		[ModelLib.CustomAttributes.AllowOverrideControl]
		[ModelLib.CustomAttributes.Description("if true then sounds in this category will not be filtered/attenuated when behind the camera")]
		public ModelLib.Types.TriState DisableRearAtten {
			get { return _DisableRearAtten; }
			set { SetField(ref _DisableRearAtten, value, () => DisableRearAtten); }
		}

		private ModelLib.Types.TriState _Mute;
		[ModelLib.CustomAttributes.Description("Only works in BANK builds: mute this category")]
		public ModelLib.Types.TriState Mute {
			get { return _Mute; }
			set { SetField(ref _Mute, value, () => Mute); }
		}

		private ModelLib.Types.TriState _Solo;
		[ModelLib.CustomAttributes.Description("Only works in BANK builds: solo this category")]
		public ModelLib.Types.TriState Solo {
			get { return _Solo; }
			set { SetField(ref _Solo, value, () => Solo); }
		}

		public class CategoryRefDefinition: ModelBase {
			private ModelLib.ObjectRef _CategoryId;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef CategoryId {
				get { return _CategoryId; }
				set { SetField(ref _CategoryId, value, () => CategoryId); }
			}

		}
		private ItemsObservableCollection<CategoryRefDefinition> _CategoryRef = new ItemsObservableCollection<CategoryRefDefinition>();
		[ModelLib.CustomAttributes.DisplayGroup("References")]
		[ModelLib.CustomAttributes.MaxSize(24)]
		[XmlElement("CategoryRef")]
		public ItemsObservableCollection<CategoryRefDefinition> CategoryRef {
			get { return _CategoryRef; }
			set { SetField(ref _CategoryRef, value, () => CategoryRef); }
		}

	}
}


