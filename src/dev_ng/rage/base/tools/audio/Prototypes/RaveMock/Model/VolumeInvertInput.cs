using ModelLib.CustomAttributes;

namespace model {

	public enum VolumeInvertInput {
		[ModelLib.CustomAttributes.Display("Invert")]
		INPUT_INVERT,
		[ModelLib.CustomAttributes.Display("DoNothing")]
		INPUT_DO_NOTHING
	}

}