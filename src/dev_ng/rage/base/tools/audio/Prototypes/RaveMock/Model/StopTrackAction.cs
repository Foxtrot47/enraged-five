using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class StopTrackAction:  MusicAction{

		private System.Int32 _FadeTime;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Max(60000)]
		[ModelLib.CustomAttributes.Min(-1)]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.Int32 FadeTime {
			get { return _FadeTime; }
			set { SetField(ref _FadeTime, value, () => FadeTime); }
		}

		private ModelLib.Types.TriState _UnfreezeRadio;
		public ModelLib.Types.TriState UnfreezeRadio {
			get { return _UnfreezeRadio; }
			set { SetField(ref _UnfreezeRadio, value, () => UnfreezeRadio); }
		}

	}
}


