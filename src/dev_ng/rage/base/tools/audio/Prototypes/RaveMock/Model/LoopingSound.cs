using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class LoopingSound:  Sound{

		private System.Int16 _LoopCount;
		[ModelLib.CustomAttributes.Default("-1")]
		[ModelLib.CustomAttributes.Max(512)]
		[ModelLib.CustomAttributes.Min(-1)]
		public System.Int16 LoopCount {
			get { return _LoopCount; }
			set { SetField(ref _LoopCount, value, () => LoopCount); }
		}

		private System.UInt16 _LoopCountVariance;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Description("Loop count will be set to LoopCount +/- variance")]
		public System.UInt16 LoopCountVariance {
			get { return _LoopCountVariance; }
			set { SetField(ref _LoopCountVariance, value, () => LoopCountVariance); }
		}

		private System.UInt16 _LoopPoint;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt16 LoopPoint {
			get { return _LoopPoint; }
			set { SetField(ref _LoopPoint, value, () => LoopPoint); }
		}

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		private ModelLib.Types.TriState _EnableSplicing;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState EnableSplicing {
			get { return _EnableSplicing; }
			set { SetField(ref _EnableSplicing, value, () => EnableSplicing); }
		}

		private System.String _LoopCountVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String LoopCountVariable {
			get { return _LoopCountVariable; }
			set { SetField(ref _LoopCountVariable, value, () => LoopCountVariable); }
		}

	}
}


