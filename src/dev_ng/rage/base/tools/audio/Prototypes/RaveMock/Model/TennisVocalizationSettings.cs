using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class TennisVocalizationSettings:  ModelBase {

		public class LiteDefinition: ModelBase {
			private System.Single _NullWeight;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single NullWeight {
				get { return _NullWeight; }
				set { SetField(ref _NullWeight, value, () => NullWeight); }
			}

			private System.Single _LiteWeight;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single LiteWeight {
				get { return _LiteWeight; }
				set { SetField(ref _LiteWeight, value, () => LiteWeight); }
			}

			private System.Single _MedWeight;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single MedWeight {
				get { return _MedWeight; }
				set { SetField(ref _MedWeight, value, () => MedWeight); }
			}

			private System.Single _StrongWeight;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single StrongWeight {
				get { return _StrongWeight; }
				set { SetField(ref _StrongWeight, value, () => StrongWeight); }
			}

		}
		private ItemsObservableCollection<LiteDefinition> _Lite = new ItemsObservableCollection<LiteDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("Lite")]
		public ItemsObservableCollection<LiteDefinition> Lite {
			get { return _Lite; }
			set { SetField(ref _Lite, value, () => Lite); }
		}

		public class MedDefinition: ModelBase {
			private System.Single _NullWeight;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single NullWeight {
				get { return _NullWeight; }
				set { SetField(ref _NullWeight, value, () => NullWeight); }
			}

			private System.Single _LiteWeight;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single LiteWeight {
				get { return _LiteWeight; }
				set { SetField(ref _LiteWeight, value, () => LiteWeight); }
			}

			private System.Single _MedWeight;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single MedWeight {
				get { return _MedWeight; }
				set { SetField(ref _MedWeight, value, () => MedWeight); }
			}

			private System.Single _StrongWeight;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single StrongWeight {
				get { return _StrongWeight; }
				set { SetField(ref _StrongWeight, value, () => StrongWeight); }
			}

		}
		private ItemsObservableCollection<MedDefinition> _Med = new ItemsObservableCollection<MedDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("Med")]
		public ItemsObservableCollection<MedDefinition> Med {
			get { return _Med; }
			set { SetField(ref _Med, value, () => Med); }
		}

		public class StrongDefinition: ModelBase {
			private System.Single _NullWeight;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single NullWeight {
				get { return _NullWeight; }
				set { SetField(ref _NullWeight, value, () => NullWeight); }
			}

			private System.Single _LiteWeight;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single LiteWeight {
				get { return _LiteWeight; }
				set { SetField(ref _LiteWeight, value, () => LiteWeight); }
			}

			private System.Single _MedWeight;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single MedWeight {
				get { return _MedWeight; }
				set { SetField(ref _MedWeight, value, () => MedWeight); }
			}

			private System.Single _StrongWeight;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Max(10.0)]
			[ModelLib.CustomAttributes.Min(0.0)]
			public System.Single StrongWeight {
				get { return _StrongWeight; }
				set { SetField(ref _StrongWeight, value, () => StrongWeight); }
			}

		}
		private ItemsObservableCollection<StrongDefinition> _Strong = new ItemsObservableCollection<StrongDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1)]
		[XmlElement("Strong")]
		public ItemsObservableCollection<StrongDefinition> Strong {
			get { return _Strong; }
			set { SetField(ref _Strong, value, () => Strong); }
		}

	}
}


