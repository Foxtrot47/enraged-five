﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Integration;

namespace RaveMock
{
    public partial class HierarchyBrowser2 : UserControl
    {
        public HierarchyBrowser2()
        {
            InitializeComponent();
        }

        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_DockableContent;
        public Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable Content
        {
            get
            {
                return this.m_DockableContent
                       ?? (this.m_DockableContent =
                           new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
                           {
                               Title = "HierarchyBrowser2",
                               Content = new WindowsFormsHost { Child = this },
                               ContentId = this.Name
                           });
            }
        }

        public void Activate()
        {
            this.Content.Show();
        }
    }
}
