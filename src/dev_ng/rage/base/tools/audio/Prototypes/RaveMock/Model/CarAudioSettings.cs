using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class CarAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _Engine;
		[ModelLib.CustomAttributes.AllowedType(typeof(VehicleEngineAudioSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Engine {
			get { return _Engine; }
			set { SetField(ref _Engine, value, () => Engine); }
		}

		private ModelLib.ObjectRef _GranularEngine;
		[ModelLib.CustomAttributes.AllowedType(typeof(GranularEngineAudioSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GranularEngine {
			get { return _GranularEngine; }
			set { SetField(ref _GranularEngine, value, () => GranularEngine); }
		}

		private ModelLib.Types.TriState _SportsCarRevsEnabled;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		public ModelLib.Types.TriState SportsCarRevsEnabled {
			get { return _SportsCarRevsEnabled; }
			set { SetField(ref _SportsCarRevsEnabled, value, () => SportsCarRevsEnabled); }
		}

		private ModelLib.ObjectRef _HornSounds;
		[ModelLib.CustomAttributes.Default("HORN_LIST_DEFAULT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundHashList))]
		[ModelLib.CustomAttributes.DisplayGroup("Alarms/Warnings")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HornSounds {
			get { return _HornSounds; }
			set { SetField(ref _HornSounds, value, () => HornSounds); }
		}

		private ModelLib.ObjectRef _DoorOpenSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Doors")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorOpenSound {
			get { return _DoorOpenSound; }
			set { SetField(ref _DoorOpenSound, value, () => DoorOpenSound); }
		}

		private ModelLib.ObjectRef _DoorCloseSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Doors")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorCloseSound {
			get { return _DoorCloseSound; }
			set { SetField(ref _DoorCloseSound, value, () => DoorCloseSound); }
		}

		private ModelLib.ObjectRef _BootOpenSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Doors")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BootOpenSound {
			get { return _BootOpenSound; }
			set { SetField(ref _BootOpenSound, value, () => BootOpenSound); }
		}

		private ModelLib.ObjectRef _BootCloseSound;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Doors")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BootCloseSound {
			get { return _BootCloseSound; }
			set { SetField(ref _BootCloseSound, value, () => BootCloseSound); }
		}

		private ModelLib.ObjectRef _RollSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RollSound {
			get { return _RollSound; }
			set { SetField(ref _RollSound, value, () => RollSound); }
		}

		private System.Single _BrakeSqueekFactor;
		[ModelLib.CustomAttributes.Default("0.5")]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		public System.Single BrakeSqueekFactor {
			get { return _BrakeSqueekFactor; }
			set { SetField(ref _BrakeSqueekFactor, value, () => BrakeSqueekFactor); }
		}

		private ModelLib.Types.TriState _ReverseWarning;
		[ModelLib.CustomAttributes.DisplayGroup("Alarms/Warnings")]
		public ModelLib.Types.TriState ReverseWarning {
			get { return _ReverseWarning; }
			set { SetField(ref _ReverseWarning, value, () => ReverseWarning); }
		}

		private ModelLib.Types.TriState _BigRigBrakes;
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		public ModelLib.Types.TriState BigRigBrakes {
			get { return _BigRigBrakes; }
			set { SetField(ref _BigRigBrakes, value, () => BigRigBrakes); }
		}

		private ModelLib.Types.TriState _DoorOpenWarning;
		[ModelLib.CustomAttributes.DisplayGroup("Alarms/Warnings")]
		public ModelLib.Types.TriState DoorOpenWarning {
			get { return _DoorOpenWarning; }
			set { SetField(ref _DoorOpenWarning, value, () => DoorOpenWarning); }
		}

		private ModelLib.ObjectRef _SuspensionUp;
		[ModelLib.CustomAttributes.Default("SUSPENSION_UP")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.Display("Up Sound")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SuspensionUp {
			get { return _SuspensionUp; }
			set { SetField(ref _SuspensionUp, value, () => SuspensionUp); }
		}

		private ModelLib.ObjectRef _SuspensionDown;
		[ModelLib.CustomAttributes.Default("SUSPENSION_DOWN")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.Display("Down Sound")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SuspensionDown {
			get { return _SuspensionDown; }
			set { SetField(ref _SuspensionDown, value, () => SuspensionDown); }
		}

		private System.Single _MinSuspCompThresh;
		[ModelLib.CustomAttributes.Default("0.45")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		public System.Single MinSuspCompThresh {
			get { return _MinSuspCompThresh; }
			set { SetField(ref _MinSuspCompThresh, value, () => MinSuspCompThresh); }
		}

		private System.Single _MaxSuspCompThres;
		[ModelLib.CustomAttributes.Default("6.9")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		public System.Single MaxSuspCompThres {
			get { return _MaxSuspCompThres; }
			set { SetField(ref _MaxSuspCompThres, value, () => MaxSuspCompThres); }
		}

		private ModelLib.ObjectRef _VehicleCollisions;
		[ModelLib.CustomAttributes.Default("VEHICLE_COLLISION_CAR")]
		[ModelLib.CustomAttributes.AllowedType(typeof(VehicleCollisionSettings))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehicleCollisions {
			get { return _VehicleCollisions; }
			set { SetField(ref _VehicleCollisions, value, () => VehicleCollisions); }
		}

		private ModelLib.ObjectRef _CarMake;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CarMake {
			get { return _CarMake; }
			set { SetField(ref _CarMake, value, () => CarMake); }
		}

		private ModelLib.ObjectRef _CarModel;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CarModel {
			get { return _CarModel; }
			set { SetField(ref _CarModel, value, () => CarModel); }
		}

		private ModelLib.ObjectRef _CarCategory;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CarCategory {
			get { return _CarCategory; }
			set { SetField(ref _CarCategory, value, () => CarCategory); }
		}

		private ModelLib.ObjectRef _ScannerVehicleSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(ScannerVehicleParams))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScannerVehicleSettings {
			get { return _ScannerVehicleSettings; }
			set { SetField(ref _ScannerVehicleSettings, value, () => ScannerVehicleSettings); }
		}

		private ModelLib.ObjectRef _JumpLandSound;
		[ModelLib.CustomAttributes.Default("JUMP_LAND_INTACT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef JumpLandSound {
			get { return _JumpLandSound; }
			set { SetField(ref _JumpLandSound, value, () => JumpLandSound); }
		}

		private ModelLib.ObjectRef _DamagedJumpLandSound;
		[ModelLib.CustomAttributes.Default("JUMP_LAND_LOOSE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DamagedJumpLandSound {
			get { return _DamagedJumpLandSound; }
			set { SetField(ref _DamagedJumpLandSound, value, () => DamagedJumpLandSound); }
		}

		private System.UInt32 _JumpLandMinThresh;
		[ModelLib.CustomAttributes.Default("31")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt32 JumpLandMinThresh {
			get { return _JumpLandMinThresh; }
			set { SetField(ref _JumpLandMinThresh, value, () => JumpLandMinThresh); }
		}

		private System.UInt32 _JumpLandMaxThresh;
		[ModelLib.CustomAttributes.Default("36")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt32 JumpLandMaxThresh {
			get { return _JumpLandMaxThresh; }
			set { SetField(ref _JumpLandMaxThresh, value, () => JumpLandMaxThresh); }
		}

		private VehicleVolumeCategory _VolumeCategory;
		[ModelLib.CustomAttributes.Default("VEHICLE_VOLUME_NORMAL")]
		public VehicleVolumeCategory VolumeCategory {
			get { return _VolumeCategory; }
			set { SetField(ref _VolumeCategory, value, () => VolumeCategory); }
		}

		private GPSType _GpsType;
		[ModelLib.CustomAttributes.Default("GPS_TYPE_NONE")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public GPSType GpsType {
			get { return _GpsType; }
			set { SetField(ref _GpsType, value, () => GpsType); }
		}

		private RadioType _RadioType;
		[ModelLib.CustomAttributes.Default("RADIO_TYPE_NORMAL")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public RadioType RadioType {
			get { return _RadioType; }
			set { SetField(ref _RadioType, value, () => RadioType); }
		}

		private RadioGenre _RadioGenre;
		[ModelLib.CustomAttributes.Default("RADIO_GENRE_UNSPECIFIED")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public RadioGenre RadioGenre {
			get { return _RadioGenre; }
			set { SetField(ref _RadioGenre, value, () => RadioGenre); }
		}

		private ModelLib.Types.TriState _DisableAmbientRadio;
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public ModelLib.Types.TriState DisableAmbientRadio {
			get { return _DisableAmbientRadio; }
			set { SetField(ref _DisableAmbientRadio, value, () => DisableAmbientRadio); }
		}

		private ModelLib.ObjectRef _IndicatorOn;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Alarms/Warnings")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IndicatorOn {
			get { return _IndicatorOn; }
			set { SetField(ref _IndicatorOn, value, () => IndicatorOn); }
		}

		private ModelLib.ObjectRef _IndicatorOff;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Alarms/Warnings")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef IndicatorOff {
			get { return _IndicatorOff; }
			set { SetField(ref _IndicatorOff, value, () => IndicatorOff); }
		}

		private ModelLib.ObjectRef _Handbrake;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Handbrake {
			get { return _Handbrake; }
			set { SetField(ref _Handbrake, value, () => Handbrake); }
		}

		private ModelLib.Types.TriState _HeavyRoadnoise;
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		public ModelLib.Types.TriState HeavyRoadnoise {
			get { return _HeavyRoadnoise; }
			set { SetField(ref _HeavyRoadnoise, value, () => HeavyRoadnoise); }
		}

		private GPSVoice _GpsVoice;
		[ModelLib.CustomAttributes.Default("GPS_VOICE_FEMALE")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public GPSVoice GpsVoice {
			get { return _GpsVoice; }
			set { SetField(ref _GpsVoice, value, () => GpsVoice); }
		}

		private System.SByte _AmbientRadioVol;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(6)]
		[ModelLib.CustomAttributes.Min(-24)]
		public System.SByte AmbientRadioVol {
			get { return _AmbientRadioVol; }
			set { SetField(ref _AmbientRadioVol, value, () => AmbientRadioVol); }
		}

		private ModelLib.Types.TriState _IAmNotACar;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public ModelLib.Types.TriState IAmNotACar {
			get { return _IAmNotACar; }
			set { SetField(ref _IAmNotACar, value, () => IAmNotACar); }
		}

		private AmbientRadioLeakage _RadioLeakage;
		[ModelLib.CustomAttributes.Default("LEAKAGE_MIDS_MEDIUM")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public AmbientRadioLeakage RadioLeakage {
			get { return _RadioLeakage; }
			set { SetField(ref _RadioLeakage, value, () => RadioLeakage); }
		}

		private ModelLib.ObjectRef _ParkingTone;
		[ModelLib.CustomAttributes.Default("PARKING_TONES")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Alarms/Warnings")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ParkingTone {
			get { return _ParkingTone; }
			set { SetField(ref _ParkingTone, value, () => ParkingTone); }
		}

		private ModelLib.ObjectRef _RoofStuckSound;
		[ModelLib.CustomAttributes.Default("AUTOMATIC_ROOF_BROKEN")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RoofStuckSound {
			get { return _RoofStuckSound; }
			set { SetField(ref _RoofStuckSound, value, () => RoofStuckSound); }
		}

		private ModelLib.ObjectRef _FreewayPassbyTyreBumpFront;
		[ModelLib.CustomAttributes.Default("HIGHWAY_PASSBY_TYRE_BUMP")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FreewayPassbyTyreBumpFront {
			get { return _FreewayPassbyTyreBumpFront; }
			set { SetField(ref _FreewayPassbyTyreBumpFront, value, () => FreewayPassbyTyreBumpFront); }
		}

		private ModelLib.ObjectRef _FreewayPassbyTyreBumpBack;
		[ModelLib.CustomAttributes.Default("HIGHWAY_PASSBY_TYRE_BUMP")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FreewayPassbyTyreBumpBack {
			get { return _FreewayPassbyTyreBumpBack; }
			set { SetField(ref _FreewayPassbyTyreBumpBack, value, () => FreewayPassbyTyreBumpBack); }
		}

		private ModelLib.ObjectRef _FireAudio;
		[ModelLib.CustomAttributes.Default("VEH_FIRE_SOUNDSET")]
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FireAudio {
			get { return _FireAudio; }
			set { SetField(ref _FireAudio, value, () => FireAudio); }
		}

		private ModelLib.ObjectRef _StartupRevs;
		[ModelLib.CustomAttributes.Default("STARTUP_SEQUENCE_DEFAULT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef StartupRevs {
			get { return _StartupRevs; }
			set { SetField(ref _StartupRevs, value, () => StartupRevs); }
		}

		private ModelLib.ObjectRef _WindNoise;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WindNoise {
			get { return _WindNoise; }
			set { SetField(ref _WindNoise, value, () => WindNoise); }
		}

		private ModelLib.ObjectRef _FreewayPassbyTyreBumpFrontSide;
		[ModelLib.CustomAttributes.Default("HIGHWAY_PASSBY_TYRE_BUMP_SIDE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FreewayPassbyTyreBumpFrontSide {
			get { return _FreewayPassbyTyreBumpFrontSide; }
			set { SetField(ref _FreewayPassbyTyreBumpFrontSide, value, () => FreewayPassbyTyreBumpFrontSide); }
		}

		private ModelLib.ObjectRef _FreewayPassbyTyreBumpBackSide;
		[ModelLib.CustomAttributes.Default("HIGHWAY_PASSBY_TYRE_BUMP_SIDE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FreewayPassbyTyreBumpBackSide {
			get { return _FreewayPassbyTyreBumpBackSide; }
			set { SetField(ref _FreewayPassbyTyreBumpBackSide, value, () => FreewayPassbyTyreBumpBackSide); }
		}

		private System.Single _MaxRollOffScalePlayer;
		[ModelLib.CustomAttributes.Default("6.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Rolloff")]
		public System.Single MaxRollOffScalePlayer {
			get { return _MaxRollOffScalePlayer; }
			set { SetField(ref _MaxRollOffScalePlayer, value, () => MaxRollOffScalePlayer); }
		}

		private System.Single _MaxRollOffScaleNPC;
		[ModelLib.CustomAttributes.Default("3.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Rolloff")]
		public System.Single MaxRollOffScaleNPC {
			get { return _MaxRollOffScaleNPC; }
			set { SetField(ref _MaxRollOffScaleNPC, value, () => MaxRollOffScaleNPC); }
		}

		private ModelLib.ObjectRef _ConvertibleRoofSoundSet;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("Gadgets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ConvertibleRoofSoundSet {
			get { return _ConvertibleRoofSoundSet; }
			set { SetField(ref _ConvertibleRoofSoundSet, value, () => ConvertibleRoofSoundSet); }
		}

		private ModelLib.Types.TriState _TyreChirpsEnabled;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		public ModelLib.Types.TriState TyreChirpsEnabled {
			get { return _TyreChirpsEnabled; }
			set { SetField(ref _TyreChirpsEnabled, value, () => TyreChirpsEnabled); }
		}

		private System.Int32 _OffRoadRumbleSoundVolume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 OffRoadRumbleSoundVolume {
			get { return _OffRoadRumbleSoundVolume; }
			set { SetField(ref _OffRoadRumbleSoundVolume, value, () => OffRoadRumbleSoundVolume); }
		}

		private ModelLib.ObjectRef _SirenSounds;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("Alarms/Warnings")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SirenSounds {
			get { return _SirenSounds; }
			set { SetField(ref _SirenSounds, value, () => SirenSounds); }
		}

		private ModelLib.ObjectRef _AlternativeGranularEngines;
		[ModelLib.CustomAttributes.AllowedType(typeof(GranularEngineSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AlternativeGranularEngines {
			get { return _AlternativeGranularEngines; }
			set { SetField(ref _AlternativeGranularEngines, value, () => AlternativeGranularEngines); }
		}

		private System.Single _AlternativeGranularEngineProbability;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single AlternativeGranularEngineProbability {
			get { return _AlternativeGranularEngineProbability; }
			set { SetField(ref _AlternativeGranularEngineProbability, value, () => AlternativeGranularEngineProbability); }
		}

		private System.UInt32 _StopStartProb;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.Max(100)]
		[ModelLib.CustomAttributes.Min(0)]
		[ModelLib.CustomAttributes.Unit("0.01units")]
		public System.UInt32 StopStartProb {
			get { return _StopStartProb; }
			set { SetField(ref _StopStartProb, value, () => StopStartProb); }
		}

		private ModelLib.ObjectRef _NPCRoadNoise;
		[ModelLib.CustomAttributes.Default("NPC_ROADNOISE_PASSES_DEFAULT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef NPCRoadNoise {
			get { return _NPCRoadNoise; }
			set { SetField(ref _NPCRoadNoise, value, () => NPCRoadNoise); }
		}

		private ModelLib.ObjectRef _NPCRoadNoiseHighway;
		[ModelLib.CustomAttributes.Default("NPC_ROADNOISE_PASSES_DEFAULT_HIGHWAYS")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef NPCRoadNoiseHighway {
			get { return _NPCRoadNoiseHighway; }
			set { SetField(ref _NPCRoadNoiseHighway, value, () => NPCRoadNoiseHighway); }
		}

		private ModelLib.ObjectRef _ForkliftSounds;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("Gadgets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ForkliftSounds {
			get { return _ForkliftSounds; }
			set { SetField(ref _ForkliftSounds, value, () => ForkliftSounds); }
		}

		private ModelLib.ObjectRef _TurretSounds;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("Gadgets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TurretSounds {
			get { return _TurretSounds; }
			set { SetField(ref _TurretSounds, value, () => TurretSounds); }
		}

		private ClatterType _ClatterType;
		[ModelLib.CustomAttributes.Default("AUD_CLATTER_NONE")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		public ClatterType ClatterType {
			get { return _ClatterType; }
			set { SetField(ref _ClatterType, value, () => ClatterType); }
		}

		private ModelLib.Types.TriState _IsToyCar;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		public ModelLib.Types.TriState IsToyCar {
			get { return _IsToyCar; }
			set { SetField(ref _IsToyCar, value, () => IsToyCar); }
		}

		private ModelLib.ObjectRef _DiggerSounds;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("Gadgets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DiggerSounds {
			get { return _DiggerSounds; }
			set { SetField(ref _DiggerSounds, value, () => DiggerSounds); }
		}

		private ModelLib.Types.TriState _HasCBRadio;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		public ModelLib.Types.TriState HasCBRadio {
			get { return _HasCBRadio; }
			set { SetField(ref _HasCBRadio, value, () => HasCBRadio); }
		}

		private ModelLib.ObjectRef _TowTruckSounds;
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("Gadgets")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TowTruckSounds {
			get { return _TowTruckSounds; }
			set { SetField(ref _TowTruckSounds, value, () => TowTruckSounds); }
		}

		private ModelLib.Types.TriState _DisableSkids;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		public ModelLib.Types.TriState DisableSkids {
			get { return _DisableSkids; }
			set { SetField(ref _DisableSkids, value, () => DisableSkids); }
		}

		private ModelLib.Types.TriState _CauseControllerRumble;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		public ModelLib.Types.TriState CauseControllerRumble {
			get { return _CauseControllerRumble; }
			set { SetField(ref _CauseControllerRumble, value, () => CauseControllerRumble); }
		}

		private ModelLib.Types.TriState _MobileCausesRadioInterference;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		public ModelLib.Types.TriState MobileCausesRadioInterference {
			get { return _MobileCausesRadioInterference; }
			set { SetField(ref _MobileCausesRadioInterference, value, () => MobileCausesRadioInterference); }
		}

		private ModelLib.Types.TriState _IsKickStarted;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		public ModelLib.Types.TriState IsKickStarted {
			get { return _IsKickStarted; }
			set { SetField(ref _IsKickStarted, value, () => IsKickStarted); }
		}

		private CarEngineTypes _EngineType;
		[ModelLib.CustomAttributes.Default("COMBUSTION")]
		public CarEngineTypes EngineType {
			get { return _EngineType; }
			set { SetField(ref _EngineType, value, () => EngineType); }
		}

		private ModelLib.ObjectRef _ElectricEngine;
		[ModelLib.CustomAttributes.AllowedType(typeof(ElectricEngineAudioSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ElectricEngine {
			get { return _ElectricEngine; }
			set { SetField(ref _ElectricEngine, value, () => ElectricEngine); }
		}

		private System.Single _Openness;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single Openness {
			get { return _Openness; }
			set { SetField(ref _Openness, value, () => Openness); }
		}

		private ModelLib.ObjectRef _ReverseWarningSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Alarms/Warnings")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ReverseWarningSound {
			get { return _ReverseWarningSound; }
			set { SetField(ref _ReverseWarningSound, value, () => ReverseWarningSound); }
		}

		private RandomDamageClass _RandomDamage;
		[ModelLib.CustomAttributes.Default("RANDOM_DAMAGE_NONE")]
		public RandomDamageClass RandomDamage {
			get { return _RandomDamage; }
			set { SetField(ref _RandomDamage, value, () => RandomDamage); }
		}

		private ModelLib.ObjectRef _WindClothSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef WindClothSound {
			get { return _WindClothSound; }
			set { SetField(ref _WindClothSound, value, () => WindClothSound); }
		}

		private ModelLib.ObjectRef _CarSpecificShutdownSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Extras")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CarSpecificShutdownSound {
			get { return _CarSpecificShutdownSound; }
			set { SetField(ref _CarSpecificShutdownSound, value, () => CarSpecificShutdownSound); }
		}

		private ModelLib.Types.TriState _HasAlarm;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("Alarms/Warnings")]
		public ModelLib.Types.TriState HasAlarm {
			get { return _HasAlarm; }
			set { SetField(ref _HasAlarm, value, () => HasAlarm); }
		}

		private System.Single _ClatterSensitivityScalar;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		public System.Single ClatterSensitivityScalar {
			get { return _ClatterSensitivityScalar; }
			set { SetField(ref _ClatterSensitivityScalar, value, () => ClatterSensitivityScalar); }
		}

		private System.Int32 _ClatterVolumeBoost;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ClatterVolumeBoost {
			get { return _ClatterVolumeBoost; }
			set { SetField(ref _ClatterVolumeBoost, value, () => ClatterVolumeBoost); }
		}

		private System.Single _ChassisStressSensitivityScalar;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		public System.Single ChassisStressSensitivityScalar {
			get { return _ChassisStressSensitivityScalar; }
			set { SetField(ref _ChassisStressSensitivityScalar, value, () => ChassisStressSensitivityScalar); }
		}

		private System.Int32 _ChassisStressVolumeBoost;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.DisplayGroup("RoadNoise/Clatter")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int32 ChassisStressVolumeBoost {
			get { return _ChassisStressVolumeBoost; }
			set { SetField(ref _ChassisStressVolumeBoost, value, () => ChassisStressVolumeBoost); }
		}

	}
}


