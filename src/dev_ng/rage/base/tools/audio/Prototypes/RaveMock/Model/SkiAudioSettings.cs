using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class SkiAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _StraightSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef StraightSound {
			get { return _StraightSound; }
			set { SetField(ref _StraightSound, value, () => StraightSound); }
		}

		private System.Single _MinSpeed;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single MinSpeed {
			get { return _MinSpeed; }
			set { SetField(ref _MinSpeed, value, () => MinSpeed); }
		}

		private System.Single _MaxSpeed;
		[ModelLib.CustomAttributes.Default("20.0")]
		public System.Single MaxSpeed {
			get { return _MaxSpeed; }
			set { SetField(ref _MaxSpeed, value, () => MaxSpeed); }
		}

		private ModelLib.ObjectRef _TurnSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TurnSound {
			get { return _TurnSound; }
			set { SetField(ref _TurnSound, value, () => TurnSound); }
		}

		private System.Single _MinTurn;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single MinTurn {
			get { return _MinTurn; }
			set { SetField(ref _MinTurn, value, () => MinTurn); }
		}

		private System.Single _MaxTurn;
		[ModelLib.CustomAttributes.Default("0.3")]
		public System.Single MaxTurn {
			get { return _MaxTurn; }
			set { SetField(ref _MaxTurn, value, () => MaxTurn); }
		}

		private ModelLib.ObjectRef _TurnEdgeSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TurnEdgeSound {
			get { return _TurnEdgeSound; }
			set { SetField(ref _TurnEdgeSound, value, () => TurnEdgeSound); }
		}

		private System.Single _MinSlopeEdge;
		[ModelLib.CustomAttributes.Default("0.0")]
		public System.Single MinSlopeEdge {
			get { return _MinSlopeEdge; }
			set { SetField(ref _MinSlopeEdge, value, () => MinSlopeEdge); }
		}

		private System.Single _MaxSlopeEdge;
		[ModelLib.CustomAttributes.Default("0.5")]
		public System.Single MaxSlopeEdge {
			get { return _MaxSlopeEdge; }
			set { SetField(ref _MaxSlopeEdge, value, () => MaxSlopeEdge); }
		}

	}
}


