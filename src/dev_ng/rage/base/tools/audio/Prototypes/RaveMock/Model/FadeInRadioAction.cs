using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class FadeInRadioAction:  MusicAction{

		private System.Single _FadeTime;
		[ModelLib.CustomAttributes.Default("0.5")]
		[ModelLib.CustomAttributes.Max(60)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single FadeTime {
			get { return _FadeTime; }
			set { SetField(ref _FadeTime, value, () => FadeTime); }
		}

	}
}


