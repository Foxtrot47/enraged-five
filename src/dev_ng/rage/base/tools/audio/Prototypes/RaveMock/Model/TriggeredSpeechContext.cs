using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class TriggeredSpeechContext:  AudBaseObject{

		private System.Int32 _TriggeredContextRepeatTime;
		[ModelLib.CustomAttributes.Default("0")]
		public System.Int32 TriggeredContextRepeatTime {
			get { return _TriggeredContextRepeatTime; }
			set { SetField(ref _TriggeredContextRepeatTime, value, () => TriggeredContextRepeatTime); }
		}

		private System.Int32 _PrimaryRepeatTime;
		[ModelLib.CustomAttributes.Default("0")]
		public System.Int32 PrimaryRepeatTime {
			get { return _PrimaryRepeatTime; }
			set { SetField(ref _PrimaryRepeatTime, value, () => PrimaryRepeatTime); }
		}

		private System.Int32 _PrimaryRepeatTimeOnSameVoice;
		[ModelLib.CustomAttributes.Default("-1")]
		public System.Int32 PrimaryRepeatTimeOnSameVoice {
			get { return _PrimaryRepeatTimeOnSameVoice; }
			set { SetField(ref _PrimaryRepeatTimeOnSameVoice, value, () => PrimaryRepeatTimeOnSameVoice); }
		}

		private System.Int32 _BackupRepeatTime;
		[ModelLib.CustomAttributes.Default("0")]
		public System.Int32 BackupRepeatTime {
			get { return _BackupRepeatTime; }
			set { SetField(ref _BackupRepeatTime, value, () => BackupRepeatTime); }
		}

		private System.Int32 _BackupRepeatTimeOnSameVoice;
		[ModelLib.CustomAttributes.Default("-1")]
		public System.Int32 BackupRepeatTimeOnSameVoice {
			get { return _BackupRepeatTimeOnSameVoice; }
			set { SetField(ref _BackupRepeatTimeOnSameVoice, value, () => BackupRepeatTimeOnSameVoice); }
		}

		private System.UInt32 _TimeCanNextUseTriggeredContext;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 TimeCanNextUseTriggeredContext {
			get { return _TimeCanNextUseTriggeredContext; }
			set { SetField(ref _TimeCanNextUseTriggeredContext, value, () => TimeCanNextUseTriggeredContext); }
		}

		private System.UInt32 _TimeCanNextPlayPrimary;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 TimeCanNextPlayPrimary {
			get { return _TimeCanNextPlayPrimary; }
			set { SetField(ref _TimeCanNextPlayPrimary, value, () => TimeCanNextPlayPrimary); }
		}

		private System.UInt32 _TimeCanNextPlayBackup;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 TimeCanNextPlayBackup {
			get { return _TimeCanNextPlayBackup; }
			set { SetField(ref _TimeCanNextPlayBackup, value, () => TimeCanNextPlayBackup); }
		}

		private ModelLib.ObjectRef _PrimarySpeechContext;
		[ModelLib.CustomAttributes.AllowedType(typeof(SpeechContext))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PrimarySpeechContext {
			get { return _PrimarySpeechContext; }
			set { SetField(ref _PrimarySpeechContext, value, () => PrimarySpeechContext); }
		}

		public class BackupSpeechContextDefinition: ModelBase {
			private ModelLib.ObjectRef _SpeechContext;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef SpeechContext {
				get { return _SpeechContext; }
				set { SetField(ref _SpeechContext, value, () => SpeechContext); }
			}

			private System.Single _Weight;
			[ModelLib.CustomAttributes.Default("1.0")]
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(0)]
			public System.Single Weight {
				get { return _Weight; }
				set { SetField(ref _Weight, value, () => Weight); }
			}

		}
		private ItemsObservableCollection<BackupSpeechContextDefinition> _BackupSpeechContext = new ItemsObservableCollection<BackupSpeechContextDefinition>();
		[ModelLib.CustomAttributes.MaxSize(16)]
		[XmlElement("BackupSpeechContext")]
		public ItemsObservableCollection<BackupSpeechContextDefinition> BackupSpeechContext {
			get { return _BackupSpeechContext; }
			set { SetField(ref _BackupSpeechContext, value, () => BackupSpeechContext); }
		}

	}
}


