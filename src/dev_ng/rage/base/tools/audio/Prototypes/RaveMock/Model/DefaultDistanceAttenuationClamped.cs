using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class DefaultDistanceAttenuationClamped:  DefaultDistanceAttenuation{

		private System.Int16 _MaxGain;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Display("Maximum Gain")]
		[ModelLib.CustomAttributes.Max(0)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 MaxGain {
			get { return _MaxGain; }
			set { SetField(ref _MaxGain, value, () => MaxGain); }
		}

		private ModelLib.Types.TriState _Rescale;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState Rescale {
			get { return _Rescale; }
			set { SetField(ref _Rescale, value, () => Rescale); }
		}

	}
}


