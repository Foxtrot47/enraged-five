﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.Integration;
using System.Windows.Forms;

namespace RaveMock
{
     public class WaveBrowser : ContainerControl
    {
        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_DockableContent;

         TextBox content = new TextBox();

         public WaveBrowser()
         {
             content.Height = 300;
             content.Width = 100;
             content.Text = "Content of the dummy WaveBrowser control";
         }

         public Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable Content
        {
            get
            {
                return this.m_DockableContent
                       ?? (this.m_DockableContent =
                           new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
                               {
                                   Title = "WaveBrowser",
                                   Content = new WindowsFormsHost { Child = content },
                                   ContentId = this.Name
                               });
            }
        }

        public void Activate()
        {
            this.Content.Show();
        }
    }
}
