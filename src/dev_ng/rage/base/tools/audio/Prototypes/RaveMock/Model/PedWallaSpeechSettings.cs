using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class PedWallaSpeechSettings:  ModelBase {

		private ModelLib.ObjectRef _SpeechSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SpeechSound {
			get { return _SpeechSound; }
			set { SetField(ref _SpeechSound, value, () => SpeechSound); }
		}

		private System.Int16 _VolumeAboveRMSLevel;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 VolumeAboveRMSLevel {
			get { return _VolumeAboveRMSLevel; }
			set { SetField(ref _VolumeAboveRMSLevel, value, () => VolumeAboveRMSLevel); }
		}

		private System.Int16 _MaxVolume;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 MaxVolume {
			get { return _MaxVolume; }
			set { SetField(ref _MaxVolume, value, () => MaxVolume); }
		}

		private System.Single _PedDensityThreshold;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single PedDensityThreshold {
			get { return _PedDensityThreshold; }
			set { SetField(ref _PedDensityThreshold, value, () => PedDensityThreshold); }
		}

		public class SpeechContextsDefinition: ModelBase {
			private System.String _ContextName;
			[ModelLib.CustomAttributes.Length(32)]
			public System.String ContextName {
				get { return _ContextName; }
				set { SetField(ref _ContextName, value, () => ContextName); }
			}

			private System.SByte _Variations;
			[ModelLib.CustomAttributes.Default("1")]
			[ModelLib.CustomAttributes.Max(1)]
			[ModelLib.CustomAttributes.Min(-1)]
			public System.SByte Variations {
				get { return _Variations; }
				set { SetField(ref _Variations, value, () => Variations); }
			}

		}
		private ItemsObservableCollection<SpeechContextsDefinition> _SpeechContexts = new ItemsObservableCollection<SpeechContextsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(20)]
		[XmlElement("SpeechContexts")]
		public ItemsObservableCollection<SpeechContextsDefinition> SpeechContexts {
			get { return _SpeechContexts; }
			set { SetField(ref _SpeechContexts, value, () => SpeechContexts); }
		}

	}
}


