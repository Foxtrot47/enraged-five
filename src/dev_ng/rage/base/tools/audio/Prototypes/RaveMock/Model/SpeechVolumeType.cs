using ModelLib.CustomAttributes;

namespace model {

	public enum SpeechVolumeType {
		[ModelLib.CustomAttributes.Display("Normal")]
		CONTEXT_VOLUME_NORMAL,
		[ModelLib.CustomAttributes.Display("Shouted")]
		CONTEXT_VOLUME_SHOUTED,
		[ModelLib.CustomAttributes.Display("Pain")]
		CONTEXT_VOLUME_PAIN,
		[ModelLib.CustomAttributes.Display("Megaphone")]
		CONTEXT_VOLUME_MEGAPHONE,
		[ModelLib.CustomAttributes.Display("FromHeli")]
		CONTEXT_VOLUME_FROM_HELI,
		[ModelLib.CustomAttributes.Display("Frontend")]
		CONTEXT_VOLUME_FRONTEND
	}

}