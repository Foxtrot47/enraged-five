using ModelLib.CustomAttributes;

namespace model {

	public enum SpeechRequestedVolumeType {
		[ModelLib.CustomAttributes.Display("Unspecified")]
		SPEECH_VOLUME_UNSPECIFIED,
		[ModelLib.CustomAttributes.Display("Normal")]
		SPEECH_VOLUME_NORMAL,
		[ModelLib.CustomAttributes.Display("Shouted")]
		SPEECH_VOLUME_SHOUTED,
		[ModelLib.CustomAttributes.Display("Frontend")]
		SPEECH_VOLUME_FRONTEND,
		[ModelLib.CustomAttributes.Display("Megaphone")]
		SPEECH_VOLUME_MEGAPHONE,
		[ModelLib.CustomAttributes.Display("Heli")]
		SPEECH_VOLUME_HELI
	}

}