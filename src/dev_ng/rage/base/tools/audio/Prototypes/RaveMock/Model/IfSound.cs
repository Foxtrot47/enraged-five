using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Logic Sound Types")]
	public class IfSound:  Sound{

		private ModelLib.ObjectRef _TrueSoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef TrueSoundRef {
			get { return _TrueSoundRef; }
			set { SetField(ref _TrueSoundRef, value, () => TrueSoundRef); }
		}

		private ModelLib.ObjectRef _FalseSoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef FalseSoundRef {
			get { return _FalseSoundRef; }
			set { SetField(ref _FalseSoundRef, value, () => FalseSoundRef); }
		}

		private System.String _LeftVariable;
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String LeftVariable {
			get { return _LeftVariable; }
			set { SetField(ref _LeftVariable, value, () => LeftVariable); }
		}

		private ConditionTypes _ConditionType;
		[ModelLib.CustomAttributes.Default("IF_CONDITION_EQUAL_TO")]
		public ConditionTypes ConditionType {
			get { return _ConditionType; }
			set { SetField(ref _ConditionType, value, () => ConditionType); }
		}

		private ModelLib.Types.TriState _ChooseOnInit;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("If set to yes then the if sound will choose which child to play on init, rather than at the start of playback")]
		public ModelLib.Types.TriState ChooseOnInit {
			get { return _ChooseOnInit; }
			set { SetField(ref _ChooseOnInit, value, () => ChooseOnInit); }
		}

		public class RHSDefinition: ModelBase {
			private System.Single _RightValue;
			[ModelLib.CustomAttributes.Default("0")]
			public System.Single RightValue {
				get { return _RightValue; }
				set { SetField(ref _RightValue, value, () => RightValue); }
			}

			private System.String _RightVariable;
			[ModelLib.CustomAttributes.Unit("variable")]
			public System.String RightVariable {
				get { return _RightVariable; }
				set { SetField(ref _RightVariable, value, () => RightVariable); }
			}

		}
		private RHSDefinition _RHS;
		public RHSDefinition RHS {
			get { return _RHS; }
			set { SetField(ref _RHS, value, () => RHS); }
		}

	}
}


