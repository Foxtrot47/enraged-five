using ModelLib.CustomAttributes;

namespace model {

	public enum MicAttenuationType {
		[ModelLib.CustomAttributes.Display("RearAttenuationDefault")]
		REAR_ATTENUATION_DEFAULT,
		[ModelLib.CustomAttributes.Display("RearAttenuationAlways")]
		REAR_ATTENUATION_ALWAYS
	}

}