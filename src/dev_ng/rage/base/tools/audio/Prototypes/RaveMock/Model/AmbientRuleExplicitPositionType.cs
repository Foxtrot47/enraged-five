using ModelLib.CustomAttributes;

namespace model {

	public enum AmbientRuleExplicitPositionType {
		[ModelLib.CustomAttributes.Display("Don't Use")]
		RULE_EXPLICIT_SPAWN_POS_DISABLED,
		[ModelLib.CustomAttributes.Display("World Relative")]
		RULE_EXPLICIT_SPAWN_POS_WORLD_RELATIVE,
		[ModelLib.CustomAttributes.Display("Interior Relative")]
		RULE_EXPLICIT_SPAWN_POS_INTERIOR_RELATIVE
	}

}