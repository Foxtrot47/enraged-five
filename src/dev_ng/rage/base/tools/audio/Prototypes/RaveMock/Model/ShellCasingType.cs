using ModelLib.CustomAttributes;

namespace model {

	public enum ShellCasingType {
		[ModelLib.CustomAttributes.Display("MetalSmall")]
		SHELLCASING_METAL_SMALL,
		[ModelLib.CustomAttributes.Display("MetalMedium")]
		SHELLCASING_METAL,
		[ModelLib.CustomAttributes.Display("MetalLarge")]
		SHELLCASING_METAL_LARGE,
		[ModelLib.CustomAttributes.Display("Plastic")]
		SHELLCASING_PLASTIC,
		[ModelLib.CustomAttributes.Display("None")]
		SHELLCASING_NONE
	}

}