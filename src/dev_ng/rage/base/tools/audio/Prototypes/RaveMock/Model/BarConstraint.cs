using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Music")]
	public class BarConstraint:  MusicTimingConstraint{

		private System.Int32 _PatternLength;
		[ModelLib.CustomAttributes.Default("8")]
		[ModelLib.CustomAttributes.Max(512)]
		[ModelLib.CustomAttributes.Min(1)]
		[ModelLib.CustomAttributes.Unit("bars")]
		public System.Int32 PatternLength {
			get { return _PatternLength; }
			set { SetField(ref _PatternLength, value, () => PatternLength); }
		}

		private System.Int32 _ValidBar;
		[ModelLib.CustomAttributes.Default("1")]
		[ModelLib.CustomAttributes.Max(512)]
		[ModelLib.CustomAttributes.Min(1)]
		[ModelLib.CustomAttributes.Unit("bars")]
		public System.Int32 ValidBar {
			get { return _ValidBar; }
			set { SetField(ref _ValidBar, value, () => ValidBar); }
		}

	}
}


