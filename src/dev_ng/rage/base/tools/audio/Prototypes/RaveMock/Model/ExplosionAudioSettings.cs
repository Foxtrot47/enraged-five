using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Weapons")]
	public class ExplosionAudioSettings:  ModelBase {

		private ModelLib.ObjectRef _ExplosionSound;
		[ModelLib.CustomAttributes.Default("MAIN_EXPLOSION")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExplosionSound {
			get { return _ExplosionSound; }
			set { SetField(ref _ExplosionSound, value, () => ExplosionSound); }
		}

		private ModelLib.ObjectRef _DebrisSound;
		[ModelLib.CustomAttributes.Default("DEBRIS_RANDOM_ONE_SHOTS")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DebrisSound {
			get { return _DebrisSound; }
			set { SetField(ref _DebrisSound, value, () => DebrisSound); }
		}

		private System.Single _DeafeningVolume;
		[ModelLib.CustomAttributes.Default("16.0")]
		[ModelLib.CustomAttributes.DisplayGroup("ExplosionParams")]
		[ModelLib.CustomAttributes.Max(20.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single DeafeningVolume {
			get { return _DeafeningVolume; }
			set { SetField(ref _DeafeningVolume, value, () => DeafeningVolume); }
		}

		private System.Single _DebrisTimeScale;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("ExplosionParams")]
		[ModelLib.CustomAttributes.Max(20.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single DebrisTimeScale {
			get { return _DebrisTimeScale; }
			set { SetField(ref _DebrisTimeScale, value, () => DebrisTimeScale); }
		}

		private System.Single _DebrisVolume;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("ExplosionParams")]
		[ModelLib.CustomAttributes.Max(24.0)]
		[ModelLib.CustomAttributes.Min(-100.0)]
		public System.Single DebrisVolume {
			get { return _DebrisVolume; }
			set { SetField(ref _DebrisVolume, value, () => DebrisVolume); }
		}

		private System.Single _ShockwaveIntensity;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("ExplosionParams")]
		[ModelLib.CustomAttributes.Max(20.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single ShockwaveIntensity {
			get { return _ShockwaveIntensity; }
			set { SetField(ref _ShockwaveIntensity, value, () => ShockwaveIntensity); }
		}

		private System.Single _ShockwaveDelay;
		[ModelLib.CustomAttributes.Default("0.55")]
		[ModelLib.CustomAttributes.DisplayGroup("ExplosionParams")]
		[ModelLib.CustomAttributes.Max(20.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single ShockwaveDelay {
			get { return _ShockwaveDelay; }
			set { SetField(ref _ShockwaveDelay, value, () => ShockwaveDelay); }
		}

		private ModelLib.Types.TriState _DisableDebris;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("ExplosionParams")]
		public ModelLib.Types.TriState DisableDebris {
			get { return _DisableDebris; }
			set { SetField(ref _DisableDebris, value, () => DisableDebris); }
		}

		private ModelLib.Types.TriState _DisableShockwave;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("ExplosionParams")]
		public ModelLib.Types.TriState DisableShockwave {
			get { return _DisableShockwave; }
			set { SetField(ref _DisableShockwave, value, () => DisableShockwave); }
		}

		private ModelLib.Types.TriState _IsCarExplosion;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.DisplayGroup("ExplosionParams")]
		public ModelLib.Types.TriState IsCarExplosion {
			get { return _IsCarExplosion; }
			set { SetField(ref _IsCarExplosion, value, () => IsCarExplosion); }
		}

	}
}


