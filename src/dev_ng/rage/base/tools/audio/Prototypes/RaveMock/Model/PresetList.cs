using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class PresetList:  ModelBase {

		public class PresetRefDefinition: ModelBase {
			private ModelLib.ObjectRef _Preset;
			[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef Preset {
				get { return _Preset; }
				set { SetField(ref _Preset, value, () => Preset); }
			}

		}
		private ItemsObservableCollection<PresetRefDefinition> _PresetRef = new ItemsObservableCollection<PresetRefDefinition>();
		[ModelLib.CustomAttributes.MaxSize(65535)]
		[XmlElement("PresetRef")]
		public ItemsObservableCollection<PresetRefDefinition> PresetRef {
			get { return _PresetRef; }
			set { SetField(ref _PresetRef, value, () => PresetRef); }
		}

	}
}


