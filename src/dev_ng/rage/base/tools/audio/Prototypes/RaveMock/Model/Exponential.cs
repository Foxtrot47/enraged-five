using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class Exponential:  BaseCurve{

		private System.Single _Exponent;
		[ModelLib.CustomAttributes.Default("2.0")]
		[ModelLib.CustomAttributes.Max(10)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Single Exponent {
			get { return _Exponent; }
			set { SetField(ref _Exponent, value, () => Exponent); }
		}

		public class FlipDefinition: ModelBase {
			private ModelLib.Types.Bit _x;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit x {
				get { return _x; }
				set { SetField(ref _x, value, () => x); }
			}

			private ModelLib.Types.Bit _y;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

		}
		private FlipDefinition _Flip;
		public FlipDefinition Flip {
			get { return _Flip; }
			set { SetField(ref _Flip, value, () => Flip); }
		}

	}
}


