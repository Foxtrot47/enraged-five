using ModelLib.CustomAttributes;

namespace model {

	public enum MaterialType {
		[ModelLib.CustomAttributes.Display("HollowMetal")]
		HOLLOW_METAL,
		[ModelLib.CustomAttributes.Display("HollowPlastic")]
		HOLLOW_PLASTIC,
		[ModelLib.CustomAttributes.Display("Glass")]
		GLASS,
		[ModelLib.CustomAttributes.Display("Other")]
		OTHER
	}

}