using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Physics")]
	public class DoorList:  ModelBase {

		public class DoorDefinition: ModelBase {
			private System.String _PropName;
			public System.String PropName {
				get { return _PropName; }
				set { SetField(ref _PropName, value, () => PropName); }
			}

			private ModelLib.ObjectRef _DoorAudioSettings;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef DoorAudioSettings {
				get { return _DoorAudioSettings; }
				set { SetField(ref _DoorAudioSettings, value, () => DoorAudioSettings); }
			}

		}
		private ItemsObservableCollection<DoorDefinition> _Door = new ItemsObservableCollection<DoorDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1000)]
		[XmlElement("Door")]
		public ItemsObservableCollection<DoorDefinition> Door {
			get { return _Door; }
			set { SetField(ref _Door, value, () => Door); }
		}

	}
}


