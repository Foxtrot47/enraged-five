using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Logic Sound Types")]
	public class VariableBlockSound:  Sound{

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		public class VariableDefinition: ModelBase {
			private System.String _VariableRef;
			[ModelLib.CustomAttributes.Unit("variabledef")]
			public System.String VariableRef {
				get { return _VariableRef; }
				set { SetField(ref _VariableRef, value, () => VariableRef); }
			}

			private System.Single _VariableValue;
			public System.Single VariableValue {
				get { return _VariableValue; }
				set { SetField(ref _VariableValue, value, () => VariableValue); }
			}

			private System.Single _InitialVariance;
			[ModelLib.CustomAttributes.Default("0.0")]
			[ModelLib.CustomAttributes.Description("This variable will be initialised to VariableValue +/- InitialVariance")]
			public System.Single InitialVariance {
				get { return _InitialVariance; }
				set { SetField(ref _InitialVariance, value, () => InitialVariance); }
			}

			private VariableUsage _Usage;
			[ModelLib.CustomAttributes.Default("VARIABLE_USAGE_SOUND")]
			public VariableUsage Usage {
				get { return _Usage; }
				set { SetField(ref _Usage, value, () => Usage); }
			}

		}
		private ItemsObservableCollection<VariableDefinition> _Variable = new ItemsObservableCollection<VariableDefinition>();
		[ModelLib.CustomAttributes.MaxSize(20)]
		[XmlElement("Variable")]
		public ItemsObservableCollection<VariableDefinition> Variable {
			get { return _Variable; }
			set { SetField(ref _Variable, value, () => Variable); }
		}

	}
}


