using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class EnvelopeSound:  Sound{

		private System.String _AttackVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String AttackVariable {
			get { return _AttackVariable; }
			set { SetField(ref _AttackVariable, value, () => AttackVariable); }
		}

		private System.String _DecayVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String DecayVariable {
			get { return _DecayVariable; }
			set { SetField(ref _DecayVariable, value, () => DecayVariable); }
		}

		private System.String _SustainVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String SustainVariable {
			get { return _SustainVariable; }
			set { SetField(ref _SustainVariable, value, () => SustainVariable); }
		}

		private System.String _HoldVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String HoldVariable {
			get { return _HoldVariable; }
			set { SetField(ref _HoldVariable, value, () => HoldVariable); }
		}

		private System.String _ReleaseVariable;
		[ModelLib.CustomAttributes.DisplayGroup("Variables")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String ReleaseVariable {
			get { return _ReleaseVariable; }
			set { SetField(ref _ReleaseVariable, value, () => ReleaseVariable); }
		}

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		private EnvelopeSoundMode _Mode;
		[ModelLib.CustomAttributes.Default("kEnvelopeSoundVolume")]
		public EnvelopeSoundMode Mode {
			get { return _Mode; }
			set { SetField(ref _Mode, value, () => Mode); }
		}

		private System.Byte _padding0;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.Byte padding0 {
			get { return _padding0; }
			set { SetField(ref _padding0, value, () => padding0); }
		}

		private System.UInt16 _padding1;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt16 padding1 {
			get { return _padding1; }
			set { SetField(ref _padding1, value, () => padding1); }
		}

		private System.String _OutputVariable;
		[ModelLib.CustomAttributes.OutputVariable]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String OutputVariable {
			get { return _OutputVariable; }
			set { SetField(ref _OutputVariable, value, () => OutputVariable); }
		}

		private ModelLib.Types.TriState _ShouldStopChildren;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.Description("If yes the child sound will be stopped once the envelope finishes the release phase")]
		public ModelLib.Types.TriState ShouldStopChildren {
			get { return _ShouldStopChildren; }
			set { SetField(ref _ShouldStopChildren, value, () => ShouldStopChildren); }
		}

		private ModelLib.Types.TriState _ShouldCascadeRelease;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("If yes the child sound will be stopped when this sound is released, regardless of release phase length.")]
		public ModelLib.Types.TriState ShouldCascadeRelease {
			get { return _ShouldCascadeRelease; }
			set { SetField(ref _ShouldCascadeRelease, value, () => ShouldCascadeRelease); }
		}

		public class EnvelopeDefinition: ModelBase {
			private System.UInt16 _Attack;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(65535)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("ms")]
			public System.UInt16 Attack {
				get { return _Attack; }
				set { SetField(ref _Attack, value, () => Attack); }
			}

			private System.UInt16 _AttackVariance;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(65535)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("ms")]
			public System.UInt16 AttackVariance {
				get { return _AttackVariance; }
				set { SetField(ref _AttackVariance, value, () => AttackVariance); }
			}

			private System.UInt16 _Decay;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(65535)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("ms")]
			public System.UInt16 Decay {
				get { return _Decay; }
				set { SetField(ref _Decay, value, () => Decay); }
			}

			private System.UInt16 _DecayVariance;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(65535)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("ms")]
			public System.UInt16 DecayVariance {
				get { return _DecayVariance; }
				set { SetField(ref _DecayVariance, value, () => DecayVariance); }
			}

			private System.Byte _Sustain;
			[ModelLib.CustomAttributes.Default("100")]
			[ModelLib.CustomAttributes.Max(100)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("percent")]
			public System.Byte Sustain {
				get { return _Sustain; }
				set { SetField(ref _Sustain, value, () => Sustain); }
			}

			private System.Byte _SustainVariance;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(100)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("percent")]
			public System.Byte SustainVariance {
				get { return _SustainVariance; }
				set { SetField(ref _SustainVariance, value, () => SustainVariance); }
			}

			private System.Int32 _Hold;
			[ModelLib.CustomAttributes.Default("-1")]
			[ModelLib.CustomAttributes.Max(32767)]
			[ModelLib.CustomAttributes.Min(-1)]
			[ModelLib.CustomAttributes.Unit("ms")]
			public System.Int32 Hold {
				get { return _Hold; }
				set { SetField(ref _Hold, value, () => Hold); }
			}

			private System.UInt16 _HoldVariance;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(65535)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("ms")]
			public System.UInt16 HoldVariance {
				get { return _HoldVariance; }
				set { SetField(ref _HoldVariance, value, () => HoldVariance); }
			}

			private System.Int32 _Release;
			[ModelLib.CustomAttributes.Default("-1")]
			[ModelLib.CustomAttributes.Max(65535)]
			[ModelLib.CustomAttributes.Min(-1)]
			[ModelLib.CustomAttributes.Unit("ms")]
			public System.Int32 Release {
				get { return _Release; }
				set { SetField(ref _Release, value, () => Release); }
			}

			private System.UInt32 _ReleaseVariance;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(65535)]
			[ModelLib.CustomAttributes.Min(0)]
			[ModelLib.CustomAttributes.Unit("ms")]
			public System.UInt32 ReleaseVariance {
				get { return _ReleaseVariance; }
				set { SetField(ref _ReleaseVariance, value, () => ReleaseVariance); }
			}

			private ModelLib.ObjectRef _AttackCurve;
			[ModelLib.CustomAttributes.Default("LINEAR_RISE")]
			[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef AttackCurve {
				get { return _AttackCurve; }
				set { SetField(ref _AttackCurve, value, () => AttackCurve); }
			}

			private ModelLib.ObjectRef _DecayCurve;
			[ModelLib.CustomAttributes.Default("LINEAR_FALL")]
			[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef DecayCurve {
				get { return _DecayCurve; }
				set { SetField(ref _DecayCurve, value, () => DecayCurve); }
			}

			private ModelLib.ObjectRef _ReleaseCurve;
			[ModelLib.CustomAttributes.Default("DEFAULT_RELEASE_CURVE")]
			[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef ReleaseCurve {
				get { return _ReleaseCurve; }
				set { SetField(ref _ReleaseCurve, value, () => ReleaseCurve); }
			}

		}
		private EnvelopeDefinition _Envelope;
		public EnvelopeDefinition Envelope {
			get { return _Envelope; }
			set { SetField(ref _Envelope, value, () => Envelope); }
		}

		public class OutputRangeDefinition: ModelBase {
			private System.Single _min;
			[ModelLib.CustomAttributes.Default("-100.0")]
			public System.Single min {
				get { return _min; }
				set { SetField(ref _min, value, () => min); }
			}

			private System.Single _max;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single max {
				get { return _max; }
				set { SetField(ref _max, value, () => max); }
			}

		}
		private OutputRangeDefinition _OutputRange;
		public OutputRangeDefinition OutputRange {
			get { return _OutputRange; }
			set { SetField(ref _OutputRange, value, () => OutputRange); }
		}

	}
}


