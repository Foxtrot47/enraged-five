using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Speech")]
	public class PlayerBreathingSettings:  ModelBase {

		private System.Int32 _TimeBetweenLowRunBreaths;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 TimeBetweenLowRunBreaths {
			get { return _TimeBetweenLowRunBreaths; }
			set { SetField(ref _TimeBetweenLowRunBreaths, value, () => TimeBetweenLowRunBreaths); }
		}

		private System.Int32 _TimeBetweenHighRunBreaths;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 TimeBetweenHighRunBreaths {
			get { return _TimeBetweenHighRunBreaths; }
			set { SetField(ref _TimeBetweenHighRunBreaths, value, () => TimeBetweenHighRunBreaths); }
		}

		private System.Int32 _TimeBetweenExhaustedBreaths;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 TimeBetweenExhaustedBreaths {
			get { return _TimeBetweenExhaustedBreaths; }
			set { SetField(ref _TimeBetweenExhaustedBreaths, value, () => TimeBetweenExhaustedBreaths); }
		}

		private System.Int32 _TimeBetweenFinalBreaths;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 TimeBetweenFinalBreaths {
			get { return _TimeBetweenFinalBreaths; }
			set { SetField(ref _TimeBetweenFinalBreaths, value, () => TimeBetweenFinalBreaths); }
		}

		private System.Int32 _MinBreathStateChangeWaitToLow;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinBreathStateChangeWaitToLow {
			get { return _MinBreathStateChangeWaitToLow; }
			set { SetField(ref _MinBreathStateChangeWaitToLow, value, () => MinBreathStateChangeWaitToLow); }
		}

		private System.Int32 _MaxBreathStateChangeWaitToLow;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MaxBreathStateChangeWaitToLow {
			get { return _MaxBreathStateChangeWaitToLow; }
			set { SetField(ref _MaxBreathStateChangeWaitToLow, value, () => MaxBreathStateChangeWaitToLow); }
		}

		private System.Int32 _MinBreathStateChangeLowToHighFromWait;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinBreathStateChangeLowToHighFromWait {
			get { return _MinBreathStateChangeLowToHighFromWait; }
			set { SetField(ref _MinBreathStateChangeLowToHighFromWait, value, () => MinBreathStateChangeLowToHighFromWait); }
		}

		private System.Int32 _MaxBreathStateChangeLowToHighFromWait;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MaxBreathStateChangeLowToHighFromWait {
			get { return _MaxBreathStateChangeLowToHighFromWait; }
			set { SetField(ref _MaxBreathStateChangeLowToHighFromWait, value, () => MaxBreathStateChangeLowToHighFromWait); }
		}

		private System.Int32 _MinBreathStateChangeHighToLowFromLow;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinBreathStateChangeHighToLowFromLow {
			get { return _MinBreathStateChangeHighToLowFromLow; }
			set { SetField(ref _MinBreathStateChangeHighToLowFromLow, value, () => MinBreathStateChangeHighToLowFromLow); }
		}

		private System.Int32 _MaxBreathStateChangeHighToLowFromLow;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MaxBreathStateChangeHighToLowFromLow {
			get { return _MaxBreathStateChangeHighToLowFromLow; }
			set { SetField(ref _MaxBreathStateChangeHighToLowFromLow, value, () => MaxBreathStateChangeHighToLowFromLow); }
		}

		private System.Int32 _MinBreathStateChangeLowToHighFromHigh;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinBreathStateChangeLowToHighFromHigh {
			get { return _MinBreathStateChangeLowToHighFromHigh; }
			set { SetField(ref _MinBreathStateChangeLowToHighFromHigh, value, () => MinBreathStateChangeLowToHighFromHigh); }
		}

		private System.Int32 _MaxBreathStateChangeLowToHighFromHigh;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MaxBreathStateChangeLowToHighFromHigh {
			get { return _MaxBreathStateChangeLowToHighFromHigh; }
			set { SetField(ref _MaxBreathStateChangeLowToHighFromHigh, value, () => MaxBreathStateChangeLowToHighFromHigh); }
		}

		private System.Int32 _MinBreathStateChangeExhaustedToIdleFromLow;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinBreathStateChangeExhaustedToIdleFromLow {
			get { return _MinBreathStateChangeExhaustedToIdleFromLow; }
			set { SetField(ref _MinBreathStateChangeExhaustedToIdleFromLow, value, () => MinBreathStateChangeExhaustedToIdleFromLow); }
		}

		private System.Int32 _MaxBreathStateChangeExhaustedToIdleFromLow;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MaxBreathStateChangeExhaustedToIdleFromLow {
			get { return _MaxBreathStateChangeExhaustedToIdleFromLow; }
			set { SetField(ref _MaxBreathStateChangeExhaustedToIdleFromLow, value, () => MaxBreathStateChangeExhaustedToIdleFromLow); }
		}

		private System.Int32 _MinBreathStateChangeExhaustedToIdleFromHigh;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinBreathStateChangeExhaustedToIdleFromHigh {
			get { return _MinBreathStateChangeExhaustedToIdleFromHigh; }
			set { SetField(ref _MinBreathStateChangeExhaustedToIdleFromHigh, value, () => MinBreathStateChangeExhaustedToIdleFromHigh); }
		}

		private System.Int32 _MaxBreathStateChangeExhaustedToIdleFromHigh;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MaxBreathStateChangeExhaustedToIdleFromHigh {
			get { return _MaxBreathStateChangeExhaustedToIdleFromHigh; }
			set { SetField(ref _MaxBreathStateChangeExhaustedToIdleFromHigh, value, () => MaxBreathStateChangeExhaustedToIdleFromHigh); }
		}

		private System.Int32 _MinBreathStateChangeLowToHighFromExhausted;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MinBreathStateChangeLowToHighFromExhausted {
			get { return _MinBreathStateChangeLowToHighFromExhausted; }
			set { SetField(ref _MinBreathStateChangeLowToHighFromExhausted, value, () => MinBreathStateChangeLowToHighFromExhausted); }
		}

		private System.Int32 _MaxBreathStateChangeLowToHighFromExhausted;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(50000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.Int32 MaxBreathStateChangeLowToHighFromExhausted {
			get { return _MaxBreathStateChangeLowToHighFromExhausted; }
			set { SetField(ref _MaxBreathStateChangeLowToHighFromExhausted, value, () => MaxBreathStateChangeLowToHighFromExhausted); }
		}

	}
}


