using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class AlarmSettings:  ModelBase {

		private ModelLib.ObjectRef _AlarmLoop;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AlarmLoop {
			get { return _AlarmLoop; }
			set { SetField(ref _AlarmLoop, value, () => AlarmLoop); }
		}

		private ModelLib.ObjectRef _AlarmDecayCurve;
		[ModelLib.CustomAttributes.Default("AMBIENCE_DEFAULT_ALARM_DECAY")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AlarmDecayCurve {
			get { return _AlarmDecayCurve; }
			set { SetField(ref _AlarmDecayCurve, value, () => AlarmDecayCurve); }
		}

		private System.UInt32 _StopDistance;
		[ModelLib.CustomAttributes.Default("1000")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt32 StopDistance {
			get { return _StopDistance; }
			set { SetField(ref _StopDistance, value, () => StopDistance); }
		}

		private ModelLib.ObjectRef _InteriorSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(InteriorSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef InteriorSettings {
			get { return _InteriorSettings; }
			set { SetField(ref _InteriorSettings, value, () => InteriorSettings); }
		}

		private System.String _BankName;
		public System.String BankName {
			get { return _BankName; }
			set { SetField(ref _BankName, value, () => BankName); }
		}

		public class CentrePositionDefinition: ModelBase {
			private System.Single _x;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single x {
				get { return _x; }
				set { SetField(ref _x, value, () => x); }
			}

			private System.Single _y;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single y {
				get { return _y; }
				set { SetField(ref _y, value, () => y); }
			}

			private System.Single _z;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single z {
				get { return _z; }
				set { SetField(ref _z, value, () => z); }
			}

		}
		private CentrePositionDefinition _CentrePosition;
		[ModelLib.CustomAttributes.Unit("vector3")]
		public CentrePositionDefinition CentrePosition {
			get { return _CentrePosition; }
			set { SetField(ref _CentrePosition, value, () => CentrePosition); }
		}

	}
}


