using ModelLib.CustomAttributes;

namespace model {

	public enum FakeGesture {
		[ModelLib.CustomAttributes.Display("Absolutely")]
		kAbsolutelyGesture,
		[ModelLib.CustomAttributes.Display("Agree")]
		kAgreeGesture,
		[ModelLib.CustomAttributes.Display("Come_Here_Front")]
		kComeHereFrontGesture,
		[ModelLib.CustomAttributes.Display("Come_Here_Left")]
		kComeHereLeftGesture,
		[ModelLib.CustomAttributes.Display("Damn")]
		kDamnGesture,
		[ModelLib.CustomAttributes.Display("Dont_Know")]
		kDontKnowGesture,
		[ModelLib.CustomAttributes.Display("Easy_Now")]
		kEasyNowGesture,
		[ModelLib.CustomAttributes.Display("Exactly")]
		kExactlyGesture,
		[ModelLib.CustomAttributes.Display("Forget_It")]
		kForgetItGesture,
		[ModelLib.CustomAttributes.Display("Good")]
		kGoodGesture,
		[ModelLib.CustomAttributes.Display("Hello")]
		kHelloGesture,
		[ModelLib.CustomAttributes.Display("Hey")]
		kHeyGesture,
		[ModelLib.CustomAttributes.Display("I_Dont_Think_So")]
		kIDontThinkSoGesture,
		[ModelLib.CustomAttributes.Display("If_You_Say_So")]
		kIfYouSaySoGesture,
		[ModelLib.CustomAttributes.Display("Ill_Do_It")]
		kIllDoItGesture,
		[ModelLib.CustomAttributes.Display("Im_Not_Sure")]
		kImNotSureGesture,
		[ModelLib.CustomAttributes.Display("Im_Sorry")]
		kImSorryGesture,
		[ModelLib.CustomAttributes.Display("Indicate_Back")]
		kIndicateBackGesture,
		[ModelLib.CustomAttributes.Display("Indicate_Left")]
		kIndicateLeftGesture,
		[ModelLib.CustomAttributes.Display("Indicate_Right")]
		kIndicateRightGesture,
		[ModelLib.CustomAttributes.Display("No_Chance")]
		kNoChanceGesture,
		[ModelLib.CustomAttributes.Display("Yes")]
		kYesGesture,
		[ModelLib.CustomAttributes.Display("You_Understand")]
		kYouUnderstandGesture
	}

}