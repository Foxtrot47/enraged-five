using ModelLib.CustomAttributes;

namespace model {

	public enum MusicConstraintTypes {
		[ModelLib.CustomAttributes.Display("Start")]
		kConstrainStart,
		[ModelLib.CustomAttributes.Display("End")]
		kConstrainEnd
	}

}