using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class InteriorSettings:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private ModelLib.Types.TriState _HasInteriorWalla;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState HasInteriorWalla {
			get { return _HasInteriorWalla; }
			set { SetField(ref _HasInteriorWalla, value, () => HasInteriorWalla); }
		}

		private ModelLib.ObjectRef _InteriorWallaSoundSet;
		[ModelLib.CustomAttributes.Default("INTERIOR_WALLA_STREAMED_DEFAULT")]
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef InteriorWallaSoundSet {
			get { return _InteriorWallaSoundSet; }
			set { SetField(ref _InteriorWallaSoundSet, value, () => InteriorWallaSoundSet); }
		}

		private ModelLib.ObjectRef _InteriorReflections;
		[ModelLib.CustomAttributes.AllowedType(typeof(ReflectionsSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef InteriorReflections {
			get { return _InteriorReflections; }
			set { SetField(ref _InteriorReflections, value, () => InteriorReflections); }
		}

		private ModelLib.Types.TriState _InhibitIdleMusic;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState InhibitIdleMusic {
			get { return _InhibitIdleMusic; }
			set { SetField(ref _InhibitIdleMusic, value, () => InhibitIdleMusic); }
		}

		private ModelLib.Types.TriState _BuildAllExtendedPaths;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState BuildAllExtendedPaths {
			get { return _BuildAllExtendedPaths; }
			set { SetField(ref _BuildAllExtendedPaths, value, () => BuildAllExtendedPaths); }
		}

		private ModelLib.Types.TriState _BuildInterInteriorPaths;
		[ModelLib.CustomAttributes.Default("yes")]
		public ModelLib.Types.TriState BuildInterInteriorPaths {
			get { return _BuildInterInteriorPaths; }
			set { SetField(ref _BuildInterInteriorPaths, value, () => BuildInterInteriorPaths); }
		}

		public class RoomDefinition: ModelBase {
			private ModelLib.ObjectRef _Ref;
			[ModelLib.CustomAttributes.AllowedType(typeof(InteriorRoom))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef Ref {
				get { return _Ref; }
				set { SetField(ref _Ref, value, () => Ref); }
			}

		}
		private ItemsObservableCollection<RoomDefinition> _Room = new ItemsObservableCollection<RoomDefinition>();
		[ModelLib.CustomAttributes.MaxSize(64)]
		[XmlElement("Room")]
		public ItemsObservableCollection<RoomDefinition> Room {
			get { return _Room; }
			set { SetField(ref _Room, value, () => Room); }
		}

	}
}


