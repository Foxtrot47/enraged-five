using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Scanner")]
	public class ScannerArea:  ModelBase {

		private ModelLib.ObjectRef _ConjunctiveSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ConjunctiveSound {
			get { return _ConjunctiveSound; }
			set { SetField(ref _ConjunctiveSound, value, () => ConjunctiveSound); }
		}

		public class ScannerAreaBitsDefinition: ModelBase {
			private ModelLib.Types.Bit _NeverPlayDirection;
			[ModelLib.CustomAttributes.Default("no")]
			public ModelLib.Types.Bit NeverPlayDirection {
				get { return _NeverPlayDirection; }
				set { SetField(ref _NeverPlayDirection, value, () => NeverPlayDirection); }
			}

		}
		private ScannerAreaBitsDefinition _ScannerAreaBits;
		public ScannerAreaBitsDefinition ScannerAreaBits {
			get { return _ScannerAreaBits; }
			set { SetField(ref _ScannerAreaBits, value, () => ScannerAreaBits); }
		}

	}
}


