using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class WrapperSound:  Sound{

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DefaultDropField]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		private System.UInt32 _LastPlayTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Hidden]
		public System.UInt32 LastPlayTime {
			get { return _LastPlayTime; }
			set { SetField(ref _LastPlayTime, value, () => LastPlayTime); }
		}

		private ModelLib.ObjectRef _FallbackSoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("FallbackSound")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef FallbackSoundRef {
			get { return _FallbackSoundRef; }
			set { SetField(ref _FallbackSoundRef, value, () => FallbackSoundRef); }
		}

		private System.UInt16 _MinRepeatTime;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Description("If set, the fallback sound will be used if the main sound has played within this time")]
		[ModelLib.CustomAttributes.DisplayGroup("FallbackSound")]
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt16 MinRepeatTime {
			get { return _MinRepeatTime; }
			set { SetField(ref _MinRepeatTime, value, () => MinRepeatTime); }
		}

		public class VariableRefsDefinition: ModelBase {
			private System.String _VR_Ref;
			[ModelLib.CustomAttributes.Unit("variable")]
			[ModelLib.CustomAttributes.Hidden]
			public System.String VR_Ref {
				get { return _VR_Ref; }
				set { SetField(ref _VR_Ref, value, () => VR_Ref); }
			}

			private ParameterDestinations _Destination;
			public ParameterDestinations Destination {
				get { return _Destination; }
				set { SetField(ref _Destination, value, () => Destination); }
			}

		}
		private ItemsObservableCollection<VariableRefsDefinition> _VariableRefs = new ItemsObservableCollection<VariableRefsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(8)]
		[ModelLib.CustomAttributes.Hidden]
		[XmlElement("VariableRefs")]
		public ItemsObservableCollection<VariableRefsDefinition> VariableRefs {
			get { return _VariableRefs; }
			set { SetField(ref _VariableRefs, value, () => VariableRefs); }
		}

	}
}


