using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Peds")]
	public class MeleeCombatSettings:  ModelBase {

		private ModelLib.ObjectRef _SwipeSound;
		[ModelLib.CustomAttributes.Default("OVERRIDE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SwipeSound {
			get { return _SwipeSound; }
			set { SetField(ref _SwipeSound, value, () => SwipeSound); }
		}

		private ModelLib.ObjectRef _GeneralHitSound;
		[ModelLib.CustomAttributes.Default("OVERRIDE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef GeneralHitSound {
			get { return _GeneralHitSound; }
			set { SetField(ref _GeneralHitSound, value, () => GeneralHitSound); }
		}

		private ModelLib.ObjectRef _PedHitSound;
		[ModelLib.CustomAttributes.Default("OVERRIDE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedHitSound {
			get { return _PedHitSound; }
			set { SetField(ref _PedHitSound, value, () => PedHitSound); }
		}

		private ModelLib.ObjectRef _PedResponseSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef PedResponseSound {
			get { return _PedResponseSound; }
			set { SetField(ref _PedResponseSound, value, () => PedResponseSound); }
		}

		private ModelLib.ObjectRef _HeadTakeDown;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HeadTakeDown {
			get { return _HeadTakeDown; }
			set { SetField(ref _HeadTakeDown, value, () => HeadTakeDown); }
		}

		private ModelLib.ObjectRef _BodyTakeDown;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BodyTakeDown {
			get { return _BodyTakeDown; }
			set { SetField(ref _BodyTakeDown, value, () => BodyTakeDown); }
		}

		private ModelLib.ObjectRef _SmallAnimalHitSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SmallAnimalHitSound {
			get { return _SmallAnimalHitSound; }
			set { SetField(ref _SmallAnimalHitSound, value, () => SmallAnimalHitSound); }
		}

		private ModelLib.ObjectRef _SmallAnimalResponseSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SmallAnimalResponseSound {
			get { return _SmallAnimalResponseSound; }
			set { SetField(ref _SmallAnimalResponseSound, value, () => SmallAnimalResponseSound); }
		}

		private ModelLib.ObjectRef _BigAnimalHitSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BigAnimalHitSound {
			get { return _BigAnimalHitSound; }
			set { SetField(ref _BigAnimalHitSound, value, () => BigAnimalHitSound); }
		}

		private ModelLib.ObjectRef _BigAnimalResponseSound;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BigAnimalResponseSound {
			get { return _BigAnimalResponseSound; }
			set { SetField(ref _BigAnimalResponseSound, value, () => BigAnimalResponseSound); }
		}

	}
}


