using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class SequentialSound:  Sound{

		private ModelLib.Types.TriState _OptimizePlayback;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState OptimizePlayback {
			get { return _OptimizePlayback; }
			set { SetField(ref _OptimizePlayback, value, () => OptimizePlayback); }
		}

		public class SoundRefDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundId;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef SoundId {
				get { return _SoundId; }
				set { SetField(ref _SoundId, value, () => SoundId); }
			}

			private System.String _ReferenceName;
			[ModelLib.CustomAttributes.Ignore]
			[ModelLib.CustomAttributes.Hidden]
			public System.String ReferenceName {
				get { return _ReferenceName; }
				set { SetField(ref _ReferenceName, value, () => ReferenceName); }
			}

		}
		private ItemsObservableCollection<SoundRefDefinition> _SoundRef = new ItemsObservableCollection<SoundRefDefinition>();
		[ModelLib.CustomAttributes.MaxSize(255)]
		[XmlElement("SoundRef")]
		public ItemsObservableCollection<SoundRefDefinition> SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

	}
}


