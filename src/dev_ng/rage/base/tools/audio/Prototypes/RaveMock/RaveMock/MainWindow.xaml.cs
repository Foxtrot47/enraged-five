﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.AvalonDock.Controls;
using Xceed.Wpf.AvalonDock.Layout;
using Xceed.Wpf.AvalonDock.Layout.Serialization;
using Xceed.Wpf.AvalonDock.Themes;

namespace RaveMock
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            init();
        }

        private SoundBrowser m_soundBrowser;
        private HierarchyBrowser2 m_hierarchyBrowser;
        private PropertiesEditor2 m_propertiesEditor;
        private AssetManager m_assetManager;
        private WaveBrowser m_waveBrowser;

        public void init()
        {
            this.m_hierarchyBrowser = new HierarchyBrowser2();
            this.MiddlePane.Children.Clear();
            this.MiddlePane.Children.Add(this.m_hierarchyBrowser.Content);

            // Setup properties editor
            this.m_propertiesEditor= new PropertiesEditor2();
            this.RightPane.Children.Clear();
            this.RightPane.Children.Add(this.m_propertiesEditor.Content);

            // Setup asset manager
            this.m_assetManager=new AssetManager();
            this.BottomPane.Children.Clear();
            this.BottomPane.Children.Add(this.m_assetManager.Content);
            //this.m_assetManager.Content.AddToLayout(this.dockManager, AnchorableShowStrategy.Bottom);
            //this.m_assetManager.Content.Show();

            // Setup wave browser & sound browser
            this.m_soundBrowser = new SoundBrowser(new SoundBrowser.editObjectDelegate(m_propertiesEditor.EditObject));
            this.m_waveBrowser=new WaveBrowser();
            this.LeftPane.Children.Clear();
            this.LeftPane.Children.Add(this.m_waveBrowser.Content);
            this.LeftPane.Children.Add(this.m_soundBrowser.Content);

            // Workaround for:System.ArgumentException: Invisible or disabled control cannot be activated
            // http://support.microsoft.com/kb/2686194
            //WinForms.Application.ThreadException += this.Application_ThreadException;

            var timer = new UXTimer();
            //this.BottomPane.Children.Add(timer.Content);
            timer.Content.AddToLayout(this.dockManager, AnchorableShowStrategy.Left);
            timer.Content.Float();

            //dockManager.Theme = new GenericTheme();

            LayoutAnchorableControl c = new LayoutAnchorableControl();
            //c.Model.issel
           
        }

        private void SaveLayout_OnClick(object sender, RoutedEventArgs e)
        {
            var serializer = new XmlLayoutSerializer(dockManager);
            using (var stream = new StreamWriter(@"C:\Users\ben.durrenberger\Documents\Visual Studio 2012\Projects\WpfApplication1\layout.xml"))
                serializer.Serialize(stream);
        }

        private void LoadLayout_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var serializer = new XmlLayoutSerializer(dockManager);
                serializer.LayoutSerializationCallback += (s, args) =>
                {
                    string contentid = args.Model.ContentId;
                    object content = args.Content;
                    object result = Application.Current.MainWindow.FindName(args.Model.ContentId);
                    //args.Content = Application.Current.MainWindow.FindName(args.Model.ContentId);
                };
                using (var stream = new StreamReader(@"C:\Users\ben.durrenberger\Documents\Visual Studio 2012\Projects\WpfApplication1\layout.xml"))
                    serializer.Deserialize(stream);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Layout could not be loaded", "Error", MessageBoxButton.OK);
            }
        }
    }
}
