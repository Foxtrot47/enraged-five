using ModelLib.CustomAttributes;

namespace model {

	public enum VehicleVolumeCategory {
		[ModelLib.CustomAttributes.Display("VeryLoud")]
		VEHICLE_VOLUME_VERY_LOUD,
		[ModelLib.CustomAttributes.Display("Loud")]
		VEHICLE_VOLUME_LOUD,
		[ModelLib.CustomAttributes.Display("Normal")]
		VEHICLE_VOLUME_NORMAL,
		[ModelLib.CustomAttributes.Display("Quiet")]
		VEHICLE_VOLUME_QUIET,
		[ModelLib.CustomAttributes.Display("VeryQuiet")]
		VEHICLE_VOLUME_VERY_QUIET
	}

}