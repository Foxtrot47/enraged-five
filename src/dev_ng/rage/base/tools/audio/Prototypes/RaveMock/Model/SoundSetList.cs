using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.FactoryCodeGenerated(false)]
	[ModelLib.CustomAttributes.Group("Util")]
	public class SoundSetList:  ModelBase {

		public class SoundSetDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundSetHash;
			[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef SoundSetHash {
				get { return _SoundSetHash; }
				set { SetField(ref _SoundSetHash, value, () => SoundSetHash); }
			}

		}
		private ItemsObservableCollection<SoundSetDefinition> _SoundSet = new ItemsObservableCollection<SoundSetDefinition>();
		[ModelLib.CustomAttributes.MaxSize(65535, typeof(System.UInt32))]
		[XmlElement("SoundSet")]
		public ItemsObservableCollection<SoundSetDefinition> SoundSet {
			get { return _SoundSet; }
			set { SetField(ref _SoundSet, value, () => SoundSet); }
		}

	}
}


