using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Logic Sound Types")]
	public class SwitchSound:  Sound{

		private System.String _Variable;
		[ModelLib.CustomAttributes.Description("Variable containing the child sound index to play")]
		[ModelLib.CustomAttributes.Unit("variable")]
		public System.String Variable {
			get { return _Variable; }
			set { SetField(ref _Variable, value, () => Variable); }
		}

		private ModelLib.Types.TriState _ScaleInput;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.Description("If yes then the input variable is assumed to be in range [0.0,1.0] and will be scaled by number of children when choosing the child to play")]
		public ModelLib.Types.TriState ScaleInput {
			get { return _ScaleInput; }
			set { SetField(ref _ScaleInput, value, () => ScaleInput); }
		}

		public class SoundRefDefinition: ModelBase {
			private ModelLib.ObjectRef _SoundId;
			[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
			public ModelLib.ObjectRef SoundId {
				get { return _SoundId; }
				set { SetField(ref _SoundId, value, () => SoundId); }
			}

		}
		private ItemsObservableCollection<SoundRefDefinition> _SoundRef = new ItemsObservableCollection<SoundRefDefinition>();
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("SoundRef")]
		public ItemsObservableCollection<SoundRefDefinition> SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

	}
}


