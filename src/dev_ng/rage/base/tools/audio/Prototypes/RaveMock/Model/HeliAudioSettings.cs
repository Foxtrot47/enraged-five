using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Vehicles")]
	public class HeliAudioSettings:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		private ModelLib.ObjectRef _RotorLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RotorLoop {
			get { return _RotorLoop; }
			set { SetField(ref _RotorLoop, value, () => RotorLoop); }
		}

		private ModelLib.ObjectRef _RearRotorLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RearRotorLoop {
			get { return _RearRotorLoop; }
			set { SetField(ref _RearRotorLoop, value, () => RearRotorLoop); }
		}

		private ModelLib.ObjectRef _ExhaustLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustLoop {
			get { return _ExhaustLoop; }
			set { SetField(ref _ExhaustLoop, value, () => ExhaustLoop); }
		}

		private ModelLib.ObjectRef _BankingLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankingLoop {
			get { return _BankingLoop; }
			set { SetField(ref _BankingLoop, value, () => BankingLoop); }
		}

		private ModelLib.ObjectRef _CabinToneLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CabinToneLoop {
			get { return _CabinToneLoop; }
			set { SetField(ref _CabinToneLoop, value, () => CabinToneLoop); }
		}

		private System.Single _ThrottleSmoothRate;
		[ModelLib.CustomAttributes.Default("0.005")]
		[ModelLib.CustomAttributes.Description("Currently unused")]
		public System.Single ThrottleSmoothRate {
			get { return _ThrottleSmoothRate; }
			set { SetField(ref _ThrottleSmoothRate, value, () => ThrottleSmoothRate); }
		}

		private ModelLib.ObjectRef _BankAngleVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankAngleVolumeCurve {
			get { return _BankAngleVolumeCurve; }
			set { SetField(ref _BankAngleVolumeCurve, value, () => BankAngleVolumeCurve); }
		}

		private ModelLib.ObjectRef _BankThrottleVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankThrottleVolumeCurve {
			get { return _BankThrottleVolumeCurve; }
			set { SetField(ref _BankThrottleVolumeCurve, value, () => BankThrottleVolumeCurve); }
		}

		private ModelLib.ObjectRef _BankThrottlePitchCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankThrottlePitchCurve {
			get { return _BankThrottlePitchCurve; }
			set { SetField(ref _BankThrottlePitchCurve, value, () => BankThrottlePitchCurve); }
		}

		private ModelLib.ObjectRef _RotorThrottleVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RotorThrottleVolumeCurve {
			get { return _RotorThrottleVolumeCurve; }
			set { SetField(ref _RotorThrottleVolumeCurve, value, () => RotorThrottleVolumeCurve); }
		}

		private ModelLib.ObjectRef _RotorThrottlePitchCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RotorThrottlePitchCurve {
			get { return _RotorThrottlePitchCurve; }
			set { SetField(ref _RotorThrottlePitchCurve, value, () => RotorThrottlePitchCurve); }
		}

		private ModelLib.ObjectRef _RotorThrottleGapCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RotorThrottleGapCurve {
			get { return _RotorThrottleGapCurve; }
			set { SetField(ref _RotorThrottleGapCurve, value, () => RotorThrottleGapCurve); }
		}

		private ModelLib.ObjectRef _RearRotorThrottleVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RearRotorThrottleVolumeCurve {
			get { return _RearRotorThrottleVolumeCurve; }
			set { SetField(ref _RearRotorThrottleVolumeCurve, value, () => RearRotorThrottleVolumeCurve); }
		}

		private ModelLib.ObjectRef _ExhaustThrottleVolumeCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustThrottleVolumeCurve {
			get { return _ExhaustThrottleVolumeCurve; }
			set { SetField(ref _ExhaustThrottleVolumeCurve, value, () => ExhaustThrottleVolumeCurve); }
		}

		private ModelLib.ObjectRef _ExhaustThrottlePitchCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustThrottlePitchCurve {
			get { return _ExhaustThrottlePitchCurve; }
			set { SetField(ref _ExhaustThrottlePitchCurve, value, () => ExhaustThrottlePitchCurve); }
		}

		private System.UInt16 _RotorConeFrontAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 RotorConeFrontAngle {
			get { return _RotorConeFrontAngle; }
			set { SetField(ref _RotorConeFrontAngle, value, () => RotorConeFrontAngle); }
		}

		private System.UInt16 _RotorConeRearAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 RotorConeRearAngle {
			get { return _RotorConeRearAngle; }
			set { SetField(ref _RotorConeRearAngle, value, () => RotorConeRearAngle); }
		}

		private System.Int16 _RotorConeAtten;
		[ModelLib.CustomAttributes.Default("-1500")]
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 RotorConeAtten {
			get { return _RotorConeAtten; }
			set { SetField(ref _RotorConeAtten, value, () => RotorConeAtten); }
		}

		private System.UInt16 _RearRotorConeFrontAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 RearRotorConeFrontAngle {
			get { return _RearRotorConeFrontAngle; }
			set { SetField(ref _RearRotorConeFrontAngle, value, () => RearRotorConeFrontAngle); }
		}

		private System.UInt16 _RearRotorConeRearAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 RearRotorConeRearAngle {
			get { return _RearRotorConeRearAngle; }
			set { SetField(ref _RearRotorConeRearAngle, value, () => RearRotorConeRearAngle); }
		}

		private System.Int16 _RearRotorConeAtten;
		[ModelLib.CustomAttributes.Default("-1500")]
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 RearRotorConeAtten {
			get { return _RearRotorConeAtten; }
			set { SetField(ref _RearRotorConeAtten, value, () => RearRotorConeAtten); }
		}

		private System.UInt16 _ExhaustConeFrontAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 ExhaustConeFrontAngle {
			get { return _ExhaustConeFrontAngle; }
			set { SetField(ref _ExhaustConeFrontAngle, value, () => ExhaustConeFrontAngle); }
		}

		private System.UInt16 _ExhaustConeRearAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 ExhaustConeRearAngle {
			get { return _ExhaustConeRearAngle; }
			set { SetField(ref _ExhaustConeRearAngle, value, () => ExhaustConeRearAngle); }
		}

		private System.Int16 _ExhaustConeAtten;
		[ModelLib.CustomAttributes.Default("-1500")]
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 ExhaustConeAtten {
			get { return _ExhaustConeAtten; }
			set { SetField(ref _ExhaustConeAtten, value, () => ExhaustConeAtten); }
		}

		private ModelLib.ObjectRef _Filter1ThrottleResonanceCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Filter1ThrottleResonanceCurve {
			get { return _Filter1ThrottleResonanceCurve; }
			set { SetField(ref _Filter1ThrottleResonanceCurve, value, () => Filter1ThrottleResonanceCurve); }
		}

		private ModelLib.ObjectRef _Filter2ThrottleResonanceCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef Filter2ThrottleResonanceCurve {
			get { return _Filter2ThrottleResonanceCurve; }
			set { SetField(ref _Filter2ThrottleResonanceCurve, value, () => Filter2ThrottleResonanceCurve); }
		}

		private ModelLib.ObjectRef _BankingResonanceCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BankingResonanceCurve {
			get { return _BankingResonanceCurve; }
			set { SetField(ref _BankingResonanceCurve, value, () => BankingResonanceCurve); }
		}

		private ModelLib.ObjectRef _RotorVolumeStartupCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Startup")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RotorVolumeStartupCurve {
			get { return _RotorVolumeStartupCurve; }
			set { SetField(ref _RotorVolumeStartupCurve, value, () => RotorVolumeStartupCurve); }
		}

		private ModelLib.ObjectRef _BladeVolumeStartupCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Startup")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef BladeVolumeStartupCurve {
			get { return _BladeVolumeStartupCurve; }
			set { SetField(ref _BladeVolumeStartupCurve, value, () => BladeVolumeStartupCurve); }
		}

		private ModelLib.ObjectRef _RotorPitchStartupCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Startup")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RotorPitchStartupCurve {
			get { return _RotorPitchStartupCurve; }
			set { SetField(ref _RotorPitchStartupCurve, value, () => RotorPitchStartupCurve); }
		}

		private ModelLib.ObjectRef _RearRotorVolumeStartupCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Startup")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RearRotorVolumeStartupCurve {
			get { return _RearRotorVolumeStartupCurve; }
			set { SetField(ref _RearRotorVolumeStartupCurve, value, () => RearRotorVolumeStartupCurve); }
		}

		private ModelLib.ObjectRef _ExhaustVolumeStartupCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Startup")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustVolumeStartupCurve {
			get { return _ExhaustVolumeStartupCurve; }
			set { SetField(ref _ExhaustVolumeStartupCurve, value, () => ExhaustVolumeStartupCurve); }
		}

		private ModelLib.ObjectRef _RotorGapStartupCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Startup")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RotorGapStartupCurve {
			get { return _RotorGapStartupCurve; }
			set { SetField(ref _RotorGapStartupCurve, value, () => RotorGapStartupCurve); }
		}

		private ModelLib.ObjectRef _StartUpOneShot;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Startup")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef StartUpOneShot {
			get { return _StartUpOneShot; }
			set { SetField(ref _StartUpOneShot, value, () => StartUpOneShot); }
		}

		private System.UInt16 _BladeConeUpAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 BladeConeUpAngle {
			get { return _BladeConeUpAngle; }
			set { SetField(ref _BladeConeUpAngle, value, () => BladeConeUpAngle); }
		}

		private System.UInt16 _BladeConeDownAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 BladeConeDownAngle {
			get { return _BladeConeDownAngle; }
			set { SetField(ref _BladeConeDownAngle, value, () => BladeConeDownAngle); }
		}

		private System.Int16 _BladeConeAtten;
		[ModelLib.CustomAttributes.Default("-1500")]
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 BladeConeAtten {
			get { return _BladeConeAtten; }
			set { SetField(ref _BladeConeAtten, value, () => BladeConeAtten); }
		}

		private System.UInt16 _ThumpConeUpAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 ThumpConeUpAngle {
			get { return _ThumpConeUpAngle; }
			set { SetField(ref _ThumpConeUpAngle, value, () => ThumpConeUpAngle); }
		}

		private System.UInt16 _ThumpConeDownAngle;
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(180)]
		[ModelLib.CustomAttributes.Min(0)]
		public System.UInt16 ThumpConeDownAngle {
			get { return _ThumpConeDownAngle; }
			set { SetField(ref _ThumpConeDownAngle, value, () => ThumpConeDownAngle); }
		}

		private System.Int16 _ThumpConeAtten;
		[ModelLib.CustomAttributes.Default("-1500")]
		[ModelLib.CustomAttributes.DisplayGroup("Cones")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 ThumpConeAtten {
			get { return _ThumpConeAtten; }
			set { SetField(ref _ThumpConeAtten, value, () => ThumpConeAtten); }
		}

		private ModelLib.ObjectRef _ScannerMake;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScannerMake {
			get { return _ScannerMake; }
			set { SetField(ref _ScannerMake, value, () => ScannerMake); }
		}

		private ModelLib.ObjectRef _ScannerModel;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScannerModel {
			get { return _ScannerModel; }
			set { SetField(ref _ScannerModel, value, () => ScannerModel); }
		}

		private ModelLib.ObjectRef _ScannerCategory;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScannerCategory {
			get { return _ScannerCategory; }
			set { SetField(ref _ScannerCategory, value, () => ScannerCategory); }
		}

		private ModelLib.ObjectRef _ScannerVehicleSettings;
		[ModelLib.CustomAttributes.AllowedType(typeof(ScannerVehicleParams))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ScannerVehicleSettings {
			get { return _ScannerVehicleSettings; }
			set { SetField(ref _ScannerVehicleSettings, value, () => ScannerVehicleSettings); }
		}

		private RadioType _RadioType;
		[ModelLib.CustomAttributes.Default("RADIO_TYPE_NORMAL")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public RadioType RadioType {
			get { return _RadioType; }
			set { SetField(ref _RadioType, value, () => RadioType); }
		}

		private RadioGenre _RadioGenre;
		[ModelLib.CustomAttributes.Default("RADIO_GENRE_UNSPECIFIED")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public RadioGenre RadioGenre {
			get { return _RadioGenre; }
			set { SetField(ref _RadioGenre, value, () => RadioGenre); }
		}

		private ModelLib.Types.TriState _DisableAmbientRadio;
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public ModelLib.Types.TriState DisableAmbientRadio {
			get { return _DisableAmbientRadio; }
			set { SetField(ref _DisableAmbientRadio, value, () => DisableAmbientRadio); }
		}

		private ModelLib.ObjectRef _DoorOpen;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorOpen {
			get { return _DoorOpen; }
			set { SetField(ref _DoorOpen, value, () => DoorOpen); }
		}

		private ModelLib.ObjectRef _DoorClose;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorClose {
			get { return _DoorClose; }
			set { SetField(ref _DoorClose, value, () => DoorClose); }
		}

		private ModelLib.ObjectRef _DoorLimit;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DoorLimit {
			get { return _DoorLimit; }
			set { SetField(ref _DoorLimit, value, () => DoorLimit); }
		}

		private ModelLib.ObjectRef _DamageLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DamageLoop {
			get { return _DamageLoop; }
			set { SetField(ref _DamageLoop, value, () => DamageLoop); }
		}

		private ModelLib.ObjectRef _RotorSpeedToTriggerSpeedCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RotorSpeedToTriggerSpeedCurve {
			get { return _RotorSpeedToTriggerSpeedCurve; }
			set { SetField(ref _RotorSpeedToTriggerSpeedCurve, value, () => RotorSpeedToTriggerSpeedCurve); }
		}

		private System.Int16 _RotorVolumePostSubmix;
		[ModelLib.CustomAttributes.Default("0")]
		[ModelLib.CustomAttributes.Max(10000)]
		[ModelLib.CustomAttributes.Min(-10000)]
		[ModelLib.CustomAttributes.Unit("mB")]
		public System.Int16 RotorVolumePostSubmix {
			get { return _RotorVolumePostSubmix; }
			set { SetField(ref _RotorVolumePostSubmix, value, () => RotorVolumePostSubmix); }
		}

		private ModelLib.ObjectRef _EngineSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSynthDef {
			get { return _EngineSynthDef; }
			set { SetField(ref _EngineSynthDef, value, () => EngineSynthDef); }
		}

		private ModelLib.ObjectRef _EngineSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSynthPreset {
			get { return _EngineSynthPreset; }
			set { SetField(ref _EngineSynthPreset, value, () => EngineSynthPreset); }
		}

		private ModelLib.ObjectRef _ExhaustSynthDef;
		[ModelLib.CustomAttributes.AllowedType(typeof(ModularSynth))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSynthDef {
			get { return _ExhaustSynthDef; }
			set { SetField(ref _ExhaustSynthDef, value, () => ExhaustSynthDef); }
		}

		private ModelLib.ObjectRef _ExhaustSynthPreset;
		[ModelLib.CustomAttributes.AllowedType(typeof(Preset))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSynthPreset {
			get { return _ExhaustSynthPreset; }
			set { SetField(ref _ExhaustSynthPreset, value, () => ExhaustSynthPreset); }
		}

		private ModelLib.ObjectRef _EngineSubmixVoice;
		[ModelLib.CustomAttributes.Default("DEFAULT_HELI_ENGINE_SUBMIX_CONTROL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineSubmixVoice {
			get { return _EngineSubmixVoice; }
			set { SetField(ref _EngineSubmixVoice, value, () => EngineSubmixVoice); }
		}

		private ModelLib.ObjectRef _ExhaustSubmixVoice;
		[ModelLib.CustomAttributes.Default("DEFAULT_HELI_EXHAUST_SUBMIX_CONTROL")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("Filters")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustSubmixVoice {
			get { return _ExhaustSubmixVoice; }
			set { SetField(ref _ExhaustSubmixVoice, value, () => ExhaustSubmixVoice); }
		}

		private ModelLib.ObjectRef _RotorLowFreqLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RotorLowFreqLoop {
			get { return _RotorLowFreqLoop; }
			set { SetField(ref _RotorLowFreqLoop, value, () => RotorLowFreqLoop); }
		}

		private ModelLib.ObjectRef _ExhaustPitchStartupCurve;
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Startup")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ExhaustPitchStartupCurve {
			get { return _ExhaustPitchStartupCurve; }
			set { SetField(ref _ExhaustPitchStartupCurve, value, () => ExhaustPitchStartupCurve); }
		}

		private ModelLib.Types.TriState _HasMissileLockSystem;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public ModelLib.Types.TriState HasMissileLockSystem {
			get { return _HasMissileLockSystem; }
			set { SetField(ref _HasMissileLockSystem, value, () => HasMissileLockSystem); }
		}

		private ModelLib.ObjectRef _VehicleCollisions;
		[ModelLib.CustomAttributes.Default("VEHICLE_COLLISION_HELI")]
		[ModelLib.CustomAttributes.AllowedType(typeof(VehicleCollisionSettings))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef VehicleCollisions {
			get { return _VehicleCollisions; }
			set { SetField(ref _VehicleCollisions, value, () => VehicleCollisions); }
		}

		private ModelLib.ObjectRef _FireAudio;
		[ModelLib.CustomAttributes.Default("VEH_FIRE_SOUNDSET")]
		[ModelLib.CustomAttributes.AllowedType(typeof(SoundSet))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef FireAudio {
			get { return _FireAudio; }
			set { SetField(ref _FireAudio, value, () => FireAudio); }
		}

		private ModelLib.ObjectRef _DistantLoop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DistantLoop {
			get { return _DistantLoop; }
			set { SetField(ref _DistantLoop, value, () => DistantLoop); }
		}

		private ModelLib.ObjectRef _SecondaryDoorStartOpen;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SecondaryDoorStartOpen {
			get { return _SecondaryDoorStartOpen; }
			set { SetField(ref _SecondaryDoorStartOpen, value, () => SecondaryDoorStartOpen); }
		}

		private ModelLib.ObjectRef _SecondaryDoorStartClose;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SecondaryDoorStartClose {
			get { return _SecondaryDoorStartClose; }
			set { SetField(ref _SecondaryDoorStartClose, value, () => SecondaryDoorStartClose); }
		}

		private ModelLib.ObjectRef _SecondaryDoorClose;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SecondaryDoorClose {
			get { return _SecondaryDoorClose; }
			set { SetField(ref _SecondaryDoorClose, value, () => SecondaryDoorClose); }
		}

		private ModelLib.ObjectRef _SecondaryDoorLimit;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SecondaryDoorLimit {
			get { return _SecondaryDoorLimit; }
			set { SetField(ref _SecondaryDoorLimit, value, () => SecondaryDoorLimit); }
		}

		private ModelLib.ObjectRef _SuspensionUp;
		[ModelLib.CustomAttributes.Default("SUSPENSION_UP")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.Display("Up Sound")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SuspensionUp {
			get { return _SuspensionUp; }
			set { SetField(ref _SuspensionUp, value, () => SuspensionUp); }
		}

		private ModelLib.ObjectRef _SuspensionDown;
		[ModelLib.CustomAttributes.Default("SUSPENSION_DOWN")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.Display("Down Sound")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SuspensionDown {
			get { return _SuspensionDown; }
			set { SetField(ref _SuspensionDown, value, () => SuspensionDown); }
		}

		private System.Single _MinSuspCompThresh;
		[ModelLib.CustomAttributes.Default("0.4")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		public System.Single MinSuspCompThresh {
			get { return _MinSuspCompThresh; }
			set { SetField(ref _MinSuspCompThresh, value, () => MinSuspCompThresh); }
		}

		private System.Single _MaxSuspCompThres;
		[ModelLib.CustomAttributes.Default("1.0")]
		[ModelLib.CustomAttributes.DisplayGroup("Suspension")]
		public System.Single MaxSuspCompThres {
			get { return _MaxSuspCompThres; }
			set { SetField(ref _MaxSuspCompThres, value, () => MaxSuspCompThres); }
		}

		private ModelLib.ObjectRef _DamageOneShots;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DamageOneShots {
			get { return _DamageOneShots; }
			set { SetField(ref _DamageOneShots, value, () => DamageOneShots); }
		}

		private ModelLib.ObjectRef _DamageWarning;
		[ModelLib.CustomAttributes.Default("HELI_DAMAGE_WARNING")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DamageWarning {
			get { return _DamageWarning; }
			set { SetField(ref _DamageWarning, value, () => DamageWarning); }
		}

		private ModelLib.ObjectRef _TailBreak;
		[ModelLib.CustomAttributes.Default("HELI_TAIL_BREAK")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef TailBreak {
			get { return _TailBreak; }
			set { SetField(ref _TailBreak, value, () => TailBreak); }
		}

		private ModelLib.ObjectRef _RotorBreak;
		[ModelLib.CustomAttributes.Default("HELI_ROTOR_BREAK")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RotorBreak {
			get { return _RotorBreak; }
			set { SetField(ref _RotorBreak, value, () => RotorBreak); }
		}

		private ModelLib.ObjectRef _RearRotorBreak;
		[ModelLib.CustomAttributes.Default("HELI_REAR_ROTOR_BREAK")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef RearRotorBreak {
			get { return _RearRotorBreak; }
			set { SetField(ref _RearRotorBreak, value, () => RearRotorBreak); }
		}

		private ModelLib.ObjectRef _CableDeploy;
		[ModelLib.CustomAttributes.Default("HELICOPTER_CABLE_DEPLOY")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef CableDeploy {
			get { return _CableDeploy; }
			set { SetField(ref _CableDeploy, value, () => CableDeploy); }
		}

		private ModelLib.ObjectRef _HardScrape;
		[ModelLib.CustomAttributes.Default("MULTI_SCRAPE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HardScrape {
			get { return _HardScrape; }
			set { SetField(ref _HardScrape, value, () => HardScrape); }
		}

		private ModelLib.ObjectRef _EngineCooling;
		[ModelLib.CustomAttributes.Default("HEAT_STRESS_HEAT_TICK_LOOP")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef EngineCooling {
			get { return _EngineCooling; }
			set { SetField(ref _EngineCooling, value, () => EngineCooling); }
		}

		private ModelLib.ObjectRef _AltitudeWarning;
		[ModelLib.CustomAttributes.Default("ALTITUDE_WARNING")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef AltitudeWarning {
			get { return _AltitudeWarning; }
			set { SetField(ref _AltitudeWarning, value, () => AltitudeWarning); }
		}

		private ModelLib.ObjectRef _HealthToDamageVolumeCurve;
		[ModelLib.CustomAttributes.Default("HELI_DAMAGE_VOLUME_CURVE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HealthToDamageVolumeCurve {
			get { return _HealthToDamageVolumeCurve; }
			set { SetField(ref _HealthToDamageVolumeCurve, value, () => HealthToDamageVolumeCurve); }
		}

		private ModelLib.ObjectRef _HealthBelow600ToDamageVolumeCurve;
		[ModelLib.CustomAttributes.Default("HELI_DAMAGE_VOLUME_CURVE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef HealthBelow600ToDamageVolumeCurve {
			get { return _HealthBelow600ToDamageVolumeCurve; }
			set { SetField(ref _HealthBelow600ToDamageVolumeCurve, value, () => HealthBelow600ToDamageVolumeCurve); }
		}

		private ModelLib.ObjectRef _DamageBelow600Loop;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef DamageBelow600Loop {
			get { return _DamageBelow600Loop; }
			set { SetField(ref _DamageBelow600Loop, value, () => DamageBelow600Loop); }
		}

		private ModelLib.Types.TriState _AircraftWarningVoiceIsMale;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		public ModelLib.Types.TriState AircraftWarningVoiceIsMale {
			get { return _AircraftWarningVoiceIsMale; }
			set { SetField(ref _AircraftWarningVoiceIsMale, value, () => AircraftWarningVoiceIsMale); }
		}

		private System.Single _AircraftWarningSeriousDamageThresh;
		[ModelLib.CustomAttributes.Default("0.003")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(-1000.0)]
		public System.Single AircraftWarningSeriousDamageThresh {
			get { return _AircraftWarningSeriousDamageThresh; }
			set { SetField(ref _AircraftWarningSeriousDamageThresh, value, () => AircraftWarningSeriousDamageThresh); }
		}

		private System.Single _AircraftWarningCriticalDamageThresh;
		[ModelLib.CustomAttributes.Default("0.003")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(-1000.0)]
		public System.Single AircraftWarningCriticalDamageThresh {
			get { return _AircraftWarningCriticalDamageThresh; }
			set { SetField(ref _AircraftWarningCriticalDamageThresh, value, () => AircraftWarningCriticalDamageThresh); }
		}

		private System.Single _AircraftWarningMaxSpeed;
		[ModelLib.CustomAttributes.Default("100.0")]
		[ModelLib.CustomAttributes.DisplayGroup("OtherAudio")]
		[ModelLib.CustomAttributes.Max(500.0)]
		[ModelLib.CustomAttributes.Min(10.0)]
		public System.Single AircraftWarningMaxSpeed {
			get { return _AircraftWarningMaxSpeed; }
			set { SetField(ref _AircraftWarningMaxSpeed, value, () => AircraftWarningMaxSpeed); }
		}

		private ModelLib.ObjectRef _SimpleSoundForLoading;
		[ModelLib.CustomAttributes.Default("NULL_SOUND")]
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef SimpleSoundForLoading {
			get { return _SimpleSoundForLoading; }
			set { SetField(ref _SimpleSoundForLoading, value, () => SimpleSoundForLoading); }
		}

	}
}


