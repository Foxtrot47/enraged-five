using ModelLib.CustomAttributes;

namespace model {

	public enum RadioGenre {
		[ModelLib.CustomAttributes.Display("Off")]
		RADIO_GENRE_OFF,
		[ModelLib.CustomAttributes.Display("Rock")]
		RADIO_GENRE_MODERN_ROCK,
		[ModelLib.CustomAttributes.Display("UNUSED_1")]
		RADIO_GENRE_CLASSIC_ROCK,
		[ModelLib.CustomAttributes.Display("Pop")]
		RADIO_GENRE_POP,
		[ModelLib.CustomAttributes.Display("Hip Hop")]
		RADIO_GENRE_MODERN_HIPHOP,
		[ModelLib.CustomAttributes.Display("UNUSED_2")]
		RADIO_GENRE_CLASSIC_HIPHOP,
		[ModelLib.CustomAttributes.Display("Punk")]
		RADIO_GENRE_PUNK,
		[ModelLib.CustomAttributes.Display("Left Wing Talk Radio")]
		RADIO_GENRE_LEFT_WING_TALK,
		[ModelLib.CustomAttributes.Display("Right Wing Talk Radio")]
		RADIO_GENRE_RIGHT_WING_TALK,
		[ModelLib.CustomAttributes.Display("Country")]
		RADIO_GENRE_COUNTRY,
		[ModelLib.CustomAttributes.Display("Dance")]
		RADIO_GENRE_DANCE,
		[ModelLib.CustomAttributes.Display("Mexican")]
		RADIO_GENRE_MEXICAN,
		[ModelLib.CustomAttributes.Display("Reggae")]
		RADIO_GENRE_REGGAE,
		[ModelLib.CustomAttributes.Display("Jazz")]
		RADIO_GENRE_JAZZ,
		[ModelLib.CustomAttributes.Display("Motown and Funk")]
		RADIO_GENRE_MOTOWN,
		[ModelLib.CustomAttributes.Display("UNUSED_3")]
		RADIO_GENRE_SURF,
		[ModelLib.CustomAttributes.Display("Unspecified")]
		RADIO_GENRE_UNSPECIFIED
	}

}