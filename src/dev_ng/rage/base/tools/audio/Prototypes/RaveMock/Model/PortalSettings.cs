using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Environment")]
	public class PortalSettings:  ModelBase {

		private System.Single _MaxOcclusion;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(1.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single MaxOcclusion {
			get { return _MaxOcclusion; }
			set { SetField(ref _MaxOcclusion, value, () => MaxOcclusion); }
		}

	}
}


