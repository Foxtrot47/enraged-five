using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Ambience")]
	public class ShoreLineOceanAudioSettings:  ShoreLineAudioSettings{

		private OceanWaterType _OceanType;
		[ModelLib.CustomAttributes.Default("AUD_OCEAN_TYPE_BEACH")]
		public OceanWaterType OceanType {
			get { return _OceanType; }
			set { SetField(ref _OceanType, value, () => OceanType); }
		}

		private OceanWaterDirection _OceanDirection;
		[ModelLib.CustomAttributes.Default("AUD_OCEAN_DIR_NORTH")]
		public OceanWaterDirection OceanDirection {
			get { return _OceanDirection; }
			set { SetField(ref _OceanDirection, value, () => OceanDirection); }
		}

		private ModelLib.Types.TriState _WaveDetection;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.DisplayGroup("WaveDetection")]
		public ModelLib.Types.TriState WaveDetection {
			get { return _WaveDetection; }
			set { SetField(ref _WaveDetection, value, () => WaveDetection); }
		}

		private ModelLib.ObjectRef _NextShoreline;
		[ModelLib.CustomAttributes.AllowedType(typeof(ShoreLineOceanAudioSettings))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef NextShoreline {
			get { return _NextShoreline; }
			set { SetField(ref _NextShoreline, value, () => NextShoreline); }
		}

		private System.Single _WaveStartDPDistance;
		[ModelLib.CustomAttributes.Default("10.0")]
		[ModelLib.CustomAttributes.DisplayGroup("WaveDetection")]
		[ModelLib.CustomAttributes.Max(2400.0)]
		[ModelLib.CustomAttributes.Min(-2400.0)]
		public System.Single WaveStartDPDistance {
			get { return _WaveStartDPDistance; }
			set { SetField(ref _WaveStartDPDistance, value, () => WaveStartDPDistance); }
		}

		private System.Single _WaveStartHeight;
		[ModelLib.CustomAttributes.Default("0.5")]
		[ModelLib.CustomAttributes.DisplayGroup("WaveDetection")]
		[ModelLib.CustomAttributes.Max(1000.0)]
		[ModelLib.CustomAttributes.Min(-1000.0)]
		public System.Single WaveStartHeight {
			get { return _WaveStartHeight; }
			set { SetField(ref _WaveStartHeight, value, () => WaveStartHeight); }
		}

		private System.Single _WaveBreaksDPDistance;
		[ModelLib.CustomAttributes.Default("5.0")]
		[ModelLib.CustomAttributes.DisplayGroup("WaveDetection")]
		[ModelLib.CustomAttributes.Max(2400.0)]
		[ModelLib.CustomAttributes.Min(-2400.0)]
		public System.Single WaveBreaksDPDistance {
			get { return _WaveBreaksDPDistance; }
			set { SetField(ref _WaveBreaksDPDistance, value, () => WaveBreaksDPDistance); }
		}

		private System.Single _WaveBreaksHeight;
		[ModelLib.CustomAttributes.Default("0.65")]
		[ModelLib.CustomAttributes.DisplayGroup("WaveDetection")]
		[ModelLib.CustomAttributes.Max(1000.0)]
		[ModelLib.CustomAttributes.Min(-1000.0)]
		public System.Single WaveBreaksHeight {
			get { return _WaveBreaksHeight; }
			set { SetField(ref _WaveBreaksHeight, value, () => WaveBreaksHeight); }
		}

		private System.Single _WaveEndDPDistance;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.DisplayGroup("WaveDetection")]
		[ModelLib.CustomAttributes.Max(2400.0)]
		[ModelLib.CustomAttributes.Min(-2400.0)]
		public System.Single WaveEndDPDistance {
			get { return _WaveEndDPDistance; }
			set { SetField(ref _WaveEndDPDistance, value, () => WaveEndDPDistance); }
		}

		private System.Single _WaveEndHeight;
		[ModelLib.CustomAttributes.Default("0.7")]
		[ModelLib.CustomAttributes.DisplayGroup("WaveDetection")]
		[ModelLib.CustomAttributes.Max(1000.0)]
		[ModelLib.CustomAttributes.Min(-1000.0)]
		public System.Single WaveEndHeight {
			get { return _WaveEndHeight; }
			set { SetField(ref _WaveEndHeight, value, () => WaveEndHeight); }
		}

		private System.Single _RecedeHeight;
		[ModelLib.CustomAttributes.Default("0.5")]
		[ModelLib.CustomAttributes.DisplayGroup("WaveDetection")]
		[ModelLib.CustomAttributes.Max(1000.0)]
		[ModelLib.CustomAttributes.Min(-1000.0)]
		public System.Single RecedeHeight {
			get { return _RecedeHeight; }
			set { SetField(ref _RecedeHeight, value, () => RecedeHeight); }
		}

		public class ShoreLinePointsDefinition: ModelBase {
			private System.Single _X;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single X {
				get { return _X; }
				set { SetField(ref _X, value, () => X); }
			}

			private System.Single _Y;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single Y {
				get { return _Y; }
				set { SetField(ref _Y, value, () => Y); }
			}

		}
		private ItemsObservableCollection<ShoreLinePointsDefinition> _ShoreLinePoints = new ItemsObservableCollection<ShoreLinePointsDefinition>();
		[ModelLib.CustomAttributes.DisplayGroup("Definition")]
		[ModelLib.CustomAttributes.MaxSize(32)]
		[XmlElement("ShoreLinePoints")]
		public ItemsObservableCollection<ShoreLinePointsDefinition> ShoreLinePoints {
			get { return _ShoreLinePoints; }
			set { SetField(ref _ShoreLinePoints, value, () => ShoreLinePoints); }
		}

	}
}


