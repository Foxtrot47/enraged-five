using ModelLib.CustomAttributes;

namespace model {

	public enum MixModuleInput {
		[ModelLib.CustomAttributes.Display("None")]
		INPUT_NONE,
		[ModelLib.CustomAttributes.Display("PlayerVelocity")]
		INPUT_PLAYER_VEH_VELOCITY,
		[ModelLib.CustomAttributes.Display("PlayerAirtime")]
		INPUT_PLAYER_VEH_AIRTIME,
		[ModelLib.CustomAttributes.Display("PlayerVehRoll")]
		INPUT_PLAYER_VEH_ROLL,
		[ModelLib.CustomAttributes.Display("PlayerWanted")]
		INPUT_PLAYER_WANTED,
		[ModelLib.CustomAttributes.Display("VehiclesOnSides")]
		VEH_VEH_SIDES,
		[ModelLib.CustomAttributes.Display("VehicleBuildingOnSides")]
		VEH_BUILDING_SIDES
	}

}