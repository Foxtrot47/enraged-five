using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Transformer("{Type=\"rage.AMPCompiler.AMPTransformer, AMPCompiler, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\"}")]
	[ModelLib.CustomAttributes.Align(4)]
	public class ModularSynth:  ModelBase {

		private ModelLib.Types.TriState _ExportForGame;
		[ModelLib.CustomAttributes.Default("no")]
		public ModelLib.Types.TriState ExportForGame {
			get { return _ExportForGame; }
			set { SetField(ref _ExportForGame, value, () => ExportForGame); }
		}

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		public class ModuleDefinition: ModelBase {
			private System.UInt32 _TypeId;
			public System.UInt32 TypeId {
				get { return _TypeId; }
				set { SetField(ref _TypeId, value, () => TypeId); }
			}

			private System.Single _IsCollapsed;
			[ModelLib.CustomAttributes.Default("0.0")]
			public System.Single IsCollapsed {
				get { return _IsCollapsed; }
				set { SetField(ref _IsCollapsed, value, () => IsCollapsed); }
			}

			private System.Single _PosX;
			public System.Single PosX {
				get { return _PosX; }
				set { SetField(ref _PosX, value, () => PosX); }
			}

			private System.Single _PosY;
			public System.Single PosY {
				get { return _PosY; }
				set { SetField(ref _PosY, value, () => PosY); }
			}

			private ModelLib.ObjectRef _PresetList;
			[ModelLib.CustomAttributes.AllowedType(typeof(PresetList))]
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef PresetList {
				get { return _PresetList; }
				set { SetField(ref _PresetList, value, () => PresetList); }
			}

			private System.Int32 _ProcessingStage;
			[ModelLib.CustomAttributes.Default("-1")]
			public System.Int32 ProcessingStage {
				get { return _ProcessingStage; }
				set { SetField(ref _ProcessingStage, value, () => ProcessingStage); }
			}

			private System.Int32 _GroupId;
			[ModelLib.CustomAttributes.Default("-1")]
			public System.Int32 GroupId {
				get { return _GroupId; }
				set { SetField(ref _GroupId, value, () => GroupId); }
			}

			public class FieldValueDefinition: ModelBase {
				private System.Single _Value;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single Value {
					get { return _Value; }
					set { SetField(ref _Value, value, () => Value); }
				}

			}
			private ItemsObservableCollection<FieldValueDefinition> _FieldValue = new ItemsObservableCollection<FieldValueDefinition>();
			[ModelLib.CustomAttributes.MaxSize(16, typeof(System.UInt32))]
			[XmlElement("FieldValue")]
			public ItemsObservableCollection<FieldValueDefinition> FieldValue {
				get { return _FieldValue; }
				set { SetField(ref _FieldValue, value, () => FieldValue); }
			}

			public class InputPinDefinition: ModelBase {
				private System.Single _StaticValue;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single StaticValue {
					get { return _StaticValue; }
					set { SetField(ref _StaticValue, value, () => StaticValue); }
				}

				private System.Int16 _OtherPin;
				[ModelLib.CustomAttributes.Default("-1")]
				public System.Int16 OtherPin {
					get { return _OtherPin; }
					set { SetField(ref _OtherPin, value, () => OtherPin); }
				}

				private System.Int16 _OtherModule;
				[ModelLib.CustomAttributes.Default("-1")]
				public System.Int16 OtherModule {
					get { return _OtherModule; }
					set { SetField(ref _OtherModule, value, () => OtherModule); }
				}

			}
			private ItemsObservableCollection<InputPinDefinition> _InputPin = new ItemsObservableCollection<InputPinDefinition>();
			[ModelLib.CustomAttributes.MaxSize(32, typeof(System.UInt32))]
			[XmlElement("InputPin")]
			public ItemsObservableCollection<InputPinDefinition> InputPin {
				get { return _InputPin; }
				set { SetField(ref _InputPin, value, () => InputPin); }
			}

			public class OutputPinDefinition: ModelBase {
				private System.Single _StaticValue;
				[ModelLib.CustomAttributes.Default("0.0")]
				public System.Single StaticValue {
					get { return _StaticValue; }
					set { SetField(ref _StaticValue, value, () => StaticValue); }
				}

				private System.Int16 _OtherPin;
				[ModelLib.CustomAttributes.Default("-1")]
				public System.Int16 OtherPin {
					get { return _OtherPin; }
					set { SetField(ref _OtherPin, value, () => OtherPin); }
				}

				private System.Int16 _OtherModule;
				[ModelLib.CustomAttributes.Default("-1")]
				public System.Int16 OtherModule {
					get { return _OtherModule; }
					set { SetField(ref _OtherModule, value, () => OtherModule); }
				}

			}
			private ItemsObservableCollection<OutputPinDefinition> _OutputPin = new ItemsObservableCollection<OutputPinDefinition>();
			[ModelLib.CustomAttributes.MaxSize(32, typeof(System.UInt32))]
			[XmlElement("OutputPin")]
			public ItemsObservableCollection<OutputPinDefinition> OutputPin {
				get { return _OutputPin; }
				set { SetField(ref _OutputPin, value, () => OutputPin); }
			}

		}
		private ItemsObservableCollection<ModuleDefinition> _Module = new ItemsObservableCollection<ModuleDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1024)]
		[XmlElement("Module")]
		public ItemsObservableCollection<ModuleDefinition> Module {
			get { return _Module; }
			set { SetField(ref _Module, value, () => Module); }
		}

		public class ExposedVariableDefinition: ModelBase {
			private System.String _Key;
			[ModelLib.CustomAttributes.Length(32)]
			public System.String Key {
				get { return _Key; }
				set { SetField(ref _Key, value, () => Key); }
			}

			private System.Int16 _ModuleId;
			[ModelLib.CustomAttributes.Default("-1")]
			[ModelLib.CustomAttributes.Max(32767)]
			[ModelLib.CustomAttributes.Min(-1)]
			public System.Int16 ModuleId {
				get { return _ModuleId; }
				set { SetField(ref _ModuleId, value, () => ModuleId); }
			}

		}
		private ItemsObservableCollection<ExposedVariableDefinition> _ExposedVariable = new ItemsObservableCollection<ExposedVariableDefinition>();
		[ModelLib.CustomAttributes.MaxSize(128)]
		[XmlElement("ExposedVariable")]
		public ItemsObservableCollection<ExposedVariableDefinition> ExposedVariable {
			get { return _ExposedVariable; }
			set { SetField(ref _ExposedVariable, value, () => ExposedVariable); }
		}

		public class ModuleGroupDefinition: ModelBase {
			private System.String _Name;
			[ModelLib.CustomAttributes.Default("Group0")]
			[ModelLib.CustomAttributes.Length(64)]
			public System.String Name {
				get { return _Name; }
				set { SetField(ref _Name, value, () => Name); }
			}

			private System.Int32 _ParentGroupId;
			[ModelLib.CustomAttributes.Default("-1")]
			public System.Int32 ParentGroupId {
				get { return _ParentGroupId; }
				set { SetField(ref _ParentGroupId, value, () => ParentGroupId); }
			}

			public class ColourDefinition: ModelBase {
				private System.Byte _R;
				public System.Byte R {
					get { return _R; }
					set { SetField(ref _R, value, () => R); }
				}

				private System.Byte _G;
				public System.Byte G {
					get { return _G; }
					set { SetField(ref _G, value, () => G); }
				}

				private System.Byte _B;
				public System.Byte B {
					get { return _B; }
					set { SetField(ref _B, value, () => B); }
				}

			}
			private ColourDefinition _Colour;
			public ColourDefinition Colour {
				get { return _Colour; }
				set { SetField(ref _Colour, value, () => Colour); }
			}

		}
		private ItemsObservableCollection<ModuleGroupDefinition> _ModuleGroup = new ItemsObservableCollection<ModuleGroupDefinition>();
		[ModelLib.CustomAttributes.MaxSize(255)]
		[XmlElement("ModuleGroup")]
		public ItemsObservableCollection<ModuleGroupDefinition> ModuleGroup {
			get { return _ModuleGroup; }
			set { SetField(ref _ModuleGroup, value, () => ModuleGroup); }
		}

		public class CommentsDefinition: ModelBase {
			private System.Single _PosX;
			public System.Single PosX {
				get { return _PosX; }
				set { SetField(ref _PosX, value, () => PosX); }
			}

			private System.Single _PosY;
			public System.Single PosY {
				get { return _PosY; }
				set { SetField(ref _PosY, value, () => PosY); }
			}

			private System.Int32 _GroupId;
			[ModelLib.CustomAttributes.Default("-1")]
			public System.Int32 GroupId {
				get { return _GroupId; }
				set { SetField(ref _GroupId, value, () => GroupId); }
			}

			private System.String _Text;
			[ModelLib.CustomAttributes.Length(256)]
			public System.String Text {
				get { return _Text; }
				set { SetField(ref _Text, value, () => Text); }
			}

			public class ColourDefinition2: ModelBase {
				private System.Byte _R;
				public System.Byte R {
					get { return _R; }
					set { SetField(ref _R, value, () => R); }
				}

				private System.Byte _G;
				public System.Byte G {
					get { return _G; }
					set { SetField(ref _G, value, () => G); }
				}

				private System.Byte _B;
				public System.Byte B {
					get { return _B; }
					set { SetField(ref _B, value, () => B); }
				}

				private System.Byte _A;
				public System.Byte A {
					get { return _A; }
					set { SetField(ref _A, value, () => A); }
				}

			}
			private ColourDefinition2 _Colour;
			public ColourDefinition2 Colour {
				get { return _Colour; }
				set { SetField(ref _Colour, value, () => Colour); }
			}

		}
		private ItemsObservableCollection<CommentsDefinition> _Comments = new ItemsObservableCollection<CommentsDefinition>();
		[ModelLib.CustomAttributes.MaxSize(255)]
		[XmlElement("Comments")]
		public ItemsObservableCollection<CommentsDefinition> Comments {
			get { return _Comments; }
			set { SetField(ref _Comments, value, () => Comments); }
		}

		public class CameraPositionDefinition: ModelBase {
			private System.Single _PosX;
			public System.Single PosX {
				get { return _PosX; }
				set { SetField(ref _PosX, value, () => PosX); }
			}

			private System.Single _PosY;
			public System.Single PosY {
				get { return _PosY; }
				set { SetField(ref _PosY, value, () => PosY); }
			}

			private System.Single _PosZ;
			public System.Single PosZ {
				get { return _PosZ; }
				set { SetField(ref _PosZ, value, () => PosZ); }
			}

		}
		private ItemsObservableCollection<CameraPositionDefinition> _CameraPosition = new ItemsObservableCollection<CameraPositionDefinition>();
		[ModelLib.CustomAttributes.MaxSize(11)]
		[XmlElement("CameraPosition")]
		public ItemsObservableCollection<CameraPositionDefinition> CameraPosition {
			get { return _CameraPosition; }
			set { SetField(ref _CameraPosition, value, () => CameraPosition); }
		}

	}
}


