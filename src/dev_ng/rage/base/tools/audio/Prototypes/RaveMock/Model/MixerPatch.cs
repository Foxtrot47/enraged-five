using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;

namespace model {

	[ModelLib.CustomAttributes.Align(4)]
	public class MixerPatch:  ModelBase {

		private System.UInt16 _FadeIn;
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt16 FadeIn {
			get { return _FadeIn; }
			set { SetField(ref _FadeIn, value, () => FadeIn); }
		}

		private System.UInt16 _FadeOut;
		[ModelLib.CustomAttributes.Unit("ms")]
		public System.UInt16 FadeOut {
			get { return _FadeOut; }
			set { SetField(ref _FadeOut, value, () => FadeOut); }
		}

		private System.Single _PreDelay;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Unit("s")]
		public System.Single PreDelay {
			get { return _PreDelay; }
			set { SetField(ref _PreDelay, value, () => PreDelay); }
		}

		private System.Single _Duration;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Unit("s")]
		public System.Single Duration {
			get { return _Duration; }
			set { SetField(ref _Duration, value, () => Duration); }
		}

		private ModelLib.ObjectRef _ApplyFactorCurve;
		[ModelLib.CustomAttributes.Default("LINEAR_RISE")]
		[ModelLib.CustomAttributes.AllowedType(typeof(BaseCurve))]
		[ModelLib.CustomAttributes.DisplayGroup("Curves")]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
		public ModelLib.ObjectRef ApplyFactorCurve {
			get { return _ApplyFactorCurve; }
			set { SetField(ref _ApplyFactorCurve, value, () => ApplyFactorCurve); }
		}

		private System.String _ApplyVariable;
		[ModelLib.CustomAttributes.Default("apply")]
		public System.String ApplyVariable {
			get { return _ApplyVariable; }
			set { SetField(ref _ApplyVariable, value, () => ApplyVariable); }
		}

		private System.Single _ApplySmoothRate;
		[ModelLib.CustomAttributes.Default("0.0")]
		[ModelLib.CustomAttributes.Max(100.0)]
		[ModelLib.CustomAttributes.Min(0.0)]
		public System.Single ApplySmoothRate {
			get { return _ApplySmoothRate; }
			set { SetField(ref _ApplySmoothRate, value, () => ApplySmoothRate); }
		}

		private ModelLib.Types.TriState _DisabledInPauseMenu;
		[ModelLib.CustomAttributes.Default("yes")]
		[ModelLib.CustomAttributes.Description("Disable the effect of this patch when the pause menu is active")]
		public ModelLib.Types.TriState DisabledInPauseMenu {
			get { return _DisabledInPauseMenu; }
			set { SetField(ref _DisabledInPauseMenu, value, () => DisabledInPauseMenu); }
		}

		private ModelLib.Types.TriState _DoesPausedTransitions;
		[ModelLib.CustomAttributes.Default("no")]
		[ModelLib.CustomAttributes.Description("Used a timer that's not affected by pausing")]
		public ModelLib.Types.TriState DoesPausedTransitions {
			get { return _DoesPausedTransitions; }
			set { SetField(ref _DoesPausedTransitions, value, () => DoesPausedTransitions); }
		}

		public class MixCategoriesDefinition: ModelBase {
			private System.String _Name;
			[ModelLib.CustomAttributes.Default("BASE")]
			[ModelLib.CustomAttributes.Unit("CategoryRef")]
			public System.String Name {
				get { return _Name; }
				set { SetField(ref _Name, value, () => Name); }
			}

			private System.Int16 _Volume;
			[ModelLib.CustomAttributes.Default("0")]
			[ModelLib.CustomAttributes.Max(10000)]
			[ModelLib.CustomAttributes.Min(-10000)]
			[ModelLib.CustomAttributes.Unit("mB")]
			public System.Int16 Volume {
				get { return _Volume; }
				set { SetField(ref _Volume, value, () => Volume); }
			}

			private VolumeInvertInput _VolumeInvert;
			[ModelLib.CustomAttributes.Default("INPUT_DO_NOTHING")]
			public VolumeInvertInput VolumeInvert {
				get { return _VolumeInvert; }
				set { SetField(ref _VolumeInvert, value, () => VolumeInvert); }
			}

			private System.UInt16 _LPFCutoff;
			[ModelLib.CustomAttributes.Default("23900")]
			public System.UInt16 LPFCutoff {
				get { return _LPFCutoff; }
				set { SetField(ref _LPFCutoff, value, () => LPFCutoff); }
			}

			private System.UInt16 _HPFCutoff;
			[ModelLib.CustomAttributes.Default("0")]
			public System.UInt16 HPFCutoff {
				get { return _HPFCutoff; }
				set { SetField(ref _HPFCutoff, value, () => HPFCutoff); }
			}

			private System.Int16 _Pitch;
			[ModelLib.CustomAttributes.Unit("cents")]
			public System.Int16 Pitch {
				get { return _Pitch; }
				set { SetField(ref _Pitch, value, () => Pitch); }
			}

			private System.Single _Frequency;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single Frequency {
				get { return _Frequency; }
				set { SetField(ref _Frequency, value, () => Frequency); }
			}

			private VolumeInvertInput _PitchInvert;
			[ModelLib.CustomAttributes.Default("INPUT_DO_NOTHING")]
			public VolumeInvertInput PitchInvert {
				get { return _PitchInvert; }
				set { SetField(ref _PitchInvert, value, () => PitchInvert); }
			}

			private System.Single _Rolloff;
			[ModelLib.CustomAttributes.Default("1.0")]
			public System.Single Rolloff {
				get { return _Rolloff; }
				set { SetField(ref _Rolloff, value, () => Rolloff); }
			}

		}
		private ItemsObservableCollection<MixCategoriesDefinition> _MixCategories = new ItemsObservableCollection<MixCategoriesDefinition>();
		[ModelLib.CustomAttributes.MaxSize(32)]
		public ItemsObservableCollection<MixCategoriesDefinition> MixCategories {
			get { return _MixCategories; }
			set { SetField(ref _MixCategories, value, () => MixCategories); }
		}

	}
}


