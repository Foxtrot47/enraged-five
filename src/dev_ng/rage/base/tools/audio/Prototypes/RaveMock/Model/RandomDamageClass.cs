using ModelLib.CustomAttributes;

namespace model {

	public enum RandomDamageClass {
		[ModelLib.CustomAttributes.Display("Always")]
		RANDOM_DAMAGE_ALWAYS,
		[ModelLib.CustomAttributes.Display("Workhorse")]
		RANDOM_DAMAGE_WORKHORSE,
		[ModelLib.CustomAttributes.Display("Occasional Slight")]
		RANDOM_DAMAGE_OCCASIONAL,
		[ModelLib.CustomAttributes.Display("Never")]
		RANDOM_DAMAGE_NONE
	}

}