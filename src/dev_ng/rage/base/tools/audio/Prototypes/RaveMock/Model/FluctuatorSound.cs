using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Core Sound Types")]
	public class FluctuatorSound:  Sound{

		private ModelLib.ObjectRef _SoundRef;
		[ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
		[ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
		public ModelLib.ObjectRef SoundRef {
			get { return _SoundRef; }
			set { SetField(ref _SoundRef, value, () => SoundRef); }
		}

		public class FluctuatorDefinition: ModelBase {
			private FluctuatorModes _Mode;
			[ModelLib.CustomAttributes.Default("PROBABILITY_BASED")]
			public FluctuatorModes Mode {
				get { return _Mode; }
				set { SetField(ref _Mode, value, () => Mode); }
			}

			private ParameterDestinations _Destination;
			[ModelLib.CustomAttributes.Default("PARAM_DESTINATION_VOLUME")]
			public ParameterDestinations Destination {
				get { return _Destination; }
				set { SetField(ref _Destination, value, () => Destination); }
			}

			private System.String _OutputVariable;
			[ModelLib.CustomAttributes.OutputVariable]
			[ModelLib.CustomAttributes.Unit("variable")]
			public System.String OutputVariable {
				get { return _OutputVariable; }
				set { SetField(ref _OutputVariable, value, () => OutputVariable); }
			}

			private System.Single _IncreaseRate;
			public System.Single IncreaseRate {
				get { return _IncreaseRate; }
				set { SetField(ref _IncreaseRate, value, () => IncreaseRate); }
			}

			private System.Single _DecreaseRate;
			public System.Single DecreaseRate {
				get { return _DecreaseRate; }
				set { SetField(ref _DecreaseRate, value, () => DecreaseRate); }
			}

			private System.Single _BandOneMinimum;
			public System.Single BandOneMinimum {
				get { return _BandOneMinimum; }
				set { SetField(ref _BandOneMinimum, value, () => BandOneMinimum); }
			}

			private System.Single _BandOneMaximum;
			public System.Single BandOneMaximum {
				get { return _BandOneMaximum; }
				set { SetField(ref _BandOneMaximum, value, () => BandOneMaximum); }
			}

			private System.Single _BandTwoMinimum;
			public System.Single BandTwoMinimum {
				get { return _BandTwoMinimum; }
				set { SetField(ref _BandTwoMinimum, value, () => BandTwoMinimum); }
			}

			private System.Single _BandTwoMaximum;
			public System.Single BandTwoMaximum {
				get { return _BandTwoMaximum; }
				set { SetField(ref _BandTwoMaximum, value, () => BandTwoMaximum); }
			}

			private System.Single _IntraBandFlipProbabilty;
			public System.Single IntraBandFlipProbabilty {
				get { return _IntraBandFlipProbabilty; }
				set { SetField(ref _IntraBandFlipProbabilty, value, () => IntraBandFlipProbabilty); }
			}

			private System.Single _InterBandFlipProbabilty;
			public System.Single InterBandFlipProbabilty {
				get { return _InterBandFlipProbabilty; }
				set { SetField(ref _InterBandFlipProbabilty, value, () => InterBandFlipProbabilty); }
			}

			private System.UInt32 _MinSwitchTime;
			public System.UInt32 MinSwitchTime {
				get { return _MinSwitchTime; }
				set { SetField(ref _MinSwitchTime, value, () => MinSwitchTime); }
			}

			private System.UInt32 _MaxSwitchTime;
			public System.UInt32 MaxSwitchTime {
				get { return _MaxSwitchTime; }
				set { SetField(ref _MaxSwitchTime, value, () => MaxSwitchTime); }
			}

			private System.Single _InitialValue;
			public System.Single InitialValue {
				get { return _InitialValue; }
				set { SetField(ref _InitialValue, value, () => InitialValue); }
			}

		}
		private ItemsObservableCollection<FluctuatorDefinition> _Fluctuator = new ItemsObservableCollection<FluctuatorDefinition>();
		[ModelLib.CustomAttributes.MaxSize(4, typeof(System.UInt32))]
		[XmlElement("Fluctuator")]
		public ItemsObservableCollection<FluctuatorDefinition> Fluctuator {
			get { return _Fluctuator; }
			set { SetField(ref _Fluctuator, value, () => Fluctuator); }
		}

	}
}


