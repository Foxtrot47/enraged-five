using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class WaveSlots:  ModelBase {

		public class SlotRefDefinition: ModelBase {
			private ModelLib.ObjectRef _SlotNameHash;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.UInt32))]
			public ModelLib.ObjectRef SlotNameHash {
				get { return _SlotNameHash; }
				set { SetField(ref _SlotNameHash, value, () => SlotNameHash); }
			}

		}
		private ItemsObservableCollection<SlotRefDefinition> _SlotRef = new ItemsObservableCollection<SlotRefDefinition>();
		[ModelLib.CustomAttributes.MaxSize(255, typeof(System.UInt32))]
		[XmlElement("SlotRef")]
		public ItemsObservableCollection<SlotRefDefinition> SlotRef {
			get { return _SlotRef; }
			set { SetField(ref _SlotRef, value, () => SlotRef); }
		}

	}
}


