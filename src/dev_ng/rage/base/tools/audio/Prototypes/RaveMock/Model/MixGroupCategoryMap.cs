using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	public class MixGroupCategoryMap:  ModelBase {

		public class EntryDefinition: ModelBase {
			private ModelLib.ObjectRef _CategoryHash;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef CategoryHash {
				get { return _CategoryHash; }
				set { SetField(ref _CategoryHash, value, () => CategoryHash); }
			}

			private ModelLib.ObjectRef _ParentHash;
			[ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
			public ModelLib.ObjectRef ParentHash {
				get { return _ParentHash; }
				set { SetField(ref _ParentHash, value, () => ParentHash); }
			}

		}
		private ItemsObservableCollection<EntryDefinition> _Entry = new ItemsObservableCollection<EntryDefinition>();
		[ModelLib.CustomAttributes.MaxSize(1024)]
		[XmlElement("Entry")]
		public ItemsObservableCollection<EntryDefinition> Entry {
			get { return _Entry; }
			set { SetField(ref _Entry, value, () => Entry); }
		}

	}
}


