using ModelLib;
using ModelLib.CustomAttributes;
using ModelLib.Types;
using System.Xml.Serialization;

namespace model {

	[ModelLib.CustomAttributes.Group("Radio")]
	public class RadioTrackCategoryData:  ModelBase {

		private System.String _Description;
		[ModelLib.CustomAttributes.Ignore]
		public System.String Description {
			get { return _Description; }
			set { SetField(ref _Description, value, () => Description); }
		}

		public class CategoryDefinition: ModelBase {
			private TrackCats _Category;
			[ModelLib.CustomAttributes.Default("RADIO_TRACK_CAT_MUSIC")]
			public TrackCats Category {
				get { return _Category; }
				set { SetField(ref _Category, value, () => Category); }
			}

			private System.UInt32 _Value;
			[ModelLib.CustomAttributes.Default("1")]
			public System.UInt32 Value {
				get { return _Value; }
				set { SetField(ref _Value, value, () => Value); }
			}

		}
		private ItemsObservableCollection<CategoryDefinition> _Category = new ItemsObservableCollection<CategoryDefinition>();
		[ModelLib.CustomAttributes.MaxSize(30)]
		[XmlElement("Category")]
		public ItemsObservableCollection<CategoryDefinition> Category {
			get { return _Category; }
			set { SetField(ref _Category, value, () => Category); }
		}

	}
}


