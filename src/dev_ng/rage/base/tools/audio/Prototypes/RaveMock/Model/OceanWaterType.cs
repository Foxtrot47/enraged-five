using ModelLib.CustomAttributes;

namespace model {

	public enum OceanWaterType {
		[ModelLib.CustomAttributes.Display("Beach")]
		AUD_OCEAN_TYPE_BEACH,
		[ModelLib.CustomAttributes.Display("Rocks")]
		AUD_OCEAN_TYPE_ROCKS,
		[ModelLib.CustomAttributes.Display("Port")]
		AUD_OCEAN_TYPE_PORT,
		[ModelLib.CustomAttributes.Display("Pier")]
		AUD_OCEAN_TYPE_PIER
	}

}