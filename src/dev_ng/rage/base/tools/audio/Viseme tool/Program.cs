using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

namespace VisemeProcessor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            frmVisemeTool app = new frmVisemeTool(args);
            return app.error;
        }
    }
}