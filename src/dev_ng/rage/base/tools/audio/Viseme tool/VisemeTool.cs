using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Wavelib;
using System.IO;
using System.Threading;

namespace VisemeProcessor
{
    public partial class frmVisemeTool
    {

        private string m_WavInPath;
        private string m_WavOutPath;
        private string m_VisemePath;
        public int error;


        public frmVisemeTool(string[] args)
        {
            if (args.Length < 2)
            {
                return;
            }

            m_WavInPath = args[0].ToUpper();
            m_WavOutPath = args[1].ToUpper();
            m_VisemePath = args[2].ToUpper();

            error = 0;
            ProcessFile(m_WavInPath, m_WavOutPath, m_VisemePath, ref error);
        }

        public void ProcessFile(string waveFileInPath, string waveFileOutPath, string visemePath, ref int error)
        {
            try
            {
                //create wave from wave path
                if (File.Exists(waveFileInPath))
                {
                    bwWaveFile waveFile = new bwWaveFile(waveFileInPath, false);
                    Console.WriteLine("Wavefile opened");
                    //add chunk
                    if (File.Exists(visemePath))
                    {
                        FileStream inStream = new FileStream(visemePath, FileMode.Open, FileAccess.Read);
                        byte[] data = new byte[inStream.Length];
                        inStream.Read(data, 0, (int)inStream.Length);
                        inStream.Close();

                        int index = data.Length - 1;
                        while (Convert.ToInt32(data[index]) == 0xcd)
                        {
                            index--;
                        }
                        Array.Resize<byte>(ref data, index + 1);

                        waveFile.RemoveRaveChunks();

                        bwRaveChunk chunk;
                        chunk = new bwRaveChunk("XARB", data);
                        Console.WriteLine("Adding chunk");
                        waveFile.addRaveChunk(chunk);
                        Console.WriteLine("Chunk added");
                    }
                    else
                    {
                        error=1;
                        Console.WriteLine("Error1");
                    }
                    //write wave
                    waveFile.Save(waveFileOutPath);
                }
                else
                {
                    error=2;
                    Console.WriteLine("Error2");
                }

            }
            catch (Exception e)
            {
                error=3;
                Console.WriteLine("Error3");
            }

        }
    }

}