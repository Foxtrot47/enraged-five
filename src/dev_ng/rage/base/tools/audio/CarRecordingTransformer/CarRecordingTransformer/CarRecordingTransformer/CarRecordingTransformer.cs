﻿// -----------------------------------------------------------------------
// <copyright file="CarRecordingTransformer.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace rage.CarRecordingTransformer
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using rage.ToolLib.Logging;
    using rage.Transform;

    /// <summary>
    /// Car recording transformer.
    /// </summary>
    [CanTransform("CarRecordingAudioSettings")]
    public class CarRecordingTransformer : ITransformer
    {
        /// <summary>
        /// The dynamic mixer objects path.
        /// </summary>
        private string dynamicMixerObjectsPath;

        /// <summary>
        /// The dynamic mixer objects document.
        /// </summary>
        private XDocument dynamicMixerDoc;
        
        /// <summary>
        /// Initialize the transformer.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Init(ILog log, params string[] args)
        {
            log.Information("CarRecordingTransformer init");

            if (!args.Any())
            {
                log.Error("Missing CarRecordingTransformer argument(s)");
                return false;
            }

            this.dynamicMixerObjectsPath = args[0];

            if (!Directory.Exists(this.dynamicMixerObjectsPath))
            {
                log.Error("Dynamic mixer objects directory doesn't exist: " + this.dynamicMixerObjectsPath);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Transform the objects.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// The transform result <see cref="TransformResult"/>.
        /// </returns>
        public TransformResult Transform(ILog log, XElement element)
        {
            var result = new TransformResult();

            // Load the dynamic mixer object data
            if (!this.LoadDynamicMixerData(log))
            {
                log.Error("Failed to load dynamic mixer object data");
                return result;
            }

            // Get name of CarRecordingAudioSettings object
            var settingsNameElem = element.Attribute("name");
            if (null == settingsNameElem)
            {
                throw new Exception("'name' attribute expected in CarRecordingAudioSettings XElement");
            }

            var settingsName = settingsNameElem.Value;
            var requiresMixGroupElem = false;

            foreach (var oneShotSceneElem in element.Descendants("OneShotScene"))
            {
                var sceneName = oneShotSceneElem.Value;
                if (string.IsNullOrEmpty(sceneName))
                {
                    continue;
                }

                if (this.SceneHasMixGroup(sceneName))
                {
                    requiresMixGroupElem = true;
                    
                    // Only need to know that at least one scene has a mix group,
                    // so break here
                    break;
                }
            }

            if (!requiresMixGroupElem)
            {
                // Also check for persistent mixer scenes
                var persistentMixerScenesElem = element.Element("PersistentMixerScenes");
                if (null != persistentMixerScenesElem)
                {
                    foreach (var instanceElem in persistentMixerScenesElem.Elements("Instance"))
                    {
                        var sceneElem = instanceElem.Element("Scene");
                        if (null != sceneElem)
                        {
                            var sceneName = sceneElem.Value;
                            if (this.SceneHasMixGroup(sceneName))
                            {
                                requiresMixGroupElem = true;

                                // Only need to know that at least one scene has a mix group,
                                // so break here
                                break;
                            }
                        }
                    }
                }
            }

            // Prepend the CarRecordingAudioSettings name to the OneShotScene element values
            foreach (var elem in element.Descendants("OneShotScene"))
            {
                elem.Value = settingsName + "_" + elem.Value;
            }

            // Prepend the CarRecordingAudioSettings name to the Scene element values
            foreach (var elem in element.Descendants("Scene"))
            {
                elem.Value = settingsName + "_" + elem.Value;
            }

            // Add <Mixgroup> elem if required
            if (requiresMixGroupElem)
            {
                var mixGroupElem = new XElement("Mixgroup")
                {
                    Value = settingsName + "_MG"
                };
                element.Add(mixGroupElem);
            }

            result.Result = element;
            return result;
        }

        /// <summary>
        /// Cleanup on shutdown.
        /// </summary>
        public void Shutdown()
        {
            // Not required
        }

        /// <summary>
        /// Load the dynamic mixer objects data.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadDynamicMixerData(ILog log)
        {
            this.dynamicMixerDoc = new XDocument();
            var objsElem = new XElement("Objects");
            this.dynamicMixerDoc.Add(objsElem);

            var filePaths = Directory.GetFiles(this.dynamicMixerObjectsPath, "*.xml", SearchOption.AllDirectories);
            foreach (var filePath in filePaths)
            {
                try
                {
                    var doc = XDocument.Load(filePath);
                    foreach (var childElem in doc.Element("Objects").Elements())
                    {
                        this.dynamicMixerDoc.Root.Add(childElem);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Failed to load script data from " + filePath + ": " + ex.Message);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Indicates whether a scene has a mix group.
        /// </summary>
        /// <param name="sceneName">
        /// The scene name.
        /// </param>
        /// <returns>
        /// Indication of whether the scene has a mix group <see cref="bool"/>.
        /// </returns>
        private bool SceneHasMixGroup(string sceneName)
        {
            foreach (var sceneElem in this.dynamicMixerDoc.Descendants("MixerScene"))
            {
                var currSceneNameAttr = sceneElem.Attribute("name");
                if (null == currSceneNameAttr)
                {
                    // Should never happen, but safe to check.
                    continue;
                }

                // Check if this is the scene we are looking for
                var currSceneName = currSceneNameAttr.Value;
                if (currSceneName != sceneName)
                {
                    continue;
                }

                // Return hit if there are any MixGroup elements with a value set
                if (sceneElem.Descendants("MixGroup").Any(
                    mixGroupElem => !string.IsNullOrEmpty(mixGroupElem.Value)))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
