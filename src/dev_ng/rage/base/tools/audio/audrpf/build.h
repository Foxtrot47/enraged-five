//
// audrpf/build.h
// 

#ifndef AUDRPF_BUILD_H
#define AUDRPF_BUILD_H


//////////////////////////////////////////////////////////////////////////
// See audRpf.h for example on how to call this function
//
//////////////////////////////////////////////////////////////////////////
void init(void);
int build(const char *inPath, const char *packName, const char *outPath, bool bShouldEncrypt);


#endif


