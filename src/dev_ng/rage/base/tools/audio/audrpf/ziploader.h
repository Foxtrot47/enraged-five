// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUDZIP_ZIPLOADER_H
#define AUDZIP_ZIPLOADER_H

#include "file/stream.h"

namespace rage 
{
#if 0
struct fiPackEntry;

class zipLoader 
{
public:
	zipLoader();
	~zipLoader();

	fiStream* Load(const char *filename);
	fiStream* Create(const char *filename);
	void Copy(fiStream *input,fiStream *output);
	void Finish(fiStream *output);

private:
	fiPackEntry *m_Entries;
	int m_EntryCount;
	char *m_NameHeap;
	int m_NameHeapSize;
	u32 m_WriteOffset;
};
#endif
}	// namespace rage

#endif
