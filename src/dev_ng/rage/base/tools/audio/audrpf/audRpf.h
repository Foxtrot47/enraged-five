//
// audrpf.h
// 
#ifndef AUDRPF_AUDRPF_H
#define AUDRPF_AUDRPF_H

#ifdef AUDRPF_EXPORTS
#define AUDRPF_API __declspec(dllexport)
#else
#define AUDRPF_API __declspec(dllimport)
#endif

namespace rage
{

// This class is exported from the audRpf.dll
class AUDRPF_API CAudioRpf 
{
public:
	CAudioRpf(void);

	//////////////////////////////////////////////////////////////////////////
	// PURPOSE
	//   This function creates an RPF file for use in-game.
	// PARAMS
	//   inPath - the path when combined with packName, provides a relative
	//            tree from which to generate files to place in an RPF file.
	//   packName - the name of the packfile (and also the directory) to use
	//            when searching for files to add to the RPF file.
	//   outPath - the path to output the RPF file.
	// EXAMPLE
	//   Build("t:\mc4\audio\sfx", "SAMPLES", "c:\temp") will search for bank
	//	          files under t:\mc4\audio\sfx\samples, and build and place
	//            an RPF file in c:\temp\SAMPLES.rpf
	//////////////////////////////////////////////////////////////////////////
	void Build(const char *inPath, const char *packName, const char *outPath, bool bShouldEncrypt);
	void Init();
};

};

#endif

