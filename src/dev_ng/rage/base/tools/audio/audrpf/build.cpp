// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "file/direntry.h"
#include "file/compress.h"
#include "file/decompress.h"
#include "file/stream.h"
#include "file/packfile.h"
#include "file/asset.h"
#include "system/magicnumber.h"
#include "system/param.h"
#include "data/aes.h"
#include "diag/channel.h"

using namespace rage;

#define _MAX_PATH 260

const int g_MaxBanks = 4096;
int g_NumFiles = 0;
char g_PackName[64];
char g_BankArray[g_MaxBanks][g_MaxBanks];

static void FileEnumCallback(const fiFindData &data, void *)
{
	if (g_NumFiles < g_MaxBanks)
	{
		formatf(g_BankArray[g_NumFiles], g_MaxBanks, "%s/%s", g_PackName, data.m_Name);
		++g_NumFiles;
	}
}

void init(void)
{
	if(!sysParam::IsInitialized())
	{
		sysParam::Init(0, NULL);
	}
}

int build(const char *inPath, const char *packName, const char *outPath, bool bShouldEncrypt) 
{
	const bool keepNameHeap = false;

	// hack to disable diag output since its crashing
	//diagLogCallback = NullPrinterFn;

	Assert(inPath && outPath && packName);

	g_NumFiles = 0;
	formatf(g_PackName, sizeof(g_PackName), "%s", packName);

	char fullPath[_MAX_PATH];
	formatf(fullPath, sizeof(fullPath), "%s\\%s", inPath, packName);
	ASSET.SetPath(fullPath);
	ASSET.EnumFiles(fullPath, &FileEnumCallback, NULL);

	char tempname[256];
	formatf(tempname, sizeof(tempname), "%s.temp", g_PackName);
	fiSafeStream T(fiStream::Create(tempname));
	if (!T) 
	{
		Errorf("Unable to create temp output file %s",tempname);
		return 1;
	}
	T->Write("TEMP",4);  // Offset of zero in temp data structure is not supported

	DirEntry *root = NULL;
	char filename[g_MaxBanks];
	u32 totalSize = 0;
	u32 compSize = 4;

	for (int i=0; i<g_NumFiles; ++i)
	{
		formatf(filename, sizeof(filename), "%s", g_BankArray[i]);
		if (filename[0] == ';')	// comment character
			continue;
		if (filename[1] == ':')	 // ignore drive letters
		{
			Printf("Ignoring name with drive letter: %s",filename);
			continue;
		}
		
		const char *prettyName = strrchr(filename,'\\');
		if (!prettyName)
			prettyName = strrchr(filename,'/');

		if (!prettyName)
			prettyName = filename;
		else
			++prettyName;

		const char * pch;
		pch=strstr(prettyName,".tmp");

		if(pch)
		{
			return 1;
		}

		if (fiDeviceLocal::GetInstance().GetAttributes(filename) & FILE_ATTRIBUTE_DIRECTORY)
			continue;

		fiStream *thisFile = fiStream::Open(filename);
		if (!thisFile) 
		{
			Printf("Unable to open archive file %s",packName);
			continue;
		}
		
		u32 thisSize = thisFile->Size();
		u8 *src = new u8[thisSize];
		thisFile->Read(src,thisSize);
		thisFile->Close();

		

		u32 destSize;
		u8* dest;

		totalSize += thisSize;
		dest = src;
		destSize = thisSize;

		if (T->Write(dest,destSize) != (int)destSize) 
		{
			Printf("Short write on temp file");
			return 1;
		}

		DirEntry::Insert(&root,filename[0]=='.'&&strchr("/\\",filename[1])?filename+2:filename,compSize,destSize,thisSize,keepNameHeap,NULL);
		delete[] dest;
		compSize += destSize;
	}

	Assert(T->Tell() == (int)compSize);

	// build name heap
	int entryCount = 0, nameHeapSize = 0;
	DirEntry::ComputeSize(root,entryCount,nameHeapSize);
	fiPackEntry *entries = new fiPackEntry[entryCount+1];

	// Make sure there's 2k worth of zeroes at the end so padding to a sector boundary is easy
	char *nameHeap = new char[nameHeapSize+2+2047];
	memset(nameHeap,0,nameHeapSize+2+2047);
	int entryOffset = 1, heapOffset = 2;
	strcpy(nameHeap,"/");

	// Build root directory.
	entries[0].u.directory.m_DirectoryIndex = 1;
	entries[0].u.directory.m_IsDir = 1;
	entries[0].u.directory.m_DirectoryCount = entries[0].s.any.m_UncompSize = DirEntry::BuildTree(root,entries,nameHeap,entryOffset,heapOffset,keepNameHeap);
	Assert(entryCount+1 == entryOffset);
	delete root;

	++entryCount;
	nameHeapSize = heapOffset;

	// Compute total header size now
	u32 exactWriteOffset = sizeof(fiPackHeader) + entryCount*sizeof(fiPackEntry) + nameHeapSize;
	exactWriteOffset = (exactWriteOffset +15) & ~15;
	u32 writeOffset = (exactWriteOffset + 2047) & ~2047;
	u32 zeroSlop =  writeOffset -exactWriteOffset;

	// Build the output archive header
	char outFile[g_MaxBanks];
	formatf(outFile, sizeof(outFile), "%s\\%s.rpf", outPath, g_PackName);
	fiSafeStream O(fiStream::Create(outFile));
	if (!O) 
	{
		Printf("%s: Cannot create!",outFile);
		return 1;
	}

	// Write the header
	fiPackHeader hdr;
	memset(&hdr,0,sizeof(hdr));
	hdr.m_Magic = keepNameHeap ? MAKE_MAGIC_NUMBER('R','P','F','2') : MAKE_MAGIC_NUMBER('R','P','F','3');
	hdr.m_TotalSize = writeOffset - 2048;
	hdr.m_EntryCount = entryCount;
	hdr.m_WastedSpace = 0;

	if(bShouldEncrypt)
	{
	hdr.m_Encrypted = ~0U;
	}

	O->WriteInt(&hdr.m_Magic,sizeof(hdr)/4);

	// Write the entries (we will revisit these once we have final addresses)
	O->WriteInt(&entries->m_NameOffsetOrHashCode,entryCount*4);

	AES aes;
	if(bShouldEncrypt)
	{		
		aes.Encrypt(nameHeap,writeOffset - 2048 - entryCount*16 - zeroSlop);
	}

	O->Write(nameHeap,writeOffset - 2048 - entryCount*16);

	if(bShouldEncrypt)
	{
		aes.Decrypt(nameHeap,writeOffset - 2048 - entryCount*16 - zeroSlop);
	}

	Assert((u32)O->Tell() == writeOffset);

	// Copy the file contents and assign final offsets
	static char padding[2048];
	for (int i=0; i<entryCount; i++) 
	{
		fiPackEntry &pe = entries[i];
		if (pe.IsDir())
			continue;

		// If we have extra space and the file doesn't fit in the extra space,
		// pad back out to a 2k boundary again
		u32 slop = 2048 - (writeOffset & 2047);
		if (slop != 2048 && (pe.GetConsumedSize() > slop) || pe.IsResource()) 
		{
			if (O->Write(padding,slop) != (int)slop) 
			{
				Printf("Short write on internal padding");
				return 1;
			}
			writeOffset += slop;
		}

		// Displayf("Copying [%s]",nameHeap + pe.m_NameOffset);
		// Copy the file into its final position
		u8 *src = new u8[pe.GetConsumedSize()];
		T->Seek(pe.GetFileOffset());
		if (T->Read(src,pe.GetConsumedSize()) != (int)pe.GetConsumedSize()) 
		{
			Printf("Short read on temp file");
			return 1;
		}
		
		pe.SetFileOffset(writeOffset);
		Assert(!(writeOffset & 2047) || pe.GetConsumedSize() <= 2047);
		if (O->Write(src,pe.GetConsumedSize()) != (int)pe.GetConsumedSize()) 
		{
			Printf("Short write on output archive");
			return 1;
		}
		delete[] src;

		writeOffset += pe.GetConsumedSize();
	}

	T->Close();
	T = NULL;
	fiDeviceLocal::GetInstance().Delete(tempname);

	// Pad final archive
	int slop = 2048 - (writeOffset & 2047);
	if (slop != 2048) 
	{
		O->Write(padding,slop);
		writeOffset += slop;
	}

	// Rewrite the nameheap now that we have final offsets assigned.
	O->Seek(2048);
	if(bShouldEncrypt)
	{
		aes.Encrypt(&entries->m_NameOffsetOrHashCode,entryCount*16);
	}
	
	O->Write(&entries->m_NameOffsetOrHashCode,entryCount*16);
	
	if(bShouldEncrypt)
	{
		aes.Decrypt(&entries->m_NameOffsetOrHashCode,entryCount*16);
	}
	return 0;
}
