// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "ziploader.h"
#include "directory.h"

#include "file/packfile.h"
#include "file/stream.h"
#include "string/string.h"

#include "system/magicnumber.h"

#include <stdlib.h>

namespace rage 
{
#if 0
const u32 ZIP_ENTRY_MAGIC = 0x02014b50;
const u32 ZIP_CENTRAL_MAGIC = 0x06054b50;


zipLoader::zipLoader() : m_Entries(NULL), m_NameHeap(NULL) { }


zipLoader::~zipLoader() 
{
	delete[] m_Entries;
	delete[] m_NameHeap;
}


fiStream *zipLoader::Load(const char *filename)
{
	fiStream *S = fiStream::Open(filename,true);

	if (!S) 
	{
		Warningf("%s: File not found.",filename);
		return NULL;
	}

	S->Seek(S->Size() - 22);

	u32 magic = 0, size, offset;
	u16 temp, temp2;
	S->ReadInt(&magic,1);
	if (magic != ZIP_CENTRAL_MAGIC) 
	{
		Warningf("%s: Central directory signature not found (zipfile comments not supported).",filename);
		S->Close();
		return NULL;
	}

	S->ReadShort(&temp,1);	// number of this disk 
	S->ReadShort(&temp2,1);	// number of the disk with the start of the central directory
	if (temp != temp2) 
	{
		Warningf("%s: Multi-part zipfiles not supported.",filename);
		S->Close();
		return NULL;
	}

	S->ReadShort(&temp,1);	// total number of entries in the central dir on this disk 
	S->ReadShort(&temp2,1);	// total number of entries in the central dir
	Assert(temp == temp2);
	S->ReadInt(&size,1);	// size of the central directory
	S->ReadInt(&offset,1);	// offset of start of central directory with respect to the starting disk number        

	// int fileCount = temp;
	DirEntry * root = NULL;
	S->Seek(offset);

	int count = 0;
	// unsigned heapTop = 0;
	while (S->ReadInt(&magic,1) && magic == ZIP_ENTRY_MAGIC) {
		/*
		version made by                 2 bytes
		version needed to extract       2 bytes
		general purpose bit flag        2 bytes
		compression method              2 bytes
		last mod file time              2 bytes
		last mod file date              2 bytes
		crc-32                          4 bytes
		compressed size                 4 bytes
		uncompressed size               4 bytes
		filename length                 2 bytes
		extra field length              2 bytes
		file comment length             2 bytes
		disk number start               2 bytes
		internal file attributes        2 bytes
		external file attributes        4 bytes
		relative offset of local header 4 bytes == (11*2 + 5*4 = 42 bytes; but we also need \0 terminator, so it's 41 bytes)

		filename (variable size)
		extra field (variable size)
		file comment (variable size)
		*/
		char junk[32];
		char name[256], extra[256], comment[256];
		u16 filenameLen, extraLen, cmtLen, method;
		u32 compLen, uncompLen;
		int relOffs;
		S->Read(junk,6);
		S->ReadShort(&method,1);
		if (method != 0 && method != Z_DEFLATED) 
		{
			Warningf("%s: Compression method besides store or deflate encountered.",filename);
			S->Close();
			return NULL;
		}

		S->Read(junk,8);
		S->ReadInt(&compLen,1);
		S->ReadInt(&uncompLen,1);
		S->ReadShort(&filenameLen,1);
		S->ReadShort(&extraLen,1);
		S->ReadShort(&cmtLen,1);
		S->Read(junk,2 + 2 + 4);
		S->ReadInt(&relOffs,1);
		Assert(filenameLen < sizeof(name));
		Assert(extraLen < sizeof(extra));
		Assert(cmtLen < sizeof(comment));
		S->Read(name,filenameLen); 
		bool isDir = name[filenameLen-1] == '/';
		name[filenameLen++] = 0;
		S->Read(extra,extraLen); // if (extraLen) Displayf("extraLen=%d",extraLen);
		S->Read(comment,cmtLen); // if (cmtLen) Displayf("cmtLen=%d",cmtLen);

		int payload = 0;
		if (!isDir) 
		{
			int prev = S->Tell();
			S->Seek(relOffs);
			S->Read(junk,26);
			S->ReadShort(&filenameLen,1);
			S->ReadShort(&extraLen,1);
			payload = S->Tell() + filenameLen + extraLen;
			S->Seek(prev);
		}
		DirEntry::Insert(&root,name,payload,compLen,uncompLen,false,NULL);

		++count;
	}

	// DirEntry::Dump(root,0);
	int entryCount = 0, nameHeapSize = 0;
	DirEntry::ComputeSize(root,entryCount,nameHeapSize);

	// Displayf("%d nodes in tree, %d bytes in nameheap",entryCount,nameHeapSize);

	m_Entries = new fiPackEntry[m_EntryCount = entryCount+1];

	// Make sure there's 2k worth of zeroes at the end so padding to a sector boundary is easy
	m_NameHeap = new char[nameHeapSize+2+2047];
	memset(m_NameHeap,0,nameHeapSize+2+2047);
	int entryOffset = 1, heapOffset = 2;
	strcpy(m_NameHeap,"/");

	// Build root directory.
	m_Entries[0].m_NameOffset = fiPackEntry::c_IsDir | 0;
	m_Entries[0].m_FileOffset = 1;
	m_Entries[0].m_CompSize = m_Entries[0].m_UncompSize = DirEntry::BuildTree(root,m_Entries,m_NameHeap,entryOffset,heapOffset);
	Assert(entryCount+1 == entryOffset);

	// Assert(nameHeapSize+2 == heapOffset);
	m_NameHeapSize = heapOffset;
	delete root;

	// Compute total header size now
	m_WriteOffset = sizeof(fiPackHeader) + m_EntryCount*sizeof(fiPackEntry) + m_NameHeapSize;
	m_WriteOffset = (m_WriteOffset + 2047) & ~2047;

	Displayf("%d entries in archive, %d total bytes in name heap",m_EntryCount,m_NameHeapSize);

	return S;
}


fiStream *zipLoader::Create(const char *filename) 
{
	fiStream *S = fiStream::Create(filename);
	if (!S) 
	{
		Errorf("%s: Cannot create!",filename);
		return NULL;
	}

	// Write the header
	fiPackHeader hdr;
	memset(&hdr,0,sizeof(hdr));
	hdr.m_Magic = MAKE_MAGIC_NUMBER('R','P','F','0');
	hdr.m_TotalSize = m_WriteOffset - 2048;
	hdr.m_EntryCount = m_EntryCount;
	hdr.m_WastedSpace = 0;
	S->WriteInt(&hdr.m_Magic,sizeof(hdr)/4);

	// Write the entries (we will revisit these once we have final addresses)
	S->WriteInt(&m_Entries->m_NameOffset,m_EntryCount*4);
	S->Write(m_NameHeap,m_WriteOffset - 2048 - m_EntryCount*16);
	Assert(S->Tell() == (int)m_WriteOffset);

	return S;
}


void zipLoader::Copy(fiStream *input,fiStream *output)
{
	static char padding[2048];
	int lastPct = -1;

	for (int i=0; i<m_EntryCount; i++) 
	{
		fiPackEntry &pe = m_Entries[i];

		int pct = (i * 100) / m_EntryCount;
		if (pct != lastPct)
			Printf("%d%% complete...\r",lastPct = pct);

		if (pe.IsDir())
			continue;

		// If we have extra space and the file doesn't fit in the extra space,
		// pad back out to a 2k boundary again
		int slop = 2048 - (m_WriteOffset & 2047);
		if (slop != 2048 && (pe.m_CompSize > slop)) 
		{
			output->Write(padding,slop);
			m_WriteOffset += slop;
		}

		// Copy the file into its final position
		char *temp = new char[pe.m_CompSize];
		input->Seek(pe.m_FileOffset);
		input->Read(temp,pe.m_CompSize);
		pe.m_FileOffset = m_WriteOffset;
		Assert(!(m_WriteOffset & 2047) || pe.m_CompSize <= 2047);
		output->Write(temp,pe.m_CompSize);
		delete[] temp;

		m_WriteOffset += pe.m_CompSize;
	}

	// Pad final archive
	int slop = 2048 - (m_WriteOffset & 2047);
	if (slop != 2048) 
	{
		output->Write(padding,slop);
		m_WriteOffset += slop;
	}
}


void zipLoader::Finish(fiStream *output) 
{
	// Rewrite the nameheap now that we have final offsets assigned.
	output->Seek(2048);
	output->WriteInt(&m_Entries->m_NameOffset,m_EntryCount*4);
}
#endif

}	// namespace rage


