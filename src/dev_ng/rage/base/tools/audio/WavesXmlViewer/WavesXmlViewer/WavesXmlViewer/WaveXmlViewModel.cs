﻿// -----------------------------------------------------------------------
// <copyright file="WaveXmlViewModel.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace WavesXmlViewer
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Data;
    using System.Xml.Linq;

    using Microsoft.Win32;

    /// <summary>
    /// Wave XML view model.
    /// </summary>
    public class WaveXmlViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WaveXmlViewModel"/> class.
        /// </summary>
        public WaveXmlViewModel()
        {
            this.Items = new ObservableCollection<XElement>();
        }

        /// <summary>
        /// Gets the current XML document.
        /// </summary>
        public XDocument XmlDoc { get; private set; }

        /// <summary>
        /// Gets the items.
        /// </summary>
        public ObservableCollection<XElement> Items { get; private set; }

        /// <summary>
        /// Gets the file path.
        /// </summary>
        public string FilePath { get; private set; }

        /// <summary>
        /// Load the file.
        /// </summary>
        public void LoadFile()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "XML files (*.xml)|*.xml",
                Multiselect = false
            };

            if (dialog.ShowDialog() == true)
            {
                this.FilePath = dialog.FileName;

                var stream = dialog.OpenFile();
                this.XmlDoc = XDocument.Load(stream);

                this.Items.Clear();
                foreach (var waveElem in this.XmlDoc.Descendants("Wave").ToList())
                {
                    this.Items.Add(waveElem);
                }
            }
        }
    }
}
