﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WaveXmlView.xaml.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Interaction logic for WaveXmlView.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WavesXmlViewer
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for WaveXmlView.xaml
    /// </summary>
    public partial class WaveXmlView : Window
    {
        /// <summary>
        /// The view model.
        /// </summary>
        private readonly WaveXmlViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveXmlView"/> class.
        /// </summary>
        public WaveXmlView()
        {
            this.InitializeComponent();
            this.viewModel = new WaveXmlViewModel();
            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// Browse button click handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void BrowseButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.viewModel.LoadFile();
        }

        /// <summary>
        /// Update search results as user types.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void SearchTextBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            
        }
    }
}
