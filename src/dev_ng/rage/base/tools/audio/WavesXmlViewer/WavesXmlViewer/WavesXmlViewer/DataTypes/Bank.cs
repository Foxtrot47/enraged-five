﻿// -----------------------------------------------------------------------
// <copyright file="Bank.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace WavesXmlViewer.DataTypes
{
    using System.Collections.Generic;
    using System.Xml.Linq;

    /// <summary>
    /// Bank details.
    /// </summary>
    public class Bank
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bank"/> class.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        public Bank(XElement node)
        {
            this.Node = node;

            var nameAttr = node.Attribute("name");
            if (null != nameAttr)
            {
                this.Name = nameAttr.Value;
            }

            this.Waves = new List<Wave>();

            foreach (var waveElem in node.Descendants("Wave"))
            {
                this.Waves.Add(new Wave(this, waveElem));
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the node.
        /// </summary>
        public XElement Node { get; private set; }

        /// <summary>
        /// Gets the waves.
        /// </summary>
        public IList<Wave> Waves { get; private set; }
    }
}
