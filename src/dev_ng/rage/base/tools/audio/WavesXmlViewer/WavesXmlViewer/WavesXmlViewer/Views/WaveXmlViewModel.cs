﻿// -----------------------------------------------------------------------
// <copyright file="WaveXmlViewModel.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace WavesXmlViewer.Views
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Xml.Linq;

    using Microsoft.Win32;

    using WavesXmlViewer.DataTypes;
    using WavesXmlViewer.Utils;

    /// <summary>
    /// Wave XML view model.
    /// </summary>
    public class WaveXmlViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WaveXmlViewModel"/> class.
        /// </summary>
        public WaveXmlViewModel()
        {
            this.AllBanks = new ObservableCollection<Bank>();
            this.VisibleBanks = new ObservableCollection<Bank>();
            this.AllWaves = new ObservableCollection<Wave>();
            this.VisibleWaves = new ObservableCollection<Wave>();
        }

        /// <summary>
        /// Gets the current XML document.
        /// </summary>
        public XDocument XmlDoc { get; private set; }

        /// <summary>
        /// Gets all the banks.
        /// </summary>
        public ObservableCollection<Bank> AllBanks { get; private set; }

        /// <summary>
        /// Gets the visible banks.
        /// </summary>
        public ObservableCollection<Bank> VisibleBanks { get; private set; }

        /// <summary>
        /// Gets all the  waves.
        /// </summary>
        public ObservableCollection<Wave> AllWaves { get; private set; }

        /// <summary>
        /// Gets the visible waves.
        /// </summary>
        public ObservableCollection<Wave> VisibleWaves { get; private set; }

        /// <summary>
        /// Gets the file path.
        /// </summary>
        public string FilePath { get; private set; }

        /// <summary>
        /// Gets the bank filer text.
        /// </summary>
        public string BankFilterText { get; private set; }

        /// <summary>
        /// Gets the wave filter text.
        /// </summary>
        public string WaveFilterText { get; private set; }

        /// <summary>
        /// Load the file.
        /// </summary>
        public void LoadFile()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "XML files (*.xml)|*.xml",
                Multiselect = false
            };

            if (dialog.ShowDialog() == true)
            {
                this.FilePath = dialog.FileName;

                var stream = dialog.OpenFile();
                this.XmlDoc = XDocument.Load(stream);

                this.AllBanks.Clear();
                this.AllWaves.Clear();

                foreach (var bankElem in this.XmlDoc.Descendants("Bank").ToList())
                {
                    var bank = new Bank(bankElem);
                    this.AllBanks.Add(bank);
                    foreach (var wave in bank.Waves)
                    {
                        this.AllWaves.Add(wave);
                    }
                }
            }

            this.FilterBanks(string.Empty);
            this.FilterWaves(string.Empty);
        }

        public void Filter(string text)
        {
            this.FilterBanks(text);
            this.FilterWaves(text);
        }

        private void FilterBanks(string text)
        {
            var reset = string.IsNullOrEmpty(text);

            if (!reset && string.Compare(text, this.BankFilterText, System.StringComparison.OrdinalIgnoreCase) == 0)
            {
                return;
            }

            this.BankFilterText = text;
            this.VisibleBanks.Clear();

            foreach (var bank in this.AllBanks)
            {
                if (reset || StringUtils.ContainsIgnoreCase(bank.Node.ToString(), text))
                {
                    this.VisibleBanks.Add(bank);
                }
            }
        }

        private void FilterWaves(string text)
        {
            var reset = string.IsNullOrEmpty(text);

            if (!reset && string.Compare(text, this.WaveFilterText, System.StringComparison.OrdinalIgnoreCase) == 0)
            {
                return;
            }

            this.WaveFilterText = text;
            this.VisibleWaves.Clear();

            foreach (var wave in this.AllWaves)
            {
                if (reset || StringUtils.ContainsIgnoreCase(wave.Node.ToString(), text))
                {
                    this.VisibleWaves.Add(wave);
                }
            }
        }
    }
}
