﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WaveXmlView.xaml.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Interaction logic for WaveXmlView.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WavesXmlViewer.Views
{
    using System.Windows;
    using System.Windows.Data;

    using WavesXmlViewer.DataTypes;

    /// <summary>
    /// Interaction logic for WaveXmlView.xaml
    /// </summary>
    public partial class WaveXmlView : Window
    {
        /// <summary>
        /// The view model.
        /// </summary>
        private readonly WaveXmlViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveXmlView"/> class.
        /// </summary>
        public WaveXmlView()
        {
            this.InitializeComponent();
            this.viewModel = new WaveXmlViewModel();
            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// Browse button click handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void BrowseButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.viewModel.LoadFile();
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            var bank = e.Item as Bank;
            if (bank != null)
            {
                /*if (this.cbCompleteFilter.IsChecked == true && bank.Complete == true)
                    e.Accepted = false;
                else
                    e.Accepted = true;*/
            }
        }
    }
}
