﻿// -----------------------------------------------------------------------
// <copyright file="StringUtils.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace WavesXmlViewer.Utils
{
    using System;

    /// <summary>
    /// String utils.
    /// </summary>
    public class StringUtils
    {
        public static bool ContainsIgnoreCase(string source, string toCheck)
        {
            return source.IndexOf(toCheck, StringComparison.InvariantCultureIgnoreCase) >= 0;
        }
    }
}
