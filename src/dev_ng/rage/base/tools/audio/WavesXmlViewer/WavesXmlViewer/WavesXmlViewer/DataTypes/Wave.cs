﻿// -----------------------------------------------------------------------
// <copyright file="Wave.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace WavesXmlViewer.DataTypes
{
    using System.Xml.Linq;

    /// <summary>
    /// Wave details.
    /// </summary>
    public class Wave
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Wave"/> class.
        /// </summary>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        public Wave(Bank bank, XElement node)
        {
            this.Bank = bank;
            this.Node = node;

            var nameAttr = node.Attribute("name");
            if (null != nameAttr)
            {
                this.Name = nameAttr.Value;
            }

            var nameHashAttr = node.Attribute("nameHash");
            if (null != nameAttr)
            {
                this.NameHash = nameHashAttr.Value;
            }

            var sourceSizeAttr = node.Attribute("sourceSize");
            if (null != nameAttr)
            {
                this.SourceSize = uint.Parse(sourceSizeAttr.Value);
            }

            var sourceSampleRateAttr = node.Attribute("sourceSampleRate");
            if (null != sourceSampleRateAttr)
            {
                this.SourceSampleRate = uint.Parse(sourceSampleRateAttr.Value);
            }

            var builtSampleRateAttr = node.Attribute("builtSampleRate");
            if (null != builtSampleRateAttr)
            {
                this.BuiltSampleRate = uint.Parse(builtSampleRateAttr.Value);
            }

            var builtSizeAttr = node.Attribute("builtSize");
            if (null != builtSizeAttr)
            {
                this.BuiltSize = uint.Parse(builtSizeAttr.Value);
            }

            var builtNamehAttr = node.Attribute("builtName");
            if (null != builtNamehAttr)
            {
                this.BuiltName = builtNamehAttr.Value;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the name hash.
        /// </summary>
        public string NameHash { get; private set; }

        /// <summary>
        /// Gets the source size.
        /// </summary>
        public uint SourceSize { get; private set; }

        /// <summary>
        /// Gets the source sample rate.
        /// </summary>
        public uint SourceSampleRate { get; private set; }

        /// <summary>
        /// Gets the built sample rate.
        /// </summary>
        public uint BuiltSampleRate { get; private set; }

        /// <summary>
        /// Gets the built size.
        /// </summary>
        public uint BuiltSize { get; private set; }

        /// <summary>
        /// Gets the built name.
        /// </summary>
        public string BuiltName { get; private set; }

        /// <summary>
        /// Gets the node.
        /// </summary>
        public XElement Node { get; private set; }

        /// <summary>
        /// Gets the bank.
        /// </summary>
        public Bank Bank { get; private set; }
    }
}
