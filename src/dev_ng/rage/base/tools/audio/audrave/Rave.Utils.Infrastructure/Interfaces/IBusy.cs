﻿// -----------------------------------------------------------------------
// <copyright file="IBusy.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Utils.Infrastructure.Interfaces
{
    using System;

    /// <summary>
    /// IBusy interface.
    /// </summary>
    public interface IBusy
    {
        #region Public Events

        /// <summary>
        /// The cancelled.
        /// </summary>
        event Action Cancelled;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The finished working.
        /// </summary>
        void FinishedWorking();

        /// <summary>
        /// Set the progress.
        /// </summary>
        /// <param name="percentage">
        /// The percentage.
        /// </param>
        void SetProgress(int percentage);

        /// <summary>
        /// Set the status text.
        /// </summary>
        /// <param name="strAction">
        /// The action string.
        /// </param>
        void SetStatusText(string strAction);

        /// <summary>
        /// Set unknown duration.
        /// </summary>
        /// <param name="flag">
        /// The flag.
        /// </param>
        void SetUnknownDuration(bool flag);

        void Show();

        void Hide();

        void Close();

        #endregion
    }
}
