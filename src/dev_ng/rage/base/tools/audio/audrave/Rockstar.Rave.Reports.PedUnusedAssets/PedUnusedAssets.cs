﻿// -----------------------------------------------------------------------
// <copyright file="PedUnusedAssets.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.PedUnusedAssets
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;
    using global::Rave.Plugins.Infrastructure.Interfaces;
    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    /// <para>
    /// Report that generates a list of unused ped speech assets.
    /// </para>
    /// <para>
    /// Report includes:
    /// - Any voices that are referenced by a PVG, but where the voice doesn't exist.
    /// - Any PVGs that are referenced by a PedRaceToPVG, but where the PVG doesn't exist.
    /// - Any PVGs that are not referenced by a PedRaceToPVG (unless referenced directly by peds.meta).
    /// - Any PedRaceToPVGs not referenced anywhere in peds.meta.
    /// - Any PVGs that are referenced in peds.meta, but where the PVG doesn't exist.
    /// - Any PedRaceToPVGs that are referenced in peds.meta, but where the PedRaceToPVG doesn't exist.
    /// </para>
    /// </summary>
    public class PedUnusedAssets : IRAVEReportPlugin
    {
        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// The game objects depot path.
        /// </summary>
        private string gameObjectsDepotPath;

        /// <summary>
        /// The ped game data path.
        /// </summary>
        private string pedGameDataDepotPath;

        /// <summary>
        /// The built waves depot path.
        /// </summary>
        private string builtWavesDepotPath;

        /// <summary>
        /// The built waves local path.
        /// </summary>
        private string builtWavesLocalPath;

        /// <summary>
        /// The built waves pack list file name.
        /// </summary>
        private string builtWavesPackListFileName;

        /// <summary>
        /// Built wave packs to ignore during report generation.
        /// </summary>
        private HashSet<string> builtWavesSkipPacks;  

        /// <summary>
        /// The asset manager.
        /// </summary>
        private IAssetManager assetManager;

        /// <summary>
        /// The built waves pack file names.
        /// </summary>
        private HashSet<string> builtWavesPackFileNames;

        /// <summary>
        /// The PVGs.
        /// </summary>
        private HashSet<string> pvgs;

        /// <summary>
        /// The race-to-ped PVGs.
        /// </summary>
        private HashSet<string> pedRaceToPedPvgs; 

        /// <summary>
        /// The voice tags (found in built waves).
        /// </summary>
        private HashSet<string> voices;

        /// <summary>
        /// PVGs that are referenced by peds (specified in pedgameData file).
        /// </summary>
        private HashSet<string> pvgsReferencedByPeds;

        /// <summary>
        /// The ped race to pvgs referenced by peds.
        /// </summary>
        private HashSet<string> pedRaceToPvgsReferencedByPeds;

        /// <summary>
        /// Voices referenced by PVGs.
        /// </summary>
        private HashSet<string> voicesReferencedByPvgs;

        /// <summary>
        /// PVGs referenced by PedRaceToPVGs.
        /// </summary>
        private HashSet<string> pvgsReferencedByPedRaceToPvgs; 

        /// <summary>
        /// The get report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            this.builtWavesSkipPacks = new HashSet<string>();

            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;

                    case "GameObjectsDepotPath":
                        this.gameObjectsDepotPath = setting.InnerText;
                        break;

                    case "PedGameDataDepotPath":
                        this.pedGameDataDepotPath = setting.InnerText;
                        break;

                    case "BuiltWaves":
                        foreach (XmlNode childNode in setting.ChildNodes)
                        {
                            switch (childNode.Name)
                            {
                                case "DepotPath":
                                    this.builtWavesDepotPath = childNode.InnerText;
                                    break;

                                case "PackListFileName":
                                    this.builtWavesPackListFileName = childNode.InnerText;
                                    break;

                                case "Skip":
                                    this.builtWavesSkipPacks.Add(childNode.InnerText.ToLower());
                                    break;
                            }
                        }

                        break;
                }
            }

            // Validate args
            if (string.IsNullOrEmpty(this.name))
            {
                throw new Exception("Report name required");
            }

            if (string.IsNullOrEmpty(this.gameObjectsDepotPath))
            {
                throw new Exception("Game object depot path required");
            }

            if (string.IsNullOrEmpty(this.pedGameDataDepotPath))
            {
                throw new Exception("Ped game data depot path required");
            }

            if (string.IsNullOrEmpty(this.builtWavesDepotPath))
            {
                throw new Exception("Built waves depot path required");
            }

            // Get instance of asset manager
            this.assetManager = AssetManagerFactory.GetInstance(AssetManagerType.Perforce);

            if (null == this.assetManager)
            {
                throw new Exception("Failed to get instance of Perforce asset manager");
            }

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes (not required).
        /// </param>
        /// <param name="browsers">
        /// The sound browsers (not required).
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            // Get latest data
            if (!this.GetLatestData())
            {
                throw new Exception("Failed to get latest data required to generate report");
            }

            // Load data
            this.LoadPedGameData();
            this.LoadPackListNames();
            this.LoadVoiceData();

            if (!this.LoadGameObjectDoc())
            {
                throw new Exception("Failed to initialize game object XDocument");
            }

            // Generate and display results
            this.DisplayResults();
        }

        /// <summary>
        /// Get latest data from asset manager.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GetLatestData()
        {
            // Get latest ped game data file
            if (!this.assetManager.GetLatest(this.pedGameDataDepotPath, false))
            {
                throw new Exception("Failed to get latest ped game data: " + this.pedGameDataDepotPath);
            }

            // Get latest game object data files
            var gameObjectsFolder = this.gameObjectsDepotPath;
            if (!gameObjectsFolder.EndsWith("/"))
            {
                gameObjectsFolder += "/";
            }

            gameObjectsFolder += "...";

            if (!this.assetManager.GetLatest(gameObjectsFolder, false))
            {
                return false;
            }

            // Get latest built waves data files
            var builtWaveFolder = this.builtWavesDepotPath;
            if (!builtWaveFolder.EndsWith("/"))
            {
                builtWaveFolder += "/";
            }

            builtWaveFolder += "...";

            if (!this.assetManager.GetLatest(builtWaveFolder, false))
            {
                return false;
            }

            this.builtWavesLocalPath = this.assetManager.GetLocalPath(this.builtWavesDepotPath);

            return true;
        }

        /// <summary>
        /// Load the ped game data.
        /// </summary>
        private void LoadPedGameData()
        {
            this.pvgsReferencedByPeds = new HashSet<string>();
            this.pedRaceToPvgsReferencedByPeds = new HashSet<string>();

            var localPath = this.assetManager.GetLocalPath(this.pedGameDataDepotPath);

            if (string.IsNullOrEmpty(localPath))
            {
                throw new Exception("Failed to get local path for game file: " + this.pedGameDataDepotPath);
            }

            // Load the XML file
            var pedDoc = XDocument.Load(localPath);

            var modelInfoElem = pedDoc.Element("CPedModelInfo__InitDataList");
            var dataElem = modelInfoElem.Element("InitDatas");
            var pedElems = dataElem.Elements("Item");
            
            foreach (var pedElem in pedElems)
            {
                // Find and store all PVG and P2PVG references
                var pvgElem = pedElem.Element("PedVoiceGroup");

                if (null == pvgElem)
                {
                    continue;
                }

                var pvgName = pvgElem.Value.ToLower();

                // Separate PVG from R2PVG to help with report generation
                if (pvgName.EndsWith("_r2pvg"))
                {
                    this.pedRaceToPvgsReferencedByPeds.Add(pvgName);
                }
                else
                {
                    this.pvgsReferencedByPeds.Add(pvgName);
                }
            }
        }

        /// <summary>
        /// Load the pack list names from the pack list file.
        /// </summary>
        private void LoadPackListNames()
        {
            this.builtWavesPackFileNames = new HashSet<string>();

            // Obtain pack list file path
            var packListPath = Path.Combine(this.builtWavesLocalPath, this.builtWavesPackListFileName);

            if (!File.Exists(packListPath))
            {
                throw new Exception("Failed to find built waves pack list file: " + packListPath);
            }

            // Load pack list file
            var file = XDocument.Load(packListPath);
            foreach (var packFileElem in file.Descendants("PackFile"))
            {
                this.builtWavesPackFileNames.Add(packFileElem.Value);
            }
        }

        /// <summary>
        /// Load the voice data.
        /// </summary>
        private void LoadVoiceData()
        {
            this.voices = new HashSet<string>();

            // Locate and store all voice tags found
            foreach (var fileName in this.builtWavesPackFileNames)
            {
                // Skip current file if its name starts with any of the
                // names marked to skip
                if (this.builtWavesSkipPacks.Any(fileName.ToLower().StartsWith))
                {
                    continue;
                }

                var filePath = Path.Combine(this.builtWavesLocalPath, fileName);
                var file = XDocument.Load(filePath);

                // Find and store all voice tag values in file
                foreach (var tagElem in file.Descendants("Tag"))
                {
                    var nameAttr = tagElem.Attribute("name");
                    if (null != nameAttr && nameAttr.Value.ToLower().Equals("voice"))
                    {
                        var valueAttr = tagElem.Attribute("value");
                        if (null != valueAttr)
                        {
                            this.voices.Add(valueAttr.Value.ToLower());
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Load all game object XML files and find and store all
        /// PVG and PedRaceToPVG references.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadGameObjectDoc()
        {
            this.pvgs = new HashSet<string>();
            this.pedRaceToPedPvgs = new HashSet<string>();
            this.voicesReferencedByPvgs = new HashSet<string>();
            this.pvgsReferencedByPedRaceToPvgs = new HashSet<string>();

            // Get list of game object XML files
            var gameObjectsLocalPath = this.assetManager.GetLocalPath(this.gameObjectsDepotPath);

            var filePaths = Directory.GetFiles(gameObjectsLocalPath, "*.xml", SearchOption.AllDirectories);

            foreach (var filePath in filePaths)
            {
                var file = XDocument.Load(filePath);
                var currObjsElem = file.Element("Objects");

                if (null == currObjsElem)
                {
                    return false;
                }

                // Find and store names of all PedVoiceGroups
                foreach (var pvgElem in currObjsElem.Descendants("PedVoiceGroups"))
                {
                    var nameAttr = pvgElem.Attribute("name");
                    if (null != nameAttr)
                    {
                        this.pvgs.Add(nameAttr.Value.ToLower());
                    }

                    // Find and store names of voices referenced by PVGs
                    var currVoices = pvgElem.Descendants("VoiceName");
                    foreach (var voiceElem in currVoices)
                    {
                        this.voicesReferencedByPvgs.Add(voiceElem.Value.ToLower());
                    }
                }

                // Find and store names of all PedRaceToPVGs
                foreach (var pedRaceToPvgElem in currObjsElem.Descendants("PedRaceToPVG"))
                {
                    var nameAttr = pedRaceToPvgElem.Attribute("name");
                    if (null != nameAttr)
                    {
                        this.pedRaceToPedPvgs.Add(nameAttr.Value.ToLower());
                    }

                    // Find and store names of PVGs referenced by PedRaceToPVGs
                    // Can assume that all child elements contain a voice name
                    foreach (var pvgElem in pedRaceToPvgElem.Elements())
                    {
                        this.pvgsReferencedByPedRaceToPvgs.Add(pvgElem.Value.ToLower());
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Display the results to the user.
        /// </summary>
        private void DisplayResults()
        {
            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Ped Unused Assets</title></head><body>");

            // Output totals
            sw.WriteLine("<h3>Totals</h3>");
            sw.WriteLine("<ul>");
            sw.WriteLine("<li>Total Voices: " + this.voices.Count + "</li>");
            sw.WriteLine("<li>Total PVGs: " + this.pvgs.Count + "</li>");
            sw.WriteLine("<li>Total PedRaceToPVGs: " + this.pedRaceToPedPvgs.Count + "</li>");
            sw.WriteLine("<li>Total PVGs referenced in peds.meta: " + this.pvgsReferencedByPeds.Count + "</li>");
            sw.WriteLine("<li>Total PedRaceToPVGs referenced in peds.meta: " + this.pedRaceToPvgsReferencedByPeds.Count + "</li>");
            sw.WriteLine("</ul>");

            // Output voices not referenced by any PVGs
            sw.WriteLine("<h3>Unreferenced Voices</h3>");
            sw.WriteLine("<p><b>Definition: Any voices that are not referenced by a PVG.</b></p>");

            var unreferencedVoices = this.voices.Except(this.voicesReferencedByPvgs).ToList();
            unreferencedVoices.Sort();

            if (!unreferencedVoices.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<p>Count: " + unreferencedVoices.Count + "</p>");
                sw.WriteLine("<ul>");
                foreach (var voice in unreferencedVoices)
                {
                    sw.WriteLine("<li>" + voice + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            // Output invalid PVG voice references
            sw.WriteLine("<h3>Invalid PVG Voice Referencers</h3>");
            sw.WriteLine("<p><b>Definition: Any voices that are referenced by a PVG, but where the voice doesn't exist.</b></p>");

            var invalidPvgVoiceRef = this.voicesReferencedByPvgs.Except(this.voices).ToList();
            invalidPvgVoiceRef.Sort();

            if (!invalidPvgVoiceRef.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<p>Count: " + invalidPvgVoiceRef.Count + "</p>");
                sw.WriteLine("<ul>");
                foreach (var voice in invalidPvgVoiceRef)
                {
                    sw.WriteLine("<li>" + voice + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            // Output PVGs not referenced by any PedRacetoPVG
            sw.WriteLine("<h3>Unreferenced PVGs</h3>");
            sw.WriteLine("<p><b>Definition: Any PVGs that are not referenced by a PedRaceToPVG (unless referenced directly by peds.meta).</b></p>");

            var unreferencedPvgs = this.pvgs.Except(this.pvgsReferencedByPedRaceToPvgs).Except(this.pvgsReferencedByPeds).ToList();
            unreferencedPvgs.Sort();

            if (!unreferencedPvgs.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<p>Count: " + unreferencedPvgs.Count + "</p>");
                sw.WriteLine("<ul>");
                foreach (var voice in unreferencedPvgs)
                {
                    sw.WriteLine("<li>" + voice + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            // Output invalid PedToRacePVG voice references
            sw.WriteLine("<h3>Invalid PedRaceToPVG PVG Referencers</h3>");
            sw.WriteLine("<p><b>Definition: Any PVGs that are referenced by a PedRaceToPVG, but where the PVG doesn't exist.</b></p>");

            var invalidPedRaceToPvgPvgRef = this.pvgsReferencedByPedRaceToPvgs.Except(this.pvgs).ToList();
            invalidPedRaceToPvgPvgRef.Sort();

            if (!invalidPedRaceToPvgPvgRef.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<p>Count: " + invalidPedRaceToPvgPvgRef.Count + "</p>");
                sw.WriteLine("<ul>");
                foreach (var voice in invalidPedRaceToPvgPvgRef)
                {
                    sw.WriteLine("<li>" + voice + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            // Output PedRaceToPVGs not referenced anywhere in peds.meta
            sw.WriteLine("<h3>Unreferenced PedRaceToPVGs</h3>");
            sw.WriteLine("<p><b>Definition: Any PedRaceToPVGs not referenced anywhere in peds.meta.</b></p>");

            var unreferencedPedRaceToPvgs = this.pedRaceToPedPvgs.Except(this.pedRaceToPvgsReferencedByPeds).ToList();
            unreferencedPedRaceToPvgs.Sort();

            if (!unreferencedPedRaceToPvgs.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<p>Count: " + unreferencedPedRaceToPvgs.Count + "</p>");
                sw.WriteLine("<ul>");
                foreach (var item in unreferencedPedRaceToPvgs)
                {
                    sw.WriteLine("<li>" + item + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            // Output invalid PVGs references
            sw.WriteLine("<h3>Invalid PVG Referencers</h3>");
            sw.WriteLine("<p><b>Definition: Any PVGs that are referenced in peds.meta, but where the PVG doesn't exist.</b></p>");

            var invalidPvgRefs = this.pvgsReferencedByPeds.Except(this.pvgs).ToList();
            invalidPvgRefs.Sort();

            if (!invalidPvgRefs.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<p>Count: " + invalidPvgRefs.Count + "</p>");
                sw.WriteLine("<ul>");
                foreach (var item in invalidPvgRefs)
                {
                    sw.WriteLine("<li>" + item + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            // Output invalid PedRaceToPVG references
            sw.WriteLine("<h3>Invalid PedRaceToPVG Referencers</h3>");
            sw.WriteLine("<p><b>Definition: Any PedRaceToPVGs that are referenced in peds.meta, but where the PedRaceToPVG doesn't exist.</b></p>");

            var invalidPedToRacePvgs = this.pedRaceToPvgsReferencedByPeds.Except(this.pedRaceToPedPvgs).ToList();
            invalidPedToRacePvgs.Sort();

            if (!invalidPedToRacePvgs.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<p>Count: " + invalidPedToRacePvgs.Count + "</p>");
                sw.WriteLine("<ul>");
                foreach (var item in invalidPedToRacePvgs)
                {
                    sw.WriteLine("<li>" + item + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            sw.WriteLine("</body></html>");

            sw.Close();
            filesteam.Close();

            // Display report
            var report = new frmReport(this.name + " Report");
            report.SetURL(resultsFileName);
            report.Show();
        }
    }
}
