﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="rave.xaml.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The app.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using ProjectLoader;
using rage;
using rage.ToolLib;
using rage.ToolLib.CmdLine;
using rage.ToolLib.Logging;
using Rave.AssetManager;
using Rave.Controls.Forms.Popups;
using Rave.Drive;
using Rave.Instance;
using Rave.Metadata;
using Rave.Plugins;
using Rave.Types;
using Rave.Types.Infrastructure.Enums;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.Types.Infrastructure.Structs;
using Rave.Types.ObjectBanks;
using Rave.Utils;
using Rave.Utils.DataTools;
using Utility = rage.ToolLib.Utility;

namespace Rave
{
    /// <summary>
    ///     The app.
    /// </summary>
    public partial class App
    {
        /// <summary>
        ///     The auto save.
        /// </summary>
        private AutoSave _autoSave;

        private DriveWatcher _driveWatcher;

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="App" /> class.
        /// </summary>
        public App()
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            AppDomain.CurrentDomain.AssemblyResolve += AppDomain_AssemblyResolve;

            Debug.Assert(Instance == null);
            Instance = this;

            RaveInstance.RaveAssetManager = new RaveAssetManager();
        }

        private static void RunRaveBat(string filename)
        {
            var startInfo = new ProcessStartInfo { FileName = filename, UseShellExecute = true, Arguments = Process.GetCurrentProcess().Id.ToString() };
            var process = new Process
            {
                StartInfo = startInfo
            };
            process.Start();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static App Instance { get; private set; }

        /// <summary>
        ///     Gets the main window.
        /// </summary>
        public new MainWindow MainWindow { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Get latest runtime data.
        /// </summary>
        /// <param name="platform">
        ///     The platform.
        /// </param>
        /// <param name="result">
        ///     The result.
        /// </param>
        /// <returns>
        ///     Indication of success <see cref="bool" />.
        /// </returns>
        public static bool GetLatestRuntimeData(string platform, out string result)
        {
            ErrorManager.WriteToLog("Info", "Getting latest runtime data");
            result = string.Empty;
            foreach (var ps in Configuration.ActivePlatformSettings)
            {
                if ((ps.PlatformTag != platform) && (!ps.GetLatestEnabled || platform != "All Platforms"))
                {
                    continue;
                }

                // Get latest built wave assets
                try
                {
                    RaveInstance.AssetManager.GetLatest(ps.BuildOutput, false);
                    result += ps.PlatformTag + Environment.NewLine + " Get Latest Succeeded" + Environment.NewLine;
                }
                catch (Exception e)
                {
                    var error = ps.PlatformTag + Environment.NewLine + e.Message + Environment.NewLine;
                    var option =
                        MessageBox.Show(
                            string.Format(
                                "Failed to get the latest data from Perforce would you like to force sync?{0}{1}{2}",
                                Environment.NewLine,
                                Environment.NewLine,
                                error),
                            "Sync Failed",
                            MessageBoxButton.YesNo,
                            MessageBoxImage.Error,
                            MessageBoxResult.Yes);

                    if (option == MessageBoxResult.Yes)
                    {
                        try
                        {
                            RaveInstance.AssetManager.GetLatestForceErrors(ps.BuildOutput);
                            result += ps.PlatformTag + Environment.NewLine + " Get Latest Succeeded"
                                      + Environment.NewLine;
                        }
                        catch (Exception ex)
                        {
                            result = ps.PlatformTag + Environment.NewLine + ex.Message + Environment.NewLine;
                            return false;
                        }
                    }
                    else
                    {
                        result = error;
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        ///     Generate metadata required on startup.
        /// </summary>
        public void GenerateMetadata()
        {
            frmSplashScreen.SetText("Running metadata generation");
            MetadataUtils.RunMetadataGeneration(true);

            while (MetadataUtils.PendingMetadataFiles.Count > 0)
            {
                Thread.Sleep(50);
            }
        }

        /// <summary>
        ///     Load type definitions.
        /// </summary>
        public void LoadTypeDefinitions()
        {
            ErrorManager.WriteToLog("Info", "Loading object type definitions");
            frmSplashScreen.SetText("Loading object type definitions");

            foreach (var metadataType in Configuration.MetadataTypes)
            {
                var al = new List<string>();

                foreach (var objDef in metadataType.ObjectDefinitions)
                {
                    al.Add(Configuration.ObjectXmlPath + objDef.DefinitionsFile);
                }

                if (RaveInstance.AllTypeDefinitions.ContainsKey(metadataType.Type))
                {
                    RaveInstance.AllTypeDefinitions[metadataType.Type].Load(al.ToArray());
                }
                else
                {
                    var typedefs = new TypeDefinitions.TypeDefinitions(al.ToArray(), metadataType.SchemaPath);
                    RaveInstance.AllTypeDefinitions.Add(metadataType.Type, typedefs);
                }
            }

            var log = new XmlLog(new NullLogWriter(), new ContextStack(), "MetadataCompilerOutput");
            RaveInstance.XmlBankCompiler = new XmlBankMetaDataCompiler(log, Configuration.ProjectSettings);
        }

        /// <summary>
        ///     The on object select.
        /// </summary>
        /// <param name="objectInstance">
        ///     The object instance.
        /// </param>
        public void OnObjectSelect(IObjectInstance objectInstance)
        {
            if (objectInstance != null)
            {
                var typeHash = new Hash { Value = objectInstance.Type };
                var nameHash = new Hash { Value = objectInstance.Name };
                RaveInstance.RemoteControl.ViewObject(typeHash.Key, nameHash.Key);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Resolve assembly path.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="args">
        ///     The args.
        /// </param>
        /// <returns>
        ///     The assembly <see cref="Assembly" />.
        /// </returns>
        private static Assembly AppDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            var jitPaths = Configuration.MetadataCompilerJitPaths;
            if (jitPaths != null)
            {
                var name = new AssemblyName(args.Name);
                foreach (var jitPath in jitPaths)
                {
                    var assemblyFile = Path.Combine(jitPath, string.Concat(name.Name, ".dll"));
                    if (File.Exists(assemblyFile))
                    {
                        return Assembly.LoadFile(assemblyFile);
                    }
                }
            }

            return null;
        }

        /// <summary>
        ///     Get latest live data.
        /// </summary>
        /// <param name="platform">
        ///     The platform.
        /// </param>
        /// <param name="result">
        ///     The result.
        /// </param>
        /// <returns>
        ///     Indication of success <see cref="bool" />.
        /// </returns>
        private static bool GetLatestLiveData(string platform, out string result)
        {
            result = string.Empty;
            foreach (var ps in Configuration.ActivePlatformSettings)
            {
                if ((ps.PlatformTag != platform) && (!ps.GetLatestEnabled || platform != "All Platforms"))
                {
                    continue;
                }

                // Get latest built wave assets
                try
                {
                    ErrorManager.WriteToLog("Info", "Getting latest live data", ps.Name);

                    if (ps.LiveOutput != null)
                    {
                        RaveInstance.AssetManager.GetLatest(ps.LiveOutput, false);
                        result += ps.PlatformTag + Environment.NewLine + " Get Latest Succeeded"
                                  + Environment.NewLine;
                    }
                }
                catch (Exception e)
                {
                    result = ps.PlatformTag + Environment.NewLine + e.Message + Environment.NewLine;
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     Grab latest build by platform.
        /// </summary>
        /// <param name="platform">
        ///     The platform.
        /// </param>
        private static void GrabLatestBuildByPlatform(string platform)
        {
            ErrorManager.WriteToLog("Info", "Getting latest build", platform);

            string results;
            frmSplashScreen.SetText("Getting latest runtime data for " + platform);
            if (!GetLatestRuntimeData(platform, out results))
            {
                throw new ApplicationException(results);
            }

            frmSplashScreen.SetText("Getting latest live data for " + platform);
            if (!GetLatestLiveData(platform, out results))
            {
                throw new ApplicationException(results);
            }
        }

        /// <summary>
        ///     Check user has latest versions of required tools.
        /// </summary>
        /// <returns>
        ///     Value indicating whether user has latest versions <see cref="bool" />.
        /// </returns>
        private static bool HaveLatestVersions()
        {
            ErrorManager.WriteToLog("Info", "Check RAVE version");
            frmSplashScreen.SetText("Checking RAVE version");

            var assetManager = RaveInstance.AssetManager;
            var projectMetadataCompiler = Configuration.MetadataCompilerPath;

            if (assetManager.ExistsAsAsset(projectMetadataCompiler))
            {
                var path = string.Concat(Path.GetDirectoryName(projectMetadataCompiler), "\\");
                path = assetManager.GetDepotPath(path);
                try
                {
                    assetManager.GetLatestForceErrors(path);
                }
                catch (Exception ex)
                {
                    ErrorManager.HandleError("Failed to get latest version of RAVE: " + ex.Message);
                }
            }

            var uri = new Uri(Assembly.GetEntryAssembly().GetName().CodeBase);
            var exeLocation = uri.LocalPath;

            if (assetManager.ExistsAsAsset(exeLocation))
            {
                var directoryName = string.Concat(Path.GetDirectoryName(exeLocation), "\\");
                if (!assetManager.HaveLatest(directoryName))
                {
                    ErrorManager.WriteToLog("Warning", "RAVE out of date");
                    var builder = new StringBuilder();
                    builder.Append("There is a newer version of Rave available at:");
                    builder.Append(Environment.NewLine);
                    builder.Append(directoryName);
                    builder.Append(Environment.NewLine);
                    builder.Append(Environment.NewLine);
                    builder.Append("Please get the latest version from Perforce...");
                    ErrorManager.HandleInfo(builder.ToString());
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     Grab latest object data.
        /// </summary>
        private void GrabLatestObjectData()
        {
            ErrorManager.WriteToLog("Info", "Getting latest object data");
            frmSplashScreen.SetText("Getting latest object data");
            RaveInstance.AssetManager.GetLatest(Configuration.ObjectXmlPath, false);
        }

        /// <summary>
        ///     Grab latest tools.
        /// </summary>
        private void GrabLatestTools()
        {
            ErrorManager.WriteToLog("Info", "Getting latest tools");
            var buildToolsPath = Configuration.WaveBuildToolsPath;
            if (buildToolsPath != null)
            {
                frmSplashScreen.SetText("Getting latest build tools");
                RaveInstance.AssetManager.GetLatest(buildToolsPath, false);
            }
        }

        /// <summary>
        ///     Grab latest build info.
        /// </summary>
        private void GrabLatestBuildInfo()
        {
            foreach (var ps in Configuration.ActivePlatformSettings)
            {
                if (!ps.IsActive)
                {
                    continue;
                }

                ErrorManager.WriteToLog("Info", "Getting latest build info for " + ps.Name);
                frmSplashScreen.SetText("Getting latest build info for " + ps.Name);
                try
                {
                    RaveInstance.AssetManager.GetLatest(ps.BuildInfo, false);
                }
                catch (Exception e)
                {
                    var error = ps.PlatformTag + Environment.NewLine + e.Message + Environment.NewLine;
                    var option =
                        MessageBox.Show(
                            string.Format(
                                "Failed to get the latest data from Perforce would you like to force sync?{0}{1}{2}",
                                Environment.NewLine,
                                Environment.NewLine,
                                error),
                            "Sync Failed",
                            MessageBoxButton.YesNo,
                            MessageBoxImage.Error);

                    if (option != MessageBoxResult.Yes)
                    {
                        throw new Exception(error);
                    }

                    try
                    {
                        RaveInstance.AssetManager.GetLatestForceErrors(ps.BuildInfo);
                    }
                    catch (Exception ex)
                    {
                        var msg = ps.PlatformTag + Environment.NewLine + ex.Message + Environment.NewLine;
                        throw new ApplicationException(msg);
                    }
                }
            }
        }

        /// <summary>
        ///     Grab the latest builds.
        /// </summary>
        /// <param name="project">
        ///     The project.
        /// </param>
        private void GrabLatestBuilds(audProjectEntry project)
        {
            if (project.GrabLatestPC)
            {
                GrabLatestBuildByPlatform("PC");
            }

            if (project.GrabLatestx64XMA)
            {
                GrabLatestBuildByPlatform("Durango");
                GrabLatestBuildByPlatform("XBOXONE");
            }

            if (project.GrabLatestPS4)
            {
                GrabLatestBuildByPlatform("PS4");
            }
        }

        /// <summary>
        ///     Load the project.
        /// </summary>
        /// <param name="project">
        ///     The project to load.
        /// </param>
        /// <returns>
        ///     Indication of success <see cref="bool" />.
        /// </returns>
        private bool LoadProject(audProjectEntry project)
        {
            frmSplashScreen.SetText("Loading Project Settings");
            ErrorManager.WriteToLog("Info", "Loading Project Settings");

            Configuration.InitProject(project);
            if (!Configuration.LoadProjectSettings())
            {
                return false;
            }

            // Grab latest data
            GrabLatestObjectData();
            GrabLatestTools();
            GrabLatestBuildInfo();
            GrabLatestBuilds(project);

            if (!HaveLatestVersions())
            {
                return false;
            }

            if (Configuration.RavePlugins != null)
            {
                RaveInstance.PluginManager = new PluginManager(Configuration.RavePlugins);
            }

            // Load data
            LoadTypeDefinitions();
            GenerateMetadata();
            LoadTemplateData();
            LoadObjectData();

            ErrorManager.WriteToLog("Info", "Initializing rolloff curves list");
            if (RaveInstance.AllBankManagers.ContainsKey("CURVES"))
            {
                TypeUtils.RolloffCurves = RaveInstance.AllBankManagers["CURVES"].FindObjectBankByName("ROLLOFF_CURVES");
            }

            ErrorManager.WriteToLog("Info", "Computing bank references");
            foreach (var kvp in RaveInstance.AllBankManagers)
            {
                var name = kvp.Key;
                var bank = kvp.Value;
                frmSplashScreen.SetText("Computing " + name + " references");
                bank.ComputeReferences();
            }

            MainWindow = new MainWindow();
            RaveInstance.MainWindow = MainWindow;
            MainWindow.Init();
            MainWindow.LoadProject(project);


            if (_driveWatcher == null && Configuration.RaveDriveRoot != null)
            {
                frmSplashScreen.SetText("Initializing Rave Drive");
                _driveWatcher = new DriveWatcher(new DirectoryInfo(Configuration.RaveDriveRoot), RaveInstance.WaveBrowser);
            }

            return true;
        }

        /// <summary>
        ///     Load the template data.
        /// </summary>
        private void LoadTemplateData()
        {
            // Load the templates
            foreach (var metadataType in Configuration.MetadataTypes)
            {
                if (metadataType.TemplatePath == null)
                {
                    continue;
                }

                ErrorManager.WriteToLog("Info", "Loading templates", metadataType.Type);
                frmSplashScreen.SetText("Loading " + metadataType.Type + " templates");

                // Setup template browser
                var templateType = metadataType.Type.ToUpper() + "_TEMPLATE";
                var bankManager = new XmlBankManager(RaveInstance.XmlBankCompiler.MetadataManagers);
                var templatePath = Path.Combine(Configuration.ObjectXmlPath, metadataType.TemplatePath);

                // Load the banks
                bankManager.LoadBanks(
                    templatePath,
                    string.Empty,
                    metadataType.Type.ToUpper(),
                    RaveInstance.AllTypeDefinitions[metadataType.Type],
                    BankTypes.Template);

                // Add the bank managers to the bank managers list
                RaveInstance.AllBankManagers.Add(templateType, bankManager);

                // Get XML bank compiler
                var compiler = RaveInstance.XmlBankCompiler.MetadataManagers[metadataType.Type.ToUpper()].Compiler;
                compiler.ClearTemplates();

                // Load the templates using the XML bank compiler
                foreach (var bank in bankManager.Banks)
                {
                    if (bank.Document != null && bank.Document.DocumentElement != null)
                    {
                        compiler.LoadTemplates(Utility.ToXDoc(bank.Document));
                    }
                }
            }
        }

        /// <summary>
        ///     Load the object data.
        /// </summary>
        private void LoadObjectData()
        {
            foreach (var metadataFile in Configuration.MetadataFiles)
            {
                if (!metadataFile.ShowInBrowser)
                {
                    continue;
                }

                ErrorManager.WriteToLog("Info", "Loading objects", metadataFile.Type);
                frmSplashScreen.SetText("Loading " + metadataFile.Type + " objects");


                var type = metadataFile.Type.ToUpper();
                var id = new ObjectLookupId(type, metadataFile.Episode);

                // Create object lookup entry
                var lookup = new Dictionary<string, IObjectInstance>(StringComparer.InvariantCultureIgnoreCase);
                // in case of speech debug metadata there are multiple entries with the same type/episode so don't add it more than once 
                if (!RaveInstance.ObjectLookupTables.ContainsKey(id)) RaveInstance.ObjectLookupTables.Add(id, lookup);

                // Add XML bank manager if not already added
                if (!RaveInstance.AllBankManagers.ContainsKey(type))
                {
                    RaveInstance.AllBankManagers.Add(type,
                        new XmlBankManager(RaveInstance.XmlBankCompiler.MetadataManagers));
                }

                // Load the object banks
                RaveInstance.AllBankManagers[type].LoadBanks(
                    Configuration.ObjectXmlPath + metadataFile.DataPath,
                    metadataFile.Episode.ToUpper(),
                    type,
                    RaveInstance.AllTypeDefinitions[type],
                    BankTypes.Object);
            }
        }

        /// <summary>
        ///     The app_ startup.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="eventArgs">
        ///     The event args.
        /// </param>
        private void app_Startup(object sender, StartupEventArgs eventArgs)
        {
            Application.Current.DispatcherUnhandledException += Dispatcher_UnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Dispatcher.UnhandledException += Dispatcher_UnhandledException;
            //AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException;

            RaveInstance.RemoteControl = new RemoteControl.RemoteControl();

            var parameterParser = new CmdLineParser(eventArgs.Args);

            if (parameterParser.Arguments.Count == 0)
            {
                var filename = Path.Combine(Path.GetDirectoryName(
                    Assembly.GetExecutingAssembly().Location), "Rave.bat");
                if (File.Exists(filename))
                {
                    Task.Factory.StartNew(() => RunRaveBat(filename));
                    return;
                }
            }


            var remoteControlStart = !parameterParser.Arguments.ContainsKey("noremotecontrol");

            ErrorManager.Init(Configuration.LogDirectoryPath);
            ErrorManager.WriteToLog("Info", "RAVE launched");

            audProjectEntry projectEntry;

            var projectList = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                @"\Rave\ProjectList.xml");
            string autoSelectedProject = null;
            if (parameterParser.Arguments.ContainsKey("project")) autoSelectedProject = parameterParser["project"];

            try
            {
                Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                Loader loader = new Loader(projectList, autoSelectedProject);


                loader.CheckAutoSelectOrShow();


                if (loader.IsCancelled)
                {
                    ErrorManager.WriteToLog("Info", "RAVE shutdown");
                    Environment.Exit(0);
                }

                RaveInstance.AssetManager = loader.AssetManager;
                RaveInstance.RaveAssetManager.SetAssetManager(loader.AssetManager);
                projectEntry = loader.Project;

            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
                projectEntry = null;
            }

            if (projectEntry == null)
            {
                ErrorManager.HandleError("Project entry undefined");
                Shutdown();
                return;
            }

            frmSplashScreen.ShowSplashScreen();
            frmSimpleInput.AllowEmptyInput = false;

            try
            {
                if (!LoadProject(projectEntry))
                {
                    frmSplashScreen.CloseForm();
                    Shutdown();
                    return;
                }
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
                Environment.Exit(1);
            }

            // Explicitly call garbage collection due to large
            // number of objects created during startup
            frmSplashScreen.SetText("Performing .NET garbage collection");
            ErrorManager.WriteToLog("Info", "Performing .NET garbage collection");
            GC.Collect();

            frmSplashScreen.CloseForm();
            RaveInstance.ActiveWindow = MainWindow.WindowHandle;
            MainWindow.Show();

            //start background threads after showing the main windows to make sure handles have been created
            if (remoteControlStart)
            {
                ErrorManager.WriteToLog("Info", "Starting remote control");
                RaveInstance.RemoteControl.Start();
            }

            _autoSave = new AutoSave(projectEntry.Name.Replace(" ", "_"));
            _autoSave.RunAutoRestore();
            _autoSave.Start();
        }

        private void Dispatcher_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            ErrorManager.HandleError(e.Exception, true);
            e.Handled = true;
        }

        //private void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        //{
        //    ErrorManager.WriteToLog("First Chance Exception",e.Exception.Message, e.Exception.StackTrace);
        //}
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ErrorManager.HandleError((Exception)e.ExceptionObject, true);
        }

        /// <summary>
        ///     The application_ exit.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        private void Application_Exit(object sender, ExitEventArgs e)
        {
            ErrorManager.WriteToLog("Info", "RAVE shutting down");
            app_Shutdown();
        }

        /// <summary>
        ///     Performs shutdown actions.
        /// </summary>
        private void app_Shutdown()
        {
            if (RaveInstance.RemoteControl != null)
            {
                RaveInstance.RemoteControl.Shutdown();
            }

            if (_driveWatcher != null)
            {
                _driveWatcher.Dispose();
            }

            RaveInstance.AssetManager.Disconnect();
        }

        #endregion

        private void Application_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
        }
    }
}