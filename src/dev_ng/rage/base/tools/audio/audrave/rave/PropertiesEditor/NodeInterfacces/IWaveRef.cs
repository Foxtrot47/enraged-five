using Rave.Core.WaveBrowser.Nodes;

namespace rave.PropertiesEditor.NodeInterfacces
{
    public interface IWaveRef
    {
        void Drop(WaveNode wn);
    }
}