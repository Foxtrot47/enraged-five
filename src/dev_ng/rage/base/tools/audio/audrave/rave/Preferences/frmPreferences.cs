using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Rave.Plugins;

namespace rave
{
	/// <summary>
	/// Summary description for frmPreferences.
	/// </summary>
	public class frmPreferences : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox m_AssetManagerType;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox m_ServerName;
		private System.Windows.Forms.TextBox m_AssetProject;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox m_ProjectList;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox m_WaveEditorPath;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox m_Password;
		private System.Windows.Forms.TextBox m_UserName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox m_MetadataCompiler;
		private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox m_tbExternalScript;
        private System.Windows.Forms.Label lblExternalScriptTxt;
        private Button btnCancel;
        private TextBox m_DepotRoot;
        private Label lblDepotPath;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmPreferences()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

        public void EnableCancelButton(bool enabled)
        {
            btnCancel.Enabled = enabled;
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPreferences));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_DepotRoot = new System.Windows.Forms.TextBox();
            this.lblDepotPath = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_Password = new System.Windows.Forms.TextBox();
            this.m_UserName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_ProjectList = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.m_AssetProject = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_ServerName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_AssetManagerType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_tbExternalScript = new System.Windows.Forms.TextBox();
            this.lblExternalScriptTxt = new System.Windows.Forms.Label();
            this.m_MetadataCompiler = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.m_WaveEditorPath = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.m_DepotRoot);
            this.groupBox1.Controls.Add(this.lblDepotPath);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.m_Password);
            this.groupBox1.Controls.Add(this.m_UserName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.m_ProjectList);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.m_AssetProject);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.m_ServerName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.m_AssetManagerType);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 210);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Asset Manager Settings";
            // 
            // m_DepotRoot
            // 
            this.m_DepotRoot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_DepotRoot.Location = new System.Drawing.Point(112, 182);
            this.m_DepotRoot.Name = "m_DepotRoot";
            this.m_DepotRoot.Size = new System.Drawing.Size(248, 20);
            this.m_DepotRoot.TabIndex = 13;
            // 
            // lblDepotPath
            // 
            this.lblDepotPath.Location = new System.Drawing.Point(8, 186);
            this.lblDepotPath.Name = "lblDepotPath";
            this.lblDepotPath.Size = new System.Drawing.Size(104, 16);
            this.lblDepotPath.TabIndex = 12;
            this.lblDepotPath.Text = "Depot Root";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "Password";
            // 
            // m_Password
            // 
            this.m_Password.Font = new System.Drawing.Font("Wingdings", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.m_Password.Location = new System.Drawing.Point(112, 44);
            this.m_Password.Name = "m_Password";
            this.m_Password.PasswordChar = 'l';
            this.m_Password.Size = new System.Drawing.Size(248, 20);
            this.m_Password.TabIndex = 3;
            // 
            // m_UserName
            // 
            this.m_UserName.Location = new System.Drawing.Point(112, 20);
            this.m_UserName.Name = "m_UserName";
            this.m_UserName.Size = new System.Drawing.Size(248, 20);
            this.m_UserName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "User name";
            // 
            // m_ProjectList
            // 
            this.m_ProjectList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ProjectList.Location = new System.Drawing.Point(112, 156);
            this.m_ProjectList.Name = "m_ProjectList";
            this.m_ProjectList.Size = new System.Drawing.Size(248, 20);
            this.m_ProjectList.TabIndex = 11;
            this.m_ProjectList.Text = "ProjectList.xml";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "Project List";
            // 
            // m_AssetProject
            // 
            this.m_AssetProject.Location = new System.Drawing.Point(112, 132);
            this.m_AssetProject.Name = "m_AssetProject";
            this.m_AssetProject.Size = new System.Drawing.Size(248, 20);
            this.m_AssetProject.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Project/Workspace";
            // 
            // m_ServerName
            // 
            this.m_ServerName.Location = new System.Drawing.Point(112, 108);
            this.m_ServerName.Name = "m_ServerName";
            this.m_ServerName.Size = new System.Drawing.Size(248, 20);
            this.m_ServerName.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Server";
            // 
            // m_AssetManagerType
            // 
            this.m_AssetManagerType.Items.AddRange(new object[] {
            "AlienBrain 6.x",
            "AlienBrain 7.x",
            "Perforce",
            "Local Filesystem (no asset manager)"});
            this.m_AssetManagerType.Location = new System.Drawing.Point(112, 83);
            this.m_AssetManagerType.Name = "m_AssetManagerType";
            this.m_AssetManagerType.Size = new System.Drawing.Size(248, 21);
            this.m_AssetManagerType.TabIndex = 5;
            this.m_AssetManagerType.Text = "AlienBrain";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Asset Manager";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(220, 344);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.m_tbExternalScript);
            this.groupBox2.Controls.Add(this.lblExternalScriptTxt);
            this.groupBox2.Controls.Add(this.m_MetadataCompiler);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.m_WaveEditorPath);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(8, 224);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 114);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "RAVE Settings";
            // 
            // m_tbExternalScript
            // 
            this.m_tbExternalScript.Location = new System.Drawing.Point(112, 84);
            this.m_tbExternalScript.Name = "m_tbExternalScript";
            this.m_tbExternalScript.Size = new System.Drawing.Size(248, 20);
            this.m_tbExternalScript.TabIndex = 5;
            this.m_tbExternalScript.Text = "c:\\temp\\external_script.bat {files}";
            // 
            // lblExternalScriptTxt
            // 
            this.lblExternalScriptTxt.AutoSize = true;
            this.lblExternalScriptTxt.Location = new System.Drawing.Point(8, 88);
            this.lblExternalScriptTxt.Name = "lblExternalScriptTxt";
            this.lblExternalScriptTxt.Size = new System.Drawing.Size(75, 13);
            this.lblExternalScriptTxt.TabIndex = 4;
            this.lblExternalScriptTxt.Text = "External Script";
            // 
            // m_MetadataCompiler
            // 
            this.m_MetadataCompiler.Location = new System.Drawing.Point(112, 52);
            this.m_MetadataCompiler.Name = "m_MetadataCompiler";
            this.m_MetadataCompiler.Size = new System.Drawing.Size(248, 20);
            this.m_MetadataCompiler.TabIndex = 3;
            this.m_MetadataCompiler.Text = "x:\\tools\\RAGEAudio\\bin\\mc.exe";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(8, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 16);
            this.label9.TabIndex = 2;
            this.label9.Text = "Metadata Compiler";
            // 
            // m_WaveEditorPath
            // 
            this.m_WaveEditorPath.Location = new System.Drawing.Point(112, 19);
            this.m_WaveEditorPath.Name = "m_WaveEditorPath";
            this.m_WaveEditorPath.Size = new System.Drawing.Size(248, 20);
            this.m_WaveEditorPath.TabIndex = 1;
            this.m_WaveEditorPath.Text = "C:\\Program Files\\Sony\\Sound Forge 7.0\\forge70.exe";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Wave Editor";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(301, 344);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmPreferences
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(386, 374);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPreferences";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RAVE Settings";
            this.Load += new System.EventHandler(this.frmPreferences_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			Configuration.AssetManagementType = m_AssetManagerType.SelectedIndex;
			Configuration.AssetProject = m_AssetProject.Text;
			Configuration.AssetServer = m_ServerName.Text;
			Configuration.AssetUser = m_UserName.Text;
			Configuration.AssetPassword = m_Password.Text;

            Configuration.ProjectListPath = m_ProjectList.Text;
			Configuration.WaveEditorPath = m_WaveEditorPath.Text;
			Configuration.MetadataCompilerPath = m_MetadataCompiler.Text;
            Configuration.ExternalScript = m_tbExternalScript.Text;
            Configuration.DepotRoot = m_DepotRoot.Text;
			Configuration.SaveSettings();
			Close();
		}

		private void frmPreferences_Load(object sender, System.EventArgs e)
		{
			if(Configuration.AreSettingsLoaded)
			{
				m_AssetManagerType.SelectedIndex = Configuration.AssetManagementType;
				m_AssetProject.Text = Configuration.AssetProject;
				m_ServerName.Text = Configuration.AssetServer;
				m_UserName.Text = Configuration.AssetUser;
				m_Password.Text = Configuration.AssetPassword;
				m_ProjectList.Text = Configuration.ProjectListPath;
                m_DepotRoot.Text = Configuration.DepotRoot;

				m_WaveEditorPath.Text = Configuration.WaveEditorPath;
				m_MetadataCompiler.Text = Configuration.MetadataCompilerPath;
                m_tbExternalScript.Text = Configuration.ExternalScript;
			}
		}

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

	}
}
