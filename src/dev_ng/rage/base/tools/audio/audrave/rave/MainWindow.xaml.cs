﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The wpf 32 window.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using Rave.Controls.WPF.PreviewFolder;
using Rave.Types.Objects;
using Rave.UserPreferences;
using Xceed.Wpf.AvalonDock.Controls;
using Xceed.Wpf.AvalonDock.Layout;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace Rave
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Interop;

    using rage;
    using rage.ToolLib;
    using rage.ToolLib.Logging;

    using Rave.AssetBrowser;
    using Rave.BuildManager.Infrastructure.Enums;
    using Rave.BuildManager.Popups;
    using Rave.Controls.Forms.Popups;
    using Rave.Controls.Forms.Search;
    using Rave.Controls.WPF.UXTimer;
    using Rave.HierarchyBrowser.CategoriesBrowser;
    using Rave.HierarchyBrowser.Infrastructure.Enums;
    using Rave.HierarchyBrowser.Infrastructure.Nodes;
    using Rave.Instance;
    using Rave.Metadata;
    using Rave.ObjectBrowser;
    using Rave.ObjectBrowser.Infrastructure.Interfaces;
    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.PropertiesEditor;
    using Rave.PropertiesEditor.Editors;
    using Rave.RemoteControl.Infrastructure.Interfaces;
    using Rave.TemplateBrowser;
    using Rave.TemplateBrowser.Infrastructure.Interfaces;
    using Rave.Types;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Structs;
    using Rave.Types.ObjectBanks;
    using Rave.Utils;
    using Rave.Utils.Popups;
    using Rave.Utils.Reports;
    using Rave.WaveBrowser.WaveBrowser;
    using Rave.WaveBrowser.WaveBrowser.Search;

    using WinForms = System.Windows.Forms;
    using System.Windows.Forms.Integration;

    using Rave.UIControls;
    using Rave.Search.View;
    using System.Text.RegularExpressions;
    using System.Runtime.InteropServices;
    using System.Windows.Media;
    using WPFToolLib.Extensions;
    using System.Reflection;
    using System.Diagnostics;

    /// <summary>
    /// The wpf 32 window.
    /// </summary>
    public class Wpf32Window : WinForms.IWin32Window
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Wpf32Window"/> class.
        /// </summary>
        /// <param name="wpfWindow">
        /// The wpf window.
        /// </param>
        public Wpf32Window(Window wpfWindow)
        {
            this.Handle = new WindowInteropHelper(wpfWindow).Handle;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the handle.
        /// </summary>
        public IntPtr Handle { get; private set; }

        #endregion

    }

    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Static Fields

        /// <summary>
        /// The load layout 10 command.
        /// </summary>
        public static RoutedCommand LoadLayout10Command = new RoutedCommand();

        /// <summary>
        /// The load layout 1 command.
        /// </summary>
        public static RoutedCommand LoadLayout1Command = new RoutedCommand();

        /// <summary>
        /// The load layout 2 command.
        /// </summary>
        public static RoutedCommand LoadLayout2Command = new RoutedCommand();

        /// <summary>
        /// The load layout 3 command.
        /// </summary>
        public static RoutedCommand LoadLayout3Command = new RoutedCommand();

        /// <summary>
        /// The load layout 4 command.
        /// </summary>
        public static RoutedCommand LoadLayout4Command = new RoutedCommand();

        /// <summary>
        /// The load layout 5 command.
        /// </summary>
        public static RoutedCommand LoadLayout5Command = new RoutedCommand();

        /// <summary>
        /// The load layout 6 command.
        /// </summary>
        public static RoutedCommand LoadLayout6Command = new RoutedCommand();

        /// <summary>
        /// The load layout 7 command.
        /// </summary>
        public static RoutedCommand LoadLayout7Command = new RoutedCommand();

        /// <summary>
        /// The load layout 8 command.
        /// </summary>
        public static RoutedCommand LoadLayout8Command = new RoutedCommand();

        /// <summary>
        /// The load layout 9 command.
        /// </summary>
        public static RoutedCommand LoadLayout9Command = new RoutedCommand();

        /// <summary>
        /// The show save dialog command.
        /// </summary>
        public static RoutedCommand ShowSaveDialogCommand = new RoutedCommand();

        #endregion

        #region Fields

        /// <summary>
        /// The asset manager.
        /// </summary>
        private ctrlVisualAssetManager m_assetManager;

        /// <summary>
        /// The m_browsers.
        /// </summary>
        private readonly Dictionary<string, ctrlObjectBrowser> m_browsers;

        /// <summary>
        /// The template browsers.
        /// </summary>
        private readonly IDictionary<string, ITemplateBrowser> templateBrowsers;

        /// <summary>
        /// The m_hierarchies.
        /// </summary>
        private readonly Dictionary<string, HierarchyBrowser.HierarchyBrowser> m_hierarchies;

        /// <summary>
        /// The m_hierarchy viewer.
        /// </summary>
        private HierarchyBrowser.HierarchyBrowser m_hierarchyBrowser;

        /// <summary>
        /// The m_properties editor.
        /// </summary>
        private PropertiesEditor.PropertiesEditor m_propertiesEditor;

        /// <summary>
        /// The m_wave browser.
        /// </summary>
        private readonly ctrlWaveBrowser m_waveBrowser;

        /// <summary>
        /// The m_ get latest busy form.
        /// </summary>
        private frmBusy m_GetLatestBusyForm;

        /// <summary>
        /// The categories message listener.
        /// </summary>
        private IRemoteMessageListener categoriesMessageListener;


        /// <summary>
        /// The anchor for searchresults
        /// </summary>
        private LayoutAnchorGroup SearchLayoutAnchor = new LayoutAnchorGroup();

        private SearchResults searchResultWindow;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitCommands();
            this.InitializeComponent();
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
            var log = new XmlLog(new NullLogWriter(), new ContextStack(), "RaveTempCompiler");
            RaveInstance.TempCompiler = new XmlBankMetaDataCompiler(log, Configuration.ProjectSettings);
            this.m_browsers = new Dictionary<string, ctrlObjectBrowser>();
            this.templateBrowsers = new Dictionary<string, ITemplateBrowser>();
            this.m_hierarchies = new Dictionary<string, HierarchyBrowser.HierarchyBrowser>();
            this.m_waveBrowser = new ctrlWaveBrowser();
            this.StateChanged += MainWindow_StateChanged;
        }




        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the window handle.
        /// </summary>
        public Wpf32Window WindowHandle
        {
            get
            {
                return new Wpf32Window(this);
            }
        }

        public bool IsMaximized
        {
            get { return this.WindowState == System.Windows.WindowState.Maximized; }

        }
        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Initialize the main window.
        /// </summary>
        public void Init()
        {
            this.LeftPane.Children.Clear();
            this.MiddlePane.Children.Clear();
            this.RightPane.Children.Clear();
            this.BottomPane.Children.Clear();


            RaveInstance.WaveBrowser = this.m_waveBrowser;
            this.m_waveBrowser.ChangelistModified += this.OnChangeListModified;
            this.m_propertiesEditor = new PropertiesEditor.PropertiesEditor();
            this.m_assetManager = ctrlVisualAssetManager.Instance;
            this.m_hierarchyBrowser = new HierarchyBrowser.HierarchyBrowser();
            RaveInstance.HierarchyBrowser = this.m_hierarchyBrowser;

            // Setup hierarchy viewer
            this.m_hierarchyBrowser.Init("Hierarchy Viewer");
            this.m_hierarchyBrowser.Content.Title = "Hierarchy Viewer";
            this.m_hierarchyBrowser.Content.ContentId = "HierarchyViewer";
            this.m_hierarchyBrowser.AuditionObject += OnAuditionObject;
            this.m_hierarchyBrowser.StopAuditionObject += OnStopObjectAudition;
            this.m_hierarchyBrowser.FindObject += this.OnFindObject;
            this.m_hierarchyBrowser.FindTemplate += this.OnFindTemplateFromProxy;
            this.MiddlePane.Children.Add(this.m_hierarchyBrowser.Content);

            // Setup properties editor
            this.m_propertiesEditor.AuditionSound += OnAuditionObject;
            this.m_propertiesEditor.StopSoundAudition += OnStopObjectAudition;
            this.m_propertiesEditor.OnWaveRefClick += this.PropertiesEditor_OnWaveRefClick;
            this.m_propertiesEditor.OnWaveRefAudition += this.PropertiesEditor_OnWaveRefAudition;
            this.m_propertiesEditor.OnWaveBankRefClick += this.PropertiesEditor_OnWaveBankRefClick;
            this.m_propertiesEditor.OnObjectRefClick += this.OnFindObject;
            this.m_propertiesEditor.OnTemplateRefClick += this.OnFindTemplate;
            this.m_propertiesEditor.OnCreateFloatingEditorClick += this.m_propertiesEditor_OnCreateFloatingEditorClick;
            this.m_propertiesEditor.DockableContent.ContentId = "PropertiesEditor";
            this.KeyDown += this.m_propertiesEditor.PropertiesEditor_KeyDown;
            this.RightPane.Children.Add(this.m_propertiesEditor.DockableContent);

            // Setup asset manager
            this.m_assetManager.Content.Title = "Asset Manager";
            this.m_assetManager.Content.ContentId = "AssetManager";
            this.BottomPane.Children.Add(m_assetManager.Content);

            // Setup wave browser
            this.m_waveBrowser.GetLatest += this.OnGetLatest;
            var waveBrowserContent = m_waveBrowser.Content;
            waveBrowserContent.ContentId = "WaveBrowser";
            waveBrowserContent.Title = "Wave Browser";
            this.LeftPane.Children.Add(waveBrowserContent);

            // Workaround for:System.ArgumentException: Invisible or disabled control cannot be activated
            // http://support.microsoft.com/kb/2686194
            WinForms.Application.ThreadException += this.Application_ThreadException;

            var timer = new UXTimer();
            timer.DockableContent.Title = "Activity Log";
            timer.DockableContent.ContentId = "ActivityLog";
            timer.DockableContent.AddToLayout(this.dockManager, AnchorableShowStrategy.Bottom);
            timer.DockableContent.Float();

            if (!string.IsNullOrEmpty(Configuration.PreviewFolder))
            {
                var previewFolder = new PreviewFolder();
                previewFolder.Content.Title = "Preview Folder";
                previewFolder.Content.ContentId = "PreviewFolder";
                previewFolder.Content.AddToLayout(this.dockManager, AnchorableShowStrategy.Bottom);
                previewFolder.Content.Float();
            }
            //Create a Search window
            this.searchResultWindow = new SearchResults();

            this.searchResultWindow.Content.AddToLayout(this.dockManager, AnchorableShowStrategy.Right | AnchorableShowStrategy.Top);
            this.RightPane.Children.Add(searchResultWindow.Content);

            this.searchResultWindow.Content.IsVisible = true;
            this.searchResultWindow.Content.IsActive = true;
            this.searchResultWindow.Content.IsSelected = true;
            this.searchResultWindow.Content.CanFloat = true;

            if (Configuration.MuteSoloEnabledTypes.Count > 0)
            {
                MuteSoloManager.View.MuteSoloManagerView muteSoloManager = new MuteSoloManager.View.MuteSoloManagerView();

                muteSoloManager.Content.AddToLayout(this.dockManager, AnchorableShowStrategy.Right | AnchorableShowStrategy.Top);
                this.RightPane.Children.Add(muteSoloManager.Content);
                muteSoloManager.Content.IsVisible = true;
                muteSoloManager.Content.IsActive = true;
                muteSoloManager.Content.CanFloat = true;
                muteSoloManager.Content.Float();
                muteSoloManager.viewModel.OnObjectRefClick += this.OnFindObject;
            }

            //Setup Search bar
            Dictionary<string, string> arguments = new Dictionary<string, string>();

            arguments.Add("Search Names", "Search object names for the specified text");
            arguments.Add("Search Name Hash", "Search for the specified name hash");
            arguments.Add("Exact Name Match", "Display only results which match the name exactly");
            arguments.Add("Case Sensitive", "Conduct a case sensitive search. Capital letters will affect the search");
            arguments.Add("Multi Term Search", "Search for multiple words, separated by space. Objects matching all of the search terms will be returned");
            arguments.Add("Use Regular Expressions", "Specify a pattern to search for using the regular expression language");
            SearchTextBox.SearchArguments = arguments;

            // Choose a style for displaying sections
            SearchTextBox.SelectionStyle = SearchTextBox.PopUpSelectionStyle.CheckBoxStyle;

            // Add a routine handling the event OnSearch
            SearchTextBox.OnSearch += new RoutedEventHandler(SearchTextBox_OnSearch);


        }


        /// <summary>
        /// The create floating editor.
        /// </summary>
        /// <param name="objectToEdit">
        /// The object to edit.
        /// </param>
        public void CreateFloatingEditor(IObjectInstance objectToEdit)
        {
            var propEditor = new PropertiesEditor.PropertiesEditor();
            propEditor.AuditionSound += OnAuditionObject;
            propEditor.StopSoundAudition += OnStopObjectAudition;
            propEditor.OnWaveRefClick += this.PropertiesEditor_OnWaveRefClick;
            propEditor.OnWaveBankRefClick += this.PropertiesEditor_OnWaveBankRefClick;
            propEditor.OnObjectRefClick += this.OnFindObject;

            propEditor.DockableContent.Title = "Properties - " + objectToEdit.Name;
            propEditor.EditObject(objectToEdit);
            propEditor.DockableContent.AddToLayout(this.dockManager, AnchorableShowStrategy.Left);
            propEditor.DockableContent.Float();
        }

        /// <summary>
        /// The load project.
        /// </summary>
        /// <param name="project">
        /// The project.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool LoadProject(audProjectEntry project)
        {
            this.Title = "RAVE - " + Configuration.ProjectName;

            RaveInstance.RemoteControl.OnViewObject += this.OnFindObject;

            RaveInstance.BuildManager = new BuildManager.BuildManager(this.m_waveBrowser);
            RaveInstance.BuildManager.SaveAllBanks += OnSaveAllBanks;

            this.CreateReportMenu();

            frmSplashScreen.SetText("Loading project " + Configuration.ProjectName);

            this.m_waveBrowser.CreatePendingWaveLists();

            frmSplashScreen.SetText("Initializing wave browser");
            if (!this.m_waveBrowser.Init())
            {
                return false;
            }

            ((ctrlWaveEditorTreeView)this.m_waveBrowser.GetEditorTreeView()).FindObject += this.OnFindObject;

            frmSplashScreen.SetText("Building wave slot references");
            ctrlWaveBrowser.BuildSlotRefs();

            this.categoriesMessageListener = new CategoriesMessageListener();
            RaveInstance.RemoteControl.AddRemoteMessageListener(this.categoriesMessageListener);

            this.LoadHierarchyViewers();

            this.m_assetManager.BanksStatusChanged += AssetManager_OnBanksStatusChanged;
            this.m_assetManager.BankSelected += this.AssetManager_OnBankSelected;
            this.m_assetManager.BankStatusChangedStringList += AssetManager_OnBanksStatusChangedStringList;
            this.m_assetManager.ChangeListStatusChange += this.AssetManager_OnChangeListStatusChange;
            this.m_assetManager.ChangeListSubmitted += this.AssetManager_OnChangeListSubmitted;

            frmSplashScreen.SetText("Loading asset manager browser");
            this.m_assetManager.Init();

            frmSplashScreen.SetText("Loading wave browser");
            this.m_waveBrowser.LoadWaveLists(false);

            frmSplashScreen.SetText("Building menus");
            this.CreateWindowMenu();
            this.CreateSearchMenu();

            this.CreateBuildMenus();

            return true;
        }

        /// <summary>
        /// The on get latest.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        public void OnGetLatest(string platform)
        {
            if (this.m_GetLatestBusyForm != null)
            {
                return;
            }

            var worker = new BackgroundWorker();

            worker.DoWork += this.w_DoWork;
            worker.RunWorkerCompleted += this.w_RunWorkerCompleted;
            worker.RunWorkerAsync(platform);

            this.m_GetLatestBusyForm = new frmBusy();
            this.m_GetLatestBusyForm.SetStatusText("Getting Latest");
            this.m_GetLatestBusyForm.SetUnknownDuration(true);
            this.m_GetLatestBusyForm.SetProgress(-1);
            this.m_GetLatestBusyForm.Show(this.WindowHandle);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The asset manager_ on banks status changed.
        /// </summary>
        private static void AssetManager_OnBanksStatusChanged()
        {
            XmlBankManager.UpdateBanksStatus();
        }

        /// <summary>
        /// The asset manager_ on banks status changed string list.
        /// </summary>
        /// <param name="paths">
        /// The paths.
        /// </param>
        private static void AssetManager_OnBanksStatusChangedStringList(List<string> paths)
        {
            XmlBankManager.UpdateBanksStatus(paths);
        }

        /// <summary>
        /// The get default layout.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetDefaultLayout()
        {
            string path = GetLayoutStorePath() + "\\default.txt";
            if (File.Exists(path))
            {
                var reader = new StreamReader(path);
                string ret = reader.ReadToEnd();
                reader.Close();
                return ret;
            }

            return null;
        }

        /// <summary>
        /// The get layout store path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetLayoutStorePath()
        {
            var sb = new StringBuilder();
            sb.Append(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            sb.Append("\\Rave\\AvLayouts2.0");
            string path = sb.ToString();
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        /// <summary>
        /// The on audition sound.
        /// </summary>
        /// <param name="sound">
        /// The sound.
        /// </param>
        private static void OnAuditionObject(IObjectInstance sound)
        {
            if (sound != null)
            {
                RaveInstance.RemoteControl.PlaySound(sound.Name);
                RaveInstance.RemoteControl.StartAuditioningObject(
                    new Hash { Value = sound.Type }.Key, new Hash { Value = sound.Name }.Key);
            }
        }

        /// <summary>
        /// The on save all banks.
        /// </summary>
        private static void OnSaveAllBanks()
        {
            try
            {
                XmlBankManager.SaveAllBanks(RaveInstance.AllBankManagers);
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }
        }

        /// <summary>
        /// The on stop sound audition.
        /// </summary>
        /// <param name="sound">
        /// The sound.
        /// </param>
        private static void OnStopObjectAudition(IObjectInstance sound)
        {
            RaveInstance.RemoteControl.StopSound();
            if (sound != null)
            {
                RaveInstance.RemoteControl.StopAuditioningObject(
                    new Hash { Value = sound.Type }.Key, new Hash { Value = sound.Name }.Key);
            }
        }

        /// <summary>
        /// The save default layout.
        /// </summary>
        /// <param name="layout">
        /// The layout.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool SaveDefaultLayout(string layout)
        {
            try
            {
                string path = GetLayoutStorePath() + "\\default.txt";
                var writer = new StreamWriter(path, false);
                writer.Write(layout);
                writer.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The mnu close all build windows_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void mnuCloseAllBuildWindows_Click(object sender, RoutedEventArgs e)
        {
            frmBuildOutput.CloseAllBuildWindows();
        }

        /// <summary>
        /// The application_ thread exception.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            if (!this.FailureFromFocusedChild(e.Exception))
            {
                MessageBox.Show(e.Exception.ToString());
            }
        }

        /// <summary>
        /// The asset manager_ on bank selected.
        /// </summary>
        /// <param name="bankPath">
        /// The bank path.
        /// </param>
        private void AssetManager_OnBankSelected(string bankPath)
        {
            foreach (audMetadataFile metadataFile in Configuration.MetadataFiles)
            {
                string path = (Configuration.ObjectXmlPath + metadataFile.DataPath).ToUpper();
                if (bankPath.StartsWith(path))
                {
                    string typeUpper = metadataFile.Type.ToUpper();
                    var objectBanks = RaveInstance.AllBankManagers[typeUpper];
                    if (objectBanks != null)
                    {
                        var objectBank = objectBanks.FindObjectBank(bankPath);
                        if (objectBank != null && this.m_browsers.ContainsKey(typeUpper))
                        {
                            this.m_browsers[typeUpper].FindObjectBank(objectBank);
                        }
                        else if (objectBank != null && this.m_hierarchies.ContainsKey(typeUpper))
                        {
                            this.m_hierarchies[typeUpper].Show();
                        }
                    }

                    break;
                }
            }
        }

        /// <summary>
        /// The asset manager on change list status change.
        /// </summary>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        private void AssetManager_OnChangeListStatusChange(string changeListNumber)
        {
            this.m_waveBrowser.LoadWaveLists();
        }

        /// <summary>
        /// The asset manager on change list submitted.
        /// </summary>
        /// <param name="pendingChangeListNumber">
        /// The pending change list number.
        /// </param>
        /// <param name="submittedChangeListNumber">
        /// The submitted change list number.
        /// </param>
        private void AssetManager_OnChangeListSubmitted(
            string pendingChangeListNumber, string submittedChangeListNumber)
        {
            var assetManager = RaveInstance.AssetManager;


            Application.Current.Dispatcher.Invoke(() =>
            {
                MessageBoxResult msgresult =
                    MessageBox.Show(String.Format("Submitted Changelist: {0} \nCopy CL-Number to clipboard?", submittedChangeListNumber),
                        "Submitted Changelist", MessageBoxButton.YesNo);
                if (msgresult == MessageBoxResult.Yes)
                {
                    Clipboard.SetText(submittedChangeListNumber);
                }
            }
                );

            var files = assetManager.GetChangeListFiles(submittedChangeListNumber);

            // Check if change list contained any wave-related file changes
            foreach (var file in files)
            {
                var fileLocalPath = assetManager.GetDepotPath(file).ToUpper();
                foreach (var platformSetting in Configuration.ActivePlatformSettings)
                {
                    var buildPath = assetManager.GetDepotPath(platformSetting.BuildInfo).ToUpper();
                    buildPath = buildPath.Replace("...", string.Empty);
                    if (fileLocalPath.StartsWith(buildPath))
                    {
                        // Load wave list on first match we find and then return
                        this.m_waveBrowser.LoadWaveLists();
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// The create build menus.
        /// </summary>
        private void CreateBuildMenus()
        {
            // build metadata menu
            this.metadataMenu.Items.Clear();
            var buildCode = new MenuItem { Header = "Build Code" };
            this.metadataMenu.Items.Add(buildCode);
            var buildData = new MenuItem { Header = "Build Data" };
            this.metadataMenu.Items.Add(buildData);

            foreach (audMetadataType metadataType in Configuration.MetadataTypes)
            {
                var m = new MenuItem { Header = metadataType.Type, Tag = metadataType };
                m.Click += this.MetadataBuildCode_Click;
                buildCode.Items.Add(m);
                m = new MenuItem { Header = metadataType.Type, Tag = metadataType };
                m.Click += this.MetadataBuildData_Click;
                buildData.Items.Add(m);
            }

            this.metadataMenu.Items.Add(new Separator());
            var menuItem = new MenuItem { Header = "Reload type definitions" };
            menuItem.Click += this.mnuLoadTypeDefs_Click;
            this.metadataMenu.Items.Add(menuItem);
            menuItem = new MenuItem { Header = "Close Build Windows" };
            menuItem.Click += mnuCloseAllBuildWindows_Click;
            this.metadataMenu.Items.Add(menuItem);

            // build wave browser menu
            this.m_waveBrowser.InitMainMenu(this.mnuWaveBrowser, this.buildMenu);

            this.buildMenu.Items.Add(new Separator());

            // batch build            
            foreach (audBuildSet buildSet in Configuration.BuildSets)
            {
                var allPlatforms = new MenuItem { Header = "All Platforms", Tag = buildSet };
                allPlatforms.Click += this.mnuBatchBuild_Click;
                var item = new MenuItem { Header = buildSet.Name };
                item.Items.Add(allPlatforms);

                foreach (PlatformSetting ps in Configuration.ActivePlatformSettings)
                {
                    if (ps.BuildEnabled)
                    {
                        var platformItem = new MenuItem { Header = ps.PlatformTag, Tag = buildSet };
                        platformItem.Click += this.mnuBatchBuild_Click;
                        item.Items.Add(platformItem);
                    }
                }

                this.buildMenu.Items.Add(item);
            }
        }

        /// <summary>
        /// The create load view menu.
        /// </summary>
        private void CreateLoadLayoutMenu()
        {
            this.loadLayoutMenu.Items.Clear();

            string layoutPath = GetLayoutStorePath();

            var folderPaths = new List<string> { layoutPath };

            foreach (string folderPath in folderPaths)
            {
                if (Directory.Exists(folderPath))
                {
                    var dir = new DirectoryInfo(folderPath);
                    foreach (FileInfo file in dir.GetFiles("*.xml"))
                    {
                        var mnu = new MenuItem { Header = file.Name, Tag = file.FullName };
                        mnu.Click += this.LoadLayout_Click;
                        this.loadLayoutMenu.Items.Add(mnu);
                    }
                }
            }
        }

        /// <summary>
        /// The create metadata browser.
        /// </summary>
        /// <param name="metadataType">
        /// The metadata type.
        /// </param>
        /// <param name="isInitialView">
        /// The is initial view.
        /// </param>
        private void CreateMetadataBrowser(audMetadataType metadataType, bool isInitialView)
        {
            if (isInitialView && RaveInstance.AllBankManagers.ContainsKey(metadataType.Type.ToUpper() + "_TEMPLATE"))
            {
                var templateBrowser = new ctrlTemplateBrowser();
                templateBrowser.Init(
                    RaveInstance.AllBankManagers[metadataType.Type.ToUpper() + "_TEMPLATE"],
                    metadataType.TemplatePath,
                    metadataType.Type.ToUpper(),
                    RaveInstance.AllTypeDefinitions[metadataType.Type]);
                templateBrowser.TemplateSelected += this.TemplateBrowser_OnTemplateSelected;
                templateBrowser.ChangeListModified += this.OnChangeListModified;
                templateBrowser.FindObject += this.OnFindObject;
                templateBrowser.TemplateDeleted += this.OnTemplateDeleted;

                templateBrowser.Content.Title = metadataType.TypeNotUpper + " Templates";
                templateBrowser.Content.ContentId = metadataType.Type + "TemplateBrowser";
                this.LeftPane.Children.Add(templateBrowser.Content);

                this.templateBrowsers.Add(metadataType.Type, templateBrowser);
            }

            // Setup Object Browser
            var browser = new ctrlObjectBrowser();
            browser.Init(
                metadataType.Type,
                metadataType.DataPath,
                RaveInstance.AllBankManagers[metadataType.Type],
                RaveInstance.AllTypeDefinitions[metadataType.Type]);
            browser.CanAudition = metadataType.CanAudition;
            browser.AllowMultiCheckout = metadataType.AllowMultiCheckout;
            browser.CanViewHierarchy = metadataType.CanViewHeirarchy;
            browser.ChangelistModified += this.OnChangeListModified;
            browser.AuditionSound += OnAuditionObject;
            browser.StopSoundAudition += OnStopObjectAudition;
            browser.DeleteObject += this.OnDeleteObject;
            if (browser.CanViewHierarchy)
            {
                browser.OnSoundSelect += this.browser_OnObjectSelect;
            }
            else
            {
                browser.OnSoundSelect += this.browser_OnSoundSelect_No_Heirarchy;
            }

            browser.Content.Title = metadataType.TypeNotUpper + " Browser";

            if (isInitialView)
            {
                browser.Content.ContentId = metadataType.TypeNotUpper + "Browser";
                this.m_browsers.Add(metadataType.Type, browser);
                this.LeftPane.Children.Add(browser.Content);
            }
            else
            {
                browser.Content.AddToLayout(this.dockManager, AnchorableShowStrategy.Left);
                browser.Content.Float();
            }
        }

        /// <summary>
        /// Create the report menu from the list of available reports provided
        /// by the plugin manager.
        /// </summary>
        private void CreateReportMenu()
        {
            if (RaveInstance.PluginManager == null)
            {
                return;
            }

            // Add a menu item for each report we have avaiable
            foreach (IRAVEReportPlugin reportPlugin in RaveInstance.PluginManager.ReportPlugins)
            {
                string reportName = reportPlugin.GetName();

                // Report menu levels are specified in name, using a '.'
                // to separate each menu level
                string[] splitName = reportName.Split('.');

                // Find/create menu structure to add new menu item
                MenuItem menuItem = this.reportsMenu;
                for (int i = 0; i < splitName.Length; i++)
                {
                    string nameLower = splitName[i].ToLower();
                    bool found = false;

                    // See if there is already an existing menu item with same name
                    foreach (MenuItem item in menuItem.Items)
                    {
                        // Don't want to add a child to an existing menu item link,
                        // so check child count of item first
                        if (((string)item.Header).ToLower().Equals(nameLower))
                        {
                            if (!item.HasItems && i == splitName.Length - 1)
                            {
                                // Duplicate report name detected - throw error
                                ErrorManager.HandleError(
                                    "Duplicate report name detected - unable to create menu: " + reportName);
                                return;
                            }

                            menuItem = item;
                            found = true;
                            break;
                        }
                    }

                    // Create new menu item if required
                    if (!found)
                    {
                        var newMenuItem = new MenuItem { Header = splitName[i] };
                        menuItem.Items.Add(newMenuItem);
                        menuItem = newMenuItem;
                    }
                }

                // Add click event handler
                menuItem.Click += this.Report_Click;
                menuItem.Tag = reportName;
            }

            // Add sound set reports menu item
            var soundSetReportMenuItem = new MenuItem { Header = "SoundSet Report" };
            soundSetReportMenuItem.Click += this.SoundSetReportMenuItem_Click;
            this.reportsMenu.Items.Add(soundSetReportMenuItem);
        }

        /// <summary>
        /// Sound set report menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void SoundSetReportMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var objBrowser = this.m_browsers["SOUNDS"];

            var scriptTriggeredNode = ctrlObjectBrowser.FindTreeNode(objBrowser.GetTreeView(), "SCRIPT_TRIGGERED");

            if (null == scriptTriggeredNode)
            {
                ErrorManager.HandleInfo("Failed to get sound SCRIPT_TRIGGERED node - unable to generate report");
                return;
            }

            var objs = this.SoundSetReportHelper(scriptTriggeredNode);
            var outputPath = Path.Combine(Path.GetTempPath(), "sound_set_report.html");
            var report = new SoundSetReport(outputPath, objs, true);
            report.SaveReport();

            MessageBox.Show("Report saved to " + outputPath, "Success", MessageBoxButton.OK);
        }

        /// <summary>
        /// The sound set report helper.
        /// </summary>
        /// <param name="root">
        /// The root.
        /// </param>
        /// <returns>
        /// The populated list of sound set objects <see cref="IList{T}"/>.
        /// </returns>
        private IList<IObjectInstance> SoundSetReportHelper(WinForms.TreeNode root)
        {
            var soundSetObjs = new List<IObjectInstance>();
            foreach (WinForms.TreeNode node in root.Nodes)
            {
                var soundNode = node as SoundNode;
                if (null != soundNode)
                {
                    if (soundNode.ObjectInstance.TypeName.Equals("SoundSet"))
                    {
                        soundSetObjs.Add(soundNode.ObjectInstance);
                    }
                }

                soundSetObjs.AddRange(this.SoundSetReportHelper(node));
            }

            return soundSetObjs;
        }

        /// <summary>
        /// The create search menu.
        /// </summary>
        private void CreateSearchMenu()
        {
            foreach (audMetadataType type in Configuration.MetadataTypes)
            {
                var m = new MenuItem { Header = type.TypeNotUpper };
                m.Click += this.SearchObjects_Click;
                this.FindObjectsMenu.Items.Add(m);
            }
        }

        /// <summary>
        /// The create window menu.
        /// </summary>
        private void CreateWindowMenu()
        {
            // View Window
            this.CreateLoadLayoutMenu();

            this.showMenu.Items.Clear();
            //add all scaffolded browsers

            foreach (LayoutAnchorablePane pane in this.dockManager.Layout.Descendents().OfType<LayoutAnchorablePane>())
            {
                foreach (LayoutAnchorable content in pane.Descendents().OfType<LayoutAnchorable>())
                {
                    object browser = content.Content;
                    if (browser is WindowsFormsHost) browser = ((WindowsFormsHost)browser).Child;
                    var showMenuItem = new MenuItem { Header = content.Title, Tag = browser };
                    showMenuItem.Click += this.showMenuItem_Click;
                    this.showMenu.Items.Add(showMenuItem);
                }
            }

            foreach (audMetadataType m in Configuration.MetadataTypes)
            {
                var createBrowserItem = new MenuItem { Header = m.TypeNotUpper, Tag = m };
                createBrowserItem.Click += this.createBrowserItem_Click;
                this.createBrowserMenu.Items.Add(createBrowserItem);
            }
        }

        /// <summary>
        /// The failure from focused child.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool FailureFromFocusedChild(Exception e)
        {
            string stackTrace = e.StackTrace;

            bool result = (e is ArgumentException) && (e.Source == "System.Windows.Forms")
                          && (stackTrace.IndexOf("System.Windows.Forms.Integration.WindowsFormsHost.RestoreFocusedChild", StringComparison.Ordinal) >= 0);

            return result;
        }

        /// <summary>
        /// The find speech waves_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FindSpeechWaves_Click(object sender, RoutedEventArgs e)
        {
            var searchForm = new frmSpeechWaveSearch(this.m_waveBrowser);
            searchForm.Show(this.WindowHandle);
        }

        /// <summary>
        /// The find waves_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FindWaves_Click(object sender, RoutedEventArgs e)
        {
            var searchForm = new frmWaveSearch(this.m_waveBrowser);
            searchForm.Show(this.WindowHandle);
        }

        /// <summary>
        /// The hierarchy viewer_ sound selected.
        /// </summary>
        /// <param name="node">
        /// The object instance.
        /// </param>
        private void HierarchyBrowserNodeSelected(HierarchyNode node)
        {
            var objectNode = node as ObjectNode;
            var objectRefNode = node as ObjectRefNode;
            var templateNode = node as TemplateNode;

            IObjectInstance objectInstance = null;
            if (null != objectRefNode)
            {
                objectInstance = objectRefNode.ObjectInstance;
            }
            else if (null != objectNode)
            {
                objectInstance = objectNode.ObjectInstance;
            }

            if (null != objectInstance)
            {
                App.Instance.OnObjectSelect(objectInstance);
                if (m_propertiesEditor.AutoUpdate)
                {
                    this.m_propertiesEditor.EditObject(objectInstance);
                }
            }
            else if (null != templateNode && this.m_propertiesEditor.AutoUpdate)
            {
                if (templateNode.ProxyObject.Type == templateNode.ProxyObject.Template.TemplateBank.Type)
                {
                    this.m_propertiesEditor.EditObject(templateNode.ProxyObject);
                }
                else
                {
                    this.m_propertiesEditor.Reset();
                }
            }
        }

        /// <summary>
        /// The init commands.
        /// </summary>
        private void InitCommands()
        {
            LoadLayout1Command.InputGestures.Add(new KeyGesture(Key.D1, ModifierKeys.Alt));
            LoadLayout2Command.InputGestures.Add(new KeyGesture(Key.D2, ModifierKeys.Alt));
            LoadLayout3Command.InputGestures.Add(new KeyGesture(Key.D3, ModifierKeys.Alt));
            LoadLayout4Command.InputGestures.Add(new KeyGesture(Key.D4, ModifierKeys.Alt));
            LoadLayout5Command.InputGestures.Add(new KeyGesture(Key.D5, ModifierKeys.Alt));
            LoadLayout6Command.InputGestures.Add(new KeyGesture(Key.D6, ModifierKeys.Alt));
            LoadLayout7Command.InputGestures.Add(new KeyGesture(Key.D7, ModifierKeys.Alt));
            LoadLayout8Command.InputGestures.Add(new KeyGesture(Key.D8, ModifierKeys.Alt));
            LoadLayout9Command.InputGestures.Add(new KeyGesture(Key.D9, ModifierKeys.Alt));
            LoadLayout10Command.InputGestures.Add(new KeyGesture(Key.D0, ModifierKeys.Alt));
            ShowSaveDialogCommand.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control | ModifierKeys.Shift));
        }

        /// <summary>
        /// The load hierarchy viewers.
        /// </summary>
        private void LoadHierarchyViewers()
        {
            foreach (var metadataType in Configuration.MetadataTypes)
            {
                if (metadataType.UseHeirarchy)
                {
                    var isCategories = metadataType.Type.ToUpper() == "CATEGORIES";

                    var hierarchyViewer = new HierarchyBrowser.HierarchyBrowser { SortTree = true };

                    if (isCategories)
                    {
                        hierarchyViewer.Mode = HierarchyBrowserModes.Categories;
                        CreateMetadataBrowser(metadataType, true);
                    }

                    // TODO: Review
                    var externalEditors = RaveInstance.PluginManager != null && isCategories
                                              ? RaveInstance.PluginManager.EditorPlugins
                                              : new List<IRAVEEditorPlugin>();
                    hierarchyViewer.Init(
                        externalEditors,
                        metadataType.Type + " Viewer");

                    hierarchyViewer.ShouldExpandAll = true;
                    hierarchyViewer.CanLocalCheckOut = true;
                    hierarchyViewer.CanCheckInOut = true;
                    hierarchyViewer.CanDelete = true;
                    hierarchyViewer.NodeSelected += this.HierarchyBrowserNodeSelected;
                    hierarchyViewer.ChangeListModified += this.OnChangeListModified;
                    hierarchyViewer.Content.Title = metadataType.TypeNotUpper;
                    hierarchyViewer.Content.ContentId = metadataType.TypeNotUpper + "View";

                    if (isCategories)
                    {
                        //hierarchyViewer. // todo - add event from remote message listener to hierarchy viewer
                    }

                    this.m_hierarchies.Add(metadataType.Type, hierarchyViewer);
                    this.LeftPane.Children.Add(hierarchyViewer.Content);

                    var baseLookup = new ObjectLookupId(metadataType.Type, ObjectLookupId.Base);
                    if (RaveInstance.ObjectLookupTables[baseLookup].ContainsKey(ObjectLookupId.Base))
                    {
                        hierarchyViewer.ShowHierarchy(
                            RaveInstance.ObjectLookupTables[baseLookup][ObjectLookupId.Base]);
                    }
                }
                else
                {
                    this.CreateMetadataBrowser(metadataType, true);
                }
            }


            frmSplashScreen.SetText("Initializing object hierarchy viewer");
            this.m_hierarchyBrowser.Init("Hierarchy Viewer");
            this.m_hierarchyBrowser.ShouldAppendType = true;
            this.m_hierarchyBrowser.CanNavigate = true;
            this.m_hierarchyBrowser.CanMakeLocal = true;
            this.m_hierarchyBrowser.CanInsertWrapper = true;
            this.m_hierarchyBrowser.CanAudition = true;
            this.m_hierarchyBrowser.CanRemoveReference = true;
            this.m_hierarchyBrowser.CanFindInBrowser = true;

            this.m_hierarchyBrowser.NodeSelected += this.HierarchyBrowserNodeSelected;
        }

        /// <summary>
        /// The load layout.
        /// </summary>
        /// <param name="commandId">
        /// The command id.
        /// </param>
        private void LoadLayout(int commandId)
        {
            int index = commandId - 1;
            var dir = new DirectoryInfo(GetLayoutStorePath());
            FileInfo[] files = dir.GetFiles("*.xml");
            if (files.Count() > index)
            {
                string fileName = files[index].FullName;
                if (File.Exists(fileName))
                {
                    restoreLayout(fileName);
                }
            }
        }

        /// <summary>
        /// The load layout 10 command_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LoadLayout10Command_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.LoadLayout(10);
        }

        /// <summary>
        /// The load layout 1 command_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LoadLayout1Command_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.LoadLayout(1);
        }

        /// <summary>
        /// The load layout 2 command_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LoadLayout2Command_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.LoadLayout(2);
        }

        /// <summary>
        /// The load layout 3 command_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LoadLayout3Command_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.LoadLayout(3);
        }

        /// <summary>
        /// The load layout 4 command_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LoadLayout4Command_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.LoadLayout(4);
        }

        /// <summary>
        /// The load layout 5 command_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LoadLayout5Command_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.LoadLayout(5);
        }

        /// <summary>
        /// The load layout 6 command_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LoadLayout6Command_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.LoadLayout(6);
        }

        /// <summary>
        /// The load layout 7 command_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LoadLayout7Command_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.LoadLayout(7);
        }

        /// <summary>
        /// The load layout 8 command_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LoadLayout8Command_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.LoadLayout(8);
        }

        /// <summary>
        /// The load layout 9 command_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LoadLayout9Command_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.LoadLayout(9);
        }

        /// <summary>
        /// The load layout_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LoadLayout_Click(object sender, RoutedEventArgs e)
        {
            var menu = sender as MenuItem;
            var layoutFile = menu.Tag as string;
            if (File.Exists(layoutFile))
            {
                restoreLayout(layoutFile);
                SaveDefaultLayout(layoutFile);
            }
        }

        private void restoreLayout(string path)
        {
            try
            {
                var serializer = new XmlLayoutSerializer(dockManager);
                using (var stream = new StreamReader(path))
                    serializer.Deserialize(stream);
            }
            catch (Exception e)
            {
                MessageBox.Show("Layout could not be loaded", "Error", MessageBoxButton.OK);
            }
        }

        /// <summary>
        /// The metadata build code_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MetadataBuildCode_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            var metadataType = menuItem.Tag as audMetadataType;
            RaveInstance.BuildManager.Build(MetadataBuildType.Code, metadataType);
            this.m_assetManager.RefreshView();
        }

        /// <summary>
        /// The metadata build data_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MetadataBuildData_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            var metadataType = menuItem.Tag as audMetadataType;
            RaveInstance.BuildManager.Build(MetadataBuildType.Data, metadataType);
            this.m_assetManager.RefreshView();
        }

        /// <summary>
        /// The on change list modified.
        /// </summary>
        private void OnChangeListModified()
        {
            this.m_assetManager.RefreshView();
            this.m_propertiesEditor.RefreshView();
        }

        /// <summary>
        /// The on delete object.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        private void OnDeleteObject(IObjectInstance objectInstance)
        {
            this.m_hierarchyBrowser.DeleteObjectInstance(objectInstance);
            this.m_propertiesEditor.Reset();
        }

        /// <summary>
        /// The on delete template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        private void OnTemplateDeleted(ITemplate template)
        {
            this.m_hierarchyBrowser.DeleteTemplate(template);
            m_propertiesEditor.Reset();
        }

        /// <summary>
        /// The on find object.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        private void OnFindObject(IObjectInstance objectInstance)
        {
            if (objectInstance == null)
            {
                return;
            }

            if (this.m_browsers.ContainsKey(objectInstance.Type))
            {
                selectBrowser(this.m_browsers[objectInstance.Type]);
                this.m_browsers[objectInstance.Type].FindObject(objectInstance);
            }
            else if (this.m_hierarchies.ContainsKey(objectInstance.Type))
            {
                selectBrowser(this.m_hierarchies[objectInstance.Type]);
                this.m_hierarchies[objectInstance.Type].ShowNode(objectInstance);
            }
        }

        public void OnFindObject(string path, string episode, string type)
        {
            if (string.IsNullOrEmpty(path) && string.IsNullOrEmpty(type))
            {
                return;
            }

            if (this.m_browsers.ContainsKey(type.ToUpper()))
            {
                selectBrowser(this.m_browsers[type.ToUpper()]);
                this.m_browsers[type.ToUpper()].FindObject(path, episode);
            }
            else if (this.m_hierarchies.ContainsKey(type.ToUpper()))
            {
                selectBrowser(this.m_hierarchies[type.ToUpper()]);
                this.m_hierarchies[type.ToUpper()].ShowNode(ObjectInstance.GetObjectMatchingName(path));
            }
            else if (type == "Waves")
            {
                selectBrowser(this.m_waveBrowser);
                string bank = path.Substring(0, path.Length - Path.GetFileName(path).Length - 1);
                string wavName = Path.GetFileNameWithoutExtension(path);
                this.m_waveBrowser.GetEditorTreeView().SelectNode(bank, wavName);

            }
            else if (type == "WaveFolders")
            {
                selectBrowser(this.m_waveBrowser);
                string bank = path.Substring(0, path.Length - Path.GetFileName(path).Length - 1);
                this.m_waveBrowser.GetEditorTreeView().SelectNode(bank, null);
            }
            else if (type == "Banks")
            {
                selectBrowser(this.m_waveBrowser);
                this.m_waveBrowser.GetEditorTreeView().SelectNode(path, null);
            }
        }

        /// <summary>
        /// The on find template.
        /// </summary>
        /// <param name="proxyObject">
        /// The proxy object.
        /// </param>
        private void OnFindTemplateFromProxy(ITemplateProxyObject proxyObject)
        {
            if (null == proxyObject)
            {
                return;
            }

            this.OnFindTemplate(proxyObject.Template);
        }

        /// <summary>
        /// The on find template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        private void OnFindTemplate(ITemplate template)
        {
            if (null == template)
            {
                return;
            }

            if (this.templateBrowsers.ContainsKey(template.TemplateBank.Type))
            {
                this.templateBrowsers[template.TemplateBank.Type].FindTemplate(template);
            }
        }

        /// <summary>
        /// The on create floating editor click.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        private void m_propertiesEditor_OnCreateFloatingEditorClick(IObjectInstance obj)
        {
            this.CreateFloatingEditor(obj);
        }

        /// <summary>
        /// The prompt for save.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool PromptForSave()
        {
            bool returnVal = XmlBankManager.PromptOnExit(RaveInstance.AllBankManagers);
            this.m_assetManager.RefreshView();
            return returnVal;
        }

        /// <summary>
        /// The properties editor_ on wave bank ref click.
        /// </summary>
        /// <param name="bank">
        /// The bank.
        /// </param>
        private void PropertiesEditor_OnWaveBankRefClick(string bank)
        {
            PropertiesEditor_OnWaveRefClick(bank, null);
        }

        /// <summary>
        /// The properties editor_ on wave ref audition.
        /// </summary>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="wave">
        /// The wave.
        /// </param>
        private void PropertiesEditor_OnWaveRefAudition(string bank, string wave)
        {
            this.m_waveBrowser.GetEditorTreeView().AuditionWave(bank, wave);
        }

        /// <summary>
        /// The properties editor_ on wave ref click.
        /// </summary>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="wave">
        /// The wave.
        /// </param>
        private void PropertiesEditor_OnWaveRefClick(string bank, string wave)
        {
            selectBrowser(m_waveBrowser);
            this.m_waveBrowser.GetEditorTreeView().SelectNode(bank, wave);
        }

        /// <summary>
        /// The report_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Report_Click(object sender, RoutedEventArgs e)
        {
            var pluginName = ((MenuItem)sender).Tag.ToString();
            foreach (var plugin in RaveInstance.PluginManager.ReportPlugins)
            {
                if (plugin.GetName() == pluginName)
                {
                    try
                    {
                        Dictionary<string, IObjectBrowser> iObjectBrowsers = new Dictionary<string, IObjectBrowser>();
                        foreach (var kvp in this.m_browsers)
                        {
                            iObjectBrowsers.Add(kvp.Key, kvp.Value);
                        }

                        plugin.DisplayResults(this.m_waveBrowser, iObjectBrowsers);
                        break;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error running the report");
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// The save layout_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SaveLayout_Click(object sender, RoutedEventArgs e)
        {
            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(null, "Please enter a name for the layout", true);
            if (input.HasBeenCancelled) return;
            string name = input.data;
            if (!string.IsNullOrEmpty(name))
            {
                string filePath = string.Format(@"{0}\{1}.xml", GetLayoutStorePath(), name);
                var serializer = new XmlLayoutSerializer(dockManager);
                using (var stream = new StreamWriter(filePath))
                    serializer.Serialize(stream);
            }

            this.CreateLoadLayoutMenu();
        }

        /// <summary>
        /// The save_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Save_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var banks = RaveInstance.CheckedOutBanks;

            var savedCount = 0;
            foreach (var bank in banks)
            {
                if (bank.IsDirty)
                {
                    if (!bank.Save())
                    {
                        ErrorManager.HandleError("Failed to save bank: " + bank.Name);
                    }
                    else
                    {
                        savedCount++;
                    }
                }
            }

            if (savedCount > 0)
            {
                MessageBox.Show("Saved changes to " + savedCount + " bank(s)");
            }
        }

        /// <summary>
        /// The save check in project_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SaveCheckInProject_Click(object sender, RoutedEventArgs e)
        {
            XmlBankManager.PromptOnExit(RaveInstance.AllBankManagers, false);
            this.m_assetManager.RefreshView();
        }

        /// <summary>
        /// The show save dialog_ executed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ShowSaveDialog_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            XmlBankManager.PromptOnExit(RaveInstance.AllBankManagers, false);
            this.m_assetManager.RefreshView();
        }

        /// <summary>
        /// The search objects_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SearchObjects_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            var searchForm = new frmSearch(menuItem.Header.ToString(), menuItem.Header.ToString());
            searchForm.FindObject += this.OnFindObject;
            searchForm.Show(this.WindowHandle);
        }

        /// <summary>
        /// The template browser_ on template selected.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        private void TemplateBrowser_OnTemplateSelected(ITemplate template)
        {
            if (m_hierarchyBrowser.AutoUpdate)
            {
                this.m_hierarchyBrowser.ShowHierarchy(template);
            }
            if (m_propertiesEditor.AutoUpdate)
            {
                this.m_propertiesEditor.EditObject(template.Parent);
            }
        }

        /// <summary>
        /// The window_ closing.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                if (!this.PromptForSave())
                {
                    e.Cancel = true;
                    return;
                }

                if (!this.m_waveBrowser.OnClose())
                {
                    e.Cancel = true;
                    return;
                }

                this.m_waveBrowser.Dispose();
                this.m_hierarchyBrowser.Dispose();

                Process.GetCurrentProcess().Kill();
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }
        }

        /// <summary>
        /// The window_ content rendered.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            string lastLayout = GetDefaultLayout();
            if (lastLayout != null && File.Exists(lastLayout))
            {
                restoreLayout(lastLayout);

            }
        }

        /// <summary>
        /// The browser_ on object select.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        private void browser_OnObjectSelect(IObjectInstance objectInstance)
        {
            App.Instance.OnObjectSelect(objectInstance);

            if (m_hierarchyBrowser.AutoUpdate)
            {
                this.m_hierarchyBrowser.ShowHierarchy(objectInstance);
            }
            if (m_propertiesEditor.AutoUpdate)
            {
                this.m_propertiesEditor.EditObject(objectInstance);
            }
        }

        /// <summary>
        /// The browser_ on sound select_ no_ heirarchy.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        private void browser_OnSoundSelect_No_Heirarchy(IObjectInstance objectInstance)
        {
            App.Instance.OnObjectSelect(objectInstance);
            if (m_propertiesEditor.AutoUpdate)
            {
                this.m_propertiesEditor.EditObject(objectInstance);
            }
        }

        /// <summary>
        /// The create browser item_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void createBrowserItem_Click(object sender, RoutedEventArgs e)
        {
            var menu = sender as MenuItem;
            if (menu != null)
            {
                var m = menu.Tag as audMetadataType;
                if (m != null)
                {
                    this.CreateMetadataBrowser(m, false);
                }
            }
        }

        /// <summary>
        /// The mnu batch build_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuBatchBuild_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            string platformTag = menuItem.Header.ToString();
            var buildSet = menuItem.Tag as audBuildSet;
            RaveInstance.BuildManager.Build(buildSet, platformTag);
            this.m_assetManager.RefreshView();
        }

        /// <summary>
        /// The mnu edit clipboard_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuEditClipboard_Click(object sender, RoutedEventArgs e)
        {
            if (TypeUtils.ObjectClipboard != null)
            {
                var editor = new frmClipboardEditor();
                if (editor.Init(TypeUtils.ObjectClipboard))
                {
                    editor.Show(this.WindowHandle);
                }
            }
        }

        /// <summary>
        /// The mnu file exit_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuFileExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// The mnu help about_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuHelpAbout_Click(object sender, RoutedEventArgs e)
        {
            var frm = new frmAbout { Opacity = 1.0f };
            frm.ShowDialog(this.WindowHandle);
        }

        /// <summary>
        /// The mnu load type defs_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuLoadTypeDefs_Click(object sender, RoutedEventArgs e)
        {
            App.Instance.LoadTypeDefinitions();
            this.m_propertiesEditor.UpdateDisplay();
        }

        /// <summary>
        /// The mnu reconnect_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuReconnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RaveInstance.AssetManager.Reconnect();
                ErrorManager.HandleInfo("Reconnect Successful");
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }
        }

        /// <summary>
        /// The show menu item_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void showMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var menu = sender as MenuItem;
            if (menu != null)
            {
                bool found = selectBrowser(menu.Tag);

                if (!found)
                {
                    Rave.Utils.ErrorManager.HandleInfo("Browser not found in dockManager");
                }
            }
        }

        /// <summary>
        /// Selects the LayoutAnchorable that references the browser parameter and brings it to the front
        /// </summary>
        /// <param name="browser">reference to the browser that is the LayoutAnchorables content</param>
        /// <returns>true if browser reference has been found in available LayoutAnchorables</returns>
        private bool selectBrowser(object browser)
        {


            foreach (LayoutAnchorablePane pane in this.dockManager.Layout.Descendents().OfType<LayoutAnchorablePane>())
            {
                //read through all visible and hidden LayoutAnchorables
                foreach (LayoutAnchorable content in pane.Descendents().OfType<LayoutAnchorable>().Union(this.dockManager.Layout.Hidden))
                {
                    object currentAnchorablesBrowser = content.Content;

                    if (content.Content is WindowsFormsHost)
                        currentAnchorablesBrowser = ((WindowsFormsHost)content.Content).Child;

                    if (currentAnchorablesBrowser == browser)
                    {
                        content.Show();
                        content.IsSelected = true;
                        return true;
                    }
                }
            }

            foreach (LayoutFloatingWindowControl content in this.dockManager.FloatingWindows)
            {
                object currentFloatingWindowBrowser = content.Content;
                if (content.Content is WindowsFormsHost)
                    currentFloatingWindowBrowser = ((WindowsFormsHost)content.Content).Child;

                if (currentFloatingWindowBrowser == browser)
                {
                    content.Show();
                    content.Activate();
                    return true;
                }
            }
            PropertyInfo info = browser.GetType().GetProperties().First(p => p.Name == "Content");
            if (info != null)
            {
                LayoutAnchorable layoutcontent = info.GetValue(browser, null) as LayoutAnchorable;

                if (layoutcontent != null)
                {
                    LayoutAnchorGroup layoutAnchorGroup = new LayoutAnchorGroup();
                    layoutcontent.Parent = null;

                    layoutcontent.AddToLayout(this.dockManager, AnchorableShowStrategy.Top | AnchorableShowStrategy.Right);
                    this.dockManager.Layout.RightSide.Children.Add(layoutAnchorGroup);

                    layoutcontent.Float();

                    return true;
                }
            }
            return false;
        }


        /// <summary>
        /// The w_ do work.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void w_DoWork(object sender, DoWorkEventArgs e)
        {
            var platform = e.Argument as string;
            string message;
            if (App.GetLatestRuntimeData(platform, out message))
            {
                ErrorManager.HandleInfo(message);
            }
            else
            {
                ErrorManager.HandleError(message);
            }
        }

        /// <summary>
        /// The w_ run worker completed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void w_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.m_GetLatestBusyForm.Close();
            this.m_GetLatestBusyForm = null;
        }

        #endregion

        #region Search
        private void SearchTextBox_OnSearch(object sender, RoutedEventArgs e)
        {
            SearchResults window = null;

            if (this.dockManager.Layout.RightSide.Children.Contains(SearchLayoutAnchor))
            {

                window = new SearchResults(e as SearchEventArgs);
                window.Content.AddToLayout(this.dockManager, AnchorableShowStrategy.Top | AnchorableShowStrategy.Right);

                for (int i = 0; i < SearchLayoutAnchor.ChildrenCount; i++)
                {
                    if (SearchLayoutAnchor.ChildrenCount > i)
                    {
                        SearchResults previousWindow = (SearchResults)SearchLayoutAnchor.Children[i].Content;
                        if (previousWindow != null && previousWindow.ViewModel != null)
                        {
                            previousWindow.ViewModel.Dispose();
                        }
                        SearchLayoutAnchor.RemoveChildAt(0);
                    }
                }

                SearchLayoutAnchor.Children.Add(window.Content);

            }
            else
            {
                foreach (LayoutAnchorablePane pane in this.dockManager.Layout.Descendents().OfType<LayoutAnchorablePane>())
                {
                    foreach (LayoutAnchorable content in pane.Descendents().OfType<LayoutAnchorable>())
                    {
                        object currentAnchorablesBrowser = content.Content;

                        if (content.Content.GetType() == typeof(SearchResults) && content.IsFloating == false)
                        {
                            SearchResults previousWindow = (SearchResults)content.Content;
                            if (previousWindow != null && previousWindow.ViewModel != null)
                            {
                                previousWindow.ViewModel.Dispose();
                            }
                            window = new SearchResults(e as SearchEventArgs);
                            content.Content = window;
                        }
                    }
                }
            }

            if (window == null)
            {
                window = new SearchResults(e as SearchEventArgs);

                SearchLayoutAnchor = new LayoutAnchorGroup();


                window.Content.AddToLayout(this.dockManager, AnchorableShowStrategy.Top | AnchorableShowStrategy.Right);
                SearchLayoutAnchor.Children.Add(window.Content);

                this.dockManager.Layout.RightSide.Children.Add(SearchLayoutAnchor);

            }

            searchResultWindow = window;
            window.Content.IsActive = true;
            window.Content.IsSelected = true;
            window.Content.CanFloat = true;
            window.ViewModel.FindObject += this.OnFindObject;
        }

        private void SearchTextBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ValidateSearch();
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchTextBox.SearchArgumentList != null)
            {
                SearchTextBox.SearchArgumentList.SelectionChanged -= new SelectionChangedEventHandler(SearchTextBox_SelectionChanged);
                SearchTextBox.SearchArgumentList.SelectionChanged += new SelectionChangedEventHandler(SearchTextBox_SelectionChanged);
            }
            ValidateSearch();
        }

        private void ValidateSearch()
        {
            bool isValid = true;

            if (SearchTextBox.SelectedArguments.Contains("Use Regular Expressions"))
            {
                string text = SearchTextBox.Text;

                if ((text != null) && (text.Trim().Length > 0))
                {
                    try
                    {
                        Regex.Match("", text);
                    }
                    catch (ArgumentException e)
                    {

                        isValid = false;
                        SearchTextBox.ToolTip = e.Message;
                    }
                }
                else
                {
                    SearchTextBox.ToolTip = "Enter a regular expression";
                    isValid = false;
                }

            }

            SearchTextBox.IsValidText = isValid;

            if (isValid)
            {
                SearchTextBox.ToolTip = "Press enter to search..";
            }

        }
        #endregion

        #region TitleBarRegion

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Normal)
            {
                this.WindowState = System.Windows.WindowState.Maximized;
                this.PropertyChanged.Raise(this, "IsMaximized");

            }
            else
            {
                this.WindowState = System.Windows.WindowState.Normal;
                this.PropertyChanged.Raise(this, "IsMaximized");
            }
        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        private void MainWindow_StateChanged(object sender, EventArgs e)
        {
            WindowInteropHelper windowInteropHelper = new WindowInteropHelper(this);
            this.MaxHeight = System.Windows.Forms.Screen.FromHandle(windowInteropHelper.Handle).WorkingArea.Height + 10;

            this.PropertyChanged.Raise(this, "IsMaximized");
        }

        private void RaveIconMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                this.Close();
            }
        }
        #endregion

        private UserPreferencesWindow _userPreferencesWindow;
        private void OpenUserPreferences(object sender, RoutedEventArgs e)
        {
            if (_userPreferencesWindow == null || !_userPreferencesWindow.IsVisible)
            {
                _userPreferencesWindow = new UserPreferencesWindow();

                _userPreferencesWindow.Show();
                _userPreferencesWindow.BringIntoView();
            }

        }
    }
}

