using Rave.Core.SoundBrowser.Nodes;
using Rave.Core.WaveBrowser.Nodes;

namespace rave.PropertiesEditor.NodeInterfacces
{
    public interface IObjectRef
    {
        void Drop(SoundNode soundNode);

        void Drop(WaveNode waveNode);
    }
}