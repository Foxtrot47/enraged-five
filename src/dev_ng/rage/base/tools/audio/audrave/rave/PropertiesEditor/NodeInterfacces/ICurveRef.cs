using Rave.Core.SoundBrowser.Nodes;

namespace rave.PropertiesEditor.NodeInterfacces
{
    public interface ICurveRef
    {
        void Drop(SoundNode soundNode);
    }
}