namespace Rave.SpeechExport
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Utils.Infrastructure.Interfaces;

    using rageUsefulCSharpToolClasses;

    using Rave.Instance;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.Utils.Popups;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    using Wavelib;

    public class SpeechExport: IRAVEWaveBrowserPlugin
    {
        private string m_Name;
        private string m_Command;
        private string m_Parameters;
        private string m_Delimiter;
        private bool m_WriteToFile;
        private bool m_Skip;
        private bool m_RemoveVisemes;

        private IList<WaveNode> m_Nodes;
        private IBusy m_Busy;
        private IActionLog m_ActionLog;
        private IWaveBrowser m_WaveBrowser;
        private string m_Root;
        private List<string> m_CheckedOut;
        
        #region IRAVEPlugin Members

        public string GetName()
        {
            return m_Name;
        }

        public override string ToString()
        {
            return m_Name;
        }

        public bool Init(XmlNode settings)
        {
            m_WavesWithLipsyncData = new List<string>();
            foreach (XmlNode setting in settings.ChildNodes)
            {

                switch (setting.Name)
                {
                    case "Name":
                        m_Name = setting.InnerText;
                        break;
                    case "Command":
                        m_Command = setting.InnerText;
                        break;
                    case "Parameters":
                        m_Parameters = setting.InnerText;
                        break;
                    case "Delimiter":
                        m_Delimiter = setting.InnerText;
                        if (m_Delimiter == "")
                        {
                            m_Delimiter = " ";
                        }
                        break;
                    case "WriteToFile":
                        m_WriteToFile = setting.InnerText == "True";
                        break;
                    case "SkipCustomMetadata":
                        m_Skip = setting.InnerText == "True";
                        break;
                    case "RemoveVisemes":
                        m_RemoveVisemes = setting.InnerText == "True";
                        break;
                }
            }
            
            //nodes don't exist
            return m_Name != "" && m_Command != "";
        }

        public void Shutdown()
        {
        }

        #endregion

        #region IRAVEWaveBrowserPlugin Members

        public void Process(IWaveBrowser waveBrowser, IActionLog actionLog, ArrayList nodes, IBusy busyFrm)
        {
            m_Busy = busyFrm;
            m_WaveBrowser = waveBrowser;
            m_ActionLog = actionLog;
            m_CheckedOut = new List<string>();

            // get our root path from the asset manager
            m_Root = Configuration.PlatformWavePath;
            m_Nodes = new List<WaveNode>();

            // process each selected node in the wave browser
            foreach (EditorTreeNode n in nodes)
            {
                waveBrowser.GetEditorTreeView().GetSelectedWaveNodes(n, m_Skip, m_Nodes);
            }

            //Check for Errors
            if (m_Nodes == null || m_Nodes.Count == 0)
            {
                m_Busy.Close();
                return;
            }

            //Check for any pending actions
            if (m_ActionLog.GetCommitLogCount() > 0)
            {
                ErrorManager.HandleInfo("Please commit previous changes before editing waves");
                return;
            }

            if (!CheckOutWaves() || m_CheckedOut.Count==0) return;


            string fileList = CreateFileList();

            string fileName = Path.GetTempFileName();
            if (m_WriteToFile)
            {
                FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                try
                {
                    sw.WriteLine(fileList);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                }
                finally
                {
                    sw.Close();
                }
                fs.Dispose();

                //set fileList for parameters to be filename
                //quotes added in case of spaces in path
                fileList = "\"" + fileName + "\"";
            }

            rageStatus status;
            rageExecuteCommand cmd = new rageExecuteCommand(m_Command, m_Parameters + fileList, out status);

            if (m_WriteToFile && File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            if (status.Success())
            {
                if (ErrorManager.HandleQuestion("The Speech Export plugin successfully completed. Would you like to commit your changes?", MessageBoxButtons.YesNo, DialogResult.Yes) == DialogResult.Yes)
                {
                    CheckIn();
                }
                else
                {
                    UndoCheckOut();
                }
            }
            else
            {
                UndoCheckOut();
                string errorMsg = "The external script failed - please check the log file (" + cmd.GetLogFilename() + ") for details.";
                ErrorManager.HandleError(errorMsg);
            }

            CleanupTemporaryFiles();
        }

        private string CreateFileList()
        {
            // set the file list to the first element
            string fileList = m_CheckedOut[0];

            // do we have more than 1 item in the list?  append a comma
            if (m_CheckedOut.Count > 1)
                fileList += m_Delimiter;

            // iterate over the entire list
            for (int i = 1; i < m_CheckedOut.Count; ++i)
            {
                if (i >= m_CheckedOut.Count - 1)
                    fileList += m_CheckedOut[i];
                else
                    fileList += (m_CheckedOut[i] + m_Delimiter);
            }

            return fileList;
        }

        List<string> m_WavesWithLipsyncData;

        private bool CheckOutWaves()
        {
            if (RaveInstance.RaveAssetManager.WaveChangeList == null)
            {
                try
                {
                    RaveInstance.RaveAssetManager.WaveChangeList = RaveInstance.AssetManager.CreateChangeList("[Rave] Batch Wave Change");
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                    return false;
                }
            }

            StringBuilder sb = new StringBuilder();

            //reset form
            m_Busy.SetProgress(0);
            m_Busy.SetStatusText("Checking out Waves");

                // check out all selected waves
                int j = 0;
                m_Busy.Show();

                foreach (WaveNode wave in m_Nodes)
                {
                    m_Busy.SetProgress((int)((j++ / (float)m_Nodes.Count) * 100.0f));
                    m_Busy.SetStatusText("Checking out " + wave.GetObjectPath());
                    string assetPath = Configuration.PlatformWavePath + wave.GetObjectPath();
                    try
                    {
                        RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(assetPath, true);
                        m_CheckedOut.Add(assetPath);
                        bwWaveFile waveFile = new bwWaveFile(assetPath, false);
                        if (!m_RemoveVisemes)
                        {
                            IRiffChunk chunk = waveFile.GetChunk("XARB");
                            if(chunk != null)
                            {
                                bwRaveChunk lipsyncChunk = chunk as bwRaveChunk;
                                if(lipsyncChunk != null)
                                {
                                    BinaryWriter lipsyncWriter = new BinaryWriter(new FileStream(assetPath + ".lipsync", FileMode.CreateNew));
                                    m_WavesWithLipsyncData.Add(assetPath);
                                    lipsyncWriter.Write(lipsyncChunk.Bytes);
                                    lipsyncWriter.Close();
                                }
                            }
                        }
                        // strip off XARB etc chunks since sound forge etc don't like them
                        waveFile.RemoveRaveChunks();
                        waveFile.Save();
                    }
                    catch (Exception e)
                    {
                        sb.AppendLine(e.Message);
                    }
                   
                    Application.DoEvents();
                }
                m_Busy.Hide();

                if (sb.Length != 0)
                {
                    if (ErrorManager.HandleQuestion("Rave Encountered the following errors during check out. Do you wish to continue\r\n" +
                                        sb.ToString(), MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.No)
                    {
                        try
                        {
                            RaveInstance.RaveAssetManager.WaveChangeList.Revert(false);
                        }
                        catch (Exception e)
                        {
                            ErrorManager.HandleError(e);
                        }

                        return false;
                    }
                }
    
            return true;
        }

        void CleanupTemporaryFiles()
        {
            foreach (string wavePath in m_WavesWithLipsyncData)
            {
                if(File.Exists(wavePath + ".lipsync"))
                {
                    File.Delete(wavePath + ".lipsync");
                }
            }
        }

        private void CheckIn()
        {
            m_Busy.SetUnknownDuration(true);
            m_Busy.SetStatusText("Checking in Waves");
            //add change actions to pending waves

            try
            {
                foreach (string wavePath in m_CheckedOut)
                {
                    if(m_WavesWithLipsyncData.Contains(wavePath))
                    {
                        // restore XARB chunk
                        BinaryReader lipsyncReader = new BinaryReader(new FileStream(wavePath + ".lipsync", FileMode.Open));
                        byte[] lipsyncData = lipsyncReader.ReadBytes((int)lipsyncReader.BaseStream.Length);
                        lipsyncReader.Close();
                        bwWaveFile waveFile = new bwWaveFile(wavePath);
                        bwRaveChunk lipsyncChunk = new bwRaveChunk("XARB", lipsyncData);
                        waveFile.AddRaveChunk(lipsyncChunk);
                        waveFile.Save();
                    }

                    string path = wavePath.ToUpper().Replace(m_Root.ToUpper(), "");

                    foreach (rage.PlatformSetting ps in Configuration.ActivePlatformSettings)
                    {
                        m_WaveBrowser.GetPendingWaveLists().GetPendingWaveList(ps.PlatformTag).RecordModifyWave(path);
                        Application.DoEvents();
                    }
                }

                IUserAction dummyAction = m_WaveBrowser.CreateAction(ActionType.Dummy, null);
                if (dummyAction.Action())
                {
                    m_WaveBrowser.GetActionLog().AddAction(dummyAction);
                }

                // Submit all wave file changes
                if (!m_WaveBrowser.CommitChanges(false)) return;
            }

            catch (Exception e)
            {
                ErrorManager.HandleError("Commit Failed! " + e.Message);
                RaveInstance.RaveAssetManager.WaveChangeList.Revert(false);
                return;
            }

            ErrorManager.HandleInfo("Commit Successful");
            return;
        }

        private void UndoCheckOut()
        {
            m_Busy.Show();
            m_Busy.SetProgress(-1);
            m_Busy.SetStatusText("Undo Checkout of Waves");
            Application.DoEvents();

            try
            {
                RaveInstance.RaveAssetManager.WaveChangeList.Revert(false);
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
            }

            m_Busy.Close();
        }

        #endregion
    }
}
