namespace Rave.SpeechImport
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;

    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.UserActions;

    using Wavelib;

    public class SpeechImport : IRAVEWaveImportPlugin
    {
        private bool m_createVoiceTag;
        private string m_name;

        #region IRAVEWaveImportPlugin Members

        public string GetName()
        {
            return m_name;
        }

        public bool Init(XmlNode settings)
        {
            string createTag;
            var name = createTag = null;

            foreach (XmlNode setting in settings.ChildNodes)
            {
                switch (setting.Name)
                {
                    case "Name":
                        name = setting.InnerText;
                        break;
                    case "VoiceTag":
                        createTag = setting.InnerText;
                        break;
                }
            }
            //nodes don't exist
            if (name == null ||
                createTag == null)
            {
                return false;
            }

            m_name = name;

            if (string.Compare(createTag, "true", true) == 0 ||
                string.Compare(createTag, "yes", true) == 0)
            {
                m_createVoiceTag = true;
            }
            return true;
        }

        public void HandleDroppedFiles(IWaveBrowser waveBrowser,
                                       EditorTreeNode selectedNode,
                                       IActionLog actionLog,
                                       string[] files)
        {
            var p = selectedNode as PackNode;
            if (p == null)
            {
                ErrorManager.HandleInfo("Folders containing speech may only be dropped onto a Pack or BankFolder");
            }
            else
            {
                try
                {
                    EditorTreeNode.AllowAutoLock = true;

                    var actions = new ArrayList();
                    ImportFolders(waveBrowser, actionLog, selectedNode, files, actions, m_createVoiceTag);
                    if (actions.Count > 0)
                    {
                        var action = waveBrowser.CreateAction(ActionType.BulkAction,
                                                              new ActionParams(null,
                                                                               null,
                                                                               null,
                                                                               null,
                                                                               null,
                                                                               actionLog,
                                                                               null,
                                                                               null,
                                                                               null,
                                                                               actions));
                        if (action.Action())
                        {
                            actionLog.AddAction(action);
                        }
                    }
                }
                finally
                {
                    EditorTreeNode.AllowAutoLock = false;
                }
            }
        }

        #endregion

        public override string ToString()
        {
            return GetName();
        }

        public void ImportFolders(IWaveBrowser waveBrowser,
                                  IActionLog actionLog,
                                  EditorTreeNode parentNode,
                                  string[] files,
                                  ArrayList actions,
                                  bool voiceTag)
        {
            if (!ValidateFiles(files)) return;

            foreach (var file in files)
            {
                var name = file.Substring(file.LastIndexOf("\\") + 1);
                //not folder
                if (name.Contains("."))
                {
                    //not a folder
                    ErrorManager.HandleInfo(name + " is not a folder so will not be imported");
                }
                    //passed file is a folder
                else
                {
                    //check if the current node has bank nodes with the same name already
                    var bank = GetChildNode(parentNode, name);
                    var node = parentNode as BankNode;
                    //if bank does not exist create a new bank
                    if (bank == null)
                    {
                        //parent node is not a bank node i.e. we are creating a bank from the passed in folder
                        if (node == null)
                        {
                            var action = waveBrowser.CreateAction(ActionType.AddBank,
                                                                  new ActionParams(waveBrowser,
                                                                                   parentNode,
                                                                                   null,
                                                                                   null,
                                                                                   waveBrowser.GetEditorTreeView().
                                                                                       GetTreeView(),
                                                                                   actionLog,
                                                                                   name,
                                                                                   parentNode.GetPlatforms(),
                                                                                   null,
                                                                                   null));
                            if (action.Action())
                            {
                                actions.Add(action);
                            }
                            else
                            {
                                return;
                            }
                            //get added bank as node
                            bank = GetChildNode(parentNode, name);
                            //need to create tagnode for bank
                            if (voiceTag)
                            {
                                CreateTagNode(waveBrowser, bank, actions);
                            }
                        }
                        else
                        {
                            var action = waveBrowser.CreateAction(ActionType.AddWaveFolder,
                                                                  new ActionParams(waveBrowser,
                                                                                   parentNode,
                                                                                   null,
                                                                                   null,
                                                                                   null,
                                                                                   actionLog,
                                                                                   name,
                                                                                   parentNode.GetPlatforms(),
                                                                                   null,
                                                                                   null));
                            if (action.Action())
                            {
                                actions.Add(action);
                            }
                            else
                            {
                                return;
                            }
                            //get added bank as node
                            bank = GetChildNode(parentNode, name);
                        }
                    }
                    //at this point the bank should exist, create lists of children of the directory
                    var currentFolder = new DirectoryInfo(file);
                    var childFiles = currentFolder.GetFiles();
                    var childDirectories = currentFolder.GetDirectories();

                    //get the paths for the files/directories and pass to the appropriate methods
                    var filePaths = new string[childFiles.Length];
                    var directoryPaths = new string[childDirectories.Length];

                    for (var i = 0; i < filePaths.Length; i++)
                    {
                        filePaths[i] = childFiles[i].FullName;
                    }

                    for (var i = 0; i < directoryPaths.Length; i++)
                    {
                        directoryPaths[i] = childDirectories[i].FullName;
                    }

                    //import files and directories
                    waveBrowser.GetEditorTreeView().SetText("Importing: " + name);
                    ImportWaves(waveBrowser, actionLog, bank, filePaths, actions);
                    ImportFolders(waveBrowser, actionLog, bank, directoryPaths, actions, voiceTag);
                }
            }
        }

        //returns child bank node that matches name
        public static BankNode GetChildNode(EditorTreeNode parentNode, string name)
        {
            for (var i = 0; i < parentNode.Nodes.Count; i++)
            {
                var node = parentNode.Nodes[i] as BankNode;
                if (node != null)
                {
                    if (string.Equals(node.GetObjectName(), name, StringComparison.OrdinalIgnoreCase))
                    {
                        return node;
                    }
                }
            }

            return null;
        }

        private bool ValidateFiles(string[] files)
        {
            var errors = ValidateFilesHelper(files);
            if (!string.IsNullOrEmpty(errors))
            {
                ErrorManager.HandleError("Unable to continue due to following error(s):" + Environment.NewLine + errors);
                return false;
            }
            return true;
        }

        private string ValidateFilesHelper(string[] files)
        {
            var errors = "";

            foreach (var file in files)
            {
                var name = file.Substring(file.LastIndexOf("\\") + 1);

                // Is a file
                if (name.Contains("."))
                {
                    // Ignore non-wav files
                    if (!file.ToUpper().EndsWith("WAV")) continue;

                    if (Regex.Match(name.ToUpper(), @"_\d{2}\.(WAV)").Length == 0)
                    {
                        errors += Path.GetFullPath(file) +
                                  " is invalid: Filename must end with '_XX.wav' where X represents a digit." + 
                                  Environment.NewLine;
                    }
                }
                // Recursively process contents of folder
                else
                {
                    var innerFiles = (Directory.GetFiles(file));
                    errors += ValidateFilesHelper(innerFiles);
                }
            }
            return errors;
        }

        private static void CreateTagNode(IWaveBrowser waveBrowser, BankNode parent, IList actions)
        {
            var parentName = parent.GetObjectName();

            var values = parent.GetPlatforms().ToDictionary(platform => platform, platform => parentName);

            var action = waveBrowser.CreateAction(ActionType.AddTag,
                                                  new ActionParams(waveBrowser,
                                                                   parent,
                                                                   null,
                                                                   null,
                                                                   null,
                                                                   null,
                                                                   "voice",
                                                                   parent.GetPlatforms(),
                                                                   values,
                                                                   null));
            if (action.Action())
            {
                actions.Add(action);
            }
        }

        private static void ImportWaves(IWaveBrowser waveBrowser,
                                        IActionLog actionLog,
                                        BankNode bank,
                                        IEnumerable<string> files,
                                        IList actions)
        {
            foreach (var file in files)
            {
                if (file.ToUpper().EndsWith(".WAV"))
                {
                    //Ensures valid wave
                    new bwWaveFile(file, false);

                    var newWaveName = file.ToUpper().Substring(file.LastIndexOf("\\") + 1).Replace(" ", "_");

                    WaveNode existingWave = null;
                    for (var i = 0; i < bank.Nodes.Count; i++)
                    {
                        if (bank.Nodes[i].GetType() ==
                            typeof (WaveNode))
                        {
                            if (((WaveNode) bank.Nodes[i]).GetObjectName().ToUpper() == newWaveName)
                            {
                                existingWave = (WaveNode) bank.Nodes[i];
                            }
                        }
                    }

                    waveBrowser.GetEditorTreeView().SetText("Importing: " + bank.GetObjectName() + "/" + newWaveName);
                    if (existingWave == null)
                    {
                        // this is a new wave file
                        var action = waveBrowser.CreateAction(ActionType.AddWave,
                                                              new ActionParams(waveBrowser,
                                                                               bank,
                                                                               null,
                                                                               file,
                                                                               null,
                                                                               actionLog,
                                                                               null,
                                                                               bank.GetPlatforms(),
                                                                               null,
                                                                               null));
                        if (action.Action())
                        {
                            actions.Add(action);
                        }
                    }
                    else
                    {
                        var action = waveBrowser.CreateAction(ActionType.ChangeWave,
                                                              new ActionParams(waveBrowser,
                                                                               bank,
                                                                               existingWave,
                                                                               file,
                                                                               null,
                                                                               actionLog,
                                                                               null,
                                                                               bank.GetPlatforms(),
                                                                               null,
                                                                               null));
                        if (action.Action())
                        {
                            actions.Add(action);
                        }
                    }
                }
            }
        }
    }
}