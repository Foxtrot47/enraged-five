﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rave.MuteSoloManager.Model
{
    [Serializable]
    public class MuteSoloObject
    {
        public string Name { get; set; }
        public string TypeName { get; set; }

        public MuteSoloObject(string name, string typeName)
        {
            this.Name = name;
            this.TypeName = typeName;
        }

        public MuteSoloObject()
        {
        }

        public override int GetHashCode()
        {
            return (Name.GetHashCode() ^ TypeName.GetHashCode());
        }

       
        public override bool Equals(object obj)
        {
            if (obj is MuteSoloObject)
            {
                MuteSoloObject msObj = (MuteSoloObject)obj;

                if (msObj == null)
                {
                    return false;
                }

                return ((Name == msObj.Name) && (TypeName == msObj.TypeName));
            }
            else
            {
                return false;
            }
        }
        
       
    }
}
