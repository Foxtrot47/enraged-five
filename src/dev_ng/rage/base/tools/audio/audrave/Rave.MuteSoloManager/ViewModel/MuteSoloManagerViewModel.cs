#region

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using GalaSoft.MvvmLight.Command;
using rage.ToolLib;
using Rave.MuteSoloManager.Model;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.Types.Objects;
using WPFToolLib.Extensions;

#endregion

namespace Rave.MuteSoloManager.ViewModel
{
    public class MuteSoloManagerViewModel : INotifyPropertyChanged
    {
        public static Dictionary<string, string> TypeToPath = new Dictionary<string, string>();
        private readonly ObservableCollection<FileInfo> _savedMuteSoloPaths = new ObservableCollection<FileInfo>();
        private RelayCommand<FileInfo> _deleteCommad;
        private RelayCommand<MuteSoloObject> _deleteMuteCommand;
        private RelayCommand<MuteSoloObject> _deleteSoloCommand;
        private RelayCommand<FileInfo> _doubleClickCommand;
        private RelayCommand<string> _findObjectCommand;
        private RelayCommand<FileInfo> _rightClickCommand;
        private RelayCommand<string> _saveCommand;
        private RelayCommand<string> _unMuteAllCommand;
        private RelayCommand<string> _unSoloAllCommand;

        public RelayCommand<FileInfo> DeleteCommand
        {
            get
            {
                if (_deleteCommad == null)
                {
                    _deleteCommad = new RelayCommand<FileInfo>(
                        param => DeleteData(param)
                        );
                }
                return _deleteCommad;
            }
        }

        public RelayCommand<MuteSoloObject> DeleteMuteCommand
        {
            get
            {
                return _deleteMuteCommand ?? (_deleteMuteCommand = new RelayCommand<MuteSoloObject>(
                    RemoveMute
                    ));
            }
        }

        public RelayCommand<MuteSoloObject> DeleteSoloCommand
        {
            get
            {
                if (_deleteSoloCommand == null)
                {
                    _deleteSoloCommand = new RelayCommand<MuteSoloObject>(
                        RemoveSolo
                        );
                }
                return _deleteSoloCommand;
            }
        }

        public RelayCommand<FileInfo> DoubleClickCommand
        {
            get
            {
                if (_doubleClickCommand == null)
                {
                    _doubleClickCommand = new RelayCommand<FileInfo>(
                        param => LoadData(param)
                        );
                }
                return _doubleClickCommand;
            }
        }

        public RelayCommand<string> FindObjectCommand
        {
            get
            {
                if (_findObjectCommand == null)
                {
                    _findObjectCommand = new RelayCommand<string>(
                        FindObjectAction
                        );
                }
                return _findObjectCommand;
            }
        }

        public ObservableHashSet<MuteSoloObject> MutedObjects
        {
            get { return MuteSoloManager.Instance.MutedObjects; }
        }

        public RelayCommand<FileInfo> RightClickCommand
        {
            get
            {
                if (_rightClickCommand == null)
                {
                    _rightClickCommand = new RelayCommand<FileInfo>(
                        AddData
                        );
                }
                return _rightClickCommand;
            }
        }

        public RelayCommand<string> SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand<string>
                        (
                        param => SaveData(param),
                        param => ((SoloedObjects.Count > 0 || MutedObjects.Count > 0))
                        );
                }
                return _saveCommand;
            }
        }

        public ObservableCollection<FileInfo> SavedMuteSoloPaths
        {
            get { return _savedMuteSoloPaths; }
        }

        public ObservableHashSet<MuteSoloObject> SoloedObjects
        {
            get { return MuteSoloManager.Instance.SoloedObjects; }
        }

        public SoloMode SoloMode
        {
            get { return MuteSoloManager.Instance.SelectedSoloMode; }
            set
            {
                MuteSoloManager.Instance.SelectedSoloMode = value;
                RaisePropertyChanged("SoloMode");
            }
        }

        public RelayCommand<string> UnMuteAllCommand
        {
            get
            {
                if (_unMuteAllCommand == null)
                {
                    _unMuteAllCommand = new RelayCommand<string>(
                        param => UnMuteAll()
                        );
                }
                return _unMuteAllCommand;
            }
        }

        public RelayCommand<string> UnSoloAllCommand
        {
            get
            {
                if (_unSoloAllCommand == null)
                {
                    _unSoloAllCommand = new RelayCommand<string>(
                        param => UnSoloAll()
                        );
                }
                return _unSoloAllCommand;
            }
        }

        public MuteSoloManagerViewModel()
        {
            CheckForSavedMuteSoloLists();
            SoloedObjects.CollectionChanged += (p, r) => SaveCommand.RaiseCanExecuteChanged();
            MutedObjects.CollectionChanged += (p, r) => SaveCommand.RaiseCanExecuteChanged();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event Action<IObjectInstance> OnObjectRefClick;

        private void RaisePropertyChanged(string property)
        {
            PropertyChanged.Raise(this, property);
        }

        private static string GetStorePath()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            stringBuilder.Append("\\Rave\\MuteSoloList");
            string path = stringBuilder.ToString();

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        private void AddData(FileInfo file)
        {
            using (StreamReader fileStream = new StreamReader(file.FullName))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SerializableMuteSoloSets));
                SerializableMuteSoloSets serIalizabeMuteSoloLists =
                    (SerializableMuteSoloSets)serializer.Deserialize(fileStream);
                MuteSoloManager.Instance.Solo(serIalizabeMuteSoloLists.soloedObjects);
                MuteSoloManager.Instance.Mute(serIalizabeMuteSoloLists.mutedObjects);
            }
        }

        private void CheckForSavedMuteSoloLists()
        {
            _savedMuteSoloPaths.Clear();
            DirectoryInfo dir = new DirectoryInfo(GetStorePath());
            foreach (FileInfo file in dir.GetFiles("*.xml"))
            {
                _savedMuteSoloPaths.Add(file);
            }
        }

        private void DeleteData(FileInfo file)
        {
            file.Delete();
            CheckForSavedMuteSoloLists();
        }

        private void FindObjectAction(string soundName)
        {
            if (soundName != null && OnObjectRefClick != null)
            {
                OnObjectRefClick(ObjectInstance.GetObjectMatchingName(soundName));
            }
        }

        private void LoadData(FileInfo file)
        {
            using (StreamReader fileStream = new StreamReader(file.FullName))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SerializableMuteSoloSets));
                    SerializableMuteSoloSets serIalizabeMuteSoloLists =
                 (SerializableMuteSoloSets)serializer.Deserialize(fileStream);

                    MuteSoloManager.Instance.RemoveSoloAll();
                    MuteSoloManager.Instance.Solo(serIalizabeMuteSoloLists.soloedObjects);
                    MuteSoloManager.Instance.RemoveMuteAll();
                    MuteSoloManager.Instance.Mute(serIalizabeMuteSoloLists.mutedObjects);
                }
                catch (Exception e)
                {
                  
                }
            }
        }

        private void RemoveMute(MuteSoloObject param)
        {
            MuteSoloManager.Instance.RemoveMute(param);
        }

        private void RemoveSolo(MuteSoloObject param)
        {
            MuteSoloManager.Instance.RemoveSolo(param);
        }

        private void SaveData(string fileName)
        {
            SerializableMuteSoloSets serIalizabeMuteSoloLists = new SerializableMuteSoloSets();
            serIalizabeMuteSoloLists.soloedObjects = SoloedObjects;
            serIalizabeMuteSoloLists.mutedObjects = MutedObjects;

            using (StreamWriter fileStream = new StreamWriter(GetStorePath() + "\\" + fileName + ".xml"))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SerializableMuteSoloSets));
                serializer.Serialize(fileStream, serIalizabeMuteSoloLists);
            }
            CheckForSavedMuteSoloLists();
        }

        private void UnMuteAll()
        {
            MuteSoloManager.Instance.RemoveMuteAll();
        }

        private void UnSoloAll()
        {
            MuteSoloManager.Instance.RemoveSoloAll();
        }
    }
}