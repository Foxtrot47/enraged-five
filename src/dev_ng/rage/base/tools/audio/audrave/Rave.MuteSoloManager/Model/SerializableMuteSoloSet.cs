﻿using rage.ToolLib;
using System;
using System.Collections.ObjectModel;

namespace Rave.MuteSoloManager.Model
{
    [Serializable]
    public class SerializableMuteSoloSets
    {
        public ObservableHashSet<MuteSoloObject> soloedObjects;
        public ObservableHashSet<MuteSoloObject> mutedObjects;
    }
}
