﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Input;
using rage.ToolLib;
using Rave.Instance;
using Rave.MuteSoloManager.Model;
using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.Types.Objects;

#endregion

namespace Rave.MuteSoloManager
{
    public enum SoloMode
    {
        XOR = 0,
        Latch = 1
    }

    public class MuteSoloManager
    {
        private static MuteSoloManager instance;
        private bool _sendAutoRemote = true;

        public static MuteSoloManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MuteSoloManager();
                }
                return instance;
            }
            set { instance = value; }
        }

        public ObservableHashSet<MuteSoloObject> MutedObjects { get; private set; }
        public SoloMode SelectedSoloMode { get; set; }
        public ObservableHashSet<MuteSoloObject> SoloedObjects { get; private set; }

        private MuteSoloManager()
        {
            SoloedObjects = new ObservableHashSet<MuteSoloObject>();
            MutedObjects = new ObservableHashSet<MuteSoloObject>();
            SoloedObjects.CollectionChanged += SoloedObjects_CollectionChanged;
            MutedObjects.CollectionChanged += MutedObjects_CollectionChanged;
        }

        public event Action<NotifyCollectionChangedEventArgs> MutedObjectsChanged;
        public event Action<NotifyCollectionChangedEventArgs> SoloedObjectsChanged;

        public void Mute(MuteSoloObject muteSoloObject)
        {
            int count = 0;
            foreach (IObjectInstance soundObject in ObjectInstance.GetObjectsMatchingName(muteSoloObject.Name))
            {
                if (soundObject.TypeName == muteSoloObject.TypeName)
                {
                    if (soundObject.TypeName == muteSoloObject.TypeName)
                    {
                        if (soundObject.Mute(true))
                        {
                            count++;
                        }
                    }
                }
            }
            if (count > 0)
            {
                MutedObjects.Add(muteSoloObject);
            }
        }

        /// <summary>
        ///     Mutes a list of sound objects
        /// </summary>
        /// <param name="listOfSoundObjects">The list of sound objects.</param>
        public void Mute(IEnumerable<MuteSoloObject> listOfSoundObjects)
        {
            foreach (MuteSoloObject muteSoloObject in listOfSoundObjects)
            {
                Mute(muteSoloObject);
            }
        }

        /// <summary>
        ///     Mutes a list of sound objects
        /// </summary>
        /// <param name="listOfSoundObjects">The list of sound objects.</param>
        public void MuteSelection(IEnumerable<IObjectInstance> listOfSoundObjects)
        {
            _sendAutoRemote = false;
            MuteSoloObject[] previousMutedObjects = new MuteSoloObject[MutedObjects.Count];
            MutedObjects.CopyTo(previousMutedObjects, 0);

            bool allMuted = true;
            foreach (IObjectInstance soundObject in listOfSoundObjects)
            {
                if (!soundObject.IsMuted)
                {
                    allMuted = false;
                }
            }
            if (allMuted)
            {
                foreach (IObjectInstance soundObject in listOfSoundObjects)
                {
                    RemoveMute(new MuteSoloObject(soundObject.Name, soundObject.TypeName));
                }
            }
            else
            {
                foreach (IObjectInstance instancedObject in listOfSoundObjects)
                {
                    Mute(new MuteSoloObject(instancedObject.Name, instancedObject.TypeName));
                }
            }

            SendRemoteMuteList();
            MuteSoloObject[] currentMutedObjects = new MuteSoloObject[MutedObjects.Count];
            MutedObjects.CopyTo(currentMutedObjects, 0);
            MutedObjectsChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace,
                new ArrayList(currentMutedObjects), new ArrayList(previousMutedObjects)));

            _sendAutoRemote = true;
        }

        /// <summary>
        ///     Toggles the mute of the sound object
        /// </summary>
        /// <param name="soundObject">The sound object.</param>
        public void MuteToggle(IObjectInstance soundObject)
        {
            if (!soundObject.IsMuted)
            {
                Mute(new MuteSoloObject(soundObject.Name, soundObject.TypeName));
            }
            else
            {
                RemoveMute(new MuteSoloObject(soundObject.Name, soundObject.TypeName));
            }
        }

        public void RemoveMute(MuteSoloObject muteSoloObject)
        {
            int count = 0;
            foreach (IObjectInstance soundObject in ObjectInstance.GetObjectsMatchingName(muteSoloObject.Name))
            {
                if (soundObject.TypeName == muteSoloObject.TypeName)
                {
                    if (soundObject.Mute(false))
                    {
                        count++;
                    }
                }
            }
            if (count > 0)
            {
                MutedObjects.Remove(muteSoloObject);
            }
        }

        public void RemoveMuteAll()
        {
            List<MuteSoloObject> soundList = new List<MuteSoloObject>();
            foreach (MuteSoloObject muteSoloObject in MutedObjects)
            {
                soundList.Add(muteSoloObject);
            }

            foreach (MuteSoloObject muteSoloObject in soundList)
            {
                RemoveMute(muteSoloObject);
            }
        }

        public void RemoveSolo(MuteSoloObject soloedObject)
        {
            int count = 0;
            foreach (IObjectInstance soundObject in ObjectInstance.GetObjectsMatchingName(soloedObject.Name))
            {
                if (soloedObject.TypeName == soundObject.TypeName)
                {
                    if (soundObject.Solo(false))
                    {
                        count++;
                    }
                }
            }
            if (count > 0)
            {
                SoloedObjects.Remove(soloedObject);
            }
        }

        public void RemoveSoloAll()
        {
            List<MuteSoloObject> soundList = new List<MuteSoloObject>();
            foreach (MuteSoloObject muteSoloObject in SoloedObjects)
            {
                soundList.Add(muteSoloObject);
            }

            foreach (MuteSoloObject muteSoloObject in soundList)
            {
                RemoveSolo(muteSoloObject);
            }
        }

        public void Solo(MuteSoloObject muteSoloObject)
        {
            switch (SelectedSoloMode)
            {
                case SoloMode.XOR:
                    if (!(Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)))
                    {
                        SoloXOR(muteSoloObject);
                    }
                    else
                    {
                        SoloLatch(muteSoloObject);
                    }
                    break;

                case SoloMode.Latch:
                    if (!(Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)))
                    {
                        SoloLatch(muteSoloObject);
                    }
                    else
                    {
                        SoloXOR(muteSoloObject);
                    }
                    break;

                default:
                    SoloXOR(muteSoloObject);
                    break;
            }
        }

        public void Solo(IEnumerable<MuteSoloObject> listOfSoundObjects)
        {
            foreach (MuteSoloObject soundObject in listOfSoundObjects)
            {
                SoloLatch(soundObject);
            }
        }

        public void SoloLatch(MuteSoloObject muteSoloObject)
        {
            int count = 0;
            foreach (IObjectInstance soundObject in ObjectInstance.GetObjectsMatchingName(muteSoloObject.Name))
            {
                if (soundObject.TypeName == muteSoloObject.TypeName)
                {
                    if (soundObject.TypeName == muteSoloObject.TypeName)
                    {
                        if (soundObject.Solo(true))
                        {
                            count++;
                        }
                    }
                }
            }
            if (count > 0)
            {
                SoloedObjects.Add(muteSoloObject);
            }
        }

        public void SoloSelection(IEnumerable<IObjectInstance> listOfSoundObjects)
        {
            _sendAutoRemote = false;

            bool allSoloed = true;
            MuteSoloObject[] previousSoloedObjects = new MuteSoloObject[SoloedObjects.Count];
            SoloedObjects.CopyTo(previousSoloedObjects, 0);

            foreach (IObjectInstance soundObject in listOfSoundObjects)
            {
                if (!soundObject.IsSoloed)
                {
                    allSoloed = false;
                }
            }
            if (allSoloed)
            {
                foreach (IObjectInstance soundObject in listOfSoundObjects)
                {
                    RemoveSolo(new MuteSoloObject(soundObject.Name, soundObject.TypeName));
                }
            }
            else
            {
                if (SelectedSoloMode == SoloMode.XOR)
                {
                    RemoveSoloAll();
                }
                foreach (IObjectInstance soundObject in listOfSoundObjects)
                {
                    SoloLatch(new MuteSoloObject(soundObject.Name, soundObject.TypeName));
                }
            }
            MuteSoloObject[] currentSoloedObjects = new MuteSoloObject[SoloedObjects.Count];
            SoloedObjects.CopyTo(currentSoloedObjects, 0);
            SoloedObjectsChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace,
                new ArrayList(currentSoloedObjects), new ArrayList(previousSoloedObjects)));
            SendRemoteSoloList();
            _sendAutoRemote = true;
        }

        /// <summary>
        ///     Toggles the solo of the sound object
        /// </summary>
        /// <param name="soundObject">The sound object.</param>
        public void SoloToggle(IObjectInstance soundObject)
        {
            if (!soundObject.IsSoloed)
            {
                Solo(new MuteSoloObject(soundObject.Name, soundObject.TypeName));
            }
            else
            {
                RemoveSolo(new MuteSoloObject(soundObject.Name, soundObject.TypeName));
            }
        }

        public void SoloXOR(MuteSoloObject muteSoloObject)
        {
            int count = 0;

            foreach (IObjectInstance soundObject in ObjectInstance.GetObjectsMatchingName(muteSoloObject.Name))
            {
                if (soundObject.Solo(true))
                {
                    count++;
                }
            }
            if (count > 0)
            {
                _sendAutoRemote = false;
                MuteSoloObject[] previousSoloedObjects = new MuteSoloObject[SoloedObjects.Count];
                SoloedObjects.CopyTo(previousSoloedObjects, 0);

                RemoveSoloAll();

                foreach (IObjectInstance soundObject in ObjectInstance.GetObjectsMatchingName(muteSoloObject.Name))
                {
                    soundObject.Solo(true);
                }


                SoloedObjects.Add(muteSoloObject);
                SendRemoteSoloList();
                MuteSoloObject[] currentSoloedObjects = new MuteSoloObject[SoloedObjects.Count];
                SoloedObjects.CopyTo(currentSoloedObjects, 0);
                SoloedObjectsChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace,
                    new ArrayList(currentSoloedObjects), new ArrayList(previousSoloedObjects)));
                _sendAutoRemote = true;
            }
        }

        private void MutedObjects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (_sendAutoRemote)
            {
                MutedObjectsChanged(e);
                SendRemoteMuteList();
            }
        }

        private void SoloedObjects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (_sendAutoRemote)
            {
                SoloedObjectsChanged(e);
                SendRemoteSoloList();
            }
        }

        private void SendRemoteSoloList()
        {
            List<KeyValuePair<string, string>> stringListToRemote = new List<KeyValuePair<string, string>>();
            foreach (MuteSoloObject muteSolo in SoloedObjects)
            {
                stringListToRemote.Add(new KeyValuePair<string, string>(muteSolo.TypeName, muteSolo.Name));
            }
            RaveInstance.RemoteControl.SetSoloList(stringListToRemote);
        }

        private void SendRemoteMuteList()
        {
            List<KeyValuePair<string, string>> stringListToRemote = new List<KeyValuePair<string, string>>();
            foreach (MuteSoloObject muteSolo in MutedObjects)
            {
                stringListToRemote.Add(new KeyValuePair<string, string>(muteSolo.TypeName, muteSolo.Name));
            }
            RaveInstance.RemoteControl.SetMuteList(stringListToRemote);
        }

        public void Rename(string objectName, string typeName, string newName)
        {
            MuteSoloObject mutedObject =
                MutedObjects.FirstOrDefault(
                    p =>
                        p.Name.Equals(objectName, StringComparison.InvariantCultureIgnoreCase) &&
                        p.TypeName.Equals(typeName, StringComparison.InvariantCultureIgnoreCase));
            MuteSoloObject soloedObject =
                SoloedObjects.FirstOrDefault(
                    p =>
                        p.Name.Equals(objectName, StringComparison.InvariantCultureIgnoreCase) &&
                        p.TypeName.Equals(typeName, StringComparison.InvariantCultureIgnoreCase));
            if (mutedObject != null)
            {
                MutedObjects.Remove(mutedObject);
                Mute(new MuteSoloObject(newName, typeName));
            }

            if (soloedObject != null)
            {
                SoloedObjects.Remove(soloedObject);
                Solo(new MuteSoloObject(newName, typeName));
            }
        }

        public void OnRevert(IObjectBank muteSoloObject)
        {
            foreach (IObjectInstance objectInstance in muteSoloObject.ObjectInstances)
            {
                MuteSoloObject mutedObject =
                    MutedObjects.FirstOrDefault(
                        p => p.Name == objectInstance.Name && p.TypeName == objectInstance.TypeName);

                if (mutedObject != null)
                {
                    RemoveMute(mutedObject);
                }

                MuteSoloObject solodObject =
                    SoloedObjects.FirstOrDefault(
                        p => p.Name == objectInstance.Name && p.TypeName == objectInstance.TypeName);

                if (solodObject != null)
                {
                    RemoveSolo(solodObject);
                }
            }

        }
    }
}