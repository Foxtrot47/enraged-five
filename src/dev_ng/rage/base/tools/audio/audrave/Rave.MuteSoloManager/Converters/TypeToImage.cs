﻿using rage;
using Rave.Instance;
using Rave.MuteSoloManager.Model;
using Rave.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Data;


namespace Rave.MuteSoloManager.Converters
{
    class TypeToImage : IValueConverter
    {
        public Dictionary<string, string> TypeToPath = new Dictionary<string, string>();

        public TypeToImage()
        {
            LoadIcons();
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (TypeToPath.ContainsKey(((MuteSoloObject)value).TypeName))
            {
                return TypeToPath[((MuteSoloObject)value).TypeName];
            }
            return TypeToPath["default"];
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion


        private void LoadIcons()
        {
            foreach (audMetadataType metadataType in Configuration.MetadataTypes)
            {
                // Load meta data type icons
                RaveUtils.LoadTypeIcons(
                     Path.Combine(AppDomain.CurrentDomain.BaseDirectory, metadataType.IconPath),
                     RaveInstance.AllTypeDefinitions[metadataType.Type], TypeToPath);
            }
        }

    }
}
