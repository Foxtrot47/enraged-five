﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace Rave.MuteSoloManager.View
{
	/// <summary>
	/// Interaction logic for MuteSoloManagerView.xaml
	/// </summary>
	public partial class MuteSoloManagerView : UserControl
	{
		public Rave.MuteSoloManager.ViewModel.MuteSoloManagerViewModel viewModel;
		private ObservableCollection<UIElement> source = new ObservableCollection<UIElement>();
		public MuteSoloManagerView()
		{

			InitializeComponent();
			viewModel = new Rave.MuteSoloManager.ViewModel.MuteSoloManagerViewModel();
			DataContext = viewModel;
			Content.PropertyChanged += OnSizeChanged;
			this.SizeChanged += OnSizeChanged;
		}

		private void OnSizeChanged(object sender, EventArgs e)
		{
			double vertical = fluidWrapPanel.ItemHeight;
			double horizontal = fluidWrapPanel.ItemWidth;
			if (fluidWrapPanel.Orientation == Orientation.Horizontal)
			{
				horizontal = fluidWrapPanel.Children.Count * horizontal;
			}
			else
			{
				vertical = fluidWrapPanel.Children.Count * vertical;
			}

			if (horizontal > this.ActualWidth)
			{
				sizedBorder.Width = horizontal;
				scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;

			}
			else
			{
				scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
				sizedBorder.Width = scrollViewer.Width;
			}

			if (vertical > this.ActualHeight)
			{
				sizedBorder.Height = vertical;
				scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
			}
			else
			{
				scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
				sizedBorder.Height = scrollViewer.Height;
			}
			fluidWrapPanel.InvalidateVisual();
		}

		Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_DockableContent;

		public new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable Content
		{
			get
			{
				return this.m_DockableContent
					   ?? (this.m_DockableContent =
						   new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
						   {
							   Content = this,
							   FloatingWidth = 300,
							   FloatingHeight = 600,
							   ContentId = "MuteSoloManager",
							   Title = "Mute and Solo Manager",
							   AutoHideWidth = 250,
						   });
			}
		}



	}
}
