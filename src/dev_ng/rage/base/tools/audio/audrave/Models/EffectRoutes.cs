namespace Models
{
    public enum EffectRoutes {
        [ModelLib.CustomAttributes.Display("Unspecified")]
        EFFECT_ROUTE_AS_PARENT,
        [ModelLib.CustomAttributes.Display("Music")]
        EFFECT_ROUTE_MUSIC,
        [ModelLib.CustomAttributes.Display("Wind")]
        EFFECT_ROUTE_WIND,
        [ModelLib.CustomAttributes.Display("Front End")]
        EFFECT_ROUTE_FRONT_END,
        [ModelLib.CustomAttributes.Display("Positioned")]
        EFFECT_ROUTE_POSITIONED,
        [ModelLib.CustomAttributes.Display("FullWet_SmallReverb")]
        EFFECT_ROUTE_SML_REVERB_FULL_WET,
        [ModelLib.CustomAttributes.Display("FullWet_MediumReverb")]
        EFFECT_ROUTE_MED_REVERB_FULL_WET,
        [ModelLib.CustomAttributes.Display("FullWet_LargeReverb")]
        EFFECT_ROUTE_LRG_REVERB_FULL_WET,
        [ModelLib.CustomAttributes.Display("Direct-SFX")]
        EFFECT_ROUTE_SFX,
        [ModelLib.CustomAttributes.Display("Direct-Master")]
        EFFECT_ROUTE_MASTER,
        [ModelLib.CustomAttributes.Display("Pad Speaker 1")]
        EFFECT_ROUTE_PADSPEAKER_1,
        [ModelLib.CustomAttributes.Display("Positioned Music")]
        EFFECT_ROUTE_POSITIONED_MUSIC,
        [ModelLib.CustomAttributes.Display("Ambient Speech")]
        EFFECT_ROUTE_AMBIENT_SPEECH,
        [ModelLib.CustomAttributes.Display("Scripted Speech")]
        EFFECT_ROUTE_SCRIPTED_SPEECH,
        [ModelLib.CustomAttributes.Display("Front End Speech")]
        EFFECT_ROUTE_FRONT_END_SPEECH
    }
}