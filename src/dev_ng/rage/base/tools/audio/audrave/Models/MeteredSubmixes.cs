namespace Models
{
    public enum MeteredSubmixes {
        [ModelLib.CustomAttributes.Display("Unspecified")]
        METERED_SUBMIX_AS_PARENT,
        [ModelLib.CustomAttributes.Display("Scripted Speech")]
        METERED_SUBMIX_SCRIPTED_SPEECH,
        [ModelLib.CustomAttributes.Display("Player Vehicle")]
        METERED_SUBMIX_PLAYER_VEHICLE,
        [ModelLib.CustomAttributes.Display("Radio Front End")]
        METERED_SUBMIX_FOCUS_ENTITY,
        [ModelLib.CustomAttributes.Display("Vehicle OneOff")]
        METERED_SUBMIX_VEHICLE_ONEOFF,
        [ModelLib.CustomAttributes.Display("Player Weapons")]
        METERED_SUBMIX_PLAYER_WEAPONS,
        [ModelLib.CustomAttributes.Display("Explosions")]
        METERED_SUBMIX_EXPLOSIONS,
        [ModelLib.CustomAttributes.Display("Impacts")]
        METERED_SUBMIX_IMPACTS,
        [ModelLib.CustomAttributes.Display("Player Horse")]
        METERED_SUBMIX_PLAYER_HORSE,
        [ModelLib.CustomAttributes.Display("Death Collision")]
        METERED_SUBMIX_DEATH_COLLISION
    }
}