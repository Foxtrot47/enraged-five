namespace Models
{
    public enum EnvelopeSoundMode
    {
        [ModelLib.CustomAttributes.Display("Volume")]
        kEnvelopeSoundVolume,
        [ModelLib.CustomAttributes.Display("Pitch")]
        kEnvelopeSoundPitch,
        [ModelLib.CustomAttributes.Display("Pan")]
        kEnvelopeSoundPan,
        [ModelLib.CustomAttributes.Display("LPF Cutoff")]
        kEnvelopeSoundLPF,
        [ModelLib.CustomAttributes.Display("HPF Cutoff")]
        kEnvelopeSoundHPF,
        [ModelLib.CustomAttributes.Display("Variable")]
        kEnvelopeSoundVariable
    }
}

