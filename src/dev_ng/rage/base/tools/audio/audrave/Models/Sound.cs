using ModelLib;

namespace Models
{
    [ModelLib.CustomAttributes.AdditionalCompression]
    [ModelLib.CustomAttributes.Transformer("{Type=\"rage.SoundTransformer.SoundTransformer, SoundTransformer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\"}")]
    [ModelLib.CustomAttributes.Compressed]
    public abstract class Sound:  ModelBase  {

        private System.String _Description;
        [ModelLib.CustomAttributes.Ignore]
        public System.String Description {
            get { return _Description; }
            set { SetField(ref _Description, value, () => Description); }
        }

        private System.Int16 _Volume;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.Description("Volume at which this sound is played")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(10000)]
        [ModelLib.CustomAttributes.Min(-10000)]
        [ModelLib.CustomAttributes.Unit("mB")]
        public System.Int16 Volume {
            get { return _Volume; }
            set { SetField(ref _Volume, value, () => Volume); }
        }

        private System.UInt16 _VolumeVariance;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.Description("The sound will be played at Volume +/- VolumeVariance")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(10000)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("mB")]
        public System.UInt16 VolumeVariance {
            get { return _VolumeVariance; }
            set { SetField(ref _VolumeVariance, value, () => VolumeVariance); }
        }

        private System.Int16 _Pitch;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(2400)]
        [ModelLib.CustomAttributes.Min(-2400)]
        [ModelLib.CustomAttributes.Unit("cents")]
        public System.Int16 Pitch {
            get { return _Pitch; }
            set { SetField(ref _Pitch, value, () => Pitch); }
        }

        private System.UInt16 _PitchVariance;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(2400)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("cents")]
        public System.UInt16 PitchVariance {
            get { return _PitchVariance; }
            set { SetField(ref _PitchVariance, value, () => PitchVariance); }
        }

        private System.Int16 _Pan;
        [ModelLib.CustomAttributes.Default("-1")]
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(360)]
        [ModelLib.CustomAttributes.Min(-1)]
        [ModelLib.CustomAttributes.Unit("degrees")]
        public System.Int16 Pan {
            get { return _Pan; }
            set { SetField(ref _Pan, value, () => Pan); }
        }

        private System.UInt16 _PanVariance;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(360)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("degrees")]
        public System.UInt16 PanVariance {
            get { return _PanVariance; }
            set { SetField(ref _PanVariance, value, () => PanVariance); }
        }

        private System.UInt16 _preDelay;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(65535)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("ms")]
        public System.UInt16 preDelay {
            get { return _preDelay; }
            set { SetField(ref _preDelay, value, () => preDelay); }
        }

        private System.UInt16 _preDelayVariance;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(65535)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("ms")]
        public System.UInt16 preDelayVariance {
            get { return _preDelayVariance; }
            set { SetField(ref _preDelayVariance, value, () => preDelayVariance); }
        }

        private System.UInt32 _StartOffset;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(300000)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("ms")]
        public System.UInt32 StartOffset {
            get { return _StartOffset; }
            set { SetField(ref _StartOffset, value, () => StartOffset); }
        }

        private System.UInt32 _StartOffsetVariance;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(300000)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("ms")]
        public System.UInt32 StartOffsetVariance {
            get { return _StartOffsetVariance; }
            set { SetField(ref _StartOffsetVariance, value, () => StartOffsetVariance); }
        }

        private System.UInt16 _AttackTime;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(65535)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("ms")]
        public System.UInt16 AttackTime {
            get { return _AttackTime; }
            set { SetField(ref _AttackTime, value, () => AttackTime); }
        }

        private System.Int16 _HoldTime;
        [ModelLib.CustomAttributes.Default("-1")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(32767)]
        [ModelLib.CustomAttributes.Min(-1)]
        [ModelLib.CustomAttributes.Unit("ms")]
        public System.Int16 HoldTime {
            get { return _HoldTime; }
            set { SetField(ref _HoldTime, value, () => HoldTime); }
        }

        private System.Int16 _ReleaseTime;
        [ModelLib.CustomAttributes.Default("-1")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(8191)]
        [ModelLib.CustomAttributes.Min(-1)]
        [ModelLib.CustomAttributes.Unit("ms")]
        public System.Int16 ReleaseTime {
            get { return _ReleaseTime; }
            set { SetField(ref _ReleaseTime, value, () => ReleaseTime); }
        }

        private System.UInt16 _DopplerFactor;
        [ModelLib.CustomAttributes.Default("100")]
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(1000)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("0.01units")]
        public System.UInt16 DopplerFactor {
            get { return _DopplerFactor; }
            set { SetField(ref _DopplerFactor, value, () => DopplerFactor); }
        }

        private System.String _Category;
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Unit("CategoryRef")]
        public System.String Category {
            get { return _Category; }
            set { SetField(ref _Category, value, () => Category); }
        }

        private System.UInt16 _LPFCutoff;
        [ModelLib.CustomAttributes.Default("24000")]
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(24000)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("Hz")]
        public System.UInt16 LPFCutoff {
            get { return _LPFCutoff; }
            set { SetField(ref _LPFCutoff, value, () => LPFCutoff); }
        }

        private System.UInt16 _LPFCutoffVariance;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(24000)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("Hz")]
        public System.UInt16 LPFCutoffVariance {
            get { return _LPFCutoffVariance; }
            set { SetField(ref _LPFCutoffVariance, value, () => LPFCutoffVariance); }
        }

        private System.UInt16 _HPFCutoff;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(24000)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("Hz")]
        public System.UInt16 HPFCutoff {
            get { return _HPFCutoff; }
            set { SetField(ref _HPFCutoff, value, () => HPFCutoff); }
        }

        private System.UInt16 _HPFCutoffVariance;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        [ModelLib.CustomAttributes.Max(24000)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("Hz")]
        public System.UInt16 HPFCutoffVariance {
            get { return _HPFCutoffVariance; }
            set { SetField(ref _HPFCutoffVariance, value, () => HPFCutoffVariance); }
        }

        private System.String _VolumeCurve;
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.DisplayGroup("Rolloff Curves")]
        [ModelLib.CustomAttributes.Unit("RolloffCurveRef")]
        public System.String VolumeCurve {
            get { return _VolumeCurve; }
            set { SetField(ref _VolumeCurve, value, () => VolumeCurve); }
        }

        private System.UInt16 _VolumeCurveScale;
        [ModelLib.CustomAttributes.Default("100")]
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.DisplayGroup("Rolloff Curves")]
        [ModelLib.CustomAttributes.Unit("0.01units")]
        public System.UInt16 VolumeCurveScale {
            get { return _VolumeCurveScale; }
            set { SetField(ref _VolumeCurveScale, value, () => VolumeCurveScale); }
        }

        private System.Byte _VolumeCurvePlateau;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.Description("Rolloff curve plateau extension in metres")]
        [ModelLib.CustomAttributes.DisplayGroup("Rolloff Curves")]
        [ModelLib.CustomAttributes.Unit("m")]
        public System.Byte VolumeCurvePlateau {
            get { return _VolumeCurvePlateau; }
            set { SetField(ref _VolumeCurvePlateau, value, () => VolumeCurvePlateau); }
        }

        private EffectRoutes _EffectRoute;
        [ModelLib.CustomAttributes.Default("EFFECT_ROUTE_AS_PARENT")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        public EffectRoutes EffectRoute {
            get { return _EffectRoute; }
            set { SetField(ref _EffectRoute, value, () => EffectRoute); }
        }

        private MeteredSubmixes _MeteredSubmix;
        [ModelLib.CustomAttributes.Default("METERED_SUBMIX_AS_PARENT")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        public MeteredSubmixes MeteredSubmix {
            get { return _MeteredSubmix; }
            set { SetField(ref _MeteredSubmix, value, () => MeteredSubmix); }
        }

        private ModelLib.Types.TriState _StartOffsetPercentage;
        [ModelLib.CustomAttributes.Description("if true start offset should be interpreted as percentage rather than ms")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        public ModelLib.Types.TriState StartOffsetPercentage {
            get { return _StartOffsetPercentage; }
            set { SetField(ref _StartOffsetPercentage, value, () => StartOffsetPercentage); }
        }

        private ModelLib.Types.TriState _DistanceAttenuation;
        [ModelLib.CustomAttributes.Description("If true the sounds volume will attenuate over distance")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        public ModelLib.Types.TriState DistanceAttenuation {
            get { return _DistanceAttenuation; }
            set { SetField(ref _DistanceAttenuation, value, () => DistanceAttenuation); }
        }

        private ModelLib.Types.TriState _EnvironmentalEffects;
        [ModelLib.CustomAttributes.Description("Should environmental effects (reverb, filtering etc) be applied to this sound?")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        public ModelLib.Types.TriState EnvironmentalEffects {
            get { return _EnvironmentalEffects; }
            set { SetField(ref _EnvironmentalEffects, value, () => EnvironmentalEffects); }
        }

        private ModelLib.Types.TriState _VirtualiseAsGroup;
        [ModelLib.CustomAttributes.Description("if true this sound and all its children should be grouped for virtualisation purposes")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        public ModelLib.Types.TriState VirtualiseAsGroup {
            get { return _VirtualiseAsGroup; }
            set { SetField(ref _VirtualiseAsGroup, value, () => VirtualiseAsGroup); }
        }

        private ModelLib.Types.TriState _DynamicPrepare;
        [ModelLib.CustomAttributes.Description("if true this sound will Prepare() dynamically")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        public ModelLib.Types.TriState DynamicPrepare {
            get { return _DynamicPrepare; }
            set { SetField(ref _DynamicPrepare, value, () => DynamicPrepare); }
        }

        private ModelLib.Types.TriState _MuteOnUserMusic;
        [ModelLib.CustomAttributes.Description("If true then this sound will mute when the user is playing their music (Xbox360 only)")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        public ModelLib.Types.TriState MuteOnUserMusic {
            get { return _MuteOnUserMusic; }
            set { SetField(ref _MuteOnUserMusic, value, () => MuteOnUserMusic); }
        }

        private ModelLib.Types.TriState _InvertPhase;
        [ModelLib.CustomAttributes.Description("If true then this sound will play back with a -1 gain")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        public ModelLib.Types.TriState InvertPhase {
            get { return _InvertPhase; }
            set { SetField(ref _InvertPhase, value, () => InvertPhase); }
        }

        private ModelLib.Types.TriState _Mute;
        [ModelLib.CustomAttributes.Description("Overrides volume with -100dB when set")]
        [ModelLib.CustomAttributes.DisplayGroup("Base Settings")]
        public ModelLib.Types.TriState Mute {
            get { return _Mute; }
            set { SetField(ref _Mute, value, () => Mute); }
        }

        private System.String _VolumeVariable;
        [ModelLib.CustomAttributes.DisplayGroup("Variables")]
        [ModelLib.CustomAttributes.Ignore]
        [ModelLib.CustomAttributes.Unit("variable")]
        public System.String VolumeVariable {
            get { return _VolumeVariable; }
            set { SetField(ref _VolumeVariable, value, () => VolumeVariable); }
        }

        private System.String _PitchVariable;
        [ModelLib.CustomAttributes.DisplayGroup("Variables")]
        [ModelLib.CustomAttributes.Ignore]
        [ModelLib.CustomAttributes.Unit("variable")]
        public System.String PitchVariable {
            get { return _PitchVariable; }
            set { SetField(ref _PitchVariable, value, () => PitchVariable); }
        }

        private System.String _PanVariable;
        [ModelLib.CustomAttributes.DisplayGroup("Variables")]
        [ModelLib.CustomAttributes.Ignore]
        [ModelLib.CustomAttributes.Unit("variable")]
        public System.String PanVariable {
            get { return _PanVariable; }
            set { SetField(ref _PanVariable, value, () => PanVariable); }
        }

        private System.String _PreDelayVariable;
        [ModelLib.CustomAttributes.DisplayGroup("Variables")]
        [ModelLib.CustomAttributes.Unit("variable")]
        public System.String PreDelayVariable {
            get { return _PreDelayVariable; }
            set { SetField(ref _PreDelayVariable, value, () => PreDelayVariable); }
        }

        private System.String _StartOffsetVariable;
        [ModelLib.CustomAttributes.DisplayGroup("Variables")]
        [ModelLib.CustomAttributes.Unit("variable")]
        public System.String StartOffsetVariable {
            get { return _StartOffsetVariable; }
            set { SetField(ref _StartOffsetVariable, value, () => StartOffsetVariable); }
        }

        private System.String _FilterCutoffVariable;
        [ModelLib.CustomAttributes.DisplayGroup("Variables")]
        [ModelLib.CustomAttributes.Ignore]
        [ModelLib.CustomAttributes.Unit("variable")]
        public System.String FilterCutoffVariable {
            get { return _FilterCutoffVariable; }
            set { SetField(ref _FilterCutoffVariable, value, () => FilterCutoffVariable); }
        }

        private System.String _HPFCutoffVariable;
        [ModelLib.CustomAttributes.DisplayGroup("Variables")]
        [ModelLib.CustomAttributes.Ignore]
        [ModelLib.CustomAttributes.Unit("variable")]
        public System.String HPFCutoffVariable {
            get { return _HPFCutoffVariable; }
            set { SetField(ref _HPFCutoffVariable, value, () => HPFCutoffVariable); }
        }

        private System.String _VolumeCurveScaleVariable;
        [ModelLib.CustomAttributes.DisplayGroup("Variables")]
        [ModelLib.CustomAttributes.Ignore]
        [ModelLib.CustomAttributes.Unit("variable")]
        public System.String VolumeCurveScaleVariable {
            get { return _VolumeCurveScaleVariable; }
            set { SetField(ref _VolumeCurveScaleVariable, value, () => VolumeCurveScaleVariable); }
        }

        private System.UInt16 _SmallReverbSend;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.Description("Minimum reverb send level")]
        [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
        [ModelLib.CustomAttributes.Max(100)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("0.01units")]
        public System.UInt16 SmallReverbSend {
            get { return _SmallReverbSend; }
            set { SetField(ref _SmallReverbSend, value, () => SmallReverbSend); }
        }

        private System.UInt16 _MediumReverbSend;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.Description("Minimum reverb send level")]
        [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
        [ModelLib.CustomAttributes.Max(100)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("0.01units")]
        public System.UInt16 MediumReverbSend {
            get { return _MediumReverbSend; }
            set { SetField(ref _MediumReverbSend, value, () => MediumReverbSend); }
        }

        private System.UInt16 _LargeReverbSend;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.Description("Minimum reverb send level")]
        [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
        [ModelLib.CustomAttributes.Max(100)]
        [ModelLib.CustomAttributes.Min(0)]
        [ModelLib.CustomAttributes.Unit("0.01units")]
        public System.UInt16 LargeReverbSend {
            get { return _LargeReverbSend; }
            set { SetField(ref _LargeReverbSend, value, () => LargeReverbSend); }
        }

        public class SpeakerMaskDefinition: ModelBase {
            private ModelLib.Types.Bit _FrontLeft;
            [ModelLib.CustomAttributes.Default("no")]
            [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
            public ModelLib.Types.Bit FrontLeft {
                get { return _FrontLeft; }
                set { SetField(ref _FrontLeft, value, () => FrontLeft); }
            }

            private ModelLib.Types.Bit _FrontRight;
            [ModelLib.CustomAttributes.Default("no")]
            [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
            public ModelLib.Types.Bit FrontRight {
                get { return _FrontRight; }
                set { SetField(ref _FrontRight, value, () => FrontRight); }
            }

            private ModelLib.Types.Bit _FrontCenter;
            [ModelLib.CustomAttributes.Default("no")]
            [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
            public ModelLib.Types.Bit FrontCenter {
                get { return _FrontCenter; }
                set { SetField(ref _FrontCenter, value, () => FrontCenter); }
            }

            private ModelLib.Types.Bit _LFE;
            [ModelLib.CustomAttributes.Default("no")]
            [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
            public ModelLib.Types.Bit LFE {
                get { return _LFE; }
                set { SetField(ref _LFE, value, () => LFE); }
            }

            private ModelLib.Types.Bit _RearLeft;
            [ModelLib.CustomAttributes.Default("no")]
            [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
            public ModelLib.Types.Bit RearLeft {
                get { return _RearLeft; }
                set { SetField(ref _RearLeft, value, () => RearLeft); }
            }

            private ModelLib.Types.Bit _RearRight;
            [ModelLib.CustomAttributes.Default("no")]
            [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
            public ModelLib.Types.Bit RearRight {
                get { return _RearRight; }
                set { SetField(ref _RearRight, value, () => RearRight); }
            }

            private ModelLib.Types.Bit _FrontLeftOfCenter;
            [ModelLib.CustomAttributes.Default("no")]
            [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
            public ModelLib.Types.Bit FrontLeftOfCenter {
                get { return _FrontLeftOfCenter; }
                set { SetField(ref _FrontLeftOfCenter, value, () => FrontLeftOfCenter); }
            }

            private ModelLib.Types.Bit _FrontRightOfCenter;
            [ModelLib.CustomAttributes.Default("no")]
            [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
            public ModelLib.Types.Bit FrontRightOfCenter {
                get { return _FrontRightOfCenter; }
                set { SetField(ref _FrontRightOfCenter, value, () => FrontRightOfCenter); }
            }

        }
        private SpeakerMaskDefinition _SpeakerMask;
        [ModelLib.CustomAttributes.AllowOverrideControl]
        [ModelLib.CustomAttributes.DisplayGroup("Output Routing")]
        public SpeakerMaskDefinition SpeakerMask {
            get { return _SpeakerMask; }
            set { SetField(ref _SpeakerMask, value, () => SpeakerMask); }
        }

    }
}


