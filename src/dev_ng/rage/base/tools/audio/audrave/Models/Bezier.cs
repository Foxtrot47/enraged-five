using System.Xml.Serialization;
using ModelLib;

namespace Models
{
    public class Bezier : BaseCurve
    {

        public class TransformPointsDefinition : ModelBase
        {
            private System.Single _x;
            public System.Single x
            {
                get { return _x; }
                set { SetField(ref _x, value, () => x); }
            }

            private System.Single _y;
            public System.Single y
            {
                get { return _y; }
                set { SetField(ref _y, value, () => y); }
            }

        }
        private ItemsObservableCollection<TransformPointsDefinition> _TransformPoints = new ItemsObservableCollection<TransformPointsDefinition>();
        [ModelLib.CustomAttributes.MaxSize(16, typeof(System.UInt32))]
        [XmlElement("TransformPoints")]
        public ItemsObservableCollection<TransformPointsDefinition> TransformPoints
        {
            get { return _TransformPoints; }
            set { SetField(ref _TransformPoints, value, () => TransformPoints); }
        }

        public class BezierPointsDefinition : ModelBase
        {
            private System.Single _x;
            public System.Single x
            {
                get { return _x; }
                set { SetField(ref _x, value, () => x); }
            }

            private System.Single _y;
            public System.Single y
            {
                get { return _y; }
                set { SetField(ref _y, value, () => y); }
            }

        }
        private ItemsObservableCollection<BezierPointsDefinition> _BezierPoints = new ItemsObservableCollection<BezierPointsDefinition>();
        [ModelLib.CustomAttributes.MaxSize(16, typeof(System.UInt32))]
        [XmlElement("BezierPoints")]
        public ItemsObservableCollection<BezierPointsDefinition> BezierPoints
        {
            get { return _BezierPoints; }
            set { SetField(ref _BezierPoints, value, () => BezierPoints); }
        }

        public class OutputRangeDefinition : ModelBase
        {
            private System.Single _min;
            public System.Single min
            {
                get { return _min; }
                set { SetField(ref _min, value, () => min); }
            }

            private System.Single _max;
            public System.Single max
            {
                get { return _max; }
                set { SetField(ref _max, value, () => max); }
            }

        }
        private OutputRangeDefinition _OutputRange;
        public OutputRangeDefinition OutputRange
        {
            get { return _OutputRange; }
            set { SetField(ref _OutputRange, value, () => OutputRange); }
        }

    }
}

