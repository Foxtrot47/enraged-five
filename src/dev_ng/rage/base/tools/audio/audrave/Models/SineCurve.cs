namespace Models
{
    public  class SineCurve: BaseCurve {

        private System.Single _StartPhase;
        [ModelLib.CustomAttributes.Default("0.0")]
        [ModelLib.CustomAttributes.Display("Start Phase")]
        public System.Single StartPhase {
            get { return _StartPhase; }
            set { SetField(ref _StartPhase, value, () => StartPhase); }
        }

        private System.Single _EndPhase;
        [ModelLib.CustomAttributes.Default("-1.0")]
        [ModelLib.CustomAttributes.Display("End Phase")]
        public System.Single EndPhase {
            get { return _EndPhase; }
            set { SetField(ref _EndPhase, value, () => EndPhase); }
        }

        private System.Single _Frequency;
        [ModelLib.CustomAttributes.Default("-1.0")]
        [ModelLib.CustomAttributes.Display("Frequency")]
        public System.Single Frequency {
            get { return _Frequency; }
            set { SetField(ref _Frequency, value, () => Frequency); }
        }

        private System.Single _VerticalScaling;
        [ModelLib.CustomAttributes.Default("1.0")]
        [ModelLib.CustomAttributes.Display("Vertical Scaling")]
        public System.Single VerticalScaling {
            get { return _VerticalScaling; }
            set { SetField(ref _VerticalScaling, value, () => VerticalScaling); }
        }

        private System.Single _VerticalOffset;
        [ModelLib.CustomAttributes.Default("0.0")]
        [ModelLib.CustomAttributes.Display("Vertical Offset")]
        public System.Single VerticalOffset {
            get { return _VerticalOffset; }
            set { SetField(ref _VerticalOffset, value, () => VerticalOffset); }
        }

    }
}


