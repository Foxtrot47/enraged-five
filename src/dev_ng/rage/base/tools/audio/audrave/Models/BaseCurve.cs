using ModelLib;

namespace Models
{
    public abstract class BaseCurve : ModelBase
    {

        private System.String _Notes;
        [ModelLib.CustomAttributes.Ignore]
        public System.String Notes
        {
            get { return _Notes; }
            set { SetField(ref _Notes, value, () => Notes); }
        }

        private System.Single _MinInput;
        [ModelLib.CustomAttributes.Default("0.0")]
        public System.Single MinInput
        {
            get { return _MinInput; }
            set { SetField(ref _MinInput, value, () => MinInput); }
        }

        private System.Single _MaxInput;
        [ModelLib.CustomAttributes.Default("1.0")]
        public System.Single MaxInput
        {
            get { return _MaxInput; }
            set { SetField(ref _MaxInput, value, () => MaxInput); }
        }

        private ModelLib.Types.TriState _ClampInput;
        [ModelLib.CustomAttributes.Description("If true input will be clamped to specified range")]
        public ModelLib.Types.TriState ClampInput
        {
            get { return _ClampInput; }
            set { SetField(ref _ClampInput, value, () => ClampInput); }
        }

    }
}



