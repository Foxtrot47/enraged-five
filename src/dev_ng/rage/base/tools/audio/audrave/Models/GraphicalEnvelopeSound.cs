using System.Xml.Serialization;
using ModelLib;

namespace Models
{
    [ModelLib.CustomAttributes.Group("Core Sound Types")]
    public class GraphicalEnvelopeSound : Sound
    {

        private ModelLib.ObjectRef _SoundRef;
        [ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
        [ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
        public ModelLib.ObjectRef SoundRef
        {
            get { return _SoundRef; }
            set { SetField(ref _SoundRef, value, () => SoundRef); }
        }

        private System.Int16 _HoldLoopCount;
        [ModelLib.CustomAttributes.Default("-1")]
        [ModelLib.CustomAttributes.Max(512)]
        [ModelLib.CustomAttributes.Min(-1)]
        public System.Int16 HoldLoopCount
        {
            get { return _HoldLoopCount; }
            set { SetField(ref _HoldLoopCount, value, () => HoldLoopCount); }
        }

        private System.UInt16 _HoldLoopCountVariance;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.Description("Loop count will be set to LoopCount +/- variance")]
        public System.UInt16 HoldLoopCountVariance
        {
            get { return _HoldLoopCountVariance; }
            set { SetField(ref _HoldLoopCountVariance, value, () => HoldLoopCountVariance); }
        }

        private ModelLib.Types.TriState _ShouldStopChildren;
        [ModelLib.CustomAttributes.Default("yes")]
        [ModelLib.CustomAttributes.Description("If yes the child sound will be stopped once the envelope finishes the release phase")]
        public ModelLib.Types.TriState ShouldStopChildren
        {
            get { return _ShouldStopChildren; }
            set { SetField(ref _ShouldStopChildren, value, () => ShouldStopChildren); }
        }

        private ModelLib.Types.TriState _ShouldCascadeRelease;
        [ModelLib.CustomAttributes.Default("no")]
        [ModelLib.CustomAttributes.Description("If yes the child sound will be stopped when this sound is released, regardless of release phase length.")]
        public ModelLib.Types.TriState ShouldCascadeRelease
        {
            get { return _ShouldCascadeRelease; }
            set { SetField(ref _ShouldCascadeRelease, value, () => ShouldCascadeRelease); }
        }

        public class InputParamsDefinition : ModelBase
        {
            private System.Int32 _Time;
            [ModelLib.CustomAttributes.Default("0")]
            [ModelLib.CustomAttributes.Max(65535)]
            [ModelLib.CustomAttributes.Min(-1)]
            [ModelLib.CustomAttributes.Unit("ms")]
            public System.Int32 Time
            {
                get { return _Time; }
                set { SetField(ref _Time, value, () => Time); }
            }

            private System.UInt32 _TimeVariance;
            [ModelLib.CustomAttributes.Default("0")]
            [ModelLib.CustomAttributes.Max(65535)]
            [ModelLib.CustomAttributes.Min(0)]
            [ModelLib.CustomAttributes.Unit("ms")]
            public System.UInt32 TimeVariance
            {
                get { return _TimeVariance; }
                set { SetField(ref _TimeVariance, value, () => TimeVariance); }
            }

            private System.String _Variable;
            [ModelLib.CustomAttributes.Unit("variable")]
            public System.String Variable
            {
                get { return _Variable; }
                set { SetField(ref _Variable, value, () => Variable); }
            }

        }
        private ItemsObservableCollection<InputParamsDefinition> _InputParams = new ItemsObservableCollection<InputParamsDefinition>();
        [ModelLib.CustomAttributes.FixedSize]
        [ModelLib.CustomAttributes.MaxSize(4, typeof(System.UInt32))]
        [XmlElement("InputParams")]
        public ItemsObservableCollection<InputParamsDefinition> InputParams
        {
            get { return _InputParams; }
            set { SetField(ref _InputParams, value, () => InputParams); }
        }

        public class OutputsDefinition : ModelBase
        {
            private System.Single _SmoothRate;
            [ModelLib.CustomAttributes.Default("-1")]
            [ModelLib.CustomAttributes.Description("Time taken for input to traverse entire range, in seconds. -1 means no smoothing will be applied")]
            [ModelLib.CustomAttributes.Max(120)]
            [ModelLib.CustomAttributes.Min(-1)]
            [ModelLib.CustomAttributes.Unit("seconds")]
            public System.Single SmoothRate
            {
                get { return _SmoothRate; }
                set { SetField(ref _SmoothRate, value, () => SmoothRate); }
            }

            private System.String _OutputVariable;
            [ModelLib.CustomAttributes.OutputVariable]
            [ModelLib.CustomAttributes.Unit("variable")]
            public System.String OutputVariable
            {
                get { return _OutputVariable; }
                set { SetField(ref _OutputVariable, value, () => OutputVariable); }
            }

            private EnvelopeSoundMode _Destination;
            [ModelLib.CustomAttributes.Default("kEnvelopeSoundVolume")]
            public EnvelopeSoundMode Destination
            {
                get { return _Destination; }
                set { SetField(ref _Destination, value, () => Destination); }
            }

            private System.UInt16 _padding0;
            [ModelLib.CustomAttributes.Default("0")]
            [ModelLib.CustomAttributes.Hidden]
            public System.UInt16 padding0
            {
                get { return _padding0; }
                set { SetField(ref _padding0, value, () => padding0); }
            }

            public class OutputRangeDefinition : ModelBase
            {
                private System.Single _Min;
                [ModelLib.CustomAttributes.Default("0.0")]
                public System.Single Min
                {
                    get { return _Min; }
                    set { SetField(ref _Min, value, () => Min); }
                }

                private System.Single _Max;
                [ModelLib.CustomAttributes.Default("1.0")]
                public System.Single Max
                {
                    get { return _Max; }
                    set { SetField(ref _Max, value, () => Max); }
                }

            }
            private OutputRangeDefinition _OutputRange;
            public OutputRangeDefinition OutputRange
            {
                get { return _OutputRange; }
                set { SetField(ref _OutputRange, value, () => OutputRange); }
            }

            public class PhasesDefinition : ModelBase
            {
                private System.Single _outputVariance;
                [ModelLib.CustomAttributes.Default("0")]
                [ModelLib.CustomAttributes.Max(1)]
                [ModelLib.CustomAttributes.Min(0)]
                [ModelLib.CustomAttributes.Unit("ms")]
                public System.Single outputVariance
                {
                    get { return _outputVariance; }
                    set { SetField(ref _outputVariance, value, () => outputVariance); }
                }

                public class TransformPointsDefinition : ModelBase
                {
                    private System.Single _x;
                    [ModelLib.CustomAttributes.Max(1.0)]
                    [ModelLib.CustomAttributes.Min(0.0)]
                    public System.Single x
                    {
                        get { return _x; }
                        set { SetField(ref _x, value, () => x); }
                    }

                    private System.Single _y;
                    [ModelLib.CustomAttributes.Max(1.0)]
                    [ModelLib.CustomAttributes.Min(0.0)]
                    public System.Single y
                    {
                        get { return _y; }
                        set { SetField(ref _y, value, () => y); }
                    }

                }
                private ItemsObservableCollection<TransformPointsDefinition> _TransformPoints = new ItemsObservableCollection<TransformPointsDefinition>();
                [ModelLib.CustomAttributes.MaxSize(16, typeof(System.UInt32))]
                [XmlElement("TransformPoints")]
                public ItemsObservableCollection<TransformPointsDefinition> TransformPoints
                {
                    get { return _TransformPoints; }
                    set { SetField(ref _TransformPoints, value, () => TransformPoints); }
                }

                public class BezierPointsDefinition : ModelBase
                {
                    private System.Single _x;
                    [ModelLib.CustomAttributes.Max(1.0)]
                    [ModelLib.CustomAttributes.Min(0.0)]
                    public System.Single x
                    {
                        get { return _x; }
                        set { SetField(ref _x, value, () => x); }
                    }

                    private System.Single _y;
                    [ModelLib.CustomAttributes.Max(500)]
                    [ModelLib.CustomAttributes.Min(-500)]
                    public System.Single y
                    {
                        get { return _y; }
                        set { SetField(ref _y, value, () => y); }
                    }

                }
                private ItemsObservableCollection<BezierPointsDefinition> _BezierPoints = new ItemsObservableCollection<BezierPointsDefinition>();
                [ModelLib.CustomAttributes.MaxSize(16, typeof(System.UInt32))]
                [XmlElement("BezierPoints")]
                public ItemsObservableCollection<BezierPointsDefinition> BezierPoints
                {
                    get { return _BezierPoints; }
                    set { SetField(ref _BezierPoints, value, () => BezierPoints); }
                }

            }
            private ItemsObservableCollection<PhasesDefinition> _Phases = new ItemsObservableCollection<PhasesDefinition>();
            [ModelLib.CustomAttributes.FixedSize]
            [ModelLib.CustomAttributes.MaxSize(4, typeof(System.Byte))]
            [XmlElement("Phases")]
            public ItemsObservableCollection<PhasesDefinition> Phases
            {
                get { return _Phases; }
                set { SetField(ref _Phases, value, () => Phases); }
            }

        }
        private ItemsObservableCollection<OutputsDefinition> _Outputs = new ItemsObservableCollection<OutputsDefinition>();
        [ModelLib.CustomAttributes.MaxSize(6, typeof(System.UInt32))]
        [XmlElement("Outputs")]
        public ItemsObservableCollection<OutputsDefinition> Outputs
        {
            get { return _Outputs; }
            set { SetField(ref _Outputs, value, () => Outputs); }
        }

    }
}

