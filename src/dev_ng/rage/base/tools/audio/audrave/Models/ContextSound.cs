using System.Xml.Serialization;
using ModelLib;

namespace Models
{
    [ModelLib.CustomAttributes.Group("Logic Sound Types")]
    public class ContextSound : Sound
    {

        private ModelLib.ObjectRef _DefaultSound;
        [ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
        [ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
        public ModelLib.ObjectRef DefaultSound
        {
            get { return _DefaultSound; }
            set { SetField(ref _DefaultSound, value, () => DefaultSound); }
        }

        private ModelLib.Types.TriState _DynamicSwitchingEnabled;
        [ModelLib.CustomAttributes.Default("no")]
        public ModelLib.Types.TriState DynamicSwitchingEnabled
        {
            get { return _DynamicSwitchingEnabled; }
            set { SetField(ref _DynamicSwitchingEnabled, value, () => DynamicSwitchingEnabled); }
        }

        private System.UInt16 _CrossfadeTime;
        [ModelLib.CustomAttributes.Default("0")]
        [ModelLib.CustomAttributes.Unit("ms")]
        public System.UInt16 CrossfadeTime
        {
            get { return _CrossfadeTime; }
            set { SetField(ref _CrossfadeTime, value, () => CrossfadeTime); }
        }

        private CrossfadeType _CrossfadeType;
        [ModelLib.CustomAttributes.Default("CROSSFADE_TYPE_EQUAL_POWER")]
        public CrossfadeType CrossfadeType
        {
            get { return _CrossfadeType; }
            set { SetField(ref _CrossfadeType, value, () => CrossfadeType); }
        }

        public class ConditionMappingDefinition : ModelBase
        {
            private ModelLib.ObjectRef _Condition;
            [ModelLib.CustomAttributes.AllowedType(typeof(SoundCondition))]
            [ModelLib.CustomAttributes.SerializeAs(typeof(System.String))]
            public ModelLib.ObjectRef Condition
            {
                get { return _Condition; }
                set { SetField(ref _Condition, value, () => Condition); }
            }

            private ModelLib.ObjectRef _Sound;
            [ModelLib.CustomAttributes.AllowedType(typeof(Sound))]
            [ModelLib.CustomAttributes.SerializeAs(typeof(System.Int32))]
            public ModelLib.ObjectRef Sound
            {
                get { return _Sound; }
                set { SetField(ref _Sound, value, () => Sound); }
            }

        }
        private ItemsObservableCollection<ConditionMappingDefinition> _ConditionMapping = new ItemsObservableCollection<ConditionMappingDefinition>();
        [ModelLib.CustomAttributes.MaxSize(8)]
        [XmlElement("ConditionMapping")]
        public ItemsObservableCollection<ConditionMappingDefinition> ConditionMapping
        {
            get { return _ConditionMapping; }
            set { SetField(ref _ConditionMapping, value, () => ConditionMapping); }
        }

    }
}



