using ModelLib;

namespace Models
{
    [ModelLib.CustomAttributes.FactoryCodeGenerated(false)]
    [ModelLib.CustomAttributes.Group("Logic Sound Types")]
    public class SoundCondition:  ModelBase {

        private System.String _LeftVariable;
        [ModelLib.CustomAttributes.Unit("variable")]
        public System.String LeftVariable {
            get { return _LeftVariable; }
            set { SetField(ref _LeftVariable, value, () => LeftVariable); }
        }

        private ConditionTypes _ConditionType;
        [ModelLib.CustomAttributes.Default("IF_CONDITION_EQUAL_TO")]
        public ConditionTypes ConditionType {
            get { return _ConditionType; }
            set { SetField(ref _ConditionType, value, () => ConditionType); }
        }

        public class RHSDefinition: ModelBase {
            private System.Single _RightValue;
            [ModelLib.CustomAttributes.Default("1")]
            public System.Single RightValue {
                get { return _RightValue; }
                set { SetField(ref _RightValue, value, () => RightValue); }
            }

            private System.String _RightVariable;
            [ModelLib.CustomAttributes.Unit("variable")]
            public System.String RightVariable {
                get { return _RightVariable; }
                set { SetField(ref _RightVariable, value, () => RightVariable); }
            }

        }
        private RHSDefinition _RHS;
        public RHSDefinition RHS {
            get { return _RHS; }
            set { SetField(ref _RHS, value, () => RHS); }
        }

    }
}


