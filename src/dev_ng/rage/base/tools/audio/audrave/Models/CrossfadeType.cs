namespace Models
{
    public enum CrossfadeType
    {
        [ModelLib.CustomAttributes.Display("None")]
        CROSSFADE_TYPE_NONE,
        [ModelLib.CustomAttributes.Display("Linear")]
        CROSSFADE_TYPE_LINEAR,
        [ModelLib.CustomAttributes.Display("EqualPower")]
        CROSSFADE_TYPE_EQUAL_POWER
    }
}

