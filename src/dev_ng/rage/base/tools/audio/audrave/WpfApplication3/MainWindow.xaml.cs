﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Rave.CurveEditor2;
using Rave.CurveEditor2.Curves;
using WPFToolLib.Extensions;

namespace WpfApplication3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MainWindow()
        {
           
           
            InitializeComponent();
            DataContext = this;
            CurveEditor = new CurveEditor(10, 100,0, 0, 1, 1, true, true);
        var test =CurveEditor.AddCurve(new List<Point> { new Point(0, 0), new Point(1, 1) },
                new List<Point> { new Point(0.5, 0.5) }, new Tuple<ExtendedLineMode, ExtendedLineMode>(ExtendedLineMode.Horizontal, ExtendedLineMode.Horizontal), "Blue").Parent;
            CurveEditor.ViewModel.IsCheckedOut = true;
            CurveEditor.ViewModel.YAxis.Destination = ParameterDestinations.Variable;
            CurveEditor.ViewModel.YAxis.Max = 1;
            CurveEditor.ViewModel.YAxis.Min = 0;
            test.IsSelected = true;
            PropertyChanged.Raise(this, "CurveEditor");
           
        }

        public CurveEditor CurveEditor { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

   }
}
