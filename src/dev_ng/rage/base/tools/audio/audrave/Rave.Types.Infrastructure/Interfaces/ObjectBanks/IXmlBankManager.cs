﻿// -----------------------------------------------------------------------
// <copyright file="IXmlBankManager.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.ObjectBanks
{
    using System.Collections.Generic;
    using System.IO;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Enums;

    public interface IXmlBankManager
    {
        /// <summary>
        /// Gets the banks.
        /// </summary>
        List<IXmlBank> Banks { get; }

        /// <summary>
        /// Gets the object banks.
        /// </summary>
        List<IObjectBank> ObjectBanks { get; }

        /// <summary>
        /// Gets the template banks.
        /// </summary>
        List<ITemplateBank> TemplateBanks { get; }

        /// <summary>
        /// Gets the bank type.
        /// </summary>
        BankTypes BankType { get; }

        /// <summary>
        /// Compute the bank references.
        /// </summary>
        void ComputeReferences();

        /// <summary>
        /// Find a bank with specified path.
        /// </summary>
        /// <param name="path">
        /// The bank path.
        /// </param>
        /// <returns>
        /// The found bank <see cref="IXmlBank"/>.
        /// </returns>
        IXmlBank FindBank(string path);

        /// <summary>
        /// Find an object bank with specified path.
        /// </summary>
        /// <param name="path">
        /// The object bank path.
        /// </param>
        /// <returns>
        /// The found object bank <see cref="IXmlBank"/>.
        /// </returns>
        IObjectBank FindObjectBank(string path);

        /// <summary>
        /// Find a bank with a specified name.
        /// </summary>
        /// <param name="name">
        /// The bank name.
        /// </param>
        /// <returns>
        /// The found bank <see cref="IXmlBank"/>.
        /// </returns>
        IXmlBank FindBankByName(string name);

        /// <summary>
        /// Find an object bank with a specified name.
        /// </summary>
        /// <param name="name">
        /// The object bank name.
        /// </param>
        /// <returns>
        /// The found object bank <see cref="IXmlBank"/>.
        /// </returns>
        IObjectBank FindObjectBankByName(string name);

        /// <summary>
        /// Load the banks.
        /// </summary>
        /// <param name="path">
        /// The path to directory containing bank data.
        /// </param>
        /// <param name="episode">
        /// The episode.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="typeDefs">
        /// The type definitions.
        /// </param>
        /// <param name="bankType">
        /// The bank type.
        /// </param>
        void LoadBanks(
            string path, string episode, string type, ITypeDefinitions typeDefs, BankTypes bankType);

        /// <summary>
        /// Load the banks.
        /// </summary>
        /// <param name="directory">
        /// The info about the directory containing the bank data.
        /// </param>
        /// <param name="episode">
        /// The episode.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="bankType">
        /// The bank type.
        /// </param>
        void LoadBanks(DirectoryInfo directory, string episode, string type, BankTypes bankType);

		void LoadBank(FileInfo file, string episode, string type, BankTypes bankType);
		
		
		/// <summary>
        /// Remove a bank.
        /// </summary>
        /// <param name="bank">
        /// The bank to remove.
        /// </param>
        void RemoveBank(IXmlBank bank);
    }
}
