﻿// -----------------------------------------------------------------------
// <copyright file="ISaveable.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.Objects
{
    /// <summary>
    /// ISaveable interface.
    /// </summary>
    public interface ISaveable
    {
        #region Public Methods and Operators

        /// <summary>
        /// Mark as dirty.
        /// </summary>
        void MarkAsDirty();

        #endregion
    }
}
