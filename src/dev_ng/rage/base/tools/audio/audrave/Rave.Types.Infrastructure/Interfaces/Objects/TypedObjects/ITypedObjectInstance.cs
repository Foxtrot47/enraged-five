﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITypedObjectInstance.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Typed object instance interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects
{
    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    /// <summary>
    /// Typed object instance interface.
    /// </summary>
    public interface ITypedObjectInstance : IObjectInstance
    {
        #region Properties

        /// <summary>
        /// Gets the type definition.
        /// </summary>
        ITypeDefinition TypeDefinition { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Populate any missing fields.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool PopulateMissingFields();

        #endregion
    }
}