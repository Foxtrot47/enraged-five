﻿// -----------------------------------------------------------------------
// <copyright file="ITemplate.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.Objects.Templates
{
    using System.Collections.Generic;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;

    /// <summary>
    /// Template interface.
    /// </summary>
    public interface ITemplate : IReferencable, ISaveable
    {
        #region Public Properties

        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        string Type { get; }

        /// <summary>
        /// Gets the type name.
        /// </summary>
        string TypeName { get; }

        /// <summary>
        /// Gets the parent of the proxy object hierarchy.
        /// </summary>
        ITemplateProxyObject Parent { get; }

        /// <summary>
        /// Gets the template bank.
        /// </summary>
        ITemplateBank TemplateBank { get; }

        /// <summary>
        /// Gets the template lookup.
        /// </summary>
        IDictionary<string, ITemplate> TemplateLookup { get; }

        /// <summary>
        /// Gets the template proxy objects.
        /// </summary>
        List<ITemplateProxyObject> ProxyObjects { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Delete the template.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool Delete();

        /// <summary>
        /// Get exposed field names.
        /// </summary>
        /// <returns>
        /// Exposed field names <see cref="List{T}"/>.
        /// </returns>
        List<string> GetExposedFieldNames();

        /// <summary>
        /// Get exposed fields.
        /// </summary>
        /// <returns>
        /// Exposed fields <see cref="Dictionary{TKey,TValue}"/>.
        /// </returns>
        Dictionary<XmlNode, IFieldDefinition> GetExposedFields();

        /// <summary>
        /// Remove delegates.
        /// </summary>
        void RemoveDelegates();

        #endregion
    }
}
