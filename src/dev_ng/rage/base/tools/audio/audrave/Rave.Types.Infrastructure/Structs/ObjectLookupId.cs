﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectLookupId.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Object lookup ID.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Rave.Types.Infrastructure.Structs
{
    /// <summary>
    /// Object lookup ID.
    /// </summary>
    public struct ObjectLookupId
    {
        #region Constants

        /// <summary>
        /// The base.
        /// </summary>
        public const string Base = "BASE";

        /// <summary>
        /// The test.
        /// </summary>
        public const string Test = "TEST";

        #endregion

        #region Fields

        /// <summary>
        /// The episode.
        /// </summary>
        public readonly string Episode;

        /// <summary>
        /// The type.
        /// </summary>
        public readonly string Type;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectLookupId"/> struct.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="episode">
        /// The episode.
        /// </param>
        public ObjectLookupId(string type, string episode)
        {
            this.Type = type.ToUpper();
            this.Episode = episode.ToUpper();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get hash code.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return (this.Type + this.Episode).GetHashCode();
        }

        #endregion
    }
}