﻿// -----------------------------------------------------------------------
// <copyright file="IObjectValidator.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.Objects.Validators
{
    using System.Collections.Generic;
    using System.Text;

    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;

    /// <summary>
    /// Object validator interface.
    /// </summary>
    public interface IObjectValidator
    {
        /// <summary>
        /// Gets a list of the metadata types the object validator is applicable to.
        /// </summary>
        IList<MetadataTypes> SupportedMetadataTypes { get; }

        /// <summary>
        /// Determine if object is valid.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if object is valid <see cref="bool"/>.
        /// </returns>
        bool IsObjectValid(IObjectInstance objectInstance, out StringBuilder errors, out StringBuilder warnings);

        /// <summary>
        /// Determine if objects in bank are valid.
        /// </summary>
        /// <param name="objectBank">
        /// The object bank.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if objects in bank are valid <see cref="bool"/>.
        /// </returns>
        bool IsBankValid(IObjectBank objectBank, out StringBuilder errors, out StringBuilder warnings);
    }
}
