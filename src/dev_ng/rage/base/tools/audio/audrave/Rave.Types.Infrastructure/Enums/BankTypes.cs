﻿// -----------------------------------------------------------------------
// <copyright file="BankTypes.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Enums
{
    /// <summary>
    /// Object bank types.
    /// </summary>
    public enum BankTypes
    {
        /// <summary>
        /// The template.
        /// </summary>
        Template,

        /// <summary>
        /// The object.
        /// </summary>
        Object
    }
}
