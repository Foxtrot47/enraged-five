﻿// -----------------------------------------------------------------------
// <copyright file="IVariableList.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.VariableList
{
    using System;
    using System.Collections.Generic;

    using Forms = System.Windows.Forms;
    using Wpf = System.Windows.Controls;

    public interface IVariableList
    {
        /// <summary>
        /// Gets the root variable list group.
        /// </summary>
        IVariableListGroup Root { get; }

        /// <summary>
        /// Gets all items of every group in variable list.
        /// </summary>
        IList<IVariableListItem> AllItems { get; }

        /// <summary>
        /// Gets all variable items as a flat list of paths made up of
        /// group and item names (i.e. single string per
        /// menu item with parent groups in name separated by common
        /// delimiter).
        /// </summary>
        SortedSet<string> FlatListNames { get; }

        /// <summary>
        /// Gets all variable items as a flat list of paths made up of
        /// group and item labels (i.e. single string per
        /// menu item with parent groups in name separated by common
        /// delimiter).
        /// </summary>
        SortedSet<string> FlatListLabels { get; }

        /// <summary>
        /// Get the WPF version of the menu.
        /// </summary>
        /// <param name="handler">
        /// The onClick event handler for variable list items.
        /// </param>
        /// <returns>
        /// The <see cref="Menu"/>.
        /// </returns>
        Wpf.ContextMenu GetContextMenu_Wpf(Delegate handler = null);

        /// <summary>
        /// Get the Forms version of the menu.
        /// </summary>
        /// <param name="handler">
        /// The onClick event handler for variable list items.
        /// </param>
        /// <returns>
        /// Forms menu <see cref="System.Windows.Forms.Menu"/>.
        /// </returns>
        Forms.ContextMenu GetContextMenu_Forms(EventHandler handler = null);

        /// <summary>
        /// Add a group to the variable list.
        /// Groups are add to the group/sub-group specified in the path,
        /// which is split by a specified delimiter.
        /// E.g. "my.test.group" will add a group "group" to:
        ///     root
        ///     --- my
        ///         --- test
        ///             --- group
        /// </summary>
        /// <param name="path">
        /// The group path, split by delimiter.
        /// </param>
        /// <param name="label">
        /// The group label used for display.
        /// </param>
        /// <param name="includeInPath">
        /// Indicates whether to include group when determining path.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// The new variable list group <see cref="VariableListGroup"/>.
        /// </returns>
        IVariableListGroup AddGroup(string path, string label = null, bool includeInPath = true, char delimiter = '.');

        /// <summary>
        /// Add an item to the variable list group.
        /// Items are add to the group/sub-group specified in the path,
        /// which is split by a specified delimiter.
        /// E.g. "my.test.variable" will add a variable "variable" to:
        ///     root
        ///     --- my
        ///         --- test
        /// </summary>
        /// <param name="path">
        /// The variable path, split by delimiter.
        /// </param>
        /// <param name="label">
        /// The variable list item label.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// The new variable list item <see cref="VariableListItem"/>.
        /// </returns>
        IVariableListItem AddItem(string path, string label = null, char delimiter = '.');

        /// <summary>
        /// Get a list of sub groups of the group matching specified path.
        /// </summary>
        /// <param name="path">
        /// The path to the parent group.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// List of sub groups <see cref="IList{T}"/>.
        /// </returns>
        IList<IVariableListGroup> GetSubGroups(string path, char delimiter = '.');

        /// <summary>
        /// Get a list of items of the group matching specified path.
        /// </summary>
        /// <param name="path">
        /// The path to the parent group.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// List of items <see cref="IList{T}"/>.
        /// </returns>
        IList<IVariableListItem> GetGroupItems(string path, char delimiter = '.');

        /// <summary>
        /// Get an item matching specified path.
        /// </summary>
        /// <param name="path">
        /// The path to the item.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// The item if found, null otherwise <see cref="VariableListItem"/>.
        /// </returns>
        IVariableListItem GetItem(string path, char delimiter = '.');
    }
}
