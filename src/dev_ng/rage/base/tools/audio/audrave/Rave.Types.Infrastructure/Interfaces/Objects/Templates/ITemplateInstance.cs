﻿// -----------------------------------------------------------------------
// <copyright file="ITemplateInstance.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.Objects.Templates
{
    /// <summary>
    /// Template instance interface.
    /// </summary>
    public interface ITemplateInstance : IObjectInstance
    {
        #region Properties

        /// <summary>
        /// Gets the template.
        /// </summary>
        ITemplate Template { get; }

        /// <summary>
        /// Gets a value indicating whether this is a dummy template instance.
        /// </summary>
        bool IsDummy { get; }

        #endregion
    }
}
