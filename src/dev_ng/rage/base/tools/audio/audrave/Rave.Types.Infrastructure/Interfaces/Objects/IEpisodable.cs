﻿// -----------------------------------------------------------------------
// <copyright file="IEpisodable.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.Objects
{
    /// <summary>
    /// Episodable interface.
    /// </summary>
    public interface IEpisodable
    {
        /// <summary>
        /// Gets or sets the episode.
        /// </summary>
        string Episode { get; set; }
    }
}
