﻿// -----------------------------------------------------------------------
// <copyright file="MetadataTypes.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Enums
{
    /// <summary>
    /// The metadata types.
    /// </summary>
    public enum MetadataTypes
    {
        SOUNDS,

        CURVES,

        DYNAMICMIXER,

        CATEGORIES,

        SPEECH,

        GAMEOBJECTS,

        MODULARSYNTH,

        AUDIOCONFIG,
    }
}
