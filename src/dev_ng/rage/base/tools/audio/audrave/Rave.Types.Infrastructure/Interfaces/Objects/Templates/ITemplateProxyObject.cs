﻿// -----------------------------------------------------------------------
// <copyright file="ITemplateProxyObject.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.Objects.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    /// <summary>
    /// Template proxy object interface.
    /// </summary>
    public interface ITemplateProxyObject
    {
        #region Events

        /// <summary>
        /// The on object children changed action.
        /// </summary>
        event Action OnChildrenChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the template.
        /// </summary>
        ITemplate Template { get; }

        /// <summary>
        /// Gets or sets the parent template proxy object.
        /// </summary>
        ITemplateProxyObject Parent { get; set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the node.
        /// </summary>
        XmlNode Node { get; }

        /// <summary>
        /// Gets the type definition.
        /// </summary>
        ITypeDefinition TypeDefinition { get; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        string Type { get; }

        /// <summary>
        /// Gets the type name.
        /// </summary>
        string TypeName { get; }

        /// <summary>
        /// Gets the children.
        /// </summary>
        IList<ITemplateProxyObject> Children { get; }

        /// <summary>
        /// Gets the external references.
        /// </summary>
        IList<KeyValuePair<IObjectInstance, XmlNode>> ExternalReferences { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Compute the references for proxy object.
        /// </summary>
        /// <param name="suppressTypeErrors">
        /// The suppress type errors flag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool ComputeReferences(bool suppressTypeErrors);

        /// <summary>
        /// Returns value indicating whether proxy object exposes a field
        /// with alias matching specified field alias.
        /// </summary>
        /// <param name="fieldAlias">
        /// The field alias.
        /// </param>
        /// <returns>
        /// Value indicating whether proxy object exposes field <see cref="bool"/>.
        /// </returns>
        bool ExposesFieldAs(string fieldAlias);

        #endregion
    }
}
