﻿// -----------------------------------------------------------------------
// <copyright file="ObjectRef.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Structs
{
    using System.Xml;

    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    /// Item reference.
    /// </summary>
    public struct ObjectRef
    {
        /// <summary>
        /// Gets or sets the node.
        /// </summary>
        public XmlNode Node { get; set; }

        /// <summary>
        /// Gets or sets the object instance.
        /// </summary>
        public IObjectInstance ObjectInstance { get; set; }
    }
}
