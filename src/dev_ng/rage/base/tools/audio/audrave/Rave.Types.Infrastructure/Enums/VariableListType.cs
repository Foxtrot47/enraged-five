﻿// -----------------------------------------------------------------------
// <copyright file="VariableListType.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Enums
{
    /// <summary>
    /// The variable list type.
    /// </summary>
    public enum VariableListType
    {
        /// <summary>
        /// Auto list.
        /// </summary>
        Auto,

        /// <summary>
        /// Input list.
        /// </summary>
        Input,

        /// <summary>
        /// Output list.
        /// </summary>
        Output
    }
}
