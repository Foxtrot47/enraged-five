﻿// -----------------------------------------------------------------------
// <copyright file="IVariableListGroup.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.VariableList
{
    using System.Collections.Generic;

    public interface IVariableListGroup
    {
        /// <summary>
        /// Gets the variable list.
        /// </summary>
        IVariableList List { get; }

        /// <summary>
        /// Gets the parent variable list group.
        /// </summary>
        IVariableListGroup Parent { get; }

        /// <summary>
        /// Gets a value indicating whether group is the root group.
        /// </summary>
        bool IsRoot { get; }

        /// <summary>
        /// Gets the variable list group name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets or sets the variable list group label used for display.
        /// </summary>
        string Label { get; set; }

        /// <summary>
        /// Gets a value indicating whether to include group when determining path.
        /// </summary>
        bool IncludeInPath { get; }

        /// <summary>
        /// Gets the full name path to variable group list.
        /// </summary>
        string FullNamePath { get; }

        /// <summary>
        /// Gets the full label path to variable group list.
        /// </summary>
        string FullLabelPath { get; }

        /// <summary>
        /// Gets the variable list groups.
        /// </summary>
        IList<IVariableListGroup> Groups { get; }

        /// <summary>
        /// Gets the variable list group items.
        /// </summary>
        IList<IVariableListItem> Items { get; }

        /// <summary>
        /// The sub group matching specified name.
        /// </summary>
        /// <param name="label">
        /// The label of the sub group to find.
        /// </param>
        /// <returns>
        /// Sub group matching name (null if not found) <see cref="VariableListGroup"/>.
        /// </returns>
        IVariableListGroup GetSubGroup(string label);

        /// <summary>
        /// Get item matching specified path.
        /// </summary>
        /// <param name="path">
        /// The path to the item to find.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// Item found at specified path (null if not found) <see cref="VariableListItem"/>.
        /// </returns>
        IVariableListItem GetItem(string path, char delimiter = '.');

        /// <summary>
        /// Add a sub group to the variable list group.
        /// Groups are add to the group/sub-group specified in the path,
        /// which is split by a specified delimiter.
        /// E.g. "my.test.group" will add a group "group" to:
        ///     root
        ///     --- my
        ///         --- test
        ///             --- group
        /// </summary>
        /// <param name="path">
        /// The variable path, split by delimiter.
        /// </param>
        /// <param name="label">
        /// The label use for display.
        /// </param>
        /// <param name="includeInPath">
        /// Indicates whether to include group when determining path.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// The new variable list group <see cref="VariableListGroup"/>.
        /// </returns>
        IVariableListGroup AddGroup(string path, string label = null, bool includeInPath = true, char delimiter = '.');

        /// <summary>
        /// Add an item to the variable list group.
        /// Items are add to the group/sub-group specified in the path,
        /// which is split by a specified delimiter.
        /// E.g. "my.test.variable" will add a variable "variable" to:
        ///     root
        ///     --- my
        ///         --- test
        /// </summary>
        /// <param name="path">
        /// The variable path, split by delimiter.
        /// </param>
        /// <param name="label">
        /// The variable list item label.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// The new variable list item <see cref="VariableListItem"/>.
        /// </returns>
        IVariableListItem AddItem(string path, string label = null, char delimiter = '.');

        /// <summary>
        /// Helper method for adding a new variable list group in a recursive manner.
        /// </summary>
        /// <param name="splitPath">
        /// The path components (i.e. the group structure for new group).
        /// </param>
        /// <param name="label">
        /// The label used for display.
        /// </param>
        /// <param name="includeInPath">
        /// Indicates whether to include group when determining path.
        /// </param>
        /// <returns>
        /// The new variable list group <see cref="VariableListItem"/>.
        /// </returns>
        IVariableListGroup AddGroup(IList<string> splitPath, string label = null, bool includeInPath = true);

        /// <summary>
        /// Helper method for adding a new variable list item in a recursive manner.
        /// </summary>
        /// <param name="splitPath">
        /// The path components (i.e. the group structure for new item).
        /// </param>
        /// <param name="label">
        /// The variable list item label.
        /// </param>
        /// <returns>
        /// The new variable list item <see cref="VariableListItem"/>.
        /// </returns>
        IVariableListItem AddItem(IList<string> splitPath, string label);
    }
}
