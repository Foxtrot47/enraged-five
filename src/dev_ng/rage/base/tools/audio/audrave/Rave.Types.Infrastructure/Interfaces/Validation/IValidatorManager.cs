﻿// -----------------------------------------------------------------------
// <copyright file="IValidatorManager.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.Validation
{
    using System.Collections.Generic;
    using System.Text;

    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects.Validators;

    /// <summary>
    /// Validator manager.
    /// </summary>
    public interface IValidatorManager
    {
        /// <summary>
        /// Gets the object validators.
        /// </summary>
        IList<IObjectValidator> ObjectValidators { get; }

        /// <summary>
        /// Validate an object bank.
        /// </summary>
        /// <param name="objectBank">
        /// The object bank.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if bank is valid <see cref="bool"/>.
        /// </returns>
        bool ValidateObjectBank(IObjectBank objectBank, out StringBuilder errors, out StringBuilder warnings);

        /// <summary>
        /// Validate a list of object banks.
        /// </summary>
        /// <param name="objectBanks">
        /// The object banks.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if all banks are valid <see cref="bool"/>.
        /// </returns>
        bool ValidateObjectBanks(IList<IObjectBank> objectBanks, out StringBuilder errors, out StringBuilder warnings);
    }
}
