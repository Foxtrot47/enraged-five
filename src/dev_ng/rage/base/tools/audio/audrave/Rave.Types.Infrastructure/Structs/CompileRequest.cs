﻿// -----------------------------------------------------------------------
// <copyright file="CompileRequest.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Structs
{
    using System.Collections.Generic;
    using System.IO;

    using rage;
    using rage.Compiler;

    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;

    /// <summary>
    /// Compile request.
    /// </summary>
    public struct CompileRequest
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the compiler.
        /// </summary>
        public MetaDataCompiler Compiler { get; set; }

        /// <summary>
        /// Gets or sets the metadata file.
        /// </summary>
        public audMetadataFile MetadataFile { get; set; }

        /// <summary>
        /// Gets or sets the object banks.
        /// </summary>
        public List<IXmlBank> ObjectBanks { get; set; }

        /// <summary>
        /// Gets or sets the project settings.
        /// </summary>
        public audProjectSettings ProjectSettings { get; set; }

        /// <summary>
        /// Gets or sets the stream.
        /// </summary>
        public MemoryStream Stream { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether edit mode.
        /// </summary>
        public bool EditMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a tag table gets written by the metadata compiler
        /// </summary>
        public bool WriteTagTable { get; set; }

        #endregion
    }
}
