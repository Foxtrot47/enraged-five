﻿// -----------------------------------------------------------------------
// <copyright file="ITemplateDummyBank.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.ObjectBanks
{
    /// <summary>
    /// Template dummy bank interface.
    /// </summary>
    public interface ITemplateDummyBank : IObjectBank
    {
    }
}
