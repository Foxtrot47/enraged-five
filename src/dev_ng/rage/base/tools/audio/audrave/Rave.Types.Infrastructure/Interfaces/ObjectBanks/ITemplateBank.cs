﻿// -----------------------------------------------------------------------
// <copyright file="ITemplateBank.cs" company="Rockstar North">
//   Template bank interface.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.ObjectBanks
{
    using System.Collections.Generic;
    using System.Text;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;

    /// <summary>
    /// Template bank interface.
    /// </summary>
    public interface ITemplateBank : IXmlBank
    {
        #region Properties

        /// <summary>
        /// Gets the type definitions.
        /// </summary>
        ITypeDefinitions TypeDefinitions { get; }

        /// <summary>
        /// Gets the templates.
        /// </summary>
        IList<ITemplate> Templates { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Add a template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        void AddTemplate(ITemplate template);

        /// <summary>
        /// Remove a template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool RemoveTemplate(ITemplate template);

        /// <summary>
        /// Find a template with specified name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The template, if found <see cref="ITemplate"/>.
        /// </returns>
        ITemplate FindTemplate(string name);

        /// <summary>
        /// Compute the references.
        /// </summary>
        void ComputeReferences();

        /// <summary>
        /// Validate the template bank.
        /// </summary>
        /// <returns>
        /// Value indicating if template bank is valid <see cref="bool"/>.
        /// </returns>
        bool Validate();

        /// <summary>
        /// Remove all references.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool RemoveReferences();

        #endregion
    }
}
