﻿// -----------------------------------------------------------------------
// <copyright file="IVariableListItem.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.VariableList
{
    public interface IVariableListItem
    {
        /// <summary>
        /// Gets the variable list group.
        /// </summary>
        IVariableListGroup Group { get; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets or sets the variable list item label used for display.
        /// </summary>
        string Label { get; set; }

        /// <summary>
        /// Gets the tooltip.
        /// </summary>
        string Tooltip { get; }

        /// <summary>
        /// Gets the full name path to variable list item.
        /// </summary>
        string FullNamePath { get; }

        /// <summary>
        /// Gets the full label path to variable list item.
        /// </summary>
        string FullLabelPath { get; }
    }
}
