﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReferencable.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Referencable interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Rave.Types.Infrastructure.Interfaces.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    using Rave.Types.Infrastructure.Structs;

    /// <summary>
    /// Referencable interface.
    /// </summary>
    public interface IReferencable
    {
        #region Events

        /// <summary>
        /// The references changed event.
        /// </summary>
        event Action ReferencesChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the references.
        /// </summary>
        IList<ObjectRef> References { get; }

        /// <summary>
        /// Gets the referencers.
        /// </summary>
        IList<IReferencable> Referencers { get; } 

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Compute the references.
        /// </summary>
        /// <param name="suppressTypeErrors">
        /// The suppress type errors flag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool ComputeReferences(bool suppressTypeErrors = false);

        /// <summary>
        /// Add a reference.
        /// </summary>
        /// <param name="reference">
        /// The reference.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="index">
        /// The index to add at.
        /// </param>
        void AddReference(IReferencable reference, XmlNode node, int index = -1);

        /// <summary>
        /// Remove a reference.
        /// </summary>
        /// <param name="reference">
        /// The reference.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        void RemoveReference(IReferencable reference, XmlNode node);

        /// <summary>
        /// Gets value indicating whether typed object instance if currently being referenced.
        /// </summary>
        /// <returns>
        /// Indication of being referenced <see cref="bool"/>.
        /// </returns>
        bool IsReferenced();

        /// <summary>
        /// Add a reference.
        /// </summary>
        /// <param name="referencer">
        /// The reference.
        /// </param>
        void AddReferencer(IReferencable referencer);

        /// <summary>
        /// Remove a reference.
        /// </summary>
        /// <param name="referencer">
        /// The reference.
        /// </param>
        void RemoveReferencer(IReferencable referencer);

        #endregion
    }
}