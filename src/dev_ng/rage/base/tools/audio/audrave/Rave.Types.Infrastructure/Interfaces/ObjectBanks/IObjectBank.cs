﻿// -----------------------------------------------------------------------
// <copyright file="IObjectBank.cs" company="Rockstar North">
//   Typed object bank interface.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Infrastructure.Interfaces.ObjectBanks
{
    using System.Collections.Generic;
    using System.Text;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    /// Typed object bank interface.
    /// </summary>
    public interface IObjectBank : IXmlBank
    {
        #region Properties

        /// <summary>
        /// Gets the object lookup.
        /// </summary>
        IDictionary<string, IObjectInstance> ObjectLookup { get; }

        /// <summary>
        /// Gets the object instances.
        /// </summary>
        IList<IObjectInstance> ObjectInstances { get; }

        /// <summary>
        /// Gets the type definitions.
        /// </summary>
        ITypeDefinitions TypeDefinitions { get; }

        /// <summary>
        /// Gets the episode.
        /// </summary>
        string Episode { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Compute the references.
        /// </summary>
        void ComputeReferences();

        /// <summary>
        /// Validate the template bank.
        /// </summary>
        /// <returns>
        /// Value indicating if template bank is valid <see cref="bool"/>.
        /// </returns>
        bool Validate();

        /// <summary>
        /// Find object instance with specified name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The found object instance <see cref="IObjectInstance"/>.
        /// </returns>
        IObjectInstance FindObjectInstance(string name);

        /// <summary>
        /// Delete object instance.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool DeleteObjectInstance(IObjectInstance objectInstance);

        /// <summary>
        /// Remove all references.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool RemoveReferences();

        #endregion
    }
}
