using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Rave.Tests
{
    using Rave.Instance;
    using Rave.ObjectBrowser.Infrastructure.Interfaces;
    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.PropertiesEditor.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.UserActions;

    public static class Test
    {
        private static IWaveBrowser sm_WaveBrowser;
        private static Dictionary<string, IObjectBrowser> sm_Browsers;
        private static IPropertiesEditor sm_PropEditor;
        private static IActionLog sm_ActionLog;
        private static Thread sm_TestThread = null;
        private static bool bIsFinished = false;

        public static void Run(IWaveBrowser waveBrowser, IActionLog actionlog, Dictionary<string, IObjectBrowser> objectBrowsers, IPropertiesEditor propertiesEditor)
        {
            sm_WaveBrowser = waveBrowser;
            sm_ActionLog = actionlog;
            sm_Browsers = objectBrowsers;
            sm_PropEditor = propertiesEditor;
            sm_TestThread = new Thread(new ThreadStart(RunTests));
            sm_TestThread.IsBackground = true;
            sm_TestThread.Start();
        }

        public static bool IsAlive()
        {
            if (sm_TestThread == null || !sm_TestThread.IsAlive)
            {
                return false;
            }
            return true;
        }

        public static bool IsFinished()
        {
            return bIsFinished;
        }

        private static void RunTests()
        {
            Test1();
            Test2();
            Test3();
            Test4();
            bIsFinished = true;
        }

        //Properties Editor
        private static void Test1()
        {
            for (int j = 0; j < 20; j++)
            {
                foreach (KeyValuePair<string, IObjectBrowser> kvp in sm_Browsers)
                {
                    TreeView treeView = kvp.Value.GetTreeView();
                    SoundNode sn = ShowHierarchy(treeView.Nodes[0]);
                    kvp.Value.ShowHierarchy(sn);
                    /*for (int i = 0; i < sm_PropEditor.GetNumberofTabs(); i++)
                    {
                        sm_PropEditor.SetTab(i);
                        Thread.Sleep(200);
                    }*/
                }
            }           
           
        }

        //Wave Browser
        private static void Test2()
        {
            //Add New Pack
            List<string> platforms = new List<string>();
            platforms.Add("PS3");
            platforms.Add("XENON");
            platforms.Add("PC");

            IUserAction action = sm_WaveBrowser.CreateAction(ActionType.AddPack,new ActionParams(sm_WaveBrowser, null, null, null, sm_WaveBrowser.GetEditorTreeView().GetTreeView(), sm_ActionLog, "RAVE_TEST", platforms, null, null));
            if (action.Action())
            {
                sm_ActionLog.AddAction(action);
            }

            EditorTreeNode pack = FindNode("RAVE_TEST", sm_WaveBrowser.GetEditorTreeView().GetTreeView().Nodes);

            if (pack != null)
            {
                //Add New Bank
                IUserAction actionAddWave = sm_WaveBrowser.CreateAction(ActionType.AddBank, new ActionParams(sm_WaveBrowser, pack, null, null, sm_WaveBrowser.GetEditorTreeView().GetTreeView(), sm_ActionLog, "RAVE_TEST_BANK", platforms, null, null));
                if (actionAddWave.Action())
                {
                    sm_ActionLog.AddAction(actionAddWave);
                }
            }            

            //Add Waves
            EditorTreeNode bank = FindNode("RAVE_TEST_BANK",pack.Nodes);
            
            string wavePath;
            string waveName;

            for (int i = 1; i < 9; i++)
            {
                wavePath = RaveInstance.AssetManager.GetLocalPath("/audio/test/TEST" + i + ".WAV");
                waveName = wavePath.Substring(wavePath.LastIndexOf("\\")+1);

                if (bank != null)
                {
                    //Add New Bank
                    IUserAction actionAddWave = sm_WaveBrowser.CreateAction(ActionType.AddWave, new ActionParams(sm_WaveBrowser, bank, null, wavePath, sm_WaveBrowser.GetEditorTreeView().GetTreeView(), sm_ActionLog, waveName, platforms, null, null));
                    if (actionAddWave.Action())
                    {
                        sm_ActionLog.AddAction(actionAddWave);
                    }
                }
            }

            
            //Edit Waves
            EditorTreeNode node = null;
            for (int i = 1; i < 9; i++)
            {
                wavePath = RaveInstance.AssetManager.GetLocalPath("/audio/test/TEST" + i + ".WAV");
                waveName = wavePath.Substring(wavePath.LastIndexOf("\\")+1);

                if (bank != null)
                {
                    node = FindNode(waveName, bank.Nodes);
                    //Add New Bank
                    IUserAction changeWaveAction = sm_WaveBrowser.CreateAction(ActionType.ChangeWave, new ActionParams(sm_WaveBrowser, bank, node, wavePath, sm_WaveBrowser.GetEditorTreeView().GetTreeView(), sm_ActionLog, waveName, platforms, null, null));
                    if (changeWaveAction.Action())
                    {
                        ErrorManager.HandleError("Edit Wave allowed after Add Wave");
                    }
                }
            }

            //Commit Waves
            if (!sm_WaveBrowser.CommitChanges(false))
            {
                ErrorManager.HandleError("Commit Failed");
            }

            //Edit Waves
            node = null;
            for (int i = 1; i < 9; i++)
            {
                wavePath = RaveInstance.AssetManager.GetLocalPath("/audio/test/TEST" + i + ".WAV");
                waveName = wavePath.Substring(wavePath.LastIndexOf("\\")+1);

                if (bank != null)
                {
                    node = FindNode(waveName, bank.Nodes);

                    //Add New Bank
                    IUserAction actionChangeWave = sm_WaveBrowser.CreateAction(ActionType.ChangeWave, new ActionParams(sm_WaveBrowser, bank,node, wavePath, sm_WaveBrowser.GetEditorTreeView().GetTreeView(), sm_ActionLog, waveName, platforms, null, null));
                    if (actionChangeWave.Action())
                    {
                        sm_ActionLog.AddAction(actionChangeWave);
                    }
                }
            }

            //Commit Waves
            if (!sm_WaveBrowser.CommitChanges(false))
            {
                ErrorManager.HandleError("Commit Failed");
            }

            //Delete Waves
            pack = FindNode("RAVE_TEST", sm_WaveBrowser.GetEditorTreeView().GetTreeView().Nodes);
            sm_WaveBrowser.GetEditorTreeView().DeleteNode(pack);

            //Commit Waves
            if (!sm_WaveBrowser.CommitChanges(false))
            {
                ErrorManager.HandleError("Commit Failed");
            }
        }

        private static EditorTreeNode FindNode(string name, TreeNodeCollection nodes)
        {
            foreach(EditorTreeNode node in nodes)
            {
                if (node.GetObjectName() == name)
                {
                    return node;
                }
            }

            return null;
        }

        private static void Test3()
        {
            //Create new Object Store

            //Checkout Object Store

            //Edit store

            //Check in store

            //Delete Store

        }

        private static void Test4()
        {
            //Check out stores
            //Revert Stores

            //Check out stores
            //Check in stores

            //Check out stores
            //Move changelist
            //Revert both changelists

            //Check out stores
            //Move changelist
            //Check in changelists

        }

        private static SoundNode ShowHierarchy(TreeNode node)
        { 
            if (node.GetType() == typeof(SoundNode))
            {
                 return node as SoundNode;
            }
            foreach (TreeNode child in node.Nodes)
            {
                return ShowHierarchy(child);
            }

            return null;
        }
        

    }
}
