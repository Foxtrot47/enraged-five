﻿// -----------------------------------------------------------------------
// <copyright file="WaveSlotChecker.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.BuildManager.WaveSlots
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    using Rave.Instance;
    using Rave.Utils;

    /// <summary>
    /// The wave slot checker.
    /// </summary>
    public class WaveSlotChecker
    {

        /// <summary>
        /// Check the waves slot sizes.
        /// </summary>
        /// <param name="platforms">
        /// The platforms.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public IList<WaveSlotSizes> GetWaveSlotSizes(IList<string> platforms)
        {
            var results = new List<WaveSlotSizes>();

            if (!platforms.Any())
            {
                return results;
            }

            results.AddRange(platforms.Select(this.GetWaveSlotSizes));

            return results;
        }

        /// <summary>
        /// Get the wave slots sizes for a specified platform.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <returns>
        /// Wave slot sizes <see cref="WaveSlotSizes"/>.
        /// </returns>
        public WaveSlotSizes GetWaveSlotSizes(string platform)
        {
            if (!Configuration.ProjectSettings.SetCurrentPlatformByTag(platform))
            {
                ErrorManager.HandleError("Failed to set current platform by tag: " + platform);
                return null;
            }

            var waveSlotXmlPath = Regex.Replace(
                Configuration.ProjectSettings.WaveSlotsXmlPath, "{platform}", platform, RegexOptions.IgnoreCase);

            // Only want to get latest wave slot XML file if we're not currently working on it locally
            if (!RaveInstance.AssetManager.IsCheckedOut(waveSlotXmlPath))
            {
                RaveInstance.AssetManager.GetLatest(waveSlotXmlPath, false);
            }

            var doc = XDocument.Load(waveSlotXmlPath);
            var result = new WaveSlotSizes(platform);

            foreach (var slotElem in doc.Descendants("Slot"))
            {
                var nameElem = slotElem.Element("Name");
                if (null == nameElem)
                {
                    throw new Exception("Failed to find Name element");
                }

                // Remove trailing "_XX" digits, if they exist
                var name = nameElem.Value.ToUpper();

                var sizeElem = slotElem.Element("Size");
                if (null == sizeElem)
                {
                    throw new Exception("Failed to find Size element");
                }

                uint size;
                if (!uint.TryParse(sizeElem.Attribute("value").Value, out size))
                {
                    throw new Exception("Failed to parse Size value: " + sizeElem.Value);
                }

                result.Sizes.Add(name, size);
            }

            return result;
        }

        /// <summary>
        /// Compare two sets of wave slot sizes.
        /// </summary>
        /// <param name="sizesPreBuild">
        /// The wave slot sizes pre-build.
        /// </param>
        /// <param name="sizesPostBuild">
        /// The wave slot sizes post-build.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool CheckWaveSlotLimits(IList<WaveSlotSizes> sizesPreBuild, IList<WaveSlotSizes> sizesPostBuild)
        {
            var errors = new StringBuilder();

            foreach (var postBuildSizeList in sizesPostBuild)
            {
                var platform = postBuildSizeList.Platform;

                var preBuildSizes = sizesPreBuild.FirstOrDefault(s => s.Platform == platform);
                if (null == preBuildSizes)
                {
                    throw new Exception(
                        string.Format(
                            "List A does not contain result set for platform {0} to compare against.", platform));
                }

                if (!Configuration.ProjectSettings.WaveSlotCheckerSettings.WaveSlotLimits.ContainsKey(platform))
                {
                    // Nothing to do if there aren't any limits to check against
                    continue;
                }

                var limits = Configuration.ProjectSettings.WaveSlotCheckerSettings.WaveSlotLimits[platform];

                foreach (var kvp in postBuildSizeList.Sizes)
                {
                    var waveSlotName = kvp.Key;
                    var postBuildSize = kvp.Value;
                    var preBuildSize = 0U;
                    if (preBuildSizes.Sizes.ContainsKey(waveSlotName))
                    {
                        preBuildSize = preBuildSizes.Sizes[waveSlotName];
                    }

                    var shortWaveSlotName = Regex.Replace(waveSlotName, "\\d*$", string.Empty).TrimEnd('_');

                    // Find and check the max limit for slot, if one is set
                    foreach (var limitKvp in limits)
                    {
                        if (limitKvp.Key.ToUpper() != shortWaveSlotName)
                        {
                            continue;
                        }

                        // Only show warning if size has changed and if exceeds limit
                        if (postBuildSize != preBuildSize && postBuildSize > limitKvp.Value)
                        {
                            var error =
                                string.Format(
                                    "WARNING! Slot size has been exceeded (Slot: {0} Limit: {1} Size: {2})",
                                    waveSlotName,
                                    limitKvp.Value,
                                    postBuildSize);
                            errors.AppendLine(error);
                        }

                        break;
                    }
                }
            }

            if (errors.Length > 0)
            {
                ErrorManager.HandleInfo(errors.ToString());
                return false;
            }

            return true;
        }
    }
}
