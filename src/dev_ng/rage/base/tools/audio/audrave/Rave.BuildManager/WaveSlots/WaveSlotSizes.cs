﻿// -----------------------------------------------------------------------
// <copyright file="WaveSlotSizes.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.BuildManager.WaveSlots
{
    using System.Collections.Generic;

    /// <summary>
    /// Wave slot sizes.
    /// </summary>
    public class WaveSlotSizes
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WaveSlots.WaveSlotSizes"/> class.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        public WaveSlotSizes(string platform)
        {
            this.Platform = platform;
            this.Sizes = new Dictionary<string, uint>();
        }

        /// <summary>
        /// Gets the platform.
        /// </summary>
        public string Platform { get; private set; }

        /// <summary>
        /// Gets the wave slot sizes.
        /// </summary>
        public IDictionary<string, uint> Sizes { get; private set; } 
    }
}
