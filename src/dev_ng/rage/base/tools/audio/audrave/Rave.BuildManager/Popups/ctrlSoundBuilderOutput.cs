namespace Rave.BuildManager.Popups
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml.Linq;

    using Rave.BuildManager.Infrastructure.Enums;
    using Rave.BuildManager.Infrastructure.Types;
    using Rave.Utils;

    /// <summary>
    ///   Summary description for ctrlSoundBuilderOutput.
    /// </summary>
    public class ctrlSoundBuilderOutput : UserControl
    {
        private ListBox lbOutput;
        private Label lblStatus;
        private uint m_numErrors, m_numWarnings;

        public ctrlSoundBuilderOutput()
        {
            this.InitializeComponent();
        }

        public void Add(IEnumerable<BuildOutputElement> buildOutputElements, XElement summaryElement)
        {
            this.m_numErrors = 0;
            this.m_numWarnings = 0;

            foreach (var buildOutputElement in buildOutputElements)
            {
                this.lbOutput.Items.Add(buildOutputElement);

                switch (buildOutputElement.OutputElementType)
                {
                    case ElementType.Error:
                    case ElementType.Exception:
                        {
                            var builder = new StringBuilder(buildOutputElement.Message);
                            builder.Append(Environment.NewLine);
                            builder.Append(Environment.NewLine);

                            string file, type, obj;
                            buildOutputElement.GetFriendlyContextElements(out file, out type, out obj);

                            if (!string.IsNullOrEmpty(type))
                            {
                                builder.Append(string.Format("Type: {0}", type));
                                builder.Append(Environment.NewLine);
                            }

                            if (!string.IsNullOrEmpty(file))
                            {
                                builder.Append(string.Format("File: {0}", file));
                                builder.Append(Environment.NewLine);
                            }

                            if (!string.IsNullOrEmpty(obj))
                            {
                                builder.Append(string.Format("Object: {0}", obj));
                                builder.Append(Environment.NewLine);
                            }
                            ErrorManager.HandleError(builder.ToString());

                            this.m_numErrors += 1;
                            break;
                        }
                    case ElementType.Warning:
                        {
                            this.m_numWarnings += 1;
                            break;
                        }
                }
            }

            this.ShowSummary(summaryElement);
        }

        private void ShowSummary(XContainer summaryElement)
        {
            uint numErrors = 0;
            uint numWarnings = 0;
            uint objectsCompiled = 0;
            uint averageObjectSize = 0;
            uint largestObjectSize = 0;
            var largestObjectName = string.Empty;

            if (summaryElement != null)
            {
                foreach (var child in summaryElement.Elements())
                {
                    switch (child.Name.ToString())
                    {
                        case "ObjectsCompiled":
                            {
                                objectsCompiled = uint.Parse(child.Value);
                                break;
                            }
                        case "Errors":
                        case "Exceptions":
                            {
                                numErrors += uint.Parse(child.Value);
                                break;
                            }
                        case "Warnings":
                            {
                                numWarnings = uint.Parse(child.Value);
                                break;
                            }
                        case "AverageObjectSize":
                            {
                                averageObjectSize = uint.Parse(child.Value);
                                break;
                            }
                        case "LargestObjectSize":
                            {
                                largestObjectName = child.Attribute("name").Value;
                                largestObjectSize = uint.Parse(child.Value);
                                break;
                            }
                    }
                }
            }
            else
            {
                numErrors = this.m_numErrors;
                numWarnings = this.m_numWarnings;
            }

            var builder = new StringBuilder("Done - ");

            var isDataBuild = !string.IsNullOrEmpty(largestObjectName);
            if (isDataBuild)
            {
                builder.Append(objectsCompiled);
                builder.Append(" objects compiled. ");
            }

            builder.Append(numErrors);
            builder.Append(" error(s). ");
            builder.Append(numWarnings);
            builder.Append(" warning(s). ");

            if (isDataBuild)
            {
                builder.Append("Average Object Size = ");
                builder.Append(averageObjectSize);
                builder.Append(" bytes. Largest Object = ");
                builder.Append(largestObjectName);
                builder.Append(string.Format(" ({0} bytes).", largestObjectSize));
            }
            this.lbOutput.Items.Add(builder.ToString());
        }

        private void lbOutput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control &&
                e.KeyCode == Keys.C)
            {
                if (this.lbOutput.SelectedItems.Count > 0)
                {
                    var clipboard = "";
                    for (var i = 0; i < this.lbOutput.SelectedItems.Count; ++i)
                    {
                        var index = this.lbOutput.SelectedIndices[i];
                        clipboard = String.Concat(clipboard, this.lbOutput.Items[index].ToString(), '\n');
                    }

                    Clipboard.SetDataObject(clipboard);
                }
            }
        }

        #region Component Designer generated code

        /// <summary>
        ///   Required method for Designer support - do not modify 
        ///   the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbOutput = new System.Windows.Forms.ListBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbOutput
            // 
            this.lbOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbOutput.Location = new System.Drawing.Point(0, 16);
            this.lbOutput.Name = "lbOutput";
            this.lbOutput.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbOutput.Size = new System.Drawing.Size(512, 134);
            this.lbOutput.TabIndex = 0;
            this.lbOutput.KeyDown += new KeyEventHandler(this.lbOutput_KeyDown);
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.SystemColors.Control;
            this.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif",
                                                          9F,
                                                          System.Drawing.FontStyle.Regular,
                                                          System.Drawing.GraphicsUnit.Point,
                                                          ((byte) (0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Black;
            this.lblStatus.Location = new System.Drawing.Point(0, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(512, 16);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "Metadata Compiler Output";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ctrlSoundBuilderOutput
            // 
            this.Controls.Add(this.lbOutput);
            this.Controls.Add(this.lblStatus);
            this.Name = "ctrlSoundBuilderOutput";
            this.Size = new System.Drawing.Size(512, 150);
            this.ResumeLayout(false);
        }

        #endregion
    }
}