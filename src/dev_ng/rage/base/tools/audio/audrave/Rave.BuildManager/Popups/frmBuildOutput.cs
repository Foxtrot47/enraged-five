namespace Rave.BuildManager.Popups
{
    using System.Collections.Generic;
    using System.Windows.Forms;
    using System.Xml.Linq;

    using Rave.BuildManager;
    using Rave.BuildManager.Infrastructure.Types;

    /// <summary>
    ///   Summary description for frmBuildOutput.
    /// </summary>
    public class frmBuildOutput : Form
    {
        private static readonly List<frmBuildOutput> ms_instances = new List<frmBuildOutput>();
        private ctrlSoundBuilderOutput m_buildOutput;

        public frmBuildOutput(string name)
        {
            this.InitializeComponent();
            this.Text += " - " + name;
            ms_instances.Add(this);
        }

        public static void CloseAllBuildWindows()
        {
            foreach (var frm in ms_instances)
            {
                frm.Close();
            }
            ms_instances.Clear();
        }

        public void Add(IEnumerable<BuildOutputElement> outputs, XElement summaryElement)
        {
            this.m_buildOutput.Add(outputs, summaryElement);
        }

        /// <summary>
        ///   Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.m_buildOutput.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///   Required method for Designer support - do not modify
        ///   the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var resources = new System.Resources.ResourceManager(typeof (frmBuildOutput));
            this.m_buildOutput = new ctrlSoundBuilderOutput();
            this.SuspendLayout();
            // 
            // m_buildOutput
            // 
            this.m_buildOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_buildOutput.Location = new System.Drawing.Point(0, 0);
            this.m_buildOutput.Name = "m_buildOutput";
            this.m_buildOutput.Size = new System.Drawing.Size(544, 246);
            this.m_buildOutput.TabIndex = 0;
            // 
            // frmBuildOutput
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(544, 246);
            this.Controls.Add(this.m_buildOutput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmBuildOutput";
            this.Text = "Build Output";
            this.ResumeLayout(false);
        }

        #endregion
    }
}