// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BuildManager.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The build manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.BuildManager
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;
    using System.Xml;
    using System.Xml.Linq;

    using rage;
    using rage.ToolLib;

    using Rave.BuildManager.Infrastructure.Enums;
    using Rave.BuildManager.Infrastructure.Interfaces;
    using Rave.BuildManager.Infrastructure.Types;
    using Rave.BuildManager.Popups;
    using Rave.BuildManager.WaveSlots;
    using Rave.Instance;
    using Rave.Utils;
    using Rave.Utils.Infrastructure.Interfaces;
    using Rave.Utils.Popups;
    using Rave.WaveBrowser.Infrastructure.Interfaces;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The build manager.
    /// </summary>
    public class BuildManager: IBuildManager
    {
        #region Constants

        /// <summary>
        /// The al l_ platforms.
        /// </summary>
        private const string ALL_PLATFORMS = "All Platforms";

        #endregion

        #region Fields

        /// <summary>
        /// The packs to sync AWC files for when also syncing built waves.
        /// </summary>
        private readonly string[] packsSyncAwc = new[] { "RESIDENT" };

        /// <summary>
        /// The m_build info.
        /// </summary>
        private readonly List<KeyValuePair<string, KeyValuePair<List<BuildOutputElement>, XElement>>> m_buildInfo;

        /// <summary>
        /// The m_build queue.
        /// </summary>
        private readonly List<BuildQueueEntry> m_buildQueue;

        /// <summary>
        /// The m_wave browser.
        /// </summary>
        private readonly IWaveBrowser m_waveBrowser;

        /// <summary>
        /// The wave slot checker.
        /// </summary>
        private readonly WaveSlotChecker waveSlotChecker;

        /// <summary>
        /// The m_auto submit.
        /// </summary>
        private bool m_autoSubmit;

        /// <summary>
        /// The m_building.
        /// </summary>
        private bool m_building;

        /// <summary>
        /// The m_succeeded.
        /// </summary>
        private bool m_succeeded;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BuildManager"/> class.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        public BuildManager(IWaveBrowser waveBrowser)
        {
            if (waveBrowser == null)
            {
                throw new ArgumentNullException("waveBrowser");
            }

            this.m_waveBrowser = waveBrowser;

            this.waveSlotChecker = new WaveSlotChecker();
            this.m_buildQueue = new List<BuildQueueEntry>();
            this.m_buildInfo = new List<KeyValuePair<string, KeyValuePair<List<BuildOutputElement>, XElement>>>();
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The save all banks.
        /// </summary>
        public event Action SaveAllBanks;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="buildSet">
        /// The build set.
        /// </param>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <param name="packNames">
        /// The pack names.
        /// </param>
        public void Build(audBuildSet buildSet, string platformTag, ISet<string> packNames = null)
        {
            lock (this)
            {
                if (this.m_building)
                {
                    ErrorManager.HandleInfo("A build is already in progress. Wait until it has finished...");
                    return;
                }

                this.m_buildInfo.Clear();

                // Check if the build set includes a wave build
                var includesWaveBuild = buildSet.BuildElements.Any(element => element.Type == audBuildElementType.Waves);
                if (includesWaveBuild)
                {
                    // Check for uncommitted wave changes
                    if (this.m_waveBrowser.GetActionLog().GetCommitLogCount() > 0)
                    {
                        ErrorManager.HandleError(
                            "There are uncommitted wave browser changes - please commit these before building");
                        return;
                    }

                    GetLatestWaveBuildTools();

                    // Grab latest on AWC file if we require it
                    if (null != packNames)
                    {
                        foreach (var packName in packNames)
                        {
                            if (null != packName && this.packsSyncAwc.Contains(packName.ToUpper()))
                            {
                                foreach (var platformSetting in Configuration.ActivePlatformSettings)
                                {
                                    if (platformTag == ALL_PLATFORMS || platformSetting.PlatformTag == platformTag)
                                    {
                                        var awcPath = Path.Combine(platformSetting.BuildOutput, "SFX", packName) + "\\";
                                        if (!RaveInstance.AssetManager.GetLatest(awcPath, true))
                                        {
                                            ErrorManager.HandleInfo("Failed to grab latest on " + awcPath);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Check if the build set includes a metadata build
                var includesMetadataBuild =
                buildSet.BuildElements.Any(element => element.Type == audBuildElementType.Metadata);
                if (includesMetadataBuild)
                {
                    this.SaveAllBanks.Raise();
                    GetLatestMetadataCompiler();
                }

                // Prompt the user for auto submit and give them a chance to cancel
                var autoSubmitResult = CheckForAutoSubmit();
                if (autoSubmitResult == DialogResult.Cancel)
                {
                    return;
                }

                this.m_autoSubmit = autoSubmitResult == DialogResult.Yes;
                var platforms = Configuration.ActivePlatformSettings.Select(ps => ps.PlatformTag).ToList();

                IList<WaveSlotSizes> preBuildSlotSizes = null;
                if (includesWaveBuild)
                {
                    preBuildSlotSizes = this.waveSlotChecker.GetWaveSlotSizes(platforms);
                }

                foreach (var ps in Configuration.ActivePlatformSettings)
                {
                    if (ps.PlatformTag != platformTag && (platformTag != ALL_PLATFORMS || !ps.BuildEnabled))
                    {
                        continue;
                    }

                    var waveElements = new List<audBuildElement>();
                    var metadataElements = new List<audBuildElement>();

                    foreach (var buildElement in buildSet.BuildElements)
                    {
                        if (buildElement.Type == audBuildElementType.Waves)
                        {
                            waveElements.Add(buildElement);
                        }
                        else
                        {
                            metadataElements.Add(buildElement);
                        }
                    }

                    // Build wave elements first
                    if (null == packNames)
                    {
                        if (waveElements.Any()
                        && !this.BuildElements(
                                waveElements,
                                string.Format("{0} - Waves", buildSet.Name),
                                ps.PlatformTag,
                                null,
                                includesMetadataBuild))
                        {
                            return;
                        }
                    }
                    else
                    {
                        if (packNames.Any(packName => waveElements.Any()
                        && !this.BuildElements(
                                waveElements,
                                string.Format("{0} - Waves", buildSet.Name),
                                ps.PlatformTag,
                                packName,
                                includesMetadataBuild)))
                        {
                            return;
                        }
                    }

                    // Build remaining elements
                    if (null == packNames)
                    {
                        if (metadataElements.Any()
                        && !this.BuildElements(
                                metadataElements,
                                string.Format("{0} - Metadata", buildSet.Name),
                                ps.PlatformTag,
                                null,
                                includesMetadataBuild,
                                MetadataBuildType.Data))
                        {
                            return;
                        }
                    }
                    else
                    {
                        if (packNames.Any(packName => metadataElements.Any()
                        && !this.BuildElements(
                                metadataElements,
                                string.Format("{0} - Metadata", buildSet.Name),
                                ps.PlatformTag,
                                packName,
                                includesMetadataBuild,
                                MetadataBuildType.Data)))
                        {
                            return;
                        }
                    }
                }

                if (includesWaveBuild)
                {
                    this.m_waveBrowser.LoadWaveLists();
                    var postBuildSlotSizes = this.waveSlotChecker.GetWaveSlotSizes(platforms);
                    this.waveSlotChecker.CheckWaveSlotLimits(preBuildSlotSizes, postBuildSlotSizes);
                }

                ErrorManager.HandleInfo("Build finished");
            }
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="metadataBuildType">
        /// The metadata build type.
        /// </param>
        /// <param name="metadataType">
        /// The metadata type.
        /// </param>
        public void Build(MetadataBuildType metadataBuildType, audMetadataType metadataType)
        {
            lock (this)
            {
                if (this.m_building)
                {
                    ErrorManager.HandleInfo("A build is already in progress. Wait until it has finished...");
                    return;
                }

                this.m_buildInfo.Clear();

                this.m_autoSubmit = false;
                this.SaveAllBanks.Raise();
                GetLatestMetadataCompiler();

                switch (metadataBuildType)
                {
                    case MetadataBuildType.Data:
                        {
                            var name = "Metadata Build";

                            // Prompt the user for auto submit and give them a chance to cancel
                            var autoSubmitResult = CheckForAutoSubmit();
                            if (autoSubmitResult == DialogResult.Cancel)
                            {
                                return;
                            }

                            this.m_autoSubmit = autoSubmitResult == DialogResult.Yes;

                            name += string.Concat(" (", metadataType.Type, ")");

                            foreach (PlatformSetting ps in Configuration.ActivePlatformSettings)
                            {
                                string syncChangelistNumber;
                                var cl = CreateChangelist(name, ps.PlatformTag, metadataType.Type, out syncChangelistNumber);

                                var entry = new BuildQueueEntry {
                                    BuildElement =
                                                    new audBuildElement(
                                        audBuildElementType.Metadata, metadataType.Type),
                                    PlatformTag = ps.PlatformTag,
                                    Changelist = cl,
                                    SyncChangelistNumber = syncChangelistNumber,
                                    PackName = null,
                                    MetadataBuildType = metadataBuildType
                                };

                                this.m_buildQueue.Add(entry);
                                this.BuildQueueAndCheckin(cl, name);
                            }

                            break;
                        }

                    case MetadataBuildType.Code:
                        {
                            var name = "Metadata Code Build";
                            IChangeList cl = null;
                            string syncChangelistNumber = null;

                            if (!metadataType.DisableAutoCheckOut)
                            {
                                // Prompt the user for auto submit and give them a chance to cancel
                                var autoSubmitResult = CheckForAutoSubmit();
                                if (autoSubmitResult == DialogResult.Cancel)
                                {
                                    return;
                                }

                                this.m_autoSubmit = autoSubmitResult == DialogResult.Yes;

                                cl = CreateChangelist(name, null, metadataType.Type, out syncChangelistNumber, false);
                            }

                            name += string.Concat(" (", metadataType.Type, ")");

                            this.m_buildQueue.Add(
                                new BuildQueueEntry {
                                    BuildElement =
                                        new audBuildElement(
                                        audBuildElementType.Metadata, metadataType.Type),
                                    PlatformTag = string.Empty,
                                    Changelist = cl,
                                    SyncChangelistNumber = syncChangelistNumber,
                                    PackName = null,
                                    MetadataBuildType = metadataBuildType
                                });

                            this.BuildQueueAndCheckin(cl, name);
                            break;
                        }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The check for auto submit.
        /// </summary>
        /// <returns>
        /// The <see cref="DialogResult"/>.
        /// </returns>
        private static DialogResult CheckForAutoSubmit()
        {
            return MessageBox.Show(
                RaveInstance.ActiveWindow,
                "Would you like to automatically submit when the build completes?",
                "Build",
                MessageBoxButtons.YesNoCancel);
        }

        /// <summary>
        /// The convert to asset manager resolve type.
        /// </summary>
        /// <param name="resolve">
        /// The resolve.
        /// </param>
        /// <returns>
        /// The <see cref="ResolveAction"/>.
        /// </returns>
        private static ResolveAction ConvertToAssetManagerResolveType(ResolveType resolve)
        {
            switch (resolve)
            {
                case ResolveType.AcceptYours:
                    {
                        return ResolveAction.AcceptYours;
                    }

                case ResolveType.AcceptTheirs:
                    {
                        return ResolveAction.AcceptTheirs;
                    }

                case ResolveType.AcceptMerged:
                    {
                        return ResolveAction.AcceptMerged;
                    }

                default:
                    {
                        return ResolveAction.AcceptAuto;
                    }
            }
        }

        /// <summary>
        /// The create changelist.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <param name="extra">
        /// The extra.
        /// </param>
        /// <param name="syncChangelistNumber">
        /// The sync changelist number.
        /// </param>
        /// <param name="useSyncChangelistNumber">
        /// The use sync changelist number.
        /// </param>
        /// <returns>
        /// The <see cref="IChangeList"/>.
        /// </returns>
        private static IChangeList CreateChangelist(
            string title,
            string platformTag,
            string extra,
            out string syncChangelistNumber,
            bool useSyncChangelistNumber = true)
        {
            var assetManager = RaveInstance.AssetManager;

            syncChangelistNumber = useSyncChangelistNumber
                                       ? assetManager.GetLatestChangeListNumber().ToString(CultureInfo.InvariantCulture)
                                       : null;

            var builder = new StringBuilder();
            builder.Append(title);

            if (!string.IsNullOrEmpty(platformTag))
            {
                builder.Append(" - ");
                builder.Append(platformTag);
            }

            if (!string.IsNullOrEmpty(extra))
            {
                builder.Append(" (");
                builder.Append(extra);
                builder.Append(")");
            }

            if (!string.IsNullOrEmpty(syncChangelistNumber))
            {
                builder.Append(" @ CL ");
                builder.Append(syncChangelistNumber);
            }

            return assetManager.CreateChangeList(builder.ToString());
        }

        /// <summary>
        /// The get latest metadata compiler.
        /// </summary>
        private static void GetLatestMetadataCompiler()
        {
            var projectMetadataCompiler = Configuration.MetadataCompilerPath;
            var assetManager = RaveInstance.AssetManager;
            if (assetManager.ExistsAsAsset(projectMetadataCompiler))
            {
                var path = string.Concat(Path.GetDirectoryName(projectMetadataCompiler), "\\");
                try
                {
                    assetManager.GetLatestForceErrors(path);
                }
                catch (Exception ex)
                {
                    ErrorManager.HandleError("Failed to get latest metadata compiler: " + ex.Message);
                }
            }
        }

        /// <summary>
        /// The get latest wave build tools.
        /// </summary>
        private static void GetLatestWaveBuildTools()
        {
            // don't clobber...it makes pipeline development/testing very difficult.
            RaveInstance.AssetManager.GetLatest(Configuration.WaveBuildToolsPath, false);
        }

        /// <summary>
        /// The get metadata compiler command line.
        /// </summary>
        /// <param name="metadataType">
        /// The metadata type.
        /// </param>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <param name="buildCode">
        /// The build code.
        /// </param>
        /// <param name="syncChangelistNumber">
        /// Sync change list number.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetMetadataCompilerCommandLine(
            audMetadataType metadataType, string platformTag, bool buildCode, string syncChangelistNumber, string runtimeMode = null)
        {
            var builder = new StringBuilder();
            builder.Append(" -project ");
            builder.Append(Configuration.ProjectSettingsAssetPath);

            builder.Append(" -metadata ");
            builder.Append(metadataType.Type);

            builder.Append(" -platform ");
            builder.Append(platformTag);

            builder.Append(" -workingpath ");
            builder.Append(Configuration.WorkingPath);

            builder.Append(" -logformat XML");

            if (buildCode)
            {
                builder.Append(" -codeoutput true");

                if (!string.IsNullOrEmpty(Configuration.SoftRoot))
                {
                    builder.Append(" -softroot ");
                    builder.Append(Configuration.SoftRoot);
                }

                if (!string.IsNullOrEmpty(metadataType.NameSpaceName))
                {
                    builder.Append(" -nameSpace ");
                    builder.Append(metadataType.NameSpaceName);
                }
            }

            if (!string.IsNullOrEmpty(syncChangelistNumber))
            {
                builder.Append(" -cl ");
                builder.Append(syncChangelistNumber);
            }

            if (null != runtimeMode)
            {
                builder.Append(" -runtimemode " + runtimeMode);
            }

            return builder.ToString();
        }

        /// <summary>
        /// The get valid platform tag.
        /// </summary>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetValidPlatformTag(string platformTag)
        {
            if (string.IsNullOrEmpty(platformTag))
            {
                platformTag = Configuration.ActivePlatformSettings[0].PlatformTag;
            }

            return platformTag;
        }

        /// <summary>
        /// The inform user of failure.
        /// </summary>
        /// <param name="cl">
        /// The cl.
        /// </param>
        private static void InformUserOfFailure(IChangeList cl)
        {
            var revert = true;
            if (Configuration.IsProgrammerSupportEnabled)
            {
                revert =
                    MessageBox.Show(
                        RaveInstance.ActiveWindow,
                        string.Format(
                            "Error in build. Should I revert this changelist?. {0}Please see build monitor errors first!",
                            Environment.NewLine),
                        "Build Error!",
                        MessageBoxButtons.YesNo).Equals(DialogResult.Yes);
            }

            if (revert)
            {
                if (cl != null)
                {
                    cl.Revert(true);
                }

                MessageBox.Show(
                    RaveInstance.ActiveWindow,
                    string.Format(
                        "There were one or more errors in build. {0}All changes have been reverted.",
                        Environment.NewLine),
                    "Build Error!");
            }
        }

        /// <summary>
        /// The try resolve output files.
        /// </summary>
        /// <param name="buildModuleNodes">
        /// The build module nodes.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool TryResolveOutputFiles(IEnumerable<XmlNode> buildModuleNodes)
        {
            var assetMgr = RaveInstance.AssetManager;
            var projectSettings = new audProjectSettings(assetMgr.GetLocalPath(Configuration.ProjectSettingsAssetPath));

            foreach (var buildModuleNode in buildModuleNodes)
            {
                var outputFiles =
                    buildModuleNode.ChildNodes.Cast<XmlNode>()
                                   .Where(x => string.Compare(x.Name, "OutputFile", true) == 0)
                                   .Select(x => new BuildFile(x))
                                   .ToList();
                foreach (var outputFile in outputFiles)
                {
                    if (outputFile.Checkout && !outputFile.Lock)
                    {
                        foreach (PlatformSetting platformSetting in Configuration.ActivePlatformSettings)
                        {
                            projectSettings.SetCurrentPlatformByTag(platformSetting.PlatformTag);

                            var file = assetMgr.GetLocalPath(projectSettings.ResolvePath(outputFile.File));
                            if (assetMgr.IsCheckedOut(file) && !assetMgr.IsResolved(file))
                            {
                                var resolveType = ConvertToAssetManagerResolveType(outputFile.Resolve);
                                try
                                {
                                    assetMgr.Resolve(file, resolveType);
                                }
                                catch (Exception ex)
                                {
                                    ErrorManager.HandleError(ex);
                                    return false;
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Build and submit a list of build elements.
        /// </summary>
        /// <param name="elements">
        /// The elements.
        /// </param>
        /// <param name="buildName">
        /// The build name.
        /// </param>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <param name="includesMetadataBuild">
        /// The includes metadata build.
        /// </param>
        /// <param name="metadataBuildType">
        /// The metadata build type (if applicable).
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool BuildElements(IEnumerable<audBuildElement> elements, string buildName, string platformTag, string packName, bool includesMetadataBuild, MetadataBuildType? metadataBuildType = null)
        {
            string syncChangeListNumber;
            var changeList = CreateChangelist(buildName, platformTag, packName, out syncChangeListNumber);

            foreach (var element in elements)
            {
                var entry = new BuildQueueEntry {
                    Changelist = changeList,
                    BuildElement = element,
                    PlatformTag = platformTag,
                    SyncChangelistNumber = syncChangeListNumber,
                    PackName = packName
                };

                if (null != metadataBuildType)
                {
                    entry.MetadataBuildType = (MetadataBuildType)metadataBuildType;
                }

                this.m_buildQueue.Add(entry);
            }

            return this.BuildQueueAndCheckin(changeList, buildName, includesMetadataBuild);
        }

        /// <summary>
        /// The build metadata code.
        /// </summary>
        /// <param name="metadataType">
        /// The metadata type.
        /// </param>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <param name="cl">
        /// The cl.
        /// </param>
        /// <param name="syncChangelistNumber">
        /// The sync changelist number.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool BuildMetadataCode(
            audMetadataType metadataType, string platformTag, IChangeList cl, string syncChangelistNumber)
        {
            try
            {
                if (!metadataType.DisableAutoCheckOut)
                {
                    var assetMgr = RaveInstance.AssetManager;
                    try
                    {
                        var headerPath =
                            assetMgr.GetLocalPath(string.Concat(Configuration.SoftRoot, metadataType.CodePath));
                        var cppPath = headerPath.ToUpper().Replace(".H", ".CPP");
                        var paths = new List<string> { headerPath, cppPath };

                        if (!string.IsNullOrEmpty(metadataType.ClassFactoryCodePath))
                        {
                            paths.Add(
                                assetMgr.GetLocalPath(
                                    string.Concat(Configuration.SoftRoot, metadataType.ClassFactoryCodePath)));
                        }

                        foreach (var path in paths)
                        {
                            if (assetMgr.ExistsAsAsset(path) && !assetMgr.IsCheckedOut(path))
                            {
                                cl.CheckoutAsset(path, false);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorManager.HandleError(ex);
                        return false;
                    }
                }

                return this.ProcessMetadataBuild(metadataType, platformTag, true, syncChangelistNumber, "dev");
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }
        }

        /// <summary>
        /// The build metadata data.
        /// </summary>
        /// <param name="metadataType">
        /// The metadata type.
        /// </param>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <param name="cl">
        /// The cl.
        /// </param>
        /// <param name="syncChangelistNumber">
        /// The sync changelist number.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool BuildMetadataData(
            audMetadataType metadataType, string platformTag, IChangeList cl, string syncChangelistNumber)
        {
            try
            {
                var compiler = RaveInstance.XmlBankCompiler.MetadataManagers[metadataType.Type].Compiler;
                var schemaVersion = compiler.Version;
                var assetMgr = RaveInstance.AssetManager;

                var filesToAdd = new List<string>();
                foreach (var metadataFile in Configuration.MetadataFiles)
                {
                    if (metadataFile.Type.ToUpper() == metadataType.Type)
                    {
                        Configuration.SetCurrentPlatformByTag(platformTag);
                        var path =
                            string.Concat(
                                assetMgr.GetLocalPath(Configuration.ResolvePath(metadataFile.OutputFile)), schemaVersion);

                        // Checkout/mark for add the .DAT?? file
                        if (assetMgr.ExistsAsAsset(path))
                        {
                            if (!assetMgr.IsCheckedOut(path))
                            {
                                cl.CheckoutAsset(path, false);
                            }
                        }
                        else
                        {
                            filesToAdd.Add(path);
                        }

                        // Checkout/mark for add the .DAT??.NAMETABLE file
                        var nameTablePath = string.Concat(path, ".nametable");
                        if (assetMgr.ExistsAsAsset(nameTablePath))
                        {
                            if (!assetMgr.IsCheckedOut(nameTablePath))
                            {
                                cl.CheckoutAsset(nameTablePath, false);
                            }
                        }
                        else
                        {
                            filesToAdd.Add(nameTablePath);
                        }

                        // Checkout/add .dat??.rel file
                        var releasePath = path + ".rel";
                        if (assetMgr.ExistsAsAsset(releasePath))
                        {
                            if (!assetMgr.IsCheckedOut(releasePath))
                            {
                                cl.CheckoutAsset(releasePath, false);
                            }
                        }
                        else
                        {
                            filesToAdd.Add(releasePath);
                        }
                    }
                }

                if (!this.ProcessMetadataBuild(metadataType, platformTag, false, syncChangelistNumber, "dev"))
                {
                    return false;
                }

                if (!this.ProcessMetadataBuild(metadataType, platformTag, false, syncChangelistNumber, "release"))
                {
                    return false;
                }

                // Mark any new files for add
                foreach (var fileToAdd in filesToAdd)
                {
                    if (File.Exists(fileToAdd) && !assetMgr.IsMarkedForAdd(fileToAdd))
                    {
                        cl.MarkAssetForAdd(fileToAdd, "binary");
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }
        }

        /// <summary>
        /// The build queue.
        /// </summary>
        private void BuildQueue()
        {
            using (var busyForm = new frmBusy())
            {
                busyForm.SetStatusText("Building ...");
                busyForm.SetUnknownDuration(false);
                busyForm.SetProgress(0);
                busyForm.Show();

                this.m_building = true;

                var buildThread = new Thread(this.ProcessBuildQueue);
                buildThread.Start(busyForm);

                while (this.m_building)
                {
                    Application.DoEvents();
                    Thread.Sleep(50);
                }
            }
        }

        /// <summary>
        /// The build queue and checkin.
        /// </summary>
        /// <param name="cl">
        /// The cl.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="includesMetadataBuild">
        /// The includes metadata build.
        /// </param>
        private bool BuildQueueAndCheckin(
            IChangeList cl, string name, bool includesMetadataBuild = true)
        {
            this.BuildQueue();

            if (includesMetadataBuild)
            {
                var platforms = new List<string>();
                foreach (var buildInfo in this.m_buildInfo)
                {
                    if (!platforms.Contains(buildInfo.Key))
                    {
                        platforms.Add(buildInfo.Key);
                    }
                }

                foreach (var platform in platforms)
                {
                    var buildOutput =
                        new frmBuildOutput(
                            string.IsNullOrEmpty(platform)
                                ? string.Format("{0}", name)
                                : string.Format("{0} - {1}", name, platform));
                    foreach (var buildInfo in this.m_buildInfo)
                    {
                        if (buildInfo.Key == platform)
                        {
                            buildOutput.Add(buildInfo.Value.Key, buildInfo.Value.Value);
                        }
                    }

                    buildOutput.Show(RaveInstance.ActiveWindow);
                }
            }

            if (!this.m_succeeded)
            {
                InformUserOfFailure(cl);
                return false;
            }

            this.CheckIn(cl);

            return true;
        }

        /// <summary>
        /// The build waves.
        /// </summary>
        /// <param name="cl">
        /// The cl.
        /// </param>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <param name="syncChangelistNumber">
        /// The sync changelist number.
        /// </param>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool BuildWaves(IChangeList cl, string platformTag, string syncChangelistNumber, string packName)
        {
            if (this.m_waveBrowser.GetActionLog().GetCommitLogCount() > 0)
            {
                // inhibit a build if actions are uncommitted
                return false;
            }

            try
            {
                var commandLine = this.GetWaveBuildCommandLine(
                    cl.NumberAsString, platformTag, syncChangelistNumber, packName);
                var process =
                    Process.Start(
                        new ProcessStartInfo(Configuration.WaveBuildPath, commandLine) {
                            UseShellExecute = false,
                            CreateNoWindow = false,
                            RedirectStandardOutput = false,
                            WorkingDirectory = Path.GetDirectoryName(Configuration.WaveBuildPath)
                        });
                process.WaitForExit();

                var succeeded = process.ExitCode == 0;
                if (succeeded)
                {
                    if (!TryResolveOutputFiles(Configuration.ProjectSettings.BuildModules))
                    {
                        succeeded = false;
                    }
                }

                return succeeded;
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }
        }

        /// <summary>
        /// The check in.
        /// </summary>
        /// <param name="cl">
        /// The cl.
        /// </param>
        private void CheckIn(IChangeList cl)
        {
            try
            {
                if (cl == null)
                {
                    return;
                }

                cl.RevertUnchanged();
                if (cl.Assets.Count == 0)
                {
                    cl.Delete();
                    MessageBox.Show(
                        RaveInstance.ActiveWindow,
                        "All files in this changelist are identical to the server. \r\nThere's no need to commit them.",
                        "Changelist deleted");
                }
                else if (this.m_autoSubmit)
                {
                    var changeListNumber = cl.NumberAsString;
                    if (!cl.Submit())
                    {
                        ErrorManager.HandleError(string.Format("Failed to submit change list {0} - please check P4V and submit manually", changeListNumber));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleException("Failed to check in build. Check P4V.", ex);
            }
        }

        /// <summary>
        /// The failed.
        /// </summary>
        private void Failed()
        {
            this.m_buildQueue.Clear();
            this.m_succeeded = false;
        }

        /// <summary>
        /// The get wave build command line.
        /// </summary>
        /// <param name="changelistNumber">
        /// The changelist number.
        /// </param>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <param name="syncChangelistNumber">
        /// The sync changelist number.
        /// </param>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetWaveBuildCommandLine(
            string changelistNumber, string platformTag, string syncChangelistNumber, string packName)
        {
            var builder = new StringBuilder();
            builder.Append("-ASSETSERVER ");
            builder.Append(Configuration.AssetServer);
            builder.Append(" -ASSETPROJECT ");
            builder.Append(Configuration.AssetProject);
            builder.Append(" -ASSETUSERNAME ");
            builder.Append(Configuration.AssetUser);
            if (!string.IsNullOrEmpty(Configuration.AssetPassword))
            {
                builder.Append(" -ASSETPASSWORD ");
                builder.Append(Configuration.AssetPassword);
            }

            builder.Append(" -DEPOTROOT ");
            builder.Append(Configuration.DepotRoot);
            builder.Append(" -PROJECTSETTINGS ");
            builder.Append(RaveInstance.AssetManager.GetLocalPath(Configuration.ProjectSettingsAssetPath));
            builder.Append(" -PLATFORM ");
            builder.Append(platformTag);
            builder.Append(" -CHANGE ");
            builder.Append(changelistNumber);
            builder.Append(" -SYNCNUMBER ");
            builder.Append(syncChangelistNumber);
            builder.Append(" -STAY_OPEN_ON_ERROR true");

            if (!string.IsNullOrWhiteSpace(packName))
            {
                builder.Append(" -PACK ");
                builder.Append(packName);
            }

            return builder.ToString();
        }

        /// <summary>
        /// The process build queue.
        /// </summary>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        private void ProcessBuildQueue(object parameter)
        {
            try
            {
                var busy = parameter as IBusy;
                var numItemsToBuild = this.m_buildQueue.Count;
                this.m_succeeded = true;

                var numRemainingItems = numItemsToBuild;
                while (numRemainingItems > 0)
                {
                    var entry = this.m_buildQueue[0];
                    this.m_buildQueue.RemoveAt(0);

                    var progress = ((numItemsToBuild - numRemainingItems + 1) / (float)numItemsToBuild) * 100.0f;
                    busy.SetProgress((int)progress);
                    busy.SetStatusText(
                        string.Format(
                            "{0} {1} ({2})", entry.BuildElement.Type, entry.BuildElement.Name, entry.PlatformTag));
                    switch (entry.BuildElement.Type)
                    {
                        case audBuildElementType.Metadata:
                            {
                                var metadataType = RaveUtils.FindMetadataTypeFromName(entry.BuildElement.Name);
                                switch (entry.MetadataBuildType)
                                {
                                    case MetadataBuildType.Code:
                                        {
                                            if (
                                                !this.BuildMetadataCode(
                                                    metadataType,
                                                    entry.PlatformTag,
                                                    entry.Changelist,
                                                    entry.SyncChangelistNumber))
                                            {
                                                this.Failed();
                                                return;
                                            }

                                            break;
                                        }

                                    case MetadataBuildType.Data:
                                        {
                                            if (
                                                !this.BuildMetadataData(
                                                    metadataType,
                                                    entry.PlatformTag,
                                                    entry.Changelist,
                                                    entry.SyncChangelistNumber))
                                            {
                                                this.Failed();
                                                return;
                                            }

                                            break;
                                        }
                                }

                                break;
                            }

                        case audBuildElementType.Waves:
                            {
                                if (
                                    !this.BuildWaves(
                                        entry.Changelist, entry.PlatformTag, entry.SyncChangelistNumber, entry.PackName))
                                {
                                    this.Failed();
                                    return;
                                }

                                break;
                            }
                    }

                    numRemainingItems -= 1;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                this.Failed();
                return;
            }
            finally
            {
                this.m_building = false;
            }
        }

        /// <summary>
        /// The process metadata build.
        /// </summary>
        /// <param name="metadataType">
        /// The metadata type.
        /// </param>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <param name="buildCode">
        /// The build code.
        /// </param>
        /// <param name="syncChangelistNumber">
        /// The sync changelist number.
        /// </param>
        /// <param name="runtimeMode">
        /// The runtime mode.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool ProcessMetadataBuild(
            audMetadataType metadataType, string platformTag, bool buildCode, string syncChangelistNumber, string runtimeMode = null)
        {
            // Make sure we have the correct version of all object data and type definitions before firing off the metadata compiler
            if (buildCode)
            {
                RaveInstance.AssetManager.GetLatest(Configuration.ObjectXmlPath, false);
            }
            else
            {
                RaveInstance.AssetManager.GetLatest(Configuration.ObjectXmlPath, false, syncChangelistNumber);
            }

            var commandLine = GetMetadataCompilerCommandLine(metadataType, GetValidPlatformTag(platformTag), buildCode, syncChangelistNumber, runtimeMode);

            var psi = new ProcessStartInfo(Configuration.MetadataCompilerPath, commandLine) {
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardOutput =
                                  true
            };

            try
            {
                var p = Process.Start(psi);
                var output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();

                var index = output.IndexOf("<MetadataCompilerOutput>");
                if (index > 0)
                {
                    output = output.Substring(index);
                }

                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(output);
                var doc = rage.ToolLib.Utility.ToXDoc(xmlDoc);

                var buildOutputElements = new List<BuildOutputElement>();
                XElement summaryElement = null;
                foreach (var element in doc.Root.Elements())
                {
                    if (element.Name == "Summary")
                    {
                        summaryElement = element;
                    }
                    else if (element.Name != "Text")
                    {
                        buildOutputElements.Add(new BuildOutputElement(element));
                    }
                }

                var messages = new KeyValuePair<List<BuildOutputElement>, XElement>(buildOutputElements, summaryElement);
                var buildInfo = new KeyValuePair<string, KeyValuePair<List<BuildOutputElement>, XElement>>(
                    platformTag, messages);
                this.m_buildInfo.Add(buildInfo);

                return
                    !buildOutputElements.Any(
                        x => x.OutputElementType == ElementType.Exception || x.OutputElementType == ElementType.Error);
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex.Message);
                return false;
            }
        }

        #endregion

        /// <summary>
        /// The build queue entry.
        /// </summary>
        private struct BuildQueueEntry
        {
            #region Public Properties

            /// <summary>
            /// Gets or sets the build element.
            /// </summary>
            public audBuildElement BuildElement { get; set; }

            /// <summary>
            /// Gets or sets the changelist.
            /// </summary>
            public IChangeList Changelist { get; set; }

            /// <summary>
            /// Gets or sets the metadata build type.
            /// </summary>
            public MetadataBuildType MetadataBuildType { get; set; }

            /// <summary>
            /// Gets or sets the pack name.
            /// </summary>
            public string PackName { get; set; }

            /// <summary>
            /// Gets or sets the platform tag.
            /// </summary>
            public string PlatformTag { get; set; }

            /// <summary>
            /// Gets or sets the sync changelist number.
            /// </summary>
            public string SyncChangelistNumber { get; set; }

            #endregion
        }
    }
}