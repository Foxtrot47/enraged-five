﻿// -----------------------------------------------------------------------
// <copyright file="PedPlaceholderVoices.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.PedPlaceholderVoices
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;
    using global::Rave.Plugins.Infrastructure.Interfaces;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    /// Report that generates a list of peds with placeholder voices.
    /// </summary>
    public class PedPlaceholderVoices : IRAVEReportPlugin
    {
        /// <summary>
        /// The placeholder voices.
        /// </summary>
        private static readonly string[] PlaceholderVoices = new[]
            {
                "COP_PVG", 
                "FEMALE_PVG", 
                "MALE_BRAVE_PVG", 
                "MALE_COWARDLY_PVG", 
                "MALE_GANG_PVG"
            };

        /// <summary>
        /// The silent voices.
        /// </summary>
        private static readonly string[] SilentVoices = new[]
            {
                "SILENT_PVG"
            };

        /// <summary>
        /// The silent voices.
        /// </summary>
        private static readonly string[] OtherSilentVoices = new[]
            {
                "SILENT_CUTSCENE_PVG",
                "SILENT_MP_PVG"
            };

        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private IAssetManager assetManager;

        /// <summary>
        /// The ped game data path.
        /// </summary>
        private string pedGameDataDepotPath;

        /// <summary>
        /// The peds with placeholder voices.
        /// </summary>
        private IList<string> pedsWithPlaceholderVoices;

        /// <summary>
        /// The silent peds.
        /// </summary>
        private IList<string> silentPeds;

        /// <summary>
        /// The other silent peds.
        /// </summary>
        private IList<string> otherSilentPeds; 

        /// <summary>
        /// The get report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;

                    case "PedGameDataDepotPath":
                        this.pedGameDataDepotPath = setting.InnerText;
                        break;
                }
            }

            // Get instance of asset manager
            this.assetManager = AssetManagerFactory.GetInstance(AssetManagerType.Perforce);

            // Validate args
            if (string.IsNullOrEmpty(this.name))
            {
                throw new Exception("Report name required");
            }

            if (string.IsNullOrEmpty(this.pedGameDataDepotPath))
            {
                throw new Exception("Ped game data depot path required");
            }

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes (not required).
        /// </param>
        /// <param name="browsers">
        /// The sound browsers (not required).
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            // Load data
            this.LoadPedGameData();

            // Generate and display results
            this.DisplayResults();
        }

        /// <summary>
        /// Load the ped game data.
        /// </summary>
        private void LoadPedGameData()
        {
            this.pedsWithPlaceholderVoices = new List<string>();
            this.silentPeds = new List<string>();
            this.otherSilentPeds = new List<string>();

            var localPath = this.assetManager.GetLocalPath(this.pedGameDataDepotPath);

            if (string.IsNullOrEmpty(localPath))
            {
                throw new Exception("Failed to get local path for game file: " + this.pedGameDataDepotPath);
            }

            // Load the XML file
            var pedDoc = XDocument.Load(localPath);

            var modelInfoElem = pedDoc.Element("CPedModelInfo__InitDataList");
            var dataElem = modelInfoElem.Element("InitDatas");
            var pedElems = dataElem.Elements("Item");

            foreach (var pedElem in pedElems)
            {
                var pvgElem = pedElem.Element("PedVoiceGroup");

                if (null == pvgElem)
                {
                    continue;
                }

                var pvgName = pvgElem.Value.ToUpper();
                var pedNameElem = pedElem.Element("Name");

                if (null == pedNameElem)
                {
                    throw new Exception("Ped name element not found in game data");
                }

                var pedName = pedNameElem.Value;

                if (PlaceholderVoices.Any(voice => voice.Equals(pvgName)))
                {
                    this.pedsWithPlaceholderVoices.Add(pedName);
                }
                else if (SilentVoices.Any(voice => voice.Equals(pvgName)))
                {
                    this.silentPeds.Add(pedName);
                }
                else if (OtherSilentVoices.Any(voice => voice.Equals(pvgName)))
                {
                    this.otherSilentPeds.Add(pedName);
                }
            }
        }

        /// <summary>
        /// Display the results to the user.
        /// </summary>
        private void DisplayResults()
        {
            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Ped Placeholder Voice Report</title></head><body>");

            // Output peds with placeholder voices
            sw.WriteLine("<h3>Peds with Placeholder Voices</h3>");
            sw.Write("<p><b>Peds matching PVGs: ");
            sw.WriteLine(string.Join(", ", PlaceholderVoices));
            sw.WriteLine("</b></p>");

            ((List<string>)this.pedsWithPlaceholderVoices).Sort();

            if (!this.pedsWithPlaceholderVoices.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<p>Count: " + this.pedsWithPlaceholderVoices.Count + "</p>");
                sw.WriteLine("<ul>");
                foreach (var voice in this.pedsWithPlaceholderVoices)
                {
                    sw.WriteLine("<li>" + voice + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            // Output peds with silent voices
            sw.WriteLine("<h3>Peds with Silent Voices</h3>");
            sw.Write("<p><b>Peds matching PVGs: ");
            sw.WriteLine(string.Join(", ", SilentVoices));
            sw.WriteLine("</b></p>");

            ((List<string>)this.silentPeds).Sort();

            if (!this.silentPeds.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<p>Count: " + this.silentPeds.Count + "</p>");
                sw.WriteLine("<ul>");
                foreach (var voice in this.silentPeds)
                {
                    sw.WriteLine("<li>" + voice + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            // Output peds with other silent voices
            sw.WriteLine("<h3>Peds with Other Silent Voices</h3>");
            sw.Write("<p><b>Peds matching PVGs: ");
            sw.WriteLine(string.Join(", ", OtherSilentVoices));
            sw.WriteLine("</b></p>");

            ((List<string>)this.otherSilentPeds).Sort();

            if (!this.otherSilentPeds.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<p>Count: " + this.otherSilentPeds.Count + "</p>");
                sw.WriteLine("<ul>");
                foreach (var voice in this.otherSilentPeds)
                {
                    sw.WriteLine("<li>" + voice + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            sw.WriteLine("</body></html>");

            sw.Close();
            filesteam.Close();

            // Display report
            var report = new frmReport(this.name + " Report");
            report.SetURL(resultsFileName);
            report.Show();
        }
    }
}
