﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReaperSpeechExport.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The reaper speech export.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.ReaperSpeechExport
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;
    using System.Xml;

    using rage;
    using rage.ToolLib.CmdLine;

    using Rave.Instance;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.Utils.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.UserActions;

    using Rockstar.AssetManager.Interfaces;

    using Wavelib;

    /// <summary>
    /// The reaper speech export.
    /// </summary>
    public class ReaperSpeechExport : IRAVESpeechSearchPlugin, IRAVEWaveBrowserPlugin
    {
        #region Constants

        /// <summary>
        /// The lipsync extension.
        /// </summary>
        private const string LipsyncExtension = ".lipsync";

        /// <summary>
        /// The xarb.
        /// </summary>
        private const string Xarb = "XARB";

        #endregion

        #region Fields

        /// <summary>
        /// The assets.
        /// </summary>
        private IList<IAsset> assets;

        /// <summary>
        /// The existing processed waves.
        /// </summary>
        private Dictionary<string, string> existingProcessedWaves;

        /// <summary>
        /// The is x64 flag.
        /// </summary>
        private bool isX64;

        /// <summary>
        /// The name.
        /// </summary>
        private string name;

        /// <summary>
        /// The reaper is executing flag.
        /// </summary>
        private bool reaperIsExecuting;

        /// <summary>
        /// The reaper path.
        /// </summary>
        private string reaperPath;

        /// <summary>
        /// The remove visemes flag.
        /// </summary>
        private bool removeVisemes;

        /// <summary>
        /// The skip custom metadata flag.
        /// </summary>
        private bool skipCustomMetadata;

        /// <summary>
        /// The checked out waves.
        /// </summary>
        private IList<string> checkedOutWaves;

        /// <summary>
        /// The checked out waves with lipsync data.
        /// </summary>
        private IList<string> checkedOutWavesWithLipsyncData;

        /// <summary>
        /// The selected wave nodes.
        /// </summary>
        private IList<WaveNode> selectedWaveNodes;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Get the plugin name.
        /// </summary>
        /// <returns>
        /// The plugin name <see cref="string" />.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        /// Initialize the report speech export plugin.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            foreach (XmlNode child in settings.ChildNodes)
            {
                switch (child.Name)
                {
                    case "Name":
                        {
                            this.name = child.InnerText;
                            break;
                        }

                    case "x86":
                        {
                            if (File.Exists(child.InnerText))
                            {
                                this.reaperPath = child.InnerText;
                            }

                            break;
                        }

                    case "x64":
                        {
                            if (File.Exists(child.InnerText))
                            {
                                this.reaperPath = child.InnerText;
                                this.isX64 = true;
                            }

                            break;
                        }

                    case "SkipCustomMetadata":
                        {
                            this.skipCustomMetadata = bool.Parse(child.InnerText);
                            break;
                        }

                    case "RemoveVisemes":
                        {
                            this.removeVisemes = bool.Parse(child.InnerText);
                            break;
                        }
                }
            }

            return !string.IsNullOrEmpty(this.name) && !string.IsNullOrEmpty(this.reaperPath);
        }

        /// <summary>
        /// Process a wave in Reaper.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        public void Process(IWaveBrowser waveBrowser, IActionLog actionLog, ArrayList nodes, IBusy busyFrm)
        {
            this.assets = new List<IAsset>();

            if (this.reaperIsExecuting || System.Diagnostics.Process.GetProcessesByName("reaper").Length != 0)
            {
                ErrorManager.HandleInfo("Reaper is currently executing - Please close Reaper before using this plugin");
                return;
            }

            // Check for any non-reaper related pending actions
            if (null != RaveInstance.RaveAssetManager.WaveChangeList
                && !RaveInstance.RaveAssetManager.WaveChangeList.Description.Equals("[Rave] - Reaper Wave Changes"))
            {
                ErrorManager.HandleInfo(
                    "Please commit pending non-Reaper related wave changes before using this plugin");
                return;
            }

            try
            {
                // Refresh list of existing processed waves
                this.existingProcessedWaves = new Dictionary<string, string>();

                var packNames = new HashSet<string>();

                // process each selected node in the wave browser
                var editorTreeNodes = new List<WaveNode>();
                foreach (EditorTreeNode node in nodes)
                {
                    var pack = EditorTreeNode.FindParentPackNode(node).GetObjectName();
                    packNames.Add(pack);

                    var editorTreeView = waveBrowser.GetEditorTreeView();
                    editorTreeView.GetSelectedWaveNodes(node, this.skipCustomMetadata, editorTreeNodes);
                }

                this.selectedWaveNodes = new List<WaveNode>();

                // Remove .PROCESSED waves from the list
                foreach (var node in editorTreeNodes)
                {
                    var nodeName = node.GetWaveName().ToUpper();
                    if (nodeName.EndsWith(".PROCESSED"))
                    {
                        continue;
                    }

                    this.selectedWaveNodes.Add(node);
                }

                if (editorTreeNodes.Count == 0)
                {
                    return;
                }

                // Get any pending wave files associated with bank(s) we're using
                var projectSettings = Configuration.ProjectSettings;
                var pendingWaveFiles = new HashSet<string>();

                foreach (PlatformSetting platform in projectSettings.GetPlatformSettings())
                {
                    foreach (var packName in packNames)
                    {
                        var packFileName = packName + ".XML";
                        var pendingWavesPath = Path.Combine(platform.BuildInfo, "PendingWaves");
                        var pendingWaveFile = Path.Combine(pendingWavesPath, packFileName);
                        var depotPath = RaveInstance.AssetManager.GetDepotPath(pendingWaveFile);

                        pendingWaveFiles.Add(depotPath);
                    }
                }

                // Check that there aren't any pending wave lists in a pending change list
                foreach (var pendingWaveFile in pendingWaveFiles)
                {
                    if (RaveInstance.AssetManager.ExistsInPendingChangeList(pendingWaveFile))
                    {
                        var fileName = Path.GetFileNameWithoutExtension(pendingWaveFile);
                        ErrorManager.HandleInfo(
                            "Pending wave list for " + fileName
                            + " pack must be checked in/reverted before using this plugin");
                        return;
                    }
                }

                this.checkedOutWaves = new List<string>();
                this.checkedOutWavesWithLipsyncData = new List<string>();

                // Initialize change list
                if (!this.InitChangeList())
                {
                    return;
                }

                // checkout waves
                var checkoutWavesResult = this.CheckOutWaves(busyFrm);

                if (!checkoutWavesResult || this.checkedOutWaves.Count == 0)
                {
                    return;
                }

                var raveReaperWavePath = Path.Combine(Path.GetTempPath(), "RaveReaperSourceWaves\\");
                if (Directory.Exists(raveReaperWavePath))
                {
                    Directory.Delete(raveReaperWavePath, true);
                }

                var raveReaperDestWavePath = Path.Combine(Path.GetTempPath(), "RaveReaperDestWaves\\");
                if (Directory.Exists(raveReaperDestWavePath))
                {
                    Directory.Delete(raveReaperDestWavePath, true);
                }

                Directory.CreateDirectory(raveReaperDestWavePath);

                var reaperProject = new ReaperSpeechProject(this.checkedOutWaves);
                foreach (var checkedOutWave in this.checkedOutWaves)
                {
                    var destination = checkedOutWave.Replace(reaperProject.RenderPath, raveReaperWavePath);
                    var destinationDir = Path.GetDirectoryName(destination);
                    if (!Directory.Exists(destinationDir))
                    {
                        Directory.CreateDirectory(destinationDir);
                    }

                    File.Copy(checkedOutWave, destination);
                }

                var reaperProjectFileName = reaperProject.Generate(this.isX64, raveReaperWavePath);
                if (string.IsNullOrEmpty(reaperProjectFileName))
                {
                    ErrorManager.HandleInfo("Failed to generate Reaper project file...");
                    this.RevertWaves(busyFrm);
                    return;
                }

                this.ExecuteReaper(busyFrm, reaperProjectFileName);

                if (
                    ErrorManager.HandleQuestion(
                        "Reaper has finished executing. Would you like to keep your pending changes? "
                        + "These must be submitted manually in RAVE. Select 'No' to revert your changes.", 
                        MessageBoxButtons.YesNo, 
                        DialogResult.Yes) == DialogResult.Yes)
                {
                    this.AddPendingChanges(
                        waveBrowser, busyFrm, this.selectedWaveNodes, this.checkedOutWaves, this.checkedOutWavesWithLipsyncData);
                }
                else
                {
                    this.RevertWaves(busyFrm);
                }
            }
            finally
            {
                busyFrm.Close();
            }
        }

        /// <summary>
        /// Override ToString to return plugin name.
        /// </summary>
        /// <returns>
        /// The plugin name <see cref="string" />.
        /// </returns>
        public override string ToString()
        {
            return this.GetName();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Delete the processed waves.
        /// </summary>
        /// <param name="waveNodes">
        /// The wave nodes.
        /// </param>
        private static void DeleteProcessedWaves(IEnumerable<WaveNode> waveNodes)
        {
            foreach (var waveNode in waveNodes)
            {
                var waveAssetPath = Configuration.PlatformWavePath + waveNode.GetObjectPath();
                waveAssetPath = waveAssetPath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                waveAssetPath = waveAssetPath.ToUpper();

                var processedWaveAssetPath = waveAssetPath.Replace(".WAV", ".PROCESSED.WAV");

                if (File.Exists(processedWaveAssetPath))
                {
                    File.Delete(processedWaveAssetPath);
                }
            }
        }

        /// <summary>
        /// Find a processed wave node.
        /// </summary>
        /// <param name="waveNode">
        /// The wave node.
        /// </param>
        /// <returns>
        /// The processed wave node <see cref="EditorTreeNode"/>.
        /// </returns>
        private static EditorTreeNode FindProcessedWaveNode(WaveNode waveNode)
        {
            var processedWaveName = waveNode.GetObjectName().Replace(".WAV", ".PROCESSED.WAV");
            var parentNode = waveNode.Parent;

            return null != parentNode
                       ? parentNode.Nodes.Cast<EditorTreeNode>()
                                       .FirstOrDefault(wnode => wnode.GetObjectName() == processedWaveName)
                       : null;
        }

        /// <summary>
        /// Add pending changes.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        /// <param name="selectedWaveNodes">
        /// The selected wave nodes.
        /// </param>
        /// <param name="checkedOutWaves">
        /// The checked out waves.
        /// </param>
        /// <param name="checkOutWavesWithLipsyncData">
        /// The check out waves with lipsync data.
        /// </param>
        private void AddPendingChanges(
            IWaveBrowser waveBrowser,
            IBusy busyFrm, 
            IList<WaveNode> selectedWaveNodes, 
            IEnumerable<string> checkedOutWaves, 
            ICollection<string> checkOutWavesWithLipsyncData)
        {
            busyFrm.SetUnknownDuration(true);
            busyFrm.SetStatusText("Adding pending wave changes...");

            try
            {
                foreach (var wavePath in checkedOutWaves)
                {
                    if (!checkOutWavesWithLipsyncData.Contains(wavePath))
                    {
                        continue;
                    }

                    // restore XARB chunk
                    byte[] lipsyncData;
                    using (var lipsyncReader = new BinaryReader(new FileStream(wavePath + LipsyncExtension, FileMode.Open)))
                    {
                        lipsyncData = lipsyncReader.ReadBytes((int)lipsyncReader.BaseStream.Length);
                    }

                    var waveFile = new bwWaveFile(wavePath);
                    var lipsyncChunk = new bwRaveChunk(Xarb, lipsyncData);
                    waveFile.AddRaveChunk(lipsyncChunk);
                    waveFile.Save();
                }

                IUserAction dummyAction = waveBrowser.CreateAction(ActionType.Dummy, null);
                if (dummyAction.Action())
                {
                    waveBrowser.GetActionLog().AddAction(dummyAction);
                }

                foreach (var waveNode in selectedWaveNodes)
                {
                    var parentNode = (EditorTreeNode)waveNode.Parent;
                    var packNode = EditorTreeNode.FindParentPackNode(waveNode);
                    var waveNodeName = waveNode.GetObjectName();
                    var waveNodePath = waveNode.GetObjectPath();

                    var values = waveNode.GetPlatforms().ToDictionary(platform => platform, platform => string.Empty);

                    var noBuildParams = new ActionParams(
                        waveBrowser, 
                        waveNode, 
                        null, 
                        null, 
                        null, 
                        null, 
                        "DoNotBuild", 
                        waveNode.GetPlatforms(), 
                        values, 
                        null);

                    var noBuildAction = waveBrowser.CreateAction(ActionType.AddTag, noBuildParams);
                    if (noBuildAction.Action())
                    {
                        waveBrowser.GetActionLog().AddAction(noBuildAction);
                    }

                    // Find bank folder node
                    var bankFolderNode = EditorTreeNode.FindParentBankNode(waveNode);

                    // Determine if a processed wave currently exists
                    var waveExists = false;
                    var procWaveName = waveNodeName.Replace(".WAV", ".PROCESSED.WAV");
                    var procWaveNameUpper = procWaveName.ToUpper();
                    var waves = bankFolderNode.GetChildWaveNodes();

                    foreach (WaveNode wn in waves)
                    {
                        if (wn.Text.ToUpper() == procWaveNameUpper)
                        {
                            waveExists = true;
                        }
                    }

                    var wavePath = waveNode.GetObjectPath();
                    wavePath = wavePath.Replace(".WAV", ".PROCESSED.WAV");

                    // Check full path
                    var raveReaperWavePath = Path.Combine(Path.GetTempPath(), "RaveReaperDestWaves", wavePath);

                    // This is fairly hacky and V specific, so....yeah
                    var subPath = string.Format("{0}\\SCRIPTED_SPEECH\\", packNode.PackName);
                    var missionAndActorPaths = parentNode.GetObjectPath().ToUpper().Replace(subPath, string.Empty);

                    // Check for multiple missions
                    if (!File.Exists(raveReaperWavePath))
                    {
                        raveReaperWavePath = Path.Combine(
                            Path.GetTempPath(), "RaveReaperDestWaves", missionAndActorPaths, procWaveName);
                    }

                    // Try same thing but with bank name removed from path
                    if (!File.Exists(raveReaperWavePath) && missionAndActorPaths.Contains("\\"))
                    {
                        var trimmedPath = missionAndActorPaths.Substring(missionAndActorPaths.IndexOf('\\') + 1);
                        raveReaperWavePath = Path.Combine(
                            Path.GetTempPath(), "RaveReaperDestWaves", trimmedPath, procWaveName);
                    }

                    // Check for multiple actors, same mission
                    if (!File.Exists(raveReaperWavePath))
                    {
                        raveReaperWavePath = Path.Combine(
                            Path.GetTempPath(), "RaveReaperDestWaves", bankFolderNode.GetObjectName(), procWaveName);
                    }

                    // Check for single actor, single mission
                    if (!File.Exists(raveReaperWavePath))
                    {
                        raveReaperWavePath = Path.Combine(Path.GetTempPath(), "RaveReaperDestWaves", procWaveName);
                    }

                    if (!File.Exists(raveReaperWavePath))
                    {
                        throw new Exception("Failed to find processed wave: " + raveReaperWavePath);
                    }

                    // Determine action type (edit or add)
                    var actionType = waveExists ? ActionType.EditWave : ActionType.AddWave;

                    if (waveExists)
                    {
                        // Copy newly created wave to replace existing wave
                        actionType = ActionType.EditWave;

                        string originalProcessedWavePath;
                        this.existingProcessedWaves.TryGetValue(waveNodePath, out originalProcessedWavePath);
                        if (null != originalProcessedWavePath)
                        {
                            File.Copy(raveReaperWavePath, originalProcessedWavePath, true);
                            File.SetAttributes(originalProcessedWavePath, FileAttributes.Normal);
                        }
                        else
                        {
                            ErrorManager.HandleError(
                                "Failed to lookup path for existing processed wave: " + waveNodeName);
                        }
                    }

                    var actionParams = new ActionParams(
                        waveBrowser,
                        parentNode, 
                        null, 
                        raveReaperWavePath, 
                        null, 
                        null, 
                        null, 
                        waveNode.GetPlatforms(), 
                        null, 
                        null);

                    var addAction = waveBrowser.CreateAction(actionType, actionParams);
                    if (addAction.Action())
                    {
                        waveBrowser.GetActionLog().AddAction(addAction);
                    }

                    // find new wave node
                    var newWaveNode = FindProcessedWaveNode(waveNode);

                    var node = waveNode;
                    values = waveNode.GetPlatforms()
                                     .ToDictionary(platform => platform, platform => node.GetObjectName());

                    var renameActionParams = new ActionParams(
                        waveBrowser, 
                        newWaveNode, 
                        null, 
                        raveReaperWavePath, 
                        null, 
                        null, 
                        "rename", 
                        waveNode.GetPlatforms(), 
                        values, 
                        null);
                    var renameAction = waveBrowser.CreateAction(ActionType.AddTag, renameActionParams);
                    if (renameAction.Action())
                    {
                        waveBrowser.GetActionLog().AddAction(renameAction);
                    }
                }
            }
            catch (Exception e)
            {
                ErrorManager.HandleError("Failed to add pending changes: " + e.Message);

                if (RaveInstance.RaveAssetManager.WaveChangeList != null)
                {
                    this.RevertWaves(busyFrm);
                }

                DeleteProcessedWaves(selectedWaveNodes);
            }
        }

        /// <summary>
        /// Initialize the change list.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool InitChangeList()
        {
            // create wave change list if not currently initialised
            if (RaveInstance.RaveAssetManager.WaveChangeList == null)
            {
                try
                {
                    RaveInstance.RaveAssetManager.WaveChangeList =
                        RaveInstance.AssetManager.CreateChangeList("[Rave] - Reaper Wave Changes");
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check-out the waves.
        /// </summary>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool CheckOutWaves(IBusy busyFrm)
        {
            // update user
            busyFrm.SetProgress(0);
            busyFrm.SetStatusText("Checking out Waves");
            busyFrm.Show();

            var errors = new StringBuilder();
            for (var i = 0; i < this.selectedWaveNodes.Count; ++i)
            {
                busyFrm.SetProgress((int)((i / (float)this.selectedWaveNodes.Count) * 100.0f));
                var waveNode = this.selectedWaveNodes[i];

                if (!this.CheckOutWave(waveNode, busyFrm, errors))
                {
                    return false;
                }

                Application.DoEvents();
            }

            busyFrm.Hide();

            if (errors.Length != 0)
            {
                if (
                    ErrorManager.HandleQuestion(
                        string.Format(
                            "Rave Encountered the following errors during check out. Do you wish to continue?{0}{0}{1}", 
                            Environment.NewLine, 
                            errors), 
                        MessageBoxButtons.YesNo, 
                        DialogResult.No) == DialogResult.No)
                {
                    this.RevertWaves(busyFrm);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check out wave.
        /// </summary>
        /// <param name="waveNode">
        /// The wave node.
        /// </param>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool CheckOutWave(WaveNode waveNode, IBusy busyFrm, StringBuilder errors)
        {
            // Determine wave path
            var waveObjectPath = waveNode.GetObjectPath();
            var waveAssetPath = Path.Combine(Configuration.PlatformWavePath, waveObjectPath);
            waveAssetPath = waveAssetPath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            waveAssetPath = waveAssetPath.ToUpper();

            // Determine processed wave path
            var processedWaveAssetPath = waveAssetPath.Replace(".WAV", ".PROCESSED.WAV");

            busyFrm.SetStatusText("Checking out " + waveObjectPath);

            try
            {
                // Ensure we have latest version of wave and processed wave file (if it exists)
                RaveInstance.AssetManager.GetLatest(waveAssetPath, true);

                if (RaveInstance.AssetManager.ExistsAsAsset(processedWaveAssetPath, true))
                {
                    RaveInstance.AssetManager.GetLatest(processedWaveAssetPath, true);
                }

                // Checkout/mark for add the processed wave path
                IAsset asset;
                if (RaveInstance.AssetManager.ExistsAsAsset(processedWaveAssetPath))
                {
                    asset = RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(processedWaveAssetPath, true);

                    File.SetAttributes(processedWaveAssetPath, FileAttributes.Normal);
                    this.existingProcessedWaves.Add(waveNode.GetObjectPath(), processedWaveAssetPath);
                }
                else
                {
                    asset = RaveInstance.RaveAssetManager.WaveChangeList.MarkAssetForAdd(processedWaveAssetPath);
                }

                // Reset processed wave to initially be same as original wave
                File.Delete(processedWaveAssetPath);
                File.Copy(waveAssetPath, processedWaveAssetPath, true);
                File.SetAttributes(processedWaveAssetPath, FileAttributes.Normal);

                this.checkedOutWaves.Add(processedWaveAssetPath);

                // strip off XARB etc chunks since sound forge etc don't like them
                var waveFile = new bwWaveFile(processedWaveAssetPath, false);

                if (!this.removeVisemes)
                {
                    var xarbChunk = waveFile.GetChunk(Xarb);
                    if (xarbChunk != null)
                    {
                        var lipsyncChunk = xarbChunk as bwRaveChunk;
                        if (lipsyncChunk != null)
                        {
                            using (
                                var lipsyncWriter =
                                    new BinaryWriter(
                                        new FileStream(
                                            processedWaveAssetPath + LipsyncExtension, FileMode.CreateNew)))
                            {
                                this.checkedOutWavesWithLipsyncData.Add(processedWaveAssetPath);
                                lipsyncWriter.Write(lipsyncChunk.Bytes);
                                lipsyncWriter.Flush();
                            }
                        }
                    }
                }

                waveFile.RemoveRaveChunks();
                waveFile.Save();

                if (null != asset)
                {
                    this.assets.Add(asset);
                }
            }
            catch (Exception e)
            {
                errors.AppendLine(e.Message);
            }

            return true;
        }

        /// <summary>
        /// Execute the Reaper application.
        /// </summary>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        /// <param name="projectFileName">
        /// The project file name.
        /// </param>
        private void ExecuteReaper(IBusy busyFrm, string projectFileName)
        {
            busyFrm.SetUnknownDuration(true);
            busyFrm.SetStatusText("Waiting for Reaper to terminate...");
            busyFrm.Show();

            var commandExecutor = new NoOutputAsyncCommandExecutor();
            commandExecutor.Finished += this.OnReaperExecutionFinished;

            this.reaperIsExecuting = true;
            commandExecutor.Execute(this.reaperPath, string.Format("{0} -close:nosave", projectFileName));

            while (this.reaperIsExecuting)
            {
                Application.DoEvents();
                Thread.Sleep(20);
            }

            commandExecutor.Finished -= this.OnReaperExecutionFinished;
            busyFrm.Hide();
        }

        /// <summary>
        /// Change status of Reaper execution process to indicate it has finished.
        /// </summary>
        /// <param name="success">
        /// The success flag.
        /// </param>
        private void OnReaperExecutionFinished(bool success)
        {
            this.reaperIsExecuting = false;
        }

        /// <summary>
        /// Revert the wave files.
        /// </summary>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        private void RevertWaves(IBusy busyFrm)
        {
            busyFrm.SetStatusText("Reverting pending wave changes...");
            busyFrm.SetUnknownDuration(true);
            busyFrm.Show();
            Application.DoEvents();

            try
            {
                foreach (var asset in this.assets)
                {
                    asset.Revert();
                }

                if (null != RaveInstance.RaveAssetManager.WaveChangeList && !RaveInstance.RaveAssetManager.WaveChangeList.Assets.Any())
                {
                    RaveInstance.RaveAssetManager.WaveChangeList.Delete();
                    RaveInstance.RaveAssetManager.WaveChangeList = null;
                }
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
            }

            busyFrm.Hide();
        }

        #endregion
    }
}