﻿namespace Rave.ReaperSpeechExport
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    using rage.ToolLib.DataStructures;

    using Rave.Utils;

    using Wavelib;

    public class ReaperSpeechProject
    {
        private readonly Dictionary<string, KeyValuePair<double, double>> m_waveInfoLookup;

        public ReaperSpeechProject(IEnumerable<string> checkedOutWaves)
        {
            if (checkedOutWaves == null)
            {
                throw new ArgumentNullException("checkedOutWaves");
            }

            var basePath =
                Configuration.PlatformWavePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar).
                    ToUpper();
            RenderPath =
                ComputeRenderPath(
                    checkedOutWaves.Select(x => Path.GetDirectoryName(x.Replace(basePath, string.Empty))),
                    new Tree<string> {Root = new TreeNode<string>(null) {Data = basePath}});
            m_waveInfoLookup = BuildWaveInfoLookup(checkedOutWaves);
        }

        public string RenderPath { get; private set; }

        private static Dictionary<string, KeyValuePair<double, double>> BuildWaveInfoLookup(
            IEnumerable<string> checkedOutWaves)
        {
            var waveInfoLookup = new Dictionary<string, KeyValuePair<double, double>>();
            double position = 0;
            foreach (var checkedOutWave in checkedOutWaves)
            {
                var waveFile = new bwWaveFile(checkedOutWave, false);
                var length = waveFile.Data.NumSamples /
                             (double) (waveFile.Format.SampleRate * waveFile.Format.NumChannels);
                waveInfoLookup.Add(checkedOutWave, new KeyValuePair<double, double>(position, length));
                position += (length + 1);
            }
            return waveInfoLookup;
        }

        private static string ComputeRenderPath(IEnumerable<string> paths, Tree<string> directoryTree)
        {
            foreach (var path in paths)
            {
                var components = path.Split(Path.DirectorySeparatorChar);
                var node = directoryTree.Root;
                foreach (var component in components)
                {
                    var componentNode = node.Children.Where(x => x.Data == component).FirstOrDefault();
                    if (componentNode == null)
                    {
                        componentNode = new TreeNode<string>(node) {Data = component};
                        node.Children.Add(componentNode);
                    }
                    node = componentNode;
                }
            }

            var treeNode = directoryTree.Root;
            var renderPathBuilder = new StringBuilder(treeNode.Data);
            while (treeNode.Children.Count == 1)
            {
                renderPathBuilder.Append(treeNode.Children[0].Data);
                renderPathBuilder.Append(Path.DirectorySeparatorChar);
                treeNode = treeNode.Children[0];
            }
            return renderPathBuilder.ToString();
        }

        public string Generate(bool isX64, string sourcePath)
        {
            var projectFileName = string.Format("{0}RaveReaperExport.rpp", Path.GetTempPath());

            try
            {
                using (var writer = new StreamWriter(projectFileName, false, Encoding.ASCII))
                {
                    writer.WriteLine(isX64 ? "<REAPER_PROJECT 0.1 \"4.13/x64\"" : "<REAPER_PROJECT 0.1 \"4.13\"");
                    writer.WriteLine(string.Format("\tRENDER_FILE \"{0}\"", String.Concat(Path.GetTempPath(), "RaveReaperDestWaves") ));
                    writer.WriteLine("\tRENDER_PATTERN \"$region\"");
                    writer.WriteLine("\tRENDER_RANGE 3 0.00000000000000 0.00000000000000");

                    int regionIndex = 0;
                    foreach (var entry in m_waveInfoLookup)
                    {
                        var regionName = entry.Key.Replace(RenderPath, string.Empty).Replace(".WAV", string.Empty);
                        writer.WriteLine(string.Format("\tMARKER {0} {1} \"{2}\" 3 0", regionIndex, entry.Value.Key, regionName));
                        writer.WriteLine(string.Format("\tMARKER {0} {1} \"{2}\" 3 0", regionIndex, entry.Value.Key + entry.Value.Value, regionName));
                        regionIndex += 1;
                    }

                    writer.WriteLine("\t<TRACK");
                    {
                        writer.WriteLine("\t\tNAME \"RAVE_TRACK\"");

                        foreach (var entry in m_waveInfoLookup)
                        {
                            writer.WriteLine("\t\t<ITEM");
                            {
                                writer.WriteLine(string.Format("\t\t\tNAME \"{0}\"", Path.GetFileName(entry.Key)));
                                writer.WriteLine(string.Format("\t\t\tPOSITION {0}", entry.Value.Key));
                                writer.WriteLine(string.Format("\t\t\tLENGTH {0}", entry.Value.Value));
                                
                                writer.WriteLine("\t\t\t<SOURCE WAVE");
                                {
                                    writer.WriteLine(string.Format("\t\t\t\tFILE \"{0}\"", entry.Key.Replace(RenderPath, sourcePath)));
                                }
                                writer.WriteLine("\t\t\t>");
                            }
                            writer.WriteLine("\t\t>");
                        }
                    }
                    writer.WriteLine("\t>");
                    writer.WriteLine(">");
                    writer.Flush();
                }
            }
            catch
            {
                projectFileName = null;
            }

            return projectFileName;
        }
    }
}