﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rave.UserPreferences
{
	static class Helpers
	{

		public static string GetPrefsPath()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
			stringBuilder.Append("\\Rave\\");
			string path = stringBuilder.ToString();

			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}

			stringBuilder.Append("UserPrefs.xml");
			path = stringBuilder.ToString();

			return path;
		}
	}
}
