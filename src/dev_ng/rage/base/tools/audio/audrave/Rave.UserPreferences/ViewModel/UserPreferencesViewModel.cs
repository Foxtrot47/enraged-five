﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Rave.UserPreferences.Model;
using WPFToolLib.Extensions;

namespace Rave.UserPreferences.ViewModel
{
	public class UserPreferencesViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		private UserPreferencesModel _model;
		public bool CategoriesCollapsed
		{
			get { return _model.CategoriesCollapsed; }
			set
			{
				_model.CategoriesCollapsed = value;
				PropertyChanged.Raise(this, "CategoriesCollapsed");
			}
		} 
		
			
		public UserPreferencesViewModel()
		{
			_model = new UserPreferencesModel();
			LoadUserPrefs();
			PropertyChanged += OnPropertyChanged;
		}

		private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			SaveUserPrefs();
		}

		private void SaveUserPrefs()
		{
			using (StreamWriter fileStream = new StreamWriter(Helpers.GetPrefsPath()))
			{
				UserPreferencesModel userPreferencesModel = _model;
				XmlSerializer serializer = new XmlSerializer(typeof(UserPreferencesModel));
				serializer.Serialize(fileStream, userPreferencesModel);
			}
		}

		private void LoadUserPrefs()
		{
			if (!File.Exists(Helpers.GetPrefsPath()))
			{
				SaveUserPrefs();
			}
			using (StreamReader fileStream = new StreamReader(Helpers.GetPrefsPath()))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(UserPreferencesModel));
				_model = (UserPreferencesModel)serializer.Deserialize(fileStream);
			}
			
			//Raise all property changed https://msdn.microsoft.com/en-us/library/system.componentmodel.inotifypropertychanged.propertychanged.aspx
			PropertyChanged.Raise(this, string.Empty);
		}

		
	}
}
