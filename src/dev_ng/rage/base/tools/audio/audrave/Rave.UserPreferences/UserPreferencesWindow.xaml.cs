﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Rave.UserPreferences.ViewModel;

namespace Rave.UserPreferences
{
	/// <summary>
	/// Interaction logic for UserPreferencesWindow.xaml
	/// </summary>
	public partial class UserPreferencesWindow : Window
	{
		public UserPreferencesWindow()
		{
			InitializeComponent();
			DataContext = UserPreferences.Instance;
		}

		private void CloseButtonClick(object sender, RoutedEventArgs e)
		{
			this.Close();
		}
	}
}
