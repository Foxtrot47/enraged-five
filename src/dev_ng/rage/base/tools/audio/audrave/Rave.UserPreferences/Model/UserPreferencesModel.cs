﻿using System;

namespace Rave.UserPreferences.Model
{
	[Serializable]
	public class UserPreferencesModel
	{
		public bool CategoriesCollapsed = false;
	}

}
