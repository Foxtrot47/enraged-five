﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Rave.UserPreferences.Model;
using Rave.UserPreferences.ViewModel;

namespace Rave.UserPreferences
{
	public class UserPreferences : UserPreferencesViewModel
	{

		private static UserPreferences _userPreferences;

		public static UserPreferences Instance
		{
			get { return _userPreferences ?? (_userPreferences = new UserPreferences()); }

			internal set { _userPreferences = value; }
		}

	}
}
