﻿// -----------------------------------------------------------------------
// <copyright file="CollisionCategory.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.CollisionCategory
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.Instance;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;
    using global::Rave.Plugins.Infrastructure.Interfaces;
    using global::Rave.Types.Infrastructure.Interfaces.Objects;
    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    /// Collision category report.
    /// </summary>
    public class CollisionCategory : IRAVEReportPlugin
    {
        /// <summary>
        /// The collisions category.
        /// </summary>
        private const string CollisionsCategory = "COLLISIONS";

        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// The field definition names and fields we're interested in.
        /// </summary>
        private IDictionary<string, IList<string>> fieldDefNames;

        /// <summary>
        /// The sound objects.
        /// </summary>
        private IList<KeyValuePair<IObjectInstance, string>> soundObjects;

        /// <summary>
        /// The non collisions category sounds.
        /// </summary>
        private HashSet<KeyValuePair<IObjectInstance, string>> nonCollisionsCategorySounds; 

        /// <summary>
        /// The get name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;
                }
            }

            this.fieldDefNames = new Dictionary<string, IList<string>>();

            this.fieldDefNames["TrailerAudioSettings"] = new List<string>();
            this.fieldDefNames["TrailerAudioSettings"].Add("BumpSound");

            this.fieldDefNames["CollisionMaterialSettings"] = new List<string>();
            this.fieldDefNames["CollisionMaterialSettings"].Add("HardImpact");
            this.fieldDefNames["CollisionMaterialSettings"].Add("SolidImpact");
            this.fieldDefNames["CollisionMaterialSettings"].Add("SoftImpact");
            this.fieldDefNames["CollisionMaterialSettings"].Add("ScrapeSound");
            this.fieldDefNames["CollisionMaterialSettings"].Add("BreakSound");

            this.fieldDefNames["ModelAudioCollisionSettings"] = new List<string>();
            this.fieldDefNames["ModelAudioCollisionSettings"].Add("BreakSound");
            this.fieldDefNames["ModelAudioCollisionSettings"].Add("UprootSound");
            this.fieldDefNames["ModelAudioCollisionSettings"].Add("EntityResonance");

            this.fieldDefNames["VehicleCollisionSettings"] = new List<string>();
            this.fieldDefNames["VehicleCollisionSettings"].Add("ImpactDebris");

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes.
        /// </param>
        /// <param name="browsers">
        /// The sound browsers.
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            if (!this.InitSoundsList())
            {
                throw new Exception("Failed to initialize list of sound objects");
            }

            if (!this.InitInvalidCategoriesList())
            {
                throw new Exception("Failed to initialize list of invalid categories");
            }

            this.DisplayResults();
        }

        /// <summary>
        /// Initialize the sound objects list.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool InitSoundsList()
        {
            this.soundObjects = new List<KeyValuePair<IObjectInstance, string>>();

            var lookups =
                RaveInstance.ObjectLookupTables.Where(
                    lookupTable => lookupTable.Key.Type.ToLower().Equals("gameobjects"));

            foreach (var lookup in lookups)
            {
                foreach (var obj in lookup.Value.Values.Where(obj => this.fieldDefNames.ContainsKey(obj.TypeName)))
                {
                    var fieldDefs = this.fieldDefNames[obj.TypeName];
                    foreach (var reference in obj.References)
                    {
                        var refObj = reference.ObjectInstance;

                        if (null != refObj && 
                            refObj.Name.ToUpper() != "NULL_SOUND" &&
                            fieldDefs.Contains(reference.Node.Name))
                        {
                            this.soundObjects.Add(new KeyValuePair<IObjectInstance, string>(refObj, reference.Node.Name));
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Initialize the list of invalid categories.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool InitInvalidCategoriesList()
        {
            this.nonCollisionsCategorySounds = new HashSet<KeyValuePair<IObjectInstance, string>>();

            foreach (var kvp in this.soundObjects)
            {
                var objectInstance = kvp.Key;
                var fieldName = kvp.Value;
                if (!this.InitInvalidCategoriesListHelper(objectInstance, true, fieldName))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Helper method to initialize the list of invalid categories.
        /// </summary>
        /// <param name="objectInstance">
        /// The current object instance.
        /// </param>
        /// <param name="topInstance">
        /// Flag indicating if object is top instance.
        /// </param>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool InitInvalidCategoriesListHelper(IObjectInstance objectInstance, bool topInstance = false, string fieldName = null)
        {
            var categoryElem = objectInstance.Node.SelectSingleNode("Category");
            if (null != categoryElem)
            {
                var category = categoryElem.InnerText;

                if ((topInstance && string.IsNullOrEmpty(category)) || (!string.IsNullOrEmpty(category) && category != CollisionsCategory))
                {
                    this.nonCollisionsCategorySounds.Add(new KeyValuePair<IObjectInstance, string>(objectInstance, fieldName));
                }
            }

            foreach (var childRef in objectInstance.References)
            {
                var childObj = childRef.ObjectInstance;

                if (null == childObj)
                {
                    continue;
                }

                if (!this.InitInvalidCategoriesListHelper(childObj))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Display the results.
        /// </summary>
        private void DisplayResults()
        {
            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Game Object Category Report</title></head><body>");

            sw.WriteLine("<h2>Objects with non-COLLISIONS category</h2>");

            if (!this.nonCollisionsCategorySounds.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<table>");
                sw.WriteLine("<tr>");
                sw.WriteLine("<th>Object Name</th>");
                sw.WriteLine("<th>Bank Name</th>");
                sw.WriteLine("<th>Object Type</th>");
                sw.WriteLine("<th>Category</th>");
                sw.WriteLine("<th>Field Name</th>");
                sw.WriteLine("</tr>");
                foreach (var kvp in this.nonCollisionsCategorySounds)
                {
                    var obj = kvp.Key;
                    var fieldName = kvp.Value;

                    var categoryElem = obj.Node.SelectSingleNode("Category");
                    var category = string.Empty;
                    if (null != categoryElem)
                    {
                        category = categoryElem.InnerText;
                    }
                    
                    sw.WriteLine("<tr>");
                    sw.WriteLine("<td>" + obj.Name + "</td>");
                    sw.WriteLine("<td>" + obj.Bank.Name + "</td>");
                    sw.WriteLine("<td>" + obj.TypeName + "</td>");
                    sw.WriteLine("<td>" + category + "</td>");
                    sw.WriteLine("<td>" + (fieldName ?? string.Empty) + "</td>");
                    sw.WriteLine("</tr>");
                }

                sw.WriteLine("</table>");
            }

            sw.WriteLine("</body></html>");

            sw.Close();
            filesteam.Close();

            // Display report
            var report = new frmReport(this.name + " Report");
            report.SetURL(resultsFileName);
            report.Show();
        }
    }
}
