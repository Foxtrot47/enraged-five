﻿namespace Rave.SearchProvider.Infrastructure.Interfaces
{
    public class SearchArguments
    {
        private bool _multiTermSearch = true;
        private bool _regexMode;
        public bool SearchNameHash { get; set; }
        public bool ShouldOnlySearchNames { get; set; }
        public bool IsCaseSensitive { get; set; }
        public bool ExactNameMatch { get; set; }

        public bool MultiTermSearch
        {
            get { return _multiTermSearch; }
            set { _multiTermSearch = value; }
        }

        public bool RegexMode
        {
            get { return _regexMode; }
            set { _regexMode = value; }
        }

        public SearchArguments(bool searchNameHash, bool shouldOnlySearchNames, bool isCaseSensitive,
            bool exactNameMatch, bool multiTermSearch = false, bool regexMode = false)
        {
            SearchNameHash = searchNameHash;
            ShouldOnlySearchNames = shouldOnlySearchNames;
            IsCaseSensitive = isCaseSensitive;
            ExactNameMatch = exactNameMatch;
            _multiTermSearch = multiTermSearch;
            _regexMode = regexMode;
        }
    }
}