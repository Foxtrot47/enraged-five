﻿// -----------------------------------------------------------------------
// <copyright file="IFieldDefinitionInstance.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions.Infrastructure.Interfaces
{
    using System.Xml;

    public interface IFieldDefinitionInstance
    {
        /// <summary>
        /// Gets a value indicating whether allow override control.
        /// </summary>
        bool AllowOverrideControl { get; }

        /// <summary>
        /// Gets the allowed type.
        /// </summary>
        string AllowedType { get; }

        /// <summary>
        /// Gets the default value.
        /// </summary>
        string DefaultValue { get; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the display group.
        /// </summary>
        string DisplayGroup { get; }

        /// <summary>
        /// Gets the field def.
        /// </summary>
        IFieldDefinition FieldDef { get; }

        /// <summary>
        /// Gets a value indicating whether ignore.
        /// </summary>
        bool Ignore { get; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the node.
        /// </summary>
        XmlNode Node { get; }

        /// <summary>
        /// Gets or sets the type definition.
        /// </summary>
        ITypeDefinition TypeDefinition { get; set; }

        /// <summary>
        /// Gets the type name.
        /// </summary>
        string TypeName { get; }

		bool Tagged { get; }

        /// <summary>
        /// Gets the units.
        /// </summary>
        string Units { get; }

        /// <summary>
        /// Gets a value indicating whether visible.
        /// </summary>
        bool Visible { get; }

        /// <summary>
        /// The as composite field.
        /// </summary>
        /// <returns>
        /// The <see cref="CompositeFieldDefinitionInstance"/>.
        /// </returns>
        ICompositeFieldDefinitionInstance AsCompositeField();

        /// <summary>
        /// The as simple field.
        /// </summary>
        /// <returns>
        /// The <see cref="SimpleFieldDefinitionInstance"/>.
        /// </returns>
        ISimpleFieldDefinitionInstance AsSimpleField();

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string ToString();

	}
}
