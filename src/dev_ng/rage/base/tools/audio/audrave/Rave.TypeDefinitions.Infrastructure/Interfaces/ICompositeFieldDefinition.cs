﻿// -----------------------------------------------------------------------
// <copyright file="ICompositeFieldDefinition.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions.Infrastructure.Interfaces
{
    using System.Xml;

    public interface ICompositeFieldDefinition : IFieldDefinition
    {
        /// <summary>
        /// Gets the fields.
        /// </summary>
        IFieldDefinition[] Fields { get; }

        /// <summary>
        /// Gets the max occurs.
        /// </summary>
        int MaxOccurs { get; }

        /// <summary>
        /// The find field definition.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <returns>
        /// The <see cref="FieldDefinition"/>.
        /// </returns>
        IFieldDefinition FindFieldDefinition(string fieldName);

        /// <summary>
        /// The generate default xml node.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="force">
        /// The force.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        XmlNode GenerateDefaultXmlNode(XmlNode parentNode, bool force);
    }
}
