﻿// -----------------------------------------------------------------------
// <copyright file="ITypeDefinitions.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions.Infrastructure.Interfaces
{
    using System.Collections.Generic;

    using Rave.TypeDefinitions.Infrastructure.EnumEntry;

    public interface ITypeDefinitions
    {
        /// <summary>
        /// Gets the enums.
        /// </summary>
        EnumDefinition[] Enums { get; }

        /// <summary>
        /// Gets the file paths.
        /// </summary>
        string[] FilePaths { get; }

        /// <summary>
        /// Gets the object types.
        /// </summary>
        ITypeDefinition[] ObjectTypes { get; }

        /// <summary>
        /// Gets the schema path.
        /// </summary>
        string SchemaPath { get; }

        /// <summary>
        /// The find enum definition.
        /// </summary>
        /// <param name="enumName">
        /// The enum name.
        /// </param>
        /// <returns>
        /// The <see cref="EnumDefinition"/>.
        /// </returns>
        EnumDefinition FindEnumDefinition(string enumName);

        /// <summary>
        /// The find type definition.
        /// </summary>
        /// <param name="typeName">
        /// The type name.
        /// </param>
        /// <returns>
        /// The <see cref="ITypeDefinition"/>.
        /// </returns>
        ITypeDefinition FindTypeDefinition(string typeName);

        /// <summary>
        /// The get allowed types.
        /// </summary>
        /// <param name="fieldDef">
        /// The field def.
        /// </param>
        /// <returns>
        /// The <see cref="List{T}"/>.
        /// </returns>
        List<ITypeDefinition> GetAllowedTypes(IFieldDefinition fieldDef);

        /// <summary>
        /// The load.
        /// </summary>
        /// <param name="filePaths">
        /// The file paths.
        /// </param>
        void Load(string[] filePaths);
    }
}
