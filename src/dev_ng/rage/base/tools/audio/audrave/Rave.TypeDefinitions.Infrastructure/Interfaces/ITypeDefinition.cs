﻿// -----------------------------------------------------------------------
// <copyright file="ITypeDefinition.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions.Infrastructure.Interfaces
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Xml;

    public interface ITypeDefinition
    {
        /// <summary>
        /// Gets a value indicating whether contains bank ref.
        /// </summary>
        bool ContainsBankRef { get; }

        /// <summary>
        /// Gets a value indicating whether contains object ref.
        /// </summary>
        bool ContainsObjectRef { get; }

        /// <summary>
        /// Gets a value indicating whether contains wave ref.
        /// </summary>
        bool ContainsWaveRef { get; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        IFieldDefinition[] Fields { get; set; }

        /// <summary>
        /// Gets the group.
        /// </summary>
        string Group { get; }

        /// <summary>
        /// Gets the inherits from.
        /// </summary>
        string InheritsFrom { get; }

        /// <summary>
        /// Gets a value indicating whether is abstract.
        /// </summary>
        bool IsAbstract { get; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets a value indicating whether dev only name table.
        /// </summary>
        bool DevOnlyNameTable { get; }

        /// <summary>
        /// Gets a value indicating whether to delete container on reference delete.
        /// </summary>
        bool SkipDeleteContainerOnRefDelete { get; }

        /// <Summary>
        /// Gets a value indicating whether to use a 16bit string table index
        /// </Summary>
        bool Use16BitStringTableIndex { get; }

        /// <summary>
        /// Gets the type definitions.
        /// </summary>
        ITypeDefinitions TypeDefinitions { get; }

        /// <summary>
        /// The find field definition.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <returns>
        /// The <see cref="FieldDefinition"/>.
        /// </returns>
        IFieldDefinition FindFieldDefinition(string fieldName);

        /// <summary>
        /// The find object ref fields.
        /// </summary>
        /// <returns>
        /// The <see cref="FieldDefinition[]"/>.
        /// </returns>
        IFieldDefinition[] FindObjectRefFields();

        /// <summary>
        /// The generate default xml node.
        /// </summary>
        /// <param name="parentDoc">
        /// The parent doc.
        /// </param>
        /// <param name="soundName">
        /// The sound name.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        XmlNode GenerateDefaultXmlNode(XmlDocument parentDoc, string soundName);

        /// <summary>
        /// The get descendant types.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<ITypeDefinition> GetDescendantTypes();

        /// <summary>
        /// The get display groups.
        /// </summary>
        /// <returns>
        /// The <see cref="List{T}"/>.
        /// </returns>
        List<string> GetDisplayGroups();

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string ToString();
    }
}
