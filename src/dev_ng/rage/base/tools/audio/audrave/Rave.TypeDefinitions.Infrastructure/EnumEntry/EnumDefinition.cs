﻿// -----------------------------------------------------------------------
// <copyright file="EnumDefinition.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions.Infrastructure.EnumEntry
{
    using System.Collections;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces.EnumEntry;

    /// <summary>
    /// The enum definition.
    /// </summary>
    public class EnumDefinition : IEnumDefinition
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumDefinition"/> class.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        public EnumDefinition(XmlNode node)
        {
            this.Name = node.Attributes["name"].Value.Trim();
            var al = new ArrayList();
            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name == "Value")
                {
                    al.Add(new EnumValueEntry(child));
                }
            }

            this.Values = (EnumValueEntry[])al.ToArray(typeof(EnumValueEntry));
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the values.
        /// </summary>
        public IEnumValueEntry[] Values { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The find enum from value.
        /// </summary>
        /// <param name="valueName">
        /// The value name.
        /// </param>
        /// <returns>
        /// The <see cref="EnumValueEntry"/>.
        /// </returns>
        public IEnumValueEntry FindEnumFromValue(string valueName)
        {
            foreach (var e in this.Values)
            {
                if (e.ValueName == valueName)
                {
                    return e;
                }
            }

            return null;
        }

        #endregion
    }
}
