﻿// -----------------------------------------------------------------------
// <copyright file="IEnumDefinition.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions.Infrastructure.Interfaces.EnumEntry
{
    public interface IEnumDefinition
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the values.
        /// </summary>
        IEnumValueEntry[] Values { get; }

        /// <summary>
        /// The find enum from value.
        /// </summary>
        /// <param name="valueName">
        /// The value name.
        /// </param>
        /// <returns>
        /// The <see cref="EnumValueEntry"/>.
        /// </returns>
        IEnumValueEntry FindEnumFromValue(string valueName);
    }
}
