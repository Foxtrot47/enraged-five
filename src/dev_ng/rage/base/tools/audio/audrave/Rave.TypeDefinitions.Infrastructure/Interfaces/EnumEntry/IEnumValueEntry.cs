﻿// -----------------------------------------------------------------------
// <copyright file="IEnumValueEntry.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions.Infrastructure.Interfaces.EnumEntry
{
    public interface IEnumValueEntry
    {
        /// <summary>
        /// Gets the description.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the display name.
        /// </summary>
        string DisplayName { get; }

        /// <summary>
        /// Gets the value name.
        /// </summary>
        string ValueName { get; }

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string ToString();
    }
}
