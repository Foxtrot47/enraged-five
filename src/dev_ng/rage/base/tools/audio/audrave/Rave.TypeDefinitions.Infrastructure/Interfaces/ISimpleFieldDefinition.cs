﻿// -----------------------------------------------------------------------
// <copyright file="ISimpleFieldDefinition.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions.Infrastructure.Interfaces
{
    public interface ISimpleFieldDefinition : IFieldDefinition
    {
        /// <summary>
        /// Gets the enum name.
        /// </summary>
        string EnumName { get; }

        /// <summary>
        /// Gets the length.
        /// </summary>
        int Length { get; }

        /// <summary>
        /// Gets the max.
        /// </summary>
        float Max { get; }

        /// <summary>
        /// Gets the min.
        /// </summary>
        float Min { get; }
    }
}
