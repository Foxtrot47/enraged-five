﻿// -----------------------------------------------------------------------
// <copyright file="EnumValueEntry.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions.Infrastructure.EnumEntry
{
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces.EnumEntry;

    /// <summary>
    /// The enum value entry.
    /// </summary>
    public class EnumValueEntry : IEnumValueEntry
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumValueEntry"/> class.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        public EnumValueEntry(XmlNode node)
        {
            this.ValueName = node.InnerText.Trim();
            this.DisplayName = node.Attributes["display"] != null
                                   ? node.Attributes["display"].Value.Trim()
                                   : this.ValueName;
            if (node.Attributes["description"] != null)
            {
                this.Description = node.Attributes["description"].Value.Trim();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the description.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the display name.
        /// </summary>
        public string DisplayName { get; private set; }

        /// <summary>
        /// Gets the value name.
        /// </summary>
        public string ValueName { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.DisplayName;
        }

        #endregion
    }
}
