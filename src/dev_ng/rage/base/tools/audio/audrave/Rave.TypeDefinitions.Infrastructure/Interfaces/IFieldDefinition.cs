﻿// -----------------------------------------------------------------------
// <copyright file="IFieldDefinition.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;

namespace Rave.TypeDefinitions.Infrastructure.Interfaces
{
    using System.Xml;

    public interface IFieldDefinition
    {
        /// <summary>
        /// Gets a value indicating whether allow override control.
        /// </summary>
        bool AllowOverrideControl { get; }

        /// <summary>
        /// Gets the allowed type.
        /// </summary>
        string AllowedType { get; }

        /// <summary>
        /// Gets a value indicating whether default drop field.
        /// </summary>
        bool DefaultDropField { get; }

        /// <summary>
        /// Gets the default value.
        /// </summary>
        string DefaultValue { get; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the display group.
        /// </summary>
        string DisplayGroup { get; }

        /// <summary>
        /// Gets a value indicating whether ignore.
        /// </summary>
        bool Ignore { get; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the node.
        /// </summary>
        XmlNode Node { get; }

        /// <summary>
        /// Gets or sets the type definition.
        /// </summary>
        ITypeDefinition TypeDefinition { get; set; }

        /// <summary>
        /// Gets the type name.
        /// </summary>
        string TypeName { get; }

        /// <summary>
        /// Gets the units.
        /// </summary>
        string Units { get; }

		bool Tagged { get; }

        /// <summary>
        /// Gets a value indicating whether visible.
        /// </summary>
        bool Visible { get; }

        /// <summary>
        /// The does field contain object ref.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool DoesFieldContainObjectRef();

        /// <summary>
        /// The generate default xml node.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        XmlNode GenerateDefaultXmlNode(XmlNode parentNode);

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string ToString();
    }
}
