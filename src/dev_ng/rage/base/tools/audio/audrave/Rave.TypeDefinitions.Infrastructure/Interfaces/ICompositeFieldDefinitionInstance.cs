﻿// -----------------------------------------------------------------------
// <copyright file="ICompositeFieldDefinitionInstance.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions.Infrastructure.Interfaces
{
    public interface ICompositeFieldDefinitionInstance : IFieldDefinitionInstance
    {
        /// <summary>
        /// Gets the composite field def.
        /// </summary>
        ICompositeFieldDefinition CompositeFieldDef { get; }

        /// <summary>
        /// Gets the fields.
        /// </summary>
        IFieldDefinition[] Fields { get; }

        /// <summary>
        /// Gets the max occurs.
        /// </summary>
        int MaxOccurs { get; }
    }
}
