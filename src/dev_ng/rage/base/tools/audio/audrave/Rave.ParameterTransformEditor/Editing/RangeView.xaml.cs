﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using WPFToolLib.Extensions;

namespace Rave.ParameterTransformEditor.Editing
{
    /// <summary>
    ///   Interaction logic for RangeView.xaml
    /// </summary>
    public partial class RangeView
    {
        public static readonly DependencyProperty TitleProperty;

        private readonly Brush m_errorForegroundBrush;
        private readonly Brush m_errorBackgroundBrush;
        private readonly Brush m_minForegroundBrush;
        private readonly Brush m_minBackgroundBrush;
        private readonly Brush m_maxForegroundBrush;
        private readonly Brush m_maxBackgroundBrush;

        static RangeView()
        {
            TitleProperty = DependencyProperty.Register("Title", typeof (object), typeof (RangeView));
        }

        public RangeView()
        {
            InitializeComponent();

            m_minForegroundBrush = min.Foreground;
            m_minBackgroundBrush = min.Background;
            m_maxForegroundBrush = max.Foreground;
            m_maxBackgroundBrush = max.Background;

            m_errorForegroundBrush = Brushes.Wheat;
            m_errorBackgroundBrush = Brushes.Tomato;
        }

        public object Title
        {
            get { return GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        private void OnMinTextChanged(object sender, TextChangedEventArgs e)
        {
            float minValue;
            if (string.IsNullOrEmpty(min.Text) || float.TryParse(min.Text, out minValue))
            {
                min.Foreground = m_minForegroundBrush;
                min.Background = m_minBackgroundBrush;
            }
            else
            {
                min.Foreground = m_errorForegroundBrush;
                min.Background = m_errorBackgroundBrush;
            }
        }

        private void OnMaxTextChanged(object sender, TextChangedEventArgs e)
        {
            float maxValue;
            if (string.IsNullOrEmpty(max.Text) || float.TryParse(max.Text, out maxValue))
            {
                max.Foreground = m_maxForegroundBrush;
                max.Background = m_maxBackgroundBrush;
            }
            else
            {
                max.Foreground = m_errorForegroundBrush;
                max.Background = m_errorBackgroundBrush;
            }
        }

        private void OnOkClicked(object sender, RoutedEventArgs e)
        {
            float minValue;
            var isMinValid = float.TryParse(min.Text, out minValue);

            float maxValue;
            var isMaxValid = float.TryParse(max.Text, out maxValue);

            var range = this.FindDataContext<Range>();
            if (range != null && isMinValid && isMaxValid)
            {
                range.SetMinAndMax(minValue, maxValue);

                min.Text = null;
                max.Text = null;

                modifyExpander.IsExpanded = false;
            }
            else
            {
                MessageBox.Show("Please enter valid numeric values", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OnResetClicked(object sender, RoutedEventArgs e)
        {
            var range = this.FindDataContext<Range>();
            if (range != null)
            {
                range.Reset();
                min.Text = null;
                max.Text = null;
            }
        }

        private void OnUserControlDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            min.Text = null;
            max.Text = null;
        }
    }
}