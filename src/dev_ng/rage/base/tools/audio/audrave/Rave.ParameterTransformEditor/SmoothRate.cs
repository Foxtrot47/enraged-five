using System;
using System.ComponentModel;
using rage.ToolLib;
using WPFToolLib.Extensions;

namespace Rave.ParameterTransformEditor
{
	public class SmoothRate : INotifyPropertyChanged
	{
        private const float NO_SMOOTHING_VALUE = -1;
        private const float MIN = 0;
        private const float MAX = 120;
        private bool m_noSmoothing;
        private float m_rate;

        public SmoothRate()
        {
            NoSmoothing = true;
            Rate = MIN;
        }

        public float Rate
        {
            get { return m_rate; }
            set
            {
                var temp = (float) Math.Round(Math.Min(Math.Max(value, MIN), MAX), 3);
                if (m_rate != temp)
                {
                    m_rate = temp;
                    PropertyChanged.Raise(this, "Rate");
                    Dirty.Raise();
                }
            }
        }

        public bool NoSmoothing
        {
            get { return m_noSmoothing; }
            set
            {
                if (m_noSmoothing != value)
                {
                    m_noSmoothing = value;
                    PropertyChanged.Raise(this, "NoSmoothing");
                    Dirty.Raise();
                }
            }
        }

        public static float Min
        {
            get { return MIN; }
        }

        public static float Max
        {
            get { return MAX; }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public event Action Dirty;

        public float GetAdjustedRate()
        {
            if (m_noSmoothing)
            {
                return NO_SMOOTHING_VALUE;
            }
            return m_rate;
        }
    }
}