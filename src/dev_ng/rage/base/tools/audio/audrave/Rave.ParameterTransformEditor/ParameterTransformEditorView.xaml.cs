﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Xml;

namespace Rave.ParameterTransformEditor
{
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    ///   Interaction logic for ParameterTransformEditorView.xaml
    /// </summary>
    public partial class ParameterTransformEditorView : IRAVEObjectEditorPlugin
    {
        private ParameterTransformEditorViewModel m_viewModel;

        public ParameterTransformEditorView()
        {
            InitializeComponent();
        }

        public string GetName()
        {
            return "Parameter Transform Editor";
        }

        public bool Init(XmlNode settings)
        {
            return true;
        }

        public string ObjectType
        {
            get { return "ParameterTransformSound"; }
        }

        public void Dispose()
        {
            // Do nothing
        }

#pragma warning disable 0067
        public event Action<IObjectInstance> OnObjectEditClick;
        public event Action<IObjectInstance> OnObjectRefClick;
        public event Action<string, string> OnWaveRefClick;
        public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067

        private IObjectInstance m_objectToEdit;
        public void EditObject(IObjectInstance objectInstance, Mode mode)
        {
            OnUserControlUnloaded(null, null);
            m_objectToEdit = objectInstance;
            UserControl_Loaded(null, null);           
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsCheckedOut":
                    {
                        if (m_viewModel != null && m_viewModel.IsCheckedOut)
                        {
                            editingView.Visibility = Visibility.Visible;
                            editingView.DataContext = m_viewModel;
                            editingView.ViewModel = m_viewModel;

                            readOnlyView.Visibility = Visibility.Collapsed;
                            readOnlyView.DataContext = null;
                        }
                        else if (readOnlyView != null)
                        {
                            readOnlyView.Visibility = Visibility.Visible;
                            readOnlyView.DataContext = m_viewModel;

                            editingView.Visibility = Visibility.Collapsed;
                            editingView.DataContext = null;
                            editingView.ViewModel = null;
                            editingView.parameterTransforms.ContextMenu = null;
                        }

                        break;
                    }
            }
        }

        private void OnUserControlUnloaded(object sender, RoutedEventArgs e)
        {
            if (m_viewModel != null)
            {
                editingView.DataContext = null;
                editingView.ViewModel = null;
                readOnlyView.DataContext = null;

                m_viewModel.PropertyChanged -= OnViewModelPropertyChanged;
                m_viewModel.Dispose();
                m_viewModel = null;
            }

            if (e != null)
            {
                e.Handled = true;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (m_objectToEdit != null)
            {
                m_viewModel = new ParameterTransformEditorViewModel(new ParameterTransformEditorModel());
                m_viewModel.PropertyChanged += OnViewModelPropertyChanged;
                OnViewModelPropertyChanged(null, new PropertyChangedEventArgs("IsCheckedOut"));
                m_viewModel.EditObject(m_objectToEdit);
            }
        }
    }
}