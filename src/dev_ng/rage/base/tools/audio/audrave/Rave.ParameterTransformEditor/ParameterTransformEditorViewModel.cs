namespace Rave.ParameterTransformEditor
{
	using System;
	using System.ComponentModel;
	using System.Windows;
	using System.Windows.Controls;

	using Rave.Types.Infrastructure.Interfaces.Objects;
	using Rave.Types.Infrastructure.Interfaces.VariableList;

	using WPFToolLib.Extensions;

	public class ParameterTransformEditorViewModel : INotifyPropertyChanged,
														  IDisposable
	{
		private readonly ParameterTransformEditorModel m_model;

		private ParameterTransform m_selectedTransform;

		public ParameterTransformEditorViewModel(ParameterTransformEditorModel model)
		{
			if (model == null)
			{
				throw new ArgumentNullException("model");
			}
			m_model = model;
			m_model.IsCheckedOutChanged += OnIsCheckedOutChanged;
			m_model.CanAddTransformChanged += OnCanAddTransformChanged;
			m_model.Reset += OnReset;
			m_model.Loaded += OnLoaded;
			m_model.TransformAdded += OnTransformAdded;
			m_model.TransformRemoved += OnTransformRemoved;

			Transforms = new BindingList<ParameterTransform>();
		}

		#region ParameterTransformEditorViewModel Members

		public event PropertyChangedEventHandler PropertyChanged;

		public bool IsCheckedOut
		{
			get { return m_model.IsCheckedOut; }
		}

		public bool CanAddTransform
		{
			get { return m_model.CanAddTransform; }
		}

		public void EditObject(IObjectInstance objectInstance)
		{
			m_model.EditObject(objectInstance);
		}

		public ParameterTransform SelectedTransform
		{
			get { return m_selectedTransform; }
			set
			{
				if (m_selectedTransform != value)
				{
					m_selectedTransform = value;
					PropertyChanged.Raise(this, "SelectedTransform");
				}
			}
		}

		public IVariableList VariableList { get; private set; }

		public ContextMenu ContextMenu { get; private set; }

		public BindingList<ParameterTransform> Transforms { get; private set; }

		public int ContextMenuCount { get; private set; }

		public void RemoveTransformFor(string inputVariable)
		{
			m_model.RemoveTransformFor(inputVariable);
		}

		public void Dispose()
		{
			m_model.IsCheckedOutChanged -= OnIsCheckedOutChanged;
			m_model.CanAddTransformChanged -= OnCanAddTransformChanged;
			m_model.Reset -= OnReset;
			m_model.Loaded -= OnLoaded;
			m_model.TransformAdded -= OnTransformAdded;
			m_model.TransformRemoved -= OnTransformRemoved;
		}

		#endregion

		private void InitContextMenu()
		{
			ContextMenuCount = VariableList.AllItems.Count;
			ContextMenu = VariableList.GetContextMenu_Wpf(new RoutedEventHandler(this.MenuItem_Clicked));
			foreach (MenuItem item in ContextMenu.Items)
			{
				if (item.Items.Count > 0)
				{
					this.InitContextMenuHelper(item);
				}
				else if (m_model.HasTransformFor(item.Tag.ToString()))
				{
					item.Visibility = Visibility.Collapsed;
					ContextMenuCount--;
				}
			}
		}

		private void InitContextMenuHelper(MenuItem item)
		{
			if (item.Items.Count > 0)
			{
				foreach (MenuItem childItem in item.Items)
				{
					this.InitContextMenuHelper(childItem);
				}
			}
			else if (m_model.HasTransformFor(item.Tag.ToString()))
			{
				item.Visibility = Visibility.Collapsed;
				PropagateMenuItemVisibility(item);
				ContextMenuCount--;
			}
		}

		private MenuItem GetMenuItem(string path, char delimiter = '.', MenuItem parent = null)
		{
			if (null == this.ContextMenu)
			{
				// Context menu hasn't been loaded yet
				return null;
			}

			var items = parent != null ? parent.Items : this.ContextMenu.Items;
			var splitPath = path.Split(delimiter);

			foreach (MenuItem menuItem in items)
			{
				if (menuItem.Header.Equals(splitPath[0]))
				{
					if (splitPath.Length == 1)
					{
						return menuItem;
					}

					var subPath = path.Substring(path.IndexOf(delimiter) + 1);
					return this.GetMenuItem(subPath, delimiter, menuItem);
				}
			}

			return null;
		}

		private void PropagateMenuItemVisibility(MenuItem menuItem)
		{
			var parent = menuItem.Parent;

			if (menuItem.Visibility == Visibility.Visible)
			{
				while (null != parent)
				{
					// Make sure all parent menu items in hierarchy are visible
					var parentMenuItem = parent as MenuItem;
					if (null != parentMenuItem)
					{
						parentMenuItem.Visibility = Visibility.Visible;
						parent = parentMenuItem.Parent;
					}
					else
					{
						break;
					}
				}
			}
			else
			{
				while (null != parent)
				{
					// Hide parent menu item if no longer has any visible menu items in it
					var parentMenuItem = parent as MenuItem;
					if (null != parentMenuItem)
					{
						var hasVisibleItems = false;
						foreach (MenuItem item in parentMenuItem.Items)
						{
							if (item.Visibility == Visibility.Visible)
							{
								hasVisibleItems = true;
							}
						}

						if (!hasVisibleItems)
						{
							parentMenuItem.Visibility = Visibility.Collapsed;
							parent = parentMenuItem.Parent;
						}
						else
						{
							// Don't hide parent as it still has visible item(s)
							break;
						}
					}
					else
					{
						break;
					}
				}
			}
		}

		private void OnTransformAdded(ParameterTransform transform)
		{
			Transforms.Add(transform);

			var menuItem = this.GetMenuItem(transform.InputVariable);

			if (null != menuItem)
			{
				menuItem.Visibility = Visibility.Collapsed;

				PropagateMenuItemVisibility(menuItem);

				ContextMenuCount--;
				PropertyChanged.Raise(this, "ContextMenuCount");
			}
		}

		private void OnTransformRemoved(ParameterTransform transform)
		{
			Transforms.Remove(transform);

			var menuItem = this.GetMenuItem(transform.InputVariable);

			if (null != menuItem)
			{
				menuItem.Visibility = Visibility.Visible;

				PropagateMenuItemVisibility(menuItem);

				ContextMenuCount++;
				PropertyChanged.Raise(this, "ContextMenuCount");
			}
		}

		private void OnCanAddTransformChanged()
		{
			PropertyChanged.Raise(this, "CanAddTransform");
		}

		private void OnIsCheckedOutChanged()
		{
			PropertyChanged.Raise(this, "IsCheckedOut");
		}

		private void OnReset()
		{
			Transforms.Clear();
			ContextMenu = null;
			ContextMenuCount = 0;
			PropertyChanged.Raise(this, "ContextMenuCount");
		}

		private void OnLoaded()
		{
			VariableList = m_model.AllVariables;
			this.InitContextMenu();
			PropertyChanged.Raise(this, "ContextMenuCount");
		}

		private void MenuItem_Clicked(object obj, EventArgs args)
		{
			var item = obj as MenuItem;
			if (null != item)
			{
				var tag = item.Tag as string;
				if (null != tag)
				{
					m_model.AddTransformFor(tag, this.VariableList.FlatListLabels.Contains(tag));
				}
			}
		}
	}
}