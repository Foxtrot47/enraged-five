﻿using System.Windows.Controls;

namespace Rave.ParameterTransformEditor
{
    /// <summary>
    ///   Interaction logic for ReadOnlyView.xaml
    /// </summary>
    public partial class ReadOnlyView : UserControl
    {
        public ReadOnlyView()
        {
            InitializeComponent();
        }
    }
}