using System;
using System.ComponentModel;
using rage.ToolLib;
using Rave.CurveEditor2;
using WPFToolLib.Extensions;

namespace Rave.ParameterTransformEditor
{
    public class Range : INotifyPropertyChanged
    {
        private const string FORMAT_2_DECIMAL_PLACES = "{0:0.##}";
        private ParameterDestinations? m_destination;
        private float? m_highestValue;
        private float? m_lowestValue;
        private float m_max;
        private float m_min;
        private string m_unit;

        public Range(ParameterDestinations? destination)
        {
            SetType(destination);
        }

        public float Min
        {
            get { return m_min; }
            private set
            {
                if (m_lowestValue != null)
                {
                    m_min = value < m_lowestValue ? m_lowestValue.Value : value;
                }
                else
                {
                    m_min = value;
                }

                if (m_min > m_max)
                {
                    m_min = m_max;
                }
                RaiseMinChanged();
            }
        }

        public string MinText
        {
            get { return string.Format(FORMAT_2_DECIMAL_PLACES, Min); }
        }

        public float Max
        {
            get { return m_max; }
            private set
            {
                if (m_highestValue != null)
                {
                    m_max = value > m_highestValue ? m_highestValue.Value : value;
                }
                else
                {
                    m_max = value;
                }

                if (m_max < m_min)
                {
                    m_max = m_min;
                }
                RaiseMaxChanged();
            }
        }

        public string MaxText
        {
            get { return string.Format(FORMAT_2_DECIMAL_PLACES, Max); }
        }

        public string Unit
        {
            get { return m_unit; }
            private set
            {
                m_unit = value;
                PropertyChanged.Raise(this, "Unit");
            }
        }

        public ParameterDestinations? Destination
        {
            get { return m_destination; }
            set { SetType(value); }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public event Action Dirty;

        public void Reset()
        {
            SetType(m_destination);
        }

        public void SetMinAndMax(float min, float max)
        {
            if (min > max)
            {
                var temp = max;
                max = min;
                min = temp;
            }
            Max = max;
            Min = min;
            Dirty.Raise();
        }

        private void SetType(ParameterDestinations? destination)
        {
            m_destination = destination;
            switch (destination)
            {
                case ParameterDestinations.Volume:
                    {
                        m_highestValue = 0;
                        m_lowestValue = -100;
                        m_max = m_highestValue.Value;
                        m_min = m_lowestValue.Value;
                        Unit = "db";
                        break;
                    }
                case ParameterDestinations.Pitch:
                    {
                        m_highestValue = 24;
                        m_lowestValue = -24;
                        m_max = m_highestValue.Value;
                        m_min = m_lowestValue.Value;
                        Unit = "semitones";
                        break;
                    }
                case ParameterDestinations.Pan:
                    {
                        m_highestValue = 360;
                        m_lowestValue = 0;
                        m_max = m_highestValue.Value;
                        m_min = m_lowestValue.Value;
                        Unit = "degrees";
                        break;
                    }
                case ParameterDestinations.Lpf:
                case ParameterDestinations.Hpf:
                    {
                        m_highestValue = 24000;
                        m_lowestValue = 0;
                        m_max = m_highestValue.Value;
                        m_min = m_lowestValue.Value;
                        Unit = "Hz";
                        break;
                    }
                case ParameterDestinations.StartOffset:
                case ParameterDestinations.PreDelay:
                    {
                        m_highestValue = null;
                        m_lowestValue = null;
                        m_max = 1;
                        m_min = 0;
                        Unit = "seconds";
                        break;
                    }
                default:
                    {
                        m_highestValue = null;
                        m_lowestValue = null;
                        m_max = 1;
                        m_min = 0;
                        Unit = null;
                        break;
                    }
            }
            Dirty.Raise();
            RaiseMaxChanged();
            RaiseMinChanged();
        }

        private void RaiseMinChanged()
        {
            PropertyChanged.Raise(this, "Min");
            PropertyChanged.Raise(this, "MinText");
        }

        private void RaiseMaxChanged()
        {
            PropertyChanged.Raise(this, "Max");
            PropertyChanged.Raise(this, "MaxText");
        }
    }
}