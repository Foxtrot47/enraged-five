﻿namespace Rave.ParameterTransformEditor.Editing
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    ///   Interaction logic for EditingView.xaml
    /// </summary>
    public partial class EditingView
    {
        public EditingView()
        {
            InitializeComponent();
        }

        public ParameterTransformEditorViewModel ViewModel { get; set; }

        private void TransformsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ViewModel != null)
            {
                curveEditorContainer.Children.Clear();
                if (e.AddedItems.Count > 0)
                {
                    ViewModel.SelectedTransform = e.AddedItems[0] as ParameterTransform;
                    curveEditorContainer.Children.Add(ViewModel.SelectedTransform.EditingCurveEditorView);
                }
                else
                {
                    ViewModel.SelectedTransform = null;
                }
            }
            e.Handled = true;
         
        }

        private void LegendSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ViewModel != null)
            {
                var selectedTransform = ViewModel.SelectedTransform;
                if (selectedTransform != null)
                {
                    foreach (var curveEvaluator in selectedTransform.CurveEditorViewModel.CurveEvaluators)
                    {
                        curveEvaluator.IsSelected = false;
                    }
                    selectedTransform.CurveEditorViewModel.SelectedCurve = null;

                    foreach (var item in e.AddedItems)
                    {
                        var parameterTransformOutput = item as ParameterTransformOutput;
                        if (parameterTransformOutput != null)
                        {
                            parameterTransformOutput.CurveEvaluator.IsSelected = true;
                            selectedTransform.CurveEditorViewModel.SelectedCurve = parameterTransformOutput.CurveEvaluator;
                            break;
                        }
                    }
                }
            }
            e.Handled = true;
        }

        private void OnRemoveClicked(object sender, RoutedEventArgs e)
        {
            var selected = parameterTransforms.SelectedItem as ParameterTransform;
            if (selected != null)
            {
                ViewModel.RemoveTransformFor(selected.InputVariable);
            }
            e.Handled = true;
        }

        private void OnUserControlUnloaded(object sender, RoutedEventArgs e)
        {
            ViewModel = null;
            e.Handled = true;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            // The view model is only set if in 'edit' mode and context
            // menu only applies when in edit mode
            if (null != this.ViewModel)
            {
                this.parameterTransforms.ContextMenu = this.ViewModel.ContextMenu;
            }
        }
    }
}