﻿using System.Windows.Controls;
using WPFToolLib.Extensions;

namespace Rave.ParameterTransformEditor.Editing
{
    /// <summary>
    ///   Interaction logic for OutputView.xaml
    /// </summary>
    public partial class OutputView : UserControl
    {
        public OutputView()
        {
            InitializeComponent();
        }

        private void OutputVariableSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var output = this.FindDataContext<ParameterTransformOutput>();
                if (output != null)
                {
                    output.OutputVariable = e.AddedItems[0] as string;
                }
            }
        }
    }
}