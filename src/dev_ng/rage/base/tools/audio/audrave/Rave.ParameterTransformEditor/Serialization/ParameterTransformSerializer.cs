using Rave.CurveEditor2;

namespace Rave.ParameterTransformEditor.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.VariableList;

    using rage.ToolLib;

    using Rave.CurveEditor2.Curves;

    public class ParameterTransformSerializer
    {
        private const string FORMAT_10_DECIMAL_PLACES = "{0:0.##########}";

        private const string FORMAT_3_DECIMAL_PLACES = "{0:0.###}";

        private bool m_isLoading;

        #region ParameterTransformSerializer Members

        public IVariableList WriteableVariables { get; private set; }

        public IVariableList AllVariables { get; private set; }

        public int? MaxInputs { get; private set; }

        public int? MaxCurves { get; private set; }

        public int? MaxControlPoints { get; private set; }

        public void Load(ParameterTransformEditorModel model, IObjectInstance objectInstance)
        {
            if (objectInstance != null)
            {
                try
                {
                    m_isLoading = true;

                    SetLimits(objectInstance);
                    LoadVariables(objectInstance);

                    var badInputVariables = new List<string>();
                    var parameterTransformNodes = GetParameterTransformNodes(objectInstance);
                    foreach (var parameterTransformNode in parameterTransformNodes)
                    {
                        XmlNode inputRangeNode, inputVariableNode;
                        var outputNodes = GetInputAndOutputNodes(parameterTransformNode,
                                                                 out inputVariableNode,
                                                                 out inputRangeNode);
                        if (inputVariableNode == null ||
                            inputRangeNode == null)
                        {
                            continue;
                        }

                        var inputVariable = inputVariableNode.InnerText;
                        if (string.IsNullOrEmpty(inputVariable) ||
                            badInputVariables.Contains(inputVariable))
                        {
                            continue;
                        }

                        model.AddTransformFor(inputVariable, this.AllVariables.FlatListLabels.Contains(inputVariable));

                        var parameterTransform = model.GetTransformFor(inputVariable);
                        if (parameterTransform != null)
                        {
                            float min, max;
                            GetRangeLimits(inputRangeNode, out min, out max);
                            parameterTransform.InputRange.SetMinAndMax(min, max);
                            if (outputNodes.All(outputNode => CreateParameterOutput(parameterTransform, outputNode)))
                            {
                                continue;
                            }

                            MessageBox.Show(
                                string.Format(
                                    "Failed to parse ParameterTransform for input variable: \"{0}\".{1}This ParameterTransform will be removed.",
                                    inputVariable,
                                    Environment.NewLine),
                                "Parse Failure",
                                MessageBoxButton.OK,
                                MessageBoxImage.Warning);

                            badInputVariables.Add(inputVariable);
                            model.RemoveTransformFor(inputVariable);
                        }
                    }
                }
                finally
                {
                    m_isLoading = false;
                }
            }
        }

        public void Save(IObjectInstance objectInstance, ParameterTransformEditorModel model)
        {
            if (!m_isLoading &&
                objectInstance != null &&
                objectInstance.Node != null)
            {
                var parameterTransformNodes = GetParameterTransformNodes(objectInstance);
                foreach (var node in parameterTransformNodes)
                {
                    objectInstance.Node.RemoveChild(node);
                }

                foreach (var parameterTransform in model.Transforms)
                {
                    objectInstance.Node.AppendChild(SerializeParameterTransform(parameterTransform,
                                                                                objectInstance.Node.OwnerDocument));
                }

                objectInstance.MarkAsDirty();
            }
        }

        #endregion

        private static IEnumerable<XmlNode> GetParameterTransformNodes(IObjectInstance objectInstance)
        {
            var parameterTransformNodes = new List<XmlNode>();
            foreach (XmlNode childNode in objectInstance.Node.ChildNodes)
            {
                try
                {
                    // Default doesn't matter as long as its not ParameterTransform
                    var token = Enum<ParseTokens>.TryParse(childNode.Name, true, ParseTokens.Destination);
                    if (token == ParseTokens.ParameterTransform)
                    {
                        parameterTransformNodes.Add(childNode);
                    }
                }
                catch
                {
                }
            }
            return parameterTransformNodes;
        }

        #region Serialization

        public static float SerializeOutputRangeValue(float val, ParameterDestinations destination)
        {
            switch (destination)
            {
                case ParameterDestinations.Volume:
                    {
                        return (float)Math.Pow(10, val / 20);
                    }
                case ParameterDestinations.Pitch:
                    {
                        return (float)Math.Pow(2, val / 12);
                    }
                case ParameterDestinations.Pan:
                    {
                        return val / 360;
                    }
                case ParameterDestinations.Lpf:
                case ParameterDestinations.Hpf:
                    {
                        return val / 24000;
                    }
                default:
                    {
                        return val;
                    }
            }
        }

        private static XmlNode SerializeParameterTransform(ParameterTransform parameterTransform,
                                                            XmlDocument ownerDocument)
        {
            var parameterTransformNode = ownerDocument.CreateElement(ParseTokens.ParameterTransform.ToString());

            var inputVariableNode = ownerDocument.CreateElement(ParseTokens.InputVariable.ToString());
            inputVariableNode.AppendChild(ownerDocument.CreateTextNode(parameterTransform.InputVariable));
            parameterTransformNode.AppendChild(inputVariableNode);

            var inputRangeNode = CreateRangeNode(ParseTokens.InputRange,
                                                 parameterTransform.InputRange.Min,
                                                 parameterTransform.InputRange.Max,
                                                 ownerDocument);
            parameterTransformNode.AppendChild(inputRangeNode);

            foreach (var output in parameterTransform.Outputs)
            {
                var outputNode = CreateOutputNode(output, ownerDocument);
                parameterTransformNode.AppendChild(outputNode);
            }

            return parameterTransformNode;
        }

        private static XmlNode CreateOutputNode(ParameterTransformOutput output, XmlDocument ownerDocument)
        {
            var outputNode = ownerDocument.CreateElement(ParseTokens.Output.ToString());

            var inputSmoothRateNode = ownerDocument.CreateElement(ParseTokens.InputSmoothRate.ToString());
            var inputSmoothRateValue = string.Format(FORMAT_3_DECIMAL_PLACES, output.InputSmoothRate.GetAdjustedRate());
            inputSmoothRateNode.AppendChild(ownerDocument.CreateTextNode(inputSmoothRateValue));
            outputNode.AppendChild(inputSmoothRateNode);

            var destinationNode = ownerDocument.CreateElement(ParseTokens.Destination.ToString());
            var destinationValue = string.Format("PARAM_DESTINATION_{0}", output.Destination.ToString().ToUpper());
            destinationNode.AppendChild(ownerDocument.CreateTextNode(destinationValue));
            outputNode.AppendChild(destinationNode);

            for (var i = 0; i < 3; ++i)
            {
                AddPaddingNode(outputNode, ownerDocument, i);
            }

            var outputVariableNode = ownerDocument.CreateElement(ParseTokens.OutputVariable.ToString());
            var outputVariableValue = output.Destination == ParameterDestinations.Variable
                                          ? output.OutputVariable
                                          : string.Empty;
            outputVariableNode.AppendChild(ownerDocument.CreateTextNode(outputVariableValue));
            outputNode.AppendChild(outputVariableNode);

            var outputRangeMin = SerializeOutputRangeValue(output.OutputRange.Min, output.Destination);
            var outputRangeMax = SerializeOutputRangeValue(output.OutputRange.Max, output.Destination);
            var outputRangeNode = CreateRangeNode(ParseTokens.OutputRange, outputRangeMin, outputRangeMax, ownerDocument);
            outputNode.AppendChild(outputRangeNode);

            var curve = output.CurveEvaluator.Curve;
            foreach (var controlPoint in curve.ControlPoints)
            {
                AddControlPointNode(outputNode, ownerDocument, controlPoint);
            }

            return outputNode;
        }

        private static XmlNode CreateRangeNode(ParseTokens rangeToken, float min, float max, XmlDocument ownerDocument)
        {
            var rangeNode = ownerDocument.CreateElement(rangeToken.ToString());

            var minNode = ownerDocument.CreateElement(ParseTokens.Min.ToString());
            minNode.AppendChild(ownerDocument.CreateTextNode(string.Format(FORMAT_10_DECIMAL_PLACES, min)));
            rangeNode.AppendChild(minNode);

            var maxNode = ownerDocument.CreateElement(ParseTokens.Max.ToString());
            maxNode.AppendChild(ownerDocument.CreateTextNode(string.Format(FORMAT_10_DECIMAL_PLACES, max)));
            rangeNode.AppendChild(maxNode);

            return rangeNode;
        }

        private static void AddPaddingNode(XmlNode outputNode, XmlDocument ownerDocument, int index)
        {
            var paddingNode =
                ownerDocument.CreateElement(string.Format("{0}{1}", ParseTokens.Padding.ToString().ToLower(), index));
            paddingNode.AppendChild(ownerDocument.CreateTextNode("0"));
            outputNode.AppendChild(paddingNode);
        }

        private static void AddControlPointNode(XmlNode outputNode, XmlDocument ownerDocument, ControlPoint controlPoint)
        {
            var transformPointNode = ownerDocument.CreateElement(ParseTokens.TransformPoints.ToString());

            var xNode = ownerDocument.CreateElement(ParseTokens.X.ToString().ToLower());
            var xValue = string.Format(FORMAT_10_DECIMAL_PLACES, controlPoint.X);
            xNode.AppendChild(ownerDocument.CreateTextNode(xValue));
            transformPointNode.AppendChild(xNode);

            var yNode = ownerDocument.CreateElement(ParseTokens.Y.ToString().ToLower());
            var yValue = string.Format(FORMAT_10_DECIMAL_PLACES, controlPoint.Y);
            yNode.AppendChild(ownerDocument.CreateTextNode(yValue));
            transformPointNode.AppendChild(yNode);

            outputNode.AppendChild(transformPointNode);
        }

        #endregion

        #region Deserialization

        public static float DeserializeOutputRangeValue(float val, ParameterDestinations destination)
        {
            switch (destination)
            {
                case ParameterDestinations.Volume:
                    {
                        return (float)(Math.Log10(val) * 20);
                    }
                case ParameterDestinations.Pitch:
                    {
                        return (float)(Math.Log(val, 2) * 12);
                    }
                case ParameterDestinations.Pan:
                    {
                        return val * 360;
                    }
                case ParameterDestinations.Lpf:
                case ParameterDestinations.Hpf:
                    {
                        return val * 24000;
                    }
                default:
                    {
                        return val;
                    }
            }
        }

        private void SetLimits(IObjectInstance objectInstance)
        {
            MaxControlPoints = null;
            MaxCurves = null;
            MaxInputs = null;

            var typeDef = objectInstance.TypeDefinitions.FindTypeDefinition(objectInstance.TypeName);
            if (typeDef == null)
            {
                return;
            }

            var parameterTransformFieldDef =
                typeDef.FindFieldDefinition("ParameterTransform") as ICompositeFieldDefinition;
            if (parameterTransformFieldDef == null)
            {
                return;
            }

            var outputFieldDef = parameterTransformFieldDef.FindFieldDefinition("Output") as ICompositeFieldDefinition;
            if (outputFieldDef == null)
            {
                return;
            }

            var transformPointsFieldDef =
                outputFieldDef.FindFieldDefinition("TransformPoints") as ICompositeFieldDefinition;
            if (transformPointsFieldDef != null)
            {
                MaxInputs = parameterTransformFieldDef.MaxOccurs;
                MaxCurves = outputFieldDef.MaxOccurs;
                MaxControlPoints = transformPointsFieldDef.MaxOccurs;
            }
        }

        private void LoadVariables(IObjectInstance objectInstance)
        {
            this.AllVariables = objectInstance.GetVariableList(null, null, VariableListType.Input);
            this.WriteableVariables = objectInstance.GetVariableList(null, null, VariableListType.Output);
        }

        private static IEnumerable<XmlNode> GetInputAndOutputNodes(XmlNode parameterTransformNode,
                                                                   out XmlNode inputVariableNode,
                                                                   out XmlNode inputRangeNode)
        {
            inputVariableNode = null;
            inputRangeNode = null;

            var outputNodes = new List<XmlNode>();
            foreach (XmlNode childNode in parameterTransformNode.ChildNodes)
            {
                try
                {
                    var token = Enum<ParseTokens>.TryParse(childNode.Name, true, ParseTokens.Padding);
                    switch (token)
                    {
                        case ParseTokens.InputVariable:
                            {
                                inputVariableNode = childNode;
                                break;
                            }
                        case ParseTokens.InputRange:
                            {
                                inputRangeNode = childNode;
                                break;
                            }
                        case ParseTokens.Output:
                            {
                                outputNodes.Add(childNode);
                                break;
                            }
                    }
                }
                catch
                {
                }
            }
            return outputNodes;
        }

        private static bool CreateParameterOutput(ParameterTransform parameterTransform, XmlNode outputNode)
        {
            float? inputSmoothRate = null;
            ParameterDestinations? destination = null;
            string outputVariable = null;
            float outputRangeMin = 0, outputRangeMax = 1;
            var points = new List<Point>();

            foreach (XmlNode childNode in outputNode.ChildNodes)
            {
                try
                {
                    var token = Enum<ParseTokens>.TryParse(childNode.Name, true, ParseTokens.Padding);
                    switch (token)
                    {
                        case ParseTokens.InputSmoothRate:
                            {
                                float temp;
                                if (float.TryParse(childNode.InnerText, out temp) &&
                                    temp >= 0)
                                {
                                    inputSmoothRate = Math.Min(temp, SmoothRate.Max);
                                }
                                break;
                            }
                        case ParseTokens.Destination:
                            {
                                try
                                {
                                    destination =
                                        Enum<ParameterDestinations>.Parse(
                                            childNode.InnerText.Replace("PARAM_DESTINATION_", string.Empty));
                                }
                                catch
                                {
                                }
                                break;
                            }
                        case ParseTokens.OutputVariable:
                            {
                                outputVariable = childNode.InnerText;
                                break;
                            }
                        case ParseTokens.OutputRange:
                            {
                                GetRangeLimits(childNode, out outputRangeMin, out outputRangeMax);
                                break;
                            }
                        case ParseTokens.TransformPoints:
                            {
                                points.Add(GetTransformPoint(childNode));
                                break;
                            }
                    }
                }
                catch (Exception)
                {
                }
            }

            var curve = parameterTransform.CurveEditorViewModel.AddCurve(CurveTypes.PiecewiseLinear, points);
            if (curve != null)
            {
                var parameterTransformOutput = parameterTransform.GetOutputFor(curve);

                if (inputSmoothRate != null &&
                    inputSmoothRate.Value >= 0)
                {
                    parameterTransformOutput.InputSmoothRate.NoSmoothing = false;
                    parameterTransformOutput.InputSmoothRate.Rate = inputSmoothRate.Value;
                }

                if (destination != null)
                {
                    parameterTransformOutput.Destination = destination.Value;
                }

                parameterTransformOutput.OutputVariable = outputVariable;
                outputRangeMin = DeserializeOutputRangeValue(outputRangeMin, parameterTransformOutput.Destination);
                outputRangeMax = DeserializeOutputRangeValue(outputRangeMax, parameterTransformOutput.Destination);
                parameterTransformOutput.OutputRange.SetMinAndMax(outputRangeMin, outputRangeMax);
                return true;
            }
            return false;
        }

        private static void GetRangeLimits(XmlNode rangeNode, out float min, out float max)
        {
            min = 0;
            max = 1;

            foreach (XmlNode childNode in rangeNode.ChildNodes)
            {
                try
                {
                    var token = Enum<ParseTokens>.TryParse(childNode.Name, false, ParseTokens.Padding);
                    switch (token)
                    {
                        case ParseTokens.Min:
                            {
                                float temp;
                                if (float.TryParse(childNode.InnerText, out temp))
                                {
                                    min = temp;
                                }
                                break;
                            }
                        case ParseTokens.Max:
                            {
                                float temp;
                                if (float.TryParse(childNode.InnerText, out temp))
                                {
                                    max = temp;
                                }
                                break;
                            }
                    }
                }
                catch
                {
                }
            }
        }

        private static Point GetTransformPoint(XmlNode transformPointNode)
        {
            float x = 0;
            float y = 0;

            foreach (XmlNode childNode in transformPointNode.ChildNodes)
            {
                try
                {
                    var token = Enum<ParseTokens>.TryParse(childNode.Name, true, ParseTokens.Padding);
                    switch (token)
                    {
                        case ParseTokens.X:
                            {
                                float temp;
                                if (float.TryParse(childNode.InnerText, out temp))
                                {
                                    x = temp;
                                }
                                break;
                            }
                        case ParseTokens.Y:
                            {
                                float temp;
                                if (float.TryParse(childNode.InnerText, out temp))
                                {
                                    y = temp;
                                }
                                break;
                            }
                    }
                }
                catch
                {
                }
            }
            return new Point(x, y);
        }

        #endregion

        #region Nested type: ParseTokens

        private enum ParseTokens
        {
            ParameterTransform,
            InputVariable,
            InputRange,
            Min,
            Max,
            Output,
            InputSmoothRate,
            Destination,
            OutputVariable,
            OutputRange,
            TransformPoints,
            X,
            Y,
            Padding
        }

        #endregion
    }
}