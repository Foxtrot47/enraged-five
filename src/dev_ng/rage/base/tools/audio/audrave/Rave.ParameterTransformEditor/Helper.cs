﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Rave.ParameterTransformEditor
{
    public static class Helper
    {

        public static IEnumerable<Point> FixBezier(this IEnumerable<Point> bezierPoints,
            IEnumerable<Point> transformPoints)
        {
            var points = transformPoints.ToList();
            var bezier = bezierPoints.ToList();

            while (bezier.Count() < points.Count() - 1)
            {
                bezier.Add(GetMidPoint(points[bezier.Count()], points[bezier.Count() + 1]));
            }

            return bezier;
        }

        public static Point GetMidPoint(Point fistPoint, Point secondPoint)
        {
            double x = ((secondPoint.X - fistPoint.X) / 2) + fistPoint.X;
            double y = ((secondPoint.Y - fistPoint.Y) / 2) + fistPoint.Y;
            return new Point(x, y);

        }
    }
}
