#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Numerics;
using System.Windows;
using System.Windows.Input;
using rage.ToolLib;
using Rave.CurveEditor2;
using Rave.CurveEditor2.Axes;
using Rave.CurveEditor2.CurveEvaluators;
using Rave.CurveEditor2.Curves;
using Rave.CurveEditor2.Helpers;
using Rave.Types.Infrastructure.Interfaces.VariableList;
using WPFToolLib.Extensions;

#endregion

namespace Rave.ParameterTransformEditor
{
    public class ParameterTransform : INotifyPropertyChanged,
        IDisposable
    {
        private readonly Dictionary<CurveEvaluator, ParameterTransformOutput> m_outputLookup;
        private readonly IVariableList m_variables;
        private ParameterTransformOutput m_selectedOutput;
        public string InputVariable { get; private set; }
        public bool IsInputVariableValid { get; private set; }
        public Range InputRange { get; private set; }
        public CurveEditorViewModel CurveEditorViewModel { get; private set; }
        public CurveEditorView EditingCurveEditorView { get; private set; }
        public CurveEditorView ReadOnlyCurveEditorView { get; private set; }

        public ParameterTransformOutput SelectedOutput
        {
            get { return m_selectedOutput; }
            private set
            {
                if (m_selectedOutput != value)
                {
                    m_selectedOutput = value;
                    PropertyChanged.Raise(this, "SelectedOutput");
                }
            }
        }

        public BindingList<ParameterTransformOutput> Outputs { get; private set; }

        public ParameterTransform(string inputVariable,
            bool isInputVariableValid,
            IVariableList variables,
            int? maxCurves,
            int? maxControlPoints)
        {
            if (string.IsNullOrEmpty(inputVariable))
            {
                throw new ArgumentException("inputVariable");
            }
            if (variables == null)
            {
                throw new ArgumentNullException("variables");
            }
            InputVariable = inputVariable;
            IsInputVariableValid = isInputVariableValid;
            m_variables = variables;

            InputRange = new Range(null);
            InputRange.Dirty += OnInputRangeChanged;
            InputRange.Dirty += OnDirty;

            CurveEditorViewModel = new CurveEditorViewModel(maxCurves,
                maxControlPoints,
                new AxisModel("X", 0, 1, 1, 300),
                new AxisModel("Y", 0, 1, 1, 300),
                CanMoveControlPoint);
            CurveEditorViewModel.CurveSelected += OnCurveSelected;
            CurveEditorViewModel.CurveAdded += OnCurveAdded;
            CurveEditorViewModel.CurveRemoved += OnCurveRemoved;
            CurveEditorViewModel.CurveEdited += OnDirty;

            EditingCurveEditorView = new CurveEditorView(CurveEditorViewModel);
            ReadOnlyCurveEditorView = new CurveEditorView(CurveEditorViewModel);

            m_outputLookup = new Dictionary<CurveEvaluator, ParameterTransformOutput>();
            Outputs = new BindingList<ParameterTransformOutput>();
        }

        #region IDisposable Members

        public void Dispose()
        {
            InputRange.Dirty -= OnDirty;
            InputRange.Dirty -= OnInputRangeChanged;
            InputRange = null;

            CurveEditorViewModel.CurveSelected -= OnCurveSelected;
            CurveEditorViewModel.CurveAdded -= OnCurveAdded;
            CurveEditorViewModel.CurveRemoved -= OnCurveRemoved;
            CurveEditorViewModel.CurveEdited -= OnDirty;
            CurveEditorViewModel.Dispose();
            CurveEditorViewModel = null;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public event Action Dirty;

        public ParameterTransformOutput GetOutputFor(Curve curve)
        {
            ParameterTransformOutput output;
            m_outputLookup.TryGetValue(curve.Parent, out output);
            return output;
        }

        private void OnCurveAdded(CurveEvaluator curveEvaluator)
        {
            ParameterTransformOutput output = new ParameterTransformOutput(curveEvaluator, m_variables.FlatListLabels);
            output.Dirty += OnDirty;
            output.InputSmoothRate.Dirty += OnDirty;
            output.OutputRange.Dirty += OnOutputRangeChanged;
            output.OutputRange.Dirty += OnDirty;
            m_outputLookup.Add(curveEvaluator, output);
            Outputs.Add(output);
            OnDirty();
        }


        private void OnCurveRemoved(CurveEvaluator curveEvaluator)
        {
            ParameterTransformOutput output;
            if (m_outputLookup.TryGetValue(curveEvaluator, out output))
            {
                int index = 0;
                while (index < Outputs.Count)
                {
                    if (Outputs[index] == output)
                    {
                        break;
                    }
                    index += 1;
                }
                Outputs.RemoveAt(index);
                m_outputLookup.Remove(curveEvaluator);
                if (SelectedOutput == output)
                {
                    SelectedOutput = null;
                    UpdateAxis(CurveEditorViewModel.YAxis, new Range(null), null);
                }
                output.Dirty -= OnDirty;
                output.InputSmoothRate.Dirty -= OnDirty;
                output.OutputRange.Dirty -= OnDirty;
                output.OutputRange.Dirty -= OnOutputRangeChanged;
                OnDirty();
            }
        }

        private void OnCurveSelected(CurveEvaluator curveEvaluator)
        {
            if (curveEvaluator != null)
            {
                ParameterTransformOutput output;
                if (m_outputLookup.TryGetValue(curveEvaluator, out output))
                {
                    SelectedOutput = output;
                    UpdateAxis(CurveEditorViewModel.YAxis, SelectedOutput.OutputRange, SelectedOutput.OutputRange.Unit);
                }
                else
                {
                    throw new ApplicationException("Failed to find ParameterTransformOutput representing curve!");
                }
            }
            else
            {
                SelectedOutput = null;
                UpdateAxis(CurveEditorViewModel.YAxis, new Range(null), null);
            }
        }

        private void OnInputRangeChanged()
        {
            UpdateAxis(CurveEditorViewModel.XAxis, InputRange, null);
        }

        private void OnOutputRangeChanged()
        {
            if (SelectedOutput != null)
            {
                UpdateAxis(CurveEditorViewModel.YAxis, SelectedOutput.OutputRange, SelectedOutput.OutputRange.Unit);
            }
        }

        private static void UpdateAxis(AxisViewModel axisViewModel, Range range, string label)
        {
            axisViewModel.Destination = range.Destination;
            axisViewModel.Min = range.Min;
            axisViewModel.Max = range.Max;
            axisViewModel.Label = label;
        }

        private void OnDirty()
        {
            Dirty.Raise();
        }

        private static bool CanMoveControlPoint(Point? previousControlPoint,
            Point? nextControlPoint,
            Point currentControlPoint)
        {
            if (previousControlPoint != null)
            {
                if (currentControlPoint.X <=
                    previousControlPoint.Value.X)
                {
                    return false;
                }
            }

            if (nextControlPoint != null)
            {
                if (currentControlPoint.X >=
                    nextControlPoint.Value.X)
                {
                    return false;
                }
            }

            if (currentControlPoint.X < 0 ||
                currentControlPoint.X > 1)
            {
                return false;
            }
            return true;
        }

    }
}