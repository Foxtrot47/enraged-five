namespace Rave.ParameterTransformEditor
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	using Rave.Types.Infrastructure.Interfaces.Objects;
	using Rave.Types.Infrastructure.Interfaces.VariableList;

	using rage.ToolLib;

	using Rave.ParameterTransformEditor.Serialization;

	public class ParameterTransformEditorModel
	{
		private readonly Dictionary<string, ParameterTransform> m_transformsLookup;

		private bool m_isCheckedOut;

		private IObjectInstance m_objectInstance;

		private ParameterTransformSerializer m_serializer;

		public ParameterTransformEditorModel()
		{
			m_transformsLookup = new Dictionary<string, ParameterTransform>();
		}

		#region ParameterTransformEditorModel Members

		public IEnumerable<ParameterTransform> Transforms
		{
			get { return m_transformsLookup.Values; }
		}

		public bool CanAddTransform
		{
			get
			{
				if (m_serializer.MaxInputs == null)
				{
					return true;
				}
				return m_transformsLookup.Count < m_serializer.MaxInputs;
			}
		}

		public event Action<ParameterTransform> TransformAdded;

		public event Action<ParameterTransform> TransformRemoved;

		public event Action Reset;

		public event Action Loaded;

		public event Action IsCheckedOutChanged;

		public event Action CanAddTransformChanged;

		public bool IsCheckedOut
		{
			get { return m_isCheckedOut; }
			private set
			{
				if (m_isCheckedOut != value)
				{
					m_isCheckedOut = value;
					foreach (var transform in Transforms)
					{
						transform.CurveEditorViewModel.IsCheckedOut = m_isCheckedOut;
					}
					IsCheckedOutChanged.Raise();
				}
			}
		}

		public IVariableList AllVariables
		{
			get { return m_serializer.AllVariables; }
		}

		public void AddTransformFor(string inputVariable, bool isInputVariableValid)
		{
			if (!HasTransformFor(inputVariable) && CanAddTransform)
			{
				var transform = new ParameterTransform(inputVariable,
													   isInputVariableValid,
													   m_serializer.WriteableVariables,
													   m_serializer.MaxCurves,
													   m_serializer.MaxControlPoints) { CurveEditorViewModel = { IsCheckedOut = m_isCheckedOut } };
				transform.Dirty += OnDirty;
				m_transformsLookup.Add(inputVariable, transform);
				TransformAdded.Raise(transform);
				CanAddTransformChanged.Raise();
				Save();
			}
		}

		public bool HasTransformFor(string inputVariable)
		{
			return m_transformsLookup.ContainsKey(inputVariable);
		}

		public ParameterTransform GetTransformFor(string inputVariable)
		{
			ParameterTransform transform;
			m_transformsLookup.TryGetValue(inputVariable, out transform);
			return transform;
		}

		public void RemoveTransformFor(string inputVariable)
		{
			ParameterTransform transform;
			if (m_transformsLookup.TryGetValue(inputVariable, out transform))
			{
				transform.Dirty -= OnDirty;
				m_transformsLookup.Remove(inputVariable);
				TransformRemoved.Raise(transform);
				transform.Dispose();

				CanAddTransformChanged.Raise();

				Save();
			}
		}

		public void EditObject(IObjectInstance objectInstance)
		{
			ResetModel();

			if (m_objectInstance != null &&
				m_objectInstance.Bank != null)
			{
				m_objectInstance.Bank.BankStatusChanged -= OnBankStatusChanged;
			}

			m_objectInstance = objectInstance;
			if (m_objectInstance == null)
			{
				return;
			}

			if (m_objectInstance.Bank != null)
			{
				m_objectInstance.Bank.BankStatusChanged += OnBankStatusChanged;
			}

			IsCheckedOut = !m_objectInstance.IsReadOnly;

			m_serializer.Load(this, objectInstance);
			Loaded.Raise();
		}

		#endregion

		private void OnDirty()
		{
			Save();
		}

		private void OnBankStatusChanged()
		{
			if (m_objectInstance != null &&
				m_objectInstance.ObjectLookup.ContainsKey(m_objectInstance.Name))
			{
				// Edit the newly loaded object instance
				EditObject(m_objectInstance.ObjectLookup[m_objectInstance.Name]);
			}
			else
			{
				ResetModel();
			}
		}

		private void ResetModel()
		{
			var transforms = Transforms.ToList();
			foreach (var transform in transforms)
			{
				transform.Dirty -= OnDirty;
				m_transformsLookup.Remove(transform.InputVariable);
				transform.Dispose();
			}
			IsCheckedOut = false;
			m_serializer = new ParameterTransformSerializer();
			Reset.Raise();
		}

		private void Save()
		{
			if (IsCheckedOut)
			{
				m_serializer.Save(m_objectInstance, this);
			}
		}
	}
}