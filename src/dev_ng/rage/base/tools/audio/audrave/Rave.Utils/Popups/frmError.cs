using System.Windows.Threading;

namespace Rave.Utils.Popups
{
	using System;
	using System.Text;
	using System.Windows.Forms;

	public partial class frmError : Form
	{
		private const string SHOW = "Show Details";
		private const string HIDE = "Hide Details";

		private readonly string m_details;
		private readonly string m_message;

		public frmError(string message, string stackTrace)
		{
			this.m_message = message;
			this.m_details = stackTrace;

			this.InitializeComponent();
			this.txtMsg.Text = this.m_message;
			this.lblShowDetails.Text = SHOW;
			if (string.IsNullOrEmpty(this.m_details))
			{
				this.lblShowDetails.Visible = true;
			}
			this.Height -= this.txtDetails.Height;
		}

		private void lblShowDetails_Click(object sender, LinkLabelLinkClickedEventArgs e)
		{
			if (this.lblShowDetails.Text == SHOW)
			{
				this.txtDetails.Visible = true;
				this.txtDetails.Text = this.m_details;
				this.lblShowDetails.Text = HIDE;
				this.Height += 100;
				this.txtDetails.Height = 100;
			}
			else
			{
				this.txtDetails.Visible = false;
				this.lblShowDetails.Text = SHOW;
				this.Height -= this.txtDetails.Height;
			}
		}

		private void btCopy_Click(object sender, EventArgs e)
		{
			var sb = new StringBuilder(this.m_message);
			if (this.m_details != "")
			{
				sb.Append(" ");
				sb.Append(this.m_details);
			}
			try
			{
				Clipboard.SetText(sb.ToString());
			}
			catch
			{
				// ignored
			}
		}
	}
}