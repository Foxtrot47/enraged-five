// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Configuration.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for Configuration.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.Utils
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Windows.Forms;
	using System.Xml;

	using Rave.Instance;

	using rage;

	using Rave.Plugins.Infrastructure.Interfaces;
	using Rave.TypeDefinitions.Infrastructure.Interfaces;

	/// <summary>
	/// Summary description for Configuration.
	/// </summary>
	public static class Configuration
	{
		#region Constants

		/// <summary>
		/// The sound wrapper sound type.
		/// </summary>
		private const string SOUND_WRAPPER_SOUND_TYPE = "WrapperSound";

		/// <summary>
		/// The wave wrapper sound type.
		/// </summary>
		private const string WAVE_WRAPPER_SOUND_TYPE = "SimpleSound";

		#endregion

		#region Static Fields

		/// <summary>
		/// The project.
		/// </summary>
		private static audProjectEntry ms_project;

		#endregion

		#region Public Properties

		/// <summary>
		/// Gets the asset management type.
		/// </summary>
		public static int AssetManagementType { get; private set; }

		/// <summary>
		/// Gets the asset password.
		/// </summary>
		public static string AssetPassword { get; private set; }

		/// <summary>
		/// Gets the asset project.
		/// </summary>
		public static string AssetProject { get; private set; }

		/// <summary>
		/// Gets the asset server.
		/// </summary>
		public static string AssetServer { get; private set; }

		/// <summary>
		/// Gets the asset user.
		/// </summary>
		public static string AssetUser { get; private set; }

		/// <summary>
		/// Gets the build sets.
		/// </summary>
		public static IEnumerable<audBuildSet> BuildSets
		{
			get
			{
				return ProjectSettings.BatchBuilds;
			}
		}

		/// <summary>
		/// Gets or sets the default model path.
		/// </summary>
		public static string DefaultModelPath { get; set; }

		/// <summary>
		/// Gets the depot root.
		/// </summary>
		public static string DepotRoot { get; private set; }

		/// <summary>
		/// Gets the global variables list path.
		/// </summary>
		public static string GlobalVariablesListPath { get; private set; }

		/// <summary>
		/// Gets a value indicating whether is programmer support enabled.
		/// </summary>
		public static bool IsProgrammerSupportEnabled
		{
			get
			{
				// We don't bother to create this in the xml, since this is a "hidden" feature
				return ms_project != null && ms_project.IsProgrammerSupportEnabled;
			}
		}

		/// <summary>
		/// Gets the log directory path.
		/// </summary>
		public static string LogDirectoryPath
		{
			get
			{
				return Path.Combine(
					Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Rave", "Logs");
			}
		}

		/// <summary>
		/// Gets the metadata compiler jit paths.
		/// </summary>
		public static string[] MetadataCompilerJitPaths { get; private set; }

		/// <summary>
		/// Gets the metadata compiler path.
		/// </summary>
		public static string MetadataCompilerPath { get; private set; }

		/// <summary>
		/// Gets the metadata files.
		/// </summary>
		public static IEnumerable<audMetadataFile> MetadataFiles
		{
			get
			{
				return ProjectSettings.GetMetadataSettings();
			}
		}

		/// <summary>
		/// Gets the metadata types.
		/// </summary>
		public static IEnumerable<audMetadataType> MetadataTypes
		{
			get
			{
				return ProjectSettings.GetMetadataTypes();
			}
		}

		/// <summary>
		/// Gets the object xml path.
		/// </summary>
		public static string ObjectXmlPath { get; private set; }

		/// <summary>
		/// Gets the platform config path.
		/// </summary>
		public static string PlatformConfigPath { get; private set; }

		/// <summary>
		/// Gets the platform settings.
		/// </summary>
		public static ArrayList PlatformSettings
		{
			get
			{
				return ProjectSettings.GetPlatformSettings();
			}
		}

		/// <summary>
		/// Gets the active platform settings.
		/// </summary>
		public static IList<PlatformSetting> ActivePlatformSettings
		{
			get
			{
				return ProjectSettings.GetPlatformSettings().Cast<PlatformSetting>().Where(ps => ps.IsActive).ToList();
			}
		}

		/// <summary>
		/// Gets the platform wave path.
		/// </summary>
		public static string PlatformWavePath { get; private set; }

		/// <summary>
		/// Gets the preset path.
		/// </summary>
		public static string PresetPath { get; private set; }

		/// <summary>
		/// Gets the project name.
		/// </summary>
		public static string ProjectName
		{
			get
			{
				return ProjectSettings.GetProjectName();
			}
		}

		/// <summary>
		/// Gets the project settings.
		/// </summary>
		public static audProjectSettings ProjectSettings { get; private set; }

		/// <summary>
		/// Gets the project settings asset path.
		/// </summary>
		public static string ProjectSettingsAssetPath
		{
			get
			{
				return ms_project.ProjectSettings;
			}
		}

		/// <summary>
		/// Gets the rave plugins.
		/// </summary>
		public static XmlNode[] RavePlugins
		{
			get
			{
				return ProjectSettings.GetRavePlugins();
			}
		}

		/// <summary>
		/// Gets or sets the selected plugin.
		/// </summary>
		public static IRAVEWaveImportPlugin SelectedPlugin { get; set; }

		/// <summary>
		/// Gets the soft root.
		/// </summary>
		public static string SoftRoot { get; private set; }

		/// <summary>
		/// Gets the sound wrapper sound type.
		/// </summary>
		public static string SoundWrapperSoundType
		{
			get
			{
				return SOUND_WRAPPER_SOUND_TYPE;
			}
		}

		/// <summary>
		/// Gets the wave attributes.
		/// </summary>
		public static string[] WaveAttributes
		{
			get
			{
				return ProjectSettings.GetWaveAttributes();
			}
		}

		/// <summary>
		/// Gets the wave slot attributes.
		/// </summary>
		public static IList<string> WaveSlotAttributes
		{
			get
			{
				return ProjectSettings.GetWaveSlotAttributes();
			}
		}

		/// <summary>
		/// Gets the wave build path.
		/// </summary>
		public static string WaveBuildPath { get; private set; }

		/// <summary>
		/// Gets the wave build tools path.
		/// </summary>
		public static string WaveBuildToolsPath { get; private set; }

		/// <summary>
		/// Gets or sets the wave editor path.
		/// </summary>
		public static string WaveEditorPath { get; set; }

		/// <summary>
		/// Gets the wave gesture context list.
		/// </summary>
		public static string WaveGestureContextList { get; private set; }

		/// <summary>
		/// Gets the wave slot file.
		/// </summary>
		public static string WaveSlotFile { get; private set; }

		/// <summary>
		/// Gets the wave wrapper sound type.
		/// </summary>
		public static string WaveWrapperSoundType
		{
			get
			{
				return WAVE_WRAPPER_SOUND_TYPE;
			}
		}

		/// <summary>
		/// Gets the working path.
		/// </summary>
		public static string WorkingPath { get; private set; }

		/// <summary>
		/// Gets te mute solo enabled types
		/// </summary>
		public static List<string> MuteSoloEnabledTypes
		{
			get
			{
				return ProjectSettings.MuteSoloEnabledObjectTypes;
			}
		}

		public static List<string> AlphabeticalSortHierarchyTpes
		{
			get
			{
				return ProjectSettings.AlphabeticalSortHierarchyTypes;
			}
		}

		//Gets the rave.drive root
		public static string RaveDriveRoot { get { return ProjectSettings.RaveDriveRoot; } }

		public static string PreviewFolder {get { return ProjectSettings.PreviewFolder; }}

		#endregion

		#region Public Methods and Operators

		/// <summary>
		/// The init project.
		/// </summary>
		/// <param name="project">
		/// The project.
		/// </param>
		public static void InitProject(audProjectEntry project)
		{
			ms_project = project;
			AssetManagementType = int.Parse(project.AssetManager);
			AssetPassword = project.Password;
			AssetProject = project.AssetProject;
			AssetServer = project.AssetServer;
			AssetUser = project.UserName;
			DepotRoot = project.DepotRoot;
			SoftRoot = project.SoftRoot;
			WorkingPath = project.WorkingPath;
			WaveEditorPath = project.WaveEditor;
		}

		/// <summary>
		/// The load project settings.
		/// </summary>
		/// <returns>
		/// The <see cref="bool"/>.
		/// </returns>
		public static bool LoadProjectSettings()
		{
			try
			{
				RaveInstance.AssetManager.GetLatestForceErrors(ms_project.ProjectSettings);
			}
			catch (Exception ex)
			{
				ErrorManager.HandleError(ex);
			}

			ProjectSettings =
				new audProjectSettings(RaveInstance.AssetManager.GetLocalPath(ms_project.ProjectSettings));


			return SetupPaths();
		}

		/// <summary>
		/// The resolve path.
		/// </summary>
		/// <param name="path">
		/// The path.
		/// </param>
		/// <returns>
		/// The <see cref="string"/>.
		/// </returns>
		public static string ResolvePath(string path)
		{
			return ProjectSettings.ResolvePath(path);
		}

		/// <summary>
		/// The set current platform.
		/// </summary>
		/// <param name="setting">
		/// The setting.
		/// </param>
		public static void SetCurrentPlatform(PlatformSetting setting)
		{
			ProjectSettings.SetCurrentPlatform(setting);
		}

		/// <summary>
		/// The set current platform by tag.
		/// </summary>
		/// <param name="platformTag">
		/// The platform tag.
		/// </param>
		public static void SetCurrentPlatformByTag(string platformTag)
		{
			ProjectSettings.SetCurrentPlatformByTag(platformTag);
		}

		/// <summary>
		/// The wave refs to skip.
		/// </summary>
		/// <returns>
		/// The <see cref="List"/>.
		/// </returns>
		public static List<string> WaveRefsToSkip()
		{
			return ProjectSettings.WaveRefsToSkip();
		}

		/// <summary>
		/// Get metadata type matching specified type name.
		/// </summary>
		/// <param name="typeName">
		/// The type name.
		/// </param>
		/// <returns>
		/// The metadata type <see cref="audMetadataType"/>.
		/// </returns>
		public static audMetadataType GetMetadataType(string typeName)
		{
			return MetadataTypes.FirstOrDefault(type => type.Type.ToUpper() == typeName.ToUpper());
		}

		/// <summary>
		/// Get metadata file matching specified type name.
		/// </summary>
		/// <param name="typeName">
		/// The type name.
		/// </param>
		/// <returns>
		/// The metadata file <see cref="audMetadataFile"/>.
		/// </returns>
		public static audMetadataFile GetMetadataFile(string typeName)
		{
			return MetadataFiles.FirstOrDefault(type => type.Type.ToUpper() == typeName.ToUpper());
		}





		#endregion

		#region Methods

		/// <summary>
		/// The setup paths.
		/// </summary>
		/// <returns>
		/// The <see cref="bool"/>.
		/// </returns>
		private static bool SetupPaths()
		{
			if (ProjectSettings.MetadataCompilerJitPaths != null)
			{
				var count = ProjectSettings.MetadataCompilerJitPaths.Length;
				MetadataCompilerJitPaths = new string[count];
				for (var i = 0; i < count; ++i)
				{
					MetadataCompilerJitPaths[i] = RaveInstance.AssetManager.GetLocalPath(ProjectSettings.MetadataCompilerJitPaths[i]);
					if (!ValidatePath(MetadataCompilerJitPaths[i]))
					{
						return false;
					}
				}
			}

			PlatformConfigPath =
				RaveInstance.AssetManager.GetLocalPath(ProjectSettings.GetBuildInfoPath())
						.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			if (!ValidatePath(PlatformConfigPath))
			{
				return false;
			}

			ObjectXmlPath =
				RaveInstance.AssetManager.GetLocalPath(ProjectSettings.GetSoundXmlPath())
						.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			if (!ValidatePath(ObjectXmlPath))
			{
				return false;
			}

			PlatformWavePath =
				RaveInstance.AssetManager.GetLocalPath(ProjectSettings.GetWaveInputPath())
						.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			if (!ValidatePath(PlatformWavePath))
			{
				return false;
			}

			// Working path may already be set from project settings,
			// so only set if not currently set.
			if (string.IsNullOrEmpty(WorkingPath))
			{
				WorkingPath = RaveInstance.AssetManager.GetLocalPath(string.Empty);
				if (string.IsNullOrWhiteSpace(WorkingPath))
				{
					WorkingPath = ProjectSettings.GetBackupWorkingPath();
				}
			}

			if (!ValidatePath(WorkingPath))
			{
				return false;
			}

			MetadataCompilerPath =
				RaveInstance.AssetManager.GetLocalPath(ProjectSettings.GetMetadataCompilerPath())
						.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			if (!ValidatePath(MetadataCompilerPath))
			{
				return false;
			}

			WaveSlotFile =
				RaveInstance.AssetManager.GetLocalPath(ProjectSettings.GetWaveSlotSettings())
						.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			if (!ValidatePath(WaveSlotFile))
			{
				return false;
			}

			WaveBuildPath = RaveInstance.AssetManager.GetLocalPath(ProjectSettings.GetBuildPath())
									.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			if (!ValidatePath(WaveBuildPath))
			{
				return false;
			}

			WaveBuildToolsPath =
				RaveInstance.AssetManager.GetLocalPath(ProjectSettings.GetBuildToolsPath())
						.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			if (!ValidatePath(WaveBuildToolsPath))
			{
				return false;
			}

			PresetPath = RaveInstance.AssetManager.GetLocalPath(ProjectSettings.GetPresetPath())
								 .Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			if (!ValidatePath(PresetPath))
			{
				return false;
			}

			if (!string.IsNullOrEmpty(ProjectSettings.GlobalVariablesList))
			{
				GlobalVariablesListPath =
					RaveInstance.AssetManager.GetLocalPath(ProjectSettings.GlobalVariablesList)
							.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
				if (!ValidatePath(GlobalVariablesListPath))
				{
					return false;
				}
			}

			if (!string.IsNullOrEmpty(ProjectSettings.WaveGestureContextList))
			{
				WaveGestureContextList =
					RaveInstance.AssetManager.GetLocalPath(ProjectSettings.WaveGestureContextList)
							.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			}

			return true;
		}

		/// <summary>
		/// The validate path.
		/// </summary>
		/// <param name="path">
		/// The path.
		/// </param>
		/// <returns>
		/// The <see cref="bool"/>.
		/// </returns>
		private static bool ValidatePath(string path)
		{
			var modifiedPath = path.Trim().ToLower();
			if (string.IsNullOrEmpty(modifiedPath)
				|| modifiedPath.Contains(Path.GetDirectoryName(Application.ExecutablePath).ToLower()))
			{
				ErrorManager.HandleInfo(
					string.Format("Invalid path: \"{0}\" detected! Rave will not be started.", path));
				return false;
			}

			return true;
		}


		#endregion


	}
}
