// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmBusy.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.Utils.Popups
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    using rage.ToolLib;

    using Rave.Utils.Infrastructure.Interfaces;

    /// <summary>
    /// Busy form.
    /// </summary>
    public class frmBusy : Form, IBusy
    {
        #region Fields

        /// <summary>
        /// The allow cancel.
        /// </summary>
        private readonly bool allowCancel;

        /// <summary>
        /// The cancel button.
        /// </summary>
        private Button cancelButton;

        /// <summary>
        /// The components.
        /// </summary>
        private IContainer components;

        /// <summary>
        /// The action label.
        /// </summary>
        private Label lblAction;

        /// <summary>
        /// The unknown duration flag.
        /// </summary>
        private bool unknownDuration;

        /// <summary>
        /// The picture box.
        /// </summary>
        private PictureBox pictureBox;

        /// <summary>
        /// The progress bar.
        /// </summary>
        private ProgressBar progressBar;

        /// <summary>
        /// The timer.
        /// </summary>
        private Timer timer;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="frmBusy"/> class.
        /// </summary>
        /// <param name="allowCancel">
        /// The allow cancel.
        /// </param>
        public frmBusy(bool allowCancel = false)
        {
            this.allowCancel = allowCancel;
            this.InitializeComponent();
            this.timer.Enabled = true;
            this.SetProgress(0);

            this.cancelButton.Visible = this.allowCancel;
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The cancelled.
        /// </summary>
        public event Action Cancelled;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The finished working.
        /// </summary>
        public void FinishedWorking()
        {
            this.timer.Enabled = false;
            this.Close();
        }

        /// <summary>
        /// Set the progress.
        /// </summary>
        /// <param name="percentage">
        /// The percentage.
        /// </param>
        public void SetProgress(int percentage)
        {
            if (percentage < this.progressBar.Minimum)
            {
                percentage = this.progressBar.Minimum;
            }

            if (percentage > this.progressBar.Maximum)
            {
                percentage = this.progressBar.Maximum;
            }

            this.progressBar.Value = percentage;
        }

        /// <summary>
        /// Set the status text.
        /// </summary>
        /// <param name="strAction">
        /// The action string.
        /// </param>
        public void SetStatusText(string strAction)
        {
            this.lblAction.Text = strAction;
        }

        /// <summary>
        /// Set unknown duration.
        /// </summary>
        /// <param name="flag">
        /// The flag.
        /// </param>
        public void SetUnknownDuration(bool flag)
        {
            this.unknownDuration = flag;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBusy));
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.lblAction = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.cancelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)this.pictureBox).BeginInit();
            this.SuspendLayout();

            // progressBar
            this.progressBar.Location = new System.Drawing.Point(64, 16);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(408, 16);
            this.progressBar.Step = 20;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar.TabIndex = 0;

            // lblAction
            this.lblAction.Location = new System.Drawing.Point(8, 48);
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new System.Drawing.Size(464, 54);
            this.lblAction.TabIndex = 1;
            this.lblAction.Text = "Working ...";

            // timer
            this.timer.Tick += new System.EventHandler(this.timer_Tick);

            // pictureBox1
            this.pictureBox.Image = (System.Drawing.Image)(resources.GetObject("pictureBox.Image"));
            this.pictureBox.Location = new System.Drawing.Point(16, 8);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(32, 32);
            this.pictureBox.TabIndex = 2;
            this.pictureBox.TabStop = false;

            // cancelButton
            this.cancelButton.Anchor =
                (System.Windows.Forms.AnchorStyles)
                ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right));
            this.cancelButton.Location = new System.Drawing.Point(395, 106);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);

            // frmBusy
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(482, 141);
            this.ControlBox = false;
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.lblAction);
            this.Controls.Add(this.progressBar);
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "frmBusy";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Working ...";
            this.TopMost = false;
            ((System.ComponentModel.ISupportInitialize)this.pictureBox).EndInit();
            this.ResumeLayout(false);
        }

        /// <summary>
        /// The cancel button_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Cancelled.Raise();
        }

        /// <summary>
        /// The timer_ tick.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void timer_Tick(object sender, EventArgs e)
        {
            if (this.unknownDuration)
            {
                if (this.progressBar.Value == this.progressBar.Maximum)
                {
                    this.progressBar.Value = this.progressBar.Minimum;
                }
                else
                {
                    this.progressBar.PerformStep();
                }
            }
        }

        #endregion
    }
}