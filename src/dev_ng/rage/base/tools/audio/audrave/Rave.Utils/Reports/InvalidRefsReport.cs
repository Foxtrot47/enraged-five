﻿// -----------------------------------------------------------------------
// <copyright file="InvalidRefsReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Utils.Reports
{
    using System.Collections.Generic;
    using System.IO;
    using System.Windows.Forms;

    using Rave.Controls.Forms.Popups;
    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Structs;

    /// <summary>
    /// Report that generates a list of invalid references from
    /// a particular location in the object structure hierarchy.
    /// </summary>
    public class InvalidRefsReport
    {
        /// <summary>
        /// The invalid references list.
        /// </summary>
        private IDictionary<IObjectInstance, IList<ObjectRef>> invalidRefs;

        /// <summary>
        /// Run the invalid references report.
        /// </summary>
        /// <param name="treeNode">
        /// The tree node to use as a starting point.
        /// </param>
        public void ShowReport(TreeNode treeNode)
        {
            this.invalidRefs = new Dictionary<IObjectInstance, IList<ObjectRef>>();
            this.FindInvalidRefs(treeNode);
            this.ShowReport();
        }

        /// <summary>
        /// Find invalid object references.
        /// </summary>
        /// <param name="treeNode">
        /// The current tree node.
        /// </param>
        private void FindInvalidRefs(TreeNode treeNode)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                var objNode = node as SoundNode;

                if (null != objNode)
                {
                    var obj = objNode.ObjectInstance;
                    foreach (var objectRef in obj.References)
                    {
                        if (null == objectRef.ObjectInstance)
                        {
                            // Found an invalid reference
                            if (!this.invalidRefs.ContainsKey(obj))
                            {
                                this.invalidRefs.Add(obj, new List<ObjectRef>());
                            }

                            this.invalidRefs[obj].Add(objectRef);
                        }
                    }
                }

                this.FindInvalidRefs(node);
            }
        }

        private void ShowReport()
        {
            var path = Path.Combine(Path.GetTempPath(), "invalid_refs_report.html");

            // Write results to temp file
            var filesteam = new FileStream(path, FileMode.Create, FileAccess.Write);
            var writer = new StreamWriter(filesteam);

            writer.WriteLine("<html><head>");
            writer.WriteLine("<style type=\"text/css\">");
            writer.WriteLine("<!--");
            writer.WriteLine("body, table");
            writer.WriteLine("{");
            writer.WriteLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
            writer.WriteLine("}");
            writer.WriteLine("table");
            writer.WriteLine("{");
            writer.WriteLine("border-collapse: collapse;");
            writer.WriteLine("border: 1px solid #CCC;");
            writer.WriteLine("margin: 8px 0px;");
            writer.WriteLine("}");
            writer.WriteLine("table td, table th");
            writer.WriteLine("{");
            writer.WriteLine("padding: 2px 15px;");
            writer.WriteLine("}");
            writer.WriteLine("table th");
            writer.WriteLine("{");
            writer.WriteLine("background: #FFC;");
            writer.WriteLine("font-weight: bold;");
            writer.WriteLine("}");
            writer.WriteLine("table td");
            writer.WriteLine("{");
            writer.WriteLine("border: 1px solid #CCC;");
            writer.WriteLine("padding: 0 0.5em;");
            writer.WriteLine("}");
            writer.WriteLine("-->");
            writer.WriteLine("</style>");
            writer.WriteLine("<title>Invalid References Report</title></head><body>");

            if (this.invalidRefs.Count == 0)
            {
                writer.WriteLine("No invalid references found");
            }
            else
            {
                writer.WriteLine("<table>");
                writer.WriteLine("<tr>");
                writer.WriteLine("<th>Object Name</th>");
                writer.WriteLine("<th>Object Bank</th>");
                writer.WriteLine("<th>Field</th>");
                writer.WriteLine("<th>Invalid Reference</th>");
                writer.WriteLine("</tr>");

                // Write the sound set details out to file
                foreach (var item in this.invalidRefs)
                {
                    var obj = item.Key;
                    foreach (var invalidRef in item.Value)
                    {
                        writer.WriteLine("<tr>");
                        writer.WriteLine("<td>" + obj.Name + "</td>");
                        writer.WriteLine("<td>" + obj.Bank.Name + "</td>");
                        writer.WriteLine("<td>" + invalidRef.Node.Name + "</td>");
                        writer.WriteLine("<td>" + invalidRef.Node.InnerText + "</td>");
                        writer.WriteLine("</tr>");
                    }
                }

                writer.WriteLine("</table>");
            }

            writer.WriteLine("</body></html>");

            writer.Close();
            filesteam.Close();

            // Display report by loading the saved HTML file
            var report = new frmReport("Invalid References Report");
            report.SetURL(path);
            report.Show();
        }
    }
}
