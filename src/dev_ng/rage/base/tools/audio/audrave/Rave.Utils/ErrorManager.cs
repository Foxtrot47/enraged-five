// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ErrorManager.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The error manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.Utils
{
    using System;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Utils.Popups;

    /// <summary>
    /// The error manager.
    /// </summary>
    public static class ErrorManager
    {
        #region Static Fields

        /// <summary>
        /// The output log.
        /// </summary>
        private static StreamWriter outputLog;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the number of errors.
        /// </summary>
        public static int NumErrors { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Initialize the error manager.
        /// </summary>
        /// <param name="logDirectory">
        /// The log directory.
        /// </param>
        public static void Init(string logDirectory)
        {
            if (string.IsNullOrEmpty(logDirectory))
            {
                return;
            }

            if (!Directory.Exists(logDirectory))
            {
                Directory.CreateDirectory(logDirectory);
            }

            var dateTime = DateTime.Now.ToString("yyyy_M_dd__H_M_s");
            var logFileName = dateTime + ".csv";
            var logFilePath = Path.Combine(logDirectory, logFileName);

            try
            {
                var file = new FileStream(logFilePath, FileMode.Create, FileAccess.Write);
                outputLog = new StreamWriter(file);
            }
            catch (Exception ex)
            {
                HandleError("Failed to initalize error log: " + ex.Message);
            }
        }

        /// <summary>
        /// Handle an error.
        /// </summary>
        /// <param name="message">
        /// The error message.
        /// </param>
        /// <param name="writeToLog">
        /// The write to log flag.
        /// </param>
        public static void HandleError(string message, bool writeToLog = false)
        {
            ShowError(message, string.Empty, writeToLog);
        }

        /// <summary>
        /// Handle an error.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="message">
        /// The error message message.
        /// </param>
        /// <param name="writeToLog">
        /// The write to log flag.
        /// </param>
        public static void HandleError(string context, string message, bool writeToLog = false)
        {
            ShowError(string.Format("Context: {0}{1}Message: {2}", context, Environment.NewLine, message), string.Empty, writeToLog);
        }

        /// <summary>
        /// Handle an error.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="writeToLog">
        /// The write to log flag.
        /// </param>
        public static void HandleError(Exception exception, bool writeToLog = false)
        {
            ShowError(exception.Message, exception.StackTrace, writeToLog);
        }

        /// <summary>
        /// Handle an exception.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        public static void HandleException(string context, Exception exception)
        {
            WriteToLog("Exception", context, exception.ToString());

            var messageBuilder = new StringBuilder();
            messageBuilder.Append(string.Format("Context: {0}", context));
            messageBuilder.Append(Environment.NewLine);
            messageBuilder.Append(Environment.NewLine);
            messageBuilder.Append(string.Format("Message: {0}", exception.Message));

            var stackTraceBuilder = new StringBuilder(exception.StackTrace);

            var tempEx = exception.InnerException;
            while (tempEx != null)
            {
                if (!string.IsNullOrEmpty(tempEx.Message))
                {
                    messageBuilder.Append(Environment.NewLine);
                    messageBuilder.Append("------------------------------------------------------");
                    messageBuilder.Append(Environment.NewLine);
                    messageBuilder.Append(string.Format("Message: {0}", tempEx.Message));
                }

                if (!string.IsNullOrEmpty(tempEx.StackTrace))
                {
                    stackTraceBuilder.Append(Environment.NewLine);
                    stackTraceBuilder.Append("------------------------------------------------------");
                    stackTraceBuilder.Append(Environment.NewLine);
                    stackTraceBuilder.Append(tempEx.StackTrace);
                }

                tempEx = tempEx.InnerException;
            }

            ShowError(messageBuilder.ToString(), stackTraceBuilder.ToString(), true);
        }

        /// <summary>
        /// Handle an info message.
        /// </summary>
        /// <param name="message">
        /// The info message.
        /// </param>
        /// <param name="writeToLog">
        /// The write to log flag.
        /// </param>
        public static void HandleInfo(string message, bool writeToLog = false)
        {
            MessageBox.Show(RaveInstance.ActiveWindow, message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Handle a question.
        /// </summary>
        /// <param name="message">
        /// The question message.
        /// </param>
        /// <param name="buttons">
        /// The buttons.
        /// </param>
        /// <param name="defaultResult">
        /// The default result.
        /// </param>
        /// <param name="writeToLog">
        /// The write to log flag.
        /// </param>
        /// <returns>
        /// The result <see cref="DialogResult"/>.
        /// </returns>
        public static DialogResult HandleQuestion(string message, MessageBoxButtons buttons, DialogResult defaultResult, bool writeToLog = false)
        {
            return MessageBox.Show(RaveInstance.ActiveWindow, message, "Information", buttons, MessageBoxIcon.Question);
        }

        /// <summary>
        /// Write message to log file.
        /// </summary>
        /// <param name="severity">
        /// The severity.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="details">
        /// The details.
        /// </param>
        public static void WriteToLog(string severity, string message, string details = "")
        {
            if (null == outputLog)
            {
                return;
            }

            try
            {
                var time = DateTime.Now.ToString("H:M:s");
                var line = string.Format("{0},{1},{2},{3}", time, severity, message, details);
                outputLog.WriteLine(line);
                outputLog.Flush();
            }
            catch (Exception)
            {
                // Do nothing
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Show an error.
        /// </summary>
        /// <param name="message">
        /// The error message.
        /// </param>
        /// <param name="stackTrace">
        /// The stack trace.
        /// </param>
        /// <param name="writeToLog">
        /// The write to log flag.
        /// </param>
        private static void ShowError(string message, string stackTrace, bool writeToLog)
        {
            NumErrors++;

            if (writeToLog)
            {
                WriteToLog("Error", message, stackTrace);
            }

            using (var errorForm = new frmError(message, stackTrace))
            {
                errorForm.ShowDialog();
            }
        }

        #endregion
    }
}