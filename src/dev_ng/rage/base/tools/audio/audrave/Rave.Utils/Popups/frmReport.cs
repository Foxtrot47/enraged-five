namespace Rave.Controls.Forms.Popups
{
    using System;
    using System.Text;
    using System.Windows.Forms;
    using System.IO;
    using System.Xml;
    using System.Reflection;
    using System.Xml.Xsl;

    public partial class frmReport : Form
    {
        private string m_FilePath;

        public frmReport(string title)
        {
            this.InitializeComponent();
            this.Text = title;
            this.BringToFront();
        }

        public void SetURL(string url)
        {
            this.m_FilePath = url;
            this.m_WebBrowser.Url = new Uri(url);
        }

        public void SetText(string text)
        {
            this.m_WebBrowser.DocumentText = text;
        }

        public void SetXml(XmlDocument doc)
        {
            Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("Rave.Controls.defaultss.xsl");

            XmlReader xr = XmlReader.Create(s);
            XslCompiledTransform xct = new XslCompiledTransform();
            xct.Load(xr);

            StringBuilder sb = new StringBuilder();
            XmlWriter xw = XmlWriter.Create(sb);
            xct.Transform(doc, xw);
            xw.Close();
            this.m_WebBrowser.DocumentText = sb.ToString();
            this.m_FilePath = "xml";
        }

        private void OnClose(object sender, FormClosingEventArgs e)
        {
            if(this.m_FilePath !="" && File.Exists(this.m_FilePath))
            {
                File.Delete(this.m_FilePath);
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
          
            if (this.m_FilePath==null || this.m_FilePath.EndsWith("html"))
            {
                this.saveFileDialog1.Filter = "html files (*.html)|*.html";
            }
            else if (this.m_FilePath.EndsWith("xml"))
            {
                this.saveFileDialog1.Filter = "xml files (*.xml)|*.xml";
            }
            else
            {
                this.saveFileDialog1.Filter = "txt files (*.txt)|*.txt|csv files (*.csv)|*.csv";
            }

            if (this.saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Stream mystream;
                if ((mystream = this.saveFileDialog1.OpenFile()) != null)
                {
                    StreamReader sr = new StreamReader(this.m_FilePath);
                    StreamWriter sw = new StreamWriter(mystream);
                    sw.Write(sr.ReadToEnd());
                    sr.Close();
                    sw.Close();
                    mystream.Close();
                }
            }
            this.saveFileDialog1.Dispose();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}