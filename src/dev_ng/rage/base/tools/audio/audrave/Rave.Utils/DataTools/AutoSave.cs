﻿// -----------------------------------------------------------------------
// <copyright file="AutoSave.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Utils.DataTools
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Threading;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Instance;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Utils;

    /// <summary>
    /// Auto save.
    /// </summary>
    public class AutoSave
    {
        /// <summary>
        /// The auto save path.
        /// </summary>
        public const string AutoSavePath = "Rave\\AutoSave";

        /// <summary>
        /// The save frequency (milliseconds).
        /// </summary>
        public const int SaveFrequency = 60000;

        /// <summary>
        /// The max age of auto-saved files before they are deleted (days).
        /// </summary>
        public const int MaxAge = 7;

        /// <summary>
        /// The auto save full path.
        /// </summary>
        private readonly string AutoSaveFullPath;

        /// <summary>
        /// The thread.
        /// </summary>
        private readonly Thread thread;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoSave"/> class.
        /// </summary>
        public AutoSave(string projectName)
        {
            AutoSaveFullPath = Path.Combine(Path.Combine(Path.GetTempPath(), AutoSavePath), projectName);
            if (!Directory.Exists(AutoSaveFullPath))
            {
                Directory.CreateDirectory(AutoSaveFullPath);
            }
            this.RunCleanUp();
            this.thread = new Thread(this.Run) { IsBackground = true };
        }

        /// <summary>
        /// The run auto restore process.
        /// This allows the user to restore a backed-up version of a file if the
        /// file in the live location is older than that in the backed-up location.
        /// </summary>
        public void RunAutoRestore()
        {
            lock (RaveInstance.CheckedOutBanks)
            {
                foreach (var bank in RaveInstance.CheckedOutBanks.ToList())
                {
                    // Determine paths and file names
                    var srcFilePath = bank.FilePath;
                    var subPath = srcFilePath.Replace(Configuration.ObjectXmlPath, string.Empty);
                    var tempPath = Path.Combine(AutoSaveFullPath, subPath);

                    if (!Directory.Exists(tempPath))
                    {
                        continue;
                    }

                    var dirInfo = new DirectoryInfo(tempPath);
                    var backedUpFileInfo = dirInfo.GetFiles().OrderByDescending(f => f.LastWriteTime).FirstOrDefault();

                    if (null == backedUpFileInfo)
                    {
                        continue;
                    }

                    var srcFileInfo = new FileInfo(srcFilePath);
                    if (backedUpFileInfo.LastWriteTime <= srcFileInfo.LastWriteTime)
                    {
                        continue;
                    }

                    // Also check checksums of files - no need to prompt for restore if files are identical
                    var md5 = MD5.Create();
                    var srcFileStream = File.OpenRead(srcFilePath);
                    var srcMd5 = md5.ComputeHash(srcFileStream);
                    srcFileStream.Close();

                    var backupMd5FileStream = File.OpenRead(backedUpFileInfo.FullName);
                    var backupMd5 = md5.ComputeHash(backupMd5FileStream);
                    backupMd5FileStream.Close();
                    md5.Clear();

                    if (srcMd5.SequenceEqual(backupMd5))
                    {
                        continue;
                    }

                    // Backup file is newer than live version
                    var question =
                        string.Format(
                            "Latest backed-up version of this file is newer than live version - restore from backup?{0}"
                            + "Backed-up timestamp: {1}Live timestamp: {2}Backup path: {3}Live path: {4}",
                            Environment.NewLine + Environment.NewLine,
                            backedUpFileInfo.LastWriteTime.ToString("HH:mm:ss dd/MM/yy") + Environment.NewLine,
                            srcFileInfo.LastWriteTime.ToString("HH:mm:ss dd/MM/yy") + Environment.NewLine + Environment.NewLine,
                            backedUpFileInfo.FullName + Environment.NewLine,
                            srcFilePath);

                    var result = MessageBox.Show(
                        question,
                        "Auto Restore File",
                        MessageBoxButtons.OKCancel,
                        MessageBoxIcon.Warning);

                    if (result != DialogResult.OK)
                    {
                        continue;
                    }

                    // Create backup of current live file (don't want to lose any data)
                    SaveBank(bank, true);

                    // Replace live version with latest backed-up version
                    try
                    {
                        srcFileInfo.IsReadOnly = false;
                        File.Copy(backedUpFileInfo.FullName, srcFilePath, true);
                        bank.Reload();
                        MessageBox.Show(
                            "File restored from backup successfully",
                            "Backup Restored Successfully");
                    }
                    catch (Exception ex)
                    {
                        ErrorManager.HandleError("Failed to restore file: " + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Clean up any old auto-saved files.
        /// </summary>
        public void RunCleanUp()
        {
            this.RunCleanUp(AutoSaveFullPath);
        }

        /// <summary>
        /// Start the auto save thread.
        /// </summary>
        public void Start()
        {
            if (null != this.thread && !this.thread.IsAlive)
            {
                this.thread.Start();
            }
        }

        /// <summary>
        /// Stop the auto save thread.
        /// </summary>
        public void Stop()
        {
            // TODO
        }

        /// <summary>
        /// Save a bank.
        /// </summary>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="forceSave">
        /// Force save the bank.
        /// </param>
        private void SaveBank(IXmlBank bank, bool forceSave = false)
        {
            lock (bank)
            {
                // Determine paths and file names
                var fullPath = bank.FilePath;
                var fileNameNoExt = Path.GetFileNameWithoutExtension(fullPath);
                var ext = Path.GetExtension(fullPath);
                var subPath = fullPath.Replace(Configuration.ObjectXmlPath, string.Empty);
                var tempPath = Path.Combine(AutoSaveFullPath, subPath);

                // Create temp directory if needed and determine number of existing
                // backups of current file
                var currFileCount = 0;
                if (!Directory.Exists(tempPath))
                {
                    Directory.CreateDirectory(tempPath);
                }
                else
                {
                    currFileCount = new DirectoryInfo(tempPath).GetFiles().Length;
                }

                lock (bank.Document)
                {
                    // No need to save another version if no changes made recently
                    if (!forceSave && !ChangesMadeToFile(tempPath, bank))
                    {
                        return;
                    }

                    var newFileName = fileNameNoExt + "_" + (++currFileCount) + ext;
                    var savePath = Path.Combine(tempPath, newFileName);

                    // Make sure we're not overwriting an existing file
                    while (File.Exists(savePath))
                    {
                        newFileName = fileNameNoExt + "_" + (++currFileCount) + ext;
                        savePath = Path.Combine(tempPath, newFileName);
                    }

                    var doc = bank.Document;
                    try
                    {
                        doc.Save(savePath);
                    }
                    catch (Exception ex)
                    {
                        // TODO
                    }
                }
            }
        }

        /// <summary>
        /// Returns value indicating whether changes have been made to a file since the last version was saved.
        /// </summary>
        /// <param name="fileDir">
        /// The file directory path.
        /// </param>
        /// <param name="bank">
        /// The bank to be saved.
        /// </param>
        /// <returns>
        /// Value indicating whether changes have been made <see cref="bool"/>.
        /// </returns>
        private static bool ChangesMadeToFile(string fileDir, IXmlBank bank)
        {
            if (!Directory.Exists(fileDir))
            {
                return true;
            }

            // Get most recently created saved version of bank, if it exists
            var directory = new DirectoryInfo(fileDir);
            var newestFile =
                (from f in directory.GetFiles()
                 orderby f.CreationTime descending
                 select f).FirstOrDefault();

            if (null == newestFile)
            {
                return true;
            }

            // Check if XDocument has been modified since last save
            try
            {
                var lastSavedDoc = new XmlDocument();
                var stream = newestFile.OpenRead();
                lastSavedDoc.Load(stream);
                if (lastSavedDoc.InnerText == bank.Document.InnerText)
                {
                    // Files are identical, no need to save another copy
                    stream.Close();
                    return false;
                }

                stream.Close();
            }
            catch (Exception ex)
            {
                // TODO
            }

            return true;
        }

        /// <summary>
        /// Clean up any old files.
        /// </summary>
        /// <param name="dirPath">
        /// The directory path.
        /// </param>
        private void RunCleanUp(string dirPath)
        {
            foreach (var subDirPath in Directory.GetDirectories(dirPath))
            {
                foreach (var filePath in Directory.GetFiles(subDirPath))
                {
                    // Delete any files older than MaxAge
                    var fileInfo = new FileInfo(filePath);
                    if (fileInfo.LastWriteTime > DateTime.Now.AddDays(MaxAge * -1))
                    {
                        continue;
                    }

                    try
                    {
                        fileInfo.Delete();
                    }
                    catch (Exception ex)
                    {
                        // TODO
                    }
                }

                this.RunCleanUp(subDirPath);

                // Delete directory if now empty
                if (!Directory.EnumerateFileSystemEntries(subDirPath).Any())
                {
                    Directory.Delete(subDirPath);
                }
            }
        }

        /// <summary>
        /// Run the auto save process.
        /// </summary>
        private void Run()
        {
            while (true)
            {
                this.SaveCheckedOutBanks();
                Thread.Sleep(SaveFrequency);
            }
        }

        /// <summary>
        /// Save checked out banks.
        /// </summary>
        private void SaveCheckedOutBanks()
        {
            lock (RaveInstance.CheckedOutBanks)
            {
                var banks = RaveInstance.CheckedOutBanks;
                foreach (var bank in banks.ToList())
                {
                    SaveBank(bank);
                }
            }
        }
    }
}
