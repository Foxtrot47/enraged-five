﻿// -----------------------------------------------------------------------
// <copyright file="SoundSetReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Utils.Reports
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using Rave.Controls.Forms.Popups;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    /// Report that generates details of a sound set, including:
    /// - Sound set name
    /// - The name of each sound in the sound set
    /// - The names of any non-resident banks required for each sound
    /// </summary>
    public class SoundSetReport
    {
        /// <summary>
        /// The sound set objects.
        /// </summary>
        private readonly IList<IObjectInstance> soundSetObjs;

        /// <summary>
        /// The include table of contents flag.
        /// </summary>
        private readonly bool includeTableOfContents;

        /// <summary>
        /// The sound wave banks list.
        /// </summary>
        private Dictionary<IObjectInstance, HashSet<string>> soundWaveBanks;

        /// <summary>
        /// Initializes a new instance of the <see cref="SoundSetReport"/> class.
        /// </summary>
        /// <param name="outputPath">
        /// The output path.
        /// </param>
        /// <param name="soundSetObjs">
        /// The sound set objects.
        /// </param>
        /// <param name="includeTableOfContents">
        /// The include table of contents flag.
        /// </param>
        public SoundSetReport(string outputPath, IList<IObjectInstance> soundSetObjs, bool includeTableOfContents = false)
        {
            this.OutputPath = outputPath ?? Path.GetTempFileName() + ".html";
            this.soundSetObjs = soundSetObjs.OrderBy(obj => obj.Name).ToList();
            this.includeTableOfContents = includeTableOfContents;
        }

        /// <summary>
        /// Gets the output path.
        /// </summary>
        public string OutputPath { get; private set; }

        /// <summary>
        /// Show the report.
        /// </summary>
        public void ShowReport()
        {
            this.SaveReport();

            // Display report by loading the saved HTML file
            var report = new frmReport("Sound Set Report");
            report.SetURL(this.OutputPath);
            report.Show();
        }

        /// <summary>
        /// The save report.
        /// </summary>
        public void SaveReport()
        {
            // Write results to temp file
            var filesteam = new FileStream(this.OutputPath, FileMode.Create, FileAccess.Write);
            var writer = new StreamWriter(filesteam);

            writer.WriteLine("<html><head>");
            writer.WriteLine("<style type=\"text/css\">");
            writer.WriteLine("<!--");
            writer.WriteLine("body, table");
            writer.WriteLine("{");
            writer.WriteLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
            writer.WriteLine("}");
            writer.WriteLine("table.sound-set");
            writer.WriteLine("{");
            writer.WriteLine("border-collapse: collapse;");
            writer.WriteLine("border: 1px solid #CCC;");
            writer.WriteLine("margin: 8px 0px;");
            writer.WriteLine("}");
            writer.WriteLine("table.sound-set td, table.sound-set th");
            writer.WriteLine("{");
            writer.WriteLine("padding: 2px 15px;");
            writer.WriteLine("}");
            writer.WriteLine("table.sound-set th");
            writer.WriteLine("{");
            writer.WriteLine("background: #FFC;");
            writer.WriteLine("font-weight: bold;");
            writer.WriteLine("}");
            writer.WriteLine("table.sound-set td");
            writer.WriteLine("{");
            writer.WriteLine("border: 1px solid #CCC;");
            writer.WriteLine("padding: 0 0.5em;");
            writer.WriteLine("}");
            writer.WriteLine("-->");
            writer.WriteLine("</style>");
            writer.WriteLine("<title>Sound Set Report</title></head><body>");
            
            this.WriteTableOfContents(writer);

            // Write the sound set details out to file
            foreach (var objInstance in this.soundSetObjs)
            {
                this.WriteSoundSetDetails(objInstance, writer);
            }

            writer.WriteLine("</body></html>");

            writer.Close();
            filesteam.Close();
        }

        /// <summary>
        /// Write the table of contents.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        private void WriteTableOfContents(StreamWriter writer)
        {
            if (this.includeTableOfContents)
            {
                writer.WriteLine("<table>");

                foreach (var soundSetObj in this.soundSetObjs)
                {
                    writer.WriteLine("<tr><td><a style=\"font-size: 0.8em;\" href=\"#" + soundSetObj.Name + "\">" + soundSetObj.Name + "</a></td></tr>");
                }

                writer.WriteLine("</table>");
            }
        }

        /// <summary>
        /// Write the sound set details to the report file.
        /// </summary>
        /// <param name="objInstance">
        /// The sound set object.
        /// </param>
        /// <param name="writer">
        /// The report writer.
        /// </param>
        private void WriteSoundSetDetails(IObjectInstance objInstance, StreamWriter writer)
        {
            // Only want to process SoundSet objects
            if (!objInstance.TypeName.Equals("SoundSet"))
            {
                return;
            }

            // Initialize sound wave banks list
            this.soundWaveBanks = new Dictionary<IObjectInstance, HashSet<string>>();
            foreach (var reference in objInstance.References)
            {
                var objectInstance = reference.ObjectInstance;
                this.soundWaveBanks[objectInstance] = new HashSet<string>();
                this.InitSoundWaveBanksList(objectInstance, objectInstance);
            }

            // Header
            writer.WriteLine("<h3 id=\"" + objInstance.Name + "\">" + objInstance.Name + "</h3>");

            // Banks required
            writer.WriteLine("<table class=\"sound-set\">");
            writer.WriteLine("<tr style=\"background-color:#EEE;\">");
            writer.WriteLine("<th style=\"padding:10px;\">Sound Name</th><th style=\"padding:10px;\">Wave Bank</th><th style=\"padding:10px;\">Comment</th></tr>");

            // Generate output for each sound node in sound set
            var soundNodes = objInstance.Node.SelectNodes("Sounds");
            if (null != soundNodes)
            {
                foreach (XmlNode soundsNode in soundNodes)
                {
                    var soundNode = soundsNode.SelectSingleNode("Sound");

                    if (null == soundNode)
                    {
                        continue;
                    }

                    var sound = soundNode.InnerText;

                    // Find soundWaveBanks entry for current sound
                    IObjectInstance soundObj = null;
                    IList<string> waveBanks = null;
                    foreach (var kvp in this.soundWaveBanks)
                    {
                        if (kvp.Key.Name.Equals(sound))
                        {
                            soundObj = kvp.Key;
                            waveBanks = kvp.Value.ToList();
                            break;
                        }
                    }

                    // Should never happen, but check in case soundObj not found
                    if (null == soundObj)
                    {
                        continue;
                    }

                    // Comment is optional
                    var commentNode = soundsNode.SelectSingleNode("Comments");
                    var comment = string.Empty;
                    if (null != commentNode)
                    {
                        comment = commentNode.InnerText;
                    }

                    // Output HTML row for current sound
                    writer.WriteLine(
                        "<tr style=\"font-size: 0.8em;\"><td style=\"padding:10px;\">" + soundsNode.SelectSingleNode("Name").InnerText
                        + "</td><td style=\"padding:10px;\">");

                    if (waveBanks.Any())
                    {
                        for (var i = 0; i < waveBanks.Count - 1; i++)
                        {
                            writer.WriteLine(waveBanks[i] + "<br />");
                        }

                        writer.WriteLine(waveBanks.Last());
                    }

                    writer.WriteLine("</td><td style=\"padding:10px;\">" + comment + "</td></tr>");
                }
            }

            writer.WriteLine("</table>");
        }

        /// <summary>
        /// Initialize the list of sound wave banks list.
        /// </summary>
        /// <param name="parentSound">
        /// The parent sound.
        /// </param>
        /// <param name="objInstance">
        /// The current object instance.
        /// </param>
        private void InitSoundWaveBanksList(IObjectInstance parentSound, IObjectInstance objInstance)
        {
            if (null == objInstance)
            {
                return;
            }

            // Stop searching if we hit a StreamingSound. Script team don't load the 
            // bank for streams directly, so not required in report
            if (objInstance.TypeName.Equals("StreamingSound"))
            {
                if (this.ContainsStreamingSoundRef(objInstance))
                {
                    this.soundWaveBanks[parentSound].Add("(stream)");
                }
            }
            else
            {
                // Store name of non-resident pack wave refs of all simple sound child nodes
                if (objInstance.TypeName.Equals("SimpleSound"))
                {
                    var waveRefNode = objInstance.Node.SelectSingleNode("WaveRef");
                    if (null != waveRefNode)
                    {
                        var bankNameNode = waveRefNode.SelectSingleNode("BankName");
                        if (null != bankNameNode)
                        {
                            var bankName = bankNameNode.InnerText;

                            // Ignore resident pack (these waves are always loaded in game)
                            if (!bankName.ToLower().StartsWith("resident"))
                            {
                                var splitPath = bankName.Split('\\');
                                this.soundWaveBanks[parentSound].Add(splitPath[splitPath.Length - 1]);
                            }
                        }
                    }
                }

                // Recursively process the children of current object instance
                foreach (var childObj in objInstance.References)
                {
                    this.InitSoundWaveBanksList(parentSound, childObj.ObjectInstance);
                }
            }
        }

        /// <summary>
        /// Indicates whether object instance contains wave ref in STREAMS bank.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <returns>
        /// True/false <see cref="bool"/>.
        /// </returns>
        private bool ContainsStreamingSoundRef(IObjectInstance objectInstance)
        {
            if (null == objectInstance)
            {
                return false;
            }

            foreach (var reference in objectInstance.References)
            {
                var waveRefNode = reference.ObjectInstance.Node.SelectSingleNode("WaveRef");
                if (null != waveRefNode)
                {
                    var bankNameNode = waveRefNode.SelectSingleNode("BankName");
                    if (null != bankNameNode)
                    {
                        var bankName = bankNameNode.InnerText;
                        if (bankName.ToLower().StartsWith("streams"))
                        {
                            return true;
                        }
                    }
                }

                if (this.ContainsStreamingSoundRef(reference.ObjectInstance))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
