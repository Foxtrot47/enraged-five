// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Utility.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Static utility functions
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.Utils
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml;

    using rage;
    using rage.ToolLib;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    /// <summary>
    /// Rave utilities.
    /// </summary>
    public sealed class RaveUtils
    {
        #region Public Methods and Operators

        public static void CreateRequiredDirectories(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static string FormatGameString(string str)
        {
            return str.Replace(" ", "_");
        }

        public static bool StringIsValidObjectName(string str)
        {
            return str.All(c => (char.IsLetterOrDigit(c) || c == '_' || c == '-'));
        }

        public static bool StringIsValidWaveOrMidiOrGrnOrTxtName(string str)
        {
            if (!str.EndsWith(".WAV", StringComparison.OrdinalIgnoreCase) &&
                !str.EndsWith(".MID", StringComparison.OrdinalIgnoreCase) &&
				!str.EndsWith(".TXT", StringComparison.OrdinalIgnoreCase) &&
                !str.EndsWith(".GRN", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }
            return str.All(c => (char.IsLetterOrDigit(c) || c == '_' || c == '-' || c == '.'));
        }

        public static bool IsWaveReference(XmlNode node)
        {
            if (node.Name == "WaveRef")
            {
                return true;
            }

            if (node.ChildNodes.Count == 2)
            {
                var names = node.ChildNodes.Cast<XmlNode>().Select(x => x.Name).ToList();
                return names.Contains("WaveName") && names.Contains("BankName");
            }
            return false;
        }

        public static bool IsPopulatedWaveReference(XmlNode node)
        {
            return IsWaveReference(node) && node.ChildNodes.Count == 2;
        }

        /// <summary>
        /// The create object menus.
        /// </summary>
        /// <param name="objectTypes">
        /// The object types.
        /// </param>
        /// <param name="clickHandler">
        /// The click handler.
        /// </param>
        /// <param name="parentMenuItem">
        /// The parent menu item.
        /// </param>
        public static void CreateObjectMenus(
            ITypeDefinitions objectTypes, EventHandler clickHandler, ToolStripMenuItem parentMenuItem)
        {
            if (objectTypes.ObjectTypes == null)
            {
                return;
            }

            if (objectTypes.ObjectTypes.Length == 1)
            {
                parentMenuItem.Text += " " + objectTypes.ObjectTypes[0].Name;
                parentMenuItem.Click += clickHandler;
            }
            else
            {
                foreach (ITypeDefinition typeDef in objectTypes.ObjectTypes)
                {
                    if (typeDef.IsAbstract)
                    {
                        continue;
                    }

                    var m = new ToolStripMenuItem(typeDef.Name, null, clickHandler);

                    if (typeDef.Group == null)
                    {
                        if (parentMenuItem.DropDownItems.Count == 0)
                        {
                            parentMenuItem.DropDownItems.Add(m);
                        }
                        else
                        {
                            // no group specified, stick it in the menu root
                            for (int i = 0; i < parentMenuItem.DropDownItems.Count; i++)
                            {
                                if (string.Compare(m.Text, parentMenuItem.DropDownItems[i].Text) <= 0)
                                {
                                    parentMenuItem.DropDownItems.Insert(i, m);
                                    break;
                                }

                                if (i == (parentMenuItem.DropDownItems.Count - 1))
                                {
                                    parentMenuItem.DropDownItems.Add(m);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        ITypeDefinition definition = typeDef;
                        ToolStripMenuItem menuItem =
                            parentMenuItem.DropDownItems.Cast<ToolStripMenuItem>()
                                          .FirstOrDefault(mi => mi.Text == definition.Group);
                        if (menuItem == null)
                        {
                            menuItem = new ToolStripMenuItem(typeDef.Group);
                            parentMenuItem.DropDownItems.Add(menuItem);
                        }

                        if (menuItem.DropDownItems.Count == 0)
                        {
                            menuItem.DropDownItems.Add(m);
                        }
                        else
                        {
                            for (int i = 0; i < menuItem.DropDownItems.Count; i++)
                            {
                                if (string.Compare(m.Text, menuItem.DropDownItems[i].Text) <= 0)
                                {
                                    menuItem.DropDownItems.Insert(i, m);
                                    break;
                                }

                                if (i == menuItem.DropDownItems.Count - 1)
                                {
                                    menuItem.DropDownItems.Add(m);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The find linked metadata files.
        /// </summary>
        /// <param name="metadataFile">
        /// The metadata file.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public static IEnumerable<audMetadataFile> FindLinkedMetadataFiles(audMetadataFile metadataFile)
        {
            if (metadataFile == null)
            {
                return null;
            }

            if (!metadataFile.ShowInBrowser)
            {
                return null;
            }

            List<audMetadataFile> candidateMetadataFiles = (from candidateMetadataFile in Configuration.MetadataFiles
                                                            where
                                                                candidateMetadataFile != metadataFile
                                                                && candidateMetadataFile.Type.Equals(metadataFile.Type, StringComparison.OrdinalIgnoreCase)
                                                                && (!candidateMetadataFile.ShowInBrowser)
                                                                && !candidateMetadataFile.Episode.Equals(metadataFile.Episode, StringComparison.OrdinalIgnoreCase)
                                                            select candidateMetadataFile).ToList();

            var linkedMetadataFiles = new List<audMetadataFile>();
            string metadataFileDataPath = GetMetadataFileDataPath(metadataFile);
            foreach (audMetadataFile candidateMetadataFile in candidateMetadataFiles)
            {
                string candidateMetadataFileDataPath = GetMetadataFileDataPath(candidateMetadataFile);
                if (string.Compare(metadataFileDataPath, candidateMetadataFileDataPath) == 0)
                {
                    linkedMetadataFiles.Add(candidateMetadataFile);
                }
            }

            return linkedMetadataFiles.Count > 0 ? linkedMetadataFiles : null;
        }

        /// <summary>
        /// The find metadata type from hash.
        /// </summary>
        /// <param name="hash">
        /// The hash.
        /// </param>
        /// <returns>
        /// The <see cref="audMetadataType"/>.
        /// </returns>
        public static audMetadataType FindMetadataTypeFromHash(uint hash)
        {
            return (from type in Configuration.MetadataTypes
                    let typeHash = new Hash { Value = type.Type }
                    where typeHash.Key == hash
                    select type).FirstOrDefault();
        }

        /// <summary>
        /// The find metadata type from name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="audMetadataType"/>.
        /// </returns>
        public static audMetadataType FindMetadataTypeFromName(string name)
        {
            return Configuration.MetadataTypes.FirstOrDefault(type => type.Type.Equals(name, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// The load tag icons.
        /// </summary>
        /// <param name="baseIconPath">
        /// The base icon path.
        /// </param>
        /// <param name="iconTable">
        /// The icon table.
        /// </param>
        /// <param name="iconList">
        /// The icon list.
        /// </param>
        public static void LoadTagIcons(string baseIconPath, Hashtable iconTable, ImageList iconList)
        {
            int baseIndex = iconList.Images.Count;

            iconList.Images.Add(new Icon(baseIconPath + "default" + "B" + ".ico"));
            iconList.Images.Add(new Icon(baseIconPath + "default" + "P" + ".ico"));
            iconList.Images.Add(new Icon(baseIconPath + "default" + "L" + ".ico"));
            iconList.Images.Add(new Icon(baseIconPath + "default" + "N" + ".ico"));

            iconList.Images.Add(new Icon(baseIconPath + "default" + "B" + ".ico"));
            iconList.Images.Add(new Icon(baseIconPath + "default" + "P" + ".ico"));
            iconList.Images.Add(new Icon(baseIconPath + "default" + "L" + ".ico"));
            iconList.Images.Add(new Icon(baseIconPath + "default" + "N" + ".ico"));

            int curIndex = baseIndex + 8;
            foreach (string tag in Configuration.WaveAttributes)
            {
                int indexToUse;
                try
                {
                    if (File.Exists(baseIconPath + tag + "B" + ".ico"))
                    {
                        iconList.Images.Add(new Icon(baseIconPath + tag + "B" + ".ico"));
                        iconList.Images.Add(new Icon(baseIconPath + tag + "P" + ".ico"));
                        iconList.Images.Add(new Icon(baseIconPath + tag + "L" + ".ico"));
                        iconList.Images.Add(new Icon(baseIconPath + tag + "N" + ".ico"));

                        iconList.Images.Add(new Icon(baseIconPath + tag + "B" + ".ico"));
                        iconList.Images.Add(new Icon(baseIconPath + tag + "P" + ".ico"));
                        iconList.Images.Add(new Icon(baseIconPath + tag + "L" + ".ico"));
                        iconList.Images.Add(new Icon(baseIconPath + tag + "N" + ".ico"));
                        indexToUse = curIndex;
                        curIndex += 8;
                    }
                    else
                    {
                        indexToUse = baseIndex;
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                    indexToUse = baseIndex;
                }

                iconTable.Add(tag, indexToUse);
            }
        }

        /// <summary>
        /// The load icons.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="iconTable">
        /// The icon table.
        /// </param>
        /// <param name="iconList">
        /// The icon list.
        /// </param>
        public static void LoadIcons(string path, Hashtable iconTable, ImageList iconList)
        {
            var curIndex = iconList.Images.Count;

            // Load a single icon file
            if (File.Exists(path))
            {
                var ext = Path.GetExtension(path);
                if (string.IsNullOrEmpty(ext) || ext.ToLower() != ".ico")
                {
                    ErrorManager.HandleError("Not a valid icon file: " + path);
                    return;
                }

                iconList.Images.Add(new Icon(path));
                var iconName = Path.GetFileNameWithoutExtension(path);
                iconTable.Add(iconName, curIndex);
            }
            else
            {
                // Load a directory if icon files
                if (!Directory.Exists(path))
                {
                    ErrorManager.HandleError("Failed to find icon file/folder: " + path);
                    return;
                }

                var files = Directory.GetFiles(path, "*.ico");
                foreach (var file in files)
                {
                    iconList.Images.Add(new Icon(file));
                    var iconName = Path.GetFileNameWithoutExtension(file);
                    iconTable.Add(iconName, curIndex);
                    curIndex++;
                }
            }
        }

        /// <summary>
        /// The load type icons.
        /// </summary>
        /// <param name="baseIconPath">
        /// The base icon path.
        /// </param>
        /// <param name="iconTable">
        /// The icon table.
        /// </param>
        /// <param name="iconList">
        /// The icon list.
        /// </param>
        /// <param name="typeDefs">
        /// The type defs.
        /// </param>
        /// <param name="useDefault">
        /// The use default.
        /// </param>
        public static void LoadTypeIcons(
            string baseIconPath, Hashtable iconTable, ImageList iconList, ITypeDefinitions typeDefs, bool useDefault)
        {
            // load default icon
            iconList.Images.Add(new Icon(baseIconPath + "default.ico"));

            // try to load custom sound type icons
            int curIndex = iconList.Images.Count;
            int baseIndex = curIndex - 1;

            var usedIcons = new Dictionary<string, int>();

            if (typeDefs.ObjectTypes != null)
            {
                foreach (ITypeDefinition td in typeDefs.ObjectTypes)
                {
                    int indexToUse;
                    try
                    {
                        if (File.Exists(baseIconPath + td.Name + ".ico"))
                        {
                            string iconPath = baseIconPath + td.Name + ".ico";
                            iconList.Images.Add(new Icon(iconPath));
                            indexToUse = curIndex++;
                            usedIcons.Add(iconPath, indexToUse);
                        }
                        else
                        {
                            if (td.InheritsFrom != null)
                            {
                                ITypeDefinition current = td.TypeDefinitions.FindTypeDefinition(td.InheritsFrom);
                                indexToUse = baseIndex;

                                while (current != null)
                                {
                                    var parentIconPath = baseIconPath + current.Name + ".ico";

                                    if (usedIcons.ContainsKey(parentIconPath))
                                    {
                                        indexToUse = usedIcons[parentIconPath];
                                        break;
                                    }

                                    current = current.TypeDefinitions.FindTypeDefinition(current.InheritsFrom);
                                }
                            }
                            else
                            {
                                indexToUse = baseIndex;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                        indexToUse = baseIndex;
                    }

                    if (useDefault || indexToUse != baseIndex)
                    {
                        iconTable.Add(td.Name, indexToUse);
                    }
                }
            }
        }


        /// <summary>
        /// Loads the type icons.
        /// </summary>
        /// <param name="baseIconPath">The base icon path.</param>
        /// <param name="typeDefs">The type defs.</param>
        /// <returns>Icon dictionary</returns>
        public static void LoadTypeIcons(
            string baseIconPath, ITypeDefinitions typeDefs, Dictionary<string, string> iconDictionary)
        {
            iconDictionary["default"] = (baseIconPath + "default.ico");

            if (typeDefs.ObjectTypes != null)
            {
                foreach (ITypeDefinition td in typeDefs.ObjectTypes)
                {
                    try
                    {
                        if (File.Exists(baseIconPath + td.Name + ".ico"))
                        {
                            string iconPath = baseIconPath + td.Name + ".ico";
                            iconDictionary[td.Name] = iconPath;
                        }
                        else
                        {
                            iconDictionary[td.Name] = iconDictionary["default"];
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                    }

                }
            }


        }


        /// <summary>
        /// The swap u int 32.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="uint"/>.
        /// </returns>
        public static uint SwapUInt32(uint value)
        {
            byte[] valueBytes = BitConverter.GetBytes(value);
            valueBytes = rage.ToolLib.Utility.Reverse(valueBytes);
            return BitConverter.ToUInt32(valueBytes, 0);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get metadata file data path.
        /// </summary>
        /// <param name="metadataFile">
        /// The metadata file.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetMetadataFileDataPath(audMetadataFile metadataFile)
        {
            string dataPath = Configuration.ObjectXmlPath + metadataFile.DataPath;
            dataPath = dataPath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            if (dataPath.EndsWith("\\"))
            {
                dataPath = dataPath.Substring(0, dataPath.Length - 1);
            }

            return dataPath.ToUpper();
        }

        #endregion
    }
}
