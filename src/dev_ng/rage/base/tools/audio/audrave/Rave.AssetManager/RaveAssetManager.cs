// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssetManager.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Wrapper around the autAssetManagement library
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.AssetManager
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    using Rave.AssetManager.Infrastructure.Interfaces;
    using Rave.Utils;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Wrapper around the autAssetManagement library
    /// </summary>
    public class RaveAssetManager : IRaveAssetManager
    {
        #region Static Fields

        /// <summary>
        /// The ms_locked assets.
        /// </summary>
        private static readonly List<IAsset> ms_lockedAssets = new List<IAsset>();

        /// <summary>
        /// The ms_asset manager.
        /// </summary>
        private static IAssetManager ms_assetManager;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the wave change list.
        /// </summary>
        public IChangeList WaveChangeList { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get asset manager.
        /// </summary>
        /// <returns>
        /// The <see cref="IAssetManager"/>.
        /// </returns>
        public IAssetManager GetAssetManager()
        {
            return ms_assetManager;
        }

        /// <summary>
        /// The is wave path locked.
        /// </summary>
        /// <param name="waveAssetPath">
        /// The wave asset path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsWavePathLocked(string waveAssetPath)
        {
            var assetLockPath = Configuration.PlatformWavePath + waveAssetPath;
            var assetLockFile = assetLockPath + ".lock";

            // if the lock file exists locally but not in asset management then we say its locked...
            return (!GetAssetManager().ExistsAsAsset(assetLockFile) && File.Exists(assetLockFile))
                   || GetAssetManager().IsCheckedOut(assetLockFile);
        }

        /// <summary>
        /// The lock wave path.
        /// </summary>
        /// <param name="assetPath">
        /// The asset path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool LockWavePath(string assetPath)
        {
            if (WaveChangeList == null)
            {
                WaveChangeList = GetAssetManager().CreateChangeList("[Rave] Wave Changelist");
            }

            var assetLockPath = Configuration.PlatformWavePath + assetPath;
            var assetLockFile = assetLockPath + ".lock";

            // make sure there is a local copy of the directory structure
            RaveUtils.CreateRequiredDirectories(assetLockPath);

            if (!ms_assetManager.ExistsAsAsset(assetLockFile))
            {
                var sw = new StreamWriter(assetLockFile, true, Encoding.ASCII);
                sw.WriteLine("{0} - {1}", Environment.UserName, DateTime.Now);
                sw.Close();

                // sumbit this change straight away to "Lock" wave Path
                try
                {
                    var lockList = ms_assetManager.CreateChangeList("[Rave] Lock for Wave Path added");
                    lockList.MarkAssetForAdd(assetLockFile, "text");
                    lockList.Submit();
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                    return false;
                }

                try
                {
                    var a = WaveChangeList.CheckoutAsset(assetLockFile, true);
                    ms_lockedAssets.Add(a);
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                    return false;
                }
            }
            else
            {
                var status = ms_assetManager.GetFileStatus(assetLockFile);
                if (status.IsLocked)
                {
                    if (!status.IsLockedByMe)
                    {
                        ErrorManager.HandleInfo(assetLockFile + " is already locked by " + status.Owner);
                        return false;
                    }
                }
                else
                {
                    // Assume already in asset list
                    if (!status.IsLockedByMe)
                    {
                        try
                        {
                            var a = WaveChangeList.CheckoutAsset(assetLockFile, true);
                            ms_lockedAssets.Add(a);
                        }
                        catch (Exception e)
                        {
                            // couldn't check out
                            ErrorManager.HandleError(e);
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// The set asset manager.
        /// </summary>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public void SetAssetManager(IAssetManager assetManager)
        {
            ms_assetManager = assetManager;
        }

        /// <summary>
        /// The unlock all assets.
        /// </summary>
        public void UnlockAllAssets()
        {
            var lockedAssetsToRemove = new List<IAsset>();
            foreach (var lockedAsset in ms_lockedAssets)
            {
                try
                {
                    lockedAsset.Revert(true);
                    lockedAssetsToRemove.Add(lockedAsset);
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                }
            }

            foreach (var a in lockedAssetsToRemove)
            {
                ms_lockedAssets.Remove(a);
            }
        }

        /// <summary>
        /// The unlock wave path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        public void UnlockWavePath(string path)
        {
            var assetPath = Configuration.PlatformWavePath + "\\" + path + "\\.lock";
            if (WaveChangeList != null)
            {
                var a = WaveChangeList.GetAsset(assetPath);
                if (a != null)
                {
                    a.Revert();
                }
            }
        }

        #endregion
    }
}