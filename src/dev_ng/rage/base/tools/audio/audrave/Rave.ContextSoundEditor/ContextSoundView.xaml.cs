﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using Rave.Plugins.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.Objects;

namespace Rave.ContextSoundEditor
{
    /// <summary>
    /// Interaction logic for ContextSoundView.xaml
    /// </summary>
    public partial class ContextSoundView : IRAVEObjectEditorPlugin
    {
        private IObjectInstance _objectInstance;
        private readonly ContextSoundViewModel _contextSoundViewModel;

        public ContextSoundView()
        {

            InitializeComponent();

            _contextSoundViewModel = new ContextSoundViewModel();

            DataContext = _contextSoundViewModel;

        }


        public string GetName()
        {
            return "Context Sound Editor";
        }

        public bool Init(XmlNode settings)
        {
            return true;
        }

        public string ObjectType { get { return "ContextSound"; } }
#pragma warning disable 0067
        public event Action<IObjectInstance> OnObjectEditClick;
        public event Action<IObjectInstance> OnObjectRefClick;
        public event Action<string, string> OnWaveRefClick;
        public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067

        public void EditObject(IObjectInstance objectInstance, Mode mode)
        {
            _objectInstance = objectInstance;
            ContextSoundView_OnLoaded(this, null);
        }

        public void Dispose()
        {
            //Do nothing
        }

        private void ContextSoundView_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (_contextSoundViewModel != null && _objectInstance != null)
            {
                _contextSoundViewModel.EditObject(_objectInstance);
            }
        }


        private void OnSoundTextBoxDragOver(object sender, DragEventArgs e)
        {
            e.Handled = true;
        }

      
    }
}

