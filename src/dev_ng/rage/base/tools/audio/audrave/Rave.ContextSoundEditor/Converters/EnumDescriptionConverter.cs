﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using ModelLib.CustomAttributes;

namespace Rave.ContextSoundEditor.Converters
{
    public class EnumDisplayConverter : IValueConverter
    {
        private string GetEnumDisplay(Enum enumObj)
        {
            FieldInfo fieldInfo = enumObj.GetType().GetField(enumObj.ToString());

            object[] attribArray = fieldInfo.GetCustomAttributes(false);

            if (attribArray.Length == 0)
            {
                return enumObj.ToString();
            }
            else
            {
                foreach (Attribute attrib in attribArray)
                {
                    if (attrib is DisplayAttribute)
                    {
                        return ((DisplayAttribute)attrib).Value;
                    }
                }
            }

            return enumObj.ToString();
        }

        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Enum myEnum = (Enum)value;
            string description = GetEnumDisplay(myEnum);
            return description;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Empty;
        }
    }
}
