﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rave.Instance;

namespace Rave.ContextSoundEditor
{
    public class AutoCompleteHelper
    {


        public static IEnumerable<string> GetObjectNames(IList<string> allowedTypes, string episode)
        {
            if (episode.Equals("CORE", StringComparison.InvariantCultureIgnoreCase))
            {
                episode = "BASE";
            }
            IEnumerable<string> objectNames;
            if (allowedTypes != null)
            {
                objectNames = from table in RaveInstance.ObjectLookupTables
                              where (table.Key.Episode.Equals(episode.Replace("_", string.Empty).Trim(), StringComparison.InvariantCultureIgnoreCase))
                              from kvp in table.Value
                              where (allowedTypes.Contains(kvp.Value.TypeName))
                              select kvp.Value.Name;
            }
            else
            {
                objectNames = from table in RaveInstance.ObjectLookupTables
                              from kvp in table.Value
                              where (kvp.Value.Episode.Equals(episode, StringComparison.InvariantCultureIgnoreCase))
                              select kvp.Value.Name;
            }
            return objectNames;
        }

        public static IEnumerable<string> GetObjectNames(string type, string episode)
        {
            if (episode.Equals("CORE", StringComparison.InvariantCultureIgnoreCase))
            {
                episode = "BASE";
            }
            IEnumerable<string> objectNames;
            if (type != null)
            {
                objectNames = from table in RaveInstance.ObjectLookupTables
                              where (table.Key.Episode.Equals(episode.Replace("_", string.Empty).Trim(), StringComparison.InvariantCultureIgnoreCase))
                              from kvp in table.Value
                              where (type.Equals(kvp.Value.Type, StringComparison.InvariantCultureIgnoreCase))
                              select kvp.Value.Name;
            }
            else
            {
                objectNames = from table in RaveInstance.ObjectLookupTables
                              from kvp in table.Value
                              where (kvp.Value.Episode.Equals(episode, StringComparison.InvariantCultureIgnoreCase))
                              select kvp.Value.Name;
            }
            return objectNames;
        }
    }
}
