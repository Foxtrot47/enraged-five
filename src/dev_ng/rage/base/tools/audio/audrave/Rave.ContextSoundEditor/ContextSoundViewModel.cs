﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using GalaSoft.MvvmLight.Command;
using ModelLib;
using ModelLib.Extentions;
using ModelLib.Types;
using Models;
using rage.ToolLib;
using Rave.ObjectBrowser.Infrastructure.Nodes;
using Rave.Types.Infrastructure.Interfaces.Objects;
using WPFToolLib.Extensions;

namespace Rave.ContextSoundEditor
{
    public class ContextSoundViewModel : INotifyPropertyChanged
    {
        private IEnumerable<string> _availableConditionSounds;
        private IEnumerable<string> _availableSounds;
        private ContextSound _contextSound;
        private IObjectInstance _objectToEdit;
        public Action<IObjectInstance> OnObjectClicked;

        public ContextSoundViewModel()
        {
            RemoveCondition = new RelayCommand<ContextSound.ConditionMappingDefinition>(item => ContextSound.ConditionMapping.Remove(item));
            AddConditionCommand = new RelayCommand(() => ContextSound.ConditionMapping.Add(new ContextSound.ConditionMappingDefinition() { Condition = new ObjectRef(), Sound = new ObjectRef() }));
            DroppedAutocompleteCommand = new RelayCommand<DragEventArgs>(DroppedAutoComplete);
            PropertyChanged.Raise(this, null);
        }



        public ContextSound ContextSound
        {
            get { return _contextSound; }
            set
            {
                _contextSound = value;
                PropertyChanged.Raise(this, "ContextSound");

            }
        }

        public bool IsReadOnly
        {
            get { return _objectToEdit == null || _objectToEdit.IsReadOnly; }
        }

        public bool IsEditMode
        {
            get { return !IsReadOnly; }
        }

        public
            IEnumerable<string> AvailableConditionSounds
        {
            get
            {
                if (_availableConditionSounds == null)
                {
                    _availableConditionSounds = AutoCompleteHelper.GetObjectNames(new List<string> { "SoundCondition" },
                        _objectToEdit.Episode);
                }
                return _availableConditionSounds;
            }
        }

        public IEnumerable<string> AvailableSounds
        {
            get
            {
                if (_availableSounds == null)
                {
                    if (_objectToEdit != null)
                    {
                        _availableSounds = AutoCompleteHelper.GetObjectNames("Sounds",
                            _objectToEdit.Episode);
                    }
                }
                return _availableSounds;
            }
        }

        public RelayCommand<DragEventArgs> DroppedAutocompleteCommand { get; private set; }

        public RelayCommand AddConditionCommand { get; private set; }

        public RelayCommand<ContextSound.ConditionMappingDefinition> RemoveCondition
        { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool DynamicSwitchingEnabled
        {
            get { return ContextSound != null && ContextSound.DynamicSwitchingEnabled == TriState.yes; }
            set
            {
                ContextSound.DynamicSwitchingEnabled = value ? TriState.yes : TriState.no;
                this.PropertyChanged.Raise(this, "DynamicSwitchingEnabled");
            }
        }

        public void EditObject(IObjectInstance objectInstance)
        {
            if (objectInstance == null)
            {
                return;
            }

            if (_objectToEdit != null && _objectToEdit.Bank != null)
            {
                _objectToEdit.Bank.BankStatusChanged -= StateChanged;
            }

            _objectToEdit = objectInstance;
            if (_objectToEdit == null)
            {
                return;
            }

            if (_objectToEdit.Bank != null)
            {
                _objectToEdit.Bank.BankStatusChanged += StateChanged;
            }

            if (_objectToEdit.Node != null)
            {
                ContextSound = _objectToEdit.Node.DeserializeToModel<ContextSound>();
                ContextSound.ConditionMapping.CollectionChanged += ConditionMapping_CollectionChanged;
                PropertyChanged.Raise(this, null);
                SetPropertyChanged();
            }
        }

        private void SetPropertyChanged()
        {

            ContextSound.PropertyChanged += ContextSoundOnPropertyChanged();
            if (ContextSound.DefaultSound != null)
            { 
                ContextSound.DefaultSound.PropertyChanged += ContextSoundOnPropertyChanged();
            }
            foreach (ContextSound.ConditionMappingDefinition condition in ContextSound.ConditionMapping)
            {
                var condition1 = condition;
                condition.PropertyChanged += (p, r) =>
                {
                    ConditionMappingChanged(condition1);
                };
                ConditionMappingChanged(condition1);

            }
        }

        private void ConditionMappingChanged(ContextSound.ConditionMappingDefinition condition)
        {
            Save();
            if (condition.Condition != null)
            {
                condition.Condition.PropertyChanged += ContextSoundOnPropertyChanged();
            }
            if (condition.Sound != null)
            {
                condition.Sound.PropertyChanged += ContextSoundOnPropertyChanged();
            }
        }

        private void ConditionMapping_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            SetPropertyChanged();
            Save();
        }

        private PropertyChangedEventHandler ContextSoundOnPropertyChanged()
        {
            return (n, r) =>
            {
                Save();
            };
        }


        private void Save()
        {
            if (IsReadOnly) return;
            _objectToEdit.Node = ContextSound.SerialializeToNode();
            _objectToEdit.MarkAsDirty();
        }

        private void StateChanged()
        {
            PropertyChanged.Raise(this, "IsReadOnly");
        }

        public void DroppedAutoComplete(DragEventArgs e)
        {
            var formats = e.Data.GetFormats();

            if (formats.Contains("rage.ToolLib.DataContainer"))
            {
                var dataContainer = (DataContainer)e.Data.GetData(typeof(DataContainer));

                if (dataContainer.Data.GetType() == typeof(SoundNode[]))
                {
                    var soundNodes = (SoundNode[])dataContainer.Data;

                    var autoCompleteBox = e.Source as AutoCompleteBox;

                    if (autoCompleteBox != null)
                    {
                        SetAutoCompleteToDrag(autoCompleteBox, soundNodes);
                    }
                }
            }
        }

        private static void SetAutoCompleteToDrag(AutoCompleteBox autoCompleteBox, SoundNode[] soundNodes)
        {
            if (autoCompleteBox.ItemsSource.OfType<string>()
                .Contains(soundNodes[0].Text, StringComparer.InvariantCultureIgnoreCase))
            {
                var bindingExpression = BindingOperations.GetBindingExpression(autoCompleteBox,
                    AutoCompleteBox.TextProperty);
                autoCompleteBox.SetValue(AutoCompleteBox.TextProperty, soundNodes[0].Text);
                if (bindingExpression != null) bindingExpression.UpdateSource();
            }
        }
    }
}