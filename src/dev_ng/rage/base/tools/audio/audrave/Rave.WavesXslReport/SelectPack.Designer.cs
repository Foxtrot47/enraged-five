﻿namespace Rave.WavesXslReport
{
    partial class SelectPack
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.packListLabel = new System.Windows.Forms.Label();
            this.ok = new System.Windows.Forms.Button();
            this.allPacksCheckBox = new System.Windows.Forms.CheckBox();
            this.packList = new System.Windows.Forms.ComboBox();
            this.packGroupBox = new System.Windows.Forms.GroupBox();
            this.packGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // packListLabel
            // 
            this.packListLabel.AutoSize = true;
            this.packListLabel.Location = new System.Drawing.Point(16, 27);
            this.packListLabel.Name = "packListLabel";
            this.packListLabel.Size = new System.Drawing.Size(35, 13);
            this.packListLabel.TabIndex = 1;
            this.packListLabel.Text = "Pack:";
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(88, 120);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 2;
            this.ok.Text = "&Ok";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // allPacksCheckBox
            // 
            this.allPacksCheckBox.AutoSize = true;
            this.allPacksCheckBox.Checked = true;
            this.allPacksCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.allPacksCheckBox.Location = new System.Drawing.Point(12, 12);
            this.allPacksCheckBox.Name = "allPacksCheckBox";
            this.allPacksCheckBox.Size = new System.Drawing.Size(70, 17);
            this.allPacksCheckBox.TabIndex = 3;
            this.allPacksCheckBox.Text = "All Packs";
            this.allPacksCheckBox.UseVisualStyleBackColor = true;
            this.allPacksCheckBox.CheckedChanged += new System.EventHandler(this.allPacks_CheckedChanged);
            // 
            // packList
            // 
            this.packList.FormattingEnabled = true;
            this.packList.Location = new System.Drawing.Point(57, 24);
            this.packList.Name = "packList";
            this.packList.Size = new System.Drawing.Size(151, 21);
            this.packList.TabIndex = 7;
            // 
            // packGroupBox
            // 
            this.packGroupBox.Controls.Add(this.packList);
            this.packGroupBox.Controls.Add(this.packListLabel);
            this.packGroupBox.Location = new System.Drawing.Point(12, 35);
            this.packGroupBox.Name = "packGroupBox";
            this.packGroupBox.Size = new System.Drawing.Size(227, 69);
            this.packGroupBox.TabIndex = 8;
            this.packGroupBox.TabStop = false;
            this.packGroupBox.Visible = false;
            // 
            // SelectPack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 157);
            this.ControlBox = false;
            this.Controls.Add(this.packGroupBox);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.allPacksCheckBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectPack";
            this.Text = "Select Pack for Report";
            this.packGroupBox.ResumeLayout(false);
            this.packGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label packListLabel;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.CheckBox allPacksCheckBox;
        private System.Windows.Forms.ComboBox packList;
        private System.Windows.Forms.GroupBox packGroupBox;
    }
}