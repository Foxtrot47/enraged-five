﻿namespace Rave.WavesXslReport
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Xsl;

    using Rave.Plugins.Infrastructure.Interfaces;

    using rage.ToolLib;
    using rage.ToolLib.CmdLine;
    using rage.ToolLib.WavesFiles;

    using Rave.Instance;
    using Rave.ObjectBrowser.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;

    public class WavesXslReport : IRAVEReportPlugin
    {
        private string m_builtWavesPackListFile;
        private string m_name;
        private OutputType m_outputType;
        private string m_pendingWavesDirectory;
        private string m_platform;
        private bool m_promptForPack;
        private string m_xslPath;

        #region IRAVEReportPlugin Members

        public string GetName()
        {
            return m_name;
        }

        public bool Init(XmlNode settings)
        {
            m_outputType = OutputType.txt;
            foreach (XmlNode setting in settings.ChildNodes)
            {
                try
                {
                    var token = Enum<ParseTokens>.Parse(setting.Name);
                    switch (token)
                    {
                        case ParseTokens.Name:
                            {
                                m_name = setting.InnerText;
                                break;
                            }
                        case ParseTokens.BuiltWavesPackListFile:
                            {
                                m_builtWavesPackListFile = setting.InnerText;
                                break;
                            }
                        case ParseTokens.PendingWavesDirectory:
                            {
                                m_pendingWavesDirectory = setting.InnerText;
                                break;
                            }
                        case ParseTokens.XslPath:
                            {
                                m_xslPath = setting.InnerText;
                                break;
                            }
                        case ParseTokens.Platform:
                            {
                                m_platform = setting.InnerText;
                                break;
                            }
                        case ParseTokens.OutputType:
                            {
                                m_outputType = Enum<OutputType>.TryParse(setting.InnerText, true, OutputType.html);
                                break;
                            }
                        case ParseTokens.PromptForPack:
                            {
                                m_promptForPack = bool.Parse(setting.InnerText);
                                break;
                            }
                    }
                }
                catch
                {
                    ErrorManager.HandleError(
                        string.Format(
                            "Unrecognised setting: \"{0}\" encountered in Rave.WavesXslReport plugin settings",
                            setting.Name));
                    return false;
                }
            }

            return Validate();
        }

        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            var tempPath = Path.GetTempPath();
            string xmlFile;
            if (!string.IsNullOrEmpty(m_builtWavesPackListFile))
            {
                var wavesFileCombiner = new WavesFileCombiner();
                xmlFile = wavesFileCombiner.Run(m_builtWavesPackListFile, tempPath, null);
            }
            else
            {
                xmlFile = Path.Combine(tempPath, "PendingWaves.xml");
                var files = Directory.GetFiles(m_pendingWavesDirectory, "*.xml", SearchOption.TopDirectoryOnly).ToList();
                var root = new XElement("PendingWaves");
                foreach (var file in files)
                {
                    var tempDoc = XDocument.Load(file);
                    if (tempDoc.Root != null &&
                        tempDoc.Root.Name == "PendingWaves")
                    {
                        foreach (var element in tempDoc.Root.Elements())
                        {
                            root.Add(element);
                        }
                    }
                }
                var doc = new XDocument(root);
                doc.Save(xmlFile);
            }

            var transform = new XslCompiledTransform();
            transform.Load(m_xslPath);

            string selectedPack = null;
            if (m_promptForPack)
            {
                using (var selectPack = new SelectPack(waveBrowser.GetPendingWaveLists()))
                {
                    selectPack.ShowDialog(RaveInstance.ActiveWindow);
                    selectedPack = selectPack.SelectedPack;
                }
            }

            var hasPlatform = !string.IsNullOrEmpty(m_platform);

            var resultsFilePath =
                Path.Combine(tempPath, string.Format("Rave_{0}_WavesXslReport.{1}", m_platform, m_outputType)).Replace(" ", string.Empty);
            using (var resultsFile = File.Open(resultsFilePath, FileMode.Create, FileAccess.Write))
            {
                using (var resultsWriter = XmlWriter.Create(resultsFile,
                                                            new XmlWriterSettings {Indent = true, CloseOutput = false}))
                {
                    if (selectedPack != null || hasPlatform)
                    {
                        // we need to pass arguments to the xsl
                        var argList = new XsltArgumentList();
                        if (selectedPack != null)
                        {
                            argList.AddParam("pack", string.Empty, selectedPack);
                        }
                        if (hasPlatform)
                        {
                            argList.AddParam("platform", string.Empty, m_platform);
                        }
                        transform.Transform(xmlFile, argList, resultsWriter);
                    }
                    else
                    {
                        transform.Transform(xmlFile, resultsWriter);
                    }
                    resultsWriter.Flush();
                }
                resultsFile.Flush();
            }

            var commandExecutor = new NoOutputCommandExecutor();
            commandExecutor.Execute("cmd", string.Concat("/c start ", resultsFilePath));
        }

        #endregion

        private bool Validate()
        {
            if (string.IsNullOrEmpty(m_name) || string.IsNullOrEmpty(m_xslPath) ||
                !File.Exists(m_xslPath))
            {
                return false;
            }

            if (string.IsNullOrEmpty(m_pendingWavesDirectory) &&
                string.IsNullOrEmpty(m_builtWavesPackListFile))
            {
                return false;
            }

            if (!string.IsNullOrEmpty(m_builtWavesPackListFile) &&
                !File.Exists(m_builtWavesPackListFile))
            {
                return false;
            }

            if (!string.IsNullOrEmpty(m_pendingWavesDirectory) &&
                !Directory.Exists(m_pendingWavesDirectory))
            {
                return false;
            }

            if (!string.IsNullOrEmpty(m_pendingWavesDirectory) &&
                !string.IsNullOrEmpty(m_builtWavesPackListFile))
            {
                return false;
            }
            return true;
        }

        public override string ToString()
        {
            return m_name;
        }

        #region Nested type: OutputType

        private enum OutputType
        {
// ReSharper disable UnusedMember.Local
// ReSharper disable InconsistentNaming
            html,
            xml,
            csv,
            txt
// ReSharper restore InconsistentNaming
// ReSharper restore UnusedMember.Local
        }

        #endregion

        #region Nested type: ParseTokens

        private enum ParseTokens
        {
            Name,
            BuiltWavesPackListFile,
            PendingWavesDirectory,
            XslPath,
            Platform,
            OutputType,
            PromptForPack
        }

        #endregion
    }
}