﻿namespace Rave.WavesXslReport
{
    using System;
    using System.Windows.Forms;

    using Rave.Utils;

    using rage;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

    public partial class SelectPack : Form
    {
        public SelectPack(IPendingWaveLists pendingWaveLists)
        {
            InitializeComponent();

            SelectedPack = string.Empty;

            var pendingWaveList =
                pendingWaveLists.GetPendingWaveList(Configuration.ActivePlatformSettings[0].PlatformTag);

            foreach(var packName in pendingWaveList.getPackNamesFromBuiltWavesPackList())
            {
                packList.Items.Add(packName);
            }
        }

        public string SelectedPack { get; private set; }

        private void allPacks_CheckedChanged(object sender, EventArgs e)
        {
            if (allPacksCheckBox.Checked)
            {
                packGroupBox.Visible = false;
            }
            else
            {
                packGroupBox.Visible = true;
                packList.SelectedIndex = 0;
            }
        }

        private void ok_Click(object sender, EventArgs e)
        {
            if (allPacksCheckBox.Checked)
            {
                SelectedPack = string.Empty;
            }
            else
            {
                SelectedPack = packList.SelectedItem as string;
            }
            Close();
        }
    }
}