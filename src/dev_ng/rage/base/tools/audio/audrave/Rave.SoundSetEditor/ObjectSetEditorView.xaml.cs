﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Rave.Plugins.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.ObjectSetEditor.ViewModel;
using Rave.ObjectSetEditor.Models;

namespace Rave.ObjectSetEditor
{
    /// <summary>
    /// Interaction logic for ObjectSetEditorView.xaml
    /// </summary>
    public partial class ObjectSetEditorView : IRAVEObjectEditorPlugin
    {
        public ObjectSetEditorView()
        {
            InitializeComponent();
            this.DataContext = new ObjectSetEditorViewModel<SoundSetEntryModel>(new ObjectSetModel<SoundSetEntryModel>());
        }

        public string ObjectType
        {
            get { return "SoundSet/CurveSet/GameObjectSet"; }
        }

#pragma warning disable 0067
        public event Action<IObjectInstance> OnObjectEditClick;
        public event Action<IObjectInstance> OnObjectRefClick;
        public event Action<string, string> OnWaveRefClick;
        public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067

        private IObjectInstance _objectToEdit;
        public void EditObject(IObjectInstance objectInstance, Mode mode)
        {
            if (objectInstance == null)
            {
                return;
            }
            ObjectSetEditorView_OnUnloaded(null, null);
            _objectToEdit = objectInstance;
            ObjectSetEditorView_OnLoaded(null, null);
        }

        public void Dispose()
        {
            //do nothing
        }

        public string GetName()
        {
            return "Object Set Editor";
        }

        public bool Init(System.Xml.XmlNode settings)
        {
            return true;
        }

        private dynamic _viewModel;

        private void ObjectSetEditorView_OnUnloaded(object sender, RoutedEventArgs e)
        {
            if (_viewModel != null)
            {
                this.DataContext = null;
                _viewModel.Dispose();
                _viewModel = null;
            }

            if (e != null)
            {
                e.Handled = true;
            }
        }

        private void ObjectSetEditorView_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (_objectToEdit != null)
            {
                switch (_objectToEdit.TypeName.ToUpper())
                {
                    case "SOUNDSET":
                        _viewModel = new ObjectSetEditorViewModel<SoundSetEntryModel>(new ObjectSetModel<SoundSetEntryModel>());
                        _viewModel.SetType = "Sound";
                        break;

                    case "CURVESET":
                        _viewModel = new ObjectSetEditorViewModel<CurveSetEntryModel>(new ObjectSetModel<CurveSetEntryModel>());
                        _viewModel.SetType = "Curve";
                        break;

                    case "GAMEOBJECTSET":
                        _viewModel = new ObjectSetEditorViewModel<GameObjectSetEntryModel>(new ObjectSetModel<GameObjectSetEntryModel>());
                        _viewModel.SetType = "GameObject";
                        break;

                    default:
                        return;

                }
                this.DataContext = this._viewModel;
                _viewModel.EditObject(_objectToEdit);
                _viewModel.OnObjectRefClick += this.OnObjectRefClick;

            }
        }


        #region Obsolete Event Handlers
        //THESE EVENT HANDLERS SHOULD BE CHANGED TO COMMADS 

        private void DataGridCell_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            DataGridCell cell = sender as DataGridCell;


            if (cell != null && !cell.IsEditing && !cell.IsReadOnly)
            {

                if (!cell.IsFocused)
                {
                    cell.Focus();
                }
                DataGrid dataGrid = FindVisualParent<DataGrid>(cell);
                if (dataGrid != null)
                {
                    if (dataGrid.SelectionUnit != DataGridSelectionUnit.FullRow)
                    {
                        if (!cell.IsSelected)
                        {
                            cell.IsSelected = true;
                        }
                    }
                    else
                    {
                        DataGridRow row = FindVisualParent<DataGridRow>(cell);
                        if (row != null && !row.IsSelected)
                        {
                            row.IsSelected = true;
                        }
                    }
                }
            }
        }

        static T FindVisualParent<T>(UIElement element) where T : UIElement
        {
            UIElement parent = element;
            while (parent != null)
            {
                T correctlyTyped = parent as T;
                if (correctlyTyped != null)
                {
                    return correctlyTyped;
                }

                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }
            return null;
        }


        #endregion

	}
}
