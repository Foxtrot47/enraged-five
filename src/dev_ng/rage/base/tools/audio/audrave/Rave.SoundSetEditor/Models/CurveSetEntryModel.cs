﻿using System.ComponentModel;
using System.Xml;
using WPFToolLib.Extensions;

namespace Rave.ObjectSetEditor.Models
{
	public class CurveSetEntryModel : IObjectSetEntryModel
	{
		public string Type { get { return "Curve"; } }

		public event PropertyChangedEventHandler PropertyChanged;

		public CurveSetEntryModel()
		{
			Object = null;
			Name = null;
			Comments = null;
		}

		public CurveSetEntryModel(string name, string curve, string comments, XmlNode node)
		{
			Name = name;
			Object = curve;
			Comments = comments;
			Node = node;
		}

		private string name;

		public string Name
		{
			get { return name; }
			set
			{
				if (name != value)
				{
					name = value.Trim().Replace(' ', '_');
					PropertyChanged.Raise(this, "Name");
				}
			}
		}

		private string curve;


		public string Object
		{
			get { return curve; }
			set
			{
				if (curve != value)
				{
					curve = value;
					PropertyChanged.Raise(this, "Object");
				}
			}
		}

		private string comments;

		public string Comments
		{
			get { return comments; }
			set
			{
				if (comments != value)
				{
					comments = value;
					PropertyChanged.Raise(this, "Comments");
				}
			}
		}

		private XmlNode _node;
		public XmlNode Node
		{
			get { return _node; }
			set
			{
				_node = value;
				PropertyChanged.Raise(this, "Node");
			}
		}
	}
}