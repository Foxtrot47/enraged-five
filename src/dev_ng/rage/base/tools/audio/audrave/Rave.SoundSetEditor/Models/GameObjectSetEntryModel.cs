﻿using System.ComponentModel;
using System.Xml;
using WPFToolLib.Extensions;

namespace Rave.ObjectSetEditor.Models
{
    public class GameObjectSetEntryModel : IObjectSetEntryModel
    {
        public string Type { get { return "GameObject"; } }

        public event PropertyChangedEventHandler PropertyChanged;

        public GameObjectSetEntryModel()
        {
            Object = null;
            Name = null;
            Comments = null;
        }

        public GameObjectSetEntryModel(string name, string gameObject, string comments, XmlNode node)
        {
			Name = name;
			Object = gameObject;
			Comments = comments;
			Node = node;
        }

        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
					name = value.Trim().Replace(' ', '_');
                    PropertyChanged.Raise(this, "Name");
                }
            }
        }

        private string GameObject;

     
        public string Object
        {
            get { return GameObject; }
            set
            {
                if (GameObject != value)
                {
                    GameObject = value;
                    PropertyChanged.Raise(this, "Object");
                }
            }
        }

        private string comments;

        public string Comments
        {
            get { return comments; }
            set
            {
                if (comments != value)
                {
                    comments = value;
                    PropertyChanged.Raise(this, "Comments");
                }
            }
        }

		private XmlNode _node;
		public XmlNode Node
		{
			get { return _node; }
			set
			{
				_node = value;
				PropertyChanged.Raise(this, "Node");
			}
		}
    }
}