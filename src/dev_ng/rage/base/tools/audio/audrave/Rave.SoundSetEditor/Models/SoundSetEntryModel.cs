﻿using System.ComponentModel;
using System.Xml;
using WPFToolLib.Extensions;

namespace Rave.ObjectSetEditor.Models
{
	public class SoundSetEntryModel : IObjectSetEntryModel
	{
		public string Type { get { return "Sound"; } }
		public event PropertyChangedEventHandler PropertyChanged;

		public SoundSetEntryModel()
		{
			Object = "NULL_SOUND";
			Name = null;
			Comments = null;
		}

		public SoundSetEntryModel(string name, string sound, string comments, XmlNode node)
		{
			Name = name;
			Object = sound;
			Comments = comments;
			Node = node;
		}

		private string name;

		public string Name
		{
			get { return name; }
			set
			{
				if (name != value)
				{
					name = value.Trim().Replace(' ', '_');
					PropertyChanged.Raise(this, "Name");
				}
			}
		}

		private string sound;

		[DefaultValue("NULL_SOUND")]
		public string Object
		{
			get { return sound; }
			set
			{
				if (sound != value)
				{
					sound = value;
					PropertyChanged.Raise(this, "Object");
				}
			}
		}

		private string comments;


		public string Comments
		{
			get { return comments; }
			set
			{
				if (comments != value)
				{
					comments = value;
					PropertyChanged.Raise(this, "Comments");
				}
			}
		}

		private XmlNode _node;
		public XmlNode Node
		{
			get { return _node; }
			set
			{
				_node = value;
				PropertyChanged.Raise(this, "Node");
			}
		}
	}
}