﻿using rage.ToolLib;
using Rave.ObjectBrowser.Infrastructure.Nodes;
using Rave.ObjectSetEditor.Models;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.Types.Objects;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using WPFToolLib.Extensions;

namespace Rave.ObjectSetEditor.ViewModel
{
    public class ObjectSetEditorViewModel<T> : INotifyPropertyChanged, IDisposable where T : IObjectSetEntryModel
	{
		private RelayCommand<object> _addNewObjectCommand;
		private ICommand _dragEnter;
		private ICommand _dragLeave;
		private ICommand _droppedWav;
		private ICommand _findObjectCommand;
		private bool _isDrag;
		private ObjectSetModel<T> _model;
		private IObjectInstance _objectToEdit;
		private ICommand _removeObjectCommand;
		private string _setType = "";

		public RelayCommand<object> AddNewObject
		{
			get
			{
				if (_addNewObjectCommand == null)
				{
					_addNewObjectCommand = new RelayCommand<object>(
						AddNewObjectAction,
						param => !IsReadOnly
						);
				}
				return _addNewObjectCommand;
			}
		}

		public bool CanAddObjectSetEntry
		{
			get { return _model.CanAddObjectSetEntry; }
		}

		public ICommand DragEnter
		{
			get
			{
				if (_dragEnter == null)
				{
					_dragEnter = new RelayCommand(
						() => SetIsDrag(true),
						() => !IsReadOnly
						);
				}
				return _dragEnter;
			}
		}

		public ICommand DragLeave
		{
			get
			{
				if (_dragLeave == null)
				{
					_dragLeave = new RelayCommand(
						() => SetIsDrag(false),
						() => !IsReadOnly
						);
				}
				return _dragLeave;
			}
		}

		public ICommand DroppedWav
		{
			get
			{
				if (_droppedWav == null)
				{
					_droppedWav = new RelayCommand<object>(
						param => DroppedWavAction(param),
						param => !IsReadOnly
						);
				}
				return _droppedWav;
			}
		}

		public ICommand FindObjectCommand
		{
			get
			{
				if (_findObjectCommand == null)
				{
					_findObjectCommand = new RelayCommand<object>(
						param => FindObjectAction(param),
						param => true
						);
				}
				return _findObjectCommand;
			}
		}

		public bool IsDrag
		{
			get { return _isDrag; }

			set
			{
				_isDrag = value;
				PropertyChanged.Raise(this, "IsDrag");
			}
		}

		public bool IsReadOnly
		{
			get { return !_model.IsCheckedOut; }
		}

		public ItemsObservableCollection<T> Items
		{
			get { return _model.Items; }
			set
			{
				if (!IsReadOnly)
				{
					_model.Items = value;
				}
			}
		}

		public IObjectInstance ObjectToEdit
		{
			get { return _objectToEdit; }
			set
			{
				_objectToEdit = value; 
				this.PropertyChanged.Raise(this,"ObjectToEdit");
			}
		}

		public ICommand RemoveItem
		{
			get
			{
				if (_removeObjectCommand == null)
				{
					_removeObjectCommand = new RelayCommand<object>(
						RemoveEntryAction,
						param => !IsReadOnly
						);
				}
				return _removeObjectCommand;
			}
		}

		public string SetType
		{
			get { return _setType; }

			set
			{
				_setType = value;
				PropertyChanged.Raise(this, "SetType");
			}
		}
#pragma warning disable 0067

		public event Action<IObjectInstance> OnObjectEditClick;

		public event Action<IObjectInstance> OnObjectRefClick;

		public event Action<string> OnWaveBankRefClick;

		public event Action<string, string> OnWaveRefClick;

#pragma warning restore 0067

		public event PropertyChangedEventHandler PropertyChanged;

		public ObjectSetEditorViewModel(ObjectSetModel<T> model)
		{
			if (model == null)
			{
				throw new ArgumentNullException("model");
			}
			_model = model;
			_model.PropertyChanged += ModelPropertyChanged;
		}

		public void Dispose()
		{
			if (_model != null)
			{
				_model.PropertyChanged -= ModelPropertyChanged;
				_model = null;
			}
		}

		public void DroppedWavAction(object parameter)
		{
			var e = (DragEventArgs)parameter;

			string[] formats = e.Data.GetFormats();

			if (formats.Contains("rage.ToolLib.DataContainer"))
			{
				var dataContainer = (DataContainer)e.Data.GetData(typeof(DataContainer));

				if (dataContainer.Data.GetType() == typeof(SoundNode[]))
				{
					var soundNodes = (SoundNode[])dataContainer.Data;

					var textBlock = e.Source as TextBlock;

					if (textBlock != null)
					{
						if (soundNodes[0].Type.ToUpper() == SetType.ToUpper() + "S" &&
							(_model.AllowedTypes.Contains(soundNodes[0].ObjectInstance.TypeName) ||
							 _model.AllowedTypes.Count == 0))
						{
							BindingExpression bindingExpression = BindingOperations.GetBindingExpression(textBlock,
								TextBlock.TextProperty);
							textBlock.SetValue(TextBlock.TextProperty, soundNodes[0].Text);
							if (bindingExpression != null) bindingExpression.UpdateSource();
						}
					}
					else
					{
						var button = e.Source as Button;
						if (button == null)
						{
							var image = e.Source as Image;
							if (image == null)
							{
								return;
							}
						}

						foreach (SoundNode soundNode in soundNodes)
						{
							if (soundNode.Type.ToUpper() == SetType.ToUpper() + "S" &&
								(_model.AllowedTypes.Contains(soundNode.ObjectInstance.TypeName) ||
								 _model.AllowedTypes.Count == 0))
							{
								var entryToAdd = (T)Activator.CreateInstance(typeof(T));
								entryToAdd.Name = null;
								entryToAdd.Object = soundNode.Text;
								entryToAdd.Comments = null;
								Items.Add(entryToAdd);
							}
						}
					}
				}
			}
		}

		public void EditObject(IObjectInstance objectInstance)
		{
			if (objectInstance != null)
			{
				_model.EditObject(objectInstance);

				Items.CollectionChanged -= _model.ObjectSetEntries_CollectionChanged;
				Items.CollectionChanged += _model.ObjectSetEntries_CollectionChanged;
				ObjectToEdit = objectInstance;
				AddNewObject.RaiseCanExecuteChanged();
			}
		}

		public void FindObjectAction(object parameters)
		{
			string objectName = (string)parameters;
			if (objectName != null)
			{
				OnObjectRefClick(ObjectInstance.GetObjectMatchingName(objectName));
			}
		}

		public void RemoveEntryAction(object parameter)
		{
			var entry = (T)parameter;

			Items.Remove(entry);
		}

		private void AddNewObjectAction(object parameter)
		{
			Items.Add((T)Activator.CreateInstance(typeof(T)));
		}

		//forwarding and rewiring model propertyChanged events
		private void ModelPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "IsCheckedOut":
					PropertyChanged.Raise(this, "IsReadOnly");
					break;

				default:
					//forwarding event
					PropertyChanged.Raise(this, e.PropertyName);
					break;
			}
		}

		private void SetIsDrag(bool value)
		{
			IsDrag = value;
		}
	}
}