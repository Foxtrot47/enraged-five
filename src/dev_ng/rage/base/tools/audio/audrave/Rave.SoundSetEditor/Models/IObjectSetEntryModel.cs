﻿using System.ComponentModel;
using System.Xml;

namespace Rave.ObjectSetEditor.Models
{
    public interface IObjectSetEntryModel : INotifyPropertyChanged
    {
        string Type { get; }

        string Name { get; set; }

        //Can be sound, curve, gameObject or any object that has a soundset.
        string Object { get; set; }

        string Comments { get; set; }

		XmlNode Node { get; set; }
    }
}
