﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Xml;
using rage.ToolLib;
using Rave.TypeDefinitions.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.Objects;
using WPFToolLib.Extensions;

namespace Rave.ObjectSetEditor.Models
{
	public class ObjectSetModel<T> : INotifyPropertyChanged where T : IObjectSetEntryModel
	{

		private ItemsObservableCollection<T> _objectSetEntries;

		private bool _canAddObjectSetEntry;

		private IObjectInstance _instance;

		private bool _isCheckedOut;

		private int _maxNumObjectSetEntry;

		private bool _isLoading;

		private List<string> _allowedTypes;

		public List<string> AllowedTypes
		{
			get { return _allowedTypes; }
		}


		public ObjectSetModel()
		{
			_objectSetEntries = new ItemsObservableCollection<T>();
			_objectSetEntries.CollectionChanged += ObjectSetEntries_CollectionChanged;
		}

		public void ObjectSetEntries_CollectionChanged(object sender,
			NotifyCollectionChangedEventArgs e)
		{

			CanAddObjectSetEntry = _objectSetEntries.Count < _maxNumObjectSetEntry;
			//save collection content to xml
			if (IsCheckedOut)
			{
				if (!_isLoading && _instance != null && _instance.Node != null)
				{
					List<XmlNode> objectsList =
						_instance.Node.ChildNodes.Cast<XmlNode>()
							.Where(child => child.Name.Equals(((T) Activator.CreateInstance(typeof (T))).Type + "s"))
							.ToList();
					if (e.Action == NotifyCollectionChangedAction.Remove)
					{
						_instance.Node.RemoveChild(objectsList[e.OldStartingIndex]);
					}
					else
					{


						for (int i = 0; i < _objectSetEntries.Count; i++)
						{
							var objectSetEntry = _objectSetEntries.ToList()[i];

							if (_instance != null && _instance.Node != null && _instance.Node.OwnerDocument != null)
							{
								XmlNode objectNode = null;
								if (i < objectsList.Count)
								{
									objectNode = objectsList[i];
									if (objectSetEntry.Node == null)
									{
										objectSetEntry.Node = objectNode;
									}
								}
								else
								{
									objectNode =
										_instance.Node.AppendChild(_instance.Node.OwnerDocument.CreateElement(objectSetEntry.Type + "s"));
								}

								XmlNode name = objectNode.ChildNodes.Cast<XmlNode>().FirstOrDefault(p => p.Name == "Name");
								if (name != null)
								{
									name.InnerText = objectSetEntry.Name;
								}
								else
								{
									name = _instance.Node.OwnerDocument.CreateElement("Name");
									name.InnerText = objectSetEntry.Name;
									objectNode.AppendChild(name);
								}

								XmlNode objectElement = objectNode.ChildNodes.Cast<XmlNode>().FirstOrDefault(p => p.Name == objectSetEntry.Type);
								if (objectElement != null)
								{
									objectElement.InnerText = objectSetEntry.Object;
								}
								else
								{
									objectElement = _instance.Node.OwnerDocument.CreateElement(objectSetEntry.Type);
									objectElement.InnerText = objectSetEntry.Object;
									objectNode.AppendChild(objectElement);
								}

								if (!string.IsNullOrEmpty(objectSetEntry.Comments))
								{
									XmlNode comments = objectNode.ChildNodes.Cast<XmlNode>().FirstOrDefault(p => p.Name == "Comments");
									if (comments != null)
									{
										comments.InnerText = objectSetEntry.Comments;
									}
									else
									{
										comments = _instance.Node.OwnerDocument.CreateElement("Comments");
										comments.InnerText = objectSetEntry.Comments;
										objectNode.AppendChild(comments);
									}
								}
							}
						}
						
					}
					//compute references to inform other winows

					_instance.ComputeReferences();

					_instance.MarkAsDirty();
				}
			}
		}


		public event
			PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Gets a value indicating whether can add operation.
		/// </summary>
		public bool CanAddObjectSetEntry
		{
			get { return _canAddObjectSetEntry; }

			private set
			{
				if (_canAddObjectSetEntry != value)
				{
					_canAddObjectSetEntry = value;
					PropertyChanged.Raise(this, "CanAddObjectSetEntry");
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether is checked out.
		/// </summary>
		public bool IsCheckedOut
		{
			get
			{
				return _isCheckedOut;
			}

			set
			{
				if (_isCheckedOut != value)
				{
					_isCheckedOut = value;
					PropertyChanged.Raise(this, "IsCheckedOut");
				}
			}
		}

		/// <summary>
		/// Gets or sets the soundsetEntry
		/// </summary>
		public ItemsObservableCollection<T> Items
		{
			get
			{
				return _objectSetEntries;
			}

			set
			{
				_objectSetEntries = value;
			}
		}

		public void EditObject(IObjectInstance objectInstance)
		{
			if (objectInstance == null)
			{
				return;
			}
			_isLoading = true;
			Reset();

			if (_instance != null && _instance.Bank != null)
			{
				_instance.Bank.BankStatusChanged -= OnBankStatusChanged;
			}

			_instance = objectInstance;
			if (_instance == null)
			{
				return;
			}

			if (_instance.Bank != null)
			{
				_instance.Bank.BankStatusChanged += OnBankStatusChanged;
			}

			IsCheckedOut = !_instance.IsReadOnly;

			var typeDef = _instance.TypeDefinitions.FindTypeDefinition(_instance.TypeName);
			if (typeDef != null)
			{
				ICompositeFieldDefinition operationsFieldDef =
					typeDef.FindFieldDefinition(((T)Activator.CreateInstance(typeof(T))).Type + "s") as ICompositeFieldDefinition;
				if (operationsFieldDef != null)
				{
					_maxNumObjectSetEntry = operationsFieldDef.MaxOccurs;
					var objectRefFields =
						from field in operationsFieldDef.Fields
						where field.Units == "ObjectRef"
						select field;


					_allowedTypes = GetInheritedAllowedTypes(objectRefFields.First());
				}
			}

			if (_instance.Node != null && _instance.Node.ChildNodes.Count > 0)
			{
				foreach (XmlNode childNode in _instance.Node.ChildNodes)
				{
					try
					{
						T objectSet = ((T)Activator.CreateInstance(typeof(T)));
						if (objectSet != null && childNode != null && childNode.Name.Equals(objectSet.Type + "s"))
						{
							objectSet.Node = childNode;
							foreach (XmlNode valueNode in childNode.ChildNodes)
							{
								if (valueNode.Name.Equals("Name")) objectSet.Name = valueNode.InnerText;
								if (valueNode.Name.Equals(objectSet.Type)) objectSet.Object = valueNode.InnerText;
								if (valueNode.Name.Equals("Comments")) objectSet.Comments = valueNode.InnerText;
							}
							_objectSetEntries.Add(objectSet);
						}
					}
					catch
					{
					}
				}

				CanAddObjectSetEntry = _objectSetEntries.Count < _maxNumObjectSetEntry;

			}
			_isLoading = false;
		}

		/// <summary>
		/// The on bank status changed.
		/// </summary>
		private void OnBankStatusChanged()
		{
			if (_instance != null && _instance.ObjectLookup.ContainsKey(_instance.Name))
			{
				// Edit the newly loaded object instance
				EditObject(_instance.ObjectLookup[_instance.Name]);
			}
			else
			{
				Reset();
			}
		}

		/// <summary>
		/// The reset.
		/// </summary>
		private void Reset()
		{
			var items = _objectSetEntries.ToList();
			foreach (var item in items)
			{
				_objectSetEntries.Remove(item);
			}

			IsCheckedOut = false;
			CanAddObjectSetEntry = false;
		}





		private List<string> GetInheritedAllowedTypes(IFieldDefinition fieldDef)
		{
			List<string> allowedTypes = new List<string>();
			if (!string.IsNullOrEmpty(fieldDef.AllowedType))
			{
				var typeDefinitions = fieldDef.TypeDefinition.TypeDefinitions;
				var allowedType = typeDefinitions.FindTypeDefinition(fieldDef.AllowedType);
				if (allowedType != null)
				{
					List<string> concreteDescendantTypes =
						(from descendantType in allowedType.GetDescendantTypes()
						 where descendantType.IsAbstract == false
						 select descendantType.Name).ToList();

					if (concreteDescendantTypes.Count > 0)
					{
						allowedTypes.AddRange(concreteDescendantTypes);
					}
				}
			}
			return allowedTypes;
		}

	}

}
