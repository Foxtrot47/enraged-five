﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rave.ObjectSetEditor.Models;

namespace Rave.ObjectSetEditor.ViewModel
{
	public class DesignViewModel : ObjectSetEditorViewModel<SoundSetEntryModel>
	{
		public DesignViewModel(ObjectSetModel<SoundSetEntryModel> model) : base(model)
		{
		}
	}
}
