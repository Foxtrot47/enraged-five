﻿// -----------------------------------------------------------------------
// <copyright file="ModSynthInvalidRefs.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.ModSynthInvalidRefs
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.Instance;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;
    using global::Rave.Plugins.Infrastructure.Interfaces;
    using global::Rave.Types.Infrastructure.Interfaces.Objects;
    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    /// Report that generates a list of ModularSynth objects
    /// that are referenced by ModularSynthSounds but do
    /// not have the ExportForGame flag set.
    /// </summary>
    public class ModSynthInvalidRefs : IRAVEReportPlugin
    {
        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// Get the report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;
                }
            }

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes (not required).
        /// </param>
        /// <param name="browsers">
        /// The sound browsers (not required).
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            var invalidObjs = new HashSet<IObjectInstance>();

            foreach (var soundsLookup in RaveInstance.ObjectLookupTables.Where(lookupTable => lookupTable.Key.Type.ToLower().Equals("sounds")))
            {
                foreach (var objInstance in soundsLookup.Value.Values.Where(obj => obj.TypeName.ToLower().Equals("modularsynthsound")))
                {
                    foreach (var objectRef in objInstance.References)
                    {
                        var modSynthObj = objectRef.ObjectInstance;
                        if (null != modSynthObj && modSynthObj.TypeName.ToLower().Equals("modularsynth"))
                        {
                            var valid = (
                                from XmlNode node in modSynthObj.Node.ChildNodes 
                                where node.Name.ToLower().Equals("exportforgame") 
                                select node.InnerText.ToLower().Equals("yes")).FirstOrDefault();

                            if (!valid)
                            {
                                invalidObjs.Add(modSynthObj);
                            }
                        }
                    }
                }
            }

            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Invalid ModularSynth References</title></head><body>");
            sw.WriteLine("<h2>Invalid ModularSynth Objects</h2>");
            sw.WriteLine("<p>Description: ModularSynth objects that are referenced by ModularSynthSounds but do not have the ExportForGame flag set.");

            if (!invalidObjs.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<ul>");

                foreach (var objInstance in invalidObjs)
                {
                    sw.WriteLine("<li>" + objInstance.Bank.FilePath + "/" + objInstance.Name + "</li>");
                }

                sw.WriteLine("</ul>");

                sw.WriteLine("</body></html>");

                sw.Close();
                filesteam.Close();

                // Display report
                var report = new frmReport(this.name + " Report");
                report.SetURL(resultsFileName);
                report.Show();
            }
        }
    }
}
