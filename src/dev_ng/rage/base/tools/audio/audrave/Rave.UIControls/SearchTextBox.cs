﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls.Primitives;
using System.ComponentModel;

namespace Rave.UIControls
{

    /// <summary>
    /// A Search Text Box control which contains two dropdown listboxes
    /// </summary>
    public class SearchTextBox : TextBox, INotifyPropertyChanged
    {

        #region WPF Dependency Properties

        public static DependencyProperty LabelTextProperty =
            DependencyProperty.Register(
                "LabelText",
                typeof(string),
                typeof(SearchTextBox));

        public static DependencyProperty LabelTextColorProperty =
            DependencyProperty.Register(
                "LabelTextColor",
                typeof(Brush),
                typeof(SearchTextBox));

        private static DependencyPropertyKey HasTextPropertyKey =
            DependencyProperty.RegisterReadOnly(
                "HasText",
                typeof(bool),
                typeof(SearchTextBox),
                new PropertyMetadata());
        public static DependencyProperty HasTextProperty = HasTextPropertyKey.DependencyProperty;

        private static DependencyPropertyKey IsValidTextPropertyKey =
            DependencyProperty.RegisterReadOnly(
                "IsValidText",
                typeof(bool),
                typeof(SearchTextBox),
                new PropertyMetadata());
        public static DependencyProperty IsValidTextProperty = IsValidTextPropertyKey.DependencyProperty;

        private static DependencyPropertyKey IsMouseLeftButtonDownPropertyKey =
            DependencyProperty.RegisterReadOnly(
                "IsMouseLeftButtonDown",
                typeof(bool),
                typeof(SearchTextBox),
                new PropertyMetadata());
        public static DependencyProperty IsMouseLeftButtonDownProperty = IsMouseLeftButtonDownPropertyKey.DependencyProperty;

        public static readonly RoutedEvent SearchEvent =
            EventManager.RegisterRoutedEvent(
                "Search",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(SearchTextBox));

        public static DependencyProperty SearchArgumentsProperty =
            DependencyProperty.Register(
                "SearchArgumentsProperty",
                typeof(Dictionary<string, string>),
                typeof(SearchTextBox),
                new FrameworkPropertyMetadata(null,
                                                FrameworkPropertyMetadataOptions.None)
             );
        #endregion

        #region Public Properties
        public enum PopUpSelectionStyle
        {
            NormalStyle,
            CheckBoxStyle
        }

        public string LabelText
        {
            get { return (string)GetValue(LabelTextProperty); }
            set { SetValue(LabelTextProperty, value); }
        }

        public Brush LabelTextColor
        {
            get { return (Brush)GetValue(LabelTextColorProperty); }
            set { SetValue(LabelTextColorProperty, value); }
        }

        public bool HasText
        {
            get { return (bool)GetValue(HasTextProperty); }
            private set { SetValue(HasTextPropertyKey, value); }
        }

        public bool IsValidText
        {
            get { return (bool)GetValue(IsValidTextProperty); }
            set { SetValue(IsValidTextPropertyKey, value); }
        }

        public bool IsMouseLeftButtonDown
        {
            get { return (bool)GetValue(IsMouseLeftButtonDownProperty); }
            private set { SetValue(IsMouseLeftButtonDownPropertyKey, value); }
        }

        public event RoutedEventHandler OnSearch
        {
            add { AddHandler(SearchEvent, value); }
            remove { RemoveHandler(SearchEvent, value); }
        }


        public Dictionary<string, string> SearchArguments
        {
            get { return (Dictionary<string, string>)GetValue(SearchArgumentsProperty); }
            set
            {
                SetValue(SearchArgumentsProperty, value);

            }
        }

        public bool ShowSectionButton
        {
            get { return _showSelection; }
            set
            {
                _showSelection = value;
            }
        }

        public PopUpSelectionStyle SelectionStyle
        {
            get { return _popupSelectionStyle; }
            set { _popupSelectionStyle = value; }
        }


        public List<string> SelectedArguments
        {
            get
            {
                List<string> selected = new List<string>();
                foreach (KeyValuePair<string, string> kvp in _searchArgumentList.SelectedItems)
                {
                    selected.Add(kvp.Key);
                }

                return selected;
            }
        }

        public ListBoxMultiSelect SearchArgumentList
        {
            get { return _searchArgumentList; }
            set
            {
                _searchArgumentList = value;

            }
        }
        #endregion

        #region Fields

        private PopUpSelectionStyle _popupSelectionStyle = PopUpSelectionStyle.CheckBoxStyle;
        private Popup _argumentsPopUp = new Popup();
        private ListBoxMultiSelect _searchArgumentList = new ListBoxMultiSelect();


        private bool _showSelection = true;

        #endregion

        #region Static and Constructers
        static SearchTextBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(SearchTextBox),
                new FrameworkPropertyMetadata(typeof(SearchTextBox)));
        }

        public SearchTextBox()
            : base()
        {

        }
        #endregion

        #region Overridden Methods

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            base.OnTextChanged(e);
            HasText = Text.Length != 0;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (e.OriginalSource.GetType() != typeof(Image))
            {
                ShowPopup(SearchArgumentList);
            }

            base.OnMouseDown(e);
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            base.OnLostFocus(e);
            if (!HasText)
                this.LabelText = "Search";
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            base.OnGotFocus(e);
            if (!HasText)
                this.LabelText = "";
            _argumentsPopUp.StaysOpen = true;
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            BuildPopup();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {

            if (e.Key == Key.Escape)
            {
                this.Text = "";
            }
            else if ((e.Key == Key.Return || e.Key == Key.Enter))
            {
                if (HasText && IsValidText)
                {
                    RaiseSearchEvent();
                }
            }
            else
            {
                base.OnKeyDown(e);
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.MouseLeave += new MouseEventHandler(SearchTextBox_MouseLeave);
            Border SearchIcon = GetTemplateChild("PART_SearchIconBorder") as Border;
            if (SearchIcon != null)
            {
                SearchIcon.MouseLeftButtonDown += new MouseButtonEventHandler(IconBorder_MouseLeftButtonDown);
                SearchIcon.MouseLeftButtonUp += new MouseButtonEventHandler(IconBorder_MouseLeftButtonUp);
                SearchIcon.MouseLeave += new MouseEventHandler(IconBorder_MouseLeave);
                SearchIcon.MouseDown += new MouseButtonEventHandler(SearchIcon_MouseDown);
            }
        }

        #endregion

        #region Private Methods and Event Handlers

        private void BuildPopup()
        {
            _argumentsPopUp.PopupAnimation = PopupAnimation.Fade;
            _argumentsPopUp.Placement = PlacementMode.Relative;
            _argumentsPopUp.PlacementTarget = this;
            _argumentsPopUp.PlacementRectangle = new Rect(0, this.ActualHeight, 30, 30);
            _argumentsPopUp.Width = this.ActualWidth;

            SearchArgumentList = new ListBoxMultiSelect((int)_popupSelectionStyle + ListBoxMultiSelect.ItemStyles.NormalStyle);

            SearchArgumentList.Items.Clear();

            if (SearchArguments != null)
            {
                foreach (KeyValuePair<string, string> item in SearchArguments)
                {
                    SearchArgumentList.Items.Add(item);
                }
            }
            SearchArgumentList.Width = this.Width;
            SearchArgumentList.MouseLeave += new MouseEventHandler(ListArguments_MouseLeave);

        }

        private void HidePopup()
        {
            _argumentsPopUp.IsOpen = false;
        }

        private void ShowPopup(UIElement item)
        {
            _argumentsPopUp.StaysOpen = true;

            _argumentsPopUp.Child = item;
            _argumentsPopUp.IsOpen = true;
        }

        private void SearchTypesList_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!this.IsMouseOver && !this._argumentsPopUp.IsMouseOver)
            {
                HidePopup();
            }
        }

        private void ListArguments_MouseLeave(object sender, MouseEventArgs e)
        {
            // close the pop up
            if (!this.IsMouseOver)
            {
                HidePopup();
            }
        }

        private void ChooseSection_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (SearchArguments == null)
                return;
            if (SearchArguments.Count != 0)
                ShowPopup(SearchArgumentList);
        }

        private void SearchTextBox_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!_argumentsPopUp.IsMouseOver)
            {
                HidePopup();
            }
        }

        private void SearchIcon_MouseDown(object sender, MouseButtonEventArgs e)
        {
            HidePopup();
        }

        private void IconBorder_MouseLeftButtonDown(object obj, MouseButtonEventArgs e)
        {
            IsMouseLeftButtonDown = true;
        }

        private void IconBorder_MouseLeftButtonUp(object obj, MouseButtonEventArgs e)
        {
            if (!IsMouseLeftButtonDown) return;

            if (HasText && IsValidText)
            {
                RaiseSearchEvent();
            }

            IsMouseLeftButtonDown = false;
        }

        private void IconBorder_MouseLeave(object obj, MouseEventArgs e)
        {
            IsMouseLeftButtonDown = false;
        }

        private void RaiseSearchEvent()
        {
            if (this.Text == "")
                return;

            SearchEventArgs args = new SearchEventArgs(SearchEvent);
            args.Keyword = this.Text;
            if (SelectedArguments != null)
            {
                args.SearchArguments = SelectedArguments;
            }

            RaiseEvent(args);
        }

        #endregion


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
