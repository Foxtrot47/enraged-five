﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rave.UIControls
{

    public class ListBoxMultiSelect : ListBox
    {
        public enum ItemStyles
        {
            NormalStyle,
            CheckBoxStyle,

        }
        private ItemStyles m_extendedStyle;

        public ItemStyles ExtendedStyle
        {
            get { return m_extendedStyle; }
            set
            {
                m_extendedStyle = value;

                // load resources
                ResourceDictionary resDict = new ResourceDictionary();
                resDict.Source = new Uri("pack://application:,,,/Rave.UIControls;component/Themes/ListBoxMultiSelect.xaml");
                if (resDict.Source == null)
                    throw new SystemException();

                switch (value)
                {
                    case ItemStyles.NormalStyle:
                        this.Style = (Style)resDict["NormalListBox"];
                        break;
                    case ItemStyles.CheckBoxStyle:
                        this.Style = (Style)resDict["CheckListBox"];
                        break;
                }
            }
        }

        static ListBoxMultiSelect()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ListBoxMultiSelect), new FrameworkPropertyMetadata(typeof(ListBoxMultiSelect)));
        }

        public ListBoxMultiSelect(ItemStyles style)
            : base()
        {
            ExtendedStyle = style;
        }

        public ListBoxMultiSelect()
            : base()
        {
            ExtendedStyle = ItemStyles.NormalStyle;
        }


    }
}

