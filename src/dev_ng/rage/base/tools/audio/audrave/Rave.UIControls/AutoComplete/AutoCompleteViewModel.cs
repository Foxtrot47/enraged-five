﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Rave.Instance;
using WPFToolLib.Mouse;
using Xceed.Wpf.AvalonDock.Controls;

namespace Rave.UIControls.AutoComplete
{
	public class AutoCompleteViewModel : INotifyPropertyChanged, IDisposable
	{
		public IEnumerable<string> ListOfResults { get; private set; }
		private AutoCompleteView _autoComplete;
		public Action Disposed;
		public Action<string> NameReturned;

		private ICommand _closeCommand;
		public ICommand CloseCommand
		{
			get
			{
				if (_closeCommand == null)
				{
					_closeCommand = new RelayCommand(Dispose);
				}
				return _closeCommand;
			}
		}

		private ICommand _keyDownCommand;
		public ICommand KeyDownCommand
		{
			get
			{
				if (_keyDownCommand == null)
				{
					_keyDownCommand = new RelayCommand<string>
						(
							KeyDown
						);
				}
				return _keyDownCommand;
			}
		}

		private ICommand _activatedCommand;

		public ICommand ActivatedCommand
		{
			get
			{
				if (_activatedCommand == null)
				{
					_activatedCommand = new RelayCommand
						(
							SetFocus
						);
				}
				return _activatedCommand;
			}
		}

		private ICommand _selectionCompleteCommand;
		public ICommand SelectionCompleteCommand
		{
			get
			{
				if (_selectionCompleteCommand == null)
				{
					_selectionCompleteCommand = new RelayCommand<SelectionChangedEventArgs>
						(
							SelectionComplete
						);
				}
				return _selectionCompleteCommand;
			}
		}

		private void SelectionComplete(SelectionChangedEventArgs args)
		{
			if (NameReturned != null)
			{
				NameReturned((string)args.AddedItems[0]);
			}
			Dispose();
		}

		private void SetFocus()
		{

			_autoComplete.CompleteBox.Focus();
			Keyboard.Focus(_autoComplete.CompleteBox);

		}

		private void KeyDown(string text)
		{
			if (Keyboard.IsKeyDown(Key.Escape))
			{
				Dispose();
			}
		}

		public AutoCompleteViewModel(IEnumerable<string> listOfResults)
		{
			ListOfResults = listOfResults;
			_autoComplete = new AutoCompleteView
			{
				DataContext = this
		
			};
			_autoComplete.Top = MousePosition.GetPoint().Y - _autoComplete.Height/2;
			_autoComplete.Left = MousePosition.GetPoint().X - _autoComplete.Width/2;
			_autoComplete.Show();
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void Dispose()
		{
			CloseAutoComplete();
			ListOfResults = null;
		}

		private void CloseAutoComplete()
		{
			if (_autoComplete != null && _autoComplete.IsVisible)
			{
				if (_autoComplete.IsInitialized)
				{
					_autoComplete.Close();
				}
			}
		}
	}
}
