﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Rave.Instance;

namespace Rave.UIControls.AutoComplete
{
	public static class ObjectNameBox
	{
		private static AutoCompleteViewModel _autoComplete;

		public static void Create(IEnumerable<string> allowedTypes, Action<string> returnNameAction)
		{
			if (_autoComplete != null)
			{
				_autoComplete.Dispose();
			}

			_autoComplete = new AutoCompleteViewModel(GetObjectNames(allowedTypes.ToList()));
			_autoComplete.NameReturned += returnNameAction;
			
		}

		public static void Create(IEnumerable<string> allowedTypes, string episode, Action<string> returnNameAction)
		{
			if (_autoComplete != null)
			{
				_autoComplete.Dispose();
			}

			_autoComplete = new AutoCompleteViewModel(GetObjectNames(allowedTypes.ToList(), episode));
			_autoComplete.NameReturned += returnNameAction;
		}

		public static IEnumerable<string> GetObjectNames(IList<string> allowedTypes)
		{
			IEnumerable<string> objectNames;
			if (allowedTypes != null)
			{

				objectNames = from table in RaveInstance.ObjectLookupTables
							  from kvp in table.Value
							  where (allowedTypes.Contains(kvp.Value.TypeName))
							  select kvp.Value.Name;
			}
			else
			{
				objectNames = from table in RaveInstance.ObjectLookupTables
							  from kvp in table.Value
							  select kvp.Value.Name;
			}
			return objectNames;
		}

		public static IEnumerable<string> GetObjectNames(IList<string> allowedTypes, string episode)
		{
			if (episode.Equals("CORE", StringComparison.InvariantCultureIgnoreCase))
			{
				episode = "BASE";
			}
			IEnumerable<string> objectNames;
			if (allowedTypes != null)
			{
				objectNames = from table in RaveInstance.ObjectLookupTables
							  where (table.Key.Episode.Equals(episode.Replace("_", string.Empty).Trim(), StringComparison.InvariantCultureIgnoreCase))
							  from kvp in table.Value
							  where (allowedTypes.Contains(kvp.Value.TypeName))
							  select kvp.Value.Name;
			}
			else
			{
				objectNames = from table in RaveInstance.ObjectLookupTables
							  from kvp in table.Value
							  where (kvp.Value.Episode.Equals(episode, StringComparison.InvariantCultureIgnoreCase))
							  select kvp.Value.Name;
			}
			return objectNames;
		}
	}
}
