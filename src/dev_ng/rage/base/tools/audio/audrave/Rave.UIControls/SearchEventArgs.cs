﻿using System.Collections.Generic;
using System.Windows;

namespace Rave.UIControls
{
    public class SearchEventArgs : RoutedEventArgs
    {
        private string _keyword = "";

        public string Keyword
        {
            get { return _keyword; }
            set { _keyword = value; }
        }
        private List<string> _searchArguments = new List<string>();

        public List<string> SearchArguments
        {
            get { return _searchArguments; }
            set { _searchArguments = value; }
        }

        private List<string> _searchTypes = new List<string>();

        public List<string> SearchTypes
        {
            get { return _searchTypes; }
            set { _searchTypes = value; }
        }

        public SearchEventArgs()
            : base()
        {

        }

        public SearchEventArgs(RoutedEvent routedEvent)
            : base(routedEvent)
        {

        }
    }

}
