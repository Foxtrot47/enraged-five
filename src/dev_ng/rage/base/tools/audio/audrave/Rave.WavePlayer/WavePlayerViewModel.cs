﻿using System.ComponentModel;
using System.Windows.Media;
using WPFToolLib.Extensions;

namespace Rave.WavePlayer
{
	public class WavePlayerViewModel : INotifyPropertyChanged
	{
		private bool _canPlay;
		private double _loopAnalysisWidth = double.NaN;
		private NAudioEngine _soundEngine;
		private Brush _waveformBrush = Brushes.CornflowerBlue;
		private double _waveformVisibility;

		public WavePlayerViewModel()
		{
			SoundEngine.PropertyChanged += (p, r) => { PropertyChanged.Raise(this, null); };
		}

		public Brush WaveformBrush
		{
			get { return _waveformBrush; }
			set
			{
				_waveformBrush = value;
				PropertyChanged.Raise(this, "WaveformBrush");
			}
		}

		public double TimelineWidth
		{
			get { return SoundEngine.WaveformWidth; }
			set
			{
				SoundEngine.WaveformWidth = value;
				PropertyChanged.Raise(this, "TimelineWidth");
			}
		}

		public double WaveformVisibility
		{
			get { return WaveformBrush.Opacity * 100; }
			set
			{
				_waveformVisibility = value;
				Brush fillBrush = WaveformBrush.Clone();
				fillBrush.Opacity = _waveformVisibility / 100;
				WaveformBrush = fillBrush;

				PropertyChanged.Raise(this, "WaveformVisibility");
			}
		}

		public double LoopAnalysisWidth
		{
			get { return _loopAnalysisWidth; }
			set
			{
				_loopAnalysisWidth = value;
				PropertyChanged.Raise(this, "LoopAnalysisWidth");
			}
		}

		public bool HasLoop
		{
			get { return SoundEngine.LoopData != null; }
		}

		public NAudioEngine SoundEngine
		{
			get
			{
				if (_soundEngine == null)
				{
					_soundEngine = new NAudioEngine();
				}
				return _soundEngine;
			}
		}

		public bool CanPlay
		{
			get { return SoundEngine.CanPlay; }
		}

		public bool CanPause
		{
			get { return SoundEngine.CanPause; }
		}

		public bool CanStop
		{
			get { return SoundEngine.CanStop; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void OpenFile(string filePath)
		{
			SoundEngine.OpenFile(filePath);
			PropertyChanged.Raise(this, "SoundEngine");
		}

		public void Play()
		{
			SoundEngine.Play();
		}

		public void Pause()
		{
			SoundEngine.Pause();
		}

		public void Stop()
		{
			SoundEngine.Stop();
		}

		public void Dispose()
		{
			SoundEngine.Dispose();
		}
	}
}