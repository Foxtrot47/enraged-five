﻿using System;
using System.Collections.Generic;
using System.IO;
using NAudio.Wave;

namespace Rave.WavePlayer
{
	public class SampleLoop
	{
		public UInt32 End;
		public UInt32 Fraction;
		public UInt32 Identifier;
		public UInt32 PlayCount;
		public UInt32 Start;
		public UInt32 Type;

		public SampleLoop()
		{
		}

		public SampleLoop(BinaryReader br)
		{
			Identifier = br.ReadUInt32();
			Type = br.ReadUInt32();
			Start = br.ReadUInt32();
			End = br.ReadUInt32();
			Fraction = br.ReadUInt32();
			PlayCount = br.ReadUInt32();
		}

		public static List<SampleLoop> GetLoops(CueWaveFileReader waveformStream)
		{
			List<SampleLoop> Loops = new List<SampleLoop>();
			foreach (RiffChunk riffcuChunk in waveformStream.ExtraChunks)
			{
				if (riffcuChunk.IdentifierAsString.Equals("smpl"))
				{
					Console.WriteLine(riffcuChunk.Identifier);
					Console.WriteLine(riffcuChunk.Length);
					Console.WriteLine(riffcuChunk.StreamPosition);
					MemoryStream ms = new MemoryStream(waveformStream.GetChunkData(riffcuChunk));
					BinaryReader br = new BinaryReader(ms);
					uint Manufacturer = br.ReadUInt32();
					uint Product = br.ReadUInt32();
					uint SamplePeriod = br.ReadUInt32();
					uint MIDIUnityNote = br.ReadUInt32();
					uint MIDIPitchFraction = br.ReadUInt32();
					uint SMPTEFormat = br.ReadUInt32();
					uint SMPTEOffset = br.ReadUInt32();
					uint SampleLoops = br.ReadUInt32();
					uint SamplerData = br.ReadUInt32();
					if (SamplerData > 0)
					{
						byte[] Data = new byte[SamplerData];
					}

					for (uint i = 0; i < SampleLoops; i++)
					{
						Loops.Add(new SampleLoop(br));
					}
				}
			}
			return Loops;
		}
	}
}