﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using Rave.WaveBrowser.Infrastructure.Nodes;

namespace Rave.WavePlayer.WaveAuditionPopup
{
	/// <summary>
	///     Interaction logic for WaveAuditionPopup.xaml
	/// </summary>
	public partial class WaveAuditionPopup : INotifyPropertyChanged
	{
		public WaveAuditionPopup(string wavePath, WaveNode waveNode)
		{
			InitializeComponent();
			DataContext = new ViewModel(wavePath, waveNode);
		}

		public Thickness CaptionButtonMargin
		{
			get
			{
				if (WindowState == WindowState.Maximized)
					return new Thickness(6, 6, 0, 0); //Margin="0,0,12,0"
				return new Thickness(0, 0, 0, 0);
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
		{
			WavePlayer.Dispose();

			Close();
		}

		protected override void OnStateChanged(EventArgs e)
		{
			base.OnStateChanged(e);
			OnPropertyChanged("CaptionButtonMargin");
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			HandleSizeToContent();
		}

		private void HandleSizeToContent()
		{
			if (SizeToContent == SizeToContent.Manual)
				return;

			SizeToContent previousSizeToContent = SizeToContent;
			SizeToContent = SizeToContent.Manual;

			Dispatcher.BeginInvoke(
				DispatcherPriority.Loaded,
				(Action) (() => { SizeToContent = previousSizeToContent; }));
		}

		private void OnPropertyChanged(String info)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(info));
			}
		}
	}
}