using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;

namespace Rave.WavePlayer
{
    public class WaveFormGenerator
    {
        private NAudioEngine _nAudioEngine;

        public WaveFormGenerator(NAudioEngine nAudioEngine)
        {
            _nAudioEngine = nAudioEngine;
        }

        internal void WaveformGenerateWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            WaveformGenerationParams waveformParams = e.Argument as WaveformGenerationParams;
            if (waveformParams == null || waveformParams.Path == null) return;

            CueWaveFileReader waveformStream = new CueWaveFileReader(waveformParams.Path);
            Pcm16BitToSampleProvider sampleProvider1 = new Pcm16BitToSampleProvider(waveformStream);
            MultiplexingSampleProvider sampleProvider = new MultiplexingSampleProvider(new List<ISampleProvider> { sampleProvider1 }, 2);
            NotifyingSampleProvider sampleNotifier = new NotifyingSampleProvider(sampleProvider);
            sampleNotifier.Sample += WaveStreamSample;
            
            int frameLength =
                Math.Max((int)(Math.Pow(Math.Min((double)((_nAudioEngine.VisibleRect.Width) / _nAudioEngine.WaveformWidth), 1), 1.2) * waveformStream.Length / 4096), 10);

            int waveformLength = (int)_nAudioEngine.VisibleRect.Width * 2;
            float[] readBuffer = new float[frameLength];
            _nAudioEngine.WaveformAggregator = new SampleAggregator(frameLength);

            float maxLeftPointLevel = float.MinValue;
            float maxRightPointLevel = float.MinValue;

            float[] waveformCompressedPoints = new float[waveformLength];
            List<int> wavePointIndex = new List<int>();

            //Get the index depending to the waveform (visualizer length)
            for (int i = 1; i <= waveformLength; i++)
            {
                wavePointIndex.Add(
                    (int)
                        (Math.Floor(i *
                                    (Math.Min((double)(_nAudioEngine.VisibleRect.Width / _nAudioEngine.WaveformWidth), 1) * waveformStream.Length * _nAudioEngine.ActiveStream.WaveFormat.Channels / (waveformLength * (double)frameLength)))));
            }
            waveformStream.Seek((long)((_nAudioEngine.VisibleRect.Left / _nAudioEngine.WaveformWidth) * waveformStream.Length), SeekOrigin.Begin);

            int readCount = 0;
            float[] fftData = new float[NAudioEngine.FftDataSize];
            _nAudioEngine.FftSampleAggregator = new SampleAggregator(NAudioEngine.FftDataSize);

            int currentPointIndex = 0;
            List<float[]> spectrumData = new List<float[]>(waveformLength / 2);
            while (currentPointIndex < waveformCompressedPoints.Length - 1)
            {
                if (_nAudioEngine.WaveformGenerateWorker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                //Read the next input
                sampleNotifier.Read(readBuffer, 0, readBuffer.Length);
                //Find the max/peak point
                if (_nAudioEngine.WaveformAggregator.LeftMaxVolume > maxLeftPointLevel)
                {
                    maxLeftPointLevel = _nAudioEngine.WaveformAggregator.LeftMaxVolume;
                }

                if (_nAudioEngine.WaveformAggregator.RightMaxVolume > maxRightPointLevel)
                    maxRightPointLevel = _nAudioEngine.WaveformAggregator.RightMaxVolume;

                //If readCount reached the next point add to compressed waveform
                if (readCount >= wavePointIndex[currentPointIndex])
                {
                    waveformCompressedPoints[currentPointIndex] = maxLeftPointLevel;
                    waveformCompressedPoints[currentPointIndex + 1] = maxRightPointLevel;

                    maxLeftPointLevel = float.MinValue;
                    maxRightPointLevel = float.MinValue;
                    currentPointIndex += 2;

                    _nAudioEngine.FftSampleAggregator.GetFFTResults(fftData);
                    //get fftdata:
                    spectrumData.Add(fftData);
                    fftData = new float[NAudioEngine.FftDataSize];

                }


                if (currentPointIndex % NAudioEngine.SamplesToBuffer == 0)
                {
                    float[] clonedData = (float[])waveformCompressedPoints.Clone();
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        _nAudioEngine.WaveformData = clonedData;
                    }));
                }

                readCount++;
            }

            float[] finalClonedData = (float[])waveformCompressedPoints.Clone();
            List<float[]> finalSpectrumDataClone = spectrumData.Select(item => (float[])item.Clone()).ToList();
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {

                _nAudioEngine.WaveformData = finalClonedData;
                _nAudioEngine.SpectrumData = finalSpectrumDataClone;
            }));
            //sampleNotifier.Close();
            //sampleNotifier.Dispose();
            waveformStream.Close();
            waveformStream.Dispose();
        }

        private void WaveStreamSample(object sender, SampleEventArgs e)
        {
            _nAudioEngine.WaveformAggregator.Add(e.Left, e.Right);
            _nAudioEngine.FftSampleAggregator.Add(e.Left, e.Right);
        }
    }
}