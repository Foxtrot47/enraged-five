﻿namespace Rave.WavePlayer
{
    public class WaveformGenerationParams
    {
        public WaveformGenerationParams(string path, double width)
        {
            Path = path;
            Width = width;
        }

        public string Path { get; protected set; }
        public double Width { get; set; }
    }

}