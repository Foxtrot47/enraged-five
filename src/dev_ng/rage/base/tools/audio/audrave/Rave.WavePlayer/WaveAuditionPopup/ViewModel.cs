﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Linq;
using NAudio.Wave;
using rage;
using Rave.Utils;
using Rave.WaveBrowser.Infrastructure.Nodes;
using WPFToolLib.Extensions;

namespace Rave.WavePlayer.WaveAuditionPopup
{
	public class ViewModel : INotifyPropertyChanged
	{
		private int _bitsPerSample;
		private string _builtWavesText;
		private int _channelCount;
		private TimeSpan _duration;
		private long _sampleCount;
		private int _sampleRate;
		private string _waveName;
		private string _wavePath;

		public ViewModel(string wavePath, WaveNode waveNode)
		{
			WaveName = waveNode.Text;

			BuiltWavesText = GetBuiltWaveText(waveNode);
			WavePath = wavePath;
		    if (!File.Exists(WavePath))
		    {
		        return;
		    }
			WaveFileReader waveFileReader = new WaveFileReader(WavePath);
			Duration = waveFileReader.TotalTime;
			BitsPerSample = waveFileReader.WaveFormat.BitsPerSample;
			SampleCount = waveFileReader.SampleCount;
			SampleRate = waveFileReader.WaveFormat.SampleRate;
			ChannelCount = waveFileReader.WaveFormat.Channels;
		}

		public string BuiltWavesText
		{
			get { return _builtWavesText; }
			private set
			{
				_builtWavesText = value;
				PropertyChanged.Raise(this, "BuiltWavesText");
			}
		}

		public string WavePath
		{
			get { return _wavePath; }
			private set
			{
				_wavePath = value;
				PropertyChanged.Raise(this, "WavePath");
			}
		}

		public string WaveName
		{
			get { return _waveName; }
			private set
			{
				_waveName = value;
				PropertyChanged.Raise(this, "WaveName");
			}
		}

		public int BitsPerSample
		{
			get { return _bitsPerSample; }
			set
			{
				_bitsPerSample = value;
				PropertyChanged.Raise(this, "BitsPerSample");
			}
		}

		public int ChannelCount
		{
			get { return _channelCount; }
			set
			{
				_channelCount = value;
				PropertyChanged.Raise(this, "ChannelCount");
			}
		}

		public int SampleRate
		{
			get { return _sampleRate; }
			set
			{
				_sampleRate = value;
				PropertyChanged.Raise(this, "SampleRate");
			}
		}

		public long SampleCount
		{
			get { return _sampleCount; }
			set
			{
				_sampleCount = value;
				PropertyChanged.Raise(this, "Duration");
			}
		}

		public TimeSpan Duration
		{
			get { return _duration; }
			set
			{
				_duration = value;
				PropertyChanged.Raise(this, "Duration");
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private string GetBuiltWaveText(WaveNode node)
		{
			StringBuilder sb = new StringBuilder();
			foreach (PlatformSetting ps in Configuration.ActivePlatformSettings)
			{
				sb.AppendLine(ps.Name);

				XElement xml = node.GetNodeXmlForPlatform(ps.PlatformTag);
				if (xml != null)
				{
					DateTime? buildDate = FindBuildTimeStamp(xml);
					if (null != buildDate)
					{
						sb.AppendFormat("Build date: {0}", buildDate);
						sb.AppendLine();
					}
					foreach (XElement chunkElem in xml.Elements("Chunk"))
					{
						sb.AppendFormat(
							"{0}: {1} bytes",
							chunkElem.Attribute("name").Value,
							chunkElem.Attribute("size").Value);
						sb.AppendLine();

						foreach (XElement markerElem in chunkElem.Elements("Marker"))
						{
							XAttribute timeMsAttr = markerElem.Attribute("timeMs");
							string timeMs = null != timeMsAttr ? timeMsAttr.Value : string.Empty;
							sb.AppendFormat(" - {0}: {1}", markerElem.Value, timeMs);
							sb.AppendLine();
						}
					}
				}
				sb.AppendLine();
			}

			return sb.ToString();
		}

		private DateTime? FindBuildTimeStamp(XElement waveNode)
		{
			XElement bankNode = FindBankNode(waveNode);
			XAttribute attrTimeStamp = bankNode.Attribute("timeStamp");
			if (attrTimeStamp != null)
			{
				return DateTime.Parse(attrTimeStamp.Value);
			}
			return null;
		}

		private XElement FindBankNode(XElement searchNode)
		{
			if (searchNode == null)
			{
				return null;
			}
			if (searchNode.Name == "Bank")
			{
				return searchNode;
			}
			return FindBankNode(searchNode.Parent);
		}
	}
}