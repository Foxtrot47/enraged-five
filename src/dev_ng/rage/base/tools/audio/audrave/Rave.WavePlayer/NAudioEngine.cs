﻿// (c) Copyright Jacob Johnston.
// This source is subject to Microsoft Public License (Ms-PL).
// Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
// All other rights reserved.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using WPFSoundVisualizationLib;
using WPFToolLib.Extensions;

namespace Rave.WavePlayer
{
    public class NAudioEngine : INotifyPropertyChanged, ISpectrumPlayer, IWaveformPlayer, IDisposable
    {

        public NAudioEngine()
        {
            _positionTimer.Interval = TimeSpan.FromMilliseconds(1);
            _positionTimer.Tick += PositionTimerTick;

            _waveFormGenerator = new WaveFormGenerator(this);
            _waveformGenerateWorker.DoWork += _waveFormGenerator.WaveformGenerateWorkerDoWork;
            _waveformGenerateWorker.RunWorkerCompleted += waveformGenerateWorker_RunWorkerCompleted;
            _waveformGenerateWorker.WorkerSupportsCancellation = true;
        }

        public static NAudioEngine Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new NAudioEngine();
                return _instance;
            }
        }

        public double[] CuePositions { get; private set; }

        public List<float[]> SpectrumData
        {
            get { return _spectrumData; }
            set
            {
                _spectrumData = value;
                PropertyChanged.Raise(this, "SpectrumData");
            }
        }

        private void StopAndCloseStream()
        {
            if (_waveOutDevice != null)
            {
                _waveOutDevice.Stop();
            }
            if (_activeStream != null)
            {
                ActiveStream.Close();
                ActiveStream = null;
            }
            if (_waveOutDevice != null)
            {
                _waveOutDevice.Dispose();
                _waveOutDevice = null;
            }
        }

        private static NAudioEngine _instance;
        public const int FftDataSize = (int)FFTDataSize.FFT2048;
        private readonly DispatcherTimer _positionTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle);
        private readonly BackgroundWorker _waveformGenerateWorker = new BackgroundWorker();
        private WaveStream _activeStream;
        private bool _canPause;
        private bool _canPlay;
        private bool _canStop;
        private double _channelLength;
        private double _channelPosition;
        private CueList _cueList;
        private string _currentPath;
        private bool _disposed;
        private SampleAggregator _fftSampleAggregator;
        private float[] _fullLevelData;
        private bool _inChannelSet;
        private bool _inChannelTimerUpdate;
        private bool _inRepeatSet;
        private bool _isPlaying;
        private short[] _loopData;
        private string _pendingWaveformPath;
        private TimeSpan _repeatStart;
        private TimeSpan _repeatStop;
        private SampleAggregator _sampleAggregator;
        private List<float[]> _spectrumData;
        private Rect _visibleRect;
        private SampleAggregator _waveformAggregator;
        private float[] _waveformData = new float[2];
        private double _waveformWidth;
        private WaveOut _waveOutDevice;
        private readonly WaveFormGenerator _waveFormGenerator;

        private const int RepeatThreshold = 10;
        public const int SamplesToBuffer = 128;

        public void Dispose()
        {
            Stop();
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    StopAndCloseStream();
                }

                _disposed = true;
            }
        }

        public bool GetFFTData(float[] fftDataBuffer)
        {
            _sampleAggregator.GetFFTResults(fftDataBuffer);
            return _isPlaying;
        }

        public int GetFFTFrequencyIndex(int frequency)
        {
            double maxFrequency;
            if (ActiveStream != null)
                maxFrequency = ActiveStream.WaveFormat.SampleRate / 2.0d;
            else
                maxFrequency = 22050; // Assume a default 44.1 kHz sample rate.
            return (int)((frequency / maxFrequency) * (FftDataSize / 2));
        }

        public int GetFFTFrequencyIndex(int frequency, int fftSize)
        {
            double maxFrequency;
            if (ActiveStream != null)
                maxFrequency = ActiveStream.WaveFormat.SampleRate / 2.0d;
            else
                maxFrequency = 22050; // Assume a default 44.1 kHz sample rate.
            return (int)((frequency / maxFrequency) * (fftSize / 2));
        }


        public double GetFrequencyFromIndex(int index, int fftSize)
        {
            return index * (ActiveStream.WaveFormat.SampleRate / fftSize);
        }

        public int SampleRate { get { return ActiveStream.WaveFormat.SampleRate; } }

        public double ChannelLength
        {
            get { return _channelLength; }
            protected set
            {
                double oldValue = _channelLength;
                _channelLength = value;
                if (oldValue != _channelLength)
                    NotifyPropertyChanged("ChannelLength");
            }
        }

        public double ChannelPosition
        {
            get { return _channelPosition; }
            set
            {
                if (!_inChannelSet)
                {
                    _inChannelSet = true; // Avoid recursion
                    double oldValue = _channelPosition;
                    double position = Math.Max(0, Math.Min(value, ChannelLength));
                    if (!_inChannelTimerUpdate && ActiveStream != null)
                        ActiveStream.Position = (long)((position / ActiveStream.TotalTime.TotalSeconds) * ActiveStream.Length);
                    _channelPosition = position;
                    if (oldValue != _channelPosition)
                        NotifyPropertyChanged("ChannelPosition");
                    _inChannelSet = false;
                }
            }
        }

        public TimeSpan SelectionBegin
        {
            get { return _repeatStart; }
            set
            {
                if (!_inRepeatSet)
                {
                    _inRepeatSet = true;
                    TimeSpan oldValue = _repeatStart;
                    _repeatStart = value;
                    if (oldValue != _repeatStart)
                        NotifyPropertyChanged("SelectionBegin");
                    _inRepeatSet = false;
                }
            }
        }

        public TimeSpan SelectionEnd
        {
            get { return _repeatStop; }
            set
            {
                if (!_inChannelSet)
                {
                    _inRepeatSet = true;
                    TimeSpan oldValue = _repeatStop;
                    _repeatStop = value;
                    if (oldValue != _repeatStop)
                        NotifyPropertyChanged("SelectionEnd");
                    _inRepeatSet = false;
                }
            }
        }

        public float[] WaveformData
        {
            get { return _waveformData; }
            protected internal set
            {
                float[] oldValue = _waveformData;
                _waveformData = value;
                if (oldValue != _waveformData)
                    NotifyPropertyChanged("WaveformData");
            }
        }

        public double WaveformWidth
        {
            get { return _waveformWidth; }
            set
            {
                if (value > 100)
                {
                    _waveformWidth = value;
                    NotifyPropertyChanged("WaveformWidth");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        private void GenerateWaveformData(string path)
        {
            _pendingWaveformPath = path;
            if (_waveformGenerateWorker.IsBusy)
            {
                _waveformGenerateWorker.CancelAsync();
                return;
            }

            if (!_waveformGenerateWorker.IsBusy)
                _waveformGenerateWorker.RunWorkerAsync(new WaveformGenerationParams(path, WaveformWidth));
        }

        private void waveformGenerateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                if (!_waveformGenerateWorker.IsBusy)
                    _waveformGenerateWorker.RunWorkerAsync(new WaveformGenerationParams(_pendingWaveformPath, WaveformWidth));
            }
            //else
            //{
            //	Detail *= 2;
            //	if (!_waveformGenerateWorker.IsBusy)
            //		_waveformGenerateWorker.RunWorkerAsync(new WaveformGenerationParams(_pendingWaveformPath, WaveformWidth));

            //}
        }


        private const int LoopBufferSize = 100;

        public string CurrentPath
        {
            get { return _currentPath; }
            set
            {
                _currentPath = value;
                PropertyChanged.Raise(this, "CurrentPath");
            }
        }

        public Int16[] LoopData
        {
            get { return _loopData; }
            private set
            {
                _loopData = value;
                PropertyChanged.Raise(this, "LoopData");
            }
        }

        public void OpenFile(string path)
        {
            CurrentPath = path;
            Stop();

            if (ActiveStream != null)
            {
                SelectionBegin = TimeSpan.Zero;
                SelectionEnd = TimeSpan.Zero;
                ChannelPosition = 0;
            }

            StopAndCloseStream();

            if (File.Exists(path))
            {
                try
                {
                    _waveOutDevice = new WaveOut
                    {
                        DesiredLatency = 100
                    };
                    
                    ActiveStream = new WaveFileReader(path);
                    Pcm16BitToSampleProvider pcm16BitToSampleProvider = new Pcm16BitToSampleProvider(ActiveStream);
                    MultiplexingSampleProvider sampleProvider = new MultiplexingSampleProvider(new List<ISampleProvider> { pcm16BitToSampleProvider }, 2);

                    int outputChannels = _waveOutDevice.NumberOfBuffers;
                    int inputChannels = pcm16BitToSampleProvider.WaveFormat.Channels;
                    for (int i = 0; i < outputChannels && i < inputChannels; i++)
                    {
                        sampleProvider.ConnectInputToOutput(i, i);
                    }

                    if (outputChannels == 2 && pcm16BitToSampleProvider.WaveFormat.Channels == 1)
                    {
                        sampleProvider.ConnectInputToOutput(0,0);
                        sampleProvider.ConnectInputToOutput(0, 1);
                    }
                    
                    NotifyingSampleProvider sampleNotifier = new NotifyingSampleProvider(sampleProvider);
                    _sampleAggregator = new SampleAggregator(FftDataSize);
                    
                    sampleNotifier.Sample += InputStreamSample;
                    _waveOutDevice.Init(sampleNotifier);
                    ChannelLength = ActiveStream.TotalTime.TotalSeconds;
                    GenerateWaveformData(path);
                    CanPlay = true;
                    CueWaveFileReader waveformStream = new CueWaveFileReader(path);
                    CueList = waveformStream.Cues;

                    LoopData = GetLoopData(waveformStream);
                }
                catch
                {
                    ActiveStream = null;
                    CanPlay = false;
                }
            }
        }

        public void Pause()
        {
            if (IsPlaying && CanPause)
            {
                _waveOutDevice.Pause();
                IsPlaying = false;
                CanPlay = true;
                CanPause = false;
            }
        }

        public void Play()
        {
            if (CanPlay)
            {
                _waveOutDevice.Play();
                IsPlaying = true;
                CanPause = true;
                CanPlay = false;
                CanStop = true;
            }
        }

        public void Stop()
        {
            if (_waveOutDevice != null)
            {
                _waveOutDevice.Stop();
                ChannelPosition = 0.0;
            }
            IsPlaying = false;
            CanStop = false;
            CanPlay = true;
            CanPause = false;
        }


        private short[] GetLoopData(CueWaveFileReader waveformStream)
        {
            List<SampleLoop> loops = SampleLoop.GetLoops(waveformStream);
            if (loops.Count < 1)
            {
                return null;
            }
            byte[] readbuffer = new byte[LoopBufferSize];

            foreach (SampleLoop loop in loops)
            {
                SelectionBegin =
                    new TimeSpan(TimeSpan.TicksPerSecond * loop.Start /
                                 (waveformStream.WaveFormat.SampleRate * waveformStream.WaveFormat.Channels));
                SelectionEnd =
                    new TimeSpan(TimeSpan.TicksPerSecond * loop.End /
                                 (waveformStream.WaveFormat.SampleRate * waveformStream.WaveFormat.Channels));
                waveformStream.Seek(loop.End - LoopBufferSize / 2, SeekOrigin.Begin);
                waveformStream.Read(readbuffer, 0, LoopBufferSize / 2);
                waveformStream.Seek(loop.Start, SeekOrigin.Begin);
                waveformStream.Read(readbuffer, LoopBufferSize / 2, LoopBufferSize / 2);
            }
            short[] actualbuffer = new Int16[readbuffer.Length / 2];
            for (int i = 0; i < readbuffer.Length; i += 2)
            {
                actualbuffer[i / 2] = BitConverter.ToInt16(readbuffer, i);
            }
            return actualbuffer;
        }

        public WaveStream ActiveStream
        {
            get { return _activeStream; }
            protected set
            {
                WaveStream oldValue = _activeStream;
                _activeStream = value;
                if (oldValue != _activeStream)
                    NotifyPropertyChanged("ActiveStream");
            }
        }

        public bool CanPause
        {
            get { return _canPause; }
            protected set
            {
                bool oldValue = _canPause;
                _canPause = value;
                if (oldValue != _canPause)
                    NotifyPropertyChanged("CanPause");
            }
        }

        public bool CanPlay
        {
            get { return _canPlay; }
            protected set
            {
                bool oldValue = _canPlay;
                _canPlay = value;
                if (oldValue != _canPlay)
                    NotifyPropertyChanged("CanPlay");
            }
        }

        public bool CanStop
        {
            get { return _canStop; }
            protected set
            {
                bool oldValue = _canStop;
                _canStop = value;
                if (oldValue != _canStop)
                    NotifyPropertyChanged("CanStop");
            }
        }

        public Dictionary<double, string> CueDictionary
        {
            get { return CueListToDictionary(CueList); }
        }

        public CueList CueList
        {
            get { return _cueList; }
            private set
            {
                _cueList = value;
                PropertyChanged.Raise(this, "CueList");
            }
        }

        public bool IsPlaying
        {
            get { return _isPlaying; }
            protected set
            {
                bool oldValue = _isPlaying;
                _isPlaying = value;
                if (oldValue != _isPlaying)
                    NotifyPropertyChanged("IsPlaying");
                _positionTimer.IsEnabled = value;
            }
        }

        public Rect VisibleRect
        {
            get { return _visibleRect; }
            set
            {
                _visibleRect = value;

                PropertyChanged.Raise(this, "VisibleRect");
                GenerateWaveformData(CurrentPath);
            }
        }

        public BackgroundWorker WaveformGenerateWorker
        {
            get { return _waveformGenerateWorker; }
        }

        public SampleAggregator FftSampleAggregator
        {
            set { _fftSampleAggregator = value; }
            get { return _fftSampleAggregator; }
        }

        public SampleAggregator WaveformAggregator
        {
            set { _waveformAggregator = value; }
            get { return _waveformAggregator; }
        }

        private Dictionary<double, string> CueListToDictionary(CueList cueList)
        {
            Dictionary<double, string> cueDictionary = new Dictionary<double, string>();
            if (cueList != null)
            {
                CuePositions = new double[cueList.Count];
                for (int i = 0; i < cueList.Count; i++)
                {
                    if (ActiveStream.WaveFormat != null)
                    {
                        double key = (double)cueList[i].Position / ActiveStream.WaveFormat.SampleRate;
                        CuePositions[i] = key;
                        cueDictionary[key] = cueList[i].Label;
                    }
                }
            }
            return cueDictionary;
        }

        public void MoveToNearbyCue(bool forward = true)
        {
            if (CueDictionary == null || CueDictionary.Count < 1)
            {
                if (!forward)
                {
                    ChannelPosition = 0;
                }
                return;
            }
            if (forward)
            {
                List<double> cuesSorted = CueDictionary.Keys.ToList();
                cuesSorted.Sort();

                double position = cuesSorted.FirstOrDefault(p => p > (ChannelPosition));
                ChannelPosition = position;
            }
            else
            {
                List<double> cuesSorted = CueDictionary.Keys.ToList();
                cuesSorted.Sort();
                int index = cuesSorted.IndexOf(cuesSorted.FirstOrDefault(p => p > (ChannelPosition - 0.2))) - 1;
                double position = 0;
                if (index > 0)
                {
                    position = cuesSorted.ElementAt(index);
                }
                ChannelPosition = position;
            }
        }

        private void InputStreamSample(object sender, SampleEventArgs e)
        {
            _sampleAggregator.Add(e.Left, e.Right);
            long repeatStartPosition =
                (long)(((Math.Max(SelectionBegin.TotalSeconds, 0) / ActiveStream.TotalTime.TotalSeconds)) * ActiveStream.Length);
            long repeatStopPosition = (long)((SelectionEnd.TotalSeconds / ActiveStream.TotalTime.TotalSeconds) * ActiveStream.Length);
            if (((SelectionEnd - SelectionBegin) >= TimeSpan.FromMilliseconds(RepeatThreshold)) &&
                ActiveStream.Position >= repeatStopPosition)
            {
                _sampleAggregator.Clear();
                ActiveStream.Position = repeatStartPosition;
            }
        }

        private void PositionTimerTick(object sender, EventArgs e)
        {
            _inChannelTimerUpdate = true;
            ChannelPosition = (ActiveStream.Position / (double)ActiveStream.Length) * ActiveStream.TotalTime.TotalSeconds;
            _inChannelTimerUpdate = false;

            if (ActiveStream.Position == ActiveStream.Length)
            {
                Stop();
            }
        }
    }
}