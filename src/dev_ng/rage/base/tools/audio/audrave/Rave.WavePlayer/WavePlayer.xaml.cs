﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Rave.WavePlayer
{
	/// <summary>
	///     Interaction logic for WavePlayer.xaml
	/// </summary>
	public partial class WavePlayer : IDisposable, INotifyPropertyChanged
	{
		private double _originalLoopAnalysisWidth;
		public WavePlayerViewModel WavePlayerViewModel;

		public WavePlayer()
		{
			WavePlayerViewModel = new WavePlayerViewModel();
			InitializeComponent();
			DataContext = WavePlayerViewModel;
			BindSoundPlayer();
			WavePlayerViewModel.SoundEngine.PropertyChanged += NAudioEngine_PropertyChanged;
		}

		public string WavFile
		{
			get { return (string)GetValue(WavFileDependencyProperty); }
			set { SetValue(WavFileDependencyProperty, value); }
		}

		public void Dispose()
		{
			if (WavePlayerViewModel == null) return;
			WavePlayerViewModel.Dispose();
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void BindSoundPlayer()
		{
			SpectrumAnalyzer.RegisterSoundPlayer(WavePlayerViewModel.SoundEngine);
			LoopAnalysis.RegisterSoundPlayer(WavePlayerViewModel.SoundEngine);
			LoopAnalysisToolTip.RegisterSoundPlayer(WavePlayerViewModel.SoundEngine);
			WavePlayerViewModel.TimelineWidth = 100;
		}

		#region NAudio Engine Events

		private void NAudioEngine_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			NAudioEngine engine = WavePlayerViewModel.SoundEngine;
			switch (e.PropertyName)
			{
				case "ChannelPosition":
					ClockDisplay.Time = TimeSpan.FromSeconds(engine.ChannelPosition);
					break;
				case "CurrentPath":
					BindSoundPlayer();
					break;
			}
		}

		#endregion

		private void PlayButton_Click(object sender, RoutedEventArgs e)
		{
			if (WavePlayerViewModel.CanPlay)
				WavePlayerViewModel.Play();
		}

		private void PauseButton_Click(object sender, RoutedEventArgs e)
		{
			if (WavePlayerViewModel.CanPause)
				WavePlayerViewModel.Pause();
		}

		private void StopButton_Click(object sender, RoutedEventArgs e)
		{
			if (WavePlayerViewModel.CanStop)
				WavePlayerViewModel.Stop();
		}

		private static void DependencyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			WavePlayer control = (WavePlayer)d;
			control.WavePlayerViewModel.OpenFile(control.WavFile);
		}

		private void PreviewMouseWheelTimeline(object sender, MouseWheelEventArgs mouseWheelEventArgs)
		{
			if (WavePlayerViewModel == null) return;
			if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
			{
				if (double.IsNaN(WavePlayerViewModel.TimelineWidth))
				{
					WavePlayerViewModel.TimelineWidth = WaveFormScrollViewer.ActualWidth;
				}

				WavePlayerViewModel.TimelineWidth += mouseWheelEventArgs.Delta > 0 ? 100 : -100;
				WavePlayerViewModel.TimelineWidth = mouseWheelEventArgs.Delta > 0
					? WavePlayerViewModel.TimelineWidth * 2
					: WavePlayerViewModel.TimelineWidth / 2;
				WavePlayerViewModel.TimelineWidth = WavePlayerViewModel.TimelineWidth > WaveFormScrollViewer.ActualWidth
					? WavePlayerViewModel.TimelineWidth
					: WaveFormScrollViewer.ActualWidth - 10;
				mouseWheelEventArgs.Handled = true;
			}
		}

		private void WavePlayer_OnLoaded(object sender, RoutedEventArgs e)
		{
			if (WavePlayerViewModel == null) return;
			WavePlayerViewModel.TimelineWidth = WaveFormScrollViewer.ActualWidth - 10;
		}

		private void WaveFormScrollViewer_OnScrollChanged(object sender, ScrollChangedEventArgs e)
		{
			if (WavePlayerViewModel == null) return;
			WavePlayerViewModel.SoundEngine.VisibleRect = new Rect(WaveFormScrollViewer.ContentHorizontalOffset,
				WaveFormScrollViewer.ContentVerticalOffset, WaveFormScrollViewer.ActualWidth, WaveFormScrollViewer.ActualHeight);
		}

		private void WaveFormScrollViewer_OnSizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (WavePlayerViewModel == null) return;
			if (WavePlayerViewModel.TimelineWidth < WaveFormScrollViewer.ActualWidth)
			{
				WavePlayerViewModel.TimelineWidth = WaveFormScrollViewer.ActualWidth - 10;
			}
		}

		private void LoopAnalysis_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			if (WavePlayerViewModel == null) return;
			if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
			{
				WavePlayerViewModel viewmodel = (WavePlayerViewModel)DataContext;
				if (double.IsNaN(viewmodel.LoopAnalysisWidth))
				{
					viewmodel.LoopAnalysisWidth = LoopAnalysis.ActualWidth;
					_originalLoopAnalysisWidth = viewmodel.LoopAnalysisWidth;
				}

				double newWidth = viewmodel.LoopAnalysisWidth + (e.Delta > 0 ? 20 : -20);

				if (newWidth >= _originalLoopAnalysisWidth)
				{
					viewmodel.LoopAnalysisWidth = newWidth;
				}
			}
		}

		private void OnWaveFormKeyDown(object sender, KeyEventArgs e)
		{
			if (WavePlayerViewModel == null) return;
			if (e.Key.Equals(Key.Right))
			{
				WavePlayerViewModel.SoundEngine.MoveToNearbyCue(true);
			}
			else if (e.Key.Equals(Key.Left))
			{
				WavePlayerViewModel.SoundEngine.MoveToNearbyCue(false);
			}
			else if (e.Key.Equals(Key.Space))
			{
				if (WavePlayerViewModel.SoundEngine.IsPlaying)
				{
					WavePlayerViewModel.SoundEngine.Pause();
				}
				else
				{
					WavePlayerViewModel.SoundEngine.Play();
				}
			}
		}

		public static readonly DependencyProperty WavFileDependencyProperty =
			DependencyProperty.Register("WavFile", typeof(string),
				typeof(WavePlayer), new PropertyMetadata(default(string), DependencyPropertyChanged));
	}
}