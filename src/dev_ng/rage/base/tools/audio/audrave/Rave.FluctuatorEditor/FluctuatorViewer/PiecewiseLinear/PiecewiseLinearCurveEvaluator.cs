using System.Text;
using Rave.FluctuatorEditor.FluctuatorViewer.Curves;

namespace Rave.FluctuatorEditor.FluctuatorViewer.PiecewiseLinear
{
    public class PiecewiseLinearCurveEvaluator : CurveEvaluator
    {
        public PiecewiseLinearCurveEvaluator(ICurve curve, ICurveEditorModel parent, int curveThickness) : base(curve, curveThickness)
        {
            Parent = parent;
            Evaluate();
        }

        protected override void Evaluate()
        {
            var pathBuilder = new StringBuilder();
            pathBuilder.Append("M ");

            var first = true;
            foreach (var controlPoint in Curve.ControlPoints)
            {
                if (first)
                {
                    pathBuilder.Append(controlPoint.XPos);
                    pathBuilder.Append(",");
                    pathBuilder.Append(controlPoint.YPos);
                    pathBuilder.Append(" L");
                    first = false;
                }
                else
                {
                    pathBuilder.Append(" ");
                    pathBuilder.Append(controlPoint.XPos);
                    pathBuilder.Append(",");
                    pathBuilder.Append(controlPoint.YPos);
                }
            }

            CurveGeometry = pathBuilder.ToString();
        }
    }
}