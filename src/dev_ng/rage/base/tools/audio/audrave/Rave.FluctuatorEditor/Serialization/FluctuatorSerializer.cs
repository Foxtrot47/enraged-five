using Rave.CurveEditor2;

namespace Rave.FluctuatorEditor.Serialization
{
    using System.Collections.Generic;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    using rage.ToolLib;

    using Rave.ParameterTransformEditor;

    public class FluctuatorSerializer : IFluctuatorSerializer
    {
        private const string FORMAT_3_DECIMAL_PLACES = "{0:0.###}";
        private bool m_isLoading;

        #region IFluctuatorSerializer Members

        public IEnumerable<string> Variables { get; private set; }
        public int? MaxFluctuators { get; private set; }

        public void Load(IFluctuatorEditorModel model, IObjectInstance objectInstance)
        {
            if (objectInstance != null)
            {
                try
                {
                    m_isLoading = true;

                    Variables = objectInstance.GetObjectVariables(null, VariableListType.Input);
                    ParseMaxFluctuators(objectInstance);

                    // TODO: Load here
                    var fluctuatorNodes = GetFluctuatorNodes(objectInstance);
                    foreach (var fluctuatorNode in fluctuatorNodes)
                    {
                        model.AddFluctuator(DeserializeFluctuator(fluctuatorNode, model.SimulationDuration));
                    }
                }
                finally
                {
                    m_isLoading = false;
                }
            }
        }

        public void Save(IObjectInstance objectInstance, IFluctuatorEditorModel model)
        {
            if (!m_isLoading && objectInstance != null &&
                objectInstance.Node != null)
            {
                var fluctuatorNodes = GetFluctuatorNodes(objectInstance);
                foreach (var node in fluctuatorNodes)
                {
                    objectInstance.Node.RemoveChild(node);
                }

                foreach (var fluctuator in model.Fluctuators)
                {
                    objectInstance.Node.AppendChild(SerializeFluctuator(fluctuator, objectInstance.Node.OwnerDocument));
                }
                objectInstance.MarkAsDirty();
            }
        }

        #endregion

        private Fluctuator DeserializeFluctuator(XmlNode fluctuatorNode, SimulationDuration simulationDuration)
        {
            var fluctuator = new Fluctuator(Variables, simulationDuration);
            foreach (XmlNode child in fluctuatorNode.ChildNodes)
            {
                var parseToken = Enum<ParseTokens>.Parse(child.Name);
                switch (parseToken)
                {
                    case ParseTokens.Destination:
                        {
                            fluctuator.Destination =
                                Enum<ParameterDestinations>.Parse(child.InnerText.Replace("PARAM_DESTINATION_",
                                                                                          string.Empty));
                            break;
                        }
                    case ParseTokens.OutputVariable:
                        {
                            fluctuator.OutputVariable = child.InnerText;
                            break;
                        }
                    case ParseTokens.IncreaseRate:
                        {
                            fluctuator.IncreaseRate = float.Parse(child.InnerText);
                            break;
                        }
                    case ParseTokens.DecreaseRate:
                        {
                            fluctuator.DecreaseRate = float.Parse(child.InnerText);
                            break;
                        }
                    case ParseTokens.BandOneMinimum:
                        {
                            fluctuator.BandOneMinimum = float.Parse(child.InnerText);
                            break;
                        }
                    case ParseTokens.BandOneMaximum:
                        {
                            fluctuator.BandOneMaximum = float.Parse(child.InnerText);
                            break;
                        }
                    case ParseTokens.BandTwoMinimum:
                        {
                            fluctuator.BandTwoMinimum = float.Parse(child.InnerText);
                            break;
                        }
                    case ParseTokens.BandTwoMaximum:
                        {
                            fluctuator.BandTwoMaximum = float.Parse(child.InnerText);
                            break;
                        }
                    case ParseTokens.IntraBandFlipProbabilty:
                        {
                            fluctuator.IntraBandFlipProbability = float.Parse(child.InnerText);
                            break;
                        }
                    case ParseTokens.InterBandFlipProbabilty:
                        {
                            fluctuator.InterBandFlipProbability = float.Parse(child.InnerText);
                            break;
                        }
                    case ParseTokens.InitialValue:
                        {
                            fluctuator.InitialValue = float.Parse(child.InnerText);
                            break;
                        }
                }
            }
            return fluctuator;
        }

        private static XmlNode SerializeFluctuator(Fluctuator fluctuator, XmlDocument ownerDocument)
        {
            var fluctuatorNode = ownerDocument.CreateElement(ParseTokens.Fluctuator.ToString());

            var destinationNode = ownerDocument.CreateElement(ParseTokens.Destination.ToString());
            destinationNode.AppendChild(
                ownerDocument.CreateTextNode(string.Format("PARAM_DESTINATION_{0}",
                                                           fluctuator.Destination.ToString().ToUpper())));
            fluctuatorNode.AppendChild(destinationNode);

            if (!string.IsNullOrEmpty(fluctuator.OutputVariable))
            {
                var outputVariableNode = ownerDocument.CreateElement(ParseTokens.OutputVariable.ToString());
                outputVariableNode.AppendChild(ownerDocument.CreateTextNode(fluctuator.OutputVariable));
                fluctuatorNode.AppendChild(outputVariableNode);
            }

            var increaseRateNode = ownerDocument.CreateElement(ParseTokens.IncreaseRate.ToString());
            increaseRateNode.AppendChild(
                ownerDocument.CreateTextNode(string.Format(FORMAT_3_DECIMAL_PLACES, fluctuator.IncreaseRate)));
            fluctuatorNode.AppendChild(increaseRateNode);

            var decreaseRateNode = ownerDocument.CreateElement(ParseTokens.DecreaseRate.ToString());
            decreaseRateNode.AppendChild(
                ownerDocument.CreateTextNode(string.Format(FORMAT_3_DECIMAL_PLACES, fluctuator.DecreaseRate)));
            fluctuatorNode.AppendChild(decreaseRateNode);

            var bandOneMinimumNode = ownerDocument.CreateElement(ParseTokens.BandOneMinimum.ToString());
            bandOneMinimumNode.AppendChild(
                ownerDocument.CreateTextNode(string.Format(FORMAT_3_DECIMAL_PLACES, fluctuator.BandOneMinimum)));
            fluctuatorNode.AppendChild(bandOneMinimumNode);

            var bandOneMaximumNode = ownerDocument.CreateElement(ParseTokens.BandOneMaximum.ToString());
            bandOneMaximumNode.AppendChild(
                ownerDocument.CreateTextNode(string.Format(FORMAT_3_DECIMAL_PLACES, fluctuator.BandOneMaximum)));
            fluctuatorNode.AppendChild(bandOneMaximumNode);

            var bandTwoMinimumNode = ownerDocument.CreateElement(ParseTokens.BandTwoMinimum.ToString());
            bandTwoMinimumNode.AppendChild(
                ownerDocument.CreateTextNode(string.Format(FORMAT_3_DECIMAL_PLACES, fluctuator.BandTwoMinimum)));
            fluctuatorNode.AppendChild(bandTwoMinimumNode);

            var bandTwoMaximumNode = ownerDocument.CreateElement(ParseTokens.BandTwoMaximum.ToString());
            bandTwoMaximumNode.AppendChild(
                ownerDocument.CreateTextNode(string.Format(FORMAT_3_DECIMAL_PLACES, fluctuator.BandTwoMaximum)));
            fluctuatorNode.AppendChild(bandTwoMaximumNode);

            var interBandFlipProbabilityNode =
                ownerDocument.CreateElement(ParseTokens.InterBandFlipProbabilty.ToString());
            interBandFlipProbabilityNode.AppendChild(
                ownerDocument.CreateTextNode(string.Format(FORMAT_3_DECIMAL_PLACES, fluctuator.InterBandFlipProbability)));
            fluctuatorNode.AppendChild(interBandFlipProbabilityNode);

            var intraBandFlipProbabilityNode =
                ownerDocument.CreateElement(ParseTokens.IntraBandFlipProbabilty.ToString());
            intraBandFlipProbabilityNode.AppendChild(
                ownerDocument.CreateTextNode(string.Format(FORMAT_3_DECIMAL_PLACES, fluctuator.IntraBandFlipProbability)));
            fluctuatorNode.AppendChild(intraBandFlipProbabilityNode);

            var initialValueNode = ownerDocument.CreateElement(ParseTokens.InitialValue.ToString());
            initialValueNode.AppendChild(
                ownerDocument.CreateTextNode(string.Format(FORMAT_3_DECIMAL_PLACES, fluctuator.InitialValue)));
            fluctuatorNode.AppendChild(initialValueNode);

            return fluctuatorNode;
        }

        private static IEnumerable<XmlNode> GetFluctuatorNodes(IObjectInstance objectInstance)
        {
            var fluctuatorNodes = new List<XmlNode>();
            foreach (XmlNode childNode in objectInstance.Node.ChildNodes)
            {
                try
                {
                    var token = Enum<ParseTokens>.Parse(childNode.Name);
                    if (token == ParseTokens.Fluctuator)
                    {
                        fluctuatorNodes.Add(childNode);
                    }
                }
                catch
                {
                }
            }
            return fluctuatorNodes;
        }

        private void ParseMaxFluctuators(IObjectInstance objectInstance)
        {
            MaxFluctuators = null;

            var typeDef = objectInstance.TypeDefinitions.FindTypeDefinition(objectInstance.TypeName);
            if (typeDef == null)
            {
                return;
            }

            var fluctuatorFieldDef = typeDef.FindFieldDefinition("Fluctuator") as ICompositeFieldDefinition;
            if (fluctuatorFieldDef != null)
            {
                MaxFluctuators = fluctuatorFieldDef.MaxOccurs;
            }
        }

        #region Nested type: ParseTokens

        private enum ParseTokens
        {
            Fluctuator,
            Destination,
            OutputVariable,
            IncreaseRate,
            DecreaseRate,
            BandOneMinimum,
            BandOneMaximum,
            BandTwoMinimum,
            BandTwoMaximum,
            IntraBandFlipProbabilty,
            InterBandFlipProbabilty,
            InitialValue
        }

        #endregion
    }
}