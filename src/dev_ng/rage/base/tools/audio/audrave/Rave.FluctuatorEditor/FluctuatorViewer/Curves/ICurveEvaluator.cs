using System;
using System.ComponentModel;

namespace Rave.FluctuatorEditor.FluctuatorViewer.Curves
{
    public interface ICurveEvaluator : INotifyPropertyChanged,
                                       IDisposable
    {
        ICurveEditorModel Parent { get; }
        int CurveThickness { get; }
        string CurveGeometry { get; }
        ICurve Curve { get; }
    }
}