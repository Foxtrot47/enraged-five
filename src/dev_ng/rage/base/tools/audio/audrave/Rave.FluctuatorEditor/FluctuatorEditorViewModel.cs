using System;
using System.ComponentModel;

using WPFToolLib.Extensions;

namespace Rave.FluctuatorEditor
{
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public class FluctuatorEditorViewModel : IFluctuatorEditorViewModel
    {
        private readonly IFluctuatorEditorModel m_model;

        public FluctuatorEditorViewModel(IFluctuatorEditorModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            m_model.IsCheckedOutChanged += OnIsCheckedOutChanged;
            m_model.CanAddFluctuatorChanged += OnCanAddFluctuatorChanged;
            m_model.FluctuatorAdded += OnFluctuatorAdded;
            m_model.FluctuatorRemoved += OnFluctuatorRemoved;
            m_model.Reset += OnReset;

            Fluctuators = new BindingList<Fluctuator>();
        }

        #region IFluctuatorEditorViewModel Members

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsCheckedOut
        {
            get { return m_model.IsCheckedOut; }
        }

        public bool CanAddFluctuator
        {
            get { return m_model.CanAddFluctuator; }
        }

        public BindingList<Fluctuator> Fluctuators { get; private set; }

        public SimulationDuration SimulationDuration
        {
            get { return m_model.SimulationDuration; }
        }

        public void AddFluctuator()
        {
            m_model.AddFluctuator(new Fluctuator(m_model.Variables, m_model.SimulationDuration));
        }

        public void RemoveFluctuator(Fluctuator fluctuator)
        {
            m_model.RemoveFluctuator(fluctuator);
        }

        public void EditObject(IObjectInstance objectInstance)
        {
            m_model.EditObject(objectInstance);
        }

        public void Dispose()
        {
            m_model.IsCheckedOutChanged -= OnIsCheckedOutChanged;
            m_model.CanAddFluctuatorChanged -= OnCanAddFluctuatorChanged;
        }

        #endregion

        private void OnReset()
        {
            Fluctuators.Clear();
        }

        private void OnFluctuatorRemoved(Fluctuator fluctuator)
        {
            Fluctuators.Remove(fluctuator);
        }

        private void OnFluctuatorAdded(Fluctuator fluctuator)
        {
            Fluctuators.Add(fluctuator);
        }

        private void OnIsCheckedOutChanged()
        {
            PropertyChanged.Raise(this, "IsCheckedOut");
        }

        private void OnCanAddFluctuatorChanged()
        {
            PropertyChanged.Raise(this, "CanAddFluctuator");
        }
    }
}