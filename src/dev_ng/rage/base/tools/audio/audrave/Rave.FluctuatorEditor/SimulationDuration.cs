using System;
using System.ComponentModel;
using WPFToolLib.Extensions;

namespace Rave.FluctuatorEditor
{
    public class SimulationDuration : INotifyPropertyChanged
    {
        public const double MIN_SIM_TIME = 1;
        public const double MAX_SIM_TIME = 30;
        private readonly IFluctuatorEditorModel m_model;
        private double m_simulationDuration;

        public SimulationDuration(IFluctuatorEditorModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            m_simulationDuration = (MAX_SIM_TIME - MIN_SIM_TIME) / 2;
        }

        public double Duration
        {
            get { return m_simulationDuration; }
            set
            {
                var adjustedValue = Math.Max(MIN_SIM_TIME, Math.Min(MAX_SIM_TIME, value));
                if (adjustedValue != m_simulationDuration)
                {
                    m_simulationDuration = adjustedValue;
                    PropertyChanged.Raise(this, "Duration");
                    foreach (var fluctuator in m_model.Fluctuators)
                    {
                        fluctuator.Simulate();
                    }
                }
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}