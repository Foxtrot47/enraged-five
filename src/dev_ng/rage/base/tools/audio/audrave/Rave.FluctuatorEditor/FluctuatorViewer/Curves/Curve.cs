using System.ComponentModel;

namespace Rave.FluctuatorEditor.FluctuatorViewer.Curves
{
    public class Curve : ICurve
    {
        public Curve(string color)
        {
            ControlPoints = new BindingList<ControlPoint>();
            Color = color;
        }

        #region ICurve Members

        public ICurveEvaluator Parent { get; set; }

        public BindingList<ControlPoint> ControlPoints { get; private set; }

        public string Color { get; private set; }

        public void Add(ControlPoint controlPoint)
        {
            if (controlPoint.Parent ==
                this)
            {
                ControlPoints.Add(controlPoint);
            }
        }

        public void Insert(int index, ControlPoint controlPoint)
        {
            if (controlPoint.Parent == this && index >= 0 &&
                index < ControlPoints.Count)
            {
                ControlPoints.Insert(index, controlPoint);
            }
        }

        public void Remove(ControlPoint controlPoint)
        {
            if (controlPoint.Parent ==
                this)
            {
                ControlPoints.Remove(controlPoint);
            }
        }

        public void Clear()
        {
            ControlPoints.Clear();
        }

        #endregion
    }
}