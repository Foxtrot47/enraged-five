using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Rave.FluctuatorEditor.FluctuatorViewer.Axes;
using Rave.FluctuatorEditor.FluctuatorViewer.Curves;
using WPFToolLib.Extensions;

namespace Rave.FluctuatorEditor.FluctuatorViewer
{
    public class CurveEditorViewModel : ICurveEditorViewModel
    {
        private bool m_isCheckedOut;
        private ICurveEditorModel m_model;

        public CurveEditorViewModel(ICurveEditorModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            XAxis = new AxisViewModel(model.XAxis);
            YAxis = new AxisViewModel(model.YAxis);
        }

        #region ICurveEditorViewModel Members

        public BindingList<ICurveEvaluator> CurveEvaluators
        {
            get { return m_model.CurveEvaluators; }
        }

        public IAxisViewModel XAxis { get; private set; }
        public IAxisViewModel YAxis { get; private set; }

        public bool IsCheckedOut
        {
            get { return m_isCheckedOut; }
            set
            {
                if (m_isCheckedOut != value)
                {
                    m_isCheckedOut = value;
                    PropertyChanged.Raise(this, "IsCheckedOut");
                }
            }
        }

        public ICurve AddCurve(CurveTypes type, IEnumerable<Point> points, string color, int curveThickness)
        {
            return m_model.AddCurve(type, points, color, curveThickness);
        }

        public void UpdateCurves()
        {
            if (CurveEvaluators.Count > 0)
            {
                CurveEvaluators.ResetBindings();
                foreach (var curveEvaluator in CurveEvaluators)
                {
                    curveEvaluator.Curve.ControlPoints.ResetBindings();
                }
            }
        }

        public void RemoveCurve(ICurve curve)
        {
            m_model.RemoveCurve(curve);
        }

        public void RemoveCurves()
        {
            m_model.RemoveCurves();
        }

        public void Dispose()
        {
            XAxis.Dispose();
            XAxis = null;
            YAxis.Dispose();
            YAxis = null;
            m_model = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}