using System.Collections.Generic;

namespace Rave.FluctuatorEditor.Serialization
{
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public interface IFluctuatorSerializer
    {
        IEnumerable<string> Variables { get; }
        int? MaxFluctuators { get; }

        void Load(IFluctuatorEditorModel model, IObjectInstance objectInstance);

        void Save(IObjectInstance objectInstance, IFluctuatorEditorModel model);
    }
}