﻿namespace Rave.FluctuatorEditor
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Xml;

    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    ///   Interaction logic for FluctuatorEditorView.xaml
    /// </summary>
    public partial class FluctuatorEditorView : IRAVEObjectEditorPlugin
    {
        private IFluctuatorEditorViewModel m_viewModel;
        private IObjectInstance m_objectToEdit;

        public FluctuatorEditorView()
        {
            InitializeComponent();
        }

        #region IRAVEObjectEditorPlugin Members

        public string GetName()
        {
            return "Fluctuator Editor";
        }

        public bool Init(XmlNode settings)
        {
            return true;
        }

        public string ObjectType
        {
            get { return "FluctuatorSound"; }
        }

#pragma warning disable 0067
        public event Action<IObjectInstance> OnObjectEditClick;
        public event Action<IObjectInstance> OnObjectRefClick;
        public event Action<string, string> OnWaveRefClick;
        public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067

        public void EditObject(IObjectInstance objectInstance, Mode mode)
        {
            UserControl_Unloaded(null, null);
            m_objectToEdit = objectInstance;
            UserControl_Loaded(null, null);
        }

        public void Dispose()
        {
            // Do nothing
        }

        #endregion

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsCheckedOut":
                    {
                        if (m_viewModel != null)
                        {
                            if (m_viewModel.IsCheckedOut)
                            {
                                editingView.Visibility = System.Windows.Visibility.Visible;
                                editingView.DataContext = m_viewModel;
                                editingView.ViewModel = m_viewModel;

                                readOnlyView.Visibility = System.Windows.Visibility.Collapsed;
                                readOnlyView.DataContext = null;
                            }
                            else
                            {
                                readOnlyView.Visibility = System.Windows.Visibility.Visible;
                                readOnlyView.DataContext = m_viewModel;

                                editingView.Visibility = System.Windows.Visibility.Collapsed;
                                editingView.DataContext = null;
                                editingView.ViewModel = null;
                            } 
                        }
                        break;
                    }
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (m_objectToEdit != null)
            {
                m_viewModel = new FluctuatorEditorViewModel(new FluctuatorEditorModel());
                m_viewModel.PropertyChanged += OnViewModelPropertyChanged;
                OnViewModelPropertyChanged(null, new PropertyChangedEventArgs("IsCheckedOut"));
                m_viewModel.EditObject(m_objectToEdit);
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (m_viewModel != null)
            {
                editingView.DataContext = null;
                editingView.ViewModel = null;
                readOnlyView.DataContext = null;

                m_viewModel.PropertyChanged -= OnViewModelPropertyChanged;
                m_viewModel.Dispose();
                m_viewModel = null;
            }

            if (e != null)
            {
                e.Handled = true;
            }
        }
    }
}