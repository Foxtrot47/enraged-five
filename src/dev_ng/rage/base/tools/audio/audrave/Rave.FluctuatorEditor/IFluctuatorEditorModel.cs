using System;
using System.Collections.Generic;

namespace Rave.FluctuatorEditor
{
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public interface IFluctuatorEditorModel
    {
        bool IsCheckedOut { get; }
        bool CanAddFluctuator { get; }
        IEnumerable<Fluctuator> Fluctuators { get; }
        IEnumerable<string> Variables { get; }
        SimulationDuration SimulationDuration { get; }

        event Action Reset;
        event Action IsCheckedOutChanged;
        event Action CanAddFluctuatorChanged;
        event Action<Fluctuator> FluctuatorAdded;
        event Action<Fluctuator> FluctuatorRemoved;

        void AddFluctuator(Fluctuator fluctuator);

        void RemoveFluctuator(Fluctuator fluctuator);

        void EditObject(IObjectInstance objectInstance);
    }
}