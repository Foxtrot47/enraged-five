using System;
using System.ComponentModel;

namespace Rave.FluctuatorEditor
{
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public interface IFluctuatorEditorViewModel : INotifyPropertyChanged,
                                                  IDisposable
    {
        bool IsCheckedOut { get; }
        bool CanAddFluctuator { get; }
        BindingList<Fluctuator> Fluctuators { get; }
        SimulationDuration SimulationDuration { get; }

        void AddFluctuator();

        void RemoveFluctuator(Fluctuator fluctuator);

        void EditObject(IObjectInstance objectInstance);
    }
}