using System;
using System.Collections.Generic;
using System.Windows;
using Rave.FluctuatorEditor.FluctuatorViewer.Curves;

namespace Rave.FluctuatorEditor.FluctuatorViewer
{
    public interface ICurveEditorView : IDisposable
    {
        double XAxisLength { get; set; }
        double YAxisLength { get; set; }
        bool IsCheckedOut { get; set; }
        ICurveEditorViewModel ViewModel { get; }

        ICurve AddCurve(CurveTypes type, IEnumerable<Point> points, string color = null, int curveThickness = 1);

        void RemoveCurves();
    }
}