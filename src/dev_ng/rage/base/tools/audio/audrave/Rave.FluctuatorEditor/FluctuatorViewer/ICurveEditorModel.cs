using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Rave.FluctuatorEditor.FluctuatorViewer.Axes;
using Rave.FluctuatorEditor.FluctuatorViewer.Curves;

namespace Rave.FluctuatorEditor.FluctuatorViewer
{
    public interface ICurveEditorModel
    {
        BindingList<ICurveEvaluator> CurveEvaluators { get; }
        IAxisModel XAxis { get; }
        IAxisModel YAxis { get; }

        ICurve AddCurve(CurveTypes type, IEnumerable<Point> points, string color, int curveThickness);

        void RemoveCurve(ICurve curve);

        void RemoveCurves();
    }
}