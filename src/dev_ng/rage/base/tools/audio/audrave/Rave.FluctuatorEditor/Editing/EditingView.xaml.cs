﻿using System.Windows;

namespace Rave.FluctuatorEditor.Editing
{
    /// <summary>
    ///   Interaction logic for EditingView.xaml
    /// </summary>
    public partial class EditingView
    {
        public EditingView()
        {
            InitializeComponent();
        }

        public IFluctuatorEditorViewModel ViewModel { get; set; }

        private void OnAddClicked(object sender, RoutedEventArgs e)
        {
            ViewModel.AddFluctuator();
            e.Handled = true;
        }

        private void OnRemoveClicked(object sender, RoutedEventArgs e)
        {
            var fluctuator = fluctuators.SelectedItem as Fluctuator;
            if (fluctuator != null)
            {
                ViewModel.RemoveFluctuator(fluctuator);
            }
            e.Handled = true;
        }

        private void Simulate_Click(object sender, RoutedEventArgs e)
        {
            foreach (var fluctuator in ViewModel.Fluctuators)
            {
                fluctuator.Simulate();
            }
            e.Handled = true;
        }
    }
}