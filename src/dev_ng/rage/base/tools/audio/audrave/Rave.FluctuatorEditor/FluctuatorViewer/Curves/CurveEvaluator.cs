using System;
using System.ComponentModel;
using WPFToolLib.Extensions;

namespace Rave.FluctuatorEditor.FluctuatorViewer.Curves
{
    public abstract class CurveEvaluator : ICurveEvaluator
    {
        private string m_curveGeometry;
        private int m_curveThickness;

        protected CurveEvaluator(ICurve curve, int curveThickness)
        {
            if (curve == null)
            {
                throw new ArgumentNullException("curve");
            }
            Curve = curve;
            Curve.Parent = this;
            Curve.ControlPoints.ListChanged += OnListChanged;
            CurveThickness = curveThickness;
        }

        #region ICurveEvaluator Members

        public event PropertyChangedEventHandler PropertyChanged;

        public ICurveEditorModel Parent { get; protected set; }

        public int CurveThickness
        {
            get { return m_curveThickness; }
            private set
            {
                if (m_curveThickness != value)
                {
                    m_curveThickness = value;
                    PropertyChanged.Raise(this, "CurveThickness");
                }
            }
        }

        public string CurveGeometry
        {
            get { return m_curveGeometry; }
            protected set
            {
                if (m_curveGeometry != value)
                {
                    m_curveGeometry = value;
                    PropertyChanged.Raise(this, "CurveGeometry");
                }
            }
        }

        public ICurve Curve { get; private set; }

        public void Dispose()
        {
            Curve.ControlPoints.ListChanged -= OnListChanged;
        }

        #endregion

        protected abstract void Evaluate();

        private void OnListChanged(object sender, ListChangedEventArgs e)
        {
            Evaluate();
        }
    }
}