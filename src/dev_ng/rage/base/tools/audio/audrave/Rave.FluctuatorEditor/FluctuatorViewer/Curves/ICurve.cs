using System.ComponentModel;

namespace Rave.FluctuatorEditor.FluctuatorViewer.Curves
{
    public interface ICurve
    {
        ICurveEvaluator Parent { get; set; }
        BindingList<ControlPoint> ControlPoints { get; }
        string Color { get; }

        void Add(ControlPoint controlPoint);

        void Insert(int index, ControlPoint controlPoint);

        void Remove(ControlPoint controlPoint);

        void Clear();
    }
}