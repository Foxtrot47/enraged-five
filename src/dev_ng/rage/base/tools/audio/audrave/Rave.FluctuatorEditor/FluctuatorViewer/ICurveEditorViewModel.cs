using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Rave.FluctuatorEditor.FluctuatorViewer.Axes;
using Rave.FluctuatorEditor.FluctuatorViewer.Curves;

namespace Rave.FluctuatorEditor.FluctuatorViewer
{
    public interface ICurveEditorViewModel : INotifyPropertyChanged,
                                             IDisposable
    {
        BindingList<ICurveEvaluator> CurveEvaluators { get; }
        IAxisViewModel XAxis { get; }
        IAxisViewModel YAxis { get; }
        bool IsCheckedOut { get; set; }

        ICurve AddCurve(CurveTypes type, IEnumerable<Point> points, string color, int curveThickness);

        void UpdateCurves();

        void RemoveCurve(ICurve curve);

        void RemoveCurves();
    }
}