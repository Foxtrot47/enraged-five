using System;

namespace Rave.FluctuatorEditor
{
    public class Smoother
    {
        private readonly float m_decreaseRate;
        private readonly float m_increaseRate;
        private bool m_firstRequest;
        private float m_lastValue;

        public Smoother(float increaseRate, float decreaseRate)
        {
            m_increaseRate = increaseRate;
            m_decreaseRate = decreaseRate;
            m_firstRequest = true;
        }

        public float GetSmoothedValue(float inputValue)
        {
            if (m_firstRequest)
            {
                m_lastValue = inputValue;
                m_firstRequest = false;
            }
            else
            {
                var valueIncreasing = Math.Min(inputValue, m_lastValue + m_increaseRate);
                var valueDecreasing = Math.Max(inputValue, m_lastValue - m_decreaseRate);
                m_lastValue = Selectf(inputValue - m_lastValue, valueIncreasing, valueDecreasing);
            }
            return m_lastValue;
        }

        private static float Selectf(float condition, float valGe, float valLt)
        {
            return condition >= 0 ? valGe : valLt;
        }
    }
}