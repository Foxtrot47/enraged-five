﻿using System;
using System.Collections.Generic;
using System.Windows;
using Rave.FluctuatorEditor.FluctuatorViewer.Curves;

namespace Rave.FluctuatorEditor.FluctuatorViewer
{
    /// <summary>
    ///   Interaction logic for CurveEditorView.xaml
    /// </summary>
    public partial class CurveEditorView : ICurveEditorView
    {
        public CurveEditorView(ICurveEditorViewModel viewModel)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException("viewModel");
            }
            ViewModel = viewModel;
            DataContext = ViewModel;

            InitializeComponent();
        }

        #region ICurveEditorView Members

        public double XAxisLength
        {
            get { return ViewModel.XAxis.ActualLength; }
            set
            {
                ViewModel.XAxis.ActualLength = value;
                ViewModel.UpdateCurves();
            }
        }

        public double YAxisLength
        {
            get { return ViewModel.YAxis.ActualLength; }
            set
            {
                ViewModel.YAxis.ActualLength = value;
                ViewModel.UpdateCurves();
            }
        }

        public bool IsCheckedOut
        {
            get { return ViewModel.IsCheckedOut; }
            set { ViewModel.IsCheckedOut = value; }
        }

        public ICurveEditorViewModel ViewModel { get; private set; }

        public ICurve AddCurve(CurveTypes type, IEnumerable<Point> points, string color = null, int curveThickness = 1)
        {
            return ViewModel.AddCurve(type, points, color, curveThickness);
        }

        public void RemoveCurves()
        {
            ViewModel.RemoveCurves();
        }

        public void Dispose()
        {
            ViewModel.Dispose();
            ViewModel = null;
            DataContext = null;
        }

        #endregion
    }
}