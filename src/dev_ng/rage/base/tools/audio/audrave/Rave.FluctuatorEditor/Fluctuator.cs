using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using rage.ToolLib;
using Rave.CurveEditor2;
using Rave.FluctuatorEditor.FluctuatorViewer.Axes;
using Rave.FluctuatorEditor.FluctuatorViewer.Curves;
using Rave.ParameterTransformEditor;
using WPFToolLib.Extensions;
using CurveEditorModel = Rave.FluctuatorEditor.FluctuatorViewer.CurveEditorModel;
using CurveEditorView = Rave.FluctuatorEditor.FluctuatorViewer.CurveEditorView;
using CurveEditorViewModel = Rave.FluctuatorEditor.FluctuatorViewer.CurveEditorViewModel;

namespace Rave.FluctuatorEditor
{
    public class Fluctuator : INotifyPropertyChanged
    {
        private static readonly Random ms_random;

        private readonly SimulationDuration m_simulationDuration;
        private float m_bandOneMaximum;
        private float m_bandOneMinimum;
        private float m_bandTwoMaximum;
        private float m_bandTwoMinimum;
        private float m_decreaseRate;
        private ParameterDestinations m_destination;
        private float m_increaseRate;
        private float m_initialValue;
        private float m_interBandFlipProbability;
        private float m_intraBandFlipProbability;
        private string m_outputVariable;

        static Fluctuator()
        {
            ms_random = new Random((int) DateTime.Now.TimeOfDay.TotalSeconds);
        }

        public Fluctuator(IEnumerable<string> outputVariables, SimulationDuration simulationDuration)
        {
            m_simulationDuration = simulationDuration;
            OutputVariables = outputVariables;
            m_destination = ParameterDestinations.Volume;
            m_outputVariable = string.Empty;
            SimulationViewer =
                new CurveEditorView(
                    new CurveEditorViewModel(new CurveEditorModel(0,
                                                                  new AxisModel("Time",
                                                                                m_simulationDuration.Duration,
                                                                                300),
                                                                  new AxisModel("Value", 1, 300))));
        }

        public float InitialValue
        {
            get { return m_initialValue; }
            set
            {
                if (m_initialValue != value)
                {
                    m_initialValue = value;
                    PropertyChanged.Raise(this, "InitialValue");
                    Simulate();
                    Dirty.Raise();
                }
            }
        }

        public float IncreaseRate
        {
            get { return m_increaseRate; }
            set
            {
                if (m_increaseRate != value)
                {
                    m_increaseRate = value;
                    PropertyChanged.Raise(this, "IncreaseRate");
                    Simulate();
                    Dirty.Raise();
                }
            }
        }

        public float DecreaseRate
        {
            get { return m_decreaseRate; }
            set
            {
                if (m_decreaseRate != value)
                {
                    m_decreaseRate = value;
                    PropertyChanged.Raise(this, "DecreaseRate");
                    Simulate();
                    Dirty.Raise();
                }
            }
        }

        public float BandOneMinimum
        {
            get { return m_bandOneMinimum; }
            set
            {
                if (m_bandOneMinimum != value)
                {
                    m_bandOneMinimum = value;
                    PropertyChanged.Raise(this, "BandOneMinimum");
                    Simulate();
                    Dirty.Raise();
                }
            }
        }

        public float BandOneMaximum
        {
            get { return m_bandOneMaximum; }
            set
            {
                if (m_bandOneMaximum != value)
                {
                    m_bandOneMaximum = value;
                    PropertyChanged.Raise(this, "BandOneMaximum");
                    Simulate();
                    Dirty.Raise();
                }
            }
        }

        public float BandTwoMinimum
        {
            get { return m_bandTwoMinimum; }
            set
            {
                if (m_bandTwoMinimum != value)
                {
                    m_bandTwoMinimum = value;
                    PropertyChanged.Raise(this, "BandTwoMinimum");
                    Simulate();
                    Dirty.Raise();
                }
            }
        }

        public float BandTwoMaximum
        {
            get { return m_bandTwoMaximum; }
            set
            {
                if (m_bandTwoMaximum != value)
                {
                    m_bandTwoMaximum = value;
                    PropertyChanged.Raise(this, "BandTwoMaximum");
                    Simulate();
                    Dirty.Raise();
                }
            }
        }

        public float IntraBandFlipProbability
        {
            get { return m_intraBandFlipProbability; }
            set
            {
                if (m_intraBandFlipProbability != value)
                {
                    m_intraBandFlipProbability = value;
                    PropertyChanged.Raise(this, "IntraBandFlipProbability");
                    Simulate();
                    Dirty.Raise();
                }
            }
        }

        public float InterBandFlipProbability
        {
            get { return m_interBandFlipProbability; }
            set
            {
                if (m_interBandFlipProbability != value)
                {
                    m_interBandFlipProbability = value;
                    PropertyChanged.Raise(this, "InterBandFlipProbability");
                    Simulate();
                    Dirty.Raise();
                }
            }
        }

        public ParameterDestinations Destination
        {
            get { return m_destination; }
            set
            {
                if (m_destination != value)
                {
                    m_destination = value;
                    PropertyChanged.Raise(this, "Destination");
                    Dirty.Raise();
                }
            }
        }

        public string OutputVariable
        {
            get { return m_outputVariable; }
            set
            {
                if (m_outputVariable != value)
                {
                    m_outputVariable = value;
                    PropertyChanged.Raise(this, "OutputVariable");
                    Dirty.Raise();
                }
                IsOutputVariableValid = !string.IsNullOrEmpty(m_outputVariable) &&
                                        OutputVariables.Any(x => string.Compare(x, m_outputVariable) == 0);
                PropertyChanged.Raise(this, "IsOutputVariableValid");
            }
        }

        public bool IsOutputVariableValid { get; private set; }

        public IEnumerable<string> OutputVariables { get; private set; }

        public CurveEditorView SimulationViewer { get; private set; }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public event Action Dirty;

        public void Simulate()
        {
            SimulationViewer.RemoveCurves();

            var duration = m_simulationDuration.Duration;
            SimulationViewer.XAxisLength = duration;
            SimulationViewer.YAxisLength = GetLargestValuePlus10Percent();

            AddBandLines(m_bandOneMinimum, m_bandOneMaximum);
            AddBandLines(m_bandTwoMinimum, m_bandTwoMaximum);

            var interBandState = ResolveProbability(0.5f);
            var intraBandState = ResolveProbability(0.5f);

            var smoother = new Smoother(m_increaseRate, m_decreaseRate);
            var points = new List<Point> {new Point(0, smoother.GetSmoothedValue(m_initialValue))};

            const double TIME_STEP = 0.015; // 15 msec
            for (var time = TIME_STEP; time <= duration; time += TIME_STEP)
            {
                if (ResolveProbability(m_intraBandFlipProbability))
                {
                    intraBandState = !intraBandState;
                }

                if (ResolveProbability(m_interBandFlipProbability))
                {
                    interBandState = !interBandState;
                }

                float desiredValue;
                if (interBandState)
                {
                    desiredValue = intraBandState ? m_bandTwoMaximum : m_bandTwoMinimum;
                }
                else
                {
                    desiredValue = intraBandState ? m_bandOneMaximum : m_bandOneMinimum;
                }
                points.Add(new Point(time, smoother.GetSmoothedValue(desiredValue)));
            }

            SimulationViewer.AddCurve(CurveTypes.PiecewiseLinear, points, curveThickness: 2);
        }

        private void AddBandLines(float min, float max)
        {
            var minPoints = new List<Point>
                                {
                                    new Point(-m_simulationDuration.Duration, min),
                                    new Point(m_simulationDuration.Duration, min)
                                };

            var maxPoints = new List<Point>
                                {
                                    new Point(-m_simulationDuration.Duration, max),
                                    new Point(m_simulationDuration.Duration, max)
                                };

            var minCurve = SimulationViewer.AddCurve(CurveTypes.PiecewiseLinear, minPoints);
            SimulationViewer.AddCurve(CurveTypes.PiecewiseLinear, maxPoints, minCurve.Color);
        }

        private double GetLargestValuePlus10Percent()
        {
            var largestBandOneValue = Math.Max(Math.Abs(m_bandOneMinimum), Math.Abs(m_bandOneMaximum));
            var largestBandTwoValue = Math.Max(Math.Abs(m_bandTwoMinimum), Math.Abs(m_bandTwoMaximum));
            var largestValue = Math.Max(Math.Max(largestBandOneValue, largestBandTwoValue), m_initialValue);
            return largestValue + (largestValue / 10);
        }

        private static bool ResolveProbability(float probability)
        {
            if (probability >= 1)
            {
                return true;
            }
            return ms_random.NextDouble() < probability;
        }
    }
}