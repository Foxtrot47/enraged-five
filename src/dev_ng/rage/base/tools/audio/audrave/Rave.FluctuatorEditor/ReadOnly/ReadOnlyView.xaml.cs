﻿using System.Windows;

namespace Rave.FluctuatorEditor.ReadOnly
{
    /// <summary>
    ///   Interaction logic for ReadOnlyView.xaml
    /// </summary>
    public partial class ReadOnlyView
    {
        public ReadOnlyView()
        {
            InitializeComponent();
        }

        private void Simulate_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = DataContext as IFluctuatorEditorViewModel;
            if (viewModel != null)
            {
                foreach (var fluctuator in viewModel.Fluctuators)
                {
                    fluctuator.Simulate();
                }
            }
        }
    }
}