using System;

namespace Rave.FluctuatorEditor.FluctuatorViewer.Axes
{
    public interface IAxisModel
    {
        double Length { get; set; }
        double MarkFrequency { get; }
        double Scale { get; }
        double Size { get; set; }
        string Label { get; set; }

        event Action LengthChanged;
        event Action LabelChanged;
    }
}