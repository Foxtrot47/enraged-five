namespace Rave.FluctuatorEditor
{
    using System;
    using System.Collections.Generic;

    using rage.ToolLib;

    using Rave.FluctuatorEditor.Serialization;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public class FluctuatorEditorModel : IFluctuatorEditorModel
    {
        private readonly List<Fluctuator> m_fluctuators;
        private bool m_isCheckedOut;
        private IObjectInstance m_objectInstance;
        private IFluctuatorSerializer m_serializer;

        public FluctuatorEditorModel()
        {
            m_fluctuators = new List<Fluctuator>();
            SimulationDuration = new SimulationDuration(this);
        }

        #region IFluctuatorEditorModel Members

        public IEnumerable<string> Variables
        {
            get { return m_serializer.Variables; }
        }

        public SimulationDuration SimulationDuration { get; private set; }

        public event Action Reset;

        public bool IsCheckedOut
        {
            get { return m_isCheckedOut; }
            private set
            {
                if (m_isCheckedOut != value)
                {
                    m_isCheckedOut = value;
                    IsCheckedOutChanged.Raise();
                }
            }
        }

        public event Action IsCheckedOutChanged;

        public IEnumerable<Fluctuator> Fluctuators
        {
            get { return m_fluctuators; }
        }

        public bool CanAddFluctuator
        {
            get
            {
                if (m_serializer.MaxFluctuators == null)
                {
                    return true;
                }
                return m_fluctuators.Count < m_serializer.MaxFluctuators;
            }
        }

        public event Action CanAddFluctuatorChanged;

        public void AddFluctuator(Fluctuator fluctuator)
        {
            if (CanAddFluctuator)
            {
                fluctuator.Dirty += OnDirty;
                m_fluctuators.Add(fluctuator);
                FluctuatorAdded.Raise(fluctuator);
                CanAddFluctuatorChanged.Raise();
                Save();
            }
        }

        public event Action<Fluctuator> FluctuatorAdded;

        public void RemoveFluctuator(Fluctuator fluctuator)
        {
            if (m_fluctuators.Remove(fluctuator))
            {
                fluctuator.Dirty -= OnDirty;
                FluctuatorRemoved.Raise(fluctuator);
                CanAddFluctuatorChanged.Raise();
                Save();
            }
        }

        public event Action<Fluctuator> FluctuatorRemoved;

        public void EditObject(IObjectInstance objectInstance)
        {
            ResetModel();

            if (m_objectInstance != null &&
                m_objectInstance.Bank != null)
            {
                m_objectInstance.Bank.BankStatusChanged -= OnBankStatusChanged;
            }

            m_objectInstance = objectInstance;
            if (m_objectInstance == null)
            {
                return;
            }

            if (m_objectInstance.Bank != null)
            {
                m_objectInstance.Bank.BankStatusChanged += OnBankStatusChanged;
            }

            IsCheckedOut = !m_objectInstance.IsReadOnly;

            m_serializer.Load(this, objectInstance);
        }

        #endregion

        private void OnDirty()
        {
            Save();
        }

        private void OnBankStatusChanged()
        {
            if (m_objectInstance != null &&
                m_objectInstance.ObjectLookup.ContainsKey(m_objectInstance.Name))
            {
                // Edit the newly loaded object instance
                EditObject(m_objectInstance.ObjectLookup[m_objectInstance.Name]);
            }
            else
            {
                ResetModel();
            }
        }

        private void ResetModel()
        {
            foreach (var fluctuator in m_fluctuators)
            {
                fluctuator.Dirty -= OnDirty;
            }
            IsCheckedOut = false;
            m_serializer = new FluctuatorSerializer();
            Reset.Raise();
        }

        private void Save()
        {
            if (IsCheckedOut)
            {
                m_serializer.Save(m_objectInstance, this);
            }
        }
    }
}