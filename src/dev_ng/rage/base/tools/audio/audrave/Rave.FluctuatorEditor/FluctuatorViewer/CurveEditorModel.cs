using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Rave.FluctuatorEditor.FluctuatorViewer.Axes;
using Rave.FluctuatorEditor.FluctuatorViewer.Curves;
using Rave.FluctuatorEditor.FluctuatorViewer.PiecewiseLinear;

namespace Rave.FluctuatorEditor.FluctuatorViewer
{
    public class CurveEditorModel : ICurveEditorModel
    {
        private readonly CurveColors m_curveColors;
        private readonly int m_maxCurves;

        public CurveEditorModel(int maxCurves, IAxisModel xAxisModel, IAxisModel yAxisModel)
        {
            if (xAxisModel == null)
            {
                throw new ArgumentNullException("xAxisModel");
            }
            if (yAxisModel == null)
            {
                throw new ArgumentNullException("yAxisModel");
            }
            XAxis = xAxisModel;
            YAxis = yAxisModel;

            m_curveColors = new CurveColors();
            m_maxCurves = Math.Min(maxCurves, m_curveColors.MaxCurves);
            if (m_maxCurves <= 0)
            {
                m_maxCurves = m_curveColors.MaxCurves;
            }

            CurveEvaluators = new BindingList<ICurveEvaluator>();
            Curves = new List<ICurve>();
        }

        private List<ICurve> Curves { get; set; }

        #region ICurveEditorModel Members

        public IAxisModel XAxis { get; private set; }
        public IAxisModel YAxis { get; private set; }
        public BindingList<ICurveEvaluator> CurveEvaluators { get; private set; }

        public ICurve AddCurve(CurveTypes type, IEnumerable<Point> points, string color, int curveThickness)
        {
            var curve = new Curve(color == null ? m_curveColors.AcquireColor() : m_curveColors.AcquireColor(color));
            foreach (var point in points)
            {
                curve.Add(new ControlPoint(curve) {X = point.X, Y = point.Y});
            }

            switch (type)
            {
                case CurveTypes.PiecewiseLinear:
                    {
                        var evaluator = new PiecewiseLinearCurveEvaluator(curve, this, curveThickness);
                        CurveEvaluators.Add(evaluator);
                        Curves.Add(curve);
                        break;
                    }
            }
            return curve;
        }

        public void RemoveCurve(ICurve curve)
        {
            var index = Curves.IndexOf(curve);
            if (index >= 0)
            {
                var evaluator = CurveEvaluators[index];
                Curves.RemoveAt(index);
                CurveEvaluators.RemoveAt(index);
                m_curveColors.ReleaseColor(curve.Color);
                evaluator.Dispose();
            }
        }

        public void RemoveCurves()
        {
            var evaluators = CurveEvaluators.ToList();
            foreach (var evaluator in evaluators)
            {
                var curve = evaluator.Curve;
                var index = Curves.IndexOf(curve);
                Curves.RemoveAt(index);
                CurveEvaluators.RemoveAt(index);
                m_curveColors.ReleaseColor(curve.Color);
                evaluator.Dispose();
            }
        }

        #endregion
    }
}