using System;
using System.Collections.Generic;
using System.Linq;

namespace Rave.FluctuatorEditor.FluctuatorViewer.Curves
{
    public class CurveColors
    {
        private readonly Dictionary<string, int> m_colors;

        public CurveColors()
        {
            m_colors = new Dictionary<string, int>(3)
                           {
                               {"LimeGreen", 0},
                               {"Gold", 0},
                               {"OrangeRed", 0},
                           };
        }

        public int MaxCurves
        {
            get { return m_colors.Count; }
        }

        public string AcquireColor()
        {
            try
            {
                var freeColor = (from color in m_colors where color.Value == 0 select color.Key).First();
                m_colors[freeColor] = 1;
                return freeColor;
            }
            catch (Exception)
            {
                throw new ApplicationException(
                    "Ran out of unique colours - please increase the number of unique colours");
            }
        }

        public string AcquireColor(string color)
        {
            m_colors[color] = m_colors[color] + 1;
            return color;
        }

        public void ReleaseColor(string color)
        {
            m_colors[color] = m_colors[color] - 1;
        }
    }
}