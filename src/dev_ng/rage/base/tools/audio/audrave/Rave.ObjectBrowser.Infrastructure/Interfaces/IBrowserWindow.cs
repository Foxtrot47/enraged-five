﻿// -----------------------------------------------------------------------
// <copyright file="IBrowserWindow.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.ObjectBrowser.Infrastructure.Interfaces
{
    using Rave.TemplateBrowser.Infrastructure.Interfaces;

    /// <summary>
    /// Browser window interface.
    /// </summary>
    public interface IBrowserWindow
    {
        IObjectBrowser ObjectBrowser { get; }

        ITemplateBrowser TemplateBrowser { get; }
    }
}
