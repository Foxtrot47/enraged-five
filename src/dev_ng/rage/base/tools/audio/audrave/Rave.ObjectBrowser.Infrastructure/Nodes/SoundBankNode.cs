// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SoundBankNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for SoundBankNode.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.ObjectBrowser.Infrastructure.Nodes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;

    using rage.ToolLib;

    /// <summary>
    /// Summary description for SoundBankNode.
    /// </summary>
    public class SoundBankNode : BaseNode
    {
        #region Constants

        /// <summary>
        /// The bas e_ imag e_ index.
        /// </summary>
        private const int BASE_IMAGE_INDEX = 1;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SoundBankNode"/> class.
        /// </summary>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="iconMap">
        /// The icon map.
        /// </param>
        public SoundBankNode(IObjectBank bank, Hashtable iconMap)
            : base(bank.Name, bank.Episode, bank.Type, iconMap)
        {
            this.ObjectBank = bank;
            this.ObjectBank.BankStatusChanged += this.OnBankStatusChanged;
            this.RecreateSoundNodes();
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The bank status changed.
        /// </summary>
        public event Action BankStatusChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the object bank.
        /// </summary>
        public IObjectBank ObjectBank { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get base icon index.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public override int GetBaseIconIndex()
        {
            return this.ObjectBank != null && this.ObjectBank.IsCheckedOut ? BASE_IMAGE_INDEX + 1 : BASE_IMAGE_INDEX;
        }

        /// <summary>
        /// The get type description.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string GetTypeDescription()
        {
            return "Sound Bank";
        }

        /// <summary>
        /// The recreate sound nodes.
        /// </summary>
        public void RecreateSoundNodes()
        {
            var expandedVirtualFolders = new HashSet<string>();
            foreach (var node in this.Nodes)
            {
                var virtualFolderNode = node as VirtualFolderNode;
                if (virtualFolderNode != null && virtualFolderNode.IsExpanded
                    && !expandedVirtualFolders.Contains(virtualFolderNode.ObjectName))
                {
                    expandedVirtualFolders.Add(virtualFolderNode.ObjectName);
                }
            }

            this.Nodes.Clear();
            this.CreateSoundNodes(expandedVirtualFolders);
            this.UpdateIcon();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create sound nodes.
        /// </summary>
        /// <param name="expandedVirtualFolders">
        /// The expanded virtual folders.
        /// </param>
        private void CreateSoundNodes(HashSet<string> expandedVirtualFolders)
        {
            foreach (var itemInstance in this.ObjectBank.ObjectInstances)
            {
                if (itemInstance.VirtualFolderName == null)
                {
                    this.Nodes.Add(new SoundNode(itemInstance, this.GetIconIndexForTypeName(itemInstance.TypeName), this.IconMap));
                }
                else
                {
                    var parentNode =
                        this.Nodes.Cast<BaseNode>()
                            .FirstOrDefault(
                                bn =>
                                bn.GetType() == typeof(VirtualFolderNode)
                                && bn.ObjectName == itemInstance.VirtualFolderName);
                    if (parentNode == null)
                    {
                        parentNode = new VirtualFolderNode(
                            itemInstance.VirtualFolderName, this.ObjectBank.Episode, this.ObjectBank.Type, this.IconMap);
                        this.Nodes.Add(parentNode);
                    }

                    parentNode.Nodes.Add(
                        new SoundNode(itemInstance, this.GetIconIndexForTypeName(itemInstance.TypeName), this.IconMap));
                }
            }

            foreach (var node in this.Nodes)
            {
                var virtualFolderNode = node as VirtualFolderNode;
                if (virtualFolderNode != null && expandedVirtualFolders.Contains(virtualFolderNode.ObjectName))
                {
                    virtualFolderNode.Expand();
                }
            }
        }

        /// <summary>
        /// The on bank status changed.
        /// </summary>
        private void OnBankStatusChanged()
        {
            if (disableBankStatusChangedEvent) return;
            this.BankStatusChanged.Raise();
            this.RecreateSoundNodes();
        }

        private bool disableBankStatusChangedEvent = false;
        public void BeginUpdate()
        {
            disableBankStatusChangedEvent = true;
        }

        public void EndUpdate()
        {
            disableBankStatusChangedEvent = false;
            OnBankStatusChanged();
        }

        /// <summary>
        /// The update icon.
        /// </summary>
        private void UpdateIcon()
        {
            if (this.ObjectBank.IsAutoGenerated)
            {
                this.ImageIndex = this.GetIconIndexForTypeName("ObjectStoreLightGrey");
            }
            else if (this.ObjectBank.IsLocalCheckout)
            {
                this.ImageIndex = this.GetIconIndexForTypeName("ObjectStoreGreyYellow");
            }
            else if (this.ObjectBank.IsCheckedOut)
            {
                this.ImageIndex = this.GetIconIndexForTypeName("ObjectStoreGreyRed");
            }
            else
            {
                this.ImageIndex = this.GetIconIndexForTypeName("ObjectStoreGrey");
            }
        }

        #endregion
    }
}