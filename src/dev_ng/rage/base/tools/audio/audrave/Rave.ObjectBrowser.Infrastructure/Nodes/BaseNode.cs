// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for BaseNode.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.ObjectBrowser.Infrastructure.Nodes
{
    using System.Collections;
    using System.Windows.Forms;

    /// <summary>
    /// Summary description for BaseNode.
    /// </summary>
    public abstract class BaseNode : TreeNode
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseNode"/> class.
        /// </summary>
        /// <param name="objectName">
        /// The object name.
        /// </param>
        /// <param name="episode">
        /// The episode.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="iconMap">
        /// The icon map.
        /// </param>
        protected BaseNode(string objectName, string episode, string type, Hashtable iconMap)
        {
            this.ObjectName = objectName;
            this.Text = objectName;
            this.Type = type;
            this.Episode = episode;
            this.IconMap = iconMap;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the episode.
        /// </summary>
        public string Episode { get; set; }

        /// <summary>
        /// Gets or sets the object name.
        /// </summary>
        public string ObjectName { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets the icon map.
        /// </summary>
        public Hashtable IconMap { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get base icon index.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public abstract int GetBaseIconIndex();

        /// <summary>
        /// The get object path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetObjectPath()
        {
            if (this.Parent == null)
            {
                return this.ObjectName;
            }

            return ((BaseNode)this.Parent).GetObjectPath() + "\\" + this.ObjectName;
        }

        /// <summary>
        /// The get type description.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public abstract string GetTypeDescription();

        /// <summary>
        /// The reset text.
        /// </summary>
        public void ResetText()
        {
            this.Text = this.ObjectName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get icon index for type name.
        /// </summary>
        /// <param name="typeName">
        /// The type name.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        protected int GetIconIndexForTypeName(string typeName)
        {
            if (null != this.IconMap && this.IconMap.ContainsKey(typeName))
            {
                return (int)this.IconMap[typeName];
            }

            return 0;
        }

        #endregion
    }
}