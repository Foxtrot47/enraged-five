// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SoundNode.cs" company="Rockstar North">
//
// </copyright>
// <summary>
//   Summary description for SoundNode.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.ObjectBrowser.Infrastructure.Nodes
{
	using Rave.Types.Infrastructure.Interfaces.Objects;
	using System.Collections;
	using System.Drawing;
	using System.Windows.Forms;

	/// <summary>
	/// Summary description for SoundNode.
	/// </summary>
	public class SoundNode : BaseNode
	{

		/// <summary>
		/// The m_icon index.
		/// </summary>
		private readonly int m_iconIndex;

		public static Font boldFont = new Font(System.Windows.Forms.TreeView.DefaultFont, FontStyle.Bold);
		/// <summary>
		/// Initializes a new instance of the <see cref="SoundNode"/> class.
		/// </summary>
		/// <param name="objectInstance">
		/// The object instance.
		/// </param>
		/// <param name="iconIndex">
		/// The icon index.
		/// </param>
		/// <param name="iconMap">
		/// The icon map.
		/// </param>
		public SoundNode(IObjectInstance objectInstance, int iconIndex, Hashtable iconMap)
			: base(objectInstance.Name, objectInstance.Episode, objectInstance.Type, iconMap)
		{
			this.ObjectInstance = objectInstance;
			this.m_iconIndex = iconIndex;
			this.ImageIndex = iconIndex;
			this.SelectedImageIndex = iconIndex;
			SetFont();
		}

		/// <summary>
		/// Gets or sets the object instance.
		/// </summary>
		public IObjectInstance ObjectInstance { get; set; }

		/// The get base icon index.
		/// </summary>
		/// <returns>
		/// The <see cref="int"/>.
		/// </returns>
		public override int GetBaseIconIndex()
		{
			return this.m_iconIndex;
		}

		public void SetFont()
		{
			Font font;
			Color color;
			font = null;
			color = Color.Black;

			if (ObjectInstance == null)
			{

				color = Color.Red;
			}
			else
			{

				if (this.ObjectInstance.IsMuted)
				{
					font = boldFont;
					color = Color.DarkOrange;
				}
				else if (this.ObjectInstance.IsSoloed)
				{
					font = boldFont;
					color = Color.Blue;
				}
			}

			if (this.IsSelected)
			{
				color = SystemColors.HighlightText;
			}

			this.NodeFont = font;
			this.ForeColor = color;

		}
		/// <summary>
		/// The get type description.
		/// </summary>
		/// <returns>
		/// The <see cref="string"/>.
		/// </returns>
		public override string GetTypeDescription()
		{
			return "Sound";
		}

	}
}