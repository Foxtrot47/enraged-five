// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VirtualFolderNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for VirtualFolderNode.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.ObjectBrowser.Infrastructure.Nodes
{
    using System.Collections;

    /// <summary>
    /// Summary description for VirtualFolderNode.
    /// </summary>
    public class VirtualFolderNode : BaseNode
    {
        #region Constants

        /// <summary>
        /// The base image index.
        /// </summary>
        private const int BaseImageIndex = 3;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VirtualFolderNode"/> class.
        /// </summary>
        /// <param name="displayName">
        /// The display name.
        /// </param>
        /// <param name="episode">
        /// The episode.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="iconMap">
        /// The icon map.
        /// </param>
        public VirtualFolderNode(string displayName, string episode, string type, Hashtable iconMap)
            : base(displayName, episode, type, iconMap)
        {
            this.SelectedImageIndex = this.ImageIndex = this.GetIconIndexForTypeName("greyFolder");
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get base icon index.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public override int GetBaseIconIndex()
        {
            return BaseImageIndex;
        }

        /// <summary>
        /// The get type description.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string GetTypeDescription()
        {
            return "Virtual Folder";
        }

        #endregion
    }
}