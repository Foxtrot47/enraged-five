namespace Rave.ObjectBrowser.Infrastructure.Interfaces
{
    using System.Windows.Forms;

    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public interface IObjectBrowser
    {
        TreeView GetTreeView();

        void FindObject(IObjectInstance obj);

        void FindObjectBank(IObjectBank objectBank);

        void ShowHierarchy(SoundNode node);

        string GetObjectBrowserType();
    }
}