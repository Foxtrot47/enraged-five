﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using Models;

namespace Rave.EnvelopeSoundEditor.Converters
{
	public class OutputToStringConvertor : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value.GetType() != typeof(GraphicalEnvelopeSound.OutputsDefinition))
			{
				if (value.GetType() != typeof (EnvelopeSoundMode))
				{
					return string.Empty;
				}
				return value.ToString().TrimStart("kEnvelopeSound".ToCharArray());
			}
			return ((GraphicalEnvelopeSound.OutputsDefinition)value).Destination.ToString().TrimStart("kEnvelopeSound".ToCharArray());
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value.GetType() != typeof(string))
			{
				return EnvelopeSoundMode.kEnvelopeSoundVolume;
			}
			EnvelopeSoundMode envelopeSoundMode;
			Enum.TryParse((string)value, true, out envelopeSoundMode);
			return envelopeSoundMode;
		}
	}
}
