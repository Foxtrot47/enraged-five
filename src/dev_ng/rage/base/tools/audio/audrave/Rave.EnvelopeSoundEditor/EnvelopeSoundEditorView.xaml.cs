﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using System.Xml;
using Rave.CurveEditor2;
using Rave.CurveEditor2.Axes;
using Rave.CurveEditor2.Curves;

namespace Rave.EnvelopeSoundEditor
{
	using Rave.Plugins.Infrastructure.Interfaces;
	using Rave.Types.Infrastructure.Interfaces.Objects;

	/// <summary>
	///   Interaction logic for EnvelopeSoundEditorView.xaml
	/// </summary>
	public partial class EnvelopeSoundEditorView : IRAVEObjectEditorPlugin
	{
		private EnvelopeViewModel m_viewModel;

		public Point EditorPosition
		{
			get { return m_viewModel.CurveEditor.CurveEditorViewInstance.Editor.TranslatePoint(new Point(0, 0), Grid); }
		}

		public EnvelopeSoundEditorView()
		{
		
		}

		public string GetName()
		{
			return "EnvelopeSound Sound Editor";
		}

		public bool Init(XmlNode settings)
		{
			return true;
		}

		public string ObjectType
		{
			get { return "GraphicalEnvelopeSound"; }
		}

		public void Dispose()
		{
			// Do nothing
		}

#pragma warning disable 0067
		public event Action<IObjectInstance> OnObjectEditClick;
		public event Action<IObjectInstance> OnObjectRefClick;
		public event Action<string, string> OnWaveRefClick;
		public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067

		private IObjectInstance m_objectToEdit;
		public void EditObject(IObjectInstance objectInstance, Mode mode)
		{
			OnUserControlUnloaded(null, null);
			m_objectToEdit = objectInstance;
			UserControl_Loaded(null, null);
			InitializeComponent();
		}

		private void OnUserControlUnloaded(object sender, RoutedEventArgs e)
		{
			if (m_viewModel != null)
			{
				//m_objectToEdit.Bank.BankStatusChanged -= LoadView;
				m_viewModel = null;
			}

			if (e != null)
			{
				e.Handled = true;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (m_objectToEdit != null)
			{

				m_viewModel = new EnvelopeViewModel();
				//m_objectToEdit.Bank.BankStatusChanged += LoadView;
				m_viewModel.EditObject(m_objectToEdit);
				this.DataContext = m_viewModel;
			}
		}


		private void ContentControl_OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			var itemsControl = sender as ItemsControl;
			if (itemsControl != null)
			{
				if (itemsControl.Name == "editor")
				{
					e.Handled = true;
				}
				
				
			}
		}
	}
}