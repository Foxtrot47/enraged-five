﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using GalaSoft.MvvmLight.Command;
using Rave.CurveEditor2.Curves;
using WPFToolLib.Extensions;

namespace Rave.EnvelopeSoundEditor.Sectioning
{
	public class VerticalLineViewModel : INotifyPropertyChanged
	{
		private ControlPoint _controlPoint;

		public double X
		{
			get
			{
				return _controlPoint.XPos;

			}

			set
			{
				_controlPoint.XPos = value;
				PropertyChanged.Raise(this, "X");
			}
		}

		public double Y1
		{
			get
			{
				return Height - _controlPoint.YPos - ControlPoint.Size;

			}
			
		}

		public double Y2
		{
			get
			{
				return Height - _controlPoint.YPos + ControlPoint.Size;
			}
		}

		private readonly ItemsControl _editor;
		private string _brush;

		public bool EditMode
		{
			get { return _editMode; }
			set
			{
				_editMode = value;
				this.PropertyChanged.Raise(this, "EditMode");
			}
		}

		public double Height
		{
			get { return _editor.ActualHeight; }
		}

		public ItemsControl Editor
		{
			get { return _editor; }
		}

		public string Brush
		{
			get { return _brush; }
			set
			{
				_brush = value;
				this.PropertyChanged.Raise(this, "Brush");
			}
		}

		private ICommand _leftButtonDown;
		public ICommand LeftButtonDown
		{
			get
			{
				if (_leftButtonDown == null)
				{
					_leftButtonDown = new RelayCommand(()
						=>
						EditMode = true
						)
						;
				}
				return _leftButtonDown;
			}
		}

		private ICommand _leftButtonUp;
		private bool _editMode;
		private readonly List<DependencyObject> _hitResultsList;

		public ICommand LeftButtonUp
		{
			get
			{
				if (_leftButtonUp == null)
				{
					_leftButtonUp = new RelayCommand(
						()
				=>
				EditMode = false
				)
				;
				}
				return _leftButtonUp;
			}
		}

		public VerticalLineViewModel(ControlPoint cp, string color, ItemsControl editor)
		{
			_editor = editor;
			_editor.SizeChanged +=
				(p, r) =>
				{
					PropertyChanged.Raise(this, "X");
					PropertyChanged.Raise(this, "Height");
					PropertyChanged.Raise(this, "Editor");
				};

			Brush = color;
			_controlPoint = cp;

			_controlPoint.PropertyChanged +=
			(p, r) =>
			{
				if (r.PropertyName == "XPos")
				{
					PropertyChanged.Raise(this, "X");
				}

				if (r.PropertyName == "YPos")
				{
					PropertyChanged.Raise(this, "Y2");
					PropertyChanged.Raise(this, "Y1");
				}
			};
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}