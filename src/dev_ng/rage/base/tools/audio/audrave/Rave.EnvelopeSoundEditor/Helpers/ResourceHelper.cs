﻿using System.IO;


namespace Rave.EnvelopeSoundEditor.Helpers
{
	public class ResourceHelper
	{

		public static string GetResourceTextFile(object self, string filename)
		{
			string result = null;
			using (Stream stream = self.GetType().Assembly.
					   GetManifestResourceStream("Rave.EnvelopeSoundEditor.Resources." + filename))
			{
				if (stream != null)
					using (StreamReader sr = new StreamReader(stream))
					{
						result = sr.ReadToEnd();
					}
			}
			return result;
		}

	}
}
