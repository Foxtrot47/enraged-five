﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Models;
using Rave.CurveEditor2.Curves;
using Rave.EnvelopeSoundEditor.Extensions;

namespace Rave.EnvelopeSoundEditor.Helpers
{
	public class CurveDrawingHelper
	{
		public static void UpdateAllControlPoints(Dictionary<GraphicalEnvelopeSound.OutputsDefinition, Curve[]> segmentsDictionary, GraphicalEnvelopeSound envelopeSound)
		{
			double totalTime = EnvelopeSoundHelper.GetTotalTimeForDrawing(envelopeSound);
			
			foreach (GraphicalEnvelopeSound.OutputsDefinition outputDefinition in segmentsDictionary.Keys)
			{

				var segments = segmentsDictionary[outputDefinition];
				
				for (int i = 0; i < segments.Length; i++)
				{

					double time = 0;
					switch (i)
					{
						case 0:
							time = envelopeSound.InputParams[0].Time;
							break;

						case 1:
							time = envelopeSound.InputParams[1].Time;
							break;

						case 2:
							time = envelopeSound.GetDrawingHoldTime();
							break;

						case 3:
							time = envelopeSound.GetDrawingReleaseTime();
							break;
					}
					double startposition = EnvelopeSoundHelper.CalculateStartPositions(envelopeSound)[i];
					if (segments[i] != null)
					{
						Point[] points = outputDefinition.Phases[i].TransformPoints.Select(
							p => new Point((p.x * (time / totalTime)) + startposition, p.y)).ToArray();

						Point[] bpoints = outputDefinition.Phases[i].BezierPoints.Select(
							p => new Point((p.x * (time / totalTime)) + startposition, p.y)).ToArray();

						for (int j = 0; j < segments[i].ControlPoints.Count; j++)
						{
							segments[i].ControlPoints[j].X = points[j].X;
							segments[i].ControlPoints[j].Y = points[j].Y;
						}

						for (int j = 0; j < segments[i].BezierPoints.Count; j++)
						{
							segments[i].BezierPoints[j] = bpoints[j];
						}
					}
				}

			}

		}
	}
}
