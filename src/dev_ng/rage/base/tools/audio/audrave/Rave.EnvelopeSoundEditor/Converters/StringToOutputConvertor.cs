﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Models;

namespace Rave.EnvelopeSoundEditor.Converters
{
	public class StringToOutputConvertor : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value.GetType() != typeof(string))
			{
				return EnvelopeSoundMode.kEnvelopeSoundVolume;
			}
			EnvelopeSoundMode envelopeSoundMode;
			Enum.TryParse((string)value, true, out envelopeSoundMode);
			return envelopeSoundMode;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
