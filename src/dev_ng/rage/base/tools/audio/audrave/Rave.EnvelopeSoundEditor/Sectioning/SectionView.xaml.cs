﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rave.EnvelopeSoundEditor.Sectioning
{
	/// <summary>
	/// Interaction logic for SectionView.xaml
	/// </summary>
	public partial class SectionView : UserControl, ISectionView
	{
		public SectionView()
		{
			InitializeComponent();
		}

		public SectionView(SectionViewModel viewModel)
		{
			InitializeComponent();
			DataContext = viewModel;
		}
	}
}
