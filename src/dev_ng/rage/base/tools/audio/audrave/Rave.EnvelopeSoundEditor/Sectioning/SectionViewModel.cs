﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Rave.CurveEditor2;
using Rave.CurveEditor2.Curves;
using WPFToolLib.Extensions;

namespace Rave.EnvelopeSoundEditor.Sectioning
{
	public class SectionViewModel : INotifyPropertyChanged
	{
		private Curve _curve;
		private VerticalLineViewModel _verticalLine;
		private bool _editMode;

		public VerticalLineViewModel VerticalLine
		{
			get { return _verticalLine; }
			private set
			{
				_verticalLine = value;
				PropertyChanged.Raise(this, "VerticalLine");
			}
		}

		public Curve Curve
		{
			get { return _curve; }
			set
			{
				_curve = value;
				PropertyChanged.Raise(this, "Curve");
			}
		}

		public double Width
		{
			get { return Curve.ControlPoints.Last().XPos - Curve.ControlPoints.First().XPos; }
		}

		public bool EditMode
		{
			get { return _editMode; }
			set
			{
				_editMode = value;
				PropertyChanged.Raise(this, "EditMode");
			}
		}

		public double X1
		{
			get { return Curve.ControlPoints.First().XPos; }
		}

		public PointCollection Points
		{
			get
			{
				return
					new PointCollection()
					{
						new Point(X1, VerticalLine.Height),
						new Point(X1 + Width, VerticalLine.Height),
						GetBottomPoints().Item1,
						GetBottomPoints().Item2
					};

			}
		}

		public int LoopCount
		{
			get { return _loopCount; }
			set
			{
				_loopCount = value;
				PropertyChanged.Raise(this, "LoopCount");
			}
		}

		private int _varience;

		public Tuple<int, int> Varience
		{
			get
			{
				_varience = (int)(VerticalLine.Height * _holdVarience * Curve.Parent.Parent.YAxis.Zoom);
				return new Tuple<int, int>(_varience, -_varience);
			}
		}

		private float _holdVarience;

		public void SetVariance(float holdVarience)
		{
			_holdVarience = holdVarience;
			this.PropertyChanged.Raise(this, "Varience");
		}

		public SectionViewModel(Curve curve, ItemsControl editor, Border uiElementToStickTo)
		{
			VerticalLine = new VerticalLineViewModel(curve.ControlPoints.Last(), curve.Color, editor);

			Curve = curve;

			Curve.ControlPoints.First().PropertyChanged +=
				(p, r)
					=>
				{
					if (r.PropertyName == "XPos")
					{
						PropertyChanged.Raise(this, "X1");
						PropertyChanged.Raise(this, "Width");
						PropertyChanged.Raise(this, "Points");
					}
				};

			Curve.ControlPoints.Last().PropertyChanged +=
				(p, r)
					=>
				{
					if (r.PropertyName == "XPos")
					{
						PropertyChanged.Raise(this, "X1");
						PropertyChanged.Raise(this, "Width");
						PropertyChanged.Raise(this, "Points");
					}
				};
			VerticalLine.PropertyChanged +=
				(p, r)
					=>
				{
					if (r.PropertyName == "X")
					{
						PropertyChanged.Raise(this, "X1");
						PropertyChanged.Raise(this, "Width");
						PropertyChanged.Raise(this, "Points");
					}
					if (r.PropertyName == "Height")
					{
						PropertyChanged.Raise(this, "Varience");
					}
				};

			Curve.Parent.Parent.YAxis.PropertyChanged +=
			(p, r)
					=>
			{
				if (r.PropertyName == "Zoom")
				{
					PropertyChanged.Raise(this, "Varience");
				}
			};

			this._uiElementToStickTo = uiElementToStickTo;
			if (_uiElementToStickTo != null)
			{
				_uiElementToStickTo.SizeChanged += (p, r) => PropertyChanged.Raise(this, "Points");
			}
		}

		public void RefreshVisualTriangle()
		{
			PropertyChanged.Raise(this, "Points");
		}

		private readonly Border _uiElementToStickTo;
		private int _loopCount;

		private Tuple<Point, Point> GetBottomPoints()
		{
			Point item1 = new Point(0,0);
			Point item2 = new Point(0,0);
			if (PresentationSource.FromVisual(VerticalLine.Editor) != null && _uiElementToStickTo != null)
			{
				item1 = PointToOtherElement(VerticalLine.Editor, _uiElementToStickTo,
					new Point(_uiElementToStickTo.CornerRadius.TopRight, 0));
				item2 = PointToOtherElement(VerticalLine.Editor, _uiElementToStickTo,
					new Point(_uiElementToStickTo.ActualWidth - _uiElementToStickTo.CornerRadius.TopLeft, 0));
			}
			return new Tuple<Point, Point>(item2, item1);

		}

		public static Point PointToOtherElement(UIElement parent, UIElement child, Point pointOnChild)
		{
			try
			{
				Point childPoint = child.PointToScreen(pointOnChild);
				Point parentPoint = parent.PointToScreen(new Point(0, 0));
				return new Point(childPoint.X - parentPoint.X, childPoint.Y - parentPoint.Y);
			}
			catch (Exception)
			{
				return new Point(0, 0);
			}

		}


		public event PropertyChangedEventHandler PropertyChanged;
	}
}
