﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using Models;

namespace Rave.EnvelopeSoundEditor.Converters
{
	public class OutputToColorConvertor : IValueConverter
	{
		private static Dictionary<string, string> colors = new Dictionary<string, string>()
		{
			{"HPF","9400D3"},
			{"LPF","FF1493" },
			{"Pan", "FF8C00"},
			{"Pitch", "228B22"},
			{"Variable", "8B0000"},
			{"Volume","6495ED"}
		};
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value.GetType() != typeof(GraphicalEnvelopeSound.OutputsDefinition))
			{
				return string.Empty;
			}
			switch (((GraphicalEnvelopeSound.OutputsDefinition)value).Destination)
			{
				case EnvelopeSoundMode.kEnvelopeSoundHPF:
					return "#" + colors["HPF"];
				case EnvelopeSoundMode.kEnvelopeSoundLPF:
					return "#" + colors["LPF"];
				case EnvelopeSoundMode.kEnvelopeSoundPan:
					return "#" + colors["Pan"];
				case EnvelopeSoundMode.kEnvelopeSoundPitch:
					return "#" + colors["Pitch"];
				case EnvelopeSoundMode.kEnvelopeSoundVariable:
					return "#" + colors["Variable"];
				case EnvelopeSoundMode.kEnvelopeSoundVolume:
					return "#" + colors["Volume"];

			}

			return "Blue";
		}

		public static string Convert(GraphicalEnvelopeSound.OutputsDefinition envelope, bool selected)
		{

			string prefix = (selected ? "#32" : "#B4");
			string suffix = "HPF";
			switch (envelope.Destination)
			{
				case EnvelopeSoundMode.kEnvelopeSoundHPF:
					suffix = "HPF";
					break;
				case EnvelopeSoundMode.kEnvelopeSoundLPF:
					suffix = "LPF";
					break;
				case EnvelopeSoundMode.kEnvelopeSoundPan:
					suffix = "Pan";
					break;
				case EnvelopeSoundMode.kEnvelopeSoundPitch:
					suffix = "Pitch";
					break;
				case EnvelopeSoundMode.kEnvelopeSoundVariable:
					suffix = "Variable";
					break;
				case EnvelopeSoundMode.kEnvelopeSoundVolume:
					suffix = "Volume";
					break;

			}

			return prefix + colors[suffix];
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
