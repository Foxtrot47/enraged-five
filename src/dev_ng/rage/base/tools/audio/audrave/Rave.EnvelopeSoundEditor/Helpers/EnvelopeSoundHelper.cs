﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLib;
using Models;
using Rave.CurveEditor2;
using Rave.EnvelopeSoundEditor.Extensions;

namespace Rave.EnvelopeSoundEditor.Helpers
{
	class EnvelopeSoundHelper
	{
		/// <summary>
		/// Clamps the points to 0 - 1 inside the outputDefinition
		/// </summary>
		/// <param name="outputDefinition"></param>
		public static void ClampPoints(GraphicalEnvelopeSound.OutputsDefinition outputDefinition)
		{
			if (outputDefinition.Phases.Count <= 0)
			{
				for (int i = 0; i < 4; i++)
				{
					outputDefinition.Phases.Add(new GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition());
				}
			}

			for (int i = 0; i < 4; i++)
			{
				
				if (outputDefinition.Phases[i] == null || outputDefinition.Phases[i].TransformPoints.Count < 2)
				{
					float y2 = 0.0f;
					switch (i)
					{
						case 0:
							y2 = 1;
							break;
						case 1:
							y2 = 0.5f;
							break;
						case 2:
							y2 = outputDefinition.Phases[i - 1].TransformPoints.Last().y;
							break;
						case 3:
							y2 = 0;
							break;
					}

					outputDefinition.Phases[i] = new GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition
					{
						TransformPoints = new ItemsObservableCollection<GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition.TransformPointsDefinition>
					{
						new GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition.TransformPointsDefinition{x = 0f, y = i > 0 ? outputDefinition.Phases[i-1].TransformPoints.Last().y : 0},
						new GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition.TransformPointsDefinition {x = 1, y = y2}
					}
					};
				}

				outputDefinition.Phases[i].TransformPoints.First().x = 0;
				outputDefinition.Phases[i].TransformPoints.Last().x = 1;
			
				//Fix bezier Points
				var phase = outputDefinition.Phases[i];
				if (phase.BezierPoints == null)
				{
					phase.BezierPoints =
						new ItemsObservableCollection<GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition.BezierPointsDefinition>();
				}

				if (phase.BezierPoints.Count < phase.TransformPoints.Count - 1)
				{
					if (i == 3 && phase.TransformPoints.Count == 2)
					{
						phase.BezierPoints.Add(new GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition.BezierPointsDefinition { x = (float)(0.25), y =(0) });
					}
					else
					{
						for (int r = phase.BezierPoints.Count > 0 ? phase.BezierPoints.Count : 1; r < phase.TransformPoints.Count; r++)
						{
							phase.BezierPoints.Add(new GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition.BezierPointsDefinition { x = (float)(0.5 * (phase.TransformPoints[r].x - phase.TransformPoints[r - 1].x) + phase.TransformPoints[r - 1].x), y = (float)(0.5 * (phase.TransformPoints[r].y - phase.TransformPoints[r - 1].y) + phase.TransformPoints[r - 1].y) });
						}		
					}
				
				}

				for (int r = 1; r < phase.TransformPoints.Count; r++)
				{
					phase.BezierPoints[r - 1].x = phase.BezierPoints[r - 1].x < phase.TransformPoints[r - 1].x
						? phase.TransformPoints[r - 1].x + 0.01f
						: phase.BezierPoints[r - 1].x > phase.TransformPoints[r].x
							? phase.TransformPoints[r].x - 0.01f
							: phase.BezierPoints[r - 1].x;
				}
			}
		}





		public static double GetTotalTime(GraphicalEnvelopeSound envelopeSound)
		{
			int extraHoldTime = 0;
			if (envelopeSound.HoldLoopCount > 0)
			{
				extraHoldTime = envelopeSound.HoldLoopCount * envelopeSound.InputParams[2].Time;
			}
			return envelopeSound.InputParams.Select(p => p.Time > 0 ? p.Time : 0).Sum() + extraHoldTime;
		}

		public static double GetTotalTimeForDrawing(GraphicalEnvelopeSound envelopeSound)
		{
			double totalTime = envelopeSound.InputParams[0].Time + envelopeSound.InputParams[1].Time;
			totalTime += envelopeSound.GetDrawingHoldTime();
			totalTime += envelopeSound.GetDrawingReleaseTime();

			return totalTime;
		}

		public static double[] GetDrawingTime(GraphicalEnvelopeSound envelopeSound)
		{
			double[] times = new double[4];
			times[0] = envelopeSound.InputParams[0].Time;
			times[1] = envelopeSound.InputParams[1].Time;
			times[2] = envelopeSound.GetDrawingHoldTime();
			times[3] = envelopeSound.GetDrawingReleaseTime();
			return times;
		}

		public static int GetInfiniteDrawingTime(GraphicalEnvelopeSound envelopeSound)
		{
			double infiniteTime = (envelopeSound.InputParams[0].Time + envelopeSound.InputParams[1].Time) / 2.00;
			if (infiniteTime.Equals(0))
			{
				infiniteTime = envelopeSound.HoldTime + envelopeSound.ReleaseTime;
				if (infiniteTime.Equals(0))
				{
					infiniteTime = 50;
				}
			}
			return (short)infiniteTime;

		}

		public static double[] CalculateStartPositions(GraphicalEnvelopeSound envelopeSound)
		{
			double[] array = new double[4];
			double totalTime = GetTotalTimeForDrawing(envelopeSound);
			array[0] = 0.00;
			array[1] = envelopeSound.InputParams[0].Time / totalTime;
			array[2] = array[1] + envelopeSound.InputParams[1].Time / totalTime;
			array[3] = array[2] + envelopeSound.GetDrawingHoldTime() / totalTime;
			return array;
		}

		public static ParameterDestinations GetDestination(GraphicalEnvelopeSound.OutputsDefinition outputsDefinition)
		{
			ParameterDestinations destination = ParameterDestinations.Variable;
			switch (outputsDefinition.Destination)
			{
				case EnvelopeSoundMode.kEnvelopeSoundHPF:
					destination = ParameterDestinations.Hpf;

					break;
				case EnvelopeSoundMode.kEnvelopeSoundLPF:
					destination = ParameterDestinations.Lpf;


					break;
				case EnvelopeSoundMode.kEnvelopeSoundPan:
					destination = ParameterDestinations.Pan;


					break;
				case EnvelopeSoundMode.kEnvelopeSoundPitch:
					destination = ParameterDestinations.Pitch;

					break;
				case EnvelopeSoundMode.kEnvelopeSoundVariable:
					destination = ParameterDestinations.Variable;


					break;
				case EnvelopeSoundMode.kEnvelopeSoundVolume:
					destination = ParameterDestinations.Volume;


					break;
			}
			return destination;
		}

		public static string GetUnit(GraphicalEnvelopeSound.OutputsDefinition outputsDefinition)
		{
			string unit = "";
			switch (outputsDefinition.Destination)
			{
				case EnvelopeSoundMode.kEnvelopeSoundHPF:
				case EnvelopeSoundMode.kEnvelopeSoundLPF:
					unit = "Hz";
					break;
				case EnvelopeSoundMode.kEnvelopeSoundPan:
					unit = "degrees";

					break;
				case EnvelopeSoundMode.kEnvelopeSoundPitch:
					unit = "semitones";
					break;
				case EnvelopeSoundMode.kEnvelopeSoundVariable: 
					unit = ""; 
					break;
				case EnvelopeSoundMode.kEnvelopeSoundVolume:
					unit = "db";
					break;
			}
			return unit;
		}
	}
}
