#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;
using GalaSoft.MvvmLight.Command;
using ModelLib;
using ModelLib.Extentions;
using ModelLib.Types;
using Models;
using rage.ToolLib.Extensions;
using Rave.CurveEditor2;
using Rave.CurveEditor2.Curves;
using Rave.CurveEditor2.Helpers;
using Rave.EnvelopeSoundEditor.Converters;
using Rave.EnvelopeSoundEditor.Extensions;
using Rave.EnvelopeSoundEditor.Helpers;
using Rave.EnvelopeSoundEditor.Sectioning;
using Rave.Types.Infrastructure.Enums;
using Rave.Types.Infrastructure.Interfaces.Objects;
using WPFToolLib.Extensions;

#endregion

namespace Rave.EnvelopeSoundEditor
{
    public class EnvelopeViewModel : INotifyPropertyChanged
    {
        private readonly bool[] _phaseOn = { true, true, true, true };
        private readonly int[] _previousPhaseTimes = new int[4];
        private readonly Dictionary<GraphicalEnvelopeSound.OutputsDefinition, Curve[]> _segmentsDictionary;
        private readonly Border[] _uiElementMenu = new Border[4];
        private ICommand _addOutput;
        private RelayCommand _attackZoom;
        private bool _controlPointUpdate = true;
        private CurveEditor _curveEditor;
        private RelayCommand _decayZoom;
        private bool _firstControlPointChanging;
        private RelayCommand _holdZoom;
        private RelayCommand _leftButtonUp; 
        private ICommand _mouseMoveGraphCommand;
        private IObjectInstance _objectToEdit;
        private double _originalXZoom = 1;
        private RelayCommand _releaseZoom;
        private RelayCommand _removeOutput;
        private Curve[] _selectedAdhrCurves = new Curve[4];
        private GraphicalEnvelopeSound.OutputsDefinition _selectedOutput;
        private RelayCommand<EnvelopeSoundMode> _setOutput;
        private ICommand _stackPanelSizeChangedCommand;
        private int _zoomedPhase = -1;
        private bool _zoomSet;

        public ICommand AddOutput
        {
            get
            {
                if (_addOutput == null)
                {
                    _addOutput = new RelayCommand<EnvelopeSoundMode>(AddNewOutput);
                }
                return _addOutput;
            }
        }

        public ContextMenu AttackContextMenu
        {
            get
            {
                return
                    _objectToEdit.GetVariableList(null, null, VariableListType.Input)
                        .GetContextMenu_Wpf(
                            new RoutedEventHandler((p, r) => EnvelopeSound.InputParams[0].Variable = GetStringFromTag(p)));
            }
        }

        public bool AttackOn
        {
            get { return EnvelopeSound.InputParams[0].Time > 0; }
            set
            {
                if (value == false)
                {
                    EnvelopeSound.InputParams[0].Time = 0;
                }
                else
                {
                    EnvelopeSound.InputParams[0].Time =
                        (ushort)(_previousPhaseTimes[0] > 0 ? _previousPhaseTimes[0] : 20);
                }
                ShowEnvelopes();
                PropertyChanged.Raise(this, "AttackOn");
            }
        }

        public RelayCommand AttackZoom
        {
            get
            {
                if (_attackZoom == null)
                {
                    _attackZoom = new RelayCommand
                        (
                        () =>
                        {
                            if (AttackOn && _zoomedPhase != 0)
                            {
                                CurveEditor.ViewModel.XAxis.Displacement =
                                    _segmentsDictionary.First().Value[0].ControlPoints.First().X;
                                CurveEditor.ViewModel.XAxis.Zoom =
                                    EnvelopeSoundHelper.GetTotalTimeForDrawing(EnvelopeSound) /
                                    EnvelopeSound.InputParams[0].Time;
                                _zoomedPhase = 0;
                            }
                            else
                            {
                                CurveEditor.ViewModel.ResetZoom.Execute(null);
                                _zoomedPhase = -1;
                            }
                        });
                }
                return _attackZoom;
            }
        }

        public CurveEditor CurveEditor
        {
            get { return _curveEditor; }
            set
            {
                _curveEditor = value;
                PropertyChanged.Raise(this, "CurveEditor");
            }
        }

        public ContextMenu DecayContextMenu
        {
            get
            {
                return
                    _objectToEdit.GetVariableList(null, null, VariableListType.Input)
                        .GetContextMenu_Wpf(
                            new RoutedEventHandler((p, r) => EnvelopeSound.InputParams[1].Variable = GetStringFromTag(p)));
            }
        }

        public bool DecayOn
        {
            get { return EnvelopeSound.InputParams[1].Time > 0; }
            set
            {
                if (value == false)
                {
                    EnvelopeSound.InputParams[1].Time = 0;
                }
                else
                {
                    EnvelopeSound.InputParams[1].Time = _previousPhaseTimes[1] > 0 ? _previousPhaseTimes[1] : 20;
                }
                ShowEnvelopes();
                PropertyChanged.Raise(this, "DecayOn");
            }
        }

        public RelayCommand DecayZoom
        {
            get
            {
                if (_decayZoom == null)
                {
                    _decayZoom = new RelayCommand
                        (
                        () =>
                        {
                            if (DecayOn && _zoomedPhase != 1)
                            {
                                CurveEditor.ViewModel.XAxis.Displacement =
                                    _segmentsDictionary.First().Value[1].ControlPoints.First().X;
                                CurveEditor.ViewModel.XAxis.Zoom =
                                    EnvelopeSoundHelper.GetTotalTimeForDrawing(EnvelopeSound) /
                                    EnvelopeSound.InputParams[1].Time;
                                _zoomedPhase = 1;
                            }
                            else
                            {
                                CurveEditor.ViewModel.ResetZoom.Execute(null);
                                _zoomedPhase = -1;
                            }
                        });
                }
                return _decayZoom;
            }
        }

        public GraphicalEnvelopeSound EnvelopeSound { get; private set; }

        public ContextMenu HoldContextMenu
        {
            get
            {
                return
                    _objectToEdit.GetVariableList(null, null, VariableListType.Input)
                        .GetContextMenu_Wpf(
                            new RoutedEventHandler((p, r) => EnvelopeSound.InputParams[2].Variable = GetStringFromTag(p)));
            }
        }

        public bool HoldOn
        {
            get { return EnvelopeSound.InputParams[2].Time != 0; }
            set
            {
                if (value == false)
                {
                    EnvelopeSound.InputParams[2].Time = 0;
                }
                else
                {
                    EnvelopeSound.InputParams[2].Time = (_previousPhaseTimes[2] > 0 ? _previousPhaseTimes[2] : 20);
                }
                ShowEnvelopes();
                PropertyChanged.Raise(this, "HoldOn");
            }
        }

        public RelayCommand HoldZoom
        {
            get
            {
                if (_holdZoom == null)
                {
                    _holdZoom = new RelayCommand
                        (
                        () =>
                        {
                            if (HoldOn && _zoomedPhase != 2)
                            {
                                CurveEditor.ViewModel.XAxis.Displacement =
                                    _segmentsDictionary.First().Value[2].ControlPoints.First().X;
                                CurveEditor.ViewModel.XAxis.Zoom =
                                    EnvelopeSoundHelper.GetTotalTimeForDrawing(EnvelopeSound) /
                                    EnvelopeSound.GetDrawingHoldTime();
                                _zoomedPhase = 2;
                            }
                            else
                            {
                                CurveEditor.ViewModel.ResetZoom.Execute(null);
                                _zoomedPhase = -1;
                            }
                        });
                }
                return _holdZoom;
            }
        }

        public bool IsSelectedVariable
        {
            get
            {
                if (SelectedOutput == null)
                {
                    return false;
                }
                return SelectedOutput.Destination == EnvelopeSoundMode.kEnvelopeSoundVariable;
            }
        }

        public RelayCommand LeftButtonUp
        {
            get
            {
                return _leftButtonUp ?? (_leftButtonUp = new RelayCommand(
                    () =>
                    {
                        if (Sections.Any(p => ((SectionViewModel)p.DataContext).VerticalLine.EditMode))
                        {
                            foreach (ISectionView sectionView in Sections)
                            {
                                ((SectionViewModel)sectionView.DataContext).VerticalLine.EditMode = false;
                            }

                            Sections.Select(p => p.DataContext as SectionViewModel)
                                .ToList()
                                .ForEach(p => p.EditMode = false);
                            CurveEditor.ViewModel.XAxis.Zoom = _originalXZoom;
                            _zoomSet = false;
                        }
                    }
                    ));
            }
        }

        public ICommand MouseMoveGraphCommand
        {
            get
            {
                if (_mouseMoveGraphCommand == null)
                {
                    _mouseMoveGraphCommand = new RelayCommand<MouseEventArgs>(MouseMoveGraph);
                }
                return _mouseMoveGraphCommand;
            }
        }

        public ContextMenu OutputVariableContextMenu
        {
            get
            {
                return
                    _objectToEdit.GetVariableList(null, null, VariableListType.Output)
                        .GetContextMenu_Wpf(
                            new RoutedEventHandler((p, r) => SelectedOutput.OutputVariable = GetStringFromTag(p)));
            }
        }

        public ContextMenu ReleaseContextMenu
        {
            get
            {
                return
                    _objectToEdit.GetVariableList(null, null, VariableListType.Input)
                        .GetContextMenu_Wpf(
                            new RoutedEventHandler((p, r) => EnvelopeSound.InputParams[3].Variable = GetStringFromTag(p)));
            }
        }

        public bool ReleaseOn
        {
            get { return EnvelopeSound.InputParams[3].Time != 0; }
            set
            {
                if (value == false)
                {
                    EnvelopeSound.InputParams[3].Time = 0;
                }
                else
                {
                    EnvelopeSound.InputParams[3].Time =
                        (short)(_previousPhaseTimes[3] > 0 ? _previousPhaseTimes[3] : 20);
                }
                ShowEnvelopes();
                PropertyChanged.Raise(this, "ReleaseOn");
            }
        }

        public RelayCommand ReleaseZoom
        {
            get
            {
                if (_releaseZoom == null)
                {
                    _releaseZoom = new RelayCommand
                        (
                        () =>
                        {
                            if (ReleaseOn && _zoomedPhase != 3)
                            {
                                CurveEditor.ViewModel.XAxis.Displacement =
                                    _segmentsDictionary.First().Value[3].ControlPoints.First().X;
                                CurveEditor.ViewModel.XAxis.Zoom =
                                    EnvelopeSoundHelper.GetTotalTimeForDrawing(EnvelopeSound) /
                                    EnvelopeSound.GetDrawingReleaseTime();
                                _zoomedPhase = 3;
                            }
                            else
                            {
                                CurveEditor.ViewModel.ResetZoom.Execute(null);
                                _zoomedPhase = -1;
                            }
                        });
                }
                return _releaseZoom;
            }
        }

        public ICommand RemoveOutput
        {
            get
            {
                return _removeOutput ?? (_removeOutput = new RelayCommand(() =>
                {
                    EnvelopeSound.Outputs.Remove(SelectedOutput);
                    _selectedOutput = null;
                    Sections.Clear();
                    _selectedAdhrCurves = null;
                    SaveToObject();
                    ShowEnvelopes();
                }));
            }
        }

        public BindingList<ISectionView> Sections { get; private set; }

        public bool IsReadonly
        {
            get { return !(_objectToEdit != null && !_objectToEdit.IsReadOnly); }
        }

        public GraphicalEnvelopeSound.OutputsDefinition SelectedOutput
        {
            get { return _selectedOutput; }
            set
            {
                if (value != null && value != _selectedOutput && _objectToEdit.Bank.IsCheckedOut)
                {
                    _selectedOutput = value;
                    PropertyChanged.Raise(this, "SelectedOutput");
                    PropertyChanged.Raise(this, "IsSelectedVariable");
                    PropertyChanged.Raise(this, "Max");
                    PropertyChanged.Raise(this, "Min");
                    PropertyChanged.Raise(this, "Unit");
                    ShowEnvelopes();
                }
            }
        }

        public RelayCommand<EnvelopeSoundMode> SetOutput
        {
            get
            {
                return _setOutput ?? (_setOutput = new RelayCommand<EnvelopeSoundMode>(
                    p =>
                    {
                        SelectedOutput.Destination = p;
                        SaveToObject();
                    }));
            }
        }

        public bool ShowTotalTime
        {
            get
            {
                return (EnvelopeSound.InputParams.All(p => p.Time >= 0 && string.IsNullOrEmpty(p.Variable))) &&
                       EnvelopeSound.HoldLoopCount >= 0;
            }
        }

        public ICommand StackPanelLoaded
        {
            get
            {
                if (_stackPanelSizeChangedCommand == null)
                {
                    _stackPanelSizeChangedCommand = new RelayCommand<StackPanel>(SetVisualTriangles);
                }
                return _stackPanelSizeChangedCommand;
            }
        }

        public double TotalTime
        {
            get
            {
                if (EnvelopeSound != null)
                {
                    return EnvelopeSoundHelper.GetTotalTime(EnvelopeSound);
                }
                return 0;
            }
        }

        public float Max
        {
            get
            {
                if (SelectedOutput != null)
                {
                    float max =
                        ParameterDestinationHelper.DeserializeOutputRangeValue(SelectedOutput.OutputRange.Max,
                            EnvelopeSoundHelper.GetDestination(SelectedOutput));
                    if (float.IsNaN(max) || float.IsInfinity(max))
                    {
                        SelectedOutput.OutputRange.Max -= 0.00001f;
                        max = ParameterDestinationHelper.DeserializeOutputRangeValue(SelectedOutput.OutputRange.Max,
                            EnvelopeSoundHelper.GetDestination(SelectedOutput));
                    }
                    return max;
                }
                return 1;
            }

            set
            {
                if (SelectedOutput != null)
                {
                    if (SelectedOutput.Destination != EnvelopeSoundMode.kEnvelopeSoundVariable)
                    {
                        SelectedOutput.OutputRange.Max =
                            Math.Min(ParameterDestinationHelper.SerializeOutputRangeValue(value,
                                EnvelopeSoundHelper.GetDestination(SelectedOutput)), 1.0f);
                    }
                    else
                    {
                        SelectedOutput.OutputRange.Max = value;
                    }
                    SetRange(SelectedOutput);
                    PropertyChanged.Raise(this, "Max");
                    SaveToObject();
                }
            }
        }

        public string Unit
        {
            get
            {
                if (SelectedOutput != null)
                {
                    return EnvelopeSoundHelper.GetUnit(SelectedOutput);
                }
                return "";
            }
        }

        public float Min
        {
            get
            {
                if (SelectedOutput != null)
                {
                    float min =
                        ParameterDestinationHelper.DeserializeOutputRangeValue(SelectedOutput.OutputRange.Min,
                            EnvelopeSoundHelper.GetDestination(SelectedOutput));
                    if (float.IsNaN(min) || float.IsInfinity(min))
                    {
                        SelectedOutput.OutputRange.Min += 0.00001f;
                        min = ParameterDestinationHelper.DeserializeOutputRangeValue(SelectedOutput.OutputRange.Min,
                            EnvelopeSoundHelper.GetDestination(SelectedOutput));
                    }
                    return min;
                }
                return 0;
            }

            set
            {
                if (SelectedOutput != null)
                {
                    if (SelectedOutput.Destination != EnvelopeSoundMode.kEnvelopeSoundVariable)
                    {
                        SelectedOutput.OutputRange.Min =
                            Math.Max(ParameterDestinationHelper.SerializeOutputRangeValue(value,
                                EnvelopeSoundHelper.GetDestination(SelectedOutput)), 0.0f);
                    }
                    else
                    {
                        SelectedOutput.OutputRange.Min = value;
                    }
                    SetRange(SelectedOutput);
                    PropertyChanged.Raise(this, "Min");
                    SaveToObject();
                }
            }
        }

        public bool ShouldStopChildren
        {
            get { return EnvelopeSound.ShouldStopChildren == TriState.yes; }
            set { EnvelopeSound.ShouldStopChildren = value ? TriState.yes : TriState.no; }
        }

        public bool ShouldCascadeRelease
        {
            get { return EnvelopeSound.ShouldCascadeRelease == TriState.yes; }
            set { EnvelopeSound.ShouldCascadeRelease = value ? TriState.yes : TriState.no; }
        }

        public EnvelopeViewModel()
        {
            _segmentsDictionary = new Dictionary<GraphicalEnvelopeSound.OutputsDefinition, Curve[]>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void AddNewOutput(EnvelopeSoundMode output)
        {
            GraphicalEnvelopeSound.OutputsDefinition outputdef = new GraphicalEnvelopeSound.OutputsDefinition
            {
                Destination = output,
                OutputRange = new GraphicalEnvelopeSound.OutputsDefinition.OutputRangeDefinition { Max = 1, Min = 0 },
                SmoothRate = -1
            };
            EnvelopeSoundHelper.ClampPoints(outputdef);
            EnvelopeSound.Outputs.Add(outputdef);
            SelectedOutput = outputdef;
            SaveToObject();
        }

        public void EditObject(IObjectInstance objectInstance)
        {
            if (objectInstance == null)
            {
                return;
            }

            if (_objectToEdit != null && _objectToEdit.Bank != null)
            {
                _objectToEdit.Bank.BankStatusChanged -= StateChanged;
            }

            _objectToEdit = objectInstance;
            if (_objectToEdit == null)
            {
                return;
            }

            if (_objectToEdit.Bank != null)
            {
                _objectToEdit.Bank.BankStatusChanged += StateChanged;
            }

            if (_objectToEdit.Node != null)
            {
                EnvelopeSound = _objectToEdit.Node.DeserializeToModel<GraphicalEnvelopeSound>();
                EnvelopeSound.PropertyChanged += EnvelopeSound_PropertyChanged;
                if (EnvelopeSound.InputParams.Count <= 0)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        int time = 500;
                        if (i > 1)
                        {
                            time = -1;
                        }
                        EnvelopeSound.InputParams.Add(new GraphicalEnvelopeSound.InputParamsDefinition { Time = time });
                    }
                }

                for (int i = 0; i < EnvelopeSound.InputParams.Count; i++)
                {
                    GraphicalEnvelopeSound.InputParamsDefinition inputParamsDefinition = EnvelopeSound.InputParams[i];

                    int index = i;
                    inputParamsDefinition.PropertyChanged += (p, r) => { OnInputParamsChanged(index); };
                }

                for (int i = 0; i < EnvelopeSound.InputParams.Count; i++)
                {
                    _previousPhaseTimes[i] = EnvelopeSound.InputParams[i].Time;
                }


                ShowEnvelopes();

                if (EnvelopeSound.Outputs.Count <= 0)
                {
                    AddNewOutput(EnvelopeSoundMode.kEnvelopeSoundVolume);
                }
            }
        }

        private void OnInputParamsChanged(int index)
        {
            PropertyChanged.Raise(this, "AttackOn");
            PropertyChanged.Raise(this, "ReleaseOn");
            PropertyChanged.Raise(this, "DecayOn");
            PropertyChanged.Raise(this, "HoldOn");
            PropertyChanged.Raise(this, "TotalTime");

            SaveToObject();
            if (!_phaseOn[index])
            {
                ShowEnvelopes();
                _previousPhaseTimes[index] = EnvelopeSound.InputParams[index].Time;
                _phaseOn[index] = true;
                return;
            }

            if (EnvelopeSound.InputParams[index].Time <= 0)
            {
                int minimum = index < 2 ? 0 : -1;
                if (EnvelopeSound.InputParams[index].Time < minimum)
                {
                    EnvelopeSound.InputParams[index].Time = minimum;
                }

                ShowEnvelopes();
                _phaseOn[index] = false;
            }
            else
            {
                if (_previousPhaseTimes[index] == -1)
                {
                    ShowEnvelopes();
                }
                else if (!_firstControlPointChanging && _controlPointUpdate && _previousPhaseTimes.Sum() != 0 &&
                         Math.Abs(EnvelopeSoundHelper.GetTotalTime(EnvelopeSound) -
                                  _previousPhaseTimes.Where(p => p > 0).Sum()) > 20)
                {
                    ShowEnvelopes();
                }
                else
                {
                    UpdateXAxisMax(EnvelopeSoundHelper.GetTotalTimeForDrawing(EnvelopeSound));
                    _controlPointUpdate = false;
                    CurveDrawingHelper.UpdateAllControlPoints(_segmentsDictionary, EnvelopeSound);
                    _controlPointUpdate = true;
                }
                _previousPhaseTimes[index] = EnvelopeSound.InputParams[index].Time;
                _phaseOn[index] = true;
            }
        }

        public void SaveToObject()
        {
            if (_objectToEdit.Node != null && !IsReadonly)
            {
                ReplaceNodes("InputParams");
                ReplaceNodes("Outputs");
                ReplaceNodes("HoldLoopCount");
                ReplaceNodes("HoldLoopCountVariance");
                ReplaceNodes("ShouldStopChildren");
                ReplaceNodes("ShouldCascadeRelease");
                _objectToEdit.MarkAsDirty();
            }
        }

        private void ReplaceNodes(string name)
        {
            XmlNode node = EnvelopeSound.SerialializeToNode();
            XmlNodeList nodeList = _objectToEdit.Node.SelectNodes(name);

            if (nodeList != null)
                foreach (XmlNode xmlNode in nodeList)
                {
                    _objectToEdit.Node.RemoveChild(xmlNode);
                }
            XmlNodeList otherNodes = node.SelectNodes(name);
            if (otherNodes != null)
                foreach (XmlNode xmlNode in otherNodes)
                {
                    if (_objectToEdit != null && _objectToEdit.Node != null && _objectToEdit.Node.ParentNode != null
                        && _objectToEdit.Node.ParentNode.OwnerDocument != null)
                    {
                        XmlNode importNode = _objectToEdit.Node.ParentNode.OwnerDocument.ImportNode(xmlNode, true);
                        _objectToEdit.Node.AppendChild(importNode);
                    }
                }
        }

        private void ApplyCurveChanges()
        {
            double totalTime = EnvelopeSoundHelper.GetTotalTimeForDrawing(EnvelopeSound);

            foreach (
                KeyValuePair<GraphicalEnvelopeSound.OutputsDefinition, Curve[]> segmentCurves in
                    _segmentsDictionary)
            {
                for (int i = 0; i < segmentCurves.Value.Length; i++)
                {
                    double time = 0;
                    switch (i)
                    {
                        case 0:
                            time = EnvelopeSound.InputParams[0].Time;
                            break;

                        case 1:
                            time = EnvelopeSound.InputParams[1].Time;
                            break;

                        case 2:
                            time = EnvelopeSound.GetDrawingHoldTime();
                            break;

                        case 3:
                            time = EnvelopeSound.GetDrawingReleaseTime();
                            break;
                    }

                    if (time > 0)
                    {
                        segmentCurves.Key.Phases[i].TransformPoints =
                            new ItemsObservableCollection
                                <GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition.TransformPointsDefinition>();
                        if (segmentCurves.Value[i] == null)
                        {
                            PlotPoints(segmentCurves.Key);
                        }
                        foreach (ControlPoint p in segmentCurves.Value[i].ControlPoints)
                        {
                            segmentCurves.Key.Phases[i].TransformPoints.Add
                                (
                                    new GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition.
                                        TransformPointsDefinition
                                    {
                                        x =
                                            ((float)
                                                ((p.X - EnvelopeSoundHelper.CalculateStartPositions(EnvelopeSound)[i]) *
                                                 totalTime / time)).Clamp01(),
                                        y = (float)p.Y
                                    }
                                );
                        }

                        segmentCurves.Key.Phases[i].BezierPoints =
                            new ItemsObservableCollection
                                <GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition.BezierPointsDefinition>();

                        foreach (Point p in segmentCurves.Value[i].BezierPoints)
                        {
                            segmentCurves.Key.Phases[i].BezierPoints.Add
                                (
                                    new GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition.BezierPointsDefinition
                                    {
                                        x =
                                            ((float)
                                                ((p.X - EnvelopeSoundHelper.CalculateStartPositions(EnvelopeSound)[i]) *
                                                 totalTime / time)).Clamp01(),
                                        y = (float)p.Y
                                    }
                                );
                        }
                    }
                }
                EnvelopeSoundHelper.ClampPoints(segmentCurves.Key);
            }

            SaveToObject();
        }

        private void ControlPointPropertyChanged(object p, PropertyChangedEventArgs r)
        {
            if ((r.PropertyName.Equals("X") || r.PropertyName.Equals("Y")) &&
                _controlPointUpdate)
            {
                ControlPoint cp = ((ControlPoint)p);



                if (cp.Equals(cp.Parent.ControlPoints.First()))
                {
                    FirstControlPointChanged(cp);
                }
                if (cp.Equals(cp.Parent.ControlPoints.Last()))
                {
                    LastControlPointChanged(cp);
                }


                ApplyCurveChanges();
            }
        }

        private void EnvelopeSound_PropertyChanged(object sender, PropertyChangedEventArgs p)
        {
            if (p.PropertyName == "HoldLoopCount")
            {
                foreach (ISectionView sectionView in Sections)
                {
                    if (sectionView is HoldSectionView)
                    {
                        SectionViewModel sectionViewModel = sectionView.DataContext as SectionViewModel;
                        if (sectionViewModel != null)
                            sectionViewModel.LoopCount = EnvelopeSound.HoldLoopCount;
                    }
                }
                PropertyChanged.Raise(this, "TotalTime");
            }

            PropertyChanged.Raise(this, "ShowTotalTime");
            SaveToObject();
        }

        private void FirstControlPointChanged(ControlPoint cp)
        {
            _firstControlPointChanging = true;
            List<Curve> curves = _selectedAdhrCurves.ToList();

            if (curves.IndexOf(cp.Parent) == 0)
            {
                cp.X = 0;
            }

            int i = curves.IndexOf(cp.Parent) - 1;

            if (i < 0)
            {
                return;
            }
            while (curves[i] == null || curves[i].ControlPoints.Count < 2)
            {
                i--;
                if (i < 0)
                {
                    return;
                }
            }
            Curve otherCurve = curves[i];
            double pastDisplayValue = otherCurve.ControlPoints.Last().XDisplay;

            otherCurve.ControlPoints.Last().Y = cp.Y;
            otherCurve.ControlPoints.Last().X = cp.X;
            if (Math.Abs(otherCurve.ControlPoints.Last().X - cp.X) > 0.01)
            {
                cp.X = otherCurve.ControlPoints.Last().X;
            }
            int timeChange = (int)(cp.XDisplay - pastDisplayValue);

            switch (i)
            {
                case 0:
                    EnvelopeSound.InputParams[i].Time = (short)(EnvelopeSound.InputParams[0].Time + timeChange);
                    break;

                case 1:
                    EnvelopeSound.InputParams[1].Time = (short)(EnvelopeSound.InputParams[1].Time + timeChange);
                    break;

                case 2:
                    if (EnvelopeSound.InputParams[2].Time != -1)
                    {
                        EnvelopeSound.InputParams[2].Time = (short)(EnvelopeSound.GetDrawingHoldTime() + timeChange);
                    }
                    else
                    {
                        cp.Y = otherCurve.ControlPoints.First().Y;
                        otherCurve.ControlPoints.Last().Y = cp.Y;
                    }
                    break;

                case 3:
                    if (EnvelopeSound.InputParams[3].Time != -1)
                        EnvelopeSound.InputParams[3].Time = (short)(EnvelopeSound.GetDrawingReleaseTime() + timeChange);
                    break;
            }

            switch (curves.IndexOf(cp.Parent))
            {
                case 0:
                    EnvelopeSound.InputParams[0].Time = (short)(EnvelopeSound.InputParams[0].Time - timeChange);
                    break;

                case 1:
                    EnvelopeSound.InputParams[1].Time = (short)(EnvelopeSound.InputParams[1].Time - timeChange);
                    break;

                case 2:
                    if (EnvelopeSound.InputParams[2].Time != -1)
                    {
                        EnvelopeSound.InputParams[2].Time = (short)(EnvelopeSound.GetDrawingHoldTime() - timeChange);
                    }
                    else
                    {
                        if (_selectedAdhrCurves.Last() != null)
                        {
                            _selectedAdhrCurves.Last().ControlPoints.First().Y = cp.Y;
                        }
                    }

                    break;

                case 3:
                    if (EnvelopeSound.InputParams[3].Time != -1)
                    {
                        EnvelopeSound.InputParams[3].Time = (short)(EnvelopeSound.GetDrawingReleaseTime() - timeChange);
                    }
                    else
                    {
                        cp.Parent.ControlPoints.Last().Y = cp.Y;
                    }
                    break;
            }

            ApplyCurveChanges();

            _controlPointUpdate = false;
            CurveDrawingHelper.UpdateAllControlPoints(_segmentsDictionary, EnvelopeSound);
            _controlPointUpdate = true;
            _firstControlPointChanging = false;
        }

        private void LastControlPointChanged(ControlPoint cp)
        {
            List<Curve> curves = _selectedAdhrCurves.ToList();

            int i = curves.IndexOf(cp.Parent) + 1;

            if (EnvelopeSound.InputParams[3].Time == -1 && curves.Last() != null && curves.Last().Equals(cp.Parent))
            {
                cp.Y = cp.Parent.ControlPoints.First().Y;
            }

            if (curves.Last() != null && curves.Last().Equals(cp.Parent) && EnvelopeSound.InputParams[3].Time != 0)
            {
                cp.X = 1.00;
            }

            if (i > curves.Count - 1)
            {
                return;
            }
            while (curves[i] == null || curves[i].ControlPoints.Count < 2)
            {
                i++;
                if (i > curves.Count - 1)
                {
                    return;
                }
            }


            cp.X = curves[i].ControlPoints.First().X;
        }

        private void MouseMoveGraph(MouseEventArgs e)
        {
            if (Mouse.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }
            foreach (ISectionView sectionView in Sections)
            {
                SectionViewModel sectionviewModel = sectionView.DataContext as SectionViewModel;
                if (sectionviewModel != null && sectionviewModel.VerticalLine.EditMode)
                {
                    _controlPointUpdate = false;
                    Sections.Select(p => p.DataContext as SectionViewModel).ToList().ForEach(p => p.EditMode = true);

                    List<Curve> curves = _selectedAdhrCurves.ToList();

                    int i = curves.IndexOf(sectionviewModel.Curve);

                    if (i < 0)
                    {
                        return;
                    }
                    // Get New X
                    double newX = ControlPoint.GetValueFromScreen(((Visual)e.Source).GetMousePosition().X,
                        curves[i].Parent.Parent.XAxis);
                    double displayX = ControlPoint.GetDisplayFromX(newX, curves[i].Parent.Parent.XAxis);

                    switch (curves.IndexOf(curves[i]))
                    {
                        case 0:
                            if (displayX > 0)
                            {
                                EnvelopeSound.InputParams[0].Time = (short)displayX;
                            }
                            break;

                        case 1:
                            if ((ushort)(displayX - EnvelopeSound.InputParams[0].Time) > 0)
                            {
                                EnvelopeSound.InputParams[1].Time =
                                    (short)(displayX - EnvelopeSound.InputParams[0].Time);
                            }
                            break;

                        case 2:

                            if (EnvelopeSound.InputParams[2].Time != -1 &&
                                (short)
                                    (displayX - EnvelopeSound.InputParams[0].Time - EnvelopeSound.InputParams[1].Time) >
                                0)
                            {
                                EnvelopeSound.InputParams[2].Time =
                                    (short)
                                        (displayX - EnvelopeSound.InputParams[0].Time -
                                         EnvelopeSound.InputParams[1].Time);
                            }
                            break;

                        case 3:
                            if (EnvelopeSound.InputParams[3].Time != -1 &&
                                (displayX - EnvelopeSound.InputParams[0].Time - EnvelopeSound.InputParams[1].Time -
                                 EnvelopeSound.GetDrawingHoldTime()) > 0)
                            {
                                EnvelopeSound.InputParams[3].Time =
                                    (short)
                                        (displayX - EnvelopeSound.InputParams[0].Time -
                                         EnvelopeSound.InputParams[1].Time -
                                         EnvelopeSound.GetDrawingHoldTime());
                            }
                            break;
                    }

                    _controlPointUpdate = true;
                    e.Handled = true;
                }
            }
        }

        private void PlotPoints(GraphicalEnvelopeSound.OutputsDefinition outputsDefinition)
        {
            Curve[] segments = new Curve[4];

            double totalTime = EnvelopeSoundHelper.GetTotalTimeForDrawing(EnvelopeSound);
            UpdateXAxisMax(totalTime);
            string color = OutputToColorConvertor.Convert(outputsDefinition, SelectedOutput != null);
            double[] startpositions = EnvelopeSoundHelper.CalculateStartPositions(EnvelopeSound);

            if (outputsDefinition == SelectedOutput)
            {
                Sections = new BindingList<ISectionView>();

                SetRange(outputsDefinition);
            }
            for (int i = 0; i < segments.Length; i++)
            {
                double time = 0;
                bool isOn = false;
                string selectedColor = string.Empty;
                switch (i)
                {
                    case 0:
                        time = EnvelopeSound.InputParams[0].Time;
                        isOn = AttackOn;
                        selectedColor = "DarkGreen";
                        break;

                    case 1:
                        time = EnvelopeSound.InputParams[1].Time;
                        isOn = DecayOn;
                        selectedColor = "DarkOrange";
                        break;

                    case 2:
                        time = EnvelopeSound.GetDrawingHoldTime();
                        isOn = HoldOn;
                        selectedColor = "Blue";
                        break;

                    case 3:
                        time = EnvelopeSound.GetDrawingReleaseTime();
                        isOn = ReleaseOn;
                        selectedColor = "MediumPurple";
                        break;
                }
                int i1 = i;

                if (outputsDefinition == SelectedOutput)
                {
                    if (isOn)
                    {
                        if (i > 0)
                        {
                            ClampY(segments, outputsDefinition.Phases[i], i);
                        }
                        if (EnvelopeSound.InputParams[i].Time == -1)
                        {
                            segments[i] = CurveEditor.AddHorizontalLine
                                (
                                    outputsDefinition.Phases[i].TransformPoints[0].y, startpositions[i1],
                                    startpositions[i1] + time / totalTime, selectedColor
                                );
                        }
                        else
                        {
                            segments[i] =
                                CurveEditor.AddCurve(
                                    outputsDefinition.Phases[i].TransformPoints.Select(
                                        p => new Point((p.x * time / totalTime + startpositions[i1]), p.y)),
                                    outputsDefinition.Phases[i].BezierPoints.Select(
                                        p => new Point((p.x * time / totalTime + startpositions[i1]), p.y)),
                                    new Tuple<ExtendedLineMode, ExtendedLineMode>(ExtendedLineMode.None,
                                        ExtendedLineMode.None), selectedColor);
                        }
                        SectionViewModel sectionViewModel = new SectionViewModel(segments[i],
                            CurveEditor.CurveEditorViewInstance.Editor,
                            _uiElementMenu[i]);

                        sectionViewModel.SetVariance(outputsDefinition.Phases[i].outputVariance);
                        int i2 = i;
                        outputsDefinition.Phases[i].PropertyChanged += (p, r) =>
                        {
                            if (outputsDefinition == SelectedOutput && r.PropertyName == "outputVariance")
                            {
                                sectionViewModel.SetVariance(outputsDefinition.Phases[i2].outputVariance);
                            }
                        };
                        if (i == 2)
                        {
                            sectionViewModel.LoopCount = EnvelopeSound.HoldLoopCount;
                            Sections.Add(new HoldSectionView(sectionViewModel));
                        }
                        else
                        {
                            Sections.Add(new SectionView(sectionViewModel));
                        }
                    }
                }
                else
                {
                    if (isOn)
                    {
                        if (i > 0)
                        {
                            ClampY(segments, outputsDefinition.Phases[i], i);
                        }
                        if (EnvelopeSound.InputParams[i].Time == -1)
                        {
                            segments[i] = CurveEditor.AddHorizontalLine
                                (
                                    outputsDefinition.Phases[i].TransformPoints[0].y, startpositions[i1],
                                    startpositions[i1] + time / totalTime, color
                                );
                        }
                        else
                        {
                            segments[i] =
                                CurveEditor.AddCurve(
                                    outputsDefinition.Phases[i].TransformPoints.Select(
                                        p => new Point((p.x * time / totalTime + startpositions[i1]), p.y)),
                                    outputsDefinition.Phases[i].BezierPoints.Select(
                                        p => new Point((p.x * time / totalTime + startpositions[i1]), p.y)),
                                    new Tuple<ExtendedLineMode, ExtendedLineMode>(ExtendedLineMode.None,
                                        ExtendedLineMode.None), color);
                        }
                    }
                }
            }
            if (outputsDefinition == SelectedOutput)
            {
                _selectedAdhrCurves = segments;
                PropertyChanged.Raise(this, "Sections");
            }
            _segmentsDictionary[outputsDefinition] = segments;
        }

        private void
            SetRange(GraphicalEnvelopeSound.OutputsDefinition outputsDefinition)
        {
            ParameterDestinations destination = EnvelopeSoundHelper.GetDestination(outputsDefinition);

            float min = ParameterDestinationHelper.DeserializeOutputRangeValue(outputsDefinition.OutputRange.Min,
                destination);
            while (float.IsNaN(min) || float.IsInfinity(min))
            {
                outputsDefinition.OutputRange.Min += float.Epsilon;
                min = ParameterDestinationHelper.DeserializeOutputRangeValue(outputsDefinition.OutputRange.Min,
                    destination);
            }

            float max = ParameterDestinationHelper.DeserializeOutputRangeValue(outputsDefinition.OutputRange.Max,
                destination);
            while (float.IsNaN(max) || float.IsInfinity(max))
            {
                outputsDefinition.OutputRange.Min -= float.Epsilon;
                max = ParameterDestinationHelper.DeserializeOutputRangeValue(outputsDefinition.OutputRange.Max,
                    destination);
            }

            CurveEditor.SetMin(min);
            CurveEditor.SetMax(max);
            CurveEditor.SetDestination(destination);
        }

        private static void ClampY(Curve[] segments,
            GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition phasesDefinition, int i)
        {
            int k = 1;
            while (segments[i - k] == null)
            {
                k++;
                if ((i - k) < 0)
                {
                    return;
                }
            }
            phasesDefinition.TransformPoints.First().y = (float)segments[i - k].ControlPoints.Last().Y;
        }

        private void SetVisualTriangles(StackPanel stackPanel)
        {
            for (int i = 0; i < stackPanel.Children.Count; i++)
            {
                Border element = stackPanel.Children[i] as Border;
                _uiElementMenu[i] = element;
            }
            if (Sections != null)
                foreach (ISectionView section in Sections)
                {
                    SectionViewModel sectionViewModel = section.DataContext as SectionViewModel;
                    if (sectionViewModel != null)
                    {
                        sectionViewModel.RefreshVisualTriangle();
                    }
                }
        }

        private void ShowEnvelopes()
        {
            CurveEditor = new CurveEditor();

            foreach (GraphicalEnvelopeSound.OutputsDefinition outputsDefinition in EnvelopeSound.Outputs)
            {
                EnvelopeSoundHelper.ClampPoints(outputsDefinition);
                PlotPoints(outputsDefinition);
                StateChanged();
            }
        }

        private void StateChanged()
        {
            if (_objectToEdit == null)
            {
                Sections.Clear();
                SelectedOutput = null;
                _selectedAdhrCurves = null;
                return;
            }
            CurveEditor.ViewModel.IsCheckedOut = _objectToEdit.Bank.IsCheckedOut;
            if (CurveEditor.ViewModel.IsCheckedOut && SelectedOutput != null && _selectedAdhrCurves != null)
            {
                SelectedOutput.PropertyChanged += (s, t) => { SaveToObject(); };

                foreach (Curve curve in _selectedAdhrCurves)
                {
                    if (curve != null)
                    {
                        curve.IsSelected = true;
                        curve.ControlPoints.ToList().ForEach(p => p.PropertyChanged += ControlPointPropertyChanged);

                        curve.ControlPoints.ListChanged += (p, r) =>
                        {
                            ApplyCurveChanges();

                        };

                        curve.BezierPoints.ListChanged += (p, r) =>
                        {
                            ApplyCurveChanges();
                        };

                    }
                }
            }
        }


        private void UpdateXAxisMax(double totalTime)
        {
            if (!CurveEditor.ViewModel.XAxis.Max.Equals(totalTime + totalTime / 10))
            {
                if (!_controlPointUpdate)
                {
                    if (!_zoomSet)
                    {
                        _originalXZoom = CurveEditor.ViewModel.XAxis.Zoom;
                        _zoomSet = true;
                    }
                    CurveEditor.ViewModel.XAxis.Zoom = CurveEditor.ViewModel.XAxis.Zoom *
                                                       ((totalTime) / CurveEditor.ViewModel.XAxis.Max);
                }
                CurveEditor.ViewModel.XAxis.Max = totalTime;
            }
        }

        private static string GetStringFromTag(object obj)
        {
            MenuItem item = obj as MenuItem;
            if (null != item)
            {
                string tag = item.Tag as string;
                return tag;
            }
            return null;
        }

        #region Unused

        public void AddPresetOutput(string preset, EnvelopeSoundMode output)
        {
            ItemsObservableCollection<GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition> phasesDefinitions =
                PhaseDefFromPreset(preset);
            GraphicalEnvelopeSound.OutputsDefinition outputsDefinition = new GraphicalEnvelopeSound.OutputsDefinition
            {
                Destination = output,
                Phases = phasesDefinitions
            };
            EnvelopeSound.Outputs.Add(outputsDefinition);
            SelectedOutput = outputsDefinition;
            SaveToObject();
        }

        private ItemsObservableCollection<GraphicalEnvelopeSound.OutputsDefinition.PhasesDefinition> PhaseDefFromPreset(
            string presetName)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(ResourceHelper.GetResourceTextFile(this, presetName));
            GraphicalEnvelopeSound graphicalEnvelopeSound =
                xmlDocument.DocumentElement.DeserializeToModel<GraphicalEnvelopeSound>();
            if (TotalTime < 50)
            {
                EnvelopeSound.InputParams = graphicalEnvelopeSound.InputParams;
            }
            return graphicalEnvelopeSound.Outputs[0].Phases;
        }

        #endregion
    }
}