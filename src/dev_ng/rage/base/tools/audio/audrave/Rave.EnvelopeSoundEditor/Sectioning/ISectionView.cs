﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Rave.EnvelopeSoundEditor.Sectioning
{
	public interface ISectionView
	{
		object DataContext { get; set; }
	}
}
