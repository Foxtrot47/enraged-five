﻿using Models;

namespace Rave.EnvelopeSoundEditor.Extensions
{
	static class EnvelopeSoundExtentions
	{
		public static int GetDrawingHoldTime(this GraphicalEnvelopeSound envelopeSound)
		{
			return envelopeSound.InputParams[2].Time >= 0 ? envelopeSound.InputParams[2].Time : Helpers.EnvelopeSoundHelper.GetInfiniteDrawingTime(envelopeSound);
		}

		public static int GetDrawingReleaseTime(this GraphicalEnvelopeSound envelopeSound)
		{
			return envelopeSound.InputParams[3].Time >= 0 ? envelopeSound.InputParams[3].Time : Helpers.EnvelopeSoundHelper.GetInfiniteDrawingTime(envelopeSound);
		}
	}
}
