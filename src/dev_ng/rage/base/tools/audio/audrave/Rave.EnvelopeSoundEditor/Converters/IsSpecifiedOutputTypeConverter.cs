﻿// -----------------------------------------------------------------------
// <copyright file="IsVariableConverter.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

using Rave.CurveEditor2;

namespace Rave.EnvelopeSoundEditor.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Determines whether an output type is equal to specified output type.
    /// </summary>
    public class IsSpecifiedOutputTypeConverter : IValueConverter
    {
        /// <summary>
        /// The instance.
        /// </summary>
        public static readonly IValueConverter Instance = new IsSpecifiedOutputTypeConverter();

        /// <summary>
        /// Determines whether an output type is equal to specified output type.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="targetType">
        /// The target type.
        /// </param>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        /// <param name="culture">
        /// The culture.
        /// </param>
        /// <returns>
        /// Value indicating whether output is equal to specified output type <see cref="object"/>.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType() != typeof(ParameterDestinations))
            {
                return false;
            }

            if (parameter.GetType() != typeof(ParameterDestinations))
            {
                return false;
            }

            var valueA = (ParameterDestinations)value;
            var valueB = (ParameterDestinations)parameter;

            return valueA.ToString() == valueB.ToString();
        }

        /// <summary>
        /// Convert back (not implemented).
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="targetType">
        /// The target type.
        /// </param>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        /// <param name="culture">
        /// The culture.
        /// </param>
        /// <returns>
        /// Not implemented <see cref="object"/>.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
