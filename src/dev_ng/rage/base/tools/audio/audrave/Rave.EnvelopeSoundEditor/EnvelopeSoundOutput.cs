using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using rage.ToolLib;
using Rave.CurveEditor2;
using Rave.CurveEditor2.CurveEvaluators;
using WPFToolLib.Extensions;

namespace Rave.EnvelopeSoundEditor
{
	public class EnvelopeSoundOutput : INotifyPropertyChanged
	{
		private ParameterDestinations m_destination;
		private string m_outputVariable;

		static EnvelopeSoundOutput()
		{
			DestinationValues = Enum<ParameterDestinations>.GetValues();
		}

		public EnvelopeSoundOutput(CurveEvaluator curveEvaluator, IEnumerable<string> variables)
		{
			if (curveEvaluator == null)
			{
				throw new ArgumentNullException("curveEvaluator");
			}
			if (variables == null)
			{
				throw new ArgumentNullException("variables");
			}
			CurveEvaluator = curveEvaluator;
			OutputVariables = variables;

			InputSmoothRate = new SmoothRate();
			Destination = ParameterDestinations.Volume;
			OutputRange = new Range(Destination);
		}

		public SmoothRate InputSmoothRate { get; private set; }

		public ParameterDestinations Destination
		{
			get { return m_destination; }
			set
			{
				if (value != m_destination)
				{
					m_destination = value;
					OutputRange.Destination = value;
					PropertyChanged.Raise(this, "Destination");
					Dirty.Raise();
				}
			}
		}

		public Range OutputRange { get; private set; }

		public string OutputVariable
		{
			get { return m_outputVariable; }
			set
			{
				if (!string.IsNullOrEmpty(value) &&
				    m_outputVariable != value)
				{
					m_outputVariable = value;
					PropertyChanged.Raise(this, "OutputVariable");
					Dirty.Raise();
				}
				IsOutputVariableValid = !string.IsNullOrEmpty(m_outputVariable) &&
				                        OutputVariables.Any(v => string.Compare(v, m_outputVariable) == 0);
				PropertyChanged.Raise(this, "IsOutputVariableValid");
			}
		}

		public IEnumerable<string> OutputVariables { get; private set; }
		public bool IsOutputVariableValid { get; private set; }
		public CurveEvaluator CurveEvaluator { get; private set; }
		public static IEnumerable<ParameterDestinations> DestinationValues { get; private set; }

		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		public event Action Dirty;
	}
}