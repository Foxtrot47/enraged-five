﻿namespace Rave.ExternalObjectEditor
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Xml;

    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Utils;

    /// <summary>
    /// Interaction logic for LauncherControl.xaml
    /// </summary>
    public partial class LauncherControl : UserControl, IRAVEObjectEditorPlugin
    {
        public LauncherControl()
        {
            InitializeComponent();
        }

        private string m_Name;
        private string m_Type;
        private string m_SettingsPath;

        private XmlDocument m_SettingsDoc;
        private XmlNode m_CmdLine;
        private XmlNode m_Exe;

        private IObjectInstance m_ObjectInstance;  

        #region IRAVEPlugin Members

        public string GetName()
        {
            return m_Name;
        }

        public bool Init(XmlNode settings)
        {
            foreach (XmlNode node in settings.ChildNodes)
            {
                switch (node.Name)
                {
                    case "Type":
                        m_Type = node.InnerText;
                        break;
                    case "Name":
                        m_Name = node.InnerText;
                        break;
                }
            }

            if (string.IsNullOrEmpty(m_Type) || string.IsNullOrEmpty(m_Name))
            {
                return false;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            sb.Append(@"\Rave\");
            sb.Append(m_Type);
            sb.Append(".xml");

            m_SettingsPath = sb.ToString();
            m_SettingsDoc = new XmlDocument();

            if (File.Exists(m_SettingsPath))
            {
                m_SettingsDoc.Load(m_SettingsPath);
            }
            else
            {
                m_SettingsDoc.AppendChild(m_SettingsDoc.CreateElement("Settings"));
            }

            XmlNodeList nodeList = m_SettingsDoc.DocumentElement.SelectNodes("child::Exe");
            m_Exe = nodeList.Count != 0 ? nodeList[0] : m_SettingsDoc.DocumentElement.AppendChild(m_SettingsDoc.CreateElement("Exe"));
            nodeList = m_SettingsDoc.DocumentElement.SelectNodes("child::CmdLine");
            m_CmdLine = nodeList.Count != 0 ? nodeList[0] : m_SettingsDoc.DocumentElement.AppendChild(m_SettingsDoc.CreateElement("CmdLine"));

            return true;
        }

        #endregion

        #region IRAVEObjectEditorPlugin Members

#pragma warning disable 0067
        public event Action<IObjectInstance> OnObjectEditClick;
        public event Action<IObjectInstance> OnObjectRefClick;
        public event Action<string, string> OnWaveRefClick;
        public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067

        public string ObjectType
        {
            get { return m_Type; }
        }

        public void EditObject(IObjectInstance objectInstance, Mode mode)
        {
            ExeTextBox.Text = m_Exe.InnerText;
            ParamsTextBox.Text = m_CmdLine.InnerText;
            m_ObjectInstance = objectInstance;

            this.SetDescription();
            this.SetExportForGame();

            if (!objectInstance.Bank.IsCheckedOut)
            {
                this.Description.IsEnabled = false;
                this.ExportForGame.IsEnabled = false;
            }
            else
            {
                this.Description.TextChanged += this.Description_TextChanged;
                this.ExportForGame.Checked += this.ExportForGame_CheckedChange;
                this.ExportForGame.Unchecked += this.ExportForGame_CheckedChange;
            }
        }

        public void Dispose()
        {
            // Do nothing
        }

        #endregion

        private void SetDescription()
        {
            if (null == this.m_ObjectInstance)
            {
                return;
            }

            var descriptionNode = this.m_ObjectInstance.Node.SelectSingleNode("Description");
            if (null != descriptionNode)
            {
                this.Description.Text = descriptionNode.InnerText;
            }
        }

        private void SetExportForGame()
        {
            if (null == this.m_ObjectInstance)
            {
                return;
            }

            var exportForGameNode = this.m_ObjectInstance.Node.SelectSingleNode("ExportForGame");
            if (null != exportForGameNode && exportForGameNode.InnerText.Equals("yes"))
            {
                this.ExportForGame.IsChecked = true;
            }
            else
            {
                this.ExportForGame.IsChecked = false;
            }
        }

        private void RunButton_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(ExeTextBox.Text))
            {
                ErrorManager.HandleInfo("Please Specify a path for the exe");
                return;
            }

            m_Exe.InnerText = ExeTextBox.Text;
            m_CmdLine.InnerText = ParamsTextBox.Text;
            m_SettingsDoc.Save(m_SettingsPath);

            string input = ParamsTextBox.Text.Replace("{OBJECTNAME}", m_ObjectInstance.Name);
            input = input.Replace("\r\n", " ");

            ProcessStartInfo psi = new ProcessStartInfo(m_Exe.InnerText);
            psi.Arguments = input;
            Process p = new Process();
            p.StartInfo = psi;
            p.Start();
        }

        private void ChoosePathButton_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".exe";
            dlg.Filter = "Applications (.exe)|*.exe";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                ExeTextBox.Text = dlg.FileName; ;
            }
        }

        private void Description_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (null == this.m_ObjectInstance || !this.m_ObjectInstance.Bank.IsCheckedOut)
            {
                return;
            }

            var descriptionNode = this.m_ObjectInstance.Node.SelectSingleNode("Description");

            // Create description node if required
            if (null == descriptionNode)
            {
                descriptionNode = m_ObjectInstance.Node.OwnerDocument.CreateElement("Description");
                this.m_ObjectInstance.Node.AppendChild(descriptionNode);
            }

            // Set description node text
            descriptionNode.InnerText = this.Description.Text;
            this.m_ObjectInstance.MarkAsDirty();
        }

        private void ExportForGame_CheckedChange(object sender, RoutedEventArgs e)
        {
            if (null == this.m_ObjectInstance || !this.m_ObjectInstance.Bank.IsCheckedOut)
            {
                return;
            }

            var exportForGameNode = this.m_ObjectInstance.Node.SelectSingleNode("ExportForGame");

            // Create exportForGame node if required
            if (null == exportForGameNode)
            {
                exportForGameNode = m_ObjectInstance.Node.OwnerDocument.CreateElement("ExportForGame");
                this.m_ObjectInstance.Node.AppendChild(exportForGameNode);
            }

            // Set exportForGame node text
            if (null != this.ExportForGame.IsChecked && this.ExportForGame.IsChecked.Value)
            {
                exportForGameNode.InnerText = "yes";
            }
            else
            {
                exportForGameNode.InnerText = "no";
            }

            this.m_ObjectInstance.MarkAsDirty();
        }
    }
}
