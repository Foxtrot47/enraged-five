﻿// -----------------------------------------------------------------------
// <copyright file="WaveRefs.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.WaveRefs
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.Instance;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;
    using global::Rave.Plugins.Infrastructure.Interfaces;
    using global::Rave.Types.Infrastructure.Interfaces.Objects;
    using global::Rave.Types.Waves;
    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    /// Report that generates a list of missing or invalid wave references.
    /// </summary>
    public class WaveRefs : IRAVEReportPlugin
    {
        /// <summary>
        /// The name.
        /// </summary>
        private string name;

        /// <summary>
        /// The invalid nodes.
        /// </summary>
        private IList<KeyValuePair<IObjectInstance, XmlNode>> invalidNodes; 

        /// <summary>
        /// The get name.
        /// </summary>
        /// <returns>
        /// The name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        /// Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;
                }
            }

            return true;
        }

        /// <summary>
        /// Display the results.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="browsers">
        /// The browsers.
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            this.InitData();
            this.DisplayResults();
        }

        /// <summary>
        /// Initialize the data.
        /// </summary>
        private void InitData()
        {
            this.invalidNodes = new List<KeyValuePair<IObjectInstance, XmlNode>>();

            foreach (
                var soundsLookup in
                    RaveInstance.ObjectLookupTables.Where(
                        lookupTable => lookupTable.Key.Type.ToLower().Equals("sounds")))
            {
                foreach (var objInstance in soundsLookup.Value.Values)
                {
                    var typeDefinition = objInstance.TypeDefinitions.FindTypeDefinition(objInstance.TypeName);

                    if (null == typeDefinition)
                    {
                        continue;
                    }

                    foreach (var fieldDef in typeDefinition.Fields)
                    {
                        // Check if object should have a wave ref
                        if (null != fieldDef.Units && fieldDef.Units.ToUpper() == "WAVEREF")
                        {
                            var waveRefNode = objInstance.Node.SelectSingleNode(fieldDef.Name);
                            if (null == waveRefNode)
                            {
                                this.invalidNodes.Add(new KeyValuePair<IObjectInstance, XmlNode>(objInstance, null));
                                break;
                            }

                            var bankNameNode = waveRefNode.SelectSingleNode("BankName");
                            var waveNameNode = waveRefNode.SelectSingleNode("WaveName");

                            if (null == bankNameNode || null == waveNameNode ||
                                string.IsNullOrEmpty(bankNameNode.InnerText) ||
                                string.IsNullOrEmpty(waveNameNode.InnerText))
                            {
                                this.invalidNodes.Add(new KeyValuePair<IObjectInstance, XmlNode>(objInstance, waveRefNode));
                                break;
                            }

                            var key = Path.Combine(bankNameNode.InnerText, waveNameNode.InnerText);
                            if (!WaveManager.WaveRefs.ContainsKey(key))
                            {
                                this.invalidNodes.Add(new KeyValuePair<IObjectInstance, XmlNode>(objInstance, waveRefNode));
                            }

                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Display the results.
        /// </summary>
        private void DisplayResults()
        {
            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Missing/Invalid Wave References</title></head>");
            sw.WriteLine("<style type=\"text/css\">");
            sw.WriteLine("<!--");
            sw.WriteLine("body, table");
            sw.WriteLine("{");
            sw.WriteLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
            sw.WriteLine("}");
            sw.WriteLine("table");
            sw.WriteLine("{");
            sw.WriteLine("border-collapse: collapse;");
            sw.WriteLine("border: 1px solid #CCC;");
            sw.WriteLine("margin: 8px 0px;");
            sw.WriteLine("}");
            sw.WriteLine("table td, table th");
            sw.WriteLine("{");
            sw.WriteLine("padding: 2px 15px;");
            sw.WriteLine("}");
            sw.WriteLine("table th");
            sw.WriteLine("{");
            sw.WriteLine("background: #FFC;");
            sw.WriteLine("font-weight: bold;");
            sw.WriteLine("}");
            sw.WriteLine("table td");
            sw.WriteLine("{");
            sw.WriteLine("border: 1px solid #CCC;");
            sw.WriteLine("padding: 0 0.5em;");
            sw.WriteLine("}");
            sw.WriteLine("-->");
            sw.WriteLine("</style>");
            sw.WriteLine("<body>");
            sw.WriteLine("<h2>Missing/Invalid Wave References</h2>");
            sw.WriteLine("<p>Description: Objects with missing or invalid wave references");

            if (!this.invalidNodes.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<table>");
                sw.WriteLine("<tr>");
                sw.WriteLine("<th>Object Name</th>");
                sw.WriteLine("<th>Bank Name</th>");
                sw.WriteLine("<th>Wave Reference</th>");
                sw.WriteLine("</tr>");

                foreach (var invalidNode in this.invalidNodes)
                {
                    var objInstance = invalidNode.Key;
                    var node = invalidNode.Value;

                    sw.WriteLine("<tr>");
                    sw.WriteLine("<td>" + objInstance.Name + "</td>");
                    sw.WriteLine("<td>" + objInstance.Bank.Name + "</td>");
                    if (null != node && !string.IsNullOrEmpty(node.InnerText))
                    {
                        var bankNameNode = node.SelectSingleNode("BankName");
                        var waveNameNode = node.SelectSingleNode("WaveName");

                        if (null != bankNameNode && null != waveNameNode
                            && !string.IsNullOrEmpty(bankNameNode.InnerText)
                            && !string.IsNullOrEmpty(waveNameNode.InnerText))
                        {
                            sw.WriteLine(
                                "<td>" + Path.Combine(bankNameNode.InnerText, waveNameNode.InnerText) + "</td>");
                        }
                        else
                        {
                            sw.WriteLine("<td>---</td>");
                        }
                    }
                    else
                    {
                        sw.WriteLine("<td>Not Set</td>");
                    }

                    sw.WriteLine("</tr>");
                }

                sw.WriteLine("</table>");
                sw.WriteLine("</body></html>");

                sw.Close();
                filesteam.Close();

                // Display report
                var report = new frmReport(this.name + " Report");
                report.SetURL(resultsFileName);
                report.Show();
            }
        }
    }
}
