// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XslReport.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The XSL report.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Windows.Forms.VisualStyles;

namespace Rave.XslReport
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;
    using System.Xml.Xsl;

    using Rave.Controls.Forms.Popups;
    using Rave.Instance;
    using Rave.ObjectBrowser.Infrastructure.Interfaces;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;

    using WaveSlotDataLib;

    /// <summary>
    /// The xsl report.
    /// </summary>
    public class XslReport : IRAVEReportPlugin
    {
        #region Fields

        /// <summary>
        /// The report name.
        /// </summary>
        private string m_name;

        /// <summary>
        /// The pack file.
        /// </summary>
        private string m_platform;

        /// <summary>
        /// The pack file.
        /// </summary>
        private string m_builtWavesPackFilePath;

        /// <summary>
        /// The schema.
        /// </summary>
        private string m_schemaPath;

        /// <summary>
        /// The audio config path.
        /// </summary>
        private string m_audioConfigPath;

        /// <summary>
        /// The output path.
        /// </summary>
        private string m_output;

        /// <summary>
        /// The xsl path.
        /// </summary>
        private string m_xslPath;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Display results.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="browsers">
        /// The browsers.
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            var outputFile = Path.GetTempPath();
            switch (this.m_output.ToUpper())
            {
                case "HTML":
                    outputFile += "output.html";
                    break;
                case "XML":
                    outputFile += "output.xml";
                    break;
                default:
                    outputFile += "output.txt";
                    break;
            }

            var transform = new XslCompiledTransform();
            transform.Load(this.m_xslPath);

            Configuration.ProjectSettings.SetCurrentPlatformByTag(this.m_platform);
            var isBigEndian = Configuration.ProjectSettings.IsBigEndian();
            var assetManager = RaveInstance.AssetManager;
            //getting latest on files apart form the audioconfig, GetAudioConfigPath() will get latest on that one
            assetManager.GetLatest(this.m_schemaPath, false);
            assetManager.GetLatest(this.m_builtWavesPackFilePath, false);
            assetManager.GetLatest(this.m_xslPath, false);
            var generator = new WaveSlotDataGenerator(this.m_schemaPath, this.m_builtWavesPackFilePath, isBigEndian, this.GetAudioConfigPath());
            var newDoc = generator.Run();

            using (var writer = new StreamWriter(outputFile))
            {
                transform.Transform(newDoc.CreateReader(), null, writer);
                var report = new frmReport(this.m_name + " Report");
                report.SetURL(outputFile);
                report.Show();
            }
        }

        /// <summary>
        /// Get the audio config path - need file with latest version number.
        /// </summary>
        /// <returns>
        /// The audio config path <see cref="string"/>.
        /// </returns>
        private string GetAudioConfigPath()
        {
            var assetManager = RaveInstance.AssetManager;
            var audioConfigLocalPath = assetManager.GetLocalPath(this.m_audioConfigPath);

            var dir = Path.GetDirectoryName(audioConfigLocalPath);
            var fileName = Path.GetFileName(audioConfigLocalPath);
            assetManager.GetLatest(Path.Combine(dir, fileName)+"...", false);

            var version = 0;
            string filePath = audioConfigLocalPath;

            foreach (var file in Directory.GetFiles(dir, fileName + "*", SearchOption.TopDirectoryOnly))
            {
                var currExt = Path.GetExtension(file);
                var versionstr = currExt.Substring(4);
                int currVersion;
                if (!int.TryParse(versionstr, out currVersion))
                {
                    continue;
                }

                if (currVersion > version)
                {
                    version = currVersion;
                    filePath = file;
                }
            }

            if (string.IsNullOrEmpty(filePath))
            {
                throw new Exception("Failed to find audio config file");
            }

            return filePath;
        }

        /// <summary>
        /// Get the report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.m_name;
        }

        /// <summary>
        /// Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            foreach (XmlNode setting in settings.ChildNodes)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.m_name = setting.InnerText;
                        break;
                    case "Platform":
                        this.m_platform = setting.InnerText;
                        break;
                    case "Schema":
                        this.m_schemaPath = setting.InnerText;
                        break;
                    case "BuiltWavesPackListFile":
                        this.m_builtWavesPackFilePath = setting.InnerText;
                        break;
                    case "AudioConfigPath":
                        this.m_audioConfigPath = setting.InnerText;
                        break;
                    case "XslPath":
                        this.m_xslPath = setting.InnerText;
                        break;
                    case "Output":
                        this.m_output = setting.InnerText;
                        break;
                }
            }

            return !string.IsNullOrEmpty(this.m_name) 
                && !string.IsNullOrEmpty(this.m_platform)
                && !string.IsNullOrEmpty(this.m_builtWavesPackFilePath)
                && !string.IsNullOrEmpty(this.m_schemaPath)
                && !string.IsNullOrEmpty(this.m_audioConfigPath)
                && !string.IsNullOrEmpty(this.m_xslPath);
        }

        /// <summary>
        /// Override ToString to return report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.GetName();
        }

        #endregion
    }
}