namespace Rave.Metadata.Popups
{
    using System;
    using System.Text;
    using System.Windows.Forms;

    public partial class frmObjBankMetadataCompiler : Form
    {
        private readonly StringBuilder m_errorsBuilder;
        private readonly string m_name;
        private readonly StringBuilder m_warningsBuilder;
        private int m_numErrors;
        private int m_numWarnings;

        public frmObjBankMetadataCompiler(string objBankName)
        {
            this.InitializeComponent();
            this.m_name = objBankName;
            this.lblName.Text = "Building " + this.m_name;
            this.m_errorsBuilder = new StringBuilder();
            this.m_warningsBuilder = new StringBuilder();
        }

        public void WriteError(string error)
        {
            this.m_numErrors++;
            this.m_errorsBuilder.Append("*** ERROR ***");
            this.m_errorsBuilder.Append(Environment.NewLine);
            this.m_errorsBuilder.Append(error.Replace("\n", Environment.NewLine));
            this.m_errorsBuilder.Append(Environment.NewLine);
        }

        public void WriteWarning(string warning)
        {
            this.m_numWarnings++;
            this.m_warningsBuilder.Append("*** WARNING ***");
            this.m_warningsBuilder.Append(Environment.NewLine);
            this.m_warningsBuilder.Append(warning.Replace("\n", Environment.NewLine));
            this.m_warningsBuilder.Append(Environment.NewLine);
        }

        public bool WriteSummary()
        {
            if ((this.m_numErrors + this.m_numWarnings) == 0)
            {
                return true;
            }

            if (this.m_numErrors > 0)
            {
                this.txtResults.Text = this.m_errorsBuilder.ToString();
            }

            if (this.m_numWarnings > 0)
            {
                this.txtResults.Text += this.m_warningsBuilder.ToString();
            }

            this.txtResults.Text += Environment.NewLine + "*** " + this.m_name + " Build Summary ***" + Environment.NewLine;
            this.txtResults.Text += this.m_numErrors + " Error(s)" + Environment.NewLine;
            this.txtResults.Text += this.m_numWarnings + " Warning(s)" + Environment.NewLine + Environment.NewLine;
            this.txtResults.SelectionStart = this.txtResults.Text.Length;
            this.txtResults.SelectionLength = 0;
            this.ShowDialog();
            
            return this.m_numErrors == 0 && this.m_numWarnings == 0;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmObjBankMetadataCompiler_Load(object sender, EventArgs e)
        {

        }
    }
}