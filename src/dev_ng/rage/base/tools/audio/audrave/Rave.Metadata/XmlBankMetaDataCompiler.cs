// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XmlBankMetaDataCompiler.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the XmlBankMetaDataCompiler type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.Metadata
{
    using System;
    using System.Collections.Generic;

    using rage;
    using rage.ToolLib.Logging;

    using Rave.BuildManager.Infrastructure.Enums;
    using Rave.BuildManager.Infrastructure.Types;
    using Rave.Instance;
    using Rave.Metadata.Infrastructure.Interfaces;
    using Rave.Metadata.Popups;
    using Rave.Types;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Utils;

    /// <summary>
    /// The xml bank meta data compiler.
    /// </summary>
    public class XmlBankMetaDataCompiler : IXmlBankMetaDataCompiler
    {
        private readonly List<string> m_errors;
        private readonly Dictionary<string, IMetadataManager> m_metadataManagers;
        private readonly List<string> m_warnings;

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlBankMetaDataCompiler"/> class.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="projectSettings">
        /// The project settings.
        /// </param>
        public XmlBankMetaDataCompiler(ILog log, audProjectSettings projectSettings)
        {
            this.RuntimeUniqueId = 0;

            this.m_errors = new List<string>();
            this.m_warnings = new List<string>();

            this.ProjectSettings = projectSettings ?? new audProjectSettings(Configuration.ProjectSettingsAssetPath);

            this.m_metadataManagers = new Dictionary<string, IMetadataManager>();

            // load schema into each type compiler
            foreach (var metadataType in Configuration.MetadataTypes)
            {
                if (!this.m_metadataManagers.ContainsKey(metadataType.Type))
                {
                    var metadataManager = new MetadataMgr(log, this.ProjectSettings, metadataType.ObjectDefinitions);
                    this.m_metadataManagers.Add(metadataType.Type, metadataManager);

                    var templateTypeName = metadataType.Type + "_TEMPLATE";
                    if (RaveInstance.AllBankManagers.ContainsKey(templateTypeName))
                    {
                        var compiler = this.m_metadataManagers[metadataType.Type].Compiler;
                        compiler.ClearTemplates();
                        foreach (var bank in RaveInstance.AllBankManagers[templateTypeName].Banks)
                        {
                            compiler.LoadTemplates(rage.ToolLib.Utility.ToXDoc(bank.Document));
                        }
                    }
                }
            }

            log.ExceptionLogged += this.ObjectBankMetaDataCompiler_OnException;
            log.ErrorLogged += this.ObjectBankMetaDataCompiler_OnErrorMessage;
            log.WarningLogged += this.ObjectBankMetaDataCompiler_OnWarningMessage;
        }

        public IDictionary<string, IMetadataManager> MetadataManagers
        {
            get { return this.m_metadataManagers; }
        }

        public int RuntimeUniqueId { get; set; }

        public audProjectSettings ProjectSettings { get; private set; }

        public bool CompileMetadata(IXmlBank bank)
        {
            this.m_errors.Clear();
            this.m_warnings.Clear();

            var typeName = bank.Type;
            var output = new frmObjBankMetadataCompiler(typeName);

            TypeUtils.CompileMetadataInMemory(this, new List<IXmlBank> { bank }, bank.MetadataFile, true);
            foreach (var error in this.m_errors)
            {
                output.WriteError(error);
            }
            foreach (var warning in this.m_warnings)
            {
                output.WriteWarning(warning);
            }
            return output.WriteSummary();
        }

        private void ObjectBankMetaDataCompiler_OnWarningMessage(string context, string msg)
        {
            this.m_warnings.Add(new BuildOutputElement(ElementType.Warning, msg, context).ToString());
        }

        private void ObjectBankMetaDataCompiler_OnErrorMessage(string context, string msg, bool writeToLog)
        {
            this.m_errors.Add(new BuildOutputElement(ElementType.Error, msg, context).ToString());
        }

        private void ObjectBankMetaDataCompiler_OnException(string context, Exception ex)
        {
            this.m_errors.Add(new BuildOutputElement(ElementType.Exception, ex.ToString(), context).ToString());
        }
    }
}