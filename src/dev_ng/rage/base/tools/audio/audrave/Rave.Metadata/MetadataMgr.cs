namespace Rave.Metadata
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Linq;

    using Rave.Utils;

    using rage;
    using rage.Compiler;
    using rage.Enums;
    using rage.Reflection;
    using rage.ToolLib;
    using rage.ToolLib.Logging;
    using rage.ToolLib.Writer;

    using Rave.Instance;
    using Rave.Metadata.Infrastructure.Interfaces;

    /// <summary>
    ///   Summary description for MetadataMgr.
    /// </summary>
    public class MetadataMgr : IMetadataManager
    {
        private const string OBJECTS = "Objects";
        private readonly ICompiledObjectLookup m_compiledObjectLookup;
        private readonly audProjectSettings m_projectSettings;

        public MetadataMgr(ILog log,
                           audProjectSettings projectSettings,
                           IEnumerable<audObjectDefinition> objectDefinitions)
        {
            if (projectSettings == null)
            {
                throw new ArgumentNullException("projectSettings");
            }
            this.m_projectSettings = projectSettings;

            this.m_compiledObjectLookup = new CompiledObjectLookup();
            this.Compiler = new MetaDataCompiler(log,
                                            Configuration.WorkingPath,
                                            Configuration.ObjectXmlPath,
                                            this.m_projectSettings,
                                            new Reflector(log, Configuration.MetadataCompilerJitPaths),
                                            new ObjectSizeTracker(),
                                            new StringTable(),
                                            //new TagTable(), 
                                            this.m_compiledObjectLookup,
                                            CompilationMode.TransformAndCompile);
            foreach (var objectDefinition in objectDefinitions)
            {
                RaveInstance.AssetManager.GetLatest(
                    string.Concat(Configuration.ObjectXmlPath, objectDefinition.DefinitionsFile), false);
            }
            this.Compiler.LoadTypeDefinitions(objectDefinitions);
            this.Compiler.ForceSerialization();
        }

        public MetaDataCompiler Compiler { get; private set; }

        public int FindObjectSize(string objName)
        {
            var size = this.m_compiledObjectLookup.GetObjectSize(objName);
            if (size == null)
            {
                return -1;
            }
            return (int) size.Value;
        }

        public XDocument CompileObject(XmlNode sound, IWriter output, audMetadataFile metadataFile)
        {
            var doc = rage.ToolLib.Utility.ToXDocRooted(sound, OBJECTS);
            this.Compiler.Compile(doc, output, metadataFile, EnumRuntimeMode.Dev);
            return doc;
        }

        public uint ComputeHash(string str)
        {
            var hash = new Hash {Value = str};
            return hash.Key;
        }

        public void UpdateObjectSize(string name, int offset, int size)
        {
            this.m_compiledObjectLookup.UpdateObjectSize(name, (uint) offset, (uint) size);
        }
    }
}