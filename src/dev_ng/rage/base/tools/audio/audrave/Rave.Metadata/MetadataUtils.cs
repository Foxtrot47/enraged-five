﻿// -----------------------------------------------------------------------
// <copyright file="MetadataUtils.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Rave.Metadata
{

	using rage;
	using rage.ToolLib.Logging;

	using Rave.Instance;
	using Rave.Utils;

	/// <summary>
	/// Metadata utils.
	/// </summary>
	public class MetadataUtils
	{
		/// <summary>
		/// Initializes static members of the <see cref="MetadataUtils"/> class.
		/// </summary>
		static MetadataUtils()
		{
			PendingMetadataFiles = new ConcurrentDictionary<audMetadataFile, bool>();
		}

		/// <summary>
		/// The pending metadata files.
		/// </summary>
		public static ConcurrentDictionary<audMetadataFile, bool> PendingMetadataFiles;

		/// <summary>
		/// The run metadata generation.
		/// </summary>
		/// <param name="instructionsWithOutputOnly">
		/// Flag to only run generation instructions which create an output file.
		/// </param>
		public static void RunMetadataGeneration(bool instructionsWithOutputOnly = false)
		{
			foreach (var metaDataFile in Configuration.MetadataFiles)
			{
				if (metaDataFile.GenerateMetadataElements == null)
				{
					continue;
				}
				
				PendingMetadataFiles.GetOrAdd(metaDataFile, instructionsWithOutputOnly);

				Task task = new Task(() => MetadataGenerationThreadPoolCallback(metaDataFile, instructionsWithOutputOnly));
				task.Start();
			}
		}

		private static void MetadataGenerationThreadPoolCallback(audMetadataFile metaDataFile, bool instructionsWithOutputOnly)
		{
            List<string> logOutput = new List<string>();
			var log = new TextLog(new ListLogWriter(out logOutput), new ContextStack());

			if (null == metaDataFile)
			{
				ErrorManager.HandleError("Thread context not of expected type audMetadataFile");
				return;
			}

			var directory = string.Concat(Configuration.ObjectXmlPath, metaDataFile.DataPath);
			var documentEntries = Utility.LoadDocuments(log, directory);
			var metadataCompiler =
				RaveInstance.XmlBankCompiler.MetadataManagers[metaDataFile.Type.ToUpper()].Compiler;
			if (!metadataCompiler.Generate(documentEntries, metaDataFile, false, instructionsWithOutputOnly))
			{
			    StringBuilder errorMessage = new StringBuilder("Failed to successfully run metadata generation steps for metadata type: \"" +
                                      metaDataFile.Type.ToUpper() + "\" with output file: \"" + metaDataFile.OutputFile + "\"");
			    errorMessage.Append(Environment.NewLine + "MetadataCompilerLog: " + Environment.NewLine);
			    foreach (string outputLine in logOutput) errorMessage.Append(Environment.NewLine + outputLine);
                ErrorManager.HandleError(errorMessage.ToString());
			}

			bool success = false;
			PendingMetadataFiles.TryRemove(metaDataFile, out success);
		}
	}
}
