﻿// -----------------------------------------------------------------------
// <copyright file="RaveInstance.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Concurrent;
using rage.ToolLib;

namespace Rave.Instance
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Wpf = System.Windows;

    using rage.ToolLib.Logging;

    using Rave.AssetManager.Infrastructure.Interfaces;
    using Rave.BuildManager.Infrastructure.Interfaces;
    using Rave.HierarchyBrowser.Infrastructure.Interfaces;
    using Rave.Metadata.Infrastructure.Interfaces;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.RemoteControl.Infrastructure.Interfaces;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Structs;
    using Rave.WaveBrowser.Infrastructure.Interfaces;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Rave instance - used as a lookup for all shared object instances.
    /// </summary>
    public class RaveInstance
    {
        /// <summary>
        /// Initializes static members of the <see cref="RaveInstance"/> class.
        /// </summary>
        static RaveInstance()
        {
            RaveLog = new XmlLog(new NullLogWriter(), new ContextStack(), "Rave");
            AllTypeDefinitions = new Dictionary<string, ITypeDefinitions>(StringComparer.OrdinalIgnoreCase);
            CheckedOutBanks = new ObservableHashSet<IXmlBank>();
            AllBankManagers = new Dictionary<string, IXmlBankManager>(StringComparer.OrdinalIgnoreCase);
            ObjectLookupTables = new Dictionary<ObjectLookupId, Dictionary<string, IObjectInstance>>();
        }

        /// <summary>
        /// Gets the rave log.
        /// </summary>
        public static ILog RaveLog { get; private set; }

        /// <summary>
        /// Gets or sets the main window.
        /// </summary>
        public static Wpf.Window MainWindow { get; set; }

        /// <summary>
        /// Gets or sets the active window.
        /// </summary>
        public static IWin32Window ActiveWindow { get; set; }

        /// <summary>
        /// Gets or sets the remote control.
        /// </summary>
        public static IRemoteControl RemoteControl { get; set; }

        /// <summary>
        /// Gets or sets the plugin manager.
        /// </summary>
        public static IPluginManager PluginManager { get; set; }

        /// <summary>
        /// Gets or sets the build manager.
        /// </summary>
        public static IBuildManager BuildManager { get; set; }

        /// <summary>
        /// Gets or sets the wave browser.
        /// </summary>
        public static IWaveBrowser WaveBrowser { get; set; }

        /// <summary>
        /// Gets or sets the hierarchy browser.
        /// </summary>
        public static IHierarchyBrowser HierarchyBrowser { get; set; }

        /// <summary>
        /// Gets or sets the asset manager.
        /// </summary>
        public static IAssetManager AssetManager { get; set; }

        /// <summary>
        /// Gets or sets the rave asset manager.
        /// </summary>
        public static IRaveAssetManager RaveAssetManager { get; set; }

        /// <summary>
        /// Gets the all type definitions.
        /// </summary>
        public static IDictionary<string, ITypeDefinitions> AllTypeDefinitions { get; private set; }

        /// <summary>
        /// Gets the checked out banks.
        /// </summary>
        public static ObservableHashSet<IXmlBank> CheckedOutBanks { get; private set; }

        /// <summary>
        /// Gets the all banks.
        /// </summary>
        public static Dictionary<string, IXmlBankManager> AllBankManagers { get; private set; }

        /// <summary>
        /// Gets the metadata managers.
        /// </summary>
        public static IXmlBankMetaDataCompiler XmlBankCompiler { get; set; }

        /// <summary>
        /// Gets or sets the object lookup tables.
        /// </summary>
        public static Dictionary<ObjectLookupId, Dictionary<string, IObjectInstance>> ObjectLookupTables { get; protected set; }

        /// <summary>
        /// Gets or sets the temp compiler.
        /// </summary>
        public static IXmlBankMetaDataCompiler TempCompiler { get; set; }
    }
}
