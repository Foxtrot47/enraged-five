﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Rave.SearchProvider.Infrastructure.Interfaces
{
    public interface ISearchProvider<T>
    {
        /// <summary>
        /// An Enumerable List of objects to search in
        /// </summary>
        IEnumerable<T> ObjectsToSearch { get; }

        /// <summary>
        /// Searches for a string in an Object List
        /// </summary>
        /// <param name="searchString">
        /// The string to search for
        /// </param>
        /// <param name="searchNameHash">
        /// Search Name Hash
        /// </param>
        /// <param name="shouldOnlySearchNames">
        /// Search Names only
        /// </param>
        /// <param name="isCaseSensitive">
        /// Perform a case sensitive search
        /// </param>
        /// <param name="exactNameMatch">
        /// Perform an exact name match search
        /// </param>
        /// <returns>
        /// A list of results T
        /// </returns>
        ObservableCollection<T> Search(string searchString, bool searchNameHash = false, bool shouldOnlySearchNames = false, bool isCaseSensitive = false, bool exactNameMatch = false);
    }
}
