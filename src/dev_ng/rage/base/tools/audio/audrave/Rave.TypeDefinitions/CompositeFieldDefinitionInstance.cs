﻿// -----------------------------------------------------------------------
// <copyright file="CompositeFieldDefinitionInstance.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions
{
    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    /// <summary>
    /// The composite field definition instance.
    /// </summary>
    public class CompositeFieldDefinitionInstance : FieldDefinitionInstance, ICompositeFieldDefinitionInstance
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CompositeFieldDefinitionInstance"/> class.
        /// </summary>
        /// <param name="fieldDefinition">
        /// The field definition.
        /// </param>
        /// <param name="defaultValue">
        /// The default value.
        /// </param>
        public CompositeFieldDefinitionInstance(ICompositeFieldDefinition fieldDefinition, string defaultValue)
            : base(fieldDefinition, defaultValue)
        {
            this.CompositeFieldDef = fieldDefinition;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the composite field def.
        /// </summary>
        public ICompositeFieldDefinition CompositeFieldDef { get; private set; }

        /// <summary>
        /// Gets the fields.
        /// </summary>
        public IFieldDefinition[] Fields
        {
            get
            {
                return this.CompositeFieldDef.Fields;
            }
        }

        /// <summary>
        /// Gets the max occurs.
        /// </summary>
        public int MaxOccurs
        {
            get
            {
                return this.CompositeFieldDef.MaxOccurs;
            }
        }

        #endregion
    }
}
