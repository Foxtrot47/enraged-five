﻿// -----------------------------------------------------------------------
// <copyright file="TypeDefinitions.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Schema;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.EnumEntry;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Utils;

    /// <summary>
    /// Summary description for SoundTypes.
    /// </summary>
    public class TypeDefinitions : ITypeDefinitions
    {
        #region Static Fields

        /// <summary>
        /// The ms_error message.
        /// </summary>
        private static readonly StringBuilder ms_errorMessage = new StringBuilder();

        #endregion

        #region Fields

        /// <summary>
        /// The m_schema path.
        /// </summary>
        private readonly string m_schemaPath;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TypeDefinitions"/> class.
        /// </summary>
        /// <param name="filePaths">
        /// The file paths.
        /// </param>
        /// <param name="schemaPath">
        /// The schema path.
        /// </param>
        public TypeDefinitions(string[] filePaths, string schemaPath)
        {
            this.m_schemaPath = string.IsNullOrEmpty(schemaPath)
                                     ? null
                                     : RaveInstance.AssetManager.GetLocalPath(schemaPath);
            this.Load(filePaths);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the enums.
        /// </summary>
        public EnumDefinition[] Enums { get; private set; }

        /// <summary>
        /// Gets the file paths.
        /// </summary>
        public string[] FilePaths { get; private set; }

        /// <summary>
        /// Gets the object types.
        /// </summary>
        public ITypeDefinition[] ObjectTypes { get; private set; }

        /// <summary>
        /// Gets the schema path.
        /// </summary>
        public string SchemaPath
        {
            get
            {
                return this.m_schemaPath;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The find enum definition.
        /// </summary>
        /// <param name="enumName">
        /// The enum name.
        /// </param>
        /// <returns>
        /// The <see cref="EnumDefinition"/>.
        /// </returns>
        public EnumDefinition FindEnumDefinition(string enumName)
        {
            return this.Enums.FirstOrDefault(ed => ed.Name == enumName);
        }

        /// <summary>
        /// The find type definition.
        /// </summary>
        /// <param name="typeName">
        /// The type name.
        /// </param>
        /// <returns>
        /// The <see cref="TypeDefinition"/>.
        /// </returns>
        public ITypeDefinition FindTypeDefinition(string typeName)
        {
            return this.ObjectTypes.FirstOrDefault(t => t.Name == typeName);
        }

        /// <summary>
        /// The get allowed types.
        /// </summary>
        /// <param name="fieldDef">
        /// The field def.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ITypeDefinition> GetAllowedTypes(IFieldDefinition fieldDef)
        {
            if (!string.IsNullOrEmpty(fieldDef.AllowedType))
            {
                var typeDefinitions = fieldDef.TypeDefinition.TypeDefinitions;
                var allowedType = typeDefinitions.FindTypeDefinition(fieldDef.AllowedType);
                if (allowedType != null)
                {
                    var concreteDescendantTypes =
                        (from descendantType in allowedType.GetDescendantTypes()
                         where descendantType.IsAbstract == false
                         select descendantType).ToList();

                    if (concreteDescendantTypes.Count > 0)
                    {
                        concreteDescendantTypes.Sort((x, y) => string.Compare(x.Name, y.Name));
                    }

                    return concreteDescendantTypes;
                }
            }

            return new List<ITypeDefinition>();
        }

        /// <summary>
        /// The load.
        /// </summary>
        /// <param name="filePaths">
        /// The file paths.
        /// </param>
        public void Load(string[] filePaths)
        {
            ms_errorMessage.Remove(0, ms_errorMessage.Length);

            this.FilePaths = filePaths;

            var objectList = new ArrayList();
            var enumList = new ArrayList();

            var schemaSet = new XmlSchemaSet();
            XmlTextReader schemaReader = null;
            var settings = new XmlReaderSettings();

            if (!string.IsNullOrEmpty(this.SchemaPath))
            {
                GetLatest(this.SchemaPath);

                schemaReader = new XmlTextReader(this.SchemaPath);
                schemaSet.Add(null, schemaReader);
                settings.ValidationType = ValidationType.Schema;
                settings.Schemas = schemaSet;
            }

            foreach (var filePath in filePaths)
            {
                GetLatest(filePath);

                var document = new XmlDocument();
                try
                {
                    settings.ValidationEventHandler += settings_ValidationEventHandler;
                    var tr = new XmlTextReader(filePath);
                    var reader = XmlReader.Create(tr, settings);
                    document.Load(reader);
                    reader.Close();
                    tr.Close();
                    settings.ValidationEventHandler -= settings_ValidationEventHandler;
                }
                catch (Exception e)
                {
                    throw new ApplicationException(string.Format("Exception in: {0}", filePath), e);
                }

                foreach (XmlNode node in document.DocumentElement.ChildNodes)
                {
                    if (node.GetType() != typeof(XmlComment))
                    {
                        switch (node.Name)
                        {
                            case "Enum":
                                enumList.Add(new EnumDefinition(node));
                                break;
                            case "TypeDefinition":
                                objectList.Add(new TypeDefinition(node, this));
                                break;
                        }
                    }
                }
            }

            if (ms_errorMessage.Length > 0)
            {
                ErrorManager.HandleError(ms_errorMessage.ToString());
            }

            this.ObjectTypes = (ITypeDefinition[])objectList.ToArray(typeof(ITypeDefinition));
            this.Enums = (EnumDefinition[])enumList.ToArray(typeof(EnumDefinition));

            // add base sound fields to all object types
            foreach (var sd in this.ObjectTypes)
            {
                if (sd.InheritsFrom != null)
                {
                    var baseSound = this.FindTypeDefinition(sd.InheritsFrom);
                    if (baseSound == null)
                    {
                        throw new Exception("Object inherits from unknown type");
                    }

                    var fieldList = new ArrayList();
                    fieldList.AddRange(baseSound.Fields);
                    fieldList.AddRange(sd.Fields);
                    sd.Fields = (IFieldDefinition[])fieldList.ToArray(typeof(IFieldDefinition));
                }
            }

            if (schemaReader != null)
            {
                schemaReader.Close();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get latest.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        private static void GetLatest(string path)
        {
            try
            {
                RaveInstance.AssetManager.GetLatestForceErrors(path);
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
            }
        }

        /// <summary>
        /// The settings_ validation event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void settings_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            var xmlreader = sender as XmlReader;

            ms_errorMessage.AppendLine("Error in TypeDefinition ");
            ms_errorMessage.AppendLine(xmlreader.BaseURI);
            ms_errorMessage.AppendLine("NodeType  = " + xmlreader.NodeType);
            ms_errorMessage.AppendLine("NodeName  = " + xmlreader.Name);
            ms_errorMessage.AppendLine("NodeValue  = " + xmlreader.Value);

            if (xmlreader.NodeType.ToString() != "Attribute" && xmlreader.HasAttributes)
            {
                ms_errorMessage.AppendLine("Attributes:");
                while (xmlreader.MoveToNextAttribute())
                {
                    ms_errorMessage.AppendLine("   AttributeName = " + xmlreader.Name);
                    ms_errorMessage.AppendLine("   AttributeValue = " + xmlreader.Value);
                }
            }

            ms_errorMessage.AppendLine("-----------------------");
        }

        #endregion
    }
}
