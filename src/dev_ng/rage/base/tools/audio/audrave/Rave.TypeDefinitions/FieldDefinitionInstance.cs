﻿// -----------------------------------------------------------------------
// <copyright file="FieldDefinitionInstance.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;

namespace Rave.TypeDefinitions
{
	using System.Xml;

	using Rave.TypeDefinitions.Infrastructure.Interfaces;

	/// <summary>
	/// The field definition instance.
	/// </summary>
	public class FieldDefinitionInstance : IFieldDefinitionInstance
	{
		#region Fields

		/// <summary>
		/// The m_default value.
		/// </summary>
		private readonly string m_defaultValue;

		#endregion

		#region Constructors and Destructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FieldDefinitionInstance"/> class.
		/// </summary>
		/// <param name="fieldDefinition">
		/// The field definition.
		/// </param>
		/// <param name="defaultValue">
		/// The default value.
		/// </param>
		public FieldDefinitionInstance(IFieldDefinition fieldDefinition, string defaultValue)
		{
			this.FieldDef = fieldDefinition;
			this.m_defaultValue = defaultValue;
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// Gets a value indicating whether allow override control.
		/// </summary>
		public bool AllowOverrideControl
		{
			get
			{
				return this.FieldDef.AllowOverrideControl;
			}
		}

		/// <summary>
		/// Gets the allowed type.
		/// </summary>
		public string AllowedType
		{
			get
			{
				return this.FieldDef.AllowedType;
			}
		}

		public bool Tagged
		{
			get { return this.FieldDef.Tagged; }
		}

		/// <summary>
		/// Gets the default value.
		/// </summary>
		public string DefaultValue
		{
			get
			{
				return string.IsNullOrEmpty(this.m_defaultValue) ? this.FieldDef.DefaultValue : this.m_defaultValue;
			}
		}

		/// <summary>
		/// Gets the description.
		/// </summary>
		public string Description
		{
			get
			{
				return this.FieldDef.Description;
			}
		}

		/// <summary>
		/// Gets the display group.
		/// </summary>
		public string DisplayGroup
		{
			get
			{
				return this.FieldDef.DisplayGroup;
			}
		}

		/// <summary>
		/// Gets the field def.
		/// </summary>
		public IFieldDefinition FieldDef { get; private set; }

		/// <summary>
		/// Gets a value indicating whether ignore.
		/// </summary>
		public bool Ignore
		{
			get
			{
				return this.FieldDef.Ignore;
			}
		}

		/// <summary>
		/// Gets the name.
		/// </summary>
		public string Name
		{
			get
			{
				return this.FieldDef.Name;
			}
		}

		/// <summary>
		/// Gets the node.
		/// </summary>
		public XmlNode Node
		{
			get
			{
				return this.FieldDef.Node;
			}
		}

		/// <summary>
		/// Gets or sets the type definition.
		/// </summary>
		public ITypeDefinition TypeDefinition
		{
			get
			{
				return this.FieldDef.TypeDefinition;
			}

			set
			{
				this.FieldDef.TypeDefinition = value;
			}
		}

		/// <summary>
		/// Gets the type name.
		/// </summary>
		public string TypeName
		{
			get
			{
				return this.FieldDef.TypeName;
			}
		}

		/// <summary>
		/// Gets the units.
		/// </summary>
		public string Units
		{
			get
			{
				return this.FieldDef.Units;
			}
		}

		/// <summary>
		/// Gets a value indicating whether visible.
		/// </summary>
		public bool Visible
		{
			get
			{
				return this.FieldDef.Visible;
			}
		}

		#endregion

		#region Public Methods and Operators

		/// <summary>
		/// The as composite field.
		/// </summary>
		/// <returns>
		/// The <see cref="CompositeFieldDefinitionInstance"/>.
		/// </returns>
		public ICompositeFieldDefinitionInstance AsCompositeField()
		{
			var compositeFieldDef = this.FieldDef as ICompositeFieldDefinition;
			if (compositeFieldDef != null)
			{
				return new CompositeFieldDefinitionInstance(
					compositeFieldDef, string.IsNullOrEmpty(this.m_defaultValue) ? null : this.m_defaultValue);
			}

			return null;
		}

		/// <summary>
		/// The as simple field.
		/// </summary>
		/// <returns>
		/// The <see cref="SimpleFieldDefinitionInstance"/>.
		/// </returns>
		public ISimpleFieldDefinitionInstance AsSimpleField()
		{
			var simpleFieldDef = this.FieldDef as ISimpleFieldDefinition;
			if (simpleFieldDef != null)
			{
				return new SimpleFieldDefinitionInstance(
					simpleFieldDef, string.IsNullOrEmpty(this.m_defaultValue) ? null : this.m_defaultValue);
			}

			return null;
		}

		/// <summary>
		/// The to string.
		/// </summary>
		/// <returns>
		/// The <see cref="string"/>.
		/// </returns>
		public override string ToString()
		{
			return this.Name;
		}

		
		#endregion
	}
}
