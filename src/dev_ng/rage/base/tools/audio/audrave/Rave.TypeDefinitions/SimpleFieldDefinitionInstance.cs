﻿// -----------------------------------------------------------------------
// <copyright file="SimpleFieldDefinitionInstance.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions
{
    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    /// <summary>
    /// The simple field definition instance.
    /// </summary>
    public class SimpleFieldDefinitionInstance : FieldDefinitionInstance, ISimpleFieldDefinitionInstance
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleFieldDefinitionInstance"/> class.
        /// </summary>
        /// <param name="fieldDefinition">
        /// The field definition.
        /// </param>
        /// <param name="defaultValue">
        /// The default value.
        /// </param>
        public SimpleFieldDefinitionInstance(ISimpleFieldDefinition fieldDefinition, string defaultValue)
            : base(fieldDefinition, defaultValue)
        {
            this.SimpleFieldDef = fieldDefinition;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the enum name.
        /// </summary>
        public string EnumName
        {
            get
            {
                return this.SimpleFieldDef.EnumName;
            }
        }

        /// <summary>
        /// Gets the length.
        /// </summary>
        public int Length
        {
            get
            {
                return this.SimpleFieldDef.Length;
            }
        }

        /// <summary>
        /// Gets the max.
        /// </summary>
        public float Max
        {
            get
            {
                return this.SimpleFieldDef.Max;
            }
        }

        /// <summary>
        /// Gets the min.
        /// </summary>
        public float Min
        {
            get
            {
                return this.SimpleFieldDef.Min;
            }
        }

        /// <summary>
        /// Gets the simple field def.
        /// </summary>
        public ISimpleFieldDefinition SimpleFieldDef { get; private set; }

        #endregion
    }
}
