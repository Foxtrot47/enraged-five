﻿// -----------------------------------------------------------------------
// <copyright file="SimpleFieldDefinition.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions
{
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    /// <summary>
    /// The simple field definition.
    /// </summary>
    public class SimpleFieldDefinition : FieldDefinition, ISimpleFieldDefinition
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleFieldDefinition"/> class.
        /// </summary>
        /// <param name="definition">
        /// The definition.
        /// </param>
        /// <param name="typeDefinition">
        /// The type definition.
        /// </param>
        public SimpleFieldDefinition(XmlNode definition, ITypeDefinition typeDefinition)
            : base(definition, typeDefinition)
        {
            this.Min = this.GetFloatAttributeValue(definition, "min");
            this.Max = this.GetFloatAttributeValue(definition, "max");
            this.EnumName = this.GetAttributeValue(definition, "enum");
            this.Length = this.GetIntegerAttributeValue(definition, "length");
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the enum name.
        /// </summary>
        public string EnumName { get; private set; }

        /// <summary>
        /// Gets the length.
        /// </summary>
        public int Length { get; private set; }

        /// <summary>
        /// Gets the max.
        /// </summary>
        public float Max { get; private set; }

        /// <summary>
        /// Gets the min.
        /// </summary>
        public float Min { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The generate default xml node.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        public override XmlNode GenerateDefaultXmlNode(XmlNode parentNode)
        {
            // if there is no default value then don't create the node
            // (makes sense for flags to leave them unspecified by default)
            if (this.DefaultValue != null)
            {
                XmlNode node = parentNode.OwnerDocument.CreateElement(this.Name);
                node.InnerText = this.DefaultValue;
                parentNode.AppendChild(node);
                return node;
            }

            return null;
        }

        #endregion
    }
}
