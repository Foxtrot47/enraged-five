﻿// -----------------------------------------------------------------------
// <copyright file="TypeDefinition.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    /// <summary>
    /// The type definition.
    /// </summary>
    public class TypeDefinition : ITypeDefinition
    {
        #region Fields

        /// <summary>
        /// The m_descendant types.
        /// </summary>
        private List<ITypeDefinition> m_descendantTypes;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TypeDefinition"/> class.
        /// </summary>
        /// <param name="definition">
        /// The definition.
        /// </param>
        /// <param name="typeDefinitions">
        /// The type definitions.
        /// </param>
        public TypeDefinition(XmlNode definition, ITypeDefinitions typeDefinitions)
        {
            this.TypeDefinitions = typeDefinitions;
            this.Name = definition.Attributes["name"].Value;
            this.InheritsFrom = definition.Attributes["inheritsFrom"] != null
                                     ? definition.Attributes["inheritsFrom"].Value
                                     : null;
            this.Group = definition.Attributes["group"] != null ? definition.Attributes["group"].Value : null;
            this.Description = definition.Attributes["description"] != null
                                    ? definition.Attributes["description"].Value
                                    : null;
            this.IsAbstract = definition.Attributes["isAbstract"] != null
                               && definition.Attributes["isAbstract"].Value.ToLower() == "yes";
            this.Fields = FieldDefinition.ParseFieldDefinitions(definition, this);

            this.DevOnlyNameTable = definition.Attributes["devOnlyNameTable"] != null
                && definition.Attributes["devOnlyNameTable"].Value.ToLower() == "yes";

            this.SkipDeleteContainerOnRefDelete = definition.Attributes["skipDeleteContainerOnRefDelete"] != null
                                              && definition.Attributes["skipDeleteContainerOnRefDelete"].Value.ToLower() == "yes";

            this.Use16BitStringTableIndex = definition.Attributes["use16BitStringTableIndex"] != null
                                              && definition.Attributes["use16BitStringTableIndex"].Value.ToLower() == "yes";

            foreach (var f in this.Fields)
            {
                if (f.DoesFieldContainObjectRef())
                {
                    this.ContainsObjectRef = true;
                }
                else if (f.Units == "WaveRef")
                {
                    this.ContainsWaveRef = true;
                }
                else if (f.Units == "BankRef")
                {
                    this.ContainsBankRef = true;
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether contains bank ref.
        /// </summary>
        public bool ContainsBankRef { get; private set; }

        /// <summary>
        /// Gets a value indicating whether contains object ref.
        /// </summary>
        public bool ContainsObjectRef { get; private set; }

        /// <summary>
        /// Gets a value indicating whether contains wave ref.
        /// </summary>
        public bool ContainsWaveRef { get; private set; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        public IFieldDefinition[] Fields { get; set; }

        /// <summary>
        /// Gets the group.
        /// </summary>
        public string Group { get; private set; }

        /// <summary>
        /// Gets the inherits from.
        /// </summary>
        public string InheritsFrom { get; private set; }

        /// <summary>
        /// Gets a value indicating whether is abstract.
        /// </summary>
        public bool IsAbstract { get; private set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets a value indicating whether dev only name table.
        /// </summary>
        public bool DevOnlyNameTable { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to delete container on reference delete.
        /// </summary>
        public bool SkipDeleteContainerOnRefDelete { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to use a 16 bit string table index
        /// </summary>
        public bool Use16BitStringTableIndex { get; private set; }

        /// <summary>
        /// Gets the type definitions.
        /// </summary>
        public ITypeDefinitions TypeDefinitions { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The find field definition.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <returns>
        /// The <see cref="FieldDefinition"/>.
        /// </returns>
        public IFieldDefinition FindFieldDefinition(string fieldName)
        {
            var fieldNameUpper = fieldName.ToUpper();
            return this.Fields.FirstOrDefault(f => f.Name.ToUpper() == fieldNameUpper);
        }

        /// <summary>
        /// The find object ref fields.
        /// </summary>
        /// <returns>
        /// The <see cref="FieldDefinition[]"/>.
        /// </returns>
        public IFieldDefinition[] FindObjectRefFields()
        {
            var al = new ArrayList();

            foreach (var f in this.Fields)
            {
                if (f.DoesFieldContainObjectRef())
                {
                    al.Add(f);
                }
            }

            return (IFieldDefinition[])al.ToArray(typeof(FieldDefinition));
        }

        /// <summary>
        /// The generate default xml node.
        /// </summary>
        /// <param name="parentDoc">
        /// The parent doc.
        /// </param>
        /// <param name="soundName">
        /// The sound name.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        public XmlNode GenerateDefaultXmlNode(XmlDocument parentDoc, string soundName)
        {
            XmlNode soundNode = parentDoc.CreateElement(this.Name);
            var nameAttr = parentDoc.CreateAttribute("name");
            nameAttr.Value = soundName;
            soundNode.Attributes.Append(nameAttr);

            foreach (var f in this.Fields)
            {
                f.GenerateDefaultXmlNode(soundNode);
            }

            return soundNode;
        }

        /// <summary>
        /// The get descendant types.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<ITypeDefinition> GetDescendantTypes()
        {
            if (this.m_descendantTypes == null)
            {
                this.m_descendantTypes = new List<ITypeDefinition> { this };
                foreach (var type in this.TypeDefinitions.ObjectTypes)
                {
                    var currentType = type;
                    while (!string.IsNullOrEmpty(currentType.InheritsFrom))
                    {
                        if (currentType.InheritsFrom == this.Name)
                        {
                            this.m_descendantTypes.Add(currentType);
                            break;
                        }

                        currentType = this.TypeDefinitions.FindTypeDefinition(currentType.InheritsFrom);
                    }
                }
            }

            return this.m_descendantTypes;
        }

        /// <summary>
        /// The get display groups.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<string> GetDisplayGroups()
        {
            var set = new HashSet<string>();
            foreach (var field in this.Fields)
            {
                if (!string.IsNullOrEmpty(field.DisplayGroup))
                {
                    set.Add(field.DisplayGroup);
                }
            }

            var groups = set.ToList();
            groups.Sort();
            return groups;
        }

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
