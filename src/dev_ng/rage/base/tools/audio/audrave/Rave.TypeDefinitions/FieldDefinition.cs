﻿// -----------------------------------------------------------------------
// <copyright file="FieldDefinition.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;

namespace Rave.TypeDefinitions
{
    using System;
    using System.Collections;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    /// <summary>
    /// The field definition.
    /// </summary>
    public abstract class FieldDefinition : IFieldDefinition
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldDefinition"/> class.
        /// </summary>
        /// <param name="definition">
        /// The definition.
        /// </param>
        /// <param name="typeDefinition">
        /// The type definition.
        /// </param>
        protected FieldDefinition(XmlNode definition, ITypeDefinition typeDefinition)
        {
            this.Name = this.GetAttributeValue(definition, "name");
            this.TypeName = this.GetAttributeValue(definition, "type");
            this.Units = this.GetAttributeValue(definition, "units");
	        this.Tagged = this.GetBooleanAttributeValue(definition, "tagged", false);
            this.DefaultValue = this.GetAttributeValue(definition, "default");
            this.Description = this.GetAttributeValue(definition, "description");
            this.DisplayGroup = this.GetAttributeValue(definition, "displayGroup");
            this.Visible = this.GetBooleanAttributeValue(definition, "visible", true);
            this.Ignore = this.GetBooleanAttributeValue(definition, "ignore", false);
            this.AllowOverrideControl = this.GetBooleanAttributeValue(definition, "allowOverrideControl", false);
            this.DefaultDropField = this.GetBooleanAttributeValue(definition, "defaultDropField", false);
            this.AllowedType = this.GetAttributeValue(definition, "allowedType");
            this.Node = definition;
            this.TypeDefinition = typeDefinition;
        }

	    public bool Tagged { get; private set; }

	    #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether allow override control.
        /// </summary>
        public bool AllowOverrideControl { get; private set; }

        /// <summary>
        /// Gets the allowed type.
        /// </summary>
        public string AllowedType { get; private set; }

        /// <summary>
        /// Gets a value indicating whether default drop field.
        /// </summary>
        public bool DefaultDropField { get; private set; }

        /// <summary>
        /// Gets the default value.
        /// </summary>
        public string DefaultValue { get; private set; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the display group.
        /// </summary>
        public string DisplayGroup { get; private set; }

        /// <summary>
        /// Gets a value indicating whether ignore.
        /// </summary>
        public bool Ignore { get; private set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the node.
        /// </summary>
        public XmlNode Node { get; private set; }

        /// <summary>
        /// Gets or sets the type definition.
        /// </summary>
        public ITypeDefinition TypeDefinition { get; set; }

        /// <summary>
        /// Gets the type name.
        /// </summary>
        public string TypeName { get; private set; }

        /// <summary>
        /// Gets the units.
        /// </summary>
        public string Units { get; private set; }

        /// <summary>
        /// Gets a value indicating whether visible.
        /// </summary>
        public bool Visible { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The parse field definitions.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="typeDefinition">
        /// The type definition.
        /// </param>
        /// <returns>
        /// The <see cref="FieldDefinition[]"/>.
        /// </returns>
        public static IFieldDefinition[] ParseFieldDefinitions(XmlNode parent, ITypeDefinition typeDefinition)
        {
            var fieldList = new ArrayList();
            foreach (XmlNode fieldNode in parent.ChildNodes)
            {
                if (fieldNode.GetType() != typeof(XmlComment))
                {
                    switch (fieldNode.Name)
                    {
                        case "CompositeField":
                            fieldList.Add(new CompositeFieldDefinition(fieldNode, typeDefinition));
                            break;
                        case "AllocatedSpaceField":
                        case "Field":
                            fieldList.Add(new SimpleFieldDefinition(fieldNode, typeDefinition));
                            break;
                        default:
                            throw new Exception("Unknown field type in SoundDefinitions.xml");
                    }
                }
            }

            return (FieldDefinition[])fieldList.ToArray(typeof(FieldDefinition));
        }

        /// <summary>
        /// The does field contain object ref.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool DoesFieldContainObjectRef()
        {
            return this.Units == "ObjectRef";
        }

        /// <summary>
        /// The generate default xml node.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        public abstract XmlNode GenerateDefaultXmlNode(XmlNode parentNode);

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get attribute value.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="valName">
        /// The val name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        protected string GetAttributeValue(XmlNode node, string valName)
        {
            if (node.Attributes[valName] != null)
            {
                return node.Attributes[valName].Value.Trim();
            }

            return null;
        }

	 
        /// <summary>
        /// The get boolean attribute value.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="valName">
        /// The val name.
        /// </param>
        /// <param name="defaultVal">
        /// The default val.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        protected bool GetBooleanAttributeValue(XmlNode node, string valName, bool defaultVal)
        {
            if (node.Attributes[valName] != null)
            {
                return node.Attributes[valName].Value == "yes";
            }

            return defaultVal;
        }

        /// <summary>
        /// The get float attribute value.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="valName">
        /// The val name.
        /// </param>
        /// <returns>
        /// The <see cref="float"/>.
        /// </returns>
        protected float GetFloatAttributeValue(XmlNode node, string valName)
        {
            if (node.Attributes[valName] != null)
            {
                return float.Parse(node.Attributes[valName].Value);
            }

            return 0;
        }

        /// <summary>
        /// The get integer attribute value.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="valName">
        /// The val name.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        protected int GetIntegerAttributeValue(XmlNode node, string valName)
        {
            if (node.Attributes[valName] != null)
            {
                return int.Parse(node.Attributes[valName].Value);
            }

            return 0;
        }

        #endregion
    }
}
