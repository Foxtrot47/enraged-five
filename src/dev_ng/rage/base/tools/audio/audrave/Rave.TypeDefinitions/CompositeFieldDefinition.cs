﻿// -----------------------------------------------------------------------
// <copyright file="CompositeFieldDefinition.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TypeDefinitions
{
    using System.Linq;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    /// <summary>
    /// The composite field definition.
    /// </summary>
    public class CompositeFieldDefinition : FieldDefinition, ICompositeFieldDefinition
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CompositeFieldDefinition"/> class.
        /// </summary>
        /// <param name="definition">
        /// The definition.
        /// </param>
        /// <param name="typeDefinition">
        /// The type definition.
        /// </param>
        public CompositeFieldDefinition(XmlNode definition, ITypeDefinition typeDefinition)
            : base(definition, typeDefinition)
        {
            this.MaxOccurs = this.GetIntegerAttributeValue(definition, "maxOccurs");
            if (this.MaxOccurs == 0)
            {
                // default to 1...(0 isnt valid anyway)
                this.MaxOccurs = 1;
            }

            this.Fields = ParseFieldDefinitions(definition, this.TypeDefinition);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the fields.
        /// </summary>
        public IFieldDefinition[] Fields { get; private set; }

        /// <summary>
        /// Gets the max occurs.
        /// </summary>
        public int MaxOccurs { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The does field contain object ref.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool DoesFieldContainObjectRef()
        {
            return this.Fields.Any(f => f.DoesFieldContainObjectRef());
        }

        /// <summary>
        /// The find field definition.
        /// </summary>
        /// <param name="fieldName">
        /// The field name.
        /// </param>
        /// <returns>
        /// The <see cref="FieldDefinition"/>.
        /// </returns>
        public IFieldDefinition FindFieldDefinition(string fieldName)
        {
            return this.Fields.FirstOrDefault(f => f.Name == fieldName);
        }

        /// <summary>
        /// The generate default xml node.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        public override XmlNode GenerateDefaultXmlNode(XmlNode parentNode)
        {
            return this.GenerateDefaultXmlNode(parentNode, false);
        }

        /// <summary>
        /// The generate default xml node.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="force">
        /// The force.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        public XmlNode GenerateDefaultXmlNode(XmlNode parentNode, bool force)
        {
            // only generate default xml if maxoccurs is one, ie its an old-school composite
            // field and not a new fangled repeating composite field.  makes it 
            // more intuitive for the user
            if (this.MaxOccurs == 1 || force)
            {
                XmlNode node = parentNode.OwnerDocument.CreateElement(this.Name);
                parentNode.AppendChild(node);

                foreach (var f in this.Fields)
                {
                    f.GenerateDefaultXmlNode(node);
                }

                return node;
            }

            return null;
        }

        #endregion
    }
}
