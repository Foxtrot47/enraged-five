﻿// -----------------------------------------------------------------------
// <copyright file="PedWikiDiff.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.PedWikiDiff
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    using HtmlAgilityPack;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;
    using global::Rave.Plugins.Infrastructure.Interfaces;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    /// Report that generates a list of discrepancies between peds
    /// on the wiki and the peds in the game.
    /// </summary>
    public class PedWikiDiff : IRAVEReportPlugin
    {
        /// <summary>
        /// List of ignore strings - if ped name starts with a value in
        /// this list it can be ignored during report generation.
        /// </summary>
        private readonly List<string> ignoreList = new List<string> { "a_c", "cs", "ig", "z_z" };

        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// The ped wiki uri.
        /// </summary>
        private string pedWikiUri;

        /// <summary>
        /// The ped game data path.
        /// </summary>
        private string pedGameDataDepotPath;

        /// <summary>
        /// The peds in wiki.
        /// </summary>
        private HashSet<string> pedsInWiki;

        /// <summary>
        /// The peds in game.
        /// </summary>
        private HashSet<string> pedsInGame;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private IAssetManager assetManager;

        /// <summary>
        /// Get the report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            this.pedsInWiki = new HashSet<string>();
            this.pedsInGame = new HashSet<string>();
            
            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;
                    
                    case "PedWikiUri":
                        this.pedWikiUri = setting.InnerText;
                        break;

                    case "PedGameDataDepotPath":
                        this.pedGameDataDepotPath = setting.InnerText;
                        break;
                }
            }

            if (string.IsNullOrEmpty(this.name))
            {
                throw new Exception("Report name required");
            }

            if (string.IsNullOrEmpty(this.pedGameDataDepotPath))
            {
                throw new Exception("Ped game data path required");
            }

            // Get instance of asset manager and request handler
            this.assetManager = AssetManagerFactory.GetInstance(AssetManagerType.Perforce);

            if (null == this.assetManager)
            {
                throw new Exception("Failed to get instance of Perforce asset manager");
            }

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes (not required).
        /// </param>
        /// <param name="browsers">
        /// The sound browsers (not required).
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            this.LoadPedWikiData();
            this.LoadPedGameData();
            this.DisplayResults();
        }

        /// <summary>
        /// Load the ped data contained in the wiki.
        /// </summary>
        private void LoadPedWikiData()
        {
            var doc = new HtmlWeb().Load(this.pedWikiUri);

            foreach (var nameNode in doc.DocumentNode.SelectNodes("//p[@class=\"name\"]"))
            {
                var pedName = nameNode.InnerText;
                if (!string.IsNullOrEmpty(pedName))
                {
                    this.pedsInWiki.Add(pedName.ToLower());
                }
            }
        }

        /// <summary>
        /// Load the ped data used by the game.
        /// </summary>
        private void LoadPedGameData()
        {
            // Make sure we have latest ped game data file
            if (!this.assetManager.GetLatestForceErrors(this.pedGameDataDepotPath))
            {
                throw new Exception("Failed to get latest ped game data: " + this.pedGameDataDepotPath);
            }

            var localPath = this.assetManager.GetLocalPath(this.pedGameDataDepotPath);

            if (string.IsNullOrEmpty(localPath))
            {
                throw new Exception("Failed to get local path for game file: " + this.pedGameDataDepotPath);
            }

            // Load the XML file
            var pedDoc = XDocument.Load(localPath);

            var modelInfoElem = pedDoc.Element("CPedModelInfo__InitDataList");
            var dataElem = modelInfoElem.Element("InitDatas");
            var pedElems = dataElem.Elements("Item");

            foreach (var pedElem in pedElems)
            {
                var typeElem = pedElem.Element("Pedtype");
                if (typeElem.Value.ToLower().Equals("animal"))
                {
                    // Okay to skip all animal peds
                    continue;
                }

                var nameElem = pedElem.Element("Name");
                var pedName = nameElem.Value.ToLower();

                // Skip if ped name starts with string in ignore list
                if (this.ignoreList.Any(pedName.StartsWith))
                {
                    continue;
                }

                this.pedsInGame.Add(nameElem.Value.ToLower());
            }
        }

        /// <summary>
        /// Display the results to the user.
        /// </summary>
        private void DisplayResults()
        {
            var pedsInWikiList = this.pedsInWiki.ToList();
            pedsInWikiList.Sort();

            var pedsInGameList = this.pedsInGame.ToList();
            pedsInGameList.Sort();

            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Ped in Wiki / Game Diff</title></head><body>");

            // Write totals
            sw.WriteLine("<h2>Totals</h2>");
            sw.WriteLine("<ul>");
            sw.WriteLine("<li>Peds in Wiki: " + pedsInWikiList.Count + "</li>");
            sw.WriteLine("<li>Peds in Game: " + pedsInGameList.Count + "</li>");
            sw.WriteLine("</ul>");

            // Write names of peds in game but not in wiki
            sw.WriteLine("<h2>Peds in Game but not in Wiki</h2>");

            var pedsInGameNotWiki = pedsInGameList.Where(pedName => !pedsInWikiList.Contains(pedName)).ToList();

            if (pedsInGameNotWiki.Any())
            {
                sw.WriteLine("<p>Count: " + pedsInGameNotWiki.Count + "</p>");

                sw.WriteLine("<ul>");
                foreach (var pedName in pedsInGameNotWiki)
                {
                    sw.WriteLine("<li>" + pedName + "</li>");
                }

                sw.WriteLine("</ul>");
            }
            else
            {
                sw.WriteLine("<p>None</p>");
            }

            // Write names of peds in wiki but not in game
            sw.WriteLine("<h2>Peds in Wiki but not in Game</h2>");

            var pedsInWikiNotGame = pedsInWikiList.Where(pedName => !pedsInGameList.Contains(pedName)).ToList();

            if (pedsInWikiNotGame.Any())
            {
                sw.WriteLine("<p>Count: " + pedsInWikiNotGame.Count + "</p>");

                sw.WriteLine("<ul>");
                foreach (var pedName in pedsInWikiNotGame)
                {
                    sw.WriteLine("<li>" + pedName + "</li>");
                }

                sw.WriteLine("</ul>");
            }
            else
            {
                sw.WriteLine("<p>None</p>");
            }

            // Inform user of peds that have been ignored during report generation
            sw.WriteLine("<h2>Ignore List</h2>");
            sw.WriteLine("<p>Ignoring all peds of type 'animal' and peds with name starting with:");
            sw.WriteLine("<ul>");
            foreach (var value in this.ignoreList)
            {
                sw.WriteLine("<li>" + value + "</li>");
            }

            sw.WriteLine("</ul></p>");

            sw.WriteLine("</body></html>");

            sw.Close();
            filesteam.Close();

            // Display report
            var report = new frmReport(this.name + " Report");
            report.SetURL(resultsFileName);
            report.Show();
        }
    }
}
