namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.PropertiesEditor.Infrastructure.Interfaces;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Waves;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    internal class ctrlTemplateWaveRefCell : DataGridViewLinkCell,
                                             ITemplateFieldCell,
                                             IWaveRef
    {
        private string m_alias;
        private string m_bankPath;
        private IFieldDefinition m_field;
        private XmlNode m_fieldNode;

        private IObjectInstance m_objectInstance;
        private XmlNode m_parentNode;
        private XmlNode m_templateFieldNode;
        private XmlNode m_templateParentNode;
        private string m_waveName;

        #region ITemplateFieldCell Members

        public bool OverridesTemplate
        {
            get { return (this.m_fieldNode != null && this.m_fieldNode.InnerText != this.m_templateFieldNode.InnerText); }
        }

        public bool IsDefaultValue
        {
            get
            {
                return (this.m_fieldNode == null && this.m_templateFieldNode == null ||
                        this.m_fieldNode == null && this.m_templateFieldNode.InnerText == this.m_field.DefaultValue ||
                        this.m_fieldNode != null && this.m_fieldNode.InnerText == this.m_field.DefaultValue);
            }
        }

        public TemplateCellType Type
        {
            get { return TemplateCellType.WAVEREF; }
        }

        #endregion

        #region IWaveRef Members

        public void Drop(WaveNode wn)
        {
            if (!this.m_objectInstance.IsReadOnly)
            {
                if (wn != null)
                {
                    var oldKey = this.m_bankPath + "\\" + this.m_waveName;
                    if (WaveManager.WaveRefs.ContainsKey(oldKey))
                    {
                        WaveManager.WaveRefs[oldKey].RemoveAll(x => x.Name == this.m_objectInstance.Name);
                    }

                    this.m_bankPath = wn.GetBankPath();
                    this.m_waveName = wn.GetWaveName();

                    this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);

                    if (this.m_fieldNode == null)
                    {
                        var e = this.m_objectInstance.Bank.Document.CreateElement(this.m_alias);
                        this.m_objectInstance.Node.AppendChild(e);
                        this.m_fieldNode = e;
                    }
                    if (!this.m_fieldNode.HasChildNodes)
                    {
                        // create elements for wave/bank name
                        XmlNode n = this.m_fieldNode.OwnerDocument.CreateElement("WaveName");
                        n.InnerText = this.m_waveName;
                        this.m_fieldNode.AppendChild(n);

                        n = this.m_fieldNode.OwnerDocument.CreateElement("BankName");
                        n.InnerText = this.m_bankPath;
                        this.m_fieldNode.AppendChild(n);
                    }
                    else
                    {
                        if (this.m_fieldNode.ChildNodes[0].Name == "WaveName")
                        {
                            this.m_fieldNode.ChildNodes[0].InnerText = this.m_waveName;
                            this.m_fieldNode.ChildNodes[1].InnerText = this.m_bankPath;
                        }
                        else
                        {
                            this.m_fieldNode.ChildNodes[1].InnerText = this.m_waveName;
                            this.m_fieldNode.ChildNodes[0].InnerText = this.m_bankPath;
                        }
                    }

                    this.Value = this.m_waveName;
                    this.ToolTipText = this.m_bankPath + "\\" + this.m_waveName;

                    var newKey = this.m_bankPath + "\\" + this.m_waveName;
                    if (!WaveManager.WaveRefs.ContainsKey(newKey))
                    {
                        WaveManager.WaveRefs.Add(newKey, new List<IObjectInstance>());
                    }
                    WaveManager.WaveRefs[newKey].Add(this.m_objectInstance);
                    this.m_objectInstance.MarkAsDirty();
                }
            }
        }

        #endregion

        public event Action<string, string> OnWaveRefClick;

        public void Init(ITemplate template,
                         IObjectInstance obj,
                         ITemplateInstance templateObject,
                         XmlNode parent,
                         XmlNode templateparent,
                         IFieldDefinition field)
        {
            this.m_objectInstance = obj;
            this.m_parentNode = parent;
            this.m_templateParentNode = templateparent;

            this.m_field = field;
            this.m_alias = string.Empty;
            this.m_fieldNode = null;

            this.m_templateFieldNode = PropertyEditorUtil.FindXmlNode(field.Name, this.m_templateParentNode);
            if (this.m_templateFieldNode != null &&
                this.m_templateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_alias = this.m_templateFieldNode.Attributes["ExposeAs"].Value;
            }

            if (this.m_alias != string.Empty)
            {
                this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            }

            XmlNode fieldNode = null;
            if (this.m_fieldNode != null)
            {
                fieldNode = this.m_fieldNode;
            }
            if (fieldNode == null &&
                this.m_templateFieldNode != null)
            {
                fieldNode = this.m_templateFieldNode;
            }

            if (fieldNode == null ||
                !fieldNode.HasChildNodes)
            {
                this.Value = "not set";
            }
            else
            {
                if (fieldNode.ChildNodes[0].Name == "WaveName")
                {
                    this.m_waveName = fieldNode.ChildNodes[0].InnerText;
                    this.m_bankPath = fieldNode.ChildNodes[1].InnerText;
                }
                else
                {
                    this.m_waveName = fieldNode.ChildNodes[1].InnerText;
                    this.m_bankPath = fieldNode.ChildNodes[0].InnerText;
                }

                this.Value = this.m_waveName;
                this.ToolTipText = this.m_bankPath + "\\" + this.m_waveName;
            }

            if (!obj.IsReadOnly)
            {
                var menu = new ContextMenuStrip();
                menu.Items.Add("Reset", null, this.On_Remove);
                this.ContextMenuStrip = menu;
            }
        }

        private void On_Remove(object sender, EventArgs e)
        {
            if (this.m_fieldNode.HasChildNodes)
            {
                this.m_fieldNode.RemoveAll();

                this.m_bankPath = null;
                this.m_waveName = null;
                this.Value = "not set";
                this.ToolTipText = "";
                this.RaiseCellValueChanged(new DataGridViewCellEventArgs(this.ColumnIndex, this.RowIndex));
                this.m_objectInstance.MarkAsDirty();
            }
        }

        protected override void OnClick(DataGridViewCellEventArgs e)
        {
            base.OnClick(e);
            if (this.OnWaveRefClick != null)
            {
                this.OnWaveRefClick(this.m_bankPath, this.m_waveName);
            }
        }

        protected override void Dispose(bool disposing)
        {
            this.m_objectInstance = null;
            this.m_parentNode = null;
            this.m_fieldNode = null;
            this.m_field = null;
            base.Dispose(disposing);
        }
    }
}