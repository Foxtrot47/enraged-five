    namespace Rave.PropertiesEditor.CustomNodes
    {
        partial class ctrlExpose
        {
            /// <summary> 
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary> 
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (this.components != null))
                {
                    this.components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Component Designer generated code

            /// <summary> 
            /// Required method for Designer support - do not modify 
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
            this.ctrlCheck = new System.Windows.Forms.CheckBox();
            this.txtAlias = new System.Windows.Forms.TextBox();
            this.lblExpose = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ctrlCheck
            // 
            this.ctrlCheck.AutoSize = true;
            this.ctrlCheck.Location = new System.Drawing.Point(3, 5);
            this.ctrlCheck.Name = "ctrlCheck";
            this.ctrlCheck.Size = new System.Drawing.Size(15, 14);
            this.ctrlCheck.TabIndex = 0;
            this.ctrlCheck.UseVisualStyleBackColor = true;
            // 
            // txtAlias
            // 
            this.txtAlias.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAlias.Location = new System.Drawing.Point(80, 2);
            this.txtAlias.Name = "txtAlias";
            this.txtAlias.Size = new System.Drawing.Size(145, 20);
            this.txtAlias.TabIndex = 1;
            // 
            // lblExpose
            // 
            this.lblExpose.AutoSize = true;
            this.lblExpose.Location = new System.Drawing.Point(20, 6);
            this.lblExpose.Name = "lblExpose";
            this.lblExpose.Size = new System.Drawing.Size(59, 13);
            this.lblExpose.TabIndex = 2;
            this.lblExpose.Text = "Expose as:";
            // 
            // ctrlExpose
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblExpose);
            this.Controls.Add(this.txtAlias);
            this.Controls.Add(this.ctrlCheck);
            this.MaximumSize = new System.Drawing.Size(1024, 24);
            this.Name = "ctrlExpose";
            this.Size = new System.Drawing.Size(228, 24);
            this.ResumeLayout(false);
            this.PerformLayout();

            }

            #endregion

            private System.Windows.Forms.CheckBox ctrlCheck;
            private System.Windows.Forms.TextBox txtAlias;
            private System.Windows.Forms.Label lblExpose;
        }
    }

