namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    internal class NumericEditingControl : NumericEditor,
                                           IDataGridViewEditingControl
    {
        private string m_alias;
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_objectInstance;
        private XmlNode m_parentNode;
        private bool m_valueChanged;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.m_numeric.Value.ToString(); }
            set
            {
                if (value is String)
                {
                    this.m_numeric.Value = Int32.Parse(value.ToString());
                }
            }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
            this.m_numeric.TextAlign = ctrlNumericCell.TranslateAlignment(dataGridViewCellStyle.Alignment);
            this.BackColor = Color.White;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged
        {
            get { return this.m_valueChanged; }
            set { this.m_valueChanged = value; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        internal void Init(IObjectInstance objectInstance,
                           XmlNode parentNode,
                           IFieldDefinitionInstance fieldInstance,
                           string fieldAlias)
        {
            this.m_numeric.DecimalPlaces = 8;
            this.m_numeric.Increment = 0.01m;
            this.m_objectInstance = objectInstance;
            this.m_parentNode = parentNode;
            this.m_fieldInstance = fieldInstance;
            this.m_alias = fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);

            var simpleField = this.m_fieldInstance.AsSimpleField();
            if (simpleField.Max !=
                simpleField.Min)
            {
                this.m_numeric.Minimum = (decimal) simpleField.Min;
                this.m_numeric.Maximum = (decimal) simpleField.Max;
            }
            else
            {
                this.m_numeric.Minimum = decimal.MinValue;
                this.m_numeric.Maximum = decimal.MaxValue;
            }

            if (this.m_fieldNode != null)
            {
                this.m_numeric.Value = Decimal.Parse(this.m_fieldNode.InnerText);
            }
            else
            {
                this.m_numeric.Value = this.m_fieldInstance.DefaultValue != null ? Decimal.Parse(this.m_fieldInstance.DefaultValue) : 0;
            }
            this.m_units.Text = fieldInstance.Units;
            this.m_units.Visible = !string.IsNullOrEmpty(fieldInstance.Units);
			m_numeric.Select(0, m_numeric.Text.Length);
        }

        public void UpdateNode()
        {
            if (!this.m_objectInstance.IsReadOnly)
            {
                var fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
                if (fieldNode == null)
                {
                    fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_alias);
                    this.m_parentNode.AppendChild(fieldNode);
                }

                fieldNode.InnerText = this.m_numeric.Value.ToString();
                this.m_objectInstance.MarkAsDirty();
                this.m_valueChanged = true;
                this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.UpdateNode();
                return false;
            }
            if (!string.IsNullOrEmpty(this.m_numeric.Text) && keyData == (Keys.Control | Keys.C))
            {
                Clipboard.SetText(this.m_numeric.Text);
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}