namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [DesignerCategory("code")]
    public class ctrlBlankCell : DataGridViewTextBoxCell
    {
        public override Type EditType
        {
            get
            {
                //no Editor, read only
                return null;
            }
        }

        public override Type ValueType
        {
            get { return typeof (string); }
        }

        public override object DefaultNewRowValue
        {
            get { return ""; }
        }
    }
}