// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmClipboardEditor.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The clipboard editor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Runtime.InteropServices;
using System.Windows.Controls.Primitives;

namespace Rave.PropertiesEditor.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;
    using System.Windows.Forms.Integration;
    using System.Xml;

    using Rave.Instance;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.PropertiesEditor;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Objects;
    using Rave.Types.Objects.TypedObjects;

    /// <summary>
    /// The clipboard editor.
    /// </summary>
    public class frmClipboardEditor : Form
    {
        #region Fields

        /// <summary>
        /// The 'is multi edit' flag.
        /// </summary>
        private bool isMultiEdit;

        /// <summary>
        /// The object (when editing a single object).
        /// </summary>
        private IObjectInstance objectInstance;

        /// <summary>
        /// The object list.
        /// </summary>
        private List<IObjectInstance> objectList;

        /// <summary>
        /// Flag indicating if pending changes exist.
        /// </summary>
        private bool isDirty;

        /// <summary>
        /// The object list text box.
        /// </summary>
        private TextBox objectListTextBox;

        /// <summary>
        /// The properties editor host.
        /// </summary>
        private ElementHost propertiesEditorHost;

        /// <summary>
        /// The close button.
        /// </summary>
        private Button closeButton;

        /// <summary>
        /// The apply button.
        /// </summary>
        private Button applyButton;

        /// <summary>
        /// The properties editor.
        /// </summary>
        private PropertiesEditor propertiesEditor;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="frmClipboardEditor"/> class.
        /// </summary>
        /// <param name="pluginManager">
        /// The plugin manager.
        /// </param>
        public frmClipboardEditor()
        {
            this.InitializeComponent();
            this.Init();

            this.FormClosing += this.OnClose;
        }

        #endregion

        /// <summary>
        /// Initialize the clipboard editor for a single object.
        /// </summary>
        /// <param name="objInstance">
        /// The object instance.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Init(IObjectInstance objInstance)
        {
            this.objectInstance = objInstance;
            this.propertiesEditor.EditObject(this.objectInstance);
            return true;
        }

        /// <summary>
        /// Initialize the clipboard editor with a list of object instances for multi edit.
        /// </summary>
        /// <param name="objInstances">
        /// The object instances.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Init(List<IObjectInstance> objInstances)
        {
            if (objInstances.Count == 0)
            {
                return false;
            }
            
            var objects = new Dictionary<IObjectBank, IList<string>>();

            // Checkout bank(s)
            var checkoutRequired = objInstances.Any(objInstance => !objInstance.Bank.IsCheckedOut);
            if (checkoutRequired)
            {
                if (MessageBox.Show(
                    RaveInstance.ActiveWindow,
                    "One or more banks must be checked out - continue?",
                    "Bank Checkout Required",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question) == DialogResult.Cancel)
                {
                    return false;
                }

                // Keep track of which objects we are editing
                foreach (var currObj in objInstances)
                {
                    if (!objects.ContainsKey(currObj.Bank))
                    {
                        objects[currObj.Bank] = new List<string>();
                    }

                    objects[currObj.Bank].Add(currObj.Name);
                }

                // Checkout the object bank(s)
                foreach (var objInstance in objInstances)
                {
                    var bank = objInstance.Bank;
                    if (!bank.IsCheckedOut)
                    {
                        if (!bank.Checkout(false))
                        {
                            return false;
                        }
                    }
                }

                // Get updated objects from banks (required because bank may have been reloaded)
                this.objectList = new List<IObjectInstance>();
                foreach (var kvp in objects)
                {
                    var bank = kvp.Key;
                    var objNames = kvp.Value;
                    foreach (var objName in objNames)
                    {
                        var currObj = bank.FindObjectInstance(objName);
                        if (null == currObj)
                        {
                            throw new Exception("Failed to find object after bank reloaded: " + objName);
                        }

                        this.objectList.Add(currObj);
                    }
                }
            }
            else
            {
                this.objectList = objInstances;
            }
            
            this.isMultiEdit = true;
            this.Text = "Multi-Object Editor";
            var obj = this.objectList[0];

            ObjectInstance.ObjectModified += this.OnObjectModified;

            // Create template fieldInstance from type definitions
            var node = CreateTemplateObject(obj);
            this.objectInstance = new TypedObjectInstance(node, obj.Bank, obj.Type, obj.Episode, obj.ObjectLookup, obj.TypeDefinitions);

            // Display template
            this.propertiesEditor.EditObject(this.objectInstance, Mode.MULTIEDIT);

            // Show object name(s)
            this.objectListTextBox.Clear();
            this.objectListTextBox.AppendText(string.Join(", ", this.objectList.Select(o => o.Name)));

            return true;
        }

        #region Methods

        /// <summary>
        /// The create template object.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <returns>
        /// The object template node <see cref="XmlNode"/>.
        /// </returns>
        private static XmlNode CreateTemplateObject(IObjectInstance objectInstance)
        {
            // find base type
            ITypeDefinition baseType = null;
            var type = objectInstance.TypeName;
            var inheritsFrom = type;

            while (inheritsFrom != null)
            {
                baseType = objectInstance.TypeDefinitions.FindTypeDefinition(inheritsFrom);
                inheritsFrom = baseType.InheritsFrom;
            }

            XmlNode template = objectInstance.Node.OwnerDocument.CreateElement(type);
            var attr = objectInstance.Node.OwnerDocument.CreateAttribute("name");
            attr.Value = "Template Sound";
            template.Attributes.Append(attr);

            foreach (var fd in baseType.Fields)
            {
                fd.GenerateDefaultXmlNode(template);
            }

            return template;
        }

        /// <summary>
        /// The copy node settings.
        /// </summary>
        /// <param name="baseNode">
        /// The base node.
        /// </param>
        /// <param name="targetNode">
        /// The target node.
        /// </param>
        private static void CopyNodeSettings(XmlNode baseNode, XmlNode targetNode)
        {
            foreach (XmlNode baseChild in baseNode.ChildNodes)
            {
                if (baseChild.NodeType == XmlNodeType.Text)
                {
                    return;
                }

                var found = false;

                // Check that an equivalent node exists in the targetNode children
                foreach (XmlNode targetChild in targetNode.ChildNodes)
                {
                    // Equivalent node in the target and has apply setting attribute
                    if (baseChild.Name == targetChild.Name
                        && (baseChild.Attributes["applySetting"] != null
                            && baseChild.Attributes["applySetting"].Value == "yes"))
                    {
                        targetChild.InnerXml = baseChild.InnerXml;
                        foreach (XmlAttribute attribute in baseChild.Attributes)
                        {
                            // Skip the apply settings attribute
                            if (attribute.Name == "applySetting")
                            {
                                continue;
                            }

                            var newAttribute = (XmlAttribute)attribute.Clone();

                            // Overrides attribute if it exists already
                            ((XmlElement)targetChild).SetAttributeNode(newAttribute);
                        }

                        found = true;
                    }
                    else if (baseChild.Name == targetChild.Name)
                    {
                        // Equivalent node in the target but no apply setting attribute. Check children as could be composite field
                        found = true;
                        CopyNodeSettings(baseChild, targetChild);
                    }
                }
                
                if (found || baseChild.Attributes["applySetting"] == null
                    || baseChild.Attributes["applySetting"].Value.ToLower() != "yes")
                {
                    continue;
                }

                // Node doesn't exist as a child of the target node so create it using default values
                var newNode = targetNode.OwnerDocument.CreateElement(baseChild.Name);
                newNode.InnerXml = baseChild.InnerXml;
                foreach (XmlAttribute attribute in baseChild.Attributes)
                {
                    // Skip the apply settings attribute
                    if (attribute.Name == "applySetting")
                    {
                        continue;
                    }

                    var newAttribute = (XmlAttribute)attribute.Clone();
                    newNode.SetAttributeNode(newAttribute);
                }

                targetNode.AppendChild(newNode);

                // Then need to check children as could be a composite field
                CopyNodeSettings(baseChild, newNode);
            }
        }

        /// <summary>
        /// Initialize remaining elements.
        /// </summary>
        private void Init()
        {
            // We initialize properties editor here so that designer view still works
            this.propertiesEditor = new PropertiesEditor
            {
                AllowDrop = true,
                Name = "propertiesEditor",
                TabIndex = 6
            };
            this.propertiesEditorHost.Child = this.propertiesEditor;
        }

        /// <summary>
        /// Apply the settings to all objects being edited.
        /// </summary>
        private void ApplySettings()
        {
            foreach (var obj in this.objectList)
            {
                // Make sure we've got the up to date version of this object
                var destName = obj.Name;
                var bank = obj.Bank;
                var currObj = bank.FindObjectInstance(destName);

                // Copy node settings to this object's node
                CopyNodeSettings(this.objectInstance.Node, currObj.Node);
                obj.MarkAsDirty();
                obj.ComputeReferences();
            }
			
			MessageBox.Show("Settings have been applied", "Multi-Edit", MessageBoxButtons.OK);
		
            this.isDirty = false;
			this.Close();
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmClipboardEditor));
            this.objectListTextBox = new System.Windows.Forms.TextBox();
            this.propertiesEditorHost = new System.Windows.Forms.Integration.ElementHost();
            this.closeButton = new System.Windows.Forms.Button();
            this.applyButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // objectListTextBox
            // 
            this.objectListTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.objectListTextBox.BackColor = System.Drawing.SystemColors.Menu;
            this.objectListTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.objectListTextBox.Location = new System.Drawing.Point(12, 12);
            this.objectListTextBox.Multiline = true;
            this.objectListTextBox.Name = "objectListTextBox";
            this.objectListTextBox.Size = new System.Drawing.Size(506, 36);
            this.objectListTextBox.TabIndex = 0;
            // 
            // propertiesEditorHost
            // 
            this.propertiesEditorHost.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertiesEditorHost.BackColorTransparent = true;
            this.propertiesEditorHost.Location = new System.Drawing.Point(12, 54);
            this.propertiesEditorHost.Name = "propertiesEditorHost";
            this.propertiesEditorHost.Size = new System.Drawing.Size(506, 565);
            this.propertiesEditorHost.TabIndex = 0;
            this.propertiesEditorHost.Child = null;
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.Location = new System.Drawing.Point(443, 630);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 1;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // applyButton
            // 
            this.applyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.applyButton.Location = new System.Drawing.Point(362, 630);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(75, 23);
            this.applyButton.TabIndex = 2;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // frmClipboardEditor
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(530, 665);
            this.Controls.Add(this.applyButton);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.propertiesEditorHost);
            this.Controls.Add(this.objectListTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmClipboardEditor";
            this.Text = "Clipboard Object Editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void OnObjectModified(IObjectInstance objectInstance)
        {
            if (objectInstance == this.objectInstance)
            {
                this.isDirty = true;
            }
        }

        /// <summary>
        /// The 'on close' event handler method.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void OnClose(object sender, FormClosingEventArgs e)
        {
            if (!this.isMultiEdit)
            {
                return;
            }

            if (this.isDirty)
            {
                var result = MessageBox.Show(
                    RaveInstance.ActiveWindow,
                    "Apply pending changes?",
                    "Pending Changes",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question);

                if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    return;
                }

                if (result == DialogResult.Yes)
                {
                    this.ApplySettings();
                }
            }

            ObjectInstance.ObjectModified -= this.OnObjectModified;
        }


        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);
        private const int WM_SETREDRAW = 11; 


        protected override void OnResize(EventArgs e)
        {
            SendMessage(this.Handle, WM_SETREDRAW, false, 0);
            base.OnResize(e);
            SendMessage(this.Handle, WM_SETREDRAW, true, 0);
            this.Refresh();
        }


        #endregion

        /// <summary>
        /// Apply current changes.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void applyButton_Click(object sender, EventArgs e)
        {
            this.ApplySettings();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}