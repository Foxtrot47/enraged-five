namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    public class ctrlCheckBoxCell : DataGridViewCheckBoxCell,
                                    IFieldCell
    {
        private string m_alias;
        private CheckBoxEditor m_editor;
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_objectInstance;
        private XmlNode m_parentNode;

        public override Type EditType
        {
            get { return typeof (CheckBoxEditor); }
        }

        public override Type ValueType
        {
            get { return typeof (CheckState); }
        }

        public override object DefaultNewRowValue
        {
            get { return false; }
        }

        #region IFieldCell Members

        public bool IsDefaultValue
        {
            get
            {
                this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
                return (this.m_fieldNode == null || this.m_fieldInstance.DefaultValue == this.m_fieldNode.InnerText);
            }
        }

        public CellType Type
        {
            get { return CellType.CHECKBOX; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance,
                         XmlNode parent,
                         IFieldDefinitionInstance fieldInstance,
                         string fieldAlias)
        {
            this.m_objectInstance = objectInstance;
            this.m_parentNode = parent;
            this.m_fieldInstance = fieldInstance;
            this.m_alias = string.IsNullOrEmpty(fieldAlias) ? fieldInstance.Name : fieldAlias;
            this.Value = this.GetValue();
        }
        
       private CheckState GetValue()
        {
            var fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
       
        
            if (this.m_fieldInstance.TypeName == "tristate")
            {
                this.ThreeState = true;
                if (fieldNode == null)
                {
                    return CheckBoxHelper.GetIndeterminateValue(m_fieldInstance.DefaultValue);
                }
            
                if (fieldNode.InnerText == "yes")
                {
                    return CheckState.Checked;
                }

                return CheckState.Unchecked;
            }

            this.ThreeState = false;
            if (fieldNode == null)
            {
                if (this.m_fieldInstance.DefaultValue == "yes")
                {
                    return CheckState.Checked;
                }

                return CheckState.Unchecked;
            }

            return fieldNode.InnerText == "yes" ? 
                       CheckState.Checked : CheckState.Unchecked;
        }

        public override void InitializeEditingControl(int rowIndex,
                                                      object initialFormattedValue,
                                                      DataGridViewCellStyle dataGridViewCellStyle)
        {
            // Set the value of the editing control to the current cell value.
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
            this.m_editor = this.DataGridView.EditingControl as CheckBoxEditor;
            this.m_editor.Init(this.m_objectInstance, this.m_parentNode, this.m_fieldInstance, this.m_alias);
        }

        protected override void Paint(Graphics graphics,
                                      Rectangle clipBounds,
                                      Rectangle cellBounds,
                                      int rowIndex,
                                      DataGridViewElementStates cellState,
                                      object value,
                                      object formattedValue,
                                      string errorText,
                                      DataGridViewCellStyle cellStyle,
                                      DataGridViewAdvancedBorderStyle advancedBorderStyle,
                                      DataGridViewPaintParts paintParts)
        {
            this.Value = this.GetValue();
            base.Paint(graphics,
                clipBounds,
                cellBounds,
                rowIndex,
                cellState,
                value,
                formattedValue,
                errorText,
                cellStyle,
                advancedBorderStyle,
                paintParts);
        }

        public override void PositionEditingControl(bool setLocation,
                                                    bool setSize,
                                                    Rectangle cellBounds,
                                                    Rectangle cellClip,
                                                    DataGridViewCellStyle cellStyle,
                                                    bool singleVerticalBorderAdded,
                                                    bool singleHorizontalBorderAdded,
                                                    bool isFirstDisplayedColumn,
                                                    bool isFirstDisplayedRow)
        {
            var editingControlBounds = this.PositionEditingPanel(cellBounds,
                cellClip,
                cellStyle,
                singleVerticalBorderAdded,
                singleHorizontalBorderAdded,
                isFirstDisplayedColumn,
                isFirstDisplayedRow);

            editingControlBounds = this.GetAdjustedEditingControlBounds(editingControlBounds, cellStyle);
            this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
            this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
        }

        private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
        {
            // Add a 1 pixel padding on the left and right of the editing control
            editingControlBounds.X += 1;
            editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);

            // Adjust the vertical location of the editing control:
            var preferredHeight = this.Size.Height;
            if (preferredHeight < editingControlBounds.Height)
            {
                switch (cellStyle.Alignment)
                {
                    case DataGridViewContentAlignment.MiddleLeft:
                    case DataGridViewContentAlignment.MiddleCenter:
                    case DataGridViewContentAlignment.MiddleRight:
                        editingControlBounds.Y += (editingControlBounds.Height - preferredHeight) / 2;
                        break;
                    case DataGridViewContentAlignment.BottomLeft:
                    case DataGridViewContentAlignment.BottomCenter:
                    case DataGridViewContentAlignment.BottomRight:
                        editingControlBounds.Y += editingControlBounds.Height - preferredHeight;
                        break;
                }
            }

            return editingControlBounds;
        }

        public override void DetachEditingControl()
        {
            this.m_editor.UpdateNode();
            base.DetachEditingControl();
            this.Value = this.GetValue();
        }

        protected override void Dispose(bool disposing)
        {
            this.m_objectInstance = null;
            this.m_parentNode = null;
            this.m_fieldNode = null;
            this.m_fieldInstance = null;
            this.m_editor = null;
            base.Dispose(disposing);
        }
    }
}