// --------------------------------------------------------------------------------------------------------------------
// <copyright file="tbReferenceTab.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The tb reference tab.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.PropertiesEditor
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;

    /// <summary>
    /// The tb reference tab.
    /// </summary>
    internal class tbReferenceTab : TabPage
    {
        #region Fields

        /// <summary>
        /// The m_ list box.
        /// </summary>
        private ListBox m_ListBox;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="tbReferenceTab"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public tbReferenceTab(string name)
            : base(name)
        {
            this.InitializeComponent();
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The on object ref click.
        /// </summary>
        public event Action<IObjectInstance> OnObjectRefClick;

        /// <summary>
        /// The on template ref click.
        /// </summary>
        public event Action<ITemplate> OnTemplateRefClick;

        #endregion

        #region Enums

        /// <summary>
        /// The object reference search.
        /// </summary>
        public enum ObjectReferenceSearch
        {
            /// <summary>
            /// The top level.
            /// </summary>
            TopLevel, 

            /// <summary>
            /// The direct only.
            /// </summary>
            DirectOnly, 
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <param name="search">
        /// The search.
        /// </param>
        public void Init(IObjectInstance obj, ObjectReferenceSearch search)
        {
            this.m_ListBox.SuspendLayout();
            this.m_ListBox.Items.Clear();
            if (search == ObjectReferenceSearch.TopLevel)
            {
                var list = ResolveTopLevelParentSounds(obj);
                foreach (var item in list)
                {
                    this.m_ListBox.Items.Add(item);
                }
	        }
            else if (search == ObjectReferenceSearch.DirectOnly)
            {
                foreach (var referencer in obj.Referencers)
                {
                    this.m_ListBox.Items.Add(referencer);
                }
            }
			this.m_ListBox.Sorted = true;
            this.m_ListBox.ResumeLayout(true);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The resolve top level parent sounds.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <returns>
        /// The <see cref="List{T}"/>.
        /// </returns>
        private static List<IObjectInstance> ResolveTopLevelParentSounds(IObjectInstance objectInstance)
        {
            var currentLevel = new List<IReferencable>();

            currentLevel.AddRange(objectInstance.Referencers);

            var cont = true;
            while (cont)
            {
                cont = false;
                var nextLevel = new List<IReferencable>();
                foreach (var currObject in currentLevel)
                {
                    if (null == currObject)
                    {
                        continue;
                    }

                    if (currObject.Referencers.Count > 0)
                    {
                        cont = true;
                        nextLevel.AddRange(currObject.Referencers);
                    }
                    else
                    {
                        nextLevel.Add(currObject);
                    }
                }

                if (cont)
                {
                    currentLevel = nextLevel;
                }
            }

            var al = new List<IObjectInstance>();

            foreach (var s in currentLevel)
            {
                var objInstance = s as IObjectInstance;
                if (null != objInstance && !al.Contains(objInstance))
                {
                    al.Add(objInstance);
                }
            }

            return al;
        }

        /// <summary>
        /// The initialize component.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_ListBox = new ListBox();
            this.SuspendLayout();

            // m_ListBox
            this.m_ListBox.Dock = DockStyle.Fill;
            this.m_ListBox.FormattingEnabled = true;
            this.m_ListBox.Location = new System.Drawing.Point(0, 0);
            this.m_ListBox.Name = "m_ListBox";
            this.m_ListBox.Size = new System.Drawing.Size(200, 100);
            this.m_ListBox.TabIndex = 0;

            this.m_ListBox.DoubleClick += this.m_ListBox_DoubleClick;

            // tbReferenceTab
            this.Controls.Add(this.m_ListBox);
            this.ResumeLayout(false);
        }

        /// <summary>
        /// The m_ list box_ double click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void m_ListBox_DoubleClick(object sender, EventArgs e)
        {
            if (this.OnObjectRefClick != null)
            {
                var listBox = (ListBox)sender;
                var objectInstance = listBox.SelectedItem as IObjectInstance;
                var template = listBox.SelectedItem as ITemplate;

                if (null != objectInstance)
                {
                    this.OnObjectRefClick(objectInstance);
                }
                else if (null != template)
                {
                    this.OnTemplateRefClick(template);
                }
            }
        }

        #endregion
    }
}