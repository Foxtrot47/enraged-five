using Rave.PropertiesEditor.CustomNodes;

namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;

    [DesignerCategory("code")]
    internal class TemplateCheckBoxEditor : CheckBox,
                                            IDataGridViewEditingControl
    {
        private string m_Alias;
        private IFieldDefinition m_Field;
        private XmlNode m_FieldNode;
        private ContextMenuStrip m_Menu;
        private ITypedObjectInstance m_Object;

        private XmlNode m_ParentNode;
        private ITemplate m_Template;

        private XmlNode m_TemplateFieldNode;
        private ITypedObjectInstance m_TemplateObject;
        private XmlNode m_TemplateParentNode;
        private bool valueChanged;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.CheckState; }
            set { return; }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
            this.Text = "";
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged
        {
            get { return this.valueChanged; }
            set { this.valueChanged = value; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        public void Init(ITemplate template,
                         ITypedObjectInstance o,
                         ITypedObjectInstance templateObject,
                         XmlNode parentNode,
                         XmlNode templateParentNode,
                         IFieldDefinition field)
        {
            this.m_Template = template;

            this.m_Object = o;
            this.m_TemplateObject = templateObject;

            this.m_ParentNode = parentNode;
            this.m_TemplateParentNode = templateParentNode;

            this.m_Field = field;
            this.m_FieldNode = null;
            this.m_Alias = string.Empty;

            this.m_TemplateFieldNode = PropertyEditorUtil.FindXmlNode(field.Name, this.m_TemplateParentNode);

            if (this.m_TemplateFieldNode != null &&
                this.m_TemplateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_Alias = this.m_TemplateFieldNode.Attributes["ExposeAs"].Value;
            }

            if (this.m_Alias != string.Empty)
            {
                this.m_FieldNode = PropertyEditorUtil.FindXmlNode(this.m_Alias, this.m_ParentNode);
            }

            this.ThreeState = (this.m_Field.TypeName == "tristate") && CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue) == CheckState.Indeterminate;
            this.SetCheckboxStateFromXml();
            if (this.m_Menu == null)
            {
                this.m_Menu = new ContextMenuStrip();
                this.m_Menu.Items.Add(new ToolStripMenuItem("Reset", null, new EventHandler(this.On_Reset)));
                this.ContextMenuStrip = this.m_Menu;
                this.ContextMenuStrip.Opening += this.ContextMenuStrip_Opening;
                this.ContextMenuStrip = this.m_Menu;
            }
        }

        private void On_Reset(object sender, EventArgs e)
        {
            if (this.m_FieldNode != null)
            {
                this.m_Object.Node.RemoveChild(this.m_FieldNode);
                this.m_Object.MarkAsDirty();
                this.SetCheckboxStateFromXml();
            }
        }

        private void ContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (this.m_FieldNode == null)
            {
                e.Cancel = true;
            }
        }

        private void SetCheckboxStateFromXml()
        {
            if (this.m_Field.TypeName == "tristate")
            {
                this.ThreeState = CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue) == CheckState.Indeterminate; 
                if (this.m_FieldNode == null)
                {
                    if (this.m_TemplateFieldNode == null)
                    {
                        this.CheckState = CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue);
                    }
                    else if (this.m_TemplateFieldNode.InnerText == "yes")
                    {
                        this.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        this.CheckState = CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue);
                    }
                }
                else if (this.m_FieldNode.InnerText == "yes")
                {
                    this.CheckState = CheckState.Checked;
                }
                else
                {
                    this.CheckState = CheckState.Unchecked;
                }
            }
            else
            {
                this.ThreeState = false;
                if (this.m_FieldNode == null)
                {
                    if (this.m_TemplateFieldNode == null)
                    {
                        if (this.m_Field.DefaultValue == "yes")
                        {
                            this.CheckState = CheckState.Checked;
                        }
                        else
                        {
                            this.CheckState = CheckState.Unchecked;
                        }
                    }
                    else
                    {
                        if (this.m_TemplateFieldNode.InnerText == "yes")
                        {
                            this.CheckState = CheckState.Checked;
                        }
                        else
                        {
                            this.CheckState = CheckState.Unchecked;
                        }
                    }
                }
                else
                {
                    if (this.m_FieldNode.InnerText == "yes")
                    {
                        this.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        this.CheckState = CheckState.Unchecked;
                    }
                }
            }
        }

        public void UpdateNode()
        {
            var dirty = false;
            //create xml
            if (this.m_FieldNode == null)
            {
                if (this.m_Alias == string.Empty)
                {
                    var takenAliases = this.m_Template.GetExposedFieldNames();

                    var tempAlias = this.m_Field.Name.ToLower();
                    var variation = 1;
                    while (takenAliases.Contains(tempAlias))
                    {
                        tempAlias = this.m_Field.Name.ToLower() + variation;
                        variation++;
                    }

                    this.m_Alias = tempAlias;
                }

                if (this.m_TemplateFieldNode == null)
                {
                    this.m_TemplateFieldNode = this.m_TemplateParentNode.OwnerDocument.CreateElement(this.m_Field.Name);
                    this.m_TemplateParentNode.AppendChild(this.m_TemplateFieldNode);
                    this.m_TemplateFieldNode.InnerText = this.m_Field.DefaultValue;
                }

                ((XmlElement)this.m_TemplateFieldNode).SetAttribute("ExposeAs", this.m_Alias);

                this.m_FieldNode = this.m_ParentNode.OwnerDocument.CreateElement(this.m_Alias);
                this.m_ParentNode.AppendChild(this.m_FieldNode);
            }

            //set state
            switch (this.CheckState)
            {
                case CheckState.Unchecked:
                case CheckState.Checked:
                    var val = (this.Checked ? "yes" : "no");
                    if (this.m_FieldNode.InnerText != val)
                    {
                        this.m_FieldNode.InnerText = val;
                        dirty = true;
                    }
                    break;
                case CheckState.Indeterminate:
                    if (this.m_FieldNode != null)
                    {
                        this.m_ParentNode.RemoveChild(this.m_FieldNode);
                        this.m_FieldNode = null;
                        dirty = true;
                    }
                    break;
            }
            if (dirty)
            {
                this.m_Object.MarkAsDirty();
            }
        }

        protected override void OnCheckStateChanged(EventArgs e)
        {
            this.valueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
            base.OnCheckStateChanged(e);
            this.UpdateNode();

           switch (this.CheckState)
            {
                case CheckState.Checked:
                    this.Text = "yes";
                    break;
                case CheckState.Unchecked:
                    this.Text = "no";
                    break;
                case CheckState.Indeterminate:
                    CheckState = CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue);
                    if (CheckState == CheckState.Indeterminate)
                    {
                        this.Text = "use parent value";
                    }
                    break;
            }
        }

    
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter ||
                keyData == Keys.Space)
            {
                switch (this.CheckState)
                {
                    case CheckState.Checked:
                        if (this.ThreeState)
                        {
                            this.CheckState = CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue);
                        }
                        else
                        {
                            this.CheckState = CheckState.Unchecked;
                        }
                        return true;
                    case CheckState.Unchecked:
                        this.CheckState = CheckState.Checked;
                        return true;
                    case CheckState.Indeterminate:
                        this.CheckState = CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue);
                        return true;
                }
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}