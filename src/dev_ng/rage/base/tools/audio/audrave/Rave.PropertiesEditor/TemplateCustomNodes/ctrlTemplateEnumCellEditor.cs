namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.EnumEntry;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;

    [DesignerCategory("code")]
    internal class TemplateEnumEditor : ComboBox,
                                        IDataGridViewEditingControl
    {
        private string m_Alias;
        private EnumDefinition m_EnumDef;
        private IFieldDefinition m_Field;
        private XmlNode m_FieldNode;
        private ITypedObjectInstance m_Object;

        private XmlNode m_ParentNode;
        private ITemplate m_Template;
        private XmlNode m_TemplateFieldNode;
        private ITypedObjectInstance m_TemplateObject;
        private XmlNode m_TemplateParentNode;

        private ToolTip m_ToolTip;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.Text; }
            set
            {
                //set via xml
                return;
            }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
            this.BackColor = Color.White;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
            //done in init
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        public void Init(ITemplate template,
                         ITypedObjectInstance o,
                         ITypedObjectInstance templateObject,
                         XmlNode parent,
                         XmlNode templateParent,
                         IFieldDefinition field)
        {
            this.m_Template = template;

            this.m_Object = o;
            this.m_TemplateObject = templateObject;

            this.m_ParentNode = parent;
            this.m_TemplateParentNode = templateParent;

            this.m_Field = field;
            this.m_Alias = string.Empty;
            this.m_FieldNode = null;

            this.m_TemplateFieldNode = PropertyEditorUtil.FindXmlNode(field.Name, this.m_TemplateParentNode);

            if (this.m_TemplateFieldNode != null &&
                this.m_TemplateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_Alias = this.m_TemplateFieldNode.Attributes["ExposeAs"].Value;
            }

            if (this.m_Alias != string.Empty)
            {
                this.m_FieldNode = PropertyEditorUtil.FindXmlNode(this.m_Alias, this.m_ParentNode);
            }

            if (this.m_ToolTip == null)
            {
                this.m_ToolTip = new ToolTip();
                this.m_ToolTip.AutoPopDelay = 0;
                this.m_ToolTip.InitialDelay = 0;
                this.m_ToolTip.ReshowDelay = 0;
                this.m_ToolTip.ShowAlways = true;
                this.m_ToolTip.SetToolTip(this, "");
            }

            this.Items.Clear();
            var al = new ArrayList();
            var simpleField = this.m_Field as ISimpleFieldDefinition;

            this.m_EnumDef = RaveInstance.AllTypeDefinitions[this.m_TemplateObject.Type].FindEnumDefinition(simpleField.EnumName);

            if (this.m_EnumDef != null)
            {
                foreach (var e in this.m_EnumDef.Values)
                {
                    al.Add(e);
                }

                al.Sort(new EnumComparer());
                this.Items.AddRange(al.ToArray());
            }
            if (this.m_FieldNode != null)
            {
                this.SelectedItem = this.m_EnumDef.FindEnumFromValue(this.m_FieldNode.InnerText);
                this.Text = this.SelectedItem.ToString();
            }
            else if (this.m_TemplateFieldNode != null)
            {
                this.SelectedItem = this.m_EnumDef.FindEnumFromValue(this.m_TemplateFieldNode.InnerText);
                this.Text = this.SelectedItem.ToString();
            }
            else
            {
                var e = this.m_EnumDef.FindEnumFromValue(this.m_Field.DefaultValue);
                this.SelectedItem = e;
                if (e != null)
                {
                    this.m_ToolTip.SetToolTip(this, e.Description);
                    this.Text = e.DisplayName;
                }
                else
                {
                    this.Text = "";
                }
            }
        }

        public void UpdateNode()
        {
            if (this.SelectedItem != null)
            {
                var ev = (EnumValueEntry) this.SelectedItem;

                if (this.m_FieldNode == null)
                {
                    if (this.m_Alias == string.Empty)
                    {
                        var takenAliases = this.m_Template.GetExposedFieldNames();

                        var tempAlias = this.m_Field.Name.ToLower();
                        var variation = 1;
                        while (takenAliases.Contains(tempAlias))
                        {
                            tempAlias = this.m_Field.Name.ToLower() + variation;
                            variation++;
                        }

                        this.m_Alias = tempAlias;
                    }

                    if (this.m_TemplateFieldNode == null)
                    {
                        this.m_TemplateFieldNode = this.m_TemplateParentNode.OwnerDocument.CreateElement(this.m_Field.Name);
                        this.m_TemplateParentNode.AppendChild(this.m_TemplateFieldNode);
                        this.m_TemplateParentNode.InnerText = this.m_Field.DefaultValue;
                    }

                    ((XmlElement) this.m_TemplateFieldNode).SetAttribute("ExposeAs", this.m_Alias);

                    this.m_FieldNode = this.m_ParentNode.OwnerDocument.CreateElement(this.m_Alias);
                    this.m_ParentNode.AppendChild(this.m_FieldNode);
                }

                this.m_FieldNode.InnerText = ev.ValueName;
                this.m_Object.MarkAsDirty();
                this.m_TemplateObject.MarkAsDirty();
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.UpdateNode();
                return true;
            }
            if (keyData == (Keys.Control | Keys.C) && !string.IsNullOrEmpty(this.Text))
            {
                Clipboard.SetText(this.Text);
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        protected override void OnSelectedItemChanged(EventArgs e)
        {
            base.OnSelectedItemChanged(e);
            var ev = (EnumValueEntry) this.SelectedItem;
            if (ev != null)
            {
                this.m_ToolTip.SetToolTip(this, ev.Description);
            }
        }

        #region Nested type: EnumComparer

        public class EnumComparer : IComparer
        {
            #region IComparer Members

            public int Compare(object x, object y)
            {
                return String.Compare(((EnumValueEntry) x).DisplayName, ((EnumValueEntry) y).DisplayName);
            }

            #endregion
        }

        #endregion
    }
}