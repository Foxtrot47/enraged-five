namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    internal class SliderCellEditingControl : SliderEditor,
                                              IDataGridViewEditingControl
    {
        private string m_alias;
        private XmlNode m_fieldNode;
        private IObjectInstance m_objectInstance;
        private XmlNode m_parentNode;
        private bool m_valueChanged;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.Value; }
            set { return; }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            // this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged
        {
            get { return this.m_valueChanged; }
            set { this.m_valueChanged = value; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return Cursor.Current; }
        }

        #endregion

        public void Init(IObjectInstance obj,
                         XmlNode parentNode,
                         IFieldDefinitionInstance fieldInstance,
                         string fieldAlias,
                         bool draw)
        {
            this.m_objectInstance = obj;
            this.m_parentNode = parentNode;
            this.m_alias = fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);

            this.Setup(parentNode, fieldInstance, this.m_alias, draw);
			m_Numeric.Select();
		}

        protected override void m_TrackBar_OnValueChanged(object sender, EventArgs e)
        {
            base.m_TrackBar_OnValueChanged(sender, e);
            this.m_valueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
            this.UpdateNode();
        }

        public void UpdateNode()
        {
            if (this.m_fieldNode == null)
            {
                this.m_fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_alias);
                this.m_parentNode.AppendChild(this.m_fieldNode);
            }

            this.m_fieldNode.InnerText = this.Value;
            this.m_objectInstance.MarkAsDirty();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.UpDateValue();
                this.UpdateNode();
                return false;
            }
            if (keyData == (Keys.Control | Keys.C) && !string.IsNullOrEmpty(this.Value))
            {
                Clipboard.SetText(this.m_Numeric.Value.ToString());
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}