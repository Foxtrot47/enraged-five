namespace Rave.PropertiesEditor.CustomNodes
{
    public interface IFieldCell
    {
        bool IsDefaultValue { get; }

        CellType Type { get; }
    }
}