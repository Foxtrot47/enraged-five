namespace Rave.PropertiesEditor.Editors
{
    partial class frmGridEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_DataGridView = new System.Windows.Forms.DataGridView();
            this.mnuPop_Up = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuHide = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSaveLayout = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridView)).BeginInit();
            this.mnuPop_Up.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_DataGridView
            // 
            this.m_DataGridView.AllowDrop = true;
            this.m_DataGridView.AllowUserToAddRows = false;
            this.m_DataGridView.AllowUserToDeleteRows = false;
            this.m_DataGridView.AllowUserToOrderColumns = true;
            this.m_DataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_DataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.m_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.m_DataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.m_DataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.m_DataGridView.Location = new System.Drawing.Point(0, 24);
            this.m_DataGridView.Name = "m_DataGridView";
            this.m_DataGridView.RowHeadersWidth = 150;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.m_DataGridView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.m_DataGridView.RowTemplate.Height = 25;
            this.m_DataGridView.Size = new System.Drawing.Size(962, 380);
            this.m_DataGridView.TabIndex = 0;
            this.m_DataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ColumnHeader_Click);
            // 
            // mnuPop_Up
            // 
            this.mnuPop_Up.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHide});
            this.mnuPop_Up.Name = "mnuPop_Up";
            this.mnuPop_Up.Size = new System.Drawing.Size(145, 26);
            // 
            // mnuHide
            // 
            this.mnuHide.Name = "mnuHide";
            this.mnuHide.Size = new System.Drawing.Size(144, 22);
            this.mnuHide.Text = "Hide Column";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(962, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSaveLayout});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // mnuSaveLayout
            // 
            this.mnuSaveLayout.Name = "mnuSaveLayout";
            this.mnuSaveLayout.Size = new System.Drawing.Size(145, 22);
            this.mnuSaveLayout.Text = "Save Layout";
            this.mnuSaveLayout.Click += new System.EventHandler(this.mnuSaveLayout_Click);
            // 
            // frmGridEditor
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(962, 404);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.m_DataGridView);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmGridEditor";
            this.Text = "Multi-object Editor";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.m_DataGridView)).EndInit();
            this.mnuPop_Up.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView m_DataGridView;
        private System.Windows.Forms.ContextMenuStrip mnuPop_Up;
        private System.Windows.Forms.ToolStripMenuItem mnuHide;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuSaveLayout;
    }
}