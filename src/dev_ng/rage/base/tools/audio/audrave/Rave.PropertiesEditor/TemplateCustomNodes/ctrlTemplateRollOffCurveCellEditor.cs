namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;
    using Rave.Types.Infrastructure.Structs;
    using Rave.Types.Objects;

    [DesignerCategory("code")]
    internal class TemplateRollOffCurveEditor : ComboBox,
                                                IDataGridViewEditingControl
    {
        private string m_Alias;
        private IFieldDefinition m_Field;
        private XmlNode m_FieldNode;
        private ITypedObjectInstance m_Object;
        private XmlNode m_ParentNode;
        private ITemplate m_Template;
        private XmlNode m_TemplateFieldNode;
        private ITypedObjectInstance m_TemplateObject;
        private XmlNode m_TemplateParentNode;
        private ObjectLookupId objectLookupId;
        private IObjectInstance currCurve;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.Text; }
            set { return; }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            // Let the DateTimePicker handle the keys listed.
            switch (key & Keys.KeyCode)
            {
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Right:
                case Keys.Home:
                case Keys.End:
                case Keys.PageDown:
                case Keys.PageUp:
                    return true;
                default:
                    return false;
            }
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        public void Init(ITemplate template,
                         ITypedObjectInstance o,
                         ITypedObjectInstance templateObject,
                         XmlNode parentNode,
                         XmlNode templateParentNode,
                         IFieldDefinition field)
        {
            this.m_Template = template;
            this.m_Object = o;
            this.m_TemplateObject = templateObject;
            this.m_ParentNode = parentNode;
            this.m_TemplateParentNode = templateParentNode;
            this.m_Field = field;
            this.m_Alias = string.Empty;
            this.m_FieldNode = null;

            this.Items.Clear();
            if (TypeUtils.RolloffCurves != null)
            {
                foreach (var curve in TypeUtils.RolloffCurves.ObjectInstances)
                {
                    this.Items.Add(curve.Name);
                }
            }

            this.m_TemplateFieldNode = PropertyEditorUtil.FindXmlNode(field.Name, this.m_TemplateParentNode);

            if (this.m_TemplateFieldNode != null &&
                this.m_TemplateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_Alias = this.m_TemplateFieldNode.Attributes["ExposeAs"].Value;
            }

            if (this.m_Alias != string.Empty)
            {
                this.m_FieldNode = PropertyEditorUtil.FindXmlNode(this.m_Alias, this.m_ParentNode);
            }

            if (this.m_FieldNode != null)
            {
                this.Text = this.m_FieldNode.InnerText;
            }
            else if (this.m_TemplateFieldNode != null)
            {
                this.Text = this.m_TemplateFieldNode.InnerText;
            }
            else
            {
                this.Text = this.m_Field.DefaultValue;
            }

            this.objectLookupId = new ObjectLookupId("Curves", "BASE");

            if (RaveInstance.ObjectLookupTables[this.objectLookupId].TryGetValue(this.Text, out this.currCurve))
            {
                this.currCurve.AddReferencer(this.m_Object);
            }
        }

        public void UpdateNode()
        {
            if (this.m_FieldNode == null)
            {
                if (this.m_Alias == string.Empty)
                {
                    var takenAliases = this.m_Template.GetExposedFieldNames();

                    var tempAlias = this.m_Field.Name.ToLower();
                    var variation = 1;
                    while (takenAliases.Contains(tempAlias))
                    {
                        tempAlias = this.m_Field.Name.ToLower() + variation;
                        variation++;
                    }

                    this.m_Alias = tempAlias;
                }

                if (this.m_TemplateFieldNode == null)
                {
                    this.m_TemplateFieldNode = this.m_TemplateParentNode.OwnerDocument.CreateElement(this.m_Field.Name);
                    this.m_TemplateParentNode.AppendChild(this.m_TemplateFieldNode);
                    this.m_TemplateParentNode.InnerText = this.m_Field.DefaultValue;
                }

                ((XmlElement) this.m_TemplateFieldNode).SetAttribute("ExposeAs", this.m_Alias);

                this.m_FieldNode = this.m_ParentNode.OwnerDocument.CreateElement(this.m_Alias);
                this.m_ParentNode.AppendChild(this.m_FieldNode);
            }

            if (this.SelectedItem != null && this.Text != "")
            {
                if (null != this.currCurve)
                {
                    this.currCurve.RemoveReferencer(this.m_Object);
                }

                IObjectInstance newCurve;
                if (RaveInstance.ObjectLookupTables[this.objectLookupId].TryGetValue(this.Text, out newCurve))
                {
                    newCurve.AddReferencer(this.m_Object);
                    this.currCurve = newCurve;
                }

                this.m_FieldNode.InnerText = this.SelectedItem.ToString();
            }
            else
            {
                if (null != this.currCurve)
                {
                    this.currCurve.RemoveReferencer(this.m_Object);
                    this.currCurve = null;
                }

                this.m_FieldNode.InnerText = this.Text;
            }
            this.m_Object.MarkAsDirty();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.UpdateNode();
                return true;
            }
            if (keyData == (Keys.Control | Keys.C) && !string.IsNullOrEmpty(this.Text))
            {
                Clipboard.SetText(this.Text);
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}