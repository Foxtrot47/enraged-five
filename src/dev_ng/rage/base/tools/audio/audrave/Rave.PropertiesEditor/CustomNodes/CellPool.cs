namespace Rave.PropertiesEditor.CustomNodes
{
    using System.Collections.Generic;
    using System.Windows.Forms;

    public enum CellType
    {
        CATEGORYREF,
        CHECKBOX,
        CURVEREF,
        ENUM,
        EXPOSE,
        MULTIEDIT,
        NUMERIC,
        OBJECTREF,
        OVERRIDE,
        PARENTVALUE,
        ROLLOFF,
        SLIDER,
        TEXT,
        VARREF,
        WAVEREF,
        BANKREF
    } ;

    public class CellPool
    {
        private static readonly Dictionary<CellType, List<DataGridViewCell>> sm_CellPool =
            new Dictionary<CellType, List<DataGridViewCell>>();

        public static DataGridViewCell GetCell(CellType type)
        {
            bool poolEnabled = false;
            if (poolEnabled &&
                sm_CellPool.ContainsKey(type) &&
                sm_CellPool[type].Count > 0)
            {
                var cell = sm_CellPool[type][0];
                sm_CellPool[type].RemoveAt(0);
                return cell;
            }
            else
            {
                switch (type)
                {
                    case CellType.CATEGORYREF:
                        return new ctrlCategoryRefCell();
                    case CellType.CHECKBOX:
                        return new ctrlCheckBoxCell();
                    case CellType.ENUM:
                        return new ctrlEnumCell();
                    case CellType.EXPOSE:
                        return new ctrlExposeCell();
                    case CellType.MULTIEDIT:
                        return new ctrlMultiEditCell();
                    case CellType.NUMERIC:
                        return new ctrlNumericCell();
                    case CellType.OBJECTREF:
                        return new ctrlObjectRefCell();
                    case CellType.OVERRIDE:
                        return new ctrlOverrideParentCell();
                    case CellType.PARENTVALUE:
                        return new ctrlParentValueCell();
                    case CellType.ROLLOFF:
                        return new ctrlRollOffCurveCell();
                    case CellType.SLIDER:
                        return new ctrlSliderCell();
                    case CellType.TEXT:
                        return new ctrlTextCell();
                    case CellType.VARREF:
                        return new ctrlVariableRefCell();
                    case CellType.WAVEREF:
                        return new ctrlWaveRefCell();
                    case CellType.BANKREF:
                        return new ctrlWaveBankRefCell();
                    default:
                        return null;
                }
            }
        }

        public static void DisposeCell(DataGridViewCell cell)
        {
            /*var row = cell.OwningRow;
            row.Cells.Remove(cell);

            var fieldCell = cell as IFieldCell;
            if (fieldCell != null)
            {
                if (!sm_CellPool.ContainsKey(fieldCell.Type))
                {
                    sm_CellPool.Add(fieldCell.Type, new List<DataGridViewCell>());
                }
                sm_CellPool[fieldCell.Type].Add(cell);
            }
            else
            {*/
                cell.Dispose();
            //}
        }
    }
}