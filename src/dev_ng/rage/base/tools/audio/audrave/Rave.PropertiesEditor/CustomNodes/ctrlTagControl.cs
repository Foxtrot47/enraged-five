﻿using System.Windows.Forms;
using System.Xml;
using Rave.TagEditor;
using Rave.Types.Infrastructure.Interfaces.Objects;

namespace Rave.PropertiesEditor.CustomNodes
{
	public partial class ctrlTagControl : UserControl
	{
		public void Init(IObjectInstance objectInstance, string fieldAlias, XmlNode node)
		{
			tagListView1 = new TagListView(objectInstance, fieldAlias, node);
			InitializeComponent();
			SizeChanged += (p, j) =>
			{
				elementHost1.Size = Size;
			};
		}
	}
}