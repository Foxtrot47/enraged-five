namespace Rave.PropertiesEditor.CustomNodes
{
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    internal class ExposeEditingControl : ctrlExpose,
                                          IDataGridViewEditingControl
    {
        private IObjectInstance m_objectInstance;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.Value; }
            set { return; }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            // this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return Cursor.Current; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance, XmlNode parentNode, IFieldDefinitionInstance fieldInstance)
        {
            this.m_objectInstance = objectInstance;
            this.Setup(this.m_objectInstance, fieldInstance, parentNode);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.C) && !string.IsNullOrEmpty(this.Value))
            {
                Clipboard.SetText(this.Value);
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}