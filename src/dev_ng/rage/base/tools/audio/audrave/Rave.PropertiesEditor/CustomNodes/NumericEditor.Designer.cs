﻿namespace Rave.PropertiesEditor.CustomNodes
{
    partial class NumericEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_numeric = new System.Windows.Forms.NumericUpDown();
            this.m_units = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.m_numeric)).BeginInit();
            this.SuspendLayout();
            // 
            // m_numeric
            // 
            this.m_numeric.Location = new System.Drawing.Point(0, 0);
            this.m_numeric.Name = "m_numeric";
            this.m_numeric.Size = new System.Drawing.Size(147, 20);
            this.m_numeric.TabIndex = 0;
            // 
            // m_units
            // 
            this.m_units.Location = new System.Drawing.Point(153, 2);
            this.m_units.Name = "m_units";
            this.m_units.Size = new System.Drawing.Size(50, 18);
            this.m_units.TabIndex = 1;
            // 
            // NumericEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_units);
            this.Controls.Add(this.m_numeric);
            this.Name = "NumericEditor";
            this.Size = new System.Drawing.Size(205, 22);
            ((System.ComponentModel.ISupportInitialize)(this.m_numeric)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.NumericUpDown m_numeric;
        protected System.Windows.Forms.Label m_units;
    }
}
