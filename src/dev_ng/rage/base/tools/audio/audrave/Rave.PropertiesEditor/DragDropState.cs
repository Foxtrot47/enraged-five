namespace Rave.PropertiesEditor
{
    public enum DragPosition
    {
        TOP,
        BOTTOM,
        MIDDLE
    } ;

    public struct DragDropState
    {
        public DragPosition DragPosition;
        public int Level;
        public int RowIndex;

        public DragDropState(int rowIndex, DragPosition dragPosition, int level)
        {
            this.RowIndex = rowIndex;
            this.DragPosition = dragPosition;
            this.Level = level;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var dds = (DragDropState) obj;

            if (this.RowIndex == dds.RowIndex && this.DragPosition == dds.DragPosition &&
                this.Level == dds.Level)
            {
                return true;
            }

            return false;
        }
    }
}