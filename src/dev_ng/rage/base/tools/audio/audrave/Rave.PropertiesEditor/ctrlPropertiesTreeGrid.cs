using System.Windows.Input;

namespace Rave.PropertiesEditor
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Drawing;
	using System.Runtime.InteropServices;
	using System.Windows.Forms;

	using AdvancedDataGridView;

	public class ctrlPropertiesTreeGrid : TreeGridView
	{
		private const int WM_LBUTTONDOWN = 0x0201;
		private const int WM_LBUTTONUP = 0x0202;

		private DragDropState m_DragDropState = new DragDropState(-1, DragPosition.MIDDLE, 0);
		private int m_ExpandableColumn;

		public ctrlPropertiesTreeGrid()
		{
			this.m_ExpandableColumn = 0;
			this.ControlColumnIndex = new List<int>();
			SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			RowPostPaint += ctrlPropertiesTreeGrid_RowPostPaint;
		}

		public int ExpandableColumnIndex
		{
			get { return this.m_ExpandableColumn; }
			set { this.m_ExpandableColumn = value; }
		}

		public List<int> ControlColumnIndex { get; set; }
		public event Action<TreeGridNode> OnReorderRows;

		//Paint the drag drop lines so we can see where the node will be dropped
		private void ctrlPropertiesTreeGrid_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
		{
			var width = 0;

			foreach (DataGridViewColumn c in Columns)
			{
				width = width + c.Width;
			}

			var tgn = Rows[e.RowIndex] as TreeGridNode;
			if (tgn.Tag as string != null &&
				tgn.Tag.ToString() == "BREAK")
			{
				e.Graphics.FillRectangle(Brushes.Black,
										 e.RowBounds.X,
										 e.RowBounds.Y + (e.RowBounds.Height / 2),
										 width,
										 1);
			}

			if (e.RowIndex ==
				this.m_DragDropState.RowIndex)
			{
				var level = 5 + ((this.m_DragDropState.Level - 1) * 15);
				width -= level;

				if (this.m_DragDropState.DragPosition ==
					DragPosition.TOP)
				{
					e.Graphics.FillRectangle(Brushes.Blue, e.RowBounds.X + level, e.RowBounds.Y, width, 2);
				}
				else if (this.m_DragDropState.DragPosition ==
						 DragPosition.BOTTOM)
				{
					e.Graphics.FillRectangle(Brushes.Blue, e.RowBounds.X + level, e.RowBounds.Bottom - 2, width, 2);
				}
			}
		}

		[DllImport("user32.dll")]
		private static extern bool SendMessage(IntPtr hWnd, Int32 msg, Int32 wParam, Int32 lParam);

		[DllImport("user32.dll")]
		private static extern IntPtr ChildWindowFromPoint(IntPtr hwndParent, int x, int y);

		[DllImport("user32.dll")]
		private static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

		private int MakeLong(int loWord, int hiWord)
		{
			return (hiWord << 16) | (loWord & 0xffff);
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			if (Cursor != Cursors.Arrow)
			{
				base.OnMouseDown(e);
				return;
			}

			var info = HitTest(e.X, e.Y);

			if (info.RowIndex < 0)
			{
				ClearSelection();
				base.OnMouseDown(e);
				return;
			}

			if (e.Button ==
				MouseButtons.Right)
			{
				ClearSelection();
				if (info.RowIndex >= 0)
				{
					Rows[info.RowIndex].Selected = true;
				}
				else
				{
					ClearSelection();
				}
				base.OnMouseDown(e);
				return;
			}

			if (info.ColumnIndex == this.m_ExpandableColumn)
			{
				if (e.Button ==
					MouseButtons.Left)
				{
					base.OnMouseDown(e);
					//prepare for drag/drop
					var row = Rows[info.RowIndex];
					var node = row as TreeGridNode;
					if (row != null && node != null &&
						node.Parent.ReOrder)
					{
						this.m_DragDropState = new DragDropState(-1, DragPosition.MIDDLE, 0);
						DoDragDrop(row as TreeGridNode, DragDropEffects.Move);
					}
				}
			}
			else
			{
				var dgvc = this[info.ColumnIndex, info.RowIndex];

				if (CurrentCell != dgvc)
				{
					CurrentCell = dgvc;
					var success = BeginEdit(false);
					if (success)
					{
						// For editing controls derived from TextBoxBase, ensure text is scrolled to the left
						if (EditingControl is TextBoxBase)
						{
							var textBox = (TextBoxBase)EditingControl;
							textBox.SelectionStart = 0;
							textBox.ScrollToCaret();
						}

						// Calculate co-ordinates of where user clicked within editing control
						var clickPoint = new Point(e.X - info.ColumnX - EditingControl.Left,
												   e.Y - info.RowY - EditingControl.Top);

						// Simulate mouse click within control
						this.SimulateMouseDown(EditingControl.Handle, clickPoint);
					}
					else
					{
						base.OnMouseDown(e);
					}
				}
				else
				{
					base.OnMouseDown(e);
				}
			}
		}

		private void SimulateMouseDown(IntPtr hWnd, Point clickPoint)
		{
			// Check for child control within editing control
			var childHwnd = ChildWindowFromPoint(hWnd, clickPoint.X, clickPoint.Y);
			if (childHwnd != null)
			{
				// If no child window, simulate mouse click within editing control
				if (childHwnd == hWnd)
				{
					this.SendMouseDown(hWnd, clickPoint);
				}

					// Otherwise, simulate mouse click within child control
				else
				{
					// Get screen co-ordinates of parent (edit control) and child
					RECT parentRect, childRect;
					if (!GetWindowRect(hWnd, out parentRect))
					{
						return;
					}
					if (!GetWindowRect(childHwnd, out childRect))
					{
						return;
					}

					// Determine co-ordinates of where user clicked within child control
					var childClickPoint = new Point(clickPoint.X - (childRect.Left - parentRect.Left),
													clickPoint.Y - (childRect.Top - parentRect.Top));

					// Simulate mouse click within child control
					this.SimulateMouseDown(childHwnd, childClickPoint);
				}
			}
		}

		private void SendMouseDown(IntPtr hWnd, Point clickPoint)
		{
			var lParam = this.MakeLong(clickPoint.X, clickPoint.Y);
			SendMessage(hWnd, WM_LBUTTONDOWN, 0, lParam);
		}

		protected override void OnDragEnter(DragEventArgs drgevent)
		{
			base.OnDragEnter(drgevent);
			drgevent.Effect = DragDropEffects.Move;
		}

		protected override void OnDragOver(DragEventArgs drgevent)
		{
			drgevent.Effect = DragDropEffects.Move;
			var newDragDropState = this.m_DragDropState;

			var p = PointToClient(new Point(drgevent.X, drgevent.Y));
			var info = HitTest(p.X, p.Y);
			var rowIndex = info.RowIndex;

			var draggedNode = (TreeGridNode)drgevent.Data.GetData(typeof(TreeGridNode));
			if (draggedNode != null &&
				rowIndex >= 0)
			{
				var currentNode = (TreeGridNode)Rows[rowIndex];

				if (currentNode != null &&
					currentNode.Parent == draggedNode.Parent)
				{
					newDragDropState.RowIndex = info.RowIndex;
					newDragDropState.Level = (((TreeGridNode)Rows[info.RowIndex]).Level);

					//split the cell into top, middle and bottom
					var r = GetCellDisplayRectangle(0, info.RowIndex, true);
					var top = PointToScreen(new Point(r.Left, r.Top + 5));
					var bottom = PointToScreen(new Point(r.Left, r.Bottom - 5));

					if (drgevent.Y <
						top.Y)
					{
						ClearSelection();
						newDragDropState.DragPosition = DragPosition.TOP;
					}

					else if (drgevent.Y >
							 bottom.Y)
					{
						ClearSelection();
						newDragDropState.DragPosition = DragPosition.BOTTOM;
					}
				}
				else
				{
					drgevent.Effect = DragDropEffects.None;
				}
			}
			else if (info.RowIndex >= 0)
			{
				ClearSelection();
				Rows[info.RowIndex].Selected = true;
				newDragDropState.DragPosition = DragPosition.MIDDLE;
			}

			if (!this.m_DragDropState.Equals(newDragDropState))
			{
				this.m_DragDropState = newDragDropState;
				Invalidate();
			}

			base.OnDragOver(drgevent);
		}
		
		protected override bool ProcessDialogKey(Keys e)
		{
			if (e == Keys.Tab)
			{
				this.EndEdit();


				if (CurrentCell.RowIndex + 1 < this.Rows.Count)
				{
					CurrentCell = this[CurrentCell.ColumnIndex, CurrentCell.RowIndex + 1];
					BeginEdit(true);
				}
				else
				{
					CurrentCell = null;
				}

				return true;
			}
			if (e == (Keys.Tab | Keys.Shift))
			{
				if (CurrentCell.RowIndex - 1 >= 0)
				{
					CurrentCell = this[CurrentCell.ColumnIndex, CurrentCell.RowIndex - 1];
					BeginEdit(true);
				}
				else
				{
					CurrentCell = null;
				}
				return true;
			}

			if (e == Keys.Enter)
			{
				this.EndEdit();
				{
					CurrentCell = null;
				}
				return true;
			}
			return base.ProcessDialogKey(e);
			
		}
		protected override void OnDragDrop(DragEventArgs drgevent)
		{
			var tgn = (TreeGridNode)drgevent.Data.GetData(typeof(TreeGridNode));

			if (tgn != null)
			{
				var parent = tgn.Parent;
				if (this.m_DragDropState.DragPosition == DragPosition.TOP &&
					this.m_DragDropState.RowIndex >= 0)
				{
					var nextSibling = Rows[this.m_DragDropState.RowIndex] as TreeGridNode;
					var newParent = nextSibling.Parent;
					if (nextSibling != null && newParent != tgn && !newParent.IsDescendantOf(tgn) &&
						nextSibling != tgn)
					{
						BeginEdit(true);
						parent.Nodes.Remove(tgn);
						var siblingIndex = newParent.Nodes.IndexOf(nextSibling);
						newParent.Nodes.Insert(siblingIndex, tgn);
						EndEdit();
						if (this.OnReorderRows != null)
						{
							this.OnReorderRows(tgn);
						}
					}
				}
				else if (this.m_DragDropState.RowIndex >= 0)
				{
					var previousSibling = Rows[this.m_DragDropState.RowIndex] as TreeGridNode;
					var newParent = previousSibling.Parent;
					if (previousSibling != null && newParent != tgn && !newParent.IsDescendantOf(tgn) &&
						previousSibling != tgn)
					{
						BeginEdit(true);
						parent.Nodes.Remove(tgn);
						if (previousSibling.IsLastSibling)
						{
							newParent.Nodes.Add(tgn);
						}
						else
						{
							var siblingIndex = newParent.Nodes.IndexOf(previousSibling);
							newParent.Nodes.Insert(siblingIndex + 1, tgn);
						}

						if (this.OnReorderRows != null)
						{
							this.OnReorderRows(tgn);
						}

						EndEdit();
					}

					tgn.Selected = true;

					foreach (var node in Nodes)
					{
						if (node != tgn)
						{
							node.Selected = false;
						}
					}
				}
			}
			else
			{
				if (this.m_DragDropState.RowIndex >= 0)
				{
					Rows[this.m_DragDropState.RowIndex].Selected = true;
				}
			}
			this.m_DragDropState = new DragDropState(-1, DragPosition.MIDDLE, 0);
			base.OnDragDrop(drgevent);
		}

		private void InitializeComponent()
		{
			var dataGridViewCellStyle1 = new DataGridViewCellStyle();
			var dataGridViewCellStyle2 = new DataGridViewCellStyle();
			((ISupportInitialize)(this)).BeginInit();
			SuspendLayout();
			// 
			// ctrlPropertiesTreeGrid
			// 
			AllowDrop = true;
			AllowUserToResizeColumns = false;
			dataGridViewCellStyle1.BackColor = Color.LightGray;
			AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
			BackgroundColor = Color.White;
			CellBorderStyle = DataGridViewCellBorderStyle.SingleVertical;
			ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = SystemColors.Window;
			dataGridViewCellStyle2.Font = new Font("Microsoft Sans Serif",
												   8.25F,
												   FontStyle.Regular,
												   GraphicsUnit.Point,
												   ((0)));
			dataGridViewCellStyle2.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = Color.Gray;
			dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = DataGridViewTriState.False;
			DefaultCellStyle = dataGridViewCellStyle2;
			EditMode = DataGridViewEditMode.EditOnEnter;
			GridColor = Color.White;
			((ISupportInitialize)(this)).EndInit();
			ResumeLayout(false);
		}

		#region Nested type: RECT

		private struct RECT
		{
			public Int32 Bottom;
			public Int32 Left;
			public Int32 Right;
			public Int32 Top;
		}

		#endregion
	}
}