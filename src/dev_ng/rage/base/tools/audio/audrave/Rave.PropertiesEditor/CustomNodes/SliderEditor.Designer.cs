namespace Rave.PropertiesEditor.CustomNodes
{
    partial class SliderEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_TrackBar = new CustomSlider();
            this.m_Numeric = new System.Windows.Forms.NumericUpDown();
            this.lblUnits = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.m_TrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_Numeric)).BeginInit();
            this.SuspendLayout();
            // 
            // m_TrackBar
            // 
            this.m_TrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.m_TrackBar.AutoSize = false;
            this.m_TrackBar.Location = new System.Drawing.Point(2, 2);
            this.m_TrackBar.MaximumSize = new System.Drawing.Size(600, 20);
            this.m_TrackBar.Name = "m_TrackBar";
            this.m_TrackBar.Size = new System.Drawing.Size(99, 20);
            this.m_TrackBar.TabIndex = 0;
            this.m_TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.m_TrackBar.ValueChanged += new System.EventHandler(this.m_TrackBar_OnValueChanged);
            // 
            // m_Numeric
            // 
            this.m_Numeric.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_Numeric.BackColor = System.Drawing.SystemColors.Window;
            this.m_Numeric.DecimalPlaces = 2;
            this.m_Numeric.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.m_Numeric.Location = new System.Drawing.Point(107, 2);
            this.m_Numeric.Name = "m_Numeric";
            this.m_Numeric.Size = new System.Drawing.Size(94, 20);
            this.m_Numeric.TabIndex = 1;
            this.m_Numeric.Validating += m_Numeric_Validating;
            // 
            // lblUnits
            // 
            this.lblUnits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUnits.AutoSize = true;
            this.lblUnits.BackColor = System.Drawing.Color.Transparent;
            this.lblUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits.Location = new System.Drawing.Point(207, 5);
            this.lblUnits.Name = "lblUnits";
            this.lblUnits.Size = new System.Drawing.Size(0, 13);
            this.lblUnits.TabIndex = 2;
            // 
            // SliderEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_Numeric);
            this.Controls.Add(this.m_TrackBar);
            this.Controls.Add(this.lblUnits);
            this.MaximumSize = new System.Drawing.Size(1024, 24);
            this.Name = "SliderEditor";
            this.Size = new System.Drawing.Size(228, 24);
            ((System.ComponentModel.ISupportInitialize)(this.m_TrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_Numeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void m_Numeric_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.UpDateValue();
        }


        #endregion

        private CustomSlider m_TrackBar;
        protected System.Windows.Forms.NumericUpDown m_Numeric;
        private System.Windows.Forms.Label lblUnits;
    }
}
