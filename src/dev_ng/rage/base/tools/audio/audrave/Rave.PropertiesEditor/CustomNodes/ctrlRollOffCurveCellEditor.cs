namespace Rave.PropertiesEditor.CustomNodes
{
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Structs;

    [DesignerCategory("code")]
    internal class RollOffCurveEditor : ComboBox,
                                        IDataGridViewEditingControl
    {
        private string m_alias;
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_object;
        private XmlNode m_parentNode;
        private ObjectLookupId objectLookupId;
        private IObjectInstance currCurve;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.Text; }
            set { return; }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            // Let the DateTimePicker handle the keys listed.
            switch (key & Keys.KeyCode)
            {
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Right:
                case Keys.Home:
                case Keys.End:
                case Keys.PageDown:
                case Keys.PageUp:
                    return true;
                default:
                    return false;
            }
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        public void Init(IObjectInstance o, XmlNode parentNode, IFieldDefinitionInstance fieldInstance, string fieldAlias)
        {
            this.m_object = o;
            this.m_parentNode = parentNode;
            this.m_fieldInstance = fieldInstance;
            this.m_alias = fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            this.Items.Clear();
            if (TypeUtils.RolloffCurves != null)
            {
                foreach (var curve in TypeUtils.RolloffCurves.ObjectInstances)
                {
                    this.Items.Add(curve.Name);
                }
            }
            this.Text = this.m_fieldNode != null ? this.m_fieldNode.InnerText : this.m_fieldInstance.DefaultValue;

            this.objectLookupId = new ObjectLookupId("Curves", "BASE");

            if (RaveInstance.ObjectLookupTables[this.objectLookupId].TryGetValue(this.Text, out this.currCurve))
            {
                this.currCurve.AddReferencer(this.m_object);
            }
        }

        public void UpdateNode()
        {
            if (this.m_fieldNode == null)
            {
                this.m_fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_alias);
                this.m_parentNode.AppendChild(this.m_fieldNode);
            }
            if (this.SelectedItem != null && this.Text != "")
            {
                if (null != this.currCurve)
                {
                    this.currCurve.RemoveReferencer(this.m_object);
                }

                IObjectInstance newCurve;
                if (RaveInstance.ObjectLookupTables[this.objectLookupId].TryGetValue(this.Text, out newCurve))
                {
                    newCurve.AddReferencer(this.m_object);
                    this.currCurve = newCurve;
                }

                this.m_fieldNode.InnerText = this.SelectedItem.ToString();
            }
            else
            {
                if (null != this.currCurve)
                {
                    this.currCurve.RemoveReferencer(this.m_object);
                    this.currCurve = null;
                }

                this.m_fieldNode.InnerText = this.Text;
            }
            this.m_object.MarkAsDirty();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.UpdateNode();
                return false;
            }
            if (keyData == (Keys.Control | Keys.C) && !string.IsNullOrEmpty(this.Text))
            {
                Clipboard.SetText(this.Text);
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        protected override void OnTextChanged(System.EventArgs e)
        {
            this.UpdateNode();
            base.OnTextChanged(e);
        }
    }
}