namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System.Collections.Generic;
    using System.Windows.Forms;

    public enum TemplateCellType
    {
        CATEGORYREF,
        CHECKBOX,
        CURVEREF,
        ENUM,
        NUMERIC,
        OBJECTREF,
        OVERRIDE,
        ROLLOFF,
        SLIDER,
        TEXT,
        VARREF,
        WAVEREF
    } ;

    public class TemplateCellPool
    {
        private static readonly Dictionary<TemplateCellType, List<DataGridViewCell>> sm_CellPool =
            new Dictionary<TemplateCellType, List<DataGridViewCell>>();

        public static DataGridViewCell GetCell(TemplateCellType type)
        {
            if (sm_CellPool.ContainsKey(type) &&
                sm_CellPool[type].Count > 0)
            {
                var cell = sm_CellPool[type][0];
                sm_CellPool[type].RemoveAt(0);
                return cell;
            }
            else
            {
                switch (type)
                {
                    case TemplateCellType.CATEGORYREF:
                        return new ctrlTemplateCategoryRefCell();
                    case TemplateCellType.CHECKBOX:
                        return new ctrlTemplateCheckBoxCell();
                    case TemplateCellType.ENUM:
                        return new ctrlTemplateEnumCell();
                    case TemplateCellType.NUMERIC:
                        return new ctrlTemplateNumericCell();
                    case TemplateCellType.OBJECTREF:
                        return new ctrlTemplateObjectRefCell();
                    case TemplateCellType.OVERRIDE:
                        return new ctrlTemplateOverrideParentCell();
                    case TemplateCellType.ROLLOFF:
                        return new ctrlTemplateRollOffCurveCell();
                    case TemplateCellType.SLIDER:
                        return new ctrlTemplateSliderCell();
                    case TemplateCellType.TEXT:
                        return new ctrlTemplateTextCell();
                    case TemplateCellType.VARREF:
                        return new ctrlTemplateVariableRefCell();
                    case TemplateCellType.WAVEREF:
                        return new ctrlTemplateWaveRefCell();
                    default:
                        return null;
                }
            }
        }

        public static void DisposeCell(DataGridViewCell cell)
        {
            var row = cell.OwningRow;
            row.Cells.Remove(cell);

            var fieldCell = cell as ITemplateFieldCell;
            if (fieldCell != null)
            {
                if (!sm_CellPool.ContainsKey(fieldCell.Type))
                {
                    sm_CellPool.Add(fieldCell.Type, new List<DataGridViewCell>());
                }
                sm_CellPool[fieldCell.Type].Add(cell);
            }
            else
            {
                cell.Dispose();
            }
        }
    }
}