// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ctrlTemplateVariableRefCellEditor.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The template variable ref editor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;
    using Rave.Types.Infrastructure.Interfaces.VariableList;

    /// <summary>
    /// The template variable ref editor.
    /// </summary>
    [DesignerCategory("code")]
    internal class TemplateVariableRefEditor : TextBox, IDataGridViewEditingControl
    {
        #region Fields

        /// <summary>
        /// The input variable list.
        /// </summary>
        private IVariableList inputVariableList;

        /// <summary>
        /// The m_alias.
        /// </summary>
        private string m_alias;

        /// <summary>
        /// The m_field.
        /// </summary>
        private IFieldDefinition m_field;

        /// <summary>
        /// The m_field node.
        /// </summary>
        private XmlNode m_fieldNode;

        /// <summary>
        /// The m_object.
        /// </summary>
        private ITypedObjectInstance m_object;

        /// <summary>
        /// The m_parent node.
        /// </summary>
        private XmlNode m_parentNode;

        /// <summary>
        /// The m_template.
        /// </summary>
        private ITemplate m_template;

        /// <summary>
        /// The m_template field node.
        /// </summary>
        private XmlNode m_templateFieldNode;

        /// <summary>
        /// The m_template parent node.
        /// </summary>
        private XmlNode m_templateParentNode;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the editing control data grid view.
        /// </summary>
        public DataGridView EditingControlDataGridView { get; set; }

        /// <summary>
        /// Gets or sets the editing control formatted value.
        /// </summary>
        public object EditingControlFormattedValue
        {
            get
            {
                return this.Text;
            }

            set
            {
            }
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        /// <summary>
        /// Gets or sets the editing control row index.
        /// </summary>
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        /// <summary>
        /// Gets or sets a value indicating whether editing control value changed.
        /// </summary>
        public bool EditingControlValueChanged { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        /// <summary>
        /// Gets the editing panel cursor.
        /// </summary>
        public Cursor EditingPanelCursor
        {
            get
            {
                return this.Cursor;
            }
        }

        /// <summary>
        /// Gets a value indicating whether reposition editing control on value change.
        /// </summary>
        public bool RepositionEditingControlOnValueChange
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The apply cell style to editing control.
        /// </summary>
        /// <param name="dataGridViewCellStyle">
        /// The data grid view cell style.
        /// </param>
        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
        }

        /// <summary>
        /// The editing control wants input key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="dataGridViewWantsInputKey">
        /// The data grid view wants input key.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        /// <summary>
        /// The get editing control formatted value.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="templateObject">
        /// The template object.
        /// </param>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="templateParent">
        /// The template parent.
        /// </param>
        /// <param name="field">
        /// The field.
        /// </param>
        public void Init(
            ITemplate template, 
            ITypedObjectInstance objectInstance, 
            ITypedObjectInstance templateObject, 
            XmlNode parentNode, 
            XmlNode templateParent, 
            IFieldDefinition field)
        {
            this.m_template = template;

            this.m_object = objectInstance;

            this.m_parentNode = parentNode;
            this.m_templateParentNode = templateParent;

            this.m_field = field;
            this.m_alias = string.Empty;

            this.m_templateFieldNode = PropertyEditorUtil.FindXmlNode(field.Name, this.m_templateParentNode);

            this.m_alias = string.Empty;
            if (this.m_templateFieldNode != null && this.m_templateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_alias = this.m_templateFieldNode.Attributes["ExposeAs"].Value;
            }

            if (this.m_alias != string.Empty)
            {
                this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            }

            if (this.m_fieldNode != null)
            {
                this.Text = this.m_fieldNode.InnerText;
            }

            if (this.m_templateFieldNode != null)
            {
                this.Text = this.m_templateFieldNode.InnerText;
            }
            else
            {
                this.Text = this.m_field.DefaultValue;
            }

            // Enable textbox auto complete mode
            this.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var source = new AutoCompleteStringCollection();
            source.AddRange(this.inputVariableList.FlatListLabels.ToArray());
            this.AutoCompleteCustomSource = source;

            // Initialize variable list based on object instance
            this.inputVariableList = objectInstance.GetVariableList(this.m_alias, this.m_object.TypeDefinition, VariableListType.Input);

            // TODO - set context menu depending on type (input/output)

            // Enable context menu (displayed when cell right-clicked)
            this.ContextMenu = this.inputVariableList.GetContextMenu_Forms(this.MenuItem_Clicked);
        }

        /// <summary>
        /// The prepare editing control for edit.
        /// </summary>
        /// <param name="selectAll">
        /// The select all.
        /// </param>
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        /// <summary>
        /// The update node.
        /// </summary>
        public void UpdateNode()
        {
            if (this.m_fieldNode == null)
            {
                if (this.m_alias == string.Empty)
                {
                    var takenAliases = this.m_template.GetExposedFieldNames();

                    var tempAlias = this.m_field.Name.ToLower();
                    var variation = 1;
                    while (takenAliases.Contains(tempAlias))
                    {
                        tempAlias = this.m_field.Name.ToLower() + variation;
                        variation++;
                    }

                    this.m_alias = tempAlias;
                }

                if (this.m_templateFieldNode == null)
                {
                    this.m_templateFieldNode = this.m_templateParentNode.OwnerDocument.CreateElement(this.m_field.Name);
                    this.m_templateParentNode.AppendChild(this.m_templateFieldNode);
                    this.m_templateParentNode.InnerText = this.m_field.DefaultValue;
                }

                ((XmlElement)this.m_templateFieldNode).SetAttribute("ExposeAs", this.m_alias);

                this.m_fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_alias);
                this.m_parentNode.AppendChild(this.m_fieldNode);
            }

            this.m_fieldNode.InnerText = this.Text;

            this.m_object.MarkAsDirty();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The process cmd key.
        /// </summary>
        /// <param name="msg">
        /// The msg.
        /// </param>
        /// <param name="keyData">
        /// The key data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.UpdateNode();
                return false;
            }

            if (keyData == (Keys.Control | Keys.C) && !string.IsNullOrEmpty(this.Text))
            {
                Clipboard.SetText(this.Text);
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        /// <summary>
        /// The menu item_ clicked.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        private void MenuItem_Clicked(object obj, EventArgs args)
        {
            var item = obj as MenuItem;
            if (null != item)
            {
                var tag = item.Tag as string;
                if (null != tag)
                {
                    this.Text = tag;
                }
            }
        }

        #endregion
    }
}