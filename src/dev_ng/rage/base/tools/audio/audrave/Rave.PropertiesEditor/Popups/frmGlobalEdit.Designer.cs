namespace Rave.PropertiesEditor.Popups
{
    partial class frmGlobalEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_fieldComboBox = new System.Windows.Forms.ComboBox();
            this.lblField = new System.Windows.Forms.Label();
            this.lblMin = new System.Windows.Forms.Label();
            this.lblMax = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCanel = new System.Windows.Forms.Button();
            this.m_groupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUnits = new System.Windows.Forms.Label();
            this.m_range = new System.Windows.Forms.GroupBox();
            this.m_value = new System.Windows.Forms.TextBox();
            this.m_min = new System.Windows.Forms.TextBox();
            this.m_max = new System.Windows.Forms.TextBox();
            this.m_groupBox.SuspendLayout();
            this.m_range.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_fieldComboBox
            // 
            this.m_fieldComboBox.FormattingEnabled = true;
            this.m_fieldComboBox.Location = new System.Drawing.Point(40, 11);
            this.m_fieldComboBox.Name = "m_fieldComboBox";
            this.m_fieldComboBox.Size = new System.Drawing.Size(294, 21);
            this.m_fieldComboBox.Sorted = true;
            this.m_fieldComboBox.TabIndex = 0;
            this.m_fieldComboBox.SelectedIndexChanged += new System.EventHandler(this.FieldComboBox_SelectedIndexChanged);
            // 
            // lblField
            // 
            this.lblField.AutoSize = true;
            this.lblField.Location = new System.Drawing.Point(2, 14);
            this.lblField.Name = "lblField";
            this.lblField.Size = new System.Drawing.Size(32, 13);
            this.lblField.TabIndex = 1;
            this.lblField.Text = "Field:";
            // 
            // lblMin
            // 
            this.lblMin.AutoSize = true;
            this.lblMin.Location = new System.Drawing.Point(8, 35);
            this.lblMin.Name = "lblMin";
            this.lblMin.Size = new System.Drawing.Size(57, 13);
            this.lblMin.TabIndex = 2;
            this.lblMin.Text = "Min Value:";
            // 
            // lblMax
            // 
            this.lblMax.AutoSize = true;
            this.lblMax.Location = new System.Drawing.Point(169, 35);
            this.lblMax.Name = "lblMax";
            this.lblMax.Size = new System.Drawing.Size(60, 13);
            this.lblMax.TabIndex = 5;
            this.lblMax.Text = "Max Value:";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(171, 190);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_OnClick);
            // 
            // btnCanel
            // 
            this.btnCanel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCanel.Location = new System.Drawing.Point(259, 190);
            this.btnCanel.Name = "btnCanel";
            this.btnCanel.Size = new System.Drawing.Size(75, 23);
            this.btnCanel.TabIndex = 7;
            this.btnCanel.Text = "Cancel";
            this.btnCanel.UseVisualStyleBackColor = true;
            // 
            // m_groupBox
            // 
            this.m_groupBox.Controls.Add(this.m_value);
            this.m_groupBox.Controls.Add(this.label1);
            this.m_groupBox.Controls.Add(this.lblUnits);
            this.m_groupBox.Location = new System.Drawing.Point(5, 41);
            this.m_groupBox.Name = "m_groupBox";
            this.m_groupBox.Size = new System.Drawing.Size(329, 70);
            this.m_groupBox.TabIndex = 10;
            this.m_groupBox.TabStop = false;
            this.m_groupBox.Text = "Value";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Increase/Decrease Value by:";
            // 
            // lblUnits
            // 
            this.lblUnits.AutoSize = true;
            this.lblUnits.Location = new System.Drawing.Point(262, 31);
            this.lblUnits.Name = "lblUnits";
            this.lblUnits.Size = new System.Drawing.Size(29, 13);
            this.lblUnits.TabIndex = 12;
            this.lblUnits.Text = "units";
            // 
            // m_range
            // 
            this.m_range.Controls.Add(this.m_max);
            this.m_range.Controls.Add(this.m_min);
            this.m_range.Controls.Add(this.lblMax);
            this.m_range.Controls.Add(this.lblMin);
            this.m_range.Location = new System.Drawing.Point(5, 117);
            this.m_range.Name = "m_range";
            this.m_range.Size = new System.Drawing.Size(329, 67);
            this.m_range.TabIndex = 11;
            this.m_range.TabStop = false;
            this.m_range.Text = "Range";
            // 
            // m_value
            // 
            this.m_value.Location = new System.Drawing.Point(153, 28);
            this.m_value.Name = "m_value";
            this.m_value.Size = new System.Drawing.Size(103, 20);
            this.m_value.TabIndex = 16;
            // 
            // m_min
            // 
            this.m_min.Location = new System.Drawing.Point(69, 32);
            this.m_min.Name = "m_min";
            this.m_min.Size = new System.Drawing.Size(85, 20);
            this.m_min.TabIndex = 6;
            // 
            // m_max
            // 
            this.m_max.Location = new System.Drawing.Point(235, 32);
            this.m_max.Name = "m_max";
            this.m_max.Size = new System.Drawing.Size(85, 20);
            this.m_max.TabIndex = 7;
            // 
            // frmGlobalEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 217);
            this.Controls.Add(this.m_range);
            this.Controls.Add(this.m_groupBox);
            this.Controls.Add(this.lblField);
            this.Controls.Add(this.m_fieldComboBox);
            this.Controls.Add(this.btnCanel);
            this.Controls.Add(this.btnOK);
            this.Name = "frmGlobalEdit";
            this.Text = "Global Edit";
            this.m_groupBox.ResumeLayout(false);
            this.m_groupBox.PerformLayout();
            this.m_range.ResumeLayout(false);
            this.m_range.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox m_fieldComboBox;
        private System.Windows.Forms.Label lblField;
        private System.Windows.Forms.Label lblMin;
        private System.Windows.Forms.Label lblMax;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCanel;
        private System.Windows.Forms.GroupBox m_groupBox;
        private System.Windows.Forms.Label lblUnits;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox m_range;
        private System.Windows.Forms.TextBox m_value;
        private System.Windows.Forms.TextBox m_max;
        private System.Windows.Forms.TextBox m_min;
    }
}