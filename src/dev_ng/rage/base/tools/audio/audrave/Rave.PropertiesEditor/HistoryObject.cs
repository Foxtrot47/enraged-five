﻿// -----------------------------------------------------------------------
// <copyright file="HistoryObject.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.PropertiesEditor
{
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;

    /// <summary>
    /// Properties Editor history object.
    /// </summary>
    public class HistoryObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HistoryObject"/> class.
        /// </summary>
        /// <param name="objectName">
        /// The object name.
        /// </param>
        /// <param name="objectBank">
        /// The object bank.
        /// </param>
        /// <param name="mode">
        /// The mode.
        /// </param>
        public HistoryObject(string objectName, IXmlBank objectBank, Mode mode)
        {
            this.Name = objectName;
            this.Bank = objectBank;
            this.Mode = mode;
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the bank.
        /// </summary>
        public IXmlBank Bank { get; set; }

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        public Mode Mode { get; set; }

        /// <summary>
        /// The get template bank.
        /// </summary>
        /// <returns>
        /// The <see cref="ITemplateBank"/>.
        /// </returns>
        public ITemplateBank GetTemplateBank()
        {
            return this.Bank as ITemplateBank;
        }

        /// <summary>
        /// The get object bank.
        /// </summary>
        /// <returns>
        /// The <see cref="IObjectBank"/>.
        /// </returns>
        public IObjectBank GetObjectBank()
        {
            return this.Bank as IObjectBank;
        }

        /// <summary>
        /// Get the object as just an object.
        /// </summary>
        /// <returns>
        /// The object <see cref="object"/>.
        /// </returns>
        public object GetObject()
        {
            var objectBank = this.GetObjectBank();
            if (null != objectBank)
            {
                return objectBank.FindObjectInstance(this.Name);
            }

            var templateBank = this.GetTemplateBank();
            if (null != templateBank)
            {
                return templateBank.FindTemplate(this.Name);
            }

            return null;
        }

        /// <summary>
        /// Get the object instance.
        /// </summary>
        /// <returns>
        /// The object instance <see cref="IObjectInstance"/>.
        /// </returns>
        public IObjectInstance GetObjectInstance()
        {
            var objectBank = this.GetObjectBank();
            return null != objectBank ? objectBank.FindObjectInstance(this.Name) : null;
        }

        /// <summary>
        /// Get the template.
        /// </summary>
        /// <returns>
        /// The template <see cref="ITemplate"/>.
        /// </returns>
        public ITemplate GetTemplate()
        {
            var templateBank = this.GetTemplateBank();
            return null != templateBank ? templateBank.FindTemplate(this.Name) : null;
        }

        #endregion
    }
}
