using Rave.PropertiesEditor.CustomNodes;

namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;

    [DesignerCategory("code")]
    internal class TemplateOverrideParentEditor : CheckBox,
                                                  IDataGridViewEditingControl
    {
        private string m_Alias;
        private IFieldDefinition m_Field;
        private XmlNode m_FieldNode;
        private ITypedObjectInstance m_Object;

        private XmlNode m_ParentNode;
        private ITemplate m_Template;
        private XmlNode m_TemplateFieldNode;
        private ITypedObjectInstance m_TemplateObject;
        private XmlNode m_TemplateParentNode;
        private bool valueChanged;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.CheckState; }
            set { return; }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            // Let the DateTimePicker handle the keys listed.
            switch (key & Keys.KeyCode)
            {
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Right:
                case Keys.Home:
                case Keys.End:
                case Keys.PageDown:
                case Keys.PageUp:
                    return true;
                default:
                    return false;
            }
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged
        {
            get { return this.valueChanged; }
            set { this.valueChanged = value; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        public void Init(ITemplate template,
                         ITypedObjectInstance o,
                         ITypedObjectInstance templateObject,
                         XmlNode parentNode,
                         XmlNode templateParent,
                         IFieldDefinition field)
        {
            this.m_Template = template;

            this.m_Object = o;
            this.m_TemplateObject = templateObject;

            this.m_ParentNode = parentNode;
            this.m_TemplateParentNode = templateParent;

            this.m_Field = field;
            this.m_Alias = string.Empty;
            this.m_FieldNode = null;

            this.m_TemplateFieldNode = PropertyEditorUtil.FindXmlNode(field.Name, this.m_TemplateParentNode);

            if (this.m_TemplateFieldNode != null &&
                this.m_TemplateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_Alias = this.m_TemplateFieldNode.Attributes["ExposeAs"].Value;
            }

            if (this.m_Alias != string.Empty)
            {
                this.m_FieldNode = PropertyEditorUtil.FindXmlNode(this.m_Alias, this.m_ParentNode);
            }
            ThreeState = CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue) == CheckState.Indeterminate;
            this.SetCheckboxStateFromXml();
        }

        private void SetCheckboxStateFromXml()
        {
            XmlNode fieldNode = null;
            if (this.m_Field != null)
            {
                fieldNode = this.m_FieldNode;
            }
            else if (this.m_TemplateFieldNode != null)
            {
                fieldNode = this.m_TemplateFieldNode;
            }

            if (fieldNode == null)
            {
                this.CheckState = CheckState.Unchecked;
            }
            else if (fieldNode.Attributes["overrideParent"] == null)
            {
                this.CheckState = CheckState.Unchecked;
            }
            else
            {
                if (fieldNode.Attributes["overrideParent"].Value == "yes")
                {
                    this.CheckState = CheckState.Checked;
                }
                else
                {
                    this.CheckState = CheckState.Unchecked;
                }
            }
        }

        public void UpdateNode()
        {
            if (this.m_FieldNode == null)
            {
                if (this.m_Alias == string.Empty)
                {
                    var takenAliases = this.m_Template.GetExposedFieldNames();

                    var tempAlias = this.m_Field.Name.ToLower();
                    var variation = 1;
                    while (takenAliases.Contains(tempAlias))
                    {
                        tempAlias = this.m_Field.Name.ToLower() + variation;
                        variation++;
                    }

                    this.m_Alias = tempAlias;
                }

                if (this.m_TemplateFieldNode == null)
                {
                    this.m_TemplateFieldNode = this.m_TemplateParentNode.OwnerDocument.CreateElement(this.m_Field.Name);
                    this.m_TemplateParentNode.AppendChild(this.m_TemplateFieldNode);
                    this.m_TemplateParentNode.InnerText = this.m_Field.DefaultValue;
                }

                ((XmlElement) this.m_TemplateFieldNode).SetAttribute("ExposeAs", this.m_Alias);

                this.m_FieldNode = this.m_ParentNode.OwnerDocument.CreateElement(this.m_Alias);
                this.m_FieldNode.InnerText = this.m_TemplateFieldNode.InnerText;
                this.m_ParentNode.AppendChild(this.m_FieldNode);
            }

            var overrideParent = this.m_FieldNode.Attributes["overrideParent"];
            if (overrideParent == null)
            {
                this.m_FieldNode.Attributes.Append(this.m_FieldNode.OwnerDocument.CreateAttribute("overrideParent"));
            }

            var val = (this.Checked ? "yes" : "no");
            if (this.m_FieldNode.Attributes["overrideParent"].Value != val)
            {
                this.m_FieldNode.Attributes["overrideParent"].Value = val;
                this.m_Object.MarkAsDirty();
            }
        }

        protected override void OnCheckStateChanged(EventArgs e)
        {
            this.valueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
            base.OnCheckStateChanged(e);
            this.UpdateNode();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter ||
                keyData == Keys.Space)
            {
                switch (this.CheckState)
                {
                    case CheckState.Checked:
                        this.CheckState = CheckState.Unchecked;
                        break;
                    case CheckState.Unchecked:
                        if (this.ThreeState)
                        {
                            this.CheckState = CheckState.Indeterminate;
                        }
                        else
                        {
                            this.CheckState = CheckState.Checked;
                        }
                        break;
                    case CheckState.Indeterminate:
                        this.CheckState = CheckState.Checked;
                        break;
                }
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}