namespace Rave.PropertiesEditor
{
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public class RowData: RowInfo
    {
        public RowData(IObjectInstance obj, IFieldDefinitionInstance fieldInstance, XmlNode parent, XmlNode node, XmlNode description)
            : base(description)
        {
            this.ObjectInstance = obj;
            this.FieldDefinitionInstance = fieldInstance;
            this.ParentNode = parent;
            this.Node = node;
        }

        public IObjectInstance ObjectInstance { get; private set; }

        public IFieldDefinitionInstance FieldDefinitionInstance { get; private set; }

        public XmlNode ParentNode { get; private set; }

        public XmlNode Node { get; private set; }
    }
}