namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    public partial class SliderEditor : UserControl
    {
        private bool m_bNumericUpDownUpdated;
        private string m_alias;
        private ISimpleFieldDefinitionInstance m_simpleFieldInstance;
        private XmlNode m_fieldNode;
        private decimal m_scalarNeg;
        private decimal m_scalarPos;
        private decimal m_defaultValue;

        public SliderEditor()
        {
            this.InitializeComponent();
        }

        public string Value
        {
            //return the numeric up/down to ensure accuracy. Slider is simply a graphical representation,
            //not accurate for anything that has to be scaled
            get
            {
                switch (this.m_simpleFieldInstance.Units)
                {
                    case "mB":
                    case "0.01units":
                    case "cents":
                        return ((int)(this.m_Numeric.Value * 100)).ToString();
                    default:
                        return ((int)this.m_Numeric.Value).ToString();
                }
            }
        }

        public void Setup(XmlNode parent, IFieldDefinitionInstance fieldInstance, string fieldAlias, bool draw)
        {
            this.m_TrackBar.Setup(draw);
	        this.m_TrackBar.TabStop = false;
            this.m_simpleFieldInstance = fieldInstance.AsSimpleField();
            this.m_alias = string.IsNullOrEmpty(fieldAlias) ? fieldInstance.Name : fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, parent);
            this.m_defaultValue = Convert.ToDecimal(fieldInstance.DefaultValue);
            this.Setup();
            this.lblUnits.Visible = (this.lblUnits.Text != "");
			m_Numeric.Select(0, m_Numeric.Text.Length);
         }

        private void Setup()
        {
            string val, units;
            //set up min/max
            if (this.m_simpleFieldInstance.Min !=
                this.m_simpleFieldInstance.Max)
            {
                this.m_TrackBar.Minimum = (int)this.m_simpleFieldInstance.Min;
                this.m_TrackBar.Maximum = (int)this.m_simpleFieldInstance.Max;
                PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_simpleFieldInstance.Units, out val, out units);
                this.m_Numeric.Maximum = Decimal.Parse(val);
                PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_simpleFieldInstance.Units, out val, out units);
                this.m_Numeric.Minimum = Decimal.Parse(val);
            }
            else if (this.m_simpleFieldInstance.Units == "ms")
            {
                this.m_TrackBar.Minimum = 0;
                this.m_TrackBar.Maximum = 65535;
                PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_simpleFieldInstance.Units, out val, out units);
                this.m_Numeric.Maximum = Decimal.Parse(val);
                PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_simpleFieldInstance.Units, out val, out units);
                this.m_Numeric.Minimum = Decimal.Parse(val);
            }
            else
            {
                switch (this.m_simpleFieldInstance.TypeName)
                {
                    case "s8":
                        this.m_TrackBar.Minimum = SByte.MinValue;
                        this.m_TrackBar.Maximum = SByte.MaxValue;
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                    case "u8":
                        this.m_TrackBar.Minimum = Byte.MinValue;
                        this.m_TrackBar.Maximum = Byte.MaxValue;
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                    case "s16":
                        this.m_TrackBar.Minimum = Int16.MinValue;
                        this.m_TrackBar.Maximum = Int16.MaxValue;
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                    case "u16":
                        this.m_TrackBar.Minimum = UInt16.MinValue;
                        this.m_TrackBar.Maximum = UInt16.MaxValue;
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                    case "u32":
                        this.m_TrackBar.Minimum = 0;
                        this.m_TrackBar.Maximum = 1000;
                        this.m_scalarPos = (decimal)UInt32.MaxValue / 1000;
                        PropertyEditorUtil.ConvertToDisplayValue(UInt32.MaxValue, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(UInt32.MinValue, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                    case "s32":
                    default:
                        this.m_TrackBar.Minimum = -1000;
                        this.m_scalarNeg = (decimal)Int32.MinValue / -1000;
                        this.m_TrackBar.Maximum = 1000;
                        this.m_scalarPos = (decimal)Int32.MaxValue / 1000;
                        PropertyEditorUtil.ConvertToDisplayValue(Int32.MaxValue, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(Int32.MinValue, this.m_simpleFieldInstance.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                }
            }

            this.lblUnits.Text = units;
            this.m_TrackBar.TickFrequency = 1;

            switch (this.m_simpleFieldInstance.Units)
            {
                case "ms":
                case "degrees":
                case null:
                    this.m_Numeric.DecimalPlaces = 0;
                    this.m_Numeric.Increment = 1;
                    this.m_TrackBar.TickFrequency = 1;
                    this.m_TrackBar.SmallChange = 1;
                    this.m_TrackBar.LargeChange = 5;
                    break;
                default:
                    this.m_Numeric.DecimalPlaces = 2;
                    this.m_Numeric.Increment = 0.01m;
                    this.m_TrackBar.SmallChange = 50;
                    this.m_TrackBar.LargeChange = 200;
                    break;
            }

            //Value
            int valueIn;

            if (this.m_fieldNode != null)
            {
                valueIn = Int32.Parse(this.m_fieldNode.InnerText);
            }
            else if (this.m_simpleFieldInstance.DefaultValue != null)
            {
                valueIn = Int32.Parse(this.m_simpleFieldInstance.DefaultValue);
            }
            else
            {
                valueIn = 0;
            }

            string valueOut;
            PropertyEditorUtil.ConvertToDisplayValue(valueIn, this.m_simpleFieldInstance.Units, out valueOut, out units);
            this.m_Numeric.Value = Decimal.Parse(valueOut);
            this.lblUnits.Text = units;
            this.UpDateValue();
			
        }


        protected virtual void m_TrackBar_OnValueChanged(object sender, EventArgs e)
        {
            if (!this.m_bNumericUpDownUpdated)
            {
                string display, units;

                PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Value, this.m_simpleFieldInstance.Units, out display, out units);
                var val = Decimal.Parse(display);
                this.lblUnits.Text = units;

                if (this.m_scalarPos != 0 &&
                    val >= 0)
                {
                    val = val * this.m_scalarPos;
                }
                else if (this.m_scalarNeg != 0 &&
                         val < 0)
                {
                    val = val * this.m_scalarNeg;
                }

                if (val != this.m_Numeric.Value)
                {
                    if (val < this.m_Numeric.Minimum)
                    {
                        val = this.m_Numeric.Minimum;
                    }
                    if (val > this.m_Numeric.Maximum)
                    {
                        val = this.m_Numeric.Maximum;
                    }

                    this.m_Numeric.Value = val;
                }
                this.lblUnits.Visible = (this.lblUnits.Text != "");
            }

            this.m_bNumericUpDownUpdated = false;
        }

        public void UpDateValue()
        {
            var numericValue = this.m_Numeric.Value;


            if (this.m_scalarPos != 0 &&
                numericValue >= 0)
            {
                numericValue = numericValue / this.m_scalarPos;
            }
            else if (this.m_scalarNeg != 0 &&
                     numericValue <= 0)
            {
                numericValue = numericValue / this.m_scalarNeg;
            }

            if (string.IsNullOrEmpty(this.m_Numeric.Text))
            {
                numericValue = this.m_defaultValue;
                
                if (this.m_scalarPos != 0 &&
                numericValue >= 0)
                {
                    this.m_Numeric.Value = numericValue * this.m_scalarPos;
                }
                else if (this.m_scalarNeg != 0 &&
                         numericValue <= 0)
                {
                    this.m_Numeric.Value = numericValue * this.m_scalarNeg;
                }
                else
                {
                    this.m_Numeric.Value = numericValue;
                }
            }

            var val = PropertyEditorUtil.ConvertFromDisplayValue(numericValue.ToString(), this.m_simpleFieldInstance.Units);

            if (val != this.m_TrackBar.Value)
            {
                //this bool is set to ensure that when the track bar is updated it then doesn't update the 
                //numeric up down again. We do this as the slider is giving a graphical representation of the number
                //and is not necessarily accurate
                this.m_bNumericUpDownUpdated = true;
                this.m_TrackBar.Value = val;
            }

        }

	   
    }
}