namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Instance;
    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.PropertiesEditor.Infrastructure.Interfaces;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    internal class ctrlTemplateObjectRefCell : DataGridViewLinkCell,
                                               ITemplateFieldCell,
                                               IObjectRef
    {
        private string m_alias;
        private IFieldDefinition m_field;
        private XmlNode m_fieldNode;
        private ContextMenuStrip m_menu;

        private IObjectInstance m_object;
        private XmlNode m_parentNode;
        private IObjectInstance m_parentObject;
        private ITemplate m_template;
        private XmlNode m_templateFieldNode;
        private IObjectInstance m_templateObject;
        private XmlNode m_templateParentNode;
        private IObjectInstance m_templateParentObject;

        #region IObjectRef Members

        public void Drop(WaveNode wn)
        {
            if (!this.m_templateParentObject.IsReadOnly)
            {
                this.SetSoundReferenceToNewWaveWrapperSound(wn);
            }
        }

        public void Drop(SoundNode sn)
        {
            if (!this.m_templateParentObject.IsReadOnly)
            {
                this.SetSoundReference(sn.ObjectInstance);
            }
        }

        #endregion

        #region ITemplateFieldCell Members

        public bool OverridesTemplate
        {
            get { return (this.m_fieldNode != null && this.m_fieldNode.InnerText != this.m_templateFieldNode.InnerText); }
        }

        public bool IsDefaultValue
        {
            get
            {
                return (this.m_fieldNode == null && this.m_templateFieldNode == null ||
                        this.m_fieldNode == null && this.m_field.DefaultValue == this.m_templateFieldNode.InnerText ||
                        this.m_fieldNode != null && this.m_fieldNode.InnerText == this.m_field.DefaultValue);
            }
        }

        public TemplateCellType Type
        {
            get { return TemplateCellType.OBJECTREF; }
        }

        #endregion

        public event Action<IObjectInstance> OnObjectRefClick;

        public void Init(ITemplate template,
                         IObjectInstance o,
                         IObjectInstance templateObject,
                         XmlNode parentNode,
                         XmlNode templateParentNode,
                         IFieldDefinition field)
        {
            this.m_template = template;

            this.m_object = null;
            this.m_parentObject = o;
            this.m_templateObject = null;
            this.m_templateParentObject = templateObject;
            this.m_templateParentNode = templateParentNode;
            this.m_parentNode = parentNode;

            this.m_field = field;
            this.m_fieldNode = null;
            this.m_alias = string.Empty;
            this.Value = "null";

            this.m_templateFieldNode = this.m_templateParentNode.Name !=
                                  this.m_field.Name ? PropertyEditorUtil.FindXmlNode(field.Name, this.m_templateParentNode) : templateParentNode;

            if (this.m_templateFieldNode != null &&
                this.m_templateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_alias = this.m_templateFieldNode.Attributes["ExposeAs"].Value;
            }

            if (this.m_alias != string.Empty)
            {
                this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            }

            if (this.m_fieldNode != null)
            {
                foreach (var sr in this.m_parentObject.References)
                {
                    if (sr.Node == this.m_fieldNode)
                    {
                        this.m_object = sr.ObjectInstance;
                        this.Value = this.m_object.Name;
                        break;
                    }
                }
            }
            else if (this.m_templateFieldNode != null)
            {
                foreach (var sr in this.m_templateParentObject.References)
                {
                    if (sr.Node == this.m_templateFieldNode)
                    {
                        this.m_templateObject = sr.ObjectInstance;
                        this.Value = this.m_templateObject.Name;
                        break;
                    }
                }
            }

            if (this.m_menu == null)
            {
                this.m_menu = new ContextMenuStrip();
                this.m_menu.Items.Add(new ToolStripMenuItem("Remove", null, new EventHandler(this.OnRemoveClicked)));

                this.ContextMenuStrip = this.m_menu;
                this.ContextMenuStrip.Opening += this.ContextMenuStrip_Opening;
            }
        }

        private void ContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (this.m_object == null)
            {
                e.Cancel = true;
            }
        }

        protected override void OnClick(DataGridViewCellEventArgs e)
        {
            base.OnClick(e);

            if (this.OnObjectRefClick != null)
            {
                this.OnObjectRefClick(this.m_object);
            }
        }

        private void OnRemoveClicked(object sender, EventArgs e)
        {
            if (this.m_object != null)
            {
                this.m_object.RemoveReferencer(this.m_parentObject);
                this.m_parentObject.RemoveReference(this.m_object, this.m_fieldNode);
                var directChild = this.m_parentObject.Node.ChildNodes.Cast<XmlNode>().Any(n => n == this.m_fieldNode);
                this.m_parentObject.Node.RemoveChild(directChild ? this.m_fieldNode : this.m_fieldNode.ParentNode);

                this.m_parentObject.ComputeReferences();
                this.m_parentObject.MarkAsDirty();
                this.m_object = null;
                this.m_fieldNode = null;

                this.Value = this.m_templateFieldNode.InnerText;
            }
        }

        private void SetSoundReferenceToNewWaveWrapperSound(WaveNode wave)
        {
            var newSound = TypeUtils.CreateWrapperSound(this.m_templateParentObject.Bank,
                                                      Configuration.WaveWrapperSoundType,
                                                      wave);
            TypeUtils.ReportNewObjectCreated(newSound);
            this.SetSoundReference(newSound);
        }

        private void SetSoundReference(IObjectInstance newSound)
        {
            if (this.m_field.AllowedType != null)
            {
                var allow = false;
                ITypeDefinition baseType;
                var type = newSound.TypeName;
                var inheritsFrom = type;

                while (inheritsFrom != null || allow)
                {
                    if (this.m_field.AllowedType.ToUpper() ==
                        inheritsFrom.ToUpper())
                    {
                        allow = true;
                        break;
                    }
                    baseType = newSound.TypeDefinitions.FindTypeDefinition(inheritsFrom);
                    inheritsFrom = baseType.InheritsFrom;
                }

                if (!allow)
                {
                    var sb = new StringBuilder();
                    sb.Append("Invalid child type. Child must be of type: ");
                    sb.Append(this.m_field.AllowedType);
                    MessageBox.Show(sb.ToString());
                    return;
                }
            }
            //allow sound refs and game objects
            if (this.m_parentObject.CanAddReferences(newSound))
            {
                var index = Int32.MaxValue;
                if (this.m_object != null)
                {
                    this.m_object.RemoveReferencer(this.m_parentObject);
                    index = this.m_parentObject.FindReferenceListIndex(this.m_object, this.m_fieldNode);
                    this.m_parentObject.RemoveReference(this.m_object, this.m_fieldNode);
                }
                else if (this.m_templateObject != null)
                {
                    this.m_templateObject.RemoveReferencer(this.m_parentObject);
                    index = this.m_templateParentObject.FindReferenceListIndex(this.m_templateObject, this.m_templateFieldNode);
                    this.m_parentObject.RemoveReference(this.m_templateObject, this.m_templateFieldNode);
                }

                this.m_object = newSound;

                if (this.m_fieldNode == null)
                {
                    if (this.m_alias == string.Empty)
                    {
                        var takenAliases = this.m_template.GetExposedFieldNames();

                        var tempAlias = this.m_field.Name.ToLower();
                        var variation = 1;
                        while (takenAliases.Contains(tempAlias))
                        {
                            tempAlias = this.m_field.Name.ToLower() + variation;
                            variation++;
                        }

                        this.m_alias = tempAlias;
                    }

                    if (this.m_templateFieldNode == null)
                    {
                        this.m_templateFieldNode = this.m_templateParentNode.OwnerDocument.CreateElement(this.m_field.Name);
                        this.m_templateParentNode.AppendChild(this.m_templateFieldNode);
                        this.m_templateParentNode.InnerText = this.m_field.DefaultValue;
                    }

                    ((XmlElement) this.m_templateFieldNode).SetAttribute("ExposeAs", this.m_alias);

                    this.m_fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_alias);
                    this.m_parentNode.AppendChild(this.m_fieldNode);
                }
                //add at the correct index so it will appear in the correct order in the heirarchy
                if (index != Int32.MaxValue)
                {
                    this.m_parentObject.AddReference(this.m_object, this.m_fieldNode, index);
                }
                    //couldn't find index of child
                else
                {
                    this.m_parentObject.AddReference(this.m_object, this.m_fieldNode);
                }

                this.m_object.AddReferencer(this.m_parentObject);

                this.m_fieldNode.InnerText = this.m_object.Name;
                this.m_templateParentObject.MarkAsDirty();
                this.m_parentObject.MarkAsDirty();
                this.Value = this.m_object.Name;
            }
            else
            {
                MessageBox.Show("Invalid child");
            }
        }

        protected override void Dispose(bool disposing)
        {
            this.m_object = null;
            this.m_parentNode = null;
            this.m_field = null;
            this.m_fieldNode = null;
            this.ContextMenuStrip.Opening -= this.ContextMenuStrip_Opening;
            this.ContextMenuStrip = null;
            base.Dispose(disposing);
        }
    }
}