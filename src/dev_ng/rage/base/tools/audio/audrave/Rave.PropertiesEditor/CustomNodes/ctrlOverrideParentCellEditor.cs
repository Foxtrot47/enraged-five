namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    internal class OverrideParentEditor : CheckBox,
                                          IDataGridViewEditingControl
    {
        private string m_alias;
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_objectInstance;
        private XmlNode m_parentNode;
        private bool m_valueChanged;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.CheckState; }
            set { return; }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            // Let the DateTimePicker handle the keys listed.
            switch (key & Keys.KeyCode)
            {
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Right:
                case Keys.Home:
                case Keys.End:
                case Keys.PageDown:
                case Keys.PageUp:
                    return true;
                default:
                    return false;
            }
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged
        {
            get { return this.m_valueChanged; }
            set { this.m_valueChanged = value; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance,
                         XmlNode parentNode,
                         IFieldDefinitionInstance fieldInstance,
                         string fieldAlias)
        {
            this.m_objectInstance = objectInstance;
            this.m_parentNode = parentNode;
            this.m_fieldInstance = fieldInstance;
            this.m_alias = fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            this.SetCheckboxStateFromXml();
            ThreeState = CheckBoxHelper.GetIndeterminateValue(m_fieldInstance.DefaultValue) == CheckState.Indeterminate;
        }

        private void SetCheckboxStateFromXml()
        {
            var fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            if (fieldNode == null)
            {
                this.CheckState = CheckState.Unchecked;
            }
            else if (fieldNode.Attributes["overrideParent"] == null)
            {
                this.CheckState = CheckState.Unchecked;
            }
            else
            {
                this.CheckState = fieldNode.Attributes["overrideParent"].Value == "yes"
                                 ? CheckState.Checked
                                 : CheckState.Unchecked;
            }
        }

        public void UpdateNode()
        {
            if (this.m_fieldNode == null)
            {
                this.m_fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_alias);
                this.m_fieldNode.InnerText = this.m_fieldInstance.DefaultValue;
                this.m_parentNode.AppendChild(this.m_fieldNode);
            }

            var overrideParent = this.m_fieldNode.Attributes["overrideParent"];
            if (overrideParent == null)
            {
                this.m_fieldNode.Attributes.Append(this.m_fieldNode.OwnerDocument.CreateAttribute("overrideParent"));
            }

            var val = (this.Checked ? "yes" : "no");
            if (this.m_fieldNode.Attributes["overrideParent"].Value != val)
            {
                this.m_fieldNode.Attributes["overrideParent"].Value = val;
                this.m_objectInstance.MarkAsDirty();
            }
        }

        protected override void OnCheckStateChanged(EventArgs e)
        {
            this.m_valueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
            base.OnCheckStateChanged(e);
            this.UpdateNode();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter ||
                keyData == Keys.Space)
            {
                switch (this.CheckState)
                {
                    case CheckState.Checked:
                        this.CheckState = CheckState.Unchecked;
                        break;
                    case CheckState.Unchecked:
                        if (this.ThreeState)
                        {
                            this.CheckState = CheckState.Indeterminate;
                        }
                        else
                        {
                            this.CheckState = CheckState.Checked;
                        }
                        break;
                    case CheckState.Indeterminate:
                        this.CheckState = CheckState.Checked;
                        break;
                }
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}