
using System;

namespace Rave.PropertiesEditor.CustomNodes
{
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Structs;

    [DesignerCategory("code")]
    internal class CategoryRefEditor : ComboBox,
                                       IDataGridViewEditingControl
    {
        private string m_alias;
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_object;
        private XmlNode m_parentNode;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.Text; }
            set
            {
                //we set value via xml so ignore
                return;
            }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
            //done in init
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance,
                         XmlNode parentNode,
                         IFieldDefinitionInstance fieldInstance,
                         string fieldAlias)
        {
            this.m_object = objectInstance;
            this.m_parentNode = parentNode;
            this.m_fieldInstance = fieldInstance;
            this.m_alias = string.IsNullOrEmpty(fieldAlias) ? fieldInstance.Name : fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            this.Sorted = true;
            this.Items.Clear();
            var categoryBase = new ObjectLookupId("CATEGORIES", ObjectLookupId.Base);
            foreach (var kvp in RaveInstance.ObjectLookupTables[categoryBase])
            {
                this.Items.Add(kvp.Key);
            }
            this.SelectedItem = null;
            this.Text = this.m_fieldNode != null ? this.m_fieldNode.InnerText : this.m_fieldInstance.DefaultValue;
        }

        public void UpdateNode()
        {
            if (this.m_fieldNode == null)
            {
                this.m_fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_alias);
                this.m_parentNode.AppendChild(this.m_fieldNode);
            }
            if (this.SelectedItem != null &&
                this.Text != string.Empty)
            {
                this.m_fieldNode.InnerText = this.SelectedItem.ToString();
            }
            else
            {
                this.m_fieldNode.InnerText = this.Text;
            }

            this.m_object.MarkAsDirty();
            this.m_object.ComputeReferences();
        }

        //do nothing on return, stops the row jumping
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.UpdateNode();
                return false;
            }
            if (keyData == (Keys.Control | Keys.C) && !string.IsNullOrEmpty(this.Text))
            {
                Clipboard.SetText(this.Text);
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        protected override void OnTextChanged(System.EventArgs e)
        {
            this.UpdateNode();
            base.OnTextChanged(e);
        }

        protected override void OnDropDownClosed(EventArgs e)
        {
            base.OnDropDownClosed(e);
            this.UpdateNode();
            base.OnTextChanged(e);
        }
    }
}