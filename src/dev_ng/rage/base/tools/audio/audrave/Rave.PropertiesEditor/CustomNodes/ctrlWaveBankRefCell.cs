﻿namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.Windows.Forms;

    internal class ctrlWaveBankRefCell : ctrlWaveBaseRefCell, IFieldCell
    {
        public CellType Type
        {
            get { return CellType.BANKREF; }
        }

        public event Action<string> OnWaveBankRefClick;

        public override void SetValue()
        {
            if (this.m_fieldNode == null ||
                !this.m_fieldNode.HasChildNodes)
            {
                this.Value = "not set";
            }
            else
            {
                if (this.m_fieldInstance.Units == "BankRef")
                {
                    this.m_bankPath = this.m_fieldNode.InnerText;
                }
                else if (this.m_fieldNode.ChildNodes[0].Name == "WaveName")
                {
                    this.m_bankPath = this.m_fieldNode.ChildNodes[1].InnerText;
                }
                else if (this.m_fieldNode.ChildNodes[0].Name == "BankName")
                {
                    this.m_bankPath = this.m_fieldNode.ChildNodes[0].InnerText;
                }

                this.Value = this.m_bankPath;
            }
        }

        protected override void OnClick(DataGridViewCellEventArgs e)
        {
            base.OnClick(e);
            if (this.OnWaveBankRefClick != null)
            {
                this.OnWaveBankRefClick(this.m_bankPath);
            }
        }
    }
}
