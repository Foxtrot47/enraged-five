namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    public interface ITemplateFieldCell
    {
        bool IsDefaultValue { get; }

        bool OverridesTemplate { get; }

        TemplateCellType Type { get; }
    }
}