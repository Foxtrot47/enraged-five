namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public class ctrlVariableRefCell : DataGridViewTextBoxCell,
                                       IFieldCell
    {
        [ThreadStatic] private static Bitmap ms_renderingBitmap;
        private string m_alias;
        private bool m_bDraw;
        private ComboBox m_comboBox;
        private VariableRefEditor m_editor;
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_objectInstance;
        private XmlNode m_parentNode;

        public override Type EditType
        {
            get
            {
                // Return the type of the editing contol that CalendarCell uses.
                return typeof (VariableRefEditor);
            }
        }

        public override Type ValueType
        {
            get { return typeof (string); }
        }

        public override object DefaultNewRowValue
        {
            get { return ""; }
        }

        #region IFieldCell Members

        public bool IsDefaultValue
        {
            get { return (this.m_fieldNode == null || this.m_fieldInstance.DefaultValue == this.m_fieldNode.InnerText); }
        }

        public CellType Type
        {
            get { return CellType.VARREF; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance,
                         XmlNode parent,
                         IFieldDefinitionInstance fieldInstance,
                         string fieldAlias,
                         bool draw)
        {
            this.m_objectInstance = objectInstance;
            this.m_parentNode = parent;
            this.m_fieldInstance = fieldInstance;
            this.m_bDraw = draw;
            this.m_alias = string.IsNullOrEmpty(fieldAlias) ? fieldInstance.Name : fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(fieldInstance.Name, parent);

            if (ms_renderingBitmap == null)
            {
                ms_renderingBitmap = new Bitmap(200, 30);
            }
            this.m_comboBox = new ComboBox();
            this.Value = this.GetValue();
        }

        private string GetValue()
        {
            if (this.m_fieldNode == null)
            {
                this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            }
            if (this.m_fieldNode != null)
            {
                return this.m_fieldNode.InnerText;
            }
            return this.m_fieldInstance.DefaultValue;
        }

        public override void InitializeEditingControl(int rowIndex,
                                                      object initialFormattedValue,
                                                      DataGridViewCellStyle dataGridViewCellStyle)
        {
            // Set the value of the editing control to the current cell value.
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
            this.m_editor = this.DataGridView.EditingControl as VariableRefEditor;
            this.m_editor.Init(this.m_objectInstance, this.m_parentNode, this.m_fieldInstance, this.m_alias);
        }

        private static bool PartPainted(DataGridViewPaintParts paintParts, DataGridViewPaintParts paintPart)
        {
            return (paintParts & paintPart) != 0;
        }

        protected override void Paint(Graphics graphics,
                                      Rectangle clipBounds,
                                      Rectangle cellBounds,
                                      int rowIndex,
                                      DataGridViewElementStates cellState,
                                      object value,
                                      object formattedValue,
                                      string errorText,
                                      DataGridViewCellStyle cellStyle,
                                      DataGridViewAdvancedBorderStyle advancedBorderStyle,
                                      DataGridViewPaintParts paintParts)
        {
            if (!this.m_bDraw)
            {
                base.Paint(graphics,
                    clipBounds,
                    cellBounds,
                    rowIndex,
                    cellState,
                    value,
                    formattedValue,
                    errorText,
                    cellStyle,
                    advancedBorderStyle,
                    paintParts);
            }
            else
            {
                if (this.DataGridView == null)
                {
                    return;
                }

                // First paint the borders and background of the cell.
                base.Paint(graphics,
                    clipBounds,
                    cellBounds,
                    rowIndex,
                    cellState,
                    value,
                    formattedValue,
                    errorText,
                    cellStyle,
                    advancedBorderStyle,
                    paintParts & ~(DataGridViewPaintParts.ErrorIcon | DataGridViewPaintParts.ContentForeground));

                var ptCurrentCell = this.DataGridView.CurrentCellAddress;
                var cellCurrent = ptCurrentCell.X == this.ColumnIndex && ptCurrentCell.Y == rowIndex;
                var cellEdited = cellCurrent && this.DataGridView.EditingControl != null;

                // If the cell is in editing mode, there is nothing else to paint
                if (!cellEdited)
                {
                    if (PartPainted(paintParts, DataGridViewPaintParts.ContentForeground))
                    {
                        // Paint a NumericUpDown control
                        // Take the borders into account
                        var borderWidths = this.BorderWidths(advancedBorderStyle);
                        var valBounds = cellBounds;
                        valBounds.Offset(borderWidths.X, borderWidths.Y);
                        valBounds.Width -= borderWidths.Right;
                        valBounds.Height -= borderWidths.Bottom;
                        // Also take the padding into account
                        if (cellStyle.Padding !=
                            Padding.Empty)
                        {
                            valBounds.Offset(
                                this.DataGridView.RightToLeft == RightToLeft.Yes
                                    ? cellStyle.Padding.Right
                                    : cellStyle.Padding.Left,
                                cellStyle.Padding.Top);
                            valBounds.Width -= cellStyle.Padding.Horizontal;
                            valBounds.Height -= cellStyle.Padding.Vertical;
                        }
                        // Determine the control location
                        valBounds = this.GetAdjustedEditingControlBounds(valBounds, cellStyle);

                        var cellSelected = (cellState & DataGridViewElementStates.Selected) != 0;

                        if (ms_renderingBitmap.Width < valBounds.Width ||
                            ms_renderingBitmap.Height < valBounds.Height)
                        {
                            // The static bitmap is too small, a bigger one needs to be allocated.
                            ms_renderingBitmap.Dispose();
                            ms_renderingBitmap = new Bitmap(valBounds.Width, valBounds.Height);
                        }
                        if (this.m_comboBox.Parent != this.DataGridView)
                        {
                            this.m_comboBox.Parent = this.DataGridView;
                        }

                        // Set all the relevant properties
                        this.m_comboBox.Width = valBounds.Width;
                        this.m_comboBox.RightToLeft = this.DataGridView.RightToLeft;
                        this.m_comboBox.Location = new Point(0, -this.m_comboBox.Height - 100);

                        this.m_comboBox.Text = this.GetValue();

                        Color backColor;
                        if (PartPainted(paintParts, DataGridViewPaintParts.SelectionBackground) && cellSelected)
                        {
                            backColor = cellStyle.SelectionBackColor;
                        }
                        else
                        {
                            backColor = cellStyle.BackColor;
                        }
                        if (PartPainted(paintParts, DataGridViewPaintParts.Background))
                        {
                            this.m_comboBox.BackColor = Color.White;
                        }
                        // Finally paint the control
                        var srcRect = new Rectangle(0, 0, valBounds.Width, valBounds.Height);
                        if (srcRect.Width > 0 &&
                            srcRect.Height > 0)
                        {
                            this.m_comboBox.DrawToBitmap(ms_renderingBitmap, srcRect);
                            graphics.DrawImage(ms_renderingBitmap,
                                new Rectangle(valBounds.Location, valBounds.Size),
                                srcRect,
                                GraphicsUnit.Pixel);
                        }
                    }
                    if (PartPainted(paintParts, DataGridViewPaintParts.ErrorIcon))
                    {
                        // Paint the potential error icon on top of the control
                        base.Paint(graphics,
                            clipBounds,
                            cellBounds,
                            rowIndex,
                            cellState,
                            value,
                            formattedValue,
                            errorText,
                            cellStyle,
                            advancedBorderStyle,
                            DataGridViewPaintParts.ErrorIcon);
                    }
                }
            }
        }

        public override void PositionEditingControl(bool setLocation,
                                                    bool setSize,
                                                    Rectangle cellBounds,
                                                    Rectangle cellClip,
                                                    DataGridViewCellStyle cellStyle,
                                                    bool singleVerticalBorderAdded,
                                                    bool singleHorizontalBorderAdded,
                                                    bool isFirstDisplayedColumn,
                                                    bool isFirstDisplayedRow)
        {
            var editingControlBounds = this.PositionEditingPanel(cellBounds,
                cellClip,
                cellStyle,
                singleVerticalBorderAdded,
                singleHorizontalBorderAdded,
                isFirstDisplayedColumn,
                isFirstDisplayedRow);
            editingControlBounds = this.GetAdjustedEditingControlBounds(editingControlBounds, cellStyle);
            this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
            this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
        }

        private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
        {
            // Add a 1 pixel padding on the left and right of the editing control
            editingControlBounds.X += 1;
            editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);

            // Adjust the vertical location of the editing control:
            var preferredHeight = this.m_comboBox.Height;
            if (preferredHeight < editingControlBounds.Height)
            {
                switch (cellStyle.Alignment)
                {
                    case DataGridViewContentAlignment.MiddleLeft:
                    case DataGridViewContentAlignment.MiddleCenter:
                    case DataGridViewContentAlignment.MiddleRight:
                        editingControlBounds.Y += (editingControlBounds.Height - preferredHeight) / 2;
                        break;
                    case DataGridViewContentAlignment.BottomLeft:
                    case DataGridViewContentAlignment.BottomCenter:
                    case DataGridViewContentAlignment.BottomRight:
                        editingControlBounds.Y += editingControlBounds.Height - preferredHeight;
                        break;
                }
            }

            return editingControlBounds;
        }

        public override void DetachEditingControl()
        {
            this.m_editor.UpdateNode();
            base.DetachEditingControl();
            this.Value = this.GetValue();
        }

        protected override void Dispose(bool disposing)
        {
            this.m_objectInstance = null;
            this.m_parentNode = null;
            this.m_fieldNode = null;
            this.m_fieldInstance = null;
            this.m_editor = null;
            base.Dispose(disposing);
        }
    }
}