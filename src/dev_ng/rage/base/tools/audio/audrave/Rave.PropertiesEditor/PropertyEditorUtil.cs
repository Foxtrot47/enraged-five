// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PropertyEditorUtil.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for PropertyEditorUtil.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.PropertiesEditor
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Xml;
    
    /// <summary>
    /// The property editor utility.
    /// </summary>
    public class PropertyEditorUtil
    {
        #region Public Methods and Operators

        /// <summary>
        /// The convert from display value.
        /// </summary>
        /// <param name="displayVal">
        /// The display val.
        /// </param>
        /// <param name="units">
        /// The units.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public static int ConvertFromDisplayValue(string displayVal, string units)
        {
            float actualVal = float.Parse(displayVal);

            if (units != null)
            {
                switch (units.ToLower())
                {
                    case "cents":
                    case "0.01units":
                    case "mb":
                        actualVal *= 100.0f;
                        break;
                }
            }

            return Convert.ToInt32(actualVal);
        }

        /// <summary>
        /// The convert to display value.
        /// </summary>
        /// <param name="val">
        /// The val.
        /// </param>
        /// <param name="units">
        /// The units.
        /// </param>
        /// <param name="displayVal">
        /// The display val.
        /// </param>
        /// <param name="displayUnits">
        /// The display units.
        /// </param>
        public static void ConvertToDisplayValue(
            decimal val, string units, out string displayVal, out string displayUnits)
        {
            decimal actualVal = val;
            displayUnits = string.Empty;

            if (units != null)
            {
                switch (units.ToLower())
                {
                    case "mb":
                        actualVal /= 100;
                        displayUnits = "dB";
                        break;
                    case "0.01units":
                        actualVal /= 100;
                        break;
                    case "cents":
                        actualVal /= 100;
                        displayUnits = "st";
                        break;
                    case "ms":
                        displayUnits = "ms";
                        break;
                    case "degrees":
                        if (val == -1)
                        {
                            displayUnits = string.Empty;
                        }
                        else
                        {
                            displayUnits = "\x00B0";
                        }

                        break;
                    default:
                        displayUnits = units;
                        break;
                }
            }

            displayVal = actualVal.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// The find xml node.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        public static XmlNode FindXmlNode(string name, XmlNode parent)
        {
            return parent == null ? null : parent.ChildNodes.Cast<XmlNode>().FirstOrDefault(n => n.Name == name);
        }

        #endregion
    }
}