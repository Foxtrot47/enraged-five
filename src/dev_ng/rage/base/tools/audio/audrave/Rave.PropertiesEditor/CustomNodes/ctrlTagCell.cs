using System.Xml;
using Rave.TypeDefinitions.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.Objects;

namespace Rave.PropertiesEditor.CustomNodes
{
	using System;
	using System.ComponentModel;
	using System.Windows.Forms;

	[DesignerCategory("code")]
	public class ctrlTagCell : DataGridViewCell
	{
	
		public override object DefaultNewRowValue
		{
			get { return string.Empty; }
		}

		public ctrlTagControl EditingControl { get; private set; }
		
		public override Type ValueType
		{
			get { return typeof(string); }
		}

		public ctrlTagCell(IObjectInstance objectInstance, string fieldAlias, XmlNode node)
		{
			this.Value = "tags";
			EditingControl = new ctrlTagControl();
			EditingControl.Init(objectInstance, fieldAlias, node);
		}
		public override void  InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
		{
			base.InitializeEditingControl(rowIndex, initialFormattedValue,
				dataGridViewCellStyle);
			DetachEditingControl();
		}

		protected override void OnDataGridViewChanged()
		{
			SetUpEditingControl();
		}

		public void SetUpEditingControl()
		{
			if (this.DataGridView != null && this.RowIndex > -1 && !DataGridView.Controls.Contains(EditingControl))
			{
				this.DataGridView.Controls.Add(EditingControl);
			}
		}
	}
}