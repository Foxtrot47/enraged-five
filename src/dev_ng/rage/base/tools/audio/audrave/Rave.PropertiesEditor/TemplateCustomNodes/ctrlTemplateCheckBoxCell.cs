using Rave.PropertiesEditor.CustomNodes;

namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;

    [DesignerCategory("code")]
    public class ctrlTemplateCheckBoxCell : DataGridViewCheckBoxCell,
                                            ITemplateFieldCell
    {
        private string m_Alias;
        private TemplateCheckBoxEditor m_Editor;
        private IFieldDefinition m_Field;
        private XmlNode m_FieldNode;
        private ITypedObjectInstance m_Object;
        private XmlNode m_ParentNode;
        private ITemplate m_Template;
        private XmlNode m_TemplateFieldNode;
        private ITypedObjectInstance m_TemplateObject;

        private XmlNode m_TemplateParentNode;

        public override Type EditType
        {
            get { return typeof (TemplateCheckBoxEditor); }
        }

        public override Type ValueType
        {
            get { return typeof (CheckState); }
        }

        public override object DefaultNewRowValue
        {
            get { return false; }
        }

        #region ITemplateFieldCell Members

        public TemplateCellType Type
        {
            get { return TemplateCellType.CHECKBOX; }
        }

        public bool OverridesTemplate
        {
            get { return (this.m_FieldNode != null && this.m_FieldNode.InnerText != this.m_TemplateFieldNode.InnerText); }
        }

        public bool IsDefaultValue
        {
            get
            {
                return (this.m_FieldNode == null && this.m_TemplateFieldNode == null ||
                        this.m_FieldNode == null && this.m_TemplateFieldNode.InnerText == this.m_Field.DefaultValue ||
                        this.m_FieldNode != null && this.m_FieldNode.InnerText == this.m_Field.DefaultValue);
            }
        }

        #endregion

        public void Init(ITemplate template,
                         ITypedObjectInstance o,
                         ITypedObjectInstance templateObject,
                         XmlNode parentNode,
                         XmlNode templateParentNode,
                         IFieldDefinition field)
        {
            this.m_Template = template;

            this.m_Object = o;
            this.m_TemplateObject = templateObject;

            this.m_ParentNode = parentNode;
            this.m_TemplateParentNode = templateParentNode;

            this.m_Field = field;
            this.m_FieldNode = null;
            this.m_Alias = string.Empty;

            this.ThreeState = (this.m_Field.TypeName == "tristate") && CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue) == CheckState.Indeterminate;
            this.Value = this.GetValue();
        }

        private CheckState GetValue()
        {
            this.m_TemplateFieldNode = PropertyEditorUtil.FindXmlNode(this.m_Field.Name, this.m_TemplateParentNode);

            this.m_Alias = string.Empty;
            if (this.m_TemplateFieldNode != null &&
                this.m_TemplateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_Alias = this.m_TemplateFieldNode.Attributes["ExposeAs"].Value;
            }

            if (this.m_Alias != string.Empty)
            {
                this.m_FieldNode = PropertyEditorUtil.FindXmlNode(this.m_Alias, this.m_ParentNode);
            }

            
           if (this.m_Field.TypeName == "tristate")
            {
                this.ThreeState = true;
                if (this.m_FieldNode == null)
                {
                    if (this.m_TemplateFieldNode == null)
                    {
                        return CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue);
                    }
                    else if (this.m_TemplateFieldNode.InnerText == "yes")
                    {
                        return CheckState.Checked;
                    }
                    else
                    {
                        return CheckState.Unchecked;
                    }
                }
                else if (this.m_FieldNode.InnerText == "yes")
                {
                    return CheckState.Checked;
                }
                else
                {
                    return CheckState.Unchecked;
                }
            }
            else
            {
                this.ThreeState = false;
                if (this.m_FieldNode == null)
                {
                    if (this.m_TemplateFieldNode == null)
                    {
                        if (this.m_Field.DefaultValue == "yes")
                        {
                            return CheckState.Checked;
                        }
                        else
                        {
                            return CheckState.Unchecked;
                        }
                    }
                    else
                    {
                        if (this.m_TemplateFieldNode.InnerText == "yes")
                        {
                            return CheckState.Checked;
                        }
                        else
                        {
                            return CheckState.Unchecked;
                        }
                    }
                }
                else
                {
                    if (this.m_FieldNode.InnerText == "yes")
                    {
                        return CheckState.Checked;
                    }
                    else
                    {
                        return CheckState.Unchecked;
                    }
                }
            }
        }

        public override void InitializeEditingControl(int rowIndex,
                                                      object initialFormattedValue,
                                                      DataGridViewCellStyle dataGridViewCellStyle)
        {
            // Set the value of the editing control to the current cell value.
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
            this.m_Editor = this.DataGridView.EditingControl as TemplateCheckBoxEditor;
            this.m_Editor.Init(this.m_Template, this.m_Object, this.m_TemplateObject, this.m_ParentNode, this.m_TemplateParentNode, this.m_Field);
        }

        private static bool PartPainted(DataGridViewPaintParts paintParts, DataGridViewPaintParts paintPart)
        {
            return (paintParts & paintPart) != 0;
        }

        protected override void Paint(Graphics graphics,
                                      Rectangle clipBounds,
                                      Rectangle cellBounds,
                                      int rowIndex,
                                      DataGridViewElementStates cellState,
                                      object value,
                                      object formattedValue,
                                      string errorText,
                                      DataGridViewCellStyle cellStyle,
                                      DataGridViewAdvancedBorderStyle advancedBorderStyle,
                                      DataGridViewPaintParts paintParts)
        {
            this.Value = this.GetValue();
            base.Paint(graphics,
                clipBounds,
                cellBounds,
                rowIndex,
                cellState,
                value,
                formattedValue,
                errorText,
                cellStyle,
                advancedBorderStyle,
                paintParts);
        }

        public override void PositionEditingControl(bool setLocation,
                                                    bool setSize,
                                                    Rectangle cellBounds,
                                                    Rectangle cellClip,
                                                    DataGridViewCellStyle cellStyle,
                                                    bool singleVerticalBorderAdded,
                                                    bool singleHorizontalBorderAdded,
                                                    bool isFirstDisplayedColumn,
                                                    bool isFirstDisplayedRow)
        {
            var editingControlBounds = this.PositionEditingPanel(cellBounds,
                cellClip,
                cellStyle,
                singleVerticalBorderAdded,
                singleHorizontalBorderAdded,
                isFirstDisplayedColumn,
                isFirstDisplayedRow);

            editingControlBounds = this.GetAdjustedEditingControlBounds(editingControlBounds, cellStyle);
            this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
            this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
        }

        private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
        {
            // Add a 1 pixel padding on the left and right of the editing control
            editingControlBounds.X += 1;
            editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);

            // Adjust the vertical location of the editing control:
            var preferredHeight = this.Size.Height;
            if (preferredHeight < editingControlBounds.Height)
            {
                switch (cellStyle.Alignment)
                {
                    case DataGridViewContentAlignment.MiddleLeft:
                    case DataGridViewContentAlignment.MiddleCenter:
                    case DataGridViewContentAlignment.MiddleRight:
                        editingControlBounds.Y += (editingControlBounds.Height - preferredHeight) / 2;
                        break;
                    case DataGridViewContentAlignment.BottomLeft:
                    case DataGridViewContentAlignment.BottomCenter:
                    case DataGridViewContentAlignment.BottomRight:
                        editingControlBounds.Y += editingControlBounds.Height - preferredHeight;
                        break;
                }
            }

            return editingControlBounds;
        }

        public override void DetachEditingControl()
        {
            this.m_Editor.UpdateNode();
            base.DetachEditingControl();
            this.Value = this.GetValue();
        }

        protected override void Dispose(bool disposing)
        {
            this.m_Object = null;
            this.m_ParentNode = null;
            this.m_FieldNode = null;
            this.m_Field = null;
            this.m_Editor = null;
            base.Dispose(disposing);
        }
    }
}