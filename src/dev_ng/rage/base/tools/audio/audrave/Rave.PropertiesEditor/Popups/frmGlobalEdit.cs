namespace Rave.PropertiesEditor.Popups
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.PropertiesEditor;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public partial class frmGlobalEdit : Form
    {
        private readonly Dictionary<string, ISimpleFieldDefinition> m_fieldDefsLookup;
        private readonly List<IObjectInstance> m_objectInstances;
        private ITypeDefinition m_baseType;
        private int m_maxDefault;
        private int m_minDefault;
        private ITypeDefinitions m_typeDefinitions;

        public frmGlobalEdit(List<IObjectInstance> objectInstances)
        {
            this.InitializeComponent();

            this.m_value.KeyPress += NumericTextBox_KeyPress;
            this.m_max.KeyPress += NumericTextBox_KeyPress;
            this.m_min.KeyPress += NumericTextBox_KeyPress;
            this.m_objectInstances = objectInstances;
            this.m_fieldDefsLookup = new Dictionary<string, ISimpleFieldDefinition>();

            this.Init();
        }

        private void Init()
        {
            this.m_typeDefinitions = this.m_objectInstances[0].TypeDefinitions;
            this.m_baseType = this.GetMostDerivedCommonType(this.m_objectInstances);
            this.FindSuitableFields(string.Empty, this.m_baseType.Fields);
            this.m_fieldComboBox.Items.AddRange(this.m_fieldDefsLookup.Keys.ToArray());
            this.m_fieldComboBox.SelectedIndex = 0;
        }

        private static IEnumerable<IFieldDefinition> FilterPotentialFields(IEnumerable<IFieldDefinition> fieldDefs)
        {
            return
                fieldDefs.Where(
                    fd =>
                    (!fd.Ignore && fd.Visible) &&
                    ((fd is ISimpleFieldDefinition &&
                      (fd.TypeName == "s8" || fd.TypeName == "s16" || fd.TypeName == "s32" || fd.TypeName == "u8" ||
                       fd.TypeName == "u16" || fd.TypeName == "u32")) ||
                     (fd is ICompositeFieldDefinition && (fd as ICompositeFieldDefinition).MaxOccurs == 1)));
        }

        private void FindSuitableFields(string parentPath, IEnumerable<IFieldDefinition> fieldDefs)
        {
            var potentialFields = FilterPotentialFields(fieldDefs);
            foreach (var potentialField in potentialFields)
            {
                if (potentialField is ISimpleFieldDefinition)
                {
                    this.m_fieldDefsLookup.Add(
                        string.IsNullOrEmpty(parentPath)
                            ? potentialField.Name
                            : string.Format("{0}.{1}", parentPath, potentialField.Name),
                        potentialField as ISimpleFieldDefinition);
                }
                else if (potentialField is ICompositeFieldDefinition)
                {
                    this.FindSuitableFields(
                        string.IsNullOrEmpty(parentPath)
                            ? potentialField.Name
                            : string.Format("{0}.{1}", parentPath, potentialField.Name),
                        (potentialField as ICompositeFieldDefinition).Fields);
                }
            }
        }

        private ITypeDefinition GetMostDerivedCommonType(IList<IObjectInstance> objectInstances)
        {
            var allAreSameType = this.m_objectInstances.All(x => x.TypeName == this.m_objectInstances[0].TypeName);
            if (allAreSameType)
            {
                return this.m_typeDefinitions.FindTypeDefinition(this.m_objectInstances[0].TypeName);
            }

            var inheritanceHierarchies = new List<ITypeDefinition>[objectInstances.Count];
            for (var i = 0; i < objectInstances.Count; ++i)
            {
                var type = this.m_typeDefinitions.FindTypeDefinition(objectInstances[i].TypeName);
                var inheritanceHierarchy = new List<ITypeDefinition> {type};

                while (!string.IsNullOrEmpty(type.InheritsFrom))
                {
                    type = this.m_typeDefinitions.FindTypeDefinition(type.InheritsFrom);
                    inheritanceHierarchy.Insert(0, type);
                }
                inheritanceHierarchies[i] = inheritanceHierarchy;
            }

            var shortestInheritanceHierarchy = int.MaxValue;
            foreach (var inheritanceHierarchy in inheritanceHierarchies)
            {
                if (inheritanceHierarchy.Count < shortestInheritanceHierarchy)
                {
                    shortestInheritanceHierarchy = inheritanceHierarchy.Count;
                }
            }

            for (var i = 0; i < shortestInheritanceHierarchy; ++i)
            {
                for (var j = 1; j < inheritanceHierarchies.Length; ++j)
                {
                    if (inheritanceHierarchies[j][i] !=
                        inheritanceHierarchies[j - 1][i])
                    {
                        return inheritanceHierarchies[0][i - 1];
                    }
                }
            }
            return inheritanceHierarchies[0][0];
        }

        private static void NumericTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            var numberFormatInfo = CultureInfo.CurrentCulture.NumberFormat;
            var decimalSeparator = numberFormatInfo.NumberDecimalSeparator;
            var groupSeparator = numberFormatInfo.NumberGroupSeparator;
            var negativeSign = numberFormatInfo.NegativeSign;

            var keyInput = e.KeyChar.ToString();

            if (Char.IsDigit(e.KeyChar))
            {
                // Do Nothing
            }
            else if (keyInput.Equals(decimalSeparator) || keyInput.Equals(groupSeparator) ||
                     keyInput.Equals(negativeSign))
            {
                // Do Nothing
            }
            else if (e.KeyChar == '\b')
            {
                // Do Nothing
            }
            else
            {
                // Invalid
                e.Handled = true;
            }
        }

        private void FieldComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = this.m_fieldComboBox.SelectedItem as string;
            if (string.IsNullOrEmpty(selectedItem))
            {
                return;
            }

            ISimpleFieldDefinition simpleFieldDefinition;
            if (this.m_fieldDefsLookup.TryGetValue(selectedItem, out simpleFieldDefinition))
            {
                if (simpleFieldDefinition.Min !=
                    simpleFieldDefinition.Max)
                {
                    this.m_minDefault = (int) simpleFieldDefinition.Min;
                    this.m_maxDefault = (int) simpleFieldDefinition.Max;
                }
                else
                {
                    GetTypeBounds(simpleFieldDefinition.TypeName, out this.m_minDefault, out this.m_maxDefault);
                }

                string displayUnits;
                string displayValue;
                PropertyEditorUtil.ConvertToDisplayValue((int)simpleFieldDefinition.Min,
                                                         simpleFieldDefinition.Units,
                                                         out displayValue,
                                                         out displayUnits);
                this.m_min.Text = displayValue;

                PropertyEditorUtil.ConvertToDisplayValue((int)simpleFieldDefinition.Max,
                                                         simpleFieldDefinition.Units,
                                                         out displayValue,
                                                         out displayUnits);
                this.m_max.Text = displayValue;
                this.lblUnits.Text = displayUnits;
            }
        }

        private static void GetTypeBounds(string typeName, out int min, out int max)
        {
            switch (typeName)
            {
                case "s8":
                    min = SByte.MinValue;
                    max = SByte.MaxValue;
                    break;
                case "u8":
                    min = Byte.MinValue;
                    max = Byte.MaxValue;
                    break;
                case "s16":
                    min = Int16.MinValue;
                    max = Int16.MaxValue;
                    break;
                case "u16":
                    min = UInt16.MinValue;
                    max = UInt16.MaxValue;
                    break;
                case "u32":
                    min = 0;
                    max = Int32.MaxValue;
                    break;
                default:
                    min = Int32.MinValue;
                    max = Int32.MaxValue;
                    break;
            }
        }

        private int GetValidValue(int currentValue, ISimpleFieldDefinition simpleFieldDefinition)
        {
            var min = this.m_min.Text == string.Empty
                          ? this.m_minDefault
                          : PropertyEditorUtil.ConvertFromDisplayValue(this.m_min.Text, simpleFieldDefinition.Units);
            var max = this.m_max.Text == string.Empty
                          ? this.m_maxDefault
                          : PropertyEditorUtil.ConvertFromDisplayValue(this.m_max.Text, simpleFieldDefinition.Units);
            var increment = this.m_value.Text == string.Empty
                                ? 0
                                : PropertyEditorUtil.ConvertFromDisplayValue(this.m_value.Text, simpleFieldDefinition.Units);
            var value = currentValue + increment;

            //cap custom min/max to field min/max
            if (max > simpleFieldDefinition.Max)
            {
                max = (int) simpleFieldDefinition.Max;
            }
            if (min < simpleFieldDefinition.Min)
            {
                min = (int) simpleFieldDefinition.Min;
            }

            //cap value
            if (value > max)
            {
                value = max;
            }
            if (value < min)
            {
                value = min;
            }
            return value;
        }

        private void btnOK_OnClick(object sender, EventArgs e)
        {
            var selectedItem = this.m_fieldComboBox.SelectedItem as string;
            if (string.IsNullOrEmpty(selectedItem))
            {
                return;
            }

            ISimpleFieldDefinition simpleFieldDefinition;
            if (this.m_fieldDefsLookup.TryGetValue(selectedItem, out simpleFieldDefinition))
            {
                foreach (var objectInstance in this.m_objectInstances)
                {
                    var node = this.FindNode(selectedItem, objectInstance);
                    if (node != null)
                    {
                        node.InnerText = this.GetValidValue(Int32.Parse(node.InnerText), simpleFieldDefinition).ToString();
                        objectInstance.MarkAsDirty();
                    }
                    else
                    {
                        var newNode = this.CreateNode(selectedItem, objectInstance);
                        if (newNode != null)
                        {
                            newNode.InnerText =
                                this.GetValidValue(Int32.Parse(simpleFieldDefinition.DefaultValue), simpleFieldDefinition).
                                    ToString();
                            objectInstance.MarkAsDirty();
                        }
                    }
                }
            }
        }

        private XmlNode FindNode(string selectedItem, IObjectInstance objectInstance)
        {
            return FindNode(0,
                            selectedItem.Trim().Split('.'),
                            objectInstance.Node,
                            this.m_typeDefinitions.FindTypeDefinition(objectInstance.TypeName).Fields);
        }

        private static XmlNode FindNode(int pathIndex,
                                        IList<string> path,
                                        XmlNode parentNode,
                                        IEnumerable<IFieldDefinition> fieldDefs)
        {
            var targetFieldDef =
                (from fieldDef in fieldDefs where fieldDef.Name == path[pathIndex] select fieldDef).FirstOrDefault();
            if (targetFieldDef != null)
            {
                var targetNode =
                    (from XmlNode node in parentNode.ChildNodes where node.Name == targetFieldDef.Name select node).
                        FirstOrDefault();
                if (targetNode != null)
                {
                    if (pathIndex == path.Count - 1)
                    {
                        return targetNode;
                    }
                    return FindNode(pathIndex + 1, path, targetNode, (targetFieldDef as ICompositeFieldDefinition).Fields);
                }
            }
            return null;
        }

        private XmlNode CreateNode(string selectedItem, IObjectInstance objectInstance)
        {
            return CreateNode(0,
                              selectedItem.Trim().Split('.'),
                              objectInstance.Node,
                              this.m_typeDefinitions.FindTypeDefinition(objectInstance.TypeName).Fields);
        }

        private static XmlNode CreateNode(int pathIndex,
                                          IList<string> path,
                                          XmlNode parentNode,
                                          IEnumerable<IFieldDefinition> fieldDefs)
        {
            var targetFieldDef =
                (from fieldDef in fieldDefs where fieldDef.Name == path[pathIndex] select fieldDef).FirstOrDefault();
            if (targetFieldDef != null)
            {
                var targetNode =
                    (from XmlNode node in parentNode.ChildNodes where node.Name == targetFieldDef.Name select node).
                        FirstOrDefault();
                if (targetNode == null)
                {
                    var simpleFieldDef = targetFieldDef as ISimpleFieldDefinition;
                    if (simpleFieldDef != null)
                    {
                        return simpleFieldDef.GenerateDefaultXmlNode(parentNode);
                    }

                    var compositeFieldDef = targetFieldDef as ICompositeFieldDefinition;
                    if (compositeFieldDef != null)
                    {
                        targetNode = compositeFieldDef.GenerateDefaultXmlNode(parentNode);
                    }
                }

                if (targetNode != null)
                {
                    if (pathIndex == path.Count - 1)
                    {
                        return targetNode;
                    }
                    return CreateNode(pathIndex + 1,
                                      path,
                                      targetNode,
                                      (targetFieldDef as ICompositeFieldDefinition).Fields);
                }
            }
            return null;
        }
    }
}