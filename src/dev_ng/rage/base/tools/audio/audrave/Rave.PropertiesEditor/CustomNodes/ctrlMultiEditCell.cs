namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    public class ctrlMultiEditCell : DataGridViewCheckBoxCell,
                                     IFieldCell
    {
        [ThreadStatic] private static Bitmap ms_renderingBitmap;
        private bool m_bDraw;
        private CheckBox m_checkBox;
        private MultiEditEditor m_editor;
        private IFieldDefinitionInstance m_fieldInstance;
        private IObjectInstance m_objectInstance;
        private XmlNode m_parentNode;

        public override Type EditType
        {
            get { return typeof (MultiEditEditor); }
        }

        public override Type ValueType
        {
            get { return typeof (CheckState); }
        }

        public override object DefaultNewRowValue
        {
            get { return false; }
        }

        #region IFieldCell Members

        public bool IsDefaultValue
        {
            get { return true; }
        }

        public CellType Type
        {
            get { return CellType.MULTIEDIT; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance,
                         XmlNode parentNode,
                         IFieldDefinitionInstance fieldInstance,
                         bool draw)
        {
            this.m_objectInstance = objectInstance;
            this.m_parentNode = parentNode;
            this.m_fieldInstance = fieldInstance;
            this.m_bDraw = draw;

            if (ms_renderingBitmap == null)
            {
                ms_renderingBitmap = new Bitmap(200, 30);
            }
            this.m_checkBox = new CheckBox();

            this.Value = CheckState.Unchecked;
        }

        public override void InitializeEditingControl(int rowIndex,
                                                      object initialFormattedValue,
                                                      DataGridViewCellStyle dataGridViewCellStyle)
        {
            // Set the value of the editing control to the current cell value.
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
            this.m_editor = this.DataGridView.EditingControl as MultiEditEditor;
            this.m_editor.Init(this.m_objectInstance, this.m_parentNode, this.m_fieldInstance);
        }

        private CheckState GetCheckboxStateFromXml()
        {
	        if (m_fieldInstance == null || m_fieldInstance.Name == null || m_parentNode == null)
	        {
		        return CheckState.Unchecked;
	        }
            var fieldNode = PropertyEditorUtil.FindXmlNode(this.m_fieldInstance.Name, this.m_parentNode);
            if (fieldNode == null)
            {
                return CheckState.Unchecked;
            }
            if (fieldNode.Attributes != null && fieldNode.Attributes["applySetting"] == null)
            {
                return CheckState.Unchecked;
            }
            if (fieldNode.Attributes != null && fieldNode.Attributes["applySetting"].Value == "yes")
            {
                return CheckState.Checked;
            }
            return CheckState.Unchecked;
        }

        private static bool PartPainted(DataGridViewPaintParts paintParts, DataGridViewPaintParts paintPart)
        {
            return (paintParts & paintPart) != 0;
        }

        protected override void Paint(Graphics graphics,
                                      Rectangle clipBounds,
                                      Rectangle cellBounds,
                                      int rowIndex,
                                      DataGridViewElementStates cellState,
                                      object value,
                                      object formattedValue,
                                      string errorText,
                                      DataGridViewCellStyle cellStyle,
                                      DataGridViewAdvancedBorderStyle advancedBorderStyle,
                                      DataGridViewPaintParts paintParts)
        {
            if (!this.m_bDraw)
            {
                base.Paint(graphics,
                    clipBounds,
                    cellBounds,
                    rowIndex,
                    cellState,
                    value,
                    formattedValue,
                    errorText,
                    cellStyle,
                    advancedBorderStyle,
                    paintParts);
            }
            else
            {
                if (this.DataGridView == null)
                {
                    return;
                }

                // First paint the borders and background of the cell.
                base.Paint(graphics,
                    clipBounds,
                    cellBounds,
                    rowIndex,
                    cellState,
                    value,
                    formattedValue,
                    errorText,
                    cellStyle,
                    advancedBorderStyle,
                    paintParts & ~(DataGridViewPaintParts.ErrorIcon | DataGridViewPaintParts.ContentForeground));

                var ptCurrentCell = this.DataGridView.CurrentCellAddress;
                var cellCurrent = ptCurrentCell.X == this.ColumnIndex && ptCurrentCell.Y == rowIndex;
                var cellEdited = cellCurrent && this.DataGridView.EditingControl != null;

                // If the cell is in editing mode, there is nothing else to paint
                if (!cellEdited)
                {
                    if (PartPainted(paintParts, DataGridViewPaintParts.ContentForeground))
                    {
                        // Take the borders into account
                        var borderWidths = this.BorderWidths(advancedBorderStyle);
                        var valBounds = cellBounds;
                        valBounds.Offset(borderWidths.X, borderWidths.Y);
                        valBounds.Width -= borderWidths.Right;
                        valBounds.Height -= borderWidths.Bottom;
                        // Also take the padding into account
                        if (cellStyle.Padding !=
                            Padding.Empty)
                        {
                            if (this.DataGridView.RightToLeft ==
                                RightToLeft.Yes)
                            {
                                valBounds.Offset(cellStyle.Padding.Right, cellStyle.Padding.Top);
                            }
                            else
                            {
                                valBounds.Offset(cellStyle.Padding.Left, cellStyle.Padding.Top);
                            }
                            valBounds.Width -= cellStyle.Padding.Horizontal;
                            valBounds.Height -= cellStyle.Padding.Vertical;
                        }

                        valBounds = this.GetAdjustedEditingControlBounds(valBounds, cellStyle);
                        var cellSelected = (cellState & DataGridViewElementStates.Selected) != 0;

                        if (ms_renderingBitmap.Width < valBounds.Width ||
                            ms_renderingBitmap.Height < valBounds.Height)
                        {
                            // The static bitmap is too small, a bigger one needs to be allocated.
                            ms_renderingBitmap.Dispose();
                            ms_renderingBitmap = new Bitmap(valBounds.Width, valBounds.Height);
                        }

                        this.m_checkBox.Parent = this.DataGridView;

                        // Set all the relevant properties
                        this.m_checkBox.Width = valBounds.Width;
                        this.m_checkBox.Height = valBounds.Height;
                        this.m_checkBox.RightToLeft = this.DataGridView.RightToLeft;
                        this.m_checkBox.Location = new Point(0, -this.m_checkBox.Height - 100);

                        this.m_checkBox.CheckState = this.GetCheckboxStateFromXml();

                        this.m_checkBox.Enabled = !this.m_objectInstance.IsReadOnly;
                        //this.Value = sm_CheckBox.CheckState;

                        Color backColor;
                        if (PartPainted(paintParts, DataGridViewPaintParts.SelectionBackground) && cellSelected)
                        {
                            backColor = cellStyle.SelectionBackColor;
                        }
                        else
                        {
                            backColor = cellStyle.BackColor;
                        }
                        if (PartPainted(paintParts, DataGridViewPaintParts.Background))
                        {
                            if (backColor.A < 255)
                            {
                                backColor = Color.FromArgb(255, backColor);
                            }
                            this.m_checkBox.BackColor = backColor;
                        }
                        // Finally paint the control
                        var srcRect = new Rectangle(0, 0, valBounds.Width, valBounds.Height);
                        if (srcRect.Width > 0 &&
                            srcRect.Height > 0)
                        {
                            this.m_checkBox.DrawToBitmap(ms_renderingBitmap, srcRect);
                            graphics.DrawImage(ms_renderingBitmap,
                                new Rectangle(valBounds.Location, valBounds.Size),
                                srcRect,
                                GraphicsUnit.Pixel);
                        }
                    }
                    if (PartPainted(paintParts, DataGridViewPaintParts.ErrorIcon))
                    {
                        // Paint the potential error icon on top of the control
                        base.Paint(graphics,
                            clipBounds,
                            cellBounds,
                            rowIndex,
                            cellState,
                            value,
                            formattedValue,
                            errorText,
                            cellStyle,
                            advancedBorderStyle,
                            DataGridViewPaintParts.ErrorIcon);
                    }
                }
            }
        }

        public override void PositionEditingControl(bool setLocation,
                                                    bool setSize,
                                                    Rectangle cellBounds,
                                                    Rectangle cellClip,
                                                    DataGridViewCellStyle cellStyle,
                                                    bool singleVerticalBorderAdded,
                                                    bool singleHorizontalBorderAdded,
                                                    bool isFirstDisplayedColumn,
                                                    bool isFirstDisplayedRow)
        {
            var editingControlBounds = this.PositionEditingPanel(cellBounds,
                cellClip,
                cellStyle,
                singleVerticalBorderAdded,
                singleHorizontalBorderAdded,
                isFirstDisplayedColumn,
                isFirstDisplayedRow);

            editingControlBounds = this.GetAdjustedEditingControlBounds(editingControlBounds, cellStyle);
            this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
            this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
        }

        private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
        {
            // Add a 1 pixel padding on the left and right of the editing control
            editingControlBounds.X += 1;
            editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);

            // Adjust the vertical location of the editing control:
            var preferredHeight = this.m_checkBox.Height;
            if (preferredHeight < editingControlBounds.Height)
            {
                switch (cellStyle.Alignment)
                {
                    case DataGridViewContentAlignment.MiddleLeft:
                    case DataGridViewContentAlignment.MiddleCenter:
                    case DataGridViewContentAlignment.MiddleRight:
                        editingControlBounds.Y += (editingControlBounds.Height - preferredHeight) / 2;
                        break;
                    case DataGridViewContentAlignment.BottomLeft:
                    case DataGridViewContentAlignment.BottomCenter:
                    case DataGridViewContentAlignment.BottomRight:
                        editingControlBounds.Y += editingControlBounds.Height - preferredHeight;
                        break;
                }
            }

            return editingControlBounds;
        }

        public override void DetachEditingControl()
        {
            if (null != this.m_editor)
            {
                this.m_editor.UpdateNode();
            }

            base.DetachEditingControl();
            this.Value = this.GetCheckboxStateFromXml();
        }

        protected override void Dispose(bool disposing)
        {
            this.m_objectInstance = null;
            this.m_parentNode = null;
            this.m_fieldInstance = null;
            this.m_editor = null;
            base.Dispose(disposing);
        }
    }
}