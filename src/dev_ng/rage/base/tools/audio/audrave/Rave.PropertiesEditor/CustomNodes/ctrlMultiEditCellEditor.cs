namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    internal class MultiEditEditor : CheckBox,
                                     IDataGridViewEditingControl
    {
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_objectInstance;
        private XmlNode m_parentNode;
        private bool m_valueChanged;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.CheckState; }
            set { return; }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            // Let the DateTimePicker handle the keys listed.
            switch (key & Keys.KeyCode)
            {
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Right:
                case Keys.Home:
                case Keys.End:
                case Keys.PageDown:
                case Keys.PageUp:
                    return true;
                default:
                    return false;
            }
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged
        {
            get { return this.m_valueChanged; }
            set { this.m_valueChanged = value; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance, XmlNode parentNode, IFieldDefinitionInstance fieldInstance)
        {
            this.m_objectInstance = objectInstance;
            this.m_parentNode = parentNode;
            this.m_fieldInstance = fieldInstance;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_fieldInstance.Name, this.m_parentNode);
            this.SetCheckboxStateFromXml();
        }

        private void SetCheckboxStateFromXml()
        {
            if (this.m_fieldNode == null)
            {
                this.CheckState = CheckState.Unchecked;
            }
            else if (this.m_fieldNode.Attributes["applySetting"] == null)
            {
                this.CheckState = CheckState.Unchecked;
            }
            else
            {
                this.CheckState = this.m_fieldNode.Attributes["applySetting"].Value == "yes"
                                 ? CheckState.Checked
                                 : CheckState.Unchecked;
            }
        }

        public void UpdateNode()
        {
            //find the appropriate field
            var n = PropertyEditorUtil.FindXmlNode(this.m_fieldInstance.Name, this.m_parentNode);
            if (n == null)
            {
	            XmlDocument ownerDocument = this.m_parentNode.OwnerDocument;
	            if (ownerDocument != null)
		            n = ownerDocument.CreateElement(this.m_fieldInstance.Name);
	            // since the sound builder doesn't support elements with attributes
                // but no value, we'll pull out the default value and store that

	            if (n != null)
	            {
		            n.InnerText = this.m_fieldInstance.DefaultValue;
		            this.m_parentNode.AppendChild(n);
	            }
            }

	        if (n != null && n.Attributes != null)
	        {
		        var setting = n.Attributes["applySetting"];
		        if (setting == null)
		        {
			        if (n.OwnerDocument != null) n.Attributes.Append(n.OwnerDocument.CreateAttribute("applySetting"));
		        }
	        }

	        var val = (this.Checked ? "yes" : "no");
            if (n != null  && n.Attributes != null && n.Attributes["applySetting"].Value != val)
            {
                n.Attributes["applySetting"].Value = val;
            }
            this.m_objectInstance.MarkAsDirty();
        }

        protected override void OnCheckStateChanged(EventArgs e)
        {
            this.m_valueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
            base.OnCheckStateChanged(e);
            this.UpdateNode();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter ||
                keyData == Keys.Space)
            {
                this.CheckState++;
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}