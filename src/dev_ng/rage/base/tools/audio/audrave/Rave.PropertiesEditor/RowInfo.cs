﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Rave.PropertiesEditor
{
    public class RowInfo
    {
        public RowInfo(XmlNode DescriptionNode)
        {
            this.DescriptionNode = DescriptionNode;
        }

        public XmlNode DescriptionNode { get; private set; }
    }
}
