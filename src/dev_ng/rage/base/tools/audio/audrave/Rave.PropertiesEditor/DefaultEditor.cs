// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultEditor.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The default editor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Runtime.InteropServices;

namespace Rave.PropertiesEditor
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Drawing;
	using System.Linq;
	using System.Windows.Forms;
	using System.Xml;

	using AdvancedDataGridView;

	using Rave.Plugins.Infrastructure.Interfaces;
	using Rave.PropertiesEditor.CustomNodes;
	using Rave.TypeDefinitions;
	using Rave.TypeDefinitions.Infrastructure.Interfaces;
	using Rave.Types.Infrastructure.Interfaces.Objects;
	using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
	using Rave.Utils;

	/// <summary>
	/// The default editor.
	/// </summary>
	public partial class DefaultEditor : TabControl
	{
		#region Fields

		/// <summary>
		/// The m_display group states.
		/// </summary>
		private readonly Dictionary<string, XmlDocument> m_displayGroupStates;

		/// <summary>
		/// The m_display groups.
		/// </summary>
		private readonly Dictionary<string, tbPropertyTab> m_displayGroups;

		/// <summary>
		/// The m_doc.
		/// </summary>
		private readonly XmlDocument m_doc;

		/// <summary>
		/// The mnu collapse.
		/// </summary>
		private readonly MenuItem mnuCollapse;

		/// <summary>
		/// The mnu expand.
		/// </summary>
		private readonly MenuItem mnuExpand;

		/// <summary>
		/// The mnu remove.
		/// </summary>
		private readonly ContextMenuStrip mnuRemove;

		/// <summary>
		/// The mnu show parent value.
		/// </summary>
		private readonly MenuItem mnuShowParentValue;

		/// <summary>
		/// The m_ direct references.
		/// </summary>
		private tbReferenceTab m_DirectReferences;

		/// <summary>
		/// The m_ top level references.
		/// </summary>
		private tbReferenceTab m_TopLevelReferences;

		/// <summary>
		/// The m_current.
		/// </summary>
		private string m_current;

		/// <summary>
		/// The m_draw controls.
		/// </summary>
		private bool m_drawControls;

		/// <summary>
		/// The m_mode.
		/// </summary>
		private Mode m_mode;

		/// <summary>
		/// The m_object.
		/// </summary>
		private IObjectInstance m_object;

		/// <summary>
		/// The m_show parent value.
		/// </summary>
		private bool m_showParentValue;

		/// <summary>
		/// The m_type definition.
		/// </summary>
		private ITypeDefinition m_typeDefinition;

		/// <summary>
		/// The mnu main.
		/// </summary>
		private ContextMenu mnuMain;

		#endregion

		#region Constructors and Destructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultEditor"/> class.
		/// </summary>
		public DefaultEditor()
		{
			this.m_displayGroups = new Dictionary<string, tbPropertyTab>();
			this.m_displayGroupStates = new Dictionary<string, XmlDocument>();
			this.m_doc = new XmlDocument();
			this.m_doc.AppendChild(this.m_doc.CreateElement("state"));
			this.Alignment = TabAlignment.Bottom;

			this.mnuRemove = new ContextMenuStrip();
			this.mnuRemove.Items.Add("Remove ObjectNameBox", null, this.mnuRemoveInstance_Click);

			this.mnuExpand = new MenuItem("Expand All", this.mnuExpandAll_Click);
			this.mnuCollapse = new MenuItem("Collapse All", this.mnuCollapseAll_Click);
			this.mnuShowParentValue = new MenuItem("Show Parent Value", this.mnuShowParentValue_Click);
		}

		#endregion

		#region Public Events

		/// <summary>
		/// The on object edit click.
		/// </summary>
		public event Action<IObjectInstance> OnObjectEditClick;

		/// <summary>
		/// The on object ref click.
		/// </summary>
		public event Action<IObjectInstance> OnObjectRefClick;

		/// <summary>
		/// The on template ref click.
		/// </summary>
		public event Action<ITemplate> OnTemplateRefClick;

		/// <summary>
		/// The on wave bank ref click.
		/// </summary>
		public event Action<string> OnWaveBankRefClick;

		/// <summary>
		/// The on wave ref audition.
		/// </summary>
		public event Action<string, string> OnWaveRefAudition;

		/// <summary>
		/// The on wave ref click.
		/// </summary>
		public event Action<string, string> OnWaveRefClick;

		#endregion

		#region Public Properties

		/// <summary>
		/// Gets the object type.
		/// </summary>
		public string ObjectType
		{
			get
			{
				return "Default";
			}
		}

		#endregion

		#region Public Methods and Operators

		/// <summary>
		/// The edit object.
		/// </summary>
		/// <param name="objectInstance">
		/// The object instance.
		/// </param>
		/// <param name="mode">
		/// The mode.
		/// </param>
		/// <param name="mainMenu">
		/// The main menu.
		/// </param>
		/// <param name="drawControls">
		/// The draw controls.
		/// </param>
		public void EditObject(IObjectInstance objectInstance, Mode mode, ContextMenu mainMenu, bool drawControls)
		{
			this.m_mode = mode;
			this.m_drawControls = drawControls;
			this.mnuMain = mainMenu;
			this.CreateMainMenu();
			this.Hide();
			if (this.SelectedTab != null)
			{
				this.m_current = this.SelectedTab.Text;
			}

			if (objectInstance == null)
			{
				return;
			}

			if (this.m_object != null && this.m_object.Bank != null)
			{
				this.m_object.Bank.BankStatusChanged -= this.OnBankStatusChanged;
			}

			this.Reset();

			this.m_object = objectInstance;

			if (this.m_object.Bank != null)
			{
				this.m_object.Bank.BankStatusChanged += this.OnBankStatusChanged;
			}

			try
			{
				var templateInstance = this.m_object as ITemplateInstance;

				if (null != templateInstance)
				{
					var fieldDefs = templateInstance.Template.GetExposedFields().Values;
					this.InitTabPageEntries(fieldDefs);

					foreach (var exposedField in templateInstance.Template.GetExposedFields())
					{
						var fieldDef = exposedField.Value;
						if (fieldDef != null)
						{
							var fieldInstance = new FieldDefinitionInstance(fieldDef, exposedField.Key.InnerXml);
							this.CreateField(
								fieldInstance,
								exposedField.Key.Attributes["ExposeAs"].Value,
								this.m_object,
								this.m_object.Node);
						}
					}
				}
				else
				{
					this.m_typeDefinition = this.m_object.TypeDefinitions.FindTypeDefinition(this.m_object.TypeName);
					if (this.m_typeDefinition == null)
					{
						return;
					}

					this.InitTabPageEntries(this.m_typeDefinition.Fields);

					foreach (var f in this.m_typeDefinition.Fields)
					{
						this.CreateField(new FieldDefinitionInstance(f, null), null, this.m_object, this.m_object.Node);
					}
				}

				// Update Referencers
				if (this.m_mode != Mode.MULTIEDIT)
				{
					this.CreatRefTabs();
				}

				// default selected page to 0
				this.SelectedIndex = 0;
				if (this.m_current != null)
				{
					foreach (TabPage tab in this.TabPages)
					{
						if (tab.Text.ToUpper().Replace("*", string.Empty)
							== this.m_current.ToUpper().Replace("*", string.Empty))
						{
							this.SelectedTab = tab;
						}
					}
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
				ErrorManager.HandleError(ex);
			}

			this.InitExpandTree();
			this.Show();
		}

		/// <summary>
		/// The end editing.
		/// </summary>
		public void EndEditing()
		{
			foreach (TabPage tab in this.TabPages)
			{
				var propTab = tab as tbPropertyTab;
				if (propTab != null)
				{
					propTab.TreeGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
					propTab.TreeGrid.EndEdit();
				}
			}
		}

		/// <summary>
		/// The expand all.
		/// </summary>
		public void ExpandAll()
		{
			var tab = this.SelectedTab as tbPropertyTab;
			if (tab != null)
			{
				tab.TreeGrid.SuspendLayout();
				Expand(tab.TreeGrid.Nodes);
				tab.TreeGrid.ResumeLayout();
			}
		}

		/// <summary>
		/// The get name.
		/// </summary>
		/// <returns>
		/// The <see cref="string"/>.
		/// </returns>
		public string GetName()
		{
			return "Default Editor";
		}

		/// <summary>
		/// The get numberof tabs.
		/// </summary>
		/// <returns>
		/// The <see cref="int"/>.
		/// </returns>
		public int GetNumberofTabs()
		{
			return this.TabCount;
		}

		/// <summary>
		/// The init.
		/// </summary>
		/// <param name="settings">
		/// The settings.
		/// </param>
		/// <returns>
		/// The <see cref="bool"/>.
		/// </returns>
		public bool Init(XmlNode settings)
		{
			return true;
		}

		/// <summary>
		/// The reset.
		/// </summary>
		public void Reset()
		{
			this.SuspendLayout();
			this.EndEditing();


			this.m_displayGroups.Clear();
			this.TabPages.Clear();
			this.ResumeLayout();

			this.m_object = null;
		}

		/// <summary>
		/// The set tab.
		/// </summary>
		/// <param name="i">
		/// The i.
		/// </param>
		public void SetTab(int i)
		{
			if (this.InvokeRequired)
			{
				this.Invoke(new Action<int>(this.SetTab), new object[] { i });
			}
			else
			{
				this.SelectedIndex = i;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// The clear cells.
		/// </summary>
		/// <param name="node">
		/// The node.
		/// </param>
		/// <param name="cells">
		/// The cells.
		/// </param>
		private static void ClearCells(TreeGridNode node, List<DataGridViewCell> cells)
		{
			cells.AddRange(node.Cells.Cast<DataGridViewCell>());
			foreach (var child in node.Nodes)
			{
				ClearCells(child, cells);
			}
		}

		/// <summary>
		/// The collapse.
		/// </summary>
		/// <param name="nodes">
		/// The nodes.
		/// </param>
		private static void Collapse(IEnumerable<TreeGridNode> nodes)
		{
			foreach (var tgn in nodes)
			{
				Collapse(tgn.Nodes);
				tgn.Collapse();
			}
		}

		/// <summary>
		/// The create state xml.
		/// </summary>
		/// <param name="nodes">
		/// The nodes.
		/// </param>
		/// <param name="node">
		/// The node.
		/// </param>
		private static void CreateStateXml(IEnumerable<TreeGridNode> nodes, XmlNode node)
		{
			// we can assume that the number of nodes will not change too dramatically 
			// between check out/in so just store the structure
			foreach (var tgnChild in nodes)
			{
				if (tgnChild.IsSited)
				{
					var newNode = node.OwnerDocument.CreateElement("node");
					newNode.SetAttribute("id", tgnChild.ID);
					node.AppendChild(newNode);
					CreateStateXml(tgnChild.Nodes, newNode);
				}
			}
		}

		/// <summary>
		/// The expand.
		/// </summary>
		/// <param name="nodes">
		/// The nodes.
		/// </param>
		private static void Expand(IEnumerable<TreeGridNode> nodes)
		{
			foreach (var tgn in nodes)
			{
				tgn.Expand();
				Expand(tgn.Nodes);
			}
		}

		/// <summary>
		/// The expand tree.
		/// </summary>
		/// <param name="xmlNodes">
		/// The xml nodes.
		/// </param>
		/// <param name="treeNodes">
		/// The tree nodes.
		/// </param>
		private static void ExpandTree(XmlNodeList xmlNodes, TreeGridNodeCollection treeNodes)
		{
			// we can assume that the number of nodes will not change too dramatically 
			// between check out/in so just re-create the structure
			for (var i = 0; i < xmlNodes.Count; i++)
			{
				// missing tree node
				if (i >= treeNodes.Count)
				{
					break;
				}

				if (xmlNodes[i].HasChildNodes)
				{
					treeNodes[i].Expand();
					ExpandTree(xmlNodes[i].ChildNodes, treeNodes[i].Nodes);
				}
			}
		}

		/// <summary>
		/// The tree grid_ cell end edit.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void TreeGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			var treeGrid = sender as ctrlPropertiesTreeGrid;
			if (treeGrid != null)
			{
				var fieldCell = treeGrid[e.ColumnIndex, e.RowIndex] as IFieldCell;
				if (fieldCell != null)
				{
					if (fieldCell.IsDefaultValue)
					{
						treeGrid[0, e.RowIndex].Style.ForeColor = Color.Black;
						treeGrid[0, e.RowIndex].Style.SelectionForeColor = Color.Black;
					}
					else
					{
						treeGrid[0, e.RowIndex].Style.ForeColor = Color.Red;
						treeGrid[0, e.RowIndex].Style.SelectionForeColor = Color.Red;
					}

					/**
					 * Dont update description of parent node since description will be hidden for expanded nodes anyway
                    
					TreeGridNode node = findNodeOfCell(treeGrid.Nodes, treeGrid[0, e.RowIndex]);

					//update all composite field descriptions
					if(node != null){
						TreeGridNode parent = node;

						while (parent.Parent!=null)
						{
							parent = parent.Parent;
							if (!(parent.Tag is RowInfo) || (parent.Tag is RowInfo && ((RowInfo) parent.Tag).DescriptionNode == null)) continue;
                                
							string changedFields = "";
							foreach (XmlNode xmlNode in ((RowInfo)parent.Tag).DescriptionNode.ChildNodes)
							{
								changedFields += changedFields.Equals("") ? "" : ", ";
								changedFields += getCompositeFieldNodeDescription(xmlNode);
							}
							parent.Cells[2].Value = changedFields;
						}
					}
					**/
				}
			}
		}

		private TreeGridNode findNodeOfCell(TreeGridNodeCollection nodes, DataGridViewCell cell)
		{
			foreach (TreeGridNode node in nodes)
			{
				if (node.Cells.Contains(cell)) return node;
				TreeGridNode result = findNodeOfCell(node.Nodes, cell);
				if (result != null) return result;
			}
			return null;
		}

		/// <summary>
		/// The add allowed type items.
		/// </summary>
		/// <param name="fieldDef">
		/// The field def.
		/// </param>
		/// <param name="menu">
		/// The menu.
		/// </param>
		private void AddAllowedTypeItems(IFieldDefinition fieldDef, ToolStrip menu)
		{
			if (!string.IsNullOrEmpty(fieldDef.AllowedType))
			{
				var typeDefinitions = fieldDef.TypeDefinition.TypeDefinitions;
				var allowedType = typeDefinitions.FindTypeDefinition(fieldDef.AllowedType);
				if (allowedType != null)
				{
					var concreteDescendantTypes =
						(from descendantType in allowedType.GetDescendantTypes()
						 where descendantType.IsAbstract == false
						 select descendantType).ToList();

					if (concreteDescendantTypes.Count > 0)
					{
						concreteDescendantTypes.Sort((x, y) => string.Compare(x.Name, y.Name));
						var subItems =
							concreteDescendantTypes.Select(
								type => new ToolStripMenuItem(type.Name, null, this.OnAddNewInstanceWithObject))
												   .ToArray();
						menu.Items.Add(new ToolStripMenuItem("Add ObjectNameBox with new", null, subItems));
					}
				}
			}
		}

		/// <summary>
		/// The add new instance.
		/// </summary>
		/// <param name="objectTypeToCreate">
		/// The object type to create.
		/// </param>
		private void AddNewInstance(string objectTypeToCreate)
		{
			// get current tab
			var tab = (tbPropertyTab)this.SelectedTab;

			// get treegridview
			var tgv = tab.TreeGrid;
			tgv.SuspendLayout();

			if (tgv.SelectedRows.Count == 0)
			{
				return;
			}

			// Find current row and extract associated data
			var tgn = (TreeGridNode)tgv.SelectedRows[0];
			var data = tgn.Tag as RowData;
			if (data != null)
			{
				var cfd = data.FieldDefinitionInstance.AsCompositeField();
				if (cfd != null)
				{
					if (tgn.Nodes.Count < cfd.MaxOccurs)
					{
						// generate default data
						var node = cfd.CompositeFieldDef.GenerateDefaultXmlNode(data.ParentNode, true);

						// create row(s) for new xml node
						var instance = this.CreateInstanceControls(
							cfd, null, data.ObjectInstance, node, tgn.Nodes, tab, objectTypeToCreate, tgn.Nodes.Count);
						instance.Parent.Expand();
						instance.Expand();
						Expand(instance.Nodes);
					}
					else
					{
						ErrorManager.HandleInfo("Maximum number of instances reached");
					}
				}
			}

			tgv.ResumeLayout();
		}

		/// <summary>
		/// The creat ref tabs.
		/// </summary>
		private void CreatRefTabs()
		{
			if (this.m_TopLevelReferences == null)
			{
				this.m_TopLevelReferences = new tbReferenceTab("Top-level references");
				this.m_TopLevelReferences.OnObjectRefClick += this.m_Reference_OnObjectRefClick;
				this.m_TopLevelReferences.OnTemplateRefClick += this.m_Reference_OnTemplateRefClick;
			}

			this.m_TopLevelReferences.Init(this.m_object, tbReferenceTab.ObjectReferenceSearch.TopLevel);
			this.TabPages.Add(this.m_TopLevelReferences);

			if (this.m_DirectReferences == null)
			{
				this.m_DirectReferences = new tbReferenceTab("Direct Referencers");
				this.m_DirectReferences.OnObjectRefClick += this.m_DirectReferences_OnObjectRefClick;
				this.m_DirectReferences.OnTemplateRefClick += this.m_Reference_OnTemplateRefClick;
			}

			this.m_DirectReferences.Init(this.m_object, tbReferenceTab.ObjectReferenceSearch.DirectOnly);
			this.TabPages.Add(this.m_DirectReferences);
		}

		/// <summary>
		/// The create composite control.
		/// </summary>
		/// <param name="compositeFieldInstance">
		/// The composite field instance.
		/// </param>
		/// <param name="fieldAlias">
		/// The field alias.
		/// </param>
		/// <param name="obj">
		/// The obj.
		/// </param>
		/// <param name="parentNode">
		/// The parent node.
		/// </param>
		/// <param name="nodes">
		/// The nodes.
		/// </param>
		/// <param name="tab">
		/// The tab.
		/// </param>
		private void CreateCompositeControl(
			ICompositeFieldDefinitionInstance compositeFieldInstance,
			string fieldAlias,
			IObjectInstance obj,
			XmlNode parentNode,
			TreeGridNodeCollection nodes,
			tbPropertyTab tab)
		{
			/*Please be aware that the tree structure of the nodes differs from the xml structure 
			  to make it easier to reorder and add/delete the xml */
			var nodeName = (fieldAlias == null | fieldAlias == String.Empty) ? compositeFieldInstance.Name : fieldAlias;
			var nodeCollection = nodes;

			// find all instances
			var instances = new ArrayList();
			if (parentNode != null)
			{
				foreach (XmlNode child in parentNode.ChildNodes)
				{
					if (child.Name == nodeName)
					{
						instances.Add(child);
					}
				}

				if (instances.Count == 0 && compositeFieldInstance.MaxOccurs == 1)
				{
					var instance = compositeFieldInstance.CompositeFieldDef.GenerateDefaultXmlNode(parentNode, true);
					instances.Add(instance);
				}

				if (compositeFieldInstance.MaxOccurs != 1)
				{
					if (!nodeName.ToLower().EndsWith("s"))
					{
						nodeName = nodeName + "s";
					}

					// Add an extra layer of structure in the tree to make it easier to reorder
					var compNode = nodes.Add(nodeName, new DataGridViewCell[] { new ctrlBlankCell(), new ctrlBlankCell() });
					compNode.ReadOnly = true;
					compNode.ReOrder = !obj.IsReadOnly;

					// The data has no associated xml
					compNode.Tag = new RowData(obj, compositeFieldInstance, parentNode, null, null);

					// Add instance option
					if (!obj.IsReadOnly)
					{
						var mnuAdd = new ContextMenuStrip();
						mnuAdd.Items.Add(new ToolStripMenuItem("Add ObjectNameBox", null, this.mnuAddInstance_Click));

						var objectRefFields = from field in compositeFieldInstance.Fields
											  where field.Units == "ObjectRef"
											  select field;
						if (objectRefFields.Count() == 1)
						{
							this.AddAllowedTypeItems(objectRefFields.First(), mnuAdd);
						}

						compNode.ContextMenuStrip = mnuAdd;
					}

					// Set the node collection for the children to be added to
					nodeCollection = compNode.Nodes;
				}
			}

			bool showIndex = instances.Count > 1;
			int fieldIndex = 0;

			foreach (XmlNode instance in instances)
			{
				int? index = null;
				if (showIndex)
				{
					index = fieldIndex;
					fieldIndex++;
				}
				this.CreateInstanceControls(compositeFieldInstance, null, obj, instance, nodeCollection, tab, null, index);
			}
		}

		/// <summary>
		/// The create field.
		/// </summary>
		/// <param name="fieldInstance">
		/// The field instance.
		/// </param>
		/// <param name="fieldAlias">
		/// The field alias.
		/// </param>
		/// <param name="objectInstance">
		/// The object instance.
		/// </param>
		/// <param name="objectNode">
		/// The object node.
		/// </param>
		private void CreateField(
			IFieldDefinitionInstance fieldInstance, string fieldAlias, IObjectInstance objectInstance, XmlNode objectNode)
		{
			if (!fieldInstance.Visible)
			{
				return;
			}

			if (fieldInstance.Name.ToLower() == "description")
			{
				foreach (var currTab in this.m_displayGroups.Values)
				{
					if (currTab.Text.ToUpper() == "MAIN")
					{
						continue;
					}

					this.CreateFieldControl(fieldInstance, fieldAlias, objectInstance, objectNode, currTab.TreeGrid.Nodes, currTab, null);
				}
			}

			var tab = this.FindTabPageEntry(fieldInstance.DisplayGroup ?? "Main");

			if (null == tab)
			{
				ErrorManager.HandleError("Failed to find tab for field control: " + fieldInstance.Name);
				return;
			}

			this.CreateFieldControl(
				fieldInstance, fieldAlias, objectInstance, objectNode, tab.TreeGrid.Nodes, tab, null);
		}

		/// <summary>
		/// The create field control.
		/// </summary>
		/// <param name="fieldInstance">
		/// The field instance.
		/// </param>
		/// <param name="fieldAlias">
		/// The field alias.
		/// </param>
		/// <param name="objectInstance">
		/// The object instance.
		/// </param>
		/// <param name="objectNode">
		/// The object node.
		/// </param>
		/// <param name="nodes">
		/// The nodes.
		/// </param>
		/// <param name="tab">
		/// The tab.
		/// </param>
		/// <param name="objectTypeToCreate">
		/// The object type to create.
		/// </param>
		/// <returns>
		/// The <see cref="TreeGridNode"/>.
		/// </returns>
		private TreeGridNode CreateFieldControl(
			IFieldDefinitionInstance fieldInstance,
			string fieldAlias,
			IObjectInstance objectInstance,
			XmlNode objectNode,
			TreeGridNodeCollection nodes,
			tbPropertyTab tab,
			string objectTypeToCreate,
			int? index = null)
		{
			if (!fieldInstance.Visible)
			{
				return null;
			}

			var cfd = fieldInstance.AsCompositeField();
			if (cfd != null)
			{
				if (cfd.Units == "WaveRef")
				{
					// create wave ref cell
					var tgn = nodes.Add(
						(fieldAlias == null | fieldAlias == String.Empty) ? fieldInstance.Name : fieldAlias,
						new DataGridViewCell[] { new ctrlBlankCell(), new ctrlBlankCell() });
					tgn.ReadOnly = true;
					var waveRef = CellPool.GetCell(CellType.WAVEREF) as ctrlWaveRefCell;
					waveRef.Init(objectInstance, objectNode, fieldInstance, fieldAlias);
					waveRef.OnWaveRefClick -= this.waveRef_OnWaveRefClick;
					waveRef.OnWaveRefClick += this.waveRef_OnWaveRefClick;
					waveRef.OnWaveRefAudition -= this.waveRef_OnWaveRefAudition;
					waveRef.OnWaveRefAudition += this.waveRef_OnWaveRefAudition;

					DataGridViewCell waveCell;
					switch (this.m_mode)
					{
						case Mode.MULTIEDIT:
							var mec = CellPool.GetCell(CellType.MULTIEDIT) as ctrlMultiEditCell;
							mec.Init(objectInstance, objectNode, fieldInstance, this.m_drawControls);
							waveCell = mec;
							break;
						case Mode.TEMPLATE:
							var ex = CellPool.GetCell(CellType.EXPOSE) as ctrlExposeCell;
							ex.Init(objectInstance, objectNode, fieldInstance, this.m_drawControls);
							waveCell = ex;
							break;
						default:
							waveCell = new ctrlBlankCell();
							break;
					}

					TreeGridNode waveNode = tgn.Nodes.Add(
						"Wave", new[] { waveCell, waveRef, new ctrlBlankCell(), new ctrlBlankCell() });
					waveNode.ReadOnly = objectInstance.IsReadOnly;

					// create bank ref cell
					var bankRef = CellPool.GetCell(CellType.BANKREF) as ctrlWaveBankRefCell;
					bankRef.Init(objectInstance, objectNode, fieldInstance, fieldAlias);
					bankRef.OnWaveBankRefClick -= this.bankRef_OnWaveBankRefClick;
					bankRef.OnWaveBankRefClick += this.bankRef_OnWaveBankRefClick;

					DataGridViewCell bankCell = new ctrlBlankCell();
					TreeGridNode bankNode = tgn.Nodes.Add(
						"Bank", new[] { bankCell, bankRef, new ctrlBlankCell(), new ctrlBlankCell() });
					bankNode.ReadOnly = objectInstance.IsReadOnly;

					waveRef.OnDrop += bankRef.SetValue;
					bankRef.OnDrop += waveRef.SetValue;

					return tgn;
				}

				this.CreateCompositeControl(cfd, fieldAlias, objectInstance, objectNode, nodes, tab);
				return null;
			}

			if (fieldInstance.Name == "Break")
			{
				var tgn = nodes.Add(string.Empty);
				tgn.Tag = "BREAK";
				tgn.Height = 10;
				tgn.ReadOnly = true;
				return tgn;
			}
			else
			{
				var bIsDefaultValue = true;

				// create list of cells to hold value cell and overrideParent cell
				List<DataGridViewCell> cells = new List<DataGridViewCell>();
				if (this.m_mode == Mode.MULTIEDIT)
				{
					var cell = CellPool.GetCell(CellType.MULTIEDIT) as ctrlMultiEditCell;
					cell.Init(objectInstance, objectNode, fieldInstance, true);
					cells.Add(cell);
				}
				else if (this.m_mode == Mode.TEMPLATE)
				{
					var cell = CellPool.GetCell(CellType.EXPOSE) as ctrlExposeCell;
					cell.Init(objectInstance, objectNode, fieldInstance, true);
					cells.Add(cell);
				}
				else
				{
					cells.Add(new ctrlBlankCell());
				}

				switch (fieldInstance.Units)
				{
					case "ObjectRef":
						var orc = CellPool.GetCell(CellType.OBJECTREF) as ctrlObjectRefCell;
						orc.Init(objectInstance, objectNode, fieldInstance, fieldAlias, objectTypeToCreate);
						if (!orc.IsDefaultValue)
						{
							bIsDefaultValue = false;
						}

						orc.OnObjectEditClick -= this.orc_OnObjectEditClick;
						orc.OnObjectRefClick -= this.cell_OnObjectRefClick;
						orc.OnObjectRefClick += this.cell_OnObjectRefClick;
						orc.OnObjectEditClick += this.orc_OnObjectEditClick;
						cells.Add(orc);
						cells.Add(new ctrlBlankCell());
						break;
					case "variable":
						var vrc = CellPool.GetCell(CellType.VARREF) as ctrlVariableRefCell;
						vrc.Init(objectInstance, objectNode, fieldInstance, fieldAlias, this.m_drawControls);
						if (!vrc.IsDefaultValue)
						{
							bIsDefaultValue = false;
						}

						cells.Add(vrc);
						cells.Add(new ctrlBlankCell());
						break;
					case "CategoryRef":
						var cc = CellPool.GetCell(CellType.CATEGORYREF) as ctrlCategoryRefCell;
						cc.Init(objectInstance, objectNode, fieldInstance, fieldAlias, this.m_drawControls);
						if (!cc.IsDefaultValue)
						{
							bIsDefaultValue = false;
						}

						cells.Add(cc);
						cells.Add(new ctrlBlankCell());
						break;
					case "RolloffCurveRef":
						var rocc = CellPool.GetCell(CellType.ROLLOFF) as ctrlRollOffCurveCell;
						rocc.Init(objectInstance, objectNode, fieldInstance, fieldAlias, this.m_drawControls);
						if (!rocc.IsDefaultValue)
						{
							bIsDefaultValue = false;
						}

						cells.Add(rocc);
						cells.Add(new ctrlBlankCell());
						break;
					case "BankRef":
						var waveRef = CellPool.GetCell(CellType.BANKREF) as ctrlWaveRefCell;
						waveRef.Init(objectInstance, objectNode, fieldInstance, fieldAlias);
						waveRef.OnWaveRefClick -= this.waveRef_OnWaveRefClick;
						waveRef.OnWaveRefClick += this.waveRef_OnWaveRefClick;
						waveRef.OnWaveRefAudition -= this.waveRef_OnWaveRefAudition;
						waveRef.OnWaveRefAudition += this.waveRef_OnWaveRefAudition;
						if (!waveRef.IsDefaultValue)
						{
							bIsDefaultValue = false;
						}

						cells.Add(waveRef);
						cells.Add(new ctrlBlankCell());
						break;
					default:
						{
							switch (fieldInstance.TypeName)
							{
								case "bit":
								case "tristate":
									var cbc = CellPool.GetCell(CellType.CHECKBOX) as ctrlCheckBoxCell;
									cbc.Init(objectInstance, objectNode, fieldInstance, fieldAlias);
									if (!cbc.IsDefaultValue)
									{
										bIsDefaultValue = false;
									}

									cells.Add(cbc);
									cells.Add(new ctrlBlankCell());
									break;
								case "s8":
								case "s16":
								case "s32":
								case "u8":
								case "u16":
								case "u32":
									var sc = CellPool.GetCell(CellType.SLIDER) as ctrlSliderCell;
									sc.Init(objectInstance, objectNode, fieldInstance, fieldAlias, this.m_drawControls);
									if (!sc.IsDefaultValue)
									{
										bIsDefaultValue = false;
									}

									cells.Add(sc);
									var pvc = CellPool.GetCell(CellType.PARENTVALUE) as ctrlParentValueCell;
									pvc.Init(objectInstance, objectNode, fieldInstance, fieldAlias);
									cells.Add(pvc);
									break;
								case "enum":
									var ec = CellPool.GetCell(CellType.ENUM) as ctrlEnumCell;
									ec.Init(objectInstance, objectNode, fieldInstance, fieldAlias, this.m_drawControls);
									if (!ec.IsDefaultValue)
									{
										bIsDefaultValue = false;
									}

									cells.Add(ec);
									cells.Add(new ctrlBlankCell());
									break;
								case "f16":
								case "f32":
									var nc = CellPool.GetCell(CellType.NUMERIC) as ctrlNumericCell;
									nc.Init(objectInstance, objectNode, fieldInstance, fieldAlias, this.m_drawControls);
									if (!nc.IsDefaultValue)
									{
										bIsDefaultValue = false;
									}

									cells.Add(nc);
									cells.Add(new ctrlBlankCell());
									break;
								default:
									var tc = CellPool.GetCell(CellType.TEXT) as ctrlTextCell;
									tc.Init(objectInstance, objectNode, fieldInstance, fieldAlias);
									if (!tc.IsDefaultValue)
									{
										bIsDefaultValue = false;
									}

									cells.Add(tc);
									cells.Add(new ctrlBlankCell());
									break;
							}

							break;
						}
				}

				tab.TreeGrid.Columns["m_MultiSelect"].Visible = this.m_mode == Mode.MULTIEDIT
																|| this.m_mode == Mode.TEMPLATE;
				tab.TreeGrid.Columns["m_ParentValue"].Visible = this.m_showParentValue;


				if (fieldInstance.AllowOverrideControl)
				{
					var opc = CellPool.GetCell(CellType.OVERRIDE) as ctrlOverrideParentCell;
					opc.Init(objectInstance, objectNode, fieldInstance, fieldAlias);

					if (!opc.IsDefaultValue())
					{
						bIsDefaultValue = false;
					}

					cells.Add(opc);
				}
				else
				{
					cells.Add(new ctrlBlankCell());
				}

				string displayName = string.IsNullOrEmpty(fieldAlias) ? fieldInstance.Name : fieldAlias;
				if (index != null) displayName = displayName + "[" + index + "]";
				var tgn = nodes.Add(displayName, cells.ToArray());

				tgn.ReadOnly = this.m_object.IsReadOnly;

				//Add override parent title
				if (tgn.Cells[0] != null && tgn.Cells[0].Value != null && tgn.Cells[0].Value.Equals("Description"))
				{
					tgn.Cells[4].Value = "Override Parent";
				}
				if (!bIsDefaultValue)
				{
					tgn.Cells[0].Style.ForeColor = Color.Red;
					tgn.Cells[0].Style.SelectionForeColor = Color.Red;

					// Don't want to mark tab with asterisk if we're on description cell
					if (tab != null && !tab.Text.EndsWith("*") && fieldInstance.Name.ToLower() != "description")
					{
						tab.Text += "*";
					}
				}
				if (fieldInstance.Tagged)
				{
					List<DataGridViewCell> tagCells = new List<DataGridViewCell>();

					ctrlTagCell cell = new ctrlTagCell(objectInstance, string.IsNullOrEmpty(fieldAlias) ? fieldInstance.Name : fieldAlias, objectNode);


					tagCells.Add(new ctrlBlankCell());
					tagCells.Add(cell);
					tagCells.Add(new ctrlBlankCell());

					nodes.Add(displayName + " Tags:", tagCells.ToArray());
					tab.TreeGrid.CellPainting += (p, j) => TreeCellPaint(p, j, tab.TreeGrid);
					tab.TreeGrid.NodeCollapsed += TreeGridOnNodeCollapsed;
				}
				return tgn;
			}
		}

		private void TreeGridOnNodeCollapsed(object sender, CollapsedEventArgs collapsedEventArgs)
		{
			if (Flatten(collapsedEventArgs.Node.Nodes).Any(p => p.Cells.Cast<DataGridViewCell>().Any(r => r is ctrlTagCell)))
			{
				ctrlTagCell cell = (ctrlTagCell)Flatten(collapsedEventArgs.Node.Nodes).Select(p => p.Cells).SelectMany<DataGridViewCellCollection, DataGridViewCell>(x => x.Cast<DataGridViewCell>()).FirstOrDefault(r => r is ctrlTagCell);
				if (cell != null)
				{
					cell.EditingControl.Visible = false;
				}
			}
		}

		private IEnumerable<TreeGridNode> Flatten(IEnumerable<TreeGridNode> e)
		{
			return e.SelectMany(c => Flatten(c.Nodes)).Concat(e );
		}
		private void TreeCellPaint(object sender, DataGridViewCellPaintingEventArgs e, TreeGridView treeGrid)
		{
			if (e.ColumnIndex > -1 && e.RowIndex > -1)
			{
				if (treeGrid[e.ColumnIndex, e.RowIndex] is ctrlTagCell)
				{
					ctrlTagCell cell = treeGrid[e.ColumnIndex, e.RowIndex] as ctrlTagCell;
					cell.EditingControl.Visible = true;
					cell.SetUpEditingControl();
					cell.EditingControl.Location = new Point(e.CellBounds.X, e.CellBounds.Y);
					cell.EditingControl.Size = new Size(e.CellBounds.Width, e.CellBounds.Height);

				}
			}
		}


		/// <summary>
		/// The create instance controls.
		/// </summary>
		/// <param name="compositeFieldInstance">
		/// The composite field instance.
		/// </param>
		/// <param name="fieldAlias">
		/// The field alias.
		/// </param>
		/// <param name="obj">
		/// The obj.
		/// </param>
		/// <param name="instanceNode">
		/// The instance node.
		/// </param>
		/// <param name="nodes">
		/// The nodes.
		/// </param>
		/// <param name="tab">
		/// The tab.
		/// </param>
		/// <param name="objectTypeToCreate">
		/// The object type to create.
		/// </param>
		/// <returns>
		/// The <see cref="TreeGridNode"/>.
		/// </returns>
		private TreeGridNode CreateInstanceControls(
			ICompositeFieldDefinitionInstance compositeFieldInstance,
			string fieldAlias,
			IObjectInstance obj,
			XmlNode instanceNode,
			TreeGridNodeCollection nodes,
			tbPropertyTab tab,
			string objectTypeToCreate,
			int? index = null)
		{
			TreeGridNode instance = null;
			//create a summary cell for a composite field
			string changedFields = "";
			foreach (XmlNode node in instanceNode.ChildNodes)
			{
				changedFields += changedFields.Equals("") ? "" : ", ";
				changedFields += getCompositeFieldNodeDescription(node);
			}
			ctrlBlankCell changedFieldsCell = new ctrlBlankCell();
			changedFieldsCell.Value = changedFields;

			if (compositeFieldInstance.MaxOccurs != 1)
			{
				/*The "tree" root for the instance is set to an identification filed.
				  The reason for this is to easily identify this particular instance*/
				string indexSuffix = "";
				if (index != null) indexSuffix = "[" + index + "]";

				instance = nodes.Add(
					compositeFieldInstance.Name + indexSuffix, new DataGridViewCell[] { new ctrlBlankCell(), changedFieldsCell });

				if (instance != null)
				{
					if (!obj.IsReadOnly)
					{
						/*The node in the row data is set to the instance node rather than the field node
					  as this helps with add/remove and reorder. The field node is held in the "Value"
					  and "OverrideParent" cells */
						instance.Tag = new RowData(
							obj, compositeFieldInstance, instanceNode.ParentNode, instanceNode, instanceNode);
						instance.ContextMenuStrip = this.mnuRemove;
					}
					else
					{
						instance.Tag = new RowInfo(instanceNode);
					}
				}

				for (var i = 0; i < compositeFieldInstance.Fields.Length; i++)
				{
					// Create rest of fields as TREE children of the identification field.
					this.CreateFieldControl(
						new FieldDefinitionInstance(compositeFieldInstance.CompositeFieldDef.Fields[i], null),
						fieldAlias,
						obj,
						instanceNode,
						instance.Nodes,
						tab,
						objectTypeToCreate);
				}
			}
			else
			{
				// This is a normal composite
				instance = nodes.Add(
					compositeFieldInstance.Name, new DataGridViewCell[] { new ctrlBlankCell(), changedFieldsCell });
				instance.ReadOnly = true;
				instance.Tag = new RowData(obj, compositeFieldInstance, instanceNode.ParentNode, instanceNode, instanceNode);

				foreach (var f in compositeFieldInstance.Fields)
				{
					this.CreateFieldControl(
						new FieldDefinitionInstance(f, null),
						fieldAlias,
						obj,
						instanceNode,
						instance.Nodes,
						tab,
						objectTypeToCreate);
				}
			}

			return instance;
		}

		private string getCompositeFieldNodeDescription(XmlNode node)
		{
			string result = node.Name + ": ";
			//check for more than 1 element since the nodes text will be present as childnode with name "#text"
			if (node.ChildNodes.Count > 1)
			{
				result += "{";
				foreach (XmlNode subNode in node.ChildNodes)
				{
					if (!result.EndsWith("{")) result += ", ";
					result += getCompositeFieldNodeDescription(subNode);
				}
				result += "}";
			}
			else
			{
				result += node.InnerText;
			}
			return result;
		}

		/// <summary>
		/// The create main menu.
		/// </summary>
		private void CreateMainMenu()
		{
			while (this.mnuMain.MenuItems.Count > 1)
			{
				this.mnuMain.MenuItems.RemoveAt(this.mnuMain.MenuItems.Count - 1);
			}

			this.mnuMain.MenuItems.Add(this.mnuExpand);
			this.mnuMain.MenuItems.Add(this.mnuCollapse);
			this.mnuMain.MenuItems.Add(this.mnuShowParentValue);
		}

		/// <summary>
		/// The create state xml.
		/// </summary>
		private void CreateStateXml()
		{
			foreach (var kvp in this.m_displayGroups)
			{
				var tab = kvp.Value;
				XmlDocument doc = null;
				if (!this.m_displayGroupStates.ContainsKey(kvp.Key))
				{
					doc = new XmlDocument();
					doc.AppendChild(doc.CreateElement("state"));
					this.m_displayGroupStates.Add(kvp.Key, doc);
				}
				else
				{
					doc = this.m_displayGroupStates[kvp.Key];
					doc.DocumentElement.RemoveAll();
				}

				CreateStateXml(tab.TreeGrid.Nodes, doc.DocumentElement);
			}
		}

		/// <summary>
		/// The expand tree.
		/// </summary>
		private void ExpandTree()
		{
			foreach (var kvp in this.m_displayGroups)
			{
				var tab = kvp.Value;
				var doc = this.m_displayGroupStates[kvp.Key];
				ExpandTree(doc.DocumentElement.ChildNodes, tab.TreeGrid.Nodes);
			}
		}

		/// <summary>
		/// Initialize the tab page entries.
		/// </summary>
		/// <param name="fieldDefs">
		/// The field definitions.
		/// </param>
		private void InitTabPageEntries(IEnumerable<IFieldDefinition> fieldDefs)
		{
			foreach (var fieldDef in fieldDefs)
			{
				var displayGroup = fieldDef.DisplayGroup ?? "Main";

				if (this.m_displayGroups.ContainsKey(displayGroup.ToUpper()))
				{
					continue;
				}

				tbPropertyTab tabPage = new tbPropertyTab(displayGroup);
				tabPage.TreeGrid.CellEndEdit -= TreeGrid_CellEndEdit;
				tabPage.TreeGrid.NodeCollapsed -= TreeGrid_NodeCollapsed;
				tabPage.TreeGrid.NodeExpanded -= TreeGrid_NodeExpanded;
				tabPage.TreeGrid.CellEndEdit += TreeGrid_CellEndEdit;
				tabPage.TreeGrid.NodeCollapsed += TreeGrid_NodeCollapsed;
				tabPage.TreeGrid.NodeExpanded += TreeGrid_NodeExpanded;
				this.m_displayGroups.Add(displayGroup.ToUpper(), tabPage);

				if (this.m_object.IsReadOnly)
				{
					tabPage.TreeGrid.RowsDefaultCellStyle.BackColor = Color.LightGray;
					tabPage.TreeGrid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
					tabPage.TreeGrid.ReadOnly = true;
				}
				else
				{
					tabPage.TreeGrid.RowsDefaultCellStyle.BackColor = Color.White;
					tabPage.TreeGrid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
					tabPage.TreeGrid.ReadOnly = false;
				}
			}
		}

		void TreeGrid_NodeExpanded(object sender, ExpandedEventArgs e)
		{
			//hide composite field descriptions
			if (e.Node != null && e.Node.Tag is RowInfo)
			{
				e.Node.Cells[2].Value = "";
			}
		}

		void TreeGrid_NodeCollapsed(object sender, CollapsedEventArgs e)
		{
			if (e.Node != null && e.Node.Tag is RowInfo && ((RowInfo)e.Node.Tag).DescriptionNode != null)
			{
				string changedFields = "";
				foreach (XmlNode xmlNode in ((RowInfo)e.Node.Tag).DescriptionNode.ChildNodes)
				{
					changedFields += changedFields.Equals("") ? "" : ", ";
					changedFields += getCompositeFieldNodeDescription(xmlNode);
				}
				e.Node.Cells[2].Value = changedFields;
			}
		}

		/// <summary>
		/// The find tab page entry.
		/// </summary>
		/// <param name="tabName">
		/// The tab name.
		/// </param>
		/// <returns>
		/// The <see cref="tbPropertyTab"/>.
		/// </returns>
		private tbPropertyTab FindTabPageEntry(string tabName)
		{
			if (this.m_object.GetType() == typeof(ITemplateInstance))
			{
				tabName = "Main";
			}

			// Tab page already in control
			foreach (tbPropertyTab tab in this.TabPages)
			{
				if (tab.Text.ToUpper().Replace("*", String.Empty) == tabName.ToUpper())
				{
					if (this.m_object.IsReadOnly)
					{
						tab.TreeGrid.RowsDefaultCellStyle.BackColor = Color.LightGray;
						tab.TreeGrid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
						tab.TreeGrid.ReadOnly = true;
					}
					else
					{
						tab.TreeGrid.RowsDefaultCellStyle.BackColor = Color.White;
						tab.TreeGrid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
						tab.TreeGrid.ReadOnly = false;
					}

					return tab;
				}
			}

			// Tab page created but stored in dictionary
			var tabPage = (from kvp in this.m_displayGroups
						   where kvp.Key.ToUpper() == tabName.ToUpper()
						   select kvp.Value).First();

			if (tabName.ToUpper() == "MAIN")
			{
				this.TabPages.Insert(0, tabPage);
			}
			else
			{
				this.TabPages.Add(tabPage);
			}

			return tabPage;
		}

		/// <summary>
		/// The init expand tree.
		/// </summary>
		private void InitExpandTree()
		{
			this.SuspendLayout();
			foreach (var kvp in this.m_displayGroups)
			{
				var tgv = kvp.Value.TreeGrid;
				tgv.SuspendLayout();
				foreach (var tgn in tgv.Nodes)
				{
					tgn.Expand();
				}

				tgv.ResumeLayout();
			}

			this.ResumeLayout();
		}

		/// <summary>
		/// The on add new instance with object.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void OnAddNewInstanceWithObject(object sender, EventArgs e)
		{
			var menuItem = sender as ToolStripMenuItem;
			if (menuItem != null)
			{
				this.AddNewInstance(menuItem.Text);
			}
		}

		/// <summary>
		/// The on bank status changed.
		/// </summary>
		private void OnBankStatusChanged()
		{
			if (this.m_object != null && this.m_object.ObjectLookup.ContainsKey(this.m_object.Name))
			{
				// grab the newly loaded object instance
				this.CreateStateXml();
				this.m_object = this.m_object.ObjectLookup[this.m_object.Name];
				this.EditObject(this.m_object, this.m_mode, this.mnuMain, this.m_drawControls);
				this.SuspendLayout();
				this.ExpandTree();
				this.ResumeLayout();
			}
			else
			{
				this.Reset();
			}
		}

		/// <summary>
		/// The bank ref_ on wave bank ref click.
		/// </summary>
		/// <param name="bankPath">
		/// The bank path.
		/// </param>
		private void bankRef_OnWaveBankRefClick(string bankPath)
		{
			if (this.OnWaveBankRefClick != null)
			{
				this.OnWaveBankRefClick(bankPath);
			}
		}

		/// <summary>
		/// The cell_ on object ref click.
		/// </summary>
		/// <param name="objectRef">
		/// The object ref.
		/// </param>
		private void cell_OnObjectRefClick(IObjectInstance objectRef)
		{
			if (this.OnObjectRefClick != null)
			{
				this.OnObjectRefClick(objectRef);
			}
		}

		/// <summary>
		/// The m_ direct references_ on object ref click.
		/// </summary>
		/// <param name="objectRef">
		/// The object ref.
		/// </param>
		private void m_DirectReferences_OnObjectRefClick(IObjectInstance objectRef)
		{
			if (this.OnObjectEditClick != null)
			{
				this.OnObjectEditClick(objectRef);
			}
		}

		/// <summary>
		/// The m_ reference_ on object instance click.
		/// </summary>
		/// <param name="objectInstance">
		/// The object instance.
		/// </param>
		private void m_Reference_OnObjectRefClick(IObjectInstance objectInstance)
		{
			if (this.OnObjectRefClick != null)
			{
				this.OnObjectRefClick(objectInstance);
			}
		}

		/// <summary>
		/// The m_ reference_ on template ref click.
		/// </summary>
		/// <param name="template">
		/// The template.
		/// </param>
		private void m_Reference_OnTemplateRefClick(ITemplate template)
		{
			if (this.OnTemplateRefClick != null)
			{
				this.OnTemplateRefClick(template);
			}
		}

		/// <summary>
		/// The mnu add instance_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void mnuAddInstance_Click(object sender, EventArgs e)
		{
			this.AddNewInstance(null);
		}

		/// <summary>
		/// The mnu collapse all_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void mnuCollapseAll_Click(object sender, EventArgs e)
		{
			this.CollapseAll();
		}

		public void CollapseAll()
		{
			var tab = this.SelectedTab as tbPropertyTab;
			if (tab != null)
			{
				tab.TreeGrid.SuspendLayout();
				Collapse(tab.TreeGrid.Nodes);
				tab.TreeGrid.ResumeLayout();
			}
		}

		/// <summary>
		/// The mnu expand all_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void mnuExpandAll_Click(object sender, EventArgs e)
		{
			this.ExpandAll();
		}

		/// <summary>
		/// The mnu remove instance_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void mnuRemoveInstance_Click(object sender, EventArgs e)
		{
			// get current tab
			var tab = (tbPropertyTab)this.SelectedTab;

			// get treegridview
			var tgv = tab.TreeGrid;
			if (tgv.SelectedRows.Count > 0)
			{
				// find current selected row
				var tgn = (TreeGridNode)tgv.SelectedRows[0];

				// extract associated data
				var data = tgn.Tag as RowData;
				if (data != null && data.Node != null)
				{
					// remove xml node from parent xml node
					if (data.ParentNode.ChildNodes.Count != 0)
					{
						data.ParentNode.RemoveChild(data.Node);
					}

					// remove row from the datagridview
					tgn.Parent.Nodes.Remove(tgn);

					// Update objectInstance
					data.ObjectInstance.ComputeReferences();
					data.ObjectInstance.MarkAsDirty();
				}
			}
		}

		/// <summary>
		/// The mnu show parent value_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void mnuShowParentValue_Click(object sender, EventArgs e)
		{
			this.m_showParentValue = !this.m_showParentValue;
			if (this.m_showParentValue)
			{
				this.mnuShowParentValue.Text = "Hide Parent Value";
			}
			else
			{
				this.mnuShowParentValue.Text = "Show Parent Value";
			}

			foreach (var kvp in this.m_displayGroups)
			{
				kvp.Value.TreeGrid.SuspendLayout();
				kvp.Value.TreeGrid.Columns["m_ParentValue"].Visible = this.m_showParentValue;
				kvp.Value.TreeGrid.ResumeLayout();
			}
		}

		/// <summary>
		/// The orc_ on object edit click.
		/// </summary>
		/// <param name="objectRef">
		/// The object ref.
		/// </param>
		private void orc_OnObjectEditClick(IObjectInstance objectRef)
		{
			if (this.OnObjectEditClick != null)
			{
				this.OnObjectEditClick(objectRef);
			}
		}

		/// <summary>
		/// The wave ref_ on wave ref audition.
		/// </summary>
		/// <param name="bankPath">
		/// The bank path.
		/// </param>
		/// <param name="wave">
		/// The wave.
		/// </param>
		private void waveRef_OnWaveRefAudition(string bankPath, string wave)
		{
			if (this.OnWaveRefAudition != null)
			{
				this.OnWaveRefAudition(bankPath, wave);
			}
		}

		/// <summary>
		/// The wave ref_ on wave ref click.
		/// </summary>
		/// <param name="bankPath">
		/// The bank path.
		/// </param>
		/// <param name="wave">
		/// The wave.
		/// </param>
		private void waveRef_OnWaveRefClick(string bankPath, string wave)
		{
			if (this.OnWaveRefClick != null)
			{
				this.OnWaveRefClick(bankPath, wave);
			}
		}

		#endregion
	}
}
