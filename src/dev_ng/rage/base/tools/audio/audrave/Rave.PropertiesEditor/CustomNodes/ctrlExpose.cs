using System.Drawing;
using System.Linq;

namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public partial class ctrlExpose : UserControl
    {
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_objectInstance;
        private XmlNode m_parentNode;
        private Regex m_validChars;

        public ctrlExpose()
        {
            this.InitializeComponent();
        }

        public string Value
        {
            get { return this.txtAlias.Text; }
        }

        public void Setup(IObjectInstance objectInstance, IFieldDefinitionInstance fieldInstance, XmlNode parentNode)
        {
            this.m_validChars = new Regex("[a-z]|[A-Z]|[0-9]|_|-");

            this.m_objectInstance = objectInstance;
            this.m_parentNode = parentNode;
            if (!parentNode.OwnerDocument.BaseURI.ToUpper().Contains("TEMPLATES"))
            {
                throw new Exception("ctrlExpose.Setup() called on node outwith template store");
            }
            this.m_fieldInstance = fieldInstance;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(fieldInstance.Name, parentNode);

            if (this.m_fieldNode != null &&
                this.m_fieldNode.Attributes["ExposeAs"] != null)
            {
                this.ctrlCheck.CheckState = CheckState.Checked;
                this.txtAlias.Text = this.m_fieldNode.Attributes["ExposeAs"].Value;
                this.lblExpose.Text = "Expose as:";
                this.lblExpose.ForeColor = Color.Black;
            }
            else
            {
                this.ctrlCheck.CheckState = CheckState.Unchecked;
                this.txtAlias.Text = string.Empty;
                this.txtAlias.Visible = false;
                this.lblExpose.Text = "Not Exposed";
                this.lblExpose.ForeColor = Color.DarkGray;
            }

            this.txtAlias.LostFocus += this.txtAlias_TextChanged;
            this.ctrlCheck.CheckedChanged += this.ctrlCheck_OnCheckChanged;
        }

        private void ctrlCheck_OnCheckChanged(object sender, EventArgs e)
        {
            if (this.ctrlCheck.Checked)
            {
                if (this.m_fieldNode != null &&
                    this.m_fieldNode.Attributes["ExposeAs"] != null)
                {
                    this.txtAlias.Text = this.m_fieldNode.Attributes["ExposeAs"].Value;
                }
                this.lblExpose.ForeColor = Color.Black;
                this.txtAlias.Visible = true;
                if (this.txtAlias.Text.Length == 0 && m_fieldInstance != null)
                {
	               this.txtAlias.Text = m_parentNode.SelectSingleNode("descendant::Name").InnerXml + m_fieldInstance.Name;
                }
                this.lblExpose.Text = "Expose as:";
            }
            else
            {
                this.lblExpose.Text = "Not Exposed";
                this.lblExpose.ForeColor = Color.DarkGray;
                this.txtAlias.Visible = false;
            }
            this.SetXml(this.txtAlias.Text);
            this.m_objectInstance.MarkAsDirty();
        }

        private void txtAlias_TextChanged(object sender, EventArgs e)
        {
            this.SetXml(this.txtAlias.Text);
            this.m_objectInstance.MarkAsDirty();
        }

        private void SetXml(string text)
        {
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_fieldInstance.Name, this.m_parentNode);

            if (text.Length > 0)
            {
                text = text.Replace(" ", "_");

                text = this.txtAlias.Text.Where(c => !this.m_validChars.IsMatch(c.ToString())).Aggregate(text, (current, c) => current.Replace(c.ToString(), string.Empty));
                this.txtAlias.Text = text;
            }

            if (this.ctrlCheck.CheckState == CheckState.Checked &&
                this.txtAlias.Text != "")
            {
                if (this.m_fieldNode == null)
                {
                    this.m_fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_fieldInstance.Name);
                    this.m_parentNode.AppendChild(this.m_fieldNode);
                    this.m_fieldNode.InnerText = this.m_fieldInstance.DefaultValue;
                }
                var fieldElement = this.m_fieldNode as XmlElement;
                fieldElement.SetAttribute("ExposeAs", this.txtAlias.Text);
            }
            else
            {
                if (this.m_fieldNode != null &&
                    this.m_fieldNode.Attributes["ExposeAs"] != null)
                {
                    this.m_fieldNode.Attributes.RemoveNamedItem("ExposeAs");
                }
            }
        }


    }
}