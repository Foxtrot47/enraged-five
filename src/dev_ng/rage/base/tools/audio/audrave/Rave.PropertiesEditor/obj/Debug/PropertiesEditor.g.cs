﻿#pragma checksum "..\..\PropertiesEditor.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "98B117785DF4B22B0AE2E564B4A332D1D0D054A236EFF57C6B0E9B2A26D0D693"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Rave.PropertiesEditor {
    
    
    /// <summary>
    /// PropertiesEditor
    /// </summary>
    public partial class PropertiesEditor : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 6 "..\..\PropertiesEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Rave.PropertiesEditor.PropertiesEditor PropertiesEditorView;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\PropertiesEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid HeaderGrid;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\PropertiesEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ObjectName;
        
        #line default
        #line hidden
        
        
        #line 328 "..\..\PropertiesEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image ObjectTypeImage;
        
        #line default
        #line hidden
        
        
        #line 336 "..\..\PropertiesEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ObjectType;
        
        #line default
        #line hidden
        
        
        #line 355 "..\..\PropertiesEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid MainPanel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Rave.PropertiesEditor;component/propertieseditor.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\PropertiesEditor.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.PropertiesEditorView = ((Rave.PropertiesEditor.PropertiesEditor)(target));
            
            #line 8 "..\..\PropertiesEditor.xaml"
            this.PropertiesEditorView.KeyDown += new System.Windows.Input.KeyEventHandler(this.PropertiesEditor2_OnKeyDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.HeaderGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            
            #line 94 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.ExpandAll_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 95 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.CollapseAll_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            
            #line 96 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.EditFloating_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 97 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.Refresh_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 98 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.AutoUpdateClick);
            
            #line default
            #line hidden
            return;
            case 8:
            this.ObjectName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            
            #line 119 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.AutoUpdateClick);
            
            #line default
            #line hidden
            return;
            case 10:
            
            #line 176 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.MuteButton_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 212 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.SoloButton_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            
            #line 263 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.UndoButton_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            
            #line 277 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.RedoButton_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            
            #line 291 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.PlayButton_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            
            #line 304 "..\..\PropertiesEditor.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.StopButton_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.ObjectTypeImage = ((System.Windows.Controls.Image)(target));
            return;
            case 17:
            this.ObjectType = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 18:
            this.MainPanel = ((System.Windows.Controls.Grid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

