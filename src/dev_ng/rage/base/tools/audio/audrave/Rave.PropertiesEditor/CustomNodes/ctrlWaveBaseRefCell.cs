﻿namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.PropertiesEditor.Infrastructure.Interfaces;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Waves;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    internal abstract class ctrlWaveBaseRefCell : DataGridViewLinkCell,
                                     IWaveRef
    {
        protected string m_alias;
        protected string m_bankPath;
        protected IFieldDefinitionInstance m_fieldInstance;
        protected XmlNode m_fieldNode;
        protected IObjectInstance m_object;
        protected XmlNode m_parentNode;
        protected string m_waveName;

        #region IFieldCell Members

        public bool IsDefaultValue
        {
            get { return (this.m_fieldNode == null || this.m_fieldInstance.DefaultValue == this.m_fieldNode.InnerText); }
        }

        #endregion

        #region IWaveRef Members

        public event Action OnDrop;

        public void Drop(WaveNode wn)
        {
            if (this.m_object.IsReadOnly || wn == null) return;

            var oldKey = this.m_bankPath + "\\" + this.m_waveName;
            if (WaveManager.WaveRefs.ContainsKey(oldKey))
            {
                WaveManager.WaveRefs[oldKey].RemoveAll(x => x.Name == this.m_object.Name);
            }

            this.m_bankPath = wn.GetBankPath();

            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            if (this.m_fieldNode == null)
            {
                var e = this.m_object.Bank.Document.CreateElement(this.m_alias);
                this.m_object.Node.AppendChild(e);
                this.m_fieldNode = e;
            }

            if (this.m_fieldNode.Name != "BankName")
            {
                this.m_waveName = wn.GetWaveName();
            }

            if (!this.m_fieldNode.HasChildNodes &&
                this.m_waveName != null)
            {
                // create elements for wave/bank name
                XmlNode n = this.m_fieldNode.OwnerDocument.CreateElement("WaveName");
                n.InnerText = this.m_waveName;
                this.m_fieldNode.AppendChild(n);

                n = this.m_fieldNode.OwnerDocument.CreateElement("BankName");
                n.InnerText = this.m_bankPath;
                this.m_fieldNode.AppendChild(n);
            }
            else
            {
                if (this.m_fieldInstance.Units == "BankRef")
                {
                    this.m_fieldNode.InnerText = this.m_bankPath;
                }
                else if (this.m_fieldNode.ChildNodes[0].Name == "WaveName")
                {
                    this.m_fieldNode.ChildNodes[0].InnerText = this.m_waveName;
                    this.m_fieldNode.ChildNodes[1].InnerText = this.m_bankPath;
                }
                else if (this.m_fieldNode.ChildNodes[0].Name == "BankName")
                {
                    this.m_fieldNode.ChildNodes[1].InnerText = this.m_waveName;
                    this.m_fieldNode.ChildNodes[0].InnerText = this.m_bankPath;
                }
            }

            var newKey = this.m_bankPath;
            if (this.m_waveName != null)
            {
                newKey += "\\" + this.m_waveName;
            }

            this.SetValue();

            if (!WaveManager.WaveRefs.ContainsKey(newKey))
            {
                WaveManager.WaveRefs.Add(newKey, new List<IObjectInstance>());
            }
            WaveManager.WaveRefs[newKey].Add(this.m_object);
            this.m_object.MarkAsDirty();

            this.OnDrop();
        }

        #endregion

        public event Action<string, string> OnWaveRefAudition;

        public virtual void Init(IObjectInstance obj, XmlNode parent, IFieldDefinitionInstance field, string fieldAlias)
        {
            this.m_object = obj;
            this.m_parentNode = parent;
            this.m_fieldInstance = field;
            this.m_alias = string.IsNullOrEmpty(fieldAlias) ? field.Name : fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, parent);
            this.SetValue();
            var menu = new ContextMenuStrip();
            menu.Items.Add("Audition Wave", null, this.On_Audition);
            if (!obj.IsReadOnly)
            {
                menu.Items.Add("Remove", null, this.On_Remove);
            }
            this.ContextMenuStrip = menu;
        }

        public abstract void SetValue();

        protected override void Dispose(bool disposing)
        {
            this.m_object = null;
            this.m_parentNode = null;
            this.m_fieldNode = null;
            this.m_fieldInstance = null;
            base.Dispose(disposing);
        }

        private void On_Audition(object sender, EventArgs e)
        {
            if (this.OnWaveRefAudition != null &&
                    !string.IsNullOrEmpty(this.m_bankPath) &&
                    !string.IsNullOrEmpty(this.m_waveName))
            {
                this.OnWaveRefAudition(this.m_bankPath, this.m_waveName);
            }
        }

        private void On_Remove(object sender, EventArgs e)
        {
            if (!this.m_fieldNode.HasChildNodes) return;

            this.m_fieldNode.RemoveAll();
            this.Value = "not set";
            this.ToolTipText = string.Empty;

            var key = String.Concat(this.m_bankPath, "\\", this.m_waveName);
            if (WaveManager.WaveRefs.ContainsKey(key))
            {
                WaveManager.WaveRefs[key].RemoveAll(x => x.Name == this.m_object.Name);
            }
            this.m_object.MarkAsDirty();

            this.m_bankPath = null;
            this.m_waveName = null;
        }
    }
}