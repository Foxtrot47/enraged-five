namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.VariableList;

    [DesignerCategory("code")]
    internal class VariableRefEditor : TextBox, IDataGridViewEditingControl
    {
        private string m_alias;

        private IFieldDefinitionInstance m_fieldInstance;

        private XmlNode m_fieldNode;

        private IObjectInstance m_objectInstance;

        private XmlNode m_parentNode;

        private IVariableList inputVariableList;

        /* Commented out intellisense autocomplete options - using FlatList instead
    private int currMenuLevel;
     */

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get
            {
                return this.Text;
            }

            set
            {
            }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return this.Cursor; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance, XmlNode parentNode, IFieldDefinitionInstance fieldInstance, string fieldAlias)
        {
            this.m_objectInstance = objectInstance;
            this.m_parentNode = parentNode;
            this.m_fieldInstance = fieldInstance;
            this.m_alias = fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            this.Text = this.m_fieldNode != null ? this.m_fieldNode.InnerText : this.m_fieldInstance.DefaultValue;
            var toolTip = new ToolTip();
            toolTip.SetToolTip(this, "Right-click to access variable list");

            // Initialize variable list based on object instance
            this.inputVariableList = objectInstance.GetVariableList(fieldAlias, fieldInstance.TypeDefinition);

            // Enable textbox auto complete mode
            this.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var source = new AutoCompleteStringCollection();
            source.AddRange(this.inputVariableList.FlatListLabels.ToArray());
            this.AutoCompleteCustomSource = source;

            /* Commented out intellisense autocomplete options - using FlatList instead
        this.UpdateAutoCompleteOptions();

        // Enables intellisense typing on textbox
        this.TextChanged += VariableRefEditor_TextChanged;
         */

            // Enable context menu (displayed when cell right-clicked)
            this.ContextMenu = this.inputVariableList.GetContextMenu_Forms(this.MenuItem_Clicked);
			this.SelectAll();
        }

        public void UpdateNode()
        {
            if (this.m_fieldNode == null)
            {
                this.m_fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_alias);
                this.m_parentNode.AppendChild(this.m_fieldNode);
            }

            this.m_fieldNode.InnerText = this.Text;

            this.m_objectInstance.MarkAsDirty();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.UpdateNode();
                return false;
            }

            if (keyData == (Keys.Control | Keys.C) && !string.IsNullOrEmpty(this.Text))
            {
                Clipboard.SetText(this.Text);
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        /* Commented out intellisense autocomplete options - using FlatList instead
    private void UpdateAutoCompleteOptions()
    {
        // Get list of sub groups at current menu level
        var groups = this.variableList.GetSubGroups(this.Text);

        // Get list of items at current menu level
        var items = this.variableList.GetGroupItems(this.Text);

        // Combine sub groups and items to create autocomplete options
        var options = new List<string>();
        if (null != groups)
        {
            options.AddRange(groups.Select(@group => @group.FullPath).ToList());
        }

        if (null != items)
        {
            options.AddRange(items.Select(@item => item.FullPath).ToList());
        }

        var source = new AutoCompleteStringCollection();
        source.AddRange(options.ToArray());
        this.AutoCompleteCustomSource = source;
    }

    private void VariableRefEditor_TextChanged(object sender, EventArgs e)
    {
        var menuLevel = this.Text.Count(c => c == '.');
        if (menuLevel != this.currMenuLevel)
        {
            this.UpdateAutoCompleteOptions();
            this.currMenuLevel = menuLevel;
        }
    }
     */

        private void MenuItem_Clicked(object obj, EventArgs args)
        {
            var item = obj as MenuItem;
            if (null != item)
            {
                var tag = item.Tag as string;
                if (null != tag)
                {
                    this.Text = tag;
                }
            }
        }
    }
}