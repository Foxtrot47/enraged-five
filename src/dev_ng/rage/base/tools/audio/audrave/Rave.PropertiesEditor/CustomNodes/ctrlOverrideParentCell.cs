namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    public class ctrlOverrideParentCell : DataGridViewCheckBoxCell,
                                          IFieldCell
    {
        private string m_alias;
        private OverrideParentEditor m_editor;
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_object;
        private XmlNode m_parentNode;

        public override Type EditType
        {
            get { return typeof (OverrideParentEditor); }
        }

        public override Type ValueType
        {
            get { return typeof (CheckState); }
        }

        public override object DefaultNewRowValue
        {
            get { return false; }
        }

        #region IFieldCell Members

        bool IFieldCell.IsDefaultValue
        {
            get { return true; }
        }

        public CellType Type
        {
            get { return CellType.OVERRIDE; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance,
                         XmlNode parent,
                         IFieldDefinitionInstance fieldInstance,
                         string fieldAlias)
        {
            this.m_object = objectInstance;
            this.m_parentNode = parent;
            this.m_fieldInstance = fieldInstance;
            this.m_alias = string.IsNullOrEmpty(fieldAlias) ? fieldInstance.Name : fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);

            this.Value = this.GetValue();
        }

        public bool IsDefaultValue()
        {
            if (this.m_fieldNode == null)
            {
                return true;
            }
            if (this.m_fieldNode.Attributes["overrideParent"] == null)
            {
                return true;
            }
            if (this.m_fieldNode.Attributes["overrideParent"].Value == "yes")
            {
                return false;
            }
            return true;
        }

        private CheckState GetValue()
        {
            var fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            if (fieldNode == null)
            {
                return CheckState.Unchecked;
            }
            if (fieldNode.Attributes["overrideParent"] == null)
            {
                return CheckState.Unchecked;
            }
            if (fieldNode.Attributes["overrideParent"].Value == "yes")
            {
                return CheckState.Checked;
            }
            return CheckState.Unchecked;
        }

        public override void InitializeEditingControl(int rowIndex,
                                                      object initialFormattedValue,
                                                      DataGridViewCellStyle dataGridViewCellStyle)
        {
            // Set the value of the editing control to the current cell value.
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
            this.m_editor = this.DataGridView.EditingControl as OverrideParentEditor;
            this.m_editor.Init(this.m_object, this.m_parentNode, this.m_fieldInstance, this.m_alias);
        }

        private static bool PartPainted(DataGridViewPaintParts paintParts, DataGridViewPaintParts paintPart)
        {
            return (paintParts & paintPart) != 0;
        }

        protected override void Paint(Graphics graphics,
                                      Rectangle clipBounds,
                                      Rectangle cellBounds,
                                      int rowIndex,
                                      DataGridViewElementStates cellState,
                                      object value,
                                      object formattedValue,
                                      string errorText,
                                      DataGridViewCellStyle cellStyle,
                                      DataGridViewAdvancedBorderStyle advancedBorderStyle,
                                      DataGridViewPaintParts paintParts)
        {
            this.Value = this.GetValue();
            base.Paint(graphics,
                clipBounds,
                cellBounds,
                rowIndex,
                cellState,
                value,
                formattedValue,
                errorText,
                cellStyle,
                advancedBorderStyle,
                paintParts);
        }

        public override void PositionEditingControl(bool setLocation,
                                                    bool setSize,
                                                    Rectangle cellBounds,
                                                    Rectangle cellClip,
                                                    DataGridViewCellStyle cellStyle,
                                                    bool singleVerticalBorderAdded,
                                                    bool singleHorizontalBorderAdded,
                                                    bool isFirstDisplayedColumn,
                                                    bool isFirstDisplayedRow)
        {
            var editingControlBounds = this.PositionEditingPanel(cellBounds,
                cellClip,
                cellStyle,
                singleVerticalBorderAdded,
                singleHorizontalBorderAdded,
                isFirstDisplayedColumn,
                isFirstDisplayedRow);

            editingControlBounds = this.GetAdjustedEditingControlBounds(editingControlBounds, cellStyle);
            this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
            this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
        }

        private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
        {
            // Add a 1 pixel padding on the left and right of the editing control
            editingControlBounds.X += 1;
            editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);

            // Adjust the vertical location of the editing control:
            var preferredHeight = this.Size.Height;
            if (preferredHeight < editingControlBounds.Height)
            {
                switch (cellStyle.Alignment)
                {
                    case DataGridViewContentAlignment.MiddleLeft:
                    case DataGridViewContentAlignment.MiddleCenter:
                    case DataGridViewContentAlignment.MiddleRight:
                        editingControlBounds.Y += (editingControlBounds.Height - preferredHeight) / 2;
                        break;
                    case DataGridViewContentAlignment.BottomLeft:
                    case DataGridViewContentAlignment.BottomCenter:
                    case DataGridViewContentAlignment.BottomRight:
                        editingControlBounds.Y += editingControlBounds.Height - preferredHeight;
                        break;
                }
            }

            return editingControlBounds;
        }

        public override void DetachEditingControl()
        {
            this.m_editor.UpdateNode();
            base.DetachEditingControl();
            this.Value = this.GetValue();
        }
    }
}