namespace Rave.PropertiesEditor.Editors
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Controls.Forms.Popups;
    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.PropertiesEditor.CustomNodes;
    using Rave.TypeDefinitions;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    public partial class frmGridEditor : Form
    {
        private const int WM_LBUTTONDOWN = 0x0201;
        private readonly Dictionary<string, List<string>> m_fields;
        private ITypeDefinition m_baseType;

        public frmGridEditor()
        {
            this.InitializeComponent();
            this.m_fields = new Dictionary<string, List<string>>();
            this.m_DataGridView.DragOver += DataGridView_DragOver;
            this.m_DataGridView.DragDrop += this.DataGridView_DragDrop;
            this.m_DataGridView.MouseDown += this.DataGridView_MouseDown;
        }

        [DllImport("user32.dll")]
        private static extern bool SendMessage(IntPtr hWnd, Int32 msg, Int32 wParam, Int32 lParam);

        [DllImport("user32.dll")]
        private static extern IntPtr ChildWindowFromPoint(IntPtr hwndParent, int x, int y);

        [DllImport("user32.dll")]
        private static extern bool GetWindowRect(IntPtr hWnd, out Rect lpRect);

        private static int MakeLong(int loWord, int hiWord)
        {
            return (hiWord << 16) | (loWord & 0xffff);
        }

        private void DataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            var info = this.m_DataGridView.HitTest(e.X, e.Y);
            if (info.RowIndex >= 0 &&
                info.ColumnIndex >= 0)
            {
                var dgvc = this.m_DataGridView[info.ColumnIndex, info.RowIndex];

                if (this.m_DataGridView.CurrentCell != dgvc)
                {
                    this.m_DataGridView.CurrentCell = dgvc;
                    var success = this.m_DataGridView.BeginEdit(false);
                    if (success)
                    {
                        // For editing controls derived from TextBoxBase, ensure text is scrolled to the left
                        if (this.m_DataGridView.EditingControl is TextBoxBase)
                        {
                            var textBox = (TextBoxBase) this.m_DataGridView.EditingControl;
                            textBox.SelectionStart = 0;
                            textBox.ScrollToCaret();
                        }

                        // Calculate co-ordinates of where user clicked within editing control
                        var clickPoint = new Point(e.X - info.ColumnX - this.m_DataGridView.EditingControl.Left,
                                                   e.Y - info.RowY - this.m_DataGridView.EditingControl.Top);

                        // Simulate mouse click within control
                        if (!(this.m_DataGridView.EditingControl is CategoryRefEditor)) SimulateMouseDown(this.m_DataGridView.EditingControl.Handle, clickPoint);
                    }
                    else
                    {
                        base.OnMouseDown(e);
                    }
                }
                else
                {
                    base.OnMouseDown(e);
                }
            }
        }

        private static void SimulateMouseDown(IntPtr hWnd, Point clickPoint)
        {
            // Check for child control within editing control
            var childHwnd = ChildWindowFromPoint(hWnd, clickPoint.X, clickPoint.Y);
            // If no child window, simulate mouse click within editing control
            if (childHwnd == hWnd)
            {
                SendMouseDown(hWnd, clickPoint);
            }

                // Otherwise, simulate mouse click within child control
            else
            {
                // Get screen co-ordinates of parent (edit control) and child
                Rect parentRect, childRect;
                if (!GetWindowRect(hWnd, out parentRect))
                {
                    return;
                }
                if (!GetWindowRect(childHwnd, out childRect))
                {
                    return;
                }

                // Determine co-ordinates of where user clicked within child control
                var childClickPoint = new Point(clickPoint.X - (childRect.Left - parentRect.Left),
                                                clickPoint.Y - (childRect.Top - parentRect.Top));

                // Simulate mouse click within child control
                SimulateMouseDown(childHwnd, childClickPoint);
            }
        }

        private static void SendMouseDown(IntPtr hWnd, Point clickPoint)
        {
            var lParam = MakeLong(clickPoint.X, clickPoint.Y);
            SendMessage(hWnd, WM_LBUTTONDOWN, 0, lParam);
        }

        public void Init(List<IObjectInstance> objects, ITypeDefinition baseType)
        {
            this.m_baseType = baseType;
            this.m_fields.Clear();
            //Need to add the required columns
            foreach (var obj in objects)
            {
                if (this.m_baseType != null)
                {
                    foreach (var field in this.m_baseType.Fields)
                    {
                        this.GetFields(field, obj, obj.Node, "", new List<string>());
                    }
                }
            }

            foreach (var kvp in this.m_fields)
            {
                kvp.Value.Sort();
                foreach (var columnName in kvp.Value)
                {
                    this.m_DataGridView.Columns.Add(columnName, columnName);
                    this.m_DataGridView.Columns[columnName].Width = 100;
                }
            }

            foreach (var objInstance in objects)
            {
                if (this.m_baseType != null)
                {
                    var index = this.m_DataGridView.Rows.Add();
                    this.m_DataGridView.Rows[index].HeaderCell.Value = objInstance.Name;

                    this.m_DataGridView.Rows[index].DefaultCellStyle.BackColor = Color.LightGray;
                    this.m_DataGridView.Rows[index].ReadOnly = true;

                    foreach (var field in this.m_baseType.Fields)
                    {
                        this.CreateFields(index, new FieldDefinitionInstance(field, null), objInstance, objInstance.Node, "", null, new List<string>());
                    }
                }
            }

            var sb = new StringBuilder();
            sb.Append(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            sb.Append("\\Rave\\GridLayouts\\GridView_");
            sb.Append(this.m_baseType.ToString());
            sb.Append(".xml");

            var filePath = sb.ToString();

            if (File.Exists(filePath))
            {
                try
                {
                    var doc = new XmlDocument();
                    doc.Load(filePath);
                    foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                    {
                        var col = this.m_DataGridView.Columns[node.Name];
                        if (col != null)
                        {
                            col.Visible = node.Attributes["visible"].Value == "True" ? true : false;
                            col.Width = Int32.Parse(node.Attributes["width"].Value);
                            if (node.Attributes["index"] != null)
                            {
                                col.DisplayIndex = Int32.Parse(node.Attributes["index"].Value);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                }
            }
        }

        private void CreateFields(int rowIndex,
                                  FieldDefinitionInstance fieldInstance,
                                  IObjectInstance obj,
                                  XmlNode parentNode,
                                  string parentName,
                                  string fieldAlias,
                                  List<string> processedFields)
        {
            if (!fieldInstance.Visible)
            {
                return;
            }
            var compField = fieldInstance.AsCompositeField();
            if (compField != null)
            {
                if (compField.Units == "WaveRef")
                {
                    var wrc = CellPool.GetCell(CellType.WAVEREF) as ctrlWaveRefCell;
                    wrc.Init(obj, parentNode, fieldInstance, fieldAlias);
                    wrc.ReadOnly = false;
                    this.m_DataGridView[parentName + fieldInstance.Name, rowIndex] = wrc;
                    processedFields.Add(String.Concat(parentName, fieldInstance.Name));
                    return;
                }
                this.CreateCompositeFields(rowIndex, compField, obj, parentNode, parentName, processedFields);
            }
            else
            {
                DataGridViewCell cell;
                switch (fieldInstance.Units)
                {
                    case "ObjectRef":
                        var orc = CellPool.GetCell(CellType.OBJECTREF) as ctrlObjectRefCell;
                        orc.Init(obj, parentNode, fieldInstance, fieldAlias, null);
                        cell = orc;
                        break;
                    case "variable":
                        var vrc = CellPool.GetCell(CellType.VARREF) as ctrlVariableRefCell;
                        vrc.Init(obj, parentNode, fieldInstance, fieldAlias, false);
                        cell = vrc;
                        break;
                    case "CategoryRef":
                        var catrc = CellPool.GetCell(CellType.CATEGORYREF) as ctrlCategoryRefCell;
                        catrc.Init(obj, parentNode, fieldInstance, fieldAlias, false);
                        cell = catrc;
                        break;
                    case "RolloffCurveRef":
                        var rocc = CellPool.GetCell(CellType.ROLLOFF) as ctrlRollOffCurveCell;
                        rocc.Init(obj, parentNode, fieldInstance, fieldAlias, false);
                        cell = rocc;
                        break;
                    case "BankRef":
                        var wrc = CellPool.GetCell(CellType.BANKREF) as ctrlWaveRefCell;
                        wrc.Init(obj, parentNode, fieldInstance, fieldAlias);
                        wrc.ReadOnly = false;
                        cell = wrc;
                        break;
                    default:
                        {
                            switch (fieldInstance.TypeName)
                            {
                                case "bit":
                                case "tristate":
                                    var cbc = CellPool.GetCell(CellType.CHECKBOX) as ctrlCheckBoxCell;
                                    cbc.Init(obj, parentNode, fieldInstance, fieldAlias);
                                    cell = cbc;
                                    break;
                                case "s8":
                                case "s16":
                                case "s32":
                                case "u8":
                                case "u16":
                                case "u32":
                                    var sc = CellPool.GetCell(CellType.SLIDER) as ctrlSliderCell;
                                    sc.Init(obj, parentNode, fieldInstance, fieldAlias, false);
                                    cell = sc;
                                    break;
                                case "enum":
                                    var ec = CellPool.GetCell(CellType.ENUM) as ctrlEnumCell;
                                    ec.Init(obj, parentNode, fieldInstance, fieldAlias, false);
                                    cell = ec;
                                    break;
                                case "f16":
                                case "f32":
                                    var nc = CellPool.GetCell(CellType.NUMERIC) as ctrlNumericCell;
                                    nc.Init(obj, parentNode, fieldInstance, fieldAlias, false);
                                    cell = nc;
                                    break;
                                default:
                                    var tc = CellPool.GetCell(CellType.TEXT) as ctrlTextCell;
                                    tc.Init(obj, parentNode, fieldInstance, fieldAlias);
                                    cell = tc;
                                    break;
                            }
                            break;
                        }
                }

                var name = String.Concat(parentName, fieldInstance.Name);
                if (processedFields.Contains(name))
                {
                    var i = 2;
                    name = String.Concat(parentName, i, "_", fieldInstance.Name);

                    while (processedFields.Contains(name))
                    {
                        name = String.Concat(parentName, ++i, "_", fieldInstance.Name);
                    }

                    processedFields.Add(name);
                }
                else
                {
                    processedFields.Add(name);
                }

                this.m_DataGridView[name, rowIndex] = cell;
                cell.ReadOnly = !obj.Bank.IsCheckedOut;
                cell.Style.BackColor = obj.Bank.IsCheckedOut ? Color.White : Color.LightGray;
            }
        }

        private void CreateCompositeFields(int rowIndex,
                                           ICompositeFieldDefinitionInstance compFieldInstance,
                                           IObjectInstance obj,
                                           XmlNode parentNode,
                                           string parentName,
                                           List<string> processedFields)
        {
            foreach (XmlNode child in parentNode.ChildNodes)
            {
                if (child.Name ==
                    compFieldInstance.Name)
                {
                    foreach (var field in compFieldInstance.Fields)
                    {
                        this.CreateFields(rowIndex,
                                     new FieldDefinitionInstance(field, null), 
                                     obj,
                                     child,
                                     parentName + compFieldInstance.Name + "_",
                                     null,
                                     processedFields);
                    }
                }
            }
        }

        private void GetFields(IFieldDefinition field,
                               IObjectInstance obj,
                               XmlNode parentNode,
                               string parentName,
                               List<string> processedNames)
        {
            if (!field.Visible)
            {
                return;
            }
            var compField = field as ICompositeFieldDefinition;
            if (compField != null)
            {
                if (compField.Units == "WaveRef")
                {
                    var displayGroup = compField.DisplayGroup;
                    if (displayGroup == null)
                    {
                        displayGroup = "Default";
                    }
                    if (!this.m_fields.ContainsKey(displayGroup))
                    {
                        this.m_fields.Add(displayGroup, new List<string>());
                    }
                    var name = String.Concat(parentName, field.Name);
                    if (!this.m_fields[displayGroup].Contains(name))
                    {
                        this.m_fields[displayGroup].Add(name);
                        processedNames.Add(name);
                    }
                    return;
                }
                this.FlattenFields(compField, obj, parentNode, parentName, processedNames);
            }
            else
            {
                var displayGroup = field.DisplayGroup;
                if (displayGroup == null)
                {
                    displayGroup = "Default";
                }
                if (!this.m_fields.ContainsKey(displayGroup))
                {
                    this.m_fields.Add(displayGroup, new List<string>());
                }

                var name = String.Concat(parentName, field.Name);

                if (processedNames.Contains(name))
                {
                    var i = 2;
                    name = String.Concat(parentName, i, "_", field.Name);

                    while (processedNames.Contains(name))
                    {
                        name = String.Concat(parentName, ++i, "_", field.Name);
                    }

                    processedNames.Add(name);
                }
                else
                {
                    processedNames.Add(name);
                }

                if (!this.m_fields[displayGroup].Contains(name))
                {
                    this.m_fields[displayGroup].Add(name);
                }
            }
        }

        private void FlattenFields(ICompositeFieldDefinition compField,
                                   IObjectInstance obj,
                                   XmlNode parentNode,
                                   string parentName,
                                   List<string> processedNames)
        {
            foreach (XmlNode child in parentNode.ChildNodes)
            {
                if (child.Name ==
                    compField.Name)
                {
                    foreach (var field in compField.Fields)
                    {
                        this.GetFields(field, obj, child, parentName + compField.Name + "_", processedNames);
                    }
                }
            }
        }

        private static void DataGridView_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move | DragDropEffects.Link;
        }

        private void DataGridView_DragDrop(object sender, DragEventArgs e)
        {
            var p = this.m_DataGridView.PointToClient(new Point(e.X, e.Y));
            var info = this.m_DataGridView.HitTest(p.X, p.Y);

            var dgvc = this.m_DataGridView[info.ColumnIndex, info.RowIndex];
            if (dgvc != null)
            {
                if (e.Data.GetDataPresent(typeof (WaveNode[])) &&
                    dgvc is ctrlWaveRefCell)
                {
                    var wn = ((WaveNode[]) e.Data.GetData(typeof (WaveNode[])))[0];
                    var waveRef = dgvc as ctrlWaveRefCell;
                    waveRef.Drop(wn);
                }
                else if (e.Data.GetDataPresent(typeof (SoundNode[])) &&
                         dgvc is ctrlObjectRefCell)
                {
                    var sn = ((SoundNode[]) e.Data.GetData(typeof (SoundNode[])))[0];
                    var objRef = dgvc as ctrlObjectRefCell;
                    objRef.Drop(sn);
                }
                else if (e.Data.GetDataPresent(typeof (WaveNode[])) &&
                         dgvc is ctrlObjectRefCell)
                {
                    var wn = ((WaveNode[]) e.Data.GetData(typeof (WaveNode[])))[0];
                    var objRef = dgvc as ctrlObjectRefCell;
                    objRef.Drop(wn);
                }
            }
        }

        private void ColumnHeader_Click(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right &&
                this.m_DataGridView.Columns.Count > 0)
            {
                var cc = new frmColumnChooser(this.m_DataGridView.Columns);
                cc.ShowDialog();
                cc.Dispose();
            }
        }

        private void mnuSaveLayout_Click(object sender, EventArgs e)
        {
            var doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("Layout"));

            XmlElement el;
            foreach (DataGridViewColumn col in this.m_DataGridView.Columns)
            {
                el = doc.CreateElement(col.HeaderText);
                el.SetAttribute("visible", col.Visible.ToString());
                el.SetAttribute("width", col.Width.ToString());
                el.SetAttribute("index", col.DisplayIndex.ToString());
                doc.DocumentElement.AppendChild(el);
            }

            var sb = new StringBuilder();
            sb.Append(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            sb.Append("\\Rave\\GridLayouts");

            var dir = sb.ToString();

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            sb.Append("\\");
            sb.Append("GridView_");
            sb.Append(this.m_baseType.ToString());
            sb.Append(".xml");

            var filePath = sb.ToString();

            var tw = new XmlTextWriter(filePath, null) {Formatting = Formatting.Indented};
            try
            {
                doc.WriteContentTo(tw);
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }
            finally
            {
                tw.Close();
            }
        }

        #region Nested type: RECT

        private struct Rect
        {
            public Int32 Bottom;
            public Int32 Left;
            public Int32 Right;
            public Int32 Top;
        }

        #endregion
    }
}