namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    public partial class TSliderEditor : UserControl
    {
        private bool bNumericUpDownUpdated;
        private string m_Alias;
        private ISimpleFieldDefinition m_Field;
        private XmlNode m_FieldNode;
        private XmlNode m_TemplateFieldNode;
        private decimal scalarNeg;
        private decimal scalarPos;

        public TSliderEditor()
        {
            this.InitializeComponent();
        }

        public string Value
        {
            //return the numeric up/down to ensure accuracy. Slider is simply a graphical representation,
            //not accurate for anything that has to be scaled
            get
            {
                switch (this.m_Field.Units)
                {
                    case "mB":
                    case "0.01units":
                    case "cents":
                        return ((int) (this.m_Numeric.Value * 100)).ToString();
                    default:
                        return ((int) this.m_Numeric.Value).ToString();
                }
            }
        }

        public void Setup(XmlNode parent, XmlNode templateParent, IFieldDefinition field)
        {
            this.m_Field = field as ISimpleFieldDefinition;
            this.m_Alias = string.Empty;
            this.m_FieldNode = null;

            this.m_TemplateFieldNode = PropertyEditorUtil.FindXmlNode(field.Name, templateParent);
            if (this.m_TemplateFieldNode != null &&
                this.m_TemplateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_Alias = this.m_TemplateFieldNode.Attributes["ExposeAs"].Value;
            }
            if (this.m_Alias != string.Empty)
            {
                this.m_FieldNode = PropertyEditorUtil.FindXmlNode(this.m_Alias, parent);
            }

            this.Setup();
            this.lblUnits.Visible = (this.lblUnits.Text != "");
        }

        private void Setup()
        {
            string val, units;
            //set up min/max
            if (this.m_Field.Min !=
                this.m_Field.Max)
            {
                this.m_TrackBar.Minimum = (int) this.m_Field.Min;
                this.m_TrackBar.Maximum = (int) this.m_Field.Max;
                PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_Field.Units, out val, out units);
                this.m_Numeric.Maximum = Decimal.Parse(val);
                PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_Field.Units, out val, out units);
                this.m_Numeric.Minimum = Decimal.Parse(val);
            }
            else if (this.m_Field.Units == "ms")
            {
                this.m_TrackBar.Minimum = 0;
                this.m_TrackBar.Maximum = 65535;
                PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_Field.Units, out val, out units);
                this.m_Numeric.Maximum = Decimal.Parse(val);
                PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_Field.Units, out val, out units);
                this.m_Numeric.Minimum = Decimal.Parse(val);
            }
            else
            {
                switch (this.m_Field.TypeName)
                {
                    case "s8":
                        this.m_TrackBar.Minimum = SByte.MinValue;
                        this.m_TrackBar.Maximum = SByte.MaxValue;
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                    case "u8":
                        this.m_TrackBar.Minimum = Byte.MinValue;
                        this.m_TrackBar.Maximum = Byte.MaxValue;
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                    case "s16":
                        this.m_TrackBar.Minimum = Int16.MinValue;
                        this.m_TrackBar.Maximum = Int16.MaxValue;
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                    case "u16":
                        this.m_TrackBar.Minimum = UInt16.MinValue;
                        this.m_TrackBar.Maximum = UInt16.MaxValue;
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Maximum, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Minimum, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                    case "u32":
                        this.m_TrackBar.Minimum = 0;
                        this.m_TrackBar.Maximum = 1000;
                        this.scalarPos = (decimal) UInt32.MaxValue / 1000;
                        PropertyEditorUtil.ConvertToDisplayValue(UInt32.MaxValue, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(UInt32.MinValue, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                    case "s32":
                    default:
                        this.m_TrackBar.Minimum = -1000;
                        this.scalarNeg = (decimal) Int32.MinValue / -1000;
                        this.m_TrackBar.Maximum = 1000;
                        this.scalarPos = (decimal) Int32.MaxValue / 1000;
                        PropertyEditorUtil.ConvertToDisplayValue(Int32.MaxValue, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Maximum = Decimal.Parse(val);
                        PropertyEditorUtil.ConvertToDisplayValue(Int32.MinValue, this.m_Field.Units, out val, out units);
                        this.m_Numeric.Minimum = Decimal.Parse(val);
                        break;
                }
            }

            this.lblUnits.Text = units;
            this.m_TrackBar.TickFrequency = 1;

            switch (this.m_Field.Units)
            {
                case "ms":
                case "degrees":
                case null:
                    this.m_Numeric.DecimalPlaces = 0;
                    this.m_Numeric.Increment = 1;
                    this.m_TrackBar.TickFrequency = 1;
                    this.m_TrackBar.SmallChange = 1;
                    this.m_TrackBar.LargeChange = 5;
                    break;
                default:
                    this.m_Numeric.DecimalPlaces = 2;
                    this.m_Numeric.Increment = 0.01m;
                    this.m_TrackBar.SmallChange = 50;
                    this.m_TrackBar.LargeChange = 200;
                    break;
            }

            //Value
            int valueIn;

            if (this.m_FieldNode != null)
            {
                valueIn = Int32.Parse(this.m_FieldNode.InnerText);
            }
            else if (this.m_TemplateFieldNode != null)
            {
                valueIn = Int32.Parse(this.m_TemplateFieldNode.InnerText);
            }
            else if (this.m_Field.DefaultValue != null)
            {
                valueIn = Int32.Parse(this.m_Field.DefaultValue);
            }
            else
            {
                valueIn = 0;
            }

            string valueOut;
            PropertyEditorUtil.ConvertToDisplayValue(valueIn, this.m_Field.Units, out valueOut, out units);
            this.m_Numeric.Value = Decimal.Parse(valueOut);
            this.lblUnits.Text = units;
            this.UpDateValue();
        }

        protected virtual void m_TrackBar_OnValueChanged(object sender, EventArgs e)
        {
            if (!this.bNumericUpDownUpdated)
            {
                string display, units;

                PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Value, this.m_Field.Units, out display, out units);
                var val = Decimal.Parse(display);
                this.lblUnits.Text = units;

                if (this.scalarPos != 0 &&
                    val >= 0)
                {
                    val = val * this.scalarPos;
                }
                else if (this.scalarNeg != 0 &&
                         val < 0)
                {
                    val = val * this.scalarNeg;
                }

                if (val != this.m_Numeric.Value)
                {
                    if (val < this.m_Numeric.Minimum)
                    {
                        val = this.m_Numeric.Minimum;
                    }
                    if (val > this.m_Numeric.Maximum)
                    {
                        val = this.m_Numeric.Maximum;
                    }

                    this.m_Numeric.Value = val;
                }
                this.lblUnits.Visible = (this.lblUnits.Text != "");
            }

            this.bNumericUpDownUpdated = false;
        }

        public void UpDateValue()
        {
            var numericValue = this.m_Numeric.Value;

            if (this.scalarPos != 0 &&
                numericValue >= 0)
            {
                numericValue = numericValue / this.scalarPos;
            }
            else if (this.scalarNeg != 0 &&
                     numericValue <= 0)
            {
                numericValue = numericValue / this.scalarNeg;
            }

            var val = PropertyEditorUtil.ConvertFromDisplayValue(numericValue.ToString(), this.m_Field.Units);

            if (val != this.m_TrackBar.Value)
            {
                //this bool is set to ensure that when the track bar is updated it then doesn't update the 
                //numeric up down again. We do this as the slider is giving a graphical representation of the number
                //and is not necessarily accurate
                this.bNumericUpDownUpdated = true;
                this.m_TrackBar.Value = val;
            }
        }
    }
}