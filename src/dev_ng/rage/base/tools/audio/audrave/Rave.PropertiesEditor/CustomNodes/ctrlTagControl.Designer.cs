﻿using Rave.TagEditor;

namespace Rave.PropertiesEditor.CustomNodes
{
	partial class ctrlTagControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
			this.SuspendLayout();
			// 
			// elementHost1
			// 
			this.elementHost1.Location = this.Location;
			this.elementHost1.Name = "elementHost1";
			this.elementHost1.Size = this.Size;
			this.elementHost1.TabIndex = 0;
			this.elementHost1.Text = "elementHost1";
			this.elementHost1.Child = this.tagListView1;
			// 
			// ctrlTagControl
			// 
			this.Controls.Add(this.elementHost1);
			this.Name = "ctrlTagControl";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Integration.ElementHost elementHost1;
		private TagListView tagListView1;
	}
}
