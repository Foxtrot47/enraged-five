namespace Rave.PropertiesEditor
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    using AdvancedDataGridView;

    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.PropertiesEditor.CustomNodes;
    using Rave.PropertiesEditor.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    public class tbPropertyTab : TabPage
    {
        private DataGridViewTextBoxColumn m_MultiSelect;
        private DataGridViewTextBoxColumn m_Override;
        private DataGridViewTextBoxColumn m_ParentValue;
        private TreeGridColumn m_Property;
        private ctrlPropertiesTreeGrid m_TreeGridView;
        private DataGridViewTextBoxColumn m_Value;

        public tbPropertyTab(string text)
        {
            this.Text = text;
            this.InitializeComponent();
            //Dont allow the user to resize rows
            this.TreeGrid.AllowUserToResizeRows = false;
			this.m_TreeGridView.OnReorderRows -= TreeGridView_OnReorderRows;
            this.m_TreeGridView.OnReorderRows += TreeGridView_OnReorderRows;
        }

        public TreeGridView TreeGrid
        {
            get { return this.m_TreeGridView; }
        }

        private void InitializeComponent()
        {
            var dataGridViewCellStyle1 = new DataGridViewCellStyle();
            var resources = new ComponentResourceManager(typeof (tbPropertyTab));
            var dataGridViewCellStyle2 = new DataGridViewCellStyle();
            this.m_TreeGridView = new ctrlPropertiesTreeGrid();
            this.m_Property = new TreeGridColumn();
            this.m_MultiSelect = new DataGridViewTextBoxColumn();
            this.m_Value = new DataGridViewTextBoxColumn();
            this.m_ParentValue = new DataGridViewTextBoxColumn();
            this.m_Override = new DataGridViewTextBoxColumn();
            ((ISupportInitialize) (this.m_TreeGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // m_TreeGridView
            // 
            this.m_TreeGridView.AllowDrop = true;
            this.m_TreeGridView.AllowUserToAddRows = false;
            this.m_TreeGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = Color.FromArgb(((((250)))), ((((250)))), ((((250)))));
            this.m_TreeGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.m_TreeGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.m_TreeGridView.BackgroundColor = SystemColors.Control;
            this.m_TreeGridView.CellBorderStyle = DataGridViewCellBorderStyle.SingleVertical;
            this.m_TreeGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_TreeGridView.ColumnHeadersVisible = false;
            this.m_TreeGridView.Columns.AddRange(new DataGridViewColumn[]
                                                {this.m_Property, this.m_MultiSelect, this.m_Value, this.m_ParentValue, this.m_Override});
            this.m_TreeGridView.ControlColumnIndex = ((List<int>) (resources.GetObject("m_TreeGridView.ControlColumnIndex")));
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = SystemColors.Window;
            dataGridViewCellStyle2.Font = new Font("Microsoft Sans Serif",
                                                   8.25F,
                                                   FontStyle.Regular,
                                                   GraphicsUnit.Point,
                                                   ((0)));
            dataGridViewCellStyle2.ForeColor = SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = Color.Gray;
            dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = DataGridViewTriState.False;
            this.m_TreeGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.m_TreeGridView.Dock = DockStyle.Fill;
            this.m_TreeGridView.EditMode = DataGridViewEditMode.EditOnEnter;
            this.m_TreeGridView.ExpandableColumnIndex = 0;
            this.m_TreeGridView.GridColor = Color.White;
            this.m_TreeGridView.ImageList = null;
            this.m_TreeGridView.Location = new Point(0, 0);
            this.m_TreeGridView.MultiSelect = false;
            this.m_TreeGridView.Name = "m_TreeGridView";
            this.m_TreeGridView.RowHeadersVisible = false;
            this.m_TreeGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.m_TreeGridView.Size = new Size(200, 100);
            this.m_TreeGridView.TabIndex = 0;
            this.m_TreeGridView.DragOver += TreeGridView_DragOver;
            this.m_TreeGridView.DragEnter += TreeGridView_DragEnter;
            this.m_TreeGridView.DragDrop += TreeGridView_DragDrop;
            // 
            // m_Property
            // 
            this.m_Property.DefaultNodeImage = null;
            this.m_Property.FillWeight = 121.8274F;
            this.m_Property.HeaderText = "m_Property";
            this.m_Property.Name = "m_Property";
            this.m_Property.ReadOnly = true;
            this.m_Property.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // m_MultiSelect
            // 
            this.m_MultiSelect.HeaderText = "m_MultiSelect";
            this.m_MultiSelect.Name = "m_MultiSelect";
            this.m_MultiSelect.Resizable = DataGridViewTriState.True;
            this.m_MultiSelect.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.m_MultiSelect.Visible = false;
            // 
            // m_Value
            // 
            this.m_Value.FillWeight = 137.8862F;
            this.m_Value.HeaderText = "m_Value";
            this.m_Value.Name = "m_Value";
            this.m_Value.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // m_ParentValue
            // 
            this.m_ParentValue.HeaderText = "m_ParentValue";
            this.m_ParentValue.Name = "m_ParentValue";
            this.m_ParentValue.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.m_ParentValue.Visible = false;
            // 
            // m_Override
            // 
            this.m_Override.FillWeight = 40.28637F;
            this.m_Override.HeaderText = "m_Override";
            this.m_Override.MinimumWidth = 20;
            this.m_Override.Name = "m_Override";
            this.m_Override.Resizable = DataGridViewTriState.True;
            this.m_Override.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // tbPropertyTab
            // 
            this.AllowDrop = true;
            this.BackColor = Color.White;
            this.Controls.Add(this.m_TreeGridView);
            ((ISupportInitialize) (this.m_TreeGridView)).EndInit();
            this.ResumeLayout(false);
        }

        private static void TreeGridView_OnReorderRows(TreeGridNode node)
        {
            var index = node.Parent.Nodes.IndexOf(node);
            var data = node.Tag as RowData;

            if (data != null)
            {
                var parent = data.ParentNode;
                var child = data.Node;
                parent.RemoveChild(child);

                if (index < node.Parent.Nodes.Count - 1)
                {
                    var sibling = node.Parent.Nodes[index + 1];
                    var siblingData = sibling.Tag as RowData;
                    if (null != siblingData)
                    {
                        var previousSibling = siblingData.Node;
                        parent.InsertBefore(child, previousSibling);
                    }
                }
                else
                {
                    parent.AppendChild(child);
                }
                
                data.ObjectInstance.ComputeReferences();
                data.ObjectInstance.MarkAsDirty();
            }
        }

        private static void TreeGridView_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move | DragDropEffects.Link;
        }

        private static void TreeGridView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Link;
        }

        private void TreeGridView_DragDrop(object sender, DragEventArgs e)
        {
            var p = this.m_TreeGridView.PointToClient(new Point(e.X, e.Y));
            var info = this.m_TreeGridView.HitTest(p.X, p.Y);
            var rowIndex = info.RowIndex;

            if (rowIndex < 0)
            {
                return;
            }

            var dgvc = this.m_TreeGridView.Rows[rowIndex].Cells["m_Value"];
            if (dgvc != null)
            {
                if (e.Data.GetDataPresent(typeof (WaveNode[])) &&
                    dgvc is IWaveRef)
                {
                    var wn = ((WaveNode[]) e.Data.GetData(typeof (WaveNode[])))[0];
                    var waveRef = dgvc as IWaveRef;
                    waveRef.Drop(wn);
                }
                else if (e.Data.GetDataPresent(typeof (SoundNode[])) &&
                         dgvc is IObjectRef)
                {
                    var sn = ((SoundNode[]) e.Data.GetData(typeof (SoundNode[])))[0];
                    var objRef = dgvc as IObjectRef;
                    objRef.Drop(sn);
                }
                else if (e.Data.GetDataPresent(typeof (WaveNode[])) &&
                         dgvc is ctrlObjectRefCell)
                {
                    var wn = ((WaveNode[]) e.Data.GetData(typeof (WaveNode[])))[0];
                    var objRef = dgvc as IObjectRef;
                    objRef.Drop(wn);
                }
                else if (e.Data.GetDataPresent(typeof (SoundNode[])) &&
                         dgvc is ICurveRef)
                {
                    var sn = ((SoundNode[]) e.Data.GetData(typeof (SoundNode[])))[0];
                    var curveRef = dgvc as ICurveRef;
                    curveRef.Drop(sn);
                }
            }
        }
    }
}