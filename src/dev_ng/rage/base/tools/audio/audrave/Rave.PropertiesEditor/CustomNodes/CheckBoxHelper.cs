﻿using System;
using System.Windows.Forms;
using Rave.TypeDefinitions.Infrastructure.Interfaces;

namespace Rave.PropertiesEditor.CustomNodes
{
    static public class CheckBoxHelper
    {
        public static CheckState GetIndeterminateValue(string defaultValue)
        {
            CheckState indeterminateValue = CheckState.Indeterminate;
            if (!string.IsNullOrEmpty(defaultValue))
            {
                if (defaultValue == "yes") indeterminateValue = CheckState.Checked;
                if (defaultValue == "no") indeterminateValue = CheckState.Unchecked;
            }
            return indeterminateValue;
        }
    }
}