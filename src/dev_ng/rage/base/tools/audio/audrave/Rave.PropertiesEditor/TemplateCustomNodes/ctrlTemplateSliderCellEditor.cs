namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;

    [DesignerCategory("code")]
    internal class TemplateSliderCellEditingControl : TSliderEditor,
                                                      IDataGridViewEditingControl
    {
        private string m_Alias;
        private IFieldDefinition m_Field;
        private XmlNode m_FieldNode;
        private IObjectInstance m_Object;
        private XmlNode m_ParentNode;
        private ITemplate m_Template;
        private XmlNode m_TemplateFieldNode;
        private IObjectInstance m_TemplateObject;
        private XmlNode m_TemplateParentNode;
        private bool valueChanged;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.Value; }
            set { return; }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            // this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged
        {
            get { return this.valueChanged; }
            set { this.valueChanged = value; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return Cursor.Current; }
        }

        #endregion

        public void Init(ITemplate template,
                         IObjectInstance obj,
                         IObjectInstance templateObject,
                         XmlNode parentNode,
                         XmlNode templateParent,
                         IFieldDefinition field)
        {
            this.m_Template = template;

            this.m_Object = obj;
            this.m_TemplateObject = templateObject;

            this.m_ParentNode = parentNode;
            this.m_TemplateParentNode = templateParent;

            this.m_Field = field;
            this.m_Alias = string.Empty;
            this.m_FieldNode = null;

            this.m_TemplateFieldNode = PropertyEditorUtil.FindXmlNode(field.Name, this.m_TemplateParentNode);

            if (this.m_TemplateFieldNode != null &&
                this.m_TemplateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_Alias = this.m_TemplateFieldNode.Attributes["ExposeAs"].Value;
            }

            if (this.m_Alias != string.Empty)
            {
                this.m_FieldNode = PropertyEditorUtil.FindXmlNode(this.m_Alias, this.m_ParentNode);
            }

            this.Setup(this.m_ParentNode, this.m_TemplateParentNode, this.m_Field);
        }

        protected override void m_TrackBar_OnValueChanged(object sender, EventArgs e)
        {
            base.m_TrackBar_OnValueChanged(sender, e);
            this.valueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
            this.UpdateNode();
        }

        public void UpdateNode()
        {
            if (this.m_FieldNode == null)
            {
                if (this.m_Alias == string.Empty)
                {
                    var takenAliases = this.m_Template.GetExposedFieldNames();

                    var tempAlias = this.m_Field.Name.ToLower();
                    var variation = 1;
                    while (takenAliases.Contains(tempAlias))
                    {
                        tempAlias = this.m_Field.Name.ToLower() + variation;
                        variation++;
                    }

                    this.m_Alias = tempAlias;
                }

                if (this.m_TemplateFieldNode == null)
                {
                    this.m_TemplateFieldNode = this.m_TemplateParentNode.OwnerDocument.CreateElement(this.m_Field.Name);
                    this.m_TemplateParentNode.AppendChild(this.m_TemplateFieldNode);
                    this.m_TemplateParentNode.InnerText = this.m_Field.DefaultValue;
                }

                ((XmlElement) this.m_TemplateFieldNode).SetAttribute("ExposeAs", this.m_Alias);

                this.m_FieldNode = this.m_ParentNode.OwnerDocument.CreateElement(this.m_Alias);
                this.m_ParentNode.AppendChild(this.m_FieldNode);
            }

            this.m_FieldNode.InnerText = this.Value;
            this.m_Object.MarkAsDirty();
            this.m_TemplateObject.MarkAsDirty();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.UpDateValue();
                this.UpdateNode();
                return true;
            }
            if (keyData == (Keys.Control | Keys.C) && !string.IsNullOrEmpty(this.Value))
            {
                Clipboard.SetText(this.Value);
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}