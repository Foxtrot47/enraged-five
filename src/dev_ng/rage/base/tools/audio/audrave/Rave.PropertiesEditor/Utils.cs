﻿// -----------------------------------------------------------------------
// <copyright file="Utils.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.PropertiesEditor
{
    using System.Collections.Generic;
    using System.IO;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    /// Properties editor utilities.
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// The create type icon list.
        /// </summary>
        /// <param name="baseIconPath">
        /// The base icon path.
        /// </param>
        /// <param name="typeDefs">
        /// The type definitions.
        /// </param>
        /// <returns>
        /// The type icon list <see cref="Dictionary{TKey,TValue}"/>.
        /// </returns>
        public static Dictionary<string, string> CreateTypeIconList(string baseIconPath, ITypeDefinitions typeDefs)
        {
            var table = new Dictionary<string, string>();
            var defaultPath = baseIconPath + "default.ico";

            if (typeDefs.ObjectTypes != null)
            {
                foreach (var td in typeDefs.ObjectTypes)
                {
                    if (File.Exists(baseIconPath + td.Name + ".ico"))
                    {
                        string iconPath = baseIconPath + td.Name + ".ico";
                        table.Add(td.Name, iconPath);
                    }
                    else
                    {
                        var foundIcon = false;
                        if (td.InheritsFrom != null)
                        {
                            var current = td.TypeDefinitions.FindTypeDefinition(td.InheritsFrom);

                            while (current != null)
                            {
                                var parentIconPath = baseIconPath + current.Name + ".ico";

                                if (File.Exists(parentIconPath))
                                {
                                    foundIcon = true;
                                    table.Add(td.Name, parentIconPath);
                                    break;
                                }

                                current = current.TypeDefinitions.FindTypeDefinition(current.InheritsFrom);
                            }
                        }

                        if (!foundIcon)
                        {
                            table.Add(td.Name, defaultPath);
                        }
                    }
                }

                if (File.Exists(baseIconPath + "TemplateInstance" + ".ico"))
                {
                    var iconPath = baseIconPath + "TemplateInstance" + ".ico";
                    table.Add("TemplateInstance", iconPath);
                }
                else
                {
                    table.Add("TemplateInstance", defaultPath);
                }
            }

            return table;
        }
    }
}
