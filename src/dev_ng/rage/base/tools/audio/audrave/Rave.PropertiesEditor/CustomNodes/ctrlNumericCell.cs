namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    public class ctrlNumericCell : DataGridViewTextBoxCell,
                                   IFieldCell
    {
        private const DataGridViewContentAlignment ANY_RIGHT =
            DataGridViewContentAlignment.TopRight | DataGridViewContentAlignment.MiddleRight |
            DataGridViewContentAlignment.BottomRight;

        private const DataGridViewContentAlignment ANY_CENTER =
            DataGridViewContentAlignment.TopCenter | DataGridViewContentAlignment.MiddleCenter |
            DataGridViewContentAlignment.BottomCenter;

        [ThreadStatic] private static Bitmap ms_renderingBitmap;

        private string m_alias;
        private bool m_bDraw;
        private NumericEditingControl m_editor;
        private XmlNode m_fieldNode;
        private IObjectInstance m_objectInstance;
        private NumericUpDown m_paintingNumericUpDown;
        private XmlNode m_parentNode;
        private ISimpleFieldDefinitionInstance m_simpleFieldInstance;

        public override Type EditType
        {
            get { return typeof (NumericEditingControl); }
        }

        public override Type ValueType
        {
            get { return typeof (Decimal); }
        }

        public override object DefaultNewRowValue
        {
            get { return "0"; }
        }

        #region IFieldCell Members

        public bool IsDefaultValue
        {
            get { return (this.m_fieldNode == null || this.m_simpleFieldInstance.DefaultValue == this.m_fieldNode.InnerText); }
        }

        public CellType Type
        {
            get { return CellType.NUMERIC; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance,
                         XmlNode parentNode,
                         IFieldDefinitionInstance fieldInstance,
                         string fieldAlias,
                         bool draw)
        {
            this.m_objectInstance = objectInstance;
            this.m_parentNode = parentNode;
            this.m_simpleFieldInstance = fieldInstance.AsSimpleField();
            this.m_bDraw = false;
            this.m_alias = string.IsNullOrEmpty(fieldAlias) ? fieldInstance.Name : fieldAlias;

            if (ms_renderingBitmap == null)
            {
                ms_renderingBitmap = new Bitmap(200, 30);
            }
            this.m_paintingNumericUpDown = new NumericUpDown();

            if (this.m_simpleFieldInstance.Min !=
                this.m_simpleFieldInstance.Max)
            {
                this.m_paintingNumericUpDown.Maximum = (decimal) this.m_simpleFieldInstance.Max;
                this.m_paintingNumericUpDown.Minimum = (decimal) this.m_simpleFieldInstance.Min;
            }
            else
            {
                this.m_paintingNumericUpDown.Maximum = Decimal.MaxValue;
                this.m_paintingNumericUpDown.Minimum = Decimal.MinValue;
            }

            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            this.Value = this.GetValue();
        }

        private string GetValue()
        {
            if (this.m_fieldNode == null)
            {
                this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            }
            if (this.m_fieldNode != null)
            {
                return this.m_fieldNode.InnerText;
            }
            if (null != this.m_simpleFieldInstance && this.m_simpleFieldInstance.DefaultValue != null)
            {
                return this.m_simpleFieldInstance.DefaultValue;
            }
            return "0";
        }

        public override void InitializeEditingControl(int rowIndex,
                                                      object initialFormattedValue,
                                                      DataGridViewCellStyle dataGridViewCellStyle)
        {
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
            this.m_editor = this.DataGridView.EditingControl as NumericEditingControl;
            this.m_editor.Init(this.m_objectInstance, this.m_parentNode, this.m_simpleFieldInstance, this.m_alias);
        }

        private static bool PartPainted(DataGridViewPaintParts paintParts, DataGridViewPaintParts paintPart)
        {
            return (paintParts & paintPart) != 0;
        }

        protected override void Paint(Graphics graphics,
                                      Rectangle clipBounds,
                                      Rectangle cellBounds,
                                      int rowIndex,
                                      DataGridViewElementStates cellState,
                                      object value,
                                      object formattedValue,
                                      string errorText,
                                      DataGridViewCellStyle cellStyle,
                                      DataGridViewAdvancedBorderStyle advancedBorderStyle,
                                      DataGridViewPaintParts paintParts)
        {
            if (!this.m_bDraw)
            {
                base.Paint(graphics,
                    clipBounds,
                    cellBounds,
                    rowIndex,
                    cellState,
                    value,
                    formattedValue,
                    errorText,
                    cellStyle,
                    advancedBorderStyle,
                    paintParts);
            }
            else
            {
                if (this.DataGridView == null)
                {
                    return;
                }

                // First paint the borders and background of the cell.
                base.Paint(graphics,
                    clipBounds,
                    cellBounds,
                    rowIndex,
                    cellState,
                    value,
                    formattedValue,
                    errorText,
                    cellStyle,
                    advancedBorderStyle,
                    paintParts & ~(DataGridViewPaintParts.ErrorIcon | DataGridViewPaintParts.ContentForeground));

                var ptCurrentCell = this.DataGridView.CurrentCellAddress;
                var cellCurrent = ptCurrentCell.X == this.ColumnIndex && ptCurrentCell.Y == rowIndex;
                var cellEdited = cellCurrent && this.DataGridView.EditingControl != null;

                // If the cell is in editing mode, there is nothing else to paint
                if (!cellEdited)
                {
                    if (PartPainted(paintParts, DataGridViewPaintParts.ContentForeground))
                    {
                        // Paint a NumericUpDown control
                        // Take the borders into account
                        var borderWidths = this.BorderWidths(advancedBorderStyle);
                        var valBounds = cellBounds;
                        valBounds.Offset(borderWidths.X, borderWidths.Y);
                        valBounds.Width -= borderWidths.Right;
                        valBounds.Height -= borderWidths.Bottom;
                        // Also take the padding into account
                        if (cellStyle.Padding !=
                            Padding.Empty)
                        {
                            if (this.DataGridView.RightToLeft ==
                                RightToLeft.Yes)
                            {
                                valBounds.Offset(cellStyle.Padding.Right, cellStyle.Padding.Top);
                            }
                            else
                            {
                                valBounds.Offset(cellStyle.Padding.Left, cellStyle.Padding.Top);
                            }
                            valBounds.Width -= cellStyle.Padding.Horizontal;
                            valBounds.Height -= cellStyle.Padding.Vertical;
                        }
                        // Determine the NumericUpDown control location
                        valBounds = this.GetAdjustedEditingControlBounds(valBounds, cellStyle);

                        var cellSelected = (cellState & DataGridViewElementStates.Selected) != 0;

                        if (ms_renderingBitmap.Width < valBounds.Width ||
                            ms_renderingBitmap.Height < valBounds.Height)
                        {
                            // The static bitmap is too small, a bigger one needs to be allocated.
                            ms_renderingBitmap.Dispose();
                            ms_renderingBitmap = new Bitmap(valBounds.Width, valBounds.Height);
                        }

                        if (this.m_paintingNumericUpDown.Parent != this.DataGridView)
                        {
                            this.m_paintingNumericUpDown.Parent = this.DataGridView;
                        }

                        // Set all the relevant properties
                        this.m_paintingNumericUpDown.DecimalPlaces = 8;
                        this.m_paintingNumericUpDown.Width = valBounds.Width;
                        this.m_paintingNumericUpDown.RightToLeft = this.DataGridView.RightToLeft;
                        this.m_paintingNumericUpDown.Location = new Point(0, -this.m_paintingNumericUpDown.Height - 100);

                        if (this.m_simpleFieldInstance.Min !=
                            this.m_simpleFieldInstance.Max)
                        {
                            this.m_paintingNumericUpDown.Maximum = (decimal) this.m_simpleFieldInstance.Max;
                            this.m_paintingNumericUpDown.Minimum = (decimal) this.m_simpleFieldInstance.Min;
                        }
                        else
                        {
                            this.m_paintingNumericUpDown.Maximum = Decimal.MaxValue;
                            this.m_paintingNumericUpDown.Minimum = Decimal.MinValue;
                        }

                        this.m_paintingNumericUpDown.Value = Decimal.Parse(this.GetValue());

                        this.m_paintingNumericUpDown.Enabled = !this.m_objectInstance.IsReadOnly;

                        Color backColor;
                        if (PartPainted(paintParts, DataGridViewPaintParts.SelectionBackground) && cellSelected)
                        {
                            backColor = cellStyle.SelectionBackColor;
                        }
                        else
                        {
                            backColor = cellStyle.BackColor;
                        }
                        if (PartPainted(paintParts, DataGridViewPaintParts.Background))
                        {
                            this.m_paintingNumericUpDown.BackColor = Color.White;
                        }
                        // Finally paint the NumericUpDown control
                        var srcRect = new Rectangle(0, 0, valBounds.Width, valBounds.Height);
                        if (srcRect.Width > 0 &&
                            srcRect.Height > 0)
                        {
                            this.m_paintingNumericUpDown.DrawToBitmap(ms_renderingBitmap, srcRect);
                            graphics.DrawImage(ms_renderingBitmap,
                                new Rectangle(valBounds.Location, valBounds.Size),
                                srcRect,
                                GraphicsUnit.Pixel);
                        }
                    }
                    if (PartPainted(paintParts, DataGridViewPaintParts.ErrorIcon))
                    {
                        // Paint the potential error icon on top of the NumericUpDown control
                        base.Paint(graphics,
                            clipBounds,
                            cellBounds,
                            rowIndex,
                            cellState,
                            value,
                            formattedValue,
                            errorText,
                            cellStyle,
                            advancedBorderStyle,
                            DataGridViewPaintParts.ErrorIcon);
                    }
                }
            }
        }

        internal static HorizontalAlignment TranslateAlignment(DataGridViewContentAlignment align)
        {
            if ((align & ANY_RIGHT) != 0)
            {
                return HorizontalAlignment.Right;
            }
            if ((align & ANY_CENTER) != 0)
            {
                return HorizontalAlignment.Center;
            }
            return HorizontalAlignment.Left;
        }

        public override void PositionEditingControl(bool setLocation,
                                                    bool setSize,
                                                    Rectangle cellBounds,
                                                    Rectangle cellClip,
                                                    DataGridViewCellStyle cellStyle,
                                                    bool singleVerticalBorderAdded,
                                                    bool singleHorizontalBorderAdded,
                                                    bool isFirstDisplayedColumn,
                                                    bool isFirstDisplayedRow)
        {
            var editingControlBounds = this.PositionEditingPanel(cellBounds,
                cellClip,
                cellStyle,
                singleVerticalBorderAdded,
                singleHorizontalBorderAdded,
                isFirstDisplayedColumn,
                isFirstDisplayedRow);
            editingControlBounds = this.GetAdjustedEditingControlBounds(editingControlBounds, cellStyle);
            this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
            this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
        }

        private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
        {
            // Add a 1 pixel padding on the left and right of the editing control
            editingControlBounds.X += 1;
            editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);

            // Adjust the vertical location of the editing control:
            var preferredHeight = this.m_paintingNumericUpDown.Height;
            if (preferredHeight < editingControlBounds.Height)
            {
                switch (cellStyle.Alignment)
                {
                    case DataGridViewContentAlignment.MiddleLeft:
                    case DataGridViewContentAlignment.MiddleCenter:
                    case DataGridViewContentAlignment.MiddleRight:
                        editingControlBounds.Y += (editingControlBounds.Height - preferredHeight) / 2;
                        break;
                    case DataGridViewContentAlignment.BottomLeft:
                    case DataGridViewContentAlignment.BottomCenter:
                    case DataGridViewContentAlignment.BottomRight:
                        editingControlBounds.Y += editingControlBounds.Height - preferredHeight;
                        break;
                }
            }

            return editingControlBounds;
        }

        public override void DetachEditingControl()
        {
            if (null != this.m_editor)
            {
                this.m_editor.UpdateNode();
            }
            base.DetachEditingControl();
            this.Value = this.GetValue();
        }

        protected override void Dispose(bool disposing)
        {
            this.m_objectInstance = null;
            this.m_parentNode = null;
            this.m_fieldNode = null;
            this.m_editor = null;
            this.m_simpleFieldInstance = null;
            base.Dispose(disposing);
        }
    }
}