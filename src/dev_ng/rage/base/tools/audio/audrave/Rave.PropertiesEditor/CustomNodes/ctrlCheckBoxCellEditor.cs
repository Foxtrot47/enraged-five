namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    internal class CheckBoxEditor : CheckBox,
                                    IDataGridViewEditingControl
    {
        private string m_alias;
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_objectInstance;
        private XmlNode m_parentNode;
        private bool m_valueChanged;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.CheckState; }
            set { return; }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
            this.Text = "";
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged
        {
            get { return this.m_valueChanged; }
            set { this.m_valueChanged = value; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance,
                         XmlNode parentNode,
                         IFieldDefinitionInstance fieldInstance,
                         string fieldAlias)
        {
            this.m_objectInstance = objectInstance;
            this.m_parentNode = parentNode;
            this.m_fieldInstance = fieldInstance;
            this.m_alias = fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            this.ThreeState = (this.m_fieldInstance.TypeName == "tristate") && (CheckBoxHelper.GetIndeterminateValue(m_fieldInstance.DefaultValue) == CheckState.Indeterminate);
            this.SetCheckboxStateFromXml();
        }
        
       
        private void SetCheckboxStateFromXml()
        {
            var fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            if (this.m_fieldInstance.TypeName == "tristate")
            {
                this.ThreeState = CheckBoxHelper.GetIndeterminateValue(m_fieldInstance.DefaultValue) == CheckState.Indeterminate;
                if (fieldNode == null)
                {
                    this.CheckState = CheckBoxHelper.GetIndeterminateValue(m_fieldInstance.DefaultValue);
                }
                else if (fieldNode.InnerText == "yes")
                {
                    this.CheckState = CheckState.Checked;
                }
                else
                {
                    this.CheckState = CheckState.Unchecked;
                }
            }
            else
            {
                this.ThreeState = false;
                if (fieldNode == null)
                {
                    this.CheckState = this.m_fieldInstance.DefaultValue == "yes" ? CheckState.Checked : CheckState.Unchecked;
                }
                else
                {
                    this.CheckState = fieldNode.InnerText == "yes" ? CheckState.Checked : CheckState.Unchecked;
                }
            }
        }

        public void UpdateNode()
        {
            var dirty = false;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);
            switch (this.CheckState)
            {
                case CheckState.Unchecked:
                case CheckState.Checked:
                    if (this.m_fieldNode == null)
                    {
                        this.m_fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_alias);
                        this.m_parentNode.AppendChild(this.m_fieldNode);
                        dirty = true;
                    }
                    var val = (this.Checked ? "yes" : "no");
                    if (this.m_fieldNode.InnerText != val)
                    {
                        this.m_fieldNode.InnerText = val;
                        dirty = true;
                    }

                    break;
                case CheckState.Indeterminate:
                    if (this.m_fieldNode != null)
                    {
                        this.m_parentNode.RemoveChild(this.m_fieldNode);
                        this.m_fieldNode = null;
                        dirty = true;
                    }
                    break;
            }
            if (dirty)
            {
                this.m_objectInstance.MarkAsDirty();
            }
        }

        protected override void OnCheckStateChanged(EventArgs e)
        {
            this.m_valueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
            base.OnCheckStateChanged(e);
            this.UpdateNode();

            switch (this.CheckState)
            {
                case CheckState.Checked:
                    this.Text = "yes";
                    break;
                case CheckState.Unchecked:
                    this.Text = "no";
                    break;
                default:
                    this.Text = "use parent value";
                    break;
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter ||
                keyData == Keys.Space)
            {
                switch (this.CheckState)
                {
                    case CheckState.Checked:
                        this.CheckState = this.ThreeState ? CheckState.Indeterminate : CheckState.Unchecked;
                        return true;
                    case CheckState.Unchecked:
                        this.CheckState = CheckState.Checked;
                        return true;
                    case CheckState.Indeterminate:
                        this.CheckState = CheckState.Unchecked;
                        return true;
                }
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}