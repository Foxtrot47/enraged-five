namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;

    [DesignerCategory("code")]
    public class ctrlTemplateNumericCell : DataGridViewTextBoxCell,
                                           ITemplateFieldCell
    {
        [ThreadStatic] private static Bitmap sm_RenderingBitmap;

        private static readonly DataGridViewContentAlignment anyRight = DataGridViewContentAlignment.TopRight |
                                                                        DataGridViewContentAlignment.MiddleRight |
                                                                        DataGridViewContentAlignment.BottomRight;

        private static readonly DataGridViewContentAlignment anyCenter = DataGridViewContentAlignment.TopCenter |
                                                                         DataGridViewContentAlignment.MiddleCenter |
                                                                         DataGridViewContentAlignment.BottomCenter;

        private bool bDraw;
        private string m_Alias;

        private TemplateNumericEditingControl m_Editor;
        private ISimpleFieldDefinition m_Field;
        private XmlNode m_FieldNode;
        private ContextMenuStrip m_Menu;
        private ITypedObjectInstance m_Object;
        private NumericUpDown m_PaintingNumericUpDown;
        private XmlNode m_ParentNode;
        private ITemplate m_Template;
        private XmlNode m_TemplateFieldNode;
        private ITypedObjectInstance m_TemplateObject;
        private XmlNode m_TemplateParentNode;

        public override Type EditType
        {
            get { return typeof (TemplateNumericEditingControl); }
        }

        public override Type ValueType
        {
            get { return typeof (Decimal); }
        }

        public override object DefaultNewRowValue
        {
            get { return "0"; }
        }

        #region ITemplateFieldCell Members

        public bool OverridesTemplate
        {
            get { return (this.m_FieldNode != null && this.m_FieldNode.InnerText != this.m_TemplateFieldNode.InnerText); }
        }

        public bool IsDefaultValue
        {
            get
            {
                return (this.m_FieldNode == null && this.m_TemplateFieldNode == null ||
                        this.m_FieldNode == null && this.m_TemplateFieldNode.InnerText == this.m_Field.DefaultValue ||
                        this.m_FieldNode != null & this.m_FieldNode.InnerText == this.m_Field.DefaultValue);
            }
        }

        public TemplateCellType Type
        {
            get { return TemplateCellType.NUMERIC; }
        }

        #endregion

        public void Init(ITemplate template,
                         ITypedObjectInstance o,
                         ITypedObjectInstance templateObject,
                         XmlNode parent,
                         XmlNode templateParent,
                         IFieldDefinition field,
                         bool draw)
        {
            if (sm_RenderingBitmap == null)
            {
                sm_RenderingBitmap = new Bitmap(200, 30);
            }
            this.m_PaintingNumericUpDown = new NumericUpDown();

            this.m_Template = template;

            this.m_Object = o;
            this.m_TemplateObject = templateObject;

            this.m_ParentNode = parent;
            this.m_TemplateParentNode = templateParent;

            this.m_Field = field as ISimpleFieldDefinition;
            this.m_Alias = string.Empty;
            this.m_FieldNode = null;

            if (this.m_Field.Min !=
                this.m_Field.Max)
            {
                this.m_PaintingNumericUpDown.Maximum = (decimal) this.m_Field.Max;
                this.m_PaintingNumericUpDown.Minimum = (decimal) this.m_Field.Min;
            }
            else
            {
                this.m_PaintingNumericUpDown.Maximum = Decimal.MaxValue;
                this.m_PaintingNumericUpDown.Minimum = Decimal.MinValue;
            }

            this.Value = this.GetValue();
            if (this.m_Menu == null)
            {
                this.m_Menu = new ContextMenuStrip();
                this.m_Menu.Items.Add(new ToolStripMenuItem("Reset", null, new EventHandler(this.On_Reset)));
                this.ContextMenuStrip = this.m_Menu;
                this.ContextMenuStrip.Opening += this.ContextMenuStrip_Opening;
                this.ContextMenuStrip = this.m_Menu;
            }

            this.bDraw = draw;
        }

        private void On_Reset(object sender, EventArgs e)
        {
            if (this.m_FieldNode != null)
            {
                this.m_Object.Node.RemoveChild(this.m_FieldNode);
                this.m_Object.MarkAsDirty();
                this.Value = this.GetValue();
                this.RaiseCellValueChanged(new DataGridViewCellEventArgs(this.ColumnIndex, this.RowIndex));
            }
        }

        private void ContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (this.m_FieldNode == null)
            {
                e.Cancel = true;
            }
        }

        private string GetValue()
        {
            this.m_TemplateFieldNode = PropertyEditorUtil.FindXmlNode(this.m_Field.Name, this.m_TemplateParentNode);

            if (this.m_TemplateFieldNode != null &&
                this.m_TemplateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_Alias = this.m_TemplateFieldNode.Attributes["ExposeAs"].Value;
            }

            if (this.m_Alias != string.Empty)
            {
                this.m_FieldNode = PropertyEditorUtil.FindXmlNode(this.m_Alias, this.m_ParentNode);
            }

            if (this.m_FieldNode != null)
            {
                return this.m_FieldNode.InnerText;
            }
            else if (this.m_TemplateFieldNode != null)
            {
                return this.m_TemplateFieldNode.InnerText;
            }
            else
            {
                if (this.m_Field.DefaultValue != null)
                {
                    return this.m_Field.DefaultValue;
                }
                else
                {
                    return "0";
                }
            }
        }

        public override void InitializeEditingControl(int rowIndex,
                                                      object initialFormattedValue,
                                                      DataGridViewCellStyle dataGridViewCellStyle)
        {
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
            this.m_Editor = this.DataGridView.EditingControl as TemplateNumericEditingControl;
            this.m_Editor.Init(this.m_Template, this.m_Object, this.m_TemplateObject, this.m_ParentNode, this.m_TemplateParentNode, this.m_Field);
        }

        private static bool PartPainted(DataGridViewPaintParts paintParts, DataGridViewPaintParts paintPart)
        {
            return (paintParts & paintPart) != 0;
        }

        protected override void Paint(Graphics graphics,
                                      Rectangle clipBounds,
                                      Rectangle cellBounds,
                                      int rowIndex,
                                      DataGridViewElementStates cellState,
                                      object value,
                                      object formattedValue,
                                      string errorText,
                                      DataGridViewCellStyle cellStyle,
                                      DataGridViewAdvancedBorderStyle advancedBorderStyle,
                                      DataGridViewPaintParts paintParts)
        {
            if (!this.bDraw)
            {
                this.Value = this.GetValue();
                base.Paint(graphics,
                    clipBounds,
                    cellBounds,
                    rowIndex,
                    cellState,
                    value,
                    formattedValue,
                    errorText,
                    cellStyle,
                    advancedBorderStyle,
                    paintParts);
            }
            else
            {
                if (this.DataGridView == null)
                {
                    return;
                }

                // First paint the borders and background of the cell.
                base.Paint(graphics,
                    clipBounds,
                    cellBounds,
                    rowIndex,
                    cellState,
                    value,
                    formattedValue,
                    errorText,
                    cellStyle,
                    advancedBorderStyle,
                    paintParts & ~(DataGridViewPaintParts.ErrorIcon | DataGridViewPaintParts.ContentForeground));

                var ptCurrentCell = this.DataGridView.CurrentCellAddress;
                var cellCurrent = ptCurrentCell.X == this.ColumnIndex && ptCurrentCell.Y == rowIndex;
                var cellEdited = cellCurrent && this.DataGridView.EditingControl != null;

                // If the cell is in editing mode, there is nothing else to paint
                if (!cellEdited)
                {
                    if (PartPainted(paintParts, DataGridViewPaintParts.ContentForeground))
                    {
                        // Paint a NumericUpDown control
                        // Take the borders into account
                        var borderWidths = this.BorderWidths(advancedBorderStyle);
                        var valBounds = cellBounds;
                        valBounds.Offset(borderWidths.X, borderWidths.Y);
                        valBounds.Width -= borderWidths.Right;
                        valBounds.Height -= borderWidths.Bottom;
                        // Also take the padding into account
                        if (cellStyle.Padding !=
                            Padding.Empty)
                        {
                            if (this.DataGridView.RightToLeft ==
                                RightToLeft.Yes)
                            {
                                valBounds.Offset(cellStyle.Padding.Right, cellStyle.Padding.Top);
                            }
                            else
                            {
                                valBounds.Offset(cellStyle.Padding.Left, cellStyle.Padding.Top);
                            }
                            valBounds.Width -= cellStyle.Padding.Horizontal;
                            valBounds.Height -= cellStyle.Padding.Vertical;
                        }
                        // Determine the NumericUpDown control location
                        valBounds = this.GetAdjustedEditingControlBounds(valBounds, cellStyle);

                        var cellSelected = (cellState & DataGridViewElementStates.Selected) != 0;

                        if (sm_RenderingBitmap.Width < valBounds.Width ||
                            sm_RenderingBitmap.Height < valBounds.Height)
                        {
                            // The static bitmap is too small, a bigger one needs to be allocated.
                            sm_RenderingBitmap.Dispose();
                            sm_RenderingBitmap = new Bitmap(valBounds.Width, valBounds.Height);
                        }

                        if (this.m_PaintingNumericUpDown.Parent !=
                            this.DataGridView)
                        {
                            this.m_PaintingNumericUpDown.Parent = this.DataGridView;
                        }

                        // Set all the relevant properties
                        this.m_PaintingNumericUpDown.DecimalPlaces = 8;
                        this.m_PaintingNumericUpDown.Width = valBounds.Width;
                        this.m_PaintingNumericUpDown.RightToLeft = this.DataGridView.RightToLeft;
                        this.m_PaintingNumericUpDown.Location = new Point(0, -this.m_PaintingNumericUpDown.Height - 100);

                        if (this.m_Field.Min !=
                            this.m_Field.Max)
                        {
                            this.m_PaintingNumericUpDown.Maximum = (decimal) this.m_Field.Max;
                            this.m_PaintingNumericUpDown.Minimum = (decimal) this.m_Field.Min;
                        }
                        else
                        {
                            this.m_PaintingNumericUpDown.Maximum = Decimal.MaxValue;
                            this.m_PaintingNumericUpDown.Minimum = Decimal.MinValue;
                        }

                        this.m_PaintingNumericUpDown.Value = Decimal.Parse(this.GetValue());

                        this.m_PaintingNumericUpDown.Enabled = !this.m_Object.IsReadOnly;

                        Color backColor;
                        if (PartPainted(paintParts, DataGridViewPaintParts.SelectionBackground) && cellSelected)
                        {
                            backColor = cellStyle.SelectionBackColor;
                        }
                        else
                        {
                            backColor = cellStyle.BackColor;
                        }
                        if (PartPainted(paintParts, DataGridViewPaintParts.Background))
                        {
                            this.m_PaintingNumericUpDown.BackColor = Color.White;
                        }
                        // Finally paint the NumericUpDown control
                        var srcRect = new Rectangle(0, 0, valBounds.Width, valBounds.Height);
                        if (srcRect.Width > 0 &&
                            srcRect.Height > 0)
                        {
                            this.m_PaintingNumericUpDown.DrawToBitmap(sm_RenderingBitmap, srcRect);
                            graphics.DrawImage(sm_RenderingBitmap,
                                new Rectangle(valBounds.Location, valBounds.Size),
                                srcRect,
                                GraphicsUnit.Pixel);
                        }
                    }
                    if (PartPainted(paintParts, DataGridViewPaintParts.ErrorIcon))
                    {
                        // Paint the potential error icon on top of the NumericUpDown control
                        base.Paint(graphics,
                            clipBounds,
                            cellBounds,
                            rowIndex,
                            cellState,
                            value,
                            formattedValue,
                            errorText,
                            cellStyle,
                            advancedBorderStyle,
                            DataGridViewPaintParts.ErrorIcon);
                    }
                }
            }
        }

        internal static HorizontalAlignment TranslateAlignment(DataGridViewContentAlignment align)
        {
            if ((align & anyRight) != 0)
            {
                return HorizontalAlignment.Right;
            }
            else if ((align & anyCenter) != 0)
            {
                return HorizontalAlignment.Center;
            }
            else
            {
                return HorizontalAlignment.Left;
            }
        }

        public override void PositionEditingControl(bool setLocation,
                                                    bool setSize,
                                                    Rectangle cellBounds,
                                                    Rectangle cellClip,
                                                    DataGridViewCellStyle cellStyle,
                                                    bool singleVerticalBorderAdded,
                                                    bool singleHorizontalBorderAdded,
                                                    bool isFirstDisplayedColumn,
                                                    bool isFirstDisplayedRow)
        {
            var editingControlBounds = this.PositionEditingPanel(cellBounds,
                cellClip,
                cellStyle,
                singleVerticalBorderAdded,
                singleHorizontalBorderAdded,
                isFirstDisplayedColumn,
                isFirstDisplayedRow);
            editingControlBounds = this.GetAdjustedEditingControlBounds(editingControlBounds, cellStyle);
            this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
            this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
        }

        private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
        {
            // Add a 1 pixel padding on the left and right of the editing control
            editingControlBounds.X += 1;
            editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);

            // Adjust the vertical location of the editing control:
            var preferredHeight = this.m_PaintingNumericUpDown.Height;
            if (preferredHeight < editingControlBounds.Height)
            {
                switch (cellStyle.Alignment)
                {
                    case DataGridViewContentAlignment.MiddleLeft:
                    case DataGridViewContentAlignment.MiddleCenter:
                    case DataGridViewContentAlignment.MiddleRight:
                        editingControlBounds.Y += (editingControlBounds.Height - preferredHeight) / 2;
                        break;
                    case DataGridViewContentAlignment.BottomLeft:
                    case DataGridViewContentAlignment.BottomCenter:
                    case DataGridViewContentAlignment.BottomRight:
                        editingControlBounds.Y += editingControlBounds.Height - preferredHeight;
                        break;
                }
            }

            return editingControlBounds;
        }

        public override void DetachEditingControl()
        {
            this.m_Editor.UpdateNode();
            base.DetachEditingControl();
            this.Value = this.GetValue();
        }

        protected override void Dispose(bool disposing)
        {
            this.m_Object = null;
            this.m_ParentNode = null;
            this.m_FieldNode = null;
            this.m_Editor = null;
            this.m_Field = null;
            base.Dispose(disposing);
        }
    }
}