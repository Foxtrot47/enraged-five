namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    public class ctrlExposeCell : DataGridViewTextBoxCell,
                                  IFieldCell
    {
        [ThreadStatic] private static Bitmap ms_renderingBitmap;
        private bool m_bDraw;
        private ExposeEditingControl m_editor;
        private ctrlExpose m_expose;
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;

        private IObjectInstance m_objectInstance;
        private XmlNode m_parentNode;

        public override Type EditType
        {
            get
            {
                // Return the type of the editing contol that CalendarCell uses.
                return typeof (ExposeEditingControl);
            }
        }

        public override Type ValueType
        {
            get { return typeof (string); }
        }

        public override object DefaultNewRowValue
        {
            get { return string.Empty; }
        }

        #region IFieldCell Members

        public bool IsDefaultValue
        {
            get { return true; }
        }

        public CellType Type
        {
            get { return CellType.EXPOSE; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance, XmlNode parent, IFieldDefinitionInstance fieldInstance, bool draw)
        {
            this.m_objectInstance = objectInstance;
            this.m_parentNode = parent;
            this.m_fieldInstance = fieldInstance;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(fieldInstance.Name, parent);
            this.Value = this.GetValue();
            this.m_expose = new ctrlExpose();
            if (ms_renderingBitmap == null)
            {
                ms_renderingBitmap = new Bitmap(200, 30);
            }

            this.m_bDraw = draw;
        }

        private string GetValue()
        {
            if (this.m_fieldNode == null)
            {
                this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_fieldInstance.Name, this.m_parentNode);
            }
            if (this.m_fieldNode != null &&
                this.m_fieldNode.Attributes["EposeAs"] != null)
            {
                return this.m_fieldNode.Attributes["EposeAs"].Value;
            }
            return string.Empty;
        }

        public override void InitializeEditingControl(int rowIndex,
                                                      object initialFormattedValue,
                                                      DataGridViewCellStyle dataGridViewCellStyle)
        {
            // Set the value of the editing control to the current cell value.
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
            this.m_editor = this.DataGridView.EditingControl as ExposeEditingControl;
            this.m_editor.Init(this.m_objectInstance, this.m_parentNode, this.m_fieldInstance);
        }

        private static bool PartPainted(DataGridViewPaintParts paintParts, DataGridViewPaintParts paintPart)
        {
            return (paintParts & paintPart) != 0;
        }

        protected override void Paint(Graphics graphics,
                                      Rectangle clipBounds,
                                      Rectangle cellBounds,
                                      int rowIndex,
                                      DataGridViewElementStates cellState,
                                      object value,
                                      object formattedValue,
                                      string errorText,
                                      DataGridViewCellStyle cellStyle,
                                      DataGridViewAdvancedBorderStyle advancedBorderStyle,
                                      DataGridViewPaintParts paintParts)
        {
            if (!this.m_bDraw)
            {
                base.Paint(graphics,
                    clipBounds,
                    cellBounds,
                    rowIndex,
                    cellState,
                    value,
                    formattedValue,
                    errorText,
                    cellStyle,
                    advancedBorderStyle,
                    paintParts);
            }
            else
            {
                if (this.DataGridView == null)
                {
                    return;
                }

                // First paint the borders and background of the cell.
                base.Paint(graphics,
                    clipBounds,
                    cellBounds,
                    rowIndex,
                    cellState,
                    value,
                    formattedValue,
                    errorText,
                    cellStyle,
                    advancedBorderStyle,
                    paintParts & ~(DataGridViewPaintParts.ErrorIcon | DataGridViewPaintParts.ContentForeground));

                var ptCurrentCell = this.DataGridView.CurrentCellAddress;
                var cellCurrent = ptCurrentCell.X == this.ColumnIndex && ptCurrentCell.Y == rowIndex;
                var cellEdited = cellCurrent && this.DataGridView.EditingControl != null;

                // If the cell is in editing mode, there is nothing else to paint
                if (!cellEdited)
                {
                    if (PartPainted(paintParts, DataGridViewPaintParts.ContentForeground))
                    {
                        // Paint a NumericUpDown control
                        // Take the borders into account
                        var borderWidths = this.BorderWidths(advancedBorderStyle);
                        var valBounds = cellBounds;
                        valBounds.Offset(borderWidths.X, borderWidths.Y);
                        valBounds.Width -= borderWidths.Right;
                        valBounds.Height -= borderWidths.Bottom;
                        // Also take the padding into account
                        if (cellStyle.Padding !=
                            Padding.Empty)
                        {
                            if (this.DataGridView.RightToLeft ==
                                RightToLeft.Yes)
                            {
                                valBounds.Offset(cellStyle.Padding.Right, cellStyle.Padding.Top);
                            }
                            else
                            {
                                valBounds.Offset(cellStyle.Padding.Left, cellStyle.Padding.Top);
                            }
                            valBounds.Width -= cellStyle.Padding.Horizontal;
                            valBounds.Height -= cellStyle.Padding.Vertical;
                        }
                        // Determine the NumericUpDown control location
                        valBounds = this.GetAdjustedEditingControlBounds(valBounds, cellStyle);

                        var cellSelected = (cellState & DataGridViewElementStates.Selected) != 0;

                        if (ms_renderingBitmap.Width < valBounds.Width ||
                            ms_renderingBitmap.Height < valBounds.Height)
                        {
                            // The static bitmap is too small, a bigger one needs to be allocated.
                            ms_renderingBitmap.Dispose();
                            ms_renderingBitmap = new Bitmap(valBounds.Width, valBounds.Height);
                        }
                        // Make sure the NumericUpDown control is parented to a visible control

                        if (this.m_expose.Parent != this.DataGridView)
                        {
                            this.m_expose.Parent = this.DataGridView;
                        }

                        // Set all the relevant properties
                        this.m_expose.Width = valBounds.Width;
                        this.m_expose.RightToLeft = this.DataGridView.RightToLeft;
                        this.m_expose.Location = new Point(0, -this.m_expose.Height - 100);

                        this.m_expose.Setup(this.m_objectInstance, this.m_fieldInstance, this.m_parentNode);

                        this.m_expose.Enabled = !this.m_objectInstance.IsReadOnly;

                        // this.Value = sm_Slider.Value;

                        Color backColor;
                        if (PartPainted(paintParts, DataGridViewPaintParts.SelectionBackground) && cellSelected)
                        {
                            backColor = cellStyle.SelectionBackColor;
                        }
                        else
                        {
                            backColor = cellStyle.BackColor;
                        }
                        if (PartPainted(paintParts, DataGridViewPaintParts.Background))
                        {
                            if (backColor.A < 255)
                            {
                                backColor = Color.FromArgb(255, backColor);
                            }
                            this.m_expose.BackColor = backColor;
                        }
                        // Finally paint the NumericUpDown control
                        var srcRect = new Rectangle(0, 0, valBounds.Width, valBounds.Height);
                        if (srcRect.Width > 0 &&
                            srcRect.Height > 0)
                        {
                            this.m_expose.DrawToBitmap(ms_renderingBitmap, srcRect);
                            graphics.DrawImage(ms_renderingBitmap,
                                new Rectangle(valBounds.Location, valBounds.Size),
                                srcRect,
                                GraphicsUnit.Pixel);
                        }
                    }
                    if (PartPainted(paintParts, DataGridViewPaintParts.ErrorIcon))
                    {
                        //// Paint the potential error icon on top of the NumericUpDown control
                        base.Paint(graphics,
                            clipBounds,
                            cellBounds,
                            rowIndex,
                            cellState,
                            value,
                            formattedValue,
                            errorText,
                            cellStyle,
                            advancedBorderStyle,
                            DataGridViewPaintParts.ErrorIcon);
                    }
                }
            }
        }

        [DllImport("User32.dll")]
        public static extern bool PrintWindow(IntPtr hWnd, IntPtr dc, uint reservedFlag);

        public override void PositionEditingControl(bool setLocation,
                                                    bool setSize,
                                                    Rectangle cellBounds,
                                                    Rectangle cellClip,
                                                    DataGridViewCellStyle cellStyle,
                                                    bool singleVerticalBorderAdded,
                                                    bool singleHorizontalBorderAdded,
                                                    bool isFirstDisplayedColumn,
                                                    bool isFirstDisplayedRow)
        {
            var editingControlBounds = this.PositionEditingPanel(cellBounds,
                cellClip,
                cellStyle,
                singleVerticalBorderAdded,
                singleHorizontalBorderAdded,
                isFirstDisplayedColumn,
                isFirstDisplayedRow);
            editingControlBounds = this.GetAdjustedEditingControlBounds(editingControlBounds, cellStyle);
            this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
            this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
        }

        private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
        {
            // Add a 1 pixel padding on the left and right of the editing control
            editingControlBounds.X += 1;
            editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);

            // Adjust the vertical location of the editing control:
            var preferredHeight = this.m_expose.Height;
            if (preferredHeight < editingControlBounds.Height)
            {
                switch (cellStyle.Alignment)
                {
                    case DataGridViewContentAlignment.MiddleLeft:
                    case DataGridViewContentAlignment.MiddleCenter:
                    case DataGridViewContentAlignment.MiddleRight:
                        editingControlBounds.Y += (editingControlBounds.Height - preferredHeight) / 2;
                        break;
                    case DataGridViewContentAlignment.BottomLeft:
                    case DataGridViewContentAlignment.BottomCenter:
                    case DataGridViewContentAlignment.BottomRight:
                        editingControlBounds.Y += editingControlBounds.Height - preferredHeight;
                        break;
                }
            }

            return editingControlBounds;
        }

        public override void DetachEditingControl()
        {
            base.DetachEditingControl();
            this.Value = this.GetValue();
        }

        protected override void Dispose(bool disposing)
        {
            this.m_objectInstance = null;
            this.m_parentNode = null;
            this.m_fieldNode = null;
            this.m_fieldInstance = null;
            this.m_editor = null;
            base.Dispose(disposing);
        }
    }
}