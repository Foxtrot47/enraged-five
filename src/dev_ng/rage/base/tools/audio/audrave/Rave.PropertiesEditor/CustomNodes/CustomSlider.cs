namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public partial class CustomSlider : TrackBar
    {
        private bool isDragging;
        private bool isDrawing;

        public CustomSlider()
        {
            this.InitializeComponent();
            this.isDragging = false;
        }

        public void Setup(bool draw)
        {
            this.isDrawing = draw;
            this.isDragging = draw;
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x0201: //mouse down
                    if (this.isDragging)
                    {
                        base.WndProc(ref m);
                    }
                    break;
                case 0x202: //mouse up
                    if (!this.isDrawing)
                    {
                        this.isDragging = false;
                    }
                    base.WndProc(ref m);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        [DllImport("user32.dll")]
        private static extern bool SendMessage(IntPtr hWnd, Int32 msg, Int32 wParam, Int32 lParam);

        private int MakeLong(int loWord, int hiWord)
        {
            return (hiWord << 16) | (loWord & 0xffff);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (!this.isDragging &&
                e.Button == MouseButtons.Left)
            {
                //if dragging send mouse down message
                this.isDragging = true;
                var lParam = this.MakeLong(e.X, e.Y);
                SendMessage(this.Handle, 0x0201, 0, lParam);
            }

            base.OnMouseMove(e);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            // TODO: Add custom paint code here

            // Calling the base class OnPaint
            base.OnPaint(pe);
        }
    }
}