namespace Rave.PropertiesEditor.TemplateCustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;

    [DesignerCategory("code")]
    public class ctrlTemplateTextCell : DataGridViewTextBoxCell,
                                        ITemplateFieldCell
    {
        private string m_Alias;

        private TemplateTextCellEditor m_Editor;
        private IFieldDefinition m_Field;
        private XmlNode m_FieldNode;
        private ContextMenuStrip m_Menu;
        private ITypedObjectInstance m_Object;
        private XmlNode m_ParentNode;
        private ITemplate m_Template;
        private XmlNode m_TemplateFieldNode;
        private ITypedObjectInstance m_TemplateObject;
        private XmlNode m_TemplateParentNode;

        public override Type EditType
        {
            get
            {
                // Return the type of the editing contol that CalendarCell uses.
                return typeof (TemplateTextCellEditor);
            }
        }

        public override Type ValueType
        {
            get { return typeof (string); }
        }

        public override object DefaultNewRowValue
        {
            get { return ""; }
        }

        #region ITemplateFieldCell Members

        public bool OverridesTemplate
        {
            get { return (this.m_FieldNode != null && this.m_FieldNode.InnerText != this.m_TemplateFieldNode.InnerText); }
        }

        public bool IsDefaultValue
        {
            get
            {
                return (this.m_FieldNode == null && this.m_TemplateFieldNode == null ||
                        this.m_FieldNode == null && this.m_TemplateFieldNode.InnerText == this.m_Field.DefaultValue ||
                        this.m_FieldNode != null && this.m_FieldNode.InnerText == this.m_Field.DefaultValue);
            }
        }

        public TemplateCellType Type
        {
            get { return TemplateCellType.TEXT; }
        }

        #endregion

        public void Init(ITemplate template,
                         ITypedObjectInstance o,
                         ITypedObjectInstance templateObject,
                         XmlNode parent,
                         XmlNode templateParent,
                         IFieldDefinition field)
        {
            this.m_Template = template;

            this.m_Object = o;
            this.m_TemplateObject = templateObject;

            this.m_ParentNode = parent;
            this.m_TemplateParentNode = templateParent;

            this.m_Field = field;
            this.m_Alias = string.Empty;
            this.m_FieldNode = null;

            this.Value = this.GetValue();
            if (this.m_Menu == null)
            {
                this.m_Menu = new ContextMenuStrip();
                this.m_Menu.Items.Add(new ToolStripMenuItem("Reset", null, new EventHandler(this.On_Reset)));
                this.ContextMenuStrip = this.m_Menu;
                this.ContextMenuStrip.Opening += this.ContextMenuStrip_Opening;
                this.ContextMenuStrip = this.m_Menu;
            }
        }

        private void On_Reset(object sender, EventArgs e)
        {
            if (this.m_FieldNode != null)
            {
                this.m_Object.Node.RemoveChild(this.m_FieldNode);
                this.m_Object.MarkAsDirty();
                this.Value = this.GetValue();
                this.RaiseCellValueChanged(new DataGridViewCellEventArgs(this.ColumnIndex, this.RowIndex));
            }
        }

        private void ContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (this.m_FieldNode == null)
            {
                e.Cancel = true;
            }
        }

        private string GetValue()
        {
            var returnVal = "";

            this.m_TemplateFieldNode = PropertyEditorUtil.FindXmlNode(this.m_Field.Name, this.m_TemplateParentNode);

            if (this.m_TemplateFieldNode != null &&
                this.m_TemplateFieldNode.Attributes["ExposeAs"] != null)
            {
                this.m_Alias = this.m_TemplateFieldNode.Attributes["ExposeAs"].Value;
            }

            if (this.m_Alias != string.Empty)
            {
                this.m_FieldNode = PropertyEditorUtil.FindXmlNode(this.m_Alias, this.m_ParentNode);
            }

            if (this.m_FieldNode != null)
            {
                returnVal = this.m_FieldNode.InnerText;
            }
            else if (this.m_TemplateFieldNode != null)
            {
                returnVal = this.m_TemplateFieldNode.InnerText;
            }
            else
            {
                returnVal = this.m_Field.DefaultValue;
            }

            return returnVal;
        }

        public override void InitializeEditingControl(int rowIndex,
                                                      object initialFormattedValue,
                                                      DataGridViewCellStyle dataGridViewCellStyle)
        {
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
            this.m_Editor = this.DataGridView.EditingControl as TemplateTextCellEditor;
            this.m_Editor.Init(this.m_Template, this.m_Object, this.m_TemplateObject, this.m_ParentNode, this.m_TemplateParentNode, this.m_Field);
        }

        private static bool PartPainted(DataGridViewPaintParts paintParts, DataGridViewPaintParts paintPart)
        {
            return (paintParts & paintPart) != 0;
        }

        protected override void Paint(Graphics graphics,
                                      Rectangle clipBounds,
                                      Rectangle cellBounds,
                                      int rowIndex,
                                      DataGridViewElementStates cellState,
                                      object value,
                                      object formattedValue,
                                      string errorText,
                                      DataGridViewCellStyle cellStyle,
                                      DataGridViewAdvancedBorderStyle advancedBorderStyle,
                                      DataGridViewPaintParts paintParts)
        {
            this.Value = this.GetValue();
            base.Paint(graphics,
                clipBounds,
                cellBounds,
                rowIndex,
                cellState,
                value,
                formattedValue,
                errorText,
                cellStyle,
                advancedBorderStyle,
                paintParts);
        }

        public override void PositionEditingControl(bool setLocation,
                                                    bool setSize,
                                                    Rectangle cellBounds,
                                                    Rectangle cellClip,
                                                    DataGridViewCellStyle cellStyle,
                                                    bool singleVerticalBorderAdded,
                                                    bool singleHorizontalBorderAdded,
                                                    bool isFirstDisplayedColumn,
                                                    bool isFirstDisplayedRow)
        {
            var editingControlBounds = this.PositionEditingPanel(cellBounds,
                cellClip,
                cellStyle,
                singleVerticalBorderAdded,
                singleHorizontalBorderAdded,
                isFirstDisplayedColumn,
                isFirstDisplayedRow);
            editingControlBounds = this.GetAdjustedEditingControlBounds(editingControlBounds, cellStyle);
            this.DataGridView.EditingControl.Location = new Point(editingControlBounds.X, editingControlBounds.Y);
            this.DataGridView.EditingControl.Size = new Size(editingControlBounds.Width, editingControlBounds.Height);
        }

        private Rectangle GetAdjustedEditingControlBounds(Rectangle editingControlBounds, DataGridViewCellStyle cellStyle)
        {
            // Add a 1 pixel padding on the left and right of the editing control
            editingControlBounds.X += 1;
            editingControlBounds.Width = Math.Max(0, editingControlBounds.Width - 2);

            // Adjust the vertical location of the editing control:
            var preferredHeight = this.Size.Height;
            if (preferredHeight < editingControlBounds.Height)
            {
                switch (cellStyle.Alignment)
                {
                    case DataGridViewContentAlignment.MiddleLeft:
                    case DataGridViewContentAlignment.MiddleCenter:
                    case DataGridViewContentAlignment.MiddleRight:
                        editingControlBounds.Y += (editingControlBounds.Height - preferredHeight) / 2;
                        break;
                    case DataGridViewContentAlignment.BottomLeft:
                    case DataGridViewContentAlignment.BottomCenter:
                    case DataGridViewContentAlignment.BottomRight:
                        editingControlBounds.Y += editingControlBounds.Height - preferredHeight;
                        break;
                }
            }

            return editingControlBounds;
        }

        public override void DetachEditingControl()
        {
            this.m_Editor.UpdateNode();
            base.DetachEditingControl();
            this.Value = this.GetValue();
        }

        protected override void Dispose(bool disposing)
        {
            this.m_Object = null;
            this.m_ParentNode = null;
            this.m_FieldNode = null;
            this.m_Field = null;
            this.m_Editor = null;
            base.Dispose(disposing);
        }
    }
}