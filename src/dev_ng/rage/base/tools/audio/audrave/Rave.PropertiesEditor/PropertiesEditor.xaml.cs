﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PropertiesEditor.xaml.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The properties editor v2.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Rave.Controls.Forms;

namespace Rave.PropertiesEditor
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Windows;
	using System.Windows.Controls;
	using System.Windows.Forms.Integration;
	using System.Windows.Input;
	using System.Windows.Media;
	using System.Windows.Media.Imaging;
	using System.ComponentModel;

	using rage.ToolLib;

	using Rave.Instance;
	using Rave.Plugins.Infrastructure.Interfaces;
	using Rave.PropertiesEditor.Infrastructure.Interfaces;
	using Rave.TypeDefinitions.Infrastructure.Interfaces;
	using Rave.Types.Infrastructure.Interfaces.Objects;
	using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
	using Rave.Types.ObjectBanks.Templates;
	using Rave.Types.Objects.TypedObjects;
	using Rave.Utils;
	using WPFToolLib.Extensions;

	/// <summary>
	/// The properties editor v2.
	/// </summary>
	public partial class PropertiesEditor : UserControl, IPropertiesEditor, INotifyPropertyChanged
	{
		#region Fields

		/// <summary>
		/// The m_ default editor.
		/// </summary>
		private readonly DefaultEditor m_DefaultEditor;

		/// <summary>
		/// The m_ dual editor host.
		/// </summary>
		private readonly TabControl m_DualEditorHost;

		/// <summary>
		/// The m_ icon table.
		/// </summary>
		private readonly Dictionary<string, Dictionary<string, string>> m_IconTable;

		/// <summary>
		/// The m_ parents.
		/// </summary>
		private readonly List<IObjectInstance> m_Parents;

		/// <summary>
		/// The m_scaled image cache.
		/// </summary>
		private readonly Dictionary<string, ImageSource> m_scaledImageCache = new Dictionary<string, ImageSource>();

		/// <summary>
		/// The history.
		/// </summary>
		private readonly List<HistoryObject> history;

		/// <summary>
		/// The m_ content.
		/// </summary>
		private Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_Content;

		/// <summary>
		/// The m_ custom editor.
		/// </summary>
		private IRAVEObjectEditorPlugin m_CustomEditor;

		/// <summary>
		/// The m_ mode.
		/// </summary>
		private Mode m_Mode;

		/// <summary>
		/// The current history index.
		/// </summary>
		private int currentHistoryIndex;

		/// <summary>
		/// The m_ object.
		/// </summary>
		private IObjectInstance objectInstance;

		private bool _autoUpdate = true;

		#endregion

		#region Constructors and Destructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Rave.PropertiesEditor.PropertiesEditor"/> class.
		/// </summary>
		public PropertiesEditor()
		{
			this.InitializeComponent();

			this.history = new List<HistoryObject>();

			this.m_IconTable = new Dictionary<string, Dictionary<string, string>>();

			this.m_DefaultEditor = new DefaultEditor { Dock = System.Windows.Forms.DockStyle.Fill };
			this.m_DefaultEditor.OnObjectRefClick += this.Editor_OnObjectRefClick;
			this.m_DefaultEditor.OnTemplateRefClick += this.Editor_OnTemplateRefClick;
			this.m_DefaultEditor.OnWaveRefClick += this.Editor_OnWaveRefClick;
			this.m_DefaultEditor.OnWaveRefAudition += this.Editor_OnWaveRefAudition;
			this.m_DefaultEditor.OnObjectEditClick += this.Editor_OnObjectEditClick;
			this.m_DefaultEditor.OnWaveBankRefClick += this.Editor_OnWaveBankRefClick;

			this.m_DualEditorHost = new TabControl();
			this.m_DualEditorHost.SelectionChanged += this.m_DualEditorHost_SelectionChanged;
			this.m_Parents = new List<IObjectInstance>();

			foreach (var metadataType in Configuration.MetadataTypes)
			{
				this.m_IconTable[metadataType.Type] = Utils.CreateTypeIconList(
					metadataType.IconPath, RaveInstance.AllTypeDefinitions[metadataType.Type]);
			}
		}

		#endregion

		#region Public Events

		/// <summary>
		/// The audition sound.
		/// </summary>
		public event Action<IObjectInstance> AuditionSound;

		/// <summary>
		/// The on object ref click.
		/// </summary>
		public event Action<IObjectInstance> OnObjectRefClick;

		/// <summary>
		/// The on template ref click.
		/// </summary>
		public event Action<ITemplate> OnTemplateRefClick;

		/// <summary>
		/// The on wave bank ref click.
		/// </summary>
		public event Action<string> OnWaveBankRefClick;

		/// <summary>
		/// The on wave ref audition.
		/// </summary>
		public event Action<string, string> OnWaveRefAudition;

		/// <summary>
		/// The on wave ref click.
		/// </summary>
		public event Action<string, string> OnWaveRefClick;

		/// <summary>
		/// The stop sound audition.
		/// </summary>
		public event Action<IObjectInstance> StopSoundAudition;

		/// <summary>
		/// The create floating editor.
		/// </summary>
		public event Action<IObjectInstance> OnCreateFloatingEditorClick;

		#endregion

		#region Public Properties

		/// <summary>
		/// Gets the dockable content.
		/// </summary>
		public Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable DockableContent
		{
			get
			{
				if (this.m_Content == null)
				{
					this.m_Content = new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
					{
						Title = "Properties Editor",
						Content = this,
						FloatingHeight = DockableWFControl.FLOATING_HEIGHT,
						FloatingWidth = DockableWFControl.FLOATING_WIDTH,
						FloatingLeft = DockableWFControl.FLOATING_LEFT,
						FloatingTop = DockableWFControl.FLOATING_TOP
					};
				}

				return this.m_Content;
			}
		}


		public IObjectInstance ObjectInstance
		{
			get
			{
				if (objectInstance != null)
				{
					objectInstance.PropertyChanged -= this.RouteEvent;
					objectInstance.PropertyChanged += this.RouteEvent;
				}
				return objectInstance;
			}
		}

		public bool IsMuted
		{
			get
			{
				if (ObjectInstance != null)
				{
					return ObjectInstance.IsMuted;
				}
				else
				{
					return false;
				}
			}
		}
		public bool IsSoloed
		{
			get
			{
				if (ObjectInstance != null)
				{
					return ObjectInstance.IsSoloed;
				}
				else
				{
					return false;
				}
			}
		}

		public bool IsMuteSoloEnabled
		{
			get
			{
				if (this.objectInstance != null)
				{
					return this.objectInstance.CheckIfMuteSoloEnabled();
				}
				else
				{ return false; }
			}
		}

		public bool AutoUpdate
		{
			get { return _autoUpdate; }
			set
			{
				_autoUpdate = value;
				PropertyChanged.Raise(this, "AutoUpdate");
			}
		}

		#endregion

		#region Public Methods and Operators

		/// <summary>
		/// The edit object.
		/// </summary>
		/// <param name="obj">
		/// The object.
		/// </param>
		/// <param name="mode">
		/// The mode.
		/// </param>
		/// <param name="updateHistory">
		/// The update history.
		/// </param>
		public void EditObject(object obj, Mode? mode = null, bool updateHistory = true)
		{
			if (null == obj)
			{
				this.Reset();
				return;
			}

			if (this.objectInstance != null && this.objectInstance.Bank != null)
			{
				this.objectInstance.Bank.BankStatusChanged -= this.OnBankStatusChanged;
			}

			var objInstance = obj as IObjectInstance;
			var templateProxyObject = obj as ITemplateProxyObject;
			if (null != objInstance)
			{
				this.objectInstance = objInstance;
			}
			else if (null != templateProxyObject)
			{
				// Create new object instance for template just for editing puposes
				var dummyBank = new TemplateDummyBank(templateProxyObject.Template.TemplateBank);
				ITypeDefinitions typeDefs;
				if (!RaveInstance.AllTypeDefinitions.TryGetValue(templateProxyObject.Type, out typeDefs))
				{
					throw new Exception("Failed to find type definitions: " + templateProxyObject.Type);
				}

				var element = dummyBank.Document.CreateElement("TemplateInstance");
				element.SetAttribute("name", templateProxyObject.Name);
				element.SetAttribute("template", templateProxyObject.Name);

				this.objectInstance = new TypedObjectInstance(
					templateProxyObject.Node,
					dummyBank,
					templateProxyObject.Type,
					dummyBank.Episode,
					dummyBank.ObjectLookup,
					typeDefs);
			}
			else
			{
				throw new Exception("Object type not supported");
			}

			if (null == mode)
			{
				this.m_Mode = null != objInstance ? Mode.DEFAULT : Mode.TEMPLATE;
			}
			else
			{
				this.m_Mode = (Mode)mode;
			}

			this.m_Parents.Clear();

			this.InitPropertiesEditor();

			if (updateHistory)
			{
				this.UpdateHistory();
			}

			// Disabled for now - causing RAVE to crash after running for any length of
			// time and afer a number of object drag+drop actions have been performed
			// m_Object.OnObjectChildrenChanged += ObjectInstance_ObjectChildrenChanged;
		}

		/// <summary>
		/// The properties editor_ key down.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="keyEventArgs">
		/// The key event args.
		/// </param>
		public void PropertiesEditor_KeyDown(object sender, KeyEventArgs keyEventArgs)
		{
			if (Keyboard.Modifiers != ModifierKeys.Control)
			{
				return;
			}

			// Ctrl + number key changes selected tab
			var keyValue = keyEventArgs.Key.ToString();
			if (keyValue.Length == 2)
			{
				if (keyValue.StartsWith("D"))
				{
					keyValue = keyValue.Substring(1);
				}
				else if (keyValue.StartsWith("Numpad"))
				{
					keyValue = keyValue.Substring(6);
				}

				int number;
				if (int.TryParse(keyValue, out number))
				{
					if (number == 0)
					{
						number = 10;
					}

					if (number <= this.m_DefaultEditor.TabPages.Count)
					{
						this.m_DefaultEditor.SelectedIndex = --number;
					}
				}
			}
			else
			{
				// '-' and '+' keys scroll down/up available tabs
				switch (keyEventArgs.Key)
				{
					case Key.OemMinus:
						if (this.m_DefaultEditor.SelectedIndex > 0)
						{
							this.m_DefaultEditor.SelectedIndex--;
						}

						break;
					case Key.OemPlus:
						if (this.m_DefaultEditor.SelectedIndex < this.m_DefaultEditor.TabPages.Count - 1)
						{
							this.m_DefaultEditor.SelectedIndex++;
						}

						break;
				}
			}
		}

		/// <summary>
		/// The refresh view.
		/// </summary>
		public void RefreshView()
		{
			// Only refresh properties editor contents if 
			// method called from same thread (temporary).
			if (this.Dispatcher.CheckAccess())
			{
				this.InitPropertiesEditor();
			}
		}

		/// <summary>
		/// The reset.
		/// </summary>
		public void Reset()
		{

			this.HeaderGrid.Visibility = Visibility.Visible;
			foreach (TabItem v in this.m_DualEditorHost.Items)
			{
				v.Content = null;
			}

			this.m_DualEditorHost.Items.Clear();
			this.MainPanel.Children.Clear();

			this.ObjectName.Text = null;
			this.ObjectType.Text = null;
			this.ObjectTypeImage.Source = null;
		}

		/// <summary>
		/// The update display.
		/// </summary>
		public void UpdateDisplay()
		{
			this.EditObject(this.objectInstance, this.m_Mode);
		}

		#endregion

		#region Methods

		/// <summary>
		/// The edit floating_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void EditFloating_Click(object sender, RoutedEventArgs e)
		{
			this.OnCreateFloatingEditorClick.Raise(this.objectInstance);
		}

		/// <summary>
		/// The editor_ on object edit click.
		/// </summary>
		/// <param name="objectRef">
		/// The object ref.
		/// </param>
		private void Editor_OnObjectEditClick(IObjectInstance objectRef)
		{
			this.m_Parents.Insert(this.m_Parents.Count, this.objectInstance);

			this.objectInstance = objectRef;

			if (this.Setup())
			{
				if (this.m_CustomEditor != null)
				{
					this.m_CustomEditor.EditObject(this.objectInstance, this.m_Mode);
				}

				var mnuMain = new System.Windows.Forms.ContextMenu();
				this.m_DefaultEditor.EditObject(this.objectInstance, this.m_Mode, mnuMain, false);
			}

			rage.ToolLib.EventExtensions.Raise(this.OnObjectRefClick, objectRef);
		}

		/// <summary>
		/// The editor_ on object ref click.
		/// </summary>
		/// <param name="objectRef">
		/// The object ref.
		/// </param>
		private void Editor_OnObjectRefClick(IObjectInstance objectRef)
		{
			rage.ToolLib.EventExtensions.Raise(this.OnObjectRefClick, objectRef);
		}

		/// <summary>
		/// The editor_ on template ref click.
		/// </summary>
		/// <param name="template">
		/// The template.
		/// </param>
		private void Editor_OnTemplateRefClick(ITemplate template)
		{
			rage.ToolLib.EventExtensions.Raise(this.OnTemplateRefClick, template);
		}

		/// <summary>
		/// The editor_ on wave bank ref click.
		/// </summary>
		/// <param name="bank">
		/// The bank.
		/// </param>
		private void Editor_OnWaveBankRefClick(string bank)
		{
			rage.ToolLib.EventExtensions.Raise(this.OnWaveBankRefClick, bank);
		}

		/// <summary>
		/// The editor_ on wave ref audition.
		/// </summary>
		/// <param name="bank">
		/// The bank.
		/// </param>
		/// <param name="wave">
		/// The wave.
		/// </param>
		private void Editor_OnWaveRefAudition(string bank, string wave)
		{
			rage.ToolLib.EventExtensions.Raise(this.OnWaveRefAudition, bank, wave);
		}

		/// <summary>
		/// The editor_ on wave ref click.
		/// </summary>
		/// <param name="bank">
		/// The bank.
		/// </param>
		/// <param name="wave">
		/// The wave.
		/// </param>
		private void Editor_OnWaveRefClick(string bank, string wave)
		{
			rage.ToolLib.EventExtensions.Raise(this.OnWaveRefClick, bank, wave);
		}

		/// <summary>
		/// The expand all_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void ExpandAll_Click(object sender, RoutedEventArgs e)
		{
			this.m_DefaultEditor.ExpandAll();
		}

		private void CollapseAll_Click(object sender, RoutedEventArgs e)
		{
			this.m_DefaultEditor.CollapseAll();
		}

		/// <summary>
		/// Initialize the properties editor.
		/// </summary>
		private void InitPropertiesEditor()
		{
			var mnuMain = new System.Windows.Forms.ContextMenu();
			if (this.Setup())
			{
				if (this.m_CustomEditor != null)
				{
					this.m_CustomEditor.EditObject(this.objectInstance, this.m_Mode);
				}

				// Check that current object instance in properties editor still exists after reloading
				if (null == this.objectInstance || null == this.objectInstance.Node)
				{
					this.m_DefaultEditor.Reset();
				}
				else
				{
					this.m_DefaultEditor.EditObject(this.objectInstance, this.m_Mode, mnuMain, false);
				}

				if (this.objectInstance.Bank != null)
				{
					this.objectInstance.Bank.BankStatusChanged += this.OnBankStatusChanged;
				}
			}
		}

		/// <summary>
		/// The load type image.
		/// </summary>
		/// <param name="objInst">
		/// The object instance.
		/// </param>
		/// <returns>
		/// The <see cref="ImageSource"/>.
		/// </returns>
		private ImageSource LoadTypeImage(IObjectInstance objInst)
		{
			var cacheKey = objInst.Type + objInst.TypeName;
			if (this.m_scaledImageCache.ContainsKey(cacheKey))
			{
				return this.m_scaledImageCache[cacheKey];
			}

			var b =
				new BitmapImage(
					new Uri(
						Directory.GetCurrentDirectory() + "\\" + this.m_IconTable[objInst.Type][objInst.TypeName],
						UriKind.Absolute));

			if (b.PixelHeight != 16 || b.PixelWidth != 16 || b.Format.BitsPerPixel != 32)
			{
				System.Diagnostics.Debug.WriteLine(
					"Unsupported icon format: {0}x{1} {2}bpp", b.PixelWidth, b.PixelHeight, b.Format.BitsPerPixel);
				return b;
			}

			// Rescale to 32x32 by doubling pixels
			int stride = (b.PixelWidth * b.Format.BitsPerPixel + 7) / 8;
			var pixelByteArray = new byte[b.PixelHeight * stride];
			b.CopyPixels(pixelByteArray, stride, 0);

			int scaledStride = (32 * b.Format.BitsPerPixel + 7) / 8;
			var scaledPixels = new byte[32 * scaledStride];

			for (int x = 0; x < 32; x++)
			{
				int sourceX = x / 2;
				for (int y = 0; y < 32; y++)
				{
					int sourceY = y / 2;
					for (int i = 0; i < 4; i++)
					{
						scaledPixels[(x + y * 32) * 4 + i] = pixelByteArray[(sourceX + sourceY * 16) * 4 + i];
					}
				}
			}

			var newBitmap = new WriteableBitmap(32, 32, 96, 96, b.Format, b.Palette);
			newBitmap.WritePixels(new Int32Rect(0, 0, 32, 32), scaledPixels, scaledStride, 0);

			this.m_scaledImageCache.Add(cacheKey, newBitmap);
			return newBitmap;
		}

		/// <summary>
		/// Update the history.
		/// </summary>
		private void UpdateHistory()
		{
			// clear out forward history
			while (this.history.Count - 1 > this.currentHistoryIndex)
			{
				this.history.RemoveAt(this.history.Count - 1);
			}

			this.history.Add(new HistoryObject(this.objectInstance.Name, this.objectInstance.Bank, this.m_Mode));

			this.currentHistoryIndex = this.history.Count - 1;
		}

		/// <summary>
		/// Navigate history backwards.
		/// </summary>
		private void NavigateHistoryBackwards()
		{
			if (this.currentHistoryIndex <= 0)
			{
				return;
			}

			var hb = this.history[--this.currentHistoryIndex];
			if (null == hb.GetObject())
			{
				this.history.RemoveRange(0, this.currentHistoryIndex + 1);
				this.currentHistoryIndex = 0;
			}
			else
			{
				this.EditObject(hb.GetObject(), hb.Mode, false);
			}
		}

		/// <summary>
		/// Navigate history forward.
		/// </summary>
		private void NavigateHistoryForward()
		{
			if (this.currentHistoryIndex >= this.history.Count - 1)
			{
				return;
			}

			var hb = this.history[++this.currentHistoryIndex];
			if (null == hb.GetObject())
			{
				this.history.RemoveRange(
					this.currentHistoryIndex, this.history.Count - this.currentHistoryIndex);
				this.currentHistoryIndex = this.history.Count - 1;
			}

			this.EditObject(hb.GetObject(), hb.Mode, false);
		}

		/// <summary>
		/// The on bank status changed.
		/// </summary>
		private void OnBankStatusChanged()
		{
			if (null != this.objectInstance &&
				null != this.objectInstance.ObjectLookup)
			{
				if (this.objectInstance.ObjectLookup.ContainsKey(this.objectInstance.Name))
				{
					this.objectInstance = this.objectInstance.ObjectLookup[this.objectInstance.Name];
				}
				else
				{
					// Object has been deleted
					this.Reset();
					this.HeaderGrid.Visibility = Visibility.Hidden;
				}
			}
		}

		/// <summary>
		/// The undo button_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void UndoButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigateHistoryBackwards();
		}

		/// <summary>
		/// The redo button_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void RedoButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigateHistoryForward();
		}

		/// <summary>
		/// The play button_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void PlayButton_Click(object sender, RoutedEventArgs e)
		{
			this.AuditionSound.Raise(this.objectInstance);
		}

		/// <summary>
		/// The refresh_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void Refresh_Click(object sender, RoutedEventArgs e)
		{
			this.EditObject(this.objectInstance, this.m_Mode);
		}

		/// <summary>
		/// The setup.
		/// </summary>
		/// <returns>
		/// The <see cref="bool"/>.
		/// </returns>
		private bool Setup()
		{
			this.Reset();

			if (this.objectInstance == null)
			{
				return false;
			}

			//Check if muted and soloed
			PropertyChanged.Raise(this, "IsMuted");
			PropertyChanged.Raise(this, "IsSoloed");
			PropertyChanged.Raise(this, "IsMuteSoloEnabled");

			this.OnBankStatusChanged();

			if (this.m_CustomEditor != null)
			{
				this.m_CustomEditor.OnObjectRefClick -= this.Editor_OnObjectRefClick;
				this.m_CustomEditor.OnWaveRefClick -= this.Editor_OnWaveRefClick;
				this.m_CustomEditor.OnWaveBankRefClick -= this.Editor_OnWaveBankRefClick;
				this.m_CustomEditor.OnObjectEditClick -= this.Editor_OnObjectEditClick;
				this.m_CustomEditor.Dispose();
				this.m_CustomEditor = null;
			}

			if (RaveInstance.PluginManager != null
				&& RaveInstance.PluginManager.GetObjectEditorPlugin(this.objectInstance.TypeName.ToUpper()) != null)
			{
				this.m_CustomEditor = RaveInstance.PluginManager.GetObjectEditorPlugin(this.objectInstance.TypeName.ToUpper());
			}

			if (this.m_CustomEditor != null)
			{
				this.m_CustomEditor.OnObjectRefClick += this.Editor_OnObjectRefClick;
				this.m_CustomEditor.OnWaveRefClick += this.Editor_OnWaveRefClick;
				this.m_CustomEditor.OnWaveBankRefClick += this.Editor_OnWaveBankRefClick;
				this.m_CustomEditor.OnObjectEditClick += this.Editor_OnObjectEditClick;

				var customEditorTabPage = new TabItem { Header = this.m_CustomEditor.GetName() };

				var uiElement = this.m_CustomEditor as UIElement;

				// Do we have a WPF custom editor?
				if (uiElement != null)
				{
					customEditorTabPage.Content = this.m_CustomEditor;
				}
				else
				{
					customEditorTabPage.Content = new WindowsFormsHost
					{
						Child = this.m_CustomEditor as System.Windows.Forms.Control
					};
				}

				this.m_DualEditorHost.Items.Add(customEditorTabPage);

				var defaultTabPage = new TabItem { Header = this.m_DefaultEditor.GetName() };
				defaultTabPage.Content = new WindowsFormsHost { Child = this.m_DefaultEditor };
				this.m_DualEditorHost.Items.Add(defaultTabPage);

				this.MainPanel.Children.Add(this.m_DualEditorHost);
			}
			else
			{
				this.MainPanel.Children.Add(new WindowsFormsHost { Child = this.m_DefaultEditor });
			}

			this.ObjectName.Text = this.objectInstance.Name;
			this.ObjectType.Text = this.objectInstance.TypeName;
			var templateInstance = this.objectInstance as ITemplateInstance;
			if (templateInstance != null)
			{
				this.ObjectType.Text = string.Format(
					"{0} - {1}", this.objectInstance.TypeName, templateInstance.Template.Name);
			}

			// display image
			this.ObjectTypeImage.Source = this.LoadTypeImage(this.objectInstance);

			return true;
		}

		/// <summary>
		/// The stop button_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void StopButton_Click(object sender, RoutedEventArgs e)
		{
			this.StopSoundAudition.Raise(this.objectInstance);
		}

		/// <summary>
		/// The m_ dual editor host_ selection changed.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void m_DualEditorHost_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.Source is TabControl)
			{
				if (this.m_DualEditorHost.SelectedIndex >= 0)
				{
					if (this.m_CustomEditor != null)
					{
						this.m_CustomEditor.EditObject(this.objectInstance, this.m_Mode);
					}

					var mnuMain = new System.Windows.Forms.ContextMenu();
					this.m_DefaultEditor.EditObject(this.objectInstance, this.m_Mode, mnuMain, false);
				}
			}
		}

		#endregion

		/// <summary>
		/// The properties editor 2_ on key down.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void PropertiesEditor2_OnKeyDown(object sender, KeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.E:
					{
						if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
						{
							this.m_DefaultEditor.ExpandAll();
						}
						break;
					}
			}
		}

		private void SoloButton_Click(object sender, RoutedEventArgs e)
		{
			Rave.MuteSoloManager.MuteSoloManager.Instance.SoloToggle(ObjectInstance);
		}

		private void MuteButton_Click(object sender, RoutedEventArgs e)
		{
			Rave.MuteSoloManager.MuteSoloManager.Instance.MuteToggle(ObjectInstance);
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void RouteEvent(object sender, PropertyChangedEventArgs e)
		{
			this.PropertyChanged.Raise(this, e.PropertyName);
		}

		private void AutoUpdateClick(object sender, RoutedEventArgs e)
		{
			AutoUpdate = !AutoUpdate;
		}
	}
}