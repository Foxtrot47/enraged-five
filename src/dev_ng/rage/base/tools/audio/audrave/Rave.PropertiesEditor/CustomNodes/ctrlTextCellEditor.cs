namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    internal class TextCellEditor : TextBox,
                                    IDataGridViewEditingControl
    {
        private string m_alias;
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_object;
        private XmlNode m_parentNode;
        private bool m_valueChanged;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.Text; }
            set { return; }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged
        {
            get { return this.m_valueChanged; }
            set { this.m_valueChanged = value; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        public void Init(IObjectInstance o, XmlNode parentNode, IFieldDefinitionInstance fieldInstance, string fieldAlias)
        {
            this.m_object = o;
            this.m_parentNode = parentNode;
            this.m_fieldInstance = fieldInstance;
            this.m_alias = fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);

            var fieldDef = this.m_fieldInstance.AsSimpleField();
            if (fieldDef != null &&
                fieldDef.Length > 0)
            {
                this.MaxLength = fieldDef.Length - 1;
            }
			this.SelectAll();
        }

        public void UpdateNode()
        {
            if (this.m_fieldNode == null || this.m_fieldNode.InnerText != this.Text)
            {
                if (this.m_fieldNode == null)
                {
                    this.m_fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_alias);
                    this.m_parentNode.AppendChild(this.m_fieldNode);
                }
                this.m_fieldNode.InnerText = this.Text;
                this.m_object.MarkAsDirty();
            }
        }

        protected override void OnTextChanged(EventArgs e)
        {
            this.m_valueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
            base.OnTextChanged(e);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            this.UpdateNode();
            base.OnLostFocus(e);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.UpdateNode();
                return false;
            }
            if (keyData == (Keys.Control | Keys.C) && !string.IsNullOrEmpty(this.Text))
            {
                Clipboard.SetText(this.Text);
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}