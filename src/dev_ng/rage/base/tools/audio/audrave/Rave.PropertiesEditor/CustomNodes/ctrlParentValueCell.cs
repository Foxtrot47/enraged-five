// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ctrlParentValueCell.cs" company="">
//   
// </copyright>
// <summary>
//   The ctrl parent value cell.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    /// The ctrl parent value cell.
    /// </summary>
    [DesignerCategory("code")]
    public class ctrlParentValueCell : DataGridViewTextBoxCell, IFieldCell
    {
        #region Fields

        /// <summary>
        /// The m_alias.
        /// </summary>
        private string m_alias;

        /// <summary>
        /// The m_field instance.
        /// </summary>
        private IFieldDefinitionInstance m_fieldInstance;

        /// <summary>
        /// The m_field node.
        /// </summary>
        private XmlNode m_fieldNode;

        /// <summary>
        /// The m_heirarchy node.
        /// </summary>
        //private HeirarchyNode m_heirarchyNode;

        /// <summary>
        /// The m_object instance.
        /// </summary>
        private IObjectInstance m_objectInstance;

        /// <summary>
        /// The m_parent.
        /// </summary>
        private XmlNode m_parent;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the default new row value.
        /// </summary>
        public override object DefaultNewRowValue
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the edit type.
        /// </summary>
        public override Type EditType
        {
            get
            {
                // no Editor, read only
                return null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether is default value.
        /// </summary>
        public bool IsDefaultValue
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        public CellType Type
        {
            get
            {
                return CellType.PARENTVALUE;
            }
        }

        /// <summary>
        /// Gets the value type.
        /// </summary>
        public override Type ValueType
        {
            get
            {
                return typeof(string);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="fieldInstance">
        /// The field instance.
        /// </param>
        /// <param name="fieldAlias">
        /// The field alias.
        /// </param>
        /// <param name="shn">
        /// The shn.
        /// </param>
        public void Init(
            IObjectInstance objectInstance, 
            XmlNode parent, 
            IFieldDefinitionInstance fieldInstance, 
            string fieldAlias
            /*HeirarchyNode shn*/)
        {
            this.m_objectInstance = objectInstance;
            this.m_parent = parent;
            this.m_fieldInstance = fieldInstance;
            this.m_alias = string.IsNullOrEmpty(fieldAlias) ? fieldInstance.Name : fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(fieldInstance.Name, parent);
            //this.m_heirarchyNode = shn;

            this.Value = (this.m_fieldNode != null ? this.m_fieldNode.InnerText : fieldInstance.DefaultValue) ?? String.Empty;
            this.UpdateParentValue();
        }

        /// <summary>
        /// The update parent value.
        /// </summary>
        public void UpdateParentValue()
        {
            /*
        var result = 0;
        if (m_heirarchyNode != null)
        {
            var parentNode = m_heirarchyNode.Parent as HeirarchyNode;
            var soundNode = m_heirarchyNode;
            while (parentNode != null &&
                   soundNode.fieldInstance != null)
            {
                var fieldNode = PropertyEditorUtil.FindXmlNode(m_alias, soundNode.fieldInstance.Node);
                if (fieldNode != null &&
                    (fieldNode.Attributes["overrideParent"] == null ||
                     (fieldNode.Attributes["overrideParent"] != null &&
                      fieldNode.Attributes["overrideParent"].Value == "no")))
                {
                    var parentFieldNode = PropertyEditorUtil.FindXmlNode(m_fieldInstance.Name, parentNode.fieldInstance.Node);
                    if (parentFieldNode != null)
                    {
                        result += Int32.Parse(parentFieldNode.InnerText);
                    }
                }
                else
                {
                    break;
                }

                soundNode = parentNode;
                parentNode = parentNode.Parent as HeirarchyNode;
            }

            string displayVal, units;
            PropertyEditorUtil.ConvertToDisplayValue(result, m_fieldInstance.Units, out displayVal, out units);
            Value = displayVal;
        }
        else
        {
            Value = 0;
        }*/
        }

        #endregion

        #region Methods

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            this.m_objectInstance = null;
            this.m_fieldInstance = null;
            this.m_fieldNode = null;
            //this.m_heirarchyNode = null;
            base.Dispose(disposing);
        }

        #endregion
    }
}