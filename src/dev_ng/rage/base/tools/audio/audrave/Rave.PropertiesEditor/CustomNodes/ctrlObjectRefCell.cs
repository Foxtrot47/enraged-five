// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ctrlObjectRefCell.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The ctrl object ref cell.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Windows.Input;
using Rave.Instance;
using Rave.UIControls.AutoComplete;

namespace Rave.PropertiesEditor.CustomNodes
{
	using System;
	using System.ComponentModel;
	using System.Drawing;
	using System.Linq;
	using System.Windows.Forms;
	using System.Xml;
	using Rave.Controls.Forms.Popups;
	using Rave.ObjectBrowser.Infrastructure.Nodes;
	using Rave.PropertiesEditor.Infrastructure.Interfaces;
	using Rave.TypeDefinitions.Infrastructure.Interfaces;
	using Rave.Types;
	using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
	using Rave.Types.Infrastructure.Interfaces.Objects;
	using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
	using Rave.Types.Objects;
	using Rave.UIControls;
	using Rave.Utils;
	using Rave.WaveBrowser.Infrastructure.Nodes;

	/// <summary>
	/// The ctrl object ref cell.
	/// </summary>
	internal class ctrlObjectRefCell : DataGridViewLinkCell, IFieldCell, IObjectRef
	{
		#region Fields

		/// <summary>
		/// The default value link color.
		/// </summary>
		private static readonly Color DefaultValueLinkColor;

		/// <summary>
		/// The custom value link color.
		/// </summary>
		private readonly Color customValueLinkColor;

		/// <summary>
		/// The m_alias.
		/// </summary>
		private string m_alias;

		/// <summary>
		/// The m_edit menu item.
		/// </summary>
		private ToolStripMenuItem m_editMenuItem;

		/// <summary>
		/// The m_field instance.
		/// </summary>
		private IFieldDefinitionInstance m_fieldInstance;

		/// <summary>
		/// The m_field node.
		/// </summary>
		private XmlNode m_fieldNode;

		/// <summary>
		/// The m_instance node.
		/// </summary>
		private XmlNode m_instanceNode;

		/// <summary>
		/// The m_object.
		/// </summary>
		private IObjectInstance m_object;

		/// <summary>
		/// The m_object instance.
		/// </summary>
		private IObjectInstance m_objectInstance;

		/// <summary>
		/// The m_remove menu item.
		/// </summary>
		private ToolStripMenuItem m_removeMenuItem;

		#endregion

		/// <summary>
		/// Initializes static members of the <see cref="ctrlObjectRefCell"/> class.
		/// </summary>
		static ctrlObjectRefCell()
		{
			DefaultValueLinkColor = Color.DarkGray;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ctrlObjectRefCell"/> class.
		/// </summary>
		public ctrlObjectRefCell()
		{
			this.customValueLinkColor = this.LinkColor;
		}

		#region Public Events

		/// <summary>
		/// The on object edit click.
		/// </summary>
		public event Action<IObjectInstance> OnObjectEditClick;

		/// <summary>
		/// The on object ref click.
		/// </summary>
		public event Action<IObjectInstance> OnObjectRefClick;

		#endregion

		#region Public Properties

		/// <summary>
		/// Gets a value indicating whether is default value.
		/// </summary>
		public bool IsDefaultValue
		{
			get
			{
				return this.m_fieldNode == null || this.m_fieldInstance.DefaultValue == this.m_fieldNode.InnerText;
			}
		}

		/// <summary>
		/// Gets the type.
		/// </summary>
		public CellType Type
		{
			get
			{
				return CellType.OBJECTREF;
			}
		}

		#endregion

		#region Public Methods and Operators

		/// <summary>
		/// The drop.
		/// </summary>
		/// <param name="wn">
		/// The wn.
		/// </param>
		public void Drop(WaveNode wn)
		{
			if (!this.m_objectInstance.IsReadOnly)
			{
				this.SetSoundReferenceToNewWaveWrapperSound(wn);
			}
		}

		/// <summary>
		/// The drop.
		/// </summary>
		/// <param name="sn">
		/// The sn.
		/// </param>
		public void Drop(SoundNode sn)
		{
			if (!this.m_objectInstance.IsReadOnly)
			{
				this.SetSoundReference(sn.ObjectInstance);
			}
		}

		/// <summary>
		/// The init.
		/// </summary>
		/// <param name="objectInstance">
		/// The object instance.
		/// </param>
		/// <param name="instanceNode">
		/// The instance node.
		/// </param>
		/// <param name="fieldInstance">
		/// The field instance.
		/// </param>
		/// <param name="fieldAlias">
		/// The field alias.
		/// </param>
		/// <param name="objectTypeToCreate">
		/// The object type to create.
		/// </param>
		public void Init(
			IObjectInstance objectInstance,
			XmlNode instanceNode,
			IFieldDefinitionInstance fieldInstance,
			string fieldAlias,
			string objectTypeToCreate)
		{
			if (objectInstance == null)
			{
				return;
			}
			this.m_object = null;
			this.LinkColor = DefaultValueLinkColor;
			this.VisitedLinkColor = DefaultValueLinkColor;

			this.m_fieldInstance = fieldInstance;
			this.m_instanceNode = instanceNode;
			this.m_objectInstance = objectInstance;
			this.m_alias = string.IsNullOrEmpty(fieldAlias) ? fieldInstance.Name : fieldAlias;

			this.Init();

			var menu = new ContextMenuStrip();
			if (objectInstance.GetType() != typeof(ITemplateInstance)
				&& objectInstance.Bank.GetType() != typeof(ITemplateBank))
			{
				this.m_editMenuItem = new ToolStripMenuItem("Edit", null, this.OnEdit);
				menu.Items.Add(this.m_editMenuItem);
			}

			if (!objectInstance.IsReadOnly)
			{
				this.m_removeMenuItem = new ToolStripMenuItem("Remove", null, this.OnRemove);
				menu.Items.Add(this.m_removeMenuItem);

				this.AddAllowedTypeItems(fieldInstance, menu, objectTypeToCreate);
			}

			this.ContextMenuStrip = menu;
			this.ContextMenuStrip.Opening += this.OnContextMenuOpening;
		}

		#endregion

		#region Methods

		/// <summary>
		/// The dispose.
		/// </summary>
		/// <param name="disposing">
		/// The disposing.
		/// </param>
		protected override void Dispose(bool disposing)
		{
			this.m_object = null;
			this.m_instanceNode = null;
			this.m_fieldInstance = null;
			this.m_fieldNode = null;
			if (this.ContextMenuStrip != null)
			{
				this.ContextMenuStrip.Opening -= this.OnContextMenuOpening;
				this.ContextMenuStrip = null;
			}

			base.Dispose(disposing);
		}

		/// <summary>
		/// The on click.
		/// </summary>
		/// <param name="e">
		/// The e.
		/// </param>
		protected override void OnClick(DataGridViewCellEventArgs e)
		{
			base.OnClick(e);
			
			if (!this.m_objectInstance.IsReadOnly && Keyboard.Modifiers == ModifierKeys.Control)
			{
				IEnumerable<string> allowedTypes = null;
				
				if (!string.IsNullOrEmpty(m_fieldInstance.AllowedType))
				{
					foreach (ITypeDefinitions typeDefinitions in RaveInstance.AllTypeDefinitions.Values)
					{
						var typeDefinition = typeDefinitions.FindTypeDefinition(m_fieldInstance.AllowedType);
						if (typeDefinition != null)
						{
							allowedTypes = typeDefinition.GetDescendantTypes().Select(p => p.Name);
							ObjectNameBox.Create(allowedTypes, SetSoundReference);
							break;
						}
					}
				}



				if (allowedTypes == null)
				{

					UIControls.AutoComplete.ObjectNameBox.Create(null, SetSoundReference); ;
				}

				
				return;
			}

			if (this.OnObjectRefClick != null)
			{
				// If object is null, check for default object in type definition
				var obj = this.m_object;
				if (null == obj)
				{
					var defaultObj = this.m_fieldInstance.DefaultValue;
					if (null != defaultObj)
					{
						obj = ObjectInstance.GetObjectMatchingName(defaultObj);
					}
				}

				this.OnObjectRefClick(obj);
			}
		}

		/// <summary>
		/// The remove node.
		/// </summary>
		/// <param name="parent">
		/// The parent.
		/// </param>
		/// <param name="fieldNode">
		/// The field node.
		/// </param>
		/// <returns>
		/// The <see cref="bool"/>.
		/// </returns>
		private static bool RemoveNode(XmlNode parent, XmlNode fieldNode)
		{
			var found = false;
			foreach (XmlNode node in parent.ChildNodes)
			{
				if (node == fieldNode)
				{
					found = true;
				}
			}

			if (found)
			{
				parent.RemoveChild(fieldNode);
				return true;
			}

			return parent.ChildNodes.Cast<XmlNode>().Any(childNode => RemoveNode(childNode, fieldNode));
		}

		/// <summary>
		/// The add allowed type items.
		/// </summary>
		/// <param name="fieldInstance">
		/// The field instance.
		/// </param>
		/// <param name="menu">
		/// The menu.
		/// </param>
		/// <param name="objectTypeToCreate">
		/// The object type to create.
		/// </param>
		private void AddAllowedTypeItems(
			IFieldDefinitionInstance fieldInstance, ToolStrip menu, string objectTypeToCreate)
		{
			var allowedTypes = fieldInstance.TypeDefinition.TypeDefinitions.GetAllowedTypes(fieldInstance.FieldDef);
			if (allowedTypes.Count > 0)
			{
				var subItems =
					allowedTypes.Select(type => new ToolStripMenuItem(type.Name, null, this.OnAddNewObject)).ToArray();
				menu.Items.Add(new ToolStripMenuItem("New", null, subItems));

				if (!string.IsNullOrEmpty(objectTypeToCreate))
				{
					var objectTypeToCreateItem =
						(from subItem in subItems where subItem.Text == objectTypeToCreate select subItem)
							.FirstOrDefault();
					if (objectTypeToCreateItem != null)
					{
						this.OnAddNewObject(objectTypeToCreateItem, null);
					}
				}
			}
		}

		/// <summary>
		/// The init.
		/// </summary>
		private void Init()
		{
			if (m_instanceNode == null)
			{
				return;
			}
			this.m_fieldNode = this.m_instanceNode.Name != this.m_fieldInstance.Name
								   ? PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_instanceNode)
								   : this.m_instanceNode;

			if (this.m_fieldNode != null)
			{
				foreach (var sr in this.m_objectInstance.References)
				{
					if (sr.Node.InnerText == this.m_fieldNode.InnerText)
					{
						this.m_object = sr.ObjectInstance;
						break;
					}
				}
			}

			if (null != this.m_object)
			{
				this.Value = this.m_object.Name;

				if (this.m_object.Name == this.m_fieldInstance.DefaultValue)
				{
					this.LinkColor = DefaultValueLinkColor;
					this.VisitedLinkColor = DefaultValueLinkColor;
				}
				else
				{
					this.LinkColor = this.customValueLinkColor;
					this.VisitedLinkColor = this.customValueLinkColor;
				}
			}
			else
			{
				string name = m_fieldNode != null ? m_fieldNode.InnerXml + " (Invalid Reference)" : "null";

				this.Value = !string.IsNullOrEmpty(this.m_fieldInstance.DefaultValue)
					? this.m_fieldInstance.DefaultValue
					: name;

				if (m_fieldNode == null || !string.IsNullOrEmpty(this.m_fieldInstance.DefaultValue))
				{
					this.LinkColor = DefaultValueLinkColor;
					this.VisitedLinkColor = DefaultValueLinkColor;
				}
				else
				{
					this.LinkColor = Color.Red;
					this.VisitedLinkColor = Color.Red;
				}


			}
		}

		/// <summary>
		/// The on add new object.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void OnAddNewObject(object sender, EventArgs e)
		{
			var menuItem = sender as ToolStripMenuItem;
			if (menuItem != null)
			{
				frmSimpleInput.InputResult input = frmSimpleInput.GetInput(
					string.Format("Enter a name for the new {0}", menuItem.Text), this.m_objectInstance.Name + "_");
				if (input.HasBeenCancelled) return;
				string newSoundName = input.data;

				while (ObjectInstance.CheckForNameAndHashCollision(newSoundName))
				{
					input = frmSimpleInput.GetInput(
						"That name is already in use or hash collision. Enter another name", newSoundName);
					if (input.HasBeenCancelled) return;
					newSoundName = input.data;
				}

				var newSound = TypeUtils.GenerateNewObjectInstance(
					this.m_objectInstance.Bank,
					newSoundName,
					this.m_objectInstance.VirtualFolderName,
					this.m_objectInstance.ObjectLookup,
					this.m_fieldInstance.TypeDefinition.TypeDefinitions.FindTypeDefinition(menuItem.Text));
				if (newSound != null)
				{
					this.SetSoundReference(newSound);
				}
			}
		}

		/// <summary>
		/// The on context menu opening.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void OnContextMenuOpening(object sender, CancelEventArgs e)
		{
			if (this.ContextMenuStrip.Items.Count == 0)
			{
				e.Cancel = true;
				return;
			}

			var enableObjectOperations = this.m_object != null;
			if (this.m_editMenuItem != null)
			{
				this.m_editMenuItem.Visible = enableObjectOperations;
			}

			if (this.m_removeMenuItem != null)
			{
				this.m_removeMenuItem.Visible = enableObjectOperations;
			}
		}

		/// <summary>
		/// The on edit.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void OnEdit(object sender, EventArgs e)
		{
			if (this.OnObjectEditClick != null)
			{
				this.OnObjectEditClick(this.m_object);
			}
		}

		/// <summary>
		/// The on remove.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void OnRemove(object sender, EventArgs e)
		{
			this.m_object.RemoveReferencer(this.m_objectInstance);
			this.m_objectInstance.RemoveReference(this.m_object, this.m_fieldNode);
			if (!RemoveNode(this.m_objectInstance.Node, this.m_fieldNode))
			{
				ErrorManager.HandleError("Unable to remove node");
			}

			this.m_objectInstance.MarkAsDirty();
			this.m_object = null;
			this.m_fieldNode = null;

			this.Value = "null";
			this.LinkColor = DefaultValueLinkColor;
			this.LinkColor = DefaultValueLinkColor;
		}

		private void SetSoundReference(string objectName)
		{
			SetSoundReference(ObjectInstance.GetObjectMatchingName(objectName));
		}

		/// <summary>
		/// The set sound reference.
		/// </summary>
		/// <param name="newSound">
		/// The new sound.
		/// </param>
		private void SetSoundReference(IObjectInstance newSound)
		{
			// check if template
			if (this.m_object != null && this.m_objectInstance.Bank.GetType() == typeof(ITemplateBank))
			{
				return;
			}

			if (!ObjectInstance.CheckForAllowedType(newSound, this.m_fieldInstance.FieldDef))
			{
				return;
			}

			// allow sound refs and game objects
			if (this.m_objectInstance.CanAddReferences(newSound))
			{
				var index = int.MaxValue;
				if (this.m_object != null)
				{
					this.m_object.RemoveReferencer(this.m_objectInstance);
					index = this.m_objectInstance.FindReferenceListIndex(this.m_object, this.m_fieldNode);
				}

				this.m_objectInstance.RemoveReference(this.m_object, this.m_fieldNode);

				this.m_object = newSound;
				this.LinkColor = customValueLinkColor;
				this.LinkColor = customValueLinkColor;

				if (this.m_fieldNode == null)
				{
					this.m_fieldNode = this.m_instanceNode.OwnerDocument.CreateElement(this.m_alias);
					this.m_instanceNode.AppendChild(this.m_fieldNode);
				}

				// add at the correct index so it will appear in the correct order in the heirarchy
				if (index != int.MaxValue)
				{
					this.m_objectInstance.AddReference(this.m_object, this.m_fieldNode, index);
				}
				else
				{
					// couldn't find index of child
					this.m_objectInstance.AddReference(this.m_object, this.m_fieldNode);
				}

				this.m_object.AddReferencer(this.m_objectInstance);

				this.m_fieldNode.InnerText = this.m_object.Name;
				this.m_objectInstance.MarkAsDirty();

				this.Value = this.m_object.Name;
			}
			else
			{
				MessageBox.Show("Invalid child");
			}
		}

		/// <summary>
		/// The set sound reference to new wave wrapper sound.
		/// </summary>
		/// <param name="wave">
		/// The wave.
		/// </param>
		private void SetSoundReferenceToNewWaveWrapperSound(WaveNode wave)
		{
			if (this.m_objectInstance.Bank.Type.ToUpper() != "SOUNDS")
			{
				ErrorManager.HandleInfo("Can only perform this action on sounds");
				return;
			}

			var newSound = TypeUtils.CreateWrapperSound(
				this.m_objectInstance.Bank, Configuration.WaveWrapperSoundType, wave);
			TypeUtils.ReportNewObjectCreated(newSound);
			this.SetSoundReference(newSound);
		}

		#endregion
	}
}