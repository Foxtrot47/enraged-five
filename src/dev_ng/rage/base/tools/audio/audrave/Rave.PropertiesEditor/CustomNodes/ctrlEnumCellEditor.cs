namespace Rave.PropertiesEditor.CustomNodes
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.EnumEntry;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    [DesignerCategory("code")]
    internal class EnumEditor : ComboBox,
                                IDataGridViewEditingControl
    {
        private string m_alias;
        private EnumDefinition m_enumDef;
        private IFieldDefinitionInstance m_fieldInstance;
        private XmlNode m_fieldNode;
        private IObjectInstance m_object;
        private XmlNode m_parentNode;
        private ToolTip m_toolTip;

        #region IDataGridViewEditingControl Members

        public object EditingControlFormattedValue
        {
            get { return this.Text; }
            set
            {
                //set via xml
                return;
            }
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.EditingControlFormattedValue;
        }

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;
            this.BackColor = Color.White;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex { get; set; }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
        {
            return true;
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll)
        {
            //done in init
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged { get; set; }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        #endregion

        public void Init(IObjectInstance objectInstance,
                         XmlNode parentNode,
                         IFieldDefinitionInstance fieldInstance,
                         string fieldAlias)
        {
            this.m_object = objectInstance;
            this.m_parentNode = parentNode;
            this.m_fieldInstance = fieldInstance;
            this.m_alias = fieldAlias;
            this.m_fieldNode = PropertyEditorUtil.FindXmlNode(this.m_alias, this.m_parentNode);

            if (this.m_toolTip == null)
            {
                this.m_toolTip = new ToolTip {AutoPopDelay = 0, InitialDelay = 0, ReshowDelay = 0, ShowAlways = true};
                this.m_toolTip.SetToolTip(this, "");
            }

            this.Items.Clear();
            var al = new ArrayList();
            var simpleField = this.m_fieldInstance.AsSimpleField();

            this.m_enumDef = RaveInstance.AllTypeDefinitions[this.m_object.Type].FindEnumDefinition(simpleField.EnumName);

            if (this.m_enumDef != null)
            {
                foreach (var e in this.m_enumDef.Values)
                {
                    al.Add(e);
                }

                al.Sort(new EnumComparer());
                this.Items.AddRange(al.ToArray());
            }
            if (this.m_fieldNode != null)
            {
                this.SelectedItem = this.m_enumDef.FindEnumFromValue(this.m_fieldNode.InnerText);
                this.Text = this.SelectedItem.ToString();
            }
            else
            {
                var e = this.m_enumDef.FindEnumFromValue(this.m_fieldInstance.DefaultValue);
                this.SelectedItem = e;
                if (e != null)
                {
                    this.m_toolTip.SetToolTip(this, e.Description);
                    this.Text = e.DisplayName;
                }
                else
                {
                    this.Text = "";
                }
            }
        }

        public void UpdateNode()
        {
            if (this.SelectedItem != null)
            {
                var ev = (EnumValueEntry) this.SelectedItem;
                if (this.m_fieldNode == null)
                {
                    this.m_fieldNode = this.m_parentNode.OwnerDocument.CreateElement(this.m_alias);
                    this.m_parentNode.AppendChild(this.m_fieldNode);
                }
                this.m_fieldNode.InnerText = ev.ValueName;
                this.m_object.MarkAsDirty();
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.UpdateNode();
                return false;
            }
            if (keyData == (Keys.Control | Keys.C) && !string.IsNullOrEmpty(this.Text))
            {
                Clipboard.SetText(this.Text);
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        protected override void OnSelectedItemChanged(EventArgs e)
        {
            var ev = (EnumValueEntry) this.SelectedItem;
            if (ev != null)
            {
                this.UpdateNode();
                this.m_toolTip.SetToolTip(this, ev.Description);
            }
            base.OnSelectedItemChanged(e);
        }

        protected override void OnTextChanged(EventArgs e)
        {
            var ev = (EnumValueEntry)this.SelectedItem;
            if (ev != null)
            {
                this.UpdateNode();
                this.m_toolTip.SetToolTip(this, ev.Description);
            }
            base.OnTextChanged(e);
        }

        protected override void OnDropDownClosed(EventArgs e)
        {
            base.OnDropDownClosed(e);
            this.UpdateNode();
            base.OnTextChanged(e);
        }

        #region Nested type: EnumComparer

        public class EnumComparer : IComparer
        {
            #region IComparer Members

            public int Compare(object x, object y)
            {
                return String.Compare(((EnumValueEntry) x).DisplayName, ((EnumValueEntry) y).DisplayName);
            }

            #endregion
        }

        #endregion
    }
}