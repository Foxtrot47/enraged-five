// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TemplateInstance.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The template instance.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Rave.Types.Objects.Templates
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Structs;
    using Rave.Utils;

    /// <summary>
    /// The template instance.
    /// </summary>
    public class TemplateInstance : ObjectInstance, ITemplateInstance
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateInstance"/> class.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="episode">
        /// The episode.
        /// </param>
        /// <param name="objectLookup">
        /// The object lookup.
        /// </param>
        /// <param name="typeDefinitions">
        /// The type definitions.
        /// </param>
        /// <param name="isDummy">
        /// The is dummy flag.
        /// </param>
        public TemplateInstance(
            ITemplate template, 
            XmlNode node, 
            IObjectBank bank, 
            string type, 
            string episode, 
            IDictionary<string, IObjectInstance> objectLookup, 
            ITypeDefinitions typeDefinitions,
            bool isDummy = false)
            : base(node, bank, type, episode, objectLookup, typeDefinitions)
        {
            this.Template = template;
            this.IsDummy = isDummy;

            if (!isDummy)
            {
                this.Template.TemplateBank.BankStatusChanged += this.OnBankStatusChanged;
                this.Template.AddReferencer(this);
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the template.
        /// </summary>
        public ITemplate Template { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this is a dummy template instance.
        /// </summary>
        public bool IsDummy { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Delete the object instance.
        /// </summary>
        public override void Delete()
        {
            if (null != this.Template)
            {
                this.Template.RemoveReferencer(this);
            }

            base.Delete();
        }

        /// <summary>
        /// Get a list of defined variables.
        /// </summary>
        /// <param name="type">
        /// The variable list type.
        /// </param>
        /// <returns>
        /// List of defined variables <see cref="IList{T}"/>.
        /// </returns>
        public override IList<string> GetDefinedVariables(VariableListType type)
        {
            return new List<string>();
        }

        /// <summary>
        /// Get a list of object variables.
        /// </summary>
        /// <param name="fieldName">
        /// The field name to get variables for.
        /// </param>
        /// <param name="type">
        /// The variable list type.
        /// </param>
        /// <returns>
        /// List of object variables <see cref="List{T}"/>.
        /// </returns>
        public override List<string> GetObjectVariables(string fieldName, VariableListType type)
        {
            // Get object variables of template instance (same as object instance)
            var objectVariables = this.GetObjectVariables(this, type);

            objectVariables.AddRange(this.GetTemplateProxyVariables(fieldName, type));

            objectVariables = objectVariables.Distinct().ToList();
            objectVariables.Sort();

            return objectVariables;
        }

        /// <summary>
        /// Remove bank delegate.
        /// </summary>
        public override void RemoveBankDelegate()
        {
            if (!this.IsDummy)
            {
                this.Template.TemplateBank.BankStatusChanged -= this.OnBankStatusChanged;
            }

            base.RemoveBankDelegate();
        }

        /// <summary>
        /// Compute the references.
        /// </summary>
        /// <param name="suppressTypeErrors">
        /// The suppress type errors flag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public override bool ComputeReferences(bool suppressTypeErrors = false)
        {
            this.References = new List<ObjectRef>();

            var exposedFields = this.Template.GetExposedFields();
            foreach (var exposedField in exposedFields)
            {
                foreach (XmlNode childNode in this.Node.ChildNodes)
                {
                    var currNode = exposedField.Key;
                    var fieldDef = exposedField.Value;

                    if (childNode.Name != currNode.Attributes["ExposeAs"].Value ||
                        fieldDef == null || fieldDef.Units != "ObjectRef")
                    {
                        continue;
                    }

                    var reference = new ObjectRef();

                    if (this.ObjectLookup.ContainsKey(childNode.InnerText)
                        && this.ObjectLookup[childNode.InnerText] != this)
                    {
                        reference.ObjectInstance = this.ObjectLookup[childNode.InnerText];
                    }
                    else
                    {
                        // Find allowed reference types
                        var found = false;

                        var baseDefs = new List<string>();
                        if (fieldDef.AllowedType != null)
                        {
                            foreach (var kvp in RaveInstance.AllTypeDefinitions)
                            {
                                if (kvp.Value.FindTypeDefinition(fieldDef.AllowedType) == null)
                                {
                                    continue;
                                }

                                // Found
                                if (!baseDefs.Contains(kvp.Key))
                                {
                                    baseDefs.Add(kvp.Key);
                                }
                            }
                        }
                        else
                        {
                            // Add all types
                            baseDefs.AddRange(
                                Configuration.MetadataTypes.Select(metadataType => metadataType.Type));
                        }

                        Dictionary<string, IObjectInstance> lookUp = null;

                        foreach (var lookupTable in RaveInstance.ObjectLookupTables)
                        {
                            if (baseDefs.Contains(lookupTable.Key.Type)
                                && lookupTable.Value.ContainsKey(childNode.InnerText))
                            {
                                if (lookupTable.Key.Episode == ObjectLookupId.Base
                                    || lookupTable.Key.Episode == this.Episode)
                                {
                                    // exists in base or current episode
                                    found = true;
                                    lookUp = lookupTable.Value;
                                }
                                else if (lookupTable.Key.Episode == this.Episode)
                                {
                                    // override base if that was found first
                                    found = true;
                                    lookUp = lookupTable.Value;
                                }
                            }

                            if (found)
                            {
                                break;
                            }
                        }

                        if (found && lookUp != null)
                        {
                            reference.ObjectInstance = lookUp[childNode.InnerText];
                        }
                        else
                        {
                            // Invalid object ref
                            if (childNode.InnerText != string.Empty)
                            {
                                System.Diagnostics.Debug.WriteLine(
                                    "Warning: invalid object ref: " + childNode.InnerText + " from " + this.Type
                                    + " object " + this.Name + ", node: " + childNode.Name);
                            }

                            reference.ObjectInstance = null;
                        }
                    }

                    reference.Node = childNode;
                    this.References.Add(reference);

                    if (null != reference.ObjectInstance)
                    {
                        reference.ObjectInstance.AddReferencer(this);
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Handle a dropped object.
        /// </summary>
        /// <param name="droppedObject">
        /// The dropped object.
        /// </param>
        /// <param name="globalRaveTypeDefinitions">
        /// The global rave type definitions.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public override bool HandleDroppedObject(
            IObjectInstance droppedObject, ITypeDefinitions globalRaveTypeDefinitions)
        {
            ErrorManager.HandleInfo("Template instances must be configured via the properties editor");
            return false;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// The on bank status changed.
        /// </summary>
        protected override void OnBankStatusChanged()
        {
            if (Templates.Template.TemplateLookupTables[this.Type].ContainsKey(this.Template.Name))
            {
                this.Template = Templates.Template.TemplateLookupTables[this.Type][this.Template.Name];
                this.Template.AddReferencer(this);
            }
            else
            {
                this.Template = null;
            }

            base.OnBankStatusChanged();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get object variables from a proxy object with exposed field matching field alias.
        /// </summary>
        /// <param name="fieldAlias">
        /// The field alias.
        /// </param>
        /// <param name="type">
        /// The variable list type.
        /// </param>
        /// <returns>
        /// Template proxy variables <see cref="IEnumerable{T}"/>.
        /// </returns>
        private IList<string> GetTemplateProxyVariables(string fieldAlias, VariableListType type)
        {
            // Find proxy object containing specified field name
            ITemplateProxyObject proxyObject = null;
            foreach (var obj in this.Template.ProxyObjects)
            {
                if (!obj.ExposesFieldAs(fieldAlias))
                {
                    continue;
                }

                proxyObject = obj;
                break;
            }

            var variables = new List<string>();

            // Add any variables in hierarchy of proxy object
            var curr = proxyObject;
            while (null != curr)
            {
                variables.AddRange(this.GetTemplateProxyVariables(curr, type));
                curr = curr.Parent;
            }

            return variables;
        }

        /// <summary>
        /// Get object variables from a proxy object.
        /// </summary>
        /// <param name="proxyObject">
        /// The proxy object.
        /// </param>
        /// <param name="type">
        /// The variable list type.
        /// </param>
        /// <returns>
        /// Template proxy variables <see cref="IEnumerable{T}"/>.
        /// </returns>
        private IList<string> GetTemplateProxyVariables(ITemplateProxyObject proxyObject, VariableListType type)
        {
            var variables = new List<string>();

            if (null == proxyObject)
            {
                return variables;
            }

            var fields = proxyObject.TypeDefinition.Fields;
            variables.AddRange(this.GetDefinedVariables(proxyObject.Node, fields, type));
            return variables.Distinct().ToList();
        } 

        #endregion
    }
}