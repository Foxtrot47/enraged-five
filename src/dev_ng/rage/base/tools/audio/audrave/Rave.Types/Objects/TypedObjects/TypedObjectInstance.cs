// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypedObjectInstance.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The typed object instance.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.Types.Objects.TypedObjects
{
    using System.Collections.Generic;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;

    /// <summary>
    /// The typed object instance.
    /// </summary>
    public class TypedObjectInstance : ObjectInstance, ITypedObjectInstance
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TypedObjectInstance"/> class.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="episode">
        /// The episode.
        /// </param>
        /// <param name="objectLookup">
        /// The object lookup.
        /// </param>
        /// <param name="typeDefinitions">
        /// The type definitions.
        /// </param>
        public TypedObjectInstance(
            XmlNode node, 
            IObjectBank bank, 
            string type, 
            string episode, 
            IDictionary<string, IObjectInstance> objectLookup, 
            ITypeDefinitions typeDefinitions)
            : base(node, bank, type, episode, objectLookup, typeDefinitions)
        {
            this.TypeDefinition = typeDefinitions.FindTypeDefinition(node.Name);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the type definition.
        /// </summary>
        public ITypeDefinition TypeDefinition { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Populate missing fields.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool PopulateMissingFields()
        {
            var typedef = this.TypeDefinitions.FindTypeDefinition(this.TypeName);
            return typedef != null && this.PopulateMissingFields(typedef.Fields, this.Node);
        }

        /// <summary>
        /// Get a list of defined variables.
        /// </summary>
        /// <param name="type">
        /// The variable list type.
        /// </param>
        /// <returns>
        /// List of defined variables <see cref="IList{T}"/>.
        /// </returns>
        public override IList<string> GetDefinedVariables(VariableListType type)
        {
            return this.GetDefinedVariables(this.Node, this.TypeDefinition.Fields, type);
        }

        /// <summary>
        /// Get a list of object variables.
        /// </summary>
        /// <param name="fieldName">
        /// The field name to get variables for.
        /// </param>
        /// <param name="type">
        /// The variable list type.
        /// </param>
        /// <returns>
        /// List of object variables <see cref="List{T}"/>.
        /// </returns>
        public override List<string> GetObjectVariables(string fieldName, VariableListType type)
        {
            var objectVariables = this.GetObjectVariables(this, type);
            objectVariables.Sort();
            return objectVariables;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Populate missing fields.
        /// Populates any missing fields with default values from the object type definition.
        /// Used to prevent compile warnings after extending object type definitions.
        /// </summary>
        /// <param name="fields">
        /// The fields.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool PopulateMissingFields(IEnumerable<IFieldDefinition> fields, XmlNode node)
        {
            var dirty = false;
            if (node != null)
            {
                foreach (var definition in fields)
                {
                    var fieldNode = this.FindXmlNode(definition.Name, node);
                    if (fieldNode == null)
                    {
                        dirty = true;
                        fieldNode = definition.GenerateDefaultXmlNode(node);
                    }

                    var cfd = definition as ICompositeFieldDefinition;
                    if (cfd == null)
                    {
                        continue;
                    }

                    if (cfd.MaxOccurs == 1)
                    {
                        this.PopulateMissingFields(cfd.Fields, fieldNode);
                    }
                    else
                    {
                        // Check every instance of this repeating composite field
                        foreach (XmlNode childNode in node.ChildNodes)
                        {
                            if (childNode.Name ==
                                cfd.Name)
                            {
                                this.PopulateMissingFields(cfd.Fields, childNode);
                            }
                        }
                    }
                }

                if (dirty)
                {
                    this.MarkAsDirty();
                }
            }

            return true;
        }

        #endregion
    }
}