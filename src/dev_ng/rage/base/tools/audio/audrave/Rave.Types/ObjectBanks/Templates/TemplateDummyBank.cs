﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TemplateDummyBank.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Dummy bank for temporary template instances.
//   Used to edit properties of a template.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.Types.ObjectBanks.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;

    using rage;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    using Rockstar.AssetManager.Infrastructure.Data;
    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Dummy bank for temporary template instances.
    /// Used to edit properties of a template.
    /// </summary>
    public class TemplateDummyBank : ITemplateDummyBank
    {
        /// <summary>
        /// The template bank.
        /// </summary>
        private readonly ITemplateBank templateBank;

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateDummyBank"/> class.
        /// </summary>
        /// <param name="templateBank">
        /// The template Bank.
        /// </param>
        public TemplateDummyBank(ITemplateBank templateBank)
        {
            this.templateBank = templateBank;
            this.ObjectInstances = new List<IObjectInstance>();
        }

        #region Public Events

        /// <summary>
        /// The bank deleted.
        /// </summary>
        public event Action<IXmlBank> BankDeleted;

        /// <summary>
        /// The bank saved.
        /// </summary>
        public event Action BankSaved;

        /// <summary>
        /// The bank status changed.
        /// </summary>
        public event Action BankStatusChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the asset.
        /// </summary>
        public IAsset Asset
        {
            get
            {
                return this.templateBank.Asset;
            }
        }

        /// <summary>
        /// Gets the document.
        /// </summary>
        public XmlDocument Document
        {
            get
            {
                return this.templateBank.Document;
            }
        }

        /// <summary>
        /// Gets the episode.
        /// </summary>
        public string Episode
        {
            get
            {
                // Templates always in BASE episode
                return "BASE";
            }
        }

        /// <summary>
        /// Gets the file path.
        /// </summary>
        public string FilePath
        {
            get
            {
                return this.templateBank.FilePath;
            }
        }

        /// <summary>
        /// Gets a value indicating whether is auto generated.
        /// </summary>
        public bool IsAutoGenerated
        {
            get
            {
                // Template never auto-generated
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether is checked out.
        /// </summary>
        public bool IsCheckedOut
        {
            get
            {
                return this.templateBank.IsCheckedOut;
            }
        }

        /// <summary>
        /// Gets a value indicating whether is dirty.
        /// </summary>
        public bool IsDirty
        {
            get
            {
                return this.templateBank.IsDirty;
            }
        }

        /// <summary>
        /// Gets a value indicating whether is local checkout.
        /// </summary>
        public bool IsLocalCheckout
        {
            get
            {
                return this.templateBank.IsLocalCheckout;
            }
        }

        /// <summary>
        /// Gets the linked metadata files.
        /// </summary>
        public IEnumerable<audMetadataFile> LinkedMetadataFiles
        {
            get
            {
                return this.templateBank.LinkedMetadataFiles;
            }
        }

        /// <summary>
        /// Gets the metadata file.
        /// </summary>
        public audMetadataFile MetadataFile
        {
            get
            {
                return this.templateBank.MetadataFile;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.templateBank.Name;
            }
        }

        /// <summary>
        /// Gets the object instances.
        /// </summary>
        public IList<IObjectInstance> ObjectInstances { get; private set; }

        /// <summary>
        /// Gets the object lookup.
        /// </summary>
        public IDictionary<string, IObjectInstance> ObjectLookup { get; private set; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        public string Type
        {
            get
            {
                return this.templateBank.Type;
            }
        }

        /// <summary>
        /// Gets the type definitions.
        /// </summary>
        public ITypeDefinitions TypeDefinitions
        {
            get
            {
                return this.templateBank.TypeDefinitions;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The checkout.
        /// </summary>
        /// <param name="isLocal">
        /// The is local.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Checkout(bool isLocal)
        {
            return this.templateBank.Checkout(isLocal);
        }

        /// <summary>
        /// The checkout.
        /// </summary>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        /// <param name="isLocal">
        /// The is local.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Checkout(string changeListNumber, bool isLocal)
        {
            return this.templateBank.Checkout(changeListNumber, isLocal);
        }

        /// <summary>
        /// The reload asset.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ReloadAsset()
        {
            return this.templateBank.ReloadAsset();
        }

        /// <summary>
        /// Lock the bank asset.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Lock()
        {
            return true;
        }

        /// <summary>
        /// The compute references.
        /// </summary>
        public void ComputeReferences()
        {
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="deleteAsset">
        /// The delete asset.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Delete(bool deleteAsset)
        {
            throw new Exception("Can't delete dummy template bank");
        }

        /// <summary>
        /// The delete object instance.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool DeleteObjectInstance(IObjectInstance objectInstance)
        {
            // Not supported
            return true;
        }

        /// <summary>
        /// The find object instance.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="IObjectInstance"/>.
        /// </returns>
        public IObjectInstance FindObjectInstance(string name)
        {
            return this.ObjectInstances.FirstOrDefault(objectInstance => objectInstance.Name == name);
        }

        /// <summary>
        /// The get latest.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool GetLatest()
        {
            return this.templateBank.GetLatest();
        }

        /// <summary>
        /// The get locked status.
        /// </summary>
        /// <returns>
        /// The <see cref="FileStatus"/>.
        /// </returns>
        public FileStatus GetLockedStatus()
        {
            return this.templateBank.GetLockedStatus();
        }

        /// <summary>
        /// The load.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Load()
        {
            return this.templateBank.Load();
        }

        /// <summary>
        /// The load checked in version.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool LoadCheckedInVersion()
        {
            return this.templateBank.LoadCheckedInVersion();
        }

        /// <summary>
        /// The load saved version.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool LoadSavedVersion()
        {
            return this.templateBank.LoadSavedVersion();
        }

        /// <summary>
        /// The mark as dirty.
        /// </summary>
        public void MarkAsDirty()
        {
            this.templateBank.MarkAsDirty();
        }

        /// <summary>
        /// The reload.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Reload()
        {
            return this.templateBank.Reload();
        }

        /// <summary>
        /// The remove references.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool RemoveReferences()
        {
            return true;
        }

        /// <summary>
        /// The revert.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Revert()
        {
            return this.templateBank.Revert();
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="localSave">
        /// Local save only (i.e. asset lock not required).
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Save(bool localSave = false)
        {
            return this.templateBank.Save(localSave);
        }

        /// <summary>
        /// The try to checkout.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool TryToCheckout()
        {
            return this.templateBank.TryToCheckout();
        }

        /// <summary>
        /// The validate.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Validate()
        {
            return this.templateBank.Validate();
        }

        #endregion
    }
}