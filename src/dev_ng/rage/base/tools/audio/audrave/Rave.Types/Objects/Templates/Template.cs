// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Template.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The template.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.Types.Objects.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;
    using Rave.Types.Infrastructure.Structs;
    using Rave.Utils;

    /// <summary>
    /// The template.
    /// </summary>
    public class Template : ITemplate
    {
        #region Static Fields

        /// <summary>
        /// The template lookup tables.
        /// </summary>
        public static readonly Dictionary<string, Dictionary<string, ITemplate>> TemplateLookupTables;

        #endregion

        #region Fields

        /// <summary>
        /// The template node.
        /// </summary>
        private readonly XmlNode templateNode;

        /// <summary>
        /// The type definitions.
        /// </summary>
        private readonly ITypeDefinitions typeDefinitions;

        /// <summary>
        /// The template proxy object lookup.
        /// </summary>
        private readonly Dictionary<string, ITemplateProxyObject> proxyObjectLookup;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="Template"/> class.
        /// </summary>
        static Template()
        {
            TemplateLookupTables = new Dictionary<string, Dictionary<string, ITemplate>>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Template"/> class.
        /// </summary>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="templateNode">
        /// The template node.
        /// </param>
        /// <param name="typeDefs">
        /// The type definitions.
        /// </param>
        /// <param name="templateLookup">
        /// The template Lookup.
        /// </param>
        public Template(
            ITemplateBank bank, 
            string type,
            XmlNode templateNode, 
            ITypeDefinitions typeDefs, 
            IDictionary<string, ITemplate> templateLookup) :
            this(null, bank, type, templateNode, null, typeDefs, templateLookup)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Template"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="typeDefs">
        /// The type definitions.
        /// </param>
        /// <param name="templateLookup">
        /// The template Lookup.
        /// </param>
        public Template(
            string name, 
            ITemplateBank bank, 
            string type,
            ITypedObjectInstance objectInstance, 
            ITypeDefinitions typeDefs,
            IDictionary<string, ITemplate> templateLookup) :
            this(name, bank, type, null, objectInstance, typeDefs, templateLookup)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Template"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="templateNode">
        /// The template node.
        /// </param>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="typeDefs">
        /// The type definitions.
        /// </param>
        /// <param name="templateLookup">
        /// The template Lookup.
        /// </param>
        private Template(
            string name, 
            ITemplateBank bank, 
            string type,
            XmlNode templateNode, 
            ITypedObjectInstance objectInstance, 
            ITypeDefinitions typeDefs, 
            IDictionary<string, ITemplate> templateLookup)
        {
            this.TemplateBank = bank;
            this.Type = type;
            this.typeDefinitions = typeDefs;
            this.TemplateLookup = templateLookup;
            this.proxyObjectLookup = new Dictionary<string, ITemplateProxyObject>();
            this.References = new List<ObjectRef>();
            this.Referencers = new List<IReferencable>();

            if (null != templateNode)
            {
                if (templateNode.Attributes == null)
                {
                    throw new Exception("Template node does not have 'name' attribute");
                }

                this.Name = templateNode.Attributes["name"].Value;

                // Initialize template data using template node
                this.templateNode = templateNode;

                this.InitProxyObjectsFromTemplateNode();

                // Set parent proxy object
                if (this.proxyObjectLookup.ContainsKey(this.Name))
                {
                    this.Parent = this.proxyObjectLookup[this.Name];
                }
                else
                {
                    throw new Exception("Failed to find parent proxy object");
                }
            }
            else if (null != objectInstance)
            {
                this.Name = name;

                // Initialize template data using object instance
                var newNode = bank.Document.CreateElement("Template");
                newNode.SetAttribute("name", name);
                this.templateNode = newNode;

                // Initialize the proxy objects for the template
                this.InitProxyObjectsFromObjectInstance(objectInstance, null);

                bank.Document.DocumentElement.AppendChild(this.templateNode);
            }

            // Initialize any remaining data fields
            this.Init();
        }

        #endregion

        #region Events

        /// <summary>
        /// The references changed event.
        /// </summary>
        public event Action ReferencesChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        public string Type { get; private set; }

        /// <summary>
        /// Gets the type name.
        /// </summary>
        public string TypeName
        {
            get
            {
                return "Template";
            }
        }

        /// <summary>
        /// Gets the parent proxy object.
        /// </summary>
        public ITemplateProxyObject Parent { get; private set; }

        /// <summary>
        /// Gets the template bank.
        /// </summary>
        public ITemplateBank TemplateBank { get; private set; }

        /// <summary>
        /// Gets the references.
        /// </summary>
        public IList<ObjectRef> References { get; private set; }

        /// <summary>
        /// Gets the referencers.
        /// </summary>
        public IList<IReferencable> Referencers { get; private set; }

        /// <summary>
        /// Gets the template lookup.
        /// </summary>
        public IDictionary<string, ITemplate> TemplateLookup { get; private set; }

        /// <summary>
        /// Gets the template proxy objects.
        /// </summary>
        public List<ITemplateProxyObject> ProxyObjects
        {
            get
            {
                return this.proxyObjectLookup.Select(kvp => kvp.Value).ToList();
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Compute references.
        /// </summary>
        /// <param name="suppressTypeErrors">
        /// The suppress type errors flag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool ComputeReferences(bool suppressTypeErrors = false)
        {
            // Remove any existing referencer links from this template
            foreach (var reference in this.References)
            {
                if (null != reference.ObjectInstance)
                {
                    reference.ObjectInstance.RemoveReferencer(this);
                }
            }

            this.References = new List<ObjectRef>();

            // Compute references
            this.ComputeReferences(this.Parent, suppressTypeErrors);

            return true;
        }

        /// <summary>
        /// Delete the template.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Delete()
        {
            // Check for any any referencing objects
            if (this.Referencers.Count > 0)
            {
                ErrorManager.HandleInfo(
                    "Other objects reference this template - remove referencing objects before deleting");
                return false;
            }

            var doc = this.TemplateBank.Document.DocumentElement;
            if (doc != null)
            {
                doc.RemoveChild(this.templateNode);
            }

            if (!this.TemplateBank.RemoveTemplate(this))
            {
                return false;
            }

            TemplateLookupTables[this.TemplateBank.Type].Remove(this.Name);
            this.proxyObjectLookup.Clear();
            return true;
        }

        /// <summary>
        /// Get exposed field names.
        /// </summary>
        /// <returns>
        /// Exposed field names <see cref="List{T}"/>.
        /// </returns>
        public List<string> GetExposedFieldNames()
        {
            return this.GetExposedFields().Select(
                kvp => kvp.Key.Attributes != null ? kvp.Key.Attributes["ExposeAs"].Value.ToLower() : null).ToList();
        }

        /// <summary>
        /// Get exposed fields.
        /// </summary>
        /// <returns>
        /// Exposed fields <see cref="Dictionary{TKey,TValue}"/>.
        /// </returns>
        public Dictionary<XmlNode, IFieldDefinition> GetExposedFields()
        {
            var exposed = new Dictionary<XmlNode, IFieldDefinition>();
            foreach (var kvp in this.proxyObjectLookup)
            {
                this.InitExposedNodeFields(kvp.Value.Node, exposed);
            }

            return exposed;
        }

        /// <summary>
        /// Add a reference.
        /// </summary>
        /// <param name="reference">
        /// The reference.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="index">
        /// The index to insert at.
        /// </param>
        public void AddReference(IReferencable reference, XmlNode node, int index = -1)
        {
            // Not used by templates
        }

        /// <summary>
        /// Remove a reference.
        /// </summary>
        /// <param name="reference">
        /// The reference.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        public void RemoveReference(IReferencable reference, XmlNode node)
        {
            // Not used by templates
        }

        /// <summary>
        /// Gets value indicating whether object instance if currently being referenced.
        /// </summary>
        /// <returns>
        /// Indication of being referenced <see cref="bool"/>.
        /// </returns>
        public bool IsReferenced()
        {
            return this.Referencers.Any();
        }

        /// <summary>
        /// Add a referencer.
        /// </summary>
        /// <param name="referencer">
        /// The referencer.
        /// </param>
        public void AddReferencer(IReferencable referencer)
        {
            if (!this.Referencers.Contains(referencer))
            {
                this.Referencers.Add(referencer);
            }
        }

        /// <summary>
        /// Remove a referencer.
        /// </summary>
        /// <param name="referencer">
        /// The referencer.
        /// </param>
        public void RemoveReferencer(IReferencable referencer)
        {
            this.Referencers.Remove(referencer);
        }

        /// <summary>
        /// Remove delegates.
        /// </summary>
        public void RemoveDelegates()
        {
            this.Parent.OnChildrenChanged -= this.ParentChildrenChanged;
        }

        /// <summary>
        /// Mark as dirty.
        /// </summary>
        public void MarkAsDirty()
        {
            // Not used
        }

        /// <summary>
        /// Override ToString to return template name.
        /// </summary>
        /// <returns>
        /// The template name <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Initialize the template.
        /// </summary>
        private void Init()
        {
            if (!TemplateLookupTables.ContainsKey(this.TemplateBank.Type))
            {
                TemplateLookupTables.Add(this.TemplateBank.Type, new Dictionary<string, ITemplate>());
            }

            TemplateLookupTables[this.TemplateBank.Type].Add(this.Name, this);

            this.Parent.OnChildrenChanged += this.ParentChildrenChanged;
        }

        /// <summary>
        /// Initialize proxy objects from template node.
        /// </summary>
        private void InitProxyObjectsFromTemplateNode()
        {
            foreach (XmlNode objectNode in this.templateNode.ChildNodes)
            {
                var obj = new TemplateProxyObject(this, null, this.TemplateBank.Type, objectNode, this.typeDefinitions);
                this.proxyObjectLookup.Add(obj.Name, obj);
            }
        }

        /// <summary>
        /// Initialize proxy objects from object instance.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="parent">
        /// The parent proxy object.
        /// </param>
        private void InitProxyObjectsFromObjectInstance(IObjectInstance objectInstance, ITemplateProxyObject parent)
        {
            if (null != objectInstance as ITemplateInstance)
            {
                throw new Exception("Templates containing template instances are not supported");
            }

            // Don't create proxy objects for object instances of different metadata type
            if (objectInstance.Type != this.Type)
            {
                return;
            }

            // Assume if name already exists, we already have one or more references to same instance
            if (this.proxyObjectLookup.ContainsKey(objectInstance.Name))
            {
                return;
            }

            // Initialize and populate new node
            var newNode = this.TemplateBank.Document.CreateElement(objectInstance.Node.Name);
            if (null == parent)
            {
                newNode.SetAttribute("name", this.Name);
            }
            else
            {
                newNode.SetAttribute("folder", this.Name + "_COMPONENTS");
            }

            if (objectInstance.Node.Attributes == null)
            {
                throw new Exception("Object instance missing required attributes");
            }

            // Set attributes on new node
            foreach (XmlAttribute attribute in objectInstance.Node.Attributes)
            {
                switch (attribute.Name)
                {
                    case "name":
                        if (null != parent)
                        {
                            newNode.SetAttribute(attribute.Name, attribute.Value);
                        }

                        break;
                    case "folder":

                        // do nothing
                        break;
                    default:
                        newNode.SetAttribute(attribute.Name, attribute.Value);
                        break;
                }
            }

            // Copy contents of original node to new node and add to our template node
            XmlNode proxyNode = newNode;
            proxyNode.InnerXml = objectInstance.Node.InnerXml;
            this.templateNode.AppendChild(proxyNode);

            // Create new template proxy object
            var proxyObject = new TemplateProxyObject(this, parent, objectInstance.Type, proxyNode, this.typeDefinitions);
            this.proxyObjectLookup.Add(proxyObject.Name, proxyObject);

            var typeDef = this.typeDefinitions.FindTypeDefinition(proxyNode.Name);
            if (null != typeDef)
            {
                if (null == parent)
                {
                    this.Parent = proxyObject;
                }
                else
                {
                    parent.Children.Add(proxyObject);
                }
            }

            // Initialize proxy objects for all children of current object
            foreach (var child in objectInstance.References.Where(child => child.ObjectInstance != null))
            {
                this.InitProxyObjectsFromObjectInstance(child.ObjectInstance, proxyObject);
            }
        }

        /// <summary>
        /// Initialize the exposed node fields.
        /// </summary>
        /// <param name="objectNode">
        /// The object node.
        /// </param>
        /// <param name="exposed">
        /// The exposed nodes.
        /// </param>
        private void InitExposedNodeFields(XmlNode objectNode, IDictionary<XmlNode, IFieldDefinition> exposed)
        {
            // Ingore text nodes
            if (objectNode.GetType() == typeof(XmlText))
            {
                return;
            }

            // Initialize node fields
            var typeDef = this.typeDefinitions.FindTypeDefinition(objectNode.Name);
            foreach (XmlNode child in objectNode)
            {
                foreach (var fieldDef in typeDef.Fields)
                {
                    this.InitExposedNodeFields(fieldDef, child, exposed);
                }
            }
        }

        /// <summary>
        /// Initialize the exposed node fields.
        /// </summary>
        /// <param name="fieldDef">
        /// The field definition.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="exposed">
        /// The exposed nodes.
        /// </param>
        private void InitExposedNodeFields(IFieldDefinition fieldDef, XmlNode node, IDictionary<XmlNode, IFieldDefinition> exposed)
        {
            if (node.Attributes != null && (fieldDef.Name == node.Name && node.Attributes["ExposeAs"] != null))
            {
                // Add simple field type
                exposed.Add(node, fieldDef);
            }
            else if (fieldDef as ICompositeFieldDefinition != null && fieldDef.Name == node.Name)
            {
                // Add composite field type
                if (node.Attributes != null && node.Attributes["ExposeAs"] != null)
                {
                    exposed.Add(node, fieldDef);
                }
                else
                {
                    // Recursively process fields of child nodes in composite field
                    foreach (XmlNode child in node.ChildNodes)
                    {
                        foreach (var childFieldDef in ((ICompositeFieldDefinition)fieldDef).Fields)
                        {
                            this.InitExposedNodeFields(childFieldDef, child, exposed);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Compute references.
        /// </summary>
        /// <param name="proxyObject">
        /// The proxy object.
        /// </param>
        /// <param name="suppressWarnings">
        /// The suppress warnings.
        /// </param>
        private void ComputeReferences(ITemplateProxyObject proxyObject, bool suppressWarnings)
        {
            // Compute references for current proxy object
            if (!proxyObject.ComputeReferences(suppressWarnings))
            {
                throw new Exception("Failed to initialize template references");
            }

            // Add any referencer link from this template to referenced object instances
            foreach (var externalRef in proxyObject.ExternalReferences)
            {
                var objectInstance = externalRef.Key;
                var node = externalRef.Value;
                var objectRef = new ObjectRef { Node = node, ObjectInstance = objectInstance };
                this.References.Add(objectRef);

                if (null != objectInstance)
                {
                    objectInstance.AddReferencer(this);
                }
            }

            foreach (var childProxyObject in proxyObject.Children)
            {
                this.ComputeReferences(childProxyObject, suppressWarnings);
            }
        }

        /// <summary>
        /// Performs cleanup actions when parent object's children are changed.
        /// </summary>
        private void ParentChildrenChanged()
        {
            // Check if any children has been removed
            var keysToDelete = (
                from kvp in this.proxyObjectLookup 
                where !this.proxyObjectLookup.ContainsKey(kvp.Key) 
                select kvp.Key).ToList();

            // Remove references any deleted children
            foreach (var key in keysToDelete)
            {
                this.templateNode.RemoveChild(this.proxyObjectLookup[key].Node);
                this.proxyObjectLookup.Remove(key);
            }
        }

        #endregion
    }
}