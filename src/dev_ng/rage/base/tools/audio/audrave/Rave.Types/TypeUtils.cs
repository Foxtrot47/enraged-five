﻿// -----------------------------------------------------------------------
// <copyright file="TypeUtils.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Media;

namespace Rave.Types
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Controls.WPF.UXTimer;
    using Rave.Instance;
    using Rave.Metadata.Infrastructure.Interfaces;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Structs;
    using Rave.Types.Objects.Templates;
    using Rave.Types.Objects.TypedObjects;
    using Rave.Utils;
    using Rave.Utils.Popups;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    using rage;
    using rage.Compiler;
    using rage.Enums;
    using rage.ToolLib;

    using BinaryWriter = rage.ToolLib.Writer.BinaryWriter;

    /// <summary>
    /// Type utilities.
    /// </summary>
    public class TypeUtils
    {
        #region Static Fields

        /// <summary>
        /// The ms_lock.
        /// </summary>
        private static readonly ReaderWriterLockSlim ms_lock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        /// <summary>
        /// The ms_is compiling.
        /// </summary>
        private static bool ms_isCompiling;

        #endregion

        /// <summary>
        /// The new object created.
        /// </summary>
        public static event Action<IObjectInstance> NewObjectCreated;

        /// <summary>
        /// Gets or sets the typed object clipboard.
        /// </summary>
        public static IObjectInstance ObjectClipboard { get; set; }

        /// <summary>
        /// Gets or sets the rolloff curves.
        /// </summary>
        public static IObjectBank RolloffCurves { get; set; }

        /// <summary>
        /// The compile metadata in memory.
        /// </summary>
        /// <param name="compiler">
        /// The compiler.
        /// </param>
        /// <param name="objectBanks">
        /// The object banks.
        /// </param>
        /// <param name="metadataFile">
        /// The metadata file.
        /// </param>
        /// /// <param name="writeTagTable">
        /// Indicates whether the tag table gets serialised or not.
        /// </param>
        /// <param name="editMode">
        /// The edit mode.
        /// </param>
        /// <returns>
        /// The <see cref="MemoryStream"/>.
        /// </returns>
        public static MemoryStream CompileMetadataInMemory(
            IXmlBankMetaDataCompiler compiler, List<IXmlBank> objectBanks, audMetadataFile metadataFile, bool writeTagTable, bool editMode = false)
        {
            if (objectBanks == null || objectBanks.Count == 0)
            {
                UXTimer.Instance.AppendNewLine(string.Format("There are no object banks in {0}, Episode:  {1}", metadataFile.Name, metadataFile.Episode), Brushes.OrangeRed);
                return null;
            }

            lock (typeof(TypeUtils))
            {
                var metadataCompiler = compiler.MetadataManagers[objectBanks[0].Type].Compiler;
                var outStream = new MemoryStream();
                try
                {
                    metadataCompiler.LoggedError += HandleMetadataCompileError;
                    metadataCompiler.LoggedException += HandleMetadataCompileWarning;
					
                    using (var busyForm = new frmBusy())
                    {
						if (objectBanks.Count > 1)
						{
		                    busyForm.SetStatusText("Compiling game metadata");
		                    busyForm.SetUnknownDuration(true);
		                    busyForm.Show();
	                    }
	                    var isCompiling = ms_isCompiling = true;

                        var workerThread = new Thread(CompileMetadataInMemoryWorker);
                        workerThread.Start(
                            new CompileRequest
                            {
                                ObjectBanks = objectBanks,
                                MetadataFile = metadataFile,
                                Compiler = metadataCompiler,
                                Stream = outStream,
                                ProjectSettings = compiler.ProjectSettings,
                                WriteTagTable = writeTagTable,
                                EditMode = editMode
                            });
                        while (isCompiling)
                        {
                            Application.DoEvents();
                            Thread.Sleep(50);

                            ms_lock.TryEnterReadLock(Timeout.Infinite);
                            isCompiling = ms_isCompiling;
                            ms_lock.ExitReadLock();
                        }
	                    if (objectBanks.Count > 1)
	                    {
		                    busyForm.Close();
	                    }
                    }
                }
                catch (Exception ex)
                {
                    ErrorManager.HandleException("CompileMetadataInMemory", ex);
                    UXTimer.Instance.AppendNewLine(string.Format("CompileMetadataInMemory - {0}", ex));
                }
                finally
                {
                    metadataCompiler.LoggedError -= HandleMetadataCompileError;
                    metadataCompiler.LoggedException -= HandleMetadataCompileWarning;
                }

                return outStream;
            }
        }

        /// <summary>
        /// The compile metadata in memory worker.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        private static void CompileMetadataInMemoryWorker(object token)
        {
            try
            {
                // assumes all of the object banks contain objects of the same metadata type and from the same metadata file
                var compileRequest = (CompileRequest) token;
                List<DocumentEntry> docEntries = (from bank in compileRequest.ObjectBanks
                                                  select
                                                      new DocumentEntry
                                                      {
                                                          Document =
                                                              rage.ToolLib.Utility.ToXDoc(
                                                                  bank.Document),
                                                          DocumentPath = bank.FilePath
                                                      }).ToList();
                var output = new BinaryWriter(compileRequest.Stream, compileRequest.ProjectSettings.GetCurrentPlatform().IsBigEndian);
                compileRequest.Compiler.Compile(docEntries, output, compileRequest.MetadataFile, 0U, EnumRuntimeMode.Dev,
                        null, compileRequest.EditMode);
                    //(docEntries, output, compileRequest.MetadataFile, 0U,  EnumRuntimeMode.Dev, compileRequest.WriteTagTable, null, compileRequest.EditMode);
            }
            finally
            {
                ms_lock.TryEnterWriteLock(Timeout.Infinite);
                ms_isCompiling = false;
                ms_lock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Handle metadata compile error.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="error">
        /// The error.
        /// </param>
        /// <param name="_">
        /// Not required.
        /// </param>
        private static void HandleMetadataCompileError(string context, string error, bool _)
        {
            ErrorManager.HandleError(context, error, true);
            UXTimer.Instance.AppendNewLine(string.Format("{0} - {1}", context, error));
        }

        /// <summary>
        /// Handle metadata compile warning.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="exception">
        /// The exception.
        /// </param>
        private static void HandleMetadataCompileWarning(string context, Exception exception)
        {
            ErrorManager.HandleException(context, exception);
            UXTimer.Instance.AppendNewLine(string.Format("{0} - {1}", context, exception.Message));
        }

        /// <summary>
        /// The create duplicate sound.
        /// </summary>
        /// <param name="soundToCopy">
        /// The sound to copy.
        /// </param>
        /// <param name="destBank">
        /// The dest bank.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="objectLookupTable">
        /// The object lookup table.
        /// </param>
        /// <returns>
        /// The object instance <see cref="IObjectInstance"/>.
        /// </returns>
        public static IObjectInstance CreateDuplicateSound(
            IObjectInstance soundToCopy, IObjectBank destBank, string name, IDictionary<string, IObjectInstance> objectLookupTable = null)
        {
            if (RaveInstance.ObjectLookupTables[new ObjectLookupId(destBank.Type, destBank.Episode)].ContainsKey(name))
            {
                // prevent duplicated names
                return null;
            }

            XmlNode newNode = soundToCopy.Node.Clone();
            newNode.Attributes["name"].Value = name;
            newNode = destBank.Document.ImportNode(newNode, true);
            destBank.Document.DocumentElement.AppendChild(newNode);
            IObjectInstance newSound;
            var ti = soundToCopy as ITemplateInstance;
            if (ti != null)
            {
                newSound = new TemplateInstance(
                    ti.Template, newNode, destBank, destBank.Type, destBank.Episode, ti.ObjectLookup, ti.TypeDefinitions);
            }
            else
            {
                newSound = new TypedObjectInstance(
                    newNode,
                    destBank,
                    destBank.Type,
                    destBank.Episode,
                    RaveInstance.ObjectLookupTables[new ObjectLookupId(destBank.Type, destBank.Episode)],
                    soundToCopy.TypeDefinitions);
            }

            newSound.MarkAsDirty();

            if (null != objectLookupTable)
            {
                objectLookupTable.Add(newSound.Name, newSound);
            }
            else
            {
                // don't know which lookup so add to member lookup
                newSound.ObjectLookup.Add(newSound.Name, newSound);
            }

            destBank.ObjectInstances.Add(newSound);
            newSound.ComputeReferences();

            return newSound;
        }

        /// <summary>
        /// The generate new object instance.
        /// </summary>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="objectName">
        /// The object name.
        /// </param>
        /// <param name="virtualFolder">
        /// The virtual folder.
        /// </param>
        /// <param name="objectLookup">
        /// The object lookup.
        /// </param>
        /// <param name="typeDef">
        /// The type def.
        /// </param>
        /// <returns>
        /// The <see cref="IObjectInstance"/>.
        /// </returns>
        public static IObjectInstance GenerateNewObjectInstance(
            IObjectBank bank,
            string objectName,
            string virtualFolder,
            IDictionary<string, IObjectInstance> objectLookup,
            ITypeDefinition typeDef)
        {
            var node = typeDef.GenerateDefaultXmlNode(bank.Document, objectName);

            var obj = new TypedObjectInstance(
                node, bank, bank.Type, bank.Episode, objectLookup, typeDef.TypeDefinitions)
            {
                VirtualFolderName
                    =
                    virtualFolder
            };
            obj.MarkAsDirty();
            bank.ObjectInstances.Add(obj);
            bank.Document.DocumentElement.AppendChild(node);
            obj.ObjectLookup.Add(obj.Name, obj);
            obj.ComputeReferences();
            ReportNewObjectCreated(obj);
            return obj;
        }

        /// <summary>
        /// The apply object clipboard to object.
        /// </summary>
        /// <param name="destObject">
        /// The dest object.
        /// </param>
        /// <param name="pasteType">
        /// The paste type.
        /// </param>
        public static void ApplyObjectClipboardToObject(IObjectInstance destObject, string pasteType)
        {
            // overwrite base object fields in destObject with matching base object fields in ObjectClipboard

            // ensure that both objects are same type (sound/curve etc)
            if (ObjectClipboard == null || destObject.Type != ObjectClipboard.Type
                || !destObject.Bank.TryToCheckout())
            {
                return;
            }

            string destName = destObject.Name;
            IObjectBank bank = destObject.Bank;
            destObject = bank.FindObjectInstance(destName);

            // search for the bottom level base type
            ITypeDefinition baseType;
            string inheritsFrom = destObject.TypeName;
            do
            {
                baseType = destObject.TypeDefinitions.FindTypeDefinition(inheritsFrom);
                inheritsFrom = baseType.InheritsFrom;
            }
            while (inheritsFrom != null);

            // copy all fields in baseType from clipboard obj to dest obj
            foreach (IFieldDefinition fd in baseType.Fields)
            {
				if (fd.DisplayGroup != pasteType && pasteType != string.Empty)
                {
                    continue;
                }

                IEnumerable<XmlNode> clipFieldsXml = FindChildXmlNodes(fd.Name, ObjectClipboard.Node);

                //remove all instances of that field from destination
                foreach (XmlElement n in destObject.Node.ChildNodes)
                {
                    if (n.Name == fd.Name)
                    {
                        n.ParentNode.RemoveChild(n);
                    }
                }

                //create all field instances in destination
                foreach (XmlNode clipFieldXml in clipFieldsXml)
                {  
                    XmlElement element = destObject.Node.OwnerDocument.CreateElement(fd.Name);
                    element.InnerXml = clipFieldXml.InnerXml;
                    foreach (XmlAttribute attribute in clipFieldXml.Attributes)
                    {
                        var newAttribute = (XmlAttribute)attribute.Clone();
                        element.SetAttributeNode(newAttribute);
                    }

                    destObject.Node.AppendChild(element);
                }

                
            }
            destObject.ComputeReferences();
            destObject.MarkAsDirty();
        }

        /// <summary>
        /// The find child xml node.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        public static IEnumerable<XmlNode> FindChildXmlNodes(string name, XmlNode parent)
        {
            return parent.ChildNodes.Cast<XmlNode>().Where(n => n.Name == name);
        }

        /// <summary>
        /// The get unique object name.
        /// </summary>
        /// <param name="baseName">
        /// The base name.
        /// </param>
        /// <param name="objectLookup">
        /// The object lookup.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetUniqueObjectName(string baseName, IDictionary<string, IObjectInstance> objectLookup)
        {
            string suffix = string.Empty;
            int i = 1;
            while (objectLookup.ContainsKey(baseName + suffix))
            {
                suffix = "_" + (i++);
            }

            return baseName + suffix;
        }

        /// <summary>
        /// The make reference local.
        /// </summary>
        /// <param name="refToMakeLocal">
        /// The ref to make local.
        /// </param>
        /// <param name="parentObject">
        /// The parent object.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="virtualFolderName">
        /// The virtual folder name.
        /// </param>
        /// <returns>
        /// The <see cref="IObjectInstance"/>.
        /// </returns>
        public static IObjectInstance MakeReferenceLocal(
            ObjectRef refToMakeLocal, IObjectInstance parentObject, string name, string virtualFolderName)
        {
            IObjectInstance s = CreateDuplicateSound(refToMakeLocal.ObjectInstance, parentObject.Bank, name);
            s.VirtualFolderName = virtualFolderName;

            ReportNewObjectCreated(s);
            XmlNode n = refToMakeLocal.Node;
            int index = parentObject.FindReferenceListIndex(refToMakeLocal.ObjectInstance, refToMakeLocal.Node);
            parentObject.RemoveReference(refToMakeLocal.ObjectInstance, refToMakeLocal.Node);

            refToMakeLocal.ObjectInstance.RemoveReferencer(parentObject);
            s.AddReferencer(parentObject);
            n.InnerText = s.Name;

            if (index != int.MaxValue)
            {
                parentObject.AddReference(s, n, index);
            }
            else
            {
                parentObject.AddReference(s, n);
            }

            s.ComputeReferences();
            s.MarkAsDirty();
            return s;
        }

        /// <summary>
        /// The create sound wrapper sound.
        /// </summary>
        /// <param name="soundToWrap">
        /// The sound to wrap.
        /// </param>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <returns>
        /// The <see cref="IObjectInstance"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public static IObjectInstance CreateSoundWrapperObject(IObjectInstance soundToWrap, IObjectBank bank)
        {
            var objectName = GetUniqueObjectName(soundToWrap.Name + "_WRAPPER", soundToWrap.ObjectLookup);

            var sd = RaveInstance.AllTypeDefinitions["SOUNDS"].FindTypeDefinition(Configuration.SoundWrapperSoundType);

            System.Diagnostics.Debug.Assert(sd != null);
            System.Diagnostics.Debug.Assert(sd.ContainsObjectRef);

            XmlNode newNode = sd.GenerateDefaultXmlNode(bank.Document, objectName);

            bool foundNode = false;
            string soundRefFieldName = sd.FindObjectRefFields()[0].Name;
            foreach (XmlNode f in newNode.ChildNodes)
            {
                if (f.Name == soundRefFieldName)
                {
                    f.InnerText = soundToWrap.Name;
                    foundNode = true;
                    break;
                }
            }

            if (!foundNode)
            {
                XmlNode n = bank.Document.CreateElement(soundRefFieldName);
                n.InnerText = soundToWrap.Name;
                newNode.AppendChild(n);
            }

            bank.Document.DocumentElement.AppendChild(newNode);

            var sound = new TypedObjectInstance(
                newNode,
                bank,
                bank.Type,
                bank.Episode,
                soundToWrap.ObjectLookup,
                RaveInstance.AllTypeDefinitions["SOUNDS"]);

            sound.MarkAsDirty();
            sound.ObjectLookup.Add(sound.Name, sound);
            bank.ObjectInstances.Add(sound);
            sound.ComputeReferences();
            return sound;
        }

        /// <summary>
        /// The create wrapper sound.
        /// </summary>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="soundType">
        /// The sound type.
        /// </param>
        /// <param name="wave">
        /// The wave.
        /// </param>
        /// <returns>
        /// The <see cref="IObjectInstance"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public static IObjectInstance CreateWrapperSound(IObjectBank bank, string soundType, WaveNode wave)
        {
            ITypeDefinition sd = RaveInstance.AllTypeDefinitions["SOUNDS"].FindTypeDefinition(soundType);

            var id = new ObjectLookupId(bank.Type, bank.Episode);
            string soundName = GetUniqueObjectName(
                (wave.GetBankPath() + "\\" + wave.GetWaveName()).Replace("\\", "_"),
                RaveInstance.ObjectLookupTables[id]);
            System.Diagnostics.Debug.WriteLine(string.Format("Created new wrapper sound: {0}\n", soundName));
            XmlNode newNode = sd.GenerateDefaultXmlNode(bank.Document, soundName);

            foreach (XmlNode child in newNode.ChildNodes)
            {
                if (RaveUtils.IsWaveReference(child))
                {
                    child.RemoveAll();

                    XmlNode n = child.OwnerDocument.CreateElement("WaveName");
                    n.InnerText = wave.GetWaveName();
                    child.AppendChild(n);

                    n = child.OwnerDocument.CreateElement("BankName");
                    n.InnerText = wave.GetBankPath();
                    child.AppendChild(n);
                    break;
                }
            }

            bank.Document.DocumentElement.AppendChild(newNode);
            var sound = new TypedObjectInstance(
                newNode,
                bank,
                bank.Type,
                bank.Episode,
                RaveInstance.ObjectLookupTables[id],
                RaveInstance.AllTypeDefinitions["SOUNDS"]);
            sound.MarkAsDirty();
            sound.ObjectLookup.Add(sound.Name, sound);
            bank.ObjectInstances.Add(sound);
            sound.ComputeReferences();
            return sound;
        }

        /// <summary>
        /// The move object to bank.
        /// </summary>
        /// <param name="object">
        /// The sound.
        /// </param>
        /// <param name="targetBank">
        /// The target bank.
        /// </param>
        /// <param name="virtualFolderName">
        /// The virtual folder name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool MoveObjectToBank(IObjectInstance objInstance, IObjectBank targetBank, string virtualFolderName)
        {
            var targetId = new ObjectLookupId(targetBank.Type, targetBank.Episode);
            if (objInstance.Episode != targetBank.Episode
                && RaveInstance.ObjectLookupTables[targetId].ContainsKey(objInstance.Name))
            {
                ErrorManager.HandleInfo(
                    "The destination bank already contains an object with the same name - " + objInstance.Name);
                return false;
            }

            var sourceBank = objInstance.Bank;
            var name = objInstance.Name;

            if (!sourceBank.TryToCheckout())
            {
                return false;
            }

            if (!targetBank.TryToCheckout())
            {
                return false;
            }

            objInstance = sourceBank.FindObjectInstance(name);

            sourceBank.ObjectInstances.Remove(objInstance);
            objInstance.ObjectLookup.Remove(name);
            sourceBank.MarkAsDirty();

            objInstance.VirtualFolderName = virtualFolderName;
            objInstance.Bank = targetBank;
            objInstance.Episode = targetBank.Episode;
            objInstance.Type = targetBank.Type.ToUpper();

            targetBank.ObjectInstances.Add(objInstance);
            RaveInstance.ObjectLookupTables[targetId].Add(name, objInstance);
            targetBank.MarkAsDirty();

            // update xml
            objInstance.Node.ParentNode.RemoveChild(objInstance.Node);
            objInstance.Node = targetBank.Document.ImportNode(objInstance.Node, true);
            targetBank.Document.DocumentElement.AppendChild(objInstance.Node);
            objInstance.ComputeReferences(true);
            return true;
        }

        /// <summary>
        /// The copy object to bank.
        /// </summary>
        /// <param name="Object">
        /// The Object.
        /// </param>
        /// <param name="targetBank">
        /// The target bank.
        /// </param>
        /// <param name="newName">
        /// The new Name.
        /// </param>
        /// <param name="virtualFolderName">
        /// The virtual folder name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool CopyObjectToBank(IObjectInstance Object, IObjectBank targetBank, string newName, string virtualFolderName)
        {
            var targetId = new ObjectLookupId(targetBank.Type, targetBank.Episode);
            if (Object.Episode != targetBank.Episode
                && RaveInstance.ObjectLookupTables[targetId].ContainsKey(Object.Name))
            {
                ErrorManager.HandleInfo(
                    "The destination bank already contains a sound with the same name - " + Object.Name);
                return false;
            }

            var sourceBank = Object.Bank;
            var name = Object.Name;

            var sourceBankCheckedOut = sourceBank.IsCheckedOut;

            if (!sourceBank.Checkout(true))
            {
                return false;
            }

            if (!targetBank.TryToCheckout())
            {
                return false;
            }

            Object = sourceBank.FindObjectInstance(name);

            var newObject = CreateDuplicateSound(Object, targetBank, newName, targetBank.ObjectLookup);

            if (null == newObject)
            {
                throw new Exception("Failed to duplicate object");
            }

            newObject.VirtualFolderName = virtualFolderName;

            if (!sourceBankCheckedOut && sourceBank != targetBank)
            {
                sourceBank.Revert();
            }

            return true;
        }

        /// <summary>
        /// The report new object created.
        /// </summary>
        /// <param name="newObject">
        /// The new object.
        /// </param>
        public static void ReportNewObjectCreated(IObjectInstance newObject)
        {
            NewObjectCreated.Raise(newObject);
        }

    }
}
