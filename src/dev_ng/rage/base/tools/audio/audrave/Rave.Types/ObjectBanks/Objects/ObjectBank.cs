﻿// -----------------------------------------------------------------------
// <copyright file="typedXmlBank.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.ObjectBanks.Objects
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Xsl;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Structs;
    using Rave.Types.Objects;
    using Rave.Types.Objects.Templates;
    using Rave.Types.Objects.TypedObjects;
    using Rave.Types.Validation;
    using Rave.Types.Waves;
    using Rave.Utils;

    using Rockstar.AssetManager.Infrastructure.Data;

    /// <summary>
    /// Object Bank.
    /// </summary>
    public class ObjectBank : XmlBank, IObjectBank
    {
        /// <summary>
        /// The error message.
        /// </summary>
        private string errorMessage;

        /// <summary>
        /// The error count.
        /// </summary>
        private uint errorCount;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectBank"/> class.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <param name="episode">
        /// The episode.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="typeDefinitions">
        /// The type definitions.
        /// </param>
        public ObjectBank(string filePath, string episode, string type, ITypeDefinitions typeDefinitions)
            : base(filePath, type)
        {
            var id = new ObjectLookupId(type, episode);
            this.ObjectLookup = RaveInstance.ObjectLookupTables[id];
            this.TypeDefinitions = typeDefinitions;
            this.ObjectInstances = new List<IObjectInstance>();
            this.Episode = episode.ToUpper();

            this.MetadataFile =
                Configuration.MetadataFiles.First(
                x => string.Compare(x.Episode, this.Episode, true) == 0
                    && string.Compare(x.Type, this.Type, true) == 0);

            this.LinkedMetadataFiles = RaveUtils.FindLinkedMetadataFiles(this.MetadataFile);
        }

        #region Properties

        /// <summary>
        /// Gets the object lookup.
        /// </summary>
        public IDictionary<string, IObjectInstance> ObjectLookup { get; private set; }

        /// <summary>
        /// Gets the object instances.
        /// </summary>
        public IList<IObjectInstance> ObjectInstances { get; private set; }

        /// <summary>
        /// Gets the type definitions.
        /// </summary>
        public ITypeDefinitions TypeDefinitions { get; private set; }

        /// <summary>
        /// Gets the episode.
        /// </summary>
        public string Episode { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Compute the references.
        /// </summary>
        public void ComputeReferences()
        {
            var errors = new List<string>();

            foreach (var objectInstance in this.ObjectInstances)
            {
                var typeName = objectInstance.TypeName;
                var typeDefs = objectInstance.TypeDefinitions;
                var typeDef = typeDefs.FindTypeDefinition(typeName);

                if (null == typeDef && !typeName.Equals("TemplateInstance"))
                {
                    errors.Add(objectInstance.Name + " (" + typeName + ")");
                }
            }

            if (errors.Any())
            {
                ErrorManager.HandleError(
                    this.Name + " bank has invalid object reference(s):" +
                    Environment.NewLine + Environment.NewLine +
                    string.Join(Environment.NewLine, errors.ToArray()));
            }

            foreach (var objectInstance in this.ObjectInstances)
            {
                // Find child objects with suppressTypeErrors enabled as
                // we have already alerted user of type errors grouped by
                // bank (above).
                objectInstance.ComputeReferences(true);
            }
        }

        /// <summary>
        /// Validate the bank.
        /// </summary>
        /// <returns>
        /// value indicating if bank is valid <see cref="bool"/>.
        /// </returns>
        public bool Validate()
        {
            if (this.IsAutoGenerated)
            {
                return false;
            }

            var result = new StringBuilder();

            StringBuilder errors;
            StringBuilder warnings;
            if (!this.ValidateObjects(out errors, out warnings))
            {
                result.Append(errors);
                result.Append(warnings);
                ShowValidationFailure(result);
                return false;
            }

            result.Append(errors);
            result.Append(warnings);

            string definitionsPath;
            var schemaPath = Path.GetTempPath() + "schema.xsd";
            var xmlPath = Path.GetTempPath() + "temp.xml";
            
            this.errorMessage = string.Empty;
            this.errorCount = 0;

            var temp = new XmlDocument();
            var root = temp.CreateElement("Objects");
            temp.AppendChild(root);

            this.ParseXml(root, this.Document.DocumentElement);

            // Parse XML causes fields that are no longer used to be removed
            // so we need to save the changes
            if (!this.Save())
            {
                throw new Exception("Failed to save bank: " + this.FilePath);
            }

            var tw = new XmlTextWriter(xmlPath, null) { Formatting = Formatting.Indented };
            temp.WriteTo(tw);
            tw.Flush();
            tw.Close();

            // Get type def paths
            if (this.TypeDefinitions.FilePaths.Length == 1)
            {
                definitionsPath = this.TypeDefinitions.FilePaths[0];
            }
            else
            {
                // Need to concatenate the Type definitions so that we can run
                // the xsl to produce the schema
                var doc = new XmlDocument();

                // Create the common root
                var docRoot = doc.CreateElement("TypeDefinitions");
                doc.AppendChild(docRoot);

                // Copy over all the Type definitions
                foreach (var filePath in this.TypeDefinitions.FilePaths)
                {
                    var typeDef = new XmlDocument();
                    typeDef.Load(filePath);
                    foreach (XmlNode node in typeDef.DocumentElement.ChildNodes)
                    {
                        docRoot.AppendChild(doc.ImportNode(node, true));
                    }
                }

                // Write concatenated xml to a temporary file
                definitionsPath = Path.Combine(Path.GetTempPath(), "TypeDefinitions.xml");
                var writer = new XmlTextWriter(definitionsPath, null) { Formatting = Formatting.Indented };
                doc.WriteContentTo(writer);
                writer.Flush();
                writer.Close();
            }

            try
            {
                // Create schema via xsl
                var transform = new XslCompiledTransform();
                var assembly = Assembly.GetExecutingAssembly();
                var resourceStream = assembly.GetManifestResourceStream("Rave.Types.Validation.xslt");
                var xsltReader = new XmlTextReader(resourceStream);
                transform.Load(xsltReader);
                transform.Transform(definitionsPath, schemaPath);
                xsltReader.Close();

                // Validate the temp (corrected) xml. The temp xml includes a fake parent for repeating fields
                // this fake node allows the xml to comply with a schema (as you can have any number of children in any order)
                var schemaSet = new XmlSchemaSet();
                var schemaReader = new XmlTextReader(schemaPath);
                schemaSet.Add(null, schemaReader);

                var settings = new XmlReaderSettings { ValidationType = ValidationType.Schema, Schemas = schemaSet };
                settings.ValidationEventHandler += this.ValidationEventHandler;
               
                var tr = new XmlTextReader(xmlPath);
                var reader = XmlReader.Create(tr, settings);

                while (reader.Read())
                {
                }

                reader.Close();
                tr.Close();
                schemaReader.Close();

                settings.ValidationEventHandler -= this.ValidationEventHandler;

                // Raise exception, if XML validation fails
                if (this.errorCount > 0)
                {
                    result.AppendLine(this.errorMessage);
                    ShowValidationFailure(result);
                    return false;
                }

                ShowValidationFailure(result);
                return true;
            }
            catch (Exception e)
            {
                result.AppendLine(e.Message);
                ShowValidationFailure(result);
                return false;
            }
        }

        /// <summary>
        /// Show validation failure to user.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        private static void ShowValidationFailure(StringBuilder message)
        {
            if (message.Length > 0)
            {
                var msgToDisplay = message.ToString();
                if (msgToDisplay.Length > 5000)
                {
                    msgToDisplay = msgToDisplay.Substring(0, 5000) + "... (trimmed)";
                }

                ErrorManager.HandleInfo(msgToDisplay);
            }
        }

        /// <summary>
        /// Find object instance with specified name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The object instance <see cref="IObjectInstance"/>.
        /// </returns>
        public IObjectInstance FindObjectInstance(string name)
        {
            return this.ObjectInstances.FirstOrDefault(obj => obj.Name.Equals(name));
        }

        /// <summary>
        /// Delete an object instance.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool DeleteObjectInstance(IObjectInstance objectInstance)
        {
            if (this.IsAutoGenerated)
            {
                return false;
            }

            var nodeToDelete = this.Document.DocumentElement.ChildNodes.Cast<XmlNode>().FirstOrDefault(n => n.Attributes["name"].Value == objectInstance.Name);
            if (null == nodeToDelete)
            {
                ErrorManager.HandleInfo("Tried to delete node that wasn't in bank");
                return false;
            }

            this.Document.DocumentElement.RemoveChild(nodeToDelete);
            this.MarkAsDirty();
            this.ObjectInstances.Remove(objectInstance);
            return true;
        }

        /// <summary>
        /// Remove all references.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool RemoveReferences()
        {
            foreach (var objInstance in this.ObjectInstances)
            {
                if (this.ObjectLookup.ContainsKey(objInstance.Name))
                {
                    foreach (XmlNode childNode in objInstance.Node.ChildNodes)
                    {
                        if (RaveUtils.IsPopulatedWaveReference(childNode))
                        {
                            var key = childNode.ChildNodes[1].InnerText + "\\" + childNode.ChildNodes[0].InnerText;
                            if (WaveManager.WaveRefs.ContainsKey(key))
                            {
                                WaveManager.WaveRefs[key].RemoveAll(x => x.Name == objInstance.Name);
                            }
                        }
                    }

                    this.ObjectLookup.Remove(objInstance.Name);
                }

                foreach (var reference in objInstance.References)
                {
                    if (null != reference.ObjectInstance)
                    {
                        reference.ObjectInstance.RemoveReferencer(objInstance);
                    }
                }

                objInstance.RemoveBankDelegate();
                objInstance.InvalidateNode();
            }

            return true;
        }

        #endregion

        #region Overridden Methods

        /// <summary>
        /// Checkout the bank at a specified change list.
        /// </summary>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        /// <param name="isLocal">
        /// The is local flag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public override bool Checkout(string changeListNumber, bool isLocal)
        {
            return !this.IsAutoGenerated && base.Checkout(changeListNumber, isLocal);
        }

        /// <summary>
        /// Delete the bank.
        /// </summary>
        /// <param name="deleteAsset">
        /// The delete asset.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public override bool Delete(bool deleteAsset)
        {
            if (this.IsAutoGenerated || !base.Delete(deleteAsset))
            {
                return false;
            }

            var objsToDelete = new ArrayList(this.ObjectInstances.ToList());
            foreach (IObjectInstance objectInstance in objsToDelete)
            {
                objectInstance.Delete();
            }

            this.ObjectInstances.Clear();
            this.RaiseBankDeleted(this);

            return true;
        }

        /// <summary>
        /// Get latest version of bank from asset manager.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public override bool GetLatest()
        {
            return this.IsAutoGenerated || base.GetLatest();
        }

        /// <summary>
        /// Get the bank lock status.
        /// </summary>
        /// <returns>
        /// The bank lock status <see cref="FileStatus"/>.
        /// </returns>
        public override FileStatus GetLockedStatus()
        {
            return this.IsAutoGenerated ? null : base.GetLockedStatus();
        }

        /// <summary>
        /// Load the bank.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public override bool Load()
        {
            this.Document = new XmlDocument();
            this.ObjectInstances = new List<IObjectInstance>();

            if (this.IsAutoGenerated)
            {
                // Auto-generated stores don't exist in the asset manager,
                // so safe to always treat them as if checked out
                this.IsCheckedOut = true;
                this.IsLocalCheckout = false;
            }
            else if (RaveInstance.AssetManager.IsCheckedOut(this.FilePath))
            {
                var status = this.GetLockedStatus();
                this.IsLocalCheckout = !status.IsLockedByMe;
                this.IsCheckedOut = status.IsLocked && status.IsLockedByMe;

                this.Asset = null;
                if (this.IsCheckedOut || this.IsLocalCheckout)
                {
                    foreach (var changeList in RaveInstance.AssetManager.ChangeLists.Values)
                    {
                        this.Asset = changeList.GetAsset(this.FilePath);
                        if (this.Asset != null)
                        {
                            break;
                        }
                    }

                    if (!RaveInstance.CheckedOutBanks.Contains(this))
                    {
                        RaveInstance.CheckedOutBanks.Add(this);
                    }
                }
            }
            else
            {
                this.IsCheckedOut = false;
                this.IsLocalCheckout = false;
            }

            var ret = false;
            try
            {
                var id = new ObjectLookupId(this.Type, this.Episode);
                this.Document.Load(this.FilePath);
                var bankRootNode = this.Document.DocumentElement;

                if (!bankRootNode.Name.Equals("Objects"))
                {
                    throw new Exception("Error: Malformed object store XML - " + this.FilePath);
                }

                for (var i = 0; i < bankRootNode.ChildNodes.Count; i++)
                {
                    var childNode = bankRootNode.ChildNodes[i];
                    IObjectInstance newObj = null;
                    if (childNode.Name == "TemplateInstance")
                    {
                        var templateName = childNode.Attributes["template"].Value;

                        if (!Template.TemplateLookupTables[this.Type].ContainsKey(templateName))
                        {
                            var sb = new StringBuilder();
                            sb.Append("Project no longer contains template ");
                            sb.Append(templateName);
                            sb.Append(". \r\nTemplate instance ");
                            sb.Append(childNode.Attributes["name"].Value);
                            sb.Append(" in bank ");
                            sb.Append(this.Name);
                            sb.Append(" will not be created");
                            ErrorManager.HandleError(sb.ToString());
                        }
                        else
                        {
                            newObj = new TemplateInstance(
                                Template.TemplateLookupTables[this.Type][childNode.Attributes["template"].Value],
                                childNode,
                                this,
                                this.Type,
                                this.Episode,
                                RaveInstance.ObjectLookupTables[id],
                                this.TypeDefinitions);
                        }
                    }
                    else
                    {
                        newObj = new TypedObjectInstance(
                            childNode,
                            this,
                            this.Type,
                            this.Episode,
                            RaveInstance.ObjectLookupTables[id],
                            this.TypeDefinitions);
                    }

                    if (newObj == null)
                    {
                        continue;
                    }

                    if (this.ObjectLookup.ContainsKey(newObj.Name))
                    {
                        var obj = this.ObjectLookup[newObj.Name];
                        ErrorManager.HandleError(
                            "Trying to load " + this.Name + "/" + newObj.Name + "\r\n"
                            + "This object name already exists in :" + obj.Bank.Name + "/" + obj.Name);
                    }
                    else
                    {
                        this.ObjectLookup.Add(newObj.Name, newObj);
                        this.ObjectInstances.Add(newObj);
                    }
                }

                ret = true;
                this.IsDirty = false;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                ErrorManager.HandleError(ex);
            }

            return ret;
        }

        /// <summary>
        /// Reload the bank.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public override bool Reload()
        {
            var tempObjects = new List<IObjectInstance>(this.ObjectInstances);

            this.RemoveReferences();

            if (!this.Load())
            {
                return false;
            }

            this.ComputeReferences();

            // Fix up all references 
            // Remove all references to all our objects
            foreach (var obj in tempObjects)
            {
                var referencers = new List<IReferencable>();

                foreach (var referencer in obj.Referencers)
                {
                    referencer.RemoveReferencer(obj);
                    referencers.Add(referencer);
                }

                foreach (var referencer in referencers)
                {
                    referencer.ComputeReferences();

                    var objectInstance = referencer as IObjectInstance;
                    if (null != objectInstance && !this.Name.Contains("Categories"))
                    {
                        ObjectInstance.ReportModifiedObject(objectInstance);
                    }
                }
            }

            //notify about the reload
            foreach (IObjectInstance objectInstance in this.ObjectInstances)
            {
                ObjectInstance.ReportModifiedObject(objectInstance);
            }

            this.RaiseBankStatusChanged();
            return true;
        }

        /// <summary>
        /// Save the bank to file.
        /// </summary>
        /// <param name="localSave">
        /// Local save only (i.e. asset lock not required).
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public override bool Save(bool localSave = false)
        {
            return !this.IsAutoGenerated && base.Save(this.IsLocalCheckout);
        }

        /// <summary>
        /// Try to checkout the bank data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public override bool TryToCheckout()
        {
            return !this.IsAutoGenerated && base.TryToCheckout();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Validate the objects.
        /// </summary>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool ValidateObjects(out StringBuilder errors, out StringBuilder warnings)
        {
            return ValidatorManager.Instance.ValidateObjectBank(this, out errors, out warnings);
        }

        /// <summary>
        /// Parse the XML.
        /// </summary>
        /// <param name="root">
        /// The root.
        /// </param>
        /// <param name="xmlElement">
        /// The xml element.
        /// </param>
        private void ParseXml(XmlElement root, XmlElement xmlElement)
        {
            foreach (XmlNode child in xmlElement.ChildNodes)
            {
                var typeNode = root.OwnerDocument.ImportNode(child, false);
                root.AppendChild(typeNode);
                var typeDef = this.TypeDefinitions.FindTypeDefinition(child.Name);
                if (typeDef != null)
                {
                    this.ParseFields(typeNode, child, typeDef.Fields);
                }
            }
        }

        /// <summary>
        /// Parse the XML fields.
        /// </summary>
        /// <param name="parentNew">
        /// The parent new.
        /// </param>
        /// <param name="parentOriginal">
        /// The parent original.
        /// </param>
        /// <param name="fields">
        /// The fields.
        /// </param>
        private void ParseFields(XmlNode parentNew, XmlNode parentOriginal, IEnumerable<IFieldDefinition> fields)
        {
            var repeatingFields = new Dictionary<string, List<XmlNode>>();
            var nodesToDelete = new List<XmlNode>();

            foreach (XmlNode node in parentOriginal.ChildNodes)
            {
                // Check if field exists
                var fieldDef = fields.FirstOrDefault(f => f.Name == node.Name);
                if (fieldDef != null)
                {
                    var compFieldDef = fieldDef as ICompositeFieldDefinition;
                    if (compFieldDef != null)
                    {
                        var newCompField = parentNew.OwnerDocument.ImportNode(node, false);
                        if (compFieldDef.MaxOccurs == 1)
                        {
                            parentNew.AppendChild(newCompField);
                        }
                        else
                        {
                            if (!repeatingFields.ContainsKey(node.Name))
                            {
                                repeatingFields.Add(node.Name, new List<XmlNode>());
                            }

                            repeatingFields[node.Name].Add(newCompField);
                        }

                        this.ParseFields(newCompField, node, compFieldDef.Fields);
                    }
                    else
                    {
                        parentNew.AppendChild(parentNew.OwnerDocument.ImportNode(node, true));
                    }
                }
                else
                {
                    nodesToDelete.Add(node);
                }
            }

            // Add repeating fiels under common parent
            foreach (var kvp in repeatingFields)
            {
                var repeatingRoot = parentNew.OwnerDocument.CreateElement(kvp.Key);
                foreach (var node in kvp.Value)
                {
                    repeatingRoot.AppendChild(node);
                }

                parentNew.AppendChild(repeatingRoot);
            }

            // Strip out no longer used nodes
            foreach (var nodeToDelete in nodesToDelete)
            {
                parentOriginal.RemoveChild(nodeToDelete);
            }
        }

        /// <summary>
        /// Validation event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        private void ValidationEventHandler(object sender, ValidationEventArgs args)
        {
            // Validation error count
            this.errorMessage += args.Message + "\r\n";
            this.errorCount++;
        }

        #endregion
    }
}
