﻿// -----------------------------------------------------------------------
// <copyright file="ValidatorManager.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Validation
{
    using System.Collections.Generic;
    using System.Text;

    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects.Validators;
    using Rave.Types.Infrastructure.Interfaces.Validation;
    using Rave.Types.Objects.Validators;

    /// <summary>
    /// Validator manager.
    /// </summary>
    public class ValidatorManager : IValidatorManager
    {
        /// <summary>
        /// Initializes static members of the <see cref="ValidatorManager"/> class.
        /// </summary>
        static ValidatorManager()
        {
            if (null == Instance)
            {
                Instance = new ValidatorManager();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidatorManager"/> class.
        /// </summary>
        public ValidatorManager()
        {
            this.ObjectValidators = new List<IObjectValidator>();
            this.InitValidators();
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        public static IValidatorManager Instance { get; private set; }

        /// <summary>
        /// Gets the object validators.
        /// </summary>
        public IList<IObjectValidator> ObjectValidators { get; private set; }

        /// <summary>
        /// Validate an object bank.
        /// </summary>
        /// <param name="objectBank">
        /// The object bank.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if bank is valid <see cref="bool"/>.
        /// </returns>
        public bool ValidateObjectBank(IObjectBank objectBank, out StringBuilder errors, out StringBuilder warnings)
        {
            var isValid = true;
            errors = new StringBuilder();
            warnings = new StringBuilder();

            foreach (var validator in this.ObjectValidators)
            {
                StringBuilder innerErrors;
                StringBuilder innerWarnings;
                if (!validator.IsBankValid(objectBank, out innerErrors, out innerWarnings))
                {
                    isValid = false;
                }

                if (innerErrors.Length > 0)
                {
                    errors.Append(innerErrors);
                }

                if (innerWarnings.Length > 0)
                {
                    warnings.Append(innerWarnings);
                }
            }

            return isValid;
        }

        /// <summary>
        /// Validate a list of object banks.
        /// </summary>
        /// <param name="objectBanks">
        /// The object banks.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if all banks are valid <see cref="bool"/>.
        /// </returns>
        public bool ValidateObjectBanks(IList<IObjectBank> objectBanks, out StringBuilder errors, out StringBuilder warnings)
        {
            var isValid = true;
            errors = new StringBuilder();
            warnings = new StringBuilder();

            foreach (var bank in objectBanks)
            {
                StringBuilder innerErrors;
                StringBuilder innerWarnings;
                if (!this.ValidateObjectBank(bank, out innerErrors, out innerWarnings))
                {
                    isValid = false;
                }

                if (innerErrors.Length > 0)
                {
                    errors.Append(innerErrors);
                }

                if (innerWarnings.Length > 0)
                {
                    warnings.Append(innerWarnings);
                }
            }

            return isValid;
        }

        /// <summary>
        /// Initialize the validators.
        /// </summary>
        private void InitValidators()
        {
            this.ObjectValidators.Add(new SoundSetValidator());
            this.ObjectValidators.Add(new ReferenceValidator());
            this.ObjectValidators.Add(new CategoryValidator()); 
            this.ObjectValidators.Add(new ObjectCategoryValidator());
        }
    }
}
