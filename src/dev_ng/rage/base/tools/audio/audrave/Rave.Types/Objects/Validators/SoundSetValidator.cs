﻿// -----------------------------------------------------------------------
// <copyright file="SoundSetValidator.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Objects.Validators
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    /// Sound set object instance validator.
    /// </summary>
    public class SoundSetValidator : BaseValidator
    {
        /// <summary>
        /// The sound set type name.
        /// </summary>
        private const string SoundSetTypeName = "SOUNDSET";

        /// <summary>
        /// The null sound name.
        /// </summary>
        private const string NullSoundName = "NULL_SOUND";

        /// <summary>
        /// The types to check.
        /// </summary>
        private readonly string[] typesToCheck = new[]
        {
            "EnvironmentSound", "SimpleSound", "GranularSound",
            "ModularSynthSound", "SpeechSound", "ExternalStreamSound"
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="SoundSetValidator"/> class.
        /// </summary>
        public SoundSetValidator()
        {
            this.SupportedMetadataTypes = new List<MetadataTypes> { MetadataTypes.SOUNDS };
        }

        /// <summary>
        /// Determine if object is valid.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if object is valid <see cref="bool"/>.
        /// </returns>
        public override bool IsObjectValid(IObjectInstance objectInstance, out StringBuilder errors, out StringBuilder warnings)
        {
            errors = new StringBuilder();
            warnings = new StringBuilder();

            var objType = (MetadataTypes)Enum.Parse(typeof(MetadataTypes), objectInstance.Type, true);

            return !this.SupportedMetadataTypes.Contains(objType)
                || this.IsValidObjectInstance(objectInstance, warnings);
        }

        /// <summary>
        /// Validate an object instance.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if object instance is valid <see cref="bool"/>.
        /// </returns>
        private bool IsValidObjectInstance(IObjectInstance objectInstance, StringBuilder warnings)
        {
            var isValid = true;

            var type = objectInstance.TypeName;
            if (type.ToUpper() == SoundSetTypeName)
            {
                if (!this.IsValidObjectInstanceHelper(objectInstance, warnings, objectInstance.Name))
                {
                    isValid = false;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Helper method to validate an object instance.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <param name="soundSetName">
        /// The sound set name.
        /// </param>
        /// <returns>
        /// Value indicating if object instance is valid <see cref="bool"/>.
        /// </returns>
        private bool IsValidObjectInstanceHelper(IObjectInstance objectInstance, StringBuilder warnings, string soundSetName)
        {
            if (objectInstance.Name.ToUpper() == NullSoundName)
            {
                // We ignore NULL_SOUND objects
                return true;
            }

            var objType = (MetadataTypes)Enum.Parse(typeof(MetadataTypes), objectInstance.Type, true);
            if (!this.SupportedMetadataTypes.Contains(objType))
            {
                return true;
            }

            // Determine if the current object has a "Category" field
            var typeDef = objectInstance.TypeDefinitions.FindTypeDefinition(objectInstance.TypeName);
            var categoryFieldDef = typeDef.FindFieldDefinition("Category");

            var categoryFound = false;

            if (null != categoryFieldDef)
            {
                var categoryNode = objectInstance.Node.SelectSingleNode(categoryFieldDef.Name);
                if (null != categoryNode && !string.IsNullOrWhiteSpace(categoryNode.InnerText))
                {
                    categoryFound = true;
                }
            }

            // Category found, no need to continue searching down this path
            if (categoryFound)
            {
                return true;
            }

            if (!objectInstance.References.Any())
            {
                var childrenToCheck = objectInstance.References.Select(
                    reference => reference.ObjectInstance).Where(
                    child => this.typesToCheck.Contains(child.TypeName)).ToList();

                if (childrenToCheck.Any())
                {
                    // We've hit the end of the path and still not found a category
                    // Warning only, so don't set 'isValid' to false
                    var warningMsg =
                        string.Format(
                            "Object instance '{0}' is child of SoundSet '{1}' but has no category set in hierarchy",
                            objectInstance.Name,
                            soundSetName);
                    warnings.AppendLine(warningMsg);
                }
            }
            else
            {
                // Continue searching down the path of references
                foreach (var reference in objectInstance.References)
                {
                    if (!this.IsValidObjectInstanceHelper(reference.ObjectInstance, warnings, soundSetName))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
