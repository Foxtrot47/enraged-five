﻿// -----------------------------------------------------------------------
// <copyright file="TemplateProxyObject.cs" company="Rockstar North">
//   Template proxy object.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Objects.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Structs;
    using Rave.Utils;

    /// <summary>
    /// Template proxy object.
    /// </summary>
    public class TemplateProxyObject : ITemplateProxyObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateProxyObject"/> class.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="typeDefinitions">
        /// The type Definitions.
        /// </param>
        public TemplateProxyObject(ITemplate template, ITemplateProxyObject parent, string type, XmlNode node, ITypeDefinitions typeDefinitions)
        {
            this.Template = template;
            this.Parent = parent;
            this.Type = type;
            this.Node = node;
            this.Name = node.Attributes["name"].Value;
            this.TypeDefinition = typeDefinitions.FindTypeDefinition(this.Node.Name);
            this.TypeName = this.Node.Name;
            this.Children = new List<ITemplateProxyObject>();
            this.ExternalReferences = new List<KeyValuePair<IObjectInstance, XmlNode>>();
        }

        #region Events

        /// <summary>
        /// The on children changed action.
        /// </summary>
        public event Action OnChildrenChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the template.
        /// </summary>
        public ITemplate Template { get; private set; }

        /// <summary>
        /// Gets or sets the parent template proxy object.
        /// </summary>
        public ITemplateProxyObject Parent { get; set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the node.
        /// </summary>
        public XmlNode Node { get; private set; }

        /// <summary>
        /// Gets the type definition.
        /// </summary>
        public ITypeDefinition TypeDefinition { get; private set; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        public string Type { get; private set; }

        /// <summary>
        /// Gets the type name.
        /// </summary>
        public string TypeName { get; private set; }

        /// <summary>
        /// Gets the children.
        /// </summary>
        public IList<ITemplateProxyObject> Children { get; private set; }

        /// <summary>
        /// Gets the external references.
        /// </summary>
        public IList<KeyValuePair<IObjectInstance, XmlNode>> ExternalReferences { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Compute the references for proxy object.
        /// </summary>
        /// <param name="suppressTypeErrors">
        /// The suppress type errors flag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool ComputeReferences(bool suppressTypeErrors)
        {
            this.Children = new List<ITemplateProxyObject>();
            this.ExternalReferences = new List<KeyValuePair<IObjectInstance, XmlNode>>();
            return this.ComputeReferences(this.Node, this.TypeDefinition.Fields, suppressTypeErrors);
        }

        /// <summary>
        /// Returns value indicating whether proxy object exposes a field
        /// with alias matching specified field alias.
        /// </summary>
        /// <param name="fieldAlias">
        /// The field alias.
        /// </param>
        /// <returns>
        /// Value indicating whether proxy object exposes field <see cref="bool"/>.
        /// </returns>
        public bool ExposesFieldAs(string fieldAlias)
        {
            foreach (XmlNode node in this.Node.ChildNodes)
            {
                if (null == node.Attributes)
                {
                    continue;
                }

                var exposeAsAttr = node.Attributes["ExposeAs"];
                if (null != exposeAsAttr && exposeAsAttr.Value == fieldAlias)
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Compute references.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="fieldDefinitions">
        /// The field definitions.
        /// </param>
        /// <param name="suppressTypeErrors">
        /// The suppress type errors flag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool ComputeReferences(XmlNode node, IList<IFieldDefinition> fieldDefinitions, bool suppressTypeErrors)
        {
            foreach (XmlNode childNode in node.ChildNodes)
            {
                var refName = childNode.InnerText;

                // Only need to process references that aren't empty
                if (string.IsNullOrEmpty(refName))
                {
                    continue;
                }

                var fieldDef = fieldDefinitions.FirstOrDefault(f => f.Name.Equals(childNode.Name));

                if (null == fieldDef)
                {
                    throw new Exception("Failed to find field definition for node: " + childNode.Name);
                }

                if (null != fieldDef.Units && fieldDef.Units.Equals("ObjectRef"))
                {
                    // If we already have an internal reference to a proxy object
                    // with this name then add it as a child
                    var existingProxyObj = this.Template.ProxyObjects.FirstOrDefault(obj => obj.Name.Equals(refName));
                    if (null != existingProxyObj)
                    {
                        this.Children.Add(existingProxyObj);
                        existingProxyObj.Parent = this;
                    }
                    else
                    {
                        // Try to find external reference in lookup table of own type
                        var id = new ObjectLookupId(this.Template.TemplateBank.Type, "BASE");
                        var lookup = RaveInstance.ObjectLookupTables[id];

                        IObjectInstance objInstance;
                        if (!lookup.TryGetValue(refName, out objInstance))
                        {
                            // Not found in lookup table for own type, check all types
                            foreach (var lookupTable in RaveInstance.ObjectLookupTables)
                            {
                                if (lookupTable.Key.Type.Equals(id.Type))
                                {
                                    continue;
                                }

                                if (lookupTable.Value.TryGetValue(refName, out objInstance))
                                {
                                    break;
                                }
                            }
                        }

                        if (null != objInstance)
                        {
                            this.ExternalReferences.Add(new KeyValuePair<IObjectInstance, XmlNode>(objInstance, childNode));
                        }
                        else if (!suppressTypeErrors)
                        {
                            ErrorManager.HandleError("Failed to compute template proxy object ref: " + refName);
                        }
                    }
                }

                // Compute references for children of any composite fields
                var compositeFieldDef = fieldDef as ICompositeFieldDefinition;
                if (null != compositeFieldDef)
                {
                    this.ComputeReferences(childNode, compositeFieldDef.Fields, suppressTypeErrors);
                }
            }

            return true;
        }

        #endregion
    }
}
