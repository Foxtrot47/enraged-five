﻿// -----------------------------------------------------------------------
// <copyright file="ObjectCategoryValidator.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Objects.Validators
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Rave.Instance;
    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Structs;

    /// <summary>
    /// Validate the category property of an object instance.
    /// </summary>
    public class ObjectCategoryValidator : BaseValidator
    {
        /// <summary>
        /// The category property name.
        /// </summary>
        private const string CategoryPropertyName = "Category";

        /// <summary>
        /// The category field def units.
        /// </summary>
        private const string CategoryFieldDefUnits = "CategoryRef";

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectCategoryValidator"/> class.
        /// </summary>
        public ObjectCategoryValidator()
        {
            this.SupportedMetadataTypes = new List<MetadataTypes>
            {
                MetadataTypes.SOUNDS, 
                MetadataTypes.GAMEOBJECTS, 
            };
        }

        /// <summary>
        /// Determine if object is valid.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if object is valid <see cref="bool"/>.
        /// </returns>
        public override bool IsObjectValid(IObjectInstance objectInstance, out StringBuilder errors, out StringBuilder warnings)
        {
            errors = new StringBuilder();
            warnings = new StringBuilder();

            var objType = (MetadataTypes)Enum.Parse(typeof(MetadataTypes), objectInstance.Type, true);

            return !this.SupportedMetadataTypes.Contains(objType)
                || this.IsValidObjectInstance(objectInstance, errors);
        }

        /// <summary>
        /// Validate an object instance.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <returns>
        /// Value indicating if object instance is valid <see cref="bool"/>.
        /// </returns>
        private bool IsValidObjectInstance(IObjectInstance objectInstance, StringBuilder errors)
        {
            // Determine if the object instance has a "Category" field
            var typeDef = objectInstance.TypeDefinitions.FindTypeDefinition(objectInstance.TypeName);

            if (null == typeDef)
            {
                return true;
            }

            var categoryFieldDef = typeDef.FindFieldDefinition(CategoryPropertyName);

            if (null != categoryFieldDef)
            {
                if (string.Compare(categoryFieldDef.Units, CategoryFieldDefUnits, StringComparison.OrdinalIgnoreCase) != 0)
                {
                    // Not the Category field we're looking for
                    return true;
                }

                var categoryNode = objectInstance.Node.SelectSingleNode(categoryFieldDef.Name);
                if (null == categoryNode || string.IsNullOrWhiteSpace(categoryNode.InnerText))
                {
                    // Nothing to check
                    return true;
                }

                var categoryName = categoryNode.InnerText.ToUpper();

                var categoryBase = new ObjectLookupId("CATEGORIES", ObjectLookupId.Base);
                if (RaveInstance.ObjectLookupTables[categoryBase].All(kvp => categoryName != kvp.Key.ToUpper()))
                {
                    errors.AppendLine(string.Format(
                        "Object '{0}' contains an invalid Category value: {1}",
                        objectInstance.Name,
                        categoryNode.InnerText));

                    return false;
                }
            }

            return true;
        }
    }
}
