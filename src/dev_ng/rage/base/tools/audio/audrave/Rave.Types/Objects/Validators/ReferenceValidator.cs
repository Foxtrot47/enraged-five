﻿// -----------------------------------------------------------------------
// <copyright file="ReferenceValidator.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Objects.Validators
{
    using System.Text;

    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Structs;

    /// <summary>
    /// Reference validator.
    /// </summary>
    public class ReferenceValidator : BaseValidator
    {
        /// <summary>
        /// Determine if object is valid.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if object is valid <see cref="bool"/>.
        /// </returns>
        public override bool IsObjectValid(IObjectInstance objectInstance, out StringBuilder errors, out StringBuilder warnings)
        {
            var isValid = true;
            errors = new StringBuilder();
            warnings = new StringBuilder();

            foreach (var reference in objectInstance.References)
            {
                if (reference.ObjectInstance == null
                    || (reference.ObjectInstance.Episode == objectInstance.Episode
                        || reference.ObjectInstance.Episode == ObjectLookupId.Base))
                {
                    continue;
                }

                var errMessage = string.Format(
                    "Object {0} in bank {1} has invalid child {2}",
                    objectInstance.Name,
                    objectInstance.Bank.Name,
                    reference.ObjectInstance.Name);

                errors.AppendLine(errMessage);
                isValid = false;
            }

            return isValid;
        }
    }
}
