﻿// -----------------------------------------------------------------------
// <copyright file="CategoryValidator.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Objects.Validators
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using rage;

    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    /// Category object validator.
    /// </summary>
    public class CategoryValidator : BaseValidator
    {
        /// <summary>
        /// The properties to check.
        /// </summary>
        private readonly string[] propertiesToCheck = { "Solo", "Mute" };

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryValidator"/> class.
        /// </summary>
        public CategoryValidator()
        {
            this.SupportedMetadataTypes = new List<MetadataTypes> { MetadataTypes.CATEGORIES };
        }
        
        /// <summary>
        /// Determine if object is valid.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if object is valid <see cref="bool"/>.
        /// </returns>
        public override bool IsObjectValid(IObjectInstance objectInstance, out StringBuilder errors, out StringBuilder warnings)
        {
            errors = new StringBuilder();
            warnings = new StringBuilder();

            var objType = (MetadataTypes)Enum.Parse(typeof(MetadataTypes), objectInstance.Type, true);

            return !this.SupportedMetadataTypes.Contains(objType)
                || this.IsValidObjectInstance(objectInstance, errors);
        }

        /// <summary>
        /// Validate an object instance.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <returns>
        /// Value indicating if object instance is valid <see cref="bool"/>.
        /// </returns>
        private bool IsValidObjectInstance(IObjectInstance objectInstance, StringBuilder errors)
        {
            var success = true;

            foreach (var property in this.propertiesToCheck)
            {
                var categoryNode = objectInstance.Node.SelectSingleNode(property);
                if (null != categoryNode && !string.IsNullOrWhiteSpace(categoryNode.InnerText))
                {
                    var propertySet = audProjectSettings.ParseBoolValue(categoryNode);
                    if (propertySet)
                    {
                        var error = string.Format(
                            "'{0}' property should not be set on {1} object instance", property, objectInstance.Name);
                        errors.AppendLine(error);

                        success = false;
                    }
                }
            }

            return success;
        }
    }
}
