﻿// -----------------------------------------------------------------------
// <copyright file="VariableListGroup.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.VariableList
{
    using System;
    using System.Collections.Generic;

    using Rave.Types.Infrastructure.Interfaces.VariableList;

    /// <summary>
    /// The variable list group class.
    /// </summary>
    public class VariableListGroup : IVariableListGroup
    {
        /// <summary>
        /// The variable list group label.
        /// </summary>
        private string label;

        /// <summary>
        /// Initializes a new instance of the <see cref="VariableListGroup"/> class.
        /// </summary>
        /// <param name="list">
        /// The variable list (parent).
        /// </param>
        /// <param name="name">
        /// The variable list group name.
        /// </param>
        /// <param name="label">
        /// The variable list group label used for display.
        /// </param>
        /// <param name="parent">
        /// The parent variable list group.
        /// </param>
        /// <param name="includeInPath">
        /// Indicates whether to include group when determining path.
        /// </param>
        public VariableListGroup(IVariableList list, string name, string label, IVariableListGroup parent = null, bool includeInPath = true)
        {
            this.List = list;
            this.Parent = parent;
            if (null == parent)
            {
                this.IsRoot = true;
            }

            this.Name = name;

            this.IncludeInPath = includeInPath;

            this.Label = label;
            this.Groups = new List<IVariableListGroup>();
            this.Items = new List<IVariableListItem>();
        }

        /// <summary>
        /// Gets the variable list.
        /// </summary>
        public IVariableList List { get; private set; }

        /// <summary>
        /// Gets the parent variable list group.
        /// </summary>
        public IVariableListGroup Parent { get; private set; }

        /// <summary>
        /// Gets a value indicating whether group is the root group.
        /// </summary>
        public bool IsRoot { get; private set; }

        /// <summary>
        /// Gets the variable list group name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets or sets the variable list group label used for display.
        /// </summary>
        public string Label
        {
            get
            {
                return this.label ?? this.Name;
            }

            set
            {
                this.label = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether to include group when determining path.
        /// </summary>
        public bool IncludeInPath { get; private set; }

        /// <summary>
        /// Gets the full name path to variable group list.
        /// </summary>
        public string FullNamePath
        {
            get
            {
                if (null != this.Parent && !this.Parent.IsRoot)
                {
                    if (this.IncludeInPath)
                    {
                        var parentPath = this.Parent.FullNamePath;
                        if (!string.IsNullOrEmpty(parentPath))
                        {
                            return parentPath + "." + this.Name;
                        }

                        return this.Name;
                    }

                    return this.Parent.FullNamePath;
                }

                return this.IncludeInPath ? this.Name : null;
            }
        }

        /// <summary>
        /// Gets the full label path to variable group list.
        /// </summary>
        public string FullLabelPath
        {
            get
            {
                if (null != this.Parent && !this.Parent.IsRoot)
                {
                    if (this.IncludeInPath)
                    {
                        var parentPath = this.Parent.FullLabelPath;
                        if (!string.IsNullOrEmpty(parentPath))
                        {
                            return parentPath + "." + this.Label;
                        }

                        return this.Label;
                    }

                    return this.Parent.FullLabelPath;
                }

                return this.IncludeInPath ? this.Label : null;
            }
        }

        /// <summary>
        /// Gets the variable list groups.
        /// </summary>
        public IList<IVariableListGroup> Groups { get; private set; }

        /// <summary>
        /// Gets the variable list group items.
        /// </summary>
        public IList<IVariableListItem> Items { get; private set; }

        /// <summary>
        /// The sub group matching specified name.
        /// </summary>
        /// <param name="label">
        /// The label of the sub group to find.
        /// </param>
        /// <returns>
        /// Sub group matching name (null if not found) <see cref="VariableListGroup"/>.
        /// </returns>
        public IVariableListGroup GetSubGroup(string label)
        {
            foreach (var group in this.Groups)
            {
                if (group.Label.Equals(label))
                {
                    return group;
                }
            }

            return null;
        }

        /// <summary>
        /// Get item matching specified path.
        /// </summary>
        /// <param name="path">
        /// The path to the item to find.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// Item found at specified path (null if not found) <see cref="VariableListItem"/>.
        /// </returns>
        public IVariableListItem GetItem(string path, char delimiter = '.')
        {
            var splitPath = path.Split(delimiter);

            if (splitPath.Length > 1)
            {
                // Find group
                var group = this.GetSubGroup(splitPath[0]);
                var subPath = path.Substring(path.IndexOf('.') + 1);
                return null != group ? group.GetItem(subPath, delimiter) : null;
            }

            foreach (var item in this.Items)
            {
                if (item.Label.Equals(path))
                {
                    return item;
                }
            }

            return null;
        }

        /// <summary>
        /// Add a sub group to the variable list group.
        /// Groups are add to the group/sub-group specified in the path,
        /// which is split by a specified delimiter.
        /// E.g. "my.test.group" will add a group "group" to:
        ///     root
        ///     --- my
        ///         --- test
        ///             --- group
        /// </summary>
        /// <param name="path">
        /// The variable path, split by delimiter.
        /// </param>
        /// <param name="label">
        /// The label use for display.
        /// </param>
        /// <param name="includeInPath">
        /// Indicates whether to include group when determining path.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// The new variable list group <see cref="VariableListGroup"/>.
        /// </returns>
        public IVariableListGroup AddGroup(string path, string label = null, bool includeInPath = true, char delimiter = '.')
        {
            var splitPath = new List<string>(path.Split(delimiter));
            return this.AddGroup(splitPath, label, includeInPath);
        }

        /// <summary>
        /// Add an item to the variable list group.
        /// Items are add to the group/sub-group specified in the path,
        /// which is split by a specified delimiter.
        /// E.g. "my.test.variable" will add a variable "variable" to:
        ///     root
        ///     --- my
        ///         --- test
        /// </summary>
        /// <param name="path">
        /// The variable path, split by delimiter.
        /// </param>
        /// <param name="label">
        /// The variable list item label.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// The new variable list item <see cref="VariableListItem"/>.
        /// </returns>
        public IVariableListItem AddItem(string path, string label = null, char delimiter = '.')
        {
            var splitPath = new List<string>(path.Split(delimiter));
            var item = this.AddItem(splitPath, label);

            // Add full path to flat list.
            if (null != item)
            {
                this.List.AllItems.Add(item);
                this.List.FlatListNames.Add(item.FullNamePath);
                this.List.FlatListLabels.Add(item.FullLabelPath);
            }

            return item;
        }

        /// <summary>
        /// Helper method for adding a new variable list group in a recursive manner.
        /// </summary>
        /// <param name="splitPath">
        /// The path components (i.e. the group structure for new group).
        /// </param>
        /// <param name="label">
        /// The label used for display.
        /// </param>
        /// <param name="includeInPath">
        /// Indicates whether to include group when determining path.
        /// </param>
        /// <returns>
        /// The new variable list group <see cref="VariableListItem"/>.
        /// </returns>
        public IVariableListGroup AddGroup(IList<string> splitPath, string label = null, bool includeInPath = true)
        {
            if (splitPath.Count == 0)
            {
                throw new Exception("Variable list group path must have at least one element");
            }

            var groupName = splitPath[0];
            var groupNameLower = groupName.ToLower();
            var displayLabel = label ?? groupName;

            IVariableListGroup foundGroup = null;

            // See if we already have an existing group with same name
            foreach (var existingGroup in this.Groups)
            {
                if (existingGroup.Name.ToLower().Equals(groupNameLower))
                {
                    foundGroup = existingGroup;
                }
                else if (null != label && existingGroup.Label.Equals(label))
                {
                    foundGroup = existingGroup;
                }
            }

            // We have reached the last group in the list
            if (splitPath.Count == 1)
            {
                // Return the group we have already found
                if (null != foundGroup)
                {
                    return foundGroup;
                }

                // Otherwise, create and return a new group
                var group = new VariableListGroup(this.List, groupName, displayLabel, this, includeInPath);
                this.Groups.Add(group);
                return group;
            }

            // We haven't reached the end of the group path at this point,
            // so continue to process groups in path
            splitPath.RemoveAt(0);

            // Add remainder of group(s) path to existing group if found
            if (null != foundGroup)
            {
                return foundGroup.AddGroup(splitPath);
            }

            // Otherwise, create new group and add remainder of group(s)
            // path to the new group
            var newGroup = new VariableListGroup(this.List, groupName, displayLabel, this, includeInPath);
            this.Groups.Add(newGroup);
            return newGroup.AddGroup(splitPath);
        }

        /// <summary>
        /// Helper method for adding a new variable list item in a recursive manner.
        /// </summary>
        /// <param name="splitPath">
        /// The path components (i.e. the group structure for new item).
        /// </param>
        /// <param name="label">
        /// The variable list item label.
        /// </param>
        /// <returns>
        /// The new variable list item <see cref="VariableListItem"/>.
        /// </returns>
        public IVariableListItem AddItem(IList<string> splitPath, string label)
        {
            if (splitPath.Count == 0)
            {
                throw new Exception("Variable list item path must have at least one element");
            }

            var name = splitPath[0];
            var nameLower = name.ToLower();

            // We have reached the item at the end of the list
            if (splitPath.Count == 1)
            {
                // Return existing item if we find one with matching name
                foreach (var item in this.Items)
                {
                    if (item.Name.ToLower().Equals(nameLower))
                    {
                        return item;
                    }
                }

                // Otherwise, create and return new item
                var newItem = new VariableListItem(name, label, this);
                this.Items.Add(newItem);
                return newItem;
            }

            // We haven't reached the end of the group/item path at this point,
            // so continue to process groups in path
            splitPath.RemoveAt(0);

            // Attempt to find existing group to add item to
            foreach (var group in this.Groups)
            {
                if (group.Name.ToLower().Equals(nameLower))
                {
                    return group.AddItem(splitPath, label);
                }
            }

            // Create new group if not found
            var newGroup = new VariableListGroup(this.List, name, name, this);
            this.Groups.Add(newGroup);
            return newGroup.AddItem(splitPath, label);
        }
    }
}
