﻿// -----------------------------------------------------------------------
// <copyright file="TemplateBank.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.ObjectBanks.Templates
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml;
    using System.Xml.Xsl;

    using Rave.Utils;

    using rage.Compiler;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Objects;
    using Rave.Types.Objects.Templates;

    /// <summary>
    /// Template bank.
    /// </summary>
    public class TemplateBank : XmlBank, ITemplateBank
    {
        /// <summary>
        /// The compiler.
        /// </summary>
        private readonly MetaDataCompiler compiler;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateBank"/> class.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="typeDefinitions">
        /// The type Definitions.
        /// </param>
        /// <param name="compiler">
        /// The compiler.
        /// </param>
        public TemplateBank(string filePath, string type, ITypeDefinitions typeDefinitions, MetaDataCompiler compiler)
            : base(filePath, type)
        {
            this.TypeDefinitions = typeDefinitions;
            this.compiler = compiler;

            this.Document = new XmlDocument();
            this.Templates = new List<ITemplate>();

            if (!Template.TemplateLookupTables.ContainsKey(type))
            {
                Template.TemplateLookupTables.Add(type, new Dictionary<string, ITemplate>());
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the type definitions.
        /// </summary>
        public ITypeDefinitions TypeDefinitions { get; private set; }

        /// <summary>
        /// Gets the templates.
        /// </summary>
        public IList<ITemplate> Templates { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Add a template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        public void AddTemplate(ITemplate template)
        {
            if (!this.Templates.Contains(template))
            {
                this.Templates.Add(template);
            }
        }

        /// <summary>
        /// Remove a template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool RemoveTemplate(ITemplate template)
        {
            if (this.Templates.Remove(template))
            {
                this.MarkAsDirty();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Find a template with specified name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The template, if found <see cref="ITemplate"/>.
        /// </returns>
        public ITemplate FindTemplate(string name)
        {
            return this.Templates.FirstOrDefault(template => template.Name == name);
        }

        /// <summary>
        /// Compute the references.
        /// </summary>
        public void ComputeReferences()
        {
            foreach (var template in this.Templates)
            {
                template.ComputeReferences();
            }
        }

        /// <summary>
        /// Validate the template bank.
        /// </summary>
        /// <returns>
        /// Flag indicating if template bank is valid <see cref="bool"/>.
        /// </returns>
        public bool Validate()
        {
            var output = new StringBuilder();

            // First check for duplicate "Expose As" fields
            var xslt = new XslCompiledTransform();

            var xsltReader =
                new XmlTextReader(
                    Assembly.GetExecutingAssembly().GetManifestResourceStream("Rave.Types.TemplateValidation.xslt"));
            xslt.Load(xsltReader);

            var memoryStream = new MemoryStream();
            xslt.Transform(this.Document, null, memoryStream);

            memoryStream.Position = 1;
            var streamReader = new StreamReader(memoryStream);
            var streamContents = streamReader.ReadToEnd();

            // If any output return
            if (streamContents.Contains("Error"))
            {
                output.Append(streamContents.Substring(streamContents.IndexOf("Error")));
                ShowValidationFailure(output.ToString());
                return false;
            }

            // If no errors check object names
            var templateManager = this.compiler.CreateTemplateManager();
            templateManager.Load(rage.ToolLib.Utility.ToXDoc(this.Document));
            var docs = new Dictionary<string, XmlDocument>();

            foreach (var template in this.Templates)
            {
                foreach (var proxyObject in template.ProxyObjects)
                {
                    var doc = new XmlDocument();
                    doc.AppendChild(doc.CreateElement("Objects"));
                    var templateNode = doc.ImportNode(proxyObject.Node, true);
                    doc.DocumentElement.AppendChild(templateNode);

                    var xdoc = rage.ToolLib.Utility.ToXDoc(doc);
                    templateManager.Process(xdoc);
                    doc = rage.ToolLib.Utility.ToXmlDoc(xdoc);
                    docs.Add(proxyObject.Name, doc);
                }
            }

            var errors = new List<string>();
            foreach (var doc in docs)
            {
                var names = new List<string>();
                foreach (XmlNode node in doc.Value.DocumentElement.ChildNodes)
                {
                    var name = node.Attributes["name"].Value;
                    if (names.Contains(name))
                    {
                        errors.Add("Error: name collision: " + name);
                    }
                    else
                    {
                        var currDoc = doc;
                        errors.AddRange(
                            from kvp in RaveInstance.ObjectLookupTables
                            where kvp.Key.Type == this.Type && 
                                kvp.Value.ContainsKey(name) && 
                                name != currDoc.Key
                            select "Error: name collision: " + name);
                    }
                }
            }

            if (errors.Any())
            {
                ShowValidationFailure(string.Join("\n", errors));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Show validation failure to user.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        private static void ShowValidationFailure(string message)
        {
            if (message.Length > 0)
            {
                ErrorManager.HandleInfo(message);
            }
        }

        /// <summary>
        /// Load the bank.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public override bool Load()
        {
            this.Templates = new List<ITemplate>();

            this.Document = new XmlDocument();
            this.Document.Load(this.FilePath);

            if (RaveInstance.AssetManager.IsCheckedOut(this.FilePath))
            {
                var status = this.GetLockedStatus();
                this.IsLocalCheckout = !status.IsLockedByMe;
                this.IsCheckedOut = status.IsLocked && status.IsLockedByMe;

                this.Asset = null;

                if (this.IsCheckedOut || this.IsLocalCheckout)
                {
                    foreach (var changeList in RaveInstance.AssetManager.ChangeLists.Values)
                    {
                        this.Asset = changeList.GetAsset(this.FilePath);
                        if (this.Asset != null)
                        {
                            break;
                        }
                    }

                    if (!RaveInstance.CheckedOutBanks.Contains(this))
                    {
                        RaveInstance.CheckedOutBanks.Add(this);
                    }
                }
            }
            else
            {
                this.IsCheckedOut = false;
                this.IsLocalCheckout = false;
            }

            foreach (XmlNode templateNode in this.Document.DocumentElement.ChildNodes)
            {
                var lookup = Template.TemplateLookupTables[this.Type];
                var template = new Template(this, this.Type, templateNode, this.TypeDefinitions, lookup);
                this.Templates.Add(template);
            }

            return true;
        }

        /// <summary>
        /// Reload the bank.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public override bool Reload()
        {
            var tempTemplates = new List<ITemplate>(this.Templates);

            this.RemoveReferences();

            foreach (var template in this.Templates)
            {
                template.RemoveDelegates();

                // Update references so they know that they are no long
                // referenced by these proxy objects being removed
                foreach (var proxyObj in template.ProxyObjects)
                {
                    foreach (var externalReference in proxyObj.ExternalReferences)
                    {
                        externalReference.Key.RemoveReferencer(externalReference.Key);
                    }
                }

                Template.TemplateLookupTables[this.Type].Remove(template.Name);
            }

            this.Templates.Clear();

            // Reload the templates data
            if (!this.Load())
            {
                return false;
            }

            this.ComputeReferences();

            // Fix up all references 
            // Remove all references to all our templates
            foreach (var template in tempTemplates)
            {
                var templateInstances = new List<ITemplateInstance>();

                foreach (var referencer in template.Referencers)
                {
                    var templateInstance = referencer as ITemplateInstance;

                    if (null == templateInstance)
                    {
                        throw new Exception("Expected object of type ITemplateInstance");
                    }

                    templateInstance.RemoveReferencer(template);
                    templateInstances.Add(templateInstance);
                }

                foreach (var templateInstance in templateInstances)
                {
                    templateInstance.ComputeReferences();
                    ObjectInstance.ReportModifiedObject(templateInstance);
                }
            }

            this.RaiseBankStatusChanged();
            return true;
        }

        /// <summary>
        /// Remove all references.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool RemoveReferences()
        {
            foreach (var template in this.Templates)
            {
                foreach (var reference in template.References)
                {
                    reference.ObjectInstance.RemoveReferencer(template);
                }
            }

            return true;
        }

        #endregion
    }
}
