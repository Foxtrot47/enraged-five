﻿// -----------------------------------------------------------------------
// <copyright file="VariableListItem.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.VariableList
{
    using Rave.Types.Infrastructure.Interfaces.VariableList;

    /// <summary>
    /// The variable list item class.
    /// </summary>
    public class VariableListItem : IVariableListItem
    {
        /// <summary>
        /// The variable list item label.
        /// </summary>
        private string label;

        /// <summary>
        /// Initializes a new instance of the <see cref="VariableListItem"/> class.
        /// </summary>
        /// <param name="name">
        /// The variable list item name.
        /// </param>
        /// <param name="label">
        /// The variable list item label used for display.
        /// </param>
        /// <param name="group">
        /// The variable list group (parent).
        /// </param>
        /// <param name="tooltip">
        /// The variable list item tooltip.
        /// </param>
        public VariableListItem(string name, string label, VariableListGroup group, string tooltip = null)
        {
            this.Group = group;
            this.Name = name;
            this.Label = label;
            this.Tooltip = tooltip;
        }

        /// <summary>
        /// Gets the variable list group.
        /// </summary>
        public IVariableListGroup Group { get; private set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets or sets the variable list item label used for display.
        /// </summary>
        public string Label
        {
            get
            {
                return this.label ?? this.Name;
            }

            set
            {
                this.label = value;
            }
        }

        /// <summary>
        /// Gets the tooltip.
        /// </summary>
        public string Tooltip { get; private set; }

        /// <summary>
        /// Gets the full name path to variable list item.
        /// </summary>
        public string FullNamePath
        {
            get
            {
                if (null != this.Group && !this.Group.IsRoot)
                {
                    var groupPath = this.Group.FullNamePath;
                    if (!string.IsNullOrEmpty(groupPath))
                    {
                        return groupPath + "." + this.Name;
                    }
                }

                return this.Name;
            }
        }

        /// <summary>
        /// Gets the full label path to variable list item.
        /// </summary>
        public string FullLabelPath
        {
            get
            {
                if (null != this.Group && !this.Group.IsRoot)
                {
                    var groupPath = this.Group.FullLabelPath;
                    if (!string.IsNullOrEmpty(groupPath))
                    {
                        return groupPath + "." + this.Label;
                    }
                }

                return this.Label;
            }
        }
    }
}
