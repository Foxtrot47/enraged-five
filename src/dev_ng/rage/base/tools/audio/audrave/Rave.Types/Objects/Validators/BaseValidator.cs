﻿// -----------------------------------------------------------------------
// <copyright file="BaseValidator.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Objects.Validators
{
    using System.Collections.Generic;
    using System.Text;

    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Validators;

    /// <summary>
    /// Base validator class.
    /// </summary>
    public abstract class BaseValidator : IObjectValidator
    {
        /// <summary>
        /// Gets the supported metadata types.
        /// </summary>
        public IList<MetadataTypes> SupportedMetadataTypes { get; protected set; }

        /// <summary>
        /// Determine if object is valid.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if object is valid <see cref="bool"/>.
        /// </returns>
        public abstract bool IsObjectValid(
            IObjectInstance objectInstance, out StringBuilder errors, out StringBuilder warnings);

        /// <summary>
        /// Determine if objects in bank are valid.
        /// </summary>
        /// <param name="objectBank">
        /// The object bank.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Value indicating if objects in bank are valid <see cref="bool"/>.
        /// </returns>
        public bool IsBankValid(IObjectBank objectBank, out StringBuilder errors, out StringBuilder warnings)
        {
            var isValid = true;
            errors = new StringBuilder();
            warnings = new StringBuilder();

            foreach (var objectInstance in objectBank.ObjectInstances)
            {
                StringBuilder innerErrors;
                StringBuilder innerWarnings;
                if (!this.IsObjectValid(objectInstance, out innerErrors, out innerWarnings))
                {
                    isValid = false;
                }

                if (innerErrors.Length > 0)
                {
                    errors.Append(innerErrors);
                }

                if (innerWarnings.Length > 0)
                {
                    warnings.Append(innerWarnings);
                }
            }

            return isValid;
        }
    }
}
