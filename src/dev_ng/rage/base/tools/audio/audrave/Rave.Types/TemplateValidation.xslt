<?xml version="1.0"
      encoding="utf-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"
              indent="no"
              encoding="utf-8"/>

  <xsl:template match="/">
    <xsl:for-each select="/Templates/Template">
      <xsl:call-template name="sort">
        <xsl:with-param name="nodes"
                        select="current()//node()[@ExposeAs]"/>
        <xsl:with-param name="template"
                        select="current()"/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="sort">
    <xsl:param name="nodes"/>
    <xsl:param name="template"/>
    <xsl:for-each select="$nodes">
      <xsl:variable name="exposeAs"
                    select="@ExposeAs"/>
      <xsl:if test="count($nodes[@ExposeAs = $exposeAs])>1">
        Error: Duplicate "Expose As" Field
        <xsl:value-of select="@ExposeAs"/>
        in:
        <xsl:value-of select="$template/@name"/>
        /
        <xsl:value-of select="local-name(current())"/>
        <xsl:text></xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>