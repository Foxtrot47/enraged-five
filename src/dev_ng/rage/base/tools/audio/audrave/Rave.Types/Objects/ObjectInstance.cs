﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectInstance.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Object instance base class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Rave.Types.Objects
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Popups;

    using rage.ToolLib;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.VariableList;
    using Rave.Types.Infrastructure.Structs;
    using Rave.Types.ObjectBanks;
    using Rave.Types.Waves;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using WPFToolLib.Extensions;

    using MessageBox = System.Windows.MessageBox;
    using System.ComponentModel;

    /// <summary>
    /// Object instance base class.
    /// </summary>
    public class ObjectInstance : IObjectInstance
    {
        #region Static Fields

        /// <summary>
        /// Global input variables.
        /// </summary>
        protected static readonly IList<string> GlobalInputVariables;

        /// <summary>
        /// Global output variables.
        /// </summary>
        protected static readonly IList<string> GlobalOutputVariables;

        #endregion

        #region Fields

        /// <summary>
        /// The name.
        /// </summary>
        private string name;

        /// <summary>
        /// The virtual folder name.
        /// </summary>
        private string virtualFolderName;

        private bool isSoloed;

        private bool isMuted;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectInstance"/> class.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="episode">
        /// The episode.
        /// </param>
        /// <param name="objectLookup">
        /// The object lookup.
        /// </param>
        /// <param name="typeDefinitions">
        /// The type definitions.
        /// </param>
        protected ObjectInstance(
            XmlNode node,
            IObjectBank bank,
            string type,
            string episode,
            IDictionary<string, IObjectInstance> objectLookup,
            ITypeDefinitions typeDefinitions)
        {
            this.Node = node;

            if (null == node)
            {
                var a = ""; // do not submit do not commit
            }

            this.Bank = bank;
            this.Type = type;
            this.Episode = null != episode ? episode.ToUpper() : null;
            this.ObjectLookup = objectLookup;
            this.TypeDefinitions = typeDefinitions;
            this.TypeName = node.Name;
            this.Name = node.Attributes["name"].Value;
            this.References = new List<ObjectRef>();
            this.Referencers = new List<IReferencable>();

            var folderAttr = node.Attributes["folder"];
            this.virtualFolderName = folderAttr != null ? folderAttr.Value : null;

            if (bank != null)
            {
                bank.BankStatusChanged += this.OnBankStatusChanged;
            }

            this.InitWaveRefs();
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The static object modified event.
        /// </summary>
        public static event Action<IObjectInstance> ObjectModified;

        /// <summary>
        /// The references changed event.
        /// </summary>
        public event Action ReferencesChanged;

        /// <summary>
        /// The object status changed event.
        /// </summary>
        public event Action ObjectStatusChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the bank.
        /// </summary>
        public IObjectBank Bank { get; set; }

        /// <summary>
        /// Gets or sets the references.
        /// </summary>
        public IList<ObjectRef> References { get; protected set; }

        /// <summary>
        /// Gets or sets the referencers.
        /// </summary>
        public IList<IReferencable> Referencers { get; protected set; }

        /// <summary>
        /// Gets or sets the episode.
        /// </summary>
        public string Episode { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                var hash = new Hash { Value = this.Name };
                this.NameHash = hash.Key;
                this.PropertyChanged.Raise(this, "Name");
            }
        }

        /// <summary>
        /// Gets the name hash.
        /// </summary>
        public uint NameHash { get; private set; }

        /// <summary>
        /// Gets or sets or sets the node.
        /// </summary>
        public XmlNode Node { get; set; }

        /// <summary>
        /// Gets a value indicating whether is auto generated.
        /// </summary>
        public bool IsAutoGenerated
        {
            get
            {
                return this.Bank.IsAutoGenerated;
            }
        }

        /// <summary>
        /// Gets or sets the object lookup.
        /// </summary>
        public IDictionary<string, IObjectInstance> ObjectLookup { get; protected set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the type definitions.
        /// </summary>
        public ITypeDefinitions TypeDefinitions { get; protected set; }

        /// <summary>
        /// Gets or sets the type name.
        /// </summary>
        public string TypeName { get; protected set; }

        /// <summary>
        /// Gets or sets the virtual folder name.
        /// </summary>
        public string VirtualFolderName
        {
            get
            {
                return this.virtualFolderName;
            }

            set
            {
                this.virtualFolderName = value;
                this.SetVirtualFolder(value);
            }
        }

        /// <summary>
        /// Gets a value indicating whether is read only.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                if (this.Bank == null)
                {
                    return false;
                }

                var isReadOnly = !this.Bank.IsCheckedOut;
                if (isReadOnly && this.Bank.IsLocalCheckout)
                {
                    return false;
                }

                return isReadOnly;
            }
        }

        /// <summary>
        /// Gets a value indicating wheter this object is muted
        /// </summary>
        public bool IsMuted
        {
            get
            {
                return isMuted;
            }
        }

        /// <summary>
        /// Gets a value indicating wheter this object is soloed
        /// </summary>
        public bool IsSoloed
        {
            get
            {
                return isSoloed;
            }
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Get object matching specified name.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// Object matching specified name <see cref="IObjectInstance"/>.
        /// </returns>
        public static IObjectInstance GetObjectMatchingName(string key)
        {
            return (
                from table in RaveInstance.ObjectLookupTables
                from objs in table.Value.Where(
                objs => objs.Key == key)
                select objs.Value).FirstOrDefault();
        }

        /// <summary>
        /// Get object matching specified name.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// Object matching specified name <see cref="IObjectInstance"/>.
        /// </returns>
        public static IObjectInstance[] GetObjectsMatchingName(string key)
        {
            return (
                from table in RaveInstance.ObjectLookupTables
                from objs in table.Value.Where(
                objs => objs.Key == key)
                select objs.Value).ToArray();
        }

        /// <summary>
        /// Gets value indicating whether any object lookup table contains specified value.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="episode">
        /// The episode.
        /// </param>
        /// <returns>
        /// Value indicating whether key was found <see cref="bool"/>.
        /// </returns>
        public static bool CheckForNameAndHashCollision(string key, string episode = "BASE")
        {
	        bool nameHashCollision = (RaveInstance.ObjectLookupTables.SelectMany(table => table.Value)
		        .Any(
			        obj =>
				        (obj.Value.NameHash == (new Hash {Value = key}).Key) &&
						obj.Key != key));

	        bool nameCollision =   RaveInstance.ObjectLookupTables.SelectMany(table => table.Value)
			        .Any(
				        obj =>
					        (obj.Key == key && obj.Value.Episode == episode));
	        return (nameCollision || nameHashCollision);
        }

        /// <summary>
        /// Get an object instance matching specified name hash.
        /// </summary>
        /// <param name="nameHash">
        /// The name hash.
        /// </param>
        /// <returns>
        /// The object instance <see cref="IObjectInstance"/>.
        /// </returns>
        public static IObjectInstance GetObjectInstanceMatchingNameHash(uint nameHash)
        {
            return (from table in RaveInstance.ObjectLookupTables
                    from kvp in table.Value
                    select kvp.Value).FirstOrDefault(
                    objectInstance => objectInstance.NameHash == nameHash);
        }

        /// <summary>
        /// Check for allowed type.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="targetFieldDef">
        /// The target field definition.
        /// </param>
        /// <param name="reportError">
        /// The report error.
        /// </param>
        /// <returns>
        /// Value indicating whether type is allowed <see cref="bool"/>.
        /// </returns>
        public static bool CheckForAllowedType(
            IObjectInstance objectInstance, IFieldDefinition targetFieldDef, bool reportError = true)
        {
            if (targetFieldDef.AllowedType != null)
            {
                var typeName = objectInstance.TypeName;
                if (typeName == "TemplateInstance")
                {
                    var templateKey = objectInstance.Type + "_TEMPLATE";
                    if (RaveInstance.AllBankManagers.ContainsKey(templateKey))
                    {
                        var compiler = RaveInstance.XmlBankCompiler.MetadataManagers[objectInstance.Type].Compiler;
                        var templateManager = compiler.CreateTemplateManager();

                        foreach (var bank in RaveInstance.AllBankManagers[templateKey].Banks)
                        {
                            templateManager.Load(Utility.ToXDoc(bank.Document));
                        }

                        var templateInstance = objectInstance as ITemplateInstance;
                        if (templateInstance != null)
                        {
                            var temp = new XmlDocument();
                            temp.AppendChild(temp.CreateElement("Objects"));
                            var imported = temp.ImportNode(templateInstance.Node, true);
                            temp.DocumentElement.AppendChild(imported);
                            var expandedDoc = Utility.ToXDoc(temp);
                            templateManager.Process(expandedDoc);
                            typeName = expandedDoc.Root.Descendants().First().Name.ToString();
                        }
                    }
                }

                var allow = false;
                while (typeName != null)
                {
                    if (targetFieldDef.AllowedType.ToUpper() == typeName.ToUpper())
                    {
                        allow = true;
                        break;
                    }

                    var type = objectInstance.TypeDefinitions.FindTypeDefinition(typeName);
                    if (type == null)
                    {
                        break;
                    }

                    typeName = type.InheritsFrom;
                }

                if (!allow)
                {
                    if (reportError)
                    {
                        var sb = new StringBuilder();
                        sb.Append("Invalid child type. Child must be of type: ");
                        sb.Append(targetFieldDef.AllowedType);
                        ErrorManager.HandleInfo(sb.ToString());
                    }

                    return false;
                }
            }

            return true;
        }


        /// <summary>
        /// Check for allowed type.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="targetFieldDef">
        /// The target field definition.
        /// </param>
        /// <param name="reportError">
        /// The report error.
        /// </param>
        /// <returns>
        /// Value indicating whether type is allowed <see cref="bool"/>.
        /// </returns>
        public bool CheckIfMuteSoloEnabled()
        {
            foreach (string allowedType in Configuration.MuteSoloEnabledTypes)
            {

                var typeName = this.TypeName;
                if (typeName == "TemplateInstance")
                {
                    var templateKey = this.Type + "_TEMPLATE";
                    if (RaveInstance.AllBankManagers.ContainsKey(templateKey))
                    {
                        var compiler = RaveInstance.XmlBankCompiler.MetadataManagers[this.Type].Compiler;
                        var templateManager = compiler.CreateTemplateManager();

                        foreach (var bank in RaveInstance.AllBankManagers[templateKey].Banks)
                        {
                            templateManager.Load(Utility.ToXDoc(bank.Document));
                        }

                        var templateInstance = this as ITemplateInstance;
                        if (templateInstance != null)
                        {
                            var temp = new XmlDocument();
                            temp.AppendChild(temp.CreateElement("Objects"));
                            var imported = temp.ImportNode(templateInstance.Node, true);
                            temp.DocumentElement.AppendChild(imported);
                            var expandedDoc = Utility.ToXDoc(temp);
                            templateManager.Process(expandedDoc);
                            typeName = expandedDoc.Root.Descendants().First().Name.ToString();
                        }
                    }
                }


                while (typeName != null)
                {
                    if (allowedType.ToUpper() == typeName.ToUpper())
                    {
                        return true;
                    }

                    var type = this.TypeDefinitions.FindTypeDefinition(typeName);
                    if (type == null)
                    {
                        break;
                    }

                    typeName = type.InheritsFrom;
                }

            }

            return false;
        }

        /// <summary>
        /// Report modified object event.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        public static void ReportModifiedObject(IObjectInstance objectInstance)
        {
            ObjectModified.Raise(objectInstance);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Override ToString to return object instance name.
        /// </summary>
        /// <returns>
        /// The object instance name <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Gets value indicating whether object instance if currently being referenced.
        /// </summary>
        /// <returns>
        /// Indication of being referenced <see cref="bool"/>.
        /// </returns>
        public virtual bool IsReferenced()
        {
            return this.Referencers.Any();
        }

        /// <summary>
        /// Mark object instance as dirty.
        /// </summary>
        public virtual void MarkAsDirty()
        {
            if (null != this.Bank)
            {
                this.Bank.MarkAsDirty();
                this.RaiseObjectModified(this);
            }
        }

        /// <summary>
        /// Delete the object instance.
        /// </summary>
        public virtual void Delete()
        {
            // Remove all references from this object
            foreach (var objectRef in this.References)
            {
                if (objectRef.ObjectInstance != null)
                {
                    objectRef.ObjectInstance.RemoveReferencer(this);
                }
            }

            foreach (XmlNode node in this.Node.ChildNodes)
            {
                if (!RaveUtils.IsPopulatedWaveReference(node))
                {
                    continue;
                }

                var key = node.ChildNodes[1].InnerText + "\\" + node.ChildNodes[0].InnerText;
                if (WaveManager.WaveRefs.ContainsKey(key))
                {
                    WaveManager.WaveRefs[key].RemoveAll(x => x.Name == this.Name);
                }
            }

            if (null != this.Bank)
            {
                this.Bank.DeleteObjectInstance(this);
            }

            this.ObjectLookup.Remove(this.Name);
        }

        /// <summary>
        /// Rename the object instance.
        /// </summary>
        /// <param name="newName">
        /// The new name.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public virtual bool Rename(string newName)
        {
            var checkedOutBanks = new List<IObjectBank>();

            if (null == this.Bank || !this.Bank.IsCheckedOut)
            {
                return false;
            }

            var oldName = this.Name;
            IList<IObjectInstance> refObjects;
            this.GetReferencers(out refObjects);

            var objNames = new Dictionary<string, IDictionary<string, IObjectInstance>>();
            foreach (var refObj in refObjects)
            {
                if (!objNames.ContainsKey(refObj.Name))
                {
                    objNames.Add(refObj.Name, refObj.ObjectLookup);
                }
            }

            foreach (var kvp in objNames)
            {
                var name = kvp.Key;
                var objLookup = kvp.Value;

                var obj = objLookup[name];

                if (!obj.Bank.IsCheckedOut && !obj.Bank.IsLocalCheckout)
                {
                    if (!obj.Bank.Checkout(false))
                    {
                        ErrorManager.HandleInfo("Failed to checkout required bank(s) - unable to rename");
                        return false;
                    }

                    // Update object
                    obj = objLookup[name];
                    checkedOutBanks.Add(obj.Bank);
                }
            }

            // Need to get fresh list of references again in case 
            // any banks were checked out and reloaded
            this.GetReferencers(out refObjects);

            this.ObjectLookup.Remove(this.Name);

            this.Node.Attributes["name"].Value = newName;

            this.Name = newName;
            this.MarkAsDirty();

            foreach (var objRef in refObjects)
            {
                SearchAndReplaceNodeValue(objRef.Node, oldName, newName);
                objRef.MarkAsDirty();
            }

            this.ObjectLookup.Add(this.Name, this);
            
            return true;
        }

        /// <summary>
        /// Get a list of defined variables.
        /// </summary>
        /// <param name="type">
        /// The variable list type.
        /// </param>
        /// <returns>
        /// List of defined variables <see cref="IList{T}"/>.
        /// </returns>
        public virtual IList<string> GetDefinedVariables(VariableListType type)
        {
            throw new NotImplementedException("Must be implemented by extending class");
        }

        /// <summary>
        /// Get a list of object variables.
        /// </summary>
        /// <param name="fieldName">
        /// The field name to get variables for.
        /// </param>
        /// <param name="type">
        /// The variable list type.
        /// </param>
        /// <returns>
        /// List of object variables <see cref="List{T}"/>.
        /// </returns>
        public virtual List<string> GetObjectVariables(string fieldName, VariableListType type)
        {
            throw new NotImplementedException("Must be implemented by extending class");
        }

        /// <summary>
        /// Get the input variables.
        /// </summary>
        /// <param name="fieldName">
        /// The field name to get input variables for.
        /// </param>
        /// <param name="typeDef">
        /// The type definition for the current field.
        /// </param>
        /// <param name="type">
        /// The variable list type.
        /// </param>
        /// <returns>
        /// The input variable list <see cref="VariableList"/>.
        /// </returns>
        public IVariableList GetVariableList(string fieldName, ITypeDefinition typeDef, VariableListType type = VariableListType.Auto)
        {
            var list = new VariableList.VariableList();

            // Determine variable list type based on type definition if not explicitly specified
            if (type == VariableListType.Auto)
            {
                var fieldDefinition = typeDef.Fields.FirstOrDefault(field => field.Name.Equals(fieldName));

                // Determine variable list type
                if (null != fieldDefinition && null != fieldDefinition.Node.Attributes
                    && null != fieldDefinition.Node.Attributes["outputVariable"]
                    && fieldDefinition.Node.Attributes["outputVariable"].Value.ToLower() == "yes")
                {
                    type = VariableListType.Output;
                }
                else
                {
                    type = VariableListType.Input;
                }
            }

            // Find object variables up the hierarchy of this object
            // and all referencing objects
            var objectVariables = this.GetObjectVariables(fieldName, type);
            foreach (var objectVariable in objectVariables)
            {
                list.AddItem(objectVariable);
            }

            var objectBanks = RaveInstance.AllBankManagers["AudioConfig"];

            foreach (var bank in objectBanks.Banks)
            {
                var objectBank = bank as IObjectBank;

                if (null == objectBank)
                {
                    continue;
                }

                foreach (var objectInstance in objectBank.ObjectInstances)
                {
                    if (!objectInstance.TypeName.Equals("VariableList"))
                    {
                        continue;
                    }

                    var listElem = objectInstance.Node;

                    // TODO objectInstance.OnObjectModified += ...

                    var variableElems = listElem.SelectNodes("Variable");

                    // Check that the variables list contains at least one item,
                    // otherwise no point in adding it to the list
                    if (null == variableElems || variableElems.Count == 0)
                    {
                        continue;
                    }

                    var groupNameElem = listElem.Attributes["name"];
                    var groupName = groupNameElem.Value;
                    var displayNameElem = listElem.SelectSingleNode("DisplayName");
                    var label = null == displayNameElem ? groupName : displayNameElem.InnerText;

                    // Add new group
                    if (string.IsNullOrEmpty(label))
                    {
                        throw new Exception("Global variable list requires a name or label attribute");
                    }

                    var items = new List<string>();
                    foreach (XmlNode node in variableElems)
                    {
                        // Check that variable can be used as an output
                        var validAsOutput = node["ValidAsOutput"];

                        // All variables can be used as input; only those with the
                        // ValidAsOutput element can be used as output variables
                        if (type == VariableListType.Input ||
                            (null != validAsOutput &&
                            validAsOutput.InnerText.ToLower() == "yes"))
                        {
                            var nameElem = node["Name"];

                            if (null == nameElem)
                            {
                                throw new Exception("Variable requires a name element");
                            }

                            items.Add(nameElem.InnerText);
                        }
                    }

                    // Don't want to create group if no items to add
                    if (!items.Any())
                    {
                        continue;
                    }

                    var group = list.AddGroup(groupName, label, false);

                    // Add group items
                    foreach (var item in items)
                    {
                        group.AddItem(item);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Handle a dropped object.
        /// </summary>
        /// <param name="droppedObject">
        /// The dropped object.
        /// </param>
        /// <param name="globalRaveTypeDefinitions">
        /// The global rave type definitions.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public virtual bool HandleDroppedObject(IObjectInstance droppedObject, ITypeDefinitions globalRaveTypeDefinitions)
        {
            var targetDef = this.TypeDefinitions.FindTypeDefinition(this.TypeName);

            if (targetDef.ContainsObjectRef && this.CanAddReferences(droppedObject))
            {
                // Check that the dropped object belongs to a valid Episode
                if (droppedObject.Episode != ObjectLookupId.Base &&
                    droppedObject.Episode != this.Episode)
                {
                    ErrorManager.HandleInfo("Child belongs to an invalid episode");
                    return false;
                }

                var possibleFieldDefs = targetDef.FindObjectRefFields();

                // Obtain list of paths to all valid field defs
                var validFieldDefPaths = this.GetMatchingFieldDefinitions(droppedObject, possibleFieldDefs);

                // Can't drop anywhere
                if (validFieldDefPaths.Count == 0)
                {
                    return false;
                }

                IList<IFieldDefinition> validFieldDefPath;

                // Can't determine which field to drop onto if more than 1 found
                if (validFieldDefPaths.Count > 1)
                {
                    var selectorForm = new ReferenceFieldSelector(validFieldDefPaths);
                    selectorForm.ShowDialog();
                    if (selectorForm.DialogResult == DialogResult.OK)
                    {
                        validFieldDefPath = selectorForm.SelectedFieldDefinition;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    validFieldDefPath = validFieldDefPaths[0];
                }

                // Find node at each level in path to locate
                // node to drop object onto
                var parentNode = this.Node;
                var node = parentNode;
                var nodeFieldDef = validFieldDefPath.Last();
                foreach (var fieldDef in validFieldDefPath)
                {
                    if (node == null)
                    {
                        // It's okay for last node to be null, but not before
                        if (fieldDef != nodeFieldDef)
                        {
                            ErrorManager.HandleInfo("Failed to locate node required for dropped object.");
                            return false;
                        }

                        break;
                    }

                    var tmpNode = node;
                    node = this.FindXmlNode(fieldDef.Name, parentNode);
                    parentNode = tmpNode;
                }

                var nodeCompFieldDef = nodeFieldDef as ICompositeFieldDefinition;

                if (null != nodeCompFieldDef)
                {
                    // Handle drop onto composite field type
                    return this.AddDroppedObjectComposite(nodeCompFieldDef, droppedObject, parentNode);
                }

                // Must be a simple field type, so perform drop action on this
                return this.AddDroppedObjectSimple(nodeFieldDef, droppedObject, node);
            }

            return true;
        }

        /// <summary>
        /// Handle a dropped wave node.
        /// </summary>
        /// <param name="waveNode">
        /// The wave node.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public virtual bool HandleDroppedWave(WaveNode waveNode)
        {
            var targetDef = RaveInstance.AllTypeDefinitions["SOUNDS"].FindTypeDefinition(this.TypeName);
            if (targetDef.ContainsWaveRef)
            {
                var fieldDefinition = targetDef.Fields.FirstOrDefault(f => f.Units == "WaveRef");

                if (null == fieldDefinition)
                {
                    throw new Exception("Failed to find field definition of target for dropped wave");
                }

                var waveRefNode = this.FindXmlNode(fieldDefinition.Name, this.Node);

                if (waveRefNode == null)
                {
                    waveRefNode = this.Node.OwnerDocument.CreateElement(fieldDefinition.Name);
                }
                else if (RaveUtils.IsPopulatedWaveReference(waveRefNode))
                {
                    var oldKey = waveRefNode.ChildNodes[1].InnerText + Path.DirectorySeparatorChar +
                                 waveRefNode.ChildNodes[0].InnerText;
                    if (WaveManager.WaveRefs.ContainsKey(oldKey))
                    {
                        WaveManager.WaveRefs[oldKey].RemoveAll(x => x.Name == this.Name);
                    }

                    waveRefNode.RemoveAll();
                }
                else
                {
                    waveRefNode.RemoveAll();
                }

                XmlNode node = waveRefNode.OwnerDocument.CreateElement("WaveName");
                node.InnerText = waveNode.GetWaveName();
                waveRefNode.AppendChild(node);

                node = waveRefNode.OwnerDocument.CreateElement("BankName");
                node.InnerText = waveNode.GetBankPath();
                waveRefNode.AppendChild(node);

                var newKey = waveNode.GetBankPath() + Path.DirectorySeparatorChar + waveNode.GetWaveName();
                if (!WaveManager.WaveRefs.ContainsKey(newKey))
                {
                    WaveManager.WaveRefs.Add(newKey, new List<IObjectInstance>());
                }

                WaveManager.WaveRefs[newKey].Add(this);

                this.MarkAsDirty();
            }
            else if (targetDef.ContainsBankRef)
            {
                var fieldDefinition = targetDef.Fields.FirstOrDefault(f => f.Units == "BankRef");

                if (null == fieldDefinition)
                {
                    throw new Exception("Failed to find field definition of target for dropped wave");
                }

                var waveRefNode = this.FindXmlNode(fieldDefinition.Name, this.Node);

                if (waveRefNode == null)
                {
                    waveRefNode = this.Node.OwnerDocument.CreateElement(fieldDefinition.Name);
                }
                else
                {
                    waveRefNode.RemoveAll();
                }

                waveRefNode.OwnerDocument.InnerText = waveNode.GetBankPath();

                this.MarkAsDirty();
            }
            else
            {
                var newObjectRequired = true;
                if (Control.ModifierKeys == Keys.Control)
                {
                    // Use existing object with wave ref, if it exists
                    List<IObjectInstance> objs;
                    var key = waveNode.GetBankPath() + Path.DirectorySeparatorChar + waveNode.GetWaveName();
                    if (WaveManager.WaveRefs.TryGetValue(key, out objs))
                    {
                        if (objs.Any())
                        {
                            var wavePath = (waveNode.GetBankPath() + "\\" + waveNode.GetWaveName()).Replace("\\", "_").ToUpper();

                            // Only want to use existing object with ref to wave if it has
                            // the default object name (e.g. no "_1" appended to name)
                            foreach (var obj in objs)
                            {
                                if (obj.Name.ToUpper() == wavePath)
                                {
                                    this.HandleDroppedObject(obj, obj.TypeDefinitions);
                                    newObjectRequired = false;
                                    break;
                                }
                            }
                        }
                    }
                }

                // Create new object for wave ref if required
                if (newObjectRequired)
                {
                    // Create wrapper sound
                    var newSound = TypeUtils.CreateWrapperSound(this.Bank, Configuration.WaveWrapperSoundType, waveNode);
                    newSound.VirtualFolderName = this.Name + "_COMPONENTS";
                    TypeUtils.ReportNewObjectCreated(newSound);
                    this.HandleDroppedObject(newSound, RaveInstance.AllTypeDefinitions["SOUNDS"]);
                }
            }

            return true;
        }

        /// <summary>
        /// Remove bank delegate.
        /// </summary>
        public virtual void RemoveBankDelegate()
        {
            this.Bank.BankStatusChanged -= this.OnBankStatusChanged;
        }

        /// <summary>
        /// Compute the references.
        /// </summary>
        /// <param name="suppressTypeErrors">
        /// The suppress type errors flag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public virtual bool ComputeReferences(bool suppressTypeErrors = false)
        {
            if (this.References != null)
            {
                foreach (var objRef in this.References)
                {
                    if (objRef.ObjectInstance != null)
                    {
                        objRef.ObjectInstance.RemoveReferencer(this);
                        objRef.ObjectInstance.ReferencesChanged -= this.OnReferencedObjectReferencesChanged;
                    }
                }
            }

            this.References = this.FindReferences(suppressTypeErrors);

            foreach (var objRef in this.References)
            {
                if (objRef.ObjectInstance != null)
                {
                    objRef.ObjectInstance.ReferencesChanged += this.OnReferencedObjectReferencesChanged;
                    objRef.ObjectInstance.AddReferencer(this);
                }
            }

            this.RaiseReferencesChanged();
            return true;
        }

        /// <summary>
        /// Get value indicating whether a reference can be added.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <returns>
        /// Indication of whether a reference can be added <see cref="bool"/>.
        /// </returns>
        public virtual bool CanAddReferences(IObjectInstance objectInstance)
        {
            var canAdd = !(objectInstance == this || objectInstance.GetFlatListOfReferences().Contains(this));

            if (this.Episode != ObjectLookupId.Test && objectInstance.Episode != ObjectLookupId.Base
                && objectInstance.Episode != this.Episode)
            {
                canAdd = false;
            }

            return canAdd;
        }

        /// <summary>
        /// The get flat list of references.
        /// </summary>
        /// <returns>
        /// The flat list of references <see cref="List{T}"/>.
        /// </returns>
        public virtual List<IObjectInstance> GetFlatListOfReferences()
        {
            var referencers = new List<IObjectInstance>();
            foreach (var referencer in this.References)
            {
                if (null != referencer.ObjectInstance)
                {
                    referencers.Add(referencer.ObjectInstance);
                    referencers.AddRange(referencer.ObjectInstance.GetFlatListOfReferences());
                }
            }

            return referencers;
        }

        /// <summary>
        /// The find child list index.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="objectNode">
        /// The object node.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public virtual int FindReferenceListIndex(IObjectInstance objectInstance, XmlNode objectNode)
        {
            for (var i = 0; i < this.References.Count; i++)
            {
                if (this.References[i].ObjectInstance == objectInstance && this.References[i].Node == objectNode)
                {
                    return i;
                }
            }

            return Int32.MaxValue;
        }

        /// <summary>
        /// Add a reference.
        /// </summary>
        /// <param name="reference">
        /// The reference.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="index">
        /// The index to insert at.
        /// </param>
        public virtual void AddReference(IReferencable reference, XmlNode node, int index = -1)
        {
            var objInstance = reference as IObjectInstance;
            if (null == objInstance)
            {
                throw new Exception("Failed to add reference, expected object of type IObjectInstance");
            }

            if (this.CanAddReferences(objInstance))
            {
                var objectRef = new ObjectRef { ObjectInstance = objInstance, Node = node };

                if (index >= 0)
                {
                    this.References.Insert(index, objectRef);
                }
                else
                {
                    this.References.Add(objectRef);
                }

                this.RaiseReferencesChanged();
                reference.ReferencesChanged += this.OnReferencedObjectReferencesChanged;
            }
            else
            {
                MessageBox.Show("Invalid reference");
            }
        }

        /// <summary>
        /// Remove a reference.
        /// </summary>
        /// <param name="reference">
        /// The reference.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        public virtual void RemoveReference(IReferencable reference, XmlNode node)
        {
            if (null == reference)
            {
                // Remove an invalid reference
                foreach (var objectRef in this.References)
                {
                    if (objectRef.Node == node)
                    {
                        this.References.Remove(objectRef);
                        this.RaiseReferencesChanged();
                        break;
                    }
                }
            }
            else
            {
                // Remove a valid reference
                var objInstance = reference as IObjectInstance;
                if (null == objInstance)
                {
                    throw new Exception("Failed to remove reference, expected object of type IObjectInstance");
                }

                foreach (var objectRef in this.References)
                {
                    if (objectRef.ObjectInstance == reference && objectRef.Node == node)
                    {
                        this.References.Remove(objectRef);
                        this.RaiseReferencesChanged();
                        break;
                    }
                }

                reference.ReferencesChanged -= this.OnReferencedObjectReferencesChanged;
            }
        }

        /// <summary>
        /// Add a referencer.
        /// </summary>
        /// <param name="referencer">
        /// The referencer.
        /// </param>
        public virtual void AddReferencer(IReferencable referencer)
        {
            if (!this.Referencers.Contains(referencer))
            {
                this.Referencers.Add(referencer);
            }
        }

        /// <summary>
        /// Remove a referencer.
        /// </summary>
        /// <param name="referencer">
        /// The referencer.
        /// </param>
        public virtual void RemoveReferencer(IReferencable referencer)
        {
            this.Referencers.Remove(referencer);
        }

        /// <summary>
        /// Invalidate the node by setting it to null.
        /// </summary>
        public virtual void InvalidateNode()
        {
            this.Node = null;
        }


        /// <summary>
        /// Sets the object to mute.
        /// </summary>
        /// <param name="mute">if set to <c>true</c> [mute].</param>
        /// <returns>
        /// indication of success
        /// </returns>
        public bool Mute(bool mute)
        {

            if (CheckIfMuteSoloEnabled())
            {
                this.isMuted = mute;
                this.PropertyChanged.Raise(this, "IsMuted");
                return true;
            }
            else
                return false;
        }


        /// <summary>
        /// Sets the object to solo.
        /// </summary>
        /// <param name="solo">if set to <c>true</c> [solo].</param>
        /// <returns>
        /// indication of succes
        /// </returns>
        public bool Solo(bool solo)
        {

            if (CheckIfMuteSoloEnabled())
            {
                this.isSoloed = solo;
                this.PropertyChanged.Raise(this, "IsSoloed");
                return true;
            }
            else
                return false;
        }

        #endregion

        #region Protected Static Methods

        /// <summary>
        /// Search and replace node value.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="oldValue">
        /// The old value.
        /// </param>
        /// <param name="newValue">
        /// The new value.
        /// </param>
        protected static void SearchAndReplaceNodeValue(XmlNode node, string oldValue, string newValue)
        {
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.ChildNodes.Count == 0)
                {
                    if (n.InnerText.Equals(oldValue, StringComparison.OrdinalIgnoreCase))
                    {
                        n.InnerText = newValue;
                    }
                }
                else
                {
                    SearchAndReplaceNodeValue(n, oldValue, newValue);
                }
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initialize the references.
        /// </summary>
        protected void InitWaveRefs()
        {
            foreach (var key in
                from XmlNode node in this.Node.ChildNodes
                where RaveUtils.IsPopulatedWaveReference(node)
                let waveName = node.ChildNodes[0].InnerText
                let bankPath = node.ChildNodes[1].InnerText
                select bankPath + "\\" + waveName)
            {
                if (WaveManager.WaveRefs.ContainsKey(key))
                {
                    WaveManager.WaveRefs[key].Add(this);
                }
                else
                {
                    WaveManager.WaveRefs.Add(key, new List<IObjectInstance> { this });
                }
            }
        }

        /// <summary>
        /// Set the virtual folder in the object XML.
        /// </summary>
        /// <param name="folderName">
        /// The folder name.
        /// </param>
        protected void SetVirtualFolder(string folderName)
        {
            var folderAttr = this.Node.Attributes["folder"];

            if (string.IsNullOrEmpty(folderName))
            {
                if (null != folderAttr)
                {
                    this.Node.Attributes.Remove(folderAttr);
                    this.MarkAsDirty();
                }
            }
            else
            {
                if (null == folderAttr)
                {
                    folderAttr = this.Node.OwnerDocument.CreateAttribute("folder");
                    this.Node.Attributes.Append(folderAttr);
                }

                folderAttr.Value = folderName;
                this.MarkAsDirty();
            }
        }


        /// <summary>
        /// Find the object instances that are references.
        /// </summary>
        /// <param name="suppressTypeErrors">
        /// The suppress type errors flag.
        /// </param>
        /// <returns>
        /// The list of object references <see cref="IList{T}"/>.
        /// </returns>
        protected IList<ObjectRef> FindReferences(bool suppressTypeErrors)
        {
            var typeDefinition = this.TypeDefinitions.FindTypeDefinition(this.TypeName);

            if (typeDefinition == null)
            {
                if (!suppressTypeErrors && this.TypeName != "TemplateInstance")
                {
                    ErrorManager.HandleInfo("Object with invalid type: " + this.Name + " (" + this.TypeName + ")");
                }

                return new List<ObjectRef>();
            }

            var references = new List<ObjectRef>();
            this.FindReferences(this.Node, typeDefinition.Fields, references);

            return references;
        }

        /// <summary>
        /// Find the references.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="fields">
        /// The fields.
        /// </param>
        /// <param name="references">
        /// The references.
        /// </param>
        protected void FindReferences(
            XmlNode node, IList<IFieldDefinition> fields, List<ObjectRef> references)
        {
            for (var i = 0; i < node.ChildNodes.Count; i++)
            {
                var childNode = node.ChildNodes[i];
                var fieldDefinition = fields.FirstOrDefault(f => f.Name == childNode.Name);

                if (fieldDefinition != null && (fieldDefinition.Units == "ObjectRef" || fieldDefinition.Units == "CategoryRef"))
                {
                    // Handle empty value as no reference set
                    if (string.IsNullOrEmpty(childNode.InnerText))
                    {
                        continue;
                    }

                    var objectRef = new ObjectRef();

	                if (fieldDefinition.Units == "CategoryRef")
	                {
						ObjectLookupId categoryBase = new ObjectLookupId("CATEGORIES", ObjectLookupId.Base);
		                if (RaveInstance.ObjectLookupTables[categoryBase].ContainsKey(childNode.InnerText))
		                {
			                objectRef.ObjectInstance = RaveInstance.ObjectLookupTables[categoryBase][childNode.InnerText];
		                }
		                else
		                {
							
							if (!string.IsNullOrEmpty(childNode.InnerText))
							{
								System.Diagnostics.Debug.WriteLine("Warning: invalid category ref: " + childNode.InnerText +
																   " from " + this.Type + " object " + this.Name +
																   ", node: " + childNode.Name);
							}
							objectRef.ObjectInstance = null;
		                }
	                }
					else if (this.ObjectLookup != null && this.ObjectLookup.ContainsKey(childNode.InnerText) &&
                        this.ObjectLookup[childNode.InnerText] != this)
                    {
						
						objectRef.ObjectInstance = this.ObjectLookup[childNode.InnerText];
                    }
                    else
                    {
                        // Find allowed reference types
                        var found = false;

                        var baseDefs = new List<string>();
                        if (fieldDefinition.AllowedType != null)
                        {
                            foreach (var kvp in RaveInstance.AllTypeDefinitions)
                            {
                                if (kvp.Value.FindTypeDefinition(fieldDefinition.AllowedType) != null)
                                {
                                    // Found
                                    if (!baseDefs.Contains(kvp.Key))
                                    {
                                        baseDefs.Add(kvp.Key);
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Add all types
                            baseDefs.AddRange(Configuration.MetadataTypes.Select(metadataType => metadataType.Type));
                        }

                        Dictionary<string, IObjectInstance> lookUp = null;

                        foreach (var kvp in RaveInstance.ObjectLookupTables)
                        {
                            if (baseDefs.Contains(kvp.Key.Type) &&
                                kvp.Value.ContainsKey(childNode.InnerText) &&
                                kvp.Value[childNode.InnerText] != this)
                            {
                                // Exists in base or current episode
                                if (kvp.Key.Episode == ObjectLookupId.Base ||
                                    kvp.Key.Episode == this.Episode)
                                {
                                    found = true;
                                    lookUp = kvp.Value;
                                }
                                else if (kvp.Key.Episode == this.Episode)
                                {
                                    // Override base if that was found first
                                    found = true;
                                    lookUp = kvp.Value;
                                }
                            }

                            if (found)
                            {
                                break;
                            }
                        }

                        if (found && lookUp != null)
                        {
                            objectRef.ObjectInstance = lookUp[childNode.InnerText];
                        }
                        else
                        {
                            // Invalid object reference
                            if (!string.IsNullOrEmpty(childNode.InnerText))
                            {
                                System.Diagnostics.Debug.WriteLine("Warning: invalid object ref: " + childNode.InnerText +
                                                                   " from " + this.Type + " object " + this.Name +
                                                                   ", node: " + childNode.Name);
                            }

                            objectRef.ObjectInstance = null;
                        }
                    }

                    objectRef.Node = childNode;

                    references.Add(objectRef);
                }

                var compositeFieldDef = fieldDefinition as ICompositeFieldDefinition;
                if (compositeFieldDef != null)
                {
                    this.FindReferences(node.ChildNodes[i], compositeFieldDef.Fields, references);
                }
            }
        }

        /// <summary>
        /// Find an XML node.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <returns>
        /// The found node <see cref="XmlNode"/>.
        /// </returns>
        protected XmlNode FindXmlNode(string name, XmlNode parent)
        {
            return parent.ChildNodes.Cast<XmlNode>().FirstOrDefault(n => n.Name == name);
        }

        /// <summary>
        /// Get list of defined variables.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="fields">
        /// The fields.
        /// </param>
        /// <param name="type">
        /// The variable list type.
        /// </param>
        /// <returns>
        /// The defined variables <see cref="IList{T}"/>.
        /// </returns>
        protected IList<string> GetDefinedVariables(XmlNode parentNode, IList<IFieldDefinition> fields, VariableListType type)
        {
            var variables = new List<string>();
            var useVariable = true;
            var variable = string.Empty;
	        if (parentNode == null)
	        {
		        return new List<string>();
	        }
            foreach (XmlNode node in parentNode.ChildNodes)
            {
                if (node.Name == "Usage")
                {
                    if (type == VariableListType.Output && node.InnerText.ToUpper() != "VARIABLE_USAGE_SOUND")
                    {
                        useVariable = false;
                    }
                }
                else
                {
                    var fieldDefinition = fields.FirstOrDefault(f => f.Name == node.Name);
                    if (fieldDefinition == null)
                    {
                        continue;
                    }

                    if (fieldDefinition.Units == "variabledef")
                    {
                        variable = node.InnerText;
                    }
                    else
                    {
                        var compFieldDefinition = fieldDefinition as ICompositeFieldDefinition;
                        if (null != compFieldDefinition)
                        {
                            variables.AddRange(this.GetDefinedVariables(node, compFieldDefinition.Fields, type));
                        }
                    }
                }
            }

            if (useVariable && !string.IsNullOrEmpty(variable))
            {
                variables.Add(variable);
            }

            return variables;
        }

        /// <summary>
        /// Get list of object variables.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="type">
        /// The variable list type.
        /// </param>
        /// <returns>
        /// The list of object variables <see cref="IList{T}"/>.
        /// </returns>
        protected List<string> GetObjectVariables(IObjectInstance objectInstance, VariableListType type)
        {
            var variableList = new List<string>();

            // Add variables from up the hierarchy
            variableList.AddRange(objectInstance.GetDefinedVariables(type));

            foreach (var referencer in objectInstance.Referencers)
            {
                var refObjectInstance = referencer as IObjectInstance;

                if (null != refObjectInstance)
                {
                    variableList.AddRange(this.GetObjectVariables(refObjectInstance, type));
                }
            }

            return variableList.Distinct().ToList();
        }

        /// <summary>
        /// Get matching field definitions.
        /// </summary>
        /// <param name="droppedObject">
        /// The dropped object.
        /// </param>
        /// <param name="fieldDefs">
        /// The field definitions.
        /// </param>
        /// <returns>
        /// The list of matching field definitions <see cref="IList{T}"/>.
        /// </returns>
        protected IList<IList<IFieldDefinition>> GetMatchingFieldDefinitions(
            IObjectInstance droppedObject,
            IEnumerable<IFieldDefinition> fieldDefs)
        {
            var fieldDefPaths = new List<IList<IFieldDefinition>>();

            foreach (var fieldDef in fieldDefs)
            {
                var fieldDefPath = this.GetMatchingFieldDefinitionsHelper(droppedObject, fieldDef);
                if (null != fieldDefPath)
                {
                    fieldDefPaths.AddRange(fieldDefPath);
                }
            }

            // Check if any field def is explicity set to be
            // the default drop location for dropped object
            var defaultDropFieldDef = fieldDefPaths.FirstOrDefault(
                fieldDefPath => fieldDefPath.Last().DefaultDropField);

            if (null != defaultDropFieldDef)
            {
                fieldDefPaths.Clear();
                fieldDefPaths.Add(defaultDropFieldDef);
            }

            return fieldDefPaths;
        }

        /// <summary>
        /// Get matching field definitions helper.
        /// </summary>
        /// <param name="droppedObject">
        /// The dropped object.
        /// </param>
        /// <param name="fieldDef">
        /// The field definitions.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The matching field definitions <see cref="IEnumerable{T}"/>.
        /// </returns>
        protected IEnumerable<IList<IFieldDefinition>> GetMatchingFieldDefinitionsHelper(
            IObjectInstance droppedObject,
            IFieldDefinition fieldDef,
            ICollection<IFieldDefinition> path = null)
        {
            var fieldDefPaths = new List<IList<IFieldDefinition>>();

            IList<IFieldDefinition> currPath;
            if (null == path)
            {
                currPath = new List<IFieldDefinition> { fieldDef };
            }
            else
            {
                currPath = new List<IFieldDefinition>(path);
                path.Add(fieldDef);
            }

            // Simple field type - we've reached the lowest nested field def
            if (!string.IsNullOrWhiteSpace(fieldDef.Units) &&
                fieldDef.Units.Equals("ObjectRef"))
            {
                if (!CheckForAllowedType(droppedObject, fieldDef, false))
                {
                    return null;
                }

                // Trim field def path so that field def we are looking for
                // is the last one in the path list (we couldn't just stop
                // at first instance in case the field def is nested within
                // another instance of itself).
                for (var i = currPath.Count - 1; i > 0; i--)
                {
                    if (currPath[i].TypeName.Equals(fieldDef.TypeName))
                    {
                        break;
                    }

                    currPath.RemoveAt(i);
                }

                return new List<IList<IFieldDefinition>> { currPath };
            }

            // Composite field type, so check inner fields
            var compFieldDef = fieldDef as ICompositeFieldDefinition;
            if (null == compFieldDef)
            {
                return null;
            }

            foreach (var innerField in compFieldDef.Fields)
            {
                var innerCurrPath = new List<IFieldDefinition>(currPath);
                var validFieldDefPaths = this.GetMatchingFieldDefinitionsHelper(droppedObject, innerField, innerCurrPath);
                if (null != validFieldDefPaths)
                {
                    fieldDefPaths.AddRange(validFieldDefPaths);
                }
            }

            return fieldDefPaths;
        }

        /// <summary>
        /// Add dropped object simple.
        /// </summary>
        /// <param name="matchedRefFieldDef">
        /// The matched ref field definition.
        /// </param>
        /// <param name="droppedObject">
        /// The dropped object.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        protected bool AddDroppedObjectSimple(IFieldDefinition matchedRefFieldDef, IObjectInstance droppedObject, XmlNode node)
        {
            if (node == null)
            {
                // Create new node if not replacing existing reference
                node = this.Node.OwnerDocument.CreateElement(matchedRefFieldDef.Name);
                this.Node.AppendChild(node);
            }
            else
            {
                // Remove the existing matching reference (there should only be one)
                foreach (var reference in this.References)
                {
                    if (reference.ObjectInstance != null && reference.Node == node)
                    {
                        reference.ObjectInstance.ReferencesChanged -= this.OnReferencedObjectReferencesChanged;
                        reference.ObjectInstance.RemoveReferencer(this);

                        this.References.Remove(reference);
                        break;
                    }
                }
            }

            this.AddReference(droppedObject, node);
            droppedObject.AddReferencer(this);

            node.InnerText = droppedObject.Name;
            this.MarkAsDirty();
            return true;
        }

        /// <summary>
        /// Add dropped object composite.
        /// </summary>
        /// <param name="compFieldDef">
        /// The comp field definition.
        /// </param>
        /// <param name="droppedObject">
        /// The dropped object.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        protected bool AddDroppedObjectComposite(ICompositeFieldDefinition compFieldDef, IObjectInstance droppedObject, XmlNode node)
        {
            var numObjectRefFields = 0;
            IFieldDefinition fieldDefinition = null;

            foreach (var fieldDef in compFieldDef.Fields)
            {
                if (fieldDef.DoesFieldContainObjectRef() && CheckForAllowedType(droppedObject, fieldDef, false))
                {
                    fieldDefinition = fieldDef;
                    numObjectRefFields++;
                }
            }

            if (null == fieldDefinition)
            {
                throw new Exception("Failed to find field definition for dropped composite object");
            }

            if (numObjectRefFields != 1)
            {
                throw new Exception("Target object type contains more than one compatible object reference");
            }

            if (!CheckForAllowedType(droppedObject, fieldDefinition))
            {
                return false;
            }

            if (compFieldDef.MaxOccurs == 1)
            {
                // Overwrite the existing ref (if its there)
                if (node == null)
                {
                    node = this.Node.OwnerDocument.CreateElement(compFieldDef.Name);
                    this.Node.AppendChild(node);
                }

                var existingNode = this.FindXmlNode(fieldDefinition.Name, node);

                if (existingNode == null)
                {
                    existingNode = node.OwnerDocument.CreateElement(fieldDefinition.Name);
                    node.AppendChild(existingNode);
                }
                else
                {
                    foreach (var reference in this.References)
                    {
                        if (reference.Node == existingNode)
                        {
                            this.RemoveReference(reference.ObjectInstance, reference.Node);
                            break;
                        }
                    }
                }

                existingNode.InnerText = droppedObject.Name;
                this.AddReference(droppedObject, existingNode);
                droppedObject.AddReferencer(this);

                this.MarkAsDirty();
            }
            else
            {
                // if it'll fit then add this ref to the list
                var numObjectRefs = this.Node.ChildNodes.Cast<XmlNode>().Count(child => child.Name == compFieldDef.Name);
                if (numObjectRefs + 1 > compFieldDef.MaxOccurs)
                {
                    ErrorManager.HandleInfo(
                        "Target object doesnt have any free object ref slots");
                }
                else
                {
                    XmlNode compositeNode = this.Node.OwnerDocument.CreateElement(compFieldDef.Name);
                    this.Node.AppendChild(compositeNode);

                    XmlNode simpleNode = compositeNode.OwnerDocument.CreateElement(fieldDefinition.Name);
                    simpleNode.InnerText = droppedObject.Name;

                    compositeNode.AppendChild(simpleNode);
                    this.AddReference(droppedObject, simpleNode);
                    droppedObject.AddReferencer(this);

                    this.MarkAsDirty();
                }
            }

            return true;
        }

        /// <summary>
        /// Get the referencers.
        /// </summary>
        /// <param name="refObjects">
        /// The ref objects.
        /// </param>
        protected void GetReferencers(out IList<IObjectInstance> refObjects)
        {
            refObjects = new List<IObjectInstance>();

            // Only want to locate and update references that aren't auto-generated
            foreach (var reference in this.Referencers)
            {
                var objInstance = reference as IObjectInstance;

                if (null == objInstance)
                {
                    throw new Exception("Failed to rename: expected reference of type IObjectInstance");
                }

                if (!objInstance.IsAutoGenerated)
                {
                    refObjects.Add(objInstance);
                }
            }
        }

        /// <summary>
        /// The on bank status changed.
        /// </summary>
        protected virtual void OnBankStatusChanged()
        {
            this.ObjectStatusChanged.Raise();
        }

        /// <summary>
        /// Raise object modified event.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        protected void RaiseObjectModified(IObjectInstance objectInstance)
        {
            ObjectModified.Raise(objectInstance);
        }

        /// <summary>
        /// Raise references changed event.
        /// </summary>
        protected void RaiseReferencesChanged()
        {
            this.ReferencesChanged.Raise();
        }

        /// <summary>
        /// The on references changed event.
        /// </summary>
        protected void OnReferencedObjectReferencesChanged()
        {
            this.RaiseReferencesChanged();
        }

        #endregion


    }
}