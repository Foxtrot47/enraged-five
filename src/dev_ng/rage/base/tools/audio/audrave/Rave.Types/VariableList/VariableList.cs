﻿// -----------------------------------------------------------------------
// <copyright file="VariableList.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

namespace Rave.Types.VariableList
{
    using System;
    using System.Collections.Generic;

    using Rave.Types.Infrastructure.Interfaces.VariableList;

    using Forms = System.Windows.Forms;
    using Wpf = System.Windows.Controls;

    /// <summary>
    /// The variable list class.
    /// </summary>
    public class VariableList : IVariableList
    {
        // TODO - check that group/item labels are unique as well as name?

        /// <summary>
        /// Initializes a new instance of the <see cref="VariableList"/> class.
        /// </summary>
        public VariableList()
        {
            this.Root = new VariableListGroup(this, null, null);
            this.AllItems = new List<IVariableListItem>();
            this.FlatListNames = new SortedSet<string>(StringComparer.OrdinalIgnoreCase);
            this.FlatListLabels = new SortedSet<string>(StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the root variable list group.
        /// </summary>
        public IVariableListGroup Root { get; private set; }

        /// <summary>
        /// Gets all items of every group in variable list.
        /// </summary>
        public IList<IVariableListItem> AllItems { get; private set; }

        /// <summary>
        /// Gets all variable items as a flat list of paths made up of
        /// group and item names (i.e. single string per
        /// menu item with parent groups in name separated by common
        /// delimiter).
        /// </summary>
        public SortedSet<string> FlatListNames { get; private set; }

        /// <summary>
        /// Gets all variable items as a flat list of paths made up of
        /// group and item labels (i.e. single string per
        /// menu item with parent groups in name separated by common
        /// delimiter).
        /// </summary>
        public SortedSet<string> FlatListLabels { get; private set; }

        /// <summary>
        /// Get the WPF version of the menu.
        /// </summary>
        /// <param name="handler">
        /// The onClick event handler for variable list items.
        /// </param>
        /// <returns>
        /// The menu.
        /// </returns>
        public Wpf.ContextMenu GetContextMenu_Wpf(Delegate handler = null)
        {
            Wpf.ContextMenu menu = new System.Windows.Controls.ContextMenu();
            List<System.Windows.Controls.MenuItem> menuItems = new List<System.Windows.Controls.MenuItem>();
        
            IList<IVariableListItem> list = this.Root.Items.OrderBy(p => p.Label).ToList();
            foreach (IVariableListItem item in list)
            {
                var newMenuItem = new System.Windows.Controls.MenuItem
                {
                    Header = item.Label,
                    Tag = item.FullNamePath
                };
                if (null != handler)
                {
                    newMenuItem.AddHandler(System.Windows.Controls.MenuItem.ClickEvent, handler, true);
                }

                menuItems.Add(newMenuItem);
            }
            IList<IVariableListGroup> groups = this.Root.Groups.OrderBy(p => p.Label).ToList();
            foreach (var group in groups)
            {
                var menuItem = new System.Windows.Controls.MenuItem
                {
                    Header = group.Label
                };
                menuItems.Add(menuItem);
                this.InitContextMenuWpf(group, menuItem, handler);
            }

            foreach (Wpf.MenuItem item in menuItems.OrderBy(p => p.Header))
            {
                menu.Items.Add(item);
            }

            return menu;
        }

        /// <summary>
        /// Get the Forms version of the menu.
        /// </summary>
        /// <param name="handler">
        /// The onClick event handler for variable list items.
        /// </param>
        /// <returns>
        /// Forms menu <see cref="System.Windows.Forms.Menu"/>.
        /// </returns>
        public Forms.ContextMenu GetContextMenu_Forms(EventHandler handler = null)
        {
            var menu = new Forms.ContextMenu();
            List<Forms.MenuItem> menuItems = new List<Forms.MenuItem>();
            foreach (var item in this.Root.Items)
            {
                var newMenuItem = new Forms.MenuItem(item.Label, handler)
                {
                    Tag = item.FullNamePath
                };
                menuItems.Add(newMenuItem);
            }

            foreach (var group in this.Root.Groups)
            {
                var menuItem = new Forms.MenuItem(group.Label);
                menuItems.Add(menuItem);
                this.InitContextMenuForms(group, menuItem, handler);
            }

            menu.MenuItems.AddRange(menuItems.OrderBy(p => p.Text).ToArray());

            return menu;
        }

        /// <summary>
        /// Add a group to the variable list.
        /// Groups are add to the group/sub-group specified in the path,
        /// which is split by a specified delimiter.
        /// E.g. "my.test.group" will add a group "group" to:
        ///     root
        ///     --- my
        ///         --- test
        ///             --- group
        /// </summary>
        /// <param name="path">
        /// The group path, split by delimiter.
        /// </param>
        /// <param name="label">
        /// The group label used for display.
        /// </param>
        /// <param name="includeInPath">
        /// Indicates whether to include group when determining path.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// The new variable list group <see cref="VariableListGroup"/>.
        /// </returns>
        public IVariableListGroup AddGroup(string path, string label = null, bool includeInPath = true, char delimiter = '.')
        {
            return this.Root.AddGroup(path, label, includeInPath, delimiter);
        }

        /// <summary>
        /// Add an item to the variable list group.
        /// Items are add to the group/sub-group specified in the path,
        /// which is split by a specified delimiter.
        /// E.g. "my.test.variable" will add a variable "variable" to:
        ///     root
        ///     --- my
        ///         --- test
        /// </summary>
        /// <param name="path">
        /// The variable path, split by delimiter.
        /// </param>
        /// <param name="label">
        /// The variable list item label.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// The new variable list item <see cref="VariableListItem"/>.
        /// </returns>
        public IVariableListItem AddItem(string path, string label = null, char delimiter = '.')
        {
            return this.Root.AddItem(path, label, delimiter);
        }

        /// <summary>
        /// Get a list of sub groups of the group matching specified path.
        /// </summary>
        /// <param name="path">
        /// The path to the parent group.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// List of sub groups <see cref="IList"/>.
        /// </returns>
        public IList<IVariableListGroup> GetSubGroups(string path, char delimiter = '.')
        {
            var currGroup = this.Root;

            var splitPath = path.Split(delimiter);
            foreach (var label in splitPath)
            {
                if (string.IsNullOrEmpty(label))
                {
                    continue;
                }

                if (null == currGroup)
                {
                    return null;
                }

                currGroup = currGroup.GetSubGroup(label);
            }

            return null != currGroup ? currGroup.Groups : null;
        }

        /// <summary>
        /// Get a list of items of the group matching specified path.
        /// </summary>
        /// <param name="path">
        /// The path to the parent group.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// List of items <see cref="IList"/>.
        /// </returns>
        public IList<IVariableListItem> GetGroupItems(string path, char delimiter = '.')
        {
            var currGroup = this.Root;

            var splitPath = path.Split(delimiter);
            foreach (var label in splitPath)
            {
                if (string.IsNullOrEmpty(label))
                {
                    continue;
                }

                if (null == currGroup)
                {
                    return null;
                }

                currGroup = currGroup.GetSubGroup(label);
            }

            return null != currGroup ? currGroup.Items : null;
        }

        /// <summary>
        /// Get an item matching specified path.
        /// </summary>
        /// <param name="path">
        /// The path to the item.
        /// </param>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// The item if found, null otherwise <see cref="VariableListItem"/>.
        /// </returns>
        public IVariableListItem GetItem(string path, char delimiter = '.')
        {
            return this.Root.GetItem(path, delimiter);
        }

        /// <summary>
        /// Initialize the WPF context menu.
        /// </summary>
        /// <param name="group">
        /// The current group.
        /// </param>
        /// <param name="menuItem">
        /// The current parent menu item.
        /// </param>
        /// <param name="handler">
        /// The onClick event handler for variable list items.
        /// </param>
        private void InitContextMenuWpf(IVariableListGroup group, System.Windows.Controls.MenuItem menuItem, Delegate handler = null)
        {
            List<System.Windows.Controls.MenuItem> menuItems = new List<System.Windows.Controls.MenuItem>();
            foreach (var item in group.Items)
            {
                var newMenuItem = new System.Windows.Controls.MenuItem
                {
                    Header = item.Label,
                    Tag = item.FullNamePath
                };

                if (null != handler)
                {
                    newMenuItem.AddHandler(System.Windows.Controls.MenuItem.ClickEvent, handler, true);
                }

                menuItems.Add(newMenuItem);
            }

            foreach (var subGroup in group.Groups)
            {
                var subMenuItem = new System.Windows.Controls.MenuItem
                {
                    Header = subGroup.Label
                };

                menuItems.Add(subMenuItem);
                this.InitContextMenuWpf(subGroup, subMenuItem, handler);
            }

            foreach (var item in menuItems.OrderBy(p => p.Header))
            {
                menuItem.Items.Add(item);
            }
        }

        /// <summary>
        /// Initialize the WinForms context menu.
        /// </summary>
        /// <param name="group">
        /// The current group.
        /// </param>
        /// <param name="menuItem">
        /// The current parent menu item.
        /// </param>
        /// <param name="handler">
        /// The onClick event handler for variable list items.
        /// </param>
        private void InitContextMenuForms(IVariableListGroup group, System.Windows.Forms.Menu menuItem, EventHandler handler = null)
        {
            List<Forms.MenuItem> menuItems = new List<Forms.MenuItem>();
            foreach (var item in group.Items.OrderBy(p => p.Label))
            {
                var newMenuItem = new System.Windows.Forms.MenuItem(item.Label, handler)
                {
                    Tag = item.FullNamePath
                };
                menuItems.Add(newMenuItem);
            }

            foreach (var subGroup in group.Groups.OrderBy(p => p.Label))
            {
                var subMenuItem = new System.Windows.Forms.MenuItem(subGroup.Label);
                menuItems.Add(subMenuItem);
                this.InitContextMenuForms(subGroup, subMenuItem, handler);
            }

            menuItem.MenuItems.AddRange(menuItems.OrderBy(p => p.Text).ToArray());
        }
    }
}
