﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReferenceFieldSelector.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the ReferenceFieldSelector type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.Types.Popups
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    /// <summary>
    /// The reference field selector.
    /// </summary>
    public partial class ReferenceFieldSelector : Form
    {
        /// <summary>
        /// The type definitions.
        /// </summary>
        private readonly IDictionary<string, IList<IFieldDefinition>> fieldDefinitions; 

        /// <summary>
        /// Initializes a new instance of the <see cref="ReferenceFieldSelector"/> class.
        /// </summary>
        /// <param name="fieldDefinitions">
        /// The field definitions.
        /// </param>
        public ReferenceFieldSelector(IEnumerable<IList<IFieldDefinition>> fieldDefinitions)
        {
            this.InitializeComponent();
            this.fieldDefinitions = new Dictionary<string, IList<IFieldDefinition>>();

            foreach (var group in fieldDefinitions)
            {
                var path = @group.Aggregate(string.Empty, (current, definition) => Path.Combine(current, definition.Name));
                this.fieldDefinitions.Add(path, group);
                this.lbFieldDefinitions.Items.Add(path);
            }
        }

        /// <summary>
        /// Gets the selected field definition.
        /// </summary>
        public IList<IFieldDefinition> SelectedFieldDefinition { get; private set; }

        /// <summary>
        /// Handle selection.
        /// </summary>
        private void HandleSelection()
        {
            if (this.lbFieldDefinitions.SelectedItems.Count == 0)
            {
                MessageBox.Show(
                    "Select an option from the list or Cancel", "Selection Required", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            var selectedName = this.lbFieldDefinitions.SelectedItems[0].ToString();
            this.SelectedFieldDefinition = this.fieldDefinitions[selectedName];

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Okay button click handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void btnOkay_Click(object sender, EventArgs e)
        {
            this.HandleSelection();
        }

        /// <summary>
        /// Cancel button click handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Field definitions list box double click handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void lbFieldDefinitions_DoubleClick(object sender, EventArgs e)
        {
            this.HandleSelection();
        }
    }
}
