﻿// -----------------------------------------------------------------------
// <copyright file="WaveManager.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Types.Waves
{
    using System.Collections.Generic;

    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    /// Wave manager.
    /// </summary>
    public class WaveManager
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="WaveManager"/> class.
        /// </summary>
        static WaveManager()
        {
            WaveRefs = new Dictionary<string, List<IObjectInstance>>();
            WaveSlotRefs = new Dictionary<string, List<string>>();
        }

        #endregion

        /// <summary>
        /// Gets the wave refs.
        /// </summary>
        public static Dictionary<string, List<IObjectInstance>> WaveRefs { get; private set; }

        /// <summary>
        /// Gets the wave slot refs.
        /// </summary>
        public static Dictionary<string, List<string>> WaveSlotRefs { get; private set; }
    }
}
