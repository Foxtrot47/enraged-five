﻿using System;
using System.Windows;
using System.Windows.Controls;
using Rave.Plugins.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.Objects;
using WPFToolLib.Extensions;

namespace Rave.SoundSetEditor
{
	/// <summary>
	/// Interaction logic for SoundSetEditorView.xaml
	/// </summary>
	public partial class SoundSetEditorView : IRAVEObjectEditorPlugin
	{
		public SoundSetEditorView()
		{
			InitializeComponent();

		}

		public string ObjectType
		{
			get { return "SoundSet"; }
		}



#pragma warning disable 0067
		public event Action<Rave.Types.Infrastructure.Interfaces.Objects.IObjectInstance> OnObjectEditClick;
		public event Action<Rave.Types.Infrastructure.Interfaces.Objects.IObjectInstance> OnObjectRefClick;
		public event Action<string, string> OnWaveRefClick;
		public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067

		private IObjectInstance m_objectToEdit;
		public void EditObject(Rave.Types.Infrastructure.Interfaces.Objects.IObjectInstance objectInstance, Mode mode)
		{
			SoundSetEditorView_OnUnloaded(null, null);
			m_objectToEdit = objectInstance;
			SoundSetEditorView_OnLoaded(null, null);
		}

		public void Dispose()
		{
			//do nothing
		}

		public string GetName()
		{
			return "Sound Set Editor";
		}

		public bool Init(System.Xml.XmlNode settings)
		{
			return true;
		}

		private SoundSetEditorViewModel m_viewModel;
		private void SoundSetEditorView_OnUnloaded(object sender, RoutedEventArgs e)
		{
			if (m_viewModel != null)
			{
				this.DataContext = null;

				//m_viewModel.PropertyChanged -= OnViewModelPropertyChanged;
				m_viewModel.Dispose();
				m_viewModel = null;
			}

			if (e != null)
			{
				e.Handled = true;
			}
		}

		private void SoundSetEditorView_OnLoaded(object sender, RoutedEventArgs e)
		{
			if (m_objectToEdit != null)
			{
				m_viewModel = new SoundSetEditorViewModel();
				this.DataContext = this.m_viewModel;
				m_viewModel.EditObject(m_objectToEdit);
				m_viewModel.OnObjectRefClick += this.OnObjectRefClick;
			}
		}


		#region Obsolete Event Handlers
		//THESE EVENT HANDLERS SHOULD BE CHANGED TO COMMADS 

		private void MainGrid_Selected_1(object sender, RoutedEventArgs e)
		{
			if (!m_viewModel.IsReadOnly)
			{
				DataGridCell dataGridCell = e.OriginalSource as DataGridCell;
				if (dataGridCell == null)
				{
					return;
				}
				dataGridCell.IsEditing = true;
			}
		}


		#endregion

	}
}
