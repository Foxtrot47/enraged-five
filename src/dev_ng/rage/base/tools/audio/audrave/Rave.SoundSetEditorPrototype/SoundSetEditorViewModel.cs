﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Xml;
using GalaSoft.MvvmLight.Command;
using ModelLib;
using ModelLib.CustomAttributes;
using Models;
using rage.ToolLib;
using Rave.ObjectBrowser.Infrastructure.Nodes;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.Types.Objects;
using WPFToolLib.Extensions;

namespace Rave.SoundSetEditor
{
    internal class SoundSetEditorViewModel : INotifyPropertyChanged, IDisposable
    {
        private SoundSet m_model;
        private IObjectInstance objectToEdit;

#pragma warning disable 0067
        public event Action<IObjectInstance> OnObjectEditClick;
        public event Action<IObjectInstance> OnObjectRefClick;
        public event Action<string, string> OnWaveRefClick;
        public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067

        public SoundSetEditorViewModel()
        {
            Items.CollectionChanged += Items_CollectionChanged;
        }

        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        //forwarding and rewiring model propertyChanged events
        private void m_model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsCheckedOut":
                    PropertyChanged.Raise(this, "IsReadOnly");
                    break;
                default:
                    //forwarding event
                    PropertyChanged.Raise(this, e.PropertyName);
                    break;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                if (objectToEdit != null)
                {
                    return objectToEdit.IsReadOnly;
                }
                return true;
            }
        }


        private bool _isDrag;

        public bool IsDrag
        {
            get { return _isDrag; }

            set
            {
                _isDrag = value;
                PropertyChanged.Raise(this, "IsDrag");
            }
        }


        public bool CanAddSoundSetEntry
        {
            get { return true; }
        }

        public ModelLib.ItemsObservableCollection<SoundSet.SoundsDefinition> Items
        {
            get { return m_model.Sounds; }
            set
            {
                if (!IsReadOnly)
                {
                    m_model.Sounds = value;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void EditObject(IObjectInstance objectInstance)
        {
            objectToEdit = objectInstance;


            if (objectInstance == null)
            {
                return;
            }

            if (objectToEdit != null && objectToEdit.Bank != null)
            {
                objectToEdit.Bank.BankStatusChanged -= OnBankStatusChanged;
            }

            objectToEdit = objectInstance;
            if (objectToEdit == null)
            {
                return;
            }

            if (objectToEdit.Bank != null)
            {
                objectToEdit.Bank.BankStatusChanged += OnBankStatusChanged;
            }


            if (objectToEdit.Node != null && objectToEdit.Node.ChildNodes.Count > 0)
            {
                m_model = new SoundSet();
                foreach (XmlNode childNode in objectToEdit.Node.ChildNodes)
                {
                    var soundsDefinition = new SoundSet.SoundsDefinition();
                    try
                    {
                        if (m_model != null && childNode != null && childNode.Name.Equals("Sounds"))
                        {
                            foreach (XmlNode valueNode in childNode.ChildNodes)
                            {
                                if (valueNode.Name.Equals("Name")) soundsDefinition.Name = valueNode.InnerText;
                                if (valueNode.Name.Equals("Sound"))
                                    soundsDefinition.Sound.Reference = valueNode.InnerText;
                                if (valueNode.Name.Equals("Comments")) soundsDefinition.Comments = valueNode.InnerText;
                            }
                            m_model.Sounds.Add(soundsDefinition);
                        }
                    }
                    catch
                    {
                    }
                }

                MemberInfo info = m_model.Sounds.GetType();
                var maxSize =
                    (MaxSizeAttribute)
                        info.GetCustomAttributes(true)
                            .FirstOrDefault(p => p.GetType() == typeof (MaxSizeAttribute));
                if (maxSize != null)
                {
                    CanAddObjectSetEntry = Items.Count < (int) maxSize.Value;
                }
            }

            m_model.PropertyChanged -= m_model_PropertyChanged;
            m_model.PropertyChanged += m_model_PropertyChanged;
            PropertyChanged.Raise(this, "Items");
        }

        private void OnBankStatusChanged()
        {
            PropertyChanged.Raise(this, "IsReadOnly");
        }

        public void Dispose()
        {
            if (m_model != null)
            {
                m_model.PropertyChanged -= m_model_PropertyChanged;
                m_model = null;
            }
        }


        private ICommand _addNewSoundCommand;

        public ICommand AddNewSound
        {
            get
            {
                if (_addNewSoundCommand == null)
                {
                    _addNewSoundCommand = new RelayCommand<object>(
                        AddNewSoundAction,
                        param => !IsReadOnly
                        );
                }
                return _addNewSoundCommand;
            }
        }


        private ICommand _removeSoundCommand;

        public ICommand RemoveItem
        {
            get
            {
                if (_removeSoundCommand == null)
                {
                    _removeSoundCommand = new RelayCommand<object>(
                        param => RemoveSoundAction(param),
                        param => !IsReadOnly
                        );
                }
                return _removeSoundCommand;
            }
        }

        private void AddNewSoundAction(object parameter)
        {
            Items.Add(new SoundSet.SoundsDefinition());
        }


        public void RemoveSoundAction(object parameter)
        {
            var sound = (SoundSet.SoundsDefinition) parameter;

            Items.Remove(sound);
        }

        private ICommand _findSoundCommand;

        public ICommand FindSoundCommand
        {
            get
            {
                if (_findSoundCommand == null)
                {
                    _findSoundCommand = new RelayCommand<object>(
                        FindObjectAction,
                        param => true
                        );
                }
                return _findSoundCommand;
            }
        }

        public void FindObjectAction(object parameters)
        {
            var soundName = (string) parameters;
            if (soundName != null)
            {
                OnObjectRefClick(ObjectInstance.GetObjectMatchingName(soundName));
            }
        }

        private ICommand _droppedWav;

        public ICommand DroppedWav
        {
            get
            {
                if (_droppedWav == null)
                {
                    _droppedWav = new RelayCommand<object>(
                        param => DroppedWavAction(param),
                        param => !IsReadOnly
                        );
                }
                return _droppedWav;
            }
        }

        public void DroppedWavAction(object parameter)
        {
            var e = (DragEventArgs) parameter;


            var formats = e.Data.GetFormats();

            if (formats.Contains("rage.ToolLib.DataContainer"))
            {
                var dataContainer = (DataContainer) e.Data.GetData(typeof (DataContainer));

                if (dataContainer.Data.GetType() == typeof (SoundNode[]))
                {
                    var soundNodes = (SoundNode[]) dataContainer.Data;

                    var textBlock = e.Source as TextBlock;

                    if (textBlock != null)
                    {
                        var bindingExpression = BindingOperations.GetBindingExpression(textBlock, TextBlock.TextProperty);
                        textBlock.SetValue(TextBlock.TextProperty, soundNodes[0].Text);
                        bindingExpression.UpdateSource();
                    }
                    else
                    {
                        var button = e.Source as Button;
                        if (button == null)
                        {
                            var image = e.Source as Image;
                            if (image == null)
                            {
                                return;
                            }
                        }

                        foreach (var soundNode in soundNodes)
                        {
                            Items.Add(new SoundSet.SoundsDefinition
                            {
                                Name = null,
                                Sound = new ObjectRef {Reference = soundNode.Text}
                            });
                        }
                    }
                }
            }
        }

        private ICommand _dragEnter;

        public ICommand DragEnter
        {
            get
            {
                if (_dragEnter == null)
                {
                    return _dragEnter = new RelayCommand(
                        () => SetIsDrag(true),
                        () => !IsReadOnly
                        );
                }
                return _dragEnter;
            }
        }

        private ICommand _dragLeave;

        public ICommand DragLeave
        {
            get
            {
                if (_dragLeave == null)
                {
                    _dragLeave = new RelayCommand(
                        () => SetIsDrag(false),
                        () => !IsReadOnly
                        );
                }
                return _dragLeave;
            }
        }

        private void SetIsDrag(bool value)
        {
            IsDrag = value;
        }


        public bool CanAddObjectSetEntry { get; set; }
    }
}