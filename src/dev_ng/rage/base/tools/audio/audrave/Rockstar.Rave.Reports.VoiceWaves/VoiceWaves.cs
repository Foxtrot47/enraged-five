﻿// -----------------------------------------------------------------------
// <copyright file="VoiceWaves.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.VoiceWaves
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Linq;

    using global::Rave.Controls.Forms.Popups;

    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;

    using global::Rave.Plugins.Infrastructure.Interfaces;

    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// Voice waves report - lists naming issues with waves with a voice tag.
    /// </summary>
    public class VoiceWaves : IRAVEReportPlugin
    {
        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// The built waves depot path.
        /// </summary>
        private string builtWavesDepotPath;

        /// <summary>
        /// The built waves local path.
        /// </summary>
        private string builtWavesLocalPath;

        /// <summary>
        /// The built waves pack list file name.
        /// </summary>
        private string builtWavesPackListFileName;

        /// <summary>
        /// The built waves pack file names.
        /// </summary>
        private HashSet<string> builtWavesPackFileNames;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private IAssetManager assetManager;

        /// <summary>
        /// The wave versions.
        /// </summary>
        private IDictionary<string, IList<uint>> waveVersions;

        /// <summary>
        /// The errors.
        /// </summary>
        private IList<string> errors; 

        /// <summary>
        /// Get the report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;

                    case "BuiltWaves":
                        foreach (XmlNode childNode in setting.ChildNodes)
                        {
                            switch (childNode.Name)
                            {
                                case "DepotPath":
                                    this.builtWavesDepotPath = childNode.InnerText;
                                    break;

                                case "PackListFileName":
                                    this.builtWavesPackListFileName = childNode.InnerText;
                                    break;
                            }
                        }

                        break;
                }
            }

            if (string.IsNullOrEmpty(this.name))
            {
                throw new Exception("Report name required");
            }

            if (string.IsNullOrEmpty(this.builtWavesDepotPath))
            {
                throw new Exception("Built waves depot path required");
            }

            // Get instance of asset manager
            this.assetManager = AssetManagerFactory.GetInstance(AssetManagerType.Perforce);

            if (null == this.assetManager)
            {
                throw new Exception("Failed to get instance of Perforce asset manager");
            }

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes (not required).
        /// </param>
        /// <param name="browsers">
        /// The sound browsers (not required).
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            this.waveVersions = new Dictionary<string, IList<uint>>();
            this.errors = new List<string>();

            // Get latest data
            if (!this.GetLatestData())
            {
                throw new Exception("Failed to get latest data required to generate report");
            }

            this.LoadPackListNames();
            this.LoadData();
            this.DisplayResults();
        }

        /// <summary>
        /// Get latest data from asset manager.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GetLatestData()
        {
            // Get latest built waves data files
            var builtWaveFolder = this.builtWavesDepotPath;
            if (!builtWaveFolder.EndsWith("/"))
            {
                builtWaveFolder += "/";
            }

            builtWaveFolder += "...";

            if (!this.assetManager.GetLatest(builtWaveFolder, false))
            {
                return false;
            }

            this.builtWavesLocalPath = this.assetManager.GetLocalPath(this.builtWavesDepotPath);

            return true;
        }

        /// <summary>
        /// Load the pack list names from the pack list file.
        /// </summary>
        private void LoadPackListNames()
        {
            this.builtWavesPackFileNames = new HashSet<string>();

            // Obtain pack list file path
            var packListPath = Path.Combine(this.builtWavesLocalPath, this.builtWavesPackListFileName);

            if (!File.Exists(packListPath))
            {
                throw new Exception("Failed to find built waves pack list file: " + packListPath);
            }

            // Load pack list file
            var file = XDocument.Load(packListPath);
            foreach (var packFileElem in file.Descendants("PackFile"))
            {
                this.builtWavesPackFileNames.Add(packFileElem.Value);
            }
        }

        /// <summary>
        /// Load the data.
        /// </summary>
        private void LoadData()
        {
            foreach (var fileName in this.builtWavesPackFileNames)
            {
                var filePath = Path.Combine(this.builtWavesLocalPath, fileName);
                var file = XDocument.Load(filePath);

                // Locate and waves with voice tags
                foreach (var waveElem in file.Descendants("Wave"))
                {
                    var voiceTagFound = false;
                    var voiceTagElem = this.GetVoiceTagElement(waveElem);

                    if (null != voiceTagElem)
                    {
                        var valueAttr = voiceTagElem.Attribute("value");
                        if (null != valueAttr && !string.IsNullOrEmpty(valueAttr.Value))
                        {
                            voiceTagFound = true;
                        }
                    }

                    if (voiceTagFound)
                    {
                        var waveName = waveElem.Attribute("name").Value;
                        var waveNameNoExt = Path.GetFileNameWithoutExtension(waveName);
                        var versionStr = waveNameNoExt.Substring(waveNameNoExt.LastIndexOf('_') + 1);

                        versionStr = Regex.Replace(versionStr, ".PROCESSED", string.Empty, RegexOptions.IgnoreCase);
                        uint version;

                        if (!uint.TryParse(versionStr, out version))
                        {
                            var wavePath = Path.Combine(this.GetElemPath(waveElem.Parent), waveName);
                            this.errors.Add("Failed to find version in wave name: " + wavePath);
                        }
                        else
                        {
                            var waveNameNoVersion = waveNameNoExt.Substring(0, waveNameNoExt.LastIndexOf('_'));
                            if (waveName.Contains(".PROCESSED"))
                            {
                                waveNameNoVersion += ".PROCESSED";
                            }

                            var wavePath = Path.Combine(this.GetElemPath(waveElem.Parent), waveNameNoVersion);

                            if (!this.waveVersions.ContainsKey(wavePath))
                            {
                                this.waveVersions[wavePath] = new List<uint>();
                            }

                            this.waveVersions[wavePath].Add(version);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get the closest voice tag element in the hierarchy
        /// for the given element. Stop when reached the bank node.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// The voice tag element, if found <see cref="XElement"/>.
        /// </returns>
        private XElement GetVoiceTagElement(XElement element)
        {
            if (null == element)
            {
                return null;
            }

            foreach (var tagElem in element.Elements("Tag"))
            {
                var tagName = tagElem.Attribute("name").Value;

                // Find the voice tag, if it exists
                if (tagName.ToLower() == "voice")
                {
                    return tagElem;
                }
            }

            if (element.Name.LocalName.ToLower() != "bank")
            {
                return this.GetVoiceTagElement(element.Parent);
            }

            return null;
        }

        /// <summary>
        /// Get the element path using name attributes.
        /// </summary>
        /// <param name="elem">
        /// The element.
        /// </param>
        /// <returns>
        /// The element path <see cref="string"/>.
        /// </returns>
        private string GetElemPath(XElement elem)
        {
            var nameAttr = elem.Attribute("name");
            var elemName = null != nameAttr ? nameAttr.Value : string.Empty;
            return null != elem.Parent ? Path.Combine(this.GetElemPath(elem.Parent), elemName) : elemName;
        }

        /// <summary>
        /// Display the results.
        /// </summary>
        private void DisplayResults()
        {
            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Voice Waves Report</title></head><body>");
            sw.WriteLine("<h2>Waves With Invalid Sequence Numbers</h2>");

            foreach (var kvp in this.waveVersions)
            {
                var waveName = kvp.Key;
                var versions = kvp.Value;
                ((List<uint>)versions).Sort();

                var counter = 1;

                var versionErrors = new List<string>();

                foreach (var version in versions)
                {
                    if (version != counter)
                    {
                        versionErrors.Add("Missing version " + counter);
                    }

                    counter++;
                }

                if (!versionErrors.Any())
                {
                    continue;
                }

                sw.WriteLine("<h4>" + waveName + "</h4>");
                sw.WriteLine("<ul>");

                foreach (var error in versionErrors)
                {
                    sw.WriteLine("<li>" + error + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            if (this.errors.Any())
            {
                sw.WriteLine("<h3>General Errors</h3>");
                sw.WriteLine("<ul>");

                foreach (var error in this.errors)
                {
                    sw.WriteLine("<li>" + error + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            sw.WriteLine("</body></html>");

            sw.Close();
            filesteam.Close();

            // Display report
            var report = new frmReport(this.name + " Report");
            report.SetURL(resultsFileName);
            report.Show();
        }
    }
}
