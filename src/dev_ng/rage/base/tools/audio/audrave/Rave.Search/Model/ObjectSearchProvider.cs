﻿using System;
using System.Collections.Generic;
using Rave.SearchProvider.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.ObjectBrowser.Infrastructure.Nodes;
using Rave.Instance;
using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
using rage.ToolLib;
using System.Globalization;
using System.Text.RegularExpressions;
using System.ComponentModel;
using WPFToolLib.Extensions;

namespace Rave.Search.Model
{
    class ObjectSearchProvider : ISearchProvider<SearchResult>, INotifyPropertyChanged, IDisposable
    {
        #region Properties

        //Observable collection containing search results
        public ObservableSortedSet<SearchResult> SearchResults
        {
            get
            {
                if (_searchResults == null)
                {
                    _searchResults = new ObservableSortedSet<SearchResult>(new SearchResultComparer());
                }
                return _searchResults;
            }

            private set
            {
                _searchResults = value;
                this.PropertyChanged.Raise(this, "SearchResults");
            }
        }

        public string ObjectType
        {
            get { return _objectType; }
        }

        public bool stopSearch = false;

        #endregion

        #region Fields

        private List<IObjectInstance> _objectsToSearch;

        private ObservableSortedSet<SearchResult> _searchResults;

        private bool _isReady = false;

        public bool IsReady
        {
            get { return _isReady; }
            set
            {
                _isReady = value;
                this.PropertyChanged.Raise(this, "IsReady");
            }
        }


        /// <summary>
        /// The object type.
        /// </summary>
        private readonly string _objectType;


        /// <summary>
        /// The root search node.
        /// </summary>
        private readonly BaseNode _rootSearchNode;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for Objet search provider throwing exception if there is not enough information given
        /// </summary>
        /// <param name="objectType">
        /// Object Type
        /// </param>
        /// <param name="rootSearchNode">
        /// Root search node
        /// </param>
        public ObjectSearchProvider(string objectType = null, BaseNode rootSearchNode = null)
        {
            if ((string.IsNullOrEmpty(objectType) && null == rootSearchNode) ||
               (!string.IsNullOrEmpty(objectType) && null != rootSearchNode))
            {
                throw new Exception("Either objectType or rootSearchNode must be specified");
            }

            if (null != objectType)
            {
                this._objectType = objectType;
            }

            this._rootSearchNode = rootSearchNode;

            _objectsToSearch = GetObjectList();
        }

        #endregion

        #region PublicMethods

        public ObservableSortedSet<SearchResult> Search(string searchString, SearchArguments args)
        {
            //Reset Search results
			SearchResults = new ObservableSortedSet<SearchResult>(new SearchResultComparer());
            this.IsReady = false;

            //Check if caseSensitive flag is checked
            if (!args.IsCaseSensitive)
            {
                searchString = searchString.ToUpper();
            }

            //Check if search hashname is checked
            if (args.SearchNameHash)
            {
                uint hashInt;
                if (!uint.TryParse(searchString, NumberStyles.Integer, CultureInfo.InvariantCulture, out hashInt))
                {
                    if (searchString.StartsWith("0x") || searchString.StartsWith("0X"))
                    {
                        searchString = searchString.Substring(2);
                    }

                    if (!uint.TryParse(searchString, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out hashInt))
                    {
                        this.IsReady = true;
                        return null;
                    }
                }

                return SearchNameHash(hashInt);
            }

            //Search through Objects to search
            foreach (IObjectInstance objectInstance in _objectsToSearch)
            {
                if (stopSearch)
                {
                    Dispose();
                    return SearchResults;
                }
                //Gets node text either from Name or OuterXml
                string nodeText = args.ShouldOnlySearchNames ? objectInstance.Name
                                   : objectInstance.Node.OuterXml;

                //If case insesitive
                if (!args.IsCaseSensitive)
                {
                    nodeText = nodeText.ToUpper();
                }

                //If exact name there should be exactly equal to add to hits
                if (args.ExactNameMatch)
                {
                    if (objectInstance.Name.Equals(searchString))
                    {
                        SearchResults.Add(new SearchResult(objectInstance.Name, objectInstance.Episode));
                    }
                }
                else if (args.MultiTermSearch)
                {
                    bool match = true;
                    foreach (string term in searchString.Split(new char[] { ' ' }))
                    {
                        if (!nodeText.Contains(term))
                        {
                            match = false;
                        }
                    }

                    if (match == true)
                    {
                        SearchResults.Add(new SearchResult(objectInstance.Name, objectInstance.Episode));
                    }
                }
                else if (args.RegexMode)
                {
                    if (Regex.IsMatch(nodeText, searchString))
                    {
                        SearchResults.Add(new SearchResult(objectInstance.Name, objectInstance.Episode));
                    }
                }
                else if (nodeText.Contains(searchString))
                {
                    //Add to Search results if it contains the string
                    SearchResults.Add(new SearchResult(objectInstance.Name, objectInstance.Episode));
                }

            }
            this.IsReady = true;
            return SearchResults;
        }

        #endregion

        #region PrivateMethods

        private ObservableSortedSet<SearchResult> SearchNameHash(uint hash)
        {
            foreach (IObjectInstance objectInstance in _objectsToSearch)
            {
                if (stopSearch)
                {
                    Dispose();

                    return SearchResults;
                }

                //Check if namehash equals

                if (objectInstance.NameHash.Equals(hash))
                {
                    SearchResults.Add(new SearchResult(objectInstance.Name, objectInstance.Episode));
                }
            }
            this.IsReady = true;
            return SearchResults;
        }

        /// <summary>
        /// Initialize the object list.
        /// </summary>
        private List<IObjectInstance> GetObjectList()
        {
            List<IObjectInstance> objectInstances = new List<IObjectInstance>();

            if (null == this._rootSearchNode)
            {
                foreach (IObjectBank bank in RaveInstance.AllBankManagers[this._objectType].ObjectBanks)
                {
                    objectInstances.AddRange(bank.ObjectInstances);
                }
            }
            else
            {
                List<IObjectInstance> list = GetObjectList(this._rootSearchNode);
                objectInstances.AddRange(list);
            }
            return objectInstances;
        }


        /// <summary>
        /// Get the object list.
        /// </summary>
        /// <param name="rootNode">
        /// The root node.
        /// </param>
        /// <returns>
        /// The list of objects to search <see cref="List{T}"/>.
        /// </returns>
        private static List<IObjectInstance> GetObjectList(BaseNode rootNode)
        {
            List<IObjectInstance> list = new List<IObjectInstance>();
            if (rootNode.GetType() == typeof(SoundNode))
            {
                list.Add(((SoundNode)rootNode).ObjectInstance);
            }

            foreach (BaseNode child in rootNode.Nodes)
            {
                list.AddRange(GetObjectList(child));
            }

            return list;
        }

        #endregion





        public event PropertyChangedEventHandler PropertyChanged;

        public void Dispose()
        {
            this.SearchResults.Clear();


        }
    }
}
