﻿using System;
using System.Collections;

namespace Rave.Search.Model
{
	public class SearchResult
    {
        public string Name { get; private set; }
        public string Episode { get; private set; }


        public SearchResult(string name, string episode)
        {
            Name = name;
            Episode = episode;
        }

        public SearchResult(string name)
        {
            Name = name;
        }

	  
		
	}
}
