﻿using rage.ToolLib;
using Rave.SearchProvider.Infrastructure.Interfaces;
using Rave.UIControls;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Rave.Search.Model
{
    public enum SearchProvider
    {
        Wave,
        Object,
        Both
    }

    public class SearchModel
    {
        #region Private Properties
        private string _searchTerm;

        private SearchArguments _searchArguments;

        private SearchProvider _searchProvider;

        #endregion

        #region Public Properties
        public string SearchTerm
        {
            get { return _searchTerm; }
            set
            {
                _searchTerm = value;
            }
        }

        public SearchArguments SearchArguments
        {
            get { return _searchArguments; }
            set
            {
                _searchArguments = value;
            }
        }


        public SearchProvider SearchProvider
        {
            get { return _searchProvider; }
            set
            {
                _searchProvider = value;
            }
        }
        #endregion


    }
}
