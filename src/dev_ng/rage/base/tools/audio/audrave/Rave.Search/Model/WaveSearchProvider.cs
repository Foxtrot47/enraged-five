﻿// -----------------------------------------------------------------------
// <copyright file="WaveSearchProvider.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Search.Model
{
    using Rave.SearchProvider.Infrastructure.Interfaces;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Collections.ObjectModel;
    using System.Xml.Linq;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.Utils;
    using System.IO;
    using rage.ToolLib;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using WPFToolLib.Extensions;
    using System.ComponentModel;
    using System;

    class WaveSearchProvider : ISearchProvider<SearchResult>, INotifyPropertyChanged, IDisposable
    {

        #region Public Properties

        public ObservableSortedSet<SearchResult> SearchResults
        {
            get
            {
                if (_searchResults == null)
                {
					_searchResults = new ObservableSortedSet<SearchResult>(new SearchResultComparer());
                }
                return _searchResults;
            }

            private set
            {
                _searchResults = value;
                this.PropertyChanged.Raise(this, "SearchResults");
            }
        }

        public string ObjectType
        {
            get { return (_elementType + "s"); }
        }

        public bool IsReady
        {
            get { return _isReady; }
            set
            {
                _isReady = value;
                this.PropertyChanged.Raise(this, "IsReady");
            }
        }

        public bool stopSearch = false;
        #endregion

        #region Fields And Constants

        private ObservableSortedSet<SearchResult> _searchResults;
        private EditorTreeNode _startNode;
        private string _elementType;

        private bool _isReady = false;



        /// The built waves directory name.
        /// </summary>
        private const string BUILT_WAVES_DIR_NAME = "BuiltWaves";

        /// <summary>
        /// The built waves pack list file name.
        /// </summary>
        private const string BUILT_WAVES_PACK_LIST_FILENAME = "BuiltWaves_PACK_LIST.xml";

        /// <summary>
        /// The wave file element
        /// </summary>
        private const string WAVE_ELEMENT = "Wave";

        /// <summary>
        /// The name mask.
        /// </summary>
        private const int NameMask = (1 << 29) - 1;


        #endregion

        #region Constructors

        public WaveSearchProvider(EditorTreeNode node = null, string elementType = WAVE_ELEMENT)
        {
            this._startNode = node;
            this._elementType = elementType;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Main search function to search for Waves
        /// </summary>
        /// <param name="searchString">
        /// The string to search for
        /// </param>
        /// <param name="searchNameHash">
        /// Should we search for hash
        /// </param>
        /// <param name="shouldOnlySearchNames">
        /// should we search only for names (not yet implemented)
        /// </param>
        /// <param name="isCaseSensitive">
        /// is the search case sensitive
        /// </param>
        /// <param name="exactNameMatch">
        /// are we searching for an exact name
        /// </param>
        /// <returns>
        /// An observable hash set of type string with the results
        /// </returns>
        public ObservableSortedSet<SearchResult> Search(string searchString, SearchArguments args)
        {


            if (!args.IsCaseSensitive)
            {
                searchString = searchString.ToUpper();
            }

            if (args.SearchNameHash)
            {
                return WaveSearchHashedName(searchString);
            }
            else
            {
                return WaveSearch(searchString, this._elementType, this._startNode, args.ExactNameMatch, args.IsCaseSensitive, args.MultiTermSearch, args.RegexMode);
            }


        }

        #endregion

        #region Private Methods

        /// <summary>
        /// The wave search hashed name.
        /// </summary>
        /// <param name="hash">
        /// The hash.
        /// </param>
        /// <returns>
        /// ObservableSortedSet of type string<see cref="bool"/>.
        /// </returns>
        private ObservableSortedSet<SearchResult> WaveSearchHashedName(string hash)
        {


            //Reset Search results
			SearchResults = new ObservableSortedSet<SearchResult>(new SearchResultComparer());
            this.IsReady = false;
            SearchResults.Clear();

            uint hashInt;
            if (!uint.TryParse(hash, NumberStyles.Integer, CultureInfo.InvariantCulture, out hashInt))
            {

                if (hash.StartsWith("0x") || hash.StartsWith("0X"))
                {
                    hash = hash.Substring(2);
                }
                if (!uint.TryParse(hash, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out hashInt))
                {
                    this.IsReady = true;
                    return null;
                }
            }

            // Mask off the upper three bits if 32-bit hash provided
            hashInt = hashInt & NameMask;

            foreach (var platformSetting in Configuration.ActivePlatformSettings)
            {
                var packListPath = Path.Combine(platformSetting.BuildInfo, BUILT_WAVES_DIR_NAME, BUILT_WAVES_PACK_LIST_FILENAME);
                var packListDoc = XDocument.Load(packListPath);

                foreach (var packFileElem in packListDoc.Descendants("PackFile"))
                {
                    var packFilePath = Path.Combine(platformSetting.BuildInfo, BUILT_WAVES_DIR_NAME, packFileElem.Value);
                    WaveSearchHashedName(hashInt, packFilePath);
                }
            }

            this.IsReady = true;
            return SearchResults;
        }


        /// <summary>
        /// Search for waves with their hash name
        /// </summary>
        /// <param name="hash">
        /// the hash to search for
        /// </param>
        /// <param name="filePath">
        /// the file path of the xml
        /// </param>
        private void WaveSearchHashedName(uint hash, string filePath)
        {
            XDocument doc = XDocument.Load(filePath);
            IEnumerable<XElement> waveNodes = doc.Descendants(WAVE_ELEMENT);
            ObservableSortedSet<SearchResult> results = new ObservableSortedSet<SearchResult>();

            foreach (var node in waveNodes)
            {
                if (stopSearch)
                {
                    Dispose();
                    return;
                }

                var nameHashAttr = node.Attribute("nameHash");
                if (null == nameHashAttr)
                {
                    continue;
                }

                var waveNameHash = int.Parse(nameHashAttr.Value) & NameMask;
                if (waveNameHash != hash)
                {
                    continue;
                }

                SearchResults.Add(new SearchResult(GetPath(node)));
            }
        }

        /// <summary>
        /// Search for waves
        /// </summary>
        /// <param name="searchString">
        /// the search string
        /// </param>
        /// <param name="elementType">
        /// the element type
        /// </param>
        /// <param name="startNode">
        /// the start node
        /// </param>
        /// <param name="exactNameMatch">
        /// should it match exactly
        /// </param>
        /// <param name="isCaseSensitive">
        /// is the search case sensitive
        /// </param>
        /// <returns>
        /// An observable hash set with the search results
        /// </returns>
        private ObservableSortedSet<SearchResult> WaveSearch(string searchString, string elementType, EditorTreeNode startNode = null, bool exactNameMatch = false, bool isCaseSensitive = false, bool multiTermSearch = false, bool regexMode = false)
        {
            //Reset search results
            SearchResults = new ObservableSortedSet<SearchResult>(new SearchResultComparer());
            SearchResults.Clear();
            this.IsReady = false;

            // Determine which pack to search if we're searching from a particular wave node
            PackNode packNode = null;

            if (null != startNode)
            {
                packNode = EditorTreeNode.FindParentPackNode(startNode);
            }

            // Check across built waves for all platforms in case wave info differs
            foreach (rage.PlatformSetting platformSetting in Configuration.ActivePlatformSettings)
            {
                if (stopSearch)
                {
                    Dispose();
                    return SearchResults;
                }
                string packListPath = Path.Combine(platformSetting.BuildInfo, BUILT_WAVES_DIR_NAME, BUILT_WAVES_PACK_LIST_FILENAME);
                XDocument packListDoc = XDocument.Load(packListPath);

                foreach (var packFileElem in packListDoc.Descendants("PackFile"))
                {
                    string packFilePath = Path.Combine(platformSetting.BuildInfo, BUILT_WAVES_DIR_NAME, packFileElem.Value);
                    XDocument packDoc = XDocument.Load(packFilePath);
                    XElement packElem = packDoc.Descendants("Pack").First();
                    string packName = packElem.Attribute("name").Value.ToUpper();

                    // Search all packs if no startNode specified, or current pack if matches startNode pack
                    if (null == packNode || packNode.PackName.ToUpper() == packName)
                    {
                        WaveSearch(searchString, elementType, packDoc, startNode, exactNameMatch, isCaseSensitive, multiTermSearch, regexMode);
                    }
                }
            }

            this.IsReady = true;
            return SearchResults;
        }


        /// <summary>
        /// Searches for waves
        /// </summary>
        /// <param name="searchTerm">
        /// the search term to search for
        /// </param>
        /// <param name="elementType">
        /// the element type to search for
        /// </param>
        /// <param name="doc">
        /// the document to search in
        /// </param>
        /// <param name="startNode">
        /// the starting node
        /// </param>
        /// <param name="exactNameMatch">
        /// exact matching
        /// </param>
        /// <param name="isCaseSensitive">
        /// should the search be case sensitive
        /// </param>
        private void WaveSearch(string searchTerm, string elementType, XDocument doc, EditorTreeNode startNode = null, bool exactNameMatch = false, bool isCaseSensitive = false, bool multiTermSearch = false, bool regexMode = false)
        {


            // Would use XPath compare, but .NET doesn't support XPath 2.0 which has the upper-case function required
            foreach (XElement waveElem in doc.Descendants(elementType))
            {
                if (stopSearch)
                {
                    Dispose();
                    return;
                }
                string waveName = waveElem.Attribute("name").Value;

                if (!isCaseSensitive)
                {
                    waveName = waveName.ToUpper();
                }

                if (exactNameMatch)
                {
                    if (!waveName.Equals(searchTerm))
                    {
                        continue;
                    }
                }
                else
                {
                    if (multiTermSearch)
                    {
                        bool match = true;

                        foreach (string term in searchTerm.Split(new char[] { ' ' }))
                        {
                            if (!waveName.Contains(term))
                            {
                                match = false;
                            }
                        }

                        if (!match)
                        {
                            continue;
                        }
                    }
                    else if (regexMode)
                    {
                        if (!Regex.IsMatch(waveName, searchTerm))
                        {
                            continue;
                        }
                    }
                    else if (!waveName.Contains(searchTerm))
                    {
                        continue;
                    }
                }


                string wavePath = GetPath(waveElem);

                if (null != startNode)
                {
                    string startPathUpper = startNode.GetObjectPath().ToUpper();

                    if (!wavePath.ToUpper().StartsWith(startPathUpper))
                    {
                        continue;
                    }
                }

                SearchResults.Add(new SearchResult(wavePath)); ;
            }

        }

        /// <summary>
        /// Get the string path to the specified node.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// The path to the node <see cref="string"/>.
        /// </returns>
        private static string GetPath(XElement element)
        {
            var pathBuilder = new StringBuilder();
            foreach (var parent in element.AncestorsAndSelf().InDocumentOrder())
            {
                if (parent.Attribute("name") == null)
                {
                    continue;
                }

                // Don't want backslash at start of path
                if (!string.IsNullOrEmpty(pathBuilder.ToString()))
                {
                    pathBuilder.Append("\\");
                }

                pathBuilder.Append(parent.Attribute("name").Value);
            }

            return pathBuilder.ToString().ToUpper();
        }


        #endregion


        public event PropertyChangedEventHandler PropertyChanged;

        public void Dispose()
        {
            this.SearchResults.Clear();

        }


    }
}
