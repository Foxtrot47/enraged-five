﻿using System;
using System.Collections.Generic;
using rage.ToolLib;
using System.ComponentModel;
using Rave.Search.Model;
using Rave.Utils;
using rage;
using System.Threading;
using Rave.SearchProvider.Infrastructure.Interfaces;
using System.Windows.Input;
using System.Windows.Media;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using GalaSoft.MvvmLight.Command;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.Types.Objects;
using Rave.Instance;

namespace Rave.Search.ViewModel
{
	public class SearchViewModel : INotifyPropertyChanged, IDisposable
	{
		private ObservableCollection<ISearchProvider<SearchResult>> _searchResults;

		private SearchModel _searchModel;

		private List<Thread> _threads = new List<Thread>();

		public SearchModel SearchModel
		{
			get { return _searchModel; }
		}

		private bool _isReady;

		public bool IsReady
		{
			get
			{
				return _isReady;
			}
			set
			{
				_isReady = value;
				this.OnPropertyChanged("IsReady");
			}
		}

		public event Action<string, string, string> FindObject;

		public ObservableCollection<ISearchProvider<SearchResult>> SearchResults
		{
			get { return _searchResults; }
			set
			{
				_searchResults = value;
				OnPropertyChanged("SearchResults");
			}
		}

		public SearchViewModel(SearchModel searchModel)
		{
			Search(searchModel);
		}

		public SearchViewModel()
		{
			_searchResults = new ObservableCollection<ISearchProvider<SearchResult>>();
		}

		public void Search(SearchModel searchModel)
		{
			_searchModel = searchModel;

			SearchResults = new ObservableCollection<ISearchProvider<SearchResult>>();

			foreach (audMetadataType type in Configuration.MetadataTypes)
			{
				ObjectSearchProvider objectSearchProvider = new ObjectSearchProvider(type.TypeNotUpper);
				SearchResults.Add(objectSearchProvider);
				Thread thread = new Thread(() => objectSearchProvider.Search(searchModel.SearchTerm, searchModel.SearchArguments));
				thread.Name = type.TypeNotUpper + "_SearchProvider";
				objectSearchProvider.PropertyChanged += SearchPropertyChanged;
				thread.IsBackground = true;
				thread.Start();
				_threads.Add(thread);
			}

			WaveSearchProvider wavesSearchProvider = new WaveSearchProvider();
			SearchResults.Add(wavesSearchProvider);
			Thread waveThread = new Thread(() => wavesSearchProvider.Search(searchModel.SearchTerm, searchModel.SearchArguments));
			waveThread.Name = "Wave_SearchProvider";
			wavesSearchProvider.PropertyChanged += SearchPropertyChanged;
			waveThread.IsBackground = true;
			waveThread.Start();
			_threads.Add(waveThread);

			WaveSearchProvider wavesFolderSearchProvider = new WaveSearchProvider(null, "WaveFolder");
			SearchResults.Add(wavesFolderSearchProvider);
			Thread waveFolderThread = new Thread(() => wavesFolderSearchProvider.Search(searchModel.SearchTerm, searchModel.SearchArguments));
			waveFolderThread.Name = "WaveFolder_SearchProvider";
			wavesFolderSearchProvider.PropertyChanged += SearchPropertyChanged;
			waveFolderThread.IsBackground = true;
			waveFolderThread.Start();
			_threads.Add(waveFolderThread);

            WaveSearchProvider wavesBankSearchProvider = new WaveSearchProvider(null, "Bank");
            SearchResults.Add(wavesBankSearchProvider);
            Thread wavesBankThread = new Thread(() => wavesBankSearchProvider.Search(searchModel.SearchTerm, searchModel.SearchArguments));
            wavesBankThread.Name = "WaveBank_SearchProvider";
            wavesBankSearchProvider.PropertyChanged += SearchPropertyChanged;
            wavesBankThread.IsBackground = true;
            wavesBankThread.Start();
            _threads.Add(wavesBankThread);

		}


		private void SearchPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "IsReady")
			{
				bool isReady = true;

				for (int i = 0; i < SearchResults.Count; i++)
				{
					ISearchProvider<SearchResult> iSearchprovider = SearchResults[i];
					if (iSearchprovider != null && iSearchprovider.IsReady == false)
					{
						isReady = false;
						break;
					}
				}


				this.IsReady = isReady;
			}
		}

		#region Dispose implementation
		public void Dispose()
		{

			this.OnDispose();
		}


		private void OnDispose()
		{
			foreach (ISearchProvider<SearchResult> isp in SearchResults)
			{

				if (isp is ObjectSearchProvider)
				{
					((ObjectSearchProvider)isp).PropertyChanged -= SearchPropertyChanged;

					((ObjectSearchProvider)isp).stopSearch = true;
					((ObjectSearchProvider)isp).SearchResults.Clear();
				}
				else if (isp is WaveSearchProvider)
				{
					((WaveSearchProvider)isp).PropertyChanged -= SearchPropertyChanged;

					((WaveSearchProvider)isp).stopSearch = true;
					((WaveSearchProvider)isp).SearchResults.Clear();
				}
			}

			this.SearchResults.Clear();
		}
		#endregion

		#region IPropertyChanged implementation
		public event PropertyChangedEventHandler PropertyChanged;

		private void OnPropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		#endregion

		#region Commands
		private ICommand _doubleClickCommand;

		public ICommand DoubleClickCommand
		{
			get
			{
				if (_doubleClickCommand == null)
				{
					_doubleClickCommand = new RelayCommand<object>(
					   param => this.FindObjectAction(param),
						param => true
							);
				}
				return _doubleClickCommand;
			}
		}

		private ICommand _keyDownCommand;
		public ICommand KeyDownCommand
		{
			get
			{
				if (_keyDownCommand == null)
				{
					_keyDownCommand = new RelayCommand<List<SearchResult>>(
					   param => KeyDownAction(param),
						param => true
							);
				}
				return _keyDownCommand;
			}
		}

		private RelayCommand<SearchResult> _auditionCommand ;
		public ICommand AuditionCommand
		{
			get
			{
				if (_auditionCommand == null)
				{
					_auditionCommand = new RelayCommand<SearchResult>(AuditionSearchResult);
				}
				return _auditionCommand;
			}
		}


		private RelayCommand<List<SearchResult>> _copyCommand;
		public ICommand CopyCommand
		{
			get
			{
				if (_copyCommand == null)
				{
					_copyCommand = new RelayCommand<List<SearchResult>>(SetNamesToClipBoard);
				}
				return _copyCommand;
			}
		}

		private void KeyDownAction(List<SearchResult> searchResult)
		{
			if (Keyboard.IsKeyDown(Key.Space))
			{
				AuditionSearchResult(searchResult[0]);
			}
			if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control) && Keyboard.IsKeyDown(Key.C))
			{
				SetNamesToClipBoard(searchResult);
			}
		}

		private void SetNamesToClipBoard(List<SearchResult> searchResult)
		{
			Clipboard.SetText(String.Join(Environment.NewLine, searchResult.Select(p => p.Name)));
		}

		private void AuditionSearchResult(SearchResult searchResult)
		{
			IObjectInstance sound = ObjectInstance.GetObjectMatchingName(searchResult.Name);
			if (sound != null)
			{
				RaveInstance.RemoteControl.PlaySound(sound.Name);
				RaveInstance.RemoteControl.StartAuditioningObject(
					new Hash { Value = sound.Type }.Key, new Hash { Value = sound.Name }.Key);
			}
		}

		public void FindObjectAction(object parameters)
		{
			MultiBindConvertor multibindConvertor = new MultiBindConvertor();

			string[] splitParameters = (string[])multibindConvertor.ConvertBack(parameters);
			FindObject(splitParameters[0], splitParameters[1], splitParameters[2]);
		}

		#endregion


	}
}
