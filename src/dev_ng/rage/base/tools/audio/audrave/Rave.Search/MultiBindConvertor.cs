﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Rave.Search
{
    class MultiBindConvertor : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string name;
            name = values[0] + " " + values[1] + " " + values[2];
            return name;
        }

        public object[] ConvertBack(object value, Type[] targetTypes = null, object parameter = null, System.Globalization.CultureInfo culture = null)
        {
            string[] splitValues = ((string)value).Split(' ');
            return splitValues;
        }
    }
}
