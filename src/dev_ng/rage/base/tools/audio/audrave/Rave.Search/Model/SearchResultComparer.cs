﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rave.Search.Model
{
	internal class SearchResultComparer : IComparer<SearchResult>
	{
		public int Compare(SearchResult x, SearchResult y)
		{
			if (x.Episode != y.Episode && x.Name.Equals(y.Name, StringComparison.InvariantCultureIgnoreCase))
			{
				return String.Compare(x.Episode, y.Episode, StringComparison.InvariantCultureIgnoreCase);
			}

			return String.Compare(x.Name, y.Name, StringComparison.InvariantCultureIgnoreCase);
		}
	}
}
