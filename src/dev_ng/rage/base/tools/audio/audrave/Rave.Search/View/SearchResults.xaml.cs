﻿using Rave.Search.Model;
using Rave.SearchProvider.Infrastructure.Interfaces;
using Rave.UIControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rave.Search.View
{
    /// <summary>
    /// Interaction logic for SearchResults.xaml
    /// </summary>
    public partial class SearchResults : UserControl
    {
        public SearchResults()
        {
            InitializeComponent();
            DataContext = new ViewModel.SearchViewModel();
            this.Content.Hiding += Content_Closing;
            this.Content.Closing += Content_Closing;
            
        }

        public ViewModel.SearchViewModel ViewModel
        {
            get { return this.DataContext as ViewModel.SearchViewModel; }
            set { this.DataContext = value; }
        }


        public SearchResults(SearchEventArgs e)
        {
            InitializeComponent();

            bool nameHash = e.SearchArguments.Contains("Search Name Hash");
            bool searchNames = e.SearchArguments.Contains("Search Names");
            bool caseSensitive = e.SearchArguments.Contains("Case Sensitive");
            bool exactNameMatch = e.SearchArguments.Contains("Exact Name Match");
            bool multiTermSearch = e.SearchArguments.Contains("Multi Term Search");
            bool regexMode = e.SearchArguments.Contains("Use Regular Expressions");

            if (nameHash)
            {
                exactNameMatch = true;
            }

            SearchArguments searchArguments = new SearchArguments(nameHash, searchNames, caseSensitive, exactNameMatch, multiTermSearch, regexMode);
            DataContext = new ViewModel.SearchViewModel(new SearchModel() { SearchArguments = searchArguments, SearchTerm = e.Keyword });
            this.Content.Hiding += Content_Closing;
            this.Content.Closing += Content_Closing;
            
        }

           



        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_DockableContent;

        public new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable Content
        {
            get
            {
                return this.m_DockableContent
                       ?? (this.m_DockableContent =
                           new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
                           {
                               Content = this,
                               FloatingWidth = 300,
                               FloatingHeight = 600,
                               ToolTip = "Search Results",
                               ContentId = "SearchResults",
                               Title = "Search Results",
                               AutoHideWidth = 250,


                           });
            }
        }

        public void Activate()
        {
            this.Content.Show();

        }

        private void Content_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.ViewModel.Dispose();
        }


	    private void ListViewResult_OnPreviewKeyDown(object sender, KeyEventArgs e)
	    {
		    ListView listView = sender as ListView;
		    if (e.Key == Key.Space)
		    {
			    if (listView != null) ViewModel.AuditionCommand.Execute(listView.SelectedItem);
		    }

			if (e.Key == Key.C  && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
			{
				if (listView != null) ViewModel.CopyCommand.Execute(listView.SelectedItems.Cast<SearchResult>().ToList());
			}
	    }
    }
}
