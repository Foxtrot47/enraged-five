namespace Rave.PropertiesEditor.Infrastructure.Interfaces
{
    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    public interface IObjectRef
    {
        void Drop(SoundNode soundNode);

        void Drop(WaveNode waveNode);
    }
}