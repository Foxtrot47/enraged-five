namespace Rave.PropertiesEditor.Infrastructure.Interfaces
{
    using System;
    using System.Windows.Input;

    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;

    public interface IPropertiesEditor
    {
        #region Public Events

        /// <summary>
        /// The audition sound.
        /// </summary>
        event Action<IObjectInstance> AuditionSound;

        /// <summary>
        /// The on object ref click.
        /// </summary>
        event Action<IObjectInstance> OnObjectRefClick;

        /// <summary>
        /// The on template ref click.
        /// </summary>
        event Action<ITemplate> OnTemplateRefClick;

        /// <summary>
        /// The on wave bank ref click.
        /// </summary>
        event Action<string> OnWaveBankRefClick;

        /// <summary>
        /// The on wave ref audition.
        /// </summary>
        event Action<string, string> OnWaveRefAudition;

        /// <summary>
        /// The on wave ref click.
        /// </summary>
        event Action<string, string> OnWaveRefClick;

        /// <summary>
        /// The stop sound audition.
        /// </summary>
        event Action<IObjectInstance> StopSoundAudition;

        /// <summary>
        /// The create floating editor click.
        /// </summary>
        event Action<IObjectInstance> OnCreateFloatingEditorClick;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the dockable content.
        /// </summary>
        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable DockableContent { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The edit object.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <param name="mode">
        /// The mode.
        /// </param>
        /// <param name="updateHistory">
        /// The update history.
        /// </param>
        void EditObject(object obj, Mode? mode = null, bool updateHistory = true);

        /// <summary>
        /// The properties editor_ key down.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="keyEventArgs">
        /// The key event args.
        /// </param>
        void PropertiesEditor_KeyDown(object sender, KeyEventArgs keyEventArgs);

        /// <summary>
        /// The refresh view.
        /// </summary>
        void RefreshView();

        /// <summary>
        /// The reset.
        /// </summary>
        void Reset();

        /// <summary>
        /// The update display.
        /// </summary>
        void UpdateDisplay();

        #endregion
    }
}
