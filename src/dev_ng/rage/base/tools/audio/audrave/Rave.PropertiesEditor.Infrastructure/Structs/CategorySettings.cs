﻿// -----------------------------------------------------------------------
// <copyright file="CategorySettings.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.PropertiesEditor.Infrastructure.Structs
{
    /// <summary>
    /// Category settings.
    /// </summary>
    public struct CategorySettings
    {
        #region Fields

        /// <summary>
        /// The distance roll off scale.
        /// </summary>
        public float DistanceRollOffScale;

        /// <summary>
        /// The environmental filter damping.
        /// </summary>
        public float EnvironmentalFilterDamping;

        /// <summary>
        /// The environmental loudness.
        /// </summary>
        public float EnvironmentalLoudness;

        /// <summary>
        /// The environmental reverb damping.
        /// </summary>
        public float EnvironmentalReverbDamping;

        /// <summary>
        /// The filter cutoff.
        /// </summary>
        public ushort FilterCutoff;

        /// <summary>
        /// The occlusion damping.
        /// </summary>
        public float OcclusionDamping;

        /// <summary>
        /// The pitch.
        /// </summary>
        public short Pitch;

        /// <summary>
        /// The volume.
        /// </summary>
        public float Volume;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CategorySettings"/> struct.
        /// </summary>
        /// <param name="vol">
        /// The vol.
        /// </param>
        /// <param name="dis">
        /// The dis.
        /// </param>
        /// <param name="occ">
        /// The occ.
        /// </param>
        /// <param name="envFD">
        /// The env fd.
        /// </param>
        /// <param name="envRD">
        /// The env rd.
        /// </param>
        /// <param name="envL">
        /// The env l.
        /// </param>
        /// <param name="pitch">
        /// The pitch.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        public CategorySettings(
            float vol, float dis, float occ, float envFD, float envRD, float envL, short pitch, ushort filter)
        {
            this.Volume = vol;
            this.DistanceRollOffScale = dis;
            this.OcclusionDamping = occ;
            this.EnvironmentalFilterDamping = envFD;
            this.EnvironmentalReverbDamping = envRD;
            this.EnvironmentalLoudness = envL;
            this.Pitch = pitch;
            this.FilterCutoff = filter;
        }

        #endregion
    }
}
