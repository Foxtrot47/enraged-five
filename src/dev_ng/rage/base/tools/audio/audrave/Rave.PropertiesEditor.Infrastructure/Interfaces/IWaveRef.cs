namespace Rave.PropertiesEditor.Infrastructure.Interfaces
{
    using Rave.WaveBrowser.Infrastructure.Nodes;

    public interface IWaveRef
    {
        void Drop(WaveNode wn);
    }
}