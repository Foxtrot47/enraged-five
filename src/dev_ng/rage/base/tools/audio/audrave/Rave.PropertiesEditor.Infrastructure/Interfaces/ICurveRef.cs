namespace Rave.PropertiesEditor.Infrastructure.Interfaces
{
    using Rave.ObjectBrowser.Infrastructure.Nodes;

    public interface ICurveRef
    {
        void Drop(SoundNode soundNode);
    }
}