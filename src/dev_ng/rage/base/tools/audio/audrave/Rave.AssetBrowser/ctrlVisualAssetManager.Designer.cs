using System;

namespace Rave.AssetBrowser
{
    using System.Windows.Forms;
    using System.Drawing;

    using Rave.Controls.Forms;

    partial class ctrlVisualAssetManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctrlVisualAssetManager));
            this.m_ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuNewChangelist = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSubmit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRevert = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRevertUnchanged = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDiff = new System.Windows.Forms.ToolStripMenuItem();
            this.latestRevisionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savedVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRename = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ImageList = new System.Windows.Forms.ImageList(this.components);
            this.m_TreeView = new Rave.Controls.Forms.ScrollableTreeView();
            this.m_InfoText = new RichTextBox();
            this.m_ContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_ContextMenu
            // 
            this.m_ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNewChangelist,
            this.mnuSubmit,
            this.mnuRevert,
            this.mnuRevertUnchanged,
            this.mnuDelete,
            this.mnuDiff,
            this.mnuRefresh,
            this.mnuRename});
            this.m_ContextMenu.Name = "contextMenuStrip1";
            this.m_ContextMenu.Size = new System.Drawing.Size(169, 180);
            this.m_ContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenu_Opening);
            // 
            // mnuNewChangelist
            // 
            this.mnuNewChangelist.Name = "mnuNewChangelist";
            this.mnuNewChangelist.Size = new System.Drawing.Size(168, 22);
            this.mnuNewChangelist.Text = "New Changelist";
            this.mnuNewChangelist.Click += new System.EventHandler(this.mnuNewChangelist_Click);
            // 
            // mnuSubmit
            // 
            this.mnuSubmit.Name = "mnuSubmit";
            this.mnuSubmit.Size = new System.Drawing.Size(168, 22);
            this.mnuSubmit.Text = "Submit";
            this.mnuSubmit.Click += new System.EventHandler(this.mnuSubmit_Click);
            // 
            // mnuRevert
            // 
            this.mnuRevert.Name = "mnuRevert";
            this.mnuRevert.Size = new System.Drawing.Size(168, 22);
            this.mnuRevert.Text = "Revert";
            this.mnuRevert.Click += new System.EventHandler(this.mnuRevert_Click);
            // 
            // mnuRevertUnchanged
            // 
            this.mnuRevertUnchanged.Name = "mnuRevertUnchanged";
            this.mnuRevertUnchanged.Size = new System.Drawing.Size(168, 22);
            this.mnuRevertUnchanged.Text = "RevertUnchanged";
            this.mnuRevertUnchanged.Click += new System.EventHandler(this.mnuRevertUnchanged_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Size = new System.Drawing.Size(168, 22);
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuDiff
            // 
            this.mnuDiff.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.latestRevisionToolStripMenuItem,
            this.savedVersionToolStripMenuItem});
            this.mnuDiff.Name = "mnuDiff";
            this.mnuDiff.Size = new System.Drawing.Size(168, 22);
            this.mnuDiff.Text = "Diff Against...";
            // 
            // latestRevisionToolStripMenuItem
            // 
            this.latestRevisionToolStripMenuItem.Name = "latestRevisionToolStripMenuItem";
            this.latestRevisionToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.latestRevisionToolStripMenuItem.Text = "Checked-In Version";
            this.latestRevisionToolStripMenuItem.Click += new System.EventHandler(this.mnuDiffCheckedIn_Click);
            // 
            // savedVersionToolStripMenuItem
            // 
            this.savedVersionToolStripMenuItem.Name = "savedVersionToolStripMenuItem";
            this.savedVersionToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.savedVersionToolStripMenuItem.Text = "Saved Version";
            this.savedVersionToolStripMenuItem.Click += new System.EventHandler(this.mnuDiffSaved_Click);
            // 
            // mnuRefresh
            // 
            this.mnuRefresh.Name = "mnuRefresh";
            this.mnuRefresh.Size = new System.Drawing.Size(168, 22);
            this.mnuRefresh.Text = "Refresh";
            // 
            // mnuRename
            // 
            this.mnuRename.Name = "mnuRename";
            this.mnuRename.Size = new System.Drawing.Size(168, 22);
            this.mnuRename.Text = "Rename";
            // 
            // m_ImageList
            // 
            this.m_ImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("m_ImageList.ImageStream")));
            this.m_ImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.m_ImageList.Images.SetKeyName(0, "change.ico");
            this.m_ImageList.Images.SetKeyName(1, "asset.ico");
            this.m_ImageList.Images.SetKeyName(2, "assetlock.ico");
            this.m_ImageList.Images.SetKeyName(3, "invalid.ico");
            // 
            // m_TreeView
            // 
            this.m_TreeView.AllowDrop = true;
            this.m_TreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_TreeView.ImageIndex = 0;
            this.m_TreeView.ImageList = this.m_ImageList;
            this.m_TreeView.LastSelectedNode = null;
            this.m_TreeView.LineColor = System.Drawing.Color.Empty;
            this.m_TreeView.Location = new System.Drawing.Point(0, 0);
            this.m_TreeView.Name = "m_TreeView";
            this.m_TreeView.SelectedImageIndex = 0;
            this.m_TreeView.Size = new System.Drawing.Size(0, 0);
            this.m_TreeView.Sorted = true;
            this.m_TreeView.TabIndex = 0;
            this.m_TreeView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.TreeView_ItemDrag);
            this.m_TreeView.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TreeView_NodeDoubleClick);
            this.m_TreeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.TreeView_DragDrop);
            this.m_TreeView.DragOver += new System.Windows.Forms.DragEventHandler(this.TreevView_DragOver);
            this.m_TreeView.NodeMouseClick += new TreeNodeMouseClickEventHandler(this.TreeView_MouseClick);
            this.m_TreeView.BeforeExpand += new TreeViewCancelEventHandler(this.TreeView_BeforeExpand);
            //
            // m_InfoText
            //
            this.m_InfoText.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_InfoText.Height = 80;
            this.m_InfoText.Multiline = true;
            this.m_InfoText.WordWrap = false;
            this.m_InfoText.ScrollBars = RichTextBoxScrollBars.None;
            this.m_InfoText.BorderStyle = BorderStyle.None;
            this.m_InfoText.ReadOnly = true;
            this.m_InfoText.BackColor = SystemColors.Control;
            this.m_InfoText.Hide();
            // 
            // ctrlVisualAssetManager
            // 
            this.ContextMenuStrip = this.m_ContextMenu;
            this.Controls.Add(this.m_TreeView);
            this.Controls.Add(this.m_InfoText);
            this.m_ContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip m_ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuSubmit;
        private System.Windows.Forms.ToolStripMenuItem mnuRevert;
        private System.Windows.Forms.ToolStripMenuItem mnuRevertUnchanged;
        private System.Windows.Forms.ToolStripMenuItem mnuDiff;
        private System.Windows.Forms.ToolStripMenuItem latestRevisionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savedVersionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuRename;
        private System.Windows.Forms.ImageList m_ImageList;
        private System.Windows.Forms.ToolStripMenuItem mnuNewChangelist;
        private System.Windows.Forms.ToolStripMenuItem mnuDelete;
        internal ScrollableTreeView m_TreeView;
        private ToolStripMenuItem mnuRefresh;
        private RichTextBox m_InfoText;
    }
}
