namespace Rave.AssetBrowser
{
    partial class frmCheckIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_Description = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_GridView = new System.Windows.Forms.DataGridView();
            this.m_ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuDiff = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.m_CheckAll = new System.Windows.Forms.CheckBox();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_CheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.m_ColFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.m_GridView)).BeginInit();
            this.m_ContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_Description
            // 
            this.m_Description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_Description.Location = new System.Drawing.Point(12, 23);
            this.m_Description.Multiline = true;
            this.m_Description.Name = "m_Description";
            this.m_Description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_Description.Size = new System.Drawing.Size(412, 87);
            this.m_Description.TabIndex = 0;
            this.m_Description.TextChanged += new System.EventHandler(this.Description_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Changelist description:";
            // 
            // m_GridView
            // 
            this.m_GridView.AllowUserToAddRows = false;
            this.m_GridView.AllowUserToDeleteRows = false;
            this.m_GridView.AllowUserToResizeColumns = false;
            this.m_GridView.AllowUserToResizeRows = false;
            this.m_GridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_GridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.m_CheckBox,
            this.m_ColFile});
            this.m_GridView.ContextMenuStrip = this.m_ContextMenu;
            this.m_GridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.m_GridView.Location = new System.Drawing.Point(12, 138);
            this.m_GridView.Name = "m_GridView";
            this.m_GridView.RowHeadersVisible = false;
            this.m_GridView.Size = new System.Drawing.Size(412, 220);
            this.m_GridView.TabIndex = 2;
            // 
            // m_ContextMenu
            // 
            this.m_ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDiff});
            this.m_ContextMenu.Name = "m_ContextMenu";
            this.m_ContextMenu.Size = new System.Drawing.Size(68, 26);
            // 
            // mnuDiff
            // 
            this.mnuDiff.Name = "mnuDiff";
            this.mnuDiff.Size = new System.Drawing.Size(67, 22);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Files to submit";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(268, 364);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(349, 364);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // m_CheckAll
            // 
            this.m_CheckAll.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_CheckAll.AutoSize = true;
            this.m_CheckAll.Location = new System.Drawing.Point(18, 143);
            this.m_CheckAll.Name = "m_CheckAll";
            this.m_CheckAll.Size = new System.Drawing.Size(15, 14);
            this.m_CheckAll.TabIndex = 6;
            this.m_CheckAll.UseVisualStyleBackColor = true;
            this.m_CheckAll.Click += new System.EventHandler(this.CheckAll_Click);
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 25;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "File";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // m_CheckBox
            // 
            this.m_CheckBox.HeaderText = "";
            this.m_CheckBox.Name = "m_CheckBox";
            this.m_CheckBox.Width = 25;
            // 
            // m_ColFile
            // 
            this.m_ColFile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.m_ColFile.HeaderText = "File";
            this.m_ColFile.Name = "m_ColFile";
            // 
            // frmCheckIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 399);
            this.Controls.Add(this.m_CheckAll);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.m_GridView);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_Description);
            this.Name = "frmCheckIn";
            this.Text = "Submit Changelist";
            ((System.ComponentModel.ISupportInitialize)(this.m_GridView)).EndInit();
            this.m_ContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox m_Description;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView m_GridView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn m_CheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn m_ColFile;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox m_CheckAll;
        private System.Windows.Forms.ContextMenuStrip m_ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuDiff;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    }
}