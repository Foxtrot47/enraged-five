// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssetNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The asset node.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using System.Security.Cryptography;
using Rockstar.AssetManager.Infrastructure.Data;

namespace Rave.AssetBrowser
{
    using System;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Utils;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The asset node.
    /// </summary>
    public class AssetNode : TreeNode
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AssetNode"/> class.
        /// </summary>
        /// <param name="asset">
        /// The asset.
        /// </param>
        public AssetNode(IAsset asset)
        {
            this.Asset = asset;
            this.Path = asset.LocalPath.ToUpper()
                             .Replace(System.IO.Path.AltDirectorySeparatorChar, System.IO.Path.DirectorySeparatorChar);

            this.Text =
                this.ObjectName =
                this.Path.Contains(Configuration.ObjectXmlPath.ToUpper())
                    ? this.Path.Replace(Configuration.ObjectXmlPath.ToUpper(), string.Empty)
                    : this.Path;

            this.ImageIndex = 1;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the asset.
        /// </summary>
        public IAsset Asset { get; private set; }

        /// <summary>
        /// Gets the object name.
        /// </summary>
        public string ObjectName { get; private set; }

        /// <summary>
        /// Gets the path.
        /// </summary>
        public string Path { get; private set; }

        #endregion

        public FileStatus GetFileStatus()
        {
            return RaveInstance.AssetManager.GetFileStatus(this.Asset.DepotPath);
        }

        public string GetFileStatusDescription()
        {
            FileStatus fileStatus = this.GetFileStatus();
            //string description = this.ObjectName+Environment.NewLine;
            string description = "Owner: "+fileStatus.Owner+Environment.NewLine;
            description += "Locked: " + (fileStatus.IsLocked ? "yes" : "no") + Environment.NewLine;
            description += "Locked by me: " + (fileStatus.IsLockedByMe ? "yes" : "no") + Environment.NewLine;
            description += "Open: " + (fileStatus.IsOpen ? "yes" : "no") + Environment.NewLine;
            description += "Last modified: " + fileStatus.LastModTime;
            this.ImageIndex = fileStatus.IsLocked ? 2 : 1;
            return description;
        }

        public void UpdateLockingIcon()
        {
            FileStatus fileStatus = this.GetFileStatus();
            this.ImageIndex = fileStatus.IsLocked ? 2 : 1;
        }
    }
}