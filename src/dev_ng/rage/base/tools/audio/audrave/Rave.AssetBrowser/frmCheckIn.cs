// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmCheckIn.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The frm check in.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Rave.Utils.Popups;

namespace Rave.AssetBrowser
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    using rage.ToolLib.Logging;

    using Rave.Controls.WPF.UXTimer;
    using Rave.Instance;
    using Rave.Metadata;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.ObjectBanks;
    using Rave.Utils;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The frm check in.
    /// </summary>
    public partial class frmCheckIn : Form
    {
        #region Fields

        /// <summary>
        /// The m_log.
        /// </summary>
        private readonly ILog m_log;

        /// <summary>
        /// The m_changelist.
        /// </summary>
        private IChangeList m_changelist;

        /// <summary>
        /// The m_should create new changelist.
        /// </summary>
        private bool m_shouldCreateNewChangelist;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="frmCheckIn"/> class.
        /// </summary>
        /// <param name="changeListNode">
        /// The change list node.
        /// </param>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="selectedAssets">
        /// The selected assets.
        /// </param>
        public frmCheckIn(ChangelistNode changeListNode, ILog log, IEnumerable<AssetNode> selectedAssets)
            : this(changeListNode.Changelist, log, selectedAssets.Select(asset => asset.Asset).ToList())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="frmCheckIn"/> class.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="assets">
        /// The assets.
        /// </param>
        public frmCheckIn(IChangeList changeList, ILog log, ICollection<IAsset> assets)
        {
            this.InitializeComponent();

            this.m_changelist = changeList;
            this.m_log = log;
            if (this.m_changelist != null)
            {
                this.m_Description.Text = this.m_changelist.Description.Trim();
            }

            this.btnOk.Enabled = !string.IsNullOrEmpty(this.m_Description.Text);

            if (changeList.NumberAsString == "default")
            {
                this.m_shouldCreateNewChangelist = true;
            }

            var rowIndex = 0;
            foreach (var asset in changeList.Assets)
            {
                var cbc = new DataGridViewCheckBoxCell();
                var tbc = new DataGridViewTextBoxCell();
                if (null != assets && assets.Contains(asset))
                {
                    cbc.Value = true;
                }
                else
                {
                    cbc.Value = false;
                }

                tbc.Value = asset.LocalPath;
                tbc.Tag = asset;

                this.m_GridView.Rows.Add();
                this.m_GridView[0, rowIndex] = cbc;
                this.m_GridView[1, rowIndex] = tbc;

                rowIndex++;
            }
        }

        #endregion

        /// <summary>
        /// Gets the submitted change list number.
        /// </summary>
        public string SubmittedChangeListNumber { get; private set; }

        #region Methods

        /// <summary>
        /// The save bank.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        private static void SaveBank(string path)
        {
            foreach (var kvp in RaveInstance.AllBankManagers)
            {
                foreach (var bank in kvp.Value.Banks)
                {
                    if (bank.FilePath.ToUpper() == path.ToUpper())
                    {
                        if (!bank.Save())
                        {
                            ErrorManager.HandleError("Failed to save bank: " + bank.FilePath);
                        }

                        return;
                    }
                }
            }
        }

        /// <summary>
        /// The check all_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void CheckAll_Click(object sender, EventArgs e)
        {
            for (var i = 0; i < this.m_GridView.Rows.Count; i++)
            {
                var cbc = this.m_GridView[0, i] as DataGridViewCheckBoxCell;
                if (cbc != null && !cbc.ReadOnly)
                {
                    cbc.Value = this.m_CheckAll.CheckState == CheckState.Checked ? true : false;
                }
            }
        }

        /// <summary>
        /// The description_ text changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Description_TextChanged(object sender, EventArgs e)
        {
            this.btnOk.Enabled = this.m_Description.Text.Length > 0;
        }

        /// <summary>
        /// The get checked files.
        /// </summary>
        /// <returns>
        /// The <see cref="List{T}"/>.
        /// </returns>
        private List<IAsset> GetCheckedFiles()
        {
            var assets = new List<IAsset>();

            for (var i = 0; i < this.m_GridView.Rows.Count; i++)
            {
                var cbc = this.m_GridView[0, i] as DataGridViewCheckBoxCell;
                if (cbc != null)
                {
                    if ((bool)cbc.Value)
                    {
                        var tbc = this.m_GridView[1, i] as DataGridViewTextBoxCell;
                        var a = tbc.Tag as IAsset;
                        if (a != null)
                        {
                            assets.Add(tbc.Tag as IAsset);
                            SaveBank(a.LocalPath.Replace("/", "\\"));
                        }
                    }
                }
            }

            return assets;
        }

	    /// <summary>
	    /// The btn o k_ click.
	    /// </summary>
	    /// <param name="sender">
	    /// The sender.
	    /// </param>
	    /// <param name="e">
	    /// The e.
	    /// </param>
	    private void btnOK_Click(object sender, EventArgs e)
	    {
		    this.SubmittedChangeListNumber = null;
		    this.m_Description.Text = this.m_Description.Text.Trim();
		    if (string.IsNullOrEmpty(this.m_Description.Text))
		    {
			    ErrorManager.HandleInfo("Please enter a changelist description");
			    this.btnOk.Enabled = false;
			    this.DialogResult = DialogResult.None;
			    return;
		    }

		    var formattedDescription = this.m_Description.Text.Replace("\r\n", " ");

		    // Do we need to create a new changelist?
		    var assets = this.GetCheckedFiles();
		    if (assets.Count == 0)
		    {
			    ErrorManager.HandleInfo("No files selected");
			    this.DialogResult = DialogResult.None;
			    return;
		    }

		    var timerHelper = new UXTimerHelper();
		    timerHelper.Enter("Submitting changes...");

		    if (this.m_shouldCreateNewChangelist || assets.Count < this.m_GridView.Rows.Count)
		    {
			    try
			    {
				    timerHelper.Enter("CreateNewChangelist");
				    var newChangelist = RaveInstance.AssetManager.CreateChangeList(formattedDescription);
				    foreach (var asset in assets)
				    {
					    RaveInstance.AssetManager.MoveAsset(asset, newChangelist);
				    }

				    this.m_changelist = newChangelist;
				    this.m_shouldCreateNewChangelist = false;
				    timerHelper.Exit();
			    }
			    catch (Exception ex)
			    {
				    ErrorManager.HandleError(ex);
				    timerHelper.ExitAll();
				    this.DialogResult = DialogResult.None;
				    return;
			    }
		    }
		    else
		    {
			    if (this.m_changelist.HasShelvedAssets())
			    {
				    if (
					    MessageBox.Show(
						    "Change list " + this.m_changelist.NumberAsString
						    + " has shelved changes which will be deleted on submit - do you want to continue?",
						    "Delete Shelved Changes",
						    MessageBoxButtons.OKCancel) == DialogResult.Cancel)
				    {
					    this.DialogResult = DialogResult.None;
					    timerHelper.ExitAll();
					    return;
				    }
			    }
		    }

		    if (!string.IsNullOrEmpty(formattedDescription))
		    {
			    this.m_changelist.Description = formattedDescription;
		    }
		    var compiler = new XmlBankMetaDataCompiler(this.m_log, null);
		    timerHelper.Enter("Checking assets");
		    using (frmBusy busy = new frmBusy())
			{
				busy.SetStatusText("Submitting Changelist");
				busy.SetUnknownDuration(true);
				busy.Show();
				
				
			    foreach (var asset in assets)
			    {
				    // check no resolve is needed;
				    if (!RaveInstance.AssetManager.IsMarkedForAdd(asset.DepotPath))
				    {
					    RaveInstance.AssetManager.GetLatest(asset.LocalPath, false);
					    if (!RaveInstance.AssetManager.IsResolved(asset.LocalPath))
					    {
						    ErrorManager.HandleInfo(
							    string.Format("File {0} needs resolved. Please resolve through perforce", asset.LocalPath));
						    timerHelper.ExitAll();
						    this.DialogResult = DialogResult.None;
						    return;
					    }
				    }

				    var bank = XmlBankManager.GetCheckedOutBank(asset.LocalPath);
				    var objBank = bank as IObjectBank;
				    if (objBank != null && !objBank.Validate())
				    {
					    this.DialogResult = DialogResult.None;
					    timerHelper.ExitAll();
					    return;
				    }

				    if (bank != null && !(bank is ITemplateBank))
				    {

					    if (!compiler.CompileMetadata(bank))
					    {
						    this.DialogResult = DialogResult.None;
						    timerHelper.ExitAll();
						    return;
					    }
				    }
			    }

			    try
			    {
				    timerHelper.Enter("Submitting to Perforce");
				    string changeListNumber;
				    this.m_changelist.Submit(out changeListNumber);
				    this.SubmittedChangeListNumber = changeListNumber;
			    }
			    catch (Exception ex)
			    {
				    ErrorManager.HandleError(ex);
			    }
				busy.Close();
			}
			    timerHelper.ExitAll();
		    
	    }

	    #endregion
    }
}