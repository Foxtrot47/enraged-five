namespace Rave.AssetBrowser
{
    using System.Windows.Forms;

    using Rockstar.AssetManager.Interfaces;

    public class ChangelistNode : TreeNode
    {
        public ChangelistNode(IChangeList c)
        {
            this.Changelist = c;
            this.ImageIndex = 0;
            this.Refresh();
        }

        public string ObjectName { get; private set; }

        public IChangeList Changelist { get; private set; }

        public void Refresh()
        {
            var description = string.IsNullOrEmpty(this.Changelist.Description)
                                   ? string.Empty
                                   : " - " + this.Changelist.Description;
            this.Text = this.ObjectName = this.Changelist.NumberAsString + description;
        }
    }
}