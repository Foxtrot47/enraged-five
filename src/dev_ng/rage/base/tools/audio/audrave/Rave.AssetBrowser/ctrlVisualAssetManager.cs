// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ctrlVisualAssetManager.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The ctrl visual asset manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.AssetBrowser
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;

    using rage.ToolLib;

    using Rave.AssetBrowser.Infrastructure.Interfaces;
    using Rave.Controls.Forms;
    using Rave.Controls.Forms.Popups;
    using Rave.Controls.WPF.UXTimer;
    using Rave.Instance;
    using Rave.Metadata;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Utils;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The ctrl visual asset manager.
    /// </summary>
    public partial class ctrlVisualAssetManager : DockableWFControl, IVisualAssetManager
    {
        /// <summary>
        /// The instance.
        /// </summary>
        public static readonly ctrlVisualAssetManager Instance;

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="ctrlVisualAssetManager"/> class.
        /// </summary>
        static ctrlVisualAssetManager()
        {
            if (null == Instance)
            {
                Instance = new ctrlVisualAssetManager();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ctrlVisualAssetManager"/> class.
        /// </summary>
        public ctrlVisualAssetManager()
        {
            this.InitializeComponent();
            this.m_TreeView.Sorted = false;
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The bank selected.
        /// </summary>
        public event Action<string> BankSelected;

        /// <summary>
        /// The bank status changed string list.
        /// </summary>
        public event Action<List<string>> BankStatusChangedStringList;

        /// <summary>
        /// The banks status changed.
        /// </summary>
        public event Action BanksStatusChanged;

        /// <summary>
        /// The change list status change.
        /// </summary>
        public event Action<string> ChangeListStatusChange;

        /// <summary>
        /// The change list submitted event (pending change list number, submitted change list number).
        /// </summary>
        public event Action<string, string> ChangeListSubmitted;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The diff checked in.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="bank">
        /// The bank.
        /// </param>
        public void DiffCheckedIn(string path, IXmlBank bank)
        {
            var originalFile = Path.GetTempPath()
                               + path.Substring(path.IndexOf("\\") + 1).ToUpper().Replace(".XML", "_ORIG.XML");

            var sb = new StringBuilder();

            DiffFiles(originalFile, bank, sb);
            var report = new frmReport("Diff: " + path);
            report.SetText(sb.ToString());
            report.Show();
        }

        /// <summary>
        /// The diff saved.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="bank">
        /// The bank.
        /// </param>
        public void DiffSaved(string path, IXmlBank bank)
        {
            var sb = new StringBuilder();
            DiffFiles(path, bank, sb);
            var report = new frmReport("Diff: " + path);
            report.SetText(sb.ToString());
            report.Show();
        }

        /// <summary>
        /// The init.
        /// </summary>
        public void Init()
        {
            this.m_TreeView.BeginUpdate();
            foreach (var c in RaveInstance.AssetManager.ChangeLists.Values)
            {
                this.CreateNode(c);
            }

            this.m_TreeView.EndUpdate();

            this.mnuRefresh.Click += this.mnuRefresh_Click;
        }

        /// <summary>
        /// The refresh view.
        /// </summary>
        public void RefreshView()
        {
            var timer = new UXTimerHelper();
            timer.Enter("ctrlVisualAssetManager.RefreshView");

            this.m_TreeView.BeginUpdate();

            this.m_TreeView.Nodes.Clear();

            timer.Enter("AssetManager.RefreshView");
            RaveInstance.AssetManager.RefreshView();
            timer.Exit();

            this.Init();

            this.m_TreeView.EndUpdate();

            if (this.BankStatusChangedStringList != null)
            {
                var paths = new List<string>();
                foreach (TreeNode tn in this.m_TreeView.Nodes)
                {
                    var objType = tn.GetType();
                    if (objType == typeof(ChangelistNode))
                    {
                        paths.AddRange(tn.Nodes.OfType<AssetNode>().Select(assetNode => assetNode.Path));
                    }
                    else
                    {
                        var assetNode = tn as AssetNode;
                        if (assetNode != null)
                        {
                            paths.Add(assetNode.Path);
                        }
                    }
                }

                this.BankStatusChangedStringList(paths);
            }

            timer.ExitAll();
        }

        /// <summary>
        /// Check in a change list.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="selectedAssets">
        /// The selected assets.
        /// </param>
        public void CheckIn(IChangeList changeList, IList<IAsset> selectedAssets)
        {
            var checkin = new frmCheckIn(changeList, RaveInstance.RaveLog, selectedAssets);
            if (checkin.ShowDialog() == DialogResult.OK)
            {
                this.RefreshView();
                this.BanksStatusChanged.Raise();
                this.ChangeListSubmitted.Raise(changeList.NumberAsString, checkin.SubmittedChangeListNumber);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The compare xml objects.
        /// </summary>
        /// <param name="oldNode">
        /// The old node.
        /// </param>
        /// <param name="newNode">
        /// The new node.
        /// </param>
        /// <param name="sb">
        /// The sb.
        /// </param>
        /// <param name="fieldBase">
        /// The field base.
        /// </param>
        private static void CompareXmlObjects(XmlNode oldNode, XmlNode newNode, StringBuilder sb, string fieldBase)
        {
            var matchedFields = new ArrayList();
            foreach (XmlNode oldField in oldNode.ChildNodes)
            {
                var fieldName = fieldBase + oldField.Name;
                XmlNode newField = null;
                foreach (XmlNode n in newNode.ChildNodes)
                {
                    if (n.Name == oldField.Name && !matchedFields.Contains(n))
                    {
                        newField = n;
                        break;
                    }
                }

                if (newField != null)
                {
                    matchedFields.Add(newField);
                    if (newField.InnerText != oldField.InnerText)
                    {
                        if (newField.ChildNodes.Count > 1 || oldField.ChildNodes.Count > 1)
                        {
                            CompareXmlObjects(oldField, newField, sb, fieldName + "/");
                        }
                        else
                        {
                            sb.Append("<tr><td>");
                            sb.Append(fieldName);
                            sb.Append("</td><td>");
                            sb.Append(oldField.InnerText);
                            sb.Append("</td><td>");
                            sb.Append(newField.InnerText);
                            sb.Append("</td></tr>");
                        }
                    }
                }
                else
                {
                    sb.Append("<tr><td>");
                    sb.Append(fieldName);
                    sb.Append(" removed</td><td>");
                    sb.Append(oldField.InnerText);
                    sb.Append("</td></tr>");
                }
            }

            foreach (XmlNode newField in newNode.ChildNodes)
            {
                var fieldName = fieldBase + newField.Name;
                if (!matchedFields.Contains(newField))
                {
                    sb.Append("<tr><td>");
                    sb.Append(fieldName);
                    sb.Append("</td><td>");
                    sb.Append(newField.InnerText);
                    sb.Append("</td></tr>");
                }
            }
        }

        /// <summary>
        /// The diff files.
        /// </summary>
        /// <param name="fileToCompare">
        /// The file to compare.
        /// </param>
        /// <param name="bank">
        /// The bank.
        /// </param>
        /// <param name="sb">
        /// The sb.
        /// </param>
        private static void DiffFiles(string fileToCompare, IXmlBank bank, StringBuilder sb)
        {
            sb.AppendLine("<STYLE TYPE=\"text/css\">");
            sb.AppendLine("<!--");
            sb.AppendLine(".indented");
            sb.AppendLine("{");
            sb.AppendLine("padding-left: 50pt;");
            sb.AppendLine("padding-right: 50pt;");
            sb.AppendLine("}");
            sb.AppendLine("tr.headerRow td");
            sb.AppendLine("{");
            sb.AppendLine("border-top: 1px solid #FB7A31;");
            sb.AppendLine("border-bottom: 1px solid #FB7A31;");
            sb.AppendLine("background: #FFC;");
            sb.AppendLine(" font-weight: bold;");
            sb.AppendLine("}");
            sb.AppendLine("table.fieldChanges");
            sb.AppendLine("{");
            sb.AppendLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
            sb.AppendLine("border-collapse: collapse;");
            sb.AppendLine("}");
            sb.AppendLine("td {");
            sb.AppendLine("border-bottom: 1px solid #CCC;");
            sb.AppendLine("padding: 0 0.5em;");
            sb.AppendLine("}");
            sb.AppendLine("body {");
            sb.AppendLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
            sb.AppendLine("}");
            sb.AppendLine("-->");
            sb.AppendLine("</STYLE>");
            sb.Append("<P>");

            try
            {
                var oldDoc = new XmlDocument();
                oldDoc.Load(fileToCompare);
                var newDoc = bank.Document;

                var oldObjects = new Hashtable();
                foreach (XmlNode node in oldDoc.DocumentElement.ChildNodes)
                {
                    oldObjects.Add(node.Attributes["name"].Value, node);
                }

                var newObjects = new Hashtable();
                foreach (XmlNode node in newDoc.DocumentElement.ChildNodes)
                {
                    newObjects.Add(node.Attributes["name"].Value, node);
                }

                foreach (DictionaryEntry de in oldObjects)
                {
                    var oldNode = (XmlNode)de.Value;
                    var newNode = (XmlNode)newObjects[de.Key];
                    if (newNode == null)
                    {
                        sb.Append("Deleted object <strong>" + de.Key + "</strong><br>");
                    }
                    else
                    {
                        if (oldNode.InnerText != newNode.InnerText)
                        {
                            sb.Append("Changed object <strong>");
                            sb.Append(de.Key);
                            sb.AppendLine(
                                "</strong>:<br><table class=\"fieldChanges\"><tr class=\"headerRow\"><td>Field</td><td>Old Value</td><td>New Value</td></tr>");

                            CompareXmlObjects(oldNode, newNode, sb, string.Empty);

                            sb.AppendLine("</table><br>");
                        }
                    }
                }

                foreach (DictionaryEntry de in newObjects)
                {
                    if (oldObjects[de.Key] == null)
                    {
                        sb.Append("Added object <strong>" + de.Key + "</strong><br>");
                    }
                }

                sb.AppendLine("</P>");
            }
            catch (Exception ex)
            {
                sb.AppendLine("Parse error: " + ex.Message);
            }
        }

        /// <summary>
        /// The find bank.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="IXmlBank"/>.
        /// </returns>
        private static IXmlBank FindBank(string path)
        {
            foreach (var kvp in RaveInstance.AllBankManagers)
            {
                foreach (var bank in kvp.Value.Banks)
                {
                    if (bank.FilePath.ToUpper() == path.ToUpper())
                    {
                        return bank;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// The check in.
        /// </summary>
        /// <param name="changelist">
        /// The changelist.
        /// </param>
        /// <param name="selectedAssets">
        /// The selected assets.
        /// </param>
        private void CheckIn(ChangelistNode changelist, List<AssetNode> selectedAssets)
        {
            var checkin = new frmCheckIn(changelist, RaveInstance.RaveLog, selectedAssets);
            if (checkin.ShowDialog() == DialogResult.OK)
            {
                this.RefreshView();
                this.BanksStatusChanged.Raise();
                this.ChangeListSubmitted.Raise(changelist.Changelist.NumberAsString, checkin.SubmittedChangeListNumber);
            }
        }

        /// <summary>
        /// The context menu_ opening.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ContextMenu_Opening(object sender, CancelEventArgs e)
        {
            this.mnuSubmit.Visible = false;
            this.mnuRevert.Visible = false;
            this.mnuDiff.Visible = false;
            this.mnuNewChangelist.Visible = false;
            this.mnuDelete.Visible = false;
            this.mnuRename.Visible = false;

            if (this.m_TreeView.SelectedItems.Count > 0)
            {
                if (this.m_TreeView.LastSelectedNode.GetType() == typeof(ChangelistNode))
                {
                    var cln = this.m_TreeView.LastSelectedNode as ChangelistNode;
                    if (this.m_TreeView.LastSelectedNode.Nodes.Count > 0)
                    {
                        this.mnuRevert.Visible = true;
                        this.mnuSubmit.Visible = true;
                    }

                    if (cln.Text != "default")
                    {
                        this.mnuRename.Visible = true;

                        if (this.m_TreeView.LastSelectedNode.Nodes.Count == 0)
                        {
                            this.mnuDelete.Visible = true;
                        }
                    }
                }
                else if (this.m_TreeView.LastSelectedNode.GetType() == typeof(AssetNode))
                {
                    this.mnuSubmit.Visible = true;
                    this.mnuRevert.Visible = true;
                    this.mnuDiff.Visible = true;
                }
            }
            else
            {
                this.mnuNewChangelist.Visible = true;
            }
        }

        /// <summary>
        /// The create node.
        /// </summary>
        /// <param name="c">
        /// The c.
        /// </param>
        private void CreateNode(IChangeList c)
        {
            var parent = new ChangelistNode(c);
            this.m_TreeView.Nodes.Insert(parent.ObjectName == "default" ? 0 : this.m_TreeView.Nodes.Count, parent);
            foreach (var a in c.Assets)
            {
                parent.Nodes.Add(new AssetNode(a));
            }
        }

        /// <summary>
        /// The find asset.
        /// </summary>
        /// <param name="localPath">
        /// The local path.
        /// </param>
        /// <returns>
        /// The <see cref="AssetNode"/>.
        /// </returns>
        private AssetNode FindAsset(string localPath)
        {
            foreach (ChangelistNode cln in this.m_TreeView.Nodes)
            {
                foreach (AssetNode an in cln.Nodes)
                {
                    if (string.Compare(an.Path, localPath, true) == 0)
                    {
                        return an;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// The tree view_ drag drop.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DragDrop(object sender, DragEventArgs e)
        {
            if (this.m_TreeView.LastSelectedNode != null
                && this.m_TreeView.LastSelectedNode.GetType() == typeof(ChangelistNode))
            {
                var newCln = this.m_TreeView.LastSelectedNode as ChangelistNode;

                var assets = (List<AssetNode>)e.Data.GetData(typeof(List<AssetNode>));

                if (null == assets)
                {
                    return;
                }

                foreach (var an in assets)
                {
                    var oldCln = an.Parent as ChangelistNode;
                    if (oldCln != null && newCln != null)
                    {
                        try
                        {
                            RaveInstance.AssetManager.MoveAsset(an.Asset, newCln.Changelist);
                            oldCln.Nodes.Remove(an);
                            newCln.Nodes.Add(an);
                        }
                        catch (Exception ex)
                        {
                            ErrorManager.HandleError(ex);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The tree view_ item drag.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            var nodeList = new List<AssetNode>();

            foreach (TreeNode tn in this.m_TreeView.SelectedItems)
            {
                var an = tn as AssetNode;
                if (an != null)
                {
                    if (!nodeList.Contains(an))
                    {
                        nodeList.Add(an);
                    }
                }
            }

            this.DoDragDrop(nodeList, DragDropEffects.Link);
        }

        /// <summary>
        /// The tree view_ node double click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_NodeDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node != null)
            {
                var assetNode = e.Node as AssetNode;
                if (assetNode != null)
                {
                    this.BankSelected.Raise(assetNode.Path);
                }
            }
        }

        /// <summary>
        /// The treev view_ drag over.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreevView_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
            var p = this.m_TreeView.PointToClient(new Point(e.X, e.Y));
            this.m_TreeView.LastSelectedNode = this.m_TreeView.GetNodeAt(p.X, p.Y);
        }

        private void TreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            foreach (TreeNode node in e.Node.Nodes)
            {
                if (node is AssetNode)
                {
                    ((AssetNode)node).UpdateLockingIcon();
                }
            }
        }

        private AssetNode lastNodeToolTipWasShown = null;
        private void TreeView_MouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            AssetNode node = e.Node as AssetNode;

            if (node != null)
            {
                if (node != lastNodeToolTipWasShown)
                {
                    this.m_InfoText.Text=node.GetFileStatusDescription();  
                    this.m_InfoText.Show();
                }
            }
            else
            {
                this.m_InfoText.Text = "";
                this.m_InfoText.Hide();
            }
            lastNodeToolTipWasShown = node;
        }

        /// <summary>
        /// The mnu delete_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuDelete_Click(object sender, EventArgs e)
        {
            if (this.m_TreeView.LastSelectedNode == null)
            {
                return;
            }

            var cln = this.m_TreeView.LastSelectedNode as ChangelistNode;
            if (null == cln)
            {
                return;
            }

            try
            {
                cln.Changelist.Delete();
                this.m_TreeView.BeginUpdate();
                this.m_TreeView.Nodes.Remove(this.m_TreeView.LastSelectedNode);
                this.m_TreeView.EndUpdate();
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }
        }

        /// <summary>
        /// The mnu diff checked in_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuDiffCheckedIn_Click(object sender, EventArgs e)
        {
            if (this.m_TreeView.LastSelectedNode == null)
            {
                return;
            }

            var an = this.m_TreeView.LastSelectedNode as AssetNode;
            if (an != null)
            {
                this.DiffCheckedIn(an.Path, FindBank(an.Path));
            }
        }

        /// <summary>
        /// The mnu diff saved_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuDiffSaved_Click(object sender, EventArgs e)
        {
            if (this.m_TreeView.LastSelectedNode == null)
            {
                return;
            }

            var an = this.m_TreeView.LastSelectedNode as AssetNode;
            if (an != null)
            {
                this.DiffSaved(an.Path, FindBank(an.Path));
            }
        }

        /// <summary>
        /// The mnu rename_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuRename_Click(object sender, EventArgs e)
        {
            if (this.m_TreeView.LastSelectedNode == null)
            {
                return;
            }

            var changelistNode = this.m_TreeView.LastSelectedNode as ChangelistNode;
            if (changelistNode != null)
            {
                frmSimpleInput.InputResult input = frmSimpleInput.GetInput(
                    this.ParentForm, "Enter New Change List Name", changelistNode.Changelist.Description, false, true);

                if (input.HasBeenCancelled) return;
                string newName = input.data;

                newName = newName.Trim();

                while (newName == string.Empty)
                {
                    ErrorManager.HandleInfo("Change list description required");

                    input = frmSimpleInput.GetInput(
                        this.ParentForm, "Enter New Change List Name", changelistNode.Changelist.Description, false, true);
                    if(input.HasBeenCancelled) return;
                    newName = input.data;
                    newName = newName.Trim();
                }

                try
                {
                    if (changelistNode.Changelist.UpdateDescription(newName))
                    {
                        changelistNode.Refresh();
                    }
                }
                catch (Exception ex)
                {
                    ErrorManager.HandleInfo(string.Format("Failed to rename change list: {0}", ex.Message));
                }
            }
        }

        /// <summary>
        /// The mnu new changelist_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewChangelist_Click(object sender, EventArgs e)
        {
            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(
                this.ParentForm, "Enter New Change List Name", "[Rave] Object Changelist", false, true);
            if (input.HasBeenCancelled) return;
            string newName = input.data;
            if (null == newName)
            {
                // User cancelled
                return;
            }

            newName = newName.Trim();

            while (newName == string.Empty)
            {
                ErrorManager.HandleInfo("Change list description required");

                input = frmSimpleInput.GetInput(
                    this.ParentForm, "Enter New Change List Name", "[Rave] Object Changelist", false, true);

                if (input.HasBeenCancelled) return;

                newName = input.data;

                newName = newName.Trim();
            }

            var changeList = RaveInstance.AssetManager.CreateChangeList(newName);
            this.CreateNode(changeList);
        }

        /// <summary>
        /// The mnu refresh_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuRefresh_Click(object sender, EventArgs e)
        {
            this.RefreshView();
        }

        /// <summary>
        /// The mnu revert unchanged_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuRevertUnchanged_Click(object sender, EventArgs e)
        {
            var needFullRefresh = false;
            if (this.m_TreeView.SelectedItems.Count > 0)
            {
                var nodesToRevert = new ArrayList(this.m_TreeView.SelectedItems);
                foreach (TreeNode tn in nodesToRevert)
                {
                    if (tn.GetType() == typeof(ChangelistNode))
                    {
                        var cln = tn as ChangelistNode;
                        try
                        {
                            cln.Changelist.RevertUnchanged();
                            if (cln.Changelist.Assets.Count == 0)
                            {
                                cln.Remove();
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorManager.HandleError(ex);
                        }

                        if (cln.Text == "default")
                        {
                            needFullRefresh = true;
                        }
                        else
                        {
                            this.ChangeListStatusChange.Raise(cln.Changelist.NumberAsString);
                        }
                    }
                }
            }

            if (needFullRefresh)
            {
                this.RefreshView();
            }
            else
            {
                this.BanksStatusChanged.Raise();
            }
        }

        /// <summary>
        /// The mnu revert_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuRevert_Click(object sender, EventArgs e)
        {
            if (this.m_TreeView.SelectedItems.Count > 0)
            {
                var nodesToRevert = new ArrayList(this.m_TreeView.SelectedItems);
                foreach (TreeNode tn in nodesToRevert)
                {
                    if (tn.GetType() == typeof(ChangelistNode))
                    {
                        var cln = tn as ChangelistNode;
                        var sb = new StringBuilder();
                        sb.AppendLine("Warning all changes to the following file(s) will be lost:");
                        sb.AppendLine();
                        foreach (var a in cln.Changelist.Assets)
                        {
                            var name = a.LocalPath;
                            name = name.Replace("/", "\\");
                            name = name.Replace(Configuration.ObjectXmlPath.ToUpper(), string.Empty);
                            sb.AppendLine(name);
                        }

                        sb.AppendLine();
                        sb.AppendLine("Do you wish to continue?");

                        if (ErrorManager.HandleQuestion(sb.ToString(), MessageBoxButtons.YesNo, DialogResult.Yes)
                            == DialogResult.Yes)
                        {
                            if (cln.Text == "default")
                            {
                                var nodesToRemove = new List<AssetNode>();
                                foreach (AssetNode an in cln.Nodes)
                                {
                                    try
                                    {
                                        an.Asset.Revert();
                                        nodesToRemove.Add(an);
                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorManager.HandleError(ex);
                                    }
                                }

                                foreach (var an in nodesToRemove)
                                {
                                    cln.Nodes.Remove(an);
                                }
                            }
                            else
                            {
                                try
                                {
                                    cln.Changelist.Revert(true);
                                    cln.Remove();
                                }
                                catch (Exception ex)
                                {
                                    ErrorManager.HandleError(ex);
                                }
                            }
                        }

                        this.ChangeListStatusChange.Raise(cln.Changelist.NumberAsString);
                    }
                    else if (tn.GetType() == typeof(AssetNode))
                    {
                        var an = tn as AssetNode;
                        var sb = new StringBuilder();
                        sb.Append("Changes to this file ");
                        sb.Append(an.ObjectName);
                        sb.Append(" will be lost. Do you wish to continue?");
                        if (ErrorManager.HandleQuestion(sb.ToString(), MessageBoxButtons.OKCancel, DialogResult.OK)
                            == DialogResult.OK)
                        {
                            try
                            {
                                an.Asset.Revert();
                                this.m_TreeView.BeginUpdate();
                                an.Remove();
                                this.m_TreeView.EndUpdate();
                            }
                            catch (Exception ex)
                            {
                                ErrorManager.HandleError(ex);
                            }
                        }
                    }
                }
            }

            this.BanksStatusChanged.Raise();
        }

        /// <summary>
        /// The mnu submit_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuSubmit_Click(object sender, EventArgs e)
        {
            if (this.m_TreeView.LastSelectedNode == null)
            {
                return;
            }

            var selectedItems = this.m_TreeView.SelectedItems;

            // Keep track of changelists and their selected nodes
            var changelistGroups = new Dictionary<ChangelistNode, List<AssetNode>>();

            foreach (var item in selectedItems)
            {
                var assetNode = item as AssetNode;
                if (null != assetNode)
                {
                    var asset = assetNode.Asset;
                    if (!this.ValidateBanks(asset))
                    {
                        return;
                    }

                    // Process selected asset node
                    var changelistNode = assetNode.Parent as ChangelistNode;
                    if (null == changelistNode)
                    {
                        continue; // Should never occur, but safe to check
                    }

                    if (!changelistGroups.ContainsKey(changelistNode))
                    {
                        changelistGroups[changelistNode] = new List<AssetNode>();
                    }

                    changelistGroups[changelistNode].Add(assetNode);
                }
                else
                {
                    // Process selected changelist node
                    var changelistNode = item as ChangelistNode;
                    if (null == changelistNode)
                    {
                        continue;
                    }

                    if (!this.ValidateBanks(changelistNode.Changelist.Assets.ToArray()))
                    {
                        return;
                    }

                    if (!changelistGroups.ContainsKey(changelistNode))
                    {
                        changelistGroups[changelistNode] = new List<AssetNode>();
                    }
                }
            }

            // Display submit window for each changelist
            foreach (var changelistGroup in changelistGroups)
            {
                this.CheckIn(changelistGroup.Key, changelistGroup.Value);
            }
        }

        /// <summary>
        /// Validate bank pending submit.
        /// </summary>
        /// <param name="assets">
        /// The assets.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool ValidateBanks(params IAsset[] assets)
        {
	        foreach (var asset in assets)
	        {
		        var banks = RaveInstance.CheckedOutBanks.Where(b => b.Asset.LocalPath == asset.LocalPath).ToList();

		        if (banks.Any())
		        {
			        foreach (var bank in banks)
			        {
				        if (bank.IsDirty)
				        {
					        if (MessageBox.Show(
						        "Changes to bank must be saved before submit - save changes now?",
						        "Save Changes",
						        MessageBoxButtons.OKCancel) == DialogResult.Cancel)
					        {
						        return false;
					        }

					        if (!bank.Save())
					        {
						        ErrorManager.HandleError("Failed to save object bank " + bank.Name);
						        return false;
					        }
				        }
			        }
		        }
	        }
	        return true;
        }

        #endregion
    }
}