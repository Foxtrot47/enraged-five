﻿// -----------------------------------------------------------------------
// <copyright file="IRaveAssetManager.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.AssetManager.Infrastructure.Interfaces
{
    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IRaveAssetManager
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the wave change list.
        /// </summary>
        IChangeList WaveChangeList { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get asset manager.
        /// </summary>
        /// <returns>
        /// The <see cref="IAssetManager"/>.
        /// </returns>
        IAssetManager GetAssetManager();

        /// <summary>
        /// The is wave path locked.
        /// </summary>
        /// <param name="waveAssetPath">
        /// The wave asset path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsWavePathLocked(string waveAssetPath);

        /// <summary>
        /// The lock wave path.
        /// </summary>
        /// <param name="assetPath">
        /// The asset path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool LockWavePath(string assetPath);

        /// <summary>
        /// The set asset manager.
        /// </summary>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        void SetAssetManager(IAssetManager assetManager);

        /// <summary>
        /// The unlock all assets.
        /// </summary>
        void UnlockAllAssets();

        /// <summary>
        /// The unlock wave path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        void UnlockWavePath(string path);

        #endregion
    }
}
