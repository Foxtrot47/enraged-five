﻿//---------------------------------------------------------------------
// 
//  Copyright (c) Microsoft Corporation.  All rights reserved.
// 
//THIS CODE AND INFORMATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY
//KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//PARTICULAR PURPOSE.
//---------------------------------------------------------------------

namespace TreeGridView
{
    using System;
    using System.Windows.Forms;
    using System.Diagnostics;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    using System.Drawing.Design;
    using System.Text;

    [
		ToolboxItem(false),
		DesignTimeVisible(false)
	]
    public class TreeGridNode : DataGridViewRow//, IComponent
    {
		internal TreeGridView _grid;
		internal TreeGridNode _parent;
		internal TreeGridNodeCollection _owner;
        internal bool IsExpanded;
		internal bool IsRoot;
		internal bool _isSited;
        private bool m_bAllowReorder;
        private string m_ID;
 
		private Random rndSeed = new Random();
        TreeGridCell _treeCell;
        TreeGridNodeCollection childrenNodes;

        private bool childCellsCreated = false;

		// needed for IComponent
		private ISite site = null;
		private EventHandler disposed = null;

		internal TreeGridNode(TreeGridView owner)
			: this()
		{
			this._grid = owner;
			this.IsExpanded = true;
            this.m_bAllowReorder = false;
		}

        public string ID
        {
            set
            {
                this.m_ID = value;
            }
            get
            {
                if (this.m_ID == null)
                {
                    this.m_ID = this.Parent.ID + this.Parent.Nodes.IndexOf(this).ToString();
                }
                return this.m_ID;
            }
        }

        public TreeGridNode()
        {            
            this.IsExpanded = false;
			this._isSited = false;
            this.Height = 22;
            this.m_bAllowReorder = false;
		}

        public bool ReOrder
        {
            get
            {
                return this.m_bAllowReorder;
            }
            set
            {
                this.m_bAllowReorder = value;
            }
        }

		public override object Clone()
		{
			TreeGridNode r = (TreeGridNode)base.Clone();
			r._grid = this._grid;
			r._parent = this.Parent;

			r.IsExpanded = this.IsExpanded;

			return r;
		}
		
		internal protected virtual void UnSited()
		{
			// This row is being removed from being displayed on the grid.
			TreeGridCell cell;
			foreach (DataGridViewCell DGVcell in this.Cells)
			{
				cell = DGVcell as TreeGridCell;
				if (cell != null)
				{
					cell.UnSited();
				}
			}
			this._isSited = false;
		}

		internal protected virtual void Sited()
		{
			// This row is being added to the grid.
			this._isSited = true;
			this.childCellsCreated = true;
			Debug.Assert(this._grid != null);

			TreeGridCell cell;
			foreach (DataGridViewCell DGVcell in this.Cells)
			{
				cell = DGVcell as TreeGridCell;
				if (cell != null)
				{
					cell.Sited();// Level = this.Level;
				}
			}

		}

		// Represents the index of this row in the Grid
		[System.ComponentModel.Description("Represents the index of this row in the Grid. Advanced usage."),
		EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced),
		 Browsable(false),
		 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int RowIndex{
			get{
				return base.Index;
			}
		}

		// Represents the index of this row based upon its position in the collection.
		[Browsable(false),
		 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new int Index
		{
			get
			{				
			    return this._owner.IndexOf(this);	
			}
		}

        [Browsable(false),
        EditorBrowsable( EditorBrowsableState.Never), 
        DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden)]
        public ImageList ImageList
        {
            get
            {
                if (this._grid != null)
                    return this._grid.ImageList;
                else
                    return null;
            }
        }

		void cells_CollectionChanged(object sender, System.ComponentModel.CollectionChangeEventArgs e)
		{
			// Exit if there already is a tree cell for this row
			if (this._treeCell != null) return;

			if (e.Action == System.ComponentModel.CollectionChangeAction.Add || e.Action == System.ComponentModel.CollectionChangeAction.Refresh)
			{
				TreeGridCell treeCell = null;

				if (e.Element == null)
				{
					foreach (DataGridViewCell cell in base.Cells)
					{
						if (cell.GetType().IsAssignableFrom(typeof(TreeGridCell)))
						{
							treeCell = (TreeGridCell)cell;
							break;
						}

					}
				}
				else
				{
					treeCell = e.Element as TreeGridCell;
				}

				if (treeCell != null) 
				  this._treeCell = treeCell;
			}
		}

		[Category("Data"),
		 Description("The collection of root nodes in the treelist."),
		 DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
		 Editor(typeof(CollectionEditor), typeof(UITypeEditor))]
        public TreeGridNodeCollection Nodes
        {
            get
            {
                if (this.childrenNodes == null)
                {
                    this.childrenNodes = new TreeGridNodeCollection(this);
                }
                return this.childrenNodes;
            }
            set { ;}
        }

		// Create a new Cell property because by default a row is not in the grid and won't
		// have any cells. We have to fabricate the cell collection ourself.
        [Browsable(false),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new DataGridViewCellCollection Cells
		{
			get
			{
				if (!this.childCellsCreated && this.DataGridView == null)
				{
                    if (this._grid == null) return null;

					this.CreateCells(this._grid);
					this.childCellsCreated = true;
				}
				return base.Cells;
			}
		}

		[Browsable(false),
		 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int Level
		{
            get
            {
                
                    // calculate level
                    int walk = 0;
                    TreeGridNode walkRow = this.Parent;
                    while (walkRow != null)
                    {
                        walk++;
                        walkRow = walkRow.Parent;
                    }
                                   
                return walk;
            }
		}

		[Browsable(false),
		 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TreeGridNode Parent
		{
			get
			{
				return this._parent;
			}
		}

		[Browsable(false),
		 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual bool HasChildren
		{
			get
			{
				return (this.childrenNodes != null && this.Nodes.Count != 0);
			}
		}

        [Browsable(false)]
        public bool IsSited
        {
            get
            {
                return this._isSited;
            }
        }
		[Browsable(false)]
		public bool IsFirstSibling
		{
			get
			{
				return (this.Index == 0);
			}
		}

		[Browsable(false)]
		public bool IsLastSibling
		{
			get
			{
				TreeGridNode parent = this.Parent;
				if (parent != null && parent.HasChildren)
				{
					return (this.Index == parent.Nodes.Count - 1);
				}
				else
					return true;
			}
		}
		
		public virtual bool Collapse()
		{
			return this._grid.CollapseNode(this);
		}

		public virtual bool Expand()
		{
			if (this._grid != null)
				return this._grid.ExpandNode(this);
			else
			{
				this.IsExpanded = true;
				return true;
			}
		}

		internal protected virtual bool InsertChildNode(int index, TreeGridNode node)
		{
			node._parent = this;
			node._grid = this._grid;
  
            // ensure that all children of this node has their grid set
            if (this._grid != null)
                this.UpdateChildNodes(node);

            if ((this._isSited || this.IsRoot) && this.IsExpanded)
            {
                //find next sibling. Rowindex is next sibling rowindex
                TreeGridNode NextSibling = this.Nodes[index + 1];
                if (this.IsRoot)
                {
                    this._grid.SiteNode(node, NextSibling.RowIndex);
                }
                else
                {
                    this._grid.SiteNode(node, NextSibling.RowIndex);
                }

                if (node.IsExpanded)
                {
                    //adds child nodes in order
                    foreach (TreeGridNode childNode in node.Nodes)
                    {
                        this._grid.SiteNode(childNode);
                    }
                }
            }
			return true;
		}

		internal protected virtual bool InsertChildNodes(int index, params TreeGridNode[] nodes)
		{
			foreach (TreeGridNode node in nodes)
			{
				this.InsertChildNode(index, node);
			}
			return true;
		}

		internal protected virtual bool AddChildNode(TreeGridNode node)
		{
			node._parent = this;
			node._grid = this._grid;

            // ensure that all children of this node has their grid set
            if (this._grid != null)
                this.UpdateChildNodes(node);

			if ((this._isSited || this.IsRoot) && this.IsExpanded && !node._isSited)
				this._grid.SiteNode(node);

			return true;
		}
		internal protected virtual bool AddChildNodes(params TreeGridNode[] nodes)
		{
			//TODO: Convert the final call into an SiteNodes??
			foreach (TreeGridNode node in nodes)
			{
				this.AddChildNode(node);
			}
			return true;

		}

		internal protected virtual bool RemoveChildNode(TreeGridNode node)
		{
			if ((this.IsRoot || this._isSited) && this.IsExpanded )
			{
				//We only unsite out child node if we are sited and expanded.
				this._grid.UnSiteNode(node);
			
			}
            node._grid = null;	
			node._parent = null;
			return true;

		}

		internal protected virtual bool ClearNodes()
		{
            if (this.HasChildren)
            {
                for (int i = this.Nodes.Count - 1; i >= 0; i--)
                {
                    this.Nodes.RemoveAt(i);
                }
            }
			return true;
		}

        [
            Browsable(false),
            EditorBrowsable(EditorBrowsableState.Advanced)
        ]
        public event EventHandler Disposed
        {
            add
            {
                this.disposed += value;
            }
            remove
            {
                this.disposed -= value;
            }
        }

		[
			Browsable(false),
			DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)
		]
		public ISite Site
		{
			get
			{
				return this.site;
			}
			set
			{
				this.site = value;
			}
		}

        private void UpdateChildNodes(TreeGridNode node)
        {
            if (node.HasChildren)
            {
                foreach (TreeGridNode childNode in node.Nodes)
                {
                    childNode._grid = node._grid;
                    this.UpdateChildNodes(childNode);
                }
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(this.Cells[0].Value+ " { Index=");
            sb.Append(this.RowIndex.ToString(System.Globalization.CultureInfo.CurrentCulture));
            sb.Append(" }");
            return sb.ToString();
        }

        public bool IsDescendantOf(TreeGridNode treeGridNode)
        {
            TreeGridNode parent = this.Parent;
            while (parent != null)
            {
                if (parent == treeGridNode)
                {
                    return true;
                }
                parent = parent.Parent;
            }

            return false;
        }

	}

}