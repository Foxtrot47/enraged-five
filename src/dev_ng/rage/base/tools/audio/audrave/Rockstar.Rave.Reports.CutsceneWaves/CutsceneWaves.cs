﻿// -----------------------------------------------------------------------
// <copyright file="CutsceneWaves.cs" company="RockstarNorth">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.CutsceneWaves
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;
    using global::Rave.Plugins.Infrastructure.Interfaces;
    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    /// Report that generates a list of invalid cutscene waves.
    /// </summary>
    public class CutsceneWaves : IRAVEReportPlugin
    {
        /// <summary>
        /// The valid file suffixes.
        /// </summary>
        private static readonly string[] ValidSuffixes = new[]
        {
            "_LEFT", "_RIGHT", "_CENTRE", "_CENTER", ".L", ".R", ".C", 
            ".LF", ".RF", ".LS", ".RS", ".MONO"
        };

        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// The cutscene built waves depot path.
        /// </summary>
        private string cutsceneBuiltWavesDepotPath;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private IAssetManager assetManager;

        /// <summary>
        /// The list of wave elements with invalid wave names.
        /// </summary>
        private IList<XElement> invalidWaveElems; 

        /// <summary>
        /// Get the report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;

                    case "CutsceneBuiltWavesDepotPath":
                        this.cutsceneBuiltWavesDepotPath = setting.InnerText;
                        break;
                }
            }

            if (string.IsNullOrEmpty(this.cutsceneBuiltWavesDepotPath))
            {
                throw new Exception("Cutscene built waves depot path required");
            }

            // Get instance of asset manager
            this.assetManager = AssetManagerFactory.GetInstance(AssetManagerType.Perforce);

            if (null == this.assetManager)
            {
                throw new Exception("Failed to get instance of Perforce asset manager");
            }

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes.
        /// </param>
        /// <param name="browsers">
        /// The sound browsers.
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            var cutscenePath = this.assetManager.GetLocalPath(this.cutsceneBuiltWavesDepotPath);
            var builtWavesDoc = XDocument.Load(cutscenePath);
            
            this.invalidWaveElems = new List<XElement>();

            var waveElems = builtWavesDoc.Descendants("Wave");
            foreach (var waveElem in waveElems)
            {
                var name = waveElem.Attribute("name").Value.ToUpper();
                if (!name.EndsWith(".WAV"))
                {
                    this.invalidWaveElems.Add(waveElem);
                    continue;
                }

                var nameNoExt = name.Substring(0, name.Length - 4);
                if (!ValidSuffixes.Any(nameNoExt.EndsWith))
                {
                    this.invalidWaveElems.Add(waveElem);
                }
            }

            this.DisplayResults();
        }

        /// <summary>
        /// Display the results.
        /// </summary>
        private void DisplayResults()
        {
            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Invalid Cutscene Waves</title></head><body>");

            // Write names of peds in game but not in wiki
            sw.WriteLine("<h2>Invalid Cutscene Waves</h2>");

            if (!this.invalidWaveElems.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<ul>");

                foreach (var invalidWaveElem in this.invalidWaveElems)
                {
                    var path = string.Empty;
                    var currElem = invalidWaveElem;
                    while (null != currElem)
                    {
                        var elemName = currElem.Attribute("name").Value;
                        path = elemName + (!string.IsNullOrEmpty(path) ? "\\" : string.Empty) + path;
                        currElem = currElem.Parent;
                    }

                    sw.WriteLine("<li>" + path + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            sw.WriteLine("</body></html>");

            sw.Close();
            filesteam.Close();

            // Display report
            var report = new frmReport(this.name + " Report");
            report.SetURL(resultsFileName);
            report.Show();
        }
    }
}
