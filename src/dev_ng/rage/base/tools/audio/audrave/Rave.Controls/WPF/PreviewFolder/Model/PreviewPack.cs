﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rave.Controls.WPF.PreviewFolder.Model
{
    public class PreviewPack : PreviewItem
    {

        public PreviewPack()
        {
            Items = new ObservableCollection<PreviewItem>();
        }

     }
}
