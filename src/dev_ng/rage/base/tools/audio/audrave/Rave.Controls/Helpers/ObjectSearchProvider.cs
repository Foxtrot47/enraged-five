﻿using System;
using System.Collections.Generic;
using Rave.SearchProvider.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.ObjectBrowser.Infrastructure.Nodes;
using Rave.Instance;
using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
using rage.ToolLib;

namespace Rave.Controls.Forms.Search
{
	class ObjectSearchProvider : ISearchProvider<IObjectInstance>
	{
		#region Properties

		//Observable collection containing search results
		public ObservableSortedSet<IObjectInstance> SearchResults
		{
			get
			{
				return _searchResults;
			}
		}

		public string ObjectType
		{
			get { return _objectType; }
		}

		#endregion

		#region Fields

		private List<IObjectInstance> _objectsToSearch;

		private ObservableSortedSet<IObjectInstance> _searchResults;

		/// <summary>
		/// The object type.
		/// </summary>
		private readonly string _objectType;

		/// <summary>
		/// The root search node.
		/// </summary>
		private readonly BaseNode _rootSearchNode;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructor for Objet search provider throwing exception if there is not enough information given
		/// </summary>
		/// <param name="objectType">
		/// Object Type
		/// </param>
		/// <param name="rootSearchNode">
		/// Root search node
		/// </param>
		public ObjectSearchProvider(string objectType = null, BaseNode rootSearchNode = null)
		{
			if ((string.IsNullOrEmpty(objectType) && null == rootSearchNode) ||
			   (!string.IsNullOrEmpty(objectType) && null != rootSearchNode))
			{
				throw new Exception("Either objectType or rootSearchNode must be specified");
			}

			if (null != objectType)
			{
				this._objectType = objectType.ToUpper();
			}

			this._rootSearchNode = rootSearchNode;

			_objectsToSearch = GetObjectList();
		}

		#endregion

		#region PublicMethods

		public ObservableSortedSet<IObjectInstance> Search(string searchString, SearchArguments args)
		{
			return Search(searchString, args.SearchNameHash, args.ShouldOnlySearchNames, args.IsCaseSensitive, args.ExactNameMatch);
		}

		private class SearchResultComparer : IComparer<IObjectInstance>
		{
			public int Compare(IObjectInstance x, IObjectInstance y)
			{
				if (x.Episode != y.Episode && x.Name.Equals(y.Name, StringComparison.InvariantCultureIgnoreCase))
				{
					return String.Compare(x.Episode, y.Episode, StringComparison.InvariantCultureIgnoreCase);
				}

				return String.Compare(x.Name, y.Name, StringComparison.InvariantCultureIgnoreCase);
			}
		}

		public ObservableSortedSet<IObjectInstance> Search(string searchString, bool searchNameHash = false, bool shouldOnlySearchNames = false, bool isCaseSensitive = false, bool exactNameMatch = false)
		{
			//Reset Search results
			_searchResults = new ObservableSortedSet<IObjectInstance>(new SearchResultComparer());

			//Check if caseSensitive flag is checked
			if (!isCaseSensitive)
			{
				searchString = searchString.ToUpper();
			}

			//Check if search hashname is checked
			if (searchNameHash)
			{
				uint hash;
				if (!uint.TryParse(searchString, out hash))
				{
					throw new Exception("Name hash isn't a number, Invalid Input");
				}
			}

			//Search through Objects to search
			foreach (IObjectInstance objectInstance in _objectsToSearch)
			{
				if (searchNameHash)
				{
					//Check if namehash ends with search string
					if (objectInstance.NameHash.ToString().EndsWith(searchString))
					{
						_searchResults.Add(objectInstance);
					}
				}
				else
				{
					//Gets node text either from Name or OuterXml
					string nodeText = shouldOnlySearchNames ? objectInstance.Name
									   : objectInstance.Node.OuterXml;

					//If case insesitive is unchecked
					if (!isCaseSensitive)
					{
						nodeText = nodeText.ToUpper();
					}

					//If exact name there should be exactly equal to add to hits
					if (exactNameMatch)
					{
						if (objectInstance.Name.Equals(searchString))
						{
							_searchResults.Add(objectInstance);
						}
					}
					else if (nodeText.Contains(searchString))
					{
						//Add to Search results if it contains the string
						_searchResults.Add(objectInstance);
					}
				}
			}

			return _searchResults;
		}

		#endregion

		#region PrivateMethods

		/// <summary>
		/// Initialize the object list.
		/// </summary>
		private List<IObjectInstance> GetObjectList()
		{
			List<IObjectInstance> objectInstances = new List<IObjectInstance>();

			if (null == this._rootSearchNode)
			{
				foreach (IObjectBank bank in RaveInstance.AllBankManagers[this._objectType].ObjectBanks)
				{
					objectInstances.AddRange(bank.ObjectInstances);
				}
			}
			else
			{
				List<IObjectInstance> list = GetObjectList(this._rootSearchNode);
				objectInstances.AddRange(list);
			}
			return objectInstances;
		}


		/// <summary>
		/// Get the object list.
		/// </summary>
		/// <param name="rootNode">
		/// The root node.
		/// </param>
		/// <returns>
		/// The list of objects to search <see cref="List{T}"/>.
		/// </returns>
		private static List<IObjectInstance> GetObjectList(BaseNode rootNode)
		{
			List<IObjectInstance> list = new List<IObjectInstance>();
			if (rootNode.GetType() == typeof(SoundNode))
			{
				list.Add(((SoundNode)rootNode).ObjectInstance);
			}

			foreach (BaseNode child in rootNode.Nodes)
			{
				list.AddRange(GetObjectList(child));
			}

			return list;
		}

		#endregion



		public bool IsReady
		{
			get { throw new NotImplementedException(); }
		}
	}
}
