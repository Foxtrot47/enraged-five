namespace Rave.Controls.Forms.Popups
{
    using System;
    using System.ComponentModel;
    using System.Reflection;
    using System.Threading;
    using System.Windows.Forms;

    using Rave.Instance;

    using Timer = System.Windows.Forms.Timer;

    /// <summary>
    ///   Summary description for frmSplashScreen.
    /// </summary>
    public class frmSplashScreen : Form
    {
        private static frmSplashScreen sm_SplashScreen;
        private static Thread sm_SplashThread;
        private IContainer components;
        private Label label1;
        private Label lblCompany;
        private Label lblCopyright;
        private Label lblVersion;
        private int m_NumDots = 1;
        private string m_Status;
        private Timer m_UpdateTimer;
        private double m_dblOpacityDecrement;
        private Label m_lblInitialising;
        private PictureBox pictureBox1;

        public frmSplashScreen()
        {
            this.InitializeComponent();
            RaveInstance.ActiveWindow = this;
            var ass = Assembly.GetEntryAssembly();
            this.lblVersion.Text = "Version: " + ass.GetName().Version;
            this.lblCopyright.Text =
                ((AssemblyCopyrightAttribute) (ass.GetCustomAttributes(typeof (AssemblyCopyrightAttribute), false)[0])).
                    Copyright;
            this.lblCompany.Text =
                ((AssemblyCompanyAttribute) (ass.GetCustomAttributes(typeof (AssemblyCompanyAttribute), false)[0])).
                    Company;
            this.m_UpdateTimer.Start();
        }

        public static void ShowSplashScreen()
        {
            if (sm_SplashScreen != null)
            {
                return;
            }

            sm_SplashThread = new Thread(ShowForm);
            CheckForIllegalCrossThreadCalls = false;
            sm_SplashThread.IsBackground = true;
            sm_SplashThread.SetApartmentState(ApartmentState.STA);
            sm_SplashThread.Start();
        }

        private static void ShowForm()
        {
            sm_SplashScreen = new frmSplashScreen();
            RaveInstance.ActiveWindow = sm_SplashScreen;
            Application.Run(sm_SplashScreen);
        }

        public static void CloseForm()
        {
            if (null != sm_SplashScreen)
            {
                sm_SplashScreen.m_dblOpacityDecrement = -0.1;
                sm_SplashScreen = null;
            }

            sm_SplashThread = null;
        }

        public static void Abort()
        {
            if (sm_SplashThread != null)
            {
                sm_SplashThread.Abort();
                sm_SplashThread.Join();
            }
        }

        public static void SetText(string newStatus)
        {
            if (sm_SplashScreen == null)
            {
                return;
            }
            sm_SplashScreen.m_Status = newStatus;
        }

        /// <summary>
        ///   Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        private void m_UpdateTimer_Tick(object sender, EventArgs e)
        {
            var text = this.m_Status + " ";
            for (var i = 0; i < this.m_NumDots; i++)
            {
                text += ".";
            }
            this.m_lblInitialising.Text = text;
            this.m_lblInitialising.Refresh();
            this.m_NumDots = (this.m_NumDots + 1) % 5;

            if (this.m_dblOpacityDecrement < 0)
            {
                if (this.Opacity > 0)
                {
                    this.Opacity += this.m_dblOpacityDecrement;
                }
                else
                {
                    this.Close();
                }
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///   Required method for Designer support - do not modify
        ///   the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof (frmSplashScreen));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.m_UpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.m_lblInitialising = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.lblCompany = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage =
                ((System.Drawing.Image) (resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(544, 328);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // m_UpdateTimer
            // 
            this.m_UpdateTimer.Interval = 200;
            this.m_UpdateTimer.Tick += new System.EventHandler(this.m_UpdateTimer_Tick);
            // 
            // m_lblInitialising
            // 
            this.m_lblInitialising.BackColor = System.Drawing.Color.Transparent;
            this.m_lblInitialising.Font = new System.Drawing.Font("Arial",
                                                                  8.25F,
                                                                  System.Drawing.FontStyle.Bold,
                                                                  System.Drawing.GraphicsUnit.Point,
                                                                  ((byte) (0)));
            this.m_lblInitialising.ForeColor = System.Drawing.Color.Black;
            this.m_lblInitialising.Location = new System.Drawing.Point(20, 288);
            this.m_lblInitialising.Name = "m_lblInitialising";
            this.m_lblInitialising.Size = new System.Drawing.Size(256, 16);
            this.m_lblInitialising.TabIndex = 1;
            this.m_lblInitialising.Text = "Initialising";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial",
                                                       8.25F,
                                                       System.Drawing.FontStyle.Bold,
                                                       System.Drawing.GraphicsUnit.Point,
                                                       ((byte) (0)));
            this.label1.Location = new System.Drawing.Point(300, 293);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "RAGE Audio Visualisation Environment";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblVersion
            // 
            this.lblVersion.BackColor = System.Drawing.Color.White;
            this.lblVersion.Font = new System.Drawing.Font("Arial",
                                                           8.25F,
                                                           System.Drawing.FontStyle.Regular,
                                                           System.Drawing.GraphicsUnit.Point,
                                                           ((byte) (0)));
            this.lblVersion.Location = new System.Drawing.Point(344, 309);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(176, 16);
            this.lblVersion.TabIndex = 3;
            this.lblVersion.Text = "label2";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCopyright
            // 
            this.lblCopyright.AutoSize = true;
            this.lblCopyright.BackColor = System.Drawing.Color.Transparent;
            this.lblCopyright.Location = new System.Drawing.Point(23, 23);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(0, 13);
            this.lblCopyright.TabIndex = 4;
            // 
            // lblCompany
            // 
            this.lblCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Location = new System.Drawing.Point(344, 280);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(176, 13);
            this.lblCompany.TabIndex = 5;
            this.lblCompany.Text = "label2";
            this.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmSplashScreen
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(544, 328);
            this.ControlBox = false;
            this.Controls.Add(this.lblCompany);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_lblInitialising);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSplashScreen";
            this.ShowInTaskbar = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RAVE - Initialising";
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion
    }
}