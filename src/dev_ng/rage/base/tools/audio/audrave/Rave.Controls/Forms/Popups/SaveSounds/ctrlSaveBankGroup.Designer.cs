namespace Rave.Controls.Forms.Popups.SaveSounds
{
    partial class ctrlSaveBankGroup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_GroupBox = new System.Windows.Forms.GroupBox();
            this.m_SaveList = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCheckin = new System.Windows.Forms.Label();
            this.lblUndo = new System.Windows.Forms.Label();
            this.lblSave = new System.Windows.Forms.Label();
            this.m_ChkSaveAll = new System.Windows.Forms.CheckBox();
            this.m_ChkUndoAll = new System.Windows.Forms.CheckBox();
            this.m_ChkCheckInAll = new System.Windows.Forms.CheckBox();
            this.lblBankName = new System.Windows.Forms.Label();
            this.m_GroupBox.SuspendLayout();
            this.m_SaveList.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_GroupBox
            // 
            this.m_GroupBox.AutoSize = true;
            this.m_GroupBox.Controls.Add(this.m_SaveList);
            this.m_GroupBox.Location = new System.Drawing.Point(3, 3);
            this.m_GroupBox.Name = "m_GroupBox";
            this.m_GroupBox.Size = new System.Drawing.Size(415, 93);
            this.m_GroupBox.TabIndex = 0;
            this.m_GroupBox.TabStop = false;
            this.m_GroupBox.Text = "groupBox1";
            // 
            // m_SaveList
            // 
            this.m_SaveList.AutoSize = true;
            this.m_SaveList.Controls.Add(this.panel1);
            this.m_SaveList.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.m_SaveList.Location = new System.Drawing.Point(4, 14);
            this.m_SaveList.Name = "m_SaveList";
            this.m_SaveList.Size = new System.Drawing.Size(405, 60);
            this.m_SaveList.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblCheckin);
            this.panel1.Controls.Add(this.lblUndo);
            this.panel1.Controls.Add(this.lblSave);
            this.panel1.Controls.Add(this.m_ChkSaveAll);
            this.panel1.Controls.Add(this.m_ChkUndoAll);
            this.panel1.Controls.Add(this.m_ChkCheckInAll);
            this.panel1.Controls.Add(this.lblBankName);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(405, 43);
            this.panel1.TabIndex = 0;
            // 
            // lblCheckin
            // 
            this.lblCheckin.AutoSize = true;
            this.lblCheckin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckin.Location = new System.Drawing.Point(349, 4);
            this.lblCheckin.Name = "lblCheckin";
            this.lblCheckin.Size = new System.Drawing.Size(50, 13);
            this.lblCheckin.TabIndex = 6;
            this.lblCheckin.Text = "Check In";
            // 
            // lblUndo
            // 
            this.lblUndo.AutoSize = true;
            this.lblUndo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUndo.Location = new System.Drawing.Point(261, 4);
            this.lblUndo.Name = "lblUndo";
            this.lblUndo.Size = new System.Drawing.Size(87, 13);
            this.lblUndo.TabIndex = 5;
            this.lblUndo.Text = "Undo Check Out";
            // 
            // lblSave
            // 
            this.lblSave.AutoSize = true;
            this.lblSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSave.Location = new System.Drawing.Point(220, 3);
            this.lblSave.Name = "lblSave";
            this.lblSave.Size = new System.Drawing.Size(32, 13);
            this.lblSave.TabIndex = 4;
            this.lblSave.Text = "Save";
            // 
            // m_ChkSaveAll
            // 
            this.m_ChkSaveAll.AutoSize = true;
            this.m_ChkSaveAll.Location = new System.Drawing.Point(230, 23);
            this.m_ChkSaveAll.Name = "m_ChkSaveAll";
            this.m_ChkSaveAll.Size = new System.Drawing.Size(15, 14);
            this.m_ChkSaveAll.TabIndex = 3;
            this.m_ChkSaveAll.UseVisualStyleBackColor = true;
            this.m_ChkSaveAll.CheckedChanged += new System.EventHandler(this.SaveAll_Changed);
            // 
            // m_ChkUndoAll
            // 
            this.m_ChkUndoAll.AutoSize = true;
            this.m_ChkUndoAll.Location = new System.Drawing.Point(300, 23);
            this.m_ChkUndoAll.Name = "m_ChkUndoAll";
            this.m_ChkUndoAll.Size = new System.Drawing.Size(15, 14);
            this.m_ChkUndoAll.TabIndex = 2;
            this.m_ChkUndoAll.UseVisualStyleBackColor = true;
            this.m_ChkUndoAll.CheckedChanged += new System.EventHandler(this.UndoAll_Changed);
            // 
            // m_ChkCheckInAll
            // 
            this.m_ChkCheckInAll.AutoSize = true;
            this.m_ChkCheckInAll.Location = new System.Drawing.Point(370, 23);
            this.m_ChkCheckInAll.Name = "m_ChkCheckInAll";
            this.m_ChkCheckInAll.Size = new System.Drawing.Size(15, 14);
            this.m_ChkCheckInAll.TabIndex = 1;
            this.m_ChkCheckInAll.UseVisualStyleBackColor = true;
            this.m_ChkCheckInAll.CheckedChanged += new System.EventHandler(this.CheckInAll_Changed);
            // 
            // lblBankName
            // 
            this.lblBankName.AutoSize = true;
            this.lblBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBankName.Location = new System.Drawing.Point(2, 23);
            this.lblBankName.Margin = new System.Windows.Forms.Padding(0);
            this.lblBankName.Name = "lblBankName";
            this.lblBankName.Size = new System.Drawing.Size(63, 13);
            this.lblBankName.TabIndex = 0;
            this.lblBankName.Text = "Bank Name";
            // 
            // ctrlSaveBankGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.m_GroupBox);
            this.Name = "ctrlSaveBankGroup";
            this.Size = new System.Drawing.Size(421, 99);
            this.m_GroupBox.ResumeLayout(false);
            this.m_GroupBox.PerformLayout();
            this.m_SaveList.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox m_GroupBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox m_ChkCheckInAll;
        private System.Windows.Forms.Label lblBankName;
        private System.Windows.Forms.Label lblCheckin;
        private System.Windows.Forms.Label lblUndo;
        private System.Windows.Forms.Label lblSave;
        private System.Windows.Forms.CheckBox m_ChkSaveAll;
        private System.Windows.Forms.CheckBox m_ChkUndoAll;
        private System.Windows.Forms.FlowLayoutPanel m_SaveList;
    }
}
