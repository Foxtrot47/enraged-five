namespace Rave.Controls.Forms.Popups.SaveSounds
{
    using System;
    using System.Windows.Forms;

    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;

    public partial class ctrlSaveCheckIn : UserControl
    {
        private readonly IXmlBank m_Bank;

        public ctrlSaveCheckIn(IXmlBank bank, bool enableSave, bool enableCheckIn, bool enableUndo)
        {
            this.m_Bank = bank;
            this.InitializeComponent();
            this.lblBankName.Text = bank.Name;
            this.chkCheckIn.Enabled = this.chkCheckIn.Visible = enableCheckIn;
            this.chkUndoCheckOut.Enabled = this.chkUndoCheckOut.Visible = enableUndo;
            this.chkSave.Enabled = enableSave;

            if (enableSave)
            {
                this.chkSave.Checked = true;
            }
        }

        public IXmlBank Bank
        {
            get { return this.m_Bank; }
        }

        public bool IsSaveChecked()
        {
            return this.chkSave.Checked;
        }

        public void SetSaveCheck(bool isChecked)
        {
            if (this.chkSave.Enabled)
            {
                this.chkSave.Checked = isChecked;
            }
        }

        public bool IsCheckInChecked()
        {
            return this.chkCheckIn.Checked;
        }

        public void SetCheckInCheck(bool isChecked)
        {
            if (this.chkCheckIn.Enabled)
            {
                this.chkCheckIn.Checked = isChecked;
            }
        }

        public bool IsUndoChecked()
        {
            return this.chkUndoCheckOut.Checked;
        }

        public void SetUndoCheck(bool isChecked)
        {
            if (this.chkUndoCheckOut.Enabled)
            {
                this.chkUndoCheckOut.Checked = isChecked;
            }
        }

        private void chkCheckIn_Changed(object sender, EventArgs e)
        {
            if (this.chkCheckIn.Checked)
            {
                this.SetUndoCheck(false);
            }
        }

        private void chkUndoCheckOut_Changed(object sender, EventArgs e)
        {
            if (this.chkUndoCheckOut.Checked)
            {
                this.SetCheckInCheck(false);
                this.SetSaveCheck(false);
            }
        }

        private void chkSave_Changed(object sender, EventArgs e)
        {
            if (this.chkSave.Checked)
            {
                this.SetUndoCheck(false);
            }
        }
    }
}