﻿// -----------------------------------------------------------------------
// <copyright file="DoubleBufferedTreeView.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

using System.Runtime.InteropServices;

namespace Rave.Controls.Forms.DoubleBufferedTreeView
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// Double-buffered tree view.
    /// </summary>
    public class DoubleBufferedTreeView : TreeView
    {
        private const int TV_FIRST = 0x1100;
        private const int TVM_SETBKCOLOR = TV_FIRST + 29;
        private const int TVM_SETEXTENDEDSTYLE = TV_FIRST + 44;

        private const int TVS_EX_DOUBLEBUFFER = 0x0004;

        public DoubleBufferedTreeView()
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.DoubleBuffered = true;
            this.UpdateStyles();
        }

        private void UpdateExtendedStyles()
        {
            int Style = 0;

            if (this.DoubleBuffered)
                Style |= TVS_EX_DOUBLEBUFFER;

            if (Style != 0)
                NativeInterop.SendMessage(this.Handle, TVM_SETEXTENDEDSTYLE, (IntPtr)TVS_EX_DOUBLEBUFFER, (IntPtr)Style);
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            this.UpdateExtendedStyles();
            if (!NativeInterop.IsWinXP)
                NativeInterop.SendMessage(this.Handle, TVM_SETBKCOLOR, IntPtr.Zero, (IntPtr)ColorTranslator.ToWin32(this.BackColor));
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (this.GetStyle(ControlStyles.UserPaint))
            {
                var m = new Message();
                m.HWnd = this.Handle;
                m.Msg = NativeInterop.WM_PRINTCLIENT;
                m.WParam = e.Graphics.GetHdc();
                m.LParam = (IntPtr)NativeInterop.PRF_CLIENT;
                this.DefWndProc(ref m);
                e.Graphics.ReleaseHdc(m.WParam);
            }
            base.OnPaint(e);
        }

	    public new void BeginUpdate()
	    {
			rage.ToolLib.Drawing.ExtendedControl.SuspendDrawing(this);
	    }
		
		public new void EndUpdate()
		{
			rage.ToolLib.Drawing.ExtendedControl.ResumeDrawing(this);
		}

	    //add methods and consts for handling scrollposition via native win32 functions

        [DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        public static extern int GetScrollPos(int hWnd, int nBar);

        [DllImport("user32.dll")]
        static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        private const int SB_HORZ = 0x0;
        private const int SB_VERT = 0x1;

        public Point GetScrollPos()
        {
            return new Point(
                GetScrollPos((int)this.Handle, SB_HORZ),
                GetScrollPos((int)this.Handle, SB_VERT));
        }

        public void SetScrollPos(Point scrollPosition)
        {
            SetScrollPos((IntPtr)this.Handle, SB_HORZ, scrollPosition.X, true);
            SetScrollPos((IntPtr)this.Handle, SB_VERT, scrollPosition.Y, true);
        }

    }
}
