namespace Rave.Controls.Forms.Popups.SaveSounds
{
    partial class ctrlSaveCheckIn
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBankName = new System.Windows.Forms.Label();
            this.chkCheckIn = new System.Windows.Forms.CheckBox();
            this.chkSave = new System.Windows.Forms.CheckBox();
            this.chkUndoCheckOut = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblBankName
            // 
            this.lblBankName.AutoEllipsis = true;
            this.lblBankName.AutoSize = true;
            this.lblBankName.Location = new System.Drawing.Point(3, 8);
            this.lblBankName.MaximumSize = new System.Drawing.Size(220, 0);
            this.lblBankName.Name = "lblBankName";
            this.lblBankName.Size = new System.Drawing.Size(63, 13);
            this.lblBankName.TabIndex = 0;
            this.lblBankName.Text = "Bank Name";
            // 
            // chkCheckIn
            // 
            this.chkCheckIn.AutoSize = true;
            this.chkCheckIn.Location = new System.Drawing.Point(370, 8);
            this.chkCheckIn.Name = "chkCheckIn";
            this.chkCheckIn.Size = new System.Drawing.Size(15, 14);
            this.chkCheckIn.TabIndex = 1;
            this.chkCheckIn.UseVisualStyleBackColor = true;
            this.chkCheckIn.CheckedChanged += new System.EventHandler(this.chkCheckIn_Changed);
            // 
            // chkSave
            // 
            this.chkSave.AutoSize = true;
            this.chkSave.Location = new System.Drawing.Point(230, 8);
            this.chkSave.Name = "chkSave";
            this.chkSave.Size = new System.Drawing.Size(15, 14);
            this.chkSave.TabIndex = 2;
            this.chkSave.UseVisualStyleBackColor = true;
            this.chkSave.CheckedChanged += new System.EventHandler(this.chkSave_Changed);
            // 
            // chkUndoCheckOut
            // 
            this.chkUndoCheckOut.AutoSize = true;
            this.chkUndoCheckOut.Location = new System.Drawing.Point(300, 8);
            this.chkUndoCheckOut.Name = "chkUndoCheckOut";
            this.chkUndoCheckOut.Size = new System.Drawing.Size(15, 14);
            this.chkUndoCheckOut.TabIndex = 3;
            this.chkUndoCheckOut.UseVisualStyleBackColor = true;
            this.chkUndoCheckOut.CheckedChanged += new System.EventHandler(this.chkUndoCheckOut_Changed);
            // 
            // ctrlSaveCheckIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.chkUndoCheckOut);
            this.Controls.Add(this.chkSave);
            this.Controls.Add(this.chkCheckIn);
            this.Controls.Add(this.lblBankName);
            this.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.Name = "ctrlSaveCheckIn";
            this.Size = new System.Drawing.Size(387, 30);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBankName;
        private System.Windows.Forms.CheckBox chkCheckIn;
        private System.Windows.Forms.CheckBox chkSave;
        private System.Windows.Forms.CheckBox chkUndoCheckOut;
    }
}
