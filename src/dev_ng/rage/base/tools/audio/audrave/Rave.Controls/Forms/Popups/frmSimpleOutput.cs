namespace Rave.Controls.Forms.Popups
{
    using System;
    using System.Text;
    using System.Windows.Forms;

    public partial class frmSimpleOutput : Form
    {
        public frmSimpleOutput(string title)
        {
            this.InitializeComponent();
            this.Text = title;
        }

        public event Action<object> OnSelectObject;

        public void AddObject(object item)
        {
            this.m_Output.Items.Add(item);
            this.m_OutputText2.Text = "No of Items: " + this.m_Output.Items.Count;
        }

        public void DisplayInfo(string output)
        {
            this.m_TextBox.Text = output;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OnDoubleClick(object sender, EventArgs e)
        {
            if (this.m_Output.SelectedItem != null &&
                this.OnSelectObject != null)
            {
                this.OnSelectObject(this.m_Output.SelectedItem);
            }
        }

        private void btnCopy_OnClick(object sender, EventArgs e)
        {
            var sb = new StringBuilder();
            foreach (object t in this.m_Output.Items)
            {
                sb.AppendLine(t.ToString());
            }
            Clipboard.SetText(sb.ToString());
        }
    }
}