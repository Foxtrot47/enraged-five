// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmSearch.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for frmSearch.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.Controls.Forms.Search
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Utils.Popups;

    using rage.ToolLib;

    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Summary description for frmSearch.
    /// </summary>
    public class frmSearch : Form
    {
        #region Fields

        /// <summary>
        /// The btn search.
        /// </summary>
        private Button btnSearch;

        /// <summary>
        /// The gb results.
        /// </summary>
        private GroupBox gbResults;

        /// <summary>
        /// The group box 1.
        /// </summary>
        private GroupBox groupBox1;

        /// <summary>
        /// The label 1.
        /// </summary>
        private Label label1;

        /// <summary>
        /// The m_finished.
        /// </summary>
        private bool m_finished;

        /// <summary>
        /// The m_progress.
        /// </summary>
        private int m_progress;

        /// <summary>
        /// The m_results list.
        /// </summary>
        private ListBox m_resultsList;

        /// <summary>
        /// The m_status text.
        /// </summary>
        private string m_statusText = "Searching...";

        /// <summary>
        /// The search name hash.
        /// </summary>
        private CheckBox searchNameHash;

        /// <summary>
        /// The m_should only search names.
        /// </summary>
        private CheckBox m_shouldOnlySearchNames;

        /// <summary>
        /// The m_is case sensitive.
        /// </summary>
        private CheckBox m_isCaseSensitive;

        /// <summary>
        /// The search options group box.
        /// </summary>
        private GroupBox searchOptionsGroupBox;
        private CheckBox exactNameMatch;

        /// <summary>
        /// The txt search string.
        /// </summary>
        private TextBox txtSearchString;

        private ObjectSearchProvider _objectSearchProvider;

        #endregion

        // search banks
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="frmSearch"/> class.
        /// Object type is set when doing a global search; root search node is set when
        /// searching in a particular object hierarchy.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="objectType">
        /// The object type.
        /// </param>
        /// <param name="rootSearchNode">
        /// The root search node.
        /// </param>
        public frmSearch(string title, string objectType = null, BaseNode rootSearchNode = null)
        {
            this.InitializeComponent();

            try
            {
                _objectSearchProvider = new ObjectSearchProvider(objectType, rootSearchNode);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error", MessageBoxButtons.OK);
            }

            if (title != null)
            {
                this.Text += " - " + title;
            }

            this.txtSearchString.Focus();
            this.ActiveControl = this.txtSearchString;
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The find object.
        /// </summary>
        public event Action<IObjectInstance> FindObject;

        #endregion

        #region Methods

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSearch));
            this.txtSearchString = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.gbResults = new System.Windows.Forms.GroupBox();
            this.m_resultsList = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.searchOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.m_isCaseSensitive = new System.Windows.Forms.CheckBox();
            this.searchNameHash = new System.Windows.Forms.CheckBox();
            this.m_shouldOnlySearchNames = new System.Windows.Forms.CheckBox();
            this.exactNameMatch = new System.Windows.Forms.CheckBox();
            this.gbResults.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.searchOptionsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtSearchString
            // 
            this.txtSearchString.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchString.Location = new System.Drawing.Point(96, 24);
            this.txtSearchString.Name = "txtSearchString";
            this.txtSearchString.Size = new System.Drawing.Size(289, 20);
            this.txtSearchString.TabIndex = 0;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(391, 22);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.Text = "Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(10, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Search string:";
            // 
            // gbResults
            // 
            this.gbResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbResults.Controls.Add(this.m_resultsList);
            this.gbResults.Location = new System.Drawing.Point(8, 186);
            this.gbResults.Name = "gbResults";
            this.gbResults.Size = new System.Drawing.Size(469, 286);
            this.gbResults.TabIndex = 3;
            this.gbResults.TabStop = false;
            this.gbResults.Text = "Results";
            // 
            // m_resultsList
            // 
            this.m_resultsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_resultsList.Location = new System.Drawing.Point(3, 16);
            this.m_resultsList.Name = "m_resultsList";
            this.m_resultsList.Size = new System.Drawing.Size(463, 267);
            this.m_resultsList.TabIndex = 7;
            this.m_resultsList.DoubleClick += new System.EventHandler(this.ResultsList_DoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.searchOptionsGroupBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtSearchString);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(469, 172);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Options";
            // 
            // searchOptionsGroupBox
            // 
            this.searchOptionsGroupBox.Controls.Add(this.exactNameMatch);
            this.searchOptionsGroupBox.Controls.Add(this.m_isCaseSensitive);
            this.searchOptionsGroupBox.Controls.Add(this.searchNameHash);
            this.searchOptionsGroupBox.Controls.Add(this.m_shouldOnlySearchNames);
            this.searchOptionsGroupBox.Location = new System.Drawing.Point(7, 52);
            this.searchOptionsGroupBox.Name = "searchOptionsGroupBox";
            this.searchOptionsGroupBox.Size = new System.Drawing.Size(378, 110);
            this.searchOptionsGroupBox.TabIndex = 8;
            this.searchOptionsGroupBox.TabStop = false;
            this.searchOptionsGroupBox.Text = "Search Options";
            // 
            // m_isCaseSensitive
            // 
            this.m_isCaseSensitive.Location = new System.Drawing.Point(6, 19);
            this.m_isCaseSensitive.Name = "m_isCaseSensitive";
            this.m_isCaseSensitive.Size = new System.Drawing.Size(104, 24);
            this.m_isCaseSensitive.TabIndex = 4;
            this.m_isCaseSensitive.Text = "Case sensitive";
            // 
            // searchNameHash
            // 
            this.searchNameHash.Location = new System.Drawing.Point(6, 79);
            this.searchNameHash.Name = "searchNameHash";
            this.searchNameHash.Size = new System.Drawing.Size(128, 24);
            this.searchNameHash.TabIndex = 7;
            this.searchNameHash.Text = "Search name hash";
            // 
            // m_shouldOnlySearchNames
            // 
            this.m_shouldOnlySearchNames.Location = new System.Drawing.Point(6, 49);
            this.m_shouldOnlySearchNames.Name = "m_shouldOnlySearchNames";
            this.m_shouldOnlySearchNames.Size = new System.Drawing.Size(128, 24);
            this.m_shouldOnlySearchNames.TabIndex = 5;
            this.m_shouldOnlySearchNames.Text = "Only search names";
            // 
            // exactNameMatch
            // 
            this.exactNameMatch.Location = new System.Drawing.Point(144, 19);
            this.exactNameMatch.Name = "exactNameMatch";
            this.exactNameMatch.Size = new System.Drawing.Size(120, 24);
            this.exactNameMatch.TabIndex = 8;
            this.exactNameMatch.Text = "Exact name match";
            this.exactNameMatch.CheckedChanged += new System.EventHandler(this.exactNameMatch_CheckedChanged);
            // 
            // frmSearch
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(485, 478);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbResults);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmSearch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "RAVE Object Search";
            this.gbResults.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.searchOptionsGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }


        /// <summary>
        /// The results list_ double click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ResultsList_DoubleClick(object sender, EventArgs e)
        {
            if (this.m_resultsList.SelectedItem != null)
            {
                var objectInstance = this.m_resultsList.SelectedItem as IObjectInstance;

                if (null != objectInstance)
                {
                    this.FindObject.Raise(objectInstance);
                }
            }
        }

        /// <summary>
        /// The start status bar.
        /// </summary>
        private void StartStatusBar()
        {
            var busy = new frmBusy();
            busy.SetProgress(0);
            busy.Show();
            while (!this.m_finished)
            {
                Application.DoEvents();
                busy.SetStatusText(this.m_statusText);
                busy.SetProgress(this.m_progress);
            }

            busy.FinishedWorking();

            // Reset variables
            this.m_finished = false;
            this.m_statusText = "Searching...";
            this.m_progress = 0;
        }

        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            var t = new Thread(this.StartStatusBar);
            t.Start();
            ObservableSortedSet<IObjectInstance> hits;

            try
            {
                hits = _objectSearchProvider.Search(this.txtSearchString.Text, this.searchNameHash.Checked,
                this.m_shouldOnlySearchNames.Checked, this.m_isCaseSensitive.Checked, this.exactNameMatch.Checked);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString(), "Error", MessageBoxButtons.OK);
                return;
            }

            // All the logic below should be removed with the WPF implementation

            //Clear search results
            this.m_resultsList.Items.Clear();

            //Add hit to results
            foreach (IObjectInstance hit in hits)
            {
                this.m_resultsList.Items.Add(hit);
            }

            this.gbResults.Text = "Results - " + hits.Count + " match" + (hits.Count != 1 ? "es" : string.Empty);

            //Set as finished
            this.m_finished = true;
        }

        #endregion

        private bool shouldOnlySearchNamesBackup = false;
        private void exactNameMatch_CheckedChanged(object sender, EventArgs e)
        {
            if (exactNameMatch.Checked)
            {
                shouldOnlySearchNamesBackup = m_shouldOnlySearchNames.Checked;
                m_shouldOnlySearchNames.Checked = true;
                m_shouldOnlySearchNames.Enabled = false;
            }
            else
            {
                m_shouldOnlySearchNames.Checked = shouldOnlySearchNamesBackup;
                m_shouldOnlySearchNames.Enabled = true;
            }
        }
    }
}