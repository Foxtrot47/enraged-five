namespace Rave.Controls.Forms.Popups
{
    using System;
    using System.Windows.Forms;

    public partial class frmColumnChooser : Form
    {
        private readonly DataGridViewColumnCollection m_Columns;

        public frmColumnChooser(DataGridViewColumnCollection cols)
        {
            this.InitializeComponent();
            this.m_Columns = cols;

            foreach (DataGridViewColumn col in cols)
            {
                if (col.Visible)
                {
                    this.lbVisible.Items.Add(col.HeaderText);
                }
                else
                {
                    this.lbHidden.Items.Add(col.HeaderText);
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (this.lbHidden.SelectedItem != null)
            {
                var o = this.lbHidden.SelectedItem;
                var index = this.lbHidden.SelectedIndex;
                this.lbHidden.Items.Remove(o);
                this.lbVisible.Items.Add(o);
                if (this.lbHidden.Items.Count > 0)
                {
                    if (this.lbHidden.Items.Count >=
                        index + 1)
                    {
                        this.lbHidden.SelectedItem = this.lbHidden.Items[index];
                    }
                    else
                    {
                        this.lbHidden.SelectedItem = this.lbHidden.Items[index - 1];
                    }
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (this.lbVisible.SelectedItem != null)
            {
                var o = this.lbVisible.SelectedItem;
                var index = this.lbVisible.SelectedIndex;
                this.lbVisible.Items.Remove(o);
                this.lbHidden.Items.Add(o);
                if (this.lbVisible.Items.Count > 0)
                {
                    if (this.lbVisible.Items.Count >=
                        index + 1)
                    {
                        this.lbVisible.SelectedItem = this.lbVisible.Items[index];
                    }
                    else
                    {
                        this.lbVisible.SelectedItem = this.lbVisible.Items[index - 1];
                    }
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            foreach (string header in this.lbHidden.Items)
            {
                this.m_Columns[header].Visible = false;
            }

            foreach (string header in this.lbVisible.Items)
            {
                this.m_Columns[header].Visible = true;
            }

            this.Close();
        }
    }
}