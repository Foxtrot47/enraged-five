﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using Rave.Controls.WPF.PreviewFolder.Model;
using Rave.Utils;
using WPFToolLib.Extensions;

namespace Rave.Controls.WPF.PreviewFolder
{
    public class PreviewFolderViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<PreviewItem> Items { get; private set; }

        public RelayCommand<PreviewItem> DeleteCommand { get { return _deleteCommand; } }

        private readonly FileSystemWatcher _fileSystemWatcher;
        private readonly string _previewFolderLocation = Configuration.PreviewFolder;
        private RelayCommand<PreviewItem> _deleteCommand;

        public PreviewFolderViewModel()
        {
            if (!Directory.Exists(_previewFolderLocation))
            {
                Directory.CreateDirectory(_previewFolderLocation);
            }
            _fileSystemWatcher = new FileSystemWatcher(_previewFolderLocation);
            _fileSystemWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
          | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            _fileSystemWatcher.IncludeSubdirectories = true;
            _fileSystemWatcher.Created += FileCreatedChanged;
            _fileSystemWatcher.Deleted += FileCreatedChanged;
            _fileSystemWatcher.Changed += FileCreatedChanged;
            _fileSystemWatcher.EnableRaisingEvents = true;
            Items = new ObservableCollection<PreviewItem>(GetItems(_previewFolderLocation));

            _deleteCommand = new RelayCommand<PreviewItem>(DeleteItem);
        }

        private void DeleteItem(PreviewItem item)
        {
            if (item is PreviewBank)
            {
                File.Delete(item.Path);
            }
            else
            {
                Directory.Delete(item.Path, true);
            }
        }

        void FileCreatedChanged(object sender, FileSystemEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => UpdateItems(_previewFolderLocation));
        }

        public void UpdateItems(string path)
        {
            var rootDirectory = new DirectoryInfo(path);

            RemoveDeletedDirectories(Items, rootDirectory);

            foreach (var platformDirectory in rootDirectory.GetDirectories())
            {
                PreviewPlatform platform = (PreviewPlatform)Items.FirstOrDefault(p => p.Path == platformDirectory.FullName);

                if (platform == null)
                {
                    platform = new PreviewPlatform()
                    {
                        Name = platformDirectory.Name,
                        Path = platformDirectory.FullName
                    };
                    Items.Add(platform);
                }

                RemoveDeletedDirectories(platform.Items, platformDirectory);

                foreach (var packfolder in platformDirectory.GetDirectories())
                {
                    var pack = (PreviewPack)platform.Items.FirstOrDefault(p => p.Path == packfolder.FullName);
                    if (pack == null)
                    {
                        pack = new PreviewPack()
                        {
                            Name = packfolder.Name,
                            Path = packfolder.FullName
                        };
                        platform.Items.Add(pack);
                    }

                    RemoveDeletedFiles(pack, packfolder);

                    foreach (var bankfile in packfolder.GetFiles())
                    {
                        if (bankfile.Extension.Equals(".AWC", StringComparison.InvariantCultureIgnoreCase))
                        {
                            PreviewBank bank = (PreviewBank)pack.Items.FirstOrDefault(p => p.Path == bankfile.FullName);

                            if (bank == null)
                            {
                                bank = new PreviewBank()
                                {
                                    Name = Path.GetFileNameWithoutExtension(bankfile.Name),
                                    Path = bankfile.FullName,
                                };
                             
                            }
                            if (!bank.BuiltTime.Equals(bankfile.LastWriteTime))
                            {
                                bank.BuiltTime = bankfile.LastWriteTime;
                                SendBankUpdate(pack, bank);

                                if (pack.Items.Contains(bank))
                                {
                                    pack.Items.Remove(bank);
                                }
                                pack.Items.Add(bank);
                            }
                        }
                    }
                }
            }
        }

        private static void RemoveDeletedDirectories(ObservableCollection<PreviewItem> previewItems, DirectoryInfo directory)
        {
            string[] deletedItems = previewItems.Select(t => t.Path).Except(directory.GetDirectories()
                 .Select(s => s.FullName)).ToArray();

            foreach (string deletedItem in deletedItems)
            {
                var item = previewItems.First(p => p.Path == deletedItem);
                if (item != null)
                {
                    SendBankUpdate(item);
                    previewItems.Remove(item);
                }
            }
        }

        private static void SendBankUpdate(PreviewItem item)
        {
            PreviewPack previewPack = item as PreviewPack;
            if (previewPack != null)
            {
                foreach (PreviewItem previewItem in previewPack.Items)
                {
                    PreviewBank previewBank = (PreviewBank)previewItem;
                    SendBankUpdate(previewPack, previewBank);
                }
            }
            else
            {
                foreach (PreviewItem previewItem in item.Items)
                {
                    SendBankUpdate(previewItem);
                }
            }
        }

        private static void RemoveDeletedFiles(PreviewPack previewPack, DirectoryInfo directory)
        {
            var previewItems = previewPack.Items;

            string[] deletedItems = previewItems.Select(t => t.Path).Except(directory.GetFiles()
                .Select(s => s.FullName)).ToArray();

            foreach (string deletedItem in deletedItems)
            {
                var item = previewItems.First(p => p.Path == deletedItem);
                if (item != null)
                {
                    SendBankUpdate(previewPack, (PreviewBank)item);
                    previewItems.Remove(item);
                }
            }
        }

        private static void SendBankUpdate(PreviewPack pack, PreviewBank bank)
        {
            Instance.RaveInstance.RemoteControl.SendBankPreviewUpdated(pack.Name + "/" + bank.Name);
        }

        public List<PreviewItem> GetItems(string path)
        {
            var items = new List<PreviewItem>();

            var dirInfo = new DirectoryInfo(path);

            foreach (var directory in dirInfo.GetDirectories())
            {
                var platform = new PreviewPlatform()
                {
                    Name = directory.Name,
                    Path = directory.FullName
                };
                items.Add(platform);

                foreach (var packfolder in directory.GetDirectories())
                {
                    var pack = new PreviewPack()
                    {
                        Name = packfolder.Name,
                        Path = packfolder.FullName
                    };
                    platform.Items.Add(pack);

                    foreach (var bankfile in packfolder.GetFiles())
                    {
                        if (bankfile.Extension.Equals(".AWC", StringComparison.InvariantCultureIgnoreCase))
                        {
                            var bank = new PreviewBank()
                            {
                                Name = Path.GetFileNameWithoutExtension(bankfile.Name),
                                Path = bankfile.FullName,
                                BuiltTime = bankfile.LastWriteTime
                            };
                            pack.Items.Add(bank);
                        }
                    }
                }
            }
            return items;
        }
    }
}
