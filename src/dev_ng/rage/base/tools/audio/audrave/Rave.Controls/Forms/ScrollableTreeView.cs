// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScrollableTreeView.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The node sorter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Xml.XPath;

namespace Rave.Controls.Forms
{
    using System;
    using System.Collections;
    using System.Drawing;
    using System.Windows.Forms;

    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.ObjectBrowser.Infrastructure.Nodes;

    /// <summary>
    /// The node sorter.
    /// </summary>
    public class NodeSorter : IComparer
    {
        #region Public Methods and Operators

        /// <summary>
        /// The compare.
        /// </summary>
        /// <param name="x">
        /// The x.
        /// </param>
        /// <param name="y">
        /// The y.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int Compare(object x, object y)
        {
            var NodeX = x as TreeNode;
            var NodeY = y as TreeNode;

            bool isTagX = x is TagNode || x is PresetNode;
            bool isTagY = y is TagNode || y is PresetNode;



            if (isTagX ^ isTagY)
            {
                if (isTagX)
                {
                    return -1;
                }
                return 1;
            }
            return RaveCompare(NodeX.Text, NodeY.Text);
        }

        private int RaveCompare(string x, string y)
        {
            int result = 0;
            int i = 0;
            int z = Math.Min(x.Length, y.Length);
            while (i < z && result == 0)
            {
                if (Char.IsNumber(x[i]) && Char.IsNumber(y[i]))
                {
                    int indexOf_x = x.IndexOf('_', i);
                    indexOf_x = indexOf_x > -1 ? indexOf_x : x.Length;
                    int indexOf_y = y.IndexOf('_', i);
                    indexOf_y = indexOf_y > -1 ? indexOf_y : y.Length;


                    string xSubString = x.Substring(i, indexOf_x - i);
                    string ySubString = y.Substring(i, indexOf_y - i);
                    int xInt, yInt = 0;
                    bool bothInt;
                    bothInt = int.TryParse(xSubString, out xInt) && int.TryParse(ySubString, out yInt);
                    if (bothInt)
                    {
                        result = xInt - yInt;

                    }
                    else
                    {
                        result = string.Compare(xSubString, ySubString, StringComparison.OrdinalIgnoreCase);
                    }
                    i = indexOf_y;
                }
                else
                {

                    result = Char.ToUpper(x[i]).CompareTo(Char.ToUpper(y[i]));
                    i++;
                }
            }

            if (result == 0)
            {
                result = x.Length - y.Length;
            }
           
            return result;

        }
        
        #endregion
    }

    /// <summary>
    /// The scrollable tree view.
    /// </summary>
    /// TreeView that knows how to handle the scrolling beyond its boundaries.
    public class ScrollableTreeView : TreeView
    {

        #region Fields

        /// <summary>
        /// The m_ selected nodes.
        /// </summary>
        private readonly ArrayList m_SelectedNodes = new ArrayList();

        /// <summary>
        /// The m_ selected nodes at drag start.
        /// </summary>
        private readonly ArrayList m_SelectedNodesAtDragStart = new ArrayList();

        /// <summary>
        /// The m_ ticks.
        /// </summary>
        private long m_Ticks;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ScrollableTreeView"/> class.
        /// </summary>
        public ScrollableTreeView()
        {
            this.TreeViewNodeSorter = new NodeSorter();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ScrollableTreeView"/> class.
        /// </summary>
        /// <param name="Sorted">
        /// The sorted.
        /// </param>
        public ScrollableTreeView(bool Sorted)
        {
            if (Sorted)
            {
                this.TreeViewNodeSorter = new NodeSorter();
            }
            else
            {
                this.TreeViewNodeSorter = null;
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the last selected node.
        /// </summary>
        public TreeNode LastSelectedNode
        {
            get
            {
                if (this.m_SelectedNodes.Count > 0)
                {
                    return (TreeNode)this.m_SelectedNodes[this.m_SelectedNodes.Count - 1];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                this.UnselectAllNodes();
                if (value != null)
                {
                    this.SelectNode(value);
                    value.EnsureVisible();
                }
            }
        }

        /// <summary>
        /// Gets the selected items.
        /// </summary>
        public ArrayList SelectedItems
        {
            get
            {
                return this.m_SelectedNodes;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on before select.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnBeforeSelect(TreeViewCancelEventArgs e)
        {
            base.OnBeforeSelect(e);
            e.Cancel = true;
        }

        /// <summary>
        /// The on drag leave.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnDragLeave(EventArgs e)
        {
            // reset selection to the node that was selected when the user started dragging
            this.UnselectAllNodes();
            foreach (TreeNode n in this.m_SelectedNodesAtDragStart)
            {
                this.SelectNode(n);
            }

            base.OnDragLeave(e);
        }

        /// <summary>
        /// The on drag over.
        /// </summary>
        /// <param name="drgevent">
        /// The drgevent.
        /// </param>
        protected override void OnDragOver(DragEventArgs drgevent)
        {
            base.OnDragOver(drgevent);

            Point pt = this.PointToClient(new Point(drgevent.X, drgevent.Y));
            TreeNode node = this.GetNodeAt(pt);
            var ts = new TimeSpan(DateTime.Now.Ticks - this.m_Ticks);

            // scroll up
            if (pt.Y < this.ItemHeight)
            {
                if (node != null)
                {
                    // if within one node of top, scroll quickly
                    if (node.PrevVisibleNode != null)
                    {
                        node = node.PrevVisibleNode;
                    }

                    node.EnsureVisible();
                    this.m_Ticks = DateTime.Now.Ticks;
                }
            }
            else if (pt.Y < (this.ItemHeight * 2))
            {
                // if within two nodes of the top, scroll slowly
                if (ts.TotalMilliseconds > 350)
                {
                    if (node != null)
                    {
                        node = node.PrevVisibleNode;
                        if (node.PrevVisibleNode != null)
                        {
                            node = node.PrevVisibleNode;
                        }

                        node.EnsureVisible();
                    }

                    this.m_Ticks = DateTime.Now.Ticks;
                }
            }

            // scroll down
            if (pt.Y > this.ItemHeight)
            {
                // if within one node of top, scroll quickly
                if (node != null)
                {
                    if (node != null && node.NextVisibleNode != null)
                    {
                        node = node.NextVisibleNode;
                    }

                    if (node != null)
                    {
                        node.EnsureVisible();
                    }
                }

                this.m_Ticks = DateTime.Now.Ticks;
            }
            else if (pt.Y > (this.ItemHeight * 2))
            {
                // if within two nodes of the top, scroll slowly
                if (ts.TotalMilliseconds > 350)
                {
                    if (node != null)
                    {
                        node = node.NextVisibleNode;
                        if (node.NextVisibleNode != null)
                        {
                            node = node.NextVisibleNode;
                        }

                        node.EnsureVisible();
                    }

                    this.m_Ticks = DateTime.Now.Ticks;
                }
            }
        }

        /// <summary>
        /// The on key down.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (this.LastSelectedNode != null)
            {
                if (e.KeyCode == Keys.Left)
                {
                    this.LastSelectedNode.Collapse();
                }
                else if (e.KeyCode == Keys.Right)
                {
                    this.LastSelectedNode.Expand();
                }
                else if (e.KeyCode == Keys.Up)
                {
                    TreeNode prevNode = this.LastSelectedNode.PrevVisibleNode;
                    if (prevNode != null)
                    {
                        this.UnselectAllNodes();
                        this.SelectNode(prevNode);
                    }
                }
                else if (e.KeyCode == Keys.Down)
                {
                    TreeNode nextNode = this.LastSelectedNode.NextVisibleNode;
                    if (nextNode != null)
                    {
                        this.UnselectAllNodes();
                        this.SelectNode(nextNode);
                    }
                }
            }

            base.OnKeyDown(e);
        }

        /// <summary>
        /// The on mouse down.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            TreeNode n = this.GetNodeAt(e.X, e.Y);
            if (n != null)
            {
                this.m_SelectedNodesAtDragStart.Clear();

                // is this node already selected?
                if (this.m_SelectedNodes.Contains(n))
                {
                    if (Control.ModifierKeys == Keys.Control)
                    {
                        // control is pressed - remove this from selection
                        this.UnselectNode(n);
                    }
                }
                else
                {
                    TreeNode lastNode = null;

                    if (this.m_SelectedNodes.Count > 0)
                    {
                        lastNode = (TreeNode)this.m_SelectedNodes[this.m_SelectedNodes.Count - 1];
                    }

                    if (Control.ModifierKeys != Keys.Control)
                    {
                        this.UnselectAllNodes();
                    }

                    if (Control.ModifierKeys == Keys.Shift)
                    {
                        if (lastNode != null && lastNode.Parent == n.Parent)
                        {
                            // select all nodes between lastNode and this node
                            for (int i = lastNode.Index; i != n.Index; i += Math.Sign(n.Index - lastNode.Index))
                            {
                                if (lastNode.Parent != null)
                                {
                                    this.SelectNode(lastNode.Parent.Nodes[i]);
                                }
                                else
                                {
                                    this.SelectNode(this.Nodes[i]);
                                }
                            }
                        }
                    }

                    this.SelectNode(n);
                }

                this.m_SelectedNodesAtDragStart.AddRange(this.m_SelectedNodes);
            }
            else
            {
                this.UnselectAllNodes();
            }
        }

        /// <summary>
        /// The on mouse up.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            // dont unselect on right click since it screws with popup menus
            if (e.Button != MouseButtons.Left)
            {
                return;
            }

            var n = this.GetNodeAt(e.X, e.Y);
            if (n != null)
            {
                // is this node already selected?
                if (this.m_SelectedNodes.Contains(n))
                {
                    if (Control.ModifierKeys != Keys.Control && Control.ModifierKeys != Keys.Shift)
                    {
                        this.UnselectAllNodes();
                        this.SelectNode(n);
                    }
                }
            }
        }

        /// <summary>
        /// The select node.
        /// </summary>
        /// <param name="n">
        /// The n.
        /// </param>
        private void SelectNode(TreeNode n)
        {
            if (!this.m_SelectedNodes.Contains(n))
            {
                this.m_SelectedNodes.Add(n);
                n.BackColor = SystemColors.Highlight;
                n.ForeColor = SystemColors.HighlightText;
            }

        }

        /// <summary>
        /// The unselect all nodes.
        /// </summary>
        private void UnselectAllNodes()
        {
            foreach (TreeNode sn in this.m_SelectedNodes)
            {
                sn.BackColor = Color.White;
                sn.ForeColor = EditorTreeNode.WaveRequiresGestures(sn) ? Color.MediumBlue : Color.Black;
                UpdateMuteSoloColor(sn);
            }

            this.m_SelectedNodes.Clear();
        }

        /// <summary>
        /// The unselect node.
        /// </summary>
        /// <param name="n">
        /// The n.
        /// </param>
        private void UnselectNode(TreeNode n)
        {
            this.m_SelectedNodes.Remove(n);
            n.BackColor = Color.White;
            n.ForeColor = EditorTreeNode.WaveRequiresGestures(n) ? Color.MediumBlue : Color.Black;
            UpdateMuteSoloColor(n);
        }

        private void UpdateMuteSoloColor(TreeNode n)
        {
            if (n is SoundNode)
            {
                SoundNode sn = (SoundNode)n;
                if (sn.ObjectInstance.IsSoloed)
                {
                    sn.ForeColor = Color.Blue;
                }
                if (sn.ObjectInstance.IsMuted)
                {
                    sn.ForeColor = Color.DarkOrange;
                }
            }

        }

        //Prevent treeview from flickering!
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            SendMessage(this.Handle, TVM_SETEXTENDEDSTYLE, (IntPtr)TVS_EX_DOUBLEBUFFER, (IntPtr)TVS_EX_DOUBLEBUFFER);
        }
        // Pinvoke:
        private const int TVM_SETEXTENDEDSTYLE = 0x1100 + 44;
        private const int TVM_GETEXTENDEDSTYLE = 0x1100 + 45;
        private const int TVS_EX_DOUBLEBUFFER = 0x0004;
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);


        #endregion
    }
}