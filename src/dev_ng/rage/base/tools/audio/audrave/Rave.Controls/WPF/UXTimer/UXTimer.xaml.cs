﻿using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using Rave.Controls.Forms;

namespace Rave.Controls.WPF.UXTimer
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Controls;

    using Xceed.Wpf.AvalonDock;

    public class UXTimerHelper
    {
        int m_NumStatesToPop;

        public void Enter(string identifier)
        {
            UXTimer.Instance.EnterState(identifier);
            this.m_NumStatesToPop++;
        }

        public void Exit()
        {
            UXTimer.Instance.ExitState();
            this.m_NumStatesToPop--;
        }

        public void ExitAll()
        {
            while (this.m_NumStatesToPop-- > 0)
            {
                UXTimer.Instance.ExitState();
            }
        }
    }

    /// <summary>
    /// Interaction logic for UXTimer.xaml
    /// </summary>
    public partial class UXTimer : UserControl
    {
        private FlowDocument _flowDocument = new FlowDocument();
        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_Content;
        public Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable DockableContent
        {
            get
            {
                if (this.m_Content == null)
                {
                    this.m_Content = new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
                    {
                        Title = "Activity Log",
                        Content = this,
                        FloatingHeight = DockableWFControl.FLOATING_HEIGHT,
                        FloatingWidth = DockableWFControl.FLOATING_WIDTH,
                        FloatingLeft = DockableWFControl.FLOATING_LEFT,
                        FloatingTop = DockableWFControl.FLOATING_TOP
                    };
                }
                return this.m_Content;
            }
        }

        static UXTimer sm_Instance;

        public UXTimer()
        {
            sm_Instance = this;
            this.m_StateStack = new Stack<State>();
            this.InitializeComponent();
            Style style = new Style(typeof(Paragraph));
            style.Setters.Add(new Setter(Block.MarginProperty, new Thickness(0)));
            _flowDocument.Resources.Add(typeof(Paragraph), style);
            textLog.Document = _flowDocument;
        }

        public static UXTimer Instance
        {
            get
            {
                return sm_Instance;
            }
        }

        struct State
        {
            public string Identifier;
            public DateTime EntryTime;
            public bool HasPrinted;
        }

        Stack<State> m_StateStack;
        public void EnterState(string stateIdentifier)
        {
            if (this.m_StateStack.Count > 0)
            {
                var s = this.m_StateStack.Peek();
                if (!s.HasPrinted)
                {
                    this.AddLogEntry(this.m_StateStack.Count - 1, s.Identifier, null);
                    s.HasPrinted = true;
                }
            }
            this.m_StateStack.Push(new State { Identifier = stateIdentifier, EntryTime = DateTime.Now });
        }

        public void ExitState()
        {
            var s = this.m_StateStack.Pop();
            var duration = DateTime.Now - s.EntryTime;

            this.AddLogEntry(this.m_StateStack.Count, s.Identifier, duration.TotalSeconds);
        }

        public void AppendNewLine(string text, Brush colorBrush = null)
        {
            if (colorBrush == null)
            {
                colorBrush = Brushes.Black;
            }
            var timestamp = DateTime.Now.ToString("HH:mm:ss.f");
            var newLine = string.Format("{0}: {1}", timestamp, text);

            if (!this.textLog.Dispatcher.CheckAccess())
            {
                this.textLog.Dispatcher.Invoke(
                    System.Windows.Threading.DispatcherPriority.Normal,
                    new Action(delegate
                                   {
                                       _flowDocument.Blocks.Add(new Paragraph(new Run(newLine) { Foreground = colorBrush }));
                                       this.textLog.ScrollToEnd();
                                   })
                    );
            }
            else
            {
                _flowDocument.Blocks.Add(new Paragraph(new Run(newLine) { Foreground = colorBrush }));
                this.textLog.ScrollToEnd();
            }
        }

        public void AddTextToBlock(string text, string blockId, Brush colorBrush = null)
        {
            if (colorBrush == null)
            {
                colorBrush = Brushes.Black;
            }
            var timestamp = DateTime.Now.ToString("HH:mm:ss.f");
            var loadingIndicator = string.Format("{0}: {1}", timestamp, text);



            if (!this.textLog.Dispatcher.CheckAccess())
            {
                this.textLog.Dispatcher.Invoke(
                    System.Windows.Threading.DispatcherPriority.Normal,
                    new Action(delegate
                    {
                        Paragraph newBlock = new Paragraph(new Run(loadingIndicator) { Foreground = colorBrush }) { Name = blockId };
                        var block = _flowDocument.Blocks.FirstOrDefault(p => p.Name == blockId);
                        if (block != null)
                        {

                            _flowDocument.Blocks.InsertBefore(block, newBlock);
                            _flowDocument.Blocks.Remove(block);
                        }
                        else
                        {
                            _flowDocument.Blocks.Add(block = newBlock);
                            this.textLog.ScrollToEnd();
                        }


                    })
                    );
            }
            else
            {
                Paragraph newBlock = new Paragraph(new Run(loadingIndicator) { Foreground = colorBrush }) { Name = blockId };
                var block = _flowDocument.Blocks.FirstOrDefault(p => p.Name == blockId);
                if (block != null)
                {

                    _flowDocument.Blocks.InsertBefore(block, newBlock);
                    _flowDocument.Blocks.Remove(block);
                }
                else
                {
                    _flowDocument.Blocks.Add(block = newBlock);
                    this.textLog.ScrollToEnd();
                }

            }

        }


        void AddLogEntry(int tabLevel, string identifier, double? durationSeconds)
        {
            var tabs = "";
            if (tabLevel > 0)
            {
                for (int i = 0; i < tabLevel; i++)
                {
                    tabs += "-";
                }
                tabs += "> ";
            }

            string newLine;
            if (durationSeconds != null)
            {
                newLine = string.Format("{0}{1}: {2}", tabs, identifier, durationSeconds);
            }
            else
            {
                newLine = string.Format("{0}{1}", tabs, identifier);
            }
            this.AppendNewLine(newLine);
        }
    }
}
