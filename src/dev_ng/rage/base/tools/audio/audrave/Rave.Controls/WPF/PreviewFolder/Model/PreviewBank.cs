﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rave.Controls.WPF.PreviewFolder.Model
{
    public class PreviewBank : PreviewItem
    {
        public DateTime BuiltTime { get; set; }
    }
}
