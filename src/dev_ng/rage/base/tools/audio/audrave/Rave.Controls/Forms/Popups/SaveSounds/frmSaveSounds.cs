// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmSaveSounds.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for frmSaveSounds.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.Controls.Forms.Popups.SaveSounds
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Metadata.Infrastructure.Interfaces;

    using rage.ToolLib.Logging;

    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Utils;

    /// <summary>
    /// Summary description for frmSaveSounds.
    /// </summary>
    public class frmSaveSounds : Form
    {
        #region Fields

        /// <summary>
        /// The m_log.
        /// </summary>
        private readonly ILog m_log;

        /// <summary>
        /// The compiler.
        /// </summary>
        private readonly IXmlBankMetaDataCompiler compiler;

        /// <summary>
        /// The m_btn cancel.
        /// </summary>
        private Button m_btnCancel;

        /// <summary>
        /// The m_btn save.
        /// </summary>
        private Button m_btnSave;

        /// <summary>
        /// The m_comment.
        /// </summary>
        private TextBox m_comment;

        /// <summary>
        /// The m_lbl comment.
        /// </summary>
        private Label m_lblComment;

        /// <summary>
        /// The m_panel 1.
        /// </summary>
        private Panel m_panel1;

        /// <summary>
        /// The m_panel 2.
        /// </summary>
        private Panel m_panel2;

        /// <summary>
        /// The m_table layout panel.
        /// </summary>
        private TableLayoutPanel m_tableLayoutPanel;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="frmSaveSounds"/> class.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="checkedOutBanks">
        /// The checked out banks.
        /// </param>
        /// <param name="compiler">
        /// The compiler.
        /// </param>
        public frmSaveSounds(ILog log, ICollection<KeyValuePair<string, List<IXmlBank>>> checkedOutBanks, IXmlBankMetaDataCompiler compiler)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }

            this.m_log = log;
            this.compiler = compiler;
            this.BanksToSave = new List<IXmlBank>();
            this.BanksToCheckIn = new List<IXmlBank>();
            this.BanksToUndoCheckOut = new List<IXmlBank>();

            this.InitializeComponent();

            if (checkedOutBanks.Count > 1)
            {
                this.m_tableLayoutPanel.RowCount = 2;
            }

            var row = 0;
            var col = 0;

            foreach (var bankList in checkedOutBanks)
            {
                if (row % 2 == 0)
                {
                    this.m_tableLayoutPanel.Controls.Add(new ctrlSaveBankGroup(bankList), col, row);
                    col = (col + 1) % 2;
                }
                else
                {
                    this.m_tableLayoutPanel.Controls.Add(new ctrlSaveBankGroup(bankList), col, row);
                    row ++;
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the banks to check in.
        /// </summary>
        public IList<IXmlBank> BanksToCheckIn { get; private set; }

        /// <summary>
        /// Gets the banks to save.
        /// </summary>
        public IList<IXmlBank> BanksToSave { get; private set; }

        /// <summary>
        /// Gets the banks to undo check out.
        /// </summary>
        public IList<IXmlBank> BanksToUndoCheckOut { get; private set; }

        /// <summary>
        /// Gets the comment.
        /// </summary>
        public string Comment
        {
            get
            {
                return this.m_comment.Text;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The clear lists.
        /// </summary>
        private void ClearLists()
        {
            this.BanksToCheckIn.Clear();
            this.BanksToUndoCheckOut.Clear();
            this.BanksToSave.Clear();
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btnSave = new System.Windows.Forms.Button();
            this.m_panel2 = new System.Windows.Forms.Panel();
            this.m_btnCancel = new System.Windows.Forms.Button();
            this.m_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.m_panel1 = new System.Windows.Forms.Panel();
            this.m_comment = new System.Windows.Forms.TextBox();
            this.m_lblComment = new System.Windows.Forms.Label();
            this.m_panel2.SuspendLayout();
            this.m_panel1.SuspendLayout();
            this.SuspendLayout();

            // btnSave
            this.m_btnSave.Anchor =
                (System.Windows.Forms.AnchorStyles)
                ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right));
            this.m_btnSave.Location = new System.Drawing.Point(681, 6);
            this.m_btnSave.Name = "m_btnSave";
            this.m_btnSave.Size = new System.Drawing.Size(80, 24);
            this.m_btnSave.TabIndex = 3;
            this.m_btnSave.Text = "Ok";
            this.m_btnSave.Click += new System.EventHandler(this.btnSave_Click);

            // panel2
            this.m_panel2.Controls.Add(this.m_btnCancel);
            this.m_panel2.Controls.Add(this.m_btnSave);
            this.m_panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_panel2.Location = new System.Drawing.Point(0, 700);
            this.m_panel2.Name = "m_panel2";
            this.m_panel2.Size = new System.Drawing.Size(863, 40);
            this.m_panel2.TabIndex = 1;

            // btnCancel
            this.m_btnCancel.Anchor =
                (System.Windows.Forms.AnchorStyles)
                ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right));
            this.m_btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_btnCancel.Location = new System.Drawing.Point(776, 7);
            this.m_btnCancel.Name = "m_btnCancel";
            this.m_btnCancel.Size = new System.Drawing.Size(75, 23);
            this.m_btnCancel.TabIndex = 4;
            this.m_btnCancel.Text = "Cancel";
            this.m_btnCancel.UseVisualStyleBackColor = true;
            this.m_btnCancel.Click += new System.EventHandler(this.btnCancel_OnClick);

            // m_TableLayoutPanel
            this.m_tableLayoutPanel.AutoScroll = true;
            this.m_tableLayoutPanel.ColumnCount = 2;
            this.m_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.m_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.m_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.m_tableLayoutPanel.Name = "m_tableLayoutPanel";
            this.m_tableLayoutPanel.RowCount = 1;
            this.m_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.m_tableLayoutPanel.Size = new System.Drawing.Size(863, 588);
            this.m_tableLayoutPanel.TabIndex = 2;

            // panel1
            this.m_panel1.Controls.Add(this.m_comment);
            this.m_panel1.Controls.Add(this.m_lblComment);
            this.m_panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_panel1.Location = new System.Drawing.Point(0, 588);
            this.m_panel1.Name = "m_panel1";
            this.m_panel1.Size = new System.Drawing.Size(863, 112);
            this.m_panel1.TabIndex = 0;

            // m_Comment
            this.m_comment.Anchor =
                (System.Windows.Forms.AnchorStyles)
                (((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right));
            this.m_comment.Location = new System.Drawing.Point(72, 14);
            this.m_comment.Multiline = true;
            this.m_comment.Name = "m_comment";
            this.m_comment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_comment.Size = new System.Drawing.Size(779, 83);
            this.m_comment.TabIndex = 1;

            // lblComment
            this.m_lblComment.Anchor =
                (System.Windows.Forms.AnchorStyles)
                ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left));
            this.m_lblComment.AutoSize = true;
            this.m_lblComment.Location = new System.Drawing.Point(12, 17);
            this.m_lblComment.Name = "m_lblComment";
            this.m_lblComment.Size = new System.Drawing.Size(54, 13);
            this.m_lblComment.TabIndex = 0;
            this.m_lblComment.Text = "Comment:";

            // frmSaveSounds
            this.AcceptButton = this.m_btnSave;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(863, 740);
            this.ControlBox = false;
            this.Controls.Add(this.m_tableLayoutPanel);
            this.Controls.Add(this.m_panel1);
            this.Controls.Add(this.m_panel2);
            this.MinimizeBox = false;
            this.Name = "frmSaveSounds";
            this.ShowInTaskbar = false;
            this.Text = "Save/Check-in Banks";
            this.m_panel2.ResumeLayout(false);
            this.m_panel1.ResumeLayout(false);
            this.m_panel1.PerformLayout();
            this.ResumeLayout(false);
        }

        /// <summary>
        /// The btn cancel_ on click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void btnCancel_OnClick(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// The btn save_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            this.ClearLists();
            foreach (ctrlSaveBankGroup group in this.m_tableLayoutPanel.Controls)
            {
                var checkIn = new List<IXmlBank>();
                var undo = new List<IXmlBank>();
                var save = new List<IXmlBank>();

                group.GetBanks(checkIn, undo, save);

                foreach (var bank in checkIn)
                {
                    this.BanksToCheckIn.Add(bank);
                }

                foreach (var bank in undo)
                {
                    this.BanksToUndoCheckOut.Add(bank);
                }

                foreach (var bank in save)
                {
                    this.BanksToSave.Add(bank);
                }
            }

            foreach (var obj in this.BanksToCheckIn)
            {
                if (!RaveInstance.AssetManager.IsResolved(obj.FilePath))
                {
                    ErrorManager.HandleInfo(
                        string.Format("File {0} needs resolved. Please resolve through Perforce", obj.FilePath));
                    this.ClearLists();
                    return;
                }

                var objectBank = obj as IObjectBank;
                if (null != objectBank && !objectBank.Validate())
                {
                    this.ClearLists();
                    return;
                }

                if (!(obj is ITemplateBank))
                {
                    if (!this.compiler.CompileMetadata(obj))
                    {
                        this.ClearLists();
                        return;
                    }
                }
            }

            if (this.BanksToCheckIn.Any() && string.IsNullOrEmpty(this.Comment))
            {
                ErrorManager.HandleInfo("Comment must be provided when checking in changes");
                this.DialogResult = DialogResult.None;
            }
            else
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        #endregion
    }
}