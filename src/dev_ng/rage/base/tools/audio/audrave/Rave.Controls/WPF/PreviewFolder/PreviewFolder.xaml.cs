﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Rave.Controls.Forms;
using Xceed.Wpf.AvalonDock.Layout;

namespace Rave.Controls.WPF.PreviewFolder
{
    /// <summary>
    /// Interaction logic for PreviewFolder.xaml
    /// </summary>
    public partial class PreviewFolder : UserControl
    {
        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable _content;
        public new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable Content
        {
            get
            {
                if (this._content == null)
                {
                    this._content = new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
                    {
                        Title = "Preview Folder",
                        Content = this,
                        FloatingHeight = DockableWFControl.FLOATING_HEIGHT,
                        FloatingWidth = DockableWFControl.FLOATING_WIDTH,
                        FloatingLeft = DockableWFControl.FLOATING_LEFT,
                        FloatingTop = DockableWFControl.FLOATING_TOP
                    };
                }
                return this._content;
            }
        }

        public PreviewFolder()
        {
            InitializeComponent();
            DataContext = new PreviewFolderViewModel();
        }
    }
}
