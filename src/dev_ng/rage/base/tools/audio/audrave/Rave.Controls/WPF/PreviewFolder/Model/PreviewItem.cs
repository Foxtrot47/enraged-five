﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rave.Controls.WPF.PreviewFolder.Model
{
    public class PreviewItem
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public ObservableCollection<PreviewItem> Items { get; set; }
    }
}
