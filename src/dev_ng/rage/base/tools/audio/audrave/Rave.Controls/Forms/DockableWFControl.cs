using System.ComponentModel;
using System.Reflection;

namespace Rave.Controls.Forms
{
    using System.Windows.Forms.Integration;
    using System.Windows.Forms;

    using Xceed.Wpf.AvalonDock;

    public class DockableWFControl : ContainerControl
    {
        public static readonly int FLOATING_HEIGHT = 350;
        public static readonly int FLOATING_WIDTH = 250;

        private static int actualFloatingLeft = 50;
        private static int actualFloatingTop = 50;
        public static int FLOATING_LEFT 
        {
            get
            {
                actualFloatingLeft += 20;
                if (actualFloatingLeft - 50 > 250) actualFloatingLeft -= 250;
                return actualFloatingLeft;
            }
        }

        public static int FLOATING_TOP
        {
            get
            {
                actualFloatingTop += 20;
                if (actualFloatingTop - 50 > 250) actualFloatingTop -= 250;
                return actualFloatingTop;
            }
        }

        Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable m_DockableContent;
        public Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable Content
        {
            get
            {
                return this.m_DockableContent
                       ?? (this.m_DockableContent =
                           new Xceed.Wpf.AvalonDock.Layout.LayoutAnchorable
                               {
                                   Title = Text,
                                   Content = new WindowsFormsHost { Child = this },
                                   FloatingHeight = FLOATING_HEIGHT,
                                   FloatingWidth = FLOATING_WIDTH,
                                   FloatingLeft = FLOATING_LEFT,
                                   FloatingTop = FLOATING_TOP
                               });
            }
        }

    }

}

