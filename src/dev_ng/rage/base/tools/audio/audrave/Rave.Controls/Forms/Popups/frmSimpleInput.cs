namespace Rave.Controls.Forms.Popups
{
    using System;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Utils;

    /// <summary>
    ///   Summary description for frmSimpleInput.
    /// </summary>
    public class frmSimpleInput : Form
    {
        private readonly bool m_validate;
        private Button btnCancel;
        private Button btnOk;
        private Label lblHelpString;
        private TextBox txtInput;

        public frmSimpleInput(bool validate)
        {
            //
            // Required for Windows Form Designer support
            //
            this.InitializeComponent();

            this.m_validate = validate;
            if (!this.m_validate)
            {
                this.txtInput.CharacterCasing = CharacterCasing.Normal;
            }
        }

        public static bool AllowEmptyInput { get; set; }

        public static InputResult GetInput(IWin32Window parent, string helpText)
        {
            return GetInput(parent, helpText, "", true);
        }

        public static InputResult GetInput(IWin32Window parent, string helpText, string initialValue)
        {
            return GetInput(parent, helpText, initialValue, true);
        }

        public static InputResult GetInput(string helpText)
        {
            return GetInput(RaveInstance.ActiveWindow, helpText);
        }

        public static InputResult GetInput(string helpText, string initialValue)
        {
            return GetInput(RaveInstance.ActiveWindow, helpText, initialValue);
        }

        public static InputResult GetInput(IWin32Window parent, string helpText, bool validate)
        {
            return GetInput(parent, helpText, "", validate);
        }

        public struct InputResult
        {
            public bool HasBeenCancelled;
            public string data;
        }

        public static InputResult GetInput(IWin32Window parent, string helpText, string initialValue, bool validate, bool allowEmptyInput = false)
        {
            var frm = new frmSimpleInput(validate)
                          {
                              lblHelpString = {Text = helpText==null?"":helpText + ":"},
                              txtInput = {Text = initialValue, SelectionStart = 0, SelectionLength = initialValue==null?0:initialValue.Length}
                          };

            InputResult result = new InputResult();
            bool illegalEmptyInput = false;

            do
            {
                if (illegalEmptyInput) ErrorManager.HandleInfo("Empty input not valid!");
                System.Windows.Forms.DialogResult dialogResult = frm.ShowDialog();

                result.HasBeenCancelled = (dialogResult == System.Windows.Forms.DialogResult.Cancel ||
                                           dialogResult == DialogResult.Abort);
                illegalEmptyInput = (frm.txtInput.Text.Length == 0 && !allowEmptyInput);

            } while (!result.HasBeenCancelled && illegalEmptyInput);

            if (result.HasBeenCancelled)
            {
                result.data = null;
                result.HasBeenCancelled = true;
            }
            else
            {
                result.data = validate ? RaveUtils.FormatGameString(frm.txtInput.Text) : frm.txtInput.Text;
            }

            
            frm.Dispose();
            return result;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (!this.m_validate ||
                this.txtInput.Text.Length > 0)
            {
                this.Close();
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///   Required method for Designer support - do not modify
        ///   the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblHelpString = new System.Windows.Forms.Label();
			this.txtInput = new System.Windows.Forms.TextBox();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblHelpString
			// 
			this.lblHelpString.AutoSize = true;
			this.lblHelpString.Location = new System.Drawing.Point(8, 8);
			this.lblHelpString.Name = "lblHelpString";
			this.lblHelpString.Size = new System.Drawing.Size(58, 13);
			this.lblHelpString.TabIndex = 0;
			this.lblHelpString.Text = "_helpstring";
			// 
			// txtInput
			// 
			this.txtInput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtInput.Location = new System.Drawing.Point(8, 24);
			this.txtInput.Name = "txtInput";
			this.txtInput.Size = new System.Drawing.Size(416, 20);
			this.txtInput.TabIndex = 1;
			// 
			// btnOk
			// 
			this.btnOk.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Location = new System.Drawing.Point(150, 55);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(75, 23);
			this.btnOk.TabIndex = 2;
			this.btnOk.Text = "Ok";
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(225, 55);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Cancel";
			// 
			// frmSimpleInput
			// 
			this.AcceptButton = this.btnOk;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(434, 80);
			this.ControlBox = false;
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.txtInput);
			this.Controls.Add(this.lblHelpString);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.MaximumSize = new System.Drawing.Size(1000, 114);
			this.MinimumSize = new System.Drawing.Size(282, 114);
			this.Name = "frmSimpleInput";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Input";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
    }
}