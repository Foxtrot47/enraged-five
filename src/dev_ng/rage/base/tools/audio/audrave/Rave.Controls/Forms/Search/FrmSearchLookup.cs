namespace Rave.Controls.Forms.Search
{
    using System.Windows.Forms;

    public partial class FrmSearchLookup : Form
    {
        private readonly string m_Type;

        public FrmSearchLookup(string type)
        {
            this.InitializeComponent();
            this.m_Type = type;
        }
    }
}