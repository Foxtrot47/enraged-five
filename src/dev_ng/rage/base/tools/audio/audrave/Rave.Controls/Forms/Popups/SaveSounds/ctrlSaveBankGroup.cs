namespace Rave.Controls.Forms.Popups.SaveSounds
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;

    public partial class ctrlSaveBankGroup : UserControl
    {
        private readonly List<ctrlSaveCheckIn> m_components;

        public ctrlSaveBankGroup(KeyValuePair<string, List<IXmlBank>> banks)
        {
            this.InitializeComponent();
            this.m_GroupBox.Text = banks.Key;
            this.m_components = new List<ctrlSaveCheckIn>();

            foreach (var ob in banks.Value)
            {
                var saveCheck = new ctrlSaveCheckIn(ob, ob.IsDirty, !ob.IsLocalCheckout, true);
                this.m_SaveList.Controls.Add(saveCheck);
                this.m_components.Add(saveCheck);
            }
        }

        private void CheckInAll_Changed(object sender, EventArgs e)
        {
            foreach (ctrlSaveCheckIn sc in this.m_components)
            {
                sc.SetCheckInCheck(this.m_ChkCheckInAll.Checked);
                if (this.m_ChkCheckInAll.Checked)
                {
                    sc.SetUndoCheck(false);
                }
            }
        }

        private void UndoAll_Changed(object sender, EventArgs e)
        {
            foreach (ctrlSaveCheckIn sc in this.m_components)
            {
                sc.SetUndoCheck(this.m_ChkUndoAll.Checked);
                if (this.m_ChkUndoAll.Checked)
                {
                    sc.SetCheckInCheck(false);
                }
            }
        }

        private void SaveAll_Changed(object sender, EventArgs e)
        {
            foreach (ctrlSaveCheckIn sc in this.m_components)
            {
                sc.SetSaveCheck(this.m_ChkSaveAll.Checked);
                if (this.m_ChkSaveAll.Checked)
                {
                    sc.SetUndoCheck(false);
                }
            }
        }

        public void GetBanks(IList<IXmlBank> checkIn, IList<IXmlBank> undo, IList<IXmlBank> save)
        {
            foreach (ctrlSaveCheckIn sc in this.m_components)
            {
                if (sc.IsCheckInChecked())
                {
                    checkIn.Add(sc.Bank);
                }
                else if (sc.IsUndoChecked())
                {
                    undo.Add(sc.Bank);
                }
                else if (sc.IsSaveChecked())
                {
                    save.Add(sc.Bank);
                }
            }
        }
    }
}