namespace Rave.OrphanedWaves
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Controls.Forms.Popups;
    using Rave.ObjectBrowser.Infrastructure.Interfaces;
    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    public class OrphanedWaves : IRAVEReportPlugin
    {
        private string m_name;

        private List<string> m_skipPacks;

        #region IRAVEReportPlugin Members

        public string GetName()
        {
            return m_name;
        }

        public bool Init(XmlNode settings)
        {
            m_skipPacks = new List<string>();
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        m_name = setting.InnerText;
                        break;
                    case "Skip":
                        m_skipPacks.Add(setting.InnerText.ToUpper());
                        break;
                }
            }
            return m_name != string.Empty;
        }

        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            //Add html tags
            var main = new StringBuilder();
            main.Append("<html><head><title>Orphaned Waves/Midis</title></head><body>");

            // get list of wave nodes
            var waveNodes = new List<WaveNode>();
            foreach (EditorTreeNode editorTreeNode in waveBrowser.GetEditorTreeView().GetNodes())
            {
                var pack = editorTreeNode as PackNode;
                if (pack != null)
                {
                    // skipping allows us to ignore speech and radio,these sound wave refs are handled else where
                    var objectName = pack.GetObjectName();
                    if (!m_skipPacks.Any(objectName.Contains))
                    {
                        // Only add wave nodes that don't have a voice tag above them in the hierarchy
                        waveNodes.AddRange(pack.GetChildWaveNodes().Cast<WaveNode>().Where(
                            waveNode => !HierarchyContainsVoiceTag(waveNode)));
                    }
                }
            }

            // find references to waves/midis from sounds
            var soundBrowser = browsers["SOUNDS"];
            var waveRefs = GetWaveRefs(soundBrowser.GetTreeView().Nodes);

            // compare and display reults
            var links = new StringBuilder();
            var waves = new StringBuilder();
            var currentPack = string.Empty;
            foreach (var waveNode in waveNodes)
            {
                var waveRef = waveNode.GetBankPath() + "\\" + waveNode.GetObjectName();
                waveRef = waveRef.Substring(0, waveRef.Length - 4);
                if (!waveRefs.Contains(waveRef))
                {
                    var packName = waveRef.Substring(0, waveRef.IndexOf("\\"));
                    if (packName != currentPack)
                    {
                        /*pack links are added here as we don't want links to pack's whose child
                        waves are all referenced in sounds*/

                        links.Append("<a href=\"#" + packName + "\" target=\"_self\"\">" + packName + "</a><br>");

                        waves.Append("<br><b><a name=\"" + packName + "\">" + packName + "</a></b><br>");

                        currentPack = packName;
                    }

                    waves.Append(waveNode.GetObjectPath() + "<br>");
                }
            }

            main.Append(links);
            main.Append(waves);
            main.Append("</body></html>");

            //Have to write this to file rather than setting the document text as
            //setting the document text means that there is no url which screws with 
            //the internal links

            var fileName = Path.GetTempPath() + "output.html";
            var fs = new FileStream(fileName, FileMode.Create);
            var sw = new StreamWriter(fs);
            sw.Write(main.ToString());
            sw.Close();
            fs.Close();

            var report = new frmReport(m_name + " Report");
            report.SetURL(fileName);
            report.Show();
        }

        #endregion

        private static HashSet<string> GetWaveRefs(TreeNodeCollection nodes)
        {
            var waveRefs = new HashSet<string>();
            GetWaveRefs(nodes, waveRefs);
            return waveRefs;
        }

        private static void GetWaveRefs(TreeNodeCollection treeNodes, ISet<string> waveRefs)
        {
            foreach (TreeNode treeNode in treeNodes)
            {
                var soundNode = treeNode as SoundNode;
                
                if (soundNode != null)
                {
                    foreach (XmlNode node in soundNode.ObjectInstance.Node.ChildNodes)
                    {
                        if (RaveUtils.IsPopulatedWaveReference(node))
                        {
                            var builder = new StringBuilder(node.ChildNodes[1].InnerText);
                            builder.Append(Path.DirectorySeparatorChar);
                            builder.Append(node.ChildNodes[0].InnerText);
                            var waveRef = builder.ToString();
                            if (!waveRefs.Contains(waveRef))
                            {
                                waveRefs.Add(waveRef);
                            }
                        }
                    }
                }
                else
                {
                    GetWaveRefs(treeNode.Nodes, waveRefs);
                }
            }
        }

        private static bool HierarchyContainsVoiceTag(TreeNode node)
        {
            // Check for voice tag in current node's list of children
            foreach (var childNode in node.Nodes)
            {
                var tagNode = childNode as TagNode;
                if (null != tagNode && tagNode.Text.StartsWith("voice"))
                {
                    return true;
                }
            }

            // Recursively check parent nodes
            return null != node.Parent && HierarchyContainsVoiceTag(node.Parent);
        }
    }
}