namespace Rave.CategoryEditor
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using Rave.CategoryEditor.Editors;
    using Rave.HierarchyBrowser.Infrastructure.Nodes;
    using Rave.PropertiesEditor;
    using Rave.PropertiesEditor.Infrastructure.Structs;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public partial class ctrlParent : UserControl,
                                      IExpandable,
                                      ISelectable
    {
        private readonly int m_LargeHeight;
        private readonly IObjectInstance m_ObjectInstance;
        private readonly int m_SmallHeight;
        private readonly HierarchyNode _mHierarchyNode;
        private ctrlCategoryFieldEditor m_CurrentControl;
        private IFieldDefinition m_Field;
        private Mode m_Mode;

        public ctrlParent(IObjectInstance objectInstance, HierarchyNode hierarchyNode)
        {
            this._mHierarchyNode = hierarchyNode;
            m_ObjectInstance = objectInstance;
            InitializeComponent();
            m_Field =
                objectInstance.TypeDefinitions.FindTypeDefinition(objectInstance.TypeName).FindFieldDefinition("Volume");
            var node = PropertyEditorUtil.FindXmlNode(m_Field.Name, m_ObjectInstance.Node);

            var slider = new ctrlSliderEditor(m_Field, node, objectInstance, null);
            m_CurrentControl = slider;
            m_CurrentControl.Top = m_ToolStrip.Bottom;
            m_CurrentControl.Visible = false;

            Controls.Add(m_CurrentControl);

            Text = objectInstance.Name;
            DoubleBuffered = true;
            m_LargeHeight = Height;
            m_SmallHeight = m_CurrentControl.Bottom;
            SetMode(Mode.SMALL);
            MouseEnter += Selected;
            m_ToolStrip.OnExpand += m_ToolStrip_OnExpand;
            m_ToolStrip.MouseEnter += Selected;
            m_ListView.MouseEnter += Selected;
            m_ListView.Populate(objectInstance);
        }

        public override string Text
        {
            set { m_ToolStrip.Text = value; }
        }

        #region IExpandable Members

        public void Expand()
        {
            if (m_Mode == Mode.LARGE)
            {
                SetMode(Mode.SMALL);
            }
            else
            {
                SetMode(Mode.LARGE);
            }
        }

        #endregion

        #region ISelectable Members

        void ISelectable.Select()
        {
            m_CurrentControl.SetActive(true);
        }

        void ISelectable.Deselect()
        {
            m_CurrentControl.SetActive(false);
        }

        #endregion

        public event Action<Control> OnClickEvent;

        public void Selected(object sender, EventArgs e)
        {
            if (OnClickEvent != null)
            {
                OnClickEvent(this);
            }

            if (m_CurrentControl.Visible)
            {
                m_CurrentControl.m_lblFieldName_Click(null, null);
            }
        }

        private void m_ToolStrip_OnExpand()
        {
            Expand();
            if (m_CurrentControl.Visible)
            {
                m_CurrentControl.m_lblFieldName_Click(null, null);
            }
        }

        private void SetMode(Mode mode)
        {
            switch (mode)
            {
                case Mode.LARGE:
                    m_CurrentControl.Visible = false;
                    m_ListView.Visible = true;
                    Height = m_LargeHeight;
                    m_Mode = mode;
                    m_ToolStrip.DisplayImage(ctrlToolStrip.ImageIndex.MINUS);
                    break;
                case Mode.SMALL:
                    m_ListView.Visible = false;
                    m_CurrentControl.Visible = true;
                    Height = m_SmallHeight;
                    m_Mode = mode;
                    m_ToolStrip.DisplayImage(ctrlToolStrip.ImageIndex.PLUS);
                    break;
            }
        }

        public void DisplayField(IFieldDefinition field)
        {
            var node = PropertyEditorUtil.FindXmlNode(field.Name, m_ObjectInstance.Node);

            if (field.Name !=
                m_Field.Name)
            {
                m_Field = field;
                SuspendLayout();
                switch (field.TypeName)
                {
                    case "bit":
                    case "tristate":
                        Controls.Remove(m_CurrentControl);
                        m_CurrentControl.Dispose();
                        if (m_Field.Name == "Mute" ||
                            m_Field.Name == "Solo")
                        {
                            m_CurrentControl = new ctrlCategoryButton(m_Field,
                                                                      node,
                                                                      m_ObjectInstance,
                                                                      this._mHierarchyNode);
                        }
                        else
                        {
                            m_CurrentControl = new ctrlCheckBoxEditor(field, node, m_ObjectInstance);
                        }
                        m_CurrentControl.Top = m_ToolStrip.Bottom;
                        Controls.Add(m_CurrentControl);
                        break;
                    case "s8":
                    case "s16":
                    case "s32":
                    case "u8":
                    case "u16":
                    case "u32":
                        Controls.Remove(m_CurrentControl);
                        m_CurrentControl.Dispose();
                        var slider = new ctrlSliderEditor(field, node, m_ObjectInstance, null);
                        m_CurrentControl = slider;
                        m_CurrentControl.Top = m_ToolStrip.Bottom;
                        Controls.Add(m_CurrentControl);
                        break;
                }

                ResumeLayout();
            }
        }

        internal void UpdateGameValues(Dictionary<string, CategorySettings> m_CategorySettings)
        {
            var slider = m_CurrentControl as ctrlSliderEditor;
            if (slider != null &&
                m_CategorySettings.ContainsKey(m_ObjectInstance.Name))
            {
                var categorySettings = m_CategorySettings[m_ObjectInstance.Name];

                switch (slider.Text)
                {
                    case "Volume":
                        slider.GameValue = categorySettings.Volume.ToString();
                        break;
                    case "DistanceRollOffScale":
                        slider.GameValue = categorySettings.DistanceRollOffScale.ToString();
                        break;
                    case "OcclusionDamping":
                        slider.GameValue = categorySettings.OcclusionDamping.ToString();
                        break;
                    case "EnvironmentalFilterDamping":
                        slider.GameValue = categorySettings.EnvironmentalFilterDamping.ToString();
                        break;
                    case "EnvironmentalReverbDamping":
                        slider.GameValue = categorySettings.EnvironmentalReverbDamping.ToString();
                        break;
                    case "EnvironmentalLoudness":
                        slider.GameValue = categorySettings.EnvironmentalLoudness.ToString();
                        break;
                    case "Pitch":
                        slider.GameValue = (categorySettings.Pitch / 100).ToString();
                        break;
                    case "FilterCutoff":
                        slider.GameValue = categorySettings.FilterCutoff.ToString();
                        break;
                }
            }
        }

        internal void HandleKeyInput(Keys keys)
        {
            switch (keys)
            {
                case Keys.Return:
                    if (m_CurrentControl.Visible)
                    {
                        m_CurrentControl.m_lblFieldName_DoubleClick(null, null);
                    }
                    break;
                case Keys.Control | Keys.O:
                    m_CurrentControl.SelectOverrideParent();
                    break;
            }
        }

        #region Nested type: Mode

        private enum Mode
        {
            LARGE,
            SMALL
        } ;

        #endregion
    }
}