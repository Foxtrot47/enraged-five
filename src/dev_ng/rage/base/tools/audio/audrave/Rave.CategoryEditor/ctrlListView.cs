namespace Rave.CategoryEditor
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    using Rave.PropertiesEditor;
    using Rave.PropertiesEditor.Infrastructure.Structs;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public partial class ctrlListView : ListView
    {
        private IObjectInstance m_ObjectInstance;

        public ctrlListView()
        {
            InitializeComponent();

            DoubleBuffered = true;

            View = View.Details;
            Columns.Add("Name", 153);
            Columns.Add("Value", 45);
            Columns.Add("Units", 25);
            Columns.Add("Override", 23);
            Columns.Add("GameValue", 60);
            HeaderStyle = ColumnHeaderStyle.None;
            OwnerDraw = true;
            DrawSubItem += DrawSubImage;
            BorderStyle = BorderStyle.None;
            Scrollable = false;
        }

        protected override void WndProc(ref Message m)
        {
            //disable left mouse clicks
            switch (m.Msg)
            {
                case 0x203:
                case 0x201:
                case 0x202:
                case 0xA3:
                case 0xA1:
                case 0xA2:
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        private void DrawSubImage(object sender, DrawListViewSubItemEventArgs e)
        {
            //replace yes no and default with checkbox images
            PointF point;
            switch (e.SubItem.Text)
            {
                case "yes":
                    point = e.SubItem.Bounds.Location;
                    point.X += 2;
                    e.Graphics.DrawImage(m_ImageList.Images[0], point);
                    break;
                case "no":
                    point = e.SubItem.Bounds.Location;
                    point.X += 2;
                    e.Graphics.DrawImage(m_ImageList.Images[1], point);
                    break;
                case "default":
                    point = e.SubItem.Bounds.Location;
                    point.X += 2;
                    e.Graphics.DrawImage(m_ImageList.Images[2], point);
                    break;
                default:
                    e.DrawDefault = true;
                    break;
            }
        }

        public void UpdateValues(CategorySettings categorySettings)
        {
            ListViewItem item = null;

            item = Items.Find("Volume", false)[0];
            item.SubItems[4].Text = categorySettings.Volume.ToString();

            item = Items.Find("DistanceRollOffScale", false)[0];
            item.SubItems[4].Text = categorySettings.DistanceRollOffScale.ToString();

            item = Items.Find("EnvironmentalFilterDamping", false)[0];
            item.SubItems[4].Text = categorySettings.EnvironmentalFilterDamping.ToString();

            item = Items.Find("EnvironmentalLoudness", false)[0];
            item.SubItems[4].Text = categorySettings.EnvironmentalLoudness.ToString();

            item = Items.Find("EnvironmentalReverbDamping", false)[0];
            item.SubItems[4].Text = categorySettings.EnvironmentalReverbDamping.ToString();

            item = Items.Find("FilterCutoff", false)[0];
            item.SubItems[4].Text = categorySettings.FilterCutoff.ToString();

            item = Items.Find("OcclusionDamping", false)[0];
            item.SubItems[4].Text = categorySettings.OcclusionDamping.ToString();

            item = Items.Find("Pitch", false)[0];
            item.SubItems[4].Text = categorySettings.Pitch.ToString();
        }

        public void Populate(IObjectInstance objectInstance)
        {
            //build list view skipping comments
            m_ObjectInstance = objectInstance;
            var typeDef = objectInstance.TypeDefinitions.FindTypeDefinition(objectInstance.TypeName);
            if (typeDef == null)
            {
                return;
            }

            foreach (var f in typeDef.Fields)
            {
                if (f.TypeName != "string" &&
                    f.Name != "CategoryRef")
                {
                    Items.Add(new ListViewItem());
                    var temp = new ListViewItem(f.Name);
                    temp.Name = f.Name;
                    switch (f.TypeName)
                    {
                        case "bit":
                            temp.SubItems.Add(SetState(f));
                            //blank units
                            temp.SubItems.Add("");
                            break;
                        case "tristate":
                            temp.SubItems.Add(SetTristate(f));
                            //blank units
                            temp.SubItems.Add("");
                            break;
                        case "s8":
                        case "s16":
                        case "s32":
                        case "u8":
                        case "u16":
                        case "u32":
                            var field = PropertyEditorUtil.FindXmlNode(f.Name, m_ObjectInstance.Node);
                            var value = 0;
                            if (field != null)
                            {
                                value = Int32.Parse(field.InnerText);
                            }
                            else
                            {
                                value = Int32.Parse(f.DefaultValue);
                            }
                            string units, displayValue;
                            PropertyEditorUtil.ConvertToDisplayValue(value, f.Units, out displayValue, out units);
                            temp.SubItems.Add(displayValue);
                            temp.SubItems.Add(units);
                            break;
                    }

                    if (f.AllowOverrideControl)
                    {
                        var field = PropertyEditorUtil.FindXmlNode(f.Name, m_ObjectInstance.Node);
                        if (field == null)
                        {
                            temp.SubItems.Add("no");
                        }
                        else if (field.Attributes["overrideParent"] == null)
                        {
                            temp.SubItems.Add("no");
                        }
                        else
                        {
                            temp.SubItems.Add(field.Attributes["overrideParent"].Value);
                        }
                    }
                    temp.SubItems.Add(new ListViewItem.ListViewSubItem());
                    Items.Add(temp);
                }
            }
        }

        private string SetTristate(IFieldDefinition f)
        {
            var field = PropertyEditorUtil.FindXmlNode(f.Name, m_ObjectInstance.Node);
            if (field == null)
            {
                return "default";
            }
            else if (field.InnerText == "yes")
            {
                return "yes";
            }
            else
            {
                return "no";
            }
        }

        private string SetState(IFieldDefinition f)
        {
            var field = PropertyEditorUtil.FindXmlNode(f.Name, m_ObjectInstance.Node);
            if (field == null)
            {
                if (f.DefaultValue == "yes")
                {
                    return "yes";
                }
                else
                {
                    return "no";
                }
            }
            else
            {
                if (field.InnerText == "yes")
                {
                    return "yes";
                }
                else
                {
                    return "no";
                }
            }
        }
    }
}