namespace Rave.CategoryEditor
{
    partial class ctrlMain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_ToolStrip = new System.Windows.Forms.ToolStrip();
            this.m_LblName = new System.Windows.Forms.ToolStripLabel();
            this.m_ToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_ToolStrip
            // 
            this.m_ToolStrip.BackColor = System.Drawing.Color.SkyBlue;
            this.m_ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.m_ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_LblName});
            this.m_ToolStrip.Location = new System.Drawing.Point(0, 0);
            this.m_ToolStrip.Name = "m_ToolStrip";
            this.m_ToolStrip.Size = new System.Drawing.Size(278, 25);
            this.m_ToolStrip.TabIndex = 0;
            this.m_ToolStrip.Text = "toolStrip1";
            // 
            // m_LblName
            // 
            this.m_LblName.BackColor = System.Drawing.Color.Transparent;
            this.m_LblName.ForeColor = System.Drawing.Color.White;
            this.m_LblName.Name = "m_LblName";
            this.m_LblName.Size = new System.Drawing.Size(78, 22);
            this.m_LblName.Text = "toolStripLabel1";
            // 
            // ctrlMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.m_ToolStrip);
            this.MinimumSize = new System.Drawing.Size(280, 425);
            this.Name = "ctrlMain";
            this.Size = new System.Drawing.Size(278, 425);
            this.m_ToolStrip.ResumeLayout(false);
            this.m_ToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

       #endregion

        private System.Windows.Forms.ToolStrip m_ToolStrip;
        private System.Windows.Forms.ToolStripLabel m_LblName;

    }
}
