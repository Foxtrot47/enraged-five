namespace Rave.CategoryEditor.Editors
{
    partial class ctrlCategoryButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctrlCategoryButton));
            this.m_ImageList = new System.Windows.Forms.ImageList(this.components);
            this.btnCategoryButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_ImageList
            // 
            this.m_ImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("m_ImageList.ImageStream")));
            this.m_ImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.m_ImageList.Images.SetKeyName(0, "mute2.ico");
            this.m_ImageList.Images.SetKeyName(1, "mute.ico");
            this.m_ImageList.Images.SetKeyName(2, "solo2.ico");
            this.m_ImageList.Images.SetKeyName(3, "solo.ico");
            // 
            // btnCategoryButton
            // 
            this.btnCategoryButton.ImageList = this.m_ImageList;
            this.btnCategoryButton.Location = new System.Drawing.Point(0, 0);
            this.btnCategoryButton.Name = "btnCategoryButton";
            this.btnCategoryButton.Size = new System.Drawing.Size(70, 28);
            this.btnCategoryButton.TabIndex = 0;
            this.btnCategoryButton.UseVisualStyleBackColor = true;
            this.btnCategoryButton.Click += new System.EventHandler(this.btn_OnClick);
            // 
            // ctrlCategoryButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.btnCategoryButton);
            this.Name = "ctrlCategoryButton";
            this.Controls.SetChildIndex(this.btnCategoryButton, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList m_ImageList;
        private System.Windows.Forms.Button btnCategoryButton;
    }
}
