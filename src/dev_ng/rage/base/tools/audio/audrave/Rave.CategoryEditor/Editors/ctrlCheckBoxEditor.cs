using Rave.PropertiesEditor.CustomNodes;

namespace Rave.CategoryEditor.Editors
{
    using System;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.PropertiesEditor;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public partial class ctrlCheckBoxEditor : ctrlCategoryFieldEditor
    {
        private readonly IFieldDefinition m_Field;
        private readonly IObjectInstance m_ObjectInstance;
        private XmlNode m_Node;

        public ctrlCheckBoxEditor(IFieldDefinition field, XmlNode node, IObjectInstance objectInstance)
            : base(field, node, objectInstance)
        {
            this.m_Field = field;
            this.m_Node = node;
            this.m_ObjectInstance = objectInstance;
            this.InitializeComponent();
            this.m_CheckBox.Click += this.m_lblFieldName_Click;
            this.Init();
        }

        public event Action<ctrlCategoryFieldEditor> OnDisplay;

        private void Init()
        {
            if (this.m_Field.TypeName == "tristate")
            {
                this.m_CheckBox.ThreeState = CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue) == CheckState.Indeterminate;

                if (this.m_Node == null)
                {
                    this.m_CheckBox.CheckState = CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue);
                }
                else
                {
                    this.m_CheckBox.CheckState = this.m_Node.InnerText == "yes" ? CheckState.Checked : CheckState.Unchecked;
                }
            }
            else
            {
                if (this.m_Node == null)
                {
                    this.m_CheckBox.CheckState = this.m_Field.DefaultValue == "yes" ? CheckState.Checked : CheckState.Unchecked;
                }
                else
                {
                    this.m_CheckBox.CheckState = this.m_Node.InnerText == "yes" ? CheckState.Checked : CheckState.Unchecked;
                }
            }
        }

        public override void m_lblFieldName_Click(object sender, EventArgs e)
        {
            this.SetActive(true);
            this.m_CheckBox.Focus();
            if (this.OnDisplay != null)
            {
                this.OnDisplay(this);
            }
        }

        public override void m_lblFieldName_DoubleClick(object sender, EventArgs e)
        {
            switch (this.m_CheckBox.CheckState)
            {
                case CheckState.Unchecked:
                    this.m_CheckBox.CheckState = CheckState.Checked;
                    break;
                case CheckState.Checked:
                    if (this.m_CheckBox.ThreeState)
                    {
                        this.m_CheckBox.CheckState = CheckBoxHelper.GetIndeterminateValue(m_Field.DefaultValue);
                    }
                    else
                    {
                        this.m_CheckBox.CheckState = CheckState.Unchecked;
                    }
                    break;
                case CheckState.Indeterminate:
                    {
                        this.m_CheckBox.CheckState = CheckState.Unchecked;
                    }
                    break;
            }
        }

        private void m_CheckBox_CheckedStateChanged(object sender, EventArgs e)
        {
            var dirty = false;
            string val;
            this.m_Node = PropertyEditorUtil.FindXmlNode(this.m_Field.Name, this.m_ObjectInstance.Node);

            switch (this.m_CheckBox.CheckState)
            {
                case CheckState.Unchecked:
                case CheckState.Checked:
                    if (this.m_Node == null)
                    {
                        this.m_Node = this.m_ObjectInstance.Node.OwnerDocument.CreateElement(this.m_Field.Name);
                        this.m_ObjectInstance.Node.AppendChild(this.m_Node);
                        dirty = true;
                    }
                    val = (this.m_CheckBox.Checked ? "yes" : "no");
                    if (this.m_Node.InnerText != val)
                    {
                        this.m_Node.InnerText = val;
                        dirty = true;
                    }

                    break;
                case CheckState.Indeterminate:
                    if (this.m_Node != null)
                    {
                        this.m_ObjectInstance.Node.RemoveChild(this.m_Node);
                        this.m_Node = null;
                        dirty = true;
                    }
                    break;
            }

            if (dirty)
            {
                this.m_ObjectInstance.MarkAsDirty();
            }
        }
    }
}