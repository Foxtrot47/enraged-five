using System;
using System.Drawing;
using System.Windows.Forms;

namespace Rave.CategoryEditor
{
    public partial class ctrlToolStrip : ToolStrip
    {
        #region Alignment enum

        public enum Alignment
        {
            HORIZONTAL,
            VERTICAL
        } ;

        #endregion

        #region ImageIndex enum

        public enum ImageIndex
        {
            PLUS,
            MINUS
        } ;

        #endregion

        private readonly ToolStripButton btnExpand;
        private readonly ToolStripLabel lblName;

        public ctrlToolStrip()
        {
            InitializeComponent();
            DoubleBuffered = true;
            BackColor = Color.DodgerBlue;
            GripStyle = ToolStripGripStyle.Hidden;

            btnExpand = new ToolStripButton(m_ImageList.Images[0]);
            btnExpand.Click += btnExpand_Click;
            lblName = new ToolStripLabel("Node Name");
            lblName.ForeColor = Color.White;
            Items.AddRange(new ToolStripItem[] {btnExpand, new ToolStripSeparator(), lblName});
        }

        public override string Text
        {
            get { return lblName.Text; }
            set { lblName.Text = value; }
        }

        public event Action OnExpand;

        private void btnExpand_Click(object sender, EventArgs e)
        {
            if (OnExpand != null)
            {
                OnExpand();
            }
        }

        public void SetOreintation(Alignment orientation)
        {
            if (orientation == Alignment.VERTICAL)
            {
                Dock = DockStyle.Right;
                lblName.TextDirection = ToolStripTextDirection.Vertical90;
            }
            else
            {
                Dock = DockStyle.Top;
                lblName.TextDirection = ToolStripTextDirection.Horizontal;
            }
        }

        public void DisplayImage(ImageIndex index)
        {
            btnExpand.Image = m_ImageList.Images[(int) index];
        }
    }
}