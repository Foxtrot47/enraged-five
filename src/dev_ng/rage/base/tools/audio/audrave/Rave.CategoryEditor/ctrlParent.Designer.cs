namespace Rave.CategoryEditor
{
    partial class ctrlParent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_ListView = new Rave.CategoryEditor.ctrlListView();
            this.m_ToolStrip = new Rave.CategoryEditor.ctrlToolStrip();
            this.SuspendLayout();
            // 
            // m_ListView
            // 
            this.m_ListView.BackColor = System.Drawing.Color.White;
            this.m_ListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_ListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.m_ListView.Location = new System.Drawing.Point(0, 25);
            this.m_ListView.Margin = new System.Windows.Forms.Padding(0);
            this.m_ListView.MultiSelect = false;
            this.m_ListView.Name = "m_ListView";
            this.m_ListView.OwnerDraw = true;
            this.m_ListView.Scrollable = false;
            this.m_ListView.Size = new System.Drawing.Size(280, 400);
            this.m_ListView.TabIndex = 0;
            this.m_ListView.TabStop = false;
            this.m_ListView.UseCompatibleStateImageBehavior = false;
            this.m_ListView.View = System.Windows.Forms.View.Details;
            // 
            // m_ToolStrip
            // 
            this.m_ToolStrip.BackColor = System.Drawing.Color.DodgerBlue;
            this.m_ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.m_ToolStrip.Location = new System.Drawing.Point(0, 0);
            this.m_ToolStrip.Name = "m_ToolStrip";
            this.m_ToolStrip.Size = new System.Drawing.Size(312, 25);
            this.m_ToolStrip.TabIndex = 7;
            this.m_ToolStrip.Text = "ctrlToolStrip1";
            // 
            // ctrlParent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.m_ListView);
            this.Controls.Add(this.m_ToolStrip);
            this.MinimumSize = new System.Drawing.Size(314, 30);
            this.Name = "ctrlParent";
            this.Size = new System.Drawing.Size(312, 425);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ctrlToolStrip m_ToolStrip;
        private ctrlListView m_ListView;

    }
}
