namespace Rave.CategoryEditor
{
    public interface ISelectable
    {
        void Select();

        void Deselect();
    }
}