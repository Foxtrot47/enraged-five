namespace Rave.CategoryEditor.Editors
{
    partial class ctrlSliderEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_TxtBox = new System.Windows.Forms.TextBox();
            this.m_LblUnits = new System.Windows.Forms.Label();
            this.m_TrackBar = new System.Windows.Forms.TrackBar();
            this.m_GameValue = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.m_TrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // m_TxtBox
            // 
            this.m_TxtBox.Location = new System.Drawing.Point(155, 4);
            this.m_TxtBox.Name = "m_TxtBox";
            this.m_TxtBox.Size = new System.Drawing.Size(40, 20);
            this.m_TxtBox.TabIndex = 2;
            this.m_TxtBox.TextChanged += new System.EventHandler(this.m_TxtBox_TextChanged);
            // 
            // m_LblUnits
            // 
            this.m_LblUnits.AutoEllipsis = true;
            this.m_LblUnits.Location = new System.Drawing.Point(194, 7);
            this.m_LblUnits.Name = "m_LblUnits";
            this.m_LblUnits.Size = new System.Drawing.Size(56, 13);
            this.m_LblUnits.TabIndex = 3;
            this.m_LblUnits.Text = "label1";
            // 
            // m_TrackBar
            // 
            this.m_TrackBar.Location = new System.Drawing.Point(-38, -7);
            this.m_TrackBar.Name = "m_TrackBar";
            this.m_TrackBar.Size = new System.Drawing.Size(296, 45);
            this.m_TrackBar.TabIndex = 4;
            this.m_TrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.m_TrackBar.Visible = false;
            this.m_TrackBar.ValueChanged += new System.EventHandler(this.m_TrackBar_ValueChanged);
            // 
            // m_GameValue
            // 
            this.m_GameValue.Location = new System.Drawing.Point(271, 3);
            this.m_GameValue.Name = "m_GameValue";
            this.m_GameValue.Size = new System.Drawing.Size(40, 20);
            this.m_GameValue.TabIndex = 5;
            this.m_GameValue.ReadOnly = true;
            // 
            // ctrlSliderEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoSize = true;
            this.Controls.Add(this.m_GameValue);
            this.Controls.Add(this.m_TxtBox);
            this.Controls.Add(this.m_LblUnits);
            this.Controls.Add(this.m_TrackBar);
            this.MaximumSize = new System.Drawing.Size(0, 28);
            this.MinimumSize = new System.Drawing.Size(265, 28);
            this.Name = "ctrlSliderEditor";
            this.Size = new System.Drawing.Size(314, 28);
            this.Controls.SetChildIndex(this.m_TrackBar, 0);
            this.Controls.SetChildIndex(this.m_LblUnits, 0);
            this.Controls.SetChildIndex(this.m_TxtBox, 0);
            this.Controls.SetChildIndex(this.m_GameValue, 0);
            ((System.ComponentModel.ISupportInitialize)(this.m_TrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox m_TxtBox;
        private System.Windows.Forms.Label m_LblUnits;
        private System.Windows.Forms.TrackBar m_TrackBar;
        private System.Windows.Forms.TextBox m_GameValue;
    }
}
