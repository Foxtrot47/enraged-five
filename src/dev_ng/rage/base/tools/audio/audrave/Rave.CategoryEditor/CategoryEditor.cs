namespace Rave.CategoryEditor
{
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Instance;
    using Rave.Plugins.Infrastructure.Interfaces;

    public class CategoryEditor : IRAVEEditorPlugin
    {
        private string m_Name;

        #region IRAVEEditorPlugin Members

        public void EditNodes(TreeView treeView)
        {
            var editor = new frmCategoryEditor();
            editor.Init(treeView, RaveInstance.RemoteControl);
            editor.ShowDialog();
        }

        #endregion

        #region IRAVEPlugin Members

        public string GetName()
        {
            return m_Name;
        }

        public bool Init(XmlNode settings)
        {
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        m_Name = settings.InnerText;
                        break;
                }
            }

            if (m_Name == "")
            {
                return false;
            }
            return true;
        }

        #endregion

        public void Shutdown()
        {
            return;
        }
    }
}