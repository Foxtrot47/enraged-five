namespace Rave.CategoryEditor
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using Rave.PropertiesEditor.Infrastructure.Structs;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public partial class ctrlSibling : UserControl,
                                       IExpandable,
                                       ISelectable
    {
        private readonly IObjectInstance m_ObjectInstance;
        public int largeWidth;
        private Mode m_Mode;
        public int smallWidth;

        public ctrlSibling(IObjectInstance objectInstance)
        {
            InitializeComponent();
            DoubleBuffered = true;
            m_ToolStrip.OnExpand += m_ToolStrip_OnExpand;
            m_ToolStrip.MouseEnter += Selected;
            m_ListView.MouseEnter += Selected;
            Text = objectInstance.Name;
            largeWidth = Width;
            smallWidth = m_ToolStrip.Width + 2;
            SetMode(Mode.SMALL);
            m_ListView.Populate(objectInstance);
            m_ObjectInstance = objectInstance;
        }

        public override string Text
        {
            set { m_ToolStrip.Text = value; }
        }

        #region IExpandable Members

        public void Expand()
        {
            if (m_Mode == Mode.LARGE)
            {
                SetMode(Mode.SMALL);
            }
            else
            {
                SetMode(Mode.LARGE);
            }
        }

        #endregion

        #region ISelectable Members

        void ISelectable.Select()
        {
            Focus();
        }

        void ISelectable.Deselect()
        {
            return;
        }

        #endregion

        public event Action<Control> OnClickEvent;

        public void Selected(object sender, EventArgs e)
        {
            if (OnClickEvent != null)
            {
                OnClickEvent(this);
            }
        }

        private void m_ToolStrip_OnExpand()
        {
            Expand();
            if (OnClickEvent != null)
            {
                OnClickEvent(this);
            }
        }

        private void SetMode(Mode mode)
        {
            switch (mode)
            {
                case Mode.SMALL:
                    Width = smallWidth;
                    m_Mode = Mode.SMALL;
                    m_ToolStrip.SetOreintation(ctrlToolStrip.Alignment.VERTICAL);
                    m_ToolStrip.DisplayImage(ctrlToolStrip.ImageIndex.PLUS);
                    break;
                case Mode.LARGE:
                    Width = largeWidth;
                    m_Mode = Mode.LARGE;
                    m_ToolStrip.SetOreintation(ctrlToolStrip.Alignment.HORIZONTAL);
                    m_ToolStrip.DisplayImage(ctrlToolStrip.ImageIndex.MINUS);
                    break;
            }
        }

        public void UpdateGameValues(Dictionary<string, CategorySettings> m_CategorySettings)
        {
            if (m_CategorySettings.ContainsKey(m_ObjectInstance.Name))
            {
                var categorySettings = m_CategorySettings[m_ObjectInstance.Name];
                m_ListView.UpdateValues(categorySettings);
            }
        }

        #region Nested type: Mode

        private enum Mode
        {
            LARGE,
            SMALL
        } ;

        #endregion
    }
}