namespace Rave.CategoryEditor.Editors
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.PropertiesEditor;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public abstract partial class ctrlCategoryFieldEditor : UserControl
    {
        private readonly IFieldDefinition m_Field;
        private readonly IObjectInstance m_ObjectInstance;
        private XmlNode m_Node;

        public ctrlCategoryFieldEditor()
        {
            this.InitializeComponent();
        }

        public ctrlCategoryFieldEditor(IFieldDefinition field, XmlNode node, IObjectInstance objectInstance)
        {
            this.InitializeComponent();
            this.m_Field = field;
            this.m_Node = node;
            this.m_ObjectInstance = objectInstance;
            this.m_ChkOverrideParent.Click += this.m_lblFieldName_Click;
            this.Init();
        }

        public override string Text
        {
            get { return this.m_lblFieldName.Text; }
        }

        public CheckBox Override
        {
            get { return this.m_ChkOverrideParent; }
        }

        public IFieldDefinition Field
        {
            get { return this.m_Field; }
        }

        private void Init()
        {
            //set up label and checkBox
            this.m_lblFieldName.Text = this.m_Field.Name;

            if (this.m_Field.AllowOverrideControl)
            {
                if (this.m_Node == null)
                {
                    this.m_ChkOverrideParent.CheckState = CheckState.Unchecked;
                }
                else if (this.m_Node.Attributes["overrideParent"] == null)
                {
                    this.m_ChkOverrideParent.CheckState = CheckState.Unchecked;
                }
                else
                {
                    this.m_ChkOverrideParent.CheckState = this.m_Node.Attributes["overrideParent"].Value == "yes"
                                                         ? CheckState.Checked
                                                         : CheckState.Unchecked;
                }
            }
            else
            {
                this.m_ChkOverrideParent.Visible = false;
            }
        }

        public abstract void m_lblFieldName_Click(object sender, EventArgs e);

        public abstract void m_lblFieldName_DoubleClick(object sender, EventArgs e);

        public void SetActive(bool isActive)
        {
            if (this.Enabled)
            {
                if (isActive)
                {
                    this.m_lblFieldName.ForeColor = Color.White;
                    this.m_lblFieldName.BackColor = Color.Blue;
                    this.Focus();
                }
                else
                {
                    this.m_lblFieldName.ForeColor = Color.Black;
                    this.m_lblFieldName.BackColor = Color.White;
                }
            }
        }

        public void SelectOverrideParent()
        {
            if (this.m_ChkOverrideParent.Visible)
            {
                if (this.m_ChkOverrideParent.CheckState ==
                    CheckState.Checked)
                {
                    this.m_ChkOverrideParent.CheckState = CheckState.Unchecked;
                }
                else
                {
                    this.m_ChkOverrideParent.CheckState = CheckState.Checked;
                }
            }
        }

        private void CheckedChanged(object sender, EventArgs e)
        {
            this.m_Node = PropertyEditorUtil.FindXmlNode(this.m_Field.Name, this.m_ObjectInstance.Node);
            if (this.m_Node == null)
            {
                this.m_Node = this.m_ObjectInstance.Node.OwnerDocument.CreateElement(this.m_Field.Name);
                // since the sound builder doesn't support elements with attributes
                // but no value, we'll pull out the default value and store that
                this.m_Node.InnerText = this.m_Field.DefaultValue;
                this.m_ObjectInstance.Node.AppendChild(this.m_Node);
            }

            var overrideParent = this.m_Node.Attributes["overrideParent"];
            if (overrideParent == null)
            {
                this.m_Node.Attributes.Append(this.m_Node.OwnerDocument.CreateAttribute("overrideParent"));
            }

            var val = (this.m_ChkOverrideParent.Checked ? "yes" : "no");
            if (this.m_Node.Attributes["overrideParent"].Value != val)
            {
                this.m_Node.Attributes["overrideParent"].Value = val;
                this.m_ObjectInstance.MarkAsDirty();
            }
        }
    }
}