namespace Rave.CategoryEditor.Editors
{
    using System.Windows.Forms;

    abstract partial class ctrlCategoryFieldEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_lblFieldName = new System.Windows.Forms.Label();
            this.m_ChkOverrideParent = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // m_lblFieldName
            // 
            this.m_lblFieldName.AutoSize = true;
            this.m_lblFieldName.Location = new System.Drawing.Point(0, 7);
            this.m_lblFieldName.Name = "m_lblFieldName";
            this.m_lblFieldName.Size = new System.Drawing.Size(0, 13);
            this.m_lblFieldName.TabIndex = 0;
            this.m_lblFieldName.DoubleClick += new System.EventHandler(this.m_lblFieldName_DoubleClick);
            this.m_lblFieldName.Click += new System.EventHandler(this.m_lblFieldName_Click);
            // 
            // m_ChkOverrideParent
            // 
            this.m_ChkOverrideParent.AutoSize = true;
            this.m_ChkOverrideParent.Location = new System.Drawing.Point(252, 7);
            this.m_ChkOverrideParent.Name = "m_ChkOverrideParent";
            this.m_ChkOverrideParent.Size = new System.Drawing.Size(15, 14);
            this.m_ChkOverrideParent.TabIndex = 1;
            this.m_ChkOverrideParent.UseVisualStyleBackColor = true;
            this.m_ChkOverrideParent.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            // 
            // ctrlCategoryFieldEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.m_ChkOverrideParent);
            this.Controls.Add(this.m_lblFieldName);
            this.MinimumSize = new System.Drawing.Size(314, 0);
            this.Name = "ctrlCategoryFieldEditor";
            this.Size = new System.Drawing.Size(314, 28);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_lblFieldName;
        private CheckBox m_ChkOverrideParent;
    }
}
