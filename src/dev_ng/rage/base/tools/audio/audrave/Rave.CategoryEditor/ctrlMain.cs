using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Rave.CategoryEditor
{
    using Rave.CategoryEditor.Editors;
    using Rave.HierarchyBrowser.Infrastructure.Nodes;
    using Rave.PropertiesEditor;
    using Rave.PropertiesEditor.Infrastructure.Structs;
    using Rave.RemoteControl.Infrastructure.Interfaces;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public partial class ctrlMain : UserControl,
                                    ISelectable
    {
        private readonly List<ctrlCategoryFieldEditor> m_Controls;
        private readonly IObjectInstance m_ObjectInstance;
        private readonly IRemoteControl m_RemoteControl;
        private readonly ObjectNode mHierarchyNode;
        private ctrlCategoryFieldEditor m_ActiveControl;

        public ctrlMain(IObjectInstance objectInstance, IRemoteControl remoteControl, ObjectNode shn)
        {
            InitializeComponent();
            DoubleBuffered = true;

            this.mHierarchyNode = shn;
            m_ObjectInstance = objectInstance;
            m_LblName.Text = objectInstance.Name;

            m_ToolStrip.MouseEnter += Control_Enter;
            m_Controls = new List<ctrlCategoryFieldEditor>();

            m_RemoteControl = remoteControl;
            MouseEnter += Control_Enter;
            Init();
            SetActiveControl(m_Controls[0]);
        }

        #region ISelectable Members

        void ISelectable.Select()
        {
            m_ActiveControl.SetActive(true);
        }

        void ISelectable.Deselect()
        {
            m_ActiveControl.SetActive(false);
        }

        #endregion

        public event Action<IFieldDefinition> OnControlDisplay;
        public event Action<Control> OnClickEvent;

        public void UpdateGameValues(Dictionary<string, CategorySettings> m_CategorySettings)
        {
            if (m_CategorySettings.ContainsKey(m_ObjectInstance.Name))
            {
                var categorySettings = m_CategorySettings[m_ObjectInstance.Name];
                foreach (var control in m_Controls)
                {
                    var slider = control as ctrlSliderEditor;
                    if (slider != null)
                    {
                        switch (slider.Text)
                        {
                            case "Volume":
                                slider.GameValue = categorySettings.Volume.ToString();
                                break;
                            case "DistanceRollOffScale":
                                slider.GameValue = categorySettings.DistanceRollOffScale.ToString();
                                break;
                            case "OcclusionDamping":
                                slider.GameValue = categorySettings.OcclusionDamping.ToString();
                                break;
                            case "EnvironmentalFilterDamping":
                                slider.GameValue = categorySettings.EnvironmentalFilterDamping.ToString();
                                break;
                            case "EnvironmentalReverbDamping":
                                slider.GameValue = categorySettings.EnvironmentalReverbDamping.ToString();
                                break;
                            case "EnvironmentalLoudness":
                                slider.GameValue = categorySettings.EnvironmentalLoudness.ToString();
                                break;
                            case "Pitch":
                                slider.GameValue = (categorySettings.Pitch / 100).ToString();
                                break;
                            case "FilterCutoff":
                                slider.GameValue = categorySettings.FilterCutoff.ToString();
                                break;
                        }
                    }
                }
            }
        }

        private void Control_Enter(object sender, EventArgs e)
        {
            if (OnClickEvent != null)
            {
                OnClickEvent(this);
            }
        }

        private void Init()
        {
            var typeDef = m_ObjectInstance.TypeDefinitions.FindTypeDefinition(m_ObjectInstance.TypeName);
            if (typeDef == null)
            {
                return;
            }

            foreach (var f in typeDef.Fields)
            {
                var fieldNode = PropertyEditorUtil.FindXmlNode(f.Name, m_ObjectInstance.Node);
                switch (f.TypeName)
                {
                    case "bit":
                    case "tristate":
                        if (f.Name == "Mute" ||
                            f.Name == "Solo")
                        {
                            var button = new ctrlCategoryButton(f, fieldNode, m_ObjectInstance, this.mHierarchyNode);
                            button.MouseEnter += Control_Enter;
                            button.OnDisplay += Control_OnDisplay;
                            m_Controls.Add(button);
                        }
                        else
                        {
                            var checkBox = new ctrlCheckBoxEditor(f, fieldNode, m_ObjectInstance);
                            checkBox.MouseEnter += Control_Enter;
                            checkBox.OnDisplay += Control_OnDisplay;
                            m_Controls.Add(checkBox);
                        }
                        break;
                    case "s8":
                    case "s16":
                    case "s32":
                    case "u8":
                    case "u16":
                    case "u32":
                        var slider = new ctrlSliderEditor(f, fieldNode, m_ObjectInstance, m_RemoteControl);
                        slider.MouseEnter += Control_Enter;
                        slider.OnDisplay += Control_OnDisplay;
                        m_Controls.Add(slider);
                        break;
                }
            }

            for (var i = 0; i < m_Controls.Count; i++)
            {
                if (i == 0)
                {
                    m_Controls[i].Top = m_ToolStrip.Bottom + 3;
                }
                else
                {
                    m_Controls[i].Top = m_Controls[i - 1].Bottom;
                }

                Controls.Add(m_Controls[i]);
            }
        }

        private void Control_OnDisplay(ctrlCategoryFieldEditor ctrl)
        {
            SetActiveControl(ctrl);
            if (OnControlDisplay != null)
            {
                OnControlDisplay(ctrl.Field);
            }
        }

        public void HandleKeyInput(Keys keys)
        {
            int index;
            switch (keys)
            {
                case Keys.Return:
                    m_ActiveControl.m_lblFieldName_DoubleClick(null, null);
                    break;
                case Keys.Tab:
                    index = (m_Controls.IndexOf(m_ActiveControl) + 1) % m_Controls.Count;
                    SetActiveControl(m_Controls[index]);
                    break;
                case Keys.Tab | Keys.Shift:
                    index = (m_Controls.IndexOf(m_ActiveControl) + m_Controls.Count - 1) % m_Controls.Count;
                    SetActiveControl(m_Controls[index]);
                    break;
                case Keys.Control | Keys.O:
                    m_ActiveControl.SelectOverrideParent();
                    break;
            }
        }

        private void SetActiveControl(ctrlCategoryFieldEditor control)
        {
            if (m_ActiveControl != control)
            {
                if (m_ActiveControl != null)
                {
                    m_ActiveControl.SetActive(false);
                }
                m_ActiveControl = control;
                m_ActiveControl.m_lblFieldName_Click(null, null);
            }
        }
    }
}