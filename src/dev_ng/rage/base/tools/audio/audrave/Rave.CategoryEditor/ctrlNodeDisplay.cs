// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ctrlNodeDisplay.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The ctrl node display.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.CategoryEditor
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;
    
    using Rave.HierarchyBrowser.Infrastructure.Nodes;
    using Rave.PropertiesEditor.Infrastructure.Structs;
    using Rave.RemoteControl.Infrastructure.Interfaces;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;

    /// <summary>
    /// The ctrl node display.
    /// </summary>
    public partial class ctrlNodeDisplay : TableLayoutPanel
    {
        #region Fields

        /// <summary>
        /// The m_ active control.
        /// </summary>
        private Control m_ActiveControl;

        /// <summary>
        /// The m_ category settings.
        /// </summary>
        private Dictionary<string, CategorySettings> m_CategorySettings;

        /// <summary>
        /// The m_ cell pos.
        /// </summary>
        private TableLayoutPanelCellPosition m_CellPos;

        /// <summary>
        /// The m_ current node.
        /// </summary>
        private ctrlMain m_CurrentNode;

        /// <summary>
        /// The m_ remote control.
        /// </summary>
        private IRemoteControl m_RemoteControl;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ctrlNodeDisplay"/> class.
        /// </summary>
        public ctrlNodeDisplay()
        {
            this.InitializeComponent();
            this.DoubleBuffered = true;
            this.AutoSize = true;
            this.RowStyles.Clear();
            this.ColumnStyles.Clear();
            this.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            this.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The display node.
        /// </summary>
        /// <param name="shn">
        /// The shn.
        /// </param>
        public void DisplayNode(ObjectNode shn)
        {
            this.Hide();
            this.SuspendLayout();
            this.Controls.Clear();

            // create list of parent and sibling object in order to build table
            var parents = new List<ObjectNode>();
            var previousNodes = new List<ObjectNode>();
            var nextNodes = new List<ObjectNode>();

            // get parents
            var parent = shn.Parent as ObjectNode;
            while (parent != null)
            {
                parents.Insert(0, parent);
                parent = parent.Parent as ObjectNode;
            }

            // get previous siblings
            var previous = shn.PrevNode as ObjectNode;
            while (previous != null)
            {
                previousNodes.Add(previous);
                previous = previous.PrevNode as ObjectNode;
            }

            // get next siblings
            var next = shn.NextNode as ObjectNode;
            while (next != null)
            {
                nextNodes.Add(next);
                next = next.NextNode as ObjectNode;
            }

            // row count is number of parents plus current node
            this.RowCount = parents.Count + 1;

            // column count is number of siblings plus current node
            this.ColumnCount = nextNodes.Count + previousNodes.Count + 1;

            // keep track of current row
            var row = 0;

            // fill in parent cells
            for (; row < parents.Count; row++)
            {
                var objectNode = parents[row] as ObjectNode;
                if (null != objectNode)
                {
                    var parentNode = new ctrlParent(objectNode.ObjectInstance, parents[row]);
                    parentNode.OnClickEvent += this.Node_OnClick;
                    this.Controls.Add(parentNode, previousNodes.Count, row);
                }
            }

            // keep track of current column
            var column = 0;

            // fill in previous node cells
            for (; column < previousNodes.Count; column++)
            {
                var objectNode = previousNodes[column] as ObjectNode;
                if (null != objectNode)
                {
                    var previousNode = new ctrlSibling(objectNode.ObjectInstance);
                    previousNode.OnClickEvent += this.Node_OnClick;
                    this.Controls.Add(previousNode, column, row);
                }
            }

            // add current node
            this.m_CurrentNode = new ctrlMain(shn.ObjectInstance, this.m_RemoteControl, shn);
            this.m_CurrentNode.OnControlDisplay += this.m_CurrentNode_OnControlDisplay;
            this.m_CurrentNode.OnClickEvent += this.Node_OnClick;
            this.Dock = DockStyle.Fill;
            this.Controls.Add(this.m_CurrentNode, column, row);
            this.SetActiveControl(this.m_CurrentNode);

            // fill in next node cells
            for (var i = 0; i < nextNodes.Count; i++)
            {
                var nextNode = new ctrlSibling(nextNodes[i].ObjectInstance);
                nextNode.OnClickEvent += this.Node_OnClick;
                this.Controls.Add(nextNode, column + i + 1, row);
            }

            this.ResumeLayout();
            this.Show();
            this.m_CurrentNode.Focus();
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="remoteControl">
        /// The remote control.
        /// </param>
        public void Init(IRemoteControl remoteControl)
        {
            this.m_RemoteControl = remoteControl;
            this.m_CategorySettings = new Dictionary<string, CategorySettings>();
            this.m_Timer.Interval = 100;
            this.m_Timer.Tick += this.m_Timer_Tick;
            this.m_Timer.Start();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The handle key input.
        /// </summary>
        /// <param name="keys">
        /// The keys.
        /// </param>
        internal void HandleKeyInput(Keys keys)
        {
            Control ctrl = null;
            switch (keys)
            {
                case Keys.Tab:
                case Keys.Tab | Keys.Shift:

                    // cycles through controls in the current node panel
                    this.SetActiveControl(this.m_CurrentNode);
                    this.m_CurrentNode.HandleKeyInput(keys);
                    break;
                case Keys.Space:
                    if (this.m_ActiveControl != this.m_CurrentNode)
                    {
                        var exp = this.GetControlFromPosition(this.m_CellPos.Column, this.m_CellPos.Row) as IExpandable;
                        if (exp != null)
                        {
                            exp.Expand();
                        }
                    }

                    break;
                case Keys.Return:
                    if (this.m_ActiveControl == this.m_CurrentNode)
                    {
                        this.m_CurrentNode.HandleKeyInput(keys);
                    }
                    else
                    {
                        var parent = this.m_ActiveControl as ctrlParent;
                        if (parent != null)
                        {
                            parent.HandleKeyInput(keys);
                        }
                    }

                    break;
                case Keys.Control | Keys.Up:
                    if (this.m_CellPos.Row != 0)
                    {
                        ctrl = this.GetControlFromPosition(this.m_CellPos.Column, this.m_CellPos.Row - 1);
                    }

                    break;
                case Keys.Control | Keys.Down:
                    if (this.m_CellPos.Row != this.RowCount - 1)
                    {
                        ctrl = this.GetControlFromPosition(this.m_CellPos.Column, this.m_CellPos.Row + 1);
                    }

                    break;
                case Keys.Control | Keys.Right:
                    if (this.m_CellPos.Column != this.ColumnCount - 1)
                    {
                        ctrl = this.GetControlFromPosition(this.m_CellPos.Column + 1, this.m_CellPos.Row);
                    }

                    break;
                case Keys.Control | Keys.Left:
                    if (this.m_CellPos.Column != 0)
                    {
                        ctrl = this.GetControlFromPosition(this.m_CellPos.Column - 1, this.m_CellPos.Row);
                    }

                    break;
                case Keys.Control | Keys.O:
                    if (this.m_ActiveControl == this.m_CurrentNode)
                    {
                        this.m_CurrentNode.HandleKeyInput(keys);
                    }
                    else
                    {
                        var parent = this.m_ActiveControl as ctrlParent;
                        if (parent != null)
                        {
                            parent.HandleKeyInput(keys);
                        }
                    }

                    break;
            }

            if (ctrl != null)
            {
                this.SetActiveControl(ctrl);
            }
        }

        /// <summary>
        /// The node_ on click.
        /// </summary>
        /// <param name="ctrl">
        /// The ctrl.
        /// </param>
        private void Node_OnClick(Control ctrl)
        {
            this.SetActiveControl(ctrl);
        }

        /// <summary>
        /// The on paint.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void OnPaint(object sender, PaintEventArgs e)
        {
            // draw border around active cell
            if (this.m_ActiveControl != null)
            {
                var g = e.Graphics;
                var rec = this.m_ActiveControl.Bounds;
                rec.Inflate(2, 2);
                g.FillRectangle(Brushes.Blue, rec);
            }
        }

        /// <summary>
        /// The set active control.
        /// </summary>
        /// <param name="ctrl">
        /// The ctrl.
        /// </param>
        private void SetActiveControl(Control ctrl)
        {
            if (this.m_ActiveControl != ctrl)
            {
                // deselect old active control
                var selectable = this.m_ActiveControl as ISelectable;
                if (selectable != null)
                {
                    selectable.Deselect();
                }

                this.m_ActiveControl = ctrl;

                // select new sctive control
                selectable = this.m_ActiveControl as ISelectable;
                if (selectable != null)
                {
                    selectable.Select();
                }

                this.m_CellPos = this.GetPositionFromControl(ctrl);
                ctrl.Focus();
                this.Refresh();
            }
        }

        /// <summary>
        /// The m_ current node_ on control display.
        /// </summary>
        /// <param name="field">
        /// The field.
        /// </param>
        private void m_CurrentNode_OnControlDisplay(IFieldDefinition field)
        {
            foreach (Control c in this.Controls)
            {
                if (c as ctrlParent != null)
                {
                    ((ctrlParent)c).DisplayField(field);
                }
            }
        }

        /// <summary>
        /// The m_ timer_ tick.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void m_Timer_Tick(object sender, EventArgs e)
        {
            // TODO: fix this
            // if (m_RemoteControl.IsConnected())
            // {
            // //Update the parents and current node every 100ms
            // m_RemoteControl.GetCategorySettings(m_CategorySettings);
            // foreach (Control ctrl in Controls)
            // {
            // ctrlParent parent = ctrl as ctrlParent;
            // ctrlMain main = ctrl as ctrlMain;
            // ctrlSibling sib = ctrl as ctrlSibling;

            // if (parent != null)
            // {
            // parent.UpdateGameValues(m_CategorySettings);
            // }
            // if (main != null)
            // {
            // main.UpdateGameValues(m_CategorySettings);
            // }
            // if (sib != null)
            // {
            // sib.UpdateGameValues(m_CategorySettings);
            // }
            // }
            // }
        }

        #endregion
    }
}