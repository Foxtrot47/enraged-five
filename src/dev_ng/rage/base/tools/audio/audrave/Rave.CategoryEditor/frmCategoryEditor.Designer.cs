namespace Rave.CategoryEditor
{
    partial class frmCategoryEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_ToolStrip = new System.Windows.Forms.ToolStrip();
            this.lblFind = new System.Windows.Forms.ToolStripLabel();
            this.txtFind = new System.Windows.Forms.ToolStripTextBox();
            this.lblNotFound = new System.Windows.Forms.ToolStripLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.m_TreeView = new System.Windows.Forms.TreeView();
            this.m_ImageList = new System.Windows.Forms.ImageList(this.components);
            this.m_NodeDisplay = new Rave.CategoryEditor.ctrlNodeDisplay();
            this.m_ToolStrip.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_ToolStrip
            // 
            this.m_ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.m_ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblFind,
            this.txtFind,
            this.lblNotFound});
            this.m_ToolStrip.Location = new System.Drawing.Point(0, 0);
            this.m_ToolStrip.Name = "m_ToolStrip";
            this.m_ToolStrip.Size = new System.Drawing.Size(816, 25);
            this.m_ToolStrip.TabIndex = 2;
            this.m_ToolStrip.Text = "toolStrip1";
            // 
            // lblFind
            // 
            this.lblFind.Name = "lblFind";
            this.lblFind.Size = new System.Drawing.Size(31, 22);
            this.lblFind.Text = "Find:";
            // 
            // txtFind
            // 
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(100, 25);
            this.txtFind.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFind_OnKeyPress);
            this.txtFind.TextChanged += new System.EventHandler(this.txtFind_OnTextChanged);
            // 
            // lblNotFound
            // 
            this.lblNotFound.ForeColor = System.Drawing.Color.Red;
            this.lblNotFound.Name = "lblNotFound";
            this.lblNotFound.Size = new System.Drawing.Size(61, 22);
            this.lblNotFound.Text = "Not Found!";
            this.lblNotFound.Visible = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.m_TreeView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.m_NodeDisplay);
            this.splitContainer1.Size = new System.Drawing.Size(816, 607);
            this.splitContainer1.SplitterDistance = 171;
            this.splitContainer1.TabIndex = 3;
            this.splitContainer1.TabStop = false;
            // 
            // m_TreeView
            // 
            this.m_TreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_TreeView.HideSelection = false;
            this.m_TreeView.ImageIndex = 0;
            this.m_TreeView.ImageList = this.m_ImageList;
            this.m_TreeView.Location = new System.Drawing.Point(0, 0);
            this.m_TreeView.Name = "m_TreeView";
            this.m_TreeView.SelectedImageIndex = 0;
            this.m_TreeView.Size = new System.Drawing.Size(171, 607);
            this.m_TreeView.TabIndex = 0;
            this.m_TreeView.TabStop = false;
            this.m_TreeView.DoubleClick += new System.EventHandler(this.Double_Click);
            this.m_TreeView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnTreeViewKeyPress);
            // 
            // m_ImageList
            // 
            this.m_ImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.m_ImageList.ImageSize = new System.Drawing.Size(16, 16);
            this.m_ImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // m_NodeDisplay
            // 
            this.m_NodeDisplay.AutoScroll = true;
            this.m_NodeDisplay.AutoSize = true;
            this.m_NodeDisplay.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_NodeDisplay.ColumnCount = 3;
            this.m_NodeDisplay.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.m_NodeDisplay.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.m_NodeDisplay.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.m_NodeDisplay.Location = new System.Drawing.Point(3, 3);
            this.m_NodeDisplay.Name = "m_NodeDisplay";
            this.m_NodeDisplay.RowCount = 3;
            this.m_NodeDisplay.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.m_NodeDisplay.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.m_NodeDisplay.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.m_NodeDisplay.Size = new System.Drawing.Size(0, 0);
            this.m_NodeDisplay.TabIndex = 0;
            // 
            // frmCategoryEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 632);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.m_ToolStrip);
            this.KeyPreview = true;
            this.Name = "frmCategoryEditor";
            this.Text = "Category Editor";
            this.m_ToolStrip.ResumeLayout(false);
            this.m_ToolStrip.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ctrlNodeDisplay m_NodeDisplay;
        private System.Windows.Forms.ToolStrip m_ToolStrip;
        private System.Windows.Forms.ToolStripLabel lblFind;
        private System.Windows.Forms.ToolStripTextBox txtFind;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripLabel lblNotFound;
        private System.Windows.Forms.TreeView m_TreeView;
        private System.Windows.Forms.ImageList m_ImageList;

    }
}

