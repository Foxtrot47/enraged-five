namespace Rave.CategoryEditor.Editors
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.Xml;

    using Rave.PropertiesEditor;
    using Rave.RemoteControl.Infrastructure.Interfaces;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public partial class ctrlSliderEditor : ctrlCategoryFieldEditor
    {
        private const int NAME_MAX = 155;
        private const int MARGIN = 2;
        private readonly ISimpleFieldDefinition m_Field;
        private readonly int m_GameValueLeft;
        private readonly int m_LblUnitsLeft;
        private readonly IObjectInstance m_ObjectInstance;
        private readonly int m_OverrideLeft;
        private readonly XmlNode m_ParentNode;
        private readonly int m_TextBoxLeft;
        private bool bShowTrackBar;
        private XmlNode m_Node;
        private IRemoteControl m_RemoteControl;

        public ctrlSliderEditor(IFieldDefinition field,
                                XmlNode node,
                                IObjectInstance objectInstance,
                                IRemoteControl remoteControl) : base(field, node, objectInstance)
        {
            this.InitializeComponent();
            this.DoubleBuffered = true;

            this.m_Field = field as ISimpleFieldDefinition;
            this.m_Node = node;
            this.m_ParentNode = objectInstance.Node;
            this.m_ObjectInstance = objectInstance;
            this.m_TextBoxLeft = this.m_TxtBox.Left;
            this.m_TxtBox.Click += this.m_TxtBox_Click;
            this.m_LblUnitsLeft = this.m_LblUnits.Left;
            this.m_GameValueLeft = this.m_GameValue.Left;
            this.m_OverrideLeft = this.Override.Left;
            this.m_RemoteControl = remoteControl;

            //Slider
            this.SetupTrackBar();

            //TextBox
            string display, units;
            PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Value, this.m_Field.Units, out display, out units);
            this.m_TxtBox.Text = display;
            this.m_LblUnits.Text = units;
        }

        public string GameValue
        {
            set { this.m_GameValue.Text = value; }
        }

        public int Value
        {
            get { return this.m_TrackBar.Value; }
            set { this.m_TrackBar.Value = value; }
        }

        public event Action<ctrlCategoryFieldEditor> OnDisplay;

        private void m_TxtBox_Click(object sender, EventArgs e)
        {
            this.m_TxtBox.Focus();
        }

        private void SetupTrackBar()
        {
            //set up min/max
            if (this.m_Field.Min !=
                this.m_Field.Max)
            {
                this.m_TrackBar.Minimum = (int) this.m_Field.Min;
                this.m_TrackBar.Maximum = (int) this.m_Field.Max;
            }
            else if (this.m_Field.Units == "ms")
            {
                this.m_TrackBar.Minimum = 0;
                this.m_TrackBar.Maximum = 65535;
            }
            else
            {
                switch (this.m_Field.TypeName)
                {
                    case "s8":
                        this.m_TrackBar.Minimum = SByte.MinValue;
                        this.m_TrackBar.Maximum = SByte.MaxValue;
                        break;
                    case "u8":
                        this.m_TrackBar.Minimum = Byte.MinValue;
                        this.m_TrackBar.Maximum = Byte.MaxValue;
                        break;
                    case "s16":
                        this.m_TrackBar.Minimum = Int16.MinValue;
                        this.m_TrackBar.Maximum = Int16.MaxValue;
                        break;
                    case "u16":
                        this.m_TrackBar.Minimum = UInt16.MinValue;
                        this.m_TrackBar.Maximum = UInt16.MaxValue;
                        break;
                    case "s32":
                    case "u32":
                    default:
                        this.m_TrackBar.Minimum = Int32.MinValue;
                        this.m_TrackBar.Maximum = Int32.MaxValue;
                        break;
                }
            }
            this.m_TrackBar.TickFrequency = 0; // m_TrackBar.Maximum / 2;
            this.m_TrackBar.SmallChange = 50;
            this.m_TrackBar.LargeChange = 200;

            //Value
            if (this.m_Node != null)
            {
                this.m_TrackBar.Value = Int32.Parse(this.m_Node.InnerText);
            }
            else
            {
                this.m_TrackBar.Value = Int32.Parse(this.m_Field.DefaultValue);
            }
        }

        public void ShowGameValue(bool show)
        {
            if (show)
            {
                this.m_GameValue.Visible = true;
            }
            else
            {
                this.m_GameValue.Visible = false;
            }
        }

        private void UpdateXml()
        {
            if (this.Enabled)
            {
                this.m_Node = PropertyEditorUtil.FindXmlNode(this.m_Field.Name, this.m_ParentNode);
                var dirty = false;
                if (this.m_Node == null)
                {
                    if (this.m_Node == null)
                    {
                        this.m_Node = this.m_ParentNode.OwnerDocument.CreateElement(this.m_Field.Name);
                        this.m_ParentNode.AppendChild(this.m_Node);
                        dirty = true;
                    }
                }

                var val = this.m_TrackBar.Value.ToString();
                if (this.m_Node.InnerText != val)
                {
                    this.m_Node.InnerText = val;
                    dirty = true;
                }
                if (dirty)
                {
                    this.m_ObjectInstance.MarkAsDirty();
                }

                if (this.OnDisplay != null)
                {
                    this.OnDisplay(this);
                }
            }
        }

        public void Expand()
        {
            this.SuspendLayout();
            this.bShowTrackBar = !this.bShowTrackBar;

            if (this.bShowTrackBar)
            {
                this.m_TrackBar.Left = NAME_MAX;
                this.m_TrackBar.Visible = true;
                this.m_TrackBar.BackColor = this.BackColor;
                this.m_TxtBox.Left = this.m_TrackBar.Right + MARGIN;
                this.m_LblUnits.Left = this.m_TxtBox.Right + MARGIN;
                this.Override.Left = this.m_LblUnits.Right + MARGIN;
                this.m_GameValue.Left = this.Override.Right + MARGIN;
                this.m_TrackBar.Focus();
            }
            else
            {
                this.m_TrackBar.Visible = false;
                this.m_TxtBox.Left = this.m_TextBoxLeft;
                this.m_LblUnits.Left = this.m_LblUnitsLeft;
                this.Override.Left = this.m_OverrideLeft;
                this.m_GameValue.Left = this.m_GameValueLeft;
                this.m_TxtBox.Focus();
            }

            this.ResumeLayout();
        }

        private void m_TxtBox_TextChanged(object sender, EventArgs e)
        {
            var val = 0;
            if (this.m_TxtBox.Text != "")
            {
                try
                {
                    val = PropertyEditorUtil.ConvertFromDisplayValue(this.m_TxtBox.Text, this.m_Field.Units);
                    this.m_TxtBox.ForeColor = Color.Black;
                    this.m_TrackBar.Value = val;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                    this.m_TxtBox.ForeColor = Color.Red;
                }

                this.UpdateXml();
            }
        }

        public override void m_lblFieldName_Click(object sender, EventArgs e)
        {
            this.SetActive(true);
            if (this.bShowTrackBar)
            {
                this.m_TrackBar.Focus();
            }
            else
            {
                this.m_TxtBox.Focus();
            }
            if (this.OnDisplay != null)
            {
                this.OnDisplay(this);
            }
        }

        public override void m_lblFieldName_DoubleClick(object sender, EventArgs e)
        {
            this.m_lblFieldName_Click(null, null);
            this.Expand();
        }

        private void m_TrackBar_ValueChanged(object sender, EventArgs e)
        {
            string units, displayValue;
            PropertyEditorUtil.ConvertToDisplayValue(this.m_TrackBar.Value, this.m_Field.Units, out displayValue, out units);
            if (this.m_TxtBox.Text != "")
            {
                if (PropertyEditorUtil.ConvertFromDisplayValue(this.m_TxtBox.Text, this.m_Field.Units) ==
                    float.Parse(displayValue))
                {
                    this.m_TxtBox.Text = displayValue;
                }
            }
            this.UpdateXml();
        }
    }
}