namespace Rave.CategoryEditor
{
    partial class ctrlSibling
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctrlSibling));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_ListView = new Rave.CategoryEditor.ctrlListView();
            this.m_ToolStrip = new Rave.CategoryEditor.ctrlToolStrip();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "chk1.bmp");
            this.imageList1.Images.SetKeyName(1, "chk3.bmp");
            this.imageList1.Images.SetKeyName(2, "chk2.bmp");
            this.imageList1.Images.SetKeyName(3, "plus.bmp");
            this.imageList1.Images.SetKeyName(4, "minus.bmp");
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.m_ListView);
            this.panel1.Controls.Add(this.m_ToolStrip);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(312, 423);
            this.panel1.TabIndex = 5;
            // 
            // m_ListView
            // 
            this.m_ListView.BackColor = System.Drawing.Color.White;
            this.m_ListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_ListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.m_ListView.Location = new System.Drawing.Point(0, 0);
            this.m_ListView.MultiSelect = false;
            this.m_ListView.Name = "m_ListView";
            this.m_ListView.OwnerDraw = true;
            this.m_ListView.Scrollable = false;
            this.m_ListView.Size = new System.Drawing.Size(288, 423);
            this.m_ListView.TabIndex = 3;
            this.m_ListView.TabStop = false;
            this.m_ListView.UseCompatibleStateImageBehavior = false;
            this.m_ListView.View = System.Windows.Forms.View.Details;
            // 
            // m_ToolStrip
            // 
            this.m_ToolStrip.BackColor = System.Drawing.Color.DodgerBlue;
            this.m_ToolStrip.Dock = System.Windows.Forms.DockStyle.Right;
            this.m_ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.m_ToolStrip.Location = new System.Drawing.Point(288, 0);
            this.m_ToolStrip.Name = "m_ToolStrip";
            this.m_ToolStrip.Size = new System.Drawing.Size(24, 423);
            this.m_ToolStrip.TabIndex = 4;
            this.m_ToolStrip.Text = "ctrlToolBar1";
            this.m_ToolStrip.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
            // 
            // ctrlSibling
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.MaximumSize = new System.Drawing.Size(314, 425);
            this.MinimumSize = new System.Drawing.Size(20, 425);
            this.Name = "ctrlSibling";
            this.Size = new System.Drawing.Size(314, 423);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private ctrlListView m_ListView;
        private ctrlToolStrip m_ToolStrip;
        private System.Windows.Forms.Panel panel1;

    }
}
