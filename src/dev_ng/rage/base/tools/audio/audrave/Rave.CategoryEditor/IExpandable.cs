namespace Rave.CategoryEditor
{
    public interface IExpandable
    {
        void Expand();
    }
}