namespace Rave.CategoryEditor.Editors
{
    using System;
    using System.Drawing;
    using System.Xml;

    using Rave.HierarchyBrowser.Infrastructure.Nodes;
    using Rave.PropertiesEditor;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public partial class ctrlCategoryButton : ctrlCategoryFieldEditor
    {
        private readonly int baseImageIndex;
        private readonly IFieldDefinition m_Field;
        private readonly IObjectInstance m_ObjectInstance;
        private readonly HierarchyNode mHierarchyNode;
        private bool bIsSelected;
        private XmlNode m_Node;

        public ctrlCategoryButton(IFieldDefinition field,
                                  XmlNode node,
                                  IObjectInstance objectInstance,
                                  HierarchyNode shn) : base(field, node, objectInstance)
        {
            this.m_Field = field;
            this.m_Node = node;
            this.m_ObjectInstance = objectInstance;
            this.mHierarchyNode = shn;

            this.InitializeComponent();
            switch (field.Name)
            {
                case "Mute":
                    this.baseImageIndex = 0;
                    break;
                case "Solo":
                    this.baseImageIndex = 2;
                    break;
                default:
                    this.baseImageIndex = 0;
                    break;
            }

            this.Init();
        }

        public event Action<ctrlCategoryFieldEditor> OnDisplay;

        private void Init()
        {
            if (this.m_Node == null)
            {
                this.btnCategoryButton.Image = this.m_ImageList.Images[this.baseImageIndex];
            }
            else
            {
                if (this.m_Node.InnerText == "yes")
                {
                    this.btnCategoryButton.Image = this.m_ImageList.Images[this.baseImageIndex + 1];
                    this.bIsSelected = true;
                }
                else
                {
                    this.btnCategoryButton.Image = this.m_ImageList.Images[this.baseImageIndex];
                }
            }
        }

        private void btn_OnClick(object sender, EventArgs e)
        {
            this.bIsSelected = !this.bIsSelected;
            //UpdateImage
            if (this.bIsSelected)
            {
                Color nodeText;
                if (this.m_Field.Name == "Mute")
                {
                    nodeText = Color.Orange;
                }
                else
                {
                    nodeText = Color.Red;
                }
                this.mHierarchyNode.ForeColor = nodeText;
                this.btnCategoryButton.Image = this.m_ImageList.Images[this.baseImageIndex + 1];
            }
            else
            {
                this.btnCategoryButton.Image = this.m_ImageList.Images[this.baseImageIndex];
                this.mHierarchyNode.ForeColor = Color.Black;
            }
            //update xml
            this.BtnStatusChanged();
        }

        private void BtnStatusChanged()
        {
            //Update xml
            string val;
            this.m_Node = PropertyEditorUtil.FindXmlNode(this.m_Field.Name, this.m_ObjectInstance.Node);

            if (this.m_Node == null)
            {
                this.m_Node = this.m_ObjectInstance.Node.OwnerDocument.CreateElement(this.m_Field.Name);
                this.m_ObjectInstance.Node.AppendChild(this.m_Node);
            }

            val = (this.bIsSelected ? "yes" : "no");
            if (this.m_Node.InnerText != val)
            {
                this.m_Node.InnerText = val;
            }

            this.m_ObjectInstance.MarkAsDirty();

            if (this.OnDisplay != null)
            {
                this.OnDisplay(this);
            }
        }

        public override void m_lblFieldName_Click(object sender, EventArgs e)
        {
            this.btnCategoryButton.Focus();
            if (this.OnDisplay != null)
            {
                this.OnDisplay(this);
            }
        }

        public override void m_lblFieldName_DoubleClick(object sender, EventArgs e)
        {
            //do nothing, needed for interface
            return;
        }
    }
}