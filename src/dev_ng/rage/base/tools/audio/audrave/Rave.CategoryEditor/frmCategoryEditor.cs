using System;
using System.Collections;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

namespace Rave.CategoryEditor
{
    using Rave.HierarchyBrowser.Infrastructure.Nodes;
    using Rave.Instance;
    using Rave.RemoteControl.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Utils;
    using System.IO;

    public partial class frmCategoryEditor : Form
    {
        private static Regex sm_Regex = new Regex("[a-zA-Z0-9._]");
        private Hashtable m_TypeIconTable;

        public frmCategoryEditor()
        {
            InitializeComponent();
            //catch keys before they are processed by the controls
            KeyPreview = true;
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();
        }

        public void Init(TreeView treeview, IRemoteControl remoteControl)
        {
            LoadIcons();
            BuildTree(treeview.Nodes[0], null);
            m_NodeDisplay.Init(remoteControl);
        }

        private void LoadIcons()
        {
            m_TypeIconTable = new Hashtable();
            RaveUtils.LoadTypeIcons(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"icons/categorytypes/"),
                                    m_TypeIconTable,
                                    m_ImageList,
                                    RaveInstance.AllTypeDefinitions["CATEGORIES"],
                                    true);
        }

        private void BuildTree(TreeNode node, TreeNode parent)
        {
            // rebuild tree to allow for custom features such as text colour
            TreeNode newNode = null;
            IObjectInstance sound = null;

            if (node as ObjectRefNode != null)
            {
                sound = ((ObjectRefNode) node).ObjectReference.ObjectInstance;
                newNode = new ObjectRefNode(((ObjectRefNode)node).ObjectReference, null);
                parent.Nodes.Add(newNode);
            }
            else if (node as ObjectNode != null)
            {
                sound = ((ObjectNode)node).ObjectInstance;
                newNode = new ObjectNode(sound);
                if (parent == null)
                {
                    m_TreeView.Nodes.Add(newNode);
                }
                else
                {
                    parent.Nodes.Add(newNode);
                }
            }

            if (sound != null)
            {
                foreach (XmlNode setting in sound.Node)
                {
                    if (setting.Name == "Mute" &&
                        setting.InnerText != "yes")
                    {
                        newNode.ForeColor = Color.Orange;
                    }
                    else if (setting.Name == "Solo" &&
                             setting.InnerText != "yes")
                    {
                        newNode.ForeColor = Color.Red;
                    }
                }
                if (m_TypeIconTable.ContainsKey(sound.TypeName))
                {
                    newNode.SelectedImageIndex = newNode.ImageIndex = (int) m_TypeIconTable[sound.TypeName];
                }
                else
                {
                    newNode.SelectedImageIndex = newNode.ImageIndex = 0;
                }

                foreach (TreeNode child in node.Nodes)
                {
                    BuildTree(child, newNode);
                }
            }
        }

        private void Double_Click(object sender, EventArgs e)
        {
            if (m_TreeView.SelectedNode != null)
            {
                m_NodeDisplay.DisplayNode((ObjectNode) m_TreeView.SelectedNode);
            }
        }

        private void txtFind_OnTextChanged(object sender, EventArgs e)
        {
            m_TreeView.SelectedNode = null;

            if (txtFind.Text != "")
            {
                var found = new TreeNode();
                FindNode(txtFind.Text.ToUpper(), m_TreeView.Nodes[0], ref found);
                if (found.Text == "")
                {
                    lblNotFound.Visible = true;
                }
                else
                {
                    lblNotFound.Visible = false;
                    m_TreeView.SelectedNode = found;
                    found.EnsureVisible();
                }
            }
        }

        private void txtFind_OnKeyPress(object sender, KeyPressEventArgs e)
        {
            //Display selected node
            if (e.KeyChar == (Char) Keys.Return &&
                m_TreeView.SelectedNode != null)
            {
                m_NodeDisplay.DisplayNode((ObjectNode) m_TreeView.SelectedNode);
            }
        }

        private static void FindNode(string name, TreeNode tn, ref TreeNode found)
        {
            //exact match found
            if (tn.Text == name)
            {
                found = tn;
                return;
            }
            else
            {
                //find a partial match to allow user to enter the first few leters
                if (tn.Text.StartsWith(name) &&
                    found.Text == "")
                {
                    found = tn;
                }

                if (tn.FirstNode != null)
                {
                    FindNode(name, tn.FirstNode, ref found);
                }
                while (tn.NextNode != null)
                {
                    FindNode(name, tn.NextNode, ref found);
                    tn = tn.NextNode;
                }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            //catch keystrokes here and pass them down to the appropriate controls
            switch (keyData)
            {
                case Keys.Control | Keys.F:
                    txtFind.Text = "";
                    txtFind.Focus();
                    return true;
                case Keys.Control | Keys.T:
                    m_TreeView.Focus();
                    return true;
                case Keys.Return:
                    if (!txtFind.Focused &&
                        !m_TreeView.Focused)
                    {
                        m_NodeDisplay.HandleKeyInput(keyData);
                    }
                    break;
                case Keys.Control | Keys.Up:
                case Keys.Control | Keys.Down:
                case Keys.Control | Keys.Left:
                case Keys.Control | Keys.Right:
                case Keys.Tab:
                case Keys.Tab | Keys.Shift:
                case Keys.Space:
                case Keys.Control | Keys.O:
                    m_NodeDisplay.HandleKeyInput(keyData);
                    return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void OnTreeViewKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar ==
                (Char) Keys.Return)
            {
                m_NodeDisplay.DisplayNode((ObjectNode) m_TreeView.SelectedNode);
            }
        }
    }
}