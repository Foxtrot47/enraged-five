﻿// -----------------------------------------------------------------------
// <copyright file="SpeechWavesNoVoiceTag.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.SpeechWavesNoVoiceTag
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;
    using global::Rave.Plugins.Infrastructure.Interfaces;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    /// Report that generates a list of speech and scripted speech
    /// waves that don't have a voice tag.
    /// </summary>
    public class SpeechWavesNoVoiceTag : IRAVEReportPlugin
    {
        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// The built waves depot path.
        /// </summary>
        private string builtWavesDepotPath;

        /// <summary>
        /// The built waves local path.
        /// </summary>
        private string builtWavesLocalPath;

        /// <summary>
        /// The built waves pack list file name.
        /// </summary>
        private string builtWavesPackListFileName;

        /// <summary>
        /// Built wave packs to include during report generation.
        /// </summary>
        private HashSet<string> builtWavesIncludePacks;

        /// <summary>
        /// The built waves pack file names.
        /// </summary>
        private HashSet<string> builtWavesPackFileNames;

        /// <summary>
        /// The waves without voice tags.
        /// </summary>
        private IDictionary<string, HashSet<string>> wavesWithoutVoiceTags; 

        /// <summary>
        /// The asset manager.
        /// </summary>
        private IAssetManager assetManager;

        /// <summary>
        /// Get the report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            this.builtWavesIncludePacks = new HashSet<string>();

            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;

                    case "BuiltWaves":
                        foreach (XmlNode childNode in setting.ChildNodes)
                        {
                            switch (childNode.Name)
                            {
                                case "DepotPath":
                                    this.builtWavesDepotPath = childNode.InnerText;
                                    break;

                                case "PackListFileName":
                                    this.builtWavesPackListFileName = childNode.InnerText;
                                    break;

                                case "Include":
                                    this.builtWavesIncludePacks.Add(childNode.InnerText.ToLower());
                                    break;
                            }
                        }

                        break;
                }
            }

            if (string.IsNullOrEmpty(this.name))
            {
                throw new Exception("Report name required");
            }

            if (string.IsNullOrEmpty(this.builtWavesDepotPath))
            {
                throw new Exception("Built waves depot path required");
            }

            // Get instance of asset manager
            this.assetManager = AssetManagerFactory.GetInstance(AssetManagerType.Perforce);

            if (null == this.assetManager)
            {
                throw new Exception("Failed to get instance of Perforce asset manager");
            }

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes (not required).
        /// </param>
        /// <param name="browsers">
        /// The sound browsers (not required).
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            // Get latest data
            if (!this.GetLatestData())
            {
                throw new Exception("Failed to get latest data required to generate report");
            }

            this.LoadPackListNames();
            this.LoadData();
            this.DisplayResults();
        }

        /// <summary>
        /// Get latest data from asset manager.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GetLatestData()
        {
            // Get latest built waves data files
            var builtWaveFolder = this.builtWavesDepotPath;
            if (!builtWaveFolder.EndsWith("/"))
            {
                builtWaveFolder += "/";
            }

            builtWaveFolder += "...";

            if (!this.assetManager.GetLatest(builtWaveFolder, false))
            {
                return false;
            }

            this.builtWavesLocalPath = this.assetManager.GetLocalPath(this.builtWavesDepotPath);

            return true;
        }

        /// <summary>
        /// Load the pack list names from the pack list file.
        /// </summary>
        private void LoadPackListNames()
        {
            this.builtWavesPackFileNames = new HashSet<string>();

            // Obtain pack list file path
            var packListPath = Path.Combine(this.builtWavesLocalPath, this.builtWavesPackListFileName);

            if (!File.Exists(packListPath))
            {
                throw new Exception("Failed to find built waves pack list file: " + packListPath);
            }

            // Load pack list file
            var file = XDocument.Load(packListPath);
            foreach (var packFileElem in file.Descendants("PackFile"))
            {
                this.builtWavesPackFileNames.Add(packFileElem.Value);
            }
        }

        /// <summary>
        /// Load the speech data.
        /// </summary>
        private void LoadData()
        {
            this.wavesWithoutVoiceTags = new Dictionary<string, HashSet<string>>();

            foreach (var fileName in this.builtWavesPackFileNames)
            {
                // Only process packs in the include list
                if (!this.builtWavesIncludePacks.Any(fileName.ToLower().StartsWith))
                {
                    continue;
                }

                var filePath = Path.Combine(this.builtWavesLocalPath, fileName);
                var file = XDocument.Load(filePath);

                var results = new HashSet<string>();
                this.wavesWithoutVoiceTags.Add(fileName, results);

                // Locate and store all waves that don't have a voice tag (or an empty voice tag)
                foreach (var waveElem in file.Descendants("Wave"))
                {
                    var voiceTagFound = false;
                    var voiceTagElem = this.GetVoiceTagElement(waveElem);

                    if (null != voiceTagElem)
                    {
                        var valueAttr = voiceTagElem.Attribute("value");
                        if (null != valueAttr && !string.IsNullOrEmpty(valueAttr.Value))
                        {
                            voiceTagFound = true;
                        }
                    }

                    if (!voiceTagFound)
                    {
                        var wavePath = this.GetElementPath(waveElem);
                        results.Add(wavePath);
                    }
                }
            }
        }

        /// <summary>
        /// Get the closest voice tag element in the hierarchy
        /// for the given element. Stop when reached the bank node.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// The voice tag element, if found <see cref="XElement"/>.
        /// </returns>
        private XElement GetVoiceTagElement(XElement element)
        {
            if (null == element)
            {
                return null;
            }

            foreach (var tagElem in element.Elements("Tag"))
            {
                var tagName = tagElem.Attribute("name").Value;

                // Find the voice tag, if it exists
                if (tagName.ToLower() == "voice")
                {
                    return tagElem;
                }
            }

            if (element.Name.LocalName.ToLower() != "bank")
            {
                return this.GetVoiceTagElement(element.Parent);
            }

            return null;
        }

        private string GetElementPath(XElement element)
        {
            if (null == element)
            {
                return string.Empty;
            }

            var nameAttr = element.Attribute("name");
            if (null != nameAttr)
            {
                return Path.Combine(this.GetElementPath(element.Parent), nameAttr.Value);
            }

            return string.Empty;
        }

        /// <summary>
        /// Display the results.
        /// </summary>
        private void DisplayResults()
        {
            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Speech Waves With No Voice Tag</title></head><body>");

            // Output results
            foreach (var list in this.wavesWithoutVoiceTags)
            {
                var pack = list.Key;
                var waves = list.Value;

                sw.WriteLine("<h3>" + pack + "</h3>");

                if (waves.Any())
                {
                    sw.WriteLine("<ul>");
                    foreach (var wave in waves)
                    {
                        sw.WriteLine("<li>" + wave + "</li>");
                    }

                    sw.WriteLine("</ul>");
                }
                else
                {
                    sw.WriteLine("<p>None</p>");
                }
            }

            sw.WriteLine("</body></html>");

            sw.Close();
            filesteam.Close();

            // Display report
            var report = new frmReport(this.name + " Report");
            report.SetURL(resultsFileName);
            report.Show();
        }
    }
}
