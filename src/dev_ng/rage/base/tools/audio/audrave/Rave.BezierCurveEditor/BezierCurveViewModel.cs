﻿#region

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Xml;
using ModelLib;
using ModelLib.Extentions;
using ModelLib.Types;
using Models;
using Rave.CurveEditor2;
using Rave.CurveEditor2.Curves;
using Rave.CurveEditor2.Helpers;
using Rave.Types.Infrastructure.Interfaces.Objects;
using WPFToolLib.Extensions;

#endregion

namespace Rave.BezierCurveEditor
{
    public class BezierCurveViewModel : INotifyPropertyChanged
    {
        private Curve _curve;
        private CurveEditor _curveEditor;
        private IObjectInstance _objectToEdit;
        public Bezier BezierCurve { get; private set; }

        public CurveEditor CurveEditor
        {
            get { return _curveEditor; }
            set
            {
                _curveEditor = value;
                PropertyChanged.Raise(this, "CurveEditor");
            }
        }

        public bool ClampInput
        {
            get { return BezierCurve.ClampInput == TriState.yes; }

            set
            {
                BezierCurve.ClampInput = value ? TriState.yes : TriState.no;
                PropertyChanged.Raise(this, "ClampInput");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void EditObject(IObjectInstance objectInstance)
        {
            if (objectInstance == null)
            {
                return;
            }

            if (_objectToEdit != null && _objectToEdit.Bank != null)
            {
                _objectToEdit.Bank.BankStatusChanged -= StateChanged;
            }

            _objectToEdit = objectInstance;
            if (_objectToEdit == null)
            {
                return;
            }

            if (_objectToEdit.Bank != null)
            {
                _objectToEdit.Bank.BankStatusChanged += StateChanged;
            }

            if (_objectToEdit.Node != null)
            {
                BezierCurve = _objectToEdit.Node.DeserializeToModel<Bezier>();
                BezierCurve.PropertyChanged += BezierCurve_PropertyChanged;
                BezierCurve.OutputRange.PropertyChanged += BezierCurve_PropertyChanged;
                if (BezierCurve.TransformPoints == null || BezierCurve.TransformPoints.Count < 2)
                {
                    BezierCurve.TransformPoints = new ItemsObservableCollection<Bezier.TransformPointsDefinition>
                    {
                        new Bezier.TransformPointsDefinition {x = 0, y = 0},
                        new Bezier.TransformPointsDefinition {x = 1, y = 1}
                    };
                    BezierCurve.BezierPoints = new ItemsObservableCollection<Bezier.BezierPointsDefinition>
                    {
                        new Bezier.BezierPointsDefinition {x = 0.5f, y = 0.5f}
                    };

                    BezierCurve.MinInput = 0f;
                    BezierCurve.MaxInput = 1f;
                    BezierCurve.OutputRange.max = 1f;
                    BezierCurve.OutputRange.min = 0f;
                }

                ShowCurves();
            }
        }

        private void ShowCurves()
        {
            CurveEditor = new CurveEditor(1, 100, BezierCurve.MinInput, BezierCurve.OutputRange.min,
                BezierCurve.MaxInput, BezierCurve.OutputRange.max, true, true);
            _curve = CurveEditor.AddCurve(
                BezierCurve.TransformPoints.Select(
                    p =>
                        new Point(p.x.GetLinearRatio(BezierCurve.MinInput, BezierCurve.MaxInput),
                            p.y.GetLinearRatio(BezierCurve.OutputRange.min, BezierCurve.OutputRange.max))),
                BezierCurve.BezierPoints.Select(
                    p =>
                        new Point(p.x.GetLinearRatio(BezierCurve.MinInput, BezierCurve.MaxInput),
                            p.y.GetLinearRatio(BezierCurve.OutputRange.min, BezierCurve.OutputRange.max))),
                new Tuple<ExtendedLineMode, ExtendedLineMode>(ExtendedLineMode.Horizontal, ExtendedLineMode.Horizontal),
                "Green");
            StateChanged();
        }

        private void BezierCurve_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "OutputRange" ||
                e.PropertyName == "max" ||
                e.PropertyName == "min" ||
                e.PropertyName == "MaxInput" ||
                e.PropertyName == "MinInput" ||
                e.PropertyName == "ClampInput")
            {
                ApplyCurveChanges();
                CurveEditor.ViewModel.XAxis.Min = BezierCurve.MinInput;
                CurveEditor.ViewModel.XAxis.Max = BezierCurve.MaxInput;
                CurveEditor.ViewModel.YAxis.Min = BezierCurve.OutputRange.min;
                CurveEditor.ViewModel.YAxis.Max = BezierCurve.OutputRange.max;
                SaveToObject();
            }

        }

        private void StateChanged()
        {
            CurveEditor.ViewModel.IsCheckedOut = _objectToEdit.Bank.IsCheckedOut;
            if (_objectToEdit.Bank.IsCheckedOut)
            {
                _curve.IsSelected = true;


                _curve.Dirty += () =>
                {
                    ApplyCurveChanges();
                    SaveToObject();
                };
            }
        }

        private void ApplyCurveChanges()
        {
            BezierCurve.TransformPoints = new ItemsObservableCollection<Bezier.TransformPointsDefinition>();

            foreach (
                Bezier.TransformPointsDefinition tp in
                    _curve.ControlPoints.Select(
                        p =>
                            new Bezier.TransformPointsDefinition
                            {
                                x = (float)p.X.Lerp(BezierCurve.MinInput, BezierCurve.MaxInput),
                                y = (float)p.Y.Lerp(BezierCurve.OutputRange.min, BezierCurve.OutputRange.max)
                            }))
            {
                BezierCurve.TransformPoints.Add(tp);
            }

            BezierCurve.BezierPoints =
                new ItemsObservableCollection<Bezier.BezierPointsDefinition>();

            foreach (
                Bezier.BezierPointsDefinition bp in
                    _curve.BezierPoints.Select(
                        p =>
                            new Bezier.BezierPointsDefinition
                            {
                                x = (float)p.X.Lerp(BezierCurve.MinInput, BezierCurve.MaxInput),
                                y = (float)p.Y.Lerp(BezierCurve.OutputRange.min, BezierCurve.OutputRange.max)
                            }))
            {
                BezierCurve.BezierPoints.Add(bp);
            }
        }

        public void SaveToObject()
        {
            if (_objectToEdit.Node != null)
            {
                if (BezierCurve.TransformPoints.Count > 1 && BezierCurve.BezierPoints.Count > 0)
                {
                    ReplaceNodes("BezierPoints");
                    ReplaceNodes("TransformPoints");
                }
                ReplaceNodes("OutputRange");
                ReplaceNodes("ClampInput");
                ReplaceNodes("MinInput");
                ReplaceNodes("MaxInput");
                _objectToEdit.MarkAsDirty();
            }
        }

      

        private void ReplaceNodes(string name)
        {
            XmlNode node = BezierCurve.SerialializeToNode();
            XmlNodeList nodeList = _objectToEdit.Node.SelectNodes(name);

            if (nodeList != null)
                foreach (XmlNode xmlNode in nodeList)
                {
                    _objectToEdit.Node.RemoveChild(xmlNode);
                }
            XmlNodeList otherNodes = node.SelectNodes(name);
            if (otherNodes != null)
                foreach (XmlNode xmlNode in otherNodes)
                {
                    if (_objectToEdit != null && _objectToEdit.Node != null && _objectToEdit.Node.ParentNode != null
                        && _objectToEdit.Node.ParentNode.OwnerDocument != null)
                    {
                        XmlNode importNode = _objectToEdit.Node.ParentNode.OwnerDocument.ImportNode(xmlNode, true);
                        _objectToEdit.Node.AppendChild(importNode);
                    }
                }
        }

    }
}