namespace Rave.WaveBrowser.WaveBrowser.Search
{
    partial class frmSpeechWaveSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSpeechWaveSearch));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_bIncludePlaceholder = new System.Windows.Forms.CheckBox();
            this.characterBox = new System.Windows.Forms.TextBox();
            this.missionBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStringInput = new System.Windows.Forms.TextBox();
            this.m_bSearchUnprocessed = new System.Windows.Forms.CheckBox();
            this.m_bSearchAllLines = new System.Windows.Forms.CheckBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.gbResults = new System.Windows.Forms.GroupBox();
            this.m_Results = new System.Windows.Forms.ListBox();
            this.mnuRightClick = new System.Windows.Forms.ContextMenu();
            this.mnuPlugins = new System.Windows.Forms.MenuItem();
            this.m_bSearchConversationRoots = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.gbResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.m_bSearchConversationRoots);
            this.groupBox1.Controls.Add(this.m_bIncludePlaceholder);
            this.groupBox1.Controls.Add(this.characterBox);
            this.groupBox1.Controls.Add(this.missionBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtStringInput);
            this.groupBox1.Controls.Add(this.m_bSearchUnprocessed);
            this.groupBox1.Controls.Add(this.m_bSearchAllLines);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Location = new System.Drawing.Point(5, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 253);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Input";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // m_bIncludePlaceholder
            // 
            this.m_bIncludePlaceholder.AutoSize = true;
            this.m_bIncludePlaceholder.Location = new System.Drawing.Point(23, 154);
            this.m_bIncludePlaceholder.Name = "m_bIncludePlaceholder";
            this.m_bIncludePlaceholder.Size = new System.Drawing.Size(120, 17);
            this.m_bIncludePlaceholder.TabIndex = 16;
            this.m_bIncludePlaceholder.Text = "Include Placeholder";
            this.m_bIncludePlaceholder.UseVisualStyleBackColor = true;
            // 
            // characterBox
            // 
            this.characterBox.Location = new System.Drawing.Point(100, 82);
            this.characterBox.Name = "characterBox";
            this.characterBox.Size = new System.Drawing.Size(243, 20);
            this.characterBox.TabIndex = 15;
            // 
            // missionBox
            // 
            this.missionBox.Location = new System.Drawing.Point(100, 53);
            this.missionBox.Name = "missionBox";
            this.missionBox.Size = new System.Drawing.Size(243, 20);
            this.missionBox.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Character:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Mission:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Process String:";
            // 
            // txtStringInput
            // 
            this.txtStringInput.Location = new System.Drawing.Point(100, 22);
            this.txtStringInput.Name = "txtStringInput";
            this.txtStringInput.Size = new System.Drawing.Size(243, 20);
            this.txtStringInput.TabIndex = 10;
            // 
            // m_bSearchUnprocessed
            // 
            this.m_bSearchUnprocessed.AutoSize = true;
            this.m_bSearchUnprocessed.Location = new System.Drawing.Point(23, 131);
            this.m_bSearchUnprocessed.Name = "m_bSearchUnprocessed";
            this.m_bSearchUnprocessed.Size = new System.Drawing.Size(154, 17);
            this.m_bSearchUnprocessed.TabIndex = 9;
            this.m_bSearchUnprocessed.Text = "Search Unprocessed Lines";
            this.m_bSearchUnprocessed.UseVisualStyleBackColor = true;
            this.m_bSearchUnprocessed.CheckedChanged += new System.EventHandler(this.SearchFolders_CheckedChanged);
            // 
            // m_bSearchAllLines
            // 
            this.m_bSearchAllLines.AutoSize = true;
            this.m_bSearchAllLines.Location = new System.Drawing.Point(23, 108);
            this.m_bSearchAllLines.Name = "m_bSearchAllLines";
            this.m_bSearchAllLines.Size = new System.Drawing.Size(102, 17);
            this.m_bSearchAllLines.TabIndex = 8;
            this.m_bSearchAllLines.Text = "Search All Lines";
            this.m_bSearchAllLines.UseVisualStyleBackColor = true;
            this.m_bSearchAllLines.CheckedChanged += new System.EventHandler(this.SearchWaveNames_CheckedChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(268, 215);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // gbResults
            // 
            this.gbResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbResults.Controls.Add(this.m_Results);
            this.gbResults.Location = new System.Drawing.Point(5, 261);
            this.gbResults.Name = "gbResults";
            this.gbResults.Size = new System.Drawing.Size(368, 348);
            this.gbResults.TabIndex = 1;
            this.gbResults.TabStop = false;
            this.gbResults.Text = "Results";
            // 
            // m_Results
            // 
            this.m_Results.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_Results.FormattingEnabled = true;
            this.m_Results.Location = new System.Drawing.Point(8, 19);
            this.m_Results.Name = "m_Results";
            this.m_Results.Size = new System.Drawing.Size(354, 290);
            this.m_Results.TabIndex = 0;
            this.m_Results.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_Results_KeyDown);
            // 
            // mnuRightClick
            // 
            this.mnuRightClick.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuPlugins});
            // 
            // mnuPlugins
            // 
            this.mnuPlugins.Index = 0;
            this.mnuPlugins.Text = "Plugins...";
            // 
            // m_bSearchConversationRoots
            // 
            this.m_bSearchConversationRoots.AutoSize = true;
            this.m_bSearchConversationRoots.Location = new System.Drawing.Point(23, 177);
            this.m_bSearchConversationRoots.Name = "m_bSearchConversationRoots";
            this.m_bSearchConversationRoots.Size = new System.Drawing.Size(156, 17);
            this.m_bSearchConversationRoots.TabIndex = 17;
            this.m_bSearchConversationRoots.Text = "Search Conversation Roots";
            this.m_bSearchConversationRoots.UseVisualStyleBackColor = true;
            // 
            // frmSpeechWaveSearch
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 621);
            this.ContextMenu = this.mnuRightClick;
            this.Controls.Add(this.gbResults);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmSpeechWaveSearch";
            this.Text = "RAVE Wave Search";
            this.TopMost = true;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbResults.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbResults;
        private System.Windows.Forms.ListBox m_Results;
        private System.Windows.Forms.ContextMenu mnuRightClick;
        private System.Windows.Forms.MenuItem mnuPlugins;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.CheckBox m_bSearchUnprocessed;
        private System.Windows.Forms.CheckBox m_bSearchAllLines;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStringInput;
        private System.Windows.Forms.TextBox characterBox;
        private System.Windows.Forms.TextBox missionBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox m_bIncludePlaceholder;
        private System.Windows.Forms.CheckBox m_bSearchConversationRoots;
    }
}