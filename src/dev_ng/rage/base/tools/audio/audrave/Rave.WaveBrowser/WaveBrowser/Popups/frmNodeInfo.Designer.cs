﻿namespace Rave.WaveBrowser.WaveBrowser.Popups
{
    partial class frmNodeInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNameValue = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.grpBoxBankInfo = new System.Windows.Forms.GroupBox();
            this.textBoxBankInfo = new System.Windows.Forms.TextBox();
            this.grpBoxBankInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNameValue
            // 
            this.lblNameValue.Location = new System.Drawing.Point(72, 9);
            this.lblNameValue.Name = "lblNameValue";
            this.lblNameValue.Size = new System.Drawing.Size(309, 16);
            this.lblNameValue.TabIndex = 3;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(12, 9);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(54, 16);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name:";
            // 
            // grpBoxBankInfo
            // 
            this.grpBoxBankInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpBoxBankInfo.AutoSize = true;
            this.grpBoxBankInfo.Controls.Add(this.textBoxBankInfo);
            this.grpBoxBankInfo.Location = new System.Drawing.Point(12, 28);
            this.grpBoxBankInfo.Name = "grpBoxBankInfo";
            this.grpBoxBankInfo.Size = new System.Drawing.Size(369, 193);
            this.grpBoxBankInfo.TabIndex = 5;
            this.grpBoxBankInfo.TabStop = false;
            // 
            // textBoxBankInfo
            // 
            this.textBoxBankInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxBankInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxBankInfo.Location = new System.Drawing.Point(3, 16);
            this.textBoxBankInfo.Multiline = true;
            this.textBoxBankInfo.Name = "textBoxBankInfo";
            this.textBoxBankInfo.ReadOnly = true;
            this.textBoxBankInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxBankInfo.Size = new System.Drawing.Size(366, 168);
            this.textBoxBankInfo.TabIndex = 1;
            // 
            // frmNodeInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 233);
            this.Controls.Add(this.grpBoxBankInfo);
            this.Controls.Add(this.lblNameValue);
            this.Controls.Add(this.lblName);
            this.Name = "frmNodeInfo";
            this.Text = "Node Properties";
            this.grpBoxBankInfo.ResumeLayout(false);
            this.grpBoxBankInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNameValue;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.GroupBox grpBoxBankInfo;
        private System.Windows.Forms.TextBox textBoxBankInfo;


    }
}