namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Summary description for DeleteWaveAction.
    /// </summary>
    public class DeleteWaveAction : UserAction
    {
        private readonly IWaveBrowser m_waveBrowser;
        private readonly EditorTreeNode m_node;
        private readonly string m_pack;
        private readonly string m_bank;
        private readonly string m_waveName;
        private readonly string m_assetPath;
        private readonly string m_filePath;
        private readonly string m_objectName;
        private readonly BankNode m_parent;
        private readonly string m_parentPath;
        private readonly List<string> m_platforms;
        private readonly bool m_skipReferenceCheck;
        private readonly Dictionary<string, EditorTreeNode.NodeDisplayState> m_previousStates;
        private bool m_deletedOnAllPlatforms;

        public DeleteWaveAction(IActionParams parameters, bool skipReferenceCheck = false)
        {
            this.ActionParameters = parameters;
            this.m_waveBrowser = parameters.WaveBrowser;
            this.m_node = parameters.Node;
            this.m_parent = (BankNode) this.ActionParameters.Node.Parent;
            this.m_objectName = this.ActionParameters.Node.GetObjectName();
            this.m_parentPath = this.m_parent.GetObjectPath();
            this.m_previousStates = new Dictionary<string, EditorTreeNode.NodeDisplayState>();
            this.m_platforms = new List<string>(parameters.Platforms);
            this.m_skipReferenceCheck = skipReferenceCheck;

            //asset path
            var sb = new StringBuilder();
            sb.Append(this.m_parentPath);
            sb.Append("\\");
            sb.Append(this.m_objectName);
            this.m_assetPath = sb.ToString();

            //folder path
            sb = new StringBuilder();
            sb.Append(Configuration.PlatformWavePath);
            sb.Append(this.m_assetPath);
            this.m_filePath = sb.ToString();

            // Initialise paths
            this.m_pack = EditorTreeNode.FindParentPackNode(this.m_node).GetObjectName();
            this.m_bank = EditorTreeNode.FindParentBankNode(this.m_node).GetObjectName();
            this.m_waveName = Path.GetFileNameWithoutExtension(this.m_node.GetObjectName());
        }

        public override string GetAssetPath()
        {
            return this.m_assetPath;
        }

        protected override bool doAction()
        {
            if (this.ActionParameters.ActionLog != null &&
                this.ActionParameters.ActionLog.SearchForAction(this.GetAssetPath()) != null)
            {
                ErrorManager.HandleInfo("There are pending actions on " + this.GetAssetPath() +
                                        " - you must commit your changes before you delete this wave.");
                return false;
            }

            if (this.m_parent.TryToLock())
            {
                foreach (var platform in this.m_platforms)
                {
                    this.m_previousStates.Add(platform, this.m_node.GetNodeDisplayState(platform));
                    this.m_node.RemovePlatform(platform);
                }

                var editorTreeView = this.m_waveBrowser.GetEditorTreeView();
                var treeView = this.m_waveBrowser.GetEditorTreeView().GetTreeView();
                this.m_deletedOnAllPlatforms = false;

                if (this.m_node.GetPlatforms().Count == 0)
                {
                    if (treeView.InvokeRequired)
                    {
                        treeView.Invoke(
                            (MethodInvoker)
                            (() => editorTreeView.DeleteWaveNode(this.m_parent.FullPath, this.m_waveName, this.m_skipReferenceCheck)));
                    }
                    else
                    {
                        if (!editorTreeView.DeleteWaveNode(this.m_parent.FullPath, this.m_waveName, this.m_skipReferenceCheck))
                        {
                            return false;
                        }
                    }

                    this.m_deletedOnAllPlatforms = true;
                }

                treeView.BeginUpdate();
                this.m_node.UpdateDisplay();
                treeView.EndUpdate();

                return true;
            }

            return false;
        }

        protected override bool doCommit()
        {
            if (this.m_deletedOnAllPlatforms)
            {
                try
                {
                    if (RaveInstance.RaveAssetManager.WaveChangeList == null)
                    {
                        RaveInstance.RaveAssetManager.WaveChangeList =
                            RaveInstance.AssetManager.CreateChangeList("[Rave] Wave/Midi Changelist");
                    }
                    RaveInstance.RaveAssetManager.WaveChangeList.MarkAssetForDelete(this.m_filePath);
                }
                catch (Exception ex)
                {
                    ErrorManager.HandleError(ex);
                    return false;
                }
            }
            foreach (var platform in this.m_platforms)
            {
                this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform).RecordDeleteWave(
                    this.m_parentPath + "\\" + (this.m_node).GetObjectName());
            }
            return true;
        }

        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            temporaryWaveList.RecordDeleteWave(
                    this.m_parentPath + "\\" + (this.m_node).GetObjectName());
            return true;
        }

        public override string GetSummary()
        {
            var platforms = new StringBuilder();
            for (var i = 0; i < this.ActionParameters.Platforms.Count; i++)
            {
                platforms.Append(this.ActionParameters.Platforms[i]);
                if (i != this.ActionParameters.Platforms.Count - 1)
                {
                    platforms.Append(",");
                }
            }

            var sb = new StringBuilder();
            sb.Append("delete wave ");
            sb.Append(this.m_objectName);
            sb.Append(" in ");
            sb.Append(this.m_parentPath);
            sb.Append(" on ");
            sb.Append(platforms.ToString());

            return sb.ToString();
        }

        protected override bool doUndo()
        {
            var treeView = this.m_waveBrowser.GetEditorTreeView().GetTreeView();

            if (this.m_deletedOnAllPlatforms)
            {
                if (treeView.InvokeRequired)
                {
                    treeView.Invoke(
                            new Action<string, EditorTreeNode>(this.m_waveBrowser.GetEditorTreeView().AddWaveNode),
                            new object[] { this.m_parent.FullPath, this.m_node });
                }
                else
                {
                    this.m_waveBrowser.GetEditorTreeView().AddWaveNode(this.m_parent.FullPath, this.m_node);
                }
            }

            foreach (var platform in this.m_platforms)
            {
                this.m_node.AddPlatform(platform, this.m_previousStates[platform]);
                this.m_previousStates.Remove(platform);
            }

            treeView.BeginUpdate();
            this.m_node.UpdateDisplay();
            treeView.EndUpdate();

            return true;
        }

    }
}