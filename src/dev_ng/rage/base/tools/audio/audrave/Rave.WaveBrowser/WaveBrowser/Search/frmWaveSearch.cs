// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmWaveSearch.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The frm wave search.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.WaveBrowser.WaveBrowser.Search
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Threading;
	using System.Windows.Forms;

	using Rave.Instance;
	using Rave.Plugins.Infrastructure.Interfaces;
	using Rave.Utils;
	using Rave.Utils.Popups;
	using Rave.WaveBrowser.Infrastructure.Interfaces;
	using Rave.WaveBrowser.Infrastructure.Nodes;
	using Rave.WaveBrowser.WaveBrowser;
	using System.Collections.ObjectModel;
	using rage.ToolLib;

	/// <summary>
	/// The wave search form.
	/// </summary>
	public partial class frmWaveSearch : Form
	{
		#region Fields

		/// <summary>
		/// The node.
		/// </summary>
		private readonly EditorTreeNode node;

		/// <summary>
		/// The wave browser.
		/// </summary>
		private readonly IWaveBrowser waveBrowser;

		/// <summary>
		/// The status bar thread.
		/// </summary>
		private Thread statusBarThread;

		/// <summary>
		/// The search thread.
		/// </summary>
		private Thread searchThread;

		/// <summary>
		/// The is search running flag.
		/// </summary>
		private bool isSearchRunning;

		/// <summary>
		/// The is cancelled flag.
		/// </summary>
		private bool isCancelled;

		/// <summary>
		/// The busy form.
		/// </summary>
		private frmBusy busyForm;

		/// <summary>
		/// The status text.
		/// </summary>
		private string statusText = "Searching Waves...";

		/// <summary>
		/// The search results text.
		/// </summary>
		private ISet<string> searchResultsText;


		#endregion

		#region Constructors and Destructors

		/// <summary>
		/// Initializes a new instance of the <see cref="frmWaveSearch"/> class.
		/// </summary>
		/// <param name="waveBrowser">
		/// The wave browser.
		/// </param>
		public frmWaveSearch(IWaveBrowser waveBrowser)
		{
			this.InitializeComponent();
			this.waveBrowser = waveBrowser;
			this.m_Results.SelectionMode = SelectionMode.MultiExtended;
			this.PopulatePluginMenu();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="frmWaveSearch"/> class.
		/// </summary>
		/// <param name="node">
		/// The node.
		/// </param>
		/// <param name="waveBrowser">
		/// The wave browser.
		/// </param>
		public frmWaveSearch(EditorTreeNode node, ctrlWaveBrowser waveBrowser)
		{
			this.InitializeComponent();
			this.node = node;
			this.waveBrowser = waveBrowser;
			this.m_Results.SelectionMode = SelectionMode.MultiExtended;
			this.PopulatePluginMenu();
			this.searchWaveFoldersBtn.Enabled = false;
		}

		#endregion

		#region Methods

		/// <summary>
		/// The populate plugin menu.
		/// </summary>
		private void PopulatePluginMenu()
		{
			// add plugins to right click pop up menu
			if (RaveInstance.PluginManager != null)
			{
				var plugins = new HashSet<IRAVEPlugin>();

				foreach (var plugin in RaveInstance.PluginManager.SpeechSearchPlugins)
				{
					plugins.Add(plugin);
				}

				foreach (var plugin in RaveInstance.PluginManager.WaveBrowserPlugins)
				{
					plugins.Add(plugin);
				}

				foreach (var plugin in plugins)
				{
					var menu = new MenuItem(plugin.GetName());
					menu.Click += this.mnuPlugin_Click;
					this.mnuPlugins.MenuItems.Add(menu);
				}
			}
		}

		/// <summary>
		/// Run the search.
		/// </summary>
		private void RunSearch()
		{
			this.gbResults.Text = string.Empty;
			this.ChangeEnabled(false);
			this.searchResultsText = new HashSet<string>();

			// Reset variables
			this.isCancelled = false;
			this.isSearchRunning = true;
			this.statusText = "Searching Waves";

			this.statusBarThread = new Thread(this.StartStatusBar);
			this.statusBarThread.Start();

			this.searchThread = new Thread(this.StartSearch);
			this.searchThread.Start();

			var cancelCheckerThread = new Thread(this.StartCancelChecker);
			cancelCheckerThread.Start();
			m_Results.DoubleClick -= m_Results_DoubleClick;
			m_Results.DoubleClick += m_Results_DoubleClick;
		}

		private void m_Results_DoubleClick(object sender, EventArgs e)
		{
			string path = (string)m_Results.SelectedItem;

			if (searchWaveFoldersBtn.Checked)
			{
				string bank = path.Substring(0, path.Length - Path.GetFileName(path).Length - 1);
				Rave.Instance.RaveInstance.WaveBrowser.GetEditorTreeView().SelectNode(bank, null);
			}
			else
			{
				string bank = path.Substring(0, path.Length - Path.GetFileName(path).Length - 1);
				string wavName = Path.GetFileNameWithoutExtension(path);
				Rave.Instance.RaveInstance.WaveBrowser.GetEditorTreeView().SelectNode(bank, wavName);
			}
		}

		/// <summary>
		/// The start status bar.
		/// </summary>
		private void StartStatusBar()
		{
			this.busyForm = new frmBusy(true);
			this.busyForm.SetUnknownDuration(true);
			this.busyForm.Cancelled += this.BusyForm_Cancelled;

			this.busyForm.Show();
			while (this.isSearchRunning)
			{
				Application.DoEvents();
				this.busyForm.SetStatusText(this.statusText);
			}

			this.busyForm.FinishedWorking();
		}

		/// <summary>
		/// Start Searching
		/// </summary>
		private void StartSearch()
		{
			string searchTerm = this.txtStringInput.Text.Trim();
			ObservableSortedSet<string> results;

			if (this.searchWaveHashNamesBtn.Checked)
			{
				WaveSearchProvider waveSearchProvider = new WaveSearchProvider(this.node);
				results = waveSearchProvider.Search(searchTerm.ToUpper(), true);

				if (results == null)
				{
					ErrorManager.HandleInfo("Name hash must be a number");
				}
			}
			else
			{

				bool success = false;
				results = new ObservableSortedSet<string>();

				if (this.searchWaveNamesBtn.Checked)
				{
					WaveSearchProvider waveSearchProvider = new WaveSearchProvider(this.node, "Wave");
					results = waveSearchProvider.Search(searchTerm, false, false, false, false);
				}
				else if (this.searchWaveFoldersBtn.Checked)
				{
					WaveSearchProvider waveSearchProvider = new WaveSearchProvider(this.node, "WaveFolder");
					results = waveSearchProvider.Search(searchTerm, false, false, false, false);
				}

				if (results != null && results.Count > 0)
				{
					success = true;
				}

				if (success)
				{
					foreach (string result in results)
					{
						this.searchResultsText.Add(result);
					}
				}
				else
				{
					ErrorManager.HandleInfo("Search Failed");
				}
			}

			this.m_Results.Items.Clear();

			this.statusText = "Compiling results...";

			foreach (var result in this.searchResultsText)
			{
				this.m_Results.Items.Add(result);
			}

			this.UpdateResultText();
			this.ChangeEnabled(true);
			this.isSearchRunning = false;
		}

		/// <summary>
		/// Check if user has cancelled search while it is still running.
		/// </summary>
		private void StartCancelChecker()
		{
			while (this.isSearchRunning)
			{
				if (this.isCancelled)
				{
					this.searchThread.Abort();
					this.statusBarThread.Abort();
					this.busyForm.FinishedWorking();
					this.isSearchRunning = false;
				}

				Thread.Sleep(50);
			}

			this.UpdateResultText();
			this.ChangeEnabled(true);
		}

		/// <summary>
		/// Change the enabled status of all controls on the form.
		/// </summary>
		/// <param name="enabled">
		/// The enabled flag.
		/// </param>
		private void ChangeEnabled(bool enabled)
		{
			foreach (Control control in this.Controls)
			{
				control.Enabled = enabled;
			}
		}

		/// <summary>
		/// Update the result text.
		/// </summary>
		private void UpdateResultText()
		{
			lock (this.searchResultsText)
			{
				lock (this.searchResultsText)
				{
					lock (this.gbResults)
					{
						var total = this.searchResultsText.Count;
						this.gbResults.Text = "Results - " + total + " match" + (total != 1 ? "es" : string.Empty);
					}
				}
			}
		}

		/// <summary>
		/// Busy form cancelled action handler.
		/// </summary>
		private void BusyForm_Cancelled()
		{
			this.isCancelled = true;
		}

		/// <summary>
		/// The btn search_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void btnSearch_Click(object sender, EventArgs e)
		{
			this.RunSearch();
		}

		/// <summary>
		/// The mnu plugin_ click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		private void mnuPlugin_Click(object sender, EventArgs e)
		{
			var waves = new ArrayList();

			foreach (var wave in this.m_Results.SelectedItems)
			{
				var wavePath = wave as string;
				if (string.IsNullOrEmpty(wavePath))
				{
					throw new Exception("Expected wave path in string format");
				}

				var parentPath = wavePath.Substring(0, wavePath.LastIndexOf('\\'));
				var waveName = Path.GetFileNameWithoutExtension(wavePath);
				var waveNode = RaveInstance.WaveBrowser.GetEditorTreeView().FindNode(parentPath, waveName) as WaveNode;

				if (null == waveNode)
				{
					ErrorManager.HandleInfo("Failed to find wave node - is the pack loaded? " + wavePath);
					return;
				}

				waves.Add(waveNode);
			}

			foreach (var plugin in RaveInstance.PluginManager.WaveBrowserPlugins)
			{
				if (((MenuItem)sender).Text == plugin.GetName())
				{
					this.TopMost = false;

					var count =
						(from WaveNode wave in waves
						 select this.waveBrowser.GetActionLog().SearchForWaveAdd(wave.GetObjectPath())).Count(
							 action => action != null);
					if (count > 0)
					{
						ErrorManager.HandleInfo(
							"There is one or more pending add wave action for the selected waves - you must commit before you can edit the wave");
					}
					else
					{
						this.waveBrowser.GetEditorTreeView().GetTreeView().BeginUpdate();
						plugin.Process(this.waveBrowser, this.waveBrowser.GetActionLog(), waves, new frmBusy());
						this.waveBrowser.GetEditorTreeView().GetTreeView().EndUpdate();
					}

					this.TopMost = true;
					break;
				}
			}
		}

		#endregion

		/// <summary>
		/// The form closing event handler.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The event args.
		/// </param>
		private void OnFormClosing(object sender, FormClosingEventArgs e)
		{
			if (this.isSearchRunning)
			{
				MessageBox.Show(
					"Cannot close window while search is running",
					"Search Running",
					MessageBoxButtons.OK,
					MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}
	}
}