// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MetadataStore.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The metadata store.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using System.Text.RegularExpressions;
using System.Xml.XPath;

namespace Rave.WaveBrowser.WaveBrowser
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml.Linq;

    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    /// The metadata store.
    /// </summary>
    public class MetadataStore
    {
        #region Fields

        /// <summary>
        /// The m_wave broswer.
        /// </summary>
        private readonly IWaveBrowser m_waveBroswer;

        #endregion

        #region Constructors and Destructors
        
        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataStore"/> class.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        public MetadataStore(IWaveBrowser waveBrowser)
        {
            this.m_waveBroswer = waveBrowser;
            this.Document = new XDocument();
            this.Document.Add(new XElement("LocalWaveChanges"));
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the document.
        /// </summary>
        public XDocument Document { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The apply to visual tree.
        /// </summary>
        /// <param name="tree">
        /// The tree.
        /// </param>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="packs">
        /// The packs.
        /// </param>
        public void ApplyToVisualTree(TreeView tree, IActionLog log, string platform)
        {
            this.ApplyToVisualTree("", this.Document.Root.Elements(), tree.Nodes, log, platform);
        }

        /// <summary>
        /// Combines this metadata store with the provided store
        /// </summary>
        /// <param name="store">
        /// </param>
        public virtual void Combine(MetadataStore store)
        {
            this.Combine(this.Document.Root, store.Document.Root);
        }


        /// <summary>
        /// The create visual tree.
        /// </summary>
        /// <param name="tree">
        /// The tree.
        /// </param>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        public void CreateVisualTree(TreeView tree, IActionLog log, string platform)
        {
            this.CreateVisualTree("", this.Document.Root.Elements(), tree.Nodes, log, platform);
        }

        /// <summary>
        /// The find tag value.
        /// </summary>
        /// <param name="tagPath">
        /// The tag path.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string FindTagValue(string tagPath)
        {
            XElement node = this.FindNodeFromPath(tagPath);
            if (node == null || node.Name != "Tag")
            {
                return null;
            }

            XAttribute attrValue = node.Attribute("value");
            return attrValue != null ? attrValue.Value : null;
        }

        /// <summary>
        /// The get pack names.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<string> GetPackNames()
        {
            if (this.Document.Root == null)
            {
                return null;
            }

            return (from pack in this.Document.Root.Elements()
                    let nameAttrib = pack.Attribute("name")
                    where nameAttrib != null
                    select nameAttrib.Value).ToList();
        }

        /// <summary>
        /// The record add pack folder.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordAddPackFolder(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "PackFolder", "add", externalStores);
        }

        /// <summary>
        /// The record added bank.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordAddedBank(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "Bank", "add", externalStores);
        }

        /// <summary>
        /// The record added bank folder.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordAddedBankFolder(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "BankFolder", "add", externalStores);
        }

        /// <summary>
        /// The record added pack.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordAddedPack(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "Pack", "add", externalStores);
        }

        /// <summary>
        /// The record added preset.
        /// </summary>
        /// <param name="presetPath">
        /// The preset path.
        /// </param>
        /// <param name="presetValue">
        /// The preset value.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordAddedPreset(string presetPath, string presetValue, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(presetPath, "Preset", "add", externalStores);
        }

        /// <summary>
        /// The record added tag.
        /// </summary>
        /// <param name="tagPath">
        /// The tag path.
        /// </param>
        /// <param name="tagValue">
        /// The tag value.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordAddedTag(string tagPath, string tagValue, MetadataStore[] externalStores)
        {
            this.Recordtag(tagPath, tagValue, externalStores, "add");
        }

        /// <summary>
        /// The record added wave.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordAddedWave(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "Wave", "add", externalStores);
        }

        /// <summary>
        /// The record added wave folder.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordAddedWaveFolder(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "WaveFolder", "add", externalStores);
        }

        /// <summary>
        /// The record modified tag.
        /// </summary>
        /// <param name="tagPath">
        /// The tag path.
        /// </param>
        /// <param name="tagValue">
        /// The tag value.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordModifiedTag(string tagPath, string tagValue, MetadataStore[] externalStores)
        {
            this.Recordtag(tagPath, tagValue, externalStores, "modify");
        }

        /// <summary>
        /// The record modified wave.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordModifiedWave(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "Wave", "modify", externalStores);
        }

        /// <summary>
        /// The record removed bank.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordRemovedBank(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "Bank", "remove", externalStores);
        }

        /// <summary>
        /// The record removed bank folder.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordRemovedBankFolder(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "BankFolder", "remove", externalStores);
        }

        /// <summary>
        /// The record removed pack.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordRemovedPack(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "Pack", "remove", externalStores);
        }

        /// <summary>
        /// The record removed pack folder.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordRemovedPackFolder(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "PackFolder", "remove", externalStores);
        }

        /// <summary>
        /// The record removed preset.
        /// </summary>
        /// <param name="presetPath">
        /// The preset path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordRemovedPreset(string presetPath, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(presetPath, "Preset", "remove", externalStores);
        }

        /// <summary>
        /// The record removed tag.
        /// </summary>
        /// <param name="tagPath">
        /// The tag path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordRemovedTag(string tagPath, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(tagPath, "Tag", "remove", externalStores);
        }

        /// <summary>
        /// The record removed wave.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordRemovedWave(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "Wave", "remove", externalStores);
        }

        /// <summary>
        /// The record removed wave folder.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        public void RecordRemovedWaveFolder(string path, MetadataStore[] externalStores)
        {
            this.SetObjectOperation(path, "WaveFolder", "remove", externalStores);
        }

        /// <summary>
        /// The save xml.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool SaveXml(string path)
        {
            bool ret;
            try
            {
                this.Document.Save(path);
                ret = true;
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The find node from path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <returns>
        /// The <see cref="XElement"/>.
        /// </returns>
        private static XElement FindNodeFromPath(string path, XElement parent)
        {
            string[] elems = path.Split('\\');
            XElement currentNode = parent;

            foreach (string elem in elems)
            {
                if (elem.Length <= 0)
                {
                    continue;
                }

                string elemUpper = elem.ToUpper();
                bool foundNode = false;

                foreach (XElement n in currentNode.Elements())
                {
                    XAttribute nameAttr = n.Attribute("name");
                    if (null != nameAttr && nameAttr.Value.ToUpper().Equals(elemUpper))
                    {
                        currentNode = n;
                        foundNode = true;
                        break;
                    }
                }

                if (!foundNode)
                {
                    return null;
                }
            }

            return currentNode;
        }

        /// <summary>
        /// The apply to visual tree.
        /// </summary>
        /// <param name="xmlElements">
        /// The xml elements.
        /// </param>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="packs">
        /// The packs.
        /// </param>
        private void ApplyToVisualTree(
            string path, IEnumerable<XElement> xmlElements, TreeNodeCollection treeNodes, IActionLog log, string platform)
        {
            foreach (XElement xn in xmlElements)
            {
                XAttribute attrName = xn.Attribute("name");
                XAttribute attrValue = xn.Attribute("value");

                if (attrName == null)
                {
                    continue;
                }

                string name = attrName.Value;
                string elementPath = path.Equals("") ? name : path + "\\" + name;

                EditorTreeNode foundNode = treeNodes.Cast<EditorTreeNode>().FirstOrDefault(tn => tn.GetObjectPath().Equals(elementPath));

                XAttribute attrOp = xn.Attribute("operation");
                string operation = attrOp != null ? attrOp.Value : null;
                EditorTreeNode nodeToDraw = null;

                switch (operation)
                {
                    case "add":
                        if(foundNode!=null)
                        {
                            nodeToDraw = foundNode;
                            nodeToDraw.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AddedInPendingWaveList);
                            if (xn.Name == "Tag")
                            {
                                ((TagNode)nodeToDraw).SetValue(platform, attrValue != null ? attrValue.Value : null);
                            }
                        }
                        else
                        {
                            // create new node
                            switch (xn.Name.ToString())
                            {
                                case "PackFolder":
                                    nodeToDraw = new PackFolderNode(log, name, this.m_waveBroswer);
                                    break;
                                case "Pack":
                                    nodeToDraw = new PackNode(log, name, this.m_waveBroswer, name);
                                    break;
                                case "BankFolder":
                                    nodeToDraw = new BankFolderNode(log, name, this.m_waveBroswer);
                                    break;
                                case "Bank":
                                    nodeToDraw = new BankNode(log, name, this.m_waveBroswer);
                                    break;
                                case "WaveFolder":
                                    nodeToDraw = new WaveFolderNode(log, name, this.m_waveBroswer);
                                    break;
                                case "Wave":
                                    nodeToDraw = new WaveNode(log, name, this.m_waveBroswer);
                                    break;
                                case "Tag":
                                    string val = null;
                                    if (attrValue != null)
                                    {
                                        val = attrValue.Value;
                                    }

                                    nodeToDraw = new TagNode(log, name, this.m_waveBroswer);
                                    ((TagNode)nodeToDraw).SetValue(platform, val);
                                    break;
                                case "Preset":
                                    nodeToDraw = new PresetNode(log, name, this.m_waveBroswer);
                                    break;
                            }

                            if (nodeToDraw != null)
                            {
                                nodeToDraw.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AddedInPendingWaveList);

                                treeNodes.Add(nodeToDraw);
                            }
                        }

                        break;

                    case "remove":

                        // remove the specified node from the tree node list

                        if (foundNode != null)
                        {
                            nodeToDraw = foundNode;
                            nodeToDraw.RemovePlatform(platform);
                            if (nodeToDraw.GetPlatforms().Count == 0)
                            {
                                treeNodes.Remove(nodeToDraw);
                                nodeToDraw = null;
                            }
                        }

                        break;

                    case "modify":

                        // update the specified node visually

                        if (foundNode != null)
                        {
                            nodeToDraw = foundNode;
                            nodeToDraw.SetPlatformDisplayState(
                                platform, EditorTreeNode.NodeDisplayState.ChangedInPendingWaveList);

                            if (xn.Name == "Tag")
                            {
                                ((TagNode)nodeToDraw).SetValue(platform, attrValue != null ? attrValue.Value : null);
                            }
                        }

                        break;

                    case "built":
                    case null:
                        if (foundNode != null)
                        {

                            nodeToDraw = foundNode;
                            if (xn.Elements().Any())
                            {
                                nodeToDraw.SetPlatformDisplayState(
                                    platform, EditorTreeNode.NodeDisplayState.ChangedInPendingWaveList);
                            }
                        }

                        break;
                }

                if (nodeToDraw == null)
                {
                    continue;
                }

                // Otherwise mark pack node as loaded
                var packNode = nodeToDraw as PackNode;
                if (null != packNode)
                {
                    packNode.IsLoaded = true;
                }

                this.ApplyToVisualTree(elementPath, xn.Elements(), nodeToDraw.Nodes, log, platform);
            }
        }

        /// <summary>
        /// The combine.
        /// </summary>
        /// <param name="pending">
        /// The pending.
        /// </param>
        /// <param name="local">
        /// The local.
        /// </param>
        private void Combine(XElement pending, XElement local)
        {
            if (null == pending || null == local)
            {
                return;
            }

            // ensure that everything in store2 with an attribute is copied over to store1,
            // resolving conflicts
            foreach (XElement ln in local.Elements())
            {
                // iterate through local wave list
                XElement matchElement = null;
                XAttribute lnAttrName = ln.Attribute("name");
                XAttribute lnAttrOp = ln.Attribute("operation");

                if (lnAttrName != null)
                {
                    string lname = lnAttrName.Value;

                    foreach (XElement pn in pending.Elements())
                    {
                        XAttribute attrValue = pn.Attribute("name");
                        if (attrValue != null)
                        {
                            if (lname == attrValue.Value)
                            {
                                matchElement = pn;
                                break;
                            }
                        }
                    }
                }

                if (matchElement == null)
                {
                    // this node doesnt exist in the pending wave list, copy it from local
                    matchElement = new XElement(ln);

                    if (matchElement.Attribute("operation") == null)
                    {
                        // this node has no attributes, just a hierarchy node so remove all children before adding
                        matchElement.Nodes().Remove();
                    }

                    pending.Add(matchElement);
                }
                else
                {
                    // this node exists in the pending wave list already,
                    // need to combine operations
                    XAttribute meAttrOp = matchElement.Attribute("operation");

                    if (lnAttrOp != null && meAttrOp != null)
                    {
                        // have operation in local and pending, need to decide which to use
                        string poper = meAttrOp.Value;
                        string loper = lnAttrOp.Value;

                        if (poper != loper)
                        {
                            switch (poper)
                            {
                                case "add":
                                    switch (loper)
                                    {
                                        case "remove":

                                            // these operations cancel each other out
                                            // this node's operation should be removed
                                            // nodes with no children and no operation are removed by StripEmptyElements()
                                            meAttrOp.Remove();
                                            break;
                                        case "modify":

                                            // need to copy all attributes across
                                            foreach (XAttribute attr in ln.Attributes())
                                            {
                                                if (attr.Name == "operation")
                                                {
                                                    continue;
                                                }

                                                matchElement.SetAttributeValue(attr.Name, attr.Value);
                                            }

                                            break;
                                    }

                                    break;

                                case "remove":
                                    switch (loper)
                                    {
                                        case "add":

                                            // removed in pending list, added locally
                                            // this node's operation should updated to modify
                                            XAttribute attrOp = matchElement.Attribute("operation");
                                            if (attrOp != null)
                                            {
                                                attrOp.Value = "modify";
                                            }

                                            if (matchElement.Name == "Tag")
                                            {
                                                XAttribute attrValue = matchElement.Attribute("value");

                                                // its a tag so make sure we preserve the latest value
                                                if (ln.Attribute("value") == null)
                                                {
                                                    if (attrValue != null)
                                                    {
                                                        attrValue.Remove();
                                                    }
                                                }
                                                else
                                                {
                                                    XAttribute lnAttrValue = ln.Attribute("value");
                                                    if (attrValue == null && null != lnAttrValue)
                                                    {
                                                        matchElement.SetAttributeValue("value", lnAttrValue.Value);
                                                    }
                                                }
                                            }

                                            break;
                                        case "modify":

                                            // this shouldnt ever happen
                                            break;
                                    }

                                    break;
                                case "modify":
                                    switch (loper)
                                    {
                                        case "add":

                                            // modified in pending, added in local
                                            // shouldnt ever happen
                                            break;
                                        case "remove":

                                            // modified in pending, removed locally
                                            // should remove
                                            matchElement.SetAttributeValue("operation", loper);
                                            break;
                                    }

                                    break;
                            }
                        }
                        else if (loper == "modify" && matchElement.Name == "Tag")
                        {
                            // make sure we use the most recently modified tag value
                            XAttribute lnAttrValue = ln.Attribute("value");
                            if (null != lnAttrValue)
                            {
                                matchElement.SetAttributeValue("value", lnAttrValue.Value);
                            }
                            else
                            {
                                if (matchElement.Attribute("value") != null)
                                {
                                    matchElement.SetAttributeValue("value", null);
                                }
                            }
                        }
                    }
                    else if (lnAttrOp != null)
                    {
                        // copy the local attribute into the pending wave list
                        matchElement.SetAttributeValue("operation", lnAttrOp.Value);
                    }
                    else
                    {
                        // no attribute in local, do nothing (and continue using pending wave list attrib)
                    }
                }

                // continue combine
                this.Combine(matchElement, ln);
            }
        }

        /// <summary>
        /// The create visual tree.
        /// </summary>
        /// <param name="xmlElements">
        /// The xml elements.
        /// </param>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        private void CreateVisualTree(
            string path, IEnumerable<XElement> xmlElements, TreeNodeCollection treeNodes, IActionLog log, string platform)
        {
            foreach (XElement xn in xmlElements)
            {
                XAttribute attrName = xn.Attribute("name");
                XAttribute attrValue = xn.Attribute("value");

                if (attrName == null)
                {
                    continue;
                }

                string name = attrName.Value;
                string elementPath = path.Equals("")? name: path + "\\" + name;

                EditorTreeNode foundNode = treeNodes.Cast<EditorTreeNode>().FirstOrDefault(tn => tn.GetObjectPath().Equals(elementPath));

                if (foundNode != null)
                {

                    // check if wave node and has custom metadata
                    bool waveHasAdditionalMetadata = false;
                    if (xn.Name == "Wave")
                    {
                        foreach (XElement childNode in xn.Elements())
                        {
                            if (childNode.Name == "Chunk")
                            {
                                string chunkName = attrName.Value;
                                if (chunkName == "MARKERS")
                                {
                                    waveHasAdditionalMetadata = true;
                                }
                            }
                        }
                    }

                    if (waveHasAdditionalMetadata)
                    {
                        foundNode.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AlreadyBuiltMetadata);
                    }
                    else
                    {
                        foundNode.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AlreadyBuilt);
                    }

                    if (xn.Name == "Tag")
                    {
                        ((TagNode) foundNode).SetValue(platform, attrValue != null ? attrValue.Value : null);
                    }

                    foundNode.SetPlatformXmlNode(platform, xn);

                    //all packs which have associated metadata are loaded!
                    var packNode = foundNode as PackNode;
                    if (null != packNode)
                    {
                        packNode.IsLoaded = true;
                    }

                    this.CreateVisualTree(elementPath, xn.Elements(), foundNode.Nodes, log, platform);
                }
                else //node wasn't found
                {
                    EditorTreeNode newNode = null;

                    switch (xn.Name.ToString())
                    {
                        case "PackFolder":
                            newNode = new PackFolderNode(log, name, this.m_waveBroswer);
                            break;
                        case "Pack":
                            newNode = new PackNode(log, name, this.m_waveBroswer, name);
                            break;
                        case "BankFolder":
                            newNode = new BankFolderNode(log, name, this.m_waveBroswer);
                            break;
                        case "Bank":
                            newNode = new BankNode(log, name, this.m_waveBroswer);
                            break;
                        case "WaveFolder":
                            newNode = new WaveFolderNode(log, name, this.m_waveBroswer);
                            break;
                        case "Wave":
                            newNode = new WaveNode(log, name, this.m_waveBroswer);
                            break;
                        case "Tag":
                            string val = null;
                            if (attrValue != null)
                            {
                                val = attrValue.Value;
                            }

                            newNode = new TagNode(log, name, this.m_waveBroswer);
                            ((TagNode) newNode).SetValue(platform, val);
                            break;
                        case "Preset":
                            string value = null;
                            if (attrValue != null)
                            {
                                value = attrValue.Value;
                            }

                            newNode = new PresetNode(log, value, this.m_waveBroswer);
                            break;
                    }

                    if (newNode != null)
                    {
                        bool waveHasAdditionalMetadata = false;
                        if (xn.Name == "Wave")
                        {
                            foreach (XElement childNode in xn.Elements())
                            {
                                if (childNode.Name == "Chunk" && childNode.Attribute("name") != null)
                                {
                                    XAttribute childAttrName = childNode.Attribute("name");
                                    if (childAttrName != null)
                                    {
                                        string chunkName = childAttrName.Value;
                                        if (chunkName == "MARKERS")
                                        {
                                            waveHasAdditionalMetadata = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if (waveHasAdditionalMetadata)
                        {
                            newNode.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AlreadyBuiltMetadata);
                        }
                        else
                        {
                            newNode.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AlreadyBuilt);
                        }

                        newNode.SetPlatformXmlNode(platform, xn);

                        treeNodes.Add(newNode);

                        //all packs which have associated metadata are loaded!
                        var packNode = newNode as PackNode;
                        if (null != packNode)
                        {
                            packNode.IsLoaded = true;
                        }

                        this.CreateVisualTree(elementPath, xn.Elements(), newNode.Nodes, log, platform);
                    }
                }
            }
        }

        /// <summary>
        /// The find node from path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="XElement"/>.
        /// </returns>
        public XElement FindNodeFromPath(string path)
        {
            return FindNodeFromPath(path, this.Document.Root);
        }

        /// <summary>
        /// The recordtag.
        /// </summary>
        /// <param name="tagPath">
        /// The tag path.
        /// </param>
        /// <param name="tagValue">
        /// The tag value.
        /// </param>
        /// <param name="externalStores">
        /// The external stores.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        private void Recordtag(string tagPath, string tagValue, MetadataStore[] externalStores, string action)
        {
            // this ensures that it exists locally
            XElement n = this.SetObjectOperation(tagPath, "Tag", action, externalStores);

            if (tagValue != null)
            {
                // update value
                n.SetAttributeValue("value", tagValue);
            }
        }

        /// <summary>
        /// Updates metadata store, ensures that it contains the specified object of the specified type with
        ///     the specified operation attribute value, pulling in nodes from the supplied metadatastores as
        ///     required
        /// </summary>
        /// <param name="path">
        /// </param>
        /// <param name="type">
        /// </param>
        /// <param name="operation">
        /// </param>
        /// <param name="externalStores">
        /// </param>
        /// <returns>
        /// The <see cref="XElement"/>.
        /// </returns>
        private XElement SetObjectOperation(string path, string type, string operation, MetadataStore[] externalStores)
        {
            // we need to build up structure if it is missing from pending waves + built waves
            XElement currentNode = this.Document.Root;

            if (null == currentNode)
            {
                throw new Exception("Couldn't find document root");
            }

            int indexAfterLastBackslash = 0;
            MatchCollection matches = Regex.Matches(path, @"\\");
            foreach(Match backslashMatch in matches)
            {
                string pathElement = path.Substring(indexAfterLastBackslash, backslashMatch.Index - indexAfterLastBackslash);
                if(!(backslashMatch.Index+1>=path.Length)) indexAfterLastBackslash = backslashMatch.Index+1;

                //see whether node is already in memory
                XElement localNode = null;
                foreach(XAttribute foundAttribute in currentNode.Elements().Attributes("name"))
                {
                    if (foundAttribute != null && foundAttribute.Value.Equals(pathElement))
                    {
                        //node found locally, continue
                        localNode = foundAttribute.Parent;
                        break;
                    }
                }
                //node found locally, continue
                if (localNode != null)
                {
                    currentNode = localNode;
                    continue;
                }

                //node needs to be added - see if its in the pending
                // wave list
                XElement externalNode = null;
                string tempPath = path.Substring(0, backslashMatch.Index)+"\\";

                foreach (MetadataStore ms in externalStores)
                {
                    externalNode = ms.FindNodeFromPath(tempPath);
                    if (externalNode != null) break;
                }

                if (externalNode == null)
                {
                    // node doesnt exist anywhere - this is valid if it's being
                    // added
                    if (operation != "add") throw new Exception("Node doesn't exist but isn't being added");

                    currentNode = null;
                    break;
                }
                // create deep copy of current node (don't want to mess with original)
                XElement copiedNode = new XElement(externalNode);

                // strip off operation attrib since we don't want to
                // merge with that until commit time
                XAttribute attrOp = copiedNode.Attribute("operation");
                if (attrOp != null) attrOp.Remove();

                // remove all child nodes of current node
                copiedNode.Nodes().Remove();

                // add to our local document
                currentNode.Add(copiedNode);
                currentNode = copiedNode;
            }

            if (currentNode == null) throw new Exception("Couldn't find parent");

            // search currentNode to see if the node we're after exists - 
            // if it does then we need to merge this operation, however note that
            // we're not searching pending/built wave lists for this node - that merge
            // will be done at commit time.
            String objectNodeName = path.Substring(indexAfterLastBackslash);

            XAttribute nameAttributeOfObjectNode = currentNode.Elements().Attributes("name").FirstOrDefault();

            if (nameAttributeOfObjectNode != null && nameAttributeOfObjectNode.Value.Equals(objectNodeName))
            {
                XElement objectNode = nameAttributeOfObjectNode.Parent;
                XAttribute operationAttribute = objectNode.Attribute("operation");
                string currentOperation = operationAttribute != null ? operationAttribute.Value : null;

                if (currentOperation == null)
                {
                    // this node was previously just pulled from built waves
                    objectNode.SetAttributeValue("operation", operation);
                }

                    // merge operations
                else if (currentOperation == "add" && operation == "remove")
                {
                    // it was added then removed so we can remove the node
                    objectNode.Remove();

                    // no point returning the obsolete node
                    return null;
                }
                else if (currentOperation == "add" && operation == "modify")
                {
                    // it was added then modified - should remain added
                    // attrOp.Value = "add";
                } 
                else if (currentOperation == "remove" && operation == "add")
                {
                    // it was removed then added so we need to change the add to modify
                    objectNode.Attributes().Remove();
                    objectNode.SetAttributeValue("name", objectNodeName);
                    objectNode.SetAttributeValue("operation", "modify");
                }
                else if (currentOperation == "remove" && operation == "modify")
                {
                    throw new Exception("Tried to modified a node that was removed");
                }
                else if (currentOperation == "modify" && operation == "remove")
                {
                    //it was modified then removed so we can remove the node
                    objectNode.Remove();
                    // no point returning the obsolete node
                    return null;
                }
                else if (currentOperation == "modify" && operation == "add")
                {
                    throw new Exception("Tried to add a node that already existed");
                }

                return objectNode;
            }

            // node wasn't found - create a new one
            var newElem = new XElement(type);
            newElem.SetAttributeValue("name", objectNodeName);
            newElem.SetAttributeValue("operation", operation);
            currentNode.Add(newElem);
            return newElem;
        }

        #endregion
    }
}