﻿namespace Rave.WaveBrowser.WaveBrowser
{
	using System.ComponentModel;
	using System.Windows.Forms;

	using Rave.Controls.Forms;

	partial class ctrlWaveEditorTreeView
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private IContainer components;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this.components != null)
				{
					this.components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		///   Required method for Designer support - do not modify 
		///   the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctrlWaveEditorTreeView));
			this.iconList = new System.Windows.Forms.ImageList(this.components);
			this.mnuRightClick = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.mnuLoadSelectedPacks = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuBuildPack = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuBuildBankPreview = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuBuildPackSeperator = new System.Windows.Forms.ToolStripSeparator();
			this.mnuNewPack = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuNewBank = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuNewBankFolder = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuNewWaveFolder = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuNewTag = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuNewVoiceTag = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuPreset = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuSeperator1 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuDelete = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuPlatformSpecific = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuSeperator2 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuLock = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuEditWave = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuPlugins = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuSeperator3 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuProperties = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuContents = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuSearch = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuShowInExplorer = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuBuiltPathToClipboard = new System.Windows.Forms.ToolStripMenuItem();
			this.m_treeView = new Rave.Controls.Forms.ScrollableTreeView();
			this.mnuCreateReaperProject = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuRightClick.SuspendLayout();
			this.SuspendLayout();
			// 
			// iconList
			// 
			this.iconList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iconList.ImageStream")));
			this.iconList.TransparentColor = System.Drawing.Color.Transparent;
			this.iconList.Images.SetKeyName(0, "");
			this.iconList.Images.SetKeyName(1, "");
			this.iconList.Images.SetKeyName(2, "");
			this.iconList.Images.SetKeyName(3, "");
			this.iconList.Images.SetKeyName(4, "");
			this.iconList.Images.SetKeyName(5, "");
			this.iconList.Images.SetKeyName(6, "");
			this.iconList.Images.SetKeyName(7, "");
			this.iconList.Images.SetKeyName(8, "");
			this.iconList.Images.SetKeyName(9, "");
			this.iconList.Images.SetKeyName(10, "");
			this.iconList.Images.SetKeyName(11, "");
			this.iconList.Images.SetKeyName(12, "");
			this.iconList.Images.SetKeyName(13, "");
			this.iconList.Images.SetKeyName(14, "");
			this.iconList.Images.SetKeyName(15, "");
			this.iconList.Images.SetKeyName(16, "BUILT.ico");
			this.iconList.Images.SetKeyName(17, "PENDING.ico");
			this.iconList.Images.SetKeyName(18, "LOCAL.ico");
			this.iconList.Images.SetKeyName(19, "");
			this.iconList.Images.SetKeyName(20, "BUILT_M.ico");
			this.iconList.Images.SetKeyName(21, "PENDING_M.ico");
			this.iconList.Images.SetKeyName(22, "LOCAL_M.ico");
			this.iconList.Images.SetKeyName(23, "");
			this.iconList.Images.SetKeyName(24, "");
			this.iconList.Images.SetKeyName(25, "");
			this.iconList.Images.SetKeyName(26, "");
			this.iconList.Images.SetKeyName(27, "");
			this.iconList.Images.SetKeyName(28, "");
			this.iconList.Images.SetKeyName(29, "");
			this.iconList.Images.SetKeyName(30, "");
			this.iconList.Images.SetKeyName(31, "defaultb.ico");
			// 
			// mnuRightClick
			// 
			this.mnuRightClick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuLoadSelectedPacks,
            this.mnuBuildPack,
			this.mnuBuildBankPreview,
            this.mnuBuildPackSeperator,
            this.mnuNewPack,
            this.mnuNewBank,
            this.mnuNewBankFolder,
            this.mnuNewWaveFolder,
            this.mnuNewTag,
            this.mnuNewVoiceTag,
            this.mnuPreset,
            this.mnuSeperator1,
            this.mnuDelete,
            this.mnuPlatformSpecific,
            this.mnuSeperator2,
            this.mnuLock,
            this.mnuEditWave,
            this.mnuPlugins,
            this.mnuSeperator3,
            this.mnuProperties,
            this.mnuContents,
            this.mnuSearch,
            this.mnuShowInExplorer,
            this.mnuBuiltPathToClipboard,
            this.mnuCreateReaperProject});
			this.mnuRightClick.Name = "mnuRightClick";
			this.mnuRightClick.Size = new System.Drawing.Size(226, 490);
			this.mnuRightClick.Opening += new System.ComponentModel.CancelEventHandler(this.mnuRightClick_Opening);
			// 
			// mnuLoadSelectedPacks
			// 
			this.mnuLoadSelectedPacks.Name = "mnuLoadSelectedPacks";
			this.mnuLoadSelectedPacks.Size = new System.Drawing.Size(225, 22);
			this.mnuLoadSelectedPacks.Text = "Load Pack(s)";
			this.mnuLoadSelectedPacks.Click += new System.EventHandler(this.mnuLoadSelectedPacks_Click);
			// 
			// mnuBuildPack
			// 
			this.mnuBuildPack.Name = "mnuBuildPack";
			this.mnuBuildPack.Size = new System.Drawing.Size(225, 22);
			this.mnuBuildPack.Text = "Build Waves";
			// 
			// mnuBuildBankPreview
			// 
			this.mnuBuildBankPreview.Name = "mnuBuildBankPreview";
			this.mnuBuildBankPreview.Size = new System.Drawing.Size(225, 22);
			this.mnuBuildBankPreview.Text = "Build Bank To Preview";
			// 
			// mnuBuildPackSeperator
			// 
			this.mnuBuildPackSeperator.Name = "mnuBuildPackSeperator";
			this.mnuBuildPackSeperator.Size = new System.Drawing.Size(222, 6);
			// 
			// mnuNewPack
			// 
			this.mnuNewPack.Name = "mnuNewPack";
			this.mnuNewPack.Size = new System.Drawing.Size(225, 22);
			this.mnuNewPack.Text = "New Pack";
			this.mnuNewPack.Click += new System.EventHandler(this.mnuNewPack_Click);
			// 
			// mnuNewBank
			// 
			this.mnuNewBank.Name = "mnuNewBank";
			this.mnuNewBank.Size = new System.Drawing.Size(225, 22);
			this.mnuNewBank.Text = "New Bank";
			this.mnuNewBank.Click += new System.EventHandler(this.mnuNewBank_Click);
			// 
			// mnuNewBankFolder
			// 
			this.mnuNewBankFolder.Name = "mnuNewBankFolder";
			this.mnuNewBankFolder.Size = new System.Drawing.Size(225, 22);
			this.mnuNewBankFolder.Text = "New Bank Folder";
			this.mnuNewBankFolder.Click += new System.EventHandler(this.mnuNewBankFolder_Click);
			// 
			// mnuNewWaveFolder
			// 
			this.mnuNewWaveFolder.Name = "mnuNewWaveFolder";
			this.mnuNewWaveFolder.Size = new System.Drawing.Size(225, 22);
			this.mnuNewWaveFolder.Text = "New Wave Folder";
			this.mnuNewWaveFolder.Click += new System.EventHandler(this.mnuNewWaveFolder_Click);
			// 
			// mnuNewTag
			// 
			this.mnuNewTag.Name = "mnuNewTag";
			this.mnuNewTag.Size = new System.Drawing.Size(225, 22);
			this.mnuNewTag.Text = "New Tag";
			this.mnuNewTag.Click += new System.EventHandler(this.mnuNewTag_Click);
			// 
			// mnuNewVoiceTag
			// 
			this.mnuNewVoiceTag.Name = "mnuNewVoiceTag";
			this.mnuNewVoiceTag.Size = new System.Drawing.Size(225, 22);
			this.mnuNewVoiceTag.Text = "New Voice Tag";
			this.mnuNewVoiceTag.Click += new System.EventHandler(this.mnuNewVoiceTag_Click);
			// 
			// mnuPreset
			// 
			this.mnuPreset.Name = "mnuPreset";
			this.mnuPreset.Size = new System.Drawing.Size(225, 22);
			this.mnuPreset.Text = "New Preset";
			this.mnuPreset.Click += new System.EventHandler(this.mnuPreset_Click);
			// 
			// mnuSeperator1
			// 
			this.mnuSeperator1.Name = "mnuSeperator1";
			this.mnuSeperator1.Size = new System.Drawing.Size(222, 6);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Size = new System.Drawing.Size(225, 22);
			this.mnuDelete.Text = "Delete";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuPlatformSpecific
			// 
			this.mnuPlatformSpecific.Name = "mnuPlatformSpecific";
			this.mnuPlatformSpecific.Size = new System.Drawing.Size(225, 22);
			this.mnuPlatformSpecific.Text = "Make Platform Specific";
			// 
			// mnuSeperator2
			// 
			this.mnuSeperator2.Name = "mnuSeperator2";
			this.mnuSeperator2.Size = new System.Drawing.Size(222, 6);
			// 
			// mnuLock
			// 
			this.mnuLock.Name = "mnuLock";
			this.mnuLock.Size = new System.Drawing.Size(225, 22);
			this.mnuLock.Text = "Lock for Editing";
			this.mnuLock.Click += new System.EventHandler(this.mnuLock_Click);
			// 
			// mnuEditWave
			// 
			this.mnuEditWave.Name = "mnuEditWave";
			this.mnuEditWave.Size = new System.Drawing.Size(225, 22);
			this.mnuEditWave.Text = "Edit Wave";
			this.mnuEditWave.Click += new System.EventHandler(this.mnuEditWave_Click);
			// 
			// mnuPlugins
			// 
			this.mnuPlugins.Name = "mnuPlugins";
			this.mnuPlugins.Size = new System.Drawing.Size(225, 22);
			this.mnuPlugins.Text = "Plugins...";
			// 
			// mnuSeperator3
			// 
			this.mnuSeperator3.Name = "mnuSeperator3";
			this.mnuSeperator3.Size = new System.Drawing.Size(222, 6);
			// 
			// mnuProperties
			// 
			this.mnuProperties.Name = "mnuProperties";
			this.mnuProperties.Size = new System.Drawing.Size(225, 22);
			this.mnuProperties.Text = "Properties";
			this.mnuProperties.Click += new System.EventHandler(this.mnuProperties_Click);
			// 
			// mnuContents
			// 
			this.mnuContents.Name = "mnuContents";
			this.mnuContents.Size = new System.Drawing.Size(225, 22);
			this.mnuContents.Text = "View Contents";
			this.mnuContents.Click += new System.EventHandler(this.mnuContents_Click);
			// 
			// mnuSearch
			// 
			this.mnuSearch.Name = "mnuSearch";
			this.mnuSearch.Size = new System.Drawing.Size(225, 22);
			this.mnuSearch.Text = "Search From Here...";
			this.mnuSearch.Click += new System.EventHandler(this.mnuSearch_Click);
			// 
			// mnuShowInExplorer
			// 
			this.mnuShowInExplorer.Name = "mnuShowInExplorer";
			this.mnuShowInExplorer.Size = new System.Drawing.Size(225, 22);
			this.mnuShowInExplorer.Text = "Show in Explorer";
			this.mnuShowInExplorer.Click += new System.EventHandler(this.mnuShowInExplorer_Click);
			// 
			// mnuBuiltPathToClipboard
			// 
			this.mnuBuiltPathToClipboard.Name = "mnuBuiltPathToClipboard";
			this.mnuBuiltPathToClipboard.Size = new System.Drawing.Size(225, 22);
			this.mnuBuiltPathToClipboard.Text = "Copy Built Path to Clipboard";
			this.mnuBuiltPathToClipboard.Click += new System.EventHandler(this.mnuBuiltPathToClipboard_Click);
			// 
			// m_treeView
			// 
			this.m_treeView.AllowDrop = true;
			this.m_treeView.ContextMenuStrip = this.mnuRightClick;
			this.m_treeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_treeView.HideSelection = false;
			this.m_treeView.ImageIndex = 0;
			this.m_treeView.ImageList = this.iconList;
			this.m_treeView.LastSelectedNode = null;
			this.m_treeView.Location = new System.Drawing.Point(0, 0);
			this.m_treeView.Name = "m_treeView";
			this.m_treeView.SelectedImageIndex = 0;
			this.m_treeView.Size = new System.Drawing.Size(272, 59);
			this.m_treeView.Sorted = true;
			this.m_treeView.TabIndex = 0;
			this.m_treeView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.TreeView_ItemDrag);
			this.m_treeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.TreeView_DragDrop);
			this.m_treeView.DragEnter += new System.Windows.Forms.DragEventHandler(this.TreeView_DragEnter);
			this.m_treeView.DragOver += new System.Windows.Forms.DragEventHandler(this.TreeView_DragOver);
			this.m_treeView.DoubleClick += new System.EventHandler(this.TreeView_DoubleClick);
			this.m_treeView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TreeView_KeyDown);
			this.m_treeView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TreeView_MouseMove);
			// 
			// mnuCreateReaperProject
			// 
			this.mnuCreateReaperProject.Name = "mnuCreateReaperProject";
			this.mnuCreateReaperProject.Size = new System.Drawing.Size(225, 22);
			this.mnuCreateReaperProject.Text = "Create Reaper Project";
            //this.mnuCreateReaperProject.Click += new System.EventHandler(this.mnuCreateReaperProject_Click);
			// 
			// ctrlWaveEditorTreeView
			// 
			this.AllowDrop = true;
			this.Controls.Add(this.m_treeView);
			this.Name = "ctrlWaveEditorTreeView";
			this.Size = new System.Drawing.Size(272, 59);
			this.mnuRightClick.ResumeLayout(false);
			this.ResumeLayout(false);

        }

		#endregion

		private ScrollableTreeView m_treeView;
		private ctrlWaveBrowser m_waveBrowser;
		private ToolStripMenuItem mnuBuiltPathToClipboard;
		private ImageList iconList;
		private ToolStripMenuItem mnuLoadSelectedPacks;
		private ToolStripMenuItem mnuContents;
		private ToolStripMenuItem mnuDelete;
		private ToolStripMenuItem mnuEditWave;
		private ToolStripMenuItem mnuShowInExplorer;
		private ToolStripMenuItem mnuLock;
		private ToolStripMenuItem mnuNewBank;
		private ToolStripMenuItem mnuNewBankFolder;
		private ToolStripMenuItem mnuNewPack;
		private ToolStripMenuItem mnuNewTag;
		private ToolStripMenuItem mnuNewVoiceTag;
		private ToolStripMenuItem mnuNewWaveFolder;
		private ToolStripMenuItem mnuPlatformSpecific;
		private ToolStripMenuItem mnuPlugins;
		private ToolStripMenuItem mnuPreset;
		private ToolStripMenuItem mnuProperties;
		private ContextMenuStrip mnuRightClick;
		private ToolStripMenuItem mnuSearch;
		private ToolStripSeparator mnuSeperator1;
		private ToolStripSeparator mnuSeperator2;
		private ToolStripMenuItem mnuBuildPack;
		private ToolStripMenuItem mnuBuildBankPreview;
		private ToolStripSeparator mnuBuildPackSeperator;
		private ToolStripSeparator mnuSeperator3;
		private ToolStripMenuItem mnuCreateReaperProject;
	}
}
