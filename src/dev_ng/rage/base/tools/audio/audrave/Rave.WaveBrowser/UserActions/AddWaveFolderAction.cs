namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Encapsulates the action of a user adding a wave folder to a bank / wave folder
    /// </summary>
    public class AddWaveFolderAction : UserAction
    {
        private readonly string m_AssetPath;
        private readonly string m_FolderPath;
        private readonly string m_ParentPath;
        private WaveFolderNode m_Node;

        public AddWaveFolderAction(IActionParams parameters)
        {
            this.ActionParameters = parameters;
            this.m_ParentPath = this.ActionParameters.ParentNode.GetObjectPath();

            //asset path
            var sb = new StringBuilder();
            sb.Append(this.m_ParentPath);
            sb.Append("\\");
            sb.Append(this.ActionParameters.Name);
            this.m_AssetPath = sb.ToString();

            //folder path
            sb = new StringBuilder();
            sb.Append(Configuration.PlatformWavePath);
            sb.Append(this.m_AssetPath);
            this.m_FolderPath = sb.ToString();
        }

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                return this.m_Node;
            }
        }

        public override string GetAssetPath()
        {
            return this.m_AssetPath;
        }

        protected override bool doAction()
        {
            if (this.m_Node != null)
            {
                foreach (EditorTreeNode node in this.ActionParameters.ParentNode.Nodes)
                {
                    if (node.GetObjectName() ==
                        this.m_Node.GetObjectName())
                    {
                        return false;
                    }
                }
            }
            else
            {
                this.m_Node = new WaveFolderNode(this.ActionParameters.ActionLog, this.ActionParameters.Name, this.ActionParameters.WaveBrowser);
                foreach (var platform in this.ActionParameters.Platforms)
                {
                    this.m_Node.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AddedLocally);
                }
            }
            if (this.ActionParameters.ParentNode.TryToLock())
            {
                if (this.ActionParameters.ParentNode.TreeView.InvokeRequired)
                {
                    this.ActionParameters.ParentNode.TreeView.Invoke(
                        new Func<TreeNode, int>(this.ActionParameters.ParentNode.Nodes.Add), new object[] { this.m_Node });
                }
                else
                {
                    this.ActionParameters.ParentNode.Nodes.Add(this.m_Node);
                }
                this.m_Node.UpdateDisplay();
                this.ActionParameters.ParentNode.Expand();
                this.m_Node.TreeView.SelectedNode = this.m_Node;

                try
                {
                    Directory.CreateDirectory(this.m_FolderPath);
                }
                catch (Exception)
                {
                    System.Diagnostics.Debug.WriteLine("Create Directory failed - " + this.m_FolderPath);
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        protected override bool doUndo()
        {
            if (this.m_Node == null)
            {
                return false;
            }
            try
            {
                Directory.Delete(this.m_FolderPath);
            }
            catch
            {
                ErrorManager.HandleInfo("Unable to delete the directory");
            }
            if (this.ActionParameters.ParentNode.TreeView.InvokeRequired)
            {
                this.ActionParameters.ParentNode.TreeView.Invoke(
                    new Action<TreeNode>(this.ActionParameters.ParentNode.Nodes.Remove), new object[] { this.m_Node });
            }
            else
            {
                this.ActionParameters.ParentNode.Nodes.Remove(this.m_Node);
            }
            return true;
        }

        protected override bool doCommit()
        {
            foreach (var platform in this.ActionParameters.Platforms)
            {
                // add this to the pending wave list
                this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform).RecordAddWaveFolder(
                    this.m_ParentPath + "\\" + this.ActionParameters.Name);
                this.m_Node.UpdateDisplayState(EditorTreeNode.NodeDisplayState.AddedInPendingWaveList,
                                          platform,
                                          true,
                                          true);
            }
            return true;
        }

        protected override bool doApply(IPendingWaveList wavelist, string platform)
        {
            wavelist.RecordAddWaveFolder(
                    this.m_ParentPath + "\\" + this.ActionParameters.Name);
            return true;
        }
        public override string GetSummary()
        {
            var platforms = new StringBuilder();
            for (var i = 0; i < this.ActionParameters.Platforms.Count; i++)
            {
                platforms.Append(this.ActionParameters.Platforms[i]);
                if (i != this.ActionParameters.Platforms.Count - 1)
                {
                    platforms.Append(",");
                }
            }

            var sb = new StringBuilder();
            sb.Append("new wave folder ");
            sb.Append(this.ActionParameters.Name);
            sb.Append(" in ");
            sb.Append(this.m_ParentPath);
            sb.Append(" added on ");
            sb.Append(platforms.ToString());

            return sb.ToString();
        }
    }
}