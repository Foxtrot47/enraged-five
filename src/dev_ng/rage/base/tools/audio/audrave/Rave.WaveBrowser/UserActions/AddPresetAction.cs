namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.Text;
    using System.Windows.Forms;

    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Summary description for AddTagAction.
    /// </summary>
    public class AddPresetAction : UserAction
    {
        private readonly string m_AssetPath;
        private readonly EditorTreeNode m_Parent;
        private readonly string m_ParentPath;
        private PresetNode m_Node;

        public AddPresetAction(ActionParams parameters)
        {
            this.ActionParameters = parameters;
            this.m_Parent = this.ActionParameters.ParentNode;
            this.m_ParentPath = this.m_Parent.GetObjectPath();
            this.m_Node = this.ActionParameters.Node as PresetNode;

            //asset path
            var sb = new StringBuilder();
            sb.Append(this.m_ParentPath);
            sb.Append("\\");
            sb.Append(this.ActionParameters.Name);
            this.m_AssetPath = sb.ToString();
        }

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                return this.m_Node;
            }
        }

        public override string GetAssetPath()
        {
            return this.m_AssetPath;
        }

        protected override bool doAction()
        {
            //we cant lock waves
            var nodeToLock = this.m_Parent;
            if (nodeToLock.GetType() ==
                typeof (WaveNode))
            {
                nodeToLock = (EditorTreeNode) this.m_Parent.Parent;
            }
            if (nodeToLock.TryToLock())
            {
                if (this.m_Node == null)
                {
                    if (this.ActionParameters.Name == null)
                    {
                        return false;
                    }

                    this.m_Node = new PresetNode(this.ActionParameters.ActionLog, this.ActionParameters.Name, this.ActionParameters.WaveBrowser);
                    if (this.m_Parent.TreeView.InvokeRequired)
                    {
                        this.m_Parent.TreeView.Invoke(new Func<TreeNode, int>(this.m_Parent.Nodes.Add), new object[] { this.m_Node });
                    }
                    else
                    {
                        this.m_Parent.Nodes.Add(this.m_Node);
                    }
                }
                foreach (var platform in this.ActionParameters.Platforms)
                {
                    this.m_Node.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AddedLocally);
                }
                this.m_Node.UpdateDisplay();
                return true;
            }
            return false;
        }

        protected override bool doUndo()
        {
            if (this.ActionParameters.Node == null)
            {
                if (this.m_Parent.TreeView.InvokeRequired)
                {
                    this.m_Parent.TreeView.Invoke(new Action<TreeNode>(this.m_Parent.Nodes.Remove), new object[] { this.m_Node });
                }
                else
                {
                    this.m_Parent.Nodes.Remove(this.m_Node);
                }
            }

            return true;
        }

        protected override bool doCommit()
        {
            foreach (var platform in this.ActionParameters.Platforms)
            {
                // add this to the pending wave list
                this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform).RecordAddPreset(
                    this.m_AssetPath, this.ActionParameters.Value[platform]);
                this.m_Node.UpdateDisplayState(EditorTreeNode.NodeDisplayState.AddedInPendingWaveList,
                                          platform,
                                          true,
                                          true);
            }

            return true;
        }

        protected override bool doApply(IPendingWaveList waveList, string platform)
        {
            waveList.RecordAddPreset(this.m_AssetPath, platform);
            return true;
        }


        public override string GetSummary()
        {
            var platforms = new StringBuilder();
            for (var i = 0; i < this.ActionParameters.Platforms.Count; i++)
            {
                platforms.Append(this.ActionParameters.Platforms[i]);
                if (i != this.ActionParameters.Platforms.Count - 1)
                {
                    platforms.Append(",");
                }
            }

            var sb = new StringBuilder();
            sb.Append("new tag ");
            sb.Append(this.ActionParameters.Name);
            sb.Append(" in ");
            sb.Append(this.m_ParentPath);
            sb.Append(" added on ");
            sb.Append(platforms.ToString());

            return sb.ToString();
        }
    }
}