namespace Rave.WaveBrowser.UserActions
{
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Summary description for EditWaveAction.
    /// </summary>
    public class EditWaveAction : UserAction
    {
        public EditWaveAction(IActionParams parameters)
        {
            this.ActionParameters = parameters;
        }

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                return this.ActionParameters.Node;
            }
        }

        public override string GetAssetPath()
        {
            return this.ActionParameters.Name;
        }

        protected override bool doAction()
        {
            return true;
        }

        protected override bool doCommit()
        {
            return true;
        }

        protected override bool doUndo()
        {
            return false;
        }

        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            return true;
        }

        public override string GetSummary()
        {
            return "Edit wave " + this.ActionParameters.Name + " (cannot be undone)";
        }
    }
}