namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;

    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Encapsulates the action of a user creating a new pack file
    /// </summary>
    public class AddPackAction : UserAction
    {
        private readonly string m_AssetPath;
        private readonly string m_FolderPath;
        private readonly PackFolderNode m_Parent;
        private readonly string m_ParentPath;
        private readonly TreeView m_TreeView;
        private PackNode m_Node;

        public AddPackAction(IActionParams parameters)
        {
            if (!RaveUtils.StringIsValidObjectName(parameters.Name))
            {
                throw new Exception("Pack name " + parameters.Name + " contains invalid characters");
            }
            this.ActionParameters = parameters;
            this.m_Parent = (PackFolderNode) this.ActionParameters.ParentNode;
            //asset path
            var sb = new StringBuilder();

            if (this.m_Parent != null)
            {
                this.m_ParentPath = this.m_Parent.GetObjectPath();
                sb.Append(this.m_ParentPath);
                sb.Append("\\");
            }
            this.m_TreeView = parameters.TreeView;

            sb.Append(this.ActionParameters.Name);
            this.m_AssetPath = sb.ToString();

            //folder path
            sb = new StringBuilder();
            sb.Append(Configuration.PlatformWavePath);
            sb.Append(this.m_AssetPath);
            this.m_FolderPath = sb.ToString();
        }

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                return this.m_Node;
            }
        }

        /// <summary>
        /// Gets the action parameters.
        /// </summary>
        public IActionParams ActionParameters { get; private set; }

        public override string GetAssetPath()
        {
            return this.m_AssetPath;
        }

        protected override bool doAction()
        {
            if (this.ActionParameters.ActionLog != null &&
                this.ActionParameters.ActionLog.SearchForAction(this.GetAssetPath()) != null)
            {
                ErrorManager.HandleInfo("There are pending actions on " + this.GetAssetPath() +
                                        " - you  must commit your changes before you add this pack.");
                return false;
            }

            foreach (EditorTreeNode node in this.ActionParameters.TreeView.Nodes)
            {
                if (node.GetObjectName() ==
                    this.ActionParameters.Name)
                {
                    // the user has created a node with the same name as the one they're
                    // trying to restore...
                    return false;
                }
            }

            if (this.m_Node == null)
            {
                this.m_Node = new PackNode(this.ActionParameters.ActionLog, this.ActionParameters.Name, this.ActionParameters.WaveBrowser, this.ActionParameters.Name);
                foreach (var platform in this.ActionParameters.Platforms)
                {
                    this.m_Node.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AddedLocally);
                }
            }

            try
            {
                Directory.CreateDirectory(this.m_FolderPath);
            }
            catch (Exception)
            {
            }

            if (this.m_Parent != null)
            {
                this.m_Parent.Nodes.Add(this.m_Node);
            }
            else
            {
                if (this.m_TreeView.InvokeRequired)
                {
                    this.m_TreeView.Invoke(new Func<TreeNode, int>(this.m_TreeView.Nodes.Add), new object[] { this.m_Node });
                }
                else
                {
                    this.m_TreeView.Nodes.Add(this.m_Node);
                }
            }
            this.m_Node.UpdateDisplay();
            this.ActionParameters.TreeView.SelectedNode = this.m_Node;

            return true;
        }

        protected override bool doUndo()
        {
            if (this.m_Node == null)
            {
                return false;
            }

            // firstly delete any .lock file that exists
            if (File.Exists(this.m_FolderPath + "\\.lock"))
            {
                File.Delete(this.m_FolderPath + "\\.lock");
            }

            Directory.Delete(this.m_FolderPath, true);
            if (this.m_TreeView.InvokeRequired)
            {
                this.m_TreeView.Invoke(new Action<TreeNode>(this.m_TreeView.Nodes.Remove), new object[] { this.m_Node });
            }
            else
            {
                this.m_TreeView.Nodes.Remove(this.m_Node);
            }
            return true;
        }

        protected override bool doCommit()
        {
            foreach (var platform in this.ActionParameters.Platforms)
            {
                // add this to the pending wave list
                this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform).RecordAddPack(this.m_AssetPath);
                this.m_Node.UpdateDisplayState(EditorTreeNode.NodeDisplayState.AddedInPendingWaveList,
                                          platform,
                                          true,
                                          true);
            }
            return true;
        }

        protected override bool doApply(IPendingWaveList waveList, string platform)
        {
            waveList.RecordAddPack(this.m_AssetPath);
            return true;
        }


        public override string GetSummary()
        {
            var platforms = "";
            for (var i = 0; i < this.ActionParameters.Platforms.Count; i++)
            {
                platforms += this.ActionParameters.Platforms[i];
                if (i != this.ActionParameters.Platforms.Count - 1)
                {
                    platforms += ",";
                }
            }

            return "new pack " + this.ActionParameters.Name + " added on " + platforms;
        }
    }
}