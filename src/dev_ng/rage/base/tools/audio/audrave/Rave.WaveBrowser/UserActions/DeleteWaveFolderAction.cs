namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Summary description for DeleteWaveFolderAction.
    /// </summary>
    public class DeleteWaveFolderAction : UserAction
    {
        private readonly List<string> m_Platforms;
        private readonly Dictionary<string, EditorTreeNode.NodeDisplayState> m_PreviousStates;
        private readonly string m_objectName;
        private readonly BankNode m_parent;
        private readonly string m_parentPath;
        private bool bDeleteAll;

        public DeleteWaveFolderAction(IActionParams parameters)
        {
            this.ActionParameters = parameters;
            this.m_parent = (BankNode) this.ActionParameters.Node.Parent;
            this.m_objectName = this.ActionParameters.Node.GetObjectName();
            this.m_parentPath = this.m_parent.GetObjectPath();
            this.m_PreviousStates = new Dictionary<string, EditorTreeNode.NodeDisplayState>();
            this.m_Platforms = new List<string>(parameters.Platforms);
        }

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                return this.ActionParameters.Node;
            }
        }

        public override string GetAssetPath()
        {
            return this.m_parentPath + "\\" + this.m_objectName;
        }

        protected override bool doAction()
        {
            if (this.m_parent.TryToLock())
            {
                foreach (var platform in this.m_Platforms)
                {
                    this.m_PreviousStates.Add(platform, this.ActionParameters.Node.GetNodeDisplayState(platform));
                    this.ActionParameters.Node.RemovePlatform(platform);
                }
                this.bDeleteAll = false;
                if (this.ActionParameters.Node.GetPlatforms().Count == 0)
                {
                    if (this.m_parent.TreeView.InvokeRequired)
                    {
                        this.m_parent.TreeView.Invoke(new Action<TreeNode>(this.m_parent.Nodes.Remove),
                                                 new object[] {this.ActionParameters.Node});
                    }
                    else
                    {
                        this.m_parent.Nodes.Remove(this.ActionParameters.Node);
                    }

                    this.bDeleteAll = true;
                }
                this.ActionParameters.Node.UpdateDisplay();

                return true;
            }
            return false;
        }

        protected override bool doCommit()
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(Configuration.PlatformWavePath);
                sb.Append(this.m_parentPath);
                sb.Append("\\");
                sb.Append(this.m_objectName);
                var localPath = sb.ToString();
                // firstly delete any .lock file that exists
                if (RaveInstance.AssetManager.ExistsAsAsset(localPath + "\\.lock"))
                {
                    if (RaveInstance.RaveAssetManager.WaveChangeList == null)
                    {
                        RaveInstance.RaveAssetManager.WaveChangeList =
                            RaveInstance.AssetManager.CreateChangeList("[Rave] Wave Changelist");
                    }
                    RaveInstance.RaveAssetManager.WaveChangeList.MarkAssetForDelete(localPath + "\\.lock");
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }
            foreach (var platform in this.m_Platforms)
            {
                this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform).RecordDeleteWaveFolder(
                    this.m_parentPath + "\\" + this.m_objectName);
            }
            return true;
        }

        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            temporaryWaveList.RecordDeleteWaveFolder(
                    this.m_parentPath + "\\" + this.m_objectName);
            return true;
        }

        public override string GetSummary()
        {
            var platforms = "";
            for (var i = 0; i < this.m_Platforms.Count; i++)
            {
                platforms += this.m_Platforms[i];
                if (i < this.m_Platforms.Count - 1)
                {
                    platforms += ",";
                }
            }
            return "delete wave folder " + this.m_objectName + " in " + this.m_parentPath + " on " + platforms;
        }

        protected override bool doUndo()
        {
            if (this.bDeleteAll)
            {
                if (this.m_parent.TreeView.InvokeRequired)
                {
                    this.m_parent.TreeView.Invoke(new Func<TreeNode, int>(this.m_parent.Nodes.Add),
                                             new object[] {this.ActionParameters.Node});
                }
                else
                {
                    this.m_parent.Nodes.Add(this.ActionParameters.Node);
                }
            }

            foreach (var platform in this.m_Platforms)
            {
                this.ActionParameters.Node.AddPlatform(platform, this.m_PreviousStates[platform]);
                this.m_PreviousStates.Remove(platform);
            }

            this.ActionParameters.Node.UpdateDisplay();

            return true;
        }
    }
}