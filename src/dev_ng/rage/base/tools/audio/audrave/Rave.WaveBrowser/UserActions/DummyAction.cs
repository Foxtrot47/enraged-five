namespace Rave.WaveBrowser.UserActions
{
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    public class DummyAction : UserAction
    {
        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                return null;
            }
        }

        protected override bool doCommit()
        {
            return true;
        }

        protected override bool doUndo()
        {
            return true;
        }

        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            return true;
        }

        protected override bool doAction()
        {
            return true;
        }

        public override string GetSummary()
        {
            return string.Empty;
        }

        public override string GetAssetPath()
        {
            return string.Empty;
        }
    }
}