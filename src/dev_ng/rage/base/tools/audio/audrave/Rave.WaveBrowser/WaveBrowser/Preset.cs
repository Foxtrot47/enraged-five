namespace Rave.WaveBrowser.WaveBrowser
{
    using System.Collections.Generic;
    using System.Xml;

    public class Preset
    {
        private readonly List<string> m_platforms;

        static Preset()
        {
            Presets = new Dictionary<string, Preset>();
        }

        public Preset(XmlNode node)
        {
            this.m_platforms = new List<string>();
            foreach (XmlNode platform in node.ChildNodes)
            {
                this.m_platforms.Add(platform.Attributes["name"].Value.ToUpper());
            }
        }

        public static Dictionary<string, Preset> Presets { get; private set; }
    }
}