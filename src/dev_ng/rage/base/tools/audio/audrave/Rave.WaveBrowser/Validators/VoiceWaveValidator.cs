﻿// -----------------------------------------------------------------------
// <copyright file="VoiceWaveValidator.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.WaveBrowser.Validators
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;

    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    /// Voice wave validator.
    /// </summary>
    public class VoiceWaveValidator
    {
        /// <summary>
        /// Validate the voice waves.
        /// </summary>
        /// <param name="bankNodes">
        /// The bank nodes.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Validate(IList<BankNode> bankNodes, StringBuilder errors, StringBuilder warnings)
        {
            var isValid = true;

            // Validate voice wave sequence numbers in list of banks
            foreach (var bankNode in bankNodes)
            {
                var waveSeqNumbers = new Dictionary<string, IList<uint>>();

                foreach (WaveNode waveNode in bankNode.GetChildWaveNodes())
                {
                    if (null == waveNode || !IsVoiceWave(waveNode))
                    {
                        continue;
                    }

                    var waveName = waveNode.GetWaveName().ToUpper();

                    // Get sequence number of current wave node
                    uint version;
                    if (!GetVoiceWaveVersionNumber(waveNode, out version, errors, warnings))
                    {
                        continue;
                    }

                    var isProcessedWave = waveName.Contains(".PROCESSED");
                    var baseName = waveName.Substring(0, waveName.LastIndexOf('_'));
                    var basePath = Path.Combine(waveNode.Parent.FullPath, baseName);

                    if (isProcessedWave)
                    {
                        basePath += ".PROCESSED";
                    }

                    if (!waveSeqNumbers.ContainsKey(basePath))
                    {
                        waveSeqNumbers[basePath] = new List<uint>();
                    }

                    waveSeqNumbers[basePath].Add(version);
                }

                // Check we have no gaps in the sequences
                foreach (var kvp in waveSeqNumbers)
                {
                    var basePath = kvp.Key;
                    var seqNumbers = kvp.Value;

                    var counter = 1;
                    foreach (var num in seqNumbers)
                    {
                        if (num == counter)
                        {
                            counter++;
                            continue;
                        }

                        var error = string.Format(
                            "{0}: Gap in version sequence numbers ({1})", basePath, string.Join(", ", seqNumbers));
                        warnings.AppendLine(error);

                        // Only need one error entry for each wave node
                        isValid = false;
                        break;
                    }
                }
            }

            return isValid;
        }

        /// <summary>
        /// Determine if wave node is a voice wave.
        /// </summary>
        /// <param name="waveNode">
        /// The wave node.
        /// </param>
        /// <returns>
        /// Value indicating if wave node is a voice wave <see cref="bool"/>.
        /// </returns>
        private static bool IsVoiceWave(WaveNode waveNode)
        {
            var currNode = waveNode.Parent;
            while (null != currNode)
            {
                foreach (var node in currNode.Nodes)
                {
                    var tagNode = node as TagNode;
                    if (null != tagNode && tagNode.GetTagName() == "voice")
                    {
                        return true;
                    }
                }

                currNode = currNode.Parent;
            }

            return false;
        }

        /// <summary>
        /// Get the voice wave version number.
        /// </summary>
        /// <param name="waveNode">
        /// The wave node.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="warnings">
        /// The warnings.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private static bool GetVoiceWaveVersionNumber(WaveNode waveNode, out uint version, StringBuilder errors, StringBuilder warnings)
        {
            var waveName = waveNode.GetWaveName();
            var waveNameNoExt = Path.GetFileNameWithoutExtension(waveName);
            waveNameNoExt = Regex.Replace(waveNameNoExt, ".PROCESSED", string.Empty, RegexOptions.IgnoreCase);
            var versionStr = waveNameNoExt.Substring(waveNameNoExt.LastIndexOf('_') + 1);

            if (!uint.TryParse(versionStr, out version))
            {
                warnings.AppendLine("Failed to find version number in wave name: " + waveNode.FullPath);
                return false;
            }

            return true;
        }
    }
}
