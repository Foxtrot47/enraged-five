﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rave.WaveBrowser.WaveBrowser {
    public class PackMetadata {

        public MetadataStore localMetadataStore { get; set; }
        public BuiltWavesMetadataStore buildMetadataStore { get; set; }
        public PendingWavesMetadataStore pendingMetadataStore { get; set; }
    }
}
