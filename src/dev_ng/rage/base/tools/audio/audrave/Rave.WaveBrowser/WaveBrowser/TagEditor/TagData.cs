﻿// -----------------------------------------------------------------------
// <copyright file="TagStruct.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.WaveBrowser.WaveBrowser.TagEditor
{
    using System.Collections.Generic;

    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    /// Tag data class.
    /// </summary>
    public class TagData
    {
        /// <summary>
        /// Gets or sets the tag node.
        /// </summary>
        public TagNode TagNode { get; set; }

        /// <summary>
        /// Gets or sets the parent node.
        /// </summary>
        public EditorTreeNode ParentNode { get; set; }

        /// <summary>
        /// Gets or sets the original values.
        /// </summary>
        public IDictionary<string, string> OriginalValues { get; set; }

        /// <summary>
        /// Gets or sets the current values.
        /// </summary>
        public IDictionary<string, string> CurrentValues { get; set; }

        /// <summary>
        /// Gets or sets the original platforms.
        /// </summary>
        public IList<string> OriginalPlatforms { get; set; }
    }
}
