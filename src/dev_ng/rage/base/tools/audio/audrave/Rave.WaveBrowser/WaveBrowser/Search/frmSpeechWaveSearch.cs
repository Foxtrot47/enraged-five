// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmSpeechWaveSearch.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The frm speech wave search.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.WaveBrowser.WaveBrowser.Search
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.Utils.Popups;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.WaveBrowser;

    /// <summary>
    /// The frm speech wave search.
    /// </summary>
    public partial class frmSpeechWaveSearch : Form
    {
        #region Fields

        /// <summary>
        /// The m_node.
        /// </summary>
        private readonly EditorTreeNode m_node;

        /// <summary>
        /// The m_search from node.
        /// </summary>
        private readonly bool m_searchFromNode;

        /// <summary>
        /// The m_wave browser.
        /// </summary>
        private readonly IWaveBrowser m_waveBrowser;

        /// <summary>
        /// The m_b is finished.
        /// </summary>
        private bool m_bIsFinished;

        /// <summary>
        /// The m_status text.
        /// </summary>
        private string m_statusText = "Searching Speech Waves...";

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="frmSpeechWaveSearch"/> class.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        public frmSpeechWaveSearch(IWaveBrowser waveBrowser)
        {
            this.InitializeComponent();
            this.m_waveBrowser = waveBrowser;
            this.m_Results.SelectionMode = SelectionMode.MultiExtended;
            this.PopulatePluginMenu();
            this.m_bSearchAllLines.Checked = true;

            this.txtStringInput.Focus();
            this.ActiveControl = this.txtStringInput;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="frmSpeechWaveSearch"/> class.
        /// </summary>
        /// <param name="etn">
        /// The etn.
        /// </param>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        public frmSpeechWaveSearch(EditorTreeNode etn, ctrlWaveBrowser waveBrowser)
            : this(waveBrowser)
        {
            this.m_node = etn;
            this.m_searchFromNode = true;
            this.m_bSearchUnprocessed.Enabled = false;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The populate plugin menu.
        /// </summary>
        private void PopulatePluginMenu()
        {
            // add plugins to right click pop up menu
            if (RaveInstance.PluginManager != null)
            {
                var plugins = new HashSet<IRAVEPlugin>();

                foreach (var plugin in RaveInstance.PluginManager.SpeechSearchPlugins)
                {
                    plugins.Add(plugin);
                }

                foreach (var plugin in RaveInstance.PluginManager.WaveBrowserPlugins)
                {
                    plugins.Add(plugin);
                }

                foreach (var plugin in plugins)
                {
                    var menu = new MenuItem(plugin.GetName());
                    menu.Click += this.mnuPlugin_Click;
                    this.mnuPlugins.MenuItems.Add(menu);
                }
            }
        }

        /// <summary>
        /// The search folders_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SearchFolders_CheckedChanged(object sender, EventArgs e)
        {
            this.m_bSearchAllLines.Checked = !this.m_bSearchUnprocessed.Checked;
        }

        /// <summary>
        /// The search wave names_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SearchWaveNames_CheckedChanged(object sender, EventArgs e)
        {
            this.m_bSearchUnprocessed.Checked = !this.m_bSearchAllLines.Checked;
        }

        /// <summary>
        /// The start status bar.
        /// </summary>
        private void StartStatusBar()
        {
            var busy = new frmBusy();
            busy.SetUnknownDuration(true);
            busy.Show();
            while (!this.m_bIsFinished)
            {
                Application.DoEvents();
                busy.SetStatusText(this.m_statusText);
            }

            busy.FinishedWorking();

            // Reset variables
            this.m_bIsFinished = false;
            this.m_statusText = "Searching Waves";
        }

        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            // TODO Use better search algorithm, divide and conquer?
            // too many waves to simply loop through
            IList<WaveNode> results = new List<WaveNode>();

            var t = new Thread(this.StartStatusBar);
            t.Start();

            if (this.m_searchFromNode)
            {
                var wcn = this.m_node as WaveContainerNode;
                if (wcn != null)
                {
                    foreach (WaveNode wn in wcn.GetChildWaveNodes())
                    {
                        if (wn.GetObjectName().Contains(this.txtStringInput.Text)
                            && (string.IsNullOrEmpty(this.missionBox.Text)
                                || wn.GetObjectPath().ToUpper().Contains(this.missionBox.Text.ToUpper()))
                            && (string.IsNullOrEmpty(this.characterBox.Text)
                                || wn.GetObjectPath().ToUpper().Contains(this.characterBox.Text.ToUpper())))
                        {
                            results.Add(wn);
                        }
                    }
                }
            }
            else
            {
                if (this.m_bSearchAllLines.Checked)
                {
                    this.m_waveBrowser.GetEditorTreeView()
                        .SpeechToBeProcessed(
                            this.txtStringInput.Text, 
                            this.missionBox.Text, 
                            this.characterBox.Text, 
                            this.m_bIncludePlaceholder.Checked, 
                            this.m_bSearchConversationRoots.Checked,
                            out results);
                }
                else if (this.m_bSearchUnprocessed.Checked)
                {
                    this.m_waveBrowser.GetEditorTreeView()
                        .SpeechToBeProcessed_OutOfDate(
                            this.txtStringInput.Text, 
                            this.missionBox.Text, 
                            this.characterBox.Text, 
                            this.m_bIncludePlaceholder.Checked,
                            this.m_bSearchConversationRoots.Checked,
                            out results);
                }
            }

            this.m_Results.Items.Clear();

            this.m_statusText = "Compiling results...";
            foreach (var wavePath in results)
            {
                this.m_Results.Items.Add(wavePath);
            }

            this.m_bIsFinished = true;
            this.gbResults.Text = "Results - " + results.Count + " match" + (results.Count != 1 ? "es" : string.Empty);
        }

        /// <summary>
        /// The group box 1_ enter.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void groupBox1_Enter(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// The label 2_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void label2_Click(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// The m_ results_ key down.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void m_Results_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                var sb = new System.Text.StringBuilder();
                foreach (WaveNode wave in this.m_Results.SelectedItems)
                {
                    sb.AppendLine(wave.GetObjectPath());
                }

                if (sb.Length > 0)
                {
                    Clipboard.SetText(sb.ToString());
                }
            }
        }

        /// <summary>
        /// The mnu plugin_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuPlugin_Click(object sender, EventArgs e)
        {
            var waves = new ArrayList();

            foreach (WaveNode wave in this.m_Results.SelectedItems)
            {
                waves.Add(wave);
            }

            var pluginText = ((MenuItem)sender).Text;
            IRAVEPlugin selectedPlugin = null;

            // Check for speech search plugin
            foreach (var plugin in RaveInstance.PluginManager.SpeechSearchPlugins)
            {
                if (pluginText == plugin.GetName())
                {
                    selectedPlugin = plugin;
                    break;
                }
            }

            // Check for wave browser plugin
            if (null == selectedPlugin)
            {
                foreach (var plugin in RaveInstance.PluginManager.WaveBrowserPlugins)
                {
                    if (pluginText == plugin.GetName())
                    {
                        selectedPlugin = plugin;
                        break;
                    }
                }
            }

            if (null != selectedPlugin)
            {
                this.TopMost = false;

                var count =
                    (from WaveNode wave in waves
                     select this.m_waveBrowser.GetActionLog().SearchForWaveAdd(wave.GetObjectPath())).Count(
                         action => action != null);
                if (count > 0)
                {
                    ErrorManager.HandleInfo(
                        "There is one or more pending add wave action for the selected waves - you must commit before you can edit the wave");
                }
                else
                {
                    this.m_waveBrowser.GetEditorTreeView().GetTreeView().BeginUpdate();
                    var searchPlugin = selectedPlugin as IRAVESpeechSearchPlugin;

                    if (null != searchPlugin)
                    {
                        searchPlugin.Process(
                            this.m_waveBrowser, this.m_waveBrowser.GetActionLog(), waves, new frmBusy());
                    }
                    else
                    {
                        var wavePlugin = selectedPlugin as IRAVEWaveBrowserPlugin;
                        if (null != wavePlugin)
                        {
                            wavePlugin.Process(
                                this.m_waveBrowser, this.m_waveBrowser.GetActionLog(), waves, new frmBusy());
                        }
                    }

                    this.m_waveBrowser.GetEditorTreeView().GetTreeView().EndUpdate();
                }

                this.TopMost = true;
            }
            else
            {
                ErrorManager.HandleError("Failed to find selected plugin: " + pluginText);
            }
        }

        #endregion
    }
}