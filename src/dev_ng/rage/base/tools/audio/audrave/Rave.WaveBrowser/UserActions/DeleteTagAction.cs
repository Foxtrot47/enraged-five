namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Forms;

    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.WaveBrowser;

    /// <summary>
    ///   Summary description for DeleteTagAction.
    /// </summary>
    public class DeleteTagAction : UserAction
    {
        private readonly TagNode m_node;
        private readonly string m_nodePath;
        private readonly EditorTreeNode m_parent;
        private readonly Dictionary<string, string> m_tagPlatformValues;
        private bool m_removedNode;

        public DeleteTagAction(IActionParams parameters)
        {
            this.ActionParameters = parameters;
            this.m_parent = (EditorTreeNode)this.ActionParameters.Node.Parent;
            this.m_nodePath = this.ActionParameters.Node.GetObjectPath();
            this.m_node = this.ActionParameters.Node as TagNode;
            this.m_tagPlatformValues = new Dictionary<string, string>(this.m_node.Values);
        }

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                return this.m_node;
            }
        }

        public override string GetAssetPath()
        {
            return this.m_nodePath;
        }

        protected override bool doAction()
        {
            var parent = this.m_parent;
            // we cant lock waves
            if (parent.GetType() ==
                typeof(WaveNode))
            {
                parent = (EditorTreeNode)parent.Parent;
            }
            if (parent.TryToLock())
            {
                foreach (var platform in this.ActionParameters.Platforms)
                {
                    this.ActionParameters.Node.RemovePlatform(platform);
                }

                if (this.ActionParameters.Node.GetPlatforms().Count == 0)
                {
                    if (this.ActionParameters.ParentNode.TreeView.InvokeRequired)
                    {
                        this.ActionParameters.ParentNode.TreeView.Invoke(
                            new Action<TreeNode>(this.ActionParameters.ParentNode.Nodes.Remove),
                            new object[] { this.ActionParameters.Node });
                    }
                    else
                    {
                        this.ActionParameters.ParentNode.Nodes.Remove(this.ActionParameters.Node);
                    }
                    this.m_removedNode = true;
                }
                return true;
            }
            return false;
        }

        protected override bool doUndo()
        {
            foreach (var platform in this.ActionParameters.Platforms)
            {
                this.m_node.SetValue(platform, this.m_tagPlatformValues[platform]);
            }
            if (this.m_removedNode)
            {
                if (this.ActionParameters.ParentNode.TreeView.InvokeRequired)
                {
                    this.ActionParameters.ParentNode.TreeView.Invoke(new Func<TreeNode, int>(this.ActionParameters.ParentNode.Nodes.Add),
                                                            new object[] { this.ActionParameters.Node });
                }
                else
                {
                    this.ActionParameters.ParentNode.Nodes.Add(this.ActionParameters.Node);
                }
                this.m_removedNode = false;
            }
            return true;
        }


        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            temporaryWaveList.RecordDeleteTag(
                    this.m_nodePath);
            return true;
        }


        protected override bool doCommit()
        {
            // add this to the pending wave list
            foreach (var platform in this.ActionParameters.Platforms)
            {
                this.m_node.RemoveValue(platform);
                this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform).RecordDeleteTag(this.m_nodePath);
            }
            this.ActionParameters.Node.UpdateDisplayState(EditorTreeNode.NodeDisplayState.ChangedInPendingWaveList,
                                                 ctrlWaveBrowser.ALL_PLATFORMS,
                                                 true,
                                                 true);
            return true;
        }

        public override string GetSummary()
        {
            var platforms = new StringBuilder();
            for (var i = 0; i < this.ActionParameters.Platforms.Count; i++)
            {
                platforms.Append(this.ActionParameters.Platforms[i]);
                if (i != this.ActionParameters.Platforms.Count - 1)
                {
                    platforms.Append(",");
                }
            }

            var sb = new StringBuilder();
            sb.Append("delete bank ");
            sb.Append(this.m_node.GetTagName());
            sb.Append(" from ");
            sb.Append(this.m_nodePath);
            sb.Append(" on ");
            sb.Append(platforms.ToString());

            return sb.ToString();
        }
    }
}