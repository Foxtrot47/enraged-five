namespace Rave.WaveBrowser.WaveBrowser
{
    using System;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

    using rage.ToolLib.WavesFiles;
    using System.Xml.Linq;

    public class BuiltWavesMetadataStore : MetadataStore
    {

        string packFilePath;

        public BuiltWavesMetadataStore(IWaveBrowser waveBrowser, string packFile) : base(waveBrowser) {
            this.packFilePath = packFile;
            ReloadXml();
        }

        public bool ReloadXml()
        {   
            try
            {
                XElement root = new XElement("BuiltWaves");
                root.Add(XDocument.Load(packFilePath).Root);
                this.Document = new XDocument(root);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public override bool SaveXml(string path)
        {
            throw new ApplicationException("Rave is trying to save content to the BuiltWaves folder.");
        }
    }
}