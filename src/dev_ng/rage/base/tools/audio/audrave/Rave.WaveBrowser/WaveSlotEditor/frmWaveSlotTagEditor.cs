// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmWaveSlotTagEditor.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for frmTagEditor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.WaveBrowser.WaveSlotEditor
{
    using System.Windows.Forms;

    using rage;

    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Nodes.WaveSlot;

    /// <summary>
    /// Wave slot tag editor.
    /// </summary>
    public class frmWaveSlotTagEditor : Form
    {
        #region Fields

        /// <summary>
        /// The parent.
        /// </summary>
        private readonly TreeNode parent;

        /// <summary>
        /// The cancel button.
        /// </summary>
        private Button btnCancel;

        /// <summary>
        /// The ok button.
        /// </summary>
        private Button btnOk;

        /// <summary>
        /// The tag name combo box.
        /// </summary>
        private ComboBox cbTagName;

        /// <summary>
        /// The name label.
        /// </summary>
        private Label lblName;

        /// <summary>
        /// The value label.
        /// </summary>
        private Label lblValue;

        /// <summary>
        /// The platform combo box.
        /// </summary>
        private ComboBox cbPlatform;

        /// <summary>
        /// The platform label.
        /// </summary>
        private Label lblPlatform;

        /// <summary>
        /// The value text box.
        /// </summary>
        private TextBox tbValue;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="frmWaveSlotTagEditor"/> class.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        public frmWaveSlotTagEditor(WaveSlotTagNode node, TreeNode parent)
        {
            this.InitializeComponent();
            this.Node = node;
            this.parent = parent;
            this.Init();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the node.
        /// </summary>
        public WaveSlotTagNode Node { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Initialize the editor.
        /// </summary>
        private void Init()
        {
            // Setup platform combo box
            foreach (var platformSetting in Configuration.ActivePlatformSettings)
            {
                var index = this.cbPlatform.Items.Add(platformSetting.PlatformTag);
                if (null != this.Node && platformSetting.PlatformTag == this.Node.Platform)
                {
                    this.cbPlatform.SelectedIndex = index;
                }
            }

            // Setup wave slot attributes combo box
            foreach (var attribute in Configuration.WaveSlotAttributes)
            {
                var index = this.cbTagName.Items.Add(attribute);
                if (null != this.Node && attribute == this.Node.Key)
                {
                    this.cbTagName.SelectedIndex = index;
                }
            }

            if (null != this.Node)
            {
                if (!Configuration.WaveSlotAttributes.Contains(this.Node.Key))
                {
                    ErrorManager.HandleError("Failed to find key in wave slot attributes list: " + this.Node.Key);
                    return;
                }

                this.tbValue.Text = this.Node.Value;
            }
        }

        /// <summary>
        /// Save current values.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool Save()
        {
            if (!this.IsValidTag())
            {
                return false;
            }

            var tagValue = this.tbValue.Text;
            var tagName = this.cbTagName.SelectedItem.ToString();
            var platform = this.cbPlatform.SelectedItem.ToString();

            if (null == this.Node)
            {
                // Create new tag node
                this.Node = new WaveSlotTagNode(tagName, tagValue, platform);
                this.parent.Nodes.Add(this.Node);
            }
            else
            {
                // Update existing tag node
                this.Node.Key = tagName;
                this.Node.Value = tagValue;
            }

            return true;
        }

        /// <summary>
        /// Determine if tag properties are valid.
        /// </summary>
        /// <returns>
        /// Indication of whether tag properties are valid <see cref="bool"/>.
        /// </returns>
        private bool IsValidTag()
        {
            var tagValue = this.tbValue.Text;

            if (null == this.cbPlatform.SelectedItem ||
                string.IsNullOrEmpty(this.cbPlatform.SelectedItem.ToString()))
            {
                ErrorManager.HandleInfo("Platform required.");
                return false;
            }

            if (null == this.cbTagName.SelectedItem || 
                string.IsNullOrEmpty(this.cbTagName.SelectedItem.ToString()))
            {
                ErrorManager.HandleInfo("Tag name required.");
                return false;
            }

            if (string.IsNullOrEmpty(tagValue))
            {
                ErrorManager.HandleInfo("Value required.");
                return false;
            }

            uint size;
            if (!uint.TryParse(tagValue, out size))
            {
                ErrorManager.HandleInfo("Value must be a positive integer.");
                return false;
            }

            var platform = this.cbPlatform.SelectedItem.ToString();
            var tagName = this.cbTagName.SelectedItem.ToString();

            foreach (TreeNode childNode in this.parent.Nodes)
            {
                var tagNode = childNode as WaveSlotTagNode;
                if (null != tagNode && tagNode != this.Node && tagNode.Key == tagName && tagNode.Platform == platform)
                {
                    ErrorManager.HandleInfo(string.Format("Wave slot already has tag '{0}' for platform '{1}' - edit existing node instead.", tagName, platform));
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.cbTagName = new System.Windows.Forms.ComboBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblValue = new System.Windows.Forms.Label();
            this.tbValue = new System.Windows.Forms.TextBox();
            this.cbPlatform = new System.Windows.Forms.ComboBox();
            this.lblPlatform = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(273, 110);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(192, 110);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // cbTagName
            // 
            this.cbTagName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTagName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTagName.Location = new System.Drawing.Point(92, 47);
            this.cbTagName.Name = "cbTagName";
            this.cbTagName.Size = new System.Drawing.Size(256, 21);
            this.cbTagName.TabIndex = 9;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(12, 47);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(40, 16);
            this.lblName.TabIndex = 10;
            this.lblName.Text = "Name:";
            // 
            // lblValue
            // 
            this.lblValue.AutoSize = true;
            this.lblValue.Location = new System.Drawing.Point(12, 81);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(37, 13);
            this.lblValue.TabIndex = 11;
            this.lblValue.Text = "Value:";
            // 
            // tbValue
            // 
            this.tbValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbValue.Location = new System.Drawing.Point(92, 74);
            this.tbValue.Name = "tbValue";
            this.tbValue.Size = new System.Drawing.Size(256, 20);
            this.tbValue.TabIndex = 12;
            // 
            // cbPlatform
            // 
            this.cbPlatform.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPlatform.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPlatform.Location = new System.Drawing.Point(92, 20);
            this.cbPlatform.Name = "cbPlatform";
            this.cbPlatform.Size = new System.Drawing.Size(256, 21);
            this.cbPlatform.TabIndex = 14;
            // 
            // lblPlatform
            // 
            this.lblPlatform.Location = new System.Drawing.Point(12, 20);
            this.lblPlatform.Name = "lblPlatform";
            this.lblPlatform.Size = new System.Drawing.Size(55, 21);
            this.lblPlatform.TabIndex = 15;
            this.lblPlatform.Text = "Platform:";
            // 
            // frmWaveSlotTagEditor
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(360, 145);
            this.ControlBox = false;
            this.Controls.Add(this.cbPlatform);
            this.Controls.Add(this.lblPlatform);
            this.Controls.Add(this.tbValue);
            this.Controls.Add(this.lblValue);
            this.Controls.Add(this.cbTagName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmWaveSlotTagEditor";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Wave Slot Tag Editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// Handle OK button click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void btnOk_Click(object sender, System.EventArgs e)
        {
            if (this.Save())
            {
                this.Close();
            }
        }

        /// <summary>
        /// Handle cancel button click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}