namespace Rave.WaveBrowser.WaveBrowser
{
    using System;
    using System.Collections;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;

    public class WaveReader
    {
        private readonly Data m_Data;
        private readonly FileInfo m_FileInfo;
        private readonly FileStream m_FileStream;
        private readonly string m_Filepath;
        private readonly Fmt m_Fmt;
        private readonly Riff m_Riff;
        private readonly iXML m_iXML;
        private readonly Smpl m_smpl;
        private readonly ArrayList m_unknownChunks;
        private int m_cursor, m_xoffset;
        private float m_xscale;
        private float m_xzoom = 1.0f;
        private float m_yscale;
        private float m_yzoom = 1.0f;

        public WaveReader(String inFilepath)
        {
            this.m_Filepath = inFilepath;
            this.m_FileInfo = new FileInfo(inFilepath);
            this.m_FileStream = this.m_FileInfo.OpenRead();

            this.m_Riff = new Riff();
            this.m_Fmt = new Fmt();
            this.m_Data = new Data();
            this.m_iXML = new iXML();
            this.m_smpl = new Smpl();
            this.m_unknownChunks = new ArrayList();
        }

        public void Read()
        {
            var buf = new byte[8];

            // first 4 bytes are chunk id, second are chunk size
            string chunkID;
            uint chunkSize;
            var finished = false;
            while (!finished)
            {
                if (this.m_FileStream.Read(buf, 0, 8) <= 0)
                {
                    finished = true;
                    continue;
                }
                chunkID = Encoding.ASCII.GetString(buf, 0, 4);

                if (chunkID[0] == '\0')
                {
                    finished = true;
                    continue;
                }

                chunkSize = buf[4];
                chunkSize += ((uint) buf[5]) << 8;
                chunkSize += ((uint) buf[6]) << 16;
                chunkSize += ((uint) buf[7]) << 24;

                if (chunkID == "RIFF")
                {
                    // riff chunk contains other chunks, we therefore have to change the
                    // chunk size and not read the entire file into the RIFF chunk
                    chunkSize = 4; // just the format to read
                    this.m_Riff.ReadChunk(chunkSize, this.m_FileStream);
                }
                else if (chunkID == "fmt ")
                {
                    this.m_Fmt.ReadChunk(chunkSize, this.m_FileStream);
                }
                else if (chunkID == "data")
                {
                    this.m_Data.ReadChunk(chunkSize, this.m_FileStream);
                }
                else if (chunkID == "iXML")
                {
                    this.m_iXML.ReadChunk(chunkSize, this.m_FileStream);
                }
                else if (chunkID == "smpl")
                {
                    this.m_smpl.ReadChunk(chunkSize, this.m_FileStream);
                }
                else
                {
                    var chunkData = new byte[chunkSize + 8];
                    buf.CopyTo(chunkData, 0);
                    this.m_FileStream.Read(chunkData, 8, (int) chunkSize);

                    // save this data so we can stick it back in later
                    this.m_unknownChunks.Add(chunkData);
                }
            }
            this.m_FileStream.Close();
        }

        public void DrawWave(PaintEventArgs pea)
        {
            var grfx = pea.Graphics;
            var visBounds = grfx.VisibleClipBounds;

            this.m_yscale = 1.0f * this.m_yzoom;
            // scale x so that the entire sample fits in the visbounds
            this.m_xscale = (visBounds.Width / this.m_Data.NumSamples) * this.m_xzoom;

            System.Diagnostics.Debug.Assert(this.m_Fmt.Channels == 1);
            this.RenderMonoWave(grfx, visBounds);
        }

        public void DrawLoop(PaintEventArgs pea)
        {
            var grfx = pea.Graphics;
            var visBounds = grfx.VisibleClipBounds;

            if (this.m_Fmt.Channels == 1)
            {
                this.RenderMonoLoop(grfx, visBounds);
            }
        }

        private void RenderMonoLoop(Graphics g, RectangleF visBounds)
        {
            // number of samples to show at each end of the loop
            var numSamples = (int) ((visBounds.Width / 2.0f));

            var rightStart = this.m_smpl.SamplerData.Loops[0].start;
            var rightEnd = this.m_smpl.SamplerData.Loops[0].start + numSamples;

            var leftStart = this.m_smpl.SamplerData.Loops[0].end - numSamples;
            var leftEnd = this.m_smpl.SamplerData.Loops[0].end;

            var xscale = 1.0f; //visBounds.Width / (numSamples*2);

            g.FillRectangle(new SolidBrush(Color.FromArgb(240, 240, 255)), 0, 0, visBounds.Width, visBounds.Height);

            var path = new GraphicsPath();

            Int32 lastx = 0, lasty = 0;

            // render left samples (end of loop)
            for (var i = leftStart; i <= leftEnd; i++)
            {
                Int32 sample = this.m_Data[i];

                // scale from -32768 -> 32768 to 0 -> visBounds.Height
                sample += 32768;
                // now 0 -> 65536
                var y = (Int32) ((sample / 65536.0f) * visBounds.Height);
                var x = i;

                x = (Int32) ((x - leftStart) * xscale) + (Int32) visBounds.Left;

                if (i - leftStart >= 1)
                {
                    path.AddLine(lastx, lasty, x, y);
                }

                lastx = x;
                lasty = y;
            }

            // render left samples (end of loop)
            for (var i = rightStart; i <= rightEnd; i++)
            {
                Int32 sample = this.m_Data[i];

                // scale from -32768 -> 32768 to 0 -> visBounds.Height
                sample += 32768;
                // now 0 -> 65536
                var y = (Int32) ((sample / 65536.0f) * visBounds.Height);
                var x = i;

                x = (Int32) ((x - rightStart) * xscale);
                x += (int) (visBounds.Width / 2.0f);

                if (i - rightStart >= 1)
                {
                    path.AddLine(lastx, lasty, x, y);
                }

                lastx = x;
                lasty = y;
            }

            var p = new Pen(Color.LightGray, 1.1f);
            p.DashStyle = DashStyle.Dash;

            g.DrawLine(p, visBounds.Width / 2, 0, visBounds.Width / 2, visBounds.Height);
            g.DrawLine(p, 0, visBounds.Height / 2, visBounds.Width, visBounds.Height / 2);

            g.DrawPath(Pens.DarkBlue, path);

            g.DrawString("Loop point (showing " + (numSamples * 2) + " samples)",
                         new Font("Tahoma", 8.0f),
                         Brushes.Gray,
                         5,
                         visBounds.Height - 20);
        }

        private void RenderMonoWave(Graphics g, RectangleF visBounds)
        {
            Int32 lastx = 0, lasty = 0;
            var gp = new GraphicsPath();
            var p = Pens.DarkBlue;

            g.FillRectangle(new SolidBrush(Color.FromArgb(240, 240, 255)),
                            0,
                            0,
                            (Int32) ((this.m_Data.NumSamples - this.m_xoffset) * this.m_xscale),
                            visBounds.Height);

            for (var i = this.m_xoffset; i < this.m_Data.NumSamples; i += 10)
            {
                Int32 sample = this.m_Data[i];

                // scale from -32768 -> 32768 to 0 -> visBounds.Height
                sample += 32768;
                // now 0 -> 65536
                var y = (Int32) ((sample / 65536.0f) * visBounds.Height);
                var x = i;

                x = (Int32) ((x - this.m_xoffset) * this.m_xscale);

                y = (Int32) (y * this.m_yscale);

                if (i >= 1)
                {
                    gp.AddLine(lastx, lasty, x, y);
                    //g.DrawLine(p, lastx, lasty, x, y);
                }

                if (this.m_yzoom > 20.0f &&
                    this.m_xzoom > 20.0f)
                {
                    // zoomed in a lot - draw individual samples clearly
                    //g.FillEllipse(new SolidBrush(Color.Black), x, y, 3, 3);	
                }

                lastx = x;
                lasty = y;
            }

            // draw path
            g.DrawPath(p, gp);

            // draw x axis
            g.DrawLine(p, 0, visBounds.Height / 2, visBounds.Width, visBounds.Height / 2);

            // draw cursor position
            g.DrawLine(Pens.Black,
                       (this.m_cursor - this.m_xoffset) * this.m_xscale,
                       0,
                       (this.m_cursor - this.m_xoffset) * this.m_xscale,
                       visBounds.Height);

            // draw a line to show the end of the wave
            g.DrawLine(p, lastx, 0, lastx, visBounds.Height);

            // show loop points
            for (var i = 0; i < this.m_smpl.SamplerData.sampleLoops; i++)
            {
                g.DrawLine(Pens.OrangeRed,
                           (this.m_smpl.SamplerData.Loops[i].start - this.m_xoffset) * this.m_xscale,
                           0,
                           (this.m_smpl.SamplerData.Loops[i].start - this.m_xoffset) * this.m_xscale,
                           visBounds.Height);
                g.DrawLine(Pens.OrangeRed,
                           (this.m_smpl.SamplerData.Loops[i].end - this.m_xoffset) * this.m_xscale,
                           0,
                           (this.m_smpl.SamplerData.Loops[i].end - this.m_xoffset) * this.m_xscale,
                           visBounds.Height);
            }

            var zoom = (int) (this.m_xzoom * 100);
            g.DrawString(zoom + "%", new Font("Tahoma", 8.0f), new SolidBrush(Color.Gray), 5, visBounds.Height - 20);
            g.DrawString(this.m_Fmt.ToString(),
                         new Font("Tahoma", 8.0f),
                         new SolidBrush(Color.Gray),
                         50,
                         visBounds.Height - 20);
        }

        public void ZoomIn()
        {
            this.m_xzoom *= 1.1f;
        }

        public void ZoomOut()
        {
            this.m_xzoom *= 0.9f;
        }

        public void UpdateCursor(int numBytes)
        {
            // convert to samples
            this.m_cursor = (numBytes / ((this.m_Fmt.BitsPerSample / 8) * this.m_Fmt.Channels));
//			if(m_xzoom > 1.0f)
//			{
//				// make sure you can see the playing sample
//				m_xoffset = m_cursor;
//			}
        }

        public void SetXOffset(int xoffset)
        {
            // offset specified in samples
            this.m_xoffset = xoffset;
        }

        public Fmt GetFmt()
        {
            return this.m_Fmt;
        }

        public Riff GetRiff()
        {
            return this.m_Riff;
        }

        public Data GetData()
        {
            return this.m_Data;
        }

        public Smpl GetSmpl()
        {
            return this.m_smpl;
        }

        public string GetFilepath()
        {
            return this.m_Filepath;
        }

        public bool IsLooping()
        {
            return (this.GetSmpl().SamplerData.sampleLoops >= 1);
        }

        #region Nested type: Data

        /// <summary>
        ///   The Data block is 8 bytes + ???? long
        /// </summary>
        public class Data
        {
            private Int16[] m_Data;
            private uint m_DataSize;
            private int m_NumSamples;

            public uint DataSize
            {
                get { return this.m_DataSize; }
            }

            public Int16 this[int pos]
            {
                get { return this.m_Data[pos]; }
            }

            public int NumSamples
            {
                get { return this.m_NumSamples; }
            }

            public void ReadChunk(uint chunkSize, FileStream inFS)
            {
                var binRead = new BinaryReader(inFS);

                this.m_DataSize = chunkSize;
                this.m_Data = new Int16[this.m_DataSize];
                this.m_NumSamples = (int) (this.m_DataSize / 2);

                for (var i = 0; i < this.m_NumSamples; i++)
                {
                    this.m_Data[i] = binRead.ReadInt16();
                }
            }

            public Int16[] GetBuffer()
            {
                return this.m_Data;
            }
        }

        #endregion

        #region Nested type: Fmt

        /// <summary>
        ///   The Format header is 24 bytes long
        /// </summary>
        public class Fmt
        {
            private uint m_AverageBytesPerSec;
            private ushort m_BitsPerSample;
            private ushort m_BlockAlign;
            private ushort m_Channels;
            private uint m_FmtSize;
            private ushort m_FmtTag;
            private uint m_SamplesPerSec;

            public uint FmtSize
            {
                get { return this.m_FmtSize; }
            }

            public ushort FmtTag
            {
                get { return this.m_FmtTag; }
            }

            public ushort Channels
            {
                get { return this.m_Channels; }
            }

            public uint SamplesPerSec
            {
                get { return this.m_SamplesPerSec; }
            }

            public uint AverageBytesPerSec
            {
                get { return this.m_AverageBytesPerSec; }
            }

            public ushort BlockAlign
            {
                get { return this.m_BlockAlign; }
            }

            public ushort BitsPerSample
            {
                get { return this.m_BitsPerSample; }
            }

            public void ReadChunk(uint chunkSize, FileStream inFS)
            {
                var binRead = new BinaryReader(inFS);

                var startPosition = inFS.Position - 8;

                this.m_FmtSize = chunkSize;
                this.m_FmtTag = binRead.ReadUInt16();
                this.m_Channels = binRead.ReadUInt16();
                this.m_SamplesPerSec = binRead.ReadUInt32();
                this.m_AverageBytesPerSec = binRead.ReadUInt32();
                this.m_BlockAlign = binRead.ReadUInt16();
                this.m_BitsPerSample = binRead.ReadUInt16();

                // This accounts for the variable format header size 
                inFS.Seek(this.m_FmtSize + startPosition + 8, SeekOrigin.Begin);
            }

            public override string ToString()
            {
                var s = this.m_SamplesPerSec + "Hz " + this.m_BitsPerSample + "bit ";

                if (this.m_Channels == 1)
                {
                    s += "mono";
                }
                else if (this.m_Channels == 2)
                {
                    s += "stereo";
                }
                else
                {
                    s += this.m_Channels + " channels";
                }

                return s;
            }
        }

        #endregion

        #region Nested type: Riff

        /// <summary>
        ///   The Riff header is 12 bytes long
        /// </summary>
        public class Riff
        {
            private readonly byte[] m_RiffFormat;
            private uint m_RiffSize;

            public Riff()
            {
                this.m_RiffFormat = new byte[4];
            }

            public uint RiffSize
            {
                get { return (this.m_RiffSize); }
            }

            public byte[] RiffFormat
            {
                get { return this.m_RiffFormat; }
            }

            public void ReadChunk(uint chunkSize, FileStream inFS)
            {
                this.m_RiffSize = chunkSize;
                inFS.Read(this.m_RiffFormat, 0, 4);
            }

            public void WriteChunk(FileStream fs)
            {
                fs.Write(this.m_RiffFormat, 0, 4);
            }
        }

        #endregion

        #region Nested type: Smpl

        public class Smpl
        {
            private byte[] buf;
            private uint m_chunkSize;
            private autSamplerHeader m_header;

            /*
			 * typedef struct {
  long  dwIdentifier;
  long  dwType;
  long  dwStart;
  long  dwEnd;
  long  dwFraction;
  long  dwPlayCount;
} autSampleLoop;

typedef struct {
  long           dwManufacturer;
  long           dwProduct;
  long           dwSamplePeriod;
  long           dwMIDIUnityNote;
  long           dwMIDIPitchFraction;
  long           dwSMPTEFormat;
  long           dwSMPTEOffset;
  long           cSampleLoops;
  long           cbSamplerData;
  autSampleLoop Loops[];
} autSamplerHeader;
*/

            public autSamplerHeader SamplerData
            {
                get { return this.m_header; }
            }

            public void WriteChunk(FileStream fs)
            {
                var bw = new BinaryWriter(fs);

                bw.Write(this.m_header.manufacturer);
                bw.Write(this.m_header.product);
                bw.Write(this.m_header.samplePeriod);
                bw.Write(this.m_header.MIDIUnityNode);
                bw.Write(this.m_header.MIDIPitchFraction);
                bw.Write(this.m_header.SMPTEFormat);
                bw.Write(this.m_header.SMPTEOffset);
                bw.Write(this.m_header.sampleLoops);
                bw.Write(this.m_header.samplerData);

                for (var i = 0; i < this.m_header.Loops.Length; i++)
                {
                    bw.Write(this.m_header.Loops[i].identifier);
                    bw.Write(this.m_header.Loops[i].type);
                    bw.Write(this.m_header.Loops[i].start);
                    bw.Write(this.m_header.Loops[i].end);
                    bw.Write(this.m_header.Loops[i].fraction);
                    bw.Write(this.m_header.Loops[i].playCount);
                }
            }

            public void ReadChunk(uint chunkSize, FileStream fs)
            {
                this.buf = new byte[chunkSize];
                fs.Read(this.buf, 0, (int) chunkSize);
                this.m_chunkSize = chunkSize;

                var br = new BinaryReader(new MemoryStream(this.buf));

                this.m_header.manufacturer = br.ReadInt32();
                this.m_header.product = br.ReadInt32();
                this.m_header.samplePeriod = br.ReadInt32();
                this.m_header.MIDIUnityNode = br.ReadInt32();
                this.m_header.MIDIPitchFraction = br.ReadInt32();
                this.m_header.SMPTEFormat = br.ReadInt32();
                this.m_header.SMPTEOffset = br.ReadInt32();
                this.m_header.sampleLoops = br.ReadInt32();
                this.m_header.samplerData = br.ReadInt32();

                this.m_header.Loops = new autSampleLoop[this.m_header.sampleLoops];

                for (var i = 0; i < this.m_header.Loops.Length; i++)
                {
                    this.m_header.Loops[i].identifier = br.ReadInt32();
                    this.m_header.Loops[i].type = br.ReadInt32();
                    this.m_header.Loops[i].start = br.ReadInt32();
                    this.m_header.Loops[i].end = br.ReadInt32();
                    this.m_header.Loops[i].fraction = br.ReadInt32();
                    this.m_header.Loops[i].playCount = br.ReadInt32();
                }
            }

            #region Nested type: autSampleLoop

            public struct autSampleLoop
            {
                public Int32 end;
                public Int32 fraction;
                public Int32 identifier;
                public Int32 playCount;
                public Int32 start;
                public Int32 type;
            }

            #endregion

            #region Nested type: autSamplerHeader

            public struct autSamplerHeader
            {
                public autSampleLoop[] Loops;
                public Int32 MIDIPitchFraction;
                public Int32 MIDIUnityNode;
                public Int32 SMPTEFormat;
                public Int32 SMPTEOffset;
                public Int32 manufacturer;
                public Int32 product;
                public Int32 sampleLoops;
                public Int32 samplePeriod;
                public Int32 samplerData;
            }

            #endregion
        }

        #endregion

        #region Nested type: iXML

        public class iXML
        {
            private byte[] m_buf;
            private uint m_chunkSize;
            private XmlDocument m_xmlDoc;

            public iXML()
            {
                this.m_chunkSize = 0;
                this.m_xmlDoc = null;
            }

            public void ReadChunk(uint chunkSize, FileStream fs)
            {
                this.m_chunkSize = chunkSize;
                this.m_buf = new byte[chunkSize];
                fs.Read(this.m_buf, 0, (int) chunkSize);

                this.LoadXml();
            }

            private void LoadXml()
            {
                this.m_xmlDoc = new XmlDocument();

                var xml = Encoding.ASCII.GetString(this.m_buf);
                this.m_xmlDoc.LoadXml(xml);
            }
        }

        #endregion
    }
}