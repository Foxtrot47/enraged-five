namespace Rave.WaveBrowser.WaveBrowser
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using Rave.Instance;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;

    public class PendingWavesMetadataStore : MetadataStore
    {
        private string packFilePath;

        public PendingWavesMetadataStore(IWaveBrowser waveBrowser, string packFile) : base(waveBrowser)
        {
            this.packFilePath = packFile;
            this.ReloadXml();
        }

        public bool ReloadXml() 
        {
            try {
                if(File.Exists(packFilePath)) 
                {
                    this.Document = XDocument.Load(packFilePath);
                }
                else {
                    this.Document = new XDocument(new XElement("PendingWaves"));
                }
                return true;
            }
            catch 
            {
                return false;
            }
        }

        public string getLoadedXmlPath() 
        {
            return packFilePath;
        }

        public bool SaveXml()
        {
            try
            {

                var assetMgr = RaveInstance.AssetManager;

                if((assetMgr.ExistsAsAsset(packFilePath) && assetMgr.IsCheckedOut(packFilePath)) ||
                        assetMgr.IsMarkedForAdd(packFilePath)) 
                {
                    this.Document.Save(packFilePath);
                }
            }
            catch
            {
                return false;
            }

            return true;
        }


        public void Combine(MetadataStore store, MetadataStore builtWavesMetadataStore)
        {
            base.Combine(store);
            StripEmptyElements(this.Document.Root);
            this.RemoveEmptyBanks(builtWavesMetadataStore);
        }

        private void StripEmptyElements(XElement parent) {
            var elementsToDelete = new List<XElement>();

            foreach(var child in parent.Elements()) {
                StripEmptyElements(child);

                if(!child.Elements().Any() && null == child.Attribute("operation")) {
                    elementsToDelete.Add(child);
                }
            }

            foreach(var element in elementsToDelete) {
                element.Remove();
            }
        }

        private void RemoveEmptyBanks(MetadataStore builtWavesMetadataStore)
        {
            var builtWavesDoc = builtWavesMetadataStore.Document;
            var banksToRemove = new List<XElement>();

            foreach (var pendingWavesBank in this.Document.Root.Descendants("Bank"))
            {
                var packElem = pendingWavesBank.Ancestors("Pack").First();
                var packName = packElem.Attribute("name").Value;

                var builtWavesBanks = from bank in builtWavesDoc.Root.Descendants("Bank")
                                      where bank.Attribute("name").Value == pendingWavesBank.Attribute("name").Value
                                      where bank.Ancestors("Pack").Any(pack => null != pack.Attribute("name") && pack.Attribute("name").Value == packName)
                                      select bank;

                if (builtWavesBanks.Count() > 1)
                {
                    throw new Exception(string.Format("Failed to locate bank in built waves; {0} matching banks found", builtWavesBanks.Count()));
                }

                var builtWavesBank = builtWavesBanks.FirstOrDefault();

                if (builtWavesBank == null)
                {
                    // This is a new pending bank
                    if (BankIsEmpty(pendingWavesBank))
                    {
                        banksToRemove.Add(pendingWavesBank);
                    }
                }
                else
                {
                    // This is an existing bank
                    if (WillBankBeEmptyAfterBuild(pendingWavesBank, builtWavesBank))
                    {
                        pendingWavesBank.SetAttributeValue("operation", "remove");
                    }
                }
            }

            foreach (var bankToRemove in banksToRemove)
            {
                var parent = bankToRemove.Parent;
                bankToRemove.Remove();

                while (parent != null && !parent.HasElements &&
                       parent != this.Document.Root)
                {
                    var temp = parent;
                    parent = parent.Parent;
                    temp.Remove();
                }
            }
        }

        private static bool WillBankBeEmptyAfterBuild(XElement pendingWavesBank, XElement builtWavesBank)
        {
            try
            {
                var addedWaveCount = (from wave in pendingWavesBank.Descendants("Wave")
                                      where
                                          wave.Attribute("operation") != null &&
                                          wave.Attribute("operation").Value == "add"
                                      select wave).Count();
                var removedWaveCount = (from wave in pendingWavesBank.Descendants("Wave")
                                        where
                                            wave.Attribute("operation") != null &&
                                            wave.Attribute("operation").Value == "remove"
                                        select wave).Count();

                if (addedWaveCount == 0 &&
                    removedWaveCount > 0)
                {
                    var totalWaveCount = builtWavesBank.Descendants("Wave").Count();
                    return removedWaveCount >= totalWaveCount;
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return true;
            }
            return false;
        }

        private static bool BankIsEmpty(XElement pendingWavesBank)
        {
            var hasWaves = false;
            foreach (var wave in pendingWavesBank.Descendants("Wave"))
            {
                if (wave.Attribute("operation") != null &&
                    wave.Attribute("operation").Value == "add")
                {
                    hasWaves = true;
                    break;
                }
            }
            return !hasWaves;
        }
    }
}