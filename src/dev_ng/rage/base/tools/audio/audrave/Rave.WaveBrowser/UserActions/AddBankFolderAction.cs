namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;

    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Encapsulates the action of a user adding a bank folder to a pack
    /// </summary>
    public class AddBankFolderAction : UserAction
    {
        private readonly string m_AssetPath;
        private readonly string m_FolderPath;
        private readonly string m_parentPath;

        public AddBankFolderAction(IActionParams parameters)
        {
            this.ActionParameters = parameters;
            this.m_parentPath = this.ActionParameters.ParentNode.GetObjectPath();

            //asset path
            var sb = new StringBuilder();
            sb.Append(this.m_parentPath);
            sb.Append("\\");
            sb.Append(this.ActionParameters.Name);
            this.m_AssetPath = sb.ToString();

            //folder path
            sb = new StringBuilder();
            sb.Append(Configuration.PlatformWavePath);
            sb.Append(this.m_AssetPath);
            this.m_FolderPath = sb.ToString();
        }

        public override string GetAssetPath()
        {
            return this.m_AssetPath;
        }

        protected override bool doAction()
        {
            foreach (EditorTreeNode node in this.ActionParameters.ParentNode.Nodes)
            {
                if (node.GetObjectName() ==
                    this.ActionParameters.Name)
                {
                    return false;
                }
            }

            if (this.Node == null)
            {
                this.Node = new BankFolderNode(this.ActionParameters.ActionLog, this.ActionParameters.Name, this.ActionParameters.WaveBrowser);
                ((BankFolderNode)this.Node).IsLoaded = true;
                foreach (var platform in this.ActionParameters.Platforms)
                {
                    this.Node.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AddedLocally);
                }
            }

            if (this.ActionParameters.ParentNode.TryToLock())
            {
                var sb = new StringBuilder();
                sb.Append(Configuration.PlatformWavePath);
                sb.Append("\\");
                sb.Append(this.m_parentPath);
                sb.Append("\\");
                sb.Append(this.ActionParameters.Name);

                Directory.CreateDirectory(sb.ToString());
                if (this.ActionParameters.ParentNode.TreeView.InvokeRequired)
                {
                    this.ActionParameters.ParentNode.TreeView.Invoke(
                        new Func<TreeNode, int>(this.ActionParameters.ParentNode.Nodes.Add), new object[] { this.Node });
                }
                else
                {
                    this.ActionParameters.ParentNode.Nodes.Add(this.Node);
                }
                this.Node.UpdateDisplay();
                this.ActionParameters.ParentNode.Expand();
                this.Node.TreeView.SelectedNode = this.Node;
                return true;
            }
            return false;
        }

        protected override bool doUndo()
        {
            Directory.Delete(this.m_FolderPath, false);
            if (this.ActionParameters.ParentNode.TreeView.InvokeRequired)
            {
                this.ActionParameters.ParentNode.TreeView.Invoke(
                    new Action<TreeNode>(this.ActionParameters.ParentNode.Nodes.Remove), new object[] { this.Node });
            }
            else
            {
                this.ActionParameters.ParentNode.Nodes.Remove(this.Node);
            }

            return true;
        }

        protected override bool doCommit()
        {
            // add this to the pending wave list
            foreach (var platform in this.ActionParameters.Platforms)
            {
                this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform).RecordAddBankFolder(
                    this.m_parentPath + "\\" + this.ActionParameters.Name);
                this.Node.UpdateDisplayState(EditorTreeNode.NodeDisplayState.AddedInPendingWaveList,
                                          platform,
                                          true,
                                          true);
            }
            return true;
        }

        protected override bool doApply(IPendingWaveList waveList, string platform)
        {
            waveList.RecordAddBankFolder(this.m_parentPath + "\\" + this.ActionParameters.Name);
            return true;
        }


        public override string GetSummary()
        {
            var platforms = new StringBuilder();
            for (var i = 0; i < this.ActionParameters.Platforms.Count; i++)
            {
                platforms.Append(this.ActionParameters.Platforms[i]);
                if (i != this.ActionParameters.Platforms.Count - 1)
                {
                    platforms.Append(",");
                }
            }

            var sb = new StringBuilder();
            sb.Append("new bank folder ");
            sb.Append(this.ActionParameters.Name);
            sb.Append(" in ");
            sb.Append(this.m_parentPath);
            sb.Append(" added on ");
            sb.Append(platforms.ToString());

            return sb.ToString();
        }
    }
}