﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmNodeInfo.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the frmNodeInfo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.WaveBrowser.WaveBrowser.Popups
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml.Linq;

    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    /// The bank info form.
    /// </summary>
    public partial class frmNodeInfo : Form
    {
        /// <summary>
        /// The bank node.
        /// </summary>
        private readonly EditorTreeNode node;

        /// <summary>
        /// Initializes a new instance of the <see cref="frmNodeInfo"/> class.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        public frmNodeInfo(EditorTreeNode node)
        {
            this.InitializeComponent();
            this.node = node;
            this.Init();
        }

        /// <summary>
        /// Initialize the bank info form.
        /// </summary>
        private void Init()
        {
            this.lblNameValue.Text = this.node.Text;

            var sb = new StringBuilder();

            foreach (var ps in Configuration.ActivePlatformSettings)
            {
                sb.AppendLine(string.Format("{0}:", ps.Name));
                var xml = this.node.GetNodeXmlForPlatform(ps.PlatformTag);
                if (xml != null)
                {
                    var buildDate = this.FindAttributeValue(xml, "timeStamp");
                    if (null != buildDate)
                    {
                        sb.AppendFormat("Build date: {0}", DateTime.Parse(buildDate));
                        sb.AppendLine();
                    }

                    var headerSize = this.GetTotalIntAttrValue(xml, "headerSize");
                    sb.AppendFormat("Header size: {0}", headerSize);
                    sb.AppendLine();

                    var builtSize = this.GetTotalIntAttrValue(xml, "builtSize");
                    sb.AppendFormat("Built Size: {0}", builtSize);
                    sb.AppendLine();
                }

                sb.AppendLine();
            }

            this.textBoxBankInfo.Text = sb.ToString();
        }

        /// <summary>
        /// The get total attribute value (integer values only).
        /// If the current element doesn't have the attribute set
        /// it returns the sum of all the same attribute value
        /// of its children (recursively).
        /// </summary>
        /// <param name="elem">
        /// The element.
        /// </param>
        /// <param name="attrName">
        /// The attribute name.
        /// </param>
        /// <returns>
        /// The total attribute value <see cref="int"/>.
        /// </returns>
        private int GetTotalIntAttrValue(XElement elem, string attrName)
        {
            var total = 0;

            var attrValue = this.FindAttributeValue(elem, attrName);
            if (null != attrValue)
            {
                int val;
                if (int.TryParse(attrValue, out val))
                {
                    return val;
                }
            }
            else
            {
                total += elem.Elements().Sum(childElem => this.GetTotalIntAttrValue(childElem, attrName));
            }

            return total;
        }

        /// <summary>
        /// Find the build time stamp.
        /// </summary>
        /// <param name="elem">
        /// The element.
        /// </param>
        /// <param name="attributeName">
        /// The attribute name.
        /// </param>
        /// <returns>
        /// The build time stamp <see cref="DateTime?"/>.
        /// </returns>
        private string FindAttributeValue(XElement elem, string attributeName)
        {
            var attrTimeStamp = elem.Attribute(attributeName);
            if (attrTimeStamp != null)
            {
                return attrTimeStamp.Value;
            }

            return null;
        }
    }
}
