﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rave.Utils;

namespace Rave.WaveBrowser.WaveBrowser.BankBuilding
{
    public class PreviewBuilding
    {


        public static void DeletePreviewFolderContent()
        {
            if (string.IsNullOrEmpty(Configuration.PreviewFolder))
            {
                return;
            }
            DirectoryInfo directoryInfo = new DirectoryInfo(Configuration.PreviewFolder);

            try
            {
                foreach (FileInfo file in directoryInfo.GetFiles())
                {
                    file.IsReadOnly = false;
                    file.Delete();
                }
                foreach (DirectoryInfo dir in directoryInfo.GetDirectories())
                {
                    dir.Delete(true);
                }
            }
            catch
            {
            }
        }
    }
}
