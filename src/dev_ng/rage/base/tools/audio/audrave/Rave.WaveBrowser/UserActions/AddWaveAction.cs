namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Encapsulates the action of a user adding a wave to a bank
    /// </summary>
    public class AddWaveAction : UserAction
    {
        private readonly string m_assetPath;
        private readonly string m_folderPath;
        private readonly string m_parentPath;
        private readonly string m_waveName;
        private readonly string m_wavePath;

        public AddWaveAction(IActionParams parameters)
        {
            this.ActionParameters = parameters;
            this.m_parentPath = this.ActionParameters.ParentNode.GetObjectPath();
            this.m_waveName = Path.GetFileName(this.ActionParameters.SourcePath.ToUpper().Replace(" ", "_"));

            if (!RaveUtils.StringIsValidWaveOrMidiOrGrnOrTxtName(this.m_waveName))
            {
                throw new Exception(this.m_waveName + " in " + this.m_parentPath + " contains invalid characters");
            }

            //asset path
            var sb = new StringBuilder();
            sb.Append(this.m_parentPath);
            sb.Append("\\");
            this.m_assetPath = sb.ToString();

            //folder path
            sb = new StringBuilder();
            sb.Append(Configuration.PlatformWavePath);
            sb.Append(this.m_assetPath);
            this.m_folderPath = sb.ToString();

            sb.Append(this.m_waveName);
            this.m_wavePath = sb.ToString();

            this.m_assetPath += this.m_waveName;
        }


        public override string GetAssetPath()
        {
            return this.m_assetPath;
        }

        protected override bool doAction()
        {
            if (this.ActionParameters.ActionLog != null &&
                this.ActionParameters.ActionLog.SearchForWaveDeletion(this.m_assetPath) != null)
            {
                ErrorManager.HandleInfo("There is a pending delete for \"" + this.m_assetPath +
                                        "\" - you must commit your changes before trying to add this file again");
                return false;
            }

            if (this.Node != null && this.ActionParameters.ParentNode.Nodes.Cast<EditorTreeNode>().Any(node => node.GetObjectName() == this.Node.GetObjectName()))
            {
                return false;
            }

            var bankNode = EditorTreeNode.FindParentBankNode(this.ActionParameters.ParentNode);
            var waves = bankNode.GetChildWaveNodes();

            foreach (WaveNode wn in waves)
            {
                if (Path.GetFileNameWithoutExtension(wn.Text.ToUpper()) ==
                    Path.GetFileNameWithoutExtension(this.m_waveName))
                {
                    ErrorManager.HandleInfo("There is already a file named \"" + Path.GetFileNameWithoutExtension(wn.GetObjectName()) + "\" in this bank [" +
                                            wn.GetObjectPath() + "]");
                    return false;
                }
            }

            if (this.ActionParameters.ParentNode.TryToLock())
            {
                if (this.Node == null)
                {
                    this.Node = new WaveNode(this.ActionParameters.ActionLog, this.m_waveName, this.ActionParameters.WaveBrowser);
                    foreach (var platform in this.ActionParameters.Platforms)
                    {
                        this.Node.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AddedLocally);
                    }
                }

                if (this.ActionParameters.ParentNode.TreeView.InvokeRequired)
                {
                    this.ActionParameters.ParentNode.TreeView.Invoke(new Func<TreeNode, int>(this.ActionParameters.ParentNode.Nodes.Add),
                                                            new object[] { this.Node });
                }
                else
                {
                    this.ActionParameters.ParentNode.Nodes.Add(this.Node);
                }

                this.Node.UpdateDisplay();

                // Expand all parents to make node visible in tree
                TreeNode currParent = this.ActionParameters.ParentNode;
                while (null != currParent)
                {
                    currParent.Expand();
                    currParent = currParent.Parent;
                }

                return true;
            }

            return false;
        }

        protected override bool doUndo()
        {
            if (this.Node == null)
            {
                return false;
            }

            if (this.ActionParameters.ParentNode.TreeView.InvokeRequired)
            {
                this.ActionParameters.ParentNode.TreeView.Invoke(new Action<TreeNode>(this.ActionParameters.ParentNode.Nodes.Remove),
                                                        new object[] { this.Node });
            }
            else
            {
                this.ActionParameters.ParentNode.Nodes.Remove(this.Node);
            }

            return true;
        }

        protected override bool doCommit()
        {
            try
            {
                if (RaveInstance.RaveAssetManager.WaveChangeList == null)
                {
                    RaveInstance.RaveAssetManager.WaveChangeList =
                        RaveInstance.AssetManager.CreateChangeList("[Rave] Wave/Midi Changelist");
                }

                // Check that file hasn't been checked out in another change list
                if (RaveInstance.AssetManager.IsCheckedOut(this.m_wavePath) &&
                    null == RaveInstance.RaveAssetManager.WaveChangeList.GetAsset(this.m_wavePath))
                {
                    ErrorManager.HandleError("File is checked out in another change list - unable to submit: " + this.m_wavePath);
                    return false;
                }

                RaveUtils.CreateRequiredDirectories(this.m_folderPath);

                if (File.Exists(this.m_wavePath))
                {
                    File.SetAttributes(this.m_wavePath, FileAttributes.Normal);
                }
                File.Copy(this.ActionParameters.SourcePath, this.m_wavePath, true);

                if (RaveInstance.AssetManager.ExistsAsAsset(this.m_wavePath))
                {
                    RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(this.m_wavePath, true);
                }
                else
                {
                    RaveInstance.RaveAssetManager.WaveChangeList.MarkAssetForAdd(this.m_wavePath);
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }

            var pendingWaveLists = this.ActionParameters.WaveBrowser.GetPendingWaveLists();
            foreach (var platform in this.ActionParameters.Platforms)
            {
                // add this to the pending wave list
                var pendingWaveList = pendingWaveLists.GetPendingWaveList(platform);
                var wavePath = this.m_parentPath + Path.DirectorySeparatorChar + this.m_waveName;
                pendingWaveList.RecordAddWave(wavePath);
                this.Node.UpdateDisplayState(EditorTreeNode.NodeDisplayState.AddedInPendingWaveList, platform, true, true);
            }
            return true;
        }

        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            try
            {
                RaveUtils.CreateRequiredDirectories(this.m_folderPath);
                
                if (File.Exists(this.m_wavePath))
                {
                    File.SetAttributes(this.m_wavePath, FileAttributes.Normal);
                }
             
                File.Copy(this.ActionParameters.SourcePath, this.m_wavePath, true);
                if (RaveInstance.AssetManager.ExistsAsAsset(this.m_wavePath))
                {
                    RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(this.m_wavePath, false);
                }
               
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }

            var wavePath = this.m_parentPath + Path.DirectorySeparatorChar + this.m_waveName;
            temporaryWaveList.RecordAddWave(wavePath);
            return true;
        }

        public override string GetSummary()
        {
            var platforms = new StringBuilder();
            for (var i = 0; i < this.ActionParameters.Platforms.Count; i++)
            {
                platforms.Append(this.ActionParameters.Platforms[i]);
                if (i != this.ActionParameters.Platforms.Count - 1)
                {
                    platforms.Append(",");
                }
            }

            var sb = new StringBuilder();
            sb.Append("new ");
            sb.Append(Path.GetExtension(this.m_waveName) == ".MID" ? "midi " : "wave ");
            sb.Append(this.m_waveName);
            sb.Append(" in ");
            sb.Append(this.ActionParameters.ParentNode.Text);
            sb.Append(" added on ");
            sb.Append(platforms.ToString());

            return sb.ToString();
        }

    }
}
