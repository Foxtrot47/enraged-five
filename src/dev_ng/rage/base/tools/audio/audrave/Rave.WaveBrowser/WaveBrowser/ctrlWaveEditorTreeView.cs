// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ctrlWaveEditorTreeView.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The wave editor tree view.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Drawing.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media;
using rage.ToolLib.DataStructures;
using Rave.Controls.WPF.UXTimer;
using rage.ToolLib;
namespace Rave.WaveBrowser.WaveBrowser
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.WaveBrowser.WaveBrowser.Popups;

    using rage;
    using rage.ToolLib;

    using Rave.Controls.Forms;
    using Rave.Controls.Forms.Popups;
    using Rave.Instance;
    using Rave.Metadata;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Waves;
    using Rave.Utils;
    using Rave.Utils.Popups;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.UserActions;
    using Rave.WaveBrowser.WaveBrowser.Search;
    using Rave.WaveBrowser.WaveBrowser.TagEditor;

    using Rockstar.AssetManager.Interfaces;

    using SharpDX.DirectSound;

    using Wavelib;
    using System.Security.Permissions;
    
    /// <summary>
    /// The wave editor tree view.
    /// </summary>
    public partial class ctrlWaveEditorTreeView : UserControl, IWaveEditorTreeView
    {
        #region Static Fields

        /// <summary>
        /// The ms_channel extension.
        /// </summary>
        private static readonly string[] ms_channelExtension = new[] { "_LEFT", "_RIGHT" };

        /// <summary>
        /// The m_ speech line process map.
        /// </summary>
        private static Dictionary<string, string> m_SpeechLineProcessMap;

        /// <summary>
        /// The m_ speech line process map was built.
        /// </summary>
        private static bool m_SpeechLineProcessMapWasBuilt;

        /// <summary>
        /// The ms_tag icon map.
        /// </summary>
        private static Hashtable ms_tagIconMap;

        private const String ALL_PLATFORMS = "All Platforms";

        #endregion

        #region Fields

        /// <summary>
        /// The m_audition playback.
        /// </summary>
        private readonly SoundPlayback m_auditionPlayback;

        /// <summary>
        /// The m_action log.
        /// </summary>
        private IActionLog m_actionLog;

        /// <summary>
        /// The m_is finished.
        /// </summary>
        private bool m_isFinished;

        /// <summary>
        /// The m_status text.
        /// </summary>
        private string m_statusText = "Working...";

        private static audProjectSettings m_projectSettings;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ctrlWaveEditorTreeView"/> class.
        /// </summary>
        public ctrlWaveEditorTreeView()
        {
            this.InitializeComponent();

            ms_tagIconMap = new Hashtable();
            this.m_auditionPlayback = new SoundPlayback(this);
            this.ShouldFlattenWavesOnDrag = true;

            m_SpeechLineProcessMap = new Dictionary<string, string>();
            m_SpeechLineProcessMapWasBuilt = false;
            m_projectSettings = Configuration.ProjectSettings;
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The double click node.
        /// </summary>
        public event Action<EditorTreeNode> DoubleClickNode;

        /// <summary>
        /// The find object.
        /// </summary>
        public event Action<IObjectInstance> FindObject;

        /// <summary>
        /// The mouse over node.
        /// </summary>
        public event Action<EditorTreeNode> MouseOverNode;

        public event Action<string> FolderCreated;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether should flatten waves on drag.
        /// </summary>
        public bool ShouldFlattenWavesOnDrag { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The fill speech line process map.
        /// </summary>
        public static void FillSpeechLineProcessMap()
        {
            var assetManager = RaveInstance.AssetManager;

            foreach (string path in m_projectSettings.DialoguePaths)
            {
                string dialoguePath = assetManager.Root + path;
                assetManager.GetLatest(dialoguePath, false);

                foreach (string file in Directory.EnumerateFiles(dialoguePath, "*.dstar"))
                {
                    var doc = new XmlDocument();
                    doc.Load(file);
                    foreach (XmlElement line in doc.SelectNodes("/MissionDialogue/Conversations/Conversation/Lines/Line"))
                    {
                        string wavName = line.GetAttribute("filename");
                        if (wavName.ToUpper().StartsWith("SFX_"))
                        {
                            continue;
                        }

                        string processType = line.GetAttribute("special");

                        if (m_SpeechLineProcessMap.ContainsKey(wavName.ToUpper()))
                        {
                            // throw new Exception("Found two dialogue lines with the name " + wavName + " while loading the SpeechLineProcessMap.");
                            continue;
                        }

                        m_SpeechLineProcessMap.Add(wavName.ToUpper(), processType.ToUpper());
                    }
                }
            }

            m_SpeechLineProcessMapWasBuilt = true;
        }

        /// <summary>
        /// The add wave node.
        /// </summary>
        /// <param name="path">
        /// The path to add the wave node to.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        public void AddWaveNode(string path, EditorTreeNode node)
        {
            this.m_treeView.BeginUpdate();

            var parent = this.FindNode(path, null);
            if (null != parent)
            {
                parent.Nodes.Add(node);
                var treeNode = this.FindNode(path, Path.GetFileNameWithoutExtension(node.GetObjectName()));
                this.ShowNode(treeNode);
            }
            else
            {
                ErrorManager.HandleInfo("Failed to find path to add wave node to: " + path);
            }

            this.m_treeView.EndUpdate();
        }

        /// <summary>
        /// The audition wave.
        /// </summary>
        /// <param name="bankPath">
        /// The bank path.
        /// </param>
        /// <param name="waveName">
        /// The wave name.
        /// </param>
        public void AuditionWave(string bankPath, string waveName)
        {
            if (string.IsNullOrEmpty(bankPath) || string.IsNullOrEmpty(waveName))
            {
                return;
            }

            // passed the the pack/bank as path, path ignores
            // wave folders etc.
            var pathElement = bankPath.Split('\\');
            var packName = pathElement[0];
            var bankName = pathElement[1];
            EditorTreeNode node = null;
            GetNode(this.m_treeView.Nodes, packName, bankName, waveName, ref node);
            var waveNode = node as WaveNode;
            if (waveNode != null)
            {
                this.AuditionNode(waveNode);
            }
        }

        /// <summary>
        /// The delete node.
        /// </summary>
        /// <param name="n">
        /// The n.
        /// </param>
        public void DeleteNode(EditorTreeNode n)
        {
            this.m_treeView.BeginUpdate();
            var t = new Thread(this.StartStatusBar);
            t.Start();
            var temp = new List<string>();
            temp.AddRange(n.GetPlatforms());

            HashSet<TreeNode> deletedNodes;
            if (this.CanDeleteNode(n, temp) && this.DeleteChildren(n, temp, out deletedNodes))
            {
                this.DeleteNode(n, temp);
            }

            this.m_isFinished = true;
            this.m_treeView.EndUpdate();
        }

        /// <summary>
        /// The delete wave node.
        /// </summary>
        /// <param name="parentPath">
        /// The path to the parent node.
        /// </param>
        /// <param name="waveName">
        /// The wave name.
        /// </param>
        /// <param name="skipReferenceCheck">
        /// The skip reference check flag.
        /// </param>
        /// <returns>
        /// Value indicating success <see cref="bool"/>.
        /// </returns>
        public bool DeleteWaveNode(string parentPath, string waveName, bool skipReferenceCheck = false)
        {
            EditorTreeNode node = this.FindNode(parentPath, waveName);

            // Can only delete if node is wave node and it exists
            if (null == node)
            {
                ErrorManager.HandleError("Unable to locate wave node to delete: " + parentPath + "\\" + waveName);
                return false;
            }

            if (node.GetType() != typeof(WaveNode))
            {
                ErrorManager.HandleError("Node is not of type 'WaveNode': " + parentPath + "\\" + waveName);
                return false;
            }

            this.m_treeView.BeginUpdate();

            // Wave nodes have no children, so no need to check/delete these
            var temp = new List<string>();
            var removed = true;

            if (skipReferenceCheck || this.CanDeleteNode(node, temp))
            {
                node.Remove();
            }
            else
            {
                removed = false;
            }

            this.m_treeView.EndUpdate();

            return removed;
        }

        /// <summary>
        /// The find node.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="waveName">
        /// The wave name.
        /// </param>
        /// <returns>
        /// The <see cref="EditorTreeNode"/>.
        /// </returns>
        public EditorTreeNode FindNode(string path, string waveName)
        {
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }

            // passed the the pack/bank as path, path ignores
            // wave folders etc.
            var pathElement = path.Split('\\');
            EditorTreeNode node = null;
            GetNode(this.m_treeView.Nodes, pathElement, waveName, ref node);
            return node;
        }

        /// <summary>
        /// The find node with bank path.
        /// </summary>
        /// <param name="bankPath">
        /// The bank path.
        /// </param>
        /// <param name="waveName">
        /// The wave name.
        /// </param>
        /// <returns>
        /// The <see cref="EditorTreeNode"/>.
        /// </returns>
        public EditorTreeNode FindNodeWithBankPath(string bankPath, string waveName)
        {
            // passed the the pack/bank as path, path ignores
            // wave folders etc.
            var pathElement = bankPath.Split('\\');
            var packName = pathElement[0];
            var bankName = pathElement[1];
            EditorTreeNode node = null;
            GetNode(this.m_treeView.Nodes, packName, bankName, waveName, ref node);

            int num = 2;
            while (node == null && num < pathElement.Length)
            {
                GetNode(this.m_treeView.Nodes, packName, pathElement[num], waveName, ref node);
                num++;
            }
            return node;
        }

        /// <summary>
        /// The get icon index.
        /// </summary>
        /// <param name="objName">
        /// The obj name.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetIconIndex(string objName)
        {
            if (ms_tagIconMap.ContainsKey(objName))
            {
                return (int)ms_tagIconMap[objName];
            }

            return 0;
        }

        /// <summary>
        /// The get nodes.
        /// </summary>
        /// <returns>
        /// The <see cref="TreeNodeCollection"/>.
        /// </returns>
        public TreeNodeCollection GetNodes()
        {
            return this.m_treeView.Nodes;
        }

        /// <summary>
        /// The get selected wave nodes.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="skipCustomMetadata">
        /// The skip custom metadata.
        /// </param>
        /// <param name="nodeList">
        /// The node list.
        /// </param>
        /// <param name="speechOnly">
        /// The speech only.
        /// </param>
        public void GetSelectedWaveNodes(TreeNode node, bool skipCustomMetadata, IList<WaveNode> nodeList,
            bool speechOnly)
        {
            if (speechOnly)
            {
                var packNode = node as PackNode;
                if (packNode != null)
                {
                    //TODO read exclusion criteria from projectSettings
                    var packNodePath = packNode.GetObjectPath().ToUpper();
                    if (!packNodePath.Contains("SCRIPTED_SPEECH") && !packNodePath.StartsWith("SS_") &&
                        !packNodePath.StartsWith("TEST_DLC") &&
                        !packNodePath.StartsWith("DLC"))
                    {
                        return;
                    }
                }
            }

            // for each of the nodes children, recurse into the tree and 
            // find their respective wave nodes
            for (int i = 0; i < node.Nodes.Count; ++i)
            {
                this.GetSelectedWaveNodes(node.Nodes[i], skipCustomMetadata, nodeList, speechOnly);
            }

            // we only want to save WaveNodes
            var waveNode = node as WaveNode;
            if (waveNode != null && waveNode.Type == WaveNodeType.Wave)
            {
                bool bHasCustomMetadata = false;

                // check if display state in any platform contains custom metadata
                foreach (string s in waveNode.GetPlatforms())
                {
                    if ((int)waveNode.GetNodeDisplayState(s) > 5)
                    {
                        bHasCustomMetadata = true;
                        break;
                    }
                }

                // this is horribly slow: o(n)
                if (!skipCustomMetadata && !nodeList.Contains(waveNode))
                {
                    nodeList.Add(waveNode);
                }
                else if (skipCustomMetadata && !bHasCustomMetadata && !nodeList.Contains(waveNode))
                {
                    nodeList.Add(waveNode);
                }
            }
        }


        /// <summary>
        /// The get tree view.
        /// </summary>
        /// <returns>
        /// The <see cref="ScrollableTreeView"/>.
        /// </returns>
        public TreeView GetTreeView()
        {
            return this.m_treeView;
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        public void Init(ctrlWaveBrowser waveBrowser, IActionLog actionLog)
        {
            this.m_waveBrowser = waveBrowser;
            this.m_actionLog = actionLog;
            RaveUtils.LoadTagIcons(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "icons/tags/"), ms_tagIconMap,
                this.iconList);
            foreach (PlatformSetting ps in Configuration.ActivePlatformSettings)
            {
                this.mnuPlatformSpecific.DropDownItems.Add(
                    new ToolStripMenuItem(ps.PlatformTag, null, this.MakePlatformSpecific));
            }

            var buildSet = new audBuildSet { Name = "Wave Build" };
            buildSet.BuildElements.Add(new audBuildElement(audBuildElementType.Waves));

            var allPlatforms = new ToolStripMenuItem(ALL_PLATFORMS, null, this.mnuBuildPack_Click) { Tag = buildSet };
            this.mnuBuildPack.DropDownItems.Add(allPlatforms);
            foreach (PlatformSetting ps in Configuration.ActivePlatformSettings)
            {
                if (ps.BuildEnabled)
                {
                    var platformItem = new ToolStripMenuItem(ps.PlatformTag, null, this.mnuBuildPack_Click)
                    {
                        Tag =
                            buildSet
                    };
                    this.mnuBuildPack.DropDownItems.Add(platformItem);
                }
            }

            var allPlatformBank = new ToolStripMenuItem(ALL_PLATFORMS, null, this.mnuBuildBankPreview_Click) { Tag = buildSet };

            if (!string.IsNullOrEmpty(Configuration.PreviewFolder))
            {
                this.mnuBuildBankPreview.DropDownItems.Add(allPlatformBank);
                foreach (PlatformSetting ps in Configuration.ActivePlatformSettings)
                {
                    if (ps.BuildEnabled)
                    {
                        var platformItem = new ToolStripMenuItem(ps.PlatformTag, null, this.mnuBuildBankPreview_Click)
                        {
                            Tag =
                                buildSet
                        };
                        this.mnuBuildBankPreview.DropDownItems.Add(platformItem);
                    }
                }
            }

            this.PopulatePresetMenu();
        }

        /// <summary>
        /// The load wave lists.
        /// </summary>
        /// <param name="runMetadataBuild">
        /// Run metadata build flag.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool LoadWaveLists(bool runMetadataBuild = true)
        {
            if (this.m_waveBrowser == null)
            {
                throw new Exception("Wave Browser not found");
            }

            if (this.m_actionLog.GetCommitLogCount() > 0)
            {
                return false;
            }

            var waveSlotFile = Configuration.WaveSlotFile;

            // Ensure we have the latest waveslotsettings file before running the generation steps
            RaveInstance.AssetManager.GetLatest(waveSlotFile, false);
            if (!this.m_waveBrowser.GetPendingWaveLists().LoadLatestWaveLists())
            {
                ErrorManager.HandleInfo("Couldn't load wave lists - check asset manager configuration.");
                return false;
            }

            if (runMetadataBuild)
            {
                MetadataUtils.RunMetadataGeneration(true);
            }

            // disable drawing the tree as we update it (speeds things up quite a bit)
            this.m_treeView.BeginUpdate();

            // delete all nodes in there just now
            var nodesToDelete = new ArrayList();
            foreach (TreeNode t in this.m_treeView.Nodes)
            {
                nodesToDelete.Add(t);
            }

            foreach (TreeNode t in nodesToDelete)
            {
                this.m_treeView.Nodes.Remove(t);
            }

            this.m_waveBrowser.GetPendingWaveLists().ShowBuiltWaves(
                this.m_treeView, this.m_actionLog);

            this.m_waveBrowser.GetPendingWaveLists().ShowPendingChanges(
                this.m_treeView, this.m_actionLog);

            this.m_treeView.EndUpdate();

            if (runMetadataBuild)
            {
                using (var busy = new frmBusy())
                {
                    int metadataFileCount = 0;
                    busy.SetStatusText("Run Metabuild...");
                    busy.Show();
                    while (MetadataUtils.PendingMetadataFiles.Count > 0)
                    {
                        if (metadataFileCount == 0) metadataFileCount = MetadataUtils.PendingMetadataFiles.Count;
                        if (metadataFileCount > 0)
                            busy.SetProgress(100 - (100 * MetadataUtils.PendingMetadataFiles.Count / metadataFileCount));
                        else busy.SetProgress(100);

                        Thread.Sleep(12);

                    }

                    int objectBankCount = 0;
                    busy.SetStatusText("Load Objectbanks...");
                    foreach (var entry in RaveInstance.AllBankManagers)
                    {
                        objectBankCount++;
                        busy.SetProgress(100 * objectBankCount / RaveInstance.AllBankManagers.Count);
                        var objectBanks = entry.Value;
                        foreach (var bank in objectBanks.ObjectBanks)
                        {
                            if (bank.IsAutoGenerated)
                            {
                                bank.LoadSavedVersion();
                            }
                        }
                    }
                    busy.FinishedWorking();
                }
            }

            return true;
        }

        /// <summary>
        /// Load pack child nodes.
        /// </summary>
        /// <param name="packNameToLoad">
        /// The pack name.
        /// </param>
        public void LoadPackChildNodes(string packNameToLoad)
        {
            this.Cursor = Cursors.WaitCursor;

            var packsToLoad = new HashSet<string> { packNameToLoad };

            var matchingGroups = new List<IList<string>>();
            foreach (var wavePackGroup in Configuration.ProjectSettings.WavePackGroups)
            {
                if (!wavePackGroup.Any(item => Regex.IsMatch(packNameToLoad, item, RegexOptions.IgnoreCase)))
                {
                    continue;
                }

                matchingGroups.Add(wavePackGroup);
                break;
            }

            foreach (var group in matchingGroups)
            {
                foreach (var packNode in this.GetPackNodes())
                {
                    if (packNode.IsLoaded)
                    {
                        continue;
                    }

                    var node = packNode;
                    foreach (
                        var item in @group.Where(item => Regex.IsMatch(node.Text, item, RegexOptions.IgnoreCase)))
                    {
                        packsToLoad.Add(packNode.PackName);
                    }
                }
            }

            this.m_treeView.BeginUpdate();
            foreach (var packName in packsToLoad)
            {
                this.m_waveBrowser.GetPendingWaveLists().LoadPack(packName, m_waveBrowser);
                this.m_waveBrowser.GetPendingWaveLists()
                    .ShowBuiltWavesOfPack(packName, this.m_treeView, this.m_actionLog);
            }
            this.m_waveBrowser.GetPendingWaveLists().ShowPendingChanges(this.m_treeView, this.m_actionLog);

            this.m_treeView.EndUpdate();
            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// The make platform specific.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        public void MakePlatformSpecific(object sender, EventArgs e)
        {
            var menuItem = sender as ToolStripMenuItem;
            string specificPlatform = menuItem.Text;
            var etn = this.m_treeView.LastSelectedNode as EditorTreeNode;

            EditorTreeNode.AllowAutoLock = true;
            this.m_treeView.BeginUpdate();

            // Check Children for platform specific-ness, performed here rather than at menu level to save time            
            if (CanMakePlatformSpecific(etn, specificPlatform))
            {
                var t = new Thread(this.StartStatusBar);
                t.Start();

                this.m_statusText = "Making " + etn.GetObjectName() + " " + specificPlatform + " specific";

                // build up list of platforms to be deleted
                List<string> toDelete = etn.GetPlatforms().Where(platform => platform != specificPlatform).ToList();
                HashSet<TreeNode> deletedNodes;
                if (this.DeleteChildren(etn, toDelete, out deletedNodes))
                {
                    this.DeleteNode(etn, toDelete);
                }

                this.m_statusText = "Finished";
                this.m_isFinished = true;
            }
            else
            {
                ErrorManager.HandleInfo(
                    "Cannot make platform specific as node has a child specific to a different platform");
            }

            this.m_treeView.EndUpdate();
            EditorTreeNode.AllowAutoLock = false;
        }

        /// <summary>
        /// The on active platform changed.
        /// </summary>
        public void OnActivePlatformChanged()
        {
            UpdateDisplayStates(this.m_treeView.Nodes);
        }

        /// <summary>
        /// The populate plugin menu.
        /// </summary>
        /// <param name="pluginManager">
        /// The plugin manager.
        /// </param>
        public void PopulatePluginMenu()
        {
            // add plugins to right click pop up menu
            foreach (var plugin in RaveInstance.PluginManager.WaveBrowserPlugins)
            {
                var menu = new ToolStripMenuItem(plugin.GetName(), null, this.mnuPlugin_Click);
                this.mnuPlugins.DropDownItems.Add(menu);
            }
        }

        /// <summary>
        /// The select node.
        /// </summary>
        /// <param name="bankPath">
        /// The bank path.
        /// </param>
        /// <param name="waveName">
        /// The wave name.
        /// </param>
        public void SelectNode(string bankPath, string waveName)
        {
            var split = bankPath.Split('\\');

            if (!split.Any())
            {
                return;
            }

            var packName = split[0];

            if (!this.TryToLoadPack(packName))
            {
                return;
            }

            var node = this.FindNodeWithBankPath(bankPath, waveName);
            if (node == null)
            {
                return;
            }

            this.m_treeView.BeginUpdate();
            node.EnsureVisible();
            this.m_treeView.LastSelectedNode = node;
            this.m_treeView.EndUpdate();
        }

        /// <summary>
        /// Try to load pack.
        /// </summary>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool TryToLoadPack(string packName)
        {
            return this.TryToLoadPack(packName, true);
        }

        /// <summary>
        /// Try to load pack.
        /// </summary>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool TryToLoadPack(string packName, bool question)
        {
            var packNameUpper = packName.ToUpper();

            foreach (var node in this.m_treeView.Nodes)
            {
                var packNode = node as PackNode;
                if (null == packNode)
                {
                    continue;
                }

                if (packNode.GetObjectName().ToUpper() != packNameUpper)
                {
                    continue;
                }

                if (packNode.IsLoaded)
                {
                    return true;
                }

                if (question)
                {
                    if (MessageBox.Show(
                        string.Format("Load {0} pack?", packName),
                        "Pack Load Required",
                        MessageBoxButtons.OKCancel,
                        MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        this.LoadPackChildNodes(packName);
                        return true;
                    }
                }
                else
                {
                    this.LoadPackChildNodes(packName);
                    return true;
                }
            }

            if (question)
            {
                ErrorManager.HandleInfo(string.Format("Failed to find pack {0}", packName), true);
            }
            return false;
        }
        /// <summary>
        /// Get the pack nodes.
        /// </summary>
        /// <returns>
        /// The list of pack nodes <see cref="IList"/>.
        /// </returns>
        public IList<PackNode> GetPackNodes()
        {
            return this.m_treeView.Nodes.OfType<PackNode>().ToList();
        }

        /// <summary>
        /// The set selected node.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        public void SetSelectedNode(EditorTreeNode node)
        {
            this.m_treeView.LastSelectedNode = node;
        }

        /// <summary>
        /// The set text.
        /// </summary>
        /// <param name="statusText">
        /// The status text.
        /// </param>
        public void SetText(string statusText)
        {
            this.m_statusText = statusText;
        }

        /// <summary>
        /// The show node.
        /// </summary>
        /// <param name="etn">
        /// The etn.
        /// </param>
        public void ShowNode(EditorTreeNode etn)
        {
            this.m_treeView.SelectedNode = etn;
        }

        /// <summary>
        /// The speech to be processed.
        /// </summary>
        /// <param name="s">
        /// The s.
        /// </param>
        /// <param name="mission">
        /// The mission.
        /// </param>
        /// <param name="character">
        /// The character.
        /// </param>
        /// <param name="includePlacholder">
        /// The include placholder.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        public void SpeechToBeProcessed(
            string s, string mission, string character, bool includePlacholder, bool searchConversationRoots, out IList<WaveNode> results)
        {
            results = new List<WaveNode>();

            if (!m_SpeechLineProcessMapWasBuilt)
            {
                FillSpeechLineProcessMap();
            }

            var waveList = new List<WaveNode>();
            s = s.ToUpper();
            foreach (TreeNode n in this.m_treeView.Nodes)
            {
                this.GetSelectedWaveNodes(n, false, waveList, true);
            }

            if (searchConversationRoots)
            {
                mission = mission.ToUpper();
                IAssetManager assetManager = RaveInstance.AssetManager;
                foreach (string dialoguePath in m_projectSettings.DialoguePaths)
                {
                    string path = assetManager.Root + dialoguePath;

                    foreach (string file in Directory.EnumerateFiles(path, "*.dstar"))
                    {
                        var doc = new XmlDocument();
                        doc.Load(file);
                        foreach (XmlElement conversation in doc.SelectNodes("/MissionDialogue/Conversations/Conversation"))
                        {
                            string rootName = conversation.GetAttribute("root");
                            if (rootName.ToUpper() == mission)
                            {
                                foreach (XmlElement line in conversation.SelectNodes("Lines/Line"))
                                {
                                    string fileName = line.GetAttribute("filename").ToUpper();
                                    string fileNameNoVar = fileName.Substring(0, fileName.Length - 3);
                                    foreach (WaveNode node in waveList)
                                    {
                                        string wavePath = Configuration.PlatformWavePath + node.GetObjectPath();
                                        string waveName = node.GetObjectName();
                                        if (waveName.Contains(".processed")
                                            || (!includePlacholder && wavePath.ToUpper().Contains("PLACEHOLDER")))
                                        {
                                            continue;
                                        }

                                        int waveNameLength = waveName.Length;

                                        // minus 7 for _xx.wav
                                        string waveNameNoExtension = waveName.Substring(0, waveNameLength - 7).ToUpper();
                                        if ((waveNameNoExtension == fileName || waveNameNoExtension == fileNameNoVar)
                                            && !results.Contains(node))
                                        {
                                            results.Add(node);
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                foreach (WaveNode node in waveList)
                {
                    string wavePath = Configuration.PlatformWavePath + node.GetObjectPath();
                    string waveName = node.GetObjectName();
                    if (waveName.Contains(".processed")
                        || (!includePlacholder && wavePath.ToUpper().Contains("PLACEHOLDER")))
                    {
                        continue;
                    }

                    int waveNameLength = waveName.Length;

                    // minus 7 for _xx.wav
                    string waveNameNoExtension = waveName.Substring(0, waveNameLength - 7).ToUpper();
                    if (m_SpeechLineProcessMap.ContainsKey(waveNameNoExtension)
                        && m_SpeechLineProcessMap[waveNameNoExtension] == s
                        && (string.IsNullOrEmpty(mission) || wavePath.ToUpper().Contains(mission.ToUpper()))
                        && (string.IsNullOrEmpty(character) || wavePath.ToUpper().Contains(character.ToUpper())))
                    {
                        results.Add(node);
                    }
                }
            }
        }

        /// <summary>
        /// The speech to be processed_ out of date.
        /// </summary>
        /// <param name="s">
        /// The s.
        /// </param>
        /// <param name="mission">
        /// The mission.
        /// </param>
        /// <param name="character">
        /// The character.
        /// </param>
        /// <param name="includePlacholder">
        /// The include placholder.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        public void SpeechToBeProcessed_OutOfDate(
            string s, string mission, string character, bool includePlacholder, bool searchConversationRoots, out IList<WaveNode> results)
        {
            results = new List<WaveNode>();

            if (!m_SpeechLineProcessMapWasBuilt)
            {
                FillSpeechLineProcessMap();
            }

            var waveList = new List<WaveNode>();
            s = s.ToUpper();
            foreach (TreeNode n in this.m_treeView.Nodes)
            {
                this.GetSelectedWaveNodes(n, false, waveList, true);
            }

            if (searchConversationRoots)
            {
                mission = mission.ToUpper();
                IAssetManager assetManager = RaveInstance.AssetManager;
                foreach (string dialoguePath in m_projectSettings.DialoguePaths)
                {
                    string path = assetManager.Root + dialoguePath;
                    foreach (string file in Directory.EnumerateFiles(path, "*.dstar"))
                    {
                        var doc = new XmlDocument();
                        doc.Load(file);
                        foreach (
                            XmlElement conversation in doc.SelectNodes("/MissionDialogue/Conversations/Conversation"))
                        {
                            string rootName = conversation.GetAttribute("root");
                            if (rootName.ToUpper() == mission)
                            {
                                foreach (XmlElement line in conversation.SelectNodes("Lines/Line"))
                                {
                                    string fileName = line.GetAttribute("filename").ToUpper();
                                    string fileNameNoVar = fileName.Substring(0, fileName.Length - 3);
                                    foreach (WaveNode node in waveList)
                                    {
                                        string wavePath = Configuration.PlatformWavePath + node.GetObjectPath();
                                        string waveName = node.GetObjectName();
                                        if (waveName.ToLower().Contains(".processed")
                                            || (!includePlacholder && wavePath.ToUpper().Contains("PLACEHOLDER")))
                                        {
                                            continue;
                                        }

                                        int waveNameLength = waveName.Length;
                                        int wavePathLength = wavePath.Length;

                                        // minus 7 for _xx.wav
                                        string waveNameNoExtension = waveName.Substring(0, waveNameLength - 7).ToUpper();

                                        // only 4 for .wav, we need to retain variation number
                                        string wavePathNoExtension = wavePath.Substring(0, wavePathLength - 4).ToUpper();
                                        string wavePathProcessed = wavePathNoExtension + ".PROCESSED.WAV";
                                        if ((waveNameNoExtension == fileName || waveNameNoExtension == fileNameNoVar)
                                            && !results.Contains(node))
                                        {
                                            // if there isn't a processed version, add this wave to the list
                                            if (!RaveInstance.AssetManager.ExistsAsAsset(wavePathProcessed))
                                            {
                                                results.Add(node);
                                            }

                                                // Otherwise, make sure the processed wavs timestamp is older
                                            else
                                            {
                                                DateTime processedStamp =
                                                    RaveInstance.AssetManager.GetLatestCheckinTime(wavePathProcessed);
                                                DateTime unprocessedStamp =
                                                    RaveInstance.AssetManager.GetLatestCheckinTime(wavePath);
                                                if (processedStamp.CompareTo(unprocessedStamp) < 0)
                                                {
                                                    results.Add(node);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                foreach (WaveNode node in waveList)
                {
                    string wavePath = Configuration.PlatformWavePath + node.GetObjectPath();
                    string waveName = node.GetObjectName();
                    if (waveName.ToLower().Contains(".processed")
                        || (!includePlacholder && wavePath.ToUpper().Contains("PLACEHOLDER")))
                    {
                        continue;
                    }

                    int waveNameLength = waveName.Length;
                    int wavePathLength = wavePath.Length;

                    // minus 7 for _xx.wav
                    string waveNameNoExtension = waveName.Substring(0, waveNameLength - 7).ToUpper();

                    // only 4 for .wav, we need to retain variation number
                    string wavePathNoExtension = wavePath.Substring(0, wavePathLength - 4).ToUpper();
                    string wavePathProcessed = wavePathNoExtension + ".PROCESSED.WAV";
                    if (m_SpeechLineProcessMap.ContainsKey(waveNameNoExtension)
                        && m_SpeechLineProcessMap[waveNameNoExtension] == s
                        && (string.IsNullOrEmpty(mission) || wavePath.ToUpper().Contains(mission.ToUpper()))
                        && (string.IsNullOrEmpty(character) || wavePath.ToUpper().Contains(character.ToUpper())))
                    {
                        // if there isn't a processed version, add this wave to the list
                        if (!RaveInstance.AssetManager.ExistsAsAsset(wavePathProcessed))
                        {
                            results.Add(node);
                        }

                            // Otherwise, make sure the processed wavs timestamp is older
                        else
                        {
                            DateTime processedStamp = RaveInstance.AssetManager.GetLatestCheckinTime(wavePathProcessed);
                            DateTime unprocessedStamp = RaveInstance.AssetManager.GetLatestCheckinTime(wavePath);
                            if (processedStamp.CompareTo(unprocessedStamp) < 0)
                            {
                                results.Add(node);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The unlock all nodes.
        /// </summary>
        public void UnlockAllNodes()
        {
            this.m_treeView.BeginUpdate();
            UnlockAllNodes(this.m_treeView.Nodes);
            this.m_treeView.EndUpdate();
        }

        /// <summary>
        /// The waves contain string.
        /// </summary>
        /// <param name="searchString">
        /// The search string.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        public void WavesContainString(string searchString, out IList<WaveNode> results)
        {
            var waveList = new List<WaveNode>();
            searchString = searchString.ToUpper();

            foreach (TreeNode node in this.m_treeView.Nodes)
            {
                this.GetSelectedWaveNodes(node, false, waveList, false);
            }

            results = (from wave in waveList
                       where wave.GetObjectName().ToUpper().Contains(searchString)
                       select wave).ToList();
        }

        /// <summary>
        /// The waves in folder.
        /// </summary>
        /// <param name="folderName">
        /// The folder name.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        public void WavesInFolder(string folderName, out IList<WaveNode> results)
        {
            results = new List<WaveNode>();

            foreach (EditorTreeNode node in this.m_treeView.Nodes)
            {
                GetAllWaveFolders(node, folderName, results);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The can make platform specific.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool CanMakePlatformSpecific(EditorTreeNode node, string platform)
        {
            if (!node.GetPlatforms().Contains(platform))
            {
                return false;
            }

            return node.Nodes.Cast<EditorTreeNode>().All(child => CanMakePlatformSpecific(child, platform));
        }

        /// <summary>
        /// The equal collections.
        /// </summary>
        /// <param name="x">
        /// The x.
        /// </param>
        /// <param name="y">
        /// The y.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool EqualCollections(ICollection<string> x, ICollection<string> y)
        {
            return x.All(y.Contains) && y.All(x.Contains);
        }

        /// <summary>
        /// The get all wave folders.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="folderName">
        /// The folder name.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        private static void GetAllWaveFolders(TreeNode node, string folderName, IList<WaveNode> results)
        {
            foreach (EditorTreeNode etn in node.Nodes)
            {
                var wfn = etn as WaveFolderNode;
                if (wfn != null)
                {
                    if (wfn.GetObjectName() == folderName)
                    {
                        foreach (WaveNode childWaveNode in wfn.GetChildWaveNodes())
                        {
                            results.Add(childWaveNode);
                        }
                    }
                }
                else
                {
                    GetAllWaveFolders(etn, folderName, results);
                }
            }
        }

        /// <summary>
        /// The get nodes.
        /// </summary>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <param name="bankName">
        /// The bank name.
        /// </param>
        /// <param name="waveName">
        /// The wave name.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        private static void GetNode(
            TreeNodeCollection treeNodes, string packName, string bankName, string waveName, ref EditorTreeNode node)
        {
            foreach (TreeNode tn in treeNodes)
            {
                var pack = tn as PackNode;
                var bank = tn as BankNode;

                if (pack != null && string.Compare(pack.GetObjectName(), packName, true) == 0)
                {
                    GetNode(tn.Nodes, null, bankName, waveName, ref node);
                }
                else if (bank != null && string.Compare(bank.GetObjectName(), bankName, true) == 0)
                {
                    if (!string.IsNullOrEmpty(waveName))
                    {
                        foreach (WaveNode wn in bank.GetChildWaveNodes())
                        {
                            if (string.Compare(Path.GetFileNameWithoutExtension(wn.GetObjectName()), waveName, true)
                                == 0)
                            {
                                node = wn;
                                break;
                            }
                        }
                    }
                    else
                    {
                        node = bank;
                        break;
                    }
                }

                GetNode(tn.Nodes, packName, bankName, waveName, ref node);
            }
        }

        /// <summary>
        /// The get nodes.
        /// </summary>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        /// <param name="path">
        /// The path to find.
        /// </param>
        /// <param name="waveName">
        /// The wave name.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        private static void GetNode(
            TreeNodeCollection treeNodes, IList<string> path, string waveName, ref EditorTreeNode node)
        {
            if (!path.Any())
            {
                return;
            }

            foreach (EditorTreeNode treeNode in treeNodes)
            {
                if (String.Compare(treeNode.Text, path[0], StringComparison.OrdinalIgnoreCase) != 0)
                {
                    continue;
                }

                if (path.Count > 1)
                {
                    var subPath = new List<string>(path);
                    subPath.RemoveAt(0);
                    GetNode(treeNode.Nodes, subPath, waveName, ref node);
                }
                else
                {
                    if (!string.IsNullOrEmpty(waveName))
                    {

                        foreach (EditorTreeNode waveNode in treeNode.Nodes)
                        {
                            if (string.Compare(
                                Path.GetFileNameWithoutExtension(waveNode.GetObjectName()), waveName, true) == 0
                                &&
                                waveNode is WaveNode)
                            {
                                node = waveNode;
                                break;
                            }
                        }

                        //var bankNode = EditorTreeNode.FindParentBankNode(treeNode);
                        //if (null != bankNode)
                        //{
                        //    foreach (WaveNode waveNode in bankNode.GetChildWaveNodes())
                        //    {
                        //        if (string.Compare(
                        //            Path.GetFileNameWithoutExtension(waveNode.GetObjectName()), waveName, true) != 0 
                        //            ||
                        //            string.Compare(waveNode.GetObjectName(), path[0], true) != 0)
                        //        {
                        //            continue;
                        //        }

                        //        node = waveNode;
                        //        break;
                        //    }
                        //}
                    }
                    else
                    {
                        node = treeNode;
                    }
                }
            }
        }

        /// <summary>
        /// The unlock all nodes.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        private static void UnlockAllNodes(TreeNodeCollection nodes)
        {
            foreach (EditorTreeNode n in nodes)
            {
                UnlockAllNodes(n.Nodes);
                n.Unlock();
                n.UpdateDisplay();
            }
        }

        /// <summary>
        /// The update display states.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        private static void UpdateDisplayStates(TreeNodeCollection nodes)
        {
            foreach (EditorTreeNode n in nodes)
            {
                n.UpdateDisplay();
                UpdateDisplayStates(n.Nodes);
            }
        }

        /// <summary>
        /// The audition node.
        /// </summary>
        /// <param name="wave">
        /// The wave.
        /// </param>
        private void AuditionNode(WaveNode wave)
        {
            if (wave != null)
            {
                string localPath = Path.Combine(Configuration.PlatformWavePath, wave.GetObjectPath());

                try
                {
                    RaveInstance.AssetManager.GetLatestForceErrors(localPath);
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                    return;
                }

                // Need to check if file exists in case GetLatest
                // failed and no existing localy copy exists
                if (File.Exists(localPath))
                {
                    var waveFile = new bwWaveFile(localPath, false);
                    this.m_auditionPlayback.Stop();
                    this.m_auditionPlayback.CreateSound(waveFile);
                    this.m_auditionPlayback.Play(0, PlayFlags.None);
                }
                else
                {
                    ErrorManager.HandleError("Failed to find local wave file: " + localPath);
                }
            }
        }

        /// <summary>
        /// The audition wave node.
        /// </summary>
        private void AuditionWaveNode()
        {
            var wave = (WaveNode)this.m_treeView.LastSelectedNode;
            this.AuditionNode(wave);
        }

        /// <summary>
        /// The can delete node.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="platforms">
        /// The platforms.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool CanDeleteNode(TreeNode node, ICollection<string> platforms)
        {
            if (EqualCollections(((EditorTreeNode)node).GetPlatforms(), platforms))
            {
                // all platform are being deleted so check for waves etc referenced in sounds, waveslots...
                if (node.GetType() == typeof(WaveNode))
                {
                    string bankPath = ((WaveNode)node).GetBankPath();
                    string pack = bankPath.Substring(0, bankPath.IndexOf("\\"));
                    bool skip = false;

                    if (Configuration.WaveRefsToSkip() != null)
                    {
                        skip = Configuration.WaveRefsToSkip().Any(s => pack.Contains(s.ToUpper()));
                    }

                    string key = ((WaveNode)node).GetBankPath() + "\\" + ((WaveNode)node).GetWaveName();
                    if (!skip && WaveManager.WaveRefs.ContainsKey(key) && WaveManager.WaveRefs[key].Count > 0)
                    {
                        // don't worry about references if they are live in autogenerated stores
                        if (WaveManager.WaveRefs[key].Any(obj => !obj.Bank.IsAutoGenerated))
                        {
                            var msg = new frmSimpleOutput("WaveNode Referencers");
                            msg.OnSelectObject += this.msg_OnSelectObject;
                            msg.DisplayInfo(
                                "Cannot delete wave " + ((WaveNode)node).GetObjectName()
                                + ". The wave is referenced by the following sounds:");

                            foreach (IObjectInstance obj in WaveManager.WaveRefs[key])
                            {
                                msg.AddObject(obj);
                            }

                            msg.Show(RaveInstance.ActiveWindow);
                            return false;
                        }
                    }
                }
                else if (node.GetType() == typeof(BankNode))
                {
                    string key;

                    if (node.Parent.GetType() == typeof(BankFolderNode))
                    {
                        // need to get pack path
                        key = ((PackNode)node.Parent.Parent).GetObjectName() + "\\" + ((BankNode)node).GetObjectName();
                    }
                    else
                    {
                        key = ((BankNode)node).GetObjectPath();
                    }

                    if (WaveManager.WaveSlotRefs.ContainsKey(key) && WaveManager.WaveSlotRefs[key].Count > 0)
                    {
                        var msg = new frmSimpleOutput("Wave Slot Referencers");
                        msg.DisplayInfo(
                            "Cannot delete bank " + ((BankNode)node).GetObjectName()
                            + ". The bank is referenced by the following wave slots:");

                        foreach (string s in WaveManager.WaveSlotRefs[key])
                        {
                            msg.AddObject(s);
                        }

                        msg.Show(RaveInstance.ActiveWindow);
                        return false;
                    }
                }
                else if (node.GetType() == typeof(BankFolderNode))
                {
                    string key = ((BankFolderNode)node).GetObjectPath();
                    if (WaveManager.WaveSlotRefs.ContainsKey(key) && WaveManager.WaveSlotRefs[key].Count > 0)
                    {
                        var msg = new frmSimpleOutput("Wave Slot Referencers");
                        msg.DisplayInfo(
                            "Cannot delete bank " + ((BankFolderNode)node).GetObjectName()
                            + ". The bank is referenced by the following wave slots:");

                        foreach (string s in WaveManager.WaveSlotRefs[key])
                        {
                            msg.AddObject(s);
                        }

                        msg.Show(RaveInstance.ActiveWindow);
                        return false;
                    }
                }
                else if (node.GetType() == typeof(PackNode))
                {
                    string key = ((PackNode)node).GetObjectName();
                    if (WaveManager.WaveSlotRefs.ContainsKey(key) && WaveManager.WaveSlotRefs[key].Count > 0)
                    {
                        var msg = new frmSimpleOutput("Wave Slot Referencers");
                        msg.DisplayInfo(
                            "Cannot delete pack " + ((PackNode)node).GetObjectName()
                            + ". The bank is referenced by the following wave slots:");

                        foreach (string s in WaveManager.WaveSlotRefs[key])
                        {
                            msg.AddObject(s);
                        }

                        msg.Show(RaveInstance.ActiveWindow);
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// The delete children.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="platforms">
        /// The platforms.
        /// </param>
        /// <param name="deletedNodes">
        /// The list of deleted nodes.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool DeleteChildren(TreeNode parent, List<string> platforms, out HashSet<TreeNode> deletedNodes)
        {
            var nodesToDelete = new List<TreeNode>();
            deletedNodes = new HashSet<TreeNode>();

            foreach (TreeNode child in parent.Nodes)
            {
                HashSet<TreeNode> innerDeletedNodes;
                if (this.CanDeleteNode(child, platforms) && this.DeleteChildren(child, platforms, out innerDeletedNodes))
                {
                    nodesToDelete.Add(child);
                    deletedNodes.UnionWith(innerDeletedNodes);
                }
                else
                {
                    return false;
                }
            }

            for (var i = 0; i < nodesToDelete.Count; i++)
            {
                var currNode = nodesToDelete[i];
                if (!this.DeleteNode(currNode, platforms))
                {
                    return false;
                }

                deletedNodes.Add(currNode);
            }

            return true;
        }

        /// <summary>
        /// The delete node.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="platforms">
        /// The platforms.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool DeleteNode(TreeNode node, List<string> platforms)
        {
            string platformList = string.Empty;

            for (int i = 0; i < platforms.Count; i++)
            {
                platformList += platforms[i];
                if (i < platforms.Count - 1)
                {
                    platformList += ",";
                }
            }

            this.m_statusText = "Deleting " + ((EditorTreeNode)node).GetObjectName() + " from " + platformList;
            if (!this.CanDeleteNode(node, platforms))
            {
                return false;
            }

            if (platforms.Count > 0)
            {
                if (node.GetType() == typeof(WaveNode))
                {
                    // wave nodes don't have any children so just delete this node
                    var action =
                        new DeleteWaveAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                (EditorTreeNode)node.Parent,
                                (EditorTreeNode)node,
                                null,
                                null,
                                this.m_actionLog,
                                null,
                                platforms,
                                null,
                                null));
                    if (action.Action())
                    {
                        this.m_actionLog.AddAction(action);
                        return true;
                    }
                }
                else if (node.GetType() == typeof(BankNode))
                {
                    var action =
                        new DeleteBankAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                (EditorTreeNode)node.Parent,
                                (EditorTreeNode)node,
                                null,
                                null,
                                this.m_actionLog,
                                null,
                                platforms,
                                null,
                                null));
                    if (action.Action())
                    {
                        this.m_actionLog.AddAction(action);
                        return true;
                    }
                }
                else if (node.GetType() == typeof(PackNode))
                {
                    var action =
                        new DeletePackAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                null,
                                (EditorTreeNode)node,
                                null,
                                this.m_treeView,
                                this.m_actionLog,
                                null,
                                platforms,
                                null,
                                null));
                    if (action.Action())
                    {
                        this.m_actionLog.AddAction(action);
                        return true;
                    }
                }
                else if (node.GetType() == typeof(BankFolderNode))
                {
                    var action =
                        new DeleteBankFolderAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                (EditorTreeNode)node.Parent,
                                (EditorTreeNode)node,
                                null,
                                null,
                                null,
                                null,
                                platforms,
                                null,
                                null));
                    if (action.Action())
                    {
                        this.m_actionLog.AddAction(action);
                        return true;
                    }
                }
                else if (node.GetType() == typeof(WaveFolderNode))
                {
                    var action =
                        new DeleteWaveFolderAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                (EditorTreeNode)node.Parent,
                                (EditorTreeNode)node,
                                null,
                                null,
                                null,
                                null,
                                platforms,
                                null,
                                null));
                    if (action.Action())
                    {
                        this.m_actionLog.AddAction(action);
                        return true;
                    }
                }
                else if (node.GetType() == typeof(TagNode))
                {
                    var action =
                        new DeleteTagAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                (EditorTreeNode)node.Parent,
                                (EditorTreeNode)node,
                                null,
                                null,
                                null,
                                null,
                                platforms,
                                null,
                                null));
                    if (action.Action())
                    {
                        this.m_actionLog.AddAction(action);
                        return true;
                    }
                }
                else if (node.GetType() == typeof(PresetNode))
                {
                    var action =
                        new DeletePresetAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                (EditorTreeNode)node.Parent,
                                (EditorTreeNode)node,
                                null,
                                null,
                                null,
                                null,
                                platforms,
                                null,
                                null));
                    if (action.Action())
                    {
                        this.m_actionLog.AddAction(action);
                        return true;
                    }
                }
                else
                {
                    ErrorManager.HandleInfo("Unknown node type!");
                }

                return false;
            }

            // doesn't exist on this platform anyway
            return true;
        }

        /// <summary>
        /// The delete processed version.
        /// </summary>
        /// <param name="n">
        /// The node.
        /// </param>
        /// <returns>
        /// The deleted processed wave node <see cref="WaveNode"/>.
        /// </returns>
        private EditorTreeNode DeleteProcessedVersion(EditorTreeNode n)
        {
            foreach (EditorTreeNode packNode in this.m_treeView.Nodes)
            {
                var pack = packNode as PackNode;
                if (pack == null)
                {
                    continue;
                }

                var packName = pack.GetObjectName().ToUpper();
                if (!packName.Equals("SCRIPTED_SPEECH") && !packName.StartsWith("SS_"))
                {
                    continue;
                }

                foreach (EditorTreeNode bankFolderNode in pack.Nodes)
                {
                    var bankFolder = bankFolderNode as BankFolderNode;
                    if (bankFolder == null || !bankFolder.GetObjectName().ToUpper().Equals("SCRIPTED_SPEECH"))
                    {
                        continue;
                    }

                    foreach (EditorTreeNode bankNode in bankFolder.Nodes)
                    {
                        var bank = bankNode as BankNode;
                        if (bank == null || !n.GetObjectName().Contains(bank.GetObjectName()))
                        {
                            continue;
                        }

                        foreach (EditorTreeNode waveFolderNode in bank.Nodes)
                        {
                            var waveFolder = waveFolderNode as WaveFolderNode;
                            if (waveFolder == null)
                            {
                                continue;
                            }

                            foreach (EditorTreeNode waveNode in waveFolder.Nodes)
                            {
                                var wave = waveNode as WaveNode;
                                if (wave == null
                                    || !wave.GetObjectName()
                                            .Equals(n.GetObjectName().Replace(".WAV", ".PROCESSED.WAV")))
                                {
                                    continue;
                                }

                                var temp2 = new List<string>();
                                temp2.AddRange(wave.GetPlatforms());
                                HashSet<TreeNode> deletedNodes;
                                if (!this.CanDeleteNode(wave, temp2)
                                    || !this.DeleteChildren(wave, temp2, out deletedNodes))
                                {
                                    continue;
                                }

                                this.DeleteNode(wave, temp2);
                                return wave;
                            }
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// The handle delete.
        /// </summary>
        private void HandleDelete()
        {
            this.m_treeView.BeginUpdate();
            var t = new Thread(this.StartStatusBar);
            t.Start();
            this.m_statusText = "Working";

            var deletedNodes = new HashSet<TreeNode>();

            foreach (EditorTreeNode node in this.m_treeView.SelectedItems)
            {
                // Need to check that node hasn't already been deleted
                // e.g. processed version may have already been deleted
                // so no need to try and delete it again
                if (deletedNodes.Contains(node))
                {
                    continue;
                }

                var temp = new List<string>();
                temp.AddRange(node.GetPlatforms());
                if (this.CanDeleteNode(node, temp))
                {
                    HashSet<TreeNode> currDeletedNodes;
                    if (node is WaveNode || node is TagNode)
                    {
                        //Delete just newLocation which will delete all the sub nodes if it is a tag or a wave newLocation.
                        DeleteNode(node, temp);
                        deletedNodes.Add(node);
                    }
                    else if (this.DeleteChildren(node, temp, out currDeletedNodes))
                    {
                        this.DeleteNode(node, temp);
                        deletedNodes.Add(node);
                        deletedNodes.UnionWith(currDeletedNodes);
                    }
                    else
                    {
                        break;
                    }
                }

                // If it's a wave node, we may need to kill it's processed version too
                if (node.GetType() == typeof(WaveNode))
                {
                    var deleted = this.DeleteProcessedVersion(node);
                    if (null != deleted)
                    {
                        deletedNodes.Add(deleted);
                    }
                }
            }

            this.m_statusText = "Finished";
            this.m_isFinished = true;
            this.m_treeView.EndUpdate();
        }

        /// <summary>
        /// The handle dropped files.
        /// </summary>
        /// <param name="files">
        /// The files.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool HandleDroppedFiles(string[] files)
        {
            if (!this.ValidateFiles(files))
            {
                return false;
            }

            try
            {
                var node = this.m_treeView.LastSelectedNode;

                if (Configuration.SelectedPlugin != null)
                {
                    // plugin is selected
                    try
                    {
                        this.m_isFinished = false;
                        var t = new Thread(this.StartStatusBar);
                        t.Start();

                        Configuration.SelectedPlugin.HandleDroppedFiles(
                            this.m_waveBrowser,
                            node as EditorTreeNode,
                            this.m_actionLog,
                            files);
                    }
                    finally
                    {
                        this.m_isFinished = true;
                    }
                }
                else
                {
                    this.m_statusText = "Importing files";
                    this.m_isFinished = false;
                    var statusBarThread = new Thread(this.StartStatusBar);
                    statusBarThread.Start();

                    try
                    {
                        // No plugin selected, use default drop handler
                        foreach (var file in files)
                        {
                            if (Directory.Exists(file))
                            {
                                // Handle dropped folder
                                var skippedFiles = new List<string>();
                                this.HandleDroppedFolder(node, file, skippedFiles);

                                if (skippedFiles.Any())
                                {
                                    var msg = skippedFiles.Aggregate(
                                        string.Empty,
                                        (current, skippedFile) => current + (skippedFile + Environment.NewLine));

                                    ErrorManager.HandleInfo(
                                        "The following file(s) were skipped:" + Environment.NewLine
                                        + Environment.NewLine + msg);
                                }
                            }
                            else
                            {
                                // handle dropped file
                                if (!this.HandleDroppedFile(node, file))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                    finally
                    {
                        this.m_isFinished = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }

            return true;
        }

        /// <summary>
        /// Handle a dropped file.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool HandleDroppedFile(TreeNode node, string file)
        {
            var bankNode = node as BankNode;
            if (null == bankNode)
            {
                ErrorManager.HandleInfo("Files can only be dropped onto a Bank");
                return false;
            }

            var fileUpper = file.ToUpper();
            var extension = Path.GetExtension(fileUpper);

            if (string.CompareOrdinal(extension, ".WAV") == 0)
            {
                this.m_statusText = "Importing " + file;
                this.HandleDroppedWavFile(bankNode, file);
            }
            else if (string.CompareOrdinal(extension, ".MID") == 0 || string.CompareOrdinal(extension, ".TXT") == 0 || string.CompareOrdinal(extension, ".GRN") == 0)
            {
                this.m_statusText = "Importing " + file;
                this.HandleDroppedMidOrGrnOrTxtFile(bankNode, file);
            }
            else
            {
                ErrorManager.HandleInfo("Cannot add file since file is not of type .WAV, .MID, .TXT or .GRN: " + file);
                return false;
            }

            return true;
        }



        public void HandleDriveFiles(string[] files, string rootDir)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(
                    new Action<string[], string>(this.HandleDriveFiles),
                    new object[] { files, rootDir });
            }
            else
            {
                RaveDriveHelper.HandleDriveFile(files, rootDir, this.m_waveBrowser, this.m_actionLog);
            }
        }

        /// <summary>
        /// Handle a dropped folder containing wav/mid files.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="dirPath">
        /// The directory path.
        /// </param>
        /// <param name="skippedFiles">
        /// The skipped files list.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool HandleDroppedFolder(TreeNode node, string dirPath, ICollection<string> skippedFiles)
        {
            if (!Directory.Exists(dirPath))
            {
                ErrorManager.HandleInfo("Failed to find directory path: " + dirPath);
                return false;
            }

            var bankNode = node as BankNode;

            if (bankNode == null)
            {
                // try to create a bank automatically using the folder name
                var di = new DirectoryInfo(dirPath);
                string bankName = RaveUtils.FormatGameString(di.Name);

                var parentPack = node as PackNode;
                if (parentPack != null)
                {
                    var action =
                       new AddBankAction(
                           new ActionParams(
                               this.m_waveBrowser,
                               parentPack,
                               null,
                               null,
                               this.m_treeView,
                               this.m_actionLog,
                               bankName,
                               parentPack.GetPlatforms(),
                               null,
                               null));
                    if (action.Action())
                    {
                        this.m_actionLog.AddAction(action);
                        this.FolderCreated.Raise<string>(action.Node.FullPath);
                    }

                    bankNode = action.Node as BankNode;
                }
            }

            // Handle files in current directory level
            var files = Directory.EnumerateFiles(dirPath, "*.*").Where(
                file => file.ToLower().EndsWith(".wav") || file.ToLower().EndsWith(".mid") || file.ToLower().EndsWith(".grn")).ToList();

            if (files.Any() && null == bankNode)
            {
                ErrorManager.HandleInfo("Cannot add file(s) as " + node.Name + " is not a bank node");
                return false;
            }

            foreach (var file in files)
            {
                var fileUpper = file.ToUpper();
                var extension = Path.GetExtension(fileUpper);
                if (string.CompareOrdinal(extension, ".WAV") == 0)
                {
                    this.m_statusText = "Importing " + file;
                    this.HandleDroppedWavFile(bankNode, file);
                }
                else if (string.CompareOrdinal(extension, ".MID") == 0 || string.CompareOrdinal(extension, ".TXT") == 0 || string.CompareOrdinal(extension, ".GRN") == 0)
                {
                    this.m_statusText = "Importing " + file;
                    this.HandleDroppedMidOrGrnOrTxtFile(bankNode, file);
                }
                else
                {
                    skippedFiles.Add(file);
                }
            }

            // Handle files in any sub directories
            var subDirs = Directory.GetDirectories(dirPath);
            foreach (var subDir in subDirs)
            {
                var dirName = subDir.TrimEnd('\\');
                dirName = dirName.Substring(subDir.LastIndexOf("\\", StringComparison.Ordinal) + 1);
                var dirNameUpper = dirName.ToUpper();

                TreeNode foundNode = null;

                // Locate child bank to drop files onto
                foreach (TreeNode childNode in node.Nodes)
                {
                    var packNode = childNode as PackNode;
                    var folderNode = childNode as WaveFolderNode;
                    var childBankNode = childNode as BankNode;

                    string name = null;
                    if (null != packNode)
                    {
                        name = packNode.PackName;
                    }
                    else if (null != folderNode)
                    {
                        name = folderNode.Text;
                    }
                    else if (null != childBankNode)
                    {
                        name = childBankNode.Text;
                    }

                    if (!string.IsNullOrEmpty(name) &&
                        name.ToUpper() == dirNameUpper)
                    {
                        foundNode = childNode;
                        break;
                    }
                }

                if (null == foundNode)
                {
                    ErrorManager.HandleInfo("Failed to find node matching name: " + dirName);
                    return false;
                }

                if (!this.HandleDroppedFolder(foundNode, subDir, skippedFiles))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// The handle dropped mid file.
        /// </summary>
        /// <param name="bankNode">
        /// The bank node.
        /// </param>
        /// <param name="file">
        /// The file.
        /// </param>
        private void HandleDroppedMidOrGrnOrTxtFile(EditorTreeNode bankNode, string file)
        {
            try
            {
                List<string> platforms = bankNode.GetPlatforms();
                if (platforms.Count > 0)
                {
                    string midiOrGrnOrTxtName = Path.GetFileName(file);
                    WaveNode nonWavNode = null;
                    for (int i = 0; i < bankNode.Nodes.Count; i++)
                    {
                        var waveNode = bankNode.Nodes[i] as WaveNode;
                        if (waveNode != null)
                        {
                            if (waveNode.GetObjectName().ToUpper() == midiOrGrnOrTxtName)
                            {
                                nonWavNode = bankNode.Nodes[i] as WaveNode;
                                break;
                            }
                        }
                    }

                    if (nonWavNode == null)
                    {
                        // this is a new midi/txt/grn file
                        var action =
                            new AddWaveAction(
                                new ActionParams(
                                    this.m_waveBrowser,
                                    bankNode,
                                    null,
                                    file,
                                    null,
                                    this.m_actionLog,
                                    null,
                                    platforms,
                                    null,
                                    null));
                        if (action.Action())
                        {
                            this.m_actionLog.AddAction(action);
                        }
                    }
                    else
                    {
                        // this is an existing midi/txt/grn file
                        var action =
                            new ChangeWaveAction(
                                new ActionParams(
                                    this.m_waveBrowser,
                                    bankNode,
                                    nonWavNode,
                                    file,
                                    null,
                                    this.m_actionLog,
                                    null,
                                    nonWavNode.GetPlatforms(),
                                    null,
                                    null));
                        if (action.Action())
                        {
                            this.m_actionLog.AddAction(action);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }
        }


        /// <summary>
        /// The handle dropped wav file.
        /// </summary>
        /// <param name="bankNode">
        /// The bank node.
        /// </param>
        /// <param name="file">
        /// The file.
        /// </param>
        private void HandleDroppedWavFile(EditorTreeNode bankNode, string file)
        {
            try
            {
                var waveFile = new bwWaveFile(file, false);
                uint numOfChannels = waveFile.Format.NumChannels;
                
                if (waveFile.Format.SignificantBitsPerSample != 16)
                {
                    throw new Exception(string.Format("The file you are trying to import has a bit rate of {0}. Only 16 bit wav file are supported.", waveFile.Format.SignificantBitsPerSample));
                }

                var newWaveNames = new string[numOfChannels];

                // file paths needed for multichannel
                var filePaths = new string[numOfChannels];
                string newWaveName = file.ToUpper().Substring(file.LastIndexOf("\\") + 1).Replace(" ", "_");
                if (numOfChannels == 1)
                {
                    // mono sound
                    newWaveNames[0] = newWaveName;
                    filePaths[0] = file;
                }
                else
                {
                    // multichannel add  extension
                    bwMarkerList markers = waveFile.Markers;
                    for (int i = 0; i < numOfChannels; i++)
                    {
                        string name = newWaveName;
                        newWaveNames[i] = name.Substring(0, name.Length - 4) + ms_channelExtension[i] + ".WAV";
                        filePaths[i] = Path.GetTempPath() + newWaveNames[i];

                        if (!CreateWaveFilePerChannel(waveFile, i, filePaths[i], markers)) return;

                    }
                }

                var existingWaves = new WaveNode[numOfChannels];
                for (var waveNameIndex = 0; waveNameIndex < newWaveNames.Length; waveNameIndex++)
                {
                    for (var i = 0; i < bankNode.Nodes.Count; i++)
                    {
                        if (bankNode.Nodes[i].GetType() == typeof(WaveNode))
                        {
                            if (((WaveNode)bankNode.Nodes[i]).GetObjectName().ToUpper() == newWaveNames[waveNameIndex])
                            {
                                existingWaves[waveNameIndex] = (WaveNode)bankNode.Nodes[i];
                            }
                        }
                    }
                }

                for (var i = 0; i < existingWaves.Length; i++)
                {
                    var platforms = bankNode.GetPlatforms();
                    if (platforms.Count <= 0)
                    {
                        continue;
                    }

                    if (existingWaves[i] == null)
                    {
                        // this is a new wave file
                        var action =
                            new AddWaveAction(
                                new ActionParams(
                                    this.m_waveBrowser,
                                    bankNode,
                                    null,
                                    filePaths[i],
                                    null,
                                    this.m_actionLog,
                                    null,
                                    platforms,
                                    null,
                                    null));
                        if (action.Action())
                        {
                            this.m_actionLog.AddAction(action);
                        }
                    }
                    else
                    {
                        // this is an existing wave file
                        var action =
                            new ChangeWaveAction(
                                new ActionParams(
                                    this.m_waveBrowser,
                                    bankNode,
                                    existingWaves[i],
                                    filePaths[i],
                                    null,
                                    this.m_actionLog,
                                    null,
                                    existingWaves[i].GetPlatforms(),
                                    null,
                                    null));
                        if (action.Action())
                        {
                            this.m_actionLog.AddAction(action);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }
        }

        private bool CreateWaveFilePerChannel(bwWaveFile waveFile, int channelIndex, string filePath, bwMarkerList markers)
        {
            try
            {
                var tempWave = new bwWaveFile();
                foreach (IRiffChunk chunk in waveFile.Chunks)
                {
                    var fChunk = chunk as bwFormatChunk;
                    var dChunk = chunk as bwDataChunk;

                    if (fChunk != null)
                    {
                        // current chunk is format chunk
                        var formatChunk = new bwFormatChunk(
                            fChunk.CompressionCode,
                            1,
                            fChunk.SampleRate,
                            fChunk.AverageBytesPerSecond / 2,
                            (ushort)(fChunk.BlockAlign / 2),
                            fChunk.SignificantBitsPerSample,
                            fChunk.ExtraFormatBytes);
                        tempWave.AddChunk(formatChunk);
                        tempWave.Format = formatChunk;
                    }
                    else if (dChunk != null)
                    {
                        // current chunk is data chunk
                        var newData = new byte[(uint)(0.5 * dChunk.RawData.Length)];

                        // new chunk index
                        var index = 0;

                        // iterate through current raw data and split up
                        // j set to 2* channel, 16bits/sample so increase j by 4 bytes
                        for (int j = channelIndex * 2; j < dChunk.RawData.Length; j += 4)
                        {
                            newData[index] = dChunk.RawData[j];
                            newData[index + 1] = dChunk.RawData[j + 1];
                            index += 2;
                        }

                        var dataChunk = new bwDataChunk(newData, dChunk.NumSamples, bwDataChunk.DataFormat.S16);
                        tempWave.Data = dataChunk;
                    }
                    else
                    {
                        tempWave.AddChunk(chunk);
                    }
                }

                tempWave.FileName = filePath;
                tempWave.Markers = markers;
                tempWave.Save();
                return true;
            }
            catch (Exception)
            {
                ErrorManager.HandleInfo("Invalid wave file - only 16bit waves are supported");
                return false;
            }
        }

        /// <summary>
        /// The handle dropped wave nodes.
        /// </summary>
        /// <param name="bankNode">
        /// The bank node.
        /// </param>
        /// <param name="waveNodes">
        /// The wave nodes.
        /// </param>
        private void HandleDroppedWaveNodes(BankNode bankNode, IList<WaveNode> waveNodes)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Move:");
            foreach (var waveNode in waveNodes)
            {
                sb.AppendLine(waveNode.GetObjectPath());
            }

            sb.AppendLine();
            sb.AppendLine("To:");
            sb.Append(bankNode.GetObjectPath());

            if (ErrorManager.HandleQuestion(sb.ToString(), MessageBoxButtons.YesNo, DialogResult.Yes) == DialogResult.No)
            {
                return;
            }

            // Check if any waves are referenced and prompt user to continue/cancel.
            if (this.CheckForWaveReferences(waveNodes))
            {
                if (
                    ErrorManager.HandleQuestion(
                        "One or more waves are currently referenced. These references will be updated - continue?",
                        MessageBoxButtons.OKCancel,
                        DialogResult.OK) == DialogResult.Cancel)
                {
                    return;
                }
            }

            EditorTreeNode.AllowAutoLock = true;
            foreach (var waveNode in waveNodes)
            {
                if (bankNode.GetBankPath() == waveNode.GetBankPath())
                {
                    continue;
                }

                // copy wave to temp
                var fileName = Configuration.PlatformWavePath;
                fileName = fileName + waveNode.GetObjectPath();

                //var tempFileName = Path.GetFileName(waveNode.GetObjectPath());
                //tempFileName = Path.GetTempPath() + tempFileName;

                //if (File.Exists(tempFileName))
                //{
                //    new FileInfo(tempFileName) { Attributes = FileAttributes.Normal };
                //}

                //File.Copy(fileName, tempFileName, true);

                // remove wave
                var deleteAction =
                    new DeleteWaveAction(
                        new ActionParams(
                            this.m_waveBrowser,
                            (EditorTreeNode)waveNode.Parent,
                            waveNode,
                            null,
                            null,
                            this.m_actionLog,
                            null,
                            bankNode.GetPlatforms(),
                            null,
                            null),
                            true);

                // add wave
                // this is a new wave file
                var addAction =
                    new AddWaveAction(
                        new ActionParams(
                            this.m_waveBrowser,
                            bankNode,
                            null,
                            fileName,
                            null,
                            this.m_actionLog,
                            null,
                            bankNode.GetPlatforms(),
                            null,
                            null));

                var moveWaveAction = new MoveWaveAction(addAction, deleteAction);
                if (!moveWaveAction.Action())
                {
                    moveWaveAction = null;
                }

                if (moveWaveAction != null)
                {
                    this.m_actionLog.AddAction(moveWaveAction);

                }
            }

            EditorTreeNode.AllowAutoLock = false;
        }

        /// <summary>
        /// Check if any waves are referenced by any objects.
        /// </summary>
        /// <param name="waveNodes">
        /// The wave nodes.
        /// </param>
        /// <returns>
        /// Value indicating whether any waves are referenced <see cref="bool"/>.
        /// </returns>
        private bool CheckForWaveReferences(IEnumerable<WaveNode> waveNodes)
        {
            foreach (var waveNode in waveNodes)
            {
                var pack = EditorTreeNode.FindParentPackNode(waveNode);
                var skip = false;
                if (Configuration.WaveRefsToSkip() != null)
                {
                    skip = Configuration.WaveRefsToSkip().Any(s => pack.PackName.Contains(s.ToUpper()));
                }

                var key = waveNode.GetBankPath() + "\\" + waveNode.GetWaveName();
                if (skip || !WaveManager.WaveRefs.ContainsKey(key) || !WaveManager.WaveRefs[key].Any())
                {
                    continue;
                }

                // Don't worry about references if they are live in autogenerated stores
                if (WaveManager.WaveRefs[key].Any(obj => !obj.Bank.IsAutoGenerated))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// The populate preset menu.
        /// </summary>
        private void PopulatePresetMenu()
        {
            string presetPath = Configuration.PresetPath;
            if (!string.IsNullOrEmpty(presetPath))
            {
                var dir = new DirectoryInfo(presetPath);
                if (dir.Exists && Preset.Presets.Count == 0)
                {
                    foreach (FileInfo file in dir.GetFiles())
                    {
                        var doc = new XmlDocument();
                        doc.Load(file.FullName);
                        foreach (XmlNode preset in doc.DocumentElement.ChildNodes)
                        {
                            string presetName = preset.Attributes["name"].Value.ToUpper();
                            if (!Preset.Presets.ContainsKey(presetName))
                            {
                                Preset.Presets.Add(presetName, new Preset(preset));
                            }
                            else
                            {
                                ErrorManager.HandleError("Duplicate preset name found " + presetName);
                            }
                        }
                    }
                }

                if (Preset.Presets.Count > 0)
                {
                    foreach (var kvp in Preset.Presets)
                    {
                        var mi = new ToolStripMenuItem(kvp.Key, null, this.mnuPreset_Click) { Tag = kvp.Value };
                        this.mnuPreset.DropDownItems.Add(mi);
                    }
                }
            }
        }

        /// <summary>
        /// The start status bar.
        /// </summary>
        private void StartStatusBar()
        {
            var busy = new frmBusy();
            busy.SetUnknownDuration(true);
            busy.Show();
            while (!this.m_isFinished)
            {
                Application.DoEvents();
                busy.SetStatusText(this.m_statusText);
            }

            busy.FinishedWorking();

            // Reset variables
            this.m_isFinished = false;
            this.m_statusText = "Working...";
        }

        /// <summary>
        /// The tree view_ double click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DoubleClick(object sender, EventArgs e)
        {
            this.DoubleClickNode.Raise(this.m_treeView.LastSelectedNode as EditorTreeNode);
        }

        /// <summary>
        /// The tree view_ drag drop.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DragDrop(object sender, DragEventArgs e)
        {
            var clientPoint = this.m_treeView.PointToClient(new Point(e.X, e.Y));
            var target = this.m_treeView.GetNodeAt(clientPoint);

            if (target == null)
            {
                return;
            }

            var bankNode = target as BankNode;
            var bankFolderNode = target as BankFolderNode;
            var packNode = target as PackNode;

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // File/folder drop
                var path = (string[])e.Data.GetData(DataFormats.FileDrop);
                this.HandleDroppedFiles(path);
            }
            else if (null != bankNode)
            {
                if (e.Data.GetDataPresent(typeof(WaveNode[])))
                {
                    // Wave node drop on bank
                    this.HandleDroppedWaveNodes(bankNode, (WaveNode[])e.Data.GetData(typeof(WaveNode[])));
                }
            }
            else if (null != bankFolderNode)
            {
                if (e.Data.GetDataPresent(typeof(BankNode[])))
                {
                    // Bank node drop on bank folder
                    var a = "";
                }
            }
            else if (null != packNode)
            {
                if (e.Data.GetDataPresent(typeof(BankFolderNode[])))
                {
                    // Bank folder drop on pack node
                    var b = "";
                }
                else if (e.Data.GetDataPresent(typeof(BankNode[])))
                {
                    // Bank node drop on pack node
                    var c = "";
                }
            }
        }

        /// <summary>
        /// The tree view_ drag enter.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        /// <summary>
        /// The tree view_ drag over.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DragOver(object sender, DragEventArgs e)
        {
            this.OnDragOver(e);

            e.Effect = e.AllowedEffect;

            Point p = this.m_treeView.PointToClient(new Point(e.X, e.Y));
            this.m_treeView.LastSelectedNode = this.m_treeView.GetNodeAt(p.X, p.Y);
        }

        /// <summary>
        /// The tree view_ item drag.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (this.ShouldFlattenWavesOnDrag)
            {
                var waves = new ArrayList();

                foreach (EditorTreeNode etn in this.m_treeView.SelectedItems)
                {
                    var container = etn as WaveContainerNode;
                    if (container != null)
                    {
                        ArrayList temp = container.GetChildWaveNodes();
                        foreach (WaveNode w in temp)
                        {
                            if (!waves.Contains(w))
                            {
                                waves.Add(w);
                            }
                        }
                    }
                    else if (etn.GetType() == typeof(WaveNode))
                    {
                        if (!waves.Contains(etn))
                        {
                            waves.Add(etn);
                        }
                    }
                }

                this.DoDragDrop(waves.ToArray(typeof(WaveNode)), DragDropEffects.Link);
            }
            else
            {
                this.DoDragDrop(this.m_treeView.SelectedItems.ToArray(typeof(EditorTreeNode)), DragDropEffects.Link);
            }
        }

        /// <summary>
        /// The tree view_ key down.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Delete:
                    this.HandleDelete();
                    break;
                case Keys.F2:
                    if (this.m_treeView.LabelEdit)
                    {
                        this.m_treeView.LastSelectedNode.BeginEdit();
                    }

                    break;
                case Keys.S:
                    this.m_auditionPlayback.Stop();
                    break;
                case Keys.Space:
                    try
                    {
                        if (this.m_treeView.LastSelectedNode != null
                            && this.m_treeView.LastSelectedNode.GetType() == typeof(WaveNode))
                        {
                            this.AuditionWaveNode();
                        }
                    }
                    catch (NullReferenceException)
                    {
                        // nothing selected
                    }

                    break;
            }
        }

        /// <summary>
        /// The tree view_ mouse move.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_MouseMove(object sender, MouseEventArgs e)
        {
            this.MouseOverNode.Raise(this.m_treeView.GetNodeAt(e.X, e.Y) as EditorTreeNode);
        }

        /// <summary>
        /// The validate files.
        /// </summary>
        /// <param name="files">
        /// The files.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        internal bool ValidateFiles(IEnumerable<string> files)
        {
            var errors = this.ValidateFilesHelper(files);
            if (!string.IsNullOrEmpty(errors))
            {
                ErrorManager.HandleError("Unable to continue due to following error(s):" + Environment.NewLine + errors);
                return false;
            }

            return true;
        }

        /// <summary>
        /// The validate files helper.
        /// </summary>
        /// <param name="files">
        /// The files.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ValidateFilesHelper(IEnumerable<string> files)
        {
            var errors = string.Empty;

            foreach (var file in files)
            {
                // Is a file
                if (File.Exists(file))
                {
                    // Ignore non-wav/midi/txt/grn files
                    if (!file.ToUpper().EndsWith("WAV") && !file.ToUpper().EndsWith("MID") && !file.ToUpper().EndsWith("TXT") && !file.ToUpper().EndsWith("MID") && !file.ToUpper().EndsWith("GRN"))
                    {
                        continue;
                    }

                    var name = Path.GetFileName(file);

                    // Validate wave/mid/txt/grn name
                    if (!RaveUtils.StringIsValidWaveOrMidiOrGrnOrTxtName(name))
                    {
                        errors += Path.GetFullPath(file) + " contains invalid characters in name." + Environment.NewLine;
                    }
                }
                else
                {
                    // Recursively process contents of folder
                    var innerFiles = Directory.GetFiles(file);
                    errors += this.ValidateFilesHelper(innerFiles);
                }
            }

            return errors;
        }

        private string getPackNameFromPath(string path)
        {
            int index = path.IndexOf("\\");
            if (index != -1) return path.Substring(0, index);
            else return path;
        }

        /// <summary>
        /// The mnu build pack_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuBuildPack_Click(object sender, EventArgs e)
        {
            var menuItem = sender as ToolStripMenuItem;
            if (menuItem == null)
            {
                return;
            }

            var buildSet = menuItem.Tag as audBuildSet;
            if (buildSet == null)
            {
                return;
            }

            var packNames = new HashSet<string>();
            foreach (var node in this.m_treeView.SelectedItems)
            {
                var treeNode = node as EditorTreeNode;
                if (null != treeNode)
                {
                    var packNode = EditorTreeNode.FindParentPackNode(treeNode);
                    if (null != packNode)
                    {
                        packNames.Add(packNode.PackName);
                    }
                }
            }

            if (!packNames.Any())
            {
                return;
            }

            RaveInstance.BuildManager.Build(buildSet, menuItem.Text, packNames);
        }

        private void mnuBuildBankPreview_Click(object sender, EventArgs e)
        {
            var menuItem = sender as ToolStripMenuItem;
            if (menuItem == null)
            {
                return;
            }

            var buildSet = menuItem.Tag as audBuildSet;
            if (buildSet == null)
            {
                return;
            }

            foreach (var selectedItem in m_treeView.SelectedItems)
            {


                BankNode banknode = selectedItem as BankNode;
                if (banknode != null && !(selectedItem is WaveFolderNode))
                {

                    foreach (string platform in banknode.GetPlatforms())
                    {
                        var selectedPlatform = menuItem.Text;
                        if (platform == selectedPlatform || (selectedPlatform == ALL_PLATFORMS))
                        {
                            var currentPlatform = platform;
                           // Task.Factory.StartNew(() => BuildBankPreview(currentPlatform, banknode));
                        }
                    }

                }

            }

        }

        private object textLock = new object();
       
        /// <summary>
        /// The mnu built path to clipboard_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuBuiltPathToClipboard_Click(object sender, EventArgs e)
        {
            var node = this.m_treeView.LastSelectedNode as EditorTreeNode;
            if (node != null)
            {
                Clipboard.SetText(node.GetBuiltPath().Replace("\\","/"));
            }
        }

        /// <summary>
        /// The mnu contents_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuContents_Click(object sender, EventArgs e)
        {
            var treeNode = this.m_treeView.LastSelectedNode as EditorTreeNode;
            if (treeNode != null)
            {
                var outPut = new frmSimpleOutput("View Contents");
                outPut.OnSelectObject += this.outPut_OnSelectObject;
                outPut.DisplayInfo(treeNode.GetObjectName() + " contents:");

                foreach (EditorTreeNode etn in treeNode.Nodes)
                {
                    outPut.AddObject(etn);
                }

                outPut.Show(RaveInstance.ActiveWindow);
            }
        }

        /// <summary>
        /// The mnu delete_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuDelete_Click(object sender, EventArgs e)
        {
            this.HandleDelete();
        }

        /// <summary>
        /// The mnu edit wave_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="Exception">
        /// </exception>
        private void mnuEditWave_Click(object sender, EventArgs e)
        {
            if (!File.Exists(Configuration.WaveEditorPath))
            {
                ErrorManager.HandleInfo("The configured Wave Editor " + Configuration.WaveEditorPath + " could not be found.");
                return;
            }
            var wave = (WaveNode)this.m_treeView.LastSelectedNode;
            string assetPath = Configuration.PlatformWavePath + wave.GetObjectPath();
            IAsset waveAsset = null;

            if (this.m_actionLog.GetCommitLogCount() > 0
                && this.m_actionLog.SearchForAction(wave.GetObjectPath()) != null)
            {
                ErrorManager.HandleInfo("Please commit previous wave changes before editing a wave.");
                return;
            }

            // This flag allows us to know if it's safe to revert and delete
            // the pending wave change list if the user cancels the operation
            bool nullOrEmptyChangeList = null == RaveInstance.RaveAssetManager.WaveChangeList
                                         || !RaveInstance.RaveAssetManager.WaveChangeList.Assets.Any();

            if (wave.TryToLock())
            {
                IChangeList changeList = RaveInstance.RaveAssetManager.WaveChangeList;

                try
                {
                    if (changeList == null)
                    {
                        changeList = RaveInstance.AssetManager.CreateChangeList("[Rave] Wave Changelist");
                        RaveInstance.RaveAssetManager.WaveChangeList = changeList;
                    }

                    waveAsset = changeList.CheckoutAsset(assetPath, true);
                }
                catch (Exception ex)
                {
                    ErrorManager.HandleError(ex);

                    // Try to tidy up after error
                    try
                    {
                        if (null != changeList && nullOrEmptyChangeList)
                        {
                            // Safe to revert the change list
                            // when error occurs and change list was
                            // null/empty before we started wave edit
                            changeList.Revert(true);
                            RaveInstance.RaveAssetManager.WaveChangeList = null;
                        }
                        else if (null != waveAsset)
                        {
                            waveAsset.Revert();
                        }
                    }
                    catch (Exception)
                    {
                        // We tried our best to tidy up
                    }

                    wave.Unlock();
                    return;
                }

                IAsset asset = changeList.GetAsset(assetPath);

                // remove Visemes for editing as soundforge doesn't like them
                try
                {
                    var waveFile = new bwWaveFile(assetPath, false);
                    waveFile.RemoveRaveChunks();
                    waveFile.Save();
                }
                catch (Exception exception)
                {
                    asset.Revert();
                    throw exception;
                }

                Process.Start(Configuration.WaveEditorPath, asset.LocalPath);

                if (
                    ErrorManager.HandleQuestion(
                        "Once you have finished editing the wave, click OK. To cancel the edit (and undo the checkout), click Cancel.",
                        MessageBoxButtons.OKCancel,
                        DialogResult.OK) == DialogResult.OK)
                {
                    UserAction editWavAction = new EditWaveAction(new ActionParams(this.m_waveBrowser, null, null, null, null, null, wave.GetObjectPath(), null, null, null));
                    //we dont't need to check whether the action has been successful or not, since there's no additional logic executed
                    editWavAction.Action();
                    this.m_actionLog.AddAction(editWavAction);


                    foreach (string platform in wave.GetPlatforms())
                    {
                        this.m_waveBrowser.GetPendingWaveLists()
                            .GetPendingWaveList(platform)
                            .RecordModifyWave(wave.GetObjectPath());
                    }

                    wave.UpdateDisplayState(
                        EditorTreeNode.NodeDisplayState.ChangedInPendingWaveList,
                        ctrlWaveBrowser.ALL_PLATFORMS,
                        true,
                        true);

                    // All done - submit change list
                    changeList.Submit("[Rave] Wave Changelist");
                    RaveInstance.RaveAssetManager.WaveChangeList = null;
                }
                else
                {
                    if (nullOrEmptyChangeList)
                    {
                        // Safe to revert the change list
                        // if user cancels action and change list was
                        // null/empty before we started wave edit
                        try
                        {
                            changeList.Revert(true);
                            RaveInstance.RaveAssetManager.WaveChangeList = null;
                        }
                        catch (Exception ex)
                        {
                            ErrorManager.HandleError("Failed to revert change list: " + ex.Message);
                        }
                    }
                    else
                    {
                        // Otherwise just revert the wave asset
                        waveAsset.Revert();
                    }

                    wave.Unlock();
                }
            }
        }

        /// <summary>
        /// The mnu show in explorer_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuShowInExplorer_Click(object sender, EventArgs e)
        {
            Dictionary<string, List<string>> folderFiles = new Dictionary<string, List<string>>();
            foreach (var item in this.m_treeView.SelectedItems)
            {
                var waveNode = item as WaveNode;
                if (null == waveNode)
                {
                    return;
                }

                var path = Path.Combine(Configuration.PlatformWavePath, GetTextPath(waveNode));

                // Get latest version of file
                if (!RaveInstance.AssetManager.GetLatest(path, false))
                {
                    ErrorManager.HandleInfo("Failed to get latest on " + path);
                    continue;
                }

                if (!File.Exists(path))
                {
                    ErrorManager.HandleInfo("Can't find file " + path);
                    continue;
                }
                if (!folderFiles.Keys.Contains(Path.GetDirectoryName(path)))
                {
                    folderFiles[Path.GetDirectoryName(path)] = new List<string>();
                }
                folderFiles[Path.GetDirectoryName(path)].Add(path);
                }
            foreach (string folder in folderFiles.Keys)
            {
                rage.ToolLib.FileSystem.NativeMethods.OpenFolderAndSelectFiles(folder, folderFiles[folder].ToArray());
            }
        }

        /// <summary>
        /// The mnu lock_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuLock_Click(object sender, EventArgs e)
        {
            foreach (EditorTreeNode n in this.m_treeView.SelectedItems)
            {
                n.LockForEditing();

                while (!n.IsLocked())
                {
                    if (ErrorManager.HandleQuestion(
                        "Lock operation failed.  Retry?", MessageBoxButtons.RetryCancel, DialogResult.Cancel)
                        == DialogResult.Cancel)
                    {
                        break;
                    }

                    n.LockForEditing();
                }
            }
        }

        /// <summary>
        /// The mnu new bank folder_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewBankFolder_Click(object sender, EventArgs e)
        {
            List<string> platforms = ((EditorTreeNode)this.m_treeView.LastSelectedNode).GetPlatforms();
            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "Bank Folder Name");
            if (input.HasBeenCancelled) return;
            string bankFolderName = input.data;
            if (bankFolderName != null && platforms.Count > 0)
            {
                var action =
                    new AddBankFolderAction(
                        new ActionParams(
                            this.m_waveBrowser,
                            (EditorTreeNode)this.m_treeView.LastSelectedNode,
                            null,
                            null,
                            null,
                            this.m_actionLog,
                            bankFolderName,
                            platforms,
                            null,
                            null));
                if (action.Action())
                {
                    this.m_actionLog.AddAction(action);
                    this.FolderCreated.Raise<string>(action.Node.FullPath);
                }
            }
        }

        /// <summary>
        /// The mnu new bank_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewBank_Click(object sender, EventArgs e)
        {
            try
            {
                frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "Bank Name");
                if (input.HasBeenCancelled) return;
                string bankName = input.data;
                List<string> platforms = ((EditorTreeNode)this.m_treeView.LastSelectedNode).GetPlatforms();
                if (bankName != null && platforms.Count > 0)
                {
                    var action =
                        new AddBankAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                (EditorTreeNode)this.m_treeView.LastSelectedNode,
                                null,
                                null,
                                this.m_treeView,
                                this.m_actionLog,
                                bankName,
                                platforms,
                                null,
                                null));
                    if (action.Action())
                    {
                        this.m_actionLog.AddAction(action);
                        this.FolderCreated.Raise<string>(action.Node.FullPath);
                    }
                    else
                    {
                        ErrorManager.HandleInfo("Failed to add new bank - name may be in use: " + bankName);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleInfo(ex.ToString());
            }
        }

        /// <summary>
        /// The mnu load selected packs_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuLoadSelectedPacks_Click(object sender, EventArgs e)
        {
            foreach (var item in this.m_treeView.SelectedItems)
            {
                var packNode = item as PackNode;
                if (null != packNode && !packNode.IsLoaded)
                {
                    this.LoadPackChildNodes(packNode.PackName);
                }
            }
        }

        /// <summary>
        /// The mnu new pack_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewPack_Click(object sender, EventArgs e)
        {
            var platforms = new List<string>();
            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "Pack Name");
            if (input.HasBeenCancelled) return;
            string packName = input.data;
            if (packName != null)
            {
                try
                {
                    platforms.AddRange(Configuration.ActivePlatformSettings.Select(ps => ps.PlatformTag));

                    var action =
                        new AddPackAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                (EditorTreeNode)this.m_treeView.LastSelectedNode,
                                null,
                                null,
                                this.m_treeView,
                                this.m_actionLog,
                                packName,
                                platforms,
                                null,
                                null));
                    if (action.Action())
                    {
                        this.m_actionLog.AddAction(action);
                        this.FolderCreated.Raise<string>(action.Node.FullPath);
                    }
                }
                catch (Exception ex)
                {
                    ErrorManager.HandleInfo(ex.ToString());
                }
            }
        }

        /// <summary>
        /// The mnu new tag_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewTag_Click(object sender, EventArgs e)
        {
            var nodes = this.m_treeView.SelectedItems.Cast<EditorTreeNode>().ToList();

            var editor = new frmTagEditor(nodes, this.m_actionLog, TagType.Default, this.m_waveBrowser);
            editor.ShowDialog(this);
            editor.Dispose();
        }

        /// <summary>
        /// The mnu new voice tag_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewVoiceTag_Click(object sender, EventArgs e)
        {
            var editor = new frmTagEditor(this.m_treeView.LastSelectedNode as EditorTreeNode, this.m_actionLog, TagType.Voice, this.m_waveBrowser);
            editor.ShowDialog(this);
            editor.Dispose();
        }

        /// <summary>
        /// The mnu new wave folder_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewWaveFolder_Click(object sender, EventArgs e)
        {
            List<string> platforms = ((EditorTreeNode)this.m_treeView.LastSelectedNode).GetPlatforms();
            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "Wave Folder Name");
            if (input.HasBeenCancelled) return;
            string waveFolderName = input.data;
            if (waveFolderName != null && platforms.Count > 0)
            {
                var action =
                    new AddWaveFolderAction(
                        new ActionParams(
                            this.m_waveBrowser,
                            (EditorTreeNode)this.m_treeView.LastSelectedNode,
                            null,
                            null,
                            null,
                            this.m_actionLog,
                            waveFolderName,
                            platforms,
                            null,
                            null));
                if (action.Action())
                {
                    this.m_actionLog.AddAction(action);
                    this.FolderCreated.Raise<string>(action.Node.FullPath);
                }
            }
        }

        /// <summary>
        /// The mnu plugin_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuPlugin_Click(object sender, EventArgs e)
        {
            foreach (IRAVEWaveBrowserPlugin plugin in RaveInstance.PluginManager.WaveBrowserPlugins)
            {
                if (((ToolStripMenuItem)sender).Text == plugin.GetName())
                {
                    this.m_waveBrowser.GetEditorTreeView().GetTreeView().BeginUpdate();
                    plugin.Process(
                        (IWaveBrowser)this.Parent, this.m_actionLog, this.m_treeView.SelectedItems, new frmBusy());
                    this.m_waveBrowser.GetEditorTreeView().GetTreeView().EndUpdate();
                    break;
                }
            }
        }

        /// <summary>
        /// The mnu preset_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuPreset_Click(object sender, EventArgs e)
        {
            var mi = sender as ToolStripMenuItem;
            if (mi.Tag != null)
            {
                Dictionary<string, string> values = ((List<string>)mi.Tag).ToDictionary(s => s, s => mi.Text);
                var action =
                    new AddPresetAction(
                        new ActionParams(
                            this.m_waveBrowser,
                            (EditorTreeNode)this.m_treeView.LastSelectedNode,
                            null,
                            null,
                            null,
                            this.m_actionLog,
                            mi.Text,
                            (List<string>)mi.Tag,
                            values,
                            null));
                if (action.Action())
                {
                    this.m_actionLog.AddAction(action);
                }
            }
        }

        /// <summary>
        /// The mnu properties_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuProperties_Click(object sender, EventArgs e)
        {
            var editorTreeNode = this.m_treeView.LastSelectedNode as EditorTreeNode;

            if (null != editorTreeNode)
            {
                var infoForm = new frmNodeInfo(editorTreeNode);
                infoForm.Show(RaveInstance.ActiveWindow);
            }
            else
            {
                ErrorManager.HandleInfo("No additional properties available");
            }
        }

        /// <summary>
        /// The mnu right click_ opening.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuRightClick_Opening(object sender, CancelEventArgs e)
        {
            this.mnuRightClick.SuspendLayout();

            this.mnuNewPack.Visible = true;
            this.mnuLoadSelectedPacks.Visible = false;
            this.mnuBuildPack.Visible = false;
            this.mnuBuildBankPreview.Visible = false;
            this.mnuBuildPackSeperator.Visible = false;
            this.mnuNewBank.Visible = false;
            this.mnuNewBankFolder.Visible = false;
            this.mnuNewWaveFolder.Visible = false;
            this.mnuNewTag.Visible = false;
            this.mnuNewVoiceTag.Visible = false;
            this.mnuCreateReaperProject.Visible = false;
            this.mnuPreset.Visible = false;
            this.mnuSeperator1.Visible = false;
            this.mnuDelete.Visible = false;
            this.mnuPlatformSpecific.Visible = false;
            this.mnuSeperator2.Visible = false;
            this.mnuLock.Visible = false;
            this.mnuEditWave.Visible = false;
            this.mnuPlugins.Visible = false;
            this.mnuSeperator3.Visible = false;
            this.mnuProperties.Visible = false;
            this.mnuContents.Visible = false;
            this.mnuSearch.Visible = false;
            this.mnuBuiltPathToClipboard.Visible = false;
            this.mnuShowInExplorer.Visible = false;

            var lastSelectedNode = this.m_treeView.LastSelectedNode as EditorTreeNode;
            if (lastSelectedNode != null)
            {
                this.mnuNewPack.Visible = false;

                var unloadedPackCount = this.SelectedUnloadedPackCount();
                if (unloadedPackCount > 0)
                {
                    this.mnuLoadSelectedPacks.Text = unloadedPackCount == 1 ? "Load Pack" : "Load Packs";
                    this.mnuLoadSelectedPacks.Visible = true;
                }
                else
                {
                    this.mnuNewTag.Visible = true;
                    this.mnuNewVoiceTag.Visible = true;
                    this.mnuSeperator1.Visible = true;
                    this.mnuDelete.Visible = true;
                    this.mnuSeperator2.Visible = true;
                    this.mnuPlugins.Visible = true;
                    this.mnuProperties.Visible = true;
                    this.mnuContents.Visible = true;
                    this.mnuSearch.Visible = true;
                    this.mnuSeperator3.Visible = true;
                    this.mnuBuiltPathToClipboard.Visible = true;

                    if (Preset.Presets.Count > 0)
                    {
                        this.mnuPreset.Visible = true;
                    }

                    if (lastSelectedNode.IsNodeTypeLockable())
                    {
                        // not everything is lockable (i.e. waves)
                        this.mnuLock.Visible = true;
                    }

                    // disable if no items in submenu
                    if (this.mnuPlugins.DropDownItems.Count == 0)
                    {
                        this.mnuPlugins.Enabled = false;
                    }

                    // Ensure node is not a tag node as there is a special tag editor
                    // Also ensure that the node isn't already platform specific
                    if (lastSelectedNode.GetType() != typeof(TagNode) && lastSelectedNode.GetPlatforms().Count > 1)
                    {
                        this.mnuPlatformSpecific.Visible = true;
                    }

                    if (lastSelectedNode.GetType() == typeof(PackNode))
                    {
                        // enable the pack building items
                        this.mnuBuildPack.Visible = true;
                        this.mnuBuildPackSeperator.Visible = true;
                    }

                    if (m_treeView.SelectedItems.ToArray().Any(p => p is BankNode) && !string.IsNullOrEmpty(Configuration.PreviewFolder))
                    {
                        this.mnuBuildBankPreview.Visible = true;
                    }

                    if (lastSelectedNode.GetType() == typeof(PackNode)
                        || this.m_treeView.LastSelectedNode.GetType() == typeof(BankFolderNode))
                    {
                        // can only add banks + bank folders to a pack
                        this.mnuNewBank.Visible = true;
                        this.mnuNewBankFolder.Visible = true;
                    }
                    else if (lastSelectedNode.GetType() == typeof(BankNode)
                             || this.m_treeView.LastSelectedNode.GetType() == typeof(WaveFolderNode))
                    {
                        // can only add wave folders to a bank
                        this.mnuNewWaveFolder.Visible = true;
                    }
                    else if (lastSelectedNode.GetType() == typeof(WaveNode))
                    {
                        if (!lastSelectedNode.Text.EndsWith(".WAV", StringComparison.InvariantCultureIgnoreCase) &&
                            !lastSelectedNode.Text.EndsWith(".MID", StringComparison.InvariantCultureIgnoreCase))
                        {
                            this.mnuCreateReaperProject.Visible = true;
                        }
                        else
                        {
                            this.mnuEditWave.Visible = true;
                        }
                        this.mnuShowInExplorer.Visible = true;
                    }
                    else if (lastSelectedNode.GetType() == typeof(TagNode))
                    {
                        // dont allow tagging of tags
                        this.mnuNewTag.Visible = false;
                        this.mnuNewVoiceTag.Visible = false;
                    }

                }
            }

            this.mnuRightClick.ResumeLayout(true);
        }

        /// <summary>
        /// Get the number of unloaded packs in current tree selection.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private int SelectedUnloadedPackCount()
        {
            return (from object item in
                        this.m_treeView.SelectedItems
                    select item as PackNode).Count(
                    packNode => null != packNode && !packNode.IsLoaded);
        }

        /// <summary>
        /// The mnu search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuSearch_Click(object sender, EventArgs e)
        {
            var waveSearch = new frmWaveSearch((EditorTreeNode)this.m_treeView.LastSelectedNode, this.m_waveBrowser);
            waveSearch.Show(RaveInstance.ActiveWindow);
        }

        /// <summary>
        /// The msg_ on select object.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        private void msg_OnSelectObject(object obj)
        {
            var o = obj as IObjectInstance;
            if (o != null)
            {
                this.FindObject.Raise(o);
            }
        }

        /// <summary>
        /// The out put_ on select object.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        private void outPut_OnSelectObject(object obj)
        {
            var etn = obj as EditorTreeNode;
            if (etn != null)
            {
                this.m_treeView.BeginUpdate();
                etn.EnsureVisible();
                this.m_treeView.LastSelectedNode = etn;
                this.m_treeView.EndUpdate();
            }
        }

        #endregion

        //private void mnuCreateReaperProject_Click(object sender, EventArgs e)
        //{
        //    WaveNode waveNode = this.m_treeView.LastSelectedNode as WaveNode;
        //    if (null == waveNode)
        //    {
        //        return;
        //    }
        //    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

        //    saveFileDialog1.Filter = "Reaper Project Files (*.rpp)|*.rpp";
        //    saveFileDialog1.FileName = waveNode.Text;
        //    saveFileDialog1.RestoreDirectory = true;
        //    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
        //    {


        //        var inputFile = Path.Combine(Configuration.PlatformWavePath, GetTextPath(waveNode));
        //        RaveInstance.AssetManager.GetLatest(inputFile, false);

        //        EdlToReaper.EdlToReaper edlToReaper =
        //            new EdlToReaper.EdlToReaper(inputFile, saveFileDialog1.FileName);
        //        Task.Factory.StartNew(() => edlToReaper.ConvertToReaperProject());
        //    }
        //}

        private string GetTextPath(WaveNode waveNode)
        {
            string filename = waveNode.GetObjectPath();
            if (!Path.HasExtension(waveNode.GetObjectPath()))
                filename += ".TXT";
            return filename;
        }

    }

}
