namespace Rave.WaveBrowser.WaveBrowser
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Utils;
    using Rave.Utils.Popups;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.UserActions;
    using Rave.WaveBrowser.Validators;

    /// <summary>
    ///   This class logs all user actions, enabling undo and commit functionality
    /// </summary>
    public class ActionLog : IActionLog
    {
        private readonly List<IUserAction> m_redoLog;
        private readonly List<IUserAction> m_undoLog;

        public ActionLog()
        {
            this.m_undoLog = new List<IUserAction>();
            this.m_redoLog = new List<IUserAction>();
        }

        #region IActionLog Members

        public void AddAction(IUserAction action)
        {
            this.m_undoLog.Add(action);
        }

        public void Reset()
        {
            this.m_undoLog.Clear();
            this.m_redoLog.Clear();
        }

        public IUserAction GetUndoAction()
        {
            if (this.m_undoLog.Count > 0)
            {
                return this.m_undoLog[this.m_undoLog.Count - 1];
            }
            return null;
        }

        public IUserAction GetRedoAction()
        {
            if (this.m_redoLog.Count > 0)
            {
                return this.m_redoLog[this.m_redoLog.Count - 1];
            }
            return null;
        }

        public void UndoLastAction()
        {
            if (this.m_undoLog.Count > 0)
            {
                var action = this.m_undoLog[this.m_undoLog.Count - 1];
                if (action.Undo())
                {
                    this.m_undoLog.Remove(action);
                    if (action.GetType() !=
                        typeof(BulkAction))
                    {
                        this.m_redoLog.Add(action);
                    }
                }
            }
        }

        public void RedoNextAction()
        {
            if (this.m_redoLog.Count > 0)
            {
                var action = this.m_redoLog[this.m_redoLog.Count - 1];
                if (action.Action())
                {
                    this.m_redoLog.Remove(action);
                    this.m_undoLog.Add(action);
                }
            }
        }

        public bool Commit()
        {
            if (!this.ValidateActions())
            {
                return false;
            }

            var succeeded = true;
            using (var busy = new frmBusy())
            {
                busy.SetStatusText("Working...");
                busy.Show();

                int count = 0;
                foreach (var action in this.m_undoLog)
                {
                    count += 1;
                    busy.SetStatusText("Commiting: " + action.GetSummary());
                    busy.SetProgress((int)((count / (float)this.m_undoLog.Count) * 100.0f));
                    Application.DoEvents();

                    try
                    {
                        if (!action.Commit())
                        {
                            succeeded = false;
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorManager.HandleError(e);
                        succeeded = false;
                        break;
                    }
                }
                busy.FinishedWorking();
            }
            return succeeded;
        }

        public string GetCommitSummary()
        {
            var sb = new StringBuilder();
            foreach (var action in this.m_undoLog)
            {
                sb.AppendFormat("{0}\r\n", action.GetSummary());
            }
            return sb.ToString();
        }

        public int GetCommitLogCount()
        {
            return this.m_undoLog.Count;
        }

        public IUserAction SearchForPackDeletion(string packPath)
        {
            foreach (var action in this.m_undoLog)
            {
                var deletePackAction = action as DeletePackAction;
                var ba = action as BulkAction;
                if (deletePackAction != null)
                {
                    if (deletePackAction.GetAssetPath() != null)
                    {
                        return deletePackAction;
                    }
                }
                else if (ba != null)
                {
                    return ba.FindPackDeletion(packPath);
                }
            }
            return null;
        }

        public IUserAction SearchForBankDeletion(string bankPath)
        {
            foreach (var action in this.m_undoLog)
            {
                var deleteBankAction = action as DeleteBankAction;
                var ba = action as BulkAction;
                if (deleteBankAction != null)
                {
                    if (deleteBankAction.GetAssetPath() == bankPath)
                    {
                        return deleteBankAction;
                    }
                }
                else if (ba != null)
                {
                    return ba.FindBankDeletion(bankPath);
                }
            }
            return null;
        }

        public IUserAction SearchForWaveDeletion(string wavePath)
        {
            foreach (var action in this.m_undoLog)
            {
                var deleteWaveAction = action as DeleteWaveAction;
                var bulkAction = action as BulkAction;
                if (deleteWaveAction != null)
                {
                    if (deleteWaveAction.GetAssetPath() == wavePath)
                    {
                        return deleteWaveAction;
                    }
                }
                else if (bulkAction != null)
                {
                    return bulkAction.FindWaveDeletion(wavePath);
                }
            }
            return null;
        }

        public IUserAction SearchForPackAdd(string packPath)
        {
            foreach (var action in this.m_undoLog)
            {
                var addPackAction = action as AddPackAction;
                var bulkAction = action as BulkAction;

                if (addPackAction != null)
                {
                    if (addPackAction.GetAssetPath() == packPath)
                    {
                        return addPackAction;
                    }
                }
                else if (bulkAction != null)
                {
                    return bulkAction.FindAddPack(packPath);
                }
            }
            return null;
        }

        public IUserAction SearchForBankAdd(string bankPath)
        {
            foreach (var action in this.m_undoLog)
            {
                var addBankAction = action as AddBankAction;
                var bulkAction = action as BulkAction;

                if (addBankAction != null)
                {
                    if (addBankAction.GetAssetPath() == bankPath)
                    {
                        return addBankAction;
                    }
                }
                else if (bulkAction != null)
                {
                    return bulkAction.FindAddBank(bankPath);
                }
            }
            return null;
        }

        public IUserAction SearchForWaveAdd(string wavePath)
        {
            foreach (var action in this.m_undoLog)
            {
                var addWaveAction = action as AddWaveAction;
                var bulkAction = action as BulkAction;
                if (addWaveAction != null)
                {
                    if (addWaveAction.GetAssetPath().Equals(wavePath, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return addWaveAction;
                    }
                }
                else if (bulkAction != null)
                {
                    return bulkAction.FindAddWave(wavePath);
                }
            }
            return null;
        }

        public IUserAction SearchForAction(string path)
        {
            foreach (var action in this.m_undoLog)
            {
                if (action.GetAssetPath() != null && action.GetAssetPath().Contains(path))
                {
                    return action;
                }

                var bulkAction = action as BulkAction;
                if (bulkAction != null)
                {
                    return bulkAction.FindAction(path);
                }

                var moveAction = action as MoveWaveAction;
                if (moveAction != null)
                {
                    if (moveAction.DeleteWaveAction.GetAssetPath() == path) return moveAction;
                    if (moveAction.AddWaveAction.GetAssetPath() == path) return moveAction;
                }
            }
            return null;
        }

        public IEnumerable<IUserAction> SearchForActions(BankNode bankNode)
        {
            List<IUserAction> userActions = new List<IUserAction>();

            foreach (IUserAction action in this.m_undoLog)
            {
                BulkAction bulkAction = action as BulkAction;
                if (bulkAction != null)
                {
                    userActions.AddRange(bulkAction.FindActions(bankNode));
                    continue;
                }

                MoveWaveAction moveAction = action as MoveWaveAction;
                if (moveAction != null)
                {
                    if (bankNode.FullPath.Contains(moveAction.DeleteWaveAction.Node.FullPath)) userActions.Add(moveAction);
                    else if (bankNode.FullPath.Contains(moveAction.AddWaveAction.Node.FullPath)) userActions.Add(moveAction);
                    continue;
                }

                if (action is AddTagAction || action is DeleteTagAction || action is ModifyTagAction)
                {
                    if (EditorTreeNode.FindParentPackNode(action.Node) == EditorTreeNode.FindParentPackNode(bankNode))
                    {
                        userActions.Add(action);
                    }
                    continue;
                }


                if (EditorTreeNode.FindParentBankNode(action.Node) == bankNode)
                {
                    userActions.Add(action);
                    continue;
                }
                if (action.GetAssetPath().Contains(bankNode.GetBuiltPath()))
                {
                    userActions.Add(action);
                }

            }

            return userActions;
        }

        #endregion

        /// <summary>
        /// Validate the pending actions.
        /// </summary>
        /// <returns>
        /// Indication of whether pending changes are valid <see cref="bool"/>.
        /// </returns>
        private bool ValidateActions()
        {
            var errors = new StringBuilder();
            var warnings = new StringBuilder();

            var bankNodes = new List<BankNode>();
            foreach (var action in this.m_undoLog)
            {
                if (!AddBankNode(action.ActionParameters, bankNodes))
                {
                    // Check if action is MoveWaveAction
                    var moveWaveAction = action as MoveWaveAction;
                    if (null != moveWaveAction)
                    {
                        AddBankNode(moveWaveAction.AddWaveAction.ActionParameters, bankNodes);
                        AddBankNode(moveWaveAction.DeleteWaveAction.ActionParameters, bankNodes);
                    }
                }

                if (action is ChangeWaveFileAction)
                {
                    (action as ChangeWaveFileAction).Validate(errors, warnings);
                }
            }

            var voiceWaveValidator = new VoiceWaveValidator();
            voiceWaveValidator.Validate(bankNodes, errors, warnings);

            if (warnings.Length > 0)
            {
                var msg = string.Format(
                    "The following warning(s) occurred:{0}{1}{2}Continue with submit?",
                    Environment.NewLine + Environment.NewLine,
                    warnings,
                    Environment.NewLine + Environment.NewLine);
                if (MessageBox.Show(
                    RaveInstance.ActiveWindow,
                    msg,
                    "Continue Submit?",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Warning) == DialogResult.Cancel)
                {
                    return false;
                }
            }

            if (errors.Length > 0)
            {
                ErrorManager.HandleError(errors.ToString());
                return false;
            }

            return true;
        }

        /// <summary>
        /// Add bank node from action parameters.
        /// </summary>
        /// <param name="actionParams">
        /// The action parameters.
        /// </param>
        /// <param name="bankNodes">
        /// The bank nodes.
        /// </param>
        /// <returns>
        /// Indication of whether bank node was successfully added <see cref="bool"/>.
        /// </returns>
        private static bool AddBankNode(IActionParams actionParams, IList<BankNode> bankNodes)
        {
            if (null == actionParams)
            {
                return false;
            }

            var parentNode = actionParams.ParentNode;
            if (null != parentNode)
            {
                var bankNode = EditorTreeNode.FindParentBankNode(parentNode);
                if (null != bankNode)
                {
                    bankNodes.Add(bankNode);
                    return true;
                }
            }

            return false;
        }
    }
}