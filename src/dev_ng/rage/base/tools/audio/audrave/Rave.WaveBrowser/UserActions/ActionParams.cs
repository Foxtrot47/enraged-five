// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActionParams.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The action params.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.WaveBrowser.UserActions
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    /// The action parameters.
    /// </summary>
    public class ActionParams : IActionParams
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionParams"/> class.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="sourcePath">
        /// The source path.
        /// </param>
        /// <param name="treeView">
        /// The tree view.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="platforms">
        /// The platforms.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="actions">
        /// The actions.
        /// </param>
        /// <param name="otherFileExts">
        /// The other file extensions.
        /// </param>
        public ActionParams(
            IWaveBrowser waveBrowser, 
            EditorTreeNode parentNode, 
            EditorTreeNode node, 
            string sourcePath,
            TreeView treeView, 
            IActionLog actionLog, 
            string name, 
            List<string> platforms, 
            Dictionary<string, string> value, 
            ArrayList actions, 
            IList<string> otherFileExts = null)
        {
            this.WaveBrowser = waveBrowser;
            this.ParentNode = parentNode;
            this.Node = node;
            this.SourcePath = sourcePath;
            this.TreeView = treeView;
            this.ActionLog = actionLog;
            this.Name = name;
            this.Platforms = platforms;
            this.Value = value;
            this.Actions = actions;
            this.OtherFileExts = otherFileExts;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the action log.
        /// </summary>
        public IActionLog ActionLog { get; private set; }

        /// <summary>
        /// Gets the actions.
        /// </summary>
        public ArrayList Actions { get; private set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the node.
        /// </summary>
        public EditorTreeNode Node { get; private set; }

        /// <summary>
        /// Gets the parent node.
        /// </summary>
        public EditorTreeNode ParentNode { get; private set; }

        /// <summary>
        /// Gets the platforms.
        /// </summary>
        public List<string> Platforms { get; private set; }

        /// <summary>
        /// Gets the source path.
        /// </summary>
        public string SourcePath { get; set; }

        /// <summary>
        /// Gets the tree view.
        /// </summary>
        public TreeView TreeView { get; private set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        public Dictionary<string, string> Value { get; private set; }

        /// <summary>
        /// Gets the wave browser.
        /// </summary>
        public IWaveBrowser WaveBrowser { get; private set; }

        /// <summary>
        /// Gets the other file extensions.
        /// </summary>
        public IList<string> OtherFileExts { get; private set; }

        #endregion
    }
}