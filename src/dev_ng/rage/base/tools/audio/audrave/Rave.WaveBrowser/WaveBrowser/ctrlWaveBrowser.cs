// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ctrlWaveBrowser.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for ctrlWaveBrowser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Runtime.InteropServices;
using Rave.WavePlayer.WaveAuditionPopup;

namespace Rave.WaveBrowser.WaveBrowser
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Windows;
    using System.Windows.Forms;
    using System.Xml.Linq;

    using rage;
    using rage.ToolLib;

    using Rave.AssetManager.Infrastructure.Interfaces;
    using Rave.Controls.Forms;
    using Rave.Instance;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Types.Waves;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.UserActions;
    using Rave.WaveBrowser.WaveBrowser.Popups;
    using Rave.WaveBrowser.WaveBrowser.TagEditor;
    using Rave.WaveBrowser.WaveSlotEditor;

    using MessageBox = System.Windows.MessageBox;

    /// <summary>
    /// The ctrl wave browser.
    /// </summary>
    public class ctrlWaveBrowser : DockableWFControl, IWaveBrowser
    {
        #region Constants

        /// <summary>
        /// All platforms string.
        /// </summary>
        public const string ALL_PLATFORMS = "All";

        /// <summary>
        /// The no plugin string.
        /// </summary>
        private const string NO_PLUGIN = "No Plugin";

        #endregion

        #region Fields

        /// <summary>
        /// The components.
        /// </summary>
        private IContainer components;

        /// <summary>
        /// The m_action log.
        /// </summary>
        private IActionLog m_actionLog;

        /// <summary>
        /// The m_active platform.
        /// </summary>
        private ComboBox m_activePlatform;

        /// <summary>
        /// The m_collapse all tool strip menu item.
        /// </summary>
        private ToolStripMenuItem m_collapseAllToolStripMenuItem;

        /// <summary>
        /// The m_label 1.
        /// </summary>
        private Label m_label1;

        /// <summary>
        /// The m_label 2.
        /// </summary>
        private Label m_label2;

        /// <summary>
        /// The m_label 3.
        /// </summary>
        private Label m_label3;

        /// <summary>
        /// The m_mnu edit redo.
        /// </summary>
        private System.Windows.Controls.MenuItem m_mnuEditRedo;

        /// <summary>
        /// The m_mnu edit undo.
        /// </summary>
        private System.Windows.Controls.MenuItem m_mnuEditUndo;

        /// <summary>
        /// The m_mnu get latest.
        /// </summary>
        private System.Windows.Controls.MenuItem m_mnuGetLatest;

        /// <summary>
        /// The m_panel 1.
        /// </summary>
        private Panel m_panel1;

        /// <summary>
        /// The m_pending wave lists.
        /// </summary>
        private IPendingWaveLists m_pendingWaveLists;

        /// <summary>
        /// The m_plugin chooser.
        /// </summary>
        private ComboBox m_pluginChooser;

        /// <summary>
        /// The m_wave browser details view.
        /// </summary>
        private ctrlWaveEditorNodeDetails m_waveBrowserDetailsView;

        /// <summary>
        /// The m_wave browser tree view.
        /// </summary>
        private ctrlWaveEditorTreeView m_waveBrowserTreeView;

        /// <summary>
        /// The mnu wave menu.
        /// </summary>
        private ContextMenuStrip mnuWaveMenu;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="ctrlWaveBrowser"/> class.
        /// </summary>
        static ctrlWaveBrowser()
        {
            ActivePlatform = ALL_PLATFORMS;
        }


        #endregion

        #region Public Events

        /// <summary>
        ///     The changelist modified.
        /// </summary>
        public event Action ChangelistModified;

        /// <summary>
        /// The get latest.
        /// </summary>
        public event Action<string> GetLatest;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the active platform.
        /// </summary>
        public static string ActivePlatform { get; private set; }

        /// <summary>
        /// Gets the menu.
        /// </summary>
        public ContextMenuStrip Menu
        {
            get
            {
                return this.mnuWaveMenu;
            }
        }

        /// <summary>
        /// Gets the rave asset manager.
        /// </summary>
        public IRaveAssetManager RaveAssetManager
        {
            get
            {
                return RaveInstance.RaveAssetManager;
            }
        }

        /// <summary>
        /// Gets the platform settings.
        /// </summary>
        public ArrayList PlatformSettings
        {
            get
            {
                return Configuration.PlatformSettings;
            }
        }

        /// <summary>
        /// Gets the wave gesture context list.
        /// </summary>
        public string WaveGestureContextList
        {
            get
            {
                return Configuration.WaveGestureContextList;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The build slot refs.
        /// </summary>
        public static void BuildSlotRefs()
        {
            string waveSlotFile = Configuration.WaveSlotFile;

            if (RaveInstance.AssetManager.ExistsAsAsset(waveSlotFile))
            {
                try
                {
                    RaveInstance.AssetManager.GetLatestForceErrors(waveSlotFile);
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                }

                XDocument doc = XDocument.Load(waveSlotFile);
                foreach (XElement slotNode in doc.Descendants("Slot"))
                {
                    foreach (XElement loadableNode in slotNode.Elements("Loadable"))
                    {
                        string key = loadableNode.Attribute("name").Value;
                        if (WaveManager.WaveSlotRefs.ContainsKey(key))
                        {
                            WaveManager.WaveSlotRefs[key].Add(slotNode.Attribute("name").Value);
                        }
                        else
                        {
                            WaveManager.WaveSlotRefs.Add(key, new List<string> { slotNode.Attribute("name").Value });
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The commit changes.
        /// </summary>
        /// <param name="showDialogue">
        /// The show dialogue.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CommitChanges(bool showDialogue)
        {
            if (this.m_pendingWaveLists.ArePendingWavelistsCheckedOut())
            {
                ErrorManager.HandleInfo(
                    "One or more of the pending wave lists is checked out. \r\nEnsure that pending lists are checked in before wave commits");
                return false;
            }

            if (this.m_actionLog != null && this.m_actionLog.GetCommitLogCount() > 0)
            {
                using (var frm = new frmCommit(this.m_actionLog))
                {
                    if (!showDialogue || frm.ShowDialog(this) == DialogResult.OK)
                    {
                        if (!this.m_pendingWaveLists.LoadLatestWaveLists())
                        {
                            ErrorManager.HandleInfo("Couldn't load latest wave lists.");
                            return false;
                        }

                        this.m_waveBrowserTreeView.GetTreeView().BeginUpdate();
                        if (this.m_actionLog.Commit())
                        {
                            this.m_waveBrowserTreeView.GetTreeView().EndUpdate();
                            bool locked = this.m_pendingWaveLists.LockPendingWaveList();
                            while (!locked)
                            {
                                if (ErrorManager.HandleQuestion(
                                    "Couldn't lock pending wave lists",
                                    MessageBoxButtons.RetryCancel,
                                    DialogResult.Cancel) == DialogResult.Retry)
                                {
                                    locked = this.m_pendingWaveLists.LockPendingWaveList();
                                }
                                else
                                {
                                    RaveInstance.RaveAssetManager.WaveChangeList.Revert(true);
                                    RaveInstance.RaveAssetManager.WaveChangeList = null;
                                    return false;
                                }
                            }

                            if (this.m_pendingWaveLists.SerialisePendingWaveList())
                            {
                                if (RaveInstance.RaveAssetManager.WaveChangeList.HasShelvedAssets())
                                {
                                    ErrorManager.HandleInfo("There are shelved files in the changelist. Please delete or move them via P4V before commiting. Nothing has been checked in.");
                                    return false;
                                }
                                try
                                {
                                    this.m_waveBrowserTreeView.UnlockAllNodes();
                                    string submittedChangelist;
                                    RaveInstance.RaveAssetManager.WaveChangeList.Submit(frm.GetPerforceChangelistDescription(), out submittedChangelist);
                                    this.ChangelistModified.Raise();
                                    RaveInstance.RaveAssetManager.WaveChangeList = null;
                                    this.m_actionLog.Reset();
                                    this.m_waveBrowserTreeView.LoadWaveLists();
                                    ErrorManager.HandleInfo("Commit Successful! Changelist Number: " + submittedChangelist);
                                    return true;
                                }
                                catch (Exception e)
                                {
                                    ErrorManager.HandleError(e);
                                    ErrorManager.HandleInfo("Error commiting wave changelist, files will now revert");
                                    RaveInstance.RaveAssetManager.WaveChangeList.Revert(true);
                                    RaveInstance.RaveAssetManager.WaveChangeList = null;
                                }
                            }
                            else
                            {
                                ErrorManager.HandleInfo("Failed to serialize pending wave lists");
                                RaveInstance.RaveAssetManager.WaveChangeList.Revert(true);
                                RaveInstance.RaveAssetManager.WaveChangeList = null;
                            }
                        }
                        else
                        {
                            this.m_waveBrowserTreeView.GetTreeView().EndUpdate();
                            ErrorManager.HandleInfo(
                                "Failed to commit actions from the action log. Nothing has been checked in.");
                            RaveInstance.RaveAssetManager.WaveChangeList.Revert(true);
                            RaveInstance.RaveAssetManager.WaveChangeList = null;
                        }

                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// The create action.
        /// </summary>
        /// <param name="actionType">
        /// The action type.
        /// </param>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        /// <returns>
        /// The <see cref="IUserAction"/>.
        /// </returns>
        public IUserAction CreateAction(ActionType actionType, IActionParams parameters)
        {
            switch (actionType)
            {
                case ActionType.AddBank:
                    return new AddBankAction(parameters);

                case ActionType.AddBankFolder:
                    return new AddBankFolderAction(parameters);

                case ActionType.AddPack:
                    return new AddPackAction(parameters);

                case ActionType.AddTag:
                    return new AddTagAction(parameters);

                case ActionType.AddWave:
                    return new AddWaveAction(parameters);

                case ActionType.AddWaveFolder:
                    return new AddWaveFolderAction(parameters);

                case ActionType.ChangeWave:
                    return new ChangeWaveAction(parameters);

                case ActionType.ChangeWaveFile:
                    return new ChangeWaveFileAction(parameters);

                case ActionType.DeleteBank:
                    return new DeleteBankAction(parameters);

                case ActionType.DeleteBankFolder:
                    return new DeleteBankFolderAction(parameters);

                case ActionType.DeletePack:
                    return new DeletePackAction(parameters);

                case ActionType.DeleteTag:
                    return new DeleteTagAction(parameters);

                case ActionType.DeleteWaveFolder:
                    return new DeleteWaveFolderAction(parameters);

                case ActionType.EditWave:
                    return new EditWaveAction(parameters);

                case ActionType.ModifyTag:
                    return new ModifyTagAction(parameters);

                case ActionType.BulkAction:
                    return new BulkAction(parameters);

                case ActionType.DeleteWave:
                    return new DeleteWaveAction(parameters);

                case ActionType.Dummy:
                    return new DummyAction();

                default:
                    return null;
            }
        }

        /// <summary>
        /// The create pending wave lists.
        /// </summary>
        public void CreatePendingWaveLists()
        {
            this.m_pendingWaveLists = new PendingWaveLists(this);
        }

        /// <summary>
        /// The get action log.
        /// </summary>
        /// <returns>
        /// The <see cref="IActionLog"/>.
        /// </returns>
        public IActionLog GetActionLog()
        {
            return this.m_actionLog;
        }

        /// <summary>
        /// The get active platform.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetActivePlatform()
        {
            return ActivePlatform;
        }

        /// <summary>
        /// The get editor tree view.
        /// </summary>
        /// <returns>
        /// The <see cref="IWaveEditorTreeView"/>.
        /// </returns>
        public IWaveEditorTreeView GetEditorTreeView()
        {
            return this.m_waveBrowserTreeView;
        }

        /// <summary>
        /// The get pending wave lists.
        /// </summary>
        /// <returns>
        /// The <see cref="IPendingWaveLists"/>.
        /// </returns>
        public IPendingWaveLists GetPendingWaveLists()
        {
            return this.m_pendingWaveLists;
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Init()
        {
            this.InitializeComponent();

            this.m_actionLog = new ActionLog();
            this.m_waveBrowserTreeView.Init(this, this.m_actionLog);
            this.m_waveBrowserTreeView.MouseOverNode += this.WaveBrowserTreeView_MouseOverNode;
            this.m_waveBrowserTreeView.DoubleClickNode += this.WaveBrowserTreeView_DoubleClickNode;

            if (RaveInstance.PluginManager != null)
            {
                // build right click menu
                this.m_waveBrowserTreeView.PopulatePluginMenu();

                // build a list of plugins in the wavebrowser plugin drop-down box
                foreach (IRAVEWaveImportPlugin plugin in RaveInstance.PluginManager.WaveImportPlugins)
                {
                    this.m_pluginChooser.Items.Add(plugin);
                }
            }

            this.m_pluginChooser.SelectedItem = NO_PLUGIN;

            this.m_activePlatform.Items.Add(ALL_PLATFORMS);
            foreach (PlatformSetting ps in Configuration.ActivePlatformSettings)
            {
                this.m_activePlatform.Items.Add(ps.PlatformTag);
            }

            this.m_activePlatform.Text = ALL_PLATFORMS;
            this.m_waveBrowserDetailsView.Init();

            // PreviewBuilding.DeletePreviewFolderContent();
            return true;
        }


        /// <summary>
        /// Load the wave lists (if the wave tree is visible).
        /// </summary>
        /// <param name="runMetadataBuild">
        /// Run metadata build flag.
        /// </param>
        public void LoadWaveLists(bool runMetadataBuild = true)
        {
            this.m_waveBrowserTreeView.LoadWaveLists(runMetadataBuild);
        }

        /// <summary>
        /// The init main menu.
        /// </summary>
        /// <param name="waveBrowserMenu">
        /// The wave browser menu.
        /// </param>
        /// <param name="buildMenu">
        /// The build menu.
        /// </param>
        public void InitMainMenu(System.Windows.Controls.MenuItem waveBrowserMenu, System.Windows.Controls.MenuItem buildMenu)
        {
            waveBrowserMenu.SubmenuOpened += this.waveBrowserMenu_SubmenuOpened;

            // Build Wave Broswer Menu
            var commitMenuItem = new System.Windows.Controls.MenuItem { Header = "_Commit Changes" };
            commitMenuItem.Click += this.mnuCommitChanges_Click;
            waveBrowserMenu.Items.Add(commitMenuItem);
            waveBrowserMenu.Items.Add(new System.Windows.Controls.Separator());

            this.m_mnuEditUndo = new System.Windows.Controls.MenuItem { Header = "Undo" };
            this.m_mnuEditUndo.Click += this.mnuEditUndo_Click;

            // m_mnuEditUndo.Shortcut = Shortcut.CtrlZ;
            // m_mnuEditUndo.ShowShortcut = true;
            this.m_mnuEditRedo = new System.Windows.Controls.MenuItem { Header = "Redo" };
            this.m_mnuEditRedo.Click += this.mnuEditRedo_Click;

            // m_mnuEditRedo.Shortcut = Shortcut.CtrlY;
            // m_mnuEditRedo.ShowShortcut = true;
            waveBrowserMenu.Items.Add(this.m_mnuEditUndo);
            waveBrowserMenu.Items.Add(this.m_mnuEditRedo);

            waveBrowserMenu.Items.Add(new System.Windows.Controls.Separator());
            var manageWaveSlotsItem = new System.Windows.Controls.MenuItem { Header = "Manage Wave Slots" };
            manageWaveSlotsItem.Click += this.mnuManageWaveSlots_Click;
            waveBrowserMenu.Items.Add(manageWaveSlotsItem);

            this.m_mnuGetLatest = new System.Windows.Controls.MenuItem { Header = "Get Latest Runtime Data" };
            var mnuGetLatest = new System.Windows.Controls.MenuItem { Header = "All Platforms" };
            mnuGetLatest.Click += this.mnuGetLatest_Click;
            this.m_mnuGetLatest.Items.Add(mnuGetLatest);
            this.m_mnuGetLatest.Items.Add(new System.Windows.Controls.Separator());

            foreach (PlatformSetting ps in Configuration.ActivePlatformSettings)
            {
                if (ps.BuildEnabled)
                {
                    var pm = new System.Windows.Controls.MenuItem { Header = ps.PlatformTag };
                    pm.Click += this.mnuGetLatest_Click;
                    this.m_mnuGetLatest.Items.Add(pm);
                }
            }

            buildMenu.Items.Add(this.m_mnuGetLatest);

            waveBrowserMenu.Items.Add(new System.Windows.Controls.Separator());
        }

        /// <summary>
        /// The on close.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool OnClose()
        {
            bool succeeded = this.PossiblyCommitChanges();
            if (RaveInstance.RaveAssetManager.WaveChangeList != null)
            {
                RaveInstance.RaveAssetManager.WaveChangeList.Revert(true);
            }

            return succeeded;
        }

        /// <summary>
        /// The mnu get latest_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        public void mnuGetLatest_Click(object sender, EventArgs e)
        {
            var menuItem = sender as System.Windows.Controls.MenuItem;
            this.GetLatest.Raise(menuItem.Header.ToString());
        }

        /// <summary>
        /// The format game string.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string FormatGameString(string input)
        {
            return RaveUtils.FormatGameString(input);
        }

        /// <summary>
        /// Handle an error.
        /// </summary>
        /// <param name="ex">
        /// The ex.
        /// </param>
        public void HandleError(Exception ex)
        {
            ErrorManager.HandleError(ex);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The update view.
        /// </summary>
        /// <param name="changeListNo">
        /// The change list no.
        /// </param>
        internal void UpdateView(string changeListNo)
        {
            this.m_waveBrowserTreeView.LoadWaveLists();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// The collapse.
        /// </summary>
        /// <param name="treeNodeCollection">
        /// The tree node collection.
        /// </param>
        private static void Collapse(TreeNodeCollection treeNodeCollection)
        {
            foreach (TreeNode tn in treeNodeCollection)
            {
                Collapse(tn.Nodes);
                tn.Collapse();
            }
        }

        /// <summary>
        /// The active platform_ selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ActivePlatform_SelectedIndexChanged(object sender, EventArgs e)
        {
            ActivePlatform = this.m_activePlatform.Text;

            this.m_waveBrowserTreeView.OnActivePlatformChanged();
        }

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_activePlatform = new System.Windows.Forms.ComboBox();
            this.m_panel1 = new System.Windows.Forms.Panel();
            this.m_label3 = new System.Windows.Forms.Label();
            this.m_label2 = new System.Windows.Forms.Label();
            this.m_pluginChooser = new System.Windows.Forms.ComboBox();
            this.m_label1 = new System.Windows.Forms.Label();
            this.m_waveBrowserTreeView = new Rave.WaveBrowser.WaveBrowser.ctrlWaveEditorTreeView();
            this.m_waveBrowserDetailsView = new Rave.WaveBrowser.WaveBrowser.Popups.ctrlWaveEditorNodeDetails();
            this.mnuWaveMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_collapseAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_panel1.SuspendLayout();
            this.mnuWaveMenu.SuspendLayout();
            this.SuspendLayout();

            // m_ActivePlatform
            this.m_activePlatform.Anchor =
                (System.Windows.Forms.AnchorStyles)
                (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right));
            this.m_activePlatform.Location = new System.Drawing.Point(85, 6);
            this.m_activePlatform.Name = "m_activePlatform";
            this.m_activePlatform.Size = new System.Drawing.Size(162, 21);
            this.m_activePlatform.TabIndex = 4;
            this.m_activePlatform.SelectedIndexChanged +=
                new System.EventHandler(this.ActivePlatform_SelectedIndexChanged);

            // panel1
            this.m_panel1.Controls.Add(this.m_activePlatform);
            this.m_panel1.Controls.Add(this.m_label3);
            this.m_panel1.Controls.Add(this.m_label2);
            this.m_panel1.Controls.Add(this.m_pluginChooser);
            this.m_panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_panel1.Location = new System.Drawing.Point(0, 0);
            this.m_panel1.Name = "m_panel1";
            this.m_panel1.Size = new System.Drawing.Size(250, 66);
            this.m_panel1.TabIndex = 5;

            // label3
            this.m_label3.AutoSize = true;
            this.m_label3.Location = new System.Drawing.Point(3, 37);
            this.m_label3.Name = "m_label3";
            this.m_label3.Size = new System.Drawing.Size(110, 13);
            this.m_label3.TabIndex = 24;
            this.m_label3.Text = "Import Plugin:";

            // label2
            this.m_label2.Location = new System.Drawing.Point(3, 9);
            this.m_label2.Name = "m_label2";
            this.m_label2.Size = new System.Drawing.Size(110, 21);
            this.m_label2.TabIndex = 5;
            this.m_label3.AutoSize = true;
            this.m_label2.Text = "Active Platform:";

            // m_PluginChooser
            this.m_pluginChooser.Anchor = (AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right;
            this.m_pluginChooser.FormattingEnabled = true;
            this.m_pluginChooser.Items.AddRange(new object[] { "No Plugin" });
            this.m_pluginChooser.Location = new System.Drawing.Point(85, 34);
            this.m_pluginChooser.Name = "m_pluginChooser";
            this.m_pluginChooser.Size = new System.Drawing.Size(162, 21);
            this.m_pluginChooser.TabIndex = 23;
            this.m_pluginChooser.SelectedIndexChanged += new System.EventHandler(
                this.PluginChooser_SelectedIndexChanged);

            // label1
            this.m_label1.Location = new System.Drawing.Point(0, 0);
            this.m_label1.Name = "m_label1";
            this.m_label1.Size = new System.Drawing.Size(100, 23);
            this.m_label1.TabIndex = 0;

            // m_WaveBrowserTreeView
            this.m_waveBrowserTreeView.AllowDrop = true;
            this.m_waveBrowserTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_waveBrowserTreeView.Location = new System.Drawing.Point(0, 66);
            this.m_waveBrowserTreeView.Name = "m_waveBrowserTreeView";
            this.m_waveBrowserTreeView.Size = new System.Drawing.Size(250, 217);
            this.m_waveBrowserTreeView.TabIndex = 0;
            this.m_waveBrowserTreeView.Visible = true;

            // m_WaveBrowserDetailsView
            this.m_waveBrowserDetailsView.BackColor = System.Drawing.SystemColors.Control;
            this.m_waveBrowserDetailsView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_waveBrowserDetailsView.Location = new System.Drawing.Point(0, 283);
            this.m_waveBrowserDetailsView.MaximumSize = new System.Drawing.Size(0, 117);
            this.m_waveBrowserDetailsView.Name = "m_waveBrowserDetailsView";
            this.m_waveBrowserDetailsView.Size = new System.Drawing.Size(250, 117);
            this.m_waveBrowserDetailsView.TabIndex = 1;

            // mnuWaveMenu
            this.mnuWaveMenu.Items.AddRange(new ToolStripItem[] { this.m_collapseAllToolStripMenuItem });
            this.mnuWaveMenu.Name = "mnuWaveMenu";
            this.mnuWaveMenu.Size = new System.Drawing.Size(140, 26);

            // collapseAllToolStripMenuItem
            this.m_collapseAllToolStripMenuItem.Name = "collapseAllToolStripMenuItem";
            this.m_collapseAllToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.m_collapseAllToolStripMenuItem.Text = "Collapse All";
            this.m_collapseAllToolStripMenuItem.Click += new System.EventHandler(this.mnnuCollapseAll_Click);

            // ctrlWaveBrowser
            this.Controls.Add(this.m_waveBrowserTreeView);
            this.Controls.Add(this.m_waveBrowserDetailsView);
            this.Controls.Add(this.m_panel1);
            this.Name = "ctrlWaveBrowser";
            this.Content.Title = "Wave Browser";
            this.Content.ContentId = "WaveBrowser";
            this.m_panel1.ResumeLayout(false);
            this.m_panel1.PerformLayout();
            this.mnuWaveMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        /// <summary>
        /// The plugin chooser_ selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void PluginChooser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RaveInstance.PluginManager != null)
            {
                if (this.m_pluginChooser.SelectedItem.ToString() == NO_PLUGIN)
                {
                    Configuration.SelectedPlugin = null;
                }
                else
                {
                    Configuration.SelectedPlugin = (IRAVEWaveImportPlugin)this.m_pluginChooser.SelectedItem;
                }
            }
        }

        /// <summary>
        /// The possibly commit changes.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool PossiblyCommitChanges()
        {
            // if there are any uncommitted action then prompt the user, commit if they want to
            if (this.m_actionLog != null && this.m_actionLog.GetCommitLogCount() > 0)
            {
                if (
                    ErrorManager.HandleQuestion(
                        "There are uncommitted actions in the activity log - would you like to commit these changes before continuing?",
                        MessageBoxButtons.YesNo,
                        DialogResult.Yes) == DialogResult.Yes)
                {
                    return this.CommitChanges(true);
                }
            }

            return true;
        }

        /// <summary>
        /// Load pack child nodes.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private void LoadPackChildNodes(EditorTreeNode node)
        {
            if (node == null)
            {
                return;
            }

            var packNode = node as PackNode;
            if (null != packNode && !packNode.IsLoaded)
            {
                if (MessageBox.Show(
                    "Do you want to load this pack?", "Load Pack", MessageBoxButton.OKCancel, MessageBoxImage.Question)
                    == MessageBoxResult.OK)
                {
                    this.m_waveBrowserTreeView.LoadPackChildNodes(packNode.PackName);
                }
            }
        }

        /// <summary>
        /// The show node properties.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private void ShowNodeProperties(EditorTreeNode node)
        {
            if (node == null)
            {
                return;
            }

            var assetPath = Path.Combine(Configuration.PlatformWavePath, node.GetObjectPath());
            if (!File.Exists(assetPath))
            {
                UserAction userAction = (UserAction)m_actionLog.SearchForAction(node.GetObjectName());
                if (userAction != null)
                {
                    assetPath = userAction.ActionParameters.SourcePath;
                }
            }
            if (node.GetType() == typeof(WaveNode))
            {
                if (!assetPath.EndsWith(".WAV", StringComparison.InvariantCultureIgnoreCase))
                {
                    return;
                }
                // get latest on this wave first
                if (!string.IsNullOrEmpty(RaveInstance.AssetManager.GetDepotPath(assetPath))
                    && RaveInstance.AssetManager.ExistsAsAsset(assetPath))
                {
                    try
                    {
                        RaveInstance.AssetManager.GetLatestForceErrors(assetPath);
                    }
                    catch (Exception e)
                    {
                        ErrorManager.HandleError(e);
                        return;
                    }
                }

                //var auditionForm = new frmAudition(assetPath, node as WaveNode);
                //auditionForm.Show(RaveInstance.ActiveWindow);
                WaveAuditionPopup waveAuditionPopup = new WaveAuditionPopup(assetPath, node as WaveNode);
                waveAuditionPopup.Show();

            }
            else if (node.GetType() == typeof(TagNode))
            {
                var tn = (TagNode)node;
                var tagEditor = new frmTagEditor(tn, this.m_actionLog, this);
                tagEditor.ShowDialog(this);
                tagEditor.Dispose();
            }
        }

        /// <summary>
        /// The wave browser tree view_ double click node.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private void WaveBrowserTreeView_DoubleClickNode(EditorTreeNode node)
        {
            this.LoadPackChildNodes(node);
            this.ShowNodeProperties(node);
        }

        /// <summary>
        /// The wave browser tree view_ mouse over node.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private void WaveBrowserTreeView_MouseOverNode(EditorTreeNode node)
        {
            this.m_waveBrowserDetailsView.ShowNodeDetails(node);
        }

        /// <summary>
        /// The mnnu collapse all_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnnuCollapseAll_Click(object sender, EventArgs e)
        {
            this.m_waveBrowserTreeView.GetTreeView().BeginUpdate();
            Collapse(this.m_waveBrowserTreeView.GetTreeView().Nodes);
            this.m_waveBrowserTreeView.GetTreeView().EndUpdate();
        }

        /// <summary>
        /// The mnu commit changes_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuCommitChanges_Click(object sender, EventArgs e)
        {
            this.CommitChanges(true);
        }

        /// <summary>
        /// The mnu edit redo_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuEditRedo_Click(object sender, EventArgs e)
        {
            if (this.m_actionLog != null)
            {
                this.m_actionLog.RedoNextAction();
            }
        }

        /// <summary>
        /// The mnu edit undo_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuEditUndo_Click(object sender, EventArgs e)
        {
            if (this.m_actionLog != null)
            {
                this.m_actionLog.UndoLastAction();
            }
        }

        /// <summary>
        /// The mnu manage wave slots_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuManageWaveSlots_Click(object sender, EventArgs e)
        {
            var frm = new frmWaveSlotEditor(this.m_actionLog, this);
            frm.ShowDialog(RaveInstance.ActiveWindow);
            frm.Dispose();
        }

        /// <summary>
        /// The wave browser menu_ submenu opened.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void waveBrowserMenu_SubmenuOpened(object sender, RoutedEventArgs e)
        {
            // update the edit menu before its drawn
            if (this.m_actionLog != null)
            {
                IUserAction action = this.m_actionLog.GetUndoAction();

                if (action != null)
                {
                    this.m_mnuEditUndo.IsEnabled = true;
                    this.m_mnuEditUndo.Header = "_Undo " + action.GetSummary();
                }
                else
                {
                    this.m_mnuEditUndo.IsEnabled = false;
                    this.m_mnuEditUndo.Header = "_Undo";
                }

                action = this.m_actionLog.GetRedoAction();

                if (action != null)
                {
                    this.m_mnuEditRedo.IsEnabled = true;
                    this.m_mnuEditRedo.Header = "_Redo " + action.GetSummary();
                }
                else
                {
                    this.m_mnuEditRedo.IsEnabled = false;
                    this.m_mnuEditRedo.Header = "_Redo";
                }
            }

            this.m_mnuGetLatest.IsEnabled = true; // App.Instance.AllowGetLatest;
        }

        #endregion
    }
}
