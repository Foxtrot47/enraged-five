namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Forms;

    using Rave.Instance;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Summary description for DeletePackAction.
    /// </summary>
    public class DeletePackAction : UserAction
    {
        private readonly string m_FolderPath;
        private readonly string m_ObjectName;
        private readonly List<string> m_Platforms;
        private readonly Dictionary<string, EditorTreeNode.NodeDisplayState> m_PreviousStates;
        private bool bDeleteAll;
        private TreeView m_TreeView;

        public DeletePackAction(IActionParams parameters)
        {
            this.ActionParameters = parameters;
            this.m_ObjectName = this.ActionParameters.Node.GetObjectPath();
            this.m_TreeView = parameters.TreeView;
            this.m_PreviousStates = new Dictionary<string, EditorTreeNode.NodeDisplayState>();
            this.m_Platforms = new List<string>(parameters.Platforms);

            //folder path
            var sb = new StringBuilder();
            sb.Append(Configuration.PlatformWavePath);
            sb.Append(this.m_ObjectName);
            this.m_FolderPath = sb.ToString();
        }

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                return this.ActionParameters.Node;
            }
        }

        public override string GetAssetPath()
        {
            return this.m_ObjectName;
        }

        protected override bool doAction()
        {
            if (this.ActionParameters.ActionLog != null &&
                this.ActionParameters.ActionLog.SearchForAction(this.GetAssetPath()) != null)
            {
                ErrorManager.HandleInfo("There are pending actions on " + this.GetAssetPath() +
                                        " - you must commit your changes before you delete this pack.");
                return false;
            }

            foreach (var platform in this.m_Platforms)
            {
                this.m_PreviousStates.Add(platform, this.ActionParameters.Node.GetNodeDisplayState(platform));
                this.ActionParameters.Node.RemovePlatform(platform);
            }
            this.bDeleteAll = false;
            if (this.ActionParameters.Node.GetPlatforms().Count == 0)
            {
                if (this.ActionParameters.TreeView.InvokeRequired)
                {
                    this.ActionParameters.TreeView.Invoke(new Action<TreeNode>(this.ActionParameters.TreeView.Nodes.Remove),
                                                 new object[] { this.ActionParameters.Node });
                }
                else
                {
                    this.ActionParameters.TreeView.Nodes.Remove(this.ActionParameters.Node);
                }
                this.bDeleteAll = true;
            }
            this.ActionParameters.Node.UpdateDisplay();
            return true;
        }

        protected override bool doCommit()
        {
            try
            {
                // firstly delete any .lock file that exists
                if (RaveInstance.AssetManager.ExistsAsAsset(this.m_FolderPath + "\\.lock"))
                {
                    if (RaveInstance.RaveAssetManager.WaveChangeList == null)
                    {
                        RaveInstance.RaveAssetManager.WaveChangeList =
                            RaveInstance.AssetManager.CreateChangeList("[Rave] Wave Changelist");
                    }
                    //already checked out revert and mark for delete
                    if (RaveInstance.AssetManager.IsCheckedOut(this.m_FolderPath + "\\.lock"))
                    {
                        var a = RaveInstance.RaveAssetManager.WaveChangeList.GetAsset(this.m_FolderPath + "\\.lock");
                        a.Revert();
                    }
                    RaveInstance.RaveAssetManager.WaveChangeList.MarkAssetForDelete(this.m_FolderPath + "\\.lock");
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }

            foreach (var platform in this.m_Platforms)
            {
                this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform).RecordDeletePack(
                    this.m_ObjectName);
            }
            return true;
        }

        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            try
            {
                // firstly delete any .lock file that exists
                if (RaveInstance.AssetManager.ExistsAsAsset(this.m_FolderPath + "\\.lock"))
                {
                    if (RaveInstance.RaveAssetManager.WaveChangeList == null)
                    {
                        RaveInstance.RaveAssetManager.WaveChangeList =
                            RaveInstance.AssetManager.CreateChangeList("[Rave] Wave Changelist");
                    }
                    //already checked out revert and mark for delete
                    if (RaveInstance.AssetManager.IsCheckedOut(this.m_FolderPath + "\\.lock"))
                    {
                        var a = RaveInstance.RaveAssetManager.WaveChangeList.GetAsset(this.m_FolderPath + "\\.lock");
                        a.Revert();
                    }
                    RaveInstance.RaveAssetManager.WaveChangeList.MarkAssetForDelete(this.m_FolderPath + "\\.lock");
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }

            temporaryWaveList.RecordDeletePack(
                    this.m_ObjectName);

            return true;
        }

        public override string GetSummary()
        {
            var platforms = new StringBuilder();
            for (var i = 0; i < this.ActionParameters.Platforms.Count; i++)
            {
                platforms.Append(this.ActionParameters.Platforms[i]);
                if (i != this.ActionParameters.Platforms.Count - 1)
                {
                    platforms.Append(",");
                }
            }

            var sb = new StringBuilder();
            sb.Append("delete pack ");
            sb.Append(this.m_ObjectName);
            sb.Append(" on ");
            sb.Append(platforms.ToString());

            return sb.ToString();
        }

        protected override bool doUndo()
        {
            if (this.bDeleteAll)
            {
                if (this.ActionParameters.ParentNode.TreeView.InvokeRequired)
                {
                    this.ActionParameters.ParentNode.TreeView.Invoke(new Func<TreeNode, int>(this.ActionParameters.ParentNode.Nodes.Add),
                                                            new object[] { this.ActionParameters.Node });
                }
                else
                {
                    this.ActionParameters.ParentNode.Nodes.Add(this.ActionParameters.Node);
                }
            }

            foreach (var platform in this.m_Platforms)
            {
                this.ActionParameters.Node.AddPlatform(platform, this.m_PreviousStates[platform]);
                this.m_PreviousStates.Remove(platform);
            }

            this.ActionParameters.Node.UpdateDisplay();

            return true;
        }
    }
}