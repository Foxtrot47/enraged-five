namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Instance;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Waves;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    public class MoveWaveAction: UserAction
    {
        private readonly string m_oldBankPath;
        private readonly string m_oldWavePath;
        private readonly string m_waveName;
        private string m_newBankPath;
        private string m_newWavePath;

        public MoveWaveAction(AddWaveAction addWaveAction, DeleteWaveAction deleteWaveAction)
        {
            if (addWaveAction == null)
            {
                throw new ArgumentNullException("addWaveAction");
            }
            if (deleteWaveAction == null)
            {
                throw new ArgumentNullException("deleteWaveAction");
            }

            this.m_oldBankPath = ((WaveNode)deleteWaveAction.ActionParameters.Node).GetBankPath();
            this.m_oldWavePath = ((WaveNode)deleteWaveAction.ActionParameters.Node).GetObjectPath();
            this.m_waveName = ((WaveNode)deleteWaveAction.ActionParameters.Node).GetWaveName();
            this.AddWaveAction = addWaveAction;
            this.DeleteWaveAction = deleteWaveAction;
        }

        #region IUserAction Members

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                return this.AddWaveAction.Node;
            }
        }

        /// <summary>
        /// Gets the action parameters.
        /// </summary>
        public IActionParams ActionParameters { get; private set; }

        /// <summary>
        /// Gets the add wave action.
        /// </summary>
        public AddWaveAction AddWaveAction { get; private set; }

        /// <summary>
        /// Gets the delete wave action.
        /// </summary>
        public DeleteWaveAction DeleteWaveAction { get; private set; }

        protected override bool doCommit()
        {
            if (this.AddWaveAction.Commit())
            {
                if (this.DeleteWaveAction.Commit())
                {
                    return true;
                }
                else
                {
                    this.AddWaveAction.Undo();
                }
            }
            return false;
        }

        protected override bool doAction()
        {
            try
            {
                if (this.AddWaveAction.ActionParameters.ActionLog != null &&
                this.AddWaveAction.ActionParameters.ActionLog.SearchForAction(this.AddWaveAction.GetAssetPath()) != null)
                {
                    ErrorManager.HandleInfo("There are pending actions on " + this.AddWaveAction.GetAssetPath() +
                                            " - you must commit your changes before you move this wave.");
                    return false;
                }

                if (this.DeleteWaveAction.ActionParameters.ActionLog != null &&
                this.DeleteWaveAction.ActionParameters.ActionLog.SearchForAction(this.DeleteWaveAction.GetAssetPath()) != null)
                {
                    ErrorManager.HandleInfo("There are pending actions on " + this.DeleteWaveAction.GetAssetPath() +
                                            " - you must commit your changes before you move this wave.");
                    return false;
                }

                if (this.AddWaveAction.Action())
                {
                    if (this.DeleteWaveAction.Action())
                    {
                        this.m_newBankPath = ((WaveNode)this.AddWaveAction.Node).GetBankPath();
                        this.m_newWavePath = this.AddWaveAction.Node.GetObjectPath();

                        if (UpdateObjectsReferencingWave(this.m_oldBankPath, this.m_newBankPath, this.m_waveName))
                        {
                            return true;
                        }
                        else
                        {
                            this.DeleteWaveAction.Undo();
                            this.AddWaveAction.Undo();
                            return false;
                        }
                    }
                    else
                    {
                        this.AddWaveAction.Undo();
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
                return false;
            }
        }

        protected override bool doUndo()
        {
            try
            {
                this.DeleteWaveAction.Undo();
                this.AddWaveAction.Undo();
                UpdateObjectsReferencingWave(this.m_newBankPath, this.m_oldBankPath, this.m_waveName);
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
                return false;
            }
            return true;
        }

        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            if (this.AddWaveAction.Apply(temporaryWaveList, platform))
            {
                if (this.DeleteWaveAction.Apply(temporaryWaveList, platform))
                {
                    return true;
                }
            }
            return false;
        }

        public override string GetSummary()
        {
            return string.Format("Move wave from {0} to {1}", this.m_oldWavePath, this.m_newWavePath);
        }

        #endregion

        private static bool UpdateObjectsReferencingWave(string oldBankPath, string newBankPath, string waveName)
        {
            if (oldBankPath.Equals(newBankPath)) return true;

            var oldKey = Path.Combine(oldBankPath, waveName);
            if (WaveManager.WaveRefs.ContainsKey(oldKey))
            {
                var banksNotCheckedOut =
                    WaveManager.WaveRefs[oldKey].Where(o => !o.Bank.IsCheckedOut).Select(
                        o => o.Bank).Distinct().ToList();
                if (banksNotCheckedOut.Count > 0)
                {
                    foreach (var bank in banksNotCheckedOut)
                    {
                        if (bank.Checkout(false))
                        {
                            continue;
                        }

                        var errorMsg =
                            string.Format(
                                "Failed to check out object bank \"{0}\". Can't update objects referencing wave \"{1}\".",
                                bank.Name,
                                waveName);
                        MessageBox.Show(RaveInstance.ActiveWindow, errorMsg, "Checkout Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                var newKey = Path.Combine(newBankPath, waveName);
                if (!WaveManager.WaveRefs.ContainsKey(newKey))
                {
                    WaveManager.WaveRefs.Add(newKey, new List<IObjectInstance>());
                }
                var newObjectsReferencingWaveList = WaveManager.WaveRefs[newKey];

                var oldObjectsReferencingWaveList = WaveManager.WaveRefs[oldKey];
                foreach (var objectReferencingWave in oldObjectsReferencingWaveList)
                {
                    var reference = FindReference(objectReferencingWave);
                    if (reference != null)
                    {
                        UpdateReference(reference.Value, newBankPath);
                        newObjectsReferencingWaveList.Add(objectReferencingWave);
                        objectReferencingWave.MarkAsDirty();
                    }
                }

                WaveManager.WaveRefs.Remove(oldKey);
            }

            return true;
        }

        private static void UpdateReference(KeyValuePair<IFieldDefinition, XmlNode> reference, string bankPath)
        {
            switch (reference.Key.Units)
            {
                case "BankRef":
                    {
                        reference.Value.InnerText = bankPath;
                        break;
                    }
                case "WaveRef":
                    {
                        var node = reference.Value;
                        if (node.HasChildNodes)
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                switch (childNode.Name)
                                {
                                    case "BankName":
                                        {
                                            childNode.InnerText = bankPath;
                                            break;
                                        }
                                }
                            }
                        }
                        break;
                    }
            }
        }

        private static KeyValuePair<IFieldDefinition, XmlNode>? FindReference(IObjectInstance objectReferencingWave)
        {
            var typeDef = objectReferencingWave.TypeDefinitions.FindTypeDefinition(objectReferencingWave.TypeName);
            return FindReference(objectReferencingWave.Node,
                                 typeDef.Fields.Where(
                                     fd =>
                                     fd.Units == "BankRef" || fd.Units == "WaveRef" || fd is ICompositeFieldDefinition));
        }

        private static KeyValuePair<IFieldDefinition, XmlNode>? FindReference(XmlNode parentNode,
                                                                             IEnumerable<IFieldDefinition>
                                                                                 fieldDefinitions)
        {
            foreach (XmlNode childNode in parentNode.ChildNodes)
            {
                foreach (var fieldDef in fieldDefinitions)
                {
                    if (fieldDef.Name ==
                        childNode.Name)
                    {
                        if (fieldDef.Units == "BankRef" ||
                            fieldDef.Units == "WaveRef")
                        {
                            return new KeyValuePair<IFieldDefinition, XmlNode>(fieldDef, childNode);
                        }

                        if (fieldDef is ICompositeFieldDefinition)
                        {
                            var compositeFieldDef = fieldDef as ICompositeFieldDefinition;
                            var reference = FindReference(childNode,
                                                          compositeFieldDef.Fields.Where(
                                                              fd =>
                                                              fd.Units == "BankRef" || fd.Units == "WaveRef" ||
                                                              fd is ICompositeFieldDefinition));
                            if (reference != null)
                            {
                                return reference;
                            }
                        }
                    }
                }
            }
            return null;
        }

        public override string GetAssetPath()
        {
            return null;
        }
    }
}