using System.Collections.Generic;

namespace Rave.WaveBrowser.UserActions
{
    using System.Linq;

    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    internal class BulkAction : UserAction
    {
        public BulkAction(IActionParams parameters)
        {
            this.ActionParameters = parameters;
        }

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                // Action not specific to one node
                return null;
            }
        }

        protected override bool doCommit()
        {
            return this.ActionParameters.Actions.Cast<IUserAction>().All(t => t.Commit());
        }

        protected override bool doUndo()
        {
            for (var i = this.ActionParameters.Actions.Count - 1; i >= 0; i--)
            {
                if (!(((IUserAction) this.ActionParameters.Actions[i]).Undo()))
                {
                    return false;
                }
            }
            return true;
        }

        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            return ActionParameters.Actions.Cast<IUserAction>().All(t => t.Apply(temporaryWaveList, platform));
        }

        protected override bool doAction()
        {
            return true;
        }

        public override string GetSummary()
        {
            if (string.IsNullOrWhiteSpace(this.ActionParameters.Name))
            {
                return "Batch action " + "(" + this.ActionParameters.Actions.Count + " actions" + ")";
            }
            return string.Format("{0} batch action ({1} actions)", this.ActionParameters.Name, this.ActionParameters.Actions.Count);
        }

        public override string GetAssetPath()
        {
            return null;
        }

        public IUserAction FindPackDeletion(string packPath)
        {
            foreach (IUserAction a in this.ActionParameters.Actions)
            {
                var deletePackAction = a as DeletePackAction;
                if (deletePackAction != null)
                {
                    if (deletePackAction.GetAssetPath() != null)
                    {
                        return deletePackAction;
                    }
                }
            }
            return null;
        }

        public IUserAction FindBankDeletion(string bankPath)
        {
            foreach (IUserAction a in this.ActionParameters.Actions)
            {
                var deleteBankAction = a as DeleteBankAction;
                if (deleteBankAction != null)
                {
                    if (deleteBankAction.GetAssetPath() == bankPath)
                    {
                        return deleteBankAction;
                    }
                }
            }

            return null;
        }

        public IUserAction FindWaveDeletion(string wavePath)
        {
            foreach (IUserAction a in this.ActionParameters.Actions)
            {
                var deleteWaveAction = a as DeleteWaveAction;
                if (deleteWaveAction != null)
                {
                    if (deleteWaveAction.GetAssetPath() == wavePath)
                    {
                        return deleteWaveAction;
                    }
                }
            }
            return null;
        }

        public IUserAction FindAddPack(string packPath)
        {
            foreach (IUserAction a in this.ActionParameters.Actions)
            {
                var addPackAction = a as AddPackAction;
                if (addPackAction != null)
                {
                    if (addPackAction.GetAssetPath() == packPath)
                    {
                        return addPackAction;
                    }
                }
            }
            return null;
        }

        public IUserAction FindAddBank(string bankPath)
        {
            foreach (IUserAction a in this.ActionParameters.Actions)
            {
                var addBankAction = a as AddBankAction;
                if (addBankAction != null)
                {
                    if (addBankAction.GetAssetPath() == bankPath)
                    {
                        return addBankAction;
                    }
                }
            }
            return null;
        }

        public IUserAction FindAddWave(string wavePath)
        {
            foreach (IUserAction a in this.ActionParameters.Actions)
            {
                var addWaveAction = a as AddWaveAction;
                if (addWaveAction != null)
                {
                    if (addWaveAction.GetAssetPath() == wavePath)
                    {
                        return addWaveAction;
                    }
                }
            }
            return null;
        }

        public IUserAction FindAction(string path)
        {
            foreach (IUserAction a in this.ActionParameters.Actions)
            {
                if (a.GetAssetPath() == path)
                {
                    return a;
                }
            }
            return null;
        }

        public IEnumerable<IUserAction> FindActions(BankNode bankNode)
        {
            List<IUserAction> list = (from IUserAction a in this.ActionParameters.Actions where a.Node.FullPath.Contains(bankNode.FullPath) select a).ToList();
            if (list.Count == 0)
            {
                return null;
            }
            return list;
        }
    }
}