﻿// -----------------------------------------------------------------------
// <copyright file="WaveSearchProvider.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.WaveBrowser.WaveBrowser.Search
{
    using Rave.SearchProvider.Infrastructure.Interfaces;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Collections.ObjectModel;
    using System.Xml.Linq;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.Utils;
    using System.IO;
    using rage.ToolLib;

    class WaveSearchProvider : ISearchProvider<string>
    {

        #region Public Properties

        public ObservableSortedSet<string> SearchResults
        {
            get { return _searchResults; }
        }

      
        #endregion

        #region Fields And Constants

        private ObservableSortedSet<string> _searchResults;
        private EditorTreeNode _startNode;
        private string _elementType;

        public bool searchSucceded = false;

        /// The built waves directory name.
        /// </summary>
        private const string BUILT_WAVES_DIR_NAME = "BuiltWaves";

        /// <summary>
        /// The built waves pack list file name.
        /// </summary>
        private const string BUILT_WAVES_PACK_LIST_FILENAME = "BuiltWaves_PACK_LIST.xml";

        /// <summary>
        /// The wave file element
        /// </summary>
        private const string WAVE_ELEMENT = "Wave";

        /// <summary>
        /// The name mask.
        /// </summary>
        private const int NameMask = (1 << 29) - 1;

        #endregion

        #region Constructors

        public WaveSearchProvider(EditorTreeNode node = null, string elementType = WAVE_ELEMENT)
        {
            this._startNode = node;
            this._elementType = elementType;
        }

        #endregion

        #region Public Methods

        public ObservableSortedSet<string> Search(string searchString, SearchArguments args)
        {
            return Search(searchString, args.SearchNameHash, args.ShouldOnlySearchNames, args.IsCaseSensitive, args.ExactNameMatch);
        }

        /// <summary>
        /// Main search function to search for Waves
        /// </summary>
        /// <param name="searchString">
        /// The string to search for
        /// </param>
        /// <param name="searchNameHash">
        /// Should we search for hash
        /// </param>
        /// <param name="shouldOnlySearchNames">
        /// should we search only for names (not yet implemented)
        /// </param>
        /// <param name="isCaseSensitive">
        /// is the search case sensitive
        /// </param>
        /// <param name="exactNameMatch">
        /// are we searching for an exact name
        /// </param>
        /// <returns>
        /// An observable hash set of type string with the results
        /// </returns>
        public ObservableSortedSet<string> Search(string searchString, bool searchNameHash = false, bool shouldOnlySearchNames = false, bool isCaseSensitive = false, bool exactNameMatch = false)
        {
            searchSucceded = false;

            if (!isCaseSensitive)
            {
                searchString = searchString.ToUpper();
            }

            if (searchNameHash)
            {
                return WaveSearchHashedName(searchString);
            }
            else
            {
                return WaveSearch(searchString, this._elementType, this._startNode, exactNameMatch, isCaseSensitive);
            }


        }

        #endregion

        #region Private Methods

        /// <summary>
        /// The wave search hashed name.
        /// </summary>
        /// <param name="hash">
        /// The hash.
        /// </param>
        /// <returns>
        /// ObservableSortedSet of type string<see cref="bool"/>.
        /// </returns>
        private ObservableSortedSet<string> WaveSearchHashedName(string hash)
        {
            //Reset Search results
            _searchResults = new ObservableSortedSet<string>();
            _searchResults.Clear();

            uint hashInt;
            if (!uint.TryParse(hash, out hashInt))
            {
                return null;
            }

            // Mask off the upper three bits if 32-bit hash provided
            hashInt = hashInt & NameMask;

            foreach (var platformSetting in Configuration.ActivePlatformSettings)
            {
                var packListPath = Path.Combine(platformSetting.BuildInfo, BUILT_WAVES_DIR_NAME, BUILT_WAVES_PACK_LIST_FILENAME);
                var packListDoc = XDocument.Load(packListPath);

                foreach (var packFileElem in packListDoc.Descendants("PackFile"))
                {
                    var packFilePath = Path.Combine(platformSetting.BuildInfo, BUILT_WAVES_DIR_NAME, packFileElem.Value);
                    WaveSearchHashedName(hashInt, packFilePath);
                }
            }

            return _searchResults;
        }


        /// <summary>
        /// Search for waves with their hash name
        /// </summary>
        /// <param name="hash">
        /// the hash to search for
        /// </param>
        /// <param name="filePath">
        /// the file path of the xml
        /// </param>
        private void WaveSearchHashedName(uint hash, string filePath)
        {
            XDocument doc = XDocument.Load(filePath);
            IEnumerable<XElement> waveNodes = doc.Descendants(WAVE_ELEMENT);
            ObservableSortedSet<string> results = new ObservableSortedSet<string>();

            foreach (var node in waveNodes)
            {
                var nameHashAttr = node.Attribute("nameHash");
                if (null == nameHashAttr)
                {
                    continue;
                }

                var waveNameHash = int.Parse(nameHashAttr.Value) & NameMask;
                if (waveNameHash != hash)
                {
                    continue;
                }

                _searchResults.Add(GetPath(node));
            }
        }

        /// <summary>
        /// Search for waves
        /// </summary>
        /// <param name="searchString">
        /// the search string
        /// </param>
        /// <param name="elementType">
        /// the element type
        /// </param>
        /// <param name="startNode">
        /// the start node
        /// </param>
        /// <param name="exactNameMatch">
        /// should it match exactly
        /// </param>
        /// <param name="isCaseSensitive">
        /// is the search case sensitive
        /// </param>
        /// <returns>
        /// An observable hash set with the search results
        /// </returns>
        private ObservableSortedSet<string> WaveSearch(string searchString, string elementType, EditorTreeNode startNode = null, bool exactNameMatch = false, bool isCaseSensitive = false)
        {
            //Reset search results
            _searchResults = new ObservableSortedSet<string>();
            _searchResults.Clear();

            // Determine which pack to search if we're searching from a particular wave node
            PackNode packNode = null;

            if (null != startNode)
            {
                packNode = EditorTreeNode.FindParentPackNode(startNode);
            }

            // Check across built waves for all platforms in case wave info differs
            foreach (rage.PlatformSetting platformSetting in Configuration.ActivePlatformSettings)
            {
                string packListPath = Path.Combine(platformSetting.BuildInfo, BUILT_WAVES_DIR_NAME, BUILT_WAVES_PACK_LIST_FILENAME);
                XDocument packListDoc = XDocument.Load(packListPath);

                foreach (var packFileElem in packListDoc.Descendants("PackFile"))
                {
                    string packFilePath = Path.Combine(platformSetting.BuildInfo, BUILT_WAVES_DIR_NAME, packFileElem.Value);
                    XDocument packDoc = XDocument.Load(packFilePath);
                    XElement packElem = packDoc.Descendants("Pack").First();
                    string packName = packElem.Attribute("name").Value.ToUpper();

                    // Search all packs if no startNode specified, or current pack if matches startNode pack
                    if (null == packNode || packNode.PackName.ToUpper() == packName)
                    {
                        WaveSearch(searchString, elementType, packDoc, startNode, exactNameMatch, isCaseSensitive);
                    }
                }
            }

            return _searchResults;
        }


        /// <summary>
        /// Searches for waves
        /// </summary>
        /// <param name="searchTerm">
        /// the search term to search for
        /// </param>
        /// <param name="elementType">
        /// the element type to search for
        /// </param>
        /// <param name="doc">
        /// the document to search in
        /// </param>
        /// <param name="startNode">
        /// the starting node
        /// </param>
        /// <param name="exactNameMatch">
        /// exact matching
        /// </param>
        /// <param name="isCaseSensitive">
        /// should the search be case sensitive
        /// </param>
        private void WaveSearch(string searchTerm, string elementType, XDocument doc, EditorTreeNode startNode = null, bool exactNameMatch = false, bool isCaseSensitive = false)
        {


            // Would use XPath compare, but .NET doesn't support XPath 2.0 which has the upper-case function required
            foreach (XElement waveElem in doc.Descendants(elementType))
            {
                string waveName = waveElem.Attribute("name").Value;

                if (!isCaseSensitive)
                {
                    waveName = waveName.ToUpper();
                }

                if (exactNameMatch)
                {
                    if (!waveName.Equals(searchTerm))
                    {
                        continue;
                    }
                }
                else
                {
                    if (!waveName.Contains(searchTerm))
                    {
                        continue;
                    }
                }


                string wavePath = GetPath(waveElem);

                if (null != startNode)
                {
                    string startPathUpper = startNode.GetObjectPath().ToUpper();

                    if (!wavePath.ToUpper().StartsWith(startPathUpper))
                    {
                        continue;
                    }
                }

                _searchResults.Add(wavePath);
            }

        }

        /// <summary>
        /// Get the string path to the specified node.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// The path to the node <see cref="string"/>.
        /// </returns>
        private static string GetPath(XElement element)
        {
            var pathBuilder = new StringBuilder();
            foreach (var parent in element.AncestorsAndSelf().InDocumentOrder())
            {
                if (parent.Attribute("name") == null)
                {
                    continue;
                }

                // Don't want backslash at start of path
                if (!string.IsNullOrEmpty(pathBuilder.ToString()))
                {
                    pathBuilder.Append("\\");
                }

                pathBuilder.Append(parent.Attribute("name").Value);
            }

            return pathBuilder.ToString().ToUpper();
        }


        #endregion



        public string ObjectType
        {
            get { return "Waves"; }
        }


        public bool IsReady
        {
            get { throw new System.NotImplementedException(); }
        }
    }
}
