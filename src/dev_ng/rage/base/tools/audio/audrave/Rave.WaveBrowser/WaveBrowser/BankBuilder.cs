﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Documents;
using System.Windows.Media;
using System.Xml.Linq;
using rage;
using Rave.Controls.WPF.UXTimer;
using Rave.Utils;
using Rave.WaveBrowser.Infrastructure.Nodes;
using Rockstar.AssetManager.Interfaces;
using Rockstar.Audio.AudioAssetBuilder;
using Rockstar.Audio.AudioAssetBuilder.XmlBuildExecutor;
using Rockstar.Audio.Pipeline;
using Package = Rockstar.Audio.Pipeline.Package;

namespace Rave.WaveBrowser.WaveBrowser
{
    public class BankBuilder
    {
        public string OutputFolder { get; private set; }
        public BankNode BankNode { get; private set; }
        public PackMetadata MetadataStore { get; private set; }

        public BankBuilder(BankNode bankNode, string outputFolder, PackMetadata packMetadata)
        {
            BankNode = bankNode;
            MetadataStore = packMetadata;
            OutputFolder = outputFolder;
        }

        public void WriteBank(IAssetManager assetManager, audProjectSettings projectSettings, string loadingBlockId = "")
        {
            LocalPipelineProcessor pipelineProcessor = new LocalPipelineProcessor(new CacheClient());
          
            pipelineProcessor.PropertyChanged += (p, r) =>
            {
                if (r.PropertyName == "CurrentStatus")
                {
                    UXTimer.Instance.AddTextToBlock(pipelineProcessor.CurrentStatus, loadingBlockId, Brushes.Green);
                }
            };
            UXTimer.Instance.AddTextToBlock("Creating bank configuration from current bank node status...", loadingBlockId, Brushes.Green);
           
            //Get latest on bank path.
            assetManager.GetLatest(Path.Combine(projectSettings.GetWaveInputPath(), BankNode.FullPath, "..."), false);
            
            BankBuildConfiguration bankConfig = GetBankBuildConfiguration(BankNode, MetadataStore);
           
            Package resultPackage = pipelineProcessor.Process(bankConfig);
            string bankOutputDir = OutputFolder;
            if (!Directory.Exists(bankOutputDir)) Directory.CreateDirectory(bankOutputDir);
            //write awc file
            PackageFile bankFile = resultPackage.Files.Last();

            bankFile.LoadObject(); //explicit load in case we got the file from the cache
            string outputFilePath = Path.Combine(bankOutputDir, bankFile.FileName);
            using (FileStream fs = new FileStream(outputFilePath, FileMode.Create, FileAccess.Write))
            {
                bankFile.SaveObject(fs);
            }

        }

        private BankBuildConfiguration GetBankBuildConfiguration(BankNode bankNode, PackMetadata metadataStores)
        {
            string path = bankNode.GetTreePath();
            XmlBuiltWavesInfo builtwavesinfo = new XmlBuiltWavesInfo(metadataStores.buildMetadataStore.Document);
            XElement pendingWavesElement = metadataStores.pendingMetadataStore.FindNodeFromPath(path);
            if (pendingWavesElement != null)
            {
                builtwavesinfo.updateBuiltWaves(pendingWavesElement);
            }
            XmlBuiltBankNodeInfo xmlBankNodeInfo = builtwavesinfo.GetBuiltBankInfo(bankNode.Text);

            BankBuildConfiguration bankConfig = xmlBankNodeInfo.getBankBuildConfiguration(Instance.RaveInstance.AssetManager, Configuration.ProjectSettings,
                new XmlWaveSlotSettingsInfo(Configuration.WaveSlotFile));

            return bankConfig;
        }
    }
}
