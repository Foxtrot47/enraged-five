namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;

    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Encapsulates the action of a user adding a bank to a pack
    /// </summary>
    public class AddBankAction : UserAction
    {
        private readonly string m_AssetPath;
        private readonly string m_FolderPath;
        private readonly string m_parentPath;

        public AddBankAction(IActionParams parameters)
        {
            if (!RaveUtils.StringIsValidObjectName(parameters.Name))
            {
                throw new Exception("Bank name " + parameters.Name + " in " + parameters.ParentNode.GetObjectPath() +
                                    " contains invalid characters");
            }

            this.ActionParameters = parameters;
            this.m_parentPath = this.ActionParameters.ParentNode.GetObjectPath();

            //asset path
            var sb = new StringBuilder();
            sb.Append(this.m_parentPath);
            sb.Append("\\");
            sb.Append(this.ActionParameters.Name);
            this.m_AssetPath = sb.ToString();

            //folder path
            sb = new StringBuilder();
            sb.Append(Configuration.PlatformWavePath);
            sb.Append(this.m_AssetPath);
            this.m_FolderPath = sb.ToString();
        }

        public override string GetAssetPath()
        {
            return this.m_AssetPath;
        }

        protected override bool doAction()
        {
            if (this.ActionParameters.ActionLog != null &&
                this.ActionParameters.ActionLog.SearchForAction(this.m_AssetPath) != null)
            {
                var sb = new StringBuilder();
                sb.Append("There are pending actions for ");
                sb.Append(this.m_AssetPath);
                sb.Append(" - you must commit your changes before you add this bank again");
                ErrorManager.HandleInfo(sb.ToString());
                return false;
            }

            foreach (EditorTreeNode node in this.ActionParameters.ParentNode.Nodes)
            {
                if (node.GetObjectName() ==
                    this.ActionParameters.Name)
                {
                    return false;
                }
            }

            if (this.Node == null)
            {
                this.Node = new BankNode(this.ActionParameters.ActionLog, this.ActionParameters.Name, this.ActionParameters.WaveBrowser);
                foreach (var platform in this.ActionParameters.Platforms)
                {
                    this.Node.AddPlatform(platform, EditorTreeNode.NodeDisplayState.AddedLocally);
                }
            }

            if (this.ActionParameters.ParentNode.TryToLock())
            {
                try
                {
                    Directory.CreateDirectory(this.m_FolderPath);
                }
                catch (Exception)
                {
                    System.Diagnostics.Debug.WriteLine("Failed to created directory - " + this.m_FolderPath);
                    this.Node.Remove();
                    return false;
                }

                if (this.ActionParameters.TreeView.InvokeRequired)
                {
                    this.ActionParameters.TreeView.Invoke(new Func<TreeNode, int>(this.ActionParameters.ParentNode.Nodes.Add),
                                                 new object[] { this.Node });
                }
                else
                {
                    this.ActionParameters.ParentNode.Nodes.Add(this.Node);
                }
                this.Node.UpdateDisplay();
                this.ActionParameters.ParentNode.Expand();
                if (this.Node.TreeView != null)
                {
                    this.Node.TreeView.SelectedNode = this.Node;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override bool doUndo()
        {
            if (this.Node == null)
            {
                return false;
            }
            try
            {
                Directory.Delete(this.m_FolderPath, true);
            }
            catch
            {
                ErrorManager.HandleInfo("Unable to delete file: " + this.ActionParameters.Name);
            }
            this.ActionParameters.ParentNode.Nodes.Remove(this.Node);
            return true;
        }

        protected override bool doCommit()
        {
            foreach (var platform in this.ActionParameters.Platforms)
            {
                // add this to the pending wave list
                this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform).RecordAddBank(this.m_parentPath +
                                                                                                          "\\" +
                                                                                                          this.ActionParameters.
                                                                                                              Name);
                this.Node.UpdateDisplayState(EditorTreeNode.NodeDisplayState.AddedInPendingWaveList,
                                          platform,
                                          true,
                                          true);
            }
            return true;
        }

        protected override bool doApply(IPendingWaveList waveList, string platform)
        {
            // add this to the pending wave list
            waveList.RecordAddBank(this.m_parentPath + "\\" + this.ActionParameters.Name);
            return true;
        }

        public override string GetSummary()
        {
            var platforms = new StringBuilder();
            for (var i = 0; i < this.ActionParameters.Platforms.Count; i++)
            {
                platforms.Append(this.ActionParameters.Platforms[i]);
                if (i != this.ActionParameters.Platforms.Count - 1)
                {
                    platforms.Append(",");
                }
            }

            var sb = new StringBuilder();
            sb.Append("new bank ");
            sb.Append(this.ActionParameters.Name);
            sb.Append(" in ");
            sb.Append(this.m_parentPath);
            sb.Append(" added on ");
            sb.Append(platforms.ToString());

            return sb.ToString();
        }
    }
}