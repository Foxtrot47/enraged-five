namespace Rave.WaveBrowser.WaveBrowser
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    using Rave.Utils;

    using rage;

    using Rave.Instance;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    public class PendingWaveLists : IPendingWaveLists
    {
        private static Dictionary<string, IPendingWaveList> ms_waveLists;
        private readonly IWaveBrowser m_waveBrowser;

        public PendingWaveLists(IWaveBrowser waveBrowser)
        {
            this.m_waveBrowser = waveBrowser;
            ms_waveLists = new Dictionary<string, IPendingWaveList>();

            foreach (PlatformSetting platform in Configuration.ActivePlatformSettings)
            {
                ms_waveLists.Add(platform.PlatformTag, new PendingWaveList(platform.BuildInfo, this.m_waveBrowser));
            }
        }

        #region IPendingWaveLists Members

        public void LoadPack(string packName, IWaveBrowser browser) {
            foreach(var kvp in ms_waveLists) {
                kvp.Value.LoadPack(packName, browser);
            }
        }

        public IPendingWaveList GetPendingWaveList(string name)
        {
            return ms_waveLists[name];
        }

        public bool LoadLatestWaveLists()
        {
            var ret = true;
            foreach (var kvp in ms_waveLists)
            {
                if (!kvp.Value.GetLatestBuiltWaveListFromAssetManager() ||
                    !kvp.Value.GetLatestPendingWaveListFromAssetManager())
                {
                    ret = false;
                }

                kvp.Value.LoadPendingWaveList();
                kvp.Value.LoadBuiltWaveList();
            }

            return ret;
        }

        public bool LockPendingWaveList()
        {
            var ret = true;
            var isLocked = new List<IPendingWaveList>();
            foreach (var kvp in ms_waveLists)
            {
                if (!kvp.Value.LockPendingWaveList())
                {
                    ret = false;
                    foreach (var pwl in isLocked)
                    {
                        pwl.UndoLockPendingWaveList();
                    }
                    break;
                }
                // reload it to ensure we're working with the latest version
                kvp.Value.LoadPendingWaveList();
                isLocked.Add(kvp.Value);
            }
            return ret;
        }

        public bool SerialisePendingWaveList()
        {
            var success = true;
            foreach (var kvp in ms_waveLists)
            {
                if (!kvp.Value.SerialisePendingWaveList())
                {
                    success = false;
                    break;
                }
            }

            if (success)
            {
                // check in all wave lists
                foreach (var kvp in ms_waveLists)
                {
                    kvp.Value.ClearLocalStore();
                }
            }
            else
            {
                // undo all wave list check outs
                foreach (var kvp in ms_waveLists)
                {
                    kvp.Value.UndoLockPendingWaveList();
                }
            }

            return success;
        }

        public void ShowPendingChanges(TreeView treeView, IActionLog log)
        {
            foreach (PlatformSetting platformSetting in Configuration.ActivePlatformSettings)
            {
                if (platformSetting.IsActive)
                {
                    ms_waveLists[platformSetting.PlatformTag].ShowPendingChanges(
                        treeView, log, platformSetting.PlatformTag);
                }
            }

            // force an update of all nodes
            UpdateDisplayState(treeView.Nodes);
        }

        public void ShowBuiltWaves(TreeView treeView, IActionLog log)
        {
            foreach (PlatformSetting platformSetting in Configuration.ActivePlatformSettings)
            {
                if (platformSetting.IsActive)
                {
                    ms_waveLists[platformSetting.PlatformTag].ShowBuiltWaves(
                        treeView, log, platformSetting.PlatformTag);
                }
            }
        }

        public void ShowBuiltWavesOfPack(string packName, TreeView treeView, IActionLog log)
        {
            foreach (PlatformSetting platformSetting in Configuration.ActivePlatformSettings)
            {
                if (platformSetting.IsActive)
                {
                    ms_waveLists[platformSetting.PlatformTag].ShowBuiltWavesOfPack(packName, treeView, log, platformSetting.PlatformTag);
                }
            }
        } 

        public bool ArePendingWavelistsCheckedOut()
        {
            RaveInstance.AssetManager.RefreshView();
            return ms_waveLists.Any(kvp => kvp.Value.IsPendingWaveListCheckedOut());
        }


        #endregion

        private static void UpdateDisplayState(TreeNodeCollection nodes)
        {
            foreach (EditorTreeNode n in nodes)
            {
                n.UpdateDisplay();
                UpdateDisplayState(n.Nodes);
            }
        }
    }
}