namespace Rave.WaveBrowser.WaveBrowser.Popups
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Windows.Forms;

    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    using rage;
    using System.IO;

    /// <summary>
    ///   Summary description for ctrlWaveEditorNodeDetails.
    /// </summary>
    public class ctrlWaveEditorNodeDetails : UserControl
    {
        private readonly Hashtable m_PlatformStatusLabels;
        private ImageList bigIconList;
        private IContainer components;

        private Label lblNodeType;
        private Label lblObjectName;
        private bool m_AreStatusLabelsCreated;
        private Panel pnlPlatformStates;

        public ctrlWaveEditorNodeDetails()
        {
            // This call is required by the Windows.Forms Form Designer.
            this.InitializeComponent();

            // create labels for each platform

            this.m_PlatformStatusLabels = new Hashtable();
        }

        public void Init()
        {
            var ht = new Hashtable();
            RaveUtils.LoadTagIcons(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"icons/tags/"), ht, this.bigIconList);
        }

        /// <summary>
        ///   Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        private void CreatePlatformStatusLabels()
        {
            var currentY = 0;
            foreach (PlatformSetting platform in Configuration.ActivePlatformSettings)
            {
                var platname = new Label();
                var status = new Label();

                platname.Top = status.Top = currentY;
                currentY += 20;
                platname.Height = status.Height = 20;
                platname.Left = 0;
                platname.Width = 75;
                status.Left = 80;
                status.Width = 170;
                status.Anchor = AnchorStyles.Left | AnchorStyles.Right;

                platname.Text = platform.Name + ":";

                this.m_PlatformStatusLabels.Add(platform.PlatformTag, status);

                this.pnlPlatformStates.Controls.Add(platname);
                this.pnlPlatformStates.Controls.Add(status);
            }
            this.m_AreStatusLabelsCreated = true;
        }

        //private int currentImageIndex = -1;

        public void ShowNodeDetails(EditorTreeNode node)
        {
            this.SuspendLayout();
            if (!this.m_AreStatusLabelsCreated)
            {
                this.CreatePlatformStatusLabels();
            }

            if (node == null)
            {
                //picBigIcon.Image.Dispose();
                this.lblObjectName.Text = "";
                this.lblNodeType.Text = "";
                foreach (PlatformSetting p in Configuration.ActivePlatformSettings)
                {
                    ((Label) this.m_PlatformStatusLabels[p.PlatformTag]).Text = "";
                }
            }
            else
            {
                this.lblNodeType.Text = node.GetTypeDescription();
                if (!this.lblNodeType.Text.Equals("Wave"))
                {
                    this.lblObjectName.Text = "";
                }
                else
                {
                    this.lblObjectName.Text = node.GetObjectName();
                }

                foreach (PlatformSetting p in Configuration.ActivePlatformSettings)
                {
                    var txt = node.GetStateDescription(p.PlatformTag);
                    var tag = node as TagNode;
                    if (tag != null)
                    {
                        if (tag.GetPlatforms().Contains(p.PlatformTag) &&
                            tag.GetCurrentValue(p.PlatformTag) != null)
                        {
                            txt += ": " + tag.GetCurrentValue(p.PlatformTag);
                        }
                    }
                    ((Label) this.m_PlatformStatusLabels[p.PlatformTag]).Text = txt;
                }
            }
            this.ResumeLayout();
        }

        private void lblObjectName_Click(object sender, EventArgs e)
        {
        }

        #region Component Designer generated code

        /// <summary>
        ///   Required method for Designer support - do not modify 
        ///   the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof (ctrlWaveEditorNodeDetails));
            this.bigIconList = new System.Windows.Forms.ImageList(this.components);
            this.lblObjectName = new System.Windows.Forms.Label();
            this.pnlPlatformStates = new System.Windows.Forms.Panel();
            this.lblNodeType = new System.Windows.Forms.Label();
            this.lblNodeType.AutoSize = true;
            this.SuspendLayout();
            // 
            // bigIconList
            // 
            this.bigIconList.ImageStream =
                ((System.Windows.Forms.ImageListStreamer) (resources.GetObject("bigIconList.ImageStream")));
            this.bigIconList.TransparentColor = System.Drawing.Color.Transparent;
            this.bigIconList.Images.SetKeyName(0, "");
            this.bigIconList.Images.SetKeyName(1, "");
            this.bigIconList.Images.SetKeyName(2, "");
            this.bigIconList.Images.SetKeyName(3, "");
            this.bigIconList.Images.SetKeyName(4, "");
            this.bigIconList.Images.SetKeyName(5, "");
            this.bigIconList.Images.SetKeyName(6, "");
            this.bigIconList.Images.SetKeyName(7, "");
            this.bigIconList.Images.SetKeyName(8, "");
            this.bigIconList.Images.SetKeyName(9, "");
            this.bigIconList.Images.SetKeyName(10, "");
            this.bigIconList.Images.SetKeyName(11, "");
            this.bigIconList.Images.SetKeyName(12, "");
            this.bigIconList.Images.SetKeyName(13, "");
            this.bigIconList.Images.SetKeyName(14, "");
            this.bigIconList.Images.SetKeyName(15, "");
            this.bigIconList.Images.SetKeyName(16, "");
            this.bigIconList.Images.SetKeyName(17, "");
            this.bigIconList.Images.SetKeyName(18, "");
            this.bigIconList.Images.SetKeyName(19, "");
            this.bigIconList.Images.SetKeyName(20, "");
            this.bigIconList.Images.SetKeyName(21, "");
            this.bigIconList.Images.SetKeyName(22, "");
            this.bigIconList.Images.SetKeyName(23, "");
            this.bigIconList.Images.SetKeyName(24, "");
            this.bigIconList.Images.SetKeyName(25, "");
            this.bigIconList.Images.SetKeyName(26, "");
            this.bigIconList.Images.SetKeyName(27, "");
            this.bigIconList.Images.SetKeyName(28, "");
            this.bigIconList.Images.SetKeyName(29, "");
            this.bigIconList.Images.SetKeyName(30, "");
            // 
            // lblObjectName
            // 
            this.lblObjectName.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) |
                   System.Windows.Forms.AnchorStyles.Right)));
            this.lblObjectName.Location = new System.Drawing.Point(54, 5);
            this.lblObjectName.Name = "lblObjectName";
            this.lblObjectName.Size = new System.Drawing.Size(298, 48);
            this.lblObjectName.TabIndex = 16;
            this.lblObjectName.Click += new System.EventHandler(this.lblObjectName_Click);
            // 
            // pnlPlatformStates
            // 
            this.pnlPlatformStates.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) |
                   System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPlatformStates.Location = new System.Drawing.Point(0, 56);
            this.pnlPlatformStates.Name = "pnlPlatformStates";
            this.pnlPlatformStates.Size = new System.Drawing.Size(360, 85);
            this.pnlPlatformStates.TabIndex = 17;
            // 
            // lblNodeType
            // 
            this.lblNodeType.Location = new System.Drawing.Point(4, 40);
            this.lblNodeType.Name = "lblNodeType";
            this.lblNodeType.Size = new System.Drawing.Size(120, 13);
            this.lblNodeType.TabIndex = 18;
            // 
            // ctrlWaveEditorNodeDetails
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.lblObjectName);
            this.Controls.Add(this.pnlPlatformStates);
            this.Controls.Add(this.lblNodeType);
            this.Name = "ctrlWaveEditorNodeDetails";
            this.Size = new System.Drawing.Size(360, 144);
            this.ResumeLayout(false);
        }

        #endregion
    }
}