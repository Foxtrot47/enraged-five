namespace Rave.WaveBrowser.WaveBrowser.Popups
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml.Linq;

    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    using SharpDX.DirectSound;

    using Wavelib;

    using rage;

    /// <summary>
    ///   Summary description for frmAudition.
    /// </summary>
    public class frmAudition : Form
    {
        private readonly string m_wavePath;
        private Button btnPlay;
        private Button btnStop;
        private IContainer components;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private HScrollBar hsWaveScroll;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label5;
        private Label label6;
        private Label lblBPS;
        private Label lblDuration;
        private Label lblNumChannels;
        private Label lblNumSamples;
        private Label lblSampleRate;
        private TextBox m_ChunkTextBox;
        private SoundPlayback m_soundPlayback;
        private bwWaveFile m_waveFile;
        private bwWaveVisualizer m_waveVisualizer;
        private PictureBox pbLoopDisplay;
        private PictureBox pbWaveDisplay;
        private Timer tmRedraw;
        private readonly bool _isWave;

        XElement FindBankNode(XElement searchNode)
        {
            if (searchNode == null)
            {
                return null;
            }
            if (searchNode.Name == "Bank")
            {
                return searchNode;
            }
            return this.FindBankNode(searchNode.Parent);
        }

        DateTime? FindBuildTimeStamp(XElement waveNode)
        {
            var bankNode = this.FindBankNode(waveNode);
            var attrTimeStamp = bankNode.Attribute("timeStamp");
            if (attrTimeStamp != null)
            {
                return DateTime.Parse(attrTimeStamp.Value);
            }
            return null;
        }

        public frmAudition(string strWavePath, WaveNode node)
        {
            //
            // Required for Windows Form Designer support
            //
            this.InitializeComponent();

            var sb = new StringBuilder();
            foreach (PlatformSetting ps in Configuration.ActivePlatformSettings)
            {
                sb.AppendLine(ps.Name);
                var xml = node.GetNodeXmlForPlatform(ps.PlatformTag);
                if (xml != null)
                {
                    var buildDate = this.FindBuildTimeStamp(xml);
                    if (null != buildDate)
                    {
                        sb.AppendFormat("Build date: {0}", buildDate.ToString());
                        sb.AppendLine();
                    }
                    foreach (var chunkElem in xml.Elements("Chunk"))
                    {
                        sb.AppendFormat(
                            "{0}: {1} bytes", 
                            chunkElem.Attribute("name").Value, 
                            chunkElem.Attribute("size").Value);
                        sb.AppendLine();

                        foreach (var markerElem in chunkElem.Elements("Marker"))
                        {
                            var timeMsAttr = markerElem.Attribute("timeMs");
                            var timeMs = null != timeMsAttr ? timeMsAttr.Value : string.Empty;
                            sb.AppendFormat(" - {0}: {1}", markerElem.Value, timeMs);
                            sb.AppendLine();
                        }
                    }
                }
                sb.AppendLine();
            }

            this.m_ChunkTextBox.Text = sb.ToString();
            this._isWave = node.GetObjectName().ToUpper().EndsWith(".WAV");
            this.m_wavePath = strWavePath;
            this.Text += " - " + this.m_wavePath;
        }

        /// <summary>
        ///   Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        private void frmAudition_Load(object sender, EventArgs e)
        {
            if (!this._isWave)
            {
                this.btnPlay.Enabled = false;
                this.btnStop.Enabled = false;
                this.pbWaveDisplay.Enabled = false;
                this.pbLoopDisplay.Enabled = false;
                this.hsWaveScroll.Enabled = false;
                this.groupBox3.Visible = false;
            }
            else
            {
                this.m_soundPlayback = new SoundPlayback(this);

                this.m_waveFile = new bwWaveFile(this.m_wavePath, false);
                this.m_soundPlayback.CreateSound(this.m_waveFile);

                this.m_waveVisualizer = new bwWaveVisualizer(this.m_waveFile);

                // if there is a loop display the loop display, otherwise hide it and
                // reposition the main wave display
                if (null == this.m_waveFile.Sample || this.m_waveFile.Sample.Loops.Count != 1)
                {
                    this.pbLoopDisplay.Visible = false;
                    this.pbWaveDisplay.Width = this.pbLoopDisplay.Right - this.pbWaveDisplay.Left;
                    this.hsWaveScroll.Width = this.pbLoopDisplay.Right - this.hsWaveScroll.Left;
                }

                this.lblSampleRate.Text = this.m_waveFile.Format.SampleRate + "Hz";
                this.lblBPS.Text = this.m_waveFile.Format.SignificantBitsPerSample.ToString(CultureInfo.InvariantCulture);
                this.lblNumChannels.Text = this.m_waveFile.Format.NumChannels.ToString(CultureInfo.InvariantCulture);
                this.lblNumSamples.Text = this.m_waveFile.Data.NumSamples.ToString(CultureInfo.InvariantCulture);
                var ms = (this.m_waveFile.Data.NumSamples /
                          (float)(this.m_waveFile.Format.SampleRate * this.m_waveFile.Format.NumChannels)) * 1000.0f;
                this.lblDuration.Text = ((int) ms) + "ms";

                this.hsWaveScroll.Maximum = (int)this.m_waveFile.Data.NumSamples;
            }
        }

        private void pbWaveDisplay_Paint(object sender, PaintEventArgs e)
        {
            if (this._isWave)
            {
                this.m_waveVisualizer.DrawWave(e);
            }
        }

        private void pbWaveDisplay_Click(object sender, EventArgs e)
        {
            this.pbWaveDisplay.Focus();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            var flags = this.m_waveFile.IsLooping() ? PlayFlags.Looping : PlayFlags.None;
            this.m_soundPlayback.Play(0, flags);
        }

        private void tmRedraw_Tick(object sender, EventArgs e)
        {
            if (this._isWave)
            {
                if (!this.m_soundPlayback.IsBufferNull())
                {
                    this.m_waveVisualizer.UpdateCursor(this.m_soundPlayback.GetPlaybackPosition());
                    this.pbWaveDisplay.Refresh();
                    this.pbLoopDisplay.Refresh();
                } 
            }
        }

        private void hsWaveScroll_Scroll(object sender, ScrollEventArgs e)
        {
            this.m_waveVisualizer.XOffset = e.NewValue;
        }

        private void frmAudition_Activated(object sender, EventArgs e)
        {
            this.tmRedraw.Enabled = true;
        }

        private void frmAudition_Deactivate(object sender, EventArgs e)
        {
            this.tmRedraw.Enabled = false;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (!this.m_soundPlayback.IsBufferNull())
            {
                this.m_soundPlayback.Stop();
            }
        }

        private void pbLoopDisplay_Paint(object sender, PaintEventArgs e)
        {
            if (this._isWave)
            {
                if (null != this.m_waveFile && this.m_waveFile.Sample.Loops.Count == 1)
                {
                    this.m_waveVisualizer.DrawLoop(e);
                } 
            }
        }

        private void frmAudition_Closing(object sender, CancelEventArgs e)
        {
            if (this._isWave)
            {
                if (!this.m_soundPlayback.IsBufferNull())
                {
                    this.m_soundPlayback.Stop();
                }
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///   Required method for Designer support - do not modify
        ///   the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof (frmAudition));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pbLoopDisplay = new System.Windows.Forms.PictureBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.hsWaveScroll = new System.Windows.Forms.HScrollBar();
            this.btnPlay = new System.Windows.Forms.Button();
            this.pbWaveDisplay = new System.Windows.Forms.PictureBox();
            this.tmRedraw = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblDuration = new System.Windows.Forms.Label();
            this.lblNumSamples = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblSampleRate = new System.Windows.Forms.Label();
            this.lblNumChannels = new System.Windows.Forms.Label();
            this.lblBPS = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_ChunkTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.pbLoopDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbWaveDisplay)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) |
                    System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.pbLoopDisplay);
            this.groupBox2.Controls.Add(this.btnStop);
            this.groupBox2.Controls.Add(this.hsWaveScroll);
            this.groupBox2.Controls.Add(this.btnPlay);
            this.groupBox2.Controls.Add(this.pbWaveDisplay);
            this.groupBox2.Location = new System.Drawing.Point(8, 136);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(656, 240);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Audition";
            // 
            // pbLoopDisplay
            // 
            this.pbLoopDisplay.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) |
                   System.Windows.Forms.AnchorStyles.Right)));
            this.pbLoopDisplay.BackColor = System.Drawing.Color.White;
            this.pbLoopDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbLoopDisplay.Location = new System.Drawing.Point(472, 24);
            this.pbLoopDisplay.Name = "pbLoopDisplay";
            this.pbLoopDisplay.Size = new System.Drawing.Size(176, 160);
            this.pbLoopDisplay.TabIndex = 6;
            this.pbLoopDisplay.TabStop = false;
            this.pbLoopDisplay.Paint += new System.Windows.Forms.PaintEventHandler(this.pbLoopDisplay_Paint);
            // 
            // btnStop
            // 
            this.btnStop.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStop.Location = new System.Drawing.Point(88, 208);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(72, 24);
            this.btnStop.TabIndex = 5;
            this.btnStop.Text = "Stop";
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // hsWaveScroll
            // 
            this.hsWaveScroll.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 (((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) |
                   System.Windows.Forms.AnchorStyles.Right)));
            this.hsWaveScroll.Location = new System.Drawing.Point(8, 184);
            this.hsWaveScroll.Name = "hsWaveScroll";
            this.hsWaveScroll.Size = new System.Drawing.Size(456, 16);
            this.hsWaveScroll.TabIndex = 4;
            this.hsWaveScroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsWaveScroll_Scroll);
            // 
            // btnPlay
            // 
            this.btnPlay.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPlay.Location = new System.Drawing.Point(8, 208);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(72, 24);
            this.btnPlay.TabIndex = 3;
            this.btnPlay.Text = "Play";
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // pbWaveDisplay
            // 
            this.pbWaveDisplay.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) |
                    System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.pbWaveDisplay.BackColor = System.Drawing.Color.White;
            this.pbWaveDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbWaveDisplay.Location = new System.Drawing.Point(8, 24);
            this.pbWaveDisplay.Name = "pbWaveDisplay";
            this.pbWaveDisplay.Size = new System.Drawing.Size(456, 160);
            this.pbWaveDisplay.TabIndex = 2;
            this.pbWaveDisplay.TabStop = false;
            this.pbWaveDisplay.Click += new System.EventHandler(this.pbWaveDisplay_Click);
            this.pbWaveDisplay.Paint += new System.Windows.Forms.PaintEventHandler(this.pbWaveDisplay_Paint);
            // 
            // tmRedraw
            // 
            this.tmRedraw.Enabled = true;
            this.tmRedraw.Tick += new System.EventHandler(this.tmRedraw_Tick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblDuration);
            this.groupBox3.Controls.Add(this.lblNumSamples);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.lblSampleRate);
            this.groupBox3.Controls.Add(this.lblNumChannels);
            this.groupBox3.Controls.Add(this.lblBPS);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(8, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(304, 120);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Original Wave Properties";
            // 
            // lblDuration
            // 
            this.lblDuration.Location = new System.Drawing.Point(136, 88);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(112, 16);
            this.lblDuration.TabIndex = 9;
            this.lblDuration.Text = "_duration";
            // 
            // lblNumSamples
            // 
            this.lblNumSamples.Location = new System.Drawing.Point(136, 72);
            this.lblNumSamples.Name = "lblNumSamples";
            this.lblNumSamples.Size = new System.Drawing.Size(112, 16);
            this.lblNumSamples.TabIndex = 8;
            this.lblNumSamples.Text = "_numSamples";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(16, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "Duration:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Number of Samples:";
            // 
            // lblSampleRate
            // 
            this.lblSampleRate.Location = new System.Drawing.Point(136, 56);
            this.lblSampleRate.Name = "lblSampleRate";
            this.lblSampleRate.Size = new System.Drawing.Size(112, 16);
            this.lblSampleRate.TabIndex = 5;
            this.lblSampleRate.Text = "_sampleRate";
            // 
            // lblNumChannels
            // 
            this.lblNumChannels.Location = new System.Drawing.Point(136, 40);
            this.lblNumChannels.Name = "lblNumChannels";
            this.lblNumChannels.Size = new System.Drawing.Size(112, 16);
            this.lblNumChannels.TabIndex = 4;
            this.lblNumChannels.Text = "_numChannels";
            // 
            // lblBPS
            // 
            this.lblBPS.Location = new System.Drawing.Point(136, 24);
            this.lblBPS.Name = "lblBPS";
            this.lblBPS.Size = new System.Drawing.Size(112, 16);
            this.lblBPS.TabIndex = 3;
            this.lblBPS.Text = "_bps";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Number of Channels:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Sample Rate:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bits per Sample:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) |
                   System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.m_ChunkTextBox);
            this.groupBox1.Location = new System.Drawing.Point(318, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 116);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Built Wave Properties";
            // 
            // m_ChunkTextBox
            // 
            this.m_ChunkTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ChunkTextBox.Location = new System.Drawing.Point(3, 16);
            this.m_ChunkTextBox.Multiline = true;
            this.m_ChunkTextBox.Name = "m_ChunkTextBox";
            this.m_ChunkTextBox.ReadOnly = true;
            this.m_ChunkTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.m_ChunkTextBox.Size = new System.Drawing.Size(336, 97);
            this.m_ChunkTextBox.TabIndex = 0;
            // 
            // frmAudition
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(672, 382);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.Name = "frmAudition";
            this.Text = "Wave Properties";
            this.Deactivate += new System.EventHandler(this.frmAudition_Deactivate);
            this.Load += new System.EventHandler(this.frmAudition_Load);
            this.Activated += new System.EventHandler(this.frmAudition_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmAudition_Closing);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.pbLoopDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbWaveDisplay)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
        }

        #endregion
    }
}