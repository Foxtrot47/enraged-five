namespace Rave.WaveBrowser.WaveBrowser.Popups
{
    using System;
    using System.Windows.Forms;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    ///   Summary description for frmCommit.
    /// </summary>
    public class frmCommit : Form
    {
        private Button btnNo;
        private Button btnYes;
        private Label label1;

        private bool m_result;
        private TextBox txtDescription;
        private Label label2;
        private TextBox txtActions;

        public frmCommit(IActionLog actionLog)
        {
            //
            // Required for Windows Form Designer support
            //
            this.InitializeComponent();

            this.txtActions.Text = actionLog.GetCommitSummary();
            this.txtActions.Select(0, 0);
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.m_result = true;
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.m_result = false;
            this.Close();
        }

        public bool GetResult()
        {
            return this.m_result;
        }

        public String GetPerforceChangelistDescription()
        {
            return String.IsNullOrWhiteSpace(txtDescription.Text) ? "Wavebrowser commit" : txtDescription.Text;
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///   Required method for Designer support - do not modify
        ///   the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtActions = new System.Windows.Forms.TextBox();
            this.btnYes = new System.Windows.Forms.Button();
            this.btnNo = new System.Windows.Forms.Button();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Actions to commit:";
            // 
            // txtActions
            // 
            this.txtActions.Location = new System.Drawing.Point(8, 28);
            this.txtActions.Multiline = true;
            this.txtActions.Name = "txtActions";
            this.txtActions.ReadOnly = true;
            this.txtActions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtActions.Size = new System.Drawing.Size(568, 315);
            this.txtActions.TabIndex = 1;
            // 
            // btnYes
            // 
            this.btnYes.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnYes.Location = new System.Drawing.Point(344, 492);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(112, 24);
            this.btnYes.TabIndex = 3;
            this.btnYes.Text = "Commit";
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // btnNo
            // 
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNo.Location = new System.Drawing.Point(464, 492);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(112, 24);
            this.btnNo.TabIndex = 4;
            this.btnNo.Text = "Cancel";
            this.btnNo.Click += new System.EventHandler(this.btnNo_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(8, 380);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(568, 113);
            this.txtDescription.TabIndex = 5;
            this.txtDescription.Text = "Wavebrowser commit";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 357);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Perforce changelist description:";
            // 
            // frmCommit
            // 
            this.AcceptButton = this.btnYes;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.btnNo;
            this.ClientSize = new System.Drawing.Size(584, 524);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.btnNo);
            this.Controls.Add(this.btnYes);
            this.Controls.Add(this.txtActions);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCommit";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Confirm Commit ...";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}