// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmTagEditor.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for frmTagEditor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.WaveBrowser.WaveBrowser.TagEditor
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Forms;

    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.UserActions;

    /// <summary>
    /// Tag editor.
    /// </summary>
    public class frmTagEditor : Form
    {
        #region Fields

        /// <summary>
        /// The m_action log.
        /// </summary>
        private readonly IActionLog m_actionLog;

        /// <summary>
        /// The tags.
        /// </summary>
        private readonly IList<TagData> tags;

        /// <summary>
        /// The tag editor mode.
        /// </summary>
        private readonly TagEditorMode tagEditorMode;

        /// <summary>
        /// The tag type mode.
        /// </summary>
        private readonly TagType tagTypeMode;

        /// <summary>
        /// The original name.
        /// </summary>
        private readonly string originalName;

        /// <summary>
        /// The m_wave browser.
        /// </summary>
        private readonly IWaveBrowser m_waveBrowser;

        /// <summary>
        /// The btn cancel.
        /// </summary>
        private Button btnCancel;

        /// <summary>
        /// The btn ok.
        /// </summary>
        private Button btnOk;

        /// <summary>
        /// The cb tag name.
        /// </summary>
        private ComboBox cbTagName;

        /// <summary>
        /// The components.
        /// </summary>
        private IContainer components;

        /// <summary>
        /// The label 2.
        /// </summary>
        private Label label2;

        /// <summary>
        /// The m_ platform panel.
        /// </summary>
        private Panel m_PlatformPanel;

        /// <summary>
        /// The m_ tool tip.
        /// </summary>
        private ToolTip m_ToolTip;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="frmTagEditor"/> class.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="tagTypeMode">
        /// The tag mode.
        /// </param>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        public frmTagEditor(EditorTreeNode parentNode, IActionLog actionLog, TagType tagTypeMode, IWaveBrowser waveBrowser)
            : this(actionLog, waveBrowser, tagTypeMode)
        {
            this.tagEditorMode = TagEditorMode.SingleTag;
            this.InitTagData(parentNode);
            this.InitControls();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="frmTagEditor"/> class.
        /// </summary>
        /// <param name="parentNodes">
        /// The nodes.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="tagTypeMode">
        /// The tag mode.
        /// </param>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        public frmTagEditor(IList<EditorTreeNode> parentNodes, IActionLog actionLog, TagType tagTypeMode, IWaveBrowser waveBrowser)
            : this(actionLog, waveBrowser, tagTypeMode)
        {
            this.tagEditorMode = parentNodes.Count() > 1 ? TagEditorMode.MultipleTags : TagEditorMode.SingleTag;

            foreach (var node in parentNodes)
            {
                this.InitTagData(node);
            }

            this.InitControls();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="frmTagEditor"/> class.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        public frmTagEditor(
            TagNode node, IActionLog actionLog, IWaveBrowser waveBrowser)
            : this(actionLog, waveBrowser, TagType.Default)
        {
            this.tagEditorMode = TagEditorMode.SingleTag;
            this.originalName = node.GetTagName();

            var tag = new TagData
            {
                CurrentValues = new Dictionary<string, string>(),
                OriginalPlatforms = node.GetPlatforms(),
                OriginalValues = new Dictionary<string, string>(),
                ParentNode = node.Parent as EditorTreeNode,
                TagNode = node
            };

            foreach (var platform in tag.OriginalPlatforms)
            {
                tag.CurrentValues.Add(platform, node.GetCurrentValue(platform));
                tag.OriginalValues.Add(platform, node.GetCurrentValue(platform));
            }

            this.tags.Add(tag);

            this.InitControls();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="frmTagEditor"/> class.
        /// </summary>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="tagTypeMode">
        /// The tag mode.
        /// </param>
        private frmTagEditor(IActionLog actionLog, IWaveBrowser waveBrowser, TagType tagTypeMode)
        {
            this.m_actionLog = actionLog;
            this.m_waveBrowser = waveBrowser;
            this.tagTypeMode = tagTypeMode;
            this.tags = new List<TagData>();
            this.originalName = tagTypeMode == TagType.Voice ? "voice" : null;

            this.InitializeComponent();
        }

        #endregion

        /// <summary>
        /// The tag editor mode.
        /// </summary>
        public enum TagEditorMode
        {
            /// <summary>
            /// The single tag mode.
            /// </summary>
            SingleTag,

            /// <summary>
            /// The multiple tags mode.
            /// </summary>
            MultipleTags,
        }

        #region Methods

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Initialize the tag data.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        private void InitTagData(EditorTreeNode parentNode)
        {
            var tag = new TagData
            {
                CurrentValues = new Dictionary<string, string>(),
                OriginalPlatforms = new List<string>(),
                OriginalValues = new Dictionary<string, string>(),
                ParentNode = parentNode
            };

            this.tags.Add(tag);
        }

        /// <summary>
        /// Initialize the form controls.
        /// </summary>
        private void InitControls()
        {
            if (this.tagEditorMode == TagEditorMode.SingleTag && null != this.tags[0].TagNode)
            {
                this.cbTagName.Enabled = false;
            }

            var allPlatforms = new HashSet<string>();
            var selectedPlatforms = new HashSet<string>();
            foreach (var tag in this.tags)
            {
                foreach (var platform in tag.ParentNode.GetPlatforms())
                {
                    allPlatforms.Add(platform);
                }

                foreach (var platform in tag.OriginalPlatforms)
                {
                    selectedPlatforms.Add(platform);
                }
            }

            var currentY = 5;
            foreach (var platform in allPlatforms)
            {
                this.m_PlatformPanel.Controls.Add(
                    new Label { Text = platform, Top = currentY, Height = 20, Left = 0, Width = 100 });

                var cbIncluded = new CheckBox { Top = currentY, Height = 20, Left = 105, Width = 20, Tag = platform };
                cbIncluded.CheckedChanged += this.cbIncluded_CheckedChanged;

                if ((this.tagEditorMode == TagEditorMode.SingleTag && null == this.tags[0].TagNode) || selectedPlatforms.Count() == 0)
                {
                    if (ctrlWaveBrowser.ActivePlatform == ctrlWaveBrowser.ALL_PLATFORMS
                        || ctrlWaveBrowser.ActivePlatform == platform)
                    {
                        cbIncluded.Checked = true;
                    }
                }
                else
                {
                    if (selectedPlatforms.Contains(platform))
                    {
                        cbIncluded.Checked = true;
                    }
                }

                this.m_PlatformPanel.Controls.Add(cbIncluded);

                var txtValue = new TextBox { Top = currentY, Height = 20, Left = 130, Width = 200, Tag = platform };
                txtValue.TextChanged += this.txtValue_TextChanged;

                if (cbIncluded.Checked && this.tagEditorMode == TagEditorMode.SingleTag)
                {
                    txtValue.Enabled = true;
                    if (null != this.tags[0].TagNode)
                    {
                        txtValue.Text = this.tags[0].OriginalValues[platform];
                    }
                    else if (this.tagTypeMode == TagType.Voice)
                    {
                        txtValue.Text = this.tags[0].ParentNode.GetObjectName();
                    }
                }
                else
                {
                    txtValue.Enabled = false;
                }

                this.m_PlatformPanel.Controls.Add(txtValue);

                currentY += 25;
            }

            foreach (var waveAttribute in Configuration.WaveAttributes)
            {
                this.cbTagName.Items.Add(waveAttribute);
            }

            this.cbTagName.Text = string.IsNullOrEmpty(this.originalName) ? this.cbTagName.Items[0].ToString() : this.originalName;
        }

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbTagName = new System.Windows.Forms.ComboBox();
            this.m_PlatformPanel = new System.Windows.Forms.Panel();
            this.m_ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();

            // btnOk
            this.btnOk.Anchor =
                (System.Windows.Forms.AnchorStyles)
                ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOk.Location = new System.Drawing.Point(112, 160);
            this.btnOk.Name = "btnOk";
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);

            // btnCancel
            this.btnCancel.Anchor =
                (System.Windows.Forms.AnchorStyles)
                ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(192, 160);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);

            // label2
            this.label2.Location = new System.Drawing.Point(8, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Name:";

            // cbTagName
            this.cbTagName.Anchor =
                (System.Windows.Forms.AnchorStyles)
                (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right));
            this.cbTagName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTagName.Location = new System.Drawing.Point(64, 16);
            this.cbTagName.Name = "cbTagName";
            this.cbTagName.Size = new System.Drawing.Size(256, 21);
            this.cbTagName.TabIndex = 1;

            // m_PlatformPanel
            this.m_PlatformPanel.Anchor =
                (System.Windows.Forms.AnchorStyles)
                ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                   | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right));
            this.m_PlatformPanel.AutoScroll = true;
            this.m_PlatformPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_PlatformPanel.Location = new System.Drawing.Point(8, 48);
            this.m_PlatformPanel.Name = "m_PlatformPanel";
            this.m_PlatformPanel.Size = new System.Drawing.Size(344, 104);
            this.m_PlatformPanel.TabIndex = 9;
            this.m_ToolTip.SetToolTip(this.m_PlatformPanel, "Platform specific settings");

            // frmTagEditor
            this.AcceptButton = this.btnOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(360, 190);
            this.ControlBox = false;
            this.Controls.Add(this.m_PlatformPanel);
            this.Controls.Add(this.cbTagName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Name = "frmTagEditor";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Tag Editor";
            this.ResumeLayout(false);
        }

        /// <summary>
        /// The btn cancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// The btn ok_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            var platforms = new List<string>();

            foreach (Control control in this.m_PlatformPanel.Controls)
            {
                if (control.GetType() != typeof(CheckBox))
                {
                    continue;
                }

                if (((CheckBox)control).Checked)
                {
                    platforms.Add(control.Tag.ToString());
                }
            }

            var actions = new List<IUserAction>();

            foreach (var tag in this.tags)
            {
                var toAdd = new Dictionary<string, string>();
                var toModify = new Dictionary<string, string>();

                foreach (var platform in platforms)
                {
                    if (!tag.OriginalPlatforms.Contains(platform))
                    {
                        toAdd.Add(
                            platform,
                            tag.CurrentValues.ContainsKey(platform) ? tag.CurrentValues[platform] : string.Empty);
                    }
                    else
                    {
                        if (tag.OriginalValues[platform] != tag.CurrentValues[platform])
                        {
                            toModify.Add(platform, tag.CurrentValues[platform]);
                        }
                    }
                }

                var toRemove = tag.OriginalPlatforms.Where(platform => !platforms.Contains(platform)).ToList();

                if (toAdd.Count > 0)
                {
                    var action =
                        new AddTagAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                tag.ParentNode,
                                null,
                                null,
                                null,
                                this.m_actionLog,
                                this.cbTagName.Text,
                                new List<string>(toAdd.Keys),
                                toAdd,
                                null));

                    if (action.Action())
                    {
                        if (this.tagEditorMode == TagEditorMode.SingleTag)
                        {
                            this.m_actionLog.AddAction(action);
                        }
                        else
                        {
                            actions.Add(action);
                        }
                    }
                }

                if (toModify.Count > 0)
                {
                    var action =
                        new ModifyTagAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                tag.ParentNode,
                                tag.TagNode,
                                null,
                                null,
                                this.m_actionLog,
                                this.cbTagName.Text,
                                new List<string>(toModify.Keys),
                                toModify,
                                null));

                    if (action.Action())
                    {
                        if (this.tagEditorMode == TagEditorMode.SingleTag)
                        {
                            this.m_actionLog.AddAction(action);
                        }
                        else
                        {
                            actions.Add(action);
                        }
                    }
                }

                if (tag.TagNode != null && toRemove.Count > 0)
                {
                    var action =
                        new DeleteTagAction(
                            new ActionParams(
                                this.m_waveBrowser,
                                tag.ParentNode,
                                tag.TagNode,
                                null,
                                null,
                                null,
                                tag.TagNode.GetObjectName(),
                                toRemove,
                                null,
                                null));

                    if (action.Action())
                    {
                        if (this.tagEditorMode == TagEditorMode.SingleTag)
                        {
                            this.m_actionLog.AddAction(action);
                        }
                        else
                        {
                            actions.Add(action);
                        }
                    }
                }
            }

            // Process as bulk action if editing more than one tag node
            if (actions.Any())
            {
                var action = this.m_waveBrowser.CreateAction(
                    ActionType.BulkAction,
                    new ActionParams(
                        null,
                        null,
                        null,
                        null,
                        null,
                        this.m_actionLog,
                        null,
                        null,
                        null,
                        new ArrayList(actions)));
                if (action.Action())
                {
                    this.m_actionLog.AddAction(action);
                }
            }
        }

        /// <summary>
        /// The cb included_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void cbIncluded_CheckedChanged(object sender, EventArgs e)
        {
            var cb = (CheckBox)sender;
            foreach (Control control in this.m_PlatformPanel.Controls)
            {
                if (control.GetType() == typeof(TextBox) && control.Tag == cb.Tag)
                {
                    control.Enabled = cb.Checked;
                }
            }
        }

        /// <summary>
        /// The txt value_ text changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            var txt = (TextBox)sender;
            txt.Text = txt.Text.Replace(' ', '_');
            var platform = txt.Tag.ToString();

            foreach (var tag in this.tags)
            {
                tag.CurrentValues[platform] = txt.Text;
            }

            txt.SelectionStart = txt.Text.Length;
        }

        #endregion
    }
}