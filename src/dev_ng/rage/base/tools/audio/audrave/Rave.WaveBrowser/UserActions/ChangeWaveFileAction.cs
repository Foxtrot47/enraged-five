﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChangeWaveFileAction.cs" company="Rockstar North">
//   Change file action - replaces 'live' file with temp file during commit.
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using System.Xml.XPath;

    using Rave.Instance;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    /// Encapsulates the action of a user adding/modifying a file associated with a wave.
    /// </summary>
    public class ChangeWaveFileAction : UserAction
    {
        /// <summary>
        /// The file name no ext.
        /// </summary>
        private readonly string fileNameNoExt;

        /// <summary>
        /// The source directory path.
        /// </summary>
        private readonly string sourceDirPath;

        /// <summary>
        /// The destination directory path.
        /// </summary>
        private readonly string destDirPath;

        /// <summary>
        /// The source wave path.
        /// </summary>
        private readonly string sourceWavePath;

        /// <summary>
        /// The destination wave path.
        /// </summary>
        private readonly string destWavePath;

        /// <summary>
        /// The parent path.
        /// </summary>
        private readonly string parentPath;

        /// <summary>
        /// The wave name.
        /// </summary>
        private readonly string waveName;

        /// <summary>
        /// The previous states.
        /// </summary>
        private readonly IDictionary<string, EditorTreeNode.NodeDisplayState> previousStates;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChangeWaveFileAction"/> class.
        /// </summary>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        public ChangeWaveFileAction(IActionParams parameters)
        {
            this.ActionParameters = parameters;

            this.fileNameNoExt = Path.GetFileName(Path.GetFileNameWithoutExtension(this.ActionParameters.SourcePath));
            this.sourceDirPath = Path.GetDirectoryName(parameters.SourcePath);
            this.destDirPath = Path.Combine(Configuration.PlatformWavePath, parameters.ParentNode.GetObjectPath());
            this.sourceWavePath = this.ActionParameters.SourcePath;
            this.destWavePath = Path.Combine(this.destDirPath, Path.GetFileName(this.ActionParameters.SourcePath));

            this.waveName = Path.GetFileName(this.ActionParameters.SourcePath.ToUpper().Replace(" ", "_"));
            this.parentPath = this.ActionParameters.ParentNode.GetObjectPath();

            this.previousStates = new Dictionary<string, EditorTreeNode.NodeDisplayState>();
        }

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                return this.ActionParameters.Node;
            }
        }

        /// <summary>
        /// Commit the changes.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        protected override bool doCommit()
        {
            try
            {
                if (RaveInstance.RaveAssetManager.WaveChangeList == null)
                {
                    RaveInstance.RaveAssetManager.WaveChangeList =
                        RaveInstance.AssetManager.CreateChangeList("[Rave] Wave/Midi Changelist");
                }

                // Original wave needs to be checked out at this point
                if (!RaveInstance.AssetManager.IsCheckedOut(this.destWavePath))
                {
                    if (null == RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(this.destWavePath, true))
                    {
                        ErrorManager.HandleError("Failed to check out destination wave file: " + this.destWavePath);
                        return false;
                    }
                }

                if (File.Exists(this.destWavePath))
                {
                    new FileInfo(this.destWavePath).IsReadOnly = false;
                }

                File.Copy(this.sourceWavePath, this.destWavePath, true);

                // Copy any other required files
                foreach (var ext in this.ActionParameters.OtherFileExts)
                {
                    var sourcePath = Path.Combine(this.sourceDirPath, this.fileNameNoExt + ext);
                    var destPath = Path.Combine(this.destDirPath, this.fileNameNoExt + ext);

                    if (!File.Exists(sourcePath))
                    {
                        throw new Exception("Source file does not exist: " + sourcePath);
                    }

                    // Checkout or add destination file
                    if (RaveInstance.AssetManager.ExistsAsAsset(destPath))
                    {
                        // Original file needs to be checked out at this point
                        if (!RaveInstance.AssetManager.IsCheckedOut(destPath))
                        {
                            if (null == RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(destPath, true))
                            {
                                ErrorManager.HandleError("Failed to check out destination file: " + destPath);
                                return false;
                            }
                        }
                    }
                    else
                    {
                        RaveInstance.RaveAssetManager.WaveChangeList.MarkAssetForAdd(destPath);
                    }

                    if (File.Exists(destPath))
                    {
                        new FileInfo(destPath).IsReadOnly = false;
                    }

                    File.Copy(sourcePath, destPath, true);
                }

                // Mark wave asset as modified in pending waves
                foreach (var platform in this.ActionParameters.Platforms)
                {
                    var pendingWaveList = this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform);
                    if (null == pendingWaveList)
                    {
                        throw new Exception("Failed to get pending wave list for platform: " + platform);
                    }

                    var path = Path.Combine(this.parentPath, this.waveName);
                    pendingWaveList.RecordModifyWave(path);
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Undo the change file action.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        protected override bool doUndo()
        {
            foreach (var platform in this.ActionParameters.Platforms)
            {
                this.ActionParameters.Node.UpdateDisplayState(this.previousStates[platform], platform, true, true);
                this.previousStates.Remove(platform);
            }

            return true;
        }

        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            try
            {
                // Original wave needs to be checked out at this point
                if (!RaveInstance.AssetManager.IsCheckedOut(this.destWavePath))
                {
                    if (null == RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(this.destWavePath, true))
                    {
                        ErrorManager.HandleError("Failed to check out destination wave file: " + this.destWavePath);
                        return false;
                    }
                }

                if (File.Exists(this.destWavePath))
                {
                    new FileInfo(this.destWavePath).IsReadOnly = false;
                }

                File.Copy(this.sourceWavePath, this.destWavePath, true);

                // Copy any other required files
                foreach (var ext in this.ActionParameters.OtherFileExts)
                {
                    var sourcePath = Path.Combine(this.sourceDirPath, this.fileNameNoExt + ext);
                    var destPath = Path.Combine(this.destDirPath, this.fileNameNoExt + ext);

                    if (!File.Exists(sourcePath))
                    {
                        throw new Exception("Source file does not exist: " + sourcePath);
                    }

                    // Checkout or add destination file
                    if (RaveInstance.AssetManager.ExistsAsAsset(destPath))
                    {
                        // Original file needs to be checked out at this point
                        if (!RaveInstance.AssetManager.IsCheckedOut(destPath))
                        {
                            if (null == RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(destPath, true))
                            {
                                ErrorManager.HandleError("Failed to check out destination file: " + destPath);
                                return false;
                            }
                        }
                    }

                    if (File.Exists(destPath))
                    {
                        new FileInfo(destPath).IsReadOnly = false;
                    }

                    File.Copy(sourcePath, destPath, true);
                }


                var path = Path.Combine(this.parentPath, this.waveName);
                temporaryWaveList.RecordModifyWave(path);

            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// The action.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        protected override bool doAction()
        {
            if (this.ActionParameters.ActionLog.SearchForWaveDeletion(this.destWavePath) != null)
            {
                ErrorManager.HandleInfo("There is a pending delete for " + this.destWavePath +
                                        " - you must commit your changes before trying to add this file again");
                return false;
            }

            if (this.ActionParameters.ActionLog.SearchForWaveAdd(this.destWavePath) != null)
            {
                ErrorManager.HandleInfo("There is a pending add for " + this.destWavePath +
                                        " - you must commit your changes before trying to edit this file");
                return false;
            }

            if (this.ActionParameters.ParentNode.TryToLock())
            {
                foreach (var platform in this.ActionParameters.Platforms)
                {
                    this.previousStates.Add(platform, this.ActionParameters.Node.GetNodeDisplayState(platform));
                    this.ActionParameters.Node.UpdateDisplayState(
                        EditorTreeNode.NodeDisplayState.ChangedLocally,
                        platform,
                        true,
                        true);
                }

                this.ActionParameters.ParentNode.Expand();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get the action summary.
        /// </summary>
        /// <returns>
        /// The action summary <see cref="string"/>.
        /// </returns>
        public override string GetSummary()
        {
            var platforms = string.Join(", ", this.ActionParameters.Platforms);
            var sb = new StringBuilder();
            sb.Append("change wave ");
            sb.Append(this.waveName);
            sb.Append(" in ");
            sb.Append(this.parentPath);
            sb.Append(" changed on ");
            sb.Append(platforms);
            return sb.ToString();
        }

        /// <summary>
        /// Get the asset path.
        /// </summary>
        /// <returns>
        /// The asset path <see cref="string"/>.
        /// </returns>
        public override string GetAssetPath()
        {
            return this.destWavePath;
        }

        /// <summary>
        /// Validates the copying of the .gesture file to make sure that the src file contains at least 1 marker in it
        /// provided the dest file has one.
        /// </summary>
        /// <param name="errors">String builder for errors.</param>
        /// <param name="warnings">String builder for warnings.</param>
        public void Validate(StringBuilder errors, StringBuilder warnings)
        {
            // Are we copying gesture files across?
            if (this.ActionParameters.OtherFileExts.Any(item => item == ".gesture"))
            {
                // Check whether the destination file contains any markers.
                bool checkSourceForMarkers = false;

                String destPath = Path.Combine(this.destDirPath, this.fileNameNoExt + ".gesture");
                if (File.Exists(destPath))
                {
                    XDocument doc = XDocument.Load(destPath);
                    IEnumerable<XElement> markerElems = doc.XPathSelectElements("//bwGestureMarker");
                    if (markerElems.Any())
                    {
                        checkSourceForMarkers = true;
                    }
                }

                if (checkSourceForMarkers)
                {
                    // Make sure that the source file exists and contains at least 1 marker.
                    String sourcePath = Path.Combine(this.sourceDirPath, this.fileNameNoExt + ".gesture");
                    if (File.Exists(sourcePath))
                    {
                        XDocument doc = XDocument.Load(sourcePath);
                        IEnumerable<XElement> markerElems = doc.XPathSelectElements("//bwGestureMarker");
                        if (!markerElems.Any())
                        {
                            warnings.AppendLine(String.Format("Source file '{0}' doesn't contain any gesture markers.", sourcePath));
                        }
                    }
                    else
                    {
                        warnings.AppendLine(String.Format("Source file '{0}' doesn't exist.", sourcePath));
                    }
                }
            }
        }
    }
}
