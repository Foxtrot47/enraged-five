namespace Rave.WaveBrowser.WaveBrowser
{
    using System;
    using System.Windows.Forms;

    using SharpDX;
    using SharpDX.DirectSound;
    using SharpDX.Multimedia;

    using Wavelib;

    /// <summary>
    ///   Summary description for SoundPlayback.
    /// </summary>
    public class SoundPlayback
    {

        #region Fields

        private readonly Control m_owner;
        private readonly DirectSound m_directSound;
        private SecondarySoundBuffer m_buffer;

        #endregion

        #region Constructors

        public SoundPlayback(Control owner)
        {
            this.m_owner = owner;
            this.m_directSound = new DirectSound();
        }

        #endregion

        #region Public Methods

        public void Play(int priority, PlayFlags flags)
        {
            if (!this.IsBufferNull())
            {
                this.m_buffer.Play(priority, flags);
            }
        }

        public void Pause()
        {
            if (!this.IsBufferNull())
            {
                this.m_buffer.Stop();
            }
        }

        public void Stop()
        {
            if (!this.IsBufferNull())
            {
                this.m_buffer.Stop();
                this.m_buffer.CurrentPosition = 0;
            }
        }

        public bool IsBufferNull()
        {
            return this.m_buffer == null;
        }

        public int GetPlaybackPosition()
        {
            if (!this.IsBufferNull())
            {
                int playOffset, writeOffset;
                this.m_buffer.GetCurrentPosition(out playOffset, out writeOffset);

                return playOffset;
            }
            return 0;
        }

        public void CreateSound(bwWaveFile reader)
        {
            this.m_directSound.SetCooperativeLevel(this.m_owner.Handle, CooperativeLevel.Priority);

            var desc = new SoundBufferDescription();

            var rate = (int) reader.Format.SampleRate;
            var bits = (short) reader.Format.SignificantBitsPerSample;
            var numChannels = (short) reader.Format.NumChannels;

            var loopStartOffset = -1;
            if (null != reader.Sample && reader.Sample.Loops.Count > 0)
            {
                loopStartOffset = (int) reader.Sample.Loops[0].Start;

                if (loopStartOffset > 0)
                {
                    // convert from samples to bytes
                    loopStartOffset *= (reader.Format.SignificantBitsPerSample / 8) * numChannels;
                }
            }

            var format = new WaveFormat(rate, bits, numChannels);
            desc.Format = format;

            if (loopStartOffset == 0 || loopStartOffset == -1)
            {
                desc.BufferBytes = ((int) reader.Data.NumSamples) * (format.BitsPerSample / 8) * numChannels;
            }
            else
            {
                // Wave has a pre-loop - create a large buffer with room for several copies of the loop
                desc.BufferBytes = Math.Max(reader.Data.RawData.Length, 48000);
                // ensure its a multiple of the loop size
                desc.BufferBytes += desc.BufferBytes % (reader.Data.RawData.Length - loopStartOffset);
            }

            this.CreateBuffer(reader, desc);
        }

        #endregion

        #region Private Methods

        private void CreateBuffer(bwWaveFile reader, SoundBufferDescription desc)
        {
            this.m_buffer = new SecondarySoundBuffer(this.m_directSound, desc);
            var capabilities = this.m_buffer.Capabilities;

            DataStream dataPart2;
            var dataPart1 = this.m_buffer.Lock(0, capabilities.BufferBytes, LockFlags.EntireBuffer, out dataPart2);
            dataPart1.Write(reader.Data.RawData, 0, reader.Data.RawData.Length);

            this.m_buffer.Unlock(dataPart1, dataPart2);
        }

        #endregion
    }
}