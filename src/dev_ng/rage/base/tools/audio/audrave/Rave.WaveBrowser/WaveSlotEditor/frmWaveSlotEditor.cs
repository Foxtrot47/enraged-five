// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmWaveSlotEditor.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for frmWaveSlotEditor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using SharpDX;

namespace Rave.WaveBrowser.WaveSlotEditor
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Controls.Forms;
    using Rave.Controls.Forms.Popups;
    using Rave.Instance;
    using Rave.Types.Waves;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.Infrastructure.Nodes.WaveSlot;
    using Rave.WaveBrowser.WaveBrowser;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Wave slot editor.
    /// </summary>
    public class frmWaveSlotEditor : Form
    {
        #region Fields

        /// <summary>
        /// The components.
        /// </summary>
        private IContainer components;

        /// <summary>
        /// The m_ slot image list.
        /// </summary>
        private ImageList m_SlotImageList;

        /// <summary>
        /// The m_mnu add tag.
        /// </summary>
        private ToolStripMenuItem m_mnuAddTag;

        /// <summary>
        /// The m_mnu delete tag.
        /// </summary>
        private ToolStripMenuItem m_mnuDeleteTag;

        /// <summary>
        /// The m_mnu delete slot.
        /// </summary>
        private ToolStripMenuItem m_mnuDeleteSlot;

        /// <summary>
        /// The m_mnu new buffer slot.
        /// </summary>
        private ToolStripMenuItem m_mnuNewBufferSlot;

        /// <summary>
        /// The m_mnu new slot.
        /// </summary>
        private ToolStripMenuItem m_mnuNewSlot;

        /// <summary>
        /// The m_mnu new stream slot.
        /// </summary>
        private ToolStripMenuItem m_mnuNewStreamSlot;

        /// <summary>
        /// The m_mnu new wave slot.
        /// </summary>
        private ToolStripMenuItem m_mnuNewWaveSlot;

        /// <summary>
        /// The m_mnu remove association.
        /// </summary>
        private ToolStripMenuItem m_mnuRemoveAssociation;

        /// <summary>
        /// The m_read only.
        /// </summary>
        private bool m_readOnly;

        /// <summary>
        /// The m_slot tree view.
        /// </summary>
        private ScrollableTreeView m_slotTreeView;

        /// <summary>
        /// The m_wave slot changelist.
        /// </summary>
        private IChangeList m_waveSlotChangelist;

        /// <summary>
        /// The action log.
        /// </summary>
        private IActionLog actionLog;

        /// <summary>
        /// The m_wave tree view.
        /// </summary>
        private ctrlWaveEditorTreeView m_waveTreeView;

        /// <summary>
        /// The mnu right click.
        /// </summary>
        private ContextMenuStrip mnuRightClick;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="frmWaveSlotEditor"/> class.
        /// </summary>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        public frmWaveSlotEditor(IActionLog actionLog, ctrlWaveBrowser waveBrowser)
        {
            this.InitializeComponent();
            this.actionLog = actionLog;
            this.m_waveTreeView.Init(waveBrowser, actionLog);
            this.m_waveTreeView.LoadWaveLists(false);
            this.m_waveTreeView.ShouldFlattenWavesOnDrag = false;
            this.LoadWaveSlotSettings();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Add slot child nodes.
        /// </summary>
        /// <param name="waveSlot">
        /// The wave slot.
        /// </param>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        private static void AddSlotChildNodes(WaveSlotNode waveSlot, XmlNode parentNode)
        {
            foreach (XmlNode child in parentNode)
            {
                if (child.Name == "Loadable")
                {
                    var name = child.Attributes["name"].Value;
                    var type =
                        (LoadableNode.LoadableType)
                        Enum.Parse(typeof(LoadableNode.LoadableType), child.Attributes["type"].Value, true);
                    waveSlot.Nodes.Add(new LoadableNode(name, type));
                }
                else if (child.Name == "Tag")
                {
                    var key = child.Attributes["key"].Value;
                    var value = child.Attributes["value"].Value;
                    var platform = child.Attributes["platform"].Value;

                    var tagNode = new WaveSlotTagNode(key, value, platform);
                    waveSlot.Nodes.Add(tagNode);
                }
            }
        }

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWaveSlotEditor));
            this.m_waveTreeView = new ctrlWaveEditorTreeView();
            this.mnuRightClick = new System.Windows.Forms.ContextMenuStrip();
            this.m_mnuAddTag = new System.Windows.Forms.ToolStripMenuItem();
            this.m_mnuDeleteTag = new System.Windows.Forms.ToolStripMenuItem();
            this.m_mnuDeleteSlot = new System.Windows.Forms.ToolStripMenuItem();
            this.m_mnuNewSlot = new System.Windows.Forms.ToolStripMenuItem();
            this.m_mnuNewWaveSlot = new System.Windows.Forms.ToolStripMenuItem();
            this.m_mnuNewStreamSlot = new System.Windows.Forms.ToolStripMenuItem();
            this.m_mnuNewBufferSlot = new System.Windows.Forms.ToolStripMenuItem();
            this.m_mnuRemoveAssociation = new System.Windows.Forms.ToolStripMenuItem();
            this.m_slotTreeView = new ScrollableTreeView();
            this.m_SlotImageList = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();

            // m_WaveTreeView
            this.m_waveTreeView.AllowDrop = true;
            this.m_waveTreeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.m_waveTreeView.Location = new System.Drawing.Point(0, 0);
            this.m_waveTreeView.Name = "m_waveTreeView";
            this.m_waveTreeView.Size = new System.Drawing.Size(272, 478);
            this.m_waveTreeView.TabIndex = 0;

            // mnuRightClick
            this.mnuRightClick.Items.AddRange(
                new ToolStripItem[]
                    {
                        this.m_mnuDeleteSlot, this.m_mnuNewSlot, this.m_mnuNewWaveSlot, 
                        this.m_mnuNewStreamSlot, this.m_mnuNewBufferSlot, this.m_mnuRemoveAssociation,
                        this.m_mnuAddTag, this.m_mnuDeleteTag
                    });

            this.mnuRightClick.Opening += mnuRightClick_Opening;

            // mnuAddTag
            this.m_mnuAddTag.Text = "Add Tag";
            this.m_mnuAddTag.Click += this.mnuAddTag_Click;

            // mnuRemoveTag
            this.m_mnuDeleteTag.Text = "Delete Tag";
            this.m_mnuDeleteTag.Click += this.mnuDeleteTag_Click;

            // mnuDeleteSlot
            this.m_mnuDeleteSlot.Text = "Delete Slot";
            this.m_mnuDeleteSlot.Click += new System.EventHandler(this.mnuDeleteSlot_Click);

            // mnuNewSlot
            this.m_mnuNewSlot.Text = "New Bank Slot";
            this.m_mnuNewSlot.Click += new System.EventHandler(this.mnuNewSlot_Click);

            // mnuNewWaveSlot
            this.m_mnuNewWaveSlot.Text = "New Wave Slot";
            this.m_mnuNewWaveSlot.Click += new System.EventHandler(this.mnuNewWaveSlot_Click);

            // mnuNewStreamSlot
            this.m_mnuNewStreamSlot.Text = "New Stream Slot";
            this.m_mnuNewStreamSlot.Click += new System.EventHandler(this.mnuNewStreamSlot_Click);

            // mnuNewBufferSlot
            this.m_mnuNewBufferSlot.Text = "New Buffer Slot";
            this.m_mnuNewBufferSlot.Click += new System.EventHandler(this.mnuNewBufferSlot_Click);

            // mnuRemoveAssociation
            this.m_mnuRemoveAssociation.Text = "Remove Association";
            this.m_mnuRemoveAssociation.Click += new System.EventHandler(this.mnuRemoveAssociation_Click);

            // m_SlotTreeView
            this.m_slotTreeView.AllowDrop = true;
            this.m_slotTreeView.ContextMenuStrip = this.mnuRightClick;
            this.m_slotTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_slotTreeView.HideSelection = false;
            this.m_slotTreeView.ImageIndex = 0;
            this.m_slotTreeView.ImageList = this.m_SlotImageList;
            this.m_slotTreeView.ItemHeight = 16;
            this.m_slotTreeView.LastSelectedNode = null;
            this.m_slotTreeView.Location = new System.Drawing.Point(272, 0);
            this.m_slotTreeView.Name = "m_slotTreeView";
            this.m_slotTreeView.SelectedImageIndex = 0;
            this.m_slotTreeView.Size = new System.Drawing.Size(312, 478);
            this.m_slotTreeView.Sorted = true;
            this.m_slotTreeView.TabIndex = 1;
            this.m_slotTreeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.SlotTreeView_DragDrop);
            this.m_slotTreeView.DragOver += new System.Windows.Forms.DragEventHandler(this.SlotTreeView_DragOver);
            this.m_slotTreeView.DoubleClick += new System.EventHandler(this.SlotTreeView_DoubleClick);
            this.m_slotTreeView.DragEnter += new System.Windows.Forms.DragEventHandler(this.SlotTreeView_DragEnter);

            // m_SlotImageList
            this.m_SlotImageList.ImageStream =
                (System.Windows.Forms.ImageListStreamer)(resources.GetObject("m_SlotImageList.ImageStream"));
            this.m_SlotImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.m_SlotImageList.Images.SetKeyName(0, string.Empty);
            this.m_SlotImageList.Images.SetKeyName(1, string.Empty);
            this.m_SlotImageList.Images.SetKeyName(2, string.Empty);
            this.m_SlotImageList.Images.SetKeyName(3, string.Empty);
            this.m_SlotImageList.Images.SetKeyName(4, "greyFolder.ico");
            this.m_SlotImageList.Images.SetKeyName(5, string.Empty);
            this.m_SlotImageList.Images.SetKeyName(6, string.Empty);
            this.m_SlotImageList.Images.SetKeyName(7, string.Empty);

            // frmWaveSlotEditor
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(584, 478);
            this.Controls.Add(this.m_slotTreeView);
            this.Controls.Add(this.m_waveTreeView);
            this.Name = "frmWaveSlotEditor";
            this.Text = "Wave Slot Editor";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmWaveSlotEditor_Closing);
            this.ResumeLayout(false);
        }

        /// <summary>
        /// The load wave slot settings.
        /// </summary>
        private void LoadWaveSlotSettings()
        {
            var doc = new XmlDocument();
            var waveSlotFile = Configuration.WaveSlotFile;

            if (!RaveInstance.AssetManager.ExistsAsAsset(waveSlotFile))
            {
                if (DialogResult.Yes
                    == ErrorManager.HandleQuestion(
                        waveSlotFile + " does not exist in the asset database.  Create it?", 
                        MessageBoxButtons.YesNo, 
                        DialogResult.Yes))
                {
                    // Create the root element
                    var rootNode = doc.CreateElement("WaveSlots");
                    doc.AppendChild(rootNode);
                    doc.Save(waveSlotFile);
                    if (this.m_waveSlotChangelist == null)
                    {
                        this.m_waveSlotChangelist =
                            RaveInstance.AssetManager.CreateChangeList("Initial import of WaveSlots");
                    }

                    this.m_waveSlotChangelist.MarkAssetForAdd(waveSlotFile, "text");
                }
            }
            else
            {
                try
                {
                    if (this.m_waveSlotChangelist == null)
                    {
                        this.m_waveSlotChangelist = RaveInstance.AssetManager.CreateChangeList("WaveSlots Edit");
                    }

                    this.m_waveSlotChangelist.CheckoutAsset(waveSlotFile, true);
                    this.m_readOnly = false;
                }
                catch (Exception)
                {
                    this.m_readOnly = true;
                    ErrorManager.HandleInfo(
                        "Couldn't check out wave slot settings - you won't be able to edit wave slots");
                }

                doc.Load(waveSlotFile);
            }

            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                if (node.Name == "Slot")
                {
                    var name = node.Attributes["name"].Value;
                    var type =
                        (WaveSlotNode.SlotLoadType)
                        Enum.Parse(typeof(WaveSlotNode.SlotLoadType), node.Attributes["loadType"].Value, true);
                    var n = new WaveSlotNode(name, type);
                    if (type == WaveSlotNode.SlotLoadType.BUFFER)
                    {
                        n.SetSize(int.Parse(node.Attributes["size"].Value));
                    }

                    this.m_slotTreeView.Nodes.Add(n);
                    AddSlotChildNodes(n, node);
                }
            }
        }

        /// <summary>
        /// The save wave slot settings.
        /// </summary>
        private void SaveWaveSlotSettings()
        {
            var doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("WaveSlots"));
            foreach (WaveSlotNode w in this.m_slotTreeView.Nodes)
            {
                w.Serialise(doc.DocumentElement);
            }

            doc.Save(Configuration.WaveSlotFile);
        }

        /// <summary>
        /// The slot tree view_ double click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SlotTreeView_DoubleClick(object sender, EventArgs e)
        {
            var wsn = this.m_slotTreeView.LastSelectedNode as WaveSlotNode;
            var tagNode = this.m_slotTreeView.LastSelectedNode as WaveSlotTagNode;

            if (wsn != null)
            {
                if (wsn.Type == WaveSlotNode.SlotLoadType.BUFFER)
                {
                    frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this, "Buffer size (bytes)");
                    if (input.HasBeenCancelled) return;
                    try
                    {
                        wsn.SetSize(int.Parse(input.data));
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            else if (null != tagNode)
            {
                var tagEditor = new frmWaveSlotTagEditor(tagNode, tagNode.Parent);
                tagEditor.ShowDialog(this);
                tagEditor.Dispose();
            }
        }

        /// <summary>
        /// The slot tree view_ drag drop.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SlotTreeView_DragDrop(object sender, DragEventArgs e)
        {
            var sn = this.m_slotTreeView.LastSelectedNode as WaveSlotNode;
            if (sn != null && sn.Type != WaveSlotNode.SlotLoadType.BUFFER && !this.m_readOnly
                && e.Data.GetDataPresent(typeof(EditorTreeNode[])))
            {
                var loadableNodes = new ArrayList();
                var droppedNodes = (EditorTreeNode[])e.Data.GetData(typeof(EditorTreeNode[]));
                foreach (var etn in droppedNodes)
                {
                    var pack = etn as PackNode;
                    var bankFolderNode = etn as BankFolderNode;
                    var bank = etn as BankNode;
                    if (bank != null)
                    {
                        var name = EditorTreeNode.FindParentPackNode(bank).GetObjectName() + "\\" + bank.GetObjectName();
                        var node = new LoadableNode(name, LoadableNode.LoadableType.BANK);
                        loadableNodes.Add(node);
                        if (WaveManager.WaveSlotRefs.ContainsKey(name))
                        {
                            WaveManager.WaveSlotRefs[name].Add(sn.Name);
                        }
                        else
                        {
                            WaveManager.WaveSlotRefs.Add(name, new List<string> { sn.Name });
                        }
                    }
                    else if (bankFolderNode != null)
                    {
                        var name = bankFolderNode.GetObjectPath();
                        var node = new LoadableNode(name, LoadableNode.LoadableType.BANKFOLDER);
                        loadableNodes.Add(node);
                        if (WaveManager.WaveSlotRefs.ContainsKey(name))
                        {
                            WaveManager.WaveSlotRefs[name].Add(sn.Name);
                        }
                        else
                        {
                            WaveManager.WaveSlotRefs.Add(name, new List<string> { sn.Name });
                        }
                    }
                    else if (pack != null)
                    {
                        var name = pack.GetObjectName();
                        var node = new LoadableNode(name, LoadableNode.LoadableType.PACK);
                        loadableNodes.Add(node);
                        if (WaveManager.WaveSlotRefs.ContainsKey(name))
                        {
                            WaveManager.WaveSlotRefs[name].Add(sn.Name);
                        }
                        else
                        {
                            WaveManager.WaveSlotRefs.Add(name, new List<string> { sn.Name });
                        }
                    }
                }

                if (loadableNodes.Count > 0)
                {
                    try
                    {
                        foreach (LoadableNode node in loadableNodes)
                        {
                            this.m_slotTreeView.LastSelectedNode.Nodes.Add(node);
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// The slot tree view_ drag enter.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SlotTreeView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        /// <summary>
        /// The slot tree view_ drag over.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SlotTreeView_DragOver(object sender, DragEventArgs e)
        {
            var p = this.m_slotTreeView.PointToClient(new Point(e.X, e.Y));
            this.m_slotTreeView.LastSelectedNode = this.m_slotTreeView.GetNodeAt(p);

            if (!this.m_readOnly && this.m_slotTreeView.LastSelectedNode != null)
            {
                e.Effect = e.AllowedEffect;
            }
        }

        /// <summary>
        /// The frm wave slot editor_ closing.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void frmWaveSlotEditor_Closing(object sender, CancelEventArgs e)
        {
            if (!this.m_readOnly)
            {
                this.SaveWaveSlotSettings();
                try
                {
                    this.m_waveSlotChangelist.RevertUnchanged();
                    if (this.m_waveSlotChangelist.Assets.Count > 0)
                    {
                        this.m_waveSlotChangelist.Submit("[RAVE] WaveSlotEditor");
                    }
                    else
                    {
                        this.m_waveSlotChangelist.Delete();
                    }

                    this.m_waveSlotChangelist = null;
                }
                catch (Exception exp)
                {
                    ErrorManager.HandleError(exp);
                }
            }
        }

        private void mnuRightClick_Opening(object sender, CancelEventArgs e)
        {
            this.m_mnuAddTag.Visible = false;
            this.m_mnuDeleteTag.Visible = false;
            this.m_mnuDeleteSlot.Visible = false;
            this.m_mnuNewSlot.Visible = false;
            this.m_mnuNewWaveSlot.Visible = false;
            this.m_mnuNewStreamSlot.Visible = false;
            this.m_mnuNewBufferSlot.Visible = false;
            this.m_mnuRemoveAssociation.Visible = false;

            var node = this.m_slotTreeView.LastSelectedNode;
            if (null == node)
            {
                e.Cancel = true;
                return;
            }

            var waveSlotNode = node as WaveSlotNode;
            var loadableNode = node as LoadableNode;
            var tagNode = node as WaveSlotTagNode;

            if (null != waveSlotNode)
            {
                this.m_mnuAddTag.Visible = true;
                this.m_mnuDeleteSlot.Visible = true;
                this.m_mnuNewSlot.Visible = true;
                this.m_mnuNewWaveSlot.Visible = true;
                this.m_mnuNewStreamSlot.Visible = true;
                this.m_mnuNewBufferSlot.Visible = true;
            }
            else if (null != loadableNode)
            {
                this.m_mnuRemoveAssociation.Visible = true;
            }
            else if (null != tagNode)
            {
                this.m_mnuDeleteTag.Visible = true;
            }
        }

        private void mnuAddTag_Click(object sender, EventArgs e)
        {
            var node = this.m_slotTreeView.LastSelectedNode;
            if (node == null)
            {
                return;
            }

            if (node.GetType() != typeof(WaveSlotNode))
            {
                ErrorManager.HandleInfo("Tags can only be added to wave slot nodes");
                return;
            }

            var tagEditor = new frmWaveSlotTagEditor(null, node);
            tagEditor.ShowDialog(this);
            tagEditor.Dispose();
        }

        private void mnuDeleteTag_Click(object sender, EventArgs e)
        {
            var tagNode = this.m_slotTreeView.LastSelectedNode as WaveSlotTagNode;
            if (null != tagNode)
            {
                tagNode.Remove();
            }
        }

        /// <summary>
        /// The mnu delete slot_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuDeleteSlot_Click(object sender, EventArgs e)
        {
            if (this.m_slotTreeView.LastSelectedNode != null
                && this.m_slotTreeView.LastSelectedNode.GetType() == typeof(WaveSlotNode))
            {
                this.m_slotTreeView.LastSelectedNode.Remove();
            }
        }

        /// <summary>
        /// The mnu new buffer slot_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewBufferSlot_Click(object sender, EventArgs e)
        {
            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this, "Slot Name");
            if (input.HasBeenCancelled) return;
            string name = input.data;
            input = frmSimpleInput.GetInput(this, "Slot Size (bytes)");
            if (input.HasBeenCancelled) return;
            string size = input.data;

            var slot = new WaveSlotNode(name, WaveSlotNode.SlotLoadType.BUFFER);
            try
            {
                slot.SetSize(int.Parse(size));
            }
            catch (Exception)
            {
            }

            this.m_slotTreeView.Nodes.Add(slot);
        }

        /// <summary>
        /// The mnu new slot_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewSlot_Click(object sender, EventArgs e)
        {
            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this, "Slot Name");
            if (input.HasBeenCancelled) return;
            var slot = new WaveSlotNode(input.data, WaveSlotNode.SlotLoadType.BANK);
            this.m_slotTreeView.Nodes.Add(slot);
        }

        /// <summary>
        /// The mnu new stream slot_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewStreamSlot_Click(object sender, EventArgs e)
        {
            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this, "Slot Name");
            if (input.HasBeenCancelled) return;
            var slot = new WaveSlotNode(input.data, WaveSlotNode.SlotLoadType.STREAM);
            this.m_slotTreeView.Nodes.Add(slot);
        }

        /// <summary>
        /// The mnu new wave slot_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewWaveSlot_Click(object sender, EventArgs e)
        {
            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this, "Slot Name");
            if (input.HasBeenCancelled) return;
            var slot = new WaveSlotNode(input.data, WaveSlotNode.SlotLoadType.WAVE);
            this.m_slotTreeView.Nodes.Add(slot);
        }

        /// <summary>
        /// The mnu remove association_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuRemoveAssociation_Click(object sender, EventArgs e)
        {
            if (this.m_slotTreeView.SelectedItems != null)
            {
                foreach (TreeNode tn in this.m_slotTreeView.SelectedItems)
                {
                    var ln = tn as LoadableNode;
                    if (ln != null)
                    {
                        var sn = ln.Parent as WaveSlotNode;
                        if (sn != null)
                        {
                            WaveManager.WaveSlotRefs[ln.Name].Remove(sn.Name);
                        }

                        ln.Remove();
                    }
                }
            }
        }

        #endregion
    }
}