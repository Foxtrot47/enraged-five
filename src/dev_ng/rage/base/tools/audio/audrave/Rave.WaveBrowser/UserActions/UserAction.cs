﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;
using Rave.Utils;
using Rave.WaveBrowser.Infrastructure.Interfaces;

namespace Rave.WaveBrowser.UserActions
{
    public abstract class UserAction: IUserAction
    {
        private bool isCommited = false;
        private bool isExecuted = false;

        public bool Commit()
        {
            if (isCommited)
            {
                ErrorManager.HandleInfo("attempt to commit a commited action!");
                return false;
            }
            if (!isExecuted)
            {
                ErrorManager.HandleInfo("attempt to commit an action that has not yet been executed");
                return false;
            }
            isCommited = doCommit();
            return isCommited;
        }

        public bool Apply(IPendingWaveList waveList, string platform)
        {
            doApply(waveList, platform);
            return true;
        }


        public bool Undo()
        {
            if (isCommited)
            {
                ErrorManager.HandleInfo("attempt to undo a commited action!");
                return false;
            }
            if (!isExecuted)
            {
                ErrorManager.HandleInfo("attempt to undo an action that has not yet been executed");
                return false;
            }
            isExecuted = !doUndo();
            return !isExecuted;
        }


        public bool Action()
        {
            if (isCommited)
            {
                ErrorManager.HandleInfo("attempt to execute a commited action!");
                return false;
            }
            if (isExecuted)
            {
                ErrorManager.HandleInfo("attempt to execute an action that has already been executed");
                return false;
            }
            isExecuted = doAction();
            return isExecuted;
        }

        protected abstract bool doCommit();
        protected abstract bool doAction();
        abstract protected bool doUndo();
        protected abstract bool doApply(IPendingWaveList temporaryWaveList, string platform);
        public abstract string GetSummary();

        public abstract string GetAssetPath();

        public Infrastructure.Nodes.EditorTreeNode Node { get; protected set; }

        public IActionParams ActionParameters { get; protected set; }
    }
}
