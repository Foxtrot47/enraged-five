namespace Rave.WaveBrowser.UserActions
{
    using System.Collections.Generic;

    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Summary description for ModifyTagAction.
    /// </summary>
    public class ModifyTagAction : UserAction
    {
        private readonly Dictionary<string, string> m_OriginalValues;
        private readonly TagNode m_TagNode;
        private readonly string m_objectPath;

        public ModifyTagAction(IActionParams parameters)
        {
            this.ActionParameters = parameters;
            this.m_objectPath = this.ActionParameters.Node.GetObjectPath();
            this.m_TagNode = this.ActionParameters.Node as TagNode;
            this.m_OriginalValues = new Dictionary<string, string>(this.m_TagNode.Values);
        }

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        public EditorTreeNode Node
        {
            get
            {
                return this.m_TagNode;
            }
        }

        public override string GetAssetPath()
        {
            return this.m_objectPath;
        }

        protected override bool doAction()
        {
            var parent = (EditorTreeNode) this.ActionParameters.Node.Parent;
            // we cant lock waves
            if (parent.GetType() ==
                typeof (WaveNode))
            {
                parent = (EditorTreeNode) parent.Parent;
            }
            if (parent.TryToLock())
            {
                foreach (var platform in this.ActionParameters.Platforms)
                {
                    this.ActionParameters.Node.UpdateDisplayState(EditorTreeNode.NodeDisplayState.ChangedLocally,
                                                         platform,
                                                         true,
                                                         true);
                    if (this.m_TagNode != null)
                    {
                        this.m_TagNode.SetValue(platform, this.ActionParameters.Value[platform]);
                    }
                }
                return true;
            }
            return false;
        }

        protected override bool doUndo()
        {
            foreach (var platform in this.ActionParameters.Platforms)
            {
                if (this.m_TagNode != null)
                {
                    this.m_TagNode.SetValue(platform, this.m_OriginalValues[platform]);
                }
            }

            return true;
        }

        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            temporaryWaveList.RecordModifyTag(
                    this.m_objectPath, this.ActionParameters.Value[platform]);
            return true;
        }

        protected override bool doCommit()
        {
            foreach (var platform in this.ActionParameters.Platforms)
            {
                // add this to the pending wave list
                this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform).RecordModifyTag(
                    this.m_objectPath, this.ActionParameters.Value[platform]);
                this.ActionParameters.Node.UpdateDisplayState(EditorTreeNode.NodeDisplayState.ChangedInPendingWaveList,
                                                     platform,
                                                     true,
                                                     true);
            }
            return true;
        }

        public override string GetSummary()
        {
            return "modify tag (" + this.ActionParameters.Platforms + ") " + this.m_objectPath + " new value: \"" + this.ActionParameters.Value +
                   "\"";
        }
    }
}