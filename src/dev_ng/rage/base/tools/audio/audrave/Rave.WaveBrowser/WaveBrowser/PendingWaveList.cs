using Rave.WaveBrowser.Infrastructure.Nodes;

namespace Rave.WaveBrowser.WaveBrowser
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml.Linq;

    using Rave.Instance;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;

    public class PendingWaveList : IPendingWaveList
    {
        private readonly string m_builtWavesPackListFile;
        private readonly string m_pendingWavesListDirectory;
        private readonly string m_builtWavesListDirectory;
        private readonly IWaveBrowser m_waveBrowser;

        private Dictionary<string, PackMetadata> loadedPackMetadata = new Dictionary<string, PackMetadata>();

        public PendingWaveList(string buildInfoAssetPath, IWaveBrowser waveBrowser)
        {
            this.m_waveBrowser = waveBrowser;

            var buildInfoWorkingPath = RaveInstance.AssetManager.GetLocalPath(buildInfoAssetPath);
            buildInfoWorkingPath = buildInfoWorkingPath.Replace(Path.AltDirectorySeparatorChar,
                                                                Path.DirectorySeparatorChar);
            this.m_pendingWavesListDirectory = Path.Combine(buildInfoWorkingPath, "PendingWaves\\");
            this.m_builtWavesListDirectory = Path.Combine(buildInfoWorkingPath, "BuiltWaves\\");
            this.m_builtWavesPackListFile = Path.Combine(m_builtWavesListDirectory, "BuiltWaves_PACK_LIST.xml");
        }

        private string getPackNameFromPath(string path) 
        { 
            int index = path.IndexOf("\\");
            if(index!=-1) return path.Substring(0, index);
            else return path;
        }

        #region IPendingWaveList Members

        public List<string> getPackNamesFromBuiltWavesPackList()
        {
            List<string> result = new List<string>();
            foreach(XElement packFile in XDocument.Load(m_builtWavesPackListFile).Descendants("PackFile"))
            {
                result.Add(packFile.Value.Remove(packFile.Value.Length - "_PACK_FILE.xml".Length));
            }
            return result;
        }

        public void RecordAddPack(string packPath)
        {
            
            //this.m_localStore.RecordAddedPack(packPath, getAllMetadataStores());
        }

        public void RecordAddBankFolder(string bankFolderPath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(bankFolderPath)];
            metaData.localMetadataStore.RecordAddedBankFolder(bankFolderPath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordAddBank(string bankPath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(bankPath)];
            metaData.localMetadataStore.RecordAddedBank(bankPath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordAddWaveFolder(string waveFolderPath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(waveFolderPath)];
            metaData.localMetadataStore.RecordAddedWaveFolder(waveFolderPath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordAddWave(string wavePath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(wavePath)];
            metaData.localMetadataStore.RecordAddedWave(wavePath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordAddPreset(string presetPath, string tagValue)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(presetPath)];
            metaData.localMetadataStore.RecordAddedPreset(presetPath, tagValue,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordAddTag(string tagPath, string tagValue)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(tagPath)];
            metaData.localMetadataStore.RecordAddedTag(tagPath, tagValue, 
                new MetadataStore[]{metaData.pendingMetadataStore, metaData.buildMetadataStore});
        }

        public void RecordDeletePack(string packPath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(packPath)];
            metaData.localMetadataStore.RecordRemovedPack(packPath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordDeleteBankFolder(string bankFolderPath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(bankFolderPath)];
            metaData.localMetadataStore.RecordRemovedBankFolder(bankFolderPath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordDeleteBank(string bankPath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(bankPath)];
            metaData.localMetadataStore.RecordRemovedBank(bankPath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordDeleteWaveFolder(string waveFolderPath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(waveFolderPath)];
            metaData.localMetadataStore.RecordRemovedWaveFolder(waveFolderPath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordDeleteWave(string wavePath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(wavePath)];
            metaData.localMetadataStore.RecordRemovedWave(wavePath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordDeletePreset(string tagPath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(tagPath)];
            metaData.localMetadataStore.RecordRemovedPreset(tagPath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordDeleteTag(string tagPath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(tagPath)];
            metaData.localMetadataStore.RecordRemovedTag(tagPath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordModifyWave(string wavePath)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(wavePath)];
            metaData.localMetadataStore.RecordModifiedWave(wavePath,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void RecordModifyTag(string tagPath, string tagValue)
        {
            PackMetadata metaData = loadedPackMetadata[getPackNameFromPath(tagPath)];
            metaData.localMetadataStore.RecordModifiedTag(tagPath, tagValue,
                new MetadataStore[] { metaData.pendingMetadataStore, metaData.buildMetadataStore });
        }

        public void LoadPendingWaveList()
        {
            try
            {
                foreach(PendingWavesMetadataStore store in getPendingMetadataStores()) 
                {
                    store.ReloadXml();
                }
            }
            catch (DirectoryNotFoundException)
            {
                if (DialogResult.Yes ==
                    ErrorManager.HandleQuestion(this.m_pendingWavesListDirectory + " does not exist.  Create it?",
                                                MessageBoxButtons.YesNo,
                                                DialogResult.Yes))
                {
                    Directory.CreateDirectory(this.m_pendingWavesListDirectory);
                    var placeholderPath = Path.Combine(this.m_pendingWavesListDirectory, "placeholder.txt");
                    var stream = File.Open(placeholderPath, FileMode.Create, FileAccess.Write);
                    using (var streamWriter = new StreamWriter(stream, Encoding.ASCII))
                    {
                        streamWriter.Write(
                            "The sole purpose of this file is to ensure that the PendingWaves directory exists in Perforce.");
                    }
                    var changelist =
                        RaveInstance.AssetManager.CreateChangeList("Creation of PendingWaves directory");
                    changelist.MarkAssetForAdd(placeholderPath);
                    changelist.Submit();
                    this.LoadPendingWaveList();
                }
            }
        }

        public void LoadBuiltWaveList()
        {
            try
            {
                foreach(BuiltWavesMetadataStore store in getBuiltMetadataStores()) 
                {
                    store.ReloadXml();
                }
            }
            catch (FileNotFoundException)
            {
                this.TryCreateBuiltWavesPackList();
                this.LoadBuiltWaveList();
            }
            catch (DirectoryNotFoundException)
            {
                this.TryCreateBuiltWavesPackList();
                this.LoadBuiltWaveList();
            }
        }

        public bool IsPendingWaveListCheckedOut()
        {
            var pendingWaveFiles = new List<string>();
            var assetManager = RaveInstance.AssetManager;
            var currentChangeLists = assetManager.ChangeLists;
            foreach (var currentChangeList in currentChangeLists.Values)
            {
                foreach (var currentChangeListAsset in currentChangeList.Assets)
                {
                    var localPath = currentChangeListAsset.LocalPath;
                    var localDirectory = Path.GetDirectoryName(localPath).Replace("/", "\\") + "\\";
                    if (string.Compare(localDirectory, this.m_pendingWavesListDirectory, true) == 0)
                    {
                        pendingWaveFiles.Add(localPath);
                    }
                }
            }

            return pendingWaveFiles.Count > 0;
        }

        public bool LockPendingWaveList()
        {
            if (RaveInstance.RaveAssetManager.WaveChangeList == null) 
            {
                RaveInstance.RaveAssetManager.WaveChangeList = RaveInstance.AssetManager.CreateChangeList("Wave Changelist");
            }

            try {

                var waveChangelist = RaveInstance.RaveAssetManager.WaveChangeList;
                var assetManager = RaveInstance.AssetManager;

                foreach(KeyValuePair<string, PackMetadata> kvp in loadedPackMetadata) {
                    if(kvp.Value.localMetadataStore.Document.Root.Element("Pack")==null) continue;
                    String pendingWavesFile = kvp.Value.pendingMetadataStore.getLoadedXmlPath();
                    if(assetManager.ExistsAsAsset(pendingWavesFile) &&
                        !assetManager.IsCheckedOut(pendingWavesFile)) {
                        waveChangelist.CheckoutAsset(pendingWavesFile, true);
                    }
                    else {
                        var localPath = assetManager.GetLocalPath(pendingWavesFile);
                        if(File.Exists(localPath)) {
                            new FileInfo(localPath) { IsReadOnly = false };
                        }
                        var tempDoc = new XDocument(new XElement("PendingWaves"));
                        tempDoc.Save(localPath);
                        waveChangelist.MarkAssetForAdd(pendingWavesFile);
                    }
                }
                this.LoadPendingWaveList();
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
                return false;
            }
            return true;
        }

        public void LoadPack(string packName, IWaveBrowser browser) 
        {
            if(loadedPackMetadata.ContainsKey(packName)) return;
            PackMetadata metadata = new PackMetadata();
            metadata.localMetadataStore = new MetadataStore(browser);
            metadata.buildMetadataStore = new BuiltWavesMetadataStore(browser, m_builtWavesListDirectory+packName+"_PACK_FILE.xml");
            metadata.pendingMetadataStore = new PendingWavesMetadataStore(browser, m_pendingWavesListDirectory+packName+".XML");
            loadedPackMetadata.Add(packName, metadata);
        }

       
	    public PackMetadata GetPackMetaData(string packName)
	    {
		    if (loadedPackMetadata.ContainsKey(packName))
		    {
			    return loadedPackMetadata[packName];
		    }
		    else return null;
	    }

        public bool UndoLockPendingWaveList()
        {
            try
            {
                var waveChangelist = RaveInstance.RaveAssetManager.WaveChangeList;
                var assetManager = RaveInstance.AssetManager;

                foreach(KeyValuePair<string, PackMetadata> kvp in loadedPackMetadata) 
                {
                    if(kvp.Value.localMetadataStore.Document.Root.Element("Pack")==null) continue;
                    String pendingWavesFile = kvp.Value.pendingMetadataStore.getLoadedXmlPath();
                    var asset = waveChangelist.GetAsset(pendingWavesFile);
                    if(asset != null) 
                    {
                        asset.Revert();
                    }
                }
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
                return false;
            }
            return true;
        }

        public bool SerialisePendingWaveList()
        {
            this.LoadBuiltWaveList();

            foreach(KeyValuePair<string, PackMetadata> kvp in loadedPackMetadata) 
            {
                kvp.Value.pendingMetadataStore.Combine(kvp.Value.localMetadataStore, kvp.Value.buildMetadataStore);
                if(!kvp.Value.pendingMetadataStore.SaveXml()) 
                {
                    return false;
                }
            }
            return true;
        }
        
        public void IntegrateLocalChanges()
        {
            this.LoadBuiltWaveList();

            foreach (KeyValuePair<string, PackMetadata> kvp in loadedPackMetadata)
            {
                kvp.Value.pendingMetadataStore.Combine(kvp.Value.localMetadataStore, kvp.Value.buildMetadataStore);
           }
        }


        public void ClearLocalStore()
        {
            foreach(KeyValuePair<string, PackMetadata> kvp in loadedPackMetadata) 
            {
                kvp.Value.localMetadataStore = new MetadataStore(this.m_waveBrowser);
            }
        }

        // PURPOSE: updates the local pending wave list from the server
        public bool GetLatestPendingWaveListFromAssetManager()
        {
            try
            {
                RaveInstance.AssetManager.GetLatestForceErrors(this.m_pendingWavesListDirectory);
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
                return false;
            }

            return true;
        }

        // PURPOSE: updates the local built wave list from the server
        public bool GetLatestBuiltWaveListFromAssetManager()
        {
            var directory = string.Concat(Path.GetDirectoryName(this.m_builtWavesPackListFile), "\\");
            try
            {
                RaveInstance.AssetManager.GetLatestForceErrors(directory);
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
                return false;
            }
            return true;
        }

        public void ShowPendingChanges(TreeView treeView, IActionLog log, string platform)
        {
            foreach(MetadataStore store in getPendingMetadataStores()) 
            {
                store.ApplyToVisualTree(treeView, log, platform);
            }
        }

        public void ShowBuiltWaves(TreeView treeView, IActionLog log, string platform)
        {
            foreach(string packName in getPackNamesFromBuiltWavesPackList()) 
            {
                if(loadedPackMetadata.ContainsKey(packName)) 
                {
                    loadedPackMetadata[packName].buildMetadataStore.CreateVisualTree(treeView, log, platform);
                }
                else 
                {
                    bool found = false;
                    Rave.WaveBrowser.Infrastructure.Nodes.PackNode correspondingNode = null;
                    foreach(Rave.WaveBrowser.Infrastructure.Nodes.EditorTreeNode node in treeView.Nodes) 
                    {
                        if(packName==node.GetObjectName()) 
                        {
                            found = true;
                            correspondingNode = (Rave.WaveBrowser.Infrastructure.Nodes.PackNode)node;
                        }
                    }
                    if(!found) 
                    {
                        correspondingNode = new Rave.WaveBrowser.Infrastructure.Nodes.PackNode(log, packName, this.m_waveBrowser, packName);
                        treeView.Nodes.Add(correspondingNode);
                    }
                    correspondingNode.AddPlatform(platform, Rave.WaveBrowser.Infrastructure.Nodes.EditorTreeNode.NodeDisplayState.AlreadyBuilt);
                }
            }
        }

        public void ShowBuiltWavesOfPack(string packName, TreeView treeView, IActionLog log, string platform)
        {
            if(loadedPackMetadata.ContainsKey(packName))
            {
                loadedPackMetadata[packName].buildMetadataStore.CreateVisualTree(treeView, log, platform);
            }
            else
            {
                throw new Exception("ShowBuiltWavesOfPack has been called on the unloaded pack "+packName);
            }
        }

        #endregion

        private MetadataStore[] getLocalMetadataStores() 
        {
            return getLoadedMetadataStores(false, false, true);
        }

        private PendingWavesMetadataStore[] getPendingMetadataStores() 
        {
            return getLoadedMetadataStores(true, false, false).Cast<PendingWavesMetadataStore>().ToArray();
        }

        private MetadataStore[] getBuiltMetadataStores() 
        {
            return getLoadedMetadataStores(false, true, false).Cast<BuiltWavesMetadataStore>().ToArray();
        }

        private MetadataStore[] getLoadedMetadataStores(bool getPendingStores, bool getBuiltStores, bool getLocalStores) 
        {
            List<MetadataStore> allMetadataStores = new List<MetadataStore>();
            foreach(KeyValuePair<string, PackMetadata> kvp in this.loadedPackMetadata) 
            {
                if(getBuiltStores) allMetadataStores.Add(kvp.Value.buildMetadataStore);
                if(getPendingStores) allMetadataStores.Add(kvp.Value.pendingMetadataStore);
                if(getLocalStores) allMetadataStores.Add(kvp.Value.localMetadataStore);
            }
            return allMetadataStores.ToArray();
        }

        private void TryCreateBuiltWavesPackList()
        {
            if (DialogResult.Yes ==
                ErrorManager.HandleQuestion(this.m_builtWavesPackListFile + " does not exist. Create it?",
                                            MessageBoxButtons.YesNo,
                                            DialogResult.Yes))
            {
                var directoryName = Path.GetDirectoryName(this.m_builtWavesPackListFile);
                if (!Directory.Exists(directoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }
                var doc = new XDocument(new XElement("BuiltWaves"));
                doc.Save(this.m_builtWavesPackListFile);
                var changelist =
                    RaveInstance.AssetManager.CreateChangeList("Creation of BuiltWaves pack list file.");
                changelist.MarkAssetForAdd(this.m_builtWavesPackListFile);
                changelist.Submit();
            }
        }

    }
}