namespace Rave.WaveBrowser.WaveBrowser.Search
{
    partial class frmWaveSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWaveSearch));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.searchOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.searchWaveNamesBtn = new System.Windows.Forms.RadioButton();
            this.searchWaveFoldersBtn = new System.Windows.Forms.RadioButton();
            this.searchWaveHashNamesBtn = new System.Windows.Forms.RadioButton();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStringInput = new System.Windows.Forms.TextBox();
            this.gbResults = new System.Windows.Forms.GroupBox();
            this.m_Results = new System.Windows.Forms.ListBox();
            this.mnuRightClick = new System.Windows.Forms.ContextMenu();
            this.mnuPlugins = new System.Windows.Forms.MenuItem();
            this.groupBox1.SuspendLayout();
            this.searchOptionsGroupBox.SuspendLayout();
            this.gbResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.searchOptionsGroupBox);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtStringInput);
            this.groupBox1.Location = new System.Drawing.Point(5, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(471, 155);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Input";
            // 
            // searchOptionsGroupBox
            // 
            this.searchOptionsGroupBox.Controls.Add(this.searchWaveNamesBtn);
            this.searchOptionsGroupBox.Controls.Add(this.searchWaveFoldersBtn);
            this.searchOptionsGroupBox.Controls.Add(this.searchWaveHashNamesBtn);
            this.searchOptionsGroupBox.Location = new System.Drawing.Point(23, 45);
            this.searchOptionsGroupBox.Name = "searchOptionsGroupBox";
            this.searchOptionsGroupBox.Size = new System.Drawing.Size(320, 94);
            this.searchOptionsGroupBox.TabIndex = 13;
            this.searchOptionsGroupBox.TabStop = false;
            this.searchOptionsGroupBox.Text = "Search Options";
            // 
            // searchWaveNamesBtn
            // 
            this.searchWaveNamesBtn.AutoSize = true;
            this.searchWaveNamesBtn.Checked = true;
            this.searchWaveNamesBtn.Location = new System.Drawing.Point(6, 19);
            this.searchWaveNamesBtn.Name = "searchWaveNamesBtn";
            this.searchWaveNamesBtn.Size = new System.Drawing.Size(127, 17);
            this.searchWaveNamesBtn.TabIndex = 10;
            this.searchWaveNamesBtn.TabStop = true;
            this.searchWaveNamesBtn.Text = "Search Wave Names";
            this.searchWaveNamesBtn.UseVisualStyleBackColor = true;
            // 
            // searchWaveFoldersBtn
            // 
            this.searchWaveFoldersBtn.AutoSize = true;
            this.searchWaveFoldersBtn.Location = new System.Drawing.Point(6, 67);
            this.searchWaveFoldersBtn.Name = "searchWaveFoldersBtn";
            this.searchWaveFoldersBtn.Size = new System.Drawing.Size(159, 17);
            this.searchWaveFoldersBtn.TabIndex = 12;
            this.searchWaveFoldersBtn.Text = "Search Wave Folder Names";
            this.searchWaveFoldersBtn.UseVisualStyleBackColor = true;
            // 
            // searchWaveHashNamesBtn
            // 
            this.searchWaveHashNamesBtn.AutoSize = true;
            this.searchWaveHashNamesBtn.Location = new System.Drawing.Point(6, 43);
            this.searchWaveHashNamesBtn.Name = "searchWaveHashNamesBtn";
            this.searchWaveHashNamesBtn.Size = new System.Drawing.Size(155, 17);
            this.searchWaveHashNamesBtn.TabIndex = 11;
            this.searchWaveHashNamesBtn.Text = "Search Wave Hash Names";
            this.searchWaveHashNamesBtn.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(379, 116);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Search String:";
            // 
            // txtStringInput
            // 
            this.txtStringInput.Location = new System.Drawing.Point(100, 19);
            this.txtStringInput.Name = "txtStringInput";
            this.txtStringInput.Size = new System.Drawing.Size(243, 20);
            this.txtStringInput.TabIndex = 0;
            // 
            // gbResults
            // 
            this.gbResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbResults.Controls.Add(this.m_Results);
            this.gbResults.Location = new System.Drawing.Point(5, 163);
            this.gbResults.Name = "gbResults";
            this.gbResults.Size = new System.Drawing.Size(471, 235);
            this.gbResults.TabIndex = 1;
            this.gbResults.TabStop = false;
            this.gbResults.Text = "Results";
            // 
            // m_Results
            // 
            this.m_Results.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_Results.FormattingEnabled = true;
            this.m_Results.Location = new System.Drawing.Point(8, 19);
            this.m_Results.Name = "m_Results";
            this.m_Results.Size = new System.Drawing.Size(457, 199);
            this.m_Results.TabIndex = 0;
            // 
            // mnuRightClick
            // 
            this.mnuRightClick.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuPlugins});
            // 
            // mnuPlugins
            // 
            this.mnuPlugins.Index = 0;
            this.mnuPlugins.Text = "Plugins...";
            // 
            // frmWaveSearch
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 410);
            this.ContextMenu = this.mnuRightClick;
            this.Controls.Add(this.gbResults);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmWaveSearch";
            this.Text = "RAVE Wave Search";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.searchOptionsGroupBox.ResumeLayout(false);
            this.searchOptionsGroupBox.PerformLayout();
            this.gbResults.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStringInput;
        private System.Windows.Forms.GroupBox gbResults;
        private System.Windows.Forms.ListBox m_Results;
        private System.Windows.Forms.ContextMenu mnuRightClick;
        private System.Windows.Forms.MenuItem mnuPlugins;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.GroupBox searchOptionsGroupBox;
        private System.Windows.Forms.RadioButton searchWaveNamesBtn;
        private System.Windows.Forms.RadioButton searchWaveFoldersBtn;
        private System.Windows.Forms.RadioButton searchWaveHashNamesBtn;
    }
}