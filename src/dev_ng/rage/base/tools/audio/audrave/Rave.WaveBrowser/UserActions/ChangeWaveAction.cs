namespace Rave.WaveBrowser.UserActions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    using Rave.Instance;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    ///   Encapsulates the action of a user modifying a wave
    /// </summary>
    public class ChangeWaveAction : UserAction
    {
        private readonly string m_assetPath;
        private readonly string m_parentPath;
        private readonly Dictionary<string, EditorTreeNode.NodeDisplayState> m_previousStates;
        private readonly string m_waveName;
        private readonly string m_wavePath;

        public ChangeWaveAction(IActionParams parameters)
        {
            this.ActionParameters = parameters;
            this.m_parentPath = parameters.ParentNode.GetObjectPath();
            this.m_waveName = Path.GetFileName(this.ActionParameters.SourcePath.ToUpper().Replace(" ", "_"));
            this.m_previousStates = new Dictionary<string, EditorTreeNode.NodeDisplayState>();

            //asset path
            var sb = new StringBuilder();
            sb.Append(this.m_parentPath);
            sb.Append("\\");
            sb.Append(this.m_waveName);
            this.m_assetPath = sb.ToString();

            //folder path
            sb = new StringBuilder();
            sb.Append(Configuration.PlatformWavePath);
            sb.Append(this.m_assetPath);
            this.m_wavePath = sb.ToString();
        }

        public override string GetAssetPath()
        {
            return this.m_assetPath;
        }

        protected override bool doAction()
        {
            if (this.ActionParameters.ActionLog.SearchForWaveDeletion(this.m_assetPath) != null)
            {
                ErrorManager.HandleInfo("There is a pending delete for " + this.m_assetPath +
                                        " - you must commit your changes before trying to add this file again");
                return false;
            }

            IUserAction waveAdd = this.ActionParameters.ActionLog.SearchForWaveAdd(this.m_assetPath);
            if (waveAdd != null)
            {
                waveAdd.ActionParameters.SourcePath = this.ActionParameters.SourcePath;
                return false;
            }

            if (this.ActionParameters.ParentNode.TryToLock())
            {
                foreach (var platform in this.ActionParameters.Platforms)
                {
                    this.m_previousStates.Add(platform, this.ActionParameters.Node.GetNodeDisplayState(platform));
                    (this.ActionParameters.Node).UpdateDisplayState(EditorTreeNode.NodeDisplayState.ChangedLocally,
                                                           platform,
                                                           true,
                                                           true);
                }
                this.ActionParameters.ParentNode.Expand();
                return true;
            }
            return false;
        }

        protected override bool doUndo()
        {
            foreach (var platform in this.ActionParameters.Platforms)
            {
                this.ActionParameters.Node.UpdateDisplayState(this.m_previousStates[platform], platform, true, true);
                this.m_previousStates.Remove(platform);
            }
            return true;
        }

        protected override bool doApply(IPendingWaveList temporaryWaveList, string platform)
        {
            try
            {
                if (
                    !RaveInstance.AssetManager.IsCheckedOut(this.m_wavePath))
                {
                    if (RaveInstance.RaveAssetManager.WaveChangeList == null)
                    {
                        RaveInstance.RaveAssetManager.WaveChangeList =
                            RaveInstance.AssetManager.CreateChangeList("[Rave] Wave/Midi Changelist");
                    }

                    RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(this.m_wavePath, false);
                }

                File.Copy(this.ActionParameters.SourcePath, this.m_wavePath, true);
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }

            // add this to the temp wave list
            temporaryWaveList.RecordModifyWave(
                    this.m_parentPath + "\\" + this.m_waveName);

            return true;
        }

        protected override bool doCommit()
        {
            try
            {
                if (RaveInstance.RaveAssetManager.WaveChangeList == null)
                {
                    RaveInstance.RaveAssetManager.WaveChangeList =
                        RaveInstance.AssetManager.CreateChangeList("[Rave] Wave/Midi Changelist");
                }

                RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(this.m_wavePath, true);
                File.Copy(this.ActionParameters.SourcePath, this.m_wavePath, true);
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
                return false;
            }

            // add this to the pending wave list
            foreach (var platform in this.ActionParameters.Platforms)
            {
                (this.ActionParameters.Node).UpdateDisplayState(EditorTreeNode.NodeDisplayState.ChangedInPendingWaveList,
                                                       platform,
                                                       true,
                                                       true);
                this.ActionParameters.WaveBrowser.GetPendingWaveLists().GetPendingWaveList(platform).RecordModifyWave(
                    this.m_parentPath + "\\" + this.m_waveName);
            }

            return true;
        }

        public override string GetSummary()
        {
            var platforms = new StringBuilder();
            for (var i = 0; i < this.ActionParameters.Platforms.Count; i++)
            {
                platforms.Append(this.ActionParameters.Platforms[i]);
                if (i != this.ActionParameters.Platforms.Count - 1)
                {
                    platforms.Append(",");
                }
            }

            var sb = new StringBuilder();
            sb.Append("change ");
            sb.Append(Path.GetExtension(this.m_waveName) == ".MID" ? "midi " : "wave ");
            sb.Append(this.m_waveName);
            sb.Append(" in ");
            sb.Append(this.m_parentPath);
            sb.Append(" changed on ");
            sb.Append(platforms.ToString());

            return sb.ToString();
        }
    }
}