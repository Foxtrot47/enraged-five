﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Rave.Utils;
using Rave.WaveBrowser.Infrastructure.Interfaces;
using Rave.WaveBrowser.Infrastructure.Nodes;
using Rave.WaveBrowser.UserActions;
using Wavelib;

namespace Rave.WaveBrowser.WaveBrowser
{
	internal class RaveDriveHelper
	{
		private static readonly string[] ms_channelExtension = new[] { "_LEFT", "_RIGHT" };

		public static IEnumerable<TreeNode> FlattenCollection(TreeNodeCollection coll)
		{
			return coll.Cast<TreeNode>().Concat(coll.Cast<TreeNode>().SelectMany(x => FlattenCollection(x.Nodes)));
		}

		public static TreeNode GetNodeFromPath(TreeNode node, string path)
		{
			IEnumerable<TreeNode> treeNodes = FlattenCollection(node.Nodes);
			return treeNodes.FirstOrDefault(r => String.Equals(r.FullPath, path.TrimEnd('\\'), StringComparison.CurrentCultureIgnoreCase));
		}

		public static TreeNode GetNodeFromPath(PackNode packNode, string path)
		{
			TreeNode nodeFromPath = packNode;
			string[] split = path.Split('\\');
			for (int i = split.Length - 1; i > 0; i--)
			{
				IEnumerable<TreeNode> treeNodes = FlattenCollection(packNode.Nodes);
				TreeNode resultsNode = treeNodes.FirstOrDefault(r => split[i].Equals(r.Text, StringComparison.CurrentCultureIgnoreCase));
				if (resultsNode != null && path.Contains(resultsNode.FullPath))
				{
					return resultsNode;
				}
			}

			return packNode;
		}

		public static void HandleDriveFile(string[] files, string rootDir, ctrlWaveBrowser wavebrowser, IActionLog actionLog)
		{
			ctrlWaveEditorTreeView waveEditorTree = (ctrlWaveEditorTreeView)wavebrowser.GetEditorTreeView();
			try
			{
				ArrayList actions = new ArrayList();
				EditorTreeNode.AllowAutoLock = true;
				foreach (string file in files)
				{
					if (!File.Exists(file) && !Directory.Exists(file))
					{
						return;
					}
				   

					if (!waveEditorTree.ValidateFiles(new string[] { file }))
					{
						throw new Exception(string.Format("File not valid"));
					}

                    string gamefileName = wavebrowser.FormatGameString(file);

                    string bankPath = gamefileName.Remove(0, rootDir.Length).TrimEnd(Path.GetFileName(gamefileName).ToCharArray()).TrimEnd('\\');

					string[] split = bankPath.Split('\\');

					string packName = split[0];
					if (!waveEditorTree.TryToLoadPack(packName, false))
					{
						throw new Exception(string.Format("Pack {0} cannot be loaded", packName));
					}
					if (split.Length == 1)
					{
						throw new Exception(string.Format("File not dropped in a bank."));
					}

					PackNode packNode = waveEditorTree.GetPackNodes().FirstOrDefault(p => p.GetObjectName().Equals(packName, StringComparison.CurrentCultureIgnoreCase));
					EditorTreeNode node = (EditorTreeNode)GetNodeFromPath(packNode, bankPath);

					if (node != null)
					{
						if (GetNodeFromPath(node, bankPath) != null)
						{
							node = (EditorTreeNode)(GetNodeFromPath(node, bankPath));
						}
					}

					if (!node.FullPath.Equals(bankPath, StringComparison.CurrentCultureIgnoreCase))
					{
						node = ProcessFoldersInPath(wavebrowser, actionLog, actions, bankPath, split, node);
					}

					GetNodeFromPath(node, bankPath);
					if (!node.FullPath.Equals(bankPath, StringComparison.CurrentCultureIgnoreCase))
					{
						throw new Exception(string.Format("{0} is not a valid node in wave structure", bankPath));
					}

					if (Directory.Exists(file))
					{
                        CreateDirectoryNode(wavebrowser, actionLog, actions, gamefileName, node);
					}
					else
					{
                        string newFile = GetTempRaveDrivePath(rootDir, gamefileName);

						try
						{
							File.Copy(file, newFile);
						}
						catch
						{
							return;
						}

                        if (actionLog.SearchForWaveAdd(gamefileName.Remove(0, rootDir.Length)) != null)
						{
                            actionLog.SearchForWaveAdd(gamefileName.Remove(0, rootDir.Length))
								.ActionParameters.SourcePath = newFile;
						}
						else
						{
							try { HandleWaveFile(node, newFile, wavebrowser, actions, actionLog); }
							catch
							{
								throw new Exception(string.Format("File was not processed correctly"));
							}
						}
					}
				}

				BulkAction bulkAction = new BulkAction(new ActionParams(null,
																		  null,
																		  null,
																		  null,
																		  null,
																		  actionLog,
																		  null,
																		  null,
																		  null,
																		  actions));
				if (bulkAction.Action())
				{
					actionLog.AddAction(bulkAction);
					Thread thread = new Thread(() => DeleteFiles(files));
					thread.Start();
				}
			}
			finally
			{
				EditorTreeNode.AllowAutoLock = false;
			}
		}

		public static string RenameFileIfExists(string fileName)
		{
			if (File.Exists(fileName))
			{
				string directoryName = fileName.Replace("\\" + Path.GetFileName(fileName), "");
				fileName = fileName.Replace(directoryName, directoryName + "_" + DateTime.Now.Ticks.ToString());
				if (File.Exists(fileName))
				{
					fileName = RenameFileIfExists(fileName);
				}
			}

			return fileName;
		}

		private static void CreateDirectoryNode(ctrlWaveBrowser wavebrowser, IActionLog actionLog, ArrayList actions, string file, EditorTreeNode node)
		{
			List<string> platforms = ((EditorTreeNode)node).GetPlatforms();
			AddWaveFolderAction action =
				new AddWaveFolderAction(
					new ActionParams(
						  wavebrowser,
						  node,
						  null,
						  null,
						  null,
						  actionLog,
						  new DirectoryInfo(file).Name,
						  platforms,
						  null,
						  null));

			if (action.Action())
			{
				actions.Add(action);
			}
		}

		private static bool CreateWaveFilePerChannel(bwWaveFile waveFile, int channelIndex, string filePath, bwMarkerList markers)
		{
			try
			{
				var tempWave = new bwWaveFile();
				foreach (IRiffChunk chunk in waveFile.Chunks)
				{
					var fChunk = chunk as bwFormatChunk;
					var dChunk = chunk as bwDataChunk;

					if (fChunk != null)
					{
						// current chunk is format chunk
						var formatChunk = new bwFormatChunk(
							fChunk.CompressionCode,
							1,
							fChunk.SampleRate,
							fChunk.AverageBytesPerSecond / 2,
							(ushort)(fChunk.BlockAlign / 2),
							fChunk.SignificantBitsPerSample,
							fChunk.ExtraFormatBytes);
						tempWave.AddChunk(formatChunk);
						tempWave.Format = formatChunk;
					}
					else if (dChunk != null)
					{
						// current chunk is data chunk
						var newData = new byte[(uint)(0.5 * dChunk.RawData.Length)];

						// new chunk index
						var index = 0;

						// iterate through current raw data and split up
						// j set to 2* channel, 16bits/sample so increase j by 4 bytes
						for (int j = channelIndex * 2; j < dChunk.RawData.Length; j += 4)
						{
							newData[index] = dChunk.RawData[j];
							newData[index + 1] = dChunk.RawData[j + 1];
							index += 2;
						}

						var dataChunk = new bwDataChunk(newData, dChunk.NumSamples, bwDataChunk.DataFormat.S16);
						tempWave.Data = dataChunk;
					}
					else
					{
						tempWave.AddChunk(chunk);
					}
				}

				tempWave.FileName = filePath;
				tempWave.Markers = markers;
				tempWave.Save();
				return true;
			}
			catch (Exception)
			{
				ErrorManager.HandleInfo("Invalid wave file - only 16bit waves are supported");
				return false;
			}
		}

		private static void DeleteFiles(string[] files)
		{
			bool deleted = false;

			while (deleted == false)
			{
				deleted = true;
				try
				{
					foreach (string file in files)
					{
						if (File.Exists(file))
						{
							FileInfo fileInfo = new FileInfo(file);
							fileInfo.IsReadOnly = false;
							File.Delete(file);
						}
					}
				}
				catch
				{
					deleted = false;
					Thread.Sleep(20);
				}
			}
		}

		private static string GetTempRaveDrivePath(string rootDir, string file)
		{
			string newFile = Path.Combine(Path.GetTempPath() + "\\Rave.Drive\\", file.Remove(0, rootDir.Length));

			if (string.IsNullOrEmpty(newFile))
			{
				throw new Exception(string.Format("Temp folder seems to be corrupt. Please clear. Restarting your computer may fix the problem."));
			}

			newFile = RenameFileIfExists(newFile);

			if (!Directory.Exists(Path.GetDirectoryName(newFile)))
			{
				Directory.CreateDirectory(Path.GetDirectoryName(newFile));
			}
			return newFile;
		}
		private static void HandleWaveFile(EditorTreeNode bankNode, string file, ctrlWaveBrowser wavebrowser, ArrayList userActions, IActionLog actionLog)
		{
			var waveFile = new bwWaveFile(file, false);
			uint numOfChannels = waveFile.Format.NumChannels;
			var newWaveNames = new string[numOfChannels];
            
            if (waveFile.Format.SignificantBitsPerSample != 16)
            {
                throw new Exception(string.Format("The file you are trying to import has a bit rate of {0}. Only 16 bit wav file are supported.", waveFile.Format.SignificantBitsPerSample));
            }

			// file paths needed for multichannel
			var filePaths = new string[numOfChannels];
			string newWaveName = file.ToUpper().Substring(file.LastIndexOf("\\") + 1).Replace(" ", "_");
			if (numOfChannels == 1)
			{
				// mono sound
				newWaveNames[0] = newWaveName;
				filePaths[0] = file;
			}
			else
			{
				// multichannel add  extension
				bwMarkerList markers = waveFile.Markers;
				for (int i = 0; i < numOfChannels; i++)
				{
					string name = newWaveName;
					newWaveNames[i] = name.Substring(0, name.Length - 4) + ms_channelExtension[i] + ".WAV";
					filePaths[i] = Path.GetTempPath() + newWaveNames[i];

					if (!CreateWaveFilePerChannel(waveFile, i, filePaths[i], markers)) return;
				}
			}

			var existingWaves = new WaveNode[numOfChannels];
			for (var waveNameIndex = 0; waveNameIndex < newWaveNames.Length; waveNameIndex++)
			{
				for (var i = 0; i < bankNode.Nodes.Count; i++)
				{
					if (bankNode.Nodes[i].GetType() == typeof(WaveNode))
					{
						if (((WaveNode)bankNode.Nodes[i]).GetObjectName().ToUpper() == newWaveNames[waveNameIndex])
						{
							existingWaves[waveNameIndex] = (WaveNode)bankNode.Nodes[i];
						}
					}
				}
			}

			for (var i = 0; i < existingWaves.Length; i++)
			{
				var platforms = bankNode.GetPlatforms();
				if (platforms.Count <= 0)
				{
					continue;
				}

				if (existingWaves[i] == null)
				{
					// this is a new wave file
					var action =
						new AddWaveAction(
							new ActionParams(
								wavebrowser,
								bankNode,
								null,
								filePaths[i],
								null,
								actionLog,
								null,
								platforms,
								null,
								null));
					if (action.Action())
					{
						userActions.Add(action);
					}
				}
				else
				{
					// this is an existing wave file
					var action =
						new ChangeWaveAction(
							new ActionParams(
								wavebrowser,
								bankNode,
								existingWaves[i],
								filePaths[i],
								null,
								actionLog,
								null,
								existingWaves[i].GetPlatforms(),
								null,
								null));
					if (action.Action())
					{
						userActions.Add(action);
					}
				}
			}
		}

		private static EditorTreeNode ProcessFoldersInPath(ctrlWaveBrowser wavebrowser, IActionLog actionLog, ArrayList actions, string bankPath, string[] split, EditorTreeNode node)
		{
			string newBankPath = bankPath;
			int validSubstring = 0;
			for (int i = split.Length - 1; i > 1; i--)
			{
				newBankPath = newBankPath.TrimEnd('\\').TrimEnd(split[i].ToCharArray()).TrimEnd('\\');
				if (GetNodeFromPath(node, newBankPath) != null)
				{
					node = (EditorTreeNode)(GetNodeFromPath(node, newBankPath));
				}
				validSubstring = i;
				if (node.FullPath.Equals(newBankPath, StringComparison.CurrentCultureIgnoreCase))
				{
					break;
				}
			}

			for (int i = validSubstring; i < split.Length; i++)
			{
				if (string.IsNullOrEmpty(split[i]))
				{
					break;
				}
				newBankPath = newBankPath + '\\' + split[i];
				List<string> platforms = node.GetPlatforms();
				AddWaveFolderAction action =
					new AddWaveFolderAction(
						new ActionParams(
							wavebrowser,
							node,
							null,
							null,
							null,
							actionLog,
							split[i],
							platforms,
							null,
							null));

				if (action.Action())
				{
					actions.Add(action);
				}

				if (GetNodeFromPath(node, newBankPath) != null)
				{
					node = (EditorTreeNode)(GetNodeFromPath(node, newBankPath));
				}

				if (node.FullPath.Equals(bankPath,
					StringComparison.CurrentCultureIgnoreCase))
				{
					break;
				}
			}
			return node;
		}
	}
}