﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;
using GalaSoft.MvvmLight;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.Charts.Navigation;
using Microsoft.Research.DynamicDataDisplay.Charts.Shapes;
using GalaSoft.MvvmLight.Command;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using ModelLib.CustomAttributes;
using Rave.SineCurveEditor.View;
using Rave.Types.Infrastructure.Interfaces.Objects;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;
using Point = System.Windows.Point;
using WPFToolLib.Extensions;
using Microsoft.Research.DynamicDataDisplay.Charts;
using System.Windows;
using Models;

namespace Rave.SineCurveEditor.ViewModel
{
	public class SineCurveEditorViewModel : INotifyPropertyChanged
	{
		private ICommand _mouseMove;
		private Dictionary<float, float> _pointDictionary;
		private DraggablePoint _pointOnGraph;
		private PointAndCoords _pointAndCoords;
		public ChartPlotter lineChartPlotter { get; set; }
		private SineCurve _sineCurve;


		public float EndPhase
		{
			get { return _sineCurve.EndPhase; }
			private set
			{
				if (value > StartPhase)
				{
					_sineCurve.EndPhase = Convert.ToSingle(value.ToString("n2"));
					PropertyChanged.Raise(this, "EndPhase");
				}
			}
		}

		public bool ClampInput
		{
			get
			{
				if (_sineCurve.ClampInput == ModelLib.Types.TriState.yes)
					return true;
				else
					return false;
			}

			private set
			{
				if (value == true)
				{
					_sineCurve.ClampInput = ModelLib.Types.TriState.yes;
				}
				else
				{
					_sineCurve.ClampInput = ModelLib.Types.TriState.no;
				}
				PropertyChanged.Raise(this, "ClampInput");
			}
		}


		public bool IsReadOnly
		{
			get
			{
				if (_objectToEdit != null)
				{
					return _objectToEdit.IsReadOnly;
				}
				return true;
			}
		}


		public float Frequency
		{
			get { return _sineCurve.Frequency; }
			private set
			{
				_sineCurve.Frequency = Convert.ToSingle(value.ToString("n2"));
				PropertyChanged.Raise(this, "Frequency");
			}
		}

		public float MaxInput
		{
			get { return _sineCurve.MaxInput; }
			private set
			{
				if (value > MinInput)
				{
					_sineCurve.MaxInput = Convert.ToSingle(value.ToString("n2"));
					PropertyChanged.Raise(this, "MaxInput");
				}
			}
		}

		public float MinInput
		{
			get { return _sineCurve.MinInput; }
			private set
			{
				if (value < MaxInput)
				{
					_sineCurve.MinInput = Convert.ToSingle(value.ToString("n2"));

					PropertyChanged.Raise(this, "MinInput");
				}
			}
		}

		private bool _clamp;

		public ICommand MouseMove
		{
			get
			{
				if (_mouseMove == null)
				{
					_mouseMove = new RelayCommand<MouseEventArgs>(
						OnMouseMove,
						e => true
						);
				}
				return _mouseMove;
			}
		}

		public Dictionary<float, float> PointDictionary
		{
			get { return _pointDictionary; }
			set
			{
				_pointDictionary = value;
				PropertyChanged.Raise(this, "PointDictionary");
			}
		}

		public DraggablePoint PointOnGraph
		{
			get { return _pointOnGraph; }
			set
			{
				_pointOnGraph = value;
				PropertyChanged.Raise(this, "PointOnGraph");
			}
		}

		public float StartPhase
		{
			get { return _sineCurve.StartPhase; }
			private set
			{
				if (value < EndPhase)
				{
					_sineCurve.StartPhase = Convert.ToSingle(value.ToString("n2"));
					PropertyChanged.Raise(this, "StartPhase");
				}
			}
		}

		public bool Clamp
		{
			get { return _clamp; }
			private set
			{
				_clamp = value;
				PropertyChanged.Raise(this, "Clamp");
			}
		}

		public float VerticalOffset
		{
			get { return _sineCurve.VerticalOffset; }
			private set
			{
				_sineCurve.VerticalOffset = Convert.ToSingle(value.ToString("n2"));
				PropertyChanged.Raise(this, "VerticalOffset");
			}
		}

		public float VerticalScaling
		{
			get { return _sineCurve.VerticalScaling; }
			private set
			{
				_sineCurve.VerticalScaling = Convert.ToSingle(value.ToString("n2"));
				PropertyChanged.Raise(this, "VerticalScaling");
			}
		}

		private ICommand _mouseWheel;

		public ICommand MouseWheel
		{
			get
			{
				if (_mouseWheel == null)
				{
					_mouseWheel = new RelayCommand<MouseWheelEventArgs>(
						OnMouseWheel,
						e => true
						);
				}
				return _mouseWheel;
			}
		}

		private void OnMouseWheel(MouseWheelEventArgs e)
		{
			if (Keyboard.IsKeyDown(Key.LeftShift))
			{
				VerticalScaling = (float)((e.Delta > 0) ? VerticalScaling + 0.1d : VerticalScaling - 0.1d);
			}
		}

		private Point _previousPosition;


		public SineCurveEditorViewModel()
		{
			_isEditing = false;
			_sineCurve = new SineCurve();
			PointOnGraph = new DraggablePoint();
			_pointAndCoords = new PointAndCoords();
			PointOnGraph.Content = _pointAndCoords;
			this.PropertyChanged += OnPropertyChanged;
		}

		public void EditObject(IObjectInstance objectInstance)
		{

			_isEditing = false;
			_objectToEdit = objectInstance;


			if (objectInstance == null)
			{
				return;
			}

			if (this._objectToEdit != null && this._objectToEdit.Bank != null)
			{
				this._objectToEdit.Bank.BankStatusChanged -= this.OnBankStatusChanged;
			}

			this._objectToEdit = objectInstance;
			if (this._objectToEdit == null)
			{
				return;
			}

			if (this._objectToEdit.Bank != null)
			{
				this._objectToEdit.Bank.BankStatusChanged += this.OnBankStatusChanged;
			}


			if (_objectToEdit.Node != null && _objectToEdit.Node.ChildNodes.Count > 0)
			{

				this._sineCurve = new SineCurve();

				foreach (XmlNode valueNode in _objectToEdit.Node.ChildNodes)
				{
					
					if (valueNode.Name.Equals("EndPhase")) _sineCurve.EndPhase = Convert.ToSingle(valueNode.InnerText);
					if (valueNode.Name.Equals("StartPhase")) _sineCurve.StartPhase = Convert.ToSingle(valueNode.InnerText);
					if (valueNode.Name.Equals("Frequency")) _sineCurve.Frequency = Convert.ToSingle(valueNode.InnerText);
					if (valueNode.Name.Equals("VerticalScaling")) _sineCurve.VerticalScaling = Convert.ToSingle(valueNode.InnerText);
					if (valueNode.Name.Equals("VerticalOffset")) _sineCurve.VerticalOffset = Convert.ToSingle(valueNode.InnerText);
					if (valueNode.Name.Equals("MaxInput")) _sineCurve.MaxInput = Convert.ToSingle(valueNode.InnerText);
					if (valueNode.Name.Equals("MinInput")) _sineCurve.MinInput = Convert.ToSingle(valueNode.InnerText);
					if (valueNode.Name.Equals("ClampInput"))
					{
						if (valueNode.InnerText.Trim().Equals("yes", StringComparison.InvariantCultureIgnoreCase) || valueNode.InnerText.Trim().Equals("true", StringComparison.InvariantCultureIgnoreCase))
						{
							_sineCurve.ClampInput = ModelLib.Types.TriState.yes;
						}
						else
						{
							_sineCurve.ClampInput = ModelLib.Types.TriState.no;
						}

					}
				}
				_isEditing = true;
			}

			if (!lineChartPlotter.Children.Contains(this.PointOnGraph))
			{
			
				lineChartPlotter.Children.Add(new VerticalLine { Stroke = Brushes.Black });
				lineChartPlotter.Children.Add(new HorizontalLine(0.0) { Stroke = Brushes.Black });
				lineChartPlotter.Children.Add(this.PointOnGraph);

			}


			_sineCurve.PropertyChanged -= OnPropertyChanged;
			_sineCurve.PropertyChanged += OnPropertyChanged;
			FitToVisibleAndUpdateGraph();

		}

		private void FitToVisibleAndUpdateGraph()
		{
			double minY = !Clamp ? VerticalScaling * -1 + VerticalOffset : 0;
			double maxY = !Clamp ? VerticalScaling + VerticalOffset : 1;
			lineChartPlotter.Visible = new Rect(Convert.ToDouble(this.MinInput), minY, Math.Abs(MaxInput - MinInput), Math.Abs(maxY - minY));
			lineChartPlotter.Viewport.Zoom(1.1);
			UpdateGraph();
		}

		private void OnBankStatusChanged()
		{
			this.PropertyChanged.Raise(this, "IsReadOnly");
		}

		private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "VerticalScaling":
				case "VerticalOffset":
				case "EndPhase":
				case "StartPhase":
				case "Frequency":
					if (sender == this && !IsReadOnly && _isEditing)
					{
						SaveToXml();
					}
					UpdateGraph();
					break;
				case "ClampInput":
				case "MinInput":
				case "MaxInput":
					if (sender == this && !IsReadOnly && _isEditing)
					{
						SaveToXml();
					}

					FitToVisibleAndUpdateGraph();

					break;
				case "Clamp":

					FitToVisibleAndUpdateGraph();
					break;
			}
		}

		private void UpdateGraph()
		{
			const float rangeMultiplicator = 300.0F;


			int minValue = (int)Math.Ceiling(lineChartPlotter.Visible.Left * rangeMultiplicator);
			int range = (int)Math.Ceiling((lineChartPlotter.Visible.Right - lineChartPlotter.Visible.Left) * rangeMultiplicator);


			IEnumerable<int> xMultiplied = Enumerable.Range(minValue, range);
			float[] x = xMultiplied.Select(i => i / rangeMultiplicator).ToArray();
			float[] y = new float[x.Length];
			if (ClampInput == true)
			{
				for (int i = 0; i < x.Length; i++)
				{
					if (x[i] < MinInput)
					{
						y[i] = CalculateSineCurve(MinInput);
					}
					else if (x[i] > MaxInput)
					{
						y[i] = CalculateSineCurve(MaxInput);
					}
					else
					{
						y[i] = CalculateSineCurve(x[i]);
					}
				}
			}
			else
			{
				y = x.Select(CalculateSineCurve).ToArray();
			}
			PointDictionary = x.Select((t, i) => new KeyValuePair<float, float>(t, y[i]))
				.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

			DataSource = new EnumerableDataSource<KeyValuePair<float, float>>(PointDictionary);
			DataSource.SetXYMapping(p => new Point(p.Key, p.Value));
			PropertyChanged.Raise(this, "DataSource");

			if (lineChartPlotter != null)
			{
				foreach (var child in lineChartPlotter.Children.Where(child => child.GetType() == typeof(LineGraph)))
				{
					lineChartPlotter.Children.Remove(child);
					break;
				}


				lineChartPlotter.AddLineGraph(DataSource, Colors.DodgerBlue, 2);
								
				lineChartPlotter.Viewport.PropertyChanged -= Viewport_PropertyChanged;
				lineChartPlotter.Viewport.PropertyChanged += Viewport_PropertyChanged;
			}


		}

		private void Viewport_PropertyChanged(object sender, ExtendedPropertyChangedEventArgs e)
		{
			if (e.PropertyName == "Visible")
			{
				if (_sineCurve != null)
				{
					UpdateGraph();
				}
			}
		}


		public EnumerableDataSource<KeyValuePair<float, float>> DataSource { get; private set; }

		private ICommand _clampCommand;
		private ICommand _previewMouseDown;

		public ICommand ClampCommand
		{
			get
			{
				if (_clampCommand == null)
				{
					_clampCommand = new RelayCommand
						(
						() => Clamp = !Clamp,
						() => true
						);
				}
				return _clampCommand;
			}
		}

		public ICommand PreviewMouseDown
		{
			get
			{
				if (_previewMouseDown == null)
				{
					_previewMouseDown = new RelayCommand<MouseEventArgs>
						(
						e =>
						{
							Point pt = e.GetPosition(lineChartPlotter.CentralGrid);
							Point ps = lineChartPlotter.Viewport.Transform.ScreenToViewport(pt);
							_previousPosition = new Point(ps.X, ps.Y);
							lineChartPlotter.Viewport.AutoFitToView = false;
						},
						e => true
						);
				}
				return _previewMouseDown;
			}
		}
		private ICommand _mouseLeave;
		public ICommand MouseLeave
		{
			get
			{
				if (_mouseLeave == null)
				{
					_mouseLeave = new RelayCommand
						(
						() => PointOnGraph.Visibility = System.Windows.Visibility.Collapsed
						);
				}
				return _mouseLeave;
			}
		}

		private ICommand _mouseEnter;
		public ICommand MouseEnter
		{
			get
			{
				if (_mouseEnter == null)
				{
					_mouseEnter = new RelayCommand
						(
						() => PointOnGraph.Visibility = System.Windows.Visibility.Visible
						);
				}
				return _mouseEnter;
			}
		}

		private ICommand _mouseUp;

		public ICommand MouseUp
		{
			get
			{
				if (_mouseUp == null)
				{
					_mouseUp = new RelayCommand<MouseEventArgs>
						(
						e =>
						{
							lineChartPlotter.Viewport.AutoFitToView = true;
						},
						e => true
						);
				}
				return _mouseUp;
			}
		}


		private ICommand _fitToView;
		private IObjectInstance _objectToEdit;

		public ICommand FitToView
		{
			get
			{
				if (_fitToView == null)
				{
					_fitToView = new RelayCommand
						(
						() => FitToVisibleAndUpdateGraph()

						);
				}
				return _fitToView;
			}
		}

		private ICommand _zoomIn;

		public ICommand ZoomIn
		{
			get
			{
				if (_zoomIn == null)
				{
					_zoomIn = new RelayCommand
						(
						() =>
						{
							lineChartPlotter.Viewport.Zoom(0.75);
							UpdateGraph();
						}
						);
				}
				return _zoomIn;
			}
		}

		private ICommand _zoomOut;

		public ICommand ZoomOut
		{
			get
			{
				if (_zoomOut == null)
				{
					_zoomOut = new RelayCommand
						(
						() =>
						{
							lineChartPlotter.Viewport.Zoom(1.25);
							UpdateGraph();
						}
						);
				}
				return _zoomOut;
			}
		}


		private float CalculateSineCurve(float input)
		{
			float finalFrequency = EndPhase > 0 ? EndPhase - StartPhase : Frequency;
			float result =
				(float)(VerticalScaling * Math.Sin(DegreeToRadians(StartPhase + finalFrequency * input)) + VerticalOffset);

			if (Clamp)
			{
				if (result < 0)
				{
					return 0;
				}
				if (result > 1)
				{
					return 1;
				}
			}
			return result;
		}

		private float DegreeToRadians(float x)
		{
			return (float)(1.74532925199e-2 * x);
		}

		private void OnMouseMove(MouseEventArgs e)
		{


			Point pt = e.GetPosition(lineChartPlotter.CentralGrid);
			Point ps = lineChartPlotter.Viewport.Transform.ScreenToViewport(pt);

			float actualXPoint = PointDictionary.Keys.Aggregate((x, y) => Math.Abs(x - ps.X) < Math.Abs(y - ps.X) ? x : y);

			PointOnGraph.Position = new Point(actualXPoint, PointDictionary[actualXPoint]);
			_pointAndCoords.Text =
					string.Format("{0} , {1}", String.Format("{0:0.##}", actualXPoint), String.Format("{0:0.##}", PointDictionary[actualXPoint]));

			GeneralTransform childTransform = _pointAndCoords.Coords.TransformToAncestor(lineChartPlotter);
			Rect rectangle = childTransform.TransformBounds(new Rect(new Point(0, 0), _pointAndCoords.Coords.RenderSize));
			Rect result = Rect.Intersect(new Rect(new Point(0, 0), lineChartPlotter.RenderSize), rectangle);
			if (Math.Abs(result.Width - rectangle.Width) > 1)
			{
				_pointAndCoords.ChangeAltCoorsPosition();
			}



			PropertyChanged.Raise(this, "PointOnGraph");

			if (e.Source is AxisNavigation || e.Source is Canvas)
			{
				if (e.Source is Canvas)
				{
					((Canvas)e.Source).Cursor = Cursors.ScrollAll;
				}

				return;
			}



			lineChartPlotter.Cursor = Cursors.Cross;

			if (IsReadOnly)
			{
				return;
			}
			if (e.LeftButton == MouseButtonState.Pressed)
			{
				Point currentPosition = new Point(ps.X, ps.Y);

				if (Keyboard.IsKeyDown(Key.LeftShift))
				{
					float deltaX = (float)(currentPosition.X - _previousPosition.X);

					if (Math.Abs(deltaX) > 0)
					{
						float newEndPhase = EndPhase - (deltaX * 180f / (float)Math.Abs(lineChartPlotter.Visible.Width));
						if (newEndPhase > 0 && newEndPhase > StartPhase)
						{

							EndPhase = newEndPhase;
						}
					}
				}
				else
				{
					lineChartPlotter.Cursor = Cursors.SizeAll;
					float deltaX = (float)(currentPosition.X - _previousPosition.X);

					if (Math.Abs(deltaX) > 0)
					{
						float D = EndPhase - StartPhase;
						if (StartPhase - deltaX * Math.Abs(EndPhase - StartPhase) + D > 0)
						{
							StartPhase -= (deltaX * Math.Abs(EndPhase - StartPhase));
							EndPhase = (StartPhase + D);
						}

					}
					float deltaY = (float)(currentPosition.Y - _previousPosition.Y);
					if (Math.Abs(deltaY) > 0)
					{
						float movement = (float)Math.Round(deltaY * 100f) / 100f;
						if (movement == 0)
						{
							movement = deltaY > 0 ? 0.01f : -0.01f;
						}


						VerticalOffset += movement;
					}
				}
			}
			_previousPosition = new Point(ps.X, ps.Y);
		}

		private string GetDefault(string propertyName)
		{
			PropertyInfo info = typeof(SineCurve).GetProperties().FirstOrDefault(p => p.Name == propertyName);
			if (info == null) return string.Empty;

			DefaultAttribute defaultAttribute =
				(DefaultAttribute)info.GetCustomAttributes(true).FirstOrDefault(p => p.GetType() == typeof(DefaultAttribute));
			if (defaultAttribute != null) return defaultAttribute.Value;
			return string.Empty;
		}

		private void SaveToXml()
		{
			XmlNode clampNode = null;
			foreach (XmlNode valueNode in this._objectToEdit.Node.ChildNodes)
			{
				if (valueNode.Name.Equals("StartPhase")) valueNode.InnerText = StartPhase.ToString("n2");
				if (valueNode.Name.Equals("EndPhase")) valueNode.InnerText = EndPhase.ToString("n2");
				if (valueNode.Name.Equals("Frequency")) valueNode.InnerText = Frequency.ToString("n2");
				if (valueNode.Name.Equals("VerticalScaling"))
					valueNode.InnerText = VerticalScaling.ToString("n2");
				if (valueNode.Name.Equals("VerticalOffset"))
					valueNode.InnerText = VerticalOffset.ToString("n2");
				if (valueNode.Name.Equals("MaxInput")) valueNode.InnerText = MaxInput.ToString("n2");
				if (valueNode.Name.Equals("MinInput")) valueNode.InnerText = MinInput.ToString("n2");
				if (valueNode.Name.Equals("ClampInput"))
				{
					clampNode = valueNode;
				}
			}

			if (clampNode == null)
			{
				clampNode = _objectToEdit.Node.OwnerDocument.CreateElement("ClampInput");
				if (ClampInput == true) clampNode.InnerText = "yes";
				if (ClampInput == false) clampNode.InnerText = "no";
				_objectToEdit.Node.AppendChild(clampNode);
			}
			else
			{
				if (ClampInput == true) clampNode.InnerText = "yes";
				if (ClampInput == false) clampNode.InnerText = "no";
			}
			_objectToEdit.MarkAsDirty();
		}

		private bool _isEditing;


		public event PropertyChangedEventHandler PropertyChanged;
	}
}