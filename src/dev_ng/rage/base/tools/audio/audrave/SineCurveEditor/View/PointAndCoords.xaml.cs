﻿using System.ComponentModel;
using System.Windows.Controls;
using WPFToolLib.Extensions;

namespace Rave.SineCurveEditor.View
{
	/// <summary>
	/// Interaction logic for PointAndCoords.xaml
	/// </summary>
	public partial class PointAndCoords : Grid, INotifyPropertyChanged
	{
		public string Text
		{
			set { TextBlock.Content = value; }
		}

		public Label Coords
		{
			get { return TextBlock; }
		}

		public PointAndCoords()
		{
			InitializeComponent();
			DataContext = this;
		}

		public bool AltCoordPosition
		{
			get;
			set;
		}

		public void ChangeAltCoorsPosition()
		{

			AltCoordPosition = !AltCoordPosition;
			this.PropertyChanged.Raise(this, "AltCoordPosition");
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}
