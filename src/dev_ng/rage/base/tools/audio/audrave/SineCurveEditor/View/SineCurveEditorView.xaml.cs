﻿using System;
using System.Windows;
using System.Xml;
using Rave.Plugins.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.SineCurveEditor.ViewModel;


namespace Rave.SineCurveEditor.View
{
	public partial class SineCurveEditorView : IRAVEObjectEditorPlugin
	{
		private IObjectInstance _objectInstance;
		private SineCurveEditorViewModel _sineCurveEditorViewModel;

		public SineCurveEditorView()
		{

			InitializeComponent();

			_sineCurveEditorViewModel = new SineCurveEditorViewModel();

			DataContext = _sineCurveEditorViewModel;

			_sineCurveEditorViewModel.lineChartPlotter = lineGraphChartPlotter;
		}


		public string GetName()
		{
			return "Sine Curve Editor";
		}

		public bool Init(XmlNode settings)
		{
			return true;
		}

		public string ObjectType { get { return "SineCurve"; } }
#pragma warning disable 0067
		public event Action<IObjectInstance> OnObjectEditClick;
		public event Action<IObjectInstance> OnObjectRefClick;
		public event Action<string, string> OnWaveRefClick;
		public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067
		
		public void EditObject(IObjectInstance objectInstance, Mode mode)
		{
			_objectInstance = objectInstance;
			SineCurveEditorView_OnLoaded(this, null);
		}

		public void Dispose()
		{
			//Do nothing
		}

		private void SineCurveEditorView_OnLoaded(object sender, RoutedEventArgs e)
		{
			if (_sineCurveEditorViewModel != null && _objectInstance != null)
			{
				_sineCurveEditorViewModel.EditObject(_objectInstance);
			}
		}


	}
}