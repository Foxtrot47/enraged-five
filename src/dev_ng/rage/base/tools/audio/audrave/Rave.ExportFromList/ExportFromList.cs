namespace Rave.ExportFromList
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;

    using rageUsefulCSharpToolClasses;

    using Rave.Instance;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.Utils.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Interfaces;

    public class ExportFromList : IRAVEWaveBrowserPlugin
    {
        private string m_Name;
        private string m_InputFile;
        private string m_Params;
        private string m_Command;

        private IActionLog m_ActionLog;
        private IWaveBrowser m_WaveBrowser;
        private IBusy m_Busy;

        private List<string> m_Waves;
        private List<string> m_CheckedOut;
        private string m_RootPath;

        #region IRAVEPlugin Members

        public string GetName()
        {
            return m_Name;
        }

        public bool Init(XmlNode settings)
        {
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        m_Name = setting.InnerText;
                        break;
                    case "InputFile":
                        m_InputFile = setting.InnerText;
                        break;
                    case "Command":
                        m_Command = setting.InnerText;
                        break;
                    case "Parameters":
                        m_Params = setting.InnerText;
                        break;
                }
            }

            if (m_Name != "" && m_InputFile != "" && m_Params != "")
            {
                return true;
            }

            return false;
        }

        public void Shutdown()
        {
            return;
        }

        #endregion

        #region IRAVEWaveBrowserPlugin Members

        public void Process(IWaveBrowser waveBrowser, IActionLog actionLog, System.Collections.ArrayList nodes, IBusy busyFrm)
        {

            m_WaveBrowser = waveBrowser;
            m_ActionLog = actionLog;
            m_Busy = busyFrm;

            m_Waves = LoadWaveList(m_InputFile);
            m_CheckedOut = new List<string>();

            // get our root path from the asset manager
            m_RootPath = Configuration.PlatformWavePath;
            m_RootPath = m_RootPath.Replace("/", "").ToUpper();

            //Check for Errors
            if (m_Waves == null || m_Waves.Count == 0)
            {
                m_Busy.Close();
                return;
            }

            //Check for any pending actions
            if (m_ActionLog.GetCommitLogCount() >0)
            {
                ErrorManager.HandleInfo("Please commit previous changes before editing waves");
                return;
            }


            if (!CheckOutWaves())
            {
                return;
            }

            //run external application
            rageStatus status;
            rageExecuteCommand cmd = new rageExecuteCommand(m_Command, m_Params + m_InputFile, out status);

            if (status.Success())
            {
                m_Busy.Show();
                if (ErrorManager.HandleQuestion("The Speech Export plugin successfully completed.  Would you like to commit your changes?",
                     MessageBoxButtons.YesNo, DialogResult.Yes) == DialogResult.Yes)
                {
                    CommitChanges();
                }
                else
                {
                    m_Busy.SetStatusText("Undoing Checkout Of Waves");
                    Application.DoEvents();
                    try
                    {
                        RaveInstance.RaveAssetManager.WaveChangeList.Revert(false);
                        ErrorManager.HandleInfo("Undo Checkout Complete");
                    }
                    catch(Exception e)
                    {
                        ErrorManager.HandleError(e);
                    }
                }
                m_Busy.Close();
            }
            else
            {
                m_Busy.Show();
                m_Busy.SetUnknownDuration(true);
                m_Busy.SetStatusText("Undoing Checkout Of Waves");
                Application.DoEvents();
                try
                {
                    RaveInstance.RaveAssetManager.WaveChangeList.Revert(false);
                    ErrorManager.HandleInfo("Undo Checkout Complete");
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                }
                m_Busy.Close();
                string errorMsg = "The External Script Failed - Please Check The Log File (" + cmd.GetLogFilename() + ") for details.";
                ErrorManager.HandleError(errorMsg);
            }

        }

        private bool WritePendingWaveList()
        {
            return m_WaveBrowser.CommitChanges(true);
        }

        private void CommitChanges()
        {
            m_Busy.SetUnknownDuration(true);
            m_Busy.SetStatusText("Checking in Waves");
            //add change actions to pending waves

            try
            {
                foreach (string wavePath in m_CheckedOut)
                {
                    string path = wavePath.ToUpper().Replace(m_RootPath, "");

                    foreach (rage.PlatformSetting ps in Configuration.ActivePlatformSettings)
                    {
                        m_WaveBrowser.GetPendingWaveLists().GetPendingWaveList(ps.PlatformTag).RecordModifyWave(path);
                        Application.DoEvents();
                    }
                }

                //lock and serialise pending waves
                if (!WritePendingWaveList())
                {
                    return;
                }

                //submit all wave changes
                RaveInstance.RaveAssetManager.WaveChangeList.Submit("[Rave] Batch Export Change");
                RaveInstance.RaveAssetManager.WaveChangeList = null;
            }

            catch (Exception e)
            {
                ErrorManager.HandleInfo("Commit Failed! "+ e.ToString());
                RaveInstance.RaveAssetManager.WaveChangeList.Revert(false);
                return;
            }

            ErrorManager.HandleInfo("Commit Successful");
            return;
        }

        private bool CheckOutWaves()
        {
            StringBuilder sb = new StringBuilder();

            if (RaveInstance.RaveAssetManager.WaveChangeList == null)
            {
                try
                {
                    RaveInstance.RaveAssetManager.WaveChangeList = RaveInstance.AssetManager.CreateChangeList("[Rave] Wave changelist");
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                    return false;
                }
            }


            m_Busy.Show();
            //Check out Waves
            int j = 0;
            for (int i = 0; i < m_Waves.Count; i++)
            {
                m_Busy.SetProgress((int)((j++ / (float)m_Waves.Count) * 100.0f));
                m_Busy.SetStatusText("Checking out " + m_Waves[i]);

                try
                {
                    RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(m_Waves[i], true);
                    m_CheckedOut.Add(m_Waves[i]);
                }
                catch (Exception e)
                {
                    sb.Append(e.Message); ;
                }
               
                Application.DoEvents();
            }

            //hide status bar
            m_Busy.Hide();
            //reset form
            m_Busy.SetProgress(0);
            m_Busy.SetStatusText("");

            if (sb.Length != 0)
            {
                if (ErrorManager.HandleQuestion("Rave Encountered the following errors during check out. Do you wish to continue\r\n" +
                                    sb.ToString(), MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.No)
                {
                    try
                    {
                        RaveInstance.RaveAssetManager.WaveChangeList.Revert(false);
                    }
                    catch (Exception e)
                    {
                        ErrorManager.HandleError(e);
                    }

                    return false;
                }
            }
            return true;
        }
        

        private List<string> LoadWaveList(string m_InputFile)
        {
            List<string> files = new List<string>();
            FileStream fs = null;
            StreamReader sr = null;

            try
            {
                fs = new FileStream(m_InputFile, FileMode.Create);
                sr = new StreamReader(fs);
                
                while (!sr.EndOfStream)
                {
                    files.Add(sr.ReadLine());
                }
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
            }
            finally
            {
                fs.Close();
                sr.Close();
            }       

            return files;
        }

        #endregion


    }
}
