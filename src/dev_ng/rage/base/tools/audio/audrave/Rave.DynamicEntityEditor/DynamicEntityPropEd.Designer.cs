using System.Security.AccessControl;

namespace Rave.DynamicEntityEditor
{
    partial class DynamicEntityPropEd
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblObject = new System.Windows.Forms.Label();
			this.lblField = new System.Windows.Forms.Label();
			this.cbObject = new System.Windows.Forms.ComboBox();
			this.cbField = new System.Windows.Forms.ComboBox();
			this.lblPath = new System.Windows.Forms.Label();
			this.cbPath1 = new System.Windows.Forms.ComboBox();
			this.cbPath2 = new System.Windows.Forms.ComboBox();
			this.cbPath3 = new System.Windows.Forms.ComboBox();
			this.cbPath4 = new System.Windows.Forms.ComboBox();
			this.cbPath5 = new System.Windows.Forms.ComboBox();
			this.cbPath6 = new System.Windows.Forms.ComboBox();
			this.cbSoundSetField = new System.Windows.Forms.TextBox();
			this.soundSetLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lblObject
			// 
			this.lblObject.AutoSize = true;
			this.lblObject.Location = new System.Drawing.Point(8, 6);
			this.lblObject.Name = "lblObject";
			this.lblObject.Size = new System.Drawing.Size(72, 13);
			this.lblObject.TabIndex = 0;
			this.lblObject.Text = "Game Object:";
			// 
			// lblField
			// 
			this.lblField.AutoSize = true;
			this.lblField.Location = new System.Drawing.Point(8, 38);
			this.lblField.Name = "lblField";
			this.lblField.Size = new System.Drawing.Size(97, 13);
			this.lblField.TabIndex = 1;
			this.lblField.Text = "Game Object Field:";
			// 
			// cbObject
			// 
			this.cbObject.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cbObject.FormattingEnabled = true;
			this.cbObject.Location = new System.Drawing.Point(112, 3);
			this.cbObject.MaximumSize = new System.Drawing.Size(300, 0);
			this.cbObject.MinimumSize = new System.Drawing.Size(50, 0);
			this.cbObject.Name = "cbObject";
			this.cbObject.Size = new System.Drawing.Size(144, 21);
			this.cbObject.TabIndex = 2;
			this.cbObject.TextChanged += new System.EventHandler(this.cbObject_OnTextChanged);
			// 
			// cbField
			// 
			this.cbField.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cbField.FormattingEnabled = true;
			this.cbField.Location = new System.Drawing.Point(112, 35);
			this.cbField.MaximumSize = new System.Drawing.Size(300, 0);
			this.cbField.MinimumSize = new System.Drawing.Size(50, 0);
			this.cbField.Name = "cbField";
			this.cbField.Size = new System.Drawing.Size(144, 21);
			this.cbField.TabIndex = 3;
			this.cbField.TextChanged += new System.EventHandler(this.cbField_OnTextChanged);
			// 
			// lblPath
			// 
			this.lblPath.AutoSize = true;
			this.lblPath.Location = new System.Drawing.Point(8, 106);
			this.lblPath.Name = "lblPath";
			this.lblPath.Size = new System.Drawing.Size(32, 13);
			this.lblPath.TabIndex = 4;
			this.lblPath.Text = "Path:";
			// 
			// cbPath1
			// 
			this.cbPath1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cbPath1.FormattingEnabled = true;
			this.cbPath1.Location = new System.Drawing.Point(112, 103);
			this.cbPath1.MaximumSize = new System.Drawing.Size(300, 0);
			this.cbPath1.MinimumSize = new System.Drawing.Size(50, 0);
			this.cbPath1.Name = "cbPath1";
			this.cbPath1.Size = new System.Drawing.Size(144, 21);
			this.cbPath1.TabIndex = 5;
			this.cbPath1.TextChanged += new System.EventHandler(this.cbPath1_OnTextChanged);
			// 
			// cbPath2
			// 
			this.cbPath2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cbPath2.FormattingEnabled = true;
			this.cbPath2.Location = new System.Drawing.Point(112, 130);
			this.cbPath2.MaximumSize = new System.Drawing.Size(300, 0);
			this.cbPath2.MinimumSize = new System.Drawing.Size(50, 0);
			this.cbPath2.Name = "cbPath2";
			this.cbPath2.Size = new System.Drawing.Size(144, 21);
			this.cbPath2.TabIndex = 6;
			this.cbPath2.TextChanged += new System.EventHandler(this.cbPath2_OnTextChanged);
			// 
			// cbPath3
			// 
			this.cbPath3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cbPath3.FormattingEnabled = true;
			this.cbPath3.Location = new System.Drawing.Point(112, 157);
			this.cbPath3.MaximumSize = new System.Drawing.Size(300, 0);
			this.cbPath3.MinimumSize = new System.Drawing.Size(50, 0);
			this.cbPath3.Name = "cbPath3";
			this.cbPath3.Size = new System.Drawing.Size(144, 21);
			this.cbPath3.TabIndex = 7;
			this.cbPath3.TextChanged += new System.EventHandler(this.cbPath3_OnTextChanged);
			// 
			// cbPath4
			// 
			this.cbPath4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cbPath4.FormattingEnabled = true;
			this.cbPath4.Location = new System.Drawing.Point(112, 184);
			this.cbPath4.MaximumSize = new System.Drawing.Size(300, 0);
			this.cbPath4.MinimumSize = new System.Drawing.Size(50, 0);
			this.cbPath4.Name = "cbPath4";
			this.cbPath4.Size = new System.Drawing.Size(144, 21);
			this.cbPath4.TabIndex = 8;
			this.cbPath4.TextChanged += new System.EventHandler(this.cbPath4_OnTextChanged);
			// 
			// cbPath5
			// 
			this.cbPath5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cbPath5.FormattingEnabled = true;
			this.cbPath5.Location = new System.Drawing.Point(112, 211);
			this.cbPath5.MaximumSize = new System.Drawing.Size(300, 0);
			this.cbPath5.MinimumSize = new System.Drawing.Size(50, 0);
			this.cbPath5.Name = "cbPath5";
			this.cbPath5.Size = new System.Drawing.Size(144, 21);
			this.cbPath5.TabIndex = 9;
			this.cbPath5.TextChanged += new System.EventHandler(this.cbPath5_OnTextChanged);
			// 
			// cbPath6
			// 
			this.cbPath6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cbPath6.FormattingEnabled = true;
			this.cbPath6.Location = new System.Drawing.Point(112, 238);
			this.cbPath6.MaximumSize = new System.Drawing.Size(300, 0);
			this.cbPath6.MinimumSize = new System.Drawing.Size(50, 0);
			this.cbPath6.Name = "cbPath6";
			this.cbPath6.Size = new System.Drawing.Size(144, 21);
			this.cbPath6.TabIndex = 10;
			this.cbPath6.TextChanged += new System.EventHandler(this.cbPath6_OnTextChanged);
			// 
			// cbSoundSetField
			// 
			this.cbSoundSetField.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cbSoundSetField.Location = new System.Drawing.Point(112, 64);
			this.cbSoundSetField.MaximumSize = new System.Drawing.Size(300, 21);
			this.cbSoundSetField.MinimumSize = new System.Drawing.Size(50, 21);
			this.cbSoundSetField.Name = "cbSoundSetField";
			this.cbSoundSetField.Size = new System.Drawing.Size(144, 20);
			this.cbSoundSetField.TabIndex = 12;
			this.cbSoundSetField.TextChanged += new System.EventHandler(this.cbSoundSet_TextChanged);
			// 
			// soundSetLabel
			// 
			this.soundSetLabel.AutoSize = true;
			this.soundSetLabel.Location = new System.Drawing.Point(8, 67);
			this.soundSetLabel.Name = "soundSetLabel";
			this.soundSetLabel.Size = new System.Drawing.Size(85, 13);
			this.soundSetLabel.TabIndex = 11;
			this.soundSetLabel.Text = "Sound Set Field:";
			// 
			// DynamicEntityPropEd
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.Controls.Add(this.cbSoundSetField);
			this.Controls.Add(this.soundSetLabel);
			this.Controls.Add(this.cbPath6);
			this.Controls.Add(this.cbPath5);
			this.Controls.Add(this.cbPath4);
			this.Controls.Add(this.cbPath3);
			this.Controls.Add(this.cbPath2);
			this.Controls.Add(this.cbPath1);
			this.Controls.Add(this.lblPath);
			this.Controls.Add(this.cbField);
			this.Controls.Add(this.cbObject);
			this.Controls.Add(this.lblField);
			this.Controls.Add(this.lblObject);
			this.Name = "DynamicEntityPropEd";
			this.Size = new System.Drawing.Size(300, 262);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblObject;
        private System.Windows.Forms.Label lblField;
        private System.Windows.Forms.ComboBox cbObject;
        private System.Windows.Forms.ComboBox cbField;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.ComboBox cbPath1;
        private System.Windows.Forms.ComboBox cbPath2;
        private System.Windows.Forms.ComboBox cbPath3;
        private System.Windows.Forms.ComboBox cbPath4;
        private System.Windows.Forms.ComboBox cbPath5;
        private System.Windows.Forms.ComboBox cbPath6;
		private System.Windows.Forms.TextBox cbSoundSetField;
		private System.Windows.Forms.Label soundSetLabel;
    }
}
