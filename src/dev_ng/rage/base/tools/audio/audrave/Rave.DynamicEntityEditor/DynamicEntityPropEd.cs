using System.Linq;
using Rave.TypeDefinitions.Infrastructure.Interfaces;

namespace Rave.DynamicEntityEditor
{
	using System;
	using System.Collections.Generic;
	using System.Windows.Forms;
	using System.Xml;

	using Rave.Instance;
	using Rave.Plugins.Infrastructure.Interfaces;
	using Rave.Types.Infrastructure.Interfaces.Objects;
	using Rave.Utils;

	public partial class DynamicEntityPropEd : UserControl, IRAVEObjectEditorPlugin
	{
		private IObjectInstance m_Object;
		private Dictionary<string, string> m_Objects;

		private string m_DynamicObjectsXml;
		private string m_DynamicPathsXml;

		public string GetName()
		{
			return "Dynamic Entity Editor";
		}

		public bool Init(XmlNode settings)
		{
			this.m_DynamicObjectsXml = this.m_DynamicPathsXml = string.Empty;

			foreach (XmlNode node in settings.ChildNodes)
			{
				switch (node.Name)
				{
					case "DynamicObjectsXml":
						this.m_DynamicObjectsXml = RaveInstance.AssetManager.GetLocalPath(node.InnerText);
						break;
					case "DynamicPathsXml":
						this.m_DynamicPathsXml = RaveInstance.AssetManager.GetLocalPath(node.InnerText);
						break;
				}
			}

			if (!this.Init())
			{
				return false;
			}

			return true;
		}

		private bool Init()
		{
			this.m_Objects = new Dictionary<string, string>();
			var dynamicObjXml = new XmlDocument();
			try
			{
				dynamicObjXml.Load(this.m_DynamicObjectsXml);
			}
			catch (Exception)
			{
				return false;
			}
            SortedSet<string> objectList = new SortedSet<string>();
			foreach (XmlNode node in dynamicObjXml.DocumentElement.ChildNodes)
			{
				var key = "";
				var val = "";

				foreach (XmlNode childNode in node.ChildNodes)
				{
					if (childNode.Name.ToUpper() == "OBJECT")
					{
						key = childNode.Attributes["name"].Value;
					}
					else if (childNode.Name.ToUpper() == "TYPE")
					{
						val = childNode.Attributes["name"].Value;
					}
				}

				if (key == "" ||
					val == "")
				{
					ErrorManager.HandleError("Malformed Dynamic Entity Object XML");
					return false;
				}
				this.m_Objects.Add(key, val);
				
                objectList.Add(key);
			}

            this.cbObject.Items.AddRange(objectList.ToArray());
			var dynamicPathXml = new XmlDocument();
			try
			{
				dynamicPathXml.Load(this.m_DynamicPathsXml);
			}
			catch (Exception)
			{
				return false;
			}

			var paths = new List<string>();
			foreach (XmlNode node in dynamicPathXml.DocumentElement.ChildNodes)
			{
				paths.Add(node.Attributes["Path"].Value);
			}

			this.cbPath1.Items.AddRange(paths.ToArray());
			this.cbPath2.Items.AddRange(paths.ToArray());
			this.cbPath3.Items.AddRange(paths.ToArray());
			this.cbPath4.Items.AddRange(paths.ToArray());
			this.cbPath5.Items.AddRange(paths.ToArray());
			this.cbPath6.Items.AddRange(paths.ToArray());

			return true;
		}

		public DynamicEntityPropEd()
		{
			this.InitializeComponent();
		}

#pragma warning disable 0067
		public event Action<IObjectInstance> OnObjectRefClick;
		public event Action<IObjectInstance> OnObjectEditClick;
		public event Action<string, string> OnWaveRefClick;
		public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067

		public string ObjectType
		{
			get { return "DynamicEntitySound"; }
		}

		private void cbObject_OnTextChanged(object sender, EventArgs e)
		{
			this.cbField.Items.Clear();

			if (this.m_Objects.ContainsKey(this.cbObject.Text))
			{
				var def = RaveInstance.AllTypeDefinitions["GAMEOBJECTS"].FindTypeDefinition(this.m_Objects[this.cbObject.Text]);
				if (def != null)
				{
					foreach (var field in def.Fields)
					{
						this.cbField.Items.Add(field.Name);
					}
				}
			}

			if (this.cbObject.Tag == null)
			{
				XmlNode node = this.m_Object.Node.OwnerDocument.CreateElement("ObjectRefs");
				XmlNode childNode = this.m_Object.Node.OwnerDocument.CreateElement("GameObjectHash");
				node.AppendChild(childNode);
				this.cbObject.Tag = node;
				this.UpdateNodes();
			}
			((XmlNode)this.cbObject.Tag).ChildNodes[0].InnerText = this.cbObject.Text;
			SetPathVisibility();
			this.m_Object.MarkAsDirty();
		}

		private void UpdateNodes()
		{
			var objectRefs = new List<XmlNode>();

			foreach (XmlNode node in this.m_Object.Node.ChildNodes)
			{
				if (node.Name == "ObjectRefs")
				{
					objectRefs.Add(node);
				}
			}

			foreach (var node in objectRefs)
			{
				this.m_Object.Node.RemoveChild(node);
			}
			if (this.cbPath1.Tag != null)
			{
				this.m_Object.Node.AppendChild(this.cbPath1.Tag as XmlNode);
			}
			if (this.cbPath2.Tag != null)
			{
				this.m_Object.Node.AppendChild(this.cbPath2.Tag as XmlNode);
			}
			if (this.cbPath3.Tag != null)
			{
				this.m_Object.Node.AppendChild(this.cbPath3.Tag as XmlNode);
			}
			if (this.cbPath4.Tag != null)
			{
				this.m_Object.Node.AppendChild(this.cbPath4.Tag as XmlNode);
			}
			if (this.cbPath5.Tag != null)
			{
				this.m_Object.Node.AppendChild(this.cbPath5.Tag as XmlNode);
			}
			if (this.cbPath6.Tag != null)
			{
				this.m_Object.Node.AppendChild(this.cbPath6.Tag as XmlNode);
			}




			if (this.cbObject.Tag != null)
			{
				this.m_Object.Node.AppendChild(this.cbObject.Tag as XmlNode);
			}
			if (this.cbField.Tag != null)
			{
				this.m_Object.Node.AppendChild(this.cbField.Tag as XmlNode);
			}

			if (this.cbSoundSetField.Tag != null && !string.IsNullOrEmpty(this.cbSoundSetField.Text))
			{
				this.m_Object.Node.AppendChild(this.cbSoundSetField.Tag as XmlNode);
			}
		}

		private void cbField_OnTextChanged(object sender, EventArgs e)
		{
			if (this.cbField.Tag == null)
			{
				XmlNode node = this.m_Object.Node.OwnerDocument.CreateElement("ObjectRefs");
				XmlNode childNode = this.m_Object.Node.OwnerDocument.CreateElement("GameObjectHash");
				node.AppendChild(childNode);
				this.cbField.Tag = node;


				this.UpdateNodes();
			}
			((XmlNode)this.cbField.Tag).ChildNodes[0].InnerText = this.cbField.Text;
			ComputeSoundSetField();

			this.m_Object.MarkAsDirty();
		}


		private void cbSoundSet_TextChanged(object sender, EventArgs e)
		{
			if (this.cbSoundSetField.Tag == null)
			{
				XmlNode node = this.m_Object.Node.OwnerDocument.CreateElement("ObjectRefs");
				XmlNode childNode = this.m_Object.Node.OwnerDocument.CreateElement("GameObjectHash");
				node.AppendChild(childNode);
				this.cbSoundSetField.Tag = node;
				
			}

			((XmlNode)this.cbSoundSetField.Tag).ChildNodes[0].InnerText = this.cbSoundSetField.Text;
			this.UpdateNodes();
			this.m_Object.MarkAsDirty();
		}

		private void cbPath1_OnTextChanged(object sender, EventArgs e)
		{
			if (this.cbPath1.Tag == null)
			{
				XmlNode node = this.m_Object.Node.OwnerDocument.CreateElement("ObjectRefs");
				XmlNode childNode = this.m_Object.Node.OwnerDocument.CreateElement("GameObjectHash");
				node.AppendChild(childNode);
				this.cbPath1.Tag = node;
				this.UpdateNodes();
			}
			((XmlNode)this.cbPath1.Tag).ChildNodes[0].InnerText = this.cbPath1.Text;
			SetPathVisibility();
			this.m_Object.MarkAsDirty();
		}

		private void cbPath2_OnTextChanged(object sender, EventArgs e)
		{
			if (this.cbPath2.Tag == null)
			{
				XmlNode node = this.m_Object.Node.OwnerDocument.CreateElement("ObjectRefs");
				XmlNode childNode = this.m_Object.Node.OwnerDocument.CreateElement("GameObjectHash");
				node.AppendChild(childNode);
				this.cbPath2.Tag = node;
				this.UpdateNodes();
			}
			((XmlNode)this.cbPath2.Tag).ChildNodes[0].InnerText = this.cbPath2.Text;
			SetPathVisibility();
			this.m_Object.MarkAsDirty();
		}

		private void cbPath3_OnTextChanged(object sender, EventArgs e)
		{
			if (this.cbPath3.Tag == null)
			{
				XmlNode node = this.m_Object.Node.OwnerDocument.CreateElement("ObjectRefs");
				XmlNode childNode = this.m_Object.Node.OwnerDocument.CreateElement("GameObjectHash");
				node.AppendChild(childNode);
				this.cbPath3.Tag = node;
				this.UpdateNodes();
			}
			((XmlNode)this.cbPath3.Tag).ChildNodes[0].InnerText = this.cbPath3.Text;
			SetPathVisibility();
			this.m_Object.MarkAsDirty();
		}

		private void cbPath4_OnTextChanged(object sender, EventArgs e)
		{
			if (this.cbPath4.Tag == null)
			{
				XmlNode node = this.m_Object.Node.OwnerDocument.CreateElement("ObjectRefs");
				XmlNode childNode = this.m_Object.Node.OwnerDocument.CreateElement("GameObjectHash");
				node.AppendChild(childNode);
				this.cbPath4.Tag = node;
				this.UpdateNodes();
			}
			((XmlNode)this.cbPath4.Tag).ChildNodes[0].InnerText = this.cbPath4.Text;
			SetPathVisibility();
			this.m_Object.MarkAsDirty();
		}

		private void cbPath5_OnTextChanged(object sender, EventArgs e)
		{
			if (this.cbPath5.Tag == null)
			{
				XmlNode node = this.m_Object.Node.OwnerDocument.CreateElement("ObjectRefs");
				XmlNode childNode = this.m_Object.Node.OwnerDocument.CreateElement("GameObjectHash");
				node.AppendChild(childNode);
				this.cbPath5.Tag = node;
				this.UpdateNodes();
			}
			((XmlNode)this.cbPath5.Tag).ChildNodes[0].InnerText = this.cbPath5.Text;
			SetPathVisibility();
			this.m_Object.MarkAsDirty();
		}

		private void cbPath6_OnTextChanged(object sender, EventArgs e)
		{
			if (this.cbPath6.Tag == null)
			{
				XmlNode node = this.m_Object.Node.OwnerDocument.CreateElement("ObjectRefs");
				XmlNode childNode = this.m_Object.Node.OwnerDocument.CreateElement("GameObjectHash");
				node.AppendChild(childNode);
				this.cbPath6.Tag = node;
				this.UpdateNodes();
			}
			((XmlNode)this.cbPath6.Tag).ChildNodes[0].InnerText = this.cbPath6.Text;
			SetPathVisibility();
			this.m_Object.MarkAsDirty();
		}

		private void OnSoundStatusChanged()
		{
			if (this.m_Object != null &&
				this.m_Object.ObjectLookup.ContainsKey(this.m_Object.Name))
			{
				this.m_Object = this.m_Object.ObjectLookup[this.m_Object.Name];
				this.EditObject(this.m_Object, Mode.DEFAULT);
			}
		}

		private void SetPathVisibility()
		{
			cbPath1.Visible = true;
			cbPath2.Visible = !string.IsNullOrEmpty(cbPath1.Text);
			cbPath3.Visible = !string.IsNullOrEmpty(cbPath2.Text);
			cbPath4.Visible = !string.IsNullOrEmpty(cbPath3.Text);
			cbPath5.Visible = !string.IsNullOrEmpty(cbPath4.Text);
			cbPath6.Visible = !string.IsNullOrEmpty(cbPath5.Text);
		}


		public void EditObject(IObjectInstance objectInstance, Mode mode)
		{
			this.cbObject.TextChanged -= this.cbObject_OnTextChanged;
			this.cbField.TextChanged -= this.cbField_OnTextChanged;
			this.cbSoundSetField.TextChanged -= cbSoundSet_TextChanged;
			this.cbPath1.TextChanged -= this.cbPath1_OnTextChanged;
			this.cbPath2.TextChanged -= this.cbPath2_OnTextChanged;
			this.cbPath3.TextChanged -= this.cbPath3_OnTextChanged;
			this.cbPath4.TextChanged -= this.cbPath4_OnTextChanged;
			this.cbPath5.TextChanged -= this.cbPath5_OnTextChanged;
			this.cbPath6.TextChanged -= this.cbPath6_OnTextChanged;

			soundSetLabel.Visible = false;
			cbSoundSetField.Visible = false;

			if (this.m_Object != null &&
				this.m_Object.Bank != null)
			{
				this.m_Object.Bank.BankStatusChanged -= OnSoundStatusChanged;
			}

			this.m_Object = objectInstance;

			this.Enabled = !this.m_Object.IsReadOnly;

			var soundNode = this.m_Object.Node;

			var objectRefs = new List<XmlNode>();

			foreach (XmlNode node in soundNode.ChildNodes)
			{
				if (node.Name == "ObjectRefs")
				{
					objectRefs.Add(node);
				}
			}

			var index = objectRefs.Count - 1;
			bool isSoundset = false;
			int soundsetfix = 0;
			this.cbSoundSetField.Tag = null;
			if (objectRefs.Count > 2)
			{
				string objectName = objectRefs[objectRefs.Count - 3].ChildNodes[0].InnerText;
				string fieldName = objectRefs[objectRefs.Count - 2].ChildNodes[0].InnerText;
				if (this.m_Objects.ContainsKey(objectName))
				{
					ITypeDefinition def =
						RaveInstance.AllTypeDefinitions["GAMEOBJECTS"].FindTypeDefinition(this.m_Objects[objectName]);
					if (def != null)
					{
						foreach (IFieldDefinition field in def.Fields)
						{
							if (field.Name != null && field.AllowedType != null &&
								field.Name.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase) &&
								field.AllowedType.Equals("SoundSet", StringComparison.InvariantCultureIgnoreCase))
							{
								soundSetLabel.Visible = true;
								cbSoundSetField.Visible = true;

								this.cbSoundSetField.Tag = objectRefs[index];
								index--;
								soundsetfix = 1;
								isSoundset = true;
							}
						}
					}
				}


			}

			if (!isSoundset)
			{
				soundSetLabel.Visible = false;
				cbSoundSetField.Visible = false;
			}

			SetPathVisibility();
			this.cbField.Tag = objectRefs.Count >= 1 + soundsetfix ? objectRefs[index] : null;
			index--;
			this.cbObject.Tag = objectRefs.Count >= 2 + soundsetfix ? objectRefs[index] : null;
			int pathCount = index;

			this.cbPath1.Tag = pathCount >= 1 ? objectRefs[0] : null;
			index--;
			this.cbPath2.Tag = pathCount >= 2 ? objectRefs[1] : null;
			index--;
			this.cbPath3.Tag = pathCount >= 3 ? objectRefs[2] : null;
			index--;
			this.cbPath4.Tag = pathCount >= 4 ? objectRefs[3] : null;
			index--;
			this.cbPath5.Tag = pathCount >= 5 ? objectRefs[4] : null;
			index--;
			this.cbPath6.Tag = pathCount >= 6 ? objectRefs[5] : null;

			this.cbField.Text = this.cbField.Tag != null ? ((XmlNode)this.cbField.Tag).ChildNodes[0].InnerText : "";
			this.cbObject.Text = this.cbObject.Tag != null ? ((XmlNode)this.cbObject.Tag).ChildNodes[0].InnerText : "";
			if (isSoundset)
			{
				this.cbSoundSetField.Text = this.cbObject.Tag != null
					? ((XmlNode)this.cbSoundSetField.Tag).ChildNodes[0].InnerText
					: "";
			}
			this.cbPath1.Text = this.cbPath1.Tag != null ? ((XmlNode)this.cbPath1.Tag).ChildNodes[0].InnerText : "";
			this.cbPath2.Text = this.cbPath2.Tag != null ? ((XmlNode)this.cbPath2.Tag).ChildNodes[0].InnerText : "";
			this.cbPath3.Text = this.cbPath3.Tag != null ? ((XmlNode)this.cbPath3.Tag).ChildNodes[0].InnerText : "";
			this.cbPath4.Text = this.cbPath4.Tag != null ? ((XmlNode)this.cbPath4.Tag).ChildNodes[0].InnerText : "";
			this.cbPath5.Text = this.cbPath5.Tag != null ? ((XmlNode)this.cbPath5.Tag).ChildNodes[0].InnerText : "";
			this.cbPath6.Text = this.cbPath6.Tag != null ? ((XmlNode)this.cbPath6.Tag).ChildNodes[0].InnerText : "";

			this.m_Object.Bank.BankStatusChanged += OnSoundStatusChanged;
			this.cbObject.TextChanged += this.cbObject_OnTextChanged;
			this.cbSoundSetField.TextChanged += cbSoundSet_TextChanged;
			this.cbField.TextChanged += this.cbField_OnTextChanged;
			this.cbPath1.TextChanged += this.cbPath1_OnTextChanged;
			this.cbPath2.TextChanged += this.cbPath2_OnTextChanged;
			this.cbPath3.TextChanged += this.cbPath3_OnTextChanged;
			this.cbPath4.TextChanged += this.cbPath4_OnTextChanged;
			this.cbPath5.TextChanged += this.cbPath5_OnTextChanged;
			this.cbPath6.TextChanged += this.cbPath6_OnTextChanged;
		}


		public static IEnumerable<IObjectInstance> GetObjects(string type)
		{
			IEnumerable<IObjectInstance> objects;
			if (!string.IsNullOrEmpty(type))
			{

				objects = from table in RaveInstance.ObjectLookupTables
						  from kvp in table.Value
						  where (type.Equals(kvp.Value.TypeName, StringComparison.InvariantCultureIgnoreCase))
						  select kvp.Value;
			}
			else
			{
				objects = from table in RaveInstance.ObjectLookupTables
						  from kvp in table.Value
						  select kvp.Value;
			}
			return objects;
		}

		public void ComputeSoundSetField()
		{

			IFieldDefinition soundsetField = null;
			if (this.m_Objects.ContainsKey(this.cbObject.Text))
			{
				ITypeDefinition def = RaveInstance.AllTypeDefinitions["GAMEOBJECTS"].FindTypeDefinition(this.m_Objects[this.cbObject.Text]);
				if (def != null)
				{
					foreach (IFieldDefinition field in def.Fields)
					{
						if (field is ICompositeFieldDefinition)
						{
							if (field.Name != null && field.Name.Equals(cbField.Text, StringComparison.InvariantCultureIgnoreCase))
							{
								var compositeField = field as ICompositeFieldDefinition;
								foreach (IFieldDefinition fieldDefinition in compositeField.Fields)
								{
									if (fieldDefinition.AllowedType != null &&
									fieldDefinition.AllowedType.Equals("SoundSet", StringComparison.InvariantCultureIgnoreCase))
									{
										soundsetField = field;
									}
								}
							}
						}
						else if (field.Name != null && field.AllowedType != null && field.Name.Equals(cbField.Text, StringComparison.InvariantCultureIgnoreCase) && (field.AllowedType.Equals("SoundSet", StringComparison.InvariantCultureIgnoreCase)))
						{
							soundsetField = field;
						}
					}
				}
			}

			if (soundsetField == null)
			{
				soundSetLabel.Visible = false;
				cbSoundSetField.Visible = false;
				var tag = this.cbSoundSetField.Tag;
				if (tag != null && this.m_Object.Node.ChildNodes.Cast<XmlNode>().Contains((XmlNode)tag))
				{
					this.m_Object.Node.RemoveChild((XmlNode)tag);
					this.m_Object.MarkAsDirty();
				}
				return;
			}

			soundSetLabel.Visible = true;
			cbSoundSetField.Visible = true;
			cbSoundSet_TextChanged(this, null);
			UpdateNodes();
			m_Object.MarkAsDirty();

		}



	}

}