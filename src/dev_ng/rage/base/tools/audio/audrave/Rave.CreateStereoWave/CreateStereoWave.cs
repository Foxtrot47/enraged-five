namespace Rave.CreateStereoWave
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Xml;

    using Rave.Utils.Infrastructure.Interfaces;

    using rageUsefulCSharpToolClasses;

    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    public class CreateStereoWave : IRAVEWaveBrowserPlugin
    {
        private string m_command;
        private string m_delimiter;
        private string m_name;
        private string m_parameters;

        #region IRAVEWaveBrowserPlugin Members

        public string GetName()
        {
            return m_name;
        }

        public bool Init(XmlNode settings)
        {
            m_name = m_command = m_parameters = null;

            foreach (XmlNode setting in settings.ChildNodes)
            {
                switch (setting.Name)
                {
                    case "Name":
                        m_name = setting.InnerText;
                        break;
                    case "Command":
                        m_command = setting.InnerText;
                        break;
                    case "Parameters":
                        m_parameters = setting.InnerText;
                        break;
                }
            }

            //nodes don't exist
            if (m_name == null || m_command == null ||
                m_parameters == null)
            {
                return false;
            }

            return true;
        }

        public void Process(IWaveBrowser waveBrowser, IActionLog actionLog, ArrayList nodesList, IBusy busyFrm)
        {
            var nodes = new List<WaveNode>();

            // process each selected node in the wave browser
            foreach (EditorTreeNode n in nodesList)
            {
                waveBrowser.GetEditorTreeView().GetSelectedWaveNodes(n, false, nodes);
            }

            //ensure only two nodes and that they are both wavenodes
            if (nodes.Count != 2 || nodes[0].GetType() != typeof (WaveNode) ||
                nodes[1].GetType() != typeof (WaveNode))
            {
                ErrorManager.HandleInfo("Two wave nodes need to be in selection");
                return;
            }

            var channel1 = (WaveNode) nodes[0];
            var channel2 = (WaveNode) nodes[1];
            if ((channel1.GetObjectName().EndsWith("LEFT.WAV") && channel2.GetObjectName().EndsWith("RIGHT.WAV")) ||
                (channel2.GetObjectName().EndsWith("LEFT.WAV") && channel1.GetObjectName().EndsWith("RIGHT.WAV")))
            {
                //Check if any of the waves are marked to be added
                var count = 0;
                foreach (WaveNode wave in nodes)
                {
                    if (actionLog.SearchForWaveAdd(wave.GetObjectPath()) != null)
                    {
                        count++;
                    }
                }
                if (count > 0)
                {
                    ErrorManager.HandleInfo(
                        "There is one or more pending add wave action for the selected waves - you must commit before you can edit the wave");
                    return;
                }

                // from the list of selected files, build up the parameter list to the external script
                m_delimiter = ",";
                // set the file list to the first element
                var rootPath = Configuration.PlatformWavePath;
                var fileList = rootPath + (nodes[0] as EditorTreeNode).GetObjectPath();

                // do we have more than 1 item in the list?  append a comma
                if (nodes.Count > 1)
                {
                    fileList += m_delimiter;
                }

                // iterate over the entire list
                for (var i = 1; i < nodes.Count; ++i)
                {
                    if (i >= nodes.Count - 1)
                    {
                        fileList += rootPath + (nodes[i] as EditorTreeNode).GetObjectPath();
                    }
                    else
                    {
                        fileList += (rootPath + (nodes[i] as EditorTreeNode).GetObjectPath() + m_delimiter);
                    }
                }

                fileList = fileList.Replace("\\", "/"); // this will change \\/ -> //
                fileList = fileList.Replace("//", "/"); // this will change // -> / and //// -> //
                fileList = fileList.Replace("//", "/"); // if there are // left they will go to /
                fileList = fileList.Replace('/', '\\');

                var fileName = Path.GetTempFileName();

                var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                var sw = new StreamWriter(fs);
                try
                {
                    sw.WriteLine(fileList);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                }
                finally
                {
                    sw.Close();
                }
                fs.Dispose();

                //set fileList for parameters to be filename
                //quotes added in case of spaces in path
                fileList = "\"" + fileName + "\"";

                rageStatus status;
                new rageExecuteCommand(m_command, m_parameters + fileList, out status);
            }
            else
            {
                ErrorManager.HandleInfo("Selection does not contain a left and right channel");
            }
        }

        #endregion

        public void Shutdown()
        {
            return;
        }
    }
}