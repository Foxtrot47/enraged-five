﻿// -----------------------------------------------------------------------
// <copyright file="GestureEntry.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.GestureExport
{
    using Rave.WaveBrowser.Infrastructure.Nodes;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Gesture entry.
    /// </summary>
    public class GestureEntry
    {
        /// <summary>
        /// Gets or sets the wave name.
        /// </summary>
        public string WaveName { get; set; }

        /// <summary>
        /// Gets or sets the wave asset.
        /// </summary>
        public IAsset WaveAsset { get; set; }

        /// <summary>
        /// Gets or sets the asset.
        /// </summary>
        public IAsset GestureAsset { get; set; }

        /// <summary>
        /// Gets or sets the wave node.
        /// </summary>
        public WaveNode WaveNode { get; set; }

        /// <summary>
        /// Gets or sets the wave path.
        /// </summary>
        public string WavePath { get; set; }

        /// <summary>
        /// Gets or sets the temp wave path.
        /// </summary>
        public string TempWavePath { get; set; }

        /// <summary>
        /// Gets or sets the temp gesture path.
        /// </summary>
        public string TempGesturePath { get; set; }

        /// <summary>
        /// Gets or sets the gesture path.
        /// </summary>
        public string GesturePath { get; set; }

        /// <summary>
        /// Gets the bank node.
        /// </summary>
        public BankNode BankNode
        {
            get
            {
                if (null != this.WaveNode)
                {
                    var currNode = this.WaveNode.Parent;
                    while (null != currNode)
                    {
                        var bankNode = currNode as BankNode;
                        if (null != bankNode)
                        {
                            return bankNode;
                        }

                        currNode = currNode.Parent;
                    }
                }

                return null;
            }
        }
    }
}
