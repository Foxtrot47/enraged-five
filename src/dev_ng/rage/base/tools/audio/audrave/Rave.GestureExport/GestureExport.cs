﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GestureExport.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The gesture export.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.GestureExport
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;
    using System.Xml.Linq;

    using Rave.Instance;
    using Rave.Utils.Infrastructure.Interfaces;
    using Rave.Utils.Popups;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.UserActions;

    using rage;

    using rageUsefulCSharpToolClasses;

    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    /// The gesture export.
    /// </summary>
    public class GestureExport : IRAVEWaveBrowserPlugin
    {
        #region Fields

        /// <summary>
        /// The gesture change list description.
        /// </summary>
        private const string GestureChangeListDescription = "[Rave] Batch Gesture File Change";

        /// <summary>
        /// The action log.
        /// </summary>
        private IActionLog actionLog;

        /// <summary>
        /// The busy.
        /// </summary>
        private IBusy busy;

        /// <summary>
        /// The command.
        /// </summary>
        private string command;

        /// <summary>
        /// The delimiter.
        /// </summary>
        private string delimiter;

        /// <summary>
        /// The name.
        /// </summary>
        private string name;

        /// <summary>
        /// The parameters.
        /// </summary>
        private string parameters;

        /// <summary>
        /// The root.
        /// </summary>
        private string root;

        /// <summary>
        /// The skip flag.
        /// </summary>
        private bool skip;

        /// <summary>
        /// The wave browser.
        /// </summary>
        private IWaveBrowser waveBrowser;

        /// <summary>
        /// The write to file flag.
        /// </summary>
        private bool writeToFile;

        /// <summary>
        /// The gesture entries.
        /// </summary>
        private IList<GestureEntry> gestureEntries;

        /// <summary>
        /// The temp path.
        /// </summary>
        private string tempPath;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Get the plugin name.
        /// </summary>
        /// <returns>
        /// The plugin name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        /// Initialize the plugin.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            foreach (XmlNode setting in settings.ChildNodes)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;
                    case "Command":
                        this.command = setting.InnerText;
                        break;
                    case "Parameters":
                        this.parameters = setting.InnerText;
                        break;
                    case "Delimiter":
                        this.delimiter = setting.InnerText;
                        if (this.delimiter == string.Empty)
                        {
                            this.delimiter = " ";
                        }

                        break;
                    case "WriteToFile":
                        this.writeToFile = setting.InnerText == "True";
                        break;
                    case "SkipCustomMetadata":
                        this.skip = setting.InnerText == "True";
                        break;
                }
            }

            // nodes don't exist
            return this.name != string.Empty && this.command != string.Empty;
        }

        /// <summary>
        /// Process the selected wave(s).
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        public void Process(IWaveBrowser waveBrowser, IActionLog actionLog, ArrayList treeNodes, IBusy busyFrm)
        {
            this.busy = busyFrm;
            this.waveBrowser = waveBrowser;
            this.actionLog = actionLog;
            this.gestureEntries = new List<GestureEntry>();
            this.root = Configuration.PlatformWavePath;

            this.tempPath = Path.Combine(Path.GetTempPath(), "GestureData");

            if (!Directory.Exists(this.tempPath))
            {
                Directory.CreateDirectory(this.tempPath);
            }

            if (treeNodes.Count == 0)
            {
                ErrorManager.HandleInfo("No nodes selected");
            }

            if (!PreCheck(treeNodes))
            {
                return;
            }

            this.ProcessWaves(treeNodes);
        }

        /// <summary>
        /// Shutdown the plugin.
        /// </summary>
        public void Shutdown()
        {
        }

        /// <summary>
        /// Override ToString to return plugin name.
        /// </summary>
        /// <returns>
        /// The plugin name <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.name;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Run pre-process checks.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private static bool PreCheck(IEnumerable nodes)
        {
            // Check for any non-reaper related pending actions
            if (null != RaveInstance.RaveAssetManager.WaveChangeList && !RaveInstance.RaveAssetManager.WaveChangeList.Description.Equals(GestureChangeListDescription))
            {
                ErrorManager.HandleInfo("Please commit pending non-gesture related wave changes before using this plugin");
                return false;
            }

            var packNames = new HashSet<string>();

            foreach (EditorTreeNode node in nodes)
            {
                var pack = EditorTreeNode.FindParentPackNode(node).GetObjectName();
                packNames.Add(pack);
            }

            // Get any pending wave files associated with bank(s) we're using
            var projectSettings = Configuration.ProjectSettings;
            var pendingWaveFiles = new HashSet<string>();

            foreach (PlatformSetting platform in projectSettings.GetPlatformSettings())
            {
                foreach (var packName in packNames)
                {
                    var packFileName = packName + ".XML";
                    var pendingWavesPath = Path.Combine(platform.BuildInfo, "PendingWaves");
                    var pendingWaveFile = Path.Combine(pendingWavesPath, packFileName);
                    var depotPath = RaveInstance.AssetManager.GetDepotPath(pendingWaveFile);

                    pendingWaveFiles.Add(depotPath);
                }
            }

            // Check that there aren't any pending wave lists in a pending change list
            foreach (var pendingWaveFile in pendingWaveFiles)
            {
                if (RaveInstance.AssetManager.ExistsInPendingChangeList(pendingWaveFile))
                {
                    var pendingWaveFileName = Path.GetFileNameWithoutExtension(pendingWaveFile);
                    ErrorManager.HandleInfo("Pending wave list for " + pendingWaveFileName + " pack must be checked in/reverted before using this plugin");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Set the file name value.
        /// </summary>
        /// <param name="gestureEntry">
        /// The gesture entry.
        /// </param>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        private static void SetFileNameValue(GestureEntry gestureEntry, string fileName)
        {
            var doc = XDocument.Load(gestureEntry.TempGesturePath);
            if (null != doc.Root)
            {
                var fileNameElem = doc.Descendants("FileName").FirstOrDefault();
                if (null == fileNameElem)
                {
                    throw new Exception("Failed to find FileName element in file: " + gestureEntry.TempGesturePath);
                }

                fileNameElem.SetValue(fileName);
                doc.Save(gestureEntry.TempGesturePath);
            }
        }

        /// <summary>
        /// Process the waves.
        /// </summary>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private void ProcessWaves(IEnumerable treeNodes)
        {
            // process each selected node in the wave browser
            var tmpWaveNodes = new List<WaveNode>();
            foreach (EditorTreeNode node in treeNodes)
            {
                this.waveBrowser.GetEditorTreeView().GetSelectedWaveNodes(node, this.skip, tmpWaveNodes);
            }

            foreach (WaveNode waveNode in tmpWaveNodes)
            {
                var ext = Path.GetExtension(waveNode.GetObjectName());

                if (null == ext || ext.ToLower() != ".wav")
                {
                    throw new Exception("Invalid file name - expecting \".wav\" extension: " + waveNode.GetObjectPath());
                }

                var gestureDirPath = Path.GetDirectoryName(waveNode.GetObjectPath());
                var gestureFileName = Path.GetFileNameWithoutExtension(waveNode.GetObjectPath()) + ".gesture";
                var gesturePath = Path.Combine(Configuration.PlatformWavePath, gestureDirPath, gestureFileName);
                var wavePath = Path.Combine(Configuration.PlatformWavePath, waveNode.GetObjectPath());
                var tempWavePath = Path.Combine(this.tempPath, waveNode.GetObjectPath());
                var tempGesturePath = Path.Combine(Path.GetDirectoryName(tempWavePath), gestureFileName);

                var gestureEntry = new GestureEntry
                {
                    WaveNode = waveNode,
                    WaveName = waveNode.Text,
                    WavePath = wavePath,
                    GesturePath = gesturePath,
                    TempWavePath = tempWavePath,
                    TempGesturePath = tempGesturePath
                };
                this.gestureEntries.Add(gestureEntry);

                // Create temp directory if it doesn't already exist
                var tempGestureDirPath = Path.GetDirectoryName(tempWavePath);
                if (!Directory.Exists(tempGestureDirPath))
                {
                    Directory.CreateDirectory(tempGestureDirPath);
                }
            }

            // Check for errors
            if (this.gestureEntries == null || !this.gestureEntries.Any())
            {
                this.busy.Close();
                return;
            }

            if (!this.GetLatestData())
            {
                return;
            }

            if (!this.InitChangeList())
            {
                return;
            }

            if (!this.InitTempFiles())
            {
                return;
            }

            if (!this.CheckoutWaveFiles())
            {
                return;
            }

            if (!this.CheckoutGestureFiles())
            {
                return;
            }

            var tempWavePaths = this.gestureEntries.Select(gestureEntry => gestureEntry.TempWavePath).ToList();

            var fileList = string.Join(this.delimiter, tempWavePaths);
            var fileName = Path.GetTempFileName();

            if (this.writeToFile)
            {
                var stream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                var writer = new StreamWriter(stream);
                try
                {
                    writer.WriteLine(fileList);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                }
                finally
                {
                    writer.Close();
                }

                stream.Dispose();

                // set fileList for parameters to be filename
                // quotes added in case of spaces in path
                fileList = "\"" + fileName + "\"";
            }

            rageStatus status;
            var cmd = new rageExecuteCommand(this.command, this.parameters + fileList, out status);

            if (this.writeToFile && File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            if (status.Success())
            {
                const string CheckInMsg = "The Gesture Export plugin successfully completed. Would you like to keep your pending changes? "
                                          + "These must be submitted manually in RAVE. Select 'No' to revert your changes.";
                if (ErrorManager.HandleQuestion(CheckInMsg, MessageBoxButtons.YesNo, DialogResult.Yes)
                    == DialogResult.Yes)
                {
                    this.AddPendingChanges();
                }
                else
                {
                    this.RevertChanges();
                }
            }
            else
            {
                this.RevertChanges();
                var errorMsg = "The external script failed - please check the log file (" + cmd.GetLogFilename()
                                + ") for details.";
                ErrorManager.HandleError(errorMsg);
            }
        }

        /// <summary>
        /// Initialize the change list.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool InitChangeList()
        {
            // Create wave change list if it doesn't already exist
            if (RaveInstance.RaveAssetManager.WaveChangeList == null)
            {
                try
                {
                    RaveInstance.RaveAssetManager.WaveChangeList =
                        RaveInstance.AssetManager.CreateChangeList(GestureChangeListDescription);
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Get the latest data.
        /// </summary>
        private bool GetLatestData()
        {
            foreach (var gestureEntry in this.gestureEntries)
            {
                // Get latest wave file
                RaveInstance.AssetManager.GetLatest(gestureEntry.WavePath, false);

                // Get latest gesture file
                if (RaveInstance.AssetManager.ExistsAsAsset(gestureEntry.GesturePath))
                {
                    RaveInstance.AssetManager.GetLatest(gestureEntry.GesturePath, false);
                }
            }

            return true;
        }

        /// <summary>
        /// Initialize the temp files.
        /// </summary>
        private bool InitTempFiles()
        {
            foreach (var gestureEntry in this.gestureEntries)
            {
                if (!string.IsNullOrEmpty(gestureEntry.GesturePath) && File.Exists(gestureEntry.GesturePath))
                {
                    // Copy gesture file to temp directory (only if we haven't already got the associated 
                    // wave file or the gesture file checked out, or if temp gesture file doesn't yet exist)
                    if ((!RaveInstance.AssetManager.IsCheckedOut(gestureEntry.GesturePath) &&
                        (!RaveInstance.AssetManager.ExistsAsAsset(gestureEntry.GesturePath) ||
                        !RaveInstance.AssetManager.IsCheckedOut(gestureEntry.WavePath)))
                        || !File.Exists(gestureEntry.TempGesturePath))
                    {
                        if (File.Exists(gestureEntry.TempGesturePath))
                        {
                            new FileInfo(gestureEntry.TempGesturePath).IsReadOnly = false;
                        }

                        File.Copy(gestureEntry.GesturePath, gestureEntry.TempGesturePath, true);
                        new FileInfo(gestureEntry.TempGesturePath).IsReadOnly = false;
                    }
                }

                // Copy wave file to temp directory (only if we haven't already got it 
                // checked out or if temp file doesn't yet exist)
                if (!RaveInstance.AssetManager.IsCheckedOut(gestureEntry.WavePath)
                    || !File.Exists(gestureEntry.TempWavePath))
                {
                    if (File.Exists(gestureEntry.TempWavePath))
                    {
                        new FileInfo(gestureEntry.TempWavePath).IsReadOnly = false;
                    }

                    File.Copy(gestureEntry.WavePath, gestureEntry.TempWavePath, true);
                    new FileInfo(gestureEntry.TempWavePath).IsReadOnly = false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checkout the wave files.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool CheckoutWaveFiles()
        {
            // Reset form
            this.busy.SetProgress(0);
            this.busy.SetStatusText("Getting latest Wave Files");

            // Get latest wave files
            var j = 0;
            this.busy.Show();

            foreach (var gestureEntry in this.gestureEntries)
            {
                this.busy.SetProgress((int)((j++ / (float)this.gestureEntries.Count) * 100.0f));
                this.busy.SetStatusText("Getting latest " + gestureEntry.WavePath);

                try
                {
                    // Check that wave isn't being edited elsewhere
                    gestureEntry.WaveAsset = RaveInstance.AssetManager.GetCheckedOutAsset(gestureEntry.WavePath);

                    if (null != gestureEntry.WaveAsset)
                    {
                        if (gestureEntry.WaveAsset.ChangeList != RaveInstance.RaveAssetManager.WaveChangeList)
                        {
                            this.busy.Close();
                            ErrorManager.HandleInfo(
                                "Asset is already checked out in another change list; unable to edit: "
                                + gestureEntry.WavePath);
                            return false;
                        }
                    }
                    else
                    {
                        gestureEntry.WaveAsset = RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(gestureEntry.WavePath, true);
                    }
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError("Failed to get latest " + gestureEntry.WavePath + ": " + e.Message);
                    return false;
                }

                Application.DoEvents();
            }

            return true;
        }

        /// <summary>
        /// Checkout the gesture files.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool CheckoutGestureFiles()
        {
            var errors = new StringBuilder();

            // Reset form
            this.busy.SetProgress(0);
            this.busy.SetStatusText("Checking out Gesture Files");

            // Check out all selected gesture files (if they exist)
            var j = 0;
            this.busy.Show();

            foreach (var gestureEntry in this.gestureEntries)
            {
                this.busy.SetProgress((int)((j++ / (float)this.gestureEntries.Count) * 100.0f));
                this.busy.SetStatusText("Checking out " + gestureEntry.GesturePath);

                try
                {
                    // Get latest and checkout gesture file if it exists
                    if (RaveInstance.AssetManager.ExistsAsAsset(gestureEntry.GesturePath))
                    {
                        gestureEntry.GestureAsset = RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(gestureEntry.GesturePath, true);

                        // Also need to update the FileName element in the gesture file
                        // so that the gesture tool updates the temp file not the source file
                        SetFileNameValue(gestureEntry, gestureEntry.TempGesturePath);
                    }
                }
                catch (Exception e)
                {
                    errors.AppendLine(e.Message);
                }

                Application.DoEvents();
            }

            this.busy.Hide();

            // Display details of any errors
            if (errors.Length != 0)
            {
                if (
                    ErrorManager.HandleQuestion(
                        "Rave Encountered the following errors during check out. Do you wish to continue\r\n" + errors,
                        MessageBoxButtons.YesNo,
                        DialogResult.No) == DialogResult.No)
                {
                    this.RevertChanges();
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Add pending changes.
        /// </summary>
        private void AddPendingChanges()
        {
            this.busy.SetUnknownDuration(true);
            this.busy.SetStatusText("Adding pending changes...");

            try
            {
                foreach (var gestureEntry in this.gestureEntries)
                {
                    // Restore original FileName value in gesture file
                    SetFileNameValue(gestureEntry, gestureEntry.GesturePath);

                    // Add change actions to pending waves
                    var wavePathNoRoot = gestureEntry.WavePath.ToUpper().Replace(this.root.ToUpper(), string.Empty);

                    foreach (PlatformSetting platformSetting in Configuration.ActivePlatformSettings)
                    {
                        this.waveBrowser.GetPendingWaveLists()
                            .GetPendingWaveList(platformSetting.PlatformTag)
                            .RecordModifyWave(wavePathNoRoot);
                        Application.DoEvents();
                    }

                    var fileName = Path.GetFileNameWithoutExtension(gestureEntry.WavePath);

                    // Add pending edit wave action
                    var actionparams = new ActionParams(
                        this.waveBrowser,
                        gestureEntry.BankNode,
                        gestureEntry.WaveNode,
                        gestureEntry.TempWavePath,
                        null,
                        this.actionLog,
                        fileName,
                        gestureEntry.WaveNode.GetPlatforms(),
                        null,
                        null,
                        new List<string> { ".gesture" });

                    var action = this.waveBrowser.CreateAction(ActionType.ChangeWaveFile, actionparams);
                    if (action.Action())
                    {
                        this.waveBrowser.GetActionLog().AddAction(action);
                    }
                }
            }
            catch (Exception e)
            {
                ErrorManager.HandleError("Failed to add pending changes: " + e.Message);
                this.RevertChanges();
            }
        }

        /// <summary>
        /// Revert wave files.
        /// </summary>
        private void RevertChanges()
        {
            this.busy.Show();
            this.busy.SetProgress(-1);
            this.busy.SetStatusText("Reverting Gesture Changes");
            Application.DoEvents();

            try
            {
                foreach (var gestureEntry in this.gestureEntries)
                {
                    if (null != gestureEntry.GestureAsset)
                    {
                        gestureEntry.GestureAsset.Revert();
                    }

                    if (null != gestureEntry.WaveAsset)
                    {
                        gestureEntry.WaveAsset.Revert();
                    }
                }

                if (null != RaveInstance.RaveAssetManager.WaveChangeList && !RaveInstance.RaveAssetManager.WaveChangeList.Assets.Any())
                {
                    RaveInstance.RaveAssetManager.WaveChangeList.Delete();
                    RaveInstance.RaveAssetManager.WaveChangeList = null;
                }
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
            }

            this.busy.Close();
        }

        #endregion
    }
}