using System;
using System.ComponentModel;

namespace MathOperationEditor
{
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public interface IMathOpEditorViewModel : INotifyPropertyChanged,
                                              IDisposable
    {
        bool IsCheckedOut { get; set; }

        bool CanAddOperation { get; }

        BindingList<MathOp> Items { get; }

        void EditObject(IObjectInstance objectInstance);

        void Add(MathOpType type);

        void Remove(MathOp item);

        int GetIndex(MathOp item);
    }
}