﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using WPFToolLib.Extensions;

namespace MathOperationEditor
{
    /// <summary>
    ///   Interaction logic for VariableOrConstantElementEditor.xaml
    /// </summary>
    public partial class VariableOrConstantElementEditor
    {
        private readonly Brush m_defaultConstantBackground;

        public VariableOrConstantElementEditor()
        {
            InitializeComponent();
            m_defaultConstantBackground = constant.Background;
        }

        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            var mathOpElement = (sender as FrameworkElement).FindDataContext<MathOpElement>();
            if (mathOpElement != null)
            {
                string val = null;
                if (editVariable.IsChecked.Value)
                {
                    val = variable.Text;
                }
                else if (editConstant.IsChecked.Value)
                {
                    val = constant.Text;
                }

                if (string.IsNullOrEmpty(val))
                {
                    mathOpElement.ResetValue();
                }
                else
                {
                    mathOpElement.Value = val;
                }
                mathOpElement.IsEditing = false;
            }
            e.Handled = true;
        }

        private void OnConstantLoaded(object sender, RoutedEventArgs e)
        {
            if (MathOpElement.IsDefaultValue(constant.Text))
            {
                constant.Text = null;
            }
            e.Handled = true;
        }

        private void OnConstantTextChanged(object sender, TextChangedEventArgs e)
        {
            var text = constant.Text;
            if (string.IsNullOrEmpty(text))
            {
                constant.Background = m_defaultConstantBackground;
                constant.ToolTip = null;
            }
            else
            {
                float temp;
                if (float.TryParse(constant.Text, out temp))
                {
                    constant.Background = m_defaultConstantBackground;
                    constant.ToolTip = null;
                }
                else
                {
                    constant.Background = Brushes.Tomato;
                    constant.ToolTip = "You can only enter a real number here";
                }
            }
            e.Handled = true;
        }

        private void OnConstantIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (constant.IsVisible)
            {
                var element = constant.FindDataContext<MathOpElement>();
                if (element != null)
                {
                    constant.Text = element.IsConstant ? element.Value : string.Empty;
                }
            }
        }

        private void VariableOrConstantElementEditor_OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (constant.IsVisible)
            {
                constant.Focus();
            }
            else if (variable.IsVisible)
            {
                variable.Focus();
            }
        }
    }
}