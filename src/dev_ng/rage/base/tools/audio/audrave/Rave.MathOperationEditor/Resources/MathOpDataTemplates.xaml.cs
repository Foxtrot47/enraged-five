namespace MathOperationEditor.Resources
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Media;

    using WPFToolLib.Extensions;

    public partial class MathOpDataTemplates
    {
        public MathOpDataTemplates()
        {
            InitializeComponent();
        }

        private void OnResultClicked(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                var mathOp = button.FindDataContext<MathOp>();
                if (mathOp != null)
                {
                    if (mathOp.Result.CanBeConstant ||
                        mathOp.Result.VariableList.AllItems.Any())
                    {
                        mathOp.Result.IsEditing = true;
                    }
                    e.Handled = true;
                }
            }
        }

        private void OnOperationClicked(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                var mathOp = button.FindDataContext<MathOp>();
                if (mathOp != null)
                {
                    mathOp.IsEditingOperation = true;
                    e.Handled = true;
                }
            }
        }

        private void OnParam1Clicked(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                var mathOp = button.FindDataContext<MathOp>();
                if (mathOp != null)
                {
                    if (mathOp.Param1.CanBeConstant ||
                        mathOp.Param1.VariableList.AllItems.Any())
                    {
                        mathOp.Param1.IsEditing = true;
                    }
                    e.Handled = true;
                }
            }
        }

        private void OnParam2Clicked(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                var mathOp = button.FindDataContext<MathOp>();
                if (mathOp != null)
                {
                    if (mathOp.Param2.CanBeConstant ||
                        mathOp.Param2.VariableList.AllItems.Any())
                    {
                        mathOp.Param2.IsEditing = true;
                    }
                    e.Handled = true;
                }
            }
        }

        private void OnParam3Clicked(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                var mathOp = button.FindDataContext<MathOp>();
                if (mathOp != null)
                {
                    if (mathOp.Param3.CanBeConstant ||
                        mathOp.Param3.VariableList.AllItems.Any())
                    {
                        mathOp.Param3.IsEditing = true;
                    }
                    e.Handled = true;
                }
            }
        }

        private void OnVariableOrConstantPopupOpened(object sender, EventArgs e)
        {
            var popup = sender as Popup;
            var variableEditor = popup.Child;
            variableEditor.Focus();
        }

        private void OnVariableOrConstantPopupClosed(object sender, EventArgs e)
        {
            var mathOpElement = (sender as FrameworkElement).FindDataContext<MathOpElement>();
            if (mathOpElement != null)
            {
                mathOpElement.IsEditing = false;
            }
        }

        private void OnOperationPopupClosed(object sender, EventArgs e)
        {
            var mathOpElement = (sender as FrameworkElement).FindDataContext<MathOp>();
            if (mathOpElement != null)
            {
                mathOpElement.IsEditingOperation = false;
            }
        }
    }
}