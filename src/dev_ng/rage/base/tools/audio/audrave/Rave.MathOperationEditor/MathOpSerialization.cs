// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MathOpSerialization.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The math op serialization.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MathOperationEditor
{
    using System.Collections.Generic;
    using System.Xml;

    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.VariableList;

    using rage.ToolLib;

    using WPFToolLib.List;

    /// <summary>
    /// The math op serialization.
    /// </summary>
    public class MathOpSerialization : IMathOpSerialization
    {
        #region Constants

        /// <summary>
        /// The mat h_ operatio n_ prefix.
        /// </summary>
        private const string MATH_OPERATION_PREFIX = "MATH_OPERATION_";

        #endregion

        #region Fields

        /// <summary>
        /// The m_is loading.
        /// </summary>
        private bool m_isLoading;

        #endregion

        #region Enums

        /// <summary>
        /// The parse tokens.
        /// </summary>
        private enum ParseTokens
        {
            /// <summary>
            /// The operation.
            /// </summary>
            Operation, 

            /// <summary>
            /// The param 1.
            /// </summary>
            Param1, 

            /// <summary>
            /// The param 2.
            /// </summary>
            Param2, 

            /// <summary>
            /// The param 3.
            /// </summary>
            Param3, 

            /// <summary>
            /// The result.
            /// </summary>
            Result, 

            /// <summary>
            /// The value.
            /// </summary>
            Value, 

            /// <summary>
            /// The variable.
            /// </summary>
            Variable, 

            /// <summary>
            /// The expose as.
            /// </summary>
            ExposeAs
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the param variables.
        /// </summary>
        public IVariableList ParamVariables { get; private set; }

        /// <summary>
        /// Gets the result variables.
        /// </summary>
        public IVariableList ResultVariables { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get max num operations.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetMaxNumOperations(IObjectInstance instance)
        {
            if (instance != null)
            {
                var typeDef = instance.TypeDefinitions.FindTypeDefinition(instance.TypeName);
                if (typeDef != null)
                {
                    var operationsFieldDef =
                        typeDef.FindFieldDefinition(ParseTokens.Operation.ToString()) as ICompositeFieldDefinition;
                    if (operationsFieldDef != null)
                    {
                        return operationsFieldDef.MaxOccurs;
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// The load.
        /// </summary>
        /// <param name="operations">
        /// The operations.
        /// </param>
        /// <param name="instance">
        /// The instance.
        /// </param>
        public void Load(IListEditor<MathOp> operations, IObjectInstance instance)
        {
            if (instance != null)
            {
                try
                {
                    this.m_isLoading = true;

                    this.LoadVariables(instance);

                    var operationNodes = GetOperationNodes(instance);
                    foreach (var operationNode in operationNodes)
                    {
                        operations.Add(this.DeserializeMathOp(operationNode));
                    }
                }
                finally
                {
                    this.m_isLoading = false;
                }
            }
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <param name="operations">
        /// The operations.
        /// </param>
        public void Save(IObjectInstance instance, IListEditor<MathOp> operations)
        {
            if (!this.m_isLoading && instance != null && instance.Node != null)
            {
                var operationNodes = GetOperationNodes(instance);
                foreach (var node in operationNodes)
                {
                    instance.Node.RemoveChild(node);
                }

                foreach (var mathOp in operations.Items)
                {
                    instance.Node.AppendChild(SerializeMathOp(mathOp, instance.Node.OwnerDocument));
                }

                instance.MarkAsDirty();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get expose as attribute value.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetExposeAsAttributeValue(XmlNode node)
        {
            if (node.Attributes != null)
            {
                if (node.Attributes[ParseTokens.ExposeAs.ToString()] != null)
                {
                    return node.Attributes[ParseTokens.ExposeAs.ToString()].Value;
                }
            }

            return null;
        }

        /// <summary>
        /// The get op type.
        /// </summary>
        /// <param name="op">
        /// The op.
        /// </param>
        /// <returns>
        /// The <see cref="MathOpType"/>.
        /// </returns>
        private static MathOpType GetOpType(string op)
        {
            var mathOpType = Enum<MathOpTypes>.Parse(op.Replace(MATH_OPERATION_PREFIX, string.Empty));
            return MathOpType.GetOp(mathOpType);
        }

        /// <summary>
        /// The get operation nodes.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private static IEnumerable<XmlNode> GetOperationNodes(IObjectInstance instance)
        {
            var operationNodes = new List<XmlNode>();
            foreach (XmlNode childNode in instance.Node.ChildNodes)
            {
                try
                {
                    var token = Enum<ParseTokens>.TryParse(childNode.Name, false, ParseTokens.ExposeAs);
                    if (token == ParseTokens.Operation)
                    {
                        operationNodes.Add(childNode);
                    }
                }
                catch
                {
                }
            }

            return operationNodes;
        }

        /// <summary>
        /// The get param value.
        /// </summary>
        /// <param name="variable">
        /// The variable.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetParamValue(string variable, float value)
        {
            return !string.IsNullOrEmpty(variable) ? variable : value.ToString();
        }

        /// <summary>
        /// The get variable content.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetVariableContent(MathOpElement element)
        {
            if (!element.IsConstant)
            {
                if (!MathOpElement.IsDefaultValue(element.Value))
                {
                    return element.Value;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// The parse operation components.
        /// </summary>
        /// <param name="operationNode">
        /// The operation node.
        /// </param>
        /// <param name="op">
        /// The op.
        /// </param>
        /// <param name="opExposedAs">
        /// The op exposed as.
        /// </param>
        /// <param name="param1Node">
        /// The param 1 node.
        /// </param>
        /// <param name="param2Node">
        /// The param 2 node.
        /// </param>
        /// <param name="param3Node">
        /// The param 3 node.
        /// </param>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <param name="resultExposedAs">
        /// The result exposed as.
        /// </param>
        private static void ParseOperationComponents(
            XmlNode operationNode, 
            out string op, 
            out string opExposedAs, 
            out XmlNode param1Node, 
            out XmlNode param2Node, 
            out XmlNode param3Node, 
            out string result, 
            out string resultExposedAs)
        {
            op = null;
            opExposedAs = null;

            param1Node = null;
            param2Node = null;
            param3Node = null;

            result = null;
            resultExposedAs = null;

            foreach (XmlNode child in operationNode.ChildNodes)
            {
                try
                {
                    var token = Enum<ParseTokens>.Parse(child.Name);
                    switch (token)
                    {
                        case ParseTokens.Operation:
                            {
                                op = child.InnerText;
                                opExposedAs = GetExposeAsAttributeValue(child);
                                break;
                            }

                        case ParseTokens.Param1:
                            {
                                param1Node = child;
                                break;
                            }

                        case ParseTokens.Param2:
                            {
                                param2Node = child;
                                break;
                            }

                        case ParseTokens.Param3:
                            {
                                param3Node = child;
                                break;
                            }

                        case ParseTokens.Result:
                            {
                                result = child.InnerText;
                                resultExposedAs = GetExposeAsAttributeValue(child);
                                break;
                            }
                    }
                }
                catch
                {
                    break;
                }
            }
        }

        /// <summary>
        /// The parse param components.
        /// </summary>
        /// <param name="paramNode">
        /// The param node.
        /// </param>
        /// <param name="paramExposeAs">
        /// The param expose as.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="valueExposeAs">
        /// The value expose as.
        /// </param>
        /// <param name="variable">
        /// The variable.
        /// </param>
        /// <param name="variableExposeAs">
        /// The variable expose as.
        /// </param>
        private static void ParseParamComponents(
            XmlNode paramNode, 
            out string paramExposeAs, 
            out float value, 
            out string valueExposeAs, 
            out string variable, 
            out string variableExposeAs)
        {
            paramExposeAs = GetExposeAsAttributeValue(paramNode);

            value = 0;
            valueExposeAs = null;

            variable = null;
            variableExposeAs = null;

            foreach (XmlNode child in paramNode.ChildNodes)
            {
                try
                {
                    var token = Enum<ParseTokens>.Parse(child.Name);
                    switch (token)
                    {
                        case ParseTokens.Value:
                            {
                                value = float.Parse(child.InnerText);
                                valueExposeAs = GetExposeAsAttributeValue(child);
                                break;
                            }

                        case ParseTokens.Variable:
                            {
                                variable = child.InnerText;
                                variableExposeAs = GetExposeAsAttributeValue(child);
                                break;
                            }
                    }
                }
                catch
                {
                    break;
                }
            }
        }

        /// <summary>
        /// The serialize math op.
        /// </summary>
        /// <param name="mathOp">
        /// The math op.
        /// </param>
        /// <param name="ownerDocument">
        /// The owner document.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        private static XmlNode SerializeMathOp(MathOp mathOp, XmlDocument ownerDocument)
        {
            var operationNode = ownerDocument.CreateElement(ParseTokens.Operation.ToString());
            if (!string.IsNullOrEmpty(mathOp.ExposeAs))
            {
                operationNode.SetAttribute(ParseTokens.ExposeAs.ToString(), mathOp.ExposeAs);
            }

            var opNode = ownerDocument.CreateElement(ParseTokens.Operation.ToString());
            if (!string.IsNullOrEmpty(mathOp.OperationExposeAs))
            {
                opNode.SetAttribute(ParseTokens.ExposeAs.ToString(), mathOp.OperationExposeAs);
            }

            var opName = string.Format("{0}{1}", MATH_OPERATION_PREFIX, mathOp.Operation.Type.ToString().ToUpper());
            opNode.AppendChild(ownerDocument.CreateTextNode(opName));
            operationNode.AppendChild(opNode);

            var param1Node = SerializeParam(ParseTokens.Param1, mathOp.Param1, ownerDocument);
            operationNode.AppendChild(param1Node);

            var param2Node = SerializeParam(ParseTokens.Param2, mathOp.Param2, ownerDocument);
            operationNode.AppendChild(param2Node);

            var param3Node = SerializeParam(ParseTokens.Param3, mathOp.Param3, ownerDocument);
            operationNode.AppendChild(param3Node);

            var resultNode = ownerDocument.CreateElement(ParseTokens.Result.ToString());
            if (!string.IsNullOrEmpty(mathOp.Result.ExposeAs))
            {
                resultNode.SetAttribute(ParseTokens.ExposeAs.ToString(), mathOp.Result.ExposeAs);
            }

            resultNode.AppendChild(ownerDocument.CreateTextNode(GetVariableContent(mathOp.Result)));
            operationNode.AppendChild(resultNode);

            return operationNode;
        }

        /// <summary>
        /// The serialize param.
        /// </summary>
        /// <param name="paramToken">
        /// The param token.
        /// </param>
        /// <param name="paramElement">
        /// The param element.
        /// </param>
        /// <param name="ownerDocument">
        /// The owner document.
        /// </param>
        /// <returns>
        /// The <see cref="XmlNode"/>.
        /// </returns>
        private static XmlNode SerializeParam(
            ParseTokens paramToken, MathOpElement paramElement, XmlDocument ownerDocument)
        {
            var paramNode = ownerDocument.CreateElement(paramToken.ToString().ToLower());
            if (!string.IsNullOrEmpty(paramElement.ExposeAs))
            {
                paramNode.SetAttribute(ParseTokens.ExposeAs.ToString(), paramElement.ExposeAs);
            }

            var valueNode = ownerDocument.CreateElement(ParseTokens.Value.ToString());
            if (!string.IsNullOrEmpty(paramElement.ValueExposeAs))
            {
                valueNode.SetAttribute(ParseTokens.ExposeAs.ToString(), paramElement.ValueExposeAs);
            }

            var valueContent = paramElement.IsConstant ? paramElement.Value : "0";
            valueNode.AppendChild(ownerDocument.CreateTextNode(valueContent));
            paramNode.AppendChild(valueNode);

            var variableNode = ownerDocument.CreateElement(ParseTokens.Variable.ToString());
            if (!string.IsNullOrEmpty(paramElement.VariableExposeAs))
            {
                variableNode.SetAttribute(ParseTokens.ExposeAs.ToString(), paramElement.VariableExposeAs);
            }

            variableNode.AppendChild(ownerDocument.CreateTextNode(GetVariableContent(paramElement)));
            paramNode.AppendChild(variableNode);

            return paramNode;
        }

        /// <summary>
        /// The deserialize math op.
        /// </summary>
        /// <param name="operationNode">
        /// The operation node.
        /// </param>
        /// <returns>
        /// The <see cref="MathOp"/>.
        /// </returns>
        private MathOp DeserializeMathOp(XmlNode operationNode)
        {
            string op, opExposeAs;
            XmlNode param1Node, param2Node, param3Node;
            string result, resultExposeAs;
            ParseOperationComponents(
                operationNode, 
                out op, 
                out opExposeAs, 
                out param1Node, 
                out param2Node, 
                out param3Node, 
                out result, 
                out resultExposeAs);

            var opType = GetOpType(op);
            var mathOp = new MathOp(this.ResultVariables, this.ParamVariables)
                             {
                                 Operation = opType, 
                                 OperationExposeAs = opExposeAs, 
                                 ExposeAs =
                                     GetExposeAsAttributeValue(
                                         operationNode)
                             };
            if (!string.IsNullOrEmpty(result))
            {
                mathOp.Result.Value = result;
            }

            mathOp.Result.ExposeAs = resultExposeAs;

            float value;
            string variable;
            string exposeAs, valueExposeAs, variableExposeAs;
            ParseParamComponents(
                param1Node, out exposeAs, out value, out valueExposeAs, out variable, out variableExposeAs);
            mathOp.Param1.Value = GetParamValue(variable, value);
            mathOp.Param1.ExposeAs = exposeAs;
            mathOp.Param1.ValueExposeAs = valueExposeAs;
            mathOp.Param1.VariableExposeAs = variableExposeAs;

            ParseParamComponents(
                param2Node, out exposeAs, out value, out valueExposeAs, out variable, out variableExposeAs);
            mathOp.Param2.Value = GetParamValue(variable, value);
            mathOp.Param2.ExposeAs = exposeAs;
            mathOp.Param2.ValueExposeAs = valueExposeAs;
            mathOp.Param2.VariableExposeAs = variableExposeAs;

            ParseParamComponents(
                param3Node, out exposeAs, out value, out valueExposeAs, out variable, out variableExposeAs);
            mathOp.Param3.Value = GetParamValue(variable, value);
            mathOp.Param3.ExposeAs = exposeAs;
            mathOp.Param3.ValueExposeAs = valueExposeAs;
            mathOp.Param3.VariableExposeAs = variableExposeAs;

            return mathOp;
        }

        /// <summary>
        /// The load variables.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        private void LoadVariables(IObjectInstance instance)
        {
            this.ParamVariables = instance.GetVariableList(null, null, VariableListType.Input);
            this.ResultVariables = instance.GetVariableList(null, null, VariableListType.Output);
        }

        #endregion
    }
}