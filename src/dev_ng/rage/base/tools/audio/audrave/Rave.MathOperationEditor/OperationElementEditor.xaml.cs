﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using WPFToolLib.Extensions;

namespace MathOperationEditor
{
    /// <summary>
    ///   Interaction logic for OperationElementEditor.xaml
    /// </summary>
    public partial class OperationElementEditor
    {
        public OperationElementEditor()
        {
            InitializeComponent();
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var mathOp = (sender as FrameworkElement).FindDataContext<MathOp>();
            if (mathOp != null)
            {
                if (operations.SelectedItem != null)
                {
                    var val = operations.SelectedItem as string;
                    var name = val.Split(' ').First();
                    var opType = (MathOpTypes) Enum.Parse(typeof (MathOpTypes), name, true);
                    if (mathOp.Operation.Type != opType)
                    {
                        mathOp.Operation = MathOpType.GetOp(opType);
                        mathOp.Param1.ResetValue();
                        mathOp.Param2.ResetValue();
                        mathOp.Param3.ResetValue();
                        return;
                    }
                    mathOp.IsEditingOperation = false;
                }
            }
            e.Handled = true;
        }
    }
}