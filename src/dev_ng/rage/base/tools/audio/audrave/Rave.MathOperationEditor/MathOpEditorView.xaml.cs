﻿namespace MathOperationEditor
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;
    using System.Xml;

    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    using WPFToolLib.Extensions;
    using WPFToolLib.List;

    /// <summary>
    ///   Interaction logic for MathOpEditorView.xaml
    /// </summary>
    public partial class MathOpEditorView : IRAVEObjectEditorPlugin
    {
        private readonly RoutedEventHandler m_selectionChangedHandler;
        private ListBoxDragDrop<MathOp> m_dragDrop;
        private IMathOpEditorViewModel m_viewModel;

        private IObjectInstance m_objectToEdit;

        private bool loaded;

        public MathOpEditorView()
        {
            InitializeComponent();
            m_selectionChangedHandler = new RoutedEventHandler(OnSelectionChanged);
        }

#pragma warning disable 0067
        public event Action<IObjectInstance> OnObjectEditClick;
        public event Action<IObjectInstance> OnObjectRefClick;
        public event Action<string, string> OnWaveRefClick;
        public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067

        public string ObjectType
        {
            get { return "MathOperationSound"; }
        }

        public bool Init(XmlNode settings)
        {
            return true;
        }

        public string GetName()
        {
            return "Math Operation Editor";
        }

        public void EditObject(IObjectInstance objectInstance, Mode mode)
        {
            OnUnloaded(null, null);
            m_objectToEdit = objectInstance;
        }

        public void Dispose()
        {
            // Do nothing
        }

        private void OnAddClicked(object sender, RoutedEventArgs e)
        {
            var menuItem = e.OriginalSource as MenuItem;
            if (menuItem != null)
            {
                var val = menuItem.Header as string;
                if (!string.IsNullOrEmpty(val))
                {
                    var name = val.Split(' ').First();
                    try
                    {
                        var opType = (MathOpTypes) Enum.Parse(typeof (MathOpTypes), name, true);
                        m_viewModel.Add(MathOpType.GetOp(opType));
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void OnRemoveClicked(object sender, RoutedEventArgs e)
        {
            var selected = operationsList.SelectedItem as MathOp;
            if (selected != null)
            {
                m_viewModel.Remove(selected);
            }
            e.Handled = true;
        }

        private void OnListBoxItemMouseEnter(object sender, MouseEventArgs e)
        {
            var originalSource = e.OriginalSource as DependencyObject;
            var listBoxItem = originalSource.FindAncestor<ListBoxItem>();
            if (listBoxItem != null)
            {
                var item = operationsList.ItemContainerGenerator.ItemFromContainer(listBoxItem) as MathOp;
                operationsList.SelectedIndex = m_viewModel.GetIndex(item);
            }
            e.Handled = true;
        }

        private void OnSelectionChanged(object sender, RoutedEventArgs e)
        {
            var operationElementEditor =
                (e.OriginalSource as DependencyObject).FindLogicalAncestor<OperationElementEditor>();
            if (operationElementEditor != null)
            {
                var selectedIndex = operationsList.SelectedIndex;
                if (selectedIndex >= 0)
                {
                    var item = operationsList.ItemContainerGenerator.ContainerFromIndex(selectedIndex) as ListBoxItem;
                    if (item != null)
                    {
                        var templateSelector = item.ContentTemplateSelector;
                        item.ContentTemplateSelector = null;
                        item.ContentTemplateSelector = templateSelector;
                    }
                }
                e.Handled = true;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.loaded)
            {
                // Can't assume loaded event is only fired once:
                // http://stackoverflow.com/questions/2460704/wpf-user-control-loading-twice
                return;
            }

            if (m_objectToEdit != null)
            {
                var model = new MathOpEditor();
                DataContext = m_viewModel = new MathOpEditorViewModel(model);
                m_dragDrop = new ListBoxDragDrop<MathOp>(model, operationsList);
                AddHandler(Selector.SelectionChangedEvent, m_selectionChangedHandler);
                m_viewModel.EditObject(m_objectToEdit);
                this.loaded = true;
            }
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            if (m_viewModel != null)
            {
                m_dragDrop.Dispose();
                m_dragDrop = null;

                DataContext = null;
                m_viewModel.Dispose();
                m_viewModel = null;

                RemoveHandler(Selector.SelectionChangedEvent, m_selectionChangedHandler);
            }

            if (e != null)
            {
                e.Handled = true;
            }

            this.loaded = false;
        }
    }
}