using System;
using WPFToolLib.List;

namespace MathOperationEditor
{
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public interface IMathOpEditor : IListEditor<MathOp>
    {
        bool IsCheckedOut { get; set; }

        event Action IsCheckedOutChanged;

        bool CanAddOperation { get; }

        event Action CanAddOperationChanged;

        void EditObject(IObjectInstance objectInstance);

        void Add(MathOpType type);
    }
}