namespace MathOperationEditor
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;

    using Rave.Types.Infrastructure.Interfaces.VariableList;

    using rage.ToolLib;

    using WPFToolLib.Extensions;

    public class MathOpElement : INotifyPropertyChanged
    {
        private const string DEFAULT_VALUE = "[...]";

        private bool m_isEditing;

        private string m_value;

        private string m_editedValue;

        public MathOpElement(bool canBeConstant, IVariableList variableList)
        {
            CanBeConstant = canBeConstant;
            VariableList = variableList;
            VariableListContextMenu = variableList.GetContextMenu_Wpf(new RoutedEventHandler(this.VariableListMenuItem_Clicked));
            ValueContextMenu = variableList.GetContextMenu_Wpf(new RoutedEventHandler(this.ValueMenuItem_Clicked));
            m_value = DEFAULT_VALUE;
        }

        public delegate void MenuItemClicked(object sender, RoutedEventArgs args);

        public bool CanBeConstant { get; private set; }

        public IVariableList VariableList { get; private set; }

        public ContextMenu VariableListContextMenu { get; private set; }

        public ContextMenu ValueContextMenu { get; private set; }

        public bool IsEditing
        {
            get { return m_isEditing; }
            set
            {
                m_isEditing = value;
                PropertyChanged.Raise(this, "IsEditing");
            }
        }

        public bool IsConstant
        {
            get
            {
                float temp;
                return CanBeConstant && float.TryParse(m_value, out temp);
            }
        }

        public string Value
        {
            get
            {
                return m_value;
            }

            set
            {
                m_value = value;
                ValueChanged.Raise();
                PropertyChanged.Raise(this, "Value");
                PropertyChanged.Raise(this, "IsPermittedValue");
            }
        }

        // Holds temp value while being edited
        public string EditedValue
        {
            get
            {
                return m_editedValue;
            }

            set
            {
                m_editedValue = value;
                PropertyChanged.Raise(this, "EditedValue");
            }
        }

        public string ExposeAs { get; set; }
        public string ValueExposeAs { get; set; }
        public string VariableExposeAs { get; set; }

        public bool IsPermittedValue
        {
            get
            {
                return IsConstant || IsDefaultValue(Value) || VariableList.FlatListLabels.Contains(Value);
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public event Action ValueChanged;

        public void ResetValue()
        {
            Value = DEFAULT_VALUE;
        }

        public static bool IsDefaultValue(string value)
        {
            return value == DEFAULT_VALUE;
        }

        private void VariableListMenuItem_Clicked(object obj, RoutedEventArgs e)
        {
            var item = obj as MenuItem;
            if (null != item)
            {
                var tag = item.Tag as string;
                if (null != tag)
                {
                    this.EditedValue = tag;
                }
            }
        }

        private void ValueMenuItem_Clicked(object obj, RoutedEventArgs e)
        {
            var item = obj as MenuItem;
            if (null != item)
            {
                var tag = item.Tag as string;
                if (null != tag)
                {
                    this.Value = tag;
                    this.EditedValue = tag;
                }
            }
        }
    }
}