﻿<UserControl x:Class="MathOperationEditor.MathOpEditorView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:MathOperationEditor="clr-namespace:MathOperationEditor"
             Unloaded="OnUnloaded"
             Loaded="UserControl_Loaded">
	<UserControl.Resources>
		<ResourceDictionary>
			<ResourceDictionary.MergedDictionaries>
				<ResourceDictionary Source="./Resources/MathOpDataTemplates.xaml"/>
			</ResourceDictionary.MergedDictionaries>

			<Style x:Key="ListBoxItemStyle"
			       TargetType="{x:Type ListBoxItem}">
				<Setter Property="Template">
					<Setter.Value>
						<ControlTemplate TargetType="{x:Type ListBoxItem}">
							<Border x:Name="Bd"
							        Background="Transparent"
							        BorderBrush="Black"
							        BorderThickness="1,1,1,1"
							        Padding="{TemplateBinding Padding}"
							        CornerRadius="5,5,5,5"
							        MouseEnter="OnListBoxItemMouseEnter">
								<DockPanel LastChildFill="True">
									<Button DockPanel.Dock="Right"
									        x:Name="remove"
									        Visibility="Hidden"
									        Margin="5,1,1,1"
									        ToolTip="Delete"
									        VerticalAlignment="Top"
									        BorderBrush="Black"
									        BorderThickness="1,1,1,1"
									        Background="Transparent"
									        Click="OnRemoveClicked">
										<Grid>
											<Rectangle Width="10"
											           Height="3"
											           Fill="Red"
											           HorizontalAlignment="Center"
											           VerticalAlignment="Center">
												<Rectangle.LayoutTransform>
													<RotateTransform CenterX="0"
													                 CenterY="0"
													                 Angle="45"/>
												</Rectangle.LayoutTransform>
											</Rectangle>
											<Rectangle Width="10"
											           Height="3"
											           Fill="Red"
											           HorizontalAlignment="Center"
											           VerticalAlignment="Center">
												<Rectangle.LayoutTransform>
													<RotateTransform CenterX="0"
													                 CenterY="0"
													                 Angle="-45"/>
												</Rectangle.LayoutTransform>
											</Rectangle>
										</Grid>
									</Button>
									<ContentPresenter DockPanel.Dock="Left"
									                  VerticalAlignment="Center"/>
								</DockPanel>
							</Border>
							<ControlTemplate.Triggers>
								<Trigger Property="IsSelected"
								         Value="True">
									<Setter Property="Background"
									        TargetName="Bd">
										<Setter.Value>
											<LinearGradientBrush>
												<GradientStop Offset="0"
												              Color="WhiteSmoke"/>
												<GradientStop Offset="0.6"
												              Color="WhiteSmoke"/>
												<GradientStop Offset="1"
												              Color="LightSkyBlue"/>
											</LinearGradientBrush>
										</Setter.Value>
									</Setter>
								</Trigger>
								<Trigger Property="IsEnabled"
								         Value="False">
									<Setter Property="Background"
									        TargetName="Bd"
									        Value="LightGray"/>
								</Trigger>
								<MultiTrigger>
									<MultiTrigger.Conditions>
										<Condition Property="IsEnabled"
										           Value="True"/>
										<Condition Property="IsSelected"
										           Value="True"/>
									</MultiTrigger.Conditions>
									<Setter Property="Visibility"
									        TargetName="remove"
									        Value="Visible"/>
								</MultiTrigger>
							</ControlTemplate.Triggers>
						</ControlTemplate>
					</Setter.Value>
				</Setter>
			</Style>

			<Style x:Key="ListBoxStyle"
			       TargetType="{x:Type ListBox}">
				<Style.Triggers>
					<DataTrigger Binding="{Binding Path=IsCheckedOut}"
					             Value="False">
						<Setter Property="SelectedIndex"
						        Value="-1"/>
						<Setter Property="IsEnabled"
						        Value="False"/>
					</DataTrigger>
				</Style.Triggers>
				<Setter Property="SelectionMode"
				        Value="Single"/>
				<Setter Property="ItemContainerStyle"
				        Value="{StaticResource ListBoxItemStyle}"/>
				<Setter Property="ItemsSource"
				        Value="{Binding Path=Items}"/>
				<Setter Property="ItemTemplateSelector"
				        Value="{StaticResource MathOpTemplateSelector}"/>
			</Style>

			<Style x:Key="MenuItemStyle"
			       TargetType="{x:Type MenuItem}">
				<Style.Triggers>
					<DataTrigger Binding="{Binding Path=CanAddOperation}"
					             Value="False">
						<Setter Property="IsEnabled"
						        Value="False"/>
						<Setter Property="ToolTip"
						        Value="You cannot add any more operations as the maximum limit has been reached"/>
					</DataTrigger>
				</Style.Triggers>
			</Style>

			<Style x:Key="ContextMenuStyle"
			       TargetType="{x:Type ContextMenu}">
				<Style.Triggers>
					<DataTrigger Binding="{Binding Path=CanAddOperation}"
					             Value="False">
						<Setter Property="ToolTip"
						        Value="You cannot add any more operations as the maximum limit has been reached"/>
					</DataTrigger>
				</Style.Triggers>
			</Style>
		</ResourceDictionary>
	</UserControl.Resources>

	<ScrollViewer HorizontalScrollBarVisibility="Auto"
	              VerticalScrollBarVisibility="Auto">
		<ListBox x:Name="operationsList"
		         Grid.Row="1"
		         Grid.Column="0"
		         Grid.ColumnSpan="2"
		         Style="{StaticResource ListBoxStyle}">
			<ListBox.ContextMenu>
				<ContextMenu Style="{StaticResource ContextMenuStyle}">
					<MenuItem Header="Add an Arithmetic operation"
					          Style="{StaticResource MenuItemStyle}"
					          ItemsSource="{Binding Source={x:Static MathOperationEditor:MathOpType.ArithmeticOperations}}"
					          Click="OnAddClicked">
						<MenuItem.Icon>
							<Grid>
								<Rectangle Width="10"
								           Height="3"
								           Fill="CornflowerBlue"
								           HorizontalAlignment="Center"
								           VerticalAlignment="Center"/>
								<Rectangle Width="3"
								           Height="10"
								           Fill="CornflowerBlue"
								           HorizontalAlignment="Center"
								           VerticalAlignment="Center"/>
							</Grid>
						</MenuItem.Icon>
					</MenuItem>
					<MenuItem Header="Add a Simple Function operation"
					          Style="{StaticResource MenuItemStyle}"
					          ItemsSource="{Binding Source={x:Static MathOperationEditor:MathOpType.SimpleFunctionOperations}}"
					          Click="OnAddClicked">
						<MenuItem.Icon>
							<Grid>
								<Rectangle Width="10"
								           Height="3"
								           Fill="Green"
								           HorizontalAlignment="Center"
								           VerticalAlignment="Center"/>
								<Rectangle Width="3"
								           Height="10"
								           Fill="Green"
								           HorizontalAlignment="Center"
								           VerticalAlignment="Center"/>
							</Grid>
						</MenuItem.Icon>
					</MenuItem>
					<MenuItem Header="Add a Complex Function operation"
					          Style="{StaticResource MenuItemStyle}"
					          ItemsSource="{Binding Source={x:Static MathOperationEditor:MathOpType.ComplexFunctionOperations}}"
					          Click="OnAddClicked">
						<MenuItem.Icon>
							<Grid>
								<Rectangle Width="10"
								           Height="3"
								           Fill="OrangeRed"
								           HorizontalAlignment="Center"
								           VerticalAlignment="Center"/>
								<Rectangle Width="3"
								           Height="10"
								           Fill="OrangeRed"
								           HorizontalAlignment="Center"
								           VerticalAlignment="Center"/>
							</Grid>
						</MenuItem.Icon>
					</MenuItem>
				</ContextMenu>
			</ListBox.ContextMenu>
		</ListBox>
	</ScrollViewer>
</UserControl>