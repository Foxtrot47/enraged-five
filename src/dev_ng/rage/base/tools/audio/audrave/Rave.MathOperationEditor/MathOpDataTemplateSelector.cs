using System.Windows;
using System.Windows.Controls;

namespace MathOperationEditor
{
    public class MathOpDataTemplateSelector : DataTemplateSelector
    {
        public MathOpDataTemplateSelector()
        {
            FunctionStyleTemplates = new DataTemplate[4];
            OperatorStyleTemplates = new DataTemplate[2];
        }

        public DataTemplate FST0
        {
            set { FunctionStyleTemplates[0] = value; }
        }

        public DataTemplate FST1
        {
            set { FunctionStyleTemplates[1] = value; }
        }

        public DataTemplate FST2
        {
            set { FunctionStyleTemplates[2] = value; }
        }

        public DataTemplate FST3
        {
            set { FunctionStyleTemplates[3] = value; }
        }

        public DataTemplate OST1
        {
            set { OperatorStyleTemplates[0] = value; }
        }

        public DataTemplate OST2
        {
            set { OperatorStyleTemplates[1] = value; }
        }

        private DataTemplate[] FunctionStyleTemplates { get; set; }
        private DataTemplate[] OperatorStyleTemplates { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var mathOp = item as MathOp;
            if (mathOp != null)
            {
                return mathOp.IsFunctionSyntax
                           ? FunctionStyleTemplates[mathOp.NumParams]
                           : OperatorStyleTemplates[mathOp.NumParams - 1];
            }
            return null;
        }
    }
}