using System;
using System.ComponentModel;
using WPFToolLib.Extensions;

namespace MathOperationEditor
{
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public class MathOpEditorViewModel : IMathOpEditorViewModel
    {
        private IMathOpEditor m_model;

        public MathOpEditorViewModel(IMathOpEditor model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            m_model.CanAddOperationChanged += OnCanAddOperationChanged;
            m_model.IsCheckedOutChanged += OnIsCheckedOutChanged;
        }

        #region IMathOpEditorViewModel Members

        public bool IsCheckedOut
        {
            get { return m_model.IsCheckedOut; }
            set { m_model.IsCheckedOut = value; }
        }

        public bool CanAddOperation
        {
            get { return m_model.CanAddOperation; }
        }

        public BindingList<MathOp> Items
        {
            get { return m_model.Items; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        public void EditObject(IObjectInstance objectInstance)
        {
            m_model.EditObject(objectInstance);
        }

        public void Add(MathOpType type)
        {
            m_model.Add(type);
        }

        public void Remove(MathOp item)
        {
            m_model.Remove(item);
        }

        public int GetIndex(MathOp item)
        {
            return m_model.GetIndex(item);
        }

        public void Dispose()
        {
            if (m_model != null)
            {
                m_model.CanAddOperationChanged -= OnCanAddOperationChanged;
                m_model.IsCheckedOutChanged -= OnIsCheckedOutChanged;
                m_model = null;
            }
        }

        #endregion

        private void OnCanAddOperationChanged()
        {
            PropertyChanged.Raise(this, "CanAddOperation");
        }

        private void OnIsCheckedOutChanged()
        {
            PropertyChanged.Raise(this, "IsCheckedOut");
        }
    }
}