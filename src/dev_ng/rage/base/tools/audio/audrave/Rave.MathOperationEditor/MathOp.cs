// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MathOp.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The math op.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MathOperationEditor
{
    using System;
    using System.ComponentModel;

    using Rave.Types.Infrastructure.Interfaces.VariableList;

    using rage.ToolLib;

    using WPFToolLib.Extensions;

    /// <summary>
    /// The math op.
    /// </summary>
    public class MathOp : INotifyPropertyChanged
    {
        #region Fields

        /// <summary>
        /// The m_is editing operation.
        /// </summary>
        private bool m_isEditingOperation;

        /// <summary>
        /// The m_operation.
        /// </summary>
        private MathOpType m_operation;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MathOp"/> class.
        /// </summary>
        /// <param name="resultVariables">
        /// The result variables.
        /// </param>
        /// <param name="paramVariables">
        /// The param variables.
        /// </param>
        public MathOp(IVariableList resultVariables, IVariableList paramVariables)
        {
            this.Result = new MathOpElement(false, resultVariables);
            this.Result.ValueChanged += this.OnValueChanged;
            this.Param1 = new MathOpElement(true, paramVariables);
            this.Param1.ValueChanged += this.OnValueChanged;
            this.Param2 = new MathOpElement(true, paramVariables);
            this.Param2.ValueChanged += this.OnValueChanged;
            this.Param3 = new MathOpElement(true, paramVariables);
            this.Param3.ValueChanged += this.OnValueChanged;
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The dirty.
        /// </summary>
        public event Action Dirty;

        /// <summary>
        /// The property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the expose as.
        /// </summary>
        public string ExposeAs { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is editing operation.
        /// </summary>
        public bool IsEditingOperation
        {
            get
            {
                return this.m_isEditingOperation;
            }

            set
            {
                if (this.m_isEditingOperation != value)
                {
                    this.m_isEditingOperation = value;
                    this.PropertyChanged.Raise(this, "IsEditingOperation");
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether is function syntax.
        /// </summary>
        public bool IsFunctionSyntax
        {
            get
            {
                return this.Operation.IsFunctionSyntax;
            }
        }

        /// <summary>
        /// Gets the num params.
        /// </summary>
        public uint NumParams
        {
            get
            {
                return this.Operation.NumParams;
            }
        }

        /// <summary>
        /// Gets or sets the operation.
        /// </summary>
        public MathOpType Operation
        {
            get
            {
                return this.m_operation;
            }

            set
            {
                if (this.m_operation != value)
                {
                    this.m_operation = value;
                    this.Dirty.Raise();
                }
            }
        }

        /// <summary>
        /// Gets or sets the operation expose as.
        /// </summary>
        public string OperationExposeAs { get; set; }

        /// <summary>
        /// Gets the param 1.
        /// </summary>
        public MathOpElement Param1 { get; private set; }

        /// <summary>
        /// Gets the param 2.
        /// </summary>
        public MathOpElement Param2 { get; private set; }

        /// <summary>
        /// Gets the param 3.
        /// </summary>
        public MathOpElement Param3 { get; private set; }

        /// <summary>
        /// Gets the result.
        /// </summary>
        public MathOpElement Result { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// The on value changed.
        /// </summary>
        private void OnValueChanged()
        {
            this.Dirty.Raise();
        }

        #endregion
    }
}