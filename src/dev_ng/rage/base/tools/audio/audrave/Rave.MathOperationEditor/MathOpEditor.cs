// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MathOpEditor.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The math op editor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MathOperationEditor
{
    using System;
    using System.ComponentModel;
    using System.Linq;

    using Rave.Types.Infrastructure.Interfaces.Objects;

    using rage.ToolLib;

    using WPFToolLib.List;

    /// <summary>
    /// The math op editor.
    /// </summary>
    public class MathOpEditor : IMathOpEditor
    {
        #region Fields

        /// <summary>
        /// The m_list editor.
        /// </summary>
        private readonly IListEditor<MathOp> m_listEditor;

        /// <summary>
        /// The m_serialization.
        /// </summary>
        private readonly IMathOpSerialization m_serialization;

        /// <summary>
        /// The m_can add operation.
        /// </summary>
        private bool m_canAddOperation;

        /// <summary>
        /// The m_instance.
        /// </summary>
        private IObjectInstance m_instance;

        /// <summary>
        /// The m_is checked out.
        /// </summary>
        private bool m_isCheckedOut;

        /// <summary>
        /// The m_max num operations.
        /// </summary>
        private int m_maxNumOperations;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MathOpEditor"/> class.
        /// </summary>
        public MathOpEditor()
        {
            this.m_listEditor = new ListEditor<MathOp>();
            this.m_serialization = new MathOpSerialization();
            this.Reset();
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The can add operation changed.
        /// </summary>
        public event Action CanAddOperationChanged;

        /// <summary>
        /// The is checked out changed.
        /// </summary>
        public event Action IsCheckedOutChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether can add operation.
        /// </summary>
        public bool CanAddOperation
        {
            get
            {
                return this.m_canAddOperation;
            }

            private set
            {
                if (this.m_canAddOperation != value)
                {
                    this.m_canAddOperation = value;
                    this.CanAddOperationChanged.Raise();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is checked out.
        /// </summary>
        public bool IsCheckedOut
        {
            get
            {
                return this.m_isCheckedOut;
            }

            set
            {
                if (this.m_isCheckedOut != value)
                {
                    this.m_isCheckedOut = value;
                    this.IsCheckedOutChanged.Raise();
                }
            }
        }

        /// <summary>
        /// Gets the items.
        /// </summary>
        public BindingList<MathOp> Items
        {
            get
            {
                return this.m_listEditor.Items;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        public void Add(MathOpType type)
        {
            this.Add(
                new MathOp(this.m_serialization.ResultVariables, this.m_serialization.ParamVariables)
                    {
                        Operation = type
                    });
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void Add(MathOp item)
        {
            if (this.m_listEditor.Items.Count < this.m_maxNumOperations)
            {
                this.m_listEditor.Add(item);
                item.Dirty += this.OnDirty;
                this.OnDirty();
            }

            this.CanAddOperation = this.m_listEditor.Items.Count < this.m_maxNumOperations;
        }

        /// <summary>
        /// The edit object.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        public void EditObject(IObjectInstance objectInstance)
        {
            this.Reset();

            if (this.m_instance != null && this.m_instance.Bank != null)
            {
                this.m_instance.Bank.BankStatusChanged -= this.OnBankStatusChanged;
            }

            this.m_instance = objectInstance;
            if (this.m_instance == null)
            {
                return;
            }

            if (this.m_instance.Bank != null)
            {
                this.m_instance.Bank.BankStatusChanged += this.OnBankStatusChanged;
            }

            this.m_maxNumOperations = this.m_serialization.GetMaxNumOperations(this.m_instance);
            this.IsCheckedOut = !this.m_instance.IsReadOnly;

            this.m_serialization.Load(this, this.m_instance);

            this.CanAddOperation = this.m_listEditor.Items.Count < this.m_maxNumOperations;
        }

        /// <summary>
        /// The get index.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetIndex(MathOp item)
        {
            return this.m_listEditor.GetIndex(item);
        }

        /// <summary>
        /// The insert at.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="item">
        /// The item.
        /// </param>
        public void InsertAt(int index, MathOp item)
        {
            if (this.m_listEditor.Items.Count < this.m_maxNumOperations)
            {
                this.m_listEditor.InsertAt(index, item);
                item.Dirty += this.OnDirty;
                this.OnDirty();
            }

            this.CanAddOperation = this.m_listEditor.Items.Count < this.m_maxNumOperations;
        }

        /// <summary>
        /// The move.
        /// </summary>
        /// <param name="srcIndex">
        /// The src index.
        /// </param>
        /// <param name="destIndex">
        /// The dest index.
        /// </param>
        public void Move(int srcIndex, int destIndex)
        {
            this.m_listEditor.Move(srcIndex, destIndex);
            this.OnDirty();
        }

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public void Remove(MathOp item)
        {
            this.m_listEditor.Remove(item);
            item.Dirty -= this.OnDirty;
            this.OnDirty();
            this.CanAddOperation = this.m_listEditor.Items.Count < this.m_maxNumOperations;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on bank status changed.
        /// </summary>
        private void OnBankStatusChanged()
        {
            if (this.m_instance != null && this.m_instance.ObjectLookup.ContainsKey(this.m_instance.Name))
            {
                // Edit the newly loaded object instance
                this.EditObject(this.m_instance.ObjectLookup[this.m_instance.Name]);
            }
            else
            {
                this.Reset();
            }
        }

        /// <summary>
        /// The on dirty.
        /// </summary>
        private void OnDirty()
        {
            if (this.IsCheckedOut)
            {
                this.m_serialization.Save(this.m_instance, this.m_listEditor);
            }
        }

        /// <summary>
        /// The reset.
        /// </summary>
        private void Reset()
        {
            var items = this.m_listEditor.Items.ToList();
            foreach (var item in items)
            {
                this.m_listEditor.Remove(item);
            }

            this.IsCheckedOut = false;
            this.CanAddOperation = false;
        }

        #endregion
    }
}