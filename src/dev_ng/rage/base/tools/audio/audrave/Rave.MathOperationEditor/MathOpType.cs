using System.Collections.Generic;

namespace MathOperationEditor
{
    public class MathOpType
    {
        private static readonly IDictionary<MathOpTypes, MathOpType> ms_operationsInstanceLookup;
        private readonly string m_representation;

        static MathOpType()
        {
            ms_operationsInstanceLookup = new Dictionary<MathOpTypes, MathOpType>();
            AllOperations = new List<string>();

            var arithmeticOps = GetArithmeticOperations();
            ArithmeticOperations = new List<string>(arithmeticOps.Count);
            foreach (var operation in arithmeticOps)
            {
                ms_operationsInstanceLookup.Add(operation.Type, operation);
                var representation = GetRepresentation(operation);
                ArithmeticOperations.Add(representation);
                AllOperations.Add(representation);
            }
            ArithmeticOperations.Sort();

            var simpleFunctionOps = GetSimpleFunctionOperations();
            SimpleFunctionOperations = new List<string>(simpleFunctionOps.Count);
            foreach (var operation in simpleFunctionOps)
            {
                ms_operationsInstanceLookup.Add(operation.Type, operation);
                var representation = GetRepresentation(operation);
                SimpleFunctionOperations.Add(representation);
                AllOperations.Add(representation);
            }
            SimpleFunctionOperations.Sort();

            var complexFunctionOps = GetComplexFunctionOperations();
            ComplexFunctionOperations = new List<string>(complexFunctionOps.Count);
            foreach (var operation in complexFunctionOps)
            {
                ms_operationsInstanceLookup.Add(operation.Type, operation);
                var representation = GetRepresentation(operation);
                ComplexFunctionOperations.Add(representation);
                AllOperations.Add(representation);
            }
            ComplexFunctionOperations.Sort();

            AllOperations.Sort();
            AllOperations.TrimExcess();
        }

        private MathOpType(MathOpTypes type, uint numParams)
        {
            Type = type;
            NumParams = numParams;
            Tooltip = type.ToString();
            ResultTooltip = "Result";
            Param1Tooltip = "Param1";
            Param2Tooltip = "Param2";
            Param3Tooltip = "Param3";
        }

        private MathOpType(MathOpTypes type, uint numParams, string representation) : this(type, numParams)
        {
            Type = type;
            NumParams = numParams;
            m_representation = representation;
        }

        public MathOpTypes Type { get; private set; }

        public uint NumParams { get; private set; }

        public string Tooltip { get; set; }

        public string ResultTooltip { get; set; }

        public string Param1Tooltip { get; set; }

        public string Param2Tooltip { get; set; }

        public string Param3Tooltip { get; set; }

        public bool IsFunctionSyntax
        {
            get { return m_representation == null; }
        }

        public static List<string> AllOperations { get; private set; }
        public static List<string> ArithmeticOperations { get; private set; }
        public static List<string> SimpleFunctionOperations { get; private set; }
        public static List<string> ComplexFunctionOperations { get; private set; }

        public override string ToString()
        {
            return m_representation ?? Type.ToString();
        }

        public static MathOpType GetOp(MathOpTypes type)
        {
            return ms_operationsInstanceLookup[type];
        }

        private static List<MathOpType> GetArithmeticOperations()
        {
            var operations = new List<MathOpType>();
            operations.Add(new MathOpType(MathOpTypes.Add, 2, "+") {ResultTooltip = "Param1 + Param2"});
            operations.Add(new MathOpType(MathOpTypes.Subtract, 2, "\u2212") {ResultTooltip = "Param1 \u2212 Param2"});
            operations.Add(new MathOpType(MathOpTypes.Multiply, 2, "\u00D7") {ResultTooltip = "Param1 \u00D7 Param2"});
            operations.Add(new MathOpType(MathOpTypes.Divide, 2, "\u00F7")
                               {ResultTooltip = "Quotient", Param1Tooltip = "Dividend", Param2Tooltip = "Divisor"});
            operations.Add(new MathOpType(MathOpTypes.Set, 1, "\u003D")
                               {Tooltip = "Assignment", ResultTooltip = "Value of Param1", Param1Tooltip = "Param1"});
            operations.Add(new MathOpType(MathOpTypes.Mod, 2, "mod")
                               {
                                   Tooltip = "Modulo",
                                   ResultTooltip = "Value mod Modulus",
                                   Param1Tooltip = "Value",
                                   Param2Tooltip = "Modulus"
                               });
            return operations;
        }

        private static List<MathOpType> GetSimpleFunctionOperations()
        {
            var operations = new List<MathOpType>();
            operations.Add(new MathOpType(MathOpTypes.Min, 2)
                               {Tooltip = "Minimum", ResultTooltip = "The smaller value of Param1 and Param2"});
            operations.Add(new MathOpType(MathOpTypes.Max, 2)
                               {Tooltip = "Maximum", ResultTooltip = "The larger value of Param1 and Param2"});
            operations.Add(new MathOpType(MathOpTypes.Abs, 1)
                               {
                                   Tooltip = "Absolute",
                                   ResultTooltip =
                                       "The magnitude of Param1\nif Param1 < 0 then (-1 * Param1)\notherwise Param1"
                               });
            operations.Add(new MathOpType(MathOpTypes.Sign, 1)
                               {ResultTooltip = "-1 if Param1 < 0\n+1 if Param1 > 0\notherwise 0"});
            operations.Add(new MathOpType(MathOpTypes.Floor, 1)
                               {ResultTooltip = "The largest integer that is less than or equal to Param1"});
            operations.Add(new MathOpType(MathOpTypes.Ceil, 1)
                               {ResultTooltip = "The smallest integer that is greater than or equal to Param1"});
            operations.Add(new MathOpType(MathOpTypes.Rand, 2)
                               {Tooltip = "Random", ResultTooltip = "A random number between Param1 and Param2"});
            operations.Add(new MathOpType(MathOpTypes.Sin, 1)
                               {Tooltip = "Sine", ResultTooltip = "sin(\u03F4)", Param1Tooltip = "\u03F4 (in radians)"});
            operations.Add(new MathOpType(MathOpTypes.Cos, 1)
                               {
                                   Tooltip = "Cosine",
                                   ResultTooltip = "cos(\u03F4)",
                                   Param1Tooltip = "\u03F4 (in radians)"
                               });
            operations.Add(new MathOpType(MathOpTypes.Sqrt, 1, "\u221A")
                               {Tooltip = "Square Root", ResultTooltip = "The square root of Param1"});
            operations.Add(new MathOpType(MathOpTypes.DbToLinear, 1)
                               {
                                   Tooltip = "Converts volume from decibels to linear",
                                   ResultTooltip = "Linear volume",
                                   Param1Tooltip = "Volume in decibels"
                               });
            operations.Add(new MathOpType(MathOpTypes.LinearToDb, 1)
                               {
                                   Tooltip = "Converts volume from linear to decibels",
                                   ResultTooltip = "Volume in decibels",
                                   Param1Tooltip = "Linear volume"
                               });
            operations.Add(new MathOpType(MathOpTypes.PitchToRatio, 1)
                               {Tooltip = "Converts pitch to ratio", ResultTooltip = "Ratio", Param1Tooltip = "Pitch"});
            operations.Add(new MathOpType(MathOpTypes.RatioToPitch, 1)
                               {Tooltip = "Converts ratio to pitch", ResultTooltip = "Pitch", Param1Tooltip = "Ratio"});
            operations.Add(new MathOpType(MathOpTypes.GetTime, 0)
                               {Tooltip = "Retrieves the time in seconds", ResultTooltip = "Time in seconds"});
            operations.Add(new MathOpType(MathOpTypes.GetPos_X, 0) { Tooltip = "Retrieves the X coordinate of the sound's position in world space", ResultTooltip = "X coordinate in world units" });
            operations.Add(new MathOpType(MathOpTypes.GetPos_Y, 0) { Tooltip = "Retrieves the Y coordinate of the sound's position in world space", ResultTooltip = "Y coordinate in world units" });
            operations.Add(new MathOpType(MathOpTypes.GetPos_Z, 0) { Tooltip = "Retrieves the Z coordinate of the sound's position in world space", ResultTooltip = "Z coordinate in world units" });

            operations.Add(new MathOpType(MathOpTypes.Clamp, 3)
                               {
                                   Tooltip =
                                       "Clamps a value between specified limits\nif Value < LowerLimit then LowerLimit\nif Value > UpperLimit then UpperLimit\notherwise Value",
                                   ResultTooltip = "Clamped value",
                                   Param1Tooltip = "Value",
                                   Param2Tooltip = "LowerLimit",
                                   Param3Tooltip = "UpperLimit"
                               });
            operations.Add(new MathOpType(MathOpTypes.Pow, 2)
                               {
                                   Tooltip = "Exponentiation",
                                   ResultTooltip = "a\u207F where a = Base and \u207F = Exponent",
                                   Param1Tooltip = "Base",
                                   Param2Tooltip = "Exponent"
                               });
            operations.Add(new MathOpType(MathOpTypes.Round, 1)
                               {
                                   Tooltip = "Rounds a number to the nearest integer",
                                   ResultTooltip = "Param1 rounded to the nearest integer",
                               });

            operations.Add(new MathOpType(MathOpTypes.Trace, 3)
            {
                Tooltip =
                    "Traces three variable values - view in game by selected rage - profile stats / MathOpSound",
                ResultTooltip = "Result is left unaffected",
                Param1Tooltip = "Value1",
                Param2Tooltip = "Value2",
                Param3Tooltip = "Value3"
            });
            return operations;
        }

        private static List<MathOpType> GetComplexFunctionOperations()
        {
            var operations = new List<MathOpType>();
            operations.Add(new MathOpType(MathOpTypes.FSel, 3)
                               {
                                   Tooltip =
                                       "Selects between 2 numbers based on the value of a third number.\nThis can be very useful as a way of performing conditional statements without leaving a MathOperationSound.",
                                   ResultTooltip =
                                       "\"ValueLT\" if \"Condition\" < 0\n\"ValueGT\" if \"Condition\" >= 0\n\"ValueLT\" if \"Condition\" = NaN",
                                   Param1Tooltip = "Condition",
                                   Param2Tooltip = "ValueGT",
                                   Param3Tooltip = "ValueLT"
                               });
            operations.Add(new MathOpType(MathOpTypes.ValueInRange, 3)
                               {
                                   Tooltip =
                                       "Linearly scales \"Value\" to the range [LowerLimit,UpperLimit]\nUses the formula: LowerLimit + ((UpperLimit-LowerLimit) * Value)",
                                   ResultTooltip = "Rescaled Value to the range [LowerLimit,UpperLimit]",
                                   Param1Tooltip =
                                       "Value\nNote: This is not capped so if less than 0 or greater than 1 it will still be scaled",
                                   Param2Tooltip = "LowerLimit",
                                   Param3Tooltip = "UpperLimit"
                               });
            operations.Add(new MathOpType(MathOpTypes.ClampRange, 3)
                                {
                                    Tooltip =
                                    "Find the fraction of the distance that the given \"Value\" is between the other two given values, with limits of 0 and 1.\nUses the formula: (Value-LowerLimit) / (UpperLimit-LowerLimit)",
                                    ResultTooltip = "Rescaled value in the range [0,1]",
                                    Param1Tooltip =
                                        "Value",
                                    Param2Tooltip = "LowerLimit",
                                    Param3Tooltip = "UpperLimit"
                                });

            operations.Add(new MathOpType(MathOpTypes.ScaledSin, 1)
                               {
                                   Tooltip =
                                       "Calculates the sine of Param1 and automatically scales the output.\nParam1 values from 0-1 are the same as 0-360 degrees.",
                                   ResultTooltip = "Scaled Sine of Param1 in the range [0,1]",
                                   Param1Tooltip = "Param1\nif Param1 > 1 or Param1 < 0 it is wrapped"
                               });
            operations.Add(new MathOpType(MathOpTypes.ScaledTri, 1)
                               {
                                   Tooltip =
                                       "Calculates a triangle wave from Param1 and automatically scales the output.\nParam1 values from 0-1 are the same as 0-360 degrees.",
                                   ResultTooltip = "Scaled triangle wave of Param1 in the range [0,1]",
                                   Param1Tooltip = "Param1\nif Param1 > 1 or Param1 < 0 it is wrapped"
                               });
            operations.Add(new MathOpType(MathOpTypes.ScaledSaw, 1)
                               {
                                   Tooltip =
                                       "Calculates a ramp/sawtooth wave from Param1 and automatically scales the output.\nParam1 values from 0-1 are the same as 0-360 degrees.",
                                   ResultTooltip = "Scaled ramp/sawtooth wave of Param1 in the range [0,1]",
                                   Param1Tooltip = "Param1\nif Param1 > 1 or Param1 < 0 it is wrapped"
                               });
            operations.Add(new MathOpType(MathOpTypes.ScaledSquare, 1)
                               {
                                   Tooltip =
                                       "Calculates a square wave from Param1 and automatically scales the output.\nParam1 values from 0-1 are the same as 0-360 degrees.",
                                   ResultTooltip = "Scaled square wave of Param1 in the range [0,1]",
                                   Param1Tooltip = "Param1\nif Param1 > 1 or Param1 < 0 it is wrapped"
                               });
            operations.Add(new MathOpType(MathOpTypes.Smooth, 3)
                               {
                                   Tooltip = "Smoothes a value by limiting its rate of change.",
                                   ResultTooltip = "Smoothed value",
                                   Param1Tooltip = "Param1 - the target value",
                                   Param2Tooltip = "Param2 - the previous value",
                                   Param3Tooltip =
                                       "Maximum rate of change in units per step to use to try and reach Param1 from Param2"
                               });
            operations.Add(new MathOpType(MathOpTypes.GetScaledTime, 1)
                               {
                                   Tooltip =
                                       "Scales the current time by the wavelength specified by Param1 (in seconds) so that it lies in the range [0,1].\nThis gives an inverted sawtooth shaped wave which will be discontinuous if the period is changed.",
                                   ResultTooltip =
                                       "A value in the range [0,1] that takes Param1 seconds to complete a cycle"
                               });
            return operations;
        }

        private static string GetRepresentation(MathOpType operation)
        {
            var name = operation.Type.ToString();
            var representation = operation.ToString();
            return string.Compare(representation, name) == 0 ? name : string.Format("{0} ({1})", name, representation);
        }
    }
}