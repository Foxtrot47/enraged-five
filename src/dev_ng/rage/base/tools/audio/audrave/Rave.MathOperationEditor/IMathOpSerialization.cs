using WPFToolLib.List;

namespace MathOperationEditor
{
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.VariableList;

    public interface IMathOpSerialization
    {
        IVariableList ResultVariables { get; }

        IVariableList ParamVariables { get; }

        void Load(IListEditor<MathOp> operations, IObjectInstance instance);

        void Save(IObjectInstance instance, IListEditor<MathOp> operations);

        int GetMaxNumOperations(IObjectInstance instance);
    }
}