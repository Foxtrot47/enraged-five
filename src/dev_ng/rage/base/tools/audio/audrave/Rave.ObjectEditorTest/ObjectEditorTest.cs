using System;
using System.Collections.Generic;
using System.Text;
using Rave.Plugins;
using System.Xml;

namespace Rave.ObjectEditorTest
{
    public class ObjectEditorTest:IRAVEPlugin,IRAVEObjectEditorPlugin
    {
        private string m_Name;
        private string m_Type;

        #region IRAVEPlugin Members

        public string GetName()
        {
            return m_Name;
        }

        public bool Init(System.Xml.XmlNode settings)
        {
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        m_Name = setting.InnerText;
                        break;
                }

            }
            return true;
        }

        public void Shutdown()
        {
            return;
        }

        #endregion

        #region IObjectEditor Members

        public string ObjectType
        {
            get
            {
                return m_Type;
            }
        }

        public void EditObject(IObjectInstance objectInstance)
        {
            
        }

        #endregion
    }
}
