﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Animation;
using Excel;
using Rave.Instance;
using RSG.Text.Model;
using Track = RSG.Text.Model.Track;

//using Net.SourceForge.Koogra;

namespace Rave.EdlToReaper
{
	public class EdlToReaper
	{
		public string InputFile { get; set; }
		public string OutputFile { get; set; }
		public TrackListings TrackListings { get; set; }
		public const string AmbientSpeechContextXlsxPath = @"//rdr3/docs/Design/Ambient/Ambient_Speech_Context.xlsx";
		private string _currentVocalLine;
		private double _videoPosition;
		private double _videoLength;
		public EdlToReaper(string inputFile, string outputFile)
		{
			TrackListings = new TrackListings();
			TrackListings.LoadRecordingFile(inputFile);
			OutputFile = outputFile;
			InputFile = inputFile;
			Instance.RaveInstance.AssetManager.GetLatest(AmbientSpeechContextXlsxPath, true);
		}

		public string GetVideoDepotPath(string soundId)
		{
			foreach (
				worksheet worksheet in Workbook.Worksheets(Instance.RaveInstance.AssetManager.GetLocalPath(AmbientSpeechContextXlsxPath)))
			{
				foreach (Row row in worksheet.Rows)
				{
					foreach (var cell in row.Cells.Where(p => p != null && p.Text != null && p.Text.Length > 1))
					{
						if (cell.Text.Equals(soundId, StringComparison.InvariantCultureIgnoreCase))
						{
							foreach (var cell2 in row.Cells.Where(p => p != null && p.Text != null && p.Text.Length > 1))
							{
								if (cell2.Text.ToLower().Contains(".mov"))
								{
									return cell2.Text;
								}

							}
						}
					}
				}
			}
			return null;
		}

		public void ConvertToReaperProject()
		{
			StringBuilder reaperProject = new StringBuilder();
			reaperProject.AppendLine(CreateProjectHeader());
			foreach (Track track in TrackListings.Tracks)
			{
				string trackString = GetTrackXml(track, Path.GetDirectoryName(InputFile));
				if (trackString != null)
				{
					reaperProject.AppendLine(trackString);
				}
			}
			if (!string.IsNullOrEmpty(_currentVocalLine))
			{
				string videoDepotPathOriginal = GetVideoDepotPath(_currentVocalLine);
				if (videoDepotPathOriginal != null)
				{
					string[] videoDepotPaths = videoDepotPathOriginal.Split(new Char[] {' ', ',', '\n'});
					foreach (string videoDepotPath in videoDepotPaths)
					{
						if (!string.IsNullOrEmpty(videoDepotPath) && videoDepotPath.Contains(".mov")
						    && RaveInstance.AssetManager.ExistsAsAsset(videoDepotPath))
						{
							RaveInstance.AssetManager.GetLatest(videoDepotPath, false);
							reaperProject.AppendLine(CreateVideoTrack(RaveInstance.AssetManager.GetLocalPath(videoDepotPath),
								_videoPosition, _videoLength));
						}
					}
				}
			}

			reaperProject.AppendLine(">");
			using (StreamWriter writer = new StreamWriter(OutputFile, false, Encoding.ASCII))
			{
				writer.Write(reaperProject.ToString());
				writer.Flush();
			}
		    Process.Start(new ProcessStartInfo(OutputFile));
		}

		private static string CreateProjectHeader()
		{
			StringBuilder writer = new StringBuilder();
			writer.AppendLine("<REAPER_PROJECT 0.1 \"4.13\"");
			writer.AppendLine("\tRENDER_FILE \"\"");
			writer.AppendLine("\tRENDER_PATTERN \"\"");
			writer.AppendLine("\tRENDER_RANGE 3 0.00000000000000 0.00000000000000");
			return writer.ToString();
		}

		private string GetTrackXml(Track track, string workingCategory)
		{
			int numberOfItems = 0;
		
			StringBuilder trackString = new StringBuilder();
			trackString.AppendLine("\t<TRACK");
			{
				trackString.AppendLine(string.Format("\t\tNAME \"{0}\"", track.Name));

				foreach (LineRecordedData line in track.RecordedData)
				{
					if (!line.StartDateTime.HasValue || !line.EndDateTime.HasValue)
					{ continue; }
					trackString.AppendLine("\t\t<ITEM");
					{
						trackString.AppendLine(string.Format("\t\t\tNAME \"{0}\"", line.ClipName));
						trackString.AppendLine(string.Format("\t\t\tPOSITION {0}", line.StartDateTime.Value.TimeOfDay.TotalSeconds));
						trackString.AppendLine(string.Format("\t\t\tLENGTH {0}",
							line.EndDateTime.Value.TimeOfDay.TotalSeconds - line.StartDateTime.Value.TimeOfDay.TotalSeconds));

						trackString.AppendLine("\t\t\t<SOURCE WAVE");
					    {
					        string path = Path.Combine(workingCategory, GetCleanPath(line.ClipName) + ".wav");
					        if (RaveInstance.AssetManager.ExistsAsAsset(path))
					        {
					            RaveInstance.AssetManager.GetLatest(path, false);
					        }
					        trackString.AppendLine(string.Format("\t\t\t\tFILE \"{0}\"",
								path));
						}

						if (line.ClipName.EndsWith("_01"))
						{
							_currentVocalLine = line.ClipName.TrimEnd("_01".ToCharArray());
						}
						else if (line.ClipName.EndsWith(".L"))
						{
							_videoPosition = line.StartDateTime.Value.TimeOfDay.TotalSeconds;
							_videoLength = line.EndDateTime.Value.TimeOfDay.TotalSeconds - line.StartDateTime.Value.TimeOfDay.TotalSeconds;
						}
						numberOfItems++;
						trackString.AppendLine("\t\t\t>");
					}
					trackString.AppendLine("\t\t>");
				}
				trackString.AppendLine("\t\t>");
			}
			if (numberOfItems == 0)
			{
				return null;
			}
			return trackString.ToString();
		}

		private static string CreateVideoTrack(string videoPath, double position, double length)
		{
			StringBuilder trackString = new StringBuilder();
			trackString.AppendLine("\t<TRACK");
			{
				trackString.AppendLine(string.Format("\t\tNAME \"{0}\"", "Video"));
				trackString.AppendLine("\t\tVOLPAN 0 0 -1 -1 1");
				trackString.AppendLine("\t\t<ITEM");

				trackString.AppendLine(string.Format("\t\t\tNAME \"{0}\"", Path.GetFileNameWithoutExtension(videoPath)));
				trackString.AppendLine(string.Format("\t\t\tPOSITION {0}", position));
				trackString.AppendLine(string.Format("\t\t\tLENGTH {0}",
					length));

				trackString.AppendLine("\t\t\t<SOURCE VIDEO");
				{
					trackString.AppendLine(string.Format("\t\t\t\tFILE \"{0}\"",
						videoPath));
				}
				trackString.AppendLine("\t\t\t>");

				trackString.AppendLine("\t\t>");

				trackString.AppendLine("\t\t>");
			}
			return trackString.ToString();
		}

		private static string GetCleanPath(string path)
		{
			string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

			return invalid.Aggregate(path, (current, c) => current.Replace(c.ToString(), ""));
		}
	}
}
