namespace Rave.RadioImport
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.UserActions;

    using Wavelib;

    public class RadioImport : IRAVEWaveImportPlugin
    {
        public const int CHANNELS = 2;
        private const string WAVE_EXTENSION = ".WAV";

        private static readonly string[] ms_channelExtension = new[] {"_LEFT", "_RIGHT"};
        private string m_name;
        private ArrayList m_tagNodes;

        #region IRAVEWaveImportPlugin Members

        public string GetName()
        {
            return m_name;
        }

        public bool Init(XmlNode settings)
        {
            m_tagNodes = new ArrayList();
            string name = null;
            foreach (XmlNode setting in settings.ChildNodes)
            {
                switch (setting.Name)
                {
                    case "Name":
                        name = setting.InnerText;
                        break;

                        // add all the nodes to an array for later.. we don't have an ActionLog yet
                    case "streamingBlockBytes":
                    case "compression":
                    case "sampleRate":
                        m_tagNodes.Add(setting);
                        break;
                }
            }

            if (name == null)
            {
                return false;
            }
            m_name = name;
            return true;
        }

        public void HandleDroppedFiles(IWaveBrowser waveBrowser,
                                       EditorTreeNode selectedNode,
                                       IActionLog actionLog,
                                       string[] files)
        {
            try
            {
                EditorTreeNode.AllowAutoLock = true;

                var actions = new ArrayList();
                Import(waveBrowser, actionLog, selectedNode, files, actions);
                if (actions.Count > 0)
                {
                    var action = waveBrowser.CreateAction(ActionType.BulkAction,
                                                          new ActionParams(null,
                                                                           null,
                                                                           null,
                                                                           null,
                                                                           null,
                                                                           actionLog,
                                                                           null,
                                                                           null,
                                                                           null,
                                                                           actions));
                    if (action.Action())
                    {
                        actionLog.AddAction(action);
                    }
                }
            }
            finally
            {
                EditorTreeNode.AllowAutoLock = false;
            }
        }

        #endregion

        public override string ToString()
        {
            return m_name;
        }

        private void Import(IWaveBrowser waveBrowser,
                            IActionLog actionLog,
                            EditorTreeNode parent,
                            IEnumerable<string> files,
                            ArrayList actions)
        {
            foreach (var file in files)
            {
                var name = file.Substring(file.LastIndexOf("\\") + 1).ToUpper();
                //not folder
                if (name.ToUpper().Contains(WAVE_EXTENSION))
                {
                    if (parent.GetType() == typeof (BankFolderNode) ||
                        parent.GetType() == typeof (PackNode))
                    {
                        ImportWave(waveBrowser, actionLog, parent, file, actions);
                    }
                    else
                    {
                        ErrorManager.HandleInfo("Waves can only be dropped onto a Bankfolder or Pack");
                    }
                }
                else if (!name.Contains("."))
                {
                    //assume folder
                    if (parent.GetType() ==
                        typeof (PackNode))
                    {
                        //if parent is a pack and folder does not already exist, create tags

                        var bankFolderNode = (BankFolderNode) GetChildNode(parent, name);

                        if (bankFolderNode == null)
                        {
                            waveBrowser.GetEditorTreeView().SetText("Adding Bank Folder " + name);
                            var action = waveBrowser.CreateAction(ActionType.AddBankFolder,
                                                                  new ActionParams(waveBrowser,
                                                                                   parent,
                                                                                   null,
                                                                                   null,
                                                                                   null,
                                                                                   actionLog,
                                                                                   name,
                                                                                   parent.GetPlatforms(),
                                                                                   null,
                                                                                   null));
                            if (action.Action())
                            {
                                actions.Add(action);
                            }
                            //get added bank as node
                            bankFolderNode = (BankFolderNode) GetChildNode(parent, name);
                            CreateTags(waveBrowser, actionLog, bankFolderNode, actions);
                        }

                        var currentFolder = new DirectoryInfo(file);
                        var childFiles = currentFolder.GetFiles();

                        //get the paths for the files/directories and pass to the appropriate methods
                        var filePaths = new string[childFiles.Length];

                        for (var i = 0; i < filePaths.Length; i++)
                        {
                            filePaths[i] = childFiles[i].FullName;
                        }

                        //import files and directories
                        waveBrowser.GetEditorTreeView().SetText("Importing: " + name);
                        Import(waveBrowser, actionLog, bankFolderNode, filePaths, actions);
                    }

                    else
                    {
                        ErrorManager.HandleInfo("Cannot add waveBank folder. Parent is not a Pack");
                    }
                }
            }
        }

        private static void ImportWave(IWaveBrowser waveBrowser,
                                       IActionLog actionLog,
                                       EditorTreeNode parent,
                                       string file,
                                       IList actions)
        {
            //create bank for wave files
            var stripWav = file.Substring(0, file.Length - 4);
            var bankName = stripWav.Substring(stripWav.LastIndexOf("\\") + 1).ToUpper();

            waveBrowser.GetEditorTreeView().SetText("Importing: " + bankName + WAVE_EXTENSION);

            //create wave files - only dealing with stereo
            waveBrowser.GetEditorTreeView().SetText("Loading Wave File.....");
            var waveFile = new bwWaveFile(file, false);

            //should be 2 channels
            uint numOfChannels = waveFile.Format.NumChannels;

            var filePaths = new string[CHANNELS];
            var newWaveNames = new string[CHANNELS];

            bool succeeded;
            switch (numOfChannels)
            {
                case 1:
                    succeeded = CreateMono(waveFile, file, filePaths, newWaveNames);
                    break;
                case 2:
                    waveBrowser.GetEditorTreeView().SetText("Splitting Wave...");
                    succeeded = CreateMultiChannel(waveFile, numOfChannels, file, filePaths, newWaveNames);
                    break;
                default:
                    ErrorManager.HandleInfo(numOfChannels + " waves not supported");
                    return;
            }

            if (succeeded)
            {
                //check if bank already exists
                var bankNode = (BankNode) GetChildNode(parent, bankName);

                if (bankNode == null)
                {
                    waveBrowser.GetEditorTreeView().SetText("Adding Bank " + bankName);
                    //create a new bank
                    var action = waveBrowser.CreateAction(ActionType.AddBank,
                                                          new ActionParams(waveBrowser,
                                                                           parent,
                                                                           null,
                                                                           null,
                                                                           waveBrowser.GetEditorTreeView().GetTreeView(),
                                                                           actionLog,
                                                                           bankName,
                                                                           parent.GetPlatforms(),
                                                                           null,
                                                                           null));
                    if (action.Action())
                    {
                        actions.Add(action);
                    }
                    //get added bank as node
                    bankNode = (BankNode) GetChildNode(parent, bankName);
                }

                var existingWaves = new WaveNode[CHANNELS];
                for (var waveName = 0; waveName < newWaveNames.Length; waveName++)
                {
                    for (var i = 0; i < bankNode.Nodes.Count; i++)
                    {
                        if (bankNode.Nodes[i].GetType() ==
                            typeof (WaveNode))
                        {
                            if (((WaveNode) bankNode.Nodes[i]).GetObjectName().ToUpper() ==
                                newWaveNames[waveName])
                            {
                                existingWaves[waveName] = (WaveNode) bankNode.Nodes[i];
                            }
                        }
                    }
                }

                for (var i = 0; i < existingWaves.Length; i++)
                {
                    if (existingWaves[i] == null)
                    {
                        // this is a new wave file
                        var action = waveBrowser.CreateAction(ActionType.AddWave,
                                                              new ActionParams(waveBrowser,
                                                                               bankNode,
                                                                               null,
                                                                               filePaths[i],
                                                                               null,
                                                                               actionLog,
                                                                               null,
                                                                               bankNode.GetPlatforms(),
                                                                               null,
                                                                               null));
                        if (action.Action())
                        {
                            actions.Add(action);
                        }
                    }
                    else
                    {
                        // this is an existing wave file
                        var action = waveBrowser.CreateAction(ActionType.ChangeWave,
                                                              new ActionParams(waveBrowser,
                                                                               bankNode,
                                                                               existingWaves[i],
                                                                               filePaths[i],
                                                                               null,
                                                                               actionLog,
                                                                               null,
                                                                               existingWaves[i].GetPlatforms(),
                                                                               null,
                                                                               null));
                        if (action.Action())
                        {
                            actions.Add(action);
                        }
                    }
                }
            }
            else
            {
                ErrorManager.HandleError("Could not import wave " + file);
            }
        }

        private static bool CreateMultiChannel(bwWaveFile waveFile,
                                               uint numOfChannels,
                                               string file,
                                               string[] filePaths,
                                               string[] newWaveNames)
        {
            var markers = waveFile.Markers;

            for (var i = 0; i < numOfChannels; i++)
            {
                var fileName = file.ToUpper().Substring(file.LastIndexOf("\\") + 1).Replace(" ", "_");
                newWaveNames[i] = fileName.Substring(0, fileName.Length - 4) + ms_channelExtension[i] + WAVE_EXTENSION;

                filePaths[i] = Path.GetTempPath() + newWaveNames[i];

                //create waveFile
                try
                {
                    var tempWave = new bwWaveFile();

                    foreach (IRiffChunk chunk in waveFile.Chunks)
                    {
                        var fChunk = chunk as bwFormatChunk;
                        var dChunk = chunk as bwDataChunk;
                        //current chunk is format chunk
                        if (fChunk != null)
                        {
                            var formatChunk = new bwFormatChunk(fChunk.CompressionCode,
                                                                1,
                                                                fChunk.SampleRate,
                                                                fChunk.AverageBytesPerSecond / 2,
                                                                (ushort) (fChunk.BlockAlign / 2),
                                                                fChunk.SignificantBitsPerSample,
                                                                fChunk.ExtraFormatBytes);
                            tempWave.AddChunk(formatChunk);
                            tempWave.Format = formatChunk;
                        }
                            //current chunk is data chunk
                        else if (dChunk != null)
                        {
                            var newData = new byte[(uint) (0.5 * dChunk.RawData.Length)];
                            //new chunk index
                            var index = 0;
                            //iterate through current raw data and split up
                            //j set to 2* channel, 16bits/sample so increase j by 4 bytes
                            for (var j = i * 2; j < dChunk.RawData.Length; j += 4)
                            {
                                newData[index] = dChunk.RawData[j];
                                newData[index + 1] = dChunk.RawData[j + 1];
                                index += 2;
                            }
                            var dataChunk = new bwDataChunk(newData, dChunk.NumSamples, bwDataChunk.DataFormat.S16);
                            tempWave.AddChunk(dataChunk);
                            tempWave.Data = dataChunk;
                        }
                        else
                        {
                            tempWave.AddChunk(chunk);
                        }
                    }

                    tempWave.FileName = filePaths[i];
                    if (tempWave.FileName.ToUpper().EndsWith(ms_channelExtension[0] + WAVE_EXTENSION))
                    {
                        tempWave.Markers = markers;
                    }
                    tempWave.Save();
                }
                catch (Exception)
                {
                    ErrorManager.HandleInfo("Invalid wave file - only 16bit waves are supported");
                    return false;
                }
            }
            return true;
        }

        private static bool CreateMono(bwWaveFile waveFile, string file, string[] filePaths, string[] newWaveNames)
        {
            if (
                ErrorManager.HandleQuestion(file + " is a mono wave, create two copies?",
                                            MessageBoxButtons.YesNo,
                                            DialogResult.No) ==
                DialogResult.No)
            {
                return false;
            }

            var markers = waveFile.Markers;

            for (var i = 0; i < CHANNELS; i++)
            {
                var fileName = file.ToUpper().Substring(file.LastIndexOf("\\") + 1).Replace(" ", "_");
                newWaveNames[i] = fileName.Substring(0, fileName.Length - 4) + ms_channelExtension[i] + WAVE_EXTENSION;

                filePaths[i] = Path.GetTempPath() + newWaveNames[i];

                //create waveFile
                try
                {
                    var tempWave = new bwWaveFile();

                    foreach (IRiffChunk chunk in waveFile.Chunks)
                    {
                        var fChunk = chunk as bwFormatChunk;
                        var dChunk = chunk as bwDataChunk;
                        //current chunk is format chunk
                        if (fChunk != null)
                        {
                            tempWave.AddChunk(fChunk);
                            tempWave.Format = fChunk;
                        }
                            //current chunk is data chunk
                        else if (dChunk != null)
                        {
                            tempWave.AddChunk(dChunk);
                            tempWave.Data = dChunk;
                        }
                        else
                        {
                            tempWave.AddChunk(chunk);
                        }
                    }

                    tempWave.FileName = filePaths[i];
                    if (tempWave.FileName.ToUpper().EndsWith(ms_channelExtension[0] + WAVE_EXTENSION))
                    {
                        tempWave.Markers = markers;
                    }
                    tempWave.Save();
                }
                catch (Exception)
                {
                    ErrorManager.HandleInfo("Invalid wave file - only 16bit waves are supported");
                    return false;
                }
            }
            return true;
        }

        private void CreateTags(IWaveBrowser waveBrowser, IActionLog actionLog, BankFolderNode parent, ArrayList actions)
        {
            //create each tag from the xml
            foreach (XmlNode tag in m_tagNodes)
            {
                var node = new TagNode(actionLog, tag.Name, waveBrowser);
                parent.Nodes.Add(node);

                foreach (XmlNode platform in tag.ChildNodes)
                {
                    node.AddPlatform(platform.Name, EditorTreeNode.NodeDisplayState.AddedLocally);
                    node.SetValue(platform.Name, platform.InnerText);
                }
            }
        }

        //returns child bank node that matches name
        public static EditorTreeNode GetChildNode(EditorTreeNode parentNode, string name)
        {
            return parentNode.Nodes.Cast<EditorTreeNode>().FirstOrDefault(node => node.GetObjectName() == name);
        }
    }
}