using System;
using System.Text;
using Rave.CurveEditor.Curves;

namespace Rave.CurveEditor.PiecewiseLinear
{
    public class PiecewiseLinearCurveEvaluator : CurveEvaluator
    {
        public PiecewiseLinearCurveEvaluator(ICurve curve, ICurveEditorModel parent) : base(curve)
        {
            Parent = parent;
            Evaluate();
        }

        protected override void Evaluate()
        {
            var pathBuilder = new StringBuilder();
            pathBuilder.Append("M ");

            var first = true;
            foreach (var controlPoint in Curve.ControlPoints)
            {
                if (first)
                {
                    pathBuilder.Append(controlPoint.XPos);
                    pathBuilder.Append(",");
                    pathBuilder.Append(controlPoint.YPos);
                    pathBuilder.Append(" L");
                    first = false;
                }
                else
                {
                    pathBuilder.Append(" ");
                    pathBuilder.Append(controlPoint.XPos);
                    pathBuilder.Append(",");
                    pathBuilder.Append(controlPoint.YPos);
                }
            }

            CurveGeometry = pathBuilder.ToString();
        }

        public static void PopulateCurve(ICurve curve,
                                         Func<ControlPoint, double, double, bool> canChangeValueFunc,
                                         double xLength,
                                         double yLength)
        {
            curve.Clear();
            curve.Add(new ControlPoint(curve, canChangeValueFunc) {X = 0, Y = 0});
            curve.Add(new ControlPoint(curve, canChangeValueFunc) {X = xLength / 5, Y = yLength / 5});
        }
    }
}