﻿using System;
using System.Windows;
using System.Windows.Media;
using rage.ToolLib;

namespace Rave.CurveEditor.GridLines
{
    public class GridLinesViewModel : IGridLinesViewModel
    {
        private readonly IGridLinesModel m_model;

        public GridLinesViewModel(IGridLinesModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            InitGridLines();
            m_model.SizeChanged += OnSizeChanged;
        }

        public event Action SizeChanged;

        public GeometryGroup GridLinesGemetryGroup { get; private set; }

        public double Height
        {
            get { return m_model.Height; }
            set { m_model.Height = value; }
        }

        public double Width
        {
            get { return m_model.Width; }
            set { m_model.Width = value; }
        }
        
        public void Dispose()
        {
            m_model.SizeChanged -= InitGridLines;
        }

        private void InitGridLines()
        {
            GridLinesGemetryGroup = new GeometryGroup();

            foreach (var line in m_model.AllLines)
            {
                var x = new Point(line.X1, line.Y1);
                var y = new Point(line.X2, line.Y2);
                var lineGeometry = new LineGeometry(x, y);
                GridLinesGemetryGroup.Children.Add(lineGeometry);
            }
        }

        private void OnSizeChanged()
        {
            InitGridLines();
            SizeChanged.Raise();
        }
    }
}
