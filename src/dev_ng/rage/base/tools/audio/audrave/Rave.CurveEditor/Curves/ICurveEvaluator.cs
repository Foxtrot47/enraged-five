using System;
using System.ComponentModel;

namespace Rave.CurveEditor.Curves
{
    public interface ICurveEvaluator : INotifyPropertyChanged,
                                       IDisposable
    {
        ICurveEditorModel Parent { get; }
        bool IsSelected { get; set; }
        int CurveThickness { get; }
        string CurveGeometry { get; }
        ICurve Curve { get; }
    }
}