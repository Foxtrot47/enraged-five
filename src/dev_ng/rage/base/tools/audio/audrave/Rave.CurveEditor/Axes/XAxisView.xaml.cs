﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace Rave.CurveEditor.Axes
{
    /// <summary>
    ///   Interaction logic for XAxisView.xaml
    /// </summary>
    public partial class XAxisView
    {
        private IAxisViewModel m_viewModel;

        public XAxisView()
        {
            InitializeComponent();
        }

        private void OnUserControlLoaded(object sender, RoutedEventArgs e)
        {
            m_viewModel = DataContext as IAxisViewModel;
            if (m_viewModel != null)
            {
                m_viewModel.PropertyChanged += OnViewModelPropertyChanged;
                OnViewModelPropertyChanged(null, null);
            }
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            markings.Children.Clear();

            if (null == m_viewModel)
            {
                return;
            }

            var halfMarkFreq = m_viewModel.MarkFrequency / 2;
            var current = halfMarkFreq;
            var actualCurrent = -(m_viewModel.ActualLength / 2);
            while (current <= m_viewModel.Length + halfMarkFreq)
            {
                var value = Math.Round(actualCurrent, 1);
                if (value != 0)
                {
                    var textBlock = new TextBlock
                                        {
                                            Style = Resources["TextBlockStyle"] as Style,
                                            Text = value.ToString(),
                                        };
                    Canvas.SetLeft(textBlock, current);
                    Canvas.SetTop(textBlock, 7);
                    markings.Children.Add(textBlock);

                    var line = new Line
                                   {
                                       X1 = current,
                                       Y1 = 0,
                                       X2 = current,
                                       Y2 = 5,
                                       Style = Resources["AxisLineStyle"] as Style
                                   };
                    markings.Children.Add(line);
                }

                current += m_viewModel.MarkFrequency;
                actualCurrent += m_viewModel.ActualMarkFrequency;
            }
        }

        private void OnUserControlUnloaded(object sender, RoutedEventArgs e)
        {
            if (m_viewModel != null)
            {
                m_viewModel.PropertyChanged -= OnViewModelPropertyChanged;
                m_viewModel = null;
            }
        }
    }
}