using System;
using System.ComponentModel;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor.Curves
{
    using rage.ToolLib;

    public class ControlPoint : INotifyPropertyChanged
    {
        private const int SIZE = 8;
        private const int HALF_SIZE = SIZE / 2;
        private readonly Func<ControlPoint, double, double, bool> m_canChangeValueFunc;
        private string m_coordinates;
        private bool m_isEditing;
        private double m_x;
        private double m_y;

        public ControlPoint(ICurve parent, Func<ControlPoint, double, double, bool> canChangeValueFunc)
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }
            if (canChangeValueFunc == null)
            {
                throw new ArgumentNullException("canChangeValueFunc");
            }
            m_canChangeValueFunc = canChangeValueFunc;
            Parent = parent;
            m_x = double.MinValue;
            m_y = double.MinValue;
        }

        public event Action Dirty;

        public ICurve Parent { get; private set; }

        public double X
        {
            get { return m_x; }
            set
            {
                var newX = Math.Round(value, 3);
                if (newX != m_x &&
                    m_canChangeValueFunc(this, newX, m_y))
                {
                    m_x = newX;
                    PropertyChanged.Raise(this, "X");
                    PropertyChanged.Raise(this, "XPos");
                    Coordinates = string.Format("{0},{1}", X, Y);
                    Dirty.Raise();
                }
            }
        }

        public double XPos
        {
            get
            {
                var xAxis = Parent.Parent.Parent.XAxis;
                var scaledX = X * xAxis.Scale;
                var scaledHalfLength = (xAxis.Length / 2) * xAxis.Scale;
                var scaledHalfMarkFreq = (xAxis.MarkFrequency / 2) * xAxis.Scale;
                return scaledX + scaledHalfLength + scaledHalfMarkFreq;
            }
            set
            {
                var xAxis = Parent.Parent.Parent.XAxis;
                var scaledHalfLength = (xAxis.Length / 2) * xAxis.Scale;
                var scaledHalfMarkFreq = (xAxis.MarkFrequency / 2) * xAxis.Scale;
                X = (value - scaledHalfLength - scaledHalfMarkFreq) / xAxis.Scale;
            }
        }

        public double Y
        {
            get { return m_y; }
            set
            {
                var newY = Math.Round(value, 3);
                if (newY != m_y &&
                    m_canChangeValueFunc(this, m_x, newY))
                {
                    m_y = newY;
                    PropertyChanged.Raise(this, "Y");
                    PropertyChanged.Raise(this, "YPos");
                    Coordinates = string.Format("{0},{1}", X, Y);
                    Dirty.Raise();
                }
            }
        }

        public double YPos
        {
            get
            {
                var yAxis = Parent.Parent.Parent.YAxis;
                var scaledY = Y * yAxis.Scale;
                var scaledHalfLength = (yAxis.Length / 2) * yAxis.Scale;
                var scaledHalfMarkFreq = (yAxis.MarkFrequency / 2) * yAxis.Scale;
                return scaledY + scaledHalfLength + scaledHalfMarkFreq;
            }
            set
            {
                var yAxis = Parent.Parent.Parent.YAxis;
                var scaledHalfLength = (yAxis.Length / 2) * yAxis.Scale;
                var scaledHalfMarkFreq = (yAxis.MarkFrequency / 2) * yAxis.Scale;
                Y = (value - scaledHalfLength - scaledHalfMarkFreq) / yAxis.Scale;
            }
        }

        public string Coordinates
        {
            get { return m_coordinates; }
            private set
            {
                m_coordinates = value;
                PropertyChanged.Raise(this, "Coordinates");
            }
        }

        public bool IsEditing
        {
            get { return m_isEditing; }
            set
            {
                m_isEditing = value;
                PropertyChanged.Raise(this, "IsEditing");
            }
        }

        public static double Size
        {
            get { return SIZE; }
        }

        public static double HalfSize
        {
            get { return HALF_SIZE; }
        }

        public static double NegHalfSize
        {
            get { return -HALF_SIZE; }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}