﻿using System;
using System.Collections.Generic;
using System.Windows.Shapes;

namespace Rave.CurveEditor.GridLines
{
    public interface IGridLinesModel
    {
        event Action SizeChanged;

        double Height { get; set; }

        double Width { get; set; }

        IList<Line> HorizontalLines { get; }

        IList<Line> VerticalLines { get; }

        IList<Line> AllLines { get; }
    }
}
