using System;
using System.ComponentModel;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor.Axes
{
    public class AxisViewModel : IAxisViewModel
    {
        private readonly IAxisModel m_model;

        public AxisViewModel(IAxisModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            m_model.LengthChanged += OnLengthChanged;
            m_model.LabelChanged += OnLabelChanged;
        }

        #region IAxisViewModel Members

        public event PropertyChangedEventHandler PropertyChanged;

        public double Length
        {
            get { return m_model.Length * m_model.Scale; }
        }

        public double ActualLength
        {
            get { return m_model.Length; }
            set { m_model.Length = value; }
        }

        public double MarkFrequency
        {
            get { return m_model.MarkFrequency * m_model.Scale; }
        }

        public double ActualMarkFrequency
        {
            get { return m_model.MarkFrequency; }
        }

        public double DrawingLength
        {
            get { return Length + MarkFrequency; }
        }

        public double Size
        {
            get { return m_model.Size; }
            set { m_model.Size = value; }
        }

        public string Label
        {
            get { return m_model.Label; }
            set { m_model.Label = value; }
        }

        public string PositiveLabel
        {
            get { return string.Format("(+ve {0})", Label); }
        }

        public string NegativeLabel
        {
            get { return string.Format("(-ve {0})", Label); }
        }

        public void Dispose()
        {
            m_model.LengthChanged -= OnLengthChanged;
            m_model.LabelChanged -= OnLabelChanged;
        }

        #endregion

        private void OnLengthChanged()
        {
            PropertyChanged.Raise(this, "Length");
            PropertyChanged.Raise(this, "ActualLength");
            PropertyChanged.Raise(this, "MarkFrequency");
            PropertyChanged.Raise(this, "ActualMarkFrequency");
            PropertyChanged.Raise(this, "DrawingLength");
        }

        private void OnLabelChanged()
        {
            PropertyChanged.Raise(this, "Label");
            PropertyChanged.Raise(this, "PositiveLabel");
            PropertyChanged.Raise(this, "NegativeLabel");
        }
    }
}