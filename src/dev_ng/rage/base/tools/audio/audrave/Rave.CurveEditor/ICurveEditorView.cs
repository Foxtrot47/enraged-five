using System;
using System.Collections.Generic;
using System.Windows;
using Rave.CurveEditor.Curves;

namespace Rave.CurveEditor
{
    public interface ICurveEditorView : IDisposable
    {
        double XAxisLength { get; set; }
        double YAxisLength { get; set; }
        bool IsCheckedOut { get; set; }
        ICurveEditorViewModel ViewModel { get; }
        event Action Dirty;

        void AddCurve(CurveTypes type, IEnumerable<Point> points);

        void RemoveCurves();
    }
}