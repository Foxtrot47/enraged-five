using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Rave.CurveEditor.GridLines;
using rage.ToolLib;
using Rave.CurveEditor.Axes;
using Rave.CurveEditor.Curves;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor
{
    public class CurveEditorViewModel : ICurveEditorViewModel
    {
        private bool m_isCheckedOut;
        private ICurveEditorModel m_model;
        private ICurveEvaluator m_selectedCurve;

        public CurveEditorViewModel(ICurveEditorModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            m_model = model;
            m_model.CurveAdded += OnCurveAdded;
            m_model.CurveRemoved += OnCurveRemoved;
            m_model.CurveEdited += OnCurveEdited;
            m_model.CanAddCurveChanged += OnCanAddCurveChanged;

            XAxis = new AxisViewModel(model.XAxis);
            YAxis = new AxisViewModel(model.YAxis);
            GridLines = new GridLinesViewModel(model.GridLines);
        }

        #region ICurveEditorViewModel Members

        public ICurveEvaluator SelectedCurve
        {
            get { return m_selectedCurve; }
            set
            {
                if (m_selectedCurve != value)
                {
                    m_selectedCurve = value;
                    PropertyChanged.Raise(this, "SelectedCurve");
                }
            }
        }

        public BindingList<ICurveEvaluator> CurveEvaluators
        {
            get { return m_model.CurveEvaluators; }
        }

        public IAxisViewModel XAxis { get; private set; }
        public IAxisViewModel YAxis { get; private set; }
        public IGridLinesViewModel GridLines { get; private set; }

        public bool IsCheckedOut
        {
            get { return m_isCheckedOut; }
            set
            {
                if (m_isCheckedOut != value)
                {
                    m_isCheckedOut = value;
                    PropertyChanged.Raise(this, "IsCheckedOut");
                }
            }
        }

        public bool CanAddCurve
        {
            get { return m_model.CanAddCurve; }
        }

        public event Action<ICurveEvaluator> CurveAdded;
        public event Action<ICurveEvaluator> CurveRemoved;
        public event Action CurveEdited;

        public void AddCurve(CurveTypes type)
        {
            m_model.AddCurve(type);
        }

        public void AddCurve(CurveTypes type, IEnumerable<Point> points)
        {
            m_model.AddCurve(type, points);
        }

        public void AddPointToCurve(Point point, ICurveEvaluator curveEvaluator)
        {
            m_model.AddPointToCurve(point, curveEvaluator.Curve);
        }

        public void UpdateCurves()
        {
            if (CurveEvaluators.Count > 0)
            {
                CurveEvaluators.ResetBindings();
                foreach (var curveEvaluator in CurveEvaluators)
                {
                    curveEvaluator.Curve.ControlPoints.ResetBindings();
                }
            }
        }

        public void RemoveCurve(ICurve curve)
        {
            m_model.RemoveCurve(curve);
        }

        public void RemoveCurves()
        {
            m_model.RemoveCurves();
        }
        
        public void ScaleCurves(double scaleX = 1, double scaleY = 1)
        {
            this.m_model.ScaleCurves(scaleX, scaleY);
        }

        public void Dispose()
        {
            m_model.CanAddCurveChanged -= OnCanAddCurveChanged;
            m_model.CurveAdded -= OnCurveAdded;
            m_model.CurveRemoved -= OnCurveRemoved;
            m_model.CurveEdited -= OnCurveEdited;

            XAxis.Dispose();
            XAxis = null;
            YAxis.Dispose();
            YAxis = null;
            GridLines.Dispose();
            GridLines = null;

            m_model = null;
            m_selectedCurve = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void OnCanAddCurveChanged()
        {
            PropertyChanged.Raise(this, "CanAddCurve");
        }

        private void OnCurveEdited()
        {
            CurveEdited.Raise();
        }

        private void OnCurveRemoved(ICurveEvaluator curveEvaluator)
        {
            CurveRemoved.Raise(curveEvaluator);
        }

        private void OnCurveAdded(ICurveEvaluator curveEvaluator)
        {
            CurveAdded.Raise(curveEvaluator);
        }
    }
}