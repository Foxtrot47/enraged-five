using System;
using System.ComponentModel;
using rage.ToolLib;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor.Curves
{
    public class Curve : ICurve
    {
        private bool m_isSelected;

        public Curve(string color)
        {
            ControlPoints = new BindingList<ControlPoint>();
            Color = color;
        }

        #region ICurve Members

        public event Action Dirty;
        public ICurveEvaluator Parent { get; set; }

        public bool IsSelected
        {
            get { return m_isSelected; }
            set
            {
                if (m_isSelected != value)
                {
                    m_isSelected = value;
                    PropertyChanged.Raise(this, "IsSelected");
                }
            }
        }

        public BindingList<ControlPoint> ControlPoints { get; private set; }

        public string Color { get; private set; }

        public void Add(ControlPoint controlPoint)
        {
            if (controlPoint.Parent == this)
            {
                controlPoint.Dirty += this.OnDirty;
                ControlPoints.Add(controlPoint);
                Dirty.Raise();
            }
        }

        public void Insert(int index, ControlPoint controlPoint)
        {
            if (controlPoint.Parent == this && index >= 0 &&
                index < ControlPoints.Count)
            {
                controlPoint.Dirty += this.OnDirty;
                ControlPoints.Insert(index, controlPoint);
                Dirty.Raise();
            }
        }

        public void Remove(ControlPoint controlPoint)
        {
            if (controlPoint.Parent == this)
            {
                controlPoint.Dirty -= this.OnDirty;
                ControlPoints.Remove(controlPoint);
                Dirty.Raise();
            }
        }

        public void Clear()
        {
            foreach (var controlPoint in ControlPoints)
            {
                controlPoint.Dirty -= this.OnDirty;
            }

            ControlPoints.Clear();
            Dirty.Raise();
        }

        private void OnDirty()
        {
            this.Dirty.Raise();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}