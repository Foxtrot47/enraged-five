using System;
using rage.ToolLib;

namespace Rave.CurveEditor.Axes
{
    public class AxisModel : IAxisModel
    {
        private const double MARK_PERCENT = 10;
        private double m_size;
        private string m_label;
        private double m_length;

        public AxisModel(string label, double halfLength, double size)
        {
            if (label == null)
            {
                throw new ArgumentNullException("label");
            }
            if (size <= 0)
            {
                throw new ArgumentException("size");
            }
            if (halfLength <= 0)
            {
                throw new ArgumentException("halfLength");
            }
            m_label = label;
            m_length = halfLength * 2;
            m_size = size;
        }

        #region IAxisModel Members

        public double Length
        {
            get { return m_length; }
            set
            {
                if (m_length != value * 2)
                {
                    m_length = value * 2;
                    LengthChanged.Raise();
                }
            }
        }

        public double MarkFrequency
        {
            get { return m_length / MARK_PERCENT; }
        }

        public double Scale
        {
            get { return m_size / m_length; }
        }

        public double Size
        {
            get { return m_size; }
            set
            {
                if (m_size != value)
                {
                    m_size = value;
                    LengthChanged.Raise();
                }
            }
        }

        public string Label
        {
            get { return m_label; }
            set
            {
                if (m_label != value)
                {
                    m_label = value;
                    LabelChanged.Raise();
                }
            }
        }

        public event Action LengthChanged;
        public event Action LabelChanged;

        #endregion
    }
}