﻿using System;
using System.Windows.Media;

namespace Rave.CurveEditor.GridLines
{
    public interface IGridLinesViewModel : IDisposable
    {
        event Action SizeChanged;

        double Height { get; set; }

        double Width { get; set; }

        GeometryGroup GridLinesGemetryGroup { get; }
    }
}
