﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using rage.ToolLib;
using Rave.CurveEditor.Curves;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor
{
    /// <summary>
    ///   Interaction logic for CurveEditorView.xaml
    /// </summary>
    public partial class CurveEditorView : ICurveEditorView
    {
        private ControlPointView m_draggedControlPoint;
        private Point m_lastPosition;

        public CurveEditorView(ICurveEditorViewModel viewModel)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException("viewModel");
            }
            ViewModel = viewModel;
            ViewModel.CurveAdded += OnCurveAddedOrRemoved;
            ViewModel.CurveRemoved += OnCurveAddedOrRemoved;
            ViewModel.CurveEdited += OnCurveEdited;
            DataContext = ViewModel;

            InitializeComponent();
        }

        #region ICurveEditorView Members

        public event Action Dirty;

        public double XAxisLength
        {
            get { return ViewModel.XAxis.ActualLength; }
            set
            {
                ViewModel.XAxis.ActualLength = value;
                ViewModel.UpdateCurves();
            }
        }

        public double YAxisLength
        {
            get { return ViewModel.YAxis.ActualLength; }
            set
            {
                ViewModel.YAxis.ActualLength = value;
                ViewModel.UpdateCurves();
            }
        }

        public bool IsCheckedOut
        {
            get { return ViewModel.IsCheckedOut; }
            set { ViewModel.IsCheckedOut = value; }
        }

        public ICurveEditorViewModel ViewModel { get; private set; }

        public void AddCurve(CurveTypes type, IEnumerable<Point> points)
        {
            ViewModel.AddCurve(type, points);
        }

        public void RemoveCurves()
        {
            ViewModel.RemoveCurves();
        }

        public void Dispose()
        {
            ViewModel.CurveAdded -= OnCurveAddedOrRemoved;
            ViewModel.CurveRemoved -= OnCurveAddedOrRemoved;
            ViewModel.CurveEdited -= OnCurveEdited;
            ViewModel.Dispose();
            ViewModel = null;
            DataContext = null;
        }

        #endregion

        private void OnCurveEdited()
        {
            Dirty.Raise();
        }

        private void OnCurveAddedOrRemoved(ICurveEvaluator curveEvaluator)
        {
            Dirty.Raise();
        }

        private void ListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_draggedControlPoint == null)
            {
                foreach (var item in e.RemovedItems)
                {
                    var curveEvaluator = item as ICurveEvaluator;
                    if (curveEvaluator != null)
                    {
                        curveEvaluator.IsSelected = false;
                    }
                }

                if (null != ViewModel)
                {
                    ViewModel.SelectedCurve = null;

                    foreach (var item in e.AddedItems)
                    {
                        var curveEvaluator = item as ICurveEvaluator;
                        if (curveEvaluator != null)
                        {
                            curveEvaluator.IsSelected = true;
                            ViewModel.SelectedCurve = curveEvaluator;
                            break;
                        }
                    }
                }
                e.Handled = true;
            }
        }

        private void OnAddClicked(object sender, RoutedEventArgs e)
        {
            ViewModel.AddCurve(CurveTypes.PiecewiseLinear);
            e.Handled = true;
        }

        private void OnRemoveClicked(object sender, RoutedEventArgs e)
        {
            if (editor.SelectedItem != null)
            {
                var curveEvaluator = editor.SelectedItem as ICurveEvaluator;
                if (curveEvaluator != null)
                {
                    ViewModel.RemoveCurve(curveEvaluator.Curve);
                }
            }
            e.Handled = true;
        }

        private void OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            m_lastPosition = editor.GetMousePosition();
        }

        private void OnPreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton ==
                MouseButtonState.Pressed)
            {
                if (m_draggedControlPoint == null)
                {
                    var originalSource = e.OriginalSource as DependencyObject;
                    var controlPointView = originalSource.FindAncestor<ControlPointView>();
                    if (controlPointView != null)
                    {
                        m_draggedControlPoint = controlPointView;
                        e.Handled = true;
                    }
                }
                else
                {
                    DoDrag();
                    e.Handled = true;
                }
            }
        }

        private void OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            m_draggedControlPoint = null;
        }

        private void OnPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var originalSource = e.OriginalSource as DependencyObject;
            var curveView = originalSource.FindAncestor<CurveView>();
            if (curveView != null)
            {
                curveView.InsertionPoint = null;

                var curveEvaluator = curveView.FindDataContext<ICurveEvaluator>();
                if (curveEvaluator != null && curveEvaluator.IsSelected)
                {
                    var point = editor.GetMousePosition();
                    var x = point.X;
                    var y = ViewModel.YAxis.DrawingLength - point.Y;

                    ClampXAndY(ref x, ref y);

                    point.X = x;
                    point.Y = y;

                    curveView.InsertionPoint = point;
                }
            }
        }

        private void OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var originalSource = e.OriginalSource as DependencyObject;
            var controlPointView = originalSource.FindAncestor<ControlPointView>();
            if (controlPointView == null)
            {
                var curveView = originalSource.FindAncestor<CurveView>();
                if (curveView != null)
                {
                    var curveEvaluator = curveView.FindDataContext<ICurveEvaluator>();
                    if (curveEvaluator != null && curveEvaluator.IsSelected)
                    {
                        var point = editor.GetMousePosition();
                        var x = point.X;
                        var y = ViewModel.YAxis.DrawingLength - point.Y;

                        ClampXAndY(ref x, ref y);

                        point.X = x;
                        point.Y = y;

                        ViewModel.AddPointToCurve(point, curveEvaluator);
                    }
                }
            }
            e.Handled = true;
        }

        private void DoDrag()
        {
            var controlPoint = m_draggedControlPoint.DataContext as ControlPoint;
            if (controlPoint != null)
            {
                var currentPosition = editor.GetMousePosition();
                var diff = m_lastPosition - currentPosition;

                var x = controlPoint.XPos - diff.X;
                var y = controlPoint.YPos + diff.Y;

                ClampXAndY(ref x, ref y);

                controlPoint.XPos = x;
                controlPoint.YPos = y;

                foreach (var curveEvaluator in ViewModel.CurveEvaluators)
                {
                    if (curveEvaluator.IsSelected)
                    {
                        editor.SelectedItem = curveEvaluator;
                        break;
                    }
                }

                m_lastPosition = currentPosition;

                Dirty.Raise();
            }
        }

        private void ClampXAndY(ref double x, ref double y)
        {
            var halfXMarkFreq = ViewModel.XAxis.MarkFrequency / 2;
            var halfYMarkFreq = ViewModel.YAxis.MarkFrequency / 2;

            if (x < halfXMarkFreq)
            {
                x = halfXMarkFreq;
            }
            if (y < halfYMarkFreq)
            {
                y = halfYMarkFreq;
            }
            if (x > ViewModel.XAxis.Length + halfXMarkFreq)
            {
                x = ViewModel.XAxis.Length + halfXMarkFreq;
            }
            if (y > ViewModel.YAxis.Length + halfYMarkFreq)
            {
                y = ViewModel.YAxis.Length + halfYMarkFreq;
            }
        }
    }
}