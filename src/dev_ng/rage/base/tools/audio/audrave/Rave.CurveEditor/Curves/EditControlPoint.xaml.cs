﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor.Curves
{
    /// <summary>
    ///   Interaction logic for EditControlPoint.xaml
    /// </summary>
    public partial class EditControlPoint
    {
        private readonly Brush m_errorBrush;
        private readonly Brush m_normalXValueBrush;
        private readonly Brush m_normalYValueBrush;

        public EditControlPoint()
        {
            InitializeComponent();
            m_normalXValueBrush = xValue.Background;
            m_normalYValueBrush = yValue.Background;
            m_errorBrush = Brushes.Tomato;
        }

        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            var controlPoint = this.FindDataContext<ControlPoint>();
            if (controlPoint != null)
            {
                var maxXVal = controlPoint.Parent.Parent.Parent.XAxis.Length / 2;
                var maxYVal = controlPoint.Parent.Parent.Parent.YAxis.Length / 2;

                double xVal, yVal;
                if (double.TryParse(xValue.Text, out xVal) &&
                    double.TryParse(yValue.Text, out yVal))
                {
                    if ((xVal >= -maxXVal && xVal <= maxXVal) &&
                        (yVal >= -maxYVal && yVal <= maxYVal))
                    {
                        var oldX = controlPoint.X;
                        controlPoint.X = xVal;
                        if (controlPoint.X !=
                            Math.Round(xVal, 3))
                        {
                            MessageBox.Show("The editor was prevented from moving the control point on the X axis.",
                                            "Invalid X MousePosition",
                                            MessageBoxButton.OK,
                                            MessageBoxImage.Exclamation);
                        }
                        else
                        {
                            controlPoint.Y = yVal;
                            if (controlPoint.Y !=
                                Math.Round(yVal, 3))
                            {
                                controlPoint.X = oldX;
                                MessageBox.Show(
                                    "The editor was prevented from moving the control point on the Y axis.",
                                    "Invalid Y MousePosition",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Exclamation);
                            }
                            else
                            {
                                controlPoint.IsEditing = false;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Coordinate outside axis range. Please change the axis range first.",
                                        "Error",
                                        MessageBoxButton.OK,
                                        MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Invalid coordinate value specified.",
                                    "Error",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Error);
                }
            }
        }

        private void OnCancelClick(object sender, RoutedEventArgs e)
        {
            var controlPoint = this.FindDataContext<ControlPoint>();
            if (controlPoint != null)
            {
                controlPoint.IsEditing = false;
            }
        }

        private void OnXValueTextChanged(object sender, TextChangedEventArgs e)
        {
            var maxVal = double.MaxValue;
            var controlPoint = this.FindDataContext<ControlPoint>();
            if (controlPoint != null)
            {
                maxVal = controlPoint.Parent.Parent.Parent.XAxis.Length / 2;
            }

            double temp;
            if (string.IsNullOrEmpty(xValue.Text) ||
                (double.TryParse(xValue.Text, out temp) && (temp >= -maxVal && temp <= maxVal)))
            {
                xValue.Background = m_normalXValueBrush;
            }
            else
            {
                xValue.Background = m_errorBrush;
            }
        }

        private void OnYValueTextChanged(object sender, TextChangedEventArgs e)
        {
            var maxVal = double.MaxValue;
            var controlPoint = this.FindDataContext<ControlPoint>();
            if (controlPoint != null)
            {
                maxVal = controlPoint.Parent.Parent.Parent.YAxis.Length / 2;
            }

            double temp;
            if (string.IsNullOrEmpty(yValue.Text) ||
                (double.TryParse(yValue.Text, out temp) && (temp >= -maxVal && temp <= maxVal)))
            {
                yValue.Background = m_normalYValueBrush;
            }
            else
            {
                yValue.Background = m_errorBrush;
            }
        }
    }
}