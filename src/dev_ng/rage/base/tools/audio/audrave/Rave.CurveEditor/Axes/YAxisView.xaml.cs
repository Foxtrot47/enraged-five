﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace Rave.CurveEditor.Axes
{
    /// <summary>
    ///   Interaction logic for YAxisView.xaml
    /// </summary>
    public partial class YAxisView
    {
        private IAxisViewModel m_viewModel;

        public YAxisView()
        {
            InitializeComponent();
        }

        private void OnUserControlLoaded(object sender, RoutedEventArgs e)
        {
            m_viewModel = DataContext as IAxisViewModel;
            if (m_viewModel != null)
            {
                m_viewModel.PropertyChanged += OnViewModelPropertyChanged;
                OnViewModelPropertyChanged(null, null);
            }
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            markings.Children.Clear();

            var halfMarkFreq = m_viewModel.MarkFrequency / 2;
            var current = m_viewModel.Length + halfMarkFreq;
            var actualCurrent = -(m_viewModel.ActualLength / 2);
            while (current >= halfMarkFreq)
            {
                var value = Math.Round(actualCurrent, 1);
                if (value != 0)
                {
                    var textBlock = new TextBlock
                                        {Style = Resources["TextBlockStyle"] as Style, Text = value.ToString()};
                    Canvas.SetRight(textBlock, 7);
                    Canvas.SetTop(textBlock, current);
                    markings.Children.Add(textBlock);

                    var line = new Line
                                   {
                                       X1 = 0,
                                       Y1 = current,
                                       X2 = -5,
                                       Y2 = current,
                                       Style = Resources["AxisLineStyle"] as Style
                                   };
                    markings.Children.Add(line);
                }
                current -= m_viewModel.MarkFrequency;
                actualCurrent += m_viewModel.ActualMarkFrequency;
            }
        }

        private void OnUserControlUnloaded(object sender, RoutedEventArgs e)
        {
            if (m_viewModel != null)
            {
                m_viewModel.PropertyChanged -= OnViewModelPropertyChanged;
                m_viewModel = null;
            }
        }
    }
}