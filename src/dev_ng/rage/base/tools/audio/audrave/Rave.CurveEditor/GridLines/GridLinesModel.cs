﻿using System;
using System.Collections.Generic;
using System.Windows.Shapes;
using rage.ToolLib;

namespace Rave.CurveEditor.GridLines
{
    public class GridLinesModel : IGridLinesModel
    {
        private const double MARK_PERCENT = 10;

        private double m_height;
        private double m_width;

        public GridLinesModel(double height, double width)
        {
            m_height = height;
            m_width = width;
            InitGridLines();
        }
        
        public event Action SizeChanged;

        public IList<Line> HorizontalLines { get; private set; }

        public IList<Line> VerticalLines { get; private set; }

        public IList<Line> AllLines
        {
            get
            {
                var tmp = new List<Line>();
                tmp.AddRange(HorizontalLines);
                tmp.AddRange(VerticalLines);
                return tmp;
            }
        } 

        public double Height
        {
            get { return m_height; }
            set
            {
                if (m_height != value)
                {
                    m_height = value;
                    InitGridLines();
                    SizeChanged.Raise();
                }
            }
        }

        public double Width
        {
            get { return m_width; }
            set
            {
                if (m_width != value)
                {
                    m_width = value;
                    InitGridLines();
                    SizeChanged.Raise();
                }
            }
        }

        private void InitGridLines()
        {
            const double lineCount = 100/MARK_PERCENT;
            var lineGapHorizontal = m_width / MARK_PERCENT;
            var lineGapVertical = m_height / MARK_PERCENT;

            HorizontalLines = new List<Line>();
            VerticalLines = new List<Line>();

            for (var i = 0; i <= lineCount; i += 1)
            {
                var horizontalLine = new Line
                {
                    X1 = 0,
                    Y1 = i * lineGapHorizontal,
                    X2 = m_width,
                    Y2 = i * lineGapHorizontal
                };
                HorizontalLines.Add(horizontalLine);

                var verticalLine = new Line
                {
                    X1 = i * lineGapVertical,
                    Y1 = 0,
                    X2 = i * lineGapVertical,
                    Y2 = m_height
                };
                VerticalLines.Add(verticalLine);
            }
        }
    }
}
