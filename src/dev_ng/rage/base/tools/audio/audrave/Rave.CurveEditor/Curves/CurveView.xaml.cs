﻿using System.Windows;

namespace Rave.CurveEditor.Curves
{
    /// <summary>
    ///   Interaction logic for CurveView.xaml
    /// </summary>
    public partial class CurveView
    {
        public CurveView()
        {
            InitializeComponent();
        }

        public Point? InsertionPoint { get; set; }

        private void OnAddControlPointClick(object sender, RoutedEventArgs e)
        {
            var evaluator = DataContext as ICurveEvaluator;
            if (evaluator != null &&
                evaluator.IsSelected)
            {
                var model = evaluator.Parent;
                if (InsertionPoint.HasValue)
                {
                    model.AddPointToCurve(InsertionPoint.Value, evaluator.Curve);
                }
            }
            e.Handled = true;
        }
    }
}