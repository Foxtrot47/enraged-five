using System;
using System.ComponentModel;

namespace Rave.CurveEditor.Curves
{
    public interface ICurve : INotifyPropertyChanged
    {
        ICurveEvaluator Parent { get; set; }
        bool IsSelected { get; set; }
        BindingList<ControlPoint> ControlPoints { get; }
        string Color { get; }
        event Action Dirty;

        void Add(ControlPoint controlPoint);

        void Insert(int index, ControlPoint controlPoint);

        void Remove(ControlPoint controlPoint);

        void Clear();
    }
}