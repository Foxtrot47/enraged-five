﻿<UserControl x:Class="Rave.CurveEditor.CurveEditorView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:Curves="clr-namespace:Rave.CurveEditor.Curves"
             xmlns:Axes="clr-namespace:Rave.CurveEditor.Axes"
             xmlns:GridLines="clr-namespace:Rave.CurveEditor.GridLines"
             Background="White">
	<UserControl.Resources>
		<DataTemplate x:Key="CurveDataTemplate">
			<Curves:CurveView/>
		</DataTemplate>

		<Style x:Key="AddMenuItemStyle"
		       TargetType="{x:Type MenuItem}">
			<Style.Triggers>
				<DataTrigger Binding="{Binding Path=CanAddCurve}"
				             Value="False">
					<Setter Property="IsEnabled"
					        Value="False"/>
				</DataTrigger>
			</Style.Triggers>
		</Style>
        
        <Style x:Key="GridLinesStyle"
               TargetType="GridLines:GridLinesView">
            <Setter Property="Background"
			        Value="AliceBlue"/>
        </Style>

		<Style x:Key="ListBoxStyle"
		       TargetType="{x:Type ListBox}">
			<Setter Property="ItemsPanel">
				<Setter.Value>
					<ItemsPanelTemplate>
						<Canvas Width="{Binding Path=XAxis.DrawingLength}"
						        Height="{Binding Path=YAxis.DrawingLength}">
							<Canvas.LayoutTransform>
								<ScaleTransform ScaleX="1"
								                ScaleY="-1"
								                CenterX="0.5"
								                CenterY="0.5"/>
							</Canvas.LayoutTransform>
						</Canvas>
					</ItemsPanelTemplate>
				</Setter.Value>
			</Setter>
			<Setter Property="ItemsSource"
			        Value="{Binding Path=CurveEvaluators}"/>
			<Setter Property="ItemTemplate"
			        Value="{StaticResource CurveDataTemplate}"/>
			<Setter Property="Canvas.Top"
			        Value="0"/>
			<Setter Property="Canvas.Left"
			        Value="0"/>
			<Setter Property="Background"
			        Value="Transparent"/>
			<Setter Property="BorderThickness"
			        Value="0"/>
			<Style.Triggers>
				<DataTrigger Binding="{Binding Path=IsCheckedOut}"
				             Value="False">
					<Setter Property="IsEnabled"
					        Value="False"/>
				</DataTrigger>
			</Style.Triggers>
		</Style>

		<Style x:Key="CanvasStyle"
		       TargetType="{x:Type Canvas}">
			<Setter Property="Width"
			        Value="{Binding Path=XAxis.DrawingLength}"/>
			<Setter Property="Height"
			        Value="{Binding Path=YAxis.DrawingLength}"/>
			<Setter Property="HorizontalAlignment"
			        Value="Center"/>
			<Setter Property="VerticalAlignment"
			        Value="Center"/>
		</Style>

		<Style x:Key="LabelTextBlockStyle"
		       TargetType="{x:Type TextBlock}">
			<Setter Property="TextAlignment"
			        Value="Center"/>
			<Setter Property="Background"
			        Value="Transparent"/>
			<Setter Property="Foreground"
			        Value="Blue"/>
			<Setter Property="FontWeight"
			        Value="Bold"/>
			<Setter Property="Margin"
			        Value="2"/>
		</Style>
	</UserControl.Resources>

	<Grid SnapsToDevicePixels="True">
		<Grid.RowDefinitions>
			<RowDefinition Height="*"/>
			<RowDefinition Height="Auto"/>
			<RowDefinition Height="Auto"/>
			<RowDefinition Height="Auto"/>
			<RowDefinition Height="*"/>
		</Grid.RowDefinitions>

		<Grid.ColumnDefinitions>
			<ColumnDefinition Width="*"/>
			<ColumnDefinition Width="Auto"/>
			<ColumnDefinition Width="Auto"/>
			<ColumnDefinition Width="Auto"/>
			<ColumnDefinition Width="*"/>
		</Grid.ColumnDefinitions>

        <GridLines:GridLinesView Style="{StaticResource GridLinesStyle}"
                                 Grid.Row="2"
                                 Grid.Column="2"
                                 ClipToBounds="True"
                                 HorizontalAlignment="Center"
                                 VerticalAlignment="Center"
                                 DataContext="{Binding Path=GridLines}"/>

        <Canvas Grid.Row="2"
		        Grid.Column="2"
		        Style="{StaticResource CanvasStyle}">
			<ListBox x:Name="editor"
			         Style="{StaticResource ListBoxStyle}"
			         PreviewMouseLeftButtonDown="OnPreviewMouseLeftButtonDown"
			         PreviewMouseMove="OnPreviewMouseMove"
			         PreviewMouseLeftButtonUp="OnPreviewMouseLeftButtonUp"
			         PreviewMouseRightButtonDown="OnPreviewMouseRightButtonDown"
			         MouseDoubleClick="OnMouseDoubleClick"
			         SelectionChanged="ListBoxSelectionChanged">
				<ListBox.ContextMenu>
					<ContextMenu>
						<MenuItem Header="Add Curve"
						          Style="{StaticResource AddMenuItemStyle}"
						          Click="OnAddClicked">
							<MenuItem.Icon>
								<Grid>
									<Rectangle Width="10"
									           Height="3"
									           Fill="Green"
									           HorizontalAlignment="Center"
									           VerticalAlignment="Center"/>
									<Rectangle Width="3"
									           Height="10"
									           Fill="Green"
									           HorizontalAlignment="Center"
									           VerticalAlignment="Center"/>
								</Grid>
							</MenuItem.Icon>
						</MenuItem>
						<MenuItem Header="Remove Curve"
						          Click="OnRemoveClicked">
							<MenuItem.Icon>
								<Grid>
									<Rectangle Width="10"
									           Height="3"
									           Fill="Red"
									           HorizontalAlignment="Center"
									           VerticalAlignment="Center"/>
								</Grid>
							</MenuItem.Icon>
						</MenuItem>
					</ContextMenu>
				</ListBox.ContextMenu>
				<ListBox.ItemContainerStyle>
					<Style>
						<Setter Property="Canvas.ZIndex"
						        Value="0"/>
						<Style.Triggers>
							<DataTrigger Binding="{Binding Path=Curve.IsSelected}"
							             Value="True">
								<Setter Property="Canvas.ZIndex"
								        Value="1"/>
							</DataTrigger>
						</Style.Triggers>
					</Style>
				</ListBox.ItemContainerStyle>
			</ListBox>
		</Canvas>
        
		<Axes:XAxisView Grid.Row="2"
		                Grid.Column="2"
		                HorizontalAlignment="Center"
		                VerticalAlignment="Center"
		                DataContext="{Binding Path=XAxis}"/>
		<TextBlock Grid.Row="2"
		           Grid.Column="3"
		           HorizontalAlignment="Right"
		           VerticalAlignment="Center"
		           Style="{StaticResource LabelTextBlockStyle}"
		           Text="{Binding Path=XAxis.PositiveLabel}"/>

		<TextBlock Grid.Row="2"
		           Grid.Column="1"
		           HorizontalAlignment="Left"
		           VerticalAlignment="Center"
		           Style="{StaticResource LabelTextBlockStyle}"
		           Text="{Binding Path=XAxis.NegativeLabel}"/>

		<Axes:YAxisView Grid.Row="2"
		                Grid.Column="2"
		                HorizontalAlignment="Center"
		                VerticalAlignment="Center"
		                DataContext="{Binding Path=YAxis}"/>
		<TextBlock Grid.Row="1"
		           Grid.Column="2"
		           VerticalAlignment="Top"
		           HorizontalAlignment="Center"
		           Style="{StaticResource LabelTextBlockStyle}"
		           Text="{Binding Path=YAxis.PositiveLabel}"/>

		<TextBlock Grid.Row="3"
		           Grid.Column="2"
		           VerticalAlignment="Bottom"
		           HorizontalAlignment="Center"
		           Style="{StaticResource LabelTextBlockStyle}"
		           Text="{Binding Path=YAxis.NegativeLabel}"/>
	</Grid>
</UserControl>