﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Rave.CurveEditor.GridLines
{
    /// <summary>
    /// Interaction logic for GridLinesView.xaml
    /// </summary>
    public partial class GridLinesView : UserControl
    {
        private IGridLinesViewModel m_viewModel;

        public GridLinesView()
        {
            InitializeComponent();
        }

        private void OnUserControlLoaded(object sender, RoutedEventArgs e)
        {
            m_viewModel = DataContext as IGridLinesViewModel;
            if (m_viewModel != null)
            {
                OnSizeChanged();
                m_viewModel.SizeChanged += OnSizeChanged;
            }
        }

        private void OnSizeChanged()
        {
            var drawing = new GeometryDrawing
            {
                Geometry = m_viewModel.GridLinesGemetryGroup,
                Pen = new Pen(Brushes.LightGray, 1)
            };

            var image = new DrawingImage(drawing);
            image.Freeze();

            Content = new Image
            {
                Source = image,
                Stretch = Stretch.None,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };
        }
    }
}
