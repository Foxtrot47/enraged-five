using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Rave.CurveEditor.GridLines;
using rage.ToolLib;
using Rave.CurveEditor.Axes;
using Rave.CurveEditor.Curves;
using Rave.CurveEditor.PiecewiseLinear;

namespace Rave.CurveEditor
{
    public class CurveEditorModel : ICurveEditorModel
    {
        private readonly Func<Point?, Point?, Point, bool> m_controlPointMovementFunc;
        private readonly CurveColors m_curveColors;
        private readonly int m_maxCurves;
        private bool m_canAddCurve;

        public CurveEditorModel(int maxCurves,
                                IAxisModel xAxisModel,
                                IAxisModel yAxisModel,
                                IGridLinesModel gridLinesModel,
                                Func<Point?, Point?, Point, bool> controlPointMovementFunc)
        {
            if (xAxisModel == null)
            {
                throw new ArgumentNullException("xAxisModel");
            }
            if (yAxisModel == null)
            {
                throw new ArgumentNullException("yAxisModel");
            }
            XAxis = xAxisModel;
            YAxis = yAxisModel;
            GridLines = gridLinesModel;

            m_curveColors = new CurveColors();
            m_maxCurves = Math.Min(maxCurves, m_curveColors.MaxCurves);
            if (m_maxCurves <= 0)
            {
                m_maxCurves = m_curveColors.MaxCurves;
            }

            m_controlPointMovementFunc = controlPointMovementFunc;

            CurveEvaluators = new BindingList<ICurveEvaluator>();
            Curves = new List<ICurve>();
            CanAddCurve = 0 < m_maxCurves;
        }

        private List<ICurve> Curves { get; set; }

        #region ICurveEditorModel Members

        public IAxisModel XAxis { get; private set; }
        public IAxisModel YAxis { get; private set; }
        public IGridLinesModel GridLines { get; private set; }
        public BindingList<ICurveEvaluator> CurveEvaluators { get; private set; }

        public bool CanAddCurve
        {
            get { return m_canAddCurve; }
            private set
            {
                if (value != m_canAddCurve)
                {
                    m_canAddCurve = value;
                    CanAddCurveChanged.Raise();
                }
            }
        }

        public event Action CanAddCurveChanged;
        public event Action<ICurveEvaluator> CurveAdded;
        public event Action<ICurveEvaluator> CurveRemoved;
        public event Action CurveEdited;

        public void AddCurve(CurveTypes type)
        {
            if (CanAddCurve)
            {
                switch (type)
                {
                    case CurveTypes.PiecewiseLinear:
                        {
                            var curve = new Curve(m_curveColors.AcquireColor());
                            PiecewiseLinearCurveEvaluator.PopulateCurve(curve,
                                                                        CanChangeValue,
                                                                        XAxis.Length / 2,
                                                                        YAxis.Length / 2);
                            var evaluator = new PiecewiseLinearCurveEvaluator(curve, this);
                            CurveEvaluators.Add(evaluator);
                            Curves.Add(curve);
                            curve.Dirty += OnDirty;
                            CurveAdded.Raise(evaluator);
                            CanAddCurve = Curves.Count < m_maxCurves;
                            break;
                        }
                }
            }
        }

        public void AddCurve(CurveTypes type, IEnumerable<Point> points)
        {
            if (CanAddCurve)
            {
                var curve = new Curve(m_curveColors.AcquireColor());
                foreach (var point in points)
                {
                    curve.Add(new ControlPoint(curve, CanChangeValue) {X = point.X, Y = point.Y});
                }

                switch (type)
                {
                    case CurveTypes.PiecewiseLinear:
                        {
                            var evaluator = new PiecewiseLinearCurveEvaluator(curve, this);
                            CurveEvaluators.Add(evaluator);
                            Curves.Add(curve);
                            curve.Dirty += OnDirty;
                            CurveAdded.Raise(evaluator);
                            CanAddCurve = Curves.Count < m_maxCurves;
                            break;
                        }
                }
            }
        }

        public void AddPointToCurve(Point point, ICurve curve)
        {
            var controlPoint = new ControlPoint(curve, CanChangeValue) { XPos = point.X, YPos = point.Y };

            var insertionIndex = 0;
            while (curve.ControlPoints[insertionIndex].X <
                   controlPoint.X)
            {
                insertionIndex += 1;
            }

            curve.Insert(insertionIndex, controlPoint);
        }

        public void RemoveCurve(ICurve curve)
        {
            var index = Curves.IndexOf(curve);
            if (index >= 0)
            {
                var evaluator = CurveEvaluators[index];
                CurveRemoved.Raise(evaluator);
                curve.Dirty -= OnDirty;
                Curves.RemoveAt(index);
                CurveEvaluators.RemoveAt(index);
                m_curveColors.ReleaseColor(curve.Color);
                evaluator.Dispose();
                CanAddCurve = Curves.Count < m_maxCurves;
            }
        }

        public void RemoveCurves()
        {
            var evaluators = CurveEvaluators.ToList();
            foreach (var evaluator in evaluators)
            {
                var curve = evaluator.Curve;
                var index = Curves.IndexOf(curve);
                CurveRemoved.Raise(evaluator);
                curve.Dirty -= OnDirty;
                Curves.RemoveAt(index);
                CurveEvaluators.RemoveAt(index);
                m_curveColors.ReleaseColor(curve.Color);
                evaluator.Dispose();
            }
            CanAddCurve = 0 < m_maxCurves;
        }

        public void ScaleCurves(double scaleX = 1, double scaleY = 1)
        {
            foreach (var curve in Curves)
            {
                var points = scaleX < 1 ? 
                    curve.ControlPoints.OrderBy(point => point.X) :
                    curve.ControlPoints.OrderBy(point => point.X * -1);
                
                foreach (var point in points)
                {
                    point.X *= scaleX;
                }

                points = scaleY < 1 ?
                    curve.ControlPoints.OrderBy(point => point.Y) :
                    curve.ControlPoints.OrderBy(point => point.Y * -1);

                foreach (var point in points)
                {
                    point.Y *= scaleY;
                }
            }

            CurveEdited.Raise();
        }

        #endregion

        private void OnDirty()
        {
            CurveEdited.Raise();
        }

        private bool CanChangeValue(ControlPoint controlPoint, double x, double y)
        {
            var canChangeValue = true;
            if (m_controlPointMovementFunc != null)
            {
                var controlPoints = controlPoint.Parent.ControlPoints;
                var controlPointIndex = controlPoints.IndexOf(controlPoint);
                if (controlPointIndex >= 0)
                {
                    var prevControlPointIndex = controlPointIndex - 1;
                    Point? prevControlPoint = null;
                    if (prevControlPointIndex >= 0 &&
                        controlPointIndex < controlPoints.Count)
                    {
                        prevControlPoint = new Point(controlPoints[prevControlPointIndex].X,
                                                     controlPoints[prevControlPointIndex].Y);
                    }

                    var nextControlPointIndex = controlPointIndex + 1;
                    Point? nextControlPoint = null;
                    if (nextControlPointIndex >= 0 &&
                        nextControlPointIndex < controlPoints.Count)
                    {
                        nextControlPoint = new Point(controlPoints[nextControlPointIndex].X,
                                                     controlPoints[nextControlPointIndex].Y);
                    }

                    canChangeValue = m_controlPointMovementFunc(prevControlPoint, nextControlPoint, new Point(x, y));
                }
            }
            return canChangeValue;
        }
    }
}