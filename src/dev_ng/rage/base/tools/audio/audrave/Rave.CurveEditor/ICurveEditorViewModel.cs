using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Rave.CurveEditor.Axes;
using Rave.CurveEditor.Curves;
using Rave.CurveEditor.GridLines;

namespace Rave.CurveEditor
{
    public interface ICurveEditorViewModel : INotifyPropertyChanged,
                                             IDisposable
    {
        ICurveEvaluator SelectedCurve { get; set; }
        BindingList<ICurveEvaluator> CurveEvaluators { get; }
        IAxisViewModel XAxis { get; }
        IAxisViewModel YAxis { get; }
        IGridLinesViewModel GridLines { get; }
        bool IsCheckedOut { get; set; }
        bool CanAddCurve { get; }
        event Action<ICurveEvaluator> CurveAdded;
        event Action<ICurveEvaluator> CurveRemoved;
        event Action CurveEdited;

        void AddCurve(CurveTypes type);

        void AddCurve(CurveTypes type, IEnumerable<Point> points);

        void AddPointToCurve(Point point, ICurveEvaluator curveEvaluator);

        void UpdateCurves();

        void RemoveCurve(ICurve curve);

        void RemoveCurves();

        void ScaleCurves(double scaleX = 1, double scaleY = 1);
    }
}