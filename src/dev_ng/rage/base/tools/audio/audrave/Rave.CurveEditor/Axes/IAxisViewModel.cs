using System;
using System.ComponentModel;

namespace Rave.CurveEditor.Axes
{
    public interface IAxisViewModel : INotifyPropertyChanged,
                                      IDisposable
    {
        double Length { get; }
        double ActualLength { get; set; }
        double MarkFrequency { get; }
        double ActualMarkFrequency { get; }
        double DrawingLength { get; }
        string Label { get; set; }
        string PositiveLabel { get; }
        string NegativeLabel { get; }
        double Size { get; set; }
    }
}