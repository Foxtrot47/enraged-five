﻿using System.Windows;
using System.Windows.Input;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor.Curves
{
    /// <summary>
    ///   Interaction logic for ControlPointView.xaml
    /// </summary>
    public partial class ControlPointView
    {
        public ControlPointView()
        {
            InitializeComponent();
        }

        private void OnRemoveControlPointClick(object sender, RoutedEventArgs e)
        {
            var controlPoint = DataContext as ControlPoint;
            if (controlPoint != null)
            {
                var curve = controlPoint.Parent;
                if (curve.ControlPoints.Count > 2)
                {
                    curve.Remove(controlPoint);
                }
            }
            e.Handled = true;
        }

        private void OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var obj = sender as FrameworkElement;
            if (obj != null)
            {
                var controlPoint = obj.FindDataContext<ControlPoint>();
                if (controlPoint != null)
                {
                    controlPoint.IsEditing = true;
                }
            }
            e.Handled = true;
        }
    }
}