using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Rave.CurveEditor.Axes;
using Rave.CurveEditor.Curves;
using Rave.CurveEditor.GridLines;

namespace Rave.CurveEditor
{
    public interface ICurveEditorModel
    {
        BindingList<ICurveEvaluator> CurveEvaluators { get; }
        IAxisModel XAxis { get; }
        IAxisModel YAxis { get; }
        IGridLinesModel GridLines { get; }
        bool CanAddCurve { get; }

        event Action CanAddCurveChanged;
        event Action<ICurveEvaluator> CurveAdded;
        event Action<ICurveEvaluator> CurveRemoved;
        event Action CurveEdited;

        void AddCurve(CurveTypes type);

        void AddCurve(CurveTypes type, IEnumerable<Point> points);

        void AddPointToCurve(Point point, ICurve curve);

        void RemoveCurve(ICurve curve);

        void RemoveCurves();

        void ScaleCurves(double scaleX = 1, double scaleY = 1);
    }
}