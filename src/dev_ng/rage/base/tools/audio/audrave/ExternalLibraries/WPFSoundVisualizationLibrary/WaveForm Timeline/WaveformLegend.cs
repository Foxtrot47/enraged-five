﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using WPFSoundVisualizationLib.WaveForm_Timeline;

namespace WPFSoundVisualizationLib
{
	[DisplayName("Waveform Legend")]
	[Description("Displays the legend for the timeline")]
	[ToolboxItem(true)]
	[TemplatePart(Name = "PART_Legend", Type = typeof(Canvas))]
	public class WaveformLegend : Control
	{
		private Canvas _legendCanvas;
		private Canvas _spectrumCanvas;

		private Dictionary<float, Color> GetLegendColors(float height)
		{
			Dictionary<float, Color> dbToColorDictionary = new Dictionary<float, Color>();
			float max = height;
			float arraylength = 100.0f;
			float k = arraylength / (max * 2);
			float inc = ((arraylength) / (max * max));
			float minDBValue = -100f;
			for (float j = -arraylength; j <= 0; j += k)
			{
				Color color = HsbHelper.GetColorFromDb(j, minDBValue);
				dbToColorDictionary[j] = color;
				k += inc;
			}

			return dbToColorDictionary;
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			_legendCanvas = GetTemplateChild("PART_Legend") as Canvas;
			if (_legendCanvas != null)
			{
				_spectrumCanvas = GetLegend((float)(ActualWidth), (float)(ActualHeight));
				_legendCanvas.Children.Add(_spectrumCanvas);
				this.SizeChanged += (p, r) =>
				{
					_legendCanvas.Children.Remove(_spectrumCanvas);
					_spectrumCanvas = GetLegend((float)(ActualWidth), (float)(ActualHeight));
					_legendCanvas.Children.Add(_spectrumCanvas);
				};
			}

		}



		static WaveformLegend()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(WaveformLegend),
				new FrameworkPropertyMetadata(typeof(WaveformLegend)));
		}

		private Canvas GetLegend(float width, float height)
		{

			if (float.IsNaN(width) || width < 20f)
			{
				width = MinWidth < 20f ? 100f : (float)MinWidth;
			}

			if (float.IsNaN(height) || height < 20f)
			{
				height = MinHeight < 20f ? 20f : (float)MinHeight;
			}

			bool drawHorizontal = width > height;
			var writableWaveFormImage = drawHorizontal ?
				BitmapFactory.New((int)width, (int)height / 2) : BitmapFactory.New((int)width / 2, (int)height);

			Dictionary<float, Color> dictionary = GetLegendColors(50);
			float maxValue = drawHorizontal ? width : height;
			writableWaveFormImage.Clear();
			using (writableWaveFormImage.GetBitmapContext())
			{


				float divider = maxValue / dictionary.Count;
				float currentValue = maxValue;
				foreach (float dbValue in dictionary.Keys)
				{
					try
					{


						if (drawHorizontal)
						{
							writableWaveFormImage.FillRectangle((int)(currentValue - divider), (int)(height / 3), (int)currentValue, 0,
								dictionary[dbValue]);
						}
						else
						{
							writableWaveFormImage.FillRectangle(0, (int)currentValue, (int)width / 3, (int)(currentValue - divider),
								dictionary[dbValue]);
						}
						currentValue -= divider;
					}
					catch
					{
						break;
					}
				}


			}
			Canvas canvas = new Canvas();
			maxValue = drawHorizontal ? width : height;
			int fontsize = drawHorizontal ? (int)(maxValue / (10 * 2)) : (int)(maxValue / (10 * 3.5));
			for (int i = 0; i < dictionary.Keys.Count; i += dictionary.Keys.Count / 10)
			{
				if (drawHorizontal)
				{
					canvas.Children.Add(new TextBlock()
					{
						Margin =
						new Thickness(((double)1 - ((double)i / (double)dictionary.Keys.Count)) * width + fontsize, height / 3, 0, 0

							),
						Width = width / 10,
						FontSize = fontsize,
						Text = string.Format(new NumberFormatInfo(), "{0:##}", dictionary.Keys.ToArray()[i])
					});
				}
				else
				{
					canvas.Children.Add(new TextBlock()
					{
						Margin =
						new Thickness(width / 3,
							((double)1 - ((double)i / (double)dictionary.Keys.Count)) * height - fontsize, 0, 0),
						Height = height / 10,
						Width = (width/3) * 2,
						FontSize = fontsize,
						Text = string.Format(new NumberFormatInfo(), "{0:##.#db}", dictionary.Keys.ToArray()[i])
					});
				}

			}
			if (drawHorizontal)
			{
				canvas.Children.Add(new TextBlock()
				{
					Margin =
						new Thickness(0, height / 3, 0, 0

							),
					Width = width / 10,
					FontSize = fontsize,
					Text = "0db"
				});
			}
			else
			{


				canvas.Children.Add(new TextBlock()
						{
							Margin =
							new Thickness(width / 3,
								0, 0, 0),
							Height = height / 10,
							FontSize = fontsize,
							Text = "0db"
						});
			}
			canvas.Margin = new Thickness(10);
			canvas.Children.Add(new Image { Source = writableWaveFormImage });
			return canvas;
		}
	}
}
