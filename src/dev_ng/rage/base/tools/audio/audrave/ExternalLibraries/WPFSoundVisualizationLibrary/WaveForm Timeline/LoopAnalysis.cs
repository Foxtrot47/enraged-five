﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WPFSoundVisualizationLib
{
	public class LoopAnalysis : Control
	{
		private Canvas _loopCanvas;
		private readonly Path _waveform = new Path();
		private IWaveformPlayer _soundPlayer;
		private Line _verticalLine;
		private Line _horizontalLine;

		public IWaveformPlayer SoundSource
		{
			get
			{
				return _soundPlayer;
			}
			set
			{
				this._soundPlayer = value;
			}
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			_loopCanvas = GetTemplateChild("PART_LoopCanvas") as Canvas;
			if (_loopCanvas != null)
			{

				_verticalLine = new Line { X1 = _loopCanvas.RenderSize.Width / 2, X2 = _loopCanvas.RenderSize.Width / 2, Y1 = 0, Y2 = _loopCanvas.RenderSize.Height, Stroke = Brushes.Black, StrokeThickness = 1 };
				_horizontalLine = new Line { X1 = 0, X2 = _loopCanvas.RenderSize.Width, Y1 = _loopCanvas.RenderSize.Height / 2, Y2 = _loopCanvas.RenderSize.Height / 2, Stroke = Brushes.Black, StrokeThickness = 1 };
				_loopCanvas.Children.Add(_verticalLine);
				_loopCanvas.Children.Add(_horizontalLine);
				_waveform.Stroke = Brushes.Blue;
				_waveform.StrokeThickness = 1;
				_loopCanvas.Children.Add(_waveform);
				this.SizeChanged += (p, r) =>
				{
					_verticalLine.X1 = _loopCanvas.RenderSize.Width / 2;
					_verticalLine.X2 = _loopCanvas.RenderSize.Width / 2;
					_verticalLine.Y1 = 0;
					_verticalLine.Y2 = _loopCanvas.RenderSize.Height;
					_horizontalLine.X1 = 0;
					_horizontalLine.X2 = _loopCanvas.RenderSize.Width;
					_horizontalLine.Y1 = _loopCanvas.RenderSize.Height / 2;
					_horizontalLine.Y2 = _loopCanvas.RenderSize.Height / 2;
					UpdateWaveForm();
				};
			}
		}

		public void RegisterSoundPlayer(IWaveformPlayer soundPlayer)
		{
			_soundPlayer = soundPlayer;
			soundPlayer.PropertyChanged += soundPlayer_PropertyChanged;
		}

		private void soundPlayer_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "LoopData")
			{
				UpdateWaveForm();
			}
		}

		private void UpdateWaveForm()
		{
			if (_loopCanvas == null || _soundPlayer == null || _soundPlayer.LoopData == null)
			{
				return;
			}
			List<Point> list = new List<Point>();
			for (int i = 0; i < _soundPlayer.LoopData.Length; i++)
			{
				double xPosition = (i / (double)_soundPlayer.LoopData.Length) * _loopCanvas.RenderSize.Width;
				double yPosition = (((double)_soundPlayer.LoopData[i] + (double)short.MaxValue) / (double)(short.MaxValue - short.MinValue)) * _loopCanvas.RenderSize.Height;
				list.Add(new Point(xPosition, yPosition));
			}

			var loopPolyline = new PolyLineSegment { Points = new PointCollection(list) };
			var loopPathFigure = new PathFigure();
			loopPathFigure.Segments.Add(loopPolyline);
			PathGeometry loopGeometry = new PathGeometry();
			loopGeometry.Figures.Add(loopPathFigure);

			_waveform.Data = loopGeometry;
		}

		static LoopAnalysis()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(LoopAnalysis),
				new FrameworkPropertyMetadata(typeof(LoopAnalysis)));
		}
	}
}
