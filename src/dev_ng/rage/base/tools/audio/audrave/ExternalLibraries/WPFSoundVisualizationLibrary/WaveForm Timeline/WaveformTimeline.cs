﻿using System;
using System.CodeDom;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFSoundVisualizationLib.WaveForm_Timeline;

namespace WPFSoundVisualizationLib
{
	/// <summary>
	///     A control that displays a stereo waveform and
	///     allows a user to change playback position.
	/// </summary>
	[DisplayName("Waveform Timeline")]
	[Description("Displays a stereo waveform and allows a user to change playback position.")]
	[ToolboxItem(true)]
	[TemplatePart(Name = "PART_Waveform", Type = typeof(Canvas)),
	 TemplatePart(Name = "PART_Timeline", Type = typeof(Canvas)),
	 TemplatePart(Name = "PART_Repeat", Type = typeof(Canvas)),
	 TemplatePart(Name = "PART_Progress", Type = typeof(Canvas))]
	public class WaveformTimeline : UserControl
	{
		private const int IndicatorTriangleWidth = 4;
		private const int MajorTickHeight = 10;
		private const int MinorTickHeight = 3;
		private const int MouseMoveTolerance = 3;
		private const int TimeStampMargin = 5;
		private readonly Line _centerLine = new Line();
		private readonly Path _leftPath = new Path();
		private readonly Dictionary<double, string> _markerDictionary = new Dictionary<double, string>();
		private readonly Path _progressIndicator = new Path();
		private readonly Line _progressLine = new Line();
		private readonly Rectangle _repeatRegion = new Rectangle();
		private readonly Path _rightPath = new Path();
		private readonly Rectangle _timelineBackgroundRegion = new Rectangle();
		private readonly List<Line> _timeLineTicks = new List<Line>();
		private readonly List<TextBlock> _timestampTextBlocks = new List<TextBlock>();
		private CancellationTokenSource _cancellationToken = new CancellationTokenSource();
		private List<Line> _cueIndicators = new List<Line>();
		private Point _currentPoint;
		private double _endLoopRegion = -1;
		private double _fftRectHeight;
		private double _fftRectTickness;
		private bool _isMouseDown;
		private Point _mouseDownPoint;
		private Canvas _progressCanvas;
		private Canvas _repeatCanvas;
		private CancellationTokenSource _spectrumCancellationToken = new CancellationTokenSource();
		private double _startLoopRegion = -1;
		private Canvas _timelineCanvas;
		private Canvas _waveformCanvas;
		private Image _waveFormImage;
		private WriteableBitmap _writableWaveFormImage;

		static WaveformTimeline()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(WaveformTimeline), new FrameworkPropertyMetadata(typeof(WaveformTimeline)));
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			if (SoundSource == null) return;
			_waveformCanvas = GetTemplateChild("PART_Waveform") as Canvas;

			_waveFormImage = new Image();

			if (_waveformCanvas != null)
			{
				_waveformCanvas.CacheMode = new BitmapCache();
				// Used to make the transparent regions clickable.
				_waveformCanvas.Background = new SolidColorBrush(Colors.Black);
				_waveformCanvas.Children.Add(_waveFormImage);
				_waveformCanvas.Children.Add(_centerLine);
				_waveformCanvas.Children.Add(_rightPath);
				_waveformCanvas.Children.Add(_leftPath);
			}

			_timelineCanvas = GetTemplateChild("PART_Timeline") as Canvas;
			if (_timelineCanvas != null)
			{
				_timelineCanvas.Children.Add(_timelineBackgroundRegion);
				_timelineCanvas.SizeChanged += timelineCanvas_SizeChanged;
			}

			_repeatCanvas = GetTemplateChild("PART_Repeat") as Canvas;
			if (_repeatCanvas != null)
			{
				_repeatCanvas.Children.Add(_repeatRegion);
			}

			_progressCanvas = GetTemplateChild("PART_Progress") as Canvas;
			if (_progressCanvas != null)
			{
				_progressCanvas.Children.Add(_progressIndicator);
				_progressCanvas.Children.Add(_progressLine);
			}
			UpdateWaveformCacheScaling();
		}

		/// <summary>
		///     Gets or sets a value that indicates whether repeat regions will be created via mouse drag across the waveform.
		/// </summary>
		[Category("Common")]
		public bool AllowRepeatRegions
		{
			// IMPORTANT: To maintain parity between setting a property in XAML and procedural code, do not touch the getter and setter inside this dependency property!
			get { return (bool)GetValue(AllowRepeatRegionsProperty); }
			set { SetValue(AllowRepeatRegionsProperty, value); }
		}

		/// <summary>
		///     Gets or sets a value indicating whether the waveform should attempt to autoscale
		///     its render buffer in size.
		/// </summary>
		/// <remarks>
		///     If true, the control will attempt to set the waveform's bitmap cache
		///     at a resolution based on the sum of all ScaleTransforms applied
		///     in the control's visual tree heirarchy. This can make the waveform appear
		///     less blurry if a ScaleTransform is applied at a higher level.
		///     The only ScaleTransforms that are considered here are those that have
		///     uniform vertical and horizontal scaling (generally used to "zoom in"
		///     on a window or controls).
		/// </remarks>
		[Category("Common")]
		public bool AutoScaleWaveformCache
		{
			// IMPORTANT: To maintain parity between setting a property in XAML and procedural code, do not touch the getter and setter inside this dependency property!
			get { return (bool)GetValue(AutoScaleWaveformCacheProperty); }
			set { SetValue(AutoScaleWaveformCacheProperty, value); }
		}

		/// <summary>
		///     Gets or sets a brush used to draw the center line separating left and right levels.
		/// </summary>
		[Category("Brushes")]
		public Brush CenterLineBrush
		{
			// IMPORTANT: To maintain parity between setting a property in XAML and procedural code, do not touch the getter and setter inside this dependency property!
			get { return (Brush)GetValue(CenterLineBrushProperty); }
			set { SetValue(CenterLineBrushProperty, value); }
		}

		/// <summary>
		///     Gets or sets the thickness of the center line separating left and right levels.
		/// </summary>
		[Category("Common")]
		public double CenterLineThickness
		{
			// IMPORTANT: To maintain parity between setting a property in XAML and procedural code, do not touch the getter and setter inside this dependency property!
			get { return (double)GetValue(CenterLineThicknessProperty); }
			set { SetValue(CenterLineThicknessProperty, value); }
		}

		/// <summary>
		///     Gets or sets a brush used to draw the left channel output on the waveform.
		/// </summary>
		[Category("Brushes")]
		public Brush LeftLevelBrush
		{
			// IMPORTANT: To maintain parity between setting a property in XAML and procedural code, do not touch the getter and setter inside this dependency property!
			get { return (Brush)GetValue(LeftLevelBrushProperty); }
			set { SetValue(LeftLevelBrushProperty, value); }
		}

		/// <summary>
		///     Gets or sets a brush used to draw the track progress indicator bar.
		/// </summary>
		[Category("Brushes")]
		public Brush ProgressBarBrush
		{
			// IMPORTANT: To maintain parity between setting a property in XAML and procedural code, do not touch the getter and setter inside this dependency property!
			get { return (Brush)GetValue(ProgressBarBrushProperty); }
			set { SetValue(ProgressBarBrushProperty, value); }
		}

		/// <summary>
		///     Get or sets the thickness of the progress indicator bar.
		/// </summary>
		[Category("Common")]
		public double ProgressBarThickness
		{
			// IMPORTANT: To maintain parity between setting a property in XAML and procedural code, do not touch the getter and setter inside this dependency property!
			get { return (double)GetValue(ProgressBarThicknessProperty); }
			set { SetValue(ProgressBarThicknessProperty, value); }
		}

		/// <summary>
		///     Gets or sets a brush used to draw the repeat region on the waveform.
		/// </summary>
		[Category("Brushes")]
		public Brush RepeatRegionBrush
		{
			// IMPORTANT: To maintain parity between setting a property in XAML and procedural code, do not touch the getter and setter inside this dependency property!
			get { return (Brush)GetValue(RepeatRegionBrushProperty); }
			set { SetValue(RepeatRegionBrushProperty, value); }
		}

		/// <summary>
		///     Gets or sets a brush used to draw the right speaker levels on the waveform.
		/// </summary>
		[Category("Brushes")]
		public Brush RightLevelBrush
		{
			// IMPORTANT: To maintain parity between setting a property in XAML and procedural code, do not touch the getter and setter inside this dependency property!
			get { return (Brush)GetValue(RightLevelBrushProperty); }
			set { SetValue(RightLevelBrushProperty, value); }
		}

		/// <summary>
		///     Gets or sets a brush used to draw the tickmarks on the timeline.
		/// </summary>
		[Category("Brushes")]
		public Brush TimelineTickBrush
		{
			// IMPORTANT: To maintain parity between setting a property in XAML and procedural code, do not touch the getter and setter inside this dependency property!
			get { return (Brush)GetValue(TimelineTickBrushProperty); }
			set { SetValue(TimelineTickBrushProperty, value); }
		}

		public IWaveformPlayer SoundSource
		{
			get
			{
				return (IWaveformPlayer)GetValue(SoundSourceProperty);

			}
			set { SetValue(SoundSourceProperty, value); }
		}

		/// <summary>
		/// Clears the waveform
		/// </summary>
		public void Clear()
		{
			if (_waveFormImage != null)
			{
				_waveFormImage.Source = null;
			}
			_rightPath.Data = new PathGeometry();
			_leftPath.Data = new PathGeometry();
		}


		/// <summary>
		///     Called after the <see cref="AllowRepeatRegions" /> value has changed.
		/// </summary>
		/// <param name="oldValue">The previous value of <see cref="AllowRepeatRegions" /></param>
		/// <param name="newValue">The new value of <see cref="AllowRepeatRegions" /></param>
		protected virtual void OnAllowRepeatRegionsChanged(bool oldValue, bool newValue)
		{
			if (!newValue && SoundSource != null)
			{
				SoundSource.SelectionBegin = TimeSpan.Zero;
				SoundSource.SelectionEnd = TimeSpan.Zero;
			}
		}

		/// <summary>
		///     Called after the <see cref="AutoScaleWaveformCache" /> value has changed.
		/// </summary>
		/// <param name="oldValue">The previous value of <see cref="AutoScaleWaveformCache" /></param>
		/// <param name="newValue">The new value of <see cref="AutoScaleWaveformCache" /></param>
		protected virtual void OnAutoScaleWaveformCacheChanged(bool oldValue, bool newValue)
		{
			UpdateWaveformCacheScaling();
		}

		/// <summary>
		///     Called after the <see cref="CenterLineBrush" /> value has changed.
		/// </summary>
		/// <param name="oldValue">The previous value of <see cref="CenterLineBrush" /></param>
		/// <param name="newValue">The new value of <see cref="CenterLineBrush" /></param>
		protected virtual void OnCenterLineBrushChanged(Brush oldValue, Brush newValue)
		{
			_centerLine.Stroke = CenterLineBrush;
			UpdateWaveform();
		}

		/// <summary>
		///     Called after the <see cref="CenterLineThickness" /> value has changed.
		/// </summary>
		/// <param name="oldValue">The previous value of <see cref="CenterLineThickness" /></param>
		/// <param name="newValue">The new value of <see cref="CenterLineThickness" /></param>
		protected virtual void OnCenterLineThicknessChanged(double oldValue, double newValue)
		{
			_centerLine.StrokeThickness = CenterLineThickness;
			UpdateWaveform();
		}

		/// <summary>
		///     Coerces the value of <see cref="AllowRepeatRegions" /> when a new value is applied.
		/// </summary>
		/// <param name="value">The value that was set on <see cref="AllowRepeatRegions" /></param>
		/// <returns>The adjusted value of <see cref="AllowRepeatRegions" /></returns>
		protected virtual bool OnCoerceAllowRepeatRegions(bool value)
		{
			return value;
		}

		/// <summary>
		///     Coerces the value of <see cref="AutoScaleWaveformCache" /> when a new value is applied.
		/// </summary>
		/// <param name="value">The value that was set on <see cref="AutoScaleWaveformCache" /></param>
		/// <returns>The adjusted value of <see cref="AutoScaleWaveformCache" /></returns>
		protected virtual bool OnCoerceAutoScaleWaveformCache(bool value)
		{
			return value;
		}

		/// <summary>
		///     Coerces the value of <see cref="CenterLineBrush" /> when a new value is applied.
		/// </summary>
		/// <param name="value">The value that was set on <see cref="CenterLineBrush" /></param>
		/// <returns>The adjusted value of <see cref="CenterLineBrush" /></returns>
		protected virtual Brush OnCoerceCenterLineBrush(Brush value)
		{
			return value;
		}

		/// <summary>
		///     Coerces the value of <see cref="CenterLineThickness" /> when a new value is applied.
		/// </summary>
		/// <param name="value">The value that was set on <see cref="CenterLineThickness" /></param>
		/// <returns>The adjusted value of <see cref="CenterLineThickness" /></returns>
		protected virtual double OnCoerceCenterLineThickness(double value)
		{
			value = Math.Max(value, 0.0d);
			return value;
		}

		/// <summary>
		///     Coerces the value of <see cref="LeftLevelBrush" /> when a new value is applied.
		/// </summary>
		/// <param name="value">The value that was set on <see cref="LeftLevelBrush" /></param>
		/// <returns>The adjusted value of <see cref="LeftLevelBrush" /></returns>
		protected virtual Brush OnCoerceLeftLevelBrush(Brush value)
		{
			return value;
		}

		/// <summary>
		///     Coerces the value of <see cref="ProgressBarBrush" /> when a new value is applied.
		/// </summary>
		/// <param name="value">The value that was set on <see cref="ProgressBarBrush" /></param>
		/// <returns>The adjusted value of <see cref="ProgressBarBrush" /></returns>
		protected virtual Brush OnCoerceProgressBarBrush(Brush value)
		{
			return value;
		}

		/// <summary>
		///     Coerces the value of <see cref="ProgressBarThickness" /> when a new value is applied.
		/// </summary>
		/// <param name="value">The value that was set on <see cref="ProgressBarThickness" /></param>
		/// <returns>The adjusted value of <see cref="ProgressBarThickness" /></returns>
		protected virtual double OnCoerceProgressBarThickness(double value)
		{
			value = Math.Max(value, 0.0d);
			return value;
		}

		/// <summary>
		///     Coerces the value of <see cref="RepeatRegionBrush" /> when a new value is applied.
		/// </summary>
		/// <param name="value">The value that was set on <see cref="RepeatRegionBrush" /></param>
		/// <returns>The adjusted value of <see cref="RepeatRegionBrush" /></returns>
		protected virtual Brush OnCoerceRepeatRegionBrush(Brush value)
		{
			return value;
		}

		/// <summary>
		///     Coerces the value of <see cref="RightLevelBrush" /> when a new value is applied.
		/// </summary>
		/// <param name="value">The value that was set on <see cref="RightLevelBrush" /></param>
		/// <returns>The adjusted value of <see cref="RightLevelBrush" /></returns>
		protected virtual Brush OnCoerceRightLevelBrush(Brush value)
		{
			return value;
		}

		/// <summary>
		///     Coerces the value of <see cref="TimelineTickBrush" /> when a new value is applied.
		/// </summary>
		/// <param name="value">The value that was set on <see cref="TimelineTickBrush" /></param>
		/// <returns>The adjusted value of <see cref="TimelineTickBrush" /></returns>
		protected virtual Brush OnCoerceTimelineTickBrush(Brush value)
		{
			return value;
		}

		/// <summary>
		///     Called after the <see cref="LeftLevelBrush" /> value has changed.
		/// </summary>
		/// <param name="oldValue">The previous value of <see cref="LeftLevelBrush" /></param>
		/// <param name="newValue">The new value of <see cref="LeftLevelBrush" /></param>
		protected virtual void OnLeftLevelBrushChanged(Brush oldValue, Brush newValue)
		{
			var fillBrush = LeftLevelBrush.Clone();
			fillBrush.Opacity *= 0.75;
			_leftPath.Fill = fillBrush;
			_leftPath.Stroke = LeftLevelBrush;
			_leftPath.StrokeThickness = 1;
			UpdateWaveform();
		}

		/// <summary>
		///     Invoked when an unhandled MouseLeftButtonDown routed event is raised on this element. Implement this method to add
		///     class handling for this event.
		/// </summary>
		/// <param name="e">
		///     The MouseButtonEventArgs that contains the event data. The event data reports that the left mouse
		///     button was pressed.
		/// </param>
		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			base.OnMouseLeftButtonDown(e);
			CaptureMouse();
			_isMouseDown = true;
			_mouseDownPoint = e.GetPosition(_waveformCanvas);
		}

		/// <summary>
		///     Invoked when an unhandled MouseLeftButtonUp routed event reaches an element in its route that is derived from this
		///     class. Implement this method to add class handling for this event.
		/// </summary>
		/// <param name="e">
		///     The MouseButtonEventArgs that contains the event data. The event data reports that the left mouse
		///     button was released.
		/// </param>
		protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
		{
			base.OnMouseLeftButtonUp(e);
			if (!_isMouseDown)
				return;

			var updateRepeatRegion = false;
			_isMouseDown = false;
			ReleaseMouseCapture();
			if (Math.Abs(_currentPoint.X - _mouseDownPoint.X) < MouseMoveTolerance)
			{
				if (PointInRepeatRegion(_mouseDownPoint))
				{
					var position = (_currentPoint.X / RenderSize.Width) * SoundSource.ChannelLength;
					SoundSource.ChannelPosition = Math.Min(SoundSource.ChannelLength, Math.Max(0, position));
				}
				else
				{
					SoundSource.SelectionBegin = TimeSpan.Zero;
					SoundSource.SelectionEnd = TimeSpan.Zero;
					var position = (_currentPoint.X / RenderSize.Width) * SoundSource.ChannelLength;
					SoundSource.ChannelPosition = Math.Min(SoundSource.ChannelLength, Math.Max(0, position));
					_startLoopRegion = -1;
					_endLoopRegion = -1;
					updateRepeatRegion = true;
				}
			}
			else
			{
				SoundSource.SelectionBegin = TimeSpan.FromSeconds(_startLoopRegion);
				SoundSource.SelectionEnd = TimeSpan.FromSeconds(_endLoopRegion);
				var position = _startLoopRegion;
				SoundSource.ChannelPosition = Math.Min(SoundSource.ChannelLength, Math.Max(0, position));
				updateRepeatRegion = true;
			}

			if (updateRepeatRegion)
				UpdateRepeatRegion();
		}

		/// <summary>
		///     Invoked when an unhandled Mouse.MouseMove attached event reaches an element in its route that is derived from this
		///     class. Implement this method to add class handling for this event.
		/// </summary>
		/// <param name="e">The MouseEventArgs that contains the event data.</param>
		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);
			_currentPoint = e.GetPosition(_waveformCanvas);

			if (_isMouseDown && AllowRepeatRegions)
			{
				if (Math.Abs(_currentPoint.X - _mouseDownPoint.X) > MouseMoveTolerance)
				{
					if (_mouseDownPoint.X < _currentPoint.X)
					{
						_startLoopRegion = (_mouseDownPoint.X / RenderSize.Width) * SoundSource.ChannelLength;
						_endLoopRegion = (_currentPoint.X / RenderSize.Width) * SoundSource.ChannelLength;
					}
					else
					{
						_startLoopRegion = (_currentPoint.X / RenderSize.Width) * SoundSource.ChannelLength;
						_endLoopRegion = (_mouseDownPoint.X / RenderSize.Width) * SoundSource.ChannelLength;
					}
				}
				else
				{
					_startLoopRegion = -1;
					_endLoopRegion = -1;
				}
				UpdateRepeatRegion();
			}

			if (pointToFrequencyDb != null && pointToFrequencyDb.Count > 0)
			{
				Point p = e.GetPosition(_waveFormImage);
				p.X = Math.Floor(p.X);
				p.Y = Math.Floor(p.Y);
				if (pointToFrequencyDb.ContainsKey(p))
				{
					Tuple<double, double> tuple = pointToFrequencyDb[p];
					ToolTip tooltip = ((ToolTip)ToolTip);
					if (tooltip == null)
					{
						tooltip = new ToolTip();
						ToolTip = tooltip;

					}

					tooltip.Content = string.Format("{0:## 'Hz'}, {1:##.# 'db'}", tuple.Item1, tuple.Item2);
					
				}
			}
		}

		/// <summary>
		///     Called after the <see cref="ProgressBarBrush" /> value has changed.
		/// </summary>
		/// <param name="oldValue">The previous value of <see cref="ProgressBarBrush" /></param>
		/// <param name="newValue">The new value of <see cref="ProgressBarBrush" /></param>
		protected virtual void OnProgressBarBrushChanged(Brush oldValue, Brush newValue)
		{
			_progressIndicator.Fill = ProgressBarBrush;
			_progressLine.Stroke = ProgressBarBrush;

			CreateProgressIndicator();
		}

		/// <summary>
		///     Called after the <see cref="ProgressBarThickness" /> value has changed.
		/// </summary>
		/// <param name="oldValue">The previous value of <see cref="ProgressBarThickness" /></param>
		/// <param name="newValue">The new value of <see cref="ProgressBarThickness" /></param>
		protected virtual void OnProgressBarThicknessChanged(double oldValue, double newValue)
		{
			_progressLine.StrokeThickness = ProgressBarThickness;
			CreateProgressIndicator();
		}

		/// <summary>
		///     Raises the SizeChanged event, using the specified information as part of the eventual event data.
		/// </summary>
		/// <param name="sizeInfo">Details of the old and new size involved in the change.</param>
		protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
		{
			base.OnRenderSizeChanged(sizeInfo);
			UpdateWaveformCacheScaling();
			UpdateAllRegions();
		}

		/// <summary>
		///     Called after the <see cref="RepeatRegionBrush" /> value has changed.
		/// </summary>
		/// <param name="oldValue">The previous value of <see cref="RepeatRegionBrush" /></param>
		/// <param name="newValue">The new value of <see cref="RepeatRegionBrush" /></param>
		protected virtual void OnRepeatRegionBrushChanged(Brush oldValue, Brush newValue)
		{
			_repeatRegion.Fill = RepeatRegionBrush;
			UpdateRepeatRegion();
		}

		/// <summary>
		///     Called after the <see cref="RightLevelBrush" /> value has changed.
		/// </summary>
		/// <param name="oldValue">The previous value of <see cref="RightLevelBrush" /></param>
		/// <param name="newValue">The new value of <see cref="RightLevelBrush" /></param>
		protected virtual void OnRightLevelBrushChanged(Brush oldValue, Brush newValue)
		{
			var fillBrush = RightLevelBrush.Clone();
			fillBrush.Opacity *= 0.75;
			_rightPath.Fill = fillBrush;
			_rightPath.Stroke = RightLevelBrush;
			_rightPath.StrokeThickness = 1;
			UpdateWaveform();
		}

		/// <summary>
		///     Called whenever the control's template changes.
		/// </summary>
		/// <param name="oldTemplate">The old template</param>
		/// <param name="newTemplate">The new template</param>
		protected override void OnTemplateChanged(ControlTemplate oldTemplate, ControlTemplate newTemplate)
		{
			base.OnTemplateChanged(oldTemplate, newTemplate);
			if (_waveformCanvas != null)
				_waveformCanvas.Children.Clear();
			if (_timelineCanvas != null)
			{
				_timelineCanvas.SizeChanged -= timelineCanvas_SizeChanged;
				_timelineCanvas.Children.Clear();
			}
			if (_repeatCanvas != null)
				_repeatCanvas.Children.Clear();
			if (_progressCanvas != null)
				_progressCanvas.Children.Clear();
		}

		/// <summary>
		///     Called after the <see cref="TimelineTickBrush" /> value has changed.
		/// </summary>
		/// <param name="oldValue">The previous value of <see cref="TimelineTickBrush" /></param>
		/// <param name="newValue">The new value of <see cref="TimelineTickBrush" /></param>
		protected virtual void OnTimelineTickBrushChanged(Brush oldValue, Brush newValue)
		{
			UpdateTimeline();
		}

		private static void OnAllowRepeatRegionsChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				waveformTimeline.OnAllowRepeatRegionsChanged((bool)e.OldValue, (bool)e.NewValue);
		}

		private static void OnAutoScaleWaveformCacheChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				waveformTimeline.OnAutoScaleWaveformCacheChanged((bool)e.OldValue, (bool)e.NewValue);
		}

		private static void OnCenterLineBrushChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				waveformTimeline.OnCenterLineBrushChanged((Brush)e.OldValue, (Brush)e.NewValue);
		}

		private static void OnCenterLineThicknessChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				waveformTimeline.OnCenterLineThicknessChanged((double)e.OldValue, (double)e.NewValue);
		}

		private static object OnCoerceAllowRepeatRegions(DependencyObject o, object value)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				return waveformTimeline.OnCoerceAllowRepeatRegions((bool)value);
			return value;
		}

		private static object OnCoerceAutoScaleWaveformCache(DependencyObject o, object value)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				return waveformTimeline.OnCoerceAutoScaleWaveformCache((bool)value);
			return value;
		}

		private static object OnCoerceCenterLineBrush(DependencyObject o, object value)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				return waveformTimeline.OnCoerceCenterLineBrush((Brush)value);
			return value;
		}

		private static object OnCoerceCenterLineThickness(DependencyObject o, object value)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				return waveformTimeline.OnCoerceCenterLineThickness((double)value);
			return value;
		}

		private static object OnCoerceLeftLevelBrush(DependencyObject o, object value)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				return waveformTimeline.OnCoerceLeftLevelBrush((Brush)value);
			return value;
		}

		private static object OnCoerceProgressBarBrush(DependencyObject o, object value)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				return waveformTimeline.OnCoerceProgressBarBrush((Brush)value);
			return value;
		}

		private static object OnCoerceProgressBarThickness(DependencyObject o, object value)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				return waveformTimeline.OnCoerceProgressBarThickness((double)value);
			return value;
		}

		private static object OnCoerceRepeatRegionBrush(DependencyObject o, object value)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				return waveformTimeline.OnCoerceRepeatRegionBrush((Brush)value);
			return value;
		}

		private static object OnCoerceRightLevelBrush(DependencyObject o, object value)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				return waveformTimeline.OnCoerceRightLevelBrush((Brush)value);
			return value;
		}

		private static object OnCoerceTimelineTickBrush(DependencyObject o, object value)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				return waveformTimeline.OnCoerceTimelineTickBrush((Brush)value);
			return value;
		}

		private static void OnLeftLevelBrushChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				waveformTimeline.OnLeftLevelBrushChanged((Brush)e.OldValue, (Brush)e.NewValue);
		}

		private static void OnProgressBarBrushChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				waveformTimeline.OnProgressBarBrushChanged((Brush)e.OldValue, (Brush)e.NewValue);
		}

		private static void OnProgressBarThicknessChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				waveformTimeline.OnProgressBarThicknessChanged((double)e.OldValue, (double)e.NewValue);
		}

		private static void OnRepeatRegionBrushChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				waveformTimeline.OnRepeatRegionBrushChanged((Brush)e.OldValue, (Brush)e.NewValue);
		}

		private static void OnRightLevelBrushChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				waveformTimeline.OnRightLevelBrushChanged((Brush)e.OldValue, (Brush)e.NewValue);
		}

		private static void OnTimelineTickBrushChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			var waveformTimeline = o as WaveformTimeline;
			if (waveformTimeline != null)
				waveformTimeline.OnTimelineTickBrushChanged((Brush)e.OldValue, (Brush)e.NewValue);
		}

		private void CreateProgressIndicator()
		{
			if (SoundSource == null || _timelineCanvas == null || _progressCanvas == null)
				return;

			const double xLocation = 0.0d;

			_progressLine.X1 = xLocation;
			_progressLine.X2 = xLocation;
			_progressLine.Y1 = _timelineCanvas.RenderSize.Height;
			_progressLine.Y2 = _progressCanvas.RenderSize.Height;

			var indicatorPolySegment = new PolyLineSegment();
			indicatorPolySegment.Points.Add(new Point(xLocation, _timelineCanvas.RenderSize.Height));
			indicatorPolySegment.Points.Add(new Point(xLocation - IndicatorTriangleWidth,
				_timelineCanvas.RenderSize.Height - IndicatorTriangleWidth));
			indicatorPolySegment.Points.Add(new Point(xLocation + IndicatorTriangleWidth,
				_timelineCanvas.RenderSize.Height - IndicatorTriangleWidth));
			indicatorPolySegment.Points.Add(new Point(xLocation, _timelineCanvas.RenderSize.Height));
			var indicatorGeometry = new PathGeometry();
			var indicatorFigure = new PathFigure();
			indicatorFigure.Segments.Add(indicatorPolySegment);
			indicatorGeometry.Figures.Add(indicatorFigure);

			_progressIndicator.Data = indicatorGeometry;
			UpdateProgressIndicator();
		}

		private void Draw(List<Tuple<Point, Color>> points)
		{
			_writableWaveFormImage = BitmapFactory.New((int)_waveformCanvas.ActualWidth, (int)_waveformCanvas.ActualHeight);

			using (_writableWaveFormImage.GetBitmapContext())
			{
				try
				{
					_writableWaveFormImage.Clear();
					foreach (var tuple in points)
					{
						_writableWaveFormImage.FillRectangle((int)tuple.Item1.X, (int)tuple.Item1.Y,
							(int)((int)tuple.Item1.X + _fftRectTickness + 1), (int)((int)tuple.Item1.Y + _fftRectHeight + 1), tuple.Item2);
					}
				}
				catch
				{
					return;
				}
			}

			_waveFormImage.Source = _writableWaveFormImage;
		}



		private double GetTotalTransformScale()
		{
			var totalTransform = 1.0d;
			DependencyObject currentVisualTreeElement = this;
			do
			{
				var visual = currentVisualTreeElement as Visual;
				if (visual != null)
				{
					var transform = VisualTreeHelper.GetTransform(visual);

					// This condition is a way of determining if it
					// was a uniform scale transform. Is there some better way?
					if ((transform != null) &&
						(transform.Value.M12 == 0) &&
						(transform.Value.M21 == 0) &&
						(transform.Value.OffsetX == 0) &&
						(transform.Value.OffsetY == 0) &&
						(transform.Value.M11 == transform.Value.M22))
					{
						totalTransform *= transform.Value.M11;
					}
				}
				currentVisualTreeElement = VisualTreeHelper.GetParent(currentVisualTreeElement);
			} while (currentVisualTreeElement != null);

			return totalTransform;
		}

		private bool PointInRepeatRegion(Point point)
		{
			if (SoundSource.ChannelLength == 0)
				return false;

			var regionLeft = (SoundSource.SelectionBegin.TotalSeconds / SoundSource.ChannelLength) * RenderSize.Width;
			var regionRight = (SoundSource.SelectionEnd.TotalSeconds / SoundSource.ChannelLength) * RenderSize.Width;

			return (point.X >= regionLeft && point.X < regionRight);
		}

		private void soundPlayer_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "SelectionBegin":
					_startLoopRegion = SoundSource.SelectionBegin.TotalSeconds;
					UpdateRepeatRegion();
					break;

				case "SelectionEnd":
					_endLoopRegion = SoundSource.SelectionEnd.TotalSeconds;
					UpdateRepeatRegion();
					break;

				case "WaveformData":
					_cancellationToken.Cancel();
					UpdateWaveform();
					UpdateMarkerLines();
					break;

				case "SpectrumData":
					_spectrumCancellationToken.Cancel();
					UpdateSpectrum();
					break;

				case "ChannelPosition":
					UpdateProgressIndicator();
					break;

				case "CurrentPath":
					Clear();
					break;

				case "ChannelLength":
					_startLoopRegion = -1;
					_endLoopRegion = -1;
					UpdateAllRegions();
					break;
			}
		}

		private void timelineCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			UpdateTimeline();
		}

		internal void UpdateAllRegions()
		{
			UpdateRepeatRegion();
			CreateProgressIndicator();
			UpdateTimeline();
			UpdateMarkerLines();
		}

		private void UpdateMarkerLines()
		{
			if (SoundSource == null || _timelineCanvas == null || _progressCanvas == null)
				return;

			foreach (var cuePoint in _cueIndicators)
			{
				_progressCanvas.Children.Remove(cuePoint);
			}
			_markerDictionary.Clear();
			foreach (var positionLabel in SoundSource.CueDictionary)
			{
				var position = (positionLabel.Key / SoundSource.ChannelLength) * _progressCanvas.RenderSize.Width;
				var label = positionLabel.Value;
				_markerDictionary[position] = label;
			}
			_cueIndicators = new List<Line>(_markerDictionary.Count);
			var color = Color.FromArgb(180, 0, 128, 255);
			var stroke = new SolidColorBrush(color);

			for (var i = 0; i < _markerDictionary.Count; i++)
			{
				var xLocation = _markerDictionary.ElementAt(i).Key;
				var line = new Line
				{
					X1 = xLocation,
					X2 = xLocation,
					Y1 = 20,
					Y2 = ActualHeight,
					Stroke = stroke,
					StrokeThickness = 1,
					ToolTip = _markerDictionary.ElementAt(i).Value,
					Cursor = Cursors.Hand
				};
				var index = i;
				line.MouseLeftButtonDown += (p, r) =>
				{
					SoundSource.ChannelPosition = SoundSource.CueDictionary.Keys.ElementAt(index);
					r.Handled = true;
				};

				_cueIndicators.Add(line);
			}

			foreach (var cuePoint in _cueIndicators)
			{
				_progressCanvas.Children.Add(cuePoint);
			}
		}

		private void UpdateProgressIndicator()
		{
			if (SoundSource == null || _progressCanvas == null)
				return;

			var xLocation = 0.0d;
			if (SoundSource.ChannelLength != 0)
			{
				var progressPercent = SoundSource.ChannelPosition / SoundSource.ChannelLength;
				xLocation = progressPercent * _progressCanvas.RenderSize.Width;
			}
			_progressLine.Margin = new Thickness(xLocation, 0, 0, 0);
			_progressIndicator.Margin = new Thickness(xLocation, 0, 0, 0);
		}

		private void UpdateRepeatRegion()
		{
			if (SoundSource == null || _repeatCanvas == null)
				return;

			var startPercent = _startLoopRegion / SoundSource.ChannelLength;
			var startXLocation = startPercent * _repeatCanvas.RenderSize.Width;
			var endPercent = _endLoopRegion / SoundSource.ChannelLength;
			var endXLocation = endPercent * _repeatCanvas.RenderSize.Width;

			if (SoundSource.ChannelLength == 0 ||
				endXLocation <= startXLocation)
			{
				_repeatRegion.Width = 0;
				_repeatRegion.Height = 0;
				return;
			}

			_repeatRegion.Margin = new Thickness(startXLocation, 0, 0, 0);
			_repeatRegion.Width = endXLocation - startXLocation;
			_repeatRegion.Height = _repeatCanvas.RenderSize.Height;
		}

		private void UpdateSpectrum()
		{
			if (SoundSource == null || SoundSource.WaveformData == null || _waveformCanvas == null ||
				_waveformCanvas.RenderSize.Width < 1 || _waveformCanvas.RenderSize.Height < 1)
				return;
			List<float[]> spectrumDataList = SoundSource.SpectrumData.Select(item1 => (float[])item1.Clone()).ToList();
			Rect visibileRect = SoundSource.VisibleRect;
			int sampleRate = SoundSource.SampleRate;
			_spectrumCancellationToken = new CancellationTokenSource();
			var task2 = new Task<List<Tuple<Point, Color>>>(() => UpdateSpectrumAsync(_spectrumCancellationToken.Token, spectrumDataList, visibileRect, sampleRate),
				TaskCreationOptions.AttachedToParent);
			task2.Start();
			task2.ContinueWith(newTask2 =>
			{
				if (newTask2.Result != null)
				{
					Dispatcher.Invoke(() => { Draw(newTask2.Result); });
				}
			}
				);
		}

		public static double GetFrequencyFromIndex(int index, int fftSize, int sampleRate)
		{
			return index * (sampleRate / fftSize);
		}

		private List<Tuple<Point, Color>> UpdateSpectrumAsync(CancellationToken ct, List<float[]> spectrumDataList, Rect visibileRect, int sampleRate)
		{
			int numberOfbands = (int)(_waveformCanvas.RenderSize.Height);
			if (numberOfbands > 300)
			{
				numberOfbands = 300;
			}

			if (spectrumDataList != null && spectrumDataList.Count > 1)
			{
				var pointCount = spectrumDataList.Count * numberOfbands;
				var pointThickness = visibileRect.Width / spectrumDataList.Count;
				_fftRectTickness = pointThickness;
				var totalHeight = _waveformCanvas.RenderSize.Height;
				_fftRectHeight = totalHeight / numberOfbands;
				var pointsWithColor = new List<Tuple<Point, Color>>(pointCount);
				var arrayStart = (int)visibileRect.Left;
				pointToFrequencyDb = new ConcurrentDictionary<Point, Tuple<double, double>>();
				pointToFrequencyDb.Clear();

				for (var i = 0;
					i < spectrumDataList.Count;
					i++)
				{
					try
					{
						var currentXValueList = new List<Tuple<Point, Color>>(numberOfbands);
						var xLocation = (i + arrayStart) * pointThickness;

						var currentIndex = 0;
						double max = numberOfbands;
						var arraylength = spectrumDataList[i].Length / 2.00;
						var k = arraylength / (max * 2);
						var inc = ((arraylength) / (max * max));
						var spectrumData = (float[])spectrumDataList[i].Clone();
						var minDBValue = -100f;
						for (double j = 0; j < arraylength; j += k)
						{
							if (ct.IsCancellationRequested)
							{
								break;
							}
							var currentY = totalHeight - ((double)currentIndex / numberOfbands * totalHeight);

							var length = (int)Math.Ceiling((arraylength < j + k ? arraylength - j : k));

							var currentData = new float[length];
							Array.Copy(spectrumData, (int)j, currentData, 0, length);
							var value = currentData.Max();
							var dbValue = (float)(20 * Math.Log10(value));
							currentXValueList.Add(new Tuple<Point, Color>(new Point(xLocation, currentY),
								HsbHelper.GetColorFromDb(dbValue, minDBValue)));
							pointToFrequencyDb.GetOrAdd(new Point(Math.Floor(xLocation), Math.Floor(currentY)), new Tuple<double, double>(GetFrequencyFromIndex((int)j, spectrumDataList[i].Length, sampleRate), dbValue));
							k += inc;

							currentIndex++;
						}
						pointsWithColor.AddRange(currentXValueList);
					}
					catch (Exception)
					{
						break;
					}
				}

				return pointsWithColor;
			}

			return null;
		}

		private ConcurrentDictionary<Point, Tuple<double, double>> pointToFrequencyDb = new ConcurrentDictionary<Point, Tuple<double, double>>();

		private void UpdateTimeline()
		{
			if (SoundSource == null || _timelineCanvas == null)
				return;

			foreach (var textblock in _timestampTextBlocks)
			{
				_timelineCanvas.Children.Remove(textblock);
			}
			_timestampTextBlocks.Clear();

			foreach (var line in _timeLineTicks)
			{
				_timelineCanvas.Children.Remove(line);
			}
			_timeLineTicks.Clear();

			var bottomLoc = _timelineCanvas.RenderSize.Height - 1;

			_timelineBackgroundRegion.Width = _timelineCanvas.RenderSize.Width;
			_timelineBackgroundRegion.Height = _timelineCanvas.RenderSize.Height;

			var minorTickDuration = 1.00d; // Major tick = 5 seconds, Minor tick = 1.00 second
			var majorTickDuration = 5.00d;
			if (SoundSource.ChannelLength >= 120.0d) // Major tick = 1 minute, Minor tick = 15 seconds.
			{
				minorTickDuration = 15.0d;
				majorTickDuration = 60.0d;
			}
			else if (SoundSource.ChannelLength >= 60.0d) // Major tick = 30 seconds, Minor tick = 5.0 seconds.
			{
				minorTickDuration = 5.0d;
				majorTickDuration = 30.0d;
			}
			else if (SoundSource.ChannelLength >= 30.0d) // Major tick = 10 seconds, Minor tick = 2.0 seconds.
			{
				minorTickDuration = 2.0d;
				majorTickDuration = 10.0d;
			}

			if (SoundSource.ChannelLength < minorTickDuration)
				return;

			var minorTickCount = (int)(SoundSource.ChannelLength / minorTickDuration);
			for (var i = 1; i <= minorTickCount; i++)
			{
				var timelineTick = new Line
				{
					Stroke = TimelineTickBrush,
					StrokeThickness = 1.0d
				};
				if (i % (majorTickDuration / minorTickDuration) == 0) // Draw Large Ticks and Timestamps at minute marks
				{
					var xLocation = ((i * minorTickDuration) / SoundSource.ChannelLength) * _timelineCanvas.RenderSize.Width;

					var drawTextBlock = false;
					double lastTimestampEnd;
					if (_timestampTextBlocks.Count != 0)
					{
						var lastTextBlock = _timestampTextBlocks[_timestampTextBlocks.Count - 1];
						lastTimestampEnd = lastTextBlock.Margin.Left + lastTextBlock.ActualWidth;
					}
					else
						lastTimestampEnd = 0;

					if (xLocation > lastTimestampEnd + TimeStampMargin)
						drawTextBlock = true;

					// Flag that we're at the end of the timeline such
					// that there is not enough room for the text to draw.
					var isAtEndOfTimeline = (_timelineCanvas.RenderSize.Width - xLocation < 28.0d);

					if (drawTextBlock)
					{
						timelineTick.X1 = xLocation;
						timelineTick.Y1 = bottomLoc;
						timelineTick.X2 = xLocation;
						timelineTick.Y2 = bottomLoc - MajorTickHeight;

						if (isAtEndOfTimeline)
							continue;

						var timeSpan = TimeSpan.FromSeconds(i * minorTickDuration);
						var timestampText = new TextBlock
						{
							Margin = new Thickness(xLocation + 2, 0, 0, 0),
							FontFamily = FontFamily,
							FontStyle = FontStyle,
							FontWeight = FontWeight,
							FontStretch = FontStretch,
							FontSize = FontSize,
							Foreground = Foreground,
							Text =
								(timeSpan.TotalHours >= 1.0d)
									? string.Format(@"{0:hh\:mm\:ss}", timeSpan)
									: string.Format(@"{0:mm\:ss}", timeSpan)
						};
						_timestampTextBlocks.Add(timestampText);
						_timelineCanvas.Children.Add(timestampText);
						UpdateLayout(); // Needed so that we know the width of the textblock.
					}
					else // If still on the text block, draw a minor tick mark instead of a major.
					{
						timelineTick.X1 = xLocation;
						timelineTick.Y1 = bottomLoc;
						timelineTick.X2 = xLocation;
						timelineTick.Y2 = bottomLoc - MinorTickHeight;
					}
				}
				else // Draw small ticks
				{
					var xLocation = ((i * minorTickDuration) / SoundSource.ChannelLength) * _timelineCanvas.RenderSize.Width;
					timelineTick.X1 = xLocation;
					timelineTick.Y1 = bottomLoc;
					timelineTick.X2 = xLocation;
					timelineTick.Y2 = bottomLoc - MinorTickHeight;
				}
				_timeLineTicks.Add(timelineTick);
				_timelineCanvas.Children.Add(timelineTick);
			}
		}

		private void UpdateWaveform()
		{
			if (SoundSource == null || SoundSource.WaveformData == null || _waveformCanvas == null ||
				_waveformCanvas.RenderSize.Width < 1 || _waveformCanvas.RenderSize.Height < 1 || LeftLevelBrush.Opacity < 0.1)
				return;

			if (CenterLineBrush != null)
			{
				_centerLine.X1 = 0;
				_centerLine.X2 = _waveformCanvas.RenderSize.Width;
				_centerLine.Y1 = _waveformCanvas.RenderSize.Height / 2.0d;
				;
				_centerLine.Y2 = _waveformCanvas.RenderSize.Height / 2.0d;
				;
			}
			float[] waveformData = (float[])SoundSource.WaveformData.Clone();
			Rect visibileRect = SoundSource.VisibleRect;
			_cancellationToken.Cancel();
			_cancellationToken = new CancellationTokenSource();
			var task = new Task<Tuple<List<Point>, List<Point>>>(() => UpdateWaveformAsync(waveformData, visibileRect),
				TaskCreationOptions.AttachedToParent);
			task.Start();

			task.ContinueWith(newTask =>
			{
				if (newTask.Result != null)
				{
					Dispatcher.Invoke(() =>
					{
						var leftGeometry = new PathGeometry();
						var leftPathFigure = new PathFigure();
						var leftWaveformPolyLine = new PolyLineSegment { Points = new PointCollection(newTask.Result.Item1) };
						leftPathFigure.Segments.Add(leftWaveformPolyLine);
						leftGeometry.Figures.Add(leftPathFigure);
						var rightGeometry = new PathGeometry();
						var rightPathFigure = new PathFigure();
						var rightWaveformPolyLine = new PolyLineSegment { Points = new PointCollection(newTask.Result.Item2) };
						rightPathFigure.Segments.Add(rightWaveformPolyLine);
						rightGeometry.Figures.Add(rightPathFigure);

						_leftPath.Data = leftGeometry;
						_rightPath.Data = rightGeometry;
					});
				}
			}
				);
		}

		private Tuple<List<Point>, List<Point>> UpdateWaveformAsync(float[] waveformData, Rect visibileRect)
		{
			const double minValue = 0;
			const double maxValue = 1.5;
			const double dbScale = (maxValue - minValue);

			var pointCount = (int)(waveformData.Length / 2.0d);
			var pointThickness = visibileRect.Width / pointCount;
			var waveformSideHeight = _waveformCanvas.RenderSize.Height / 2.0d;

			if (waveformData != null && waveformData.Length > 1)
			{
				var points = new Tuple<List<Point>, List<Point>>(new List<Point>(), new List<Point>());
				points.Item1.Add(new Point(0, waveformSideHeight));
				points.Item2.Add(new Point(0, waveformSideHeight));

				var xLocation = 0.0d;
				var arrayStart = (int)visibileRect.Left;

				for (var i = 0;
					i < waveformData.Length - 1;
					i += 2)
				{
					try
					{
						xLocation = ((i / 2) + arrayStart) * pointThickness;
						var leftRenderHeight = ((waveformData[i] - minValue) / dbScale) * waveformSideHeight;
						points.Item1.Add(new Point(xLocation, waveformSideHeight - leftRenderHeight));

						var rightRenderHeight = ((waveformData[i + 1] - minValue) / dbScale) * waveformSideHeight;

						points.Item2.Add(new Point(xLocation, waveformSideHeight + rightRenderHeight));
					}
					catch (Exception)
					{
						break;
					}
				}

				points.Item1.Add(new Point(xLocation, waveformSideHeight));
				points.Item1.Add(new Point(0, waveformSideHeight));
				points.Item2.Add(new Point(xLocation, waveformSideHeight));
				points.Item2.Add(new Point(0, waveformSideHeight));

				return points;
			}

			return null;
		}

		private void UpdateWaveformCacheScaling()
		{
			if (_waveformCanvas == null)
				return;

			var waveformCache = (BitmapCache)_waveformCanvas.CacheMode;
			if (AutoScaleWaveformCache)
			{
				var totalTransformScale = GetTotalTransformScale();
				if (waveformCache.RenderAtScale != totalTransformScale)
					waveformCache.RenderAtScale = totalTransformScale;
			}
			else
			{
				waveformCache.RenderAtScale = 1.0d;
			}
		}

		/// <summary>
		///     Identifies the <see cref="AllowRepeatRegions" /> dependency property.
		/// </summary>
		public static readonly DependencyProperty AllowRepeatRegionsProperty =
			DependencyProperty.Register("AllowRepeatRegions", typeof(bool), typeof(WaveformTimeline),
				new UIPropertyMetadata(true, OnAllowRepeatRegionsChanged, OnCoerceAllowRepeatRegions));

		/// <summary>
		///     Identifies the <see cref="AutoScaleWaveformCache" /> dependency property.
		/// </summary>
		public static readonly DependencyProperty AutoScaleWaveformCacheProperty =
			DependencyProperty.Register("AutoScaleWaveformCache", typeof(bool), typeof(WaveformTimeline),
				new UIPropertyMetadata(false, OnAutoScaleWaveformCacheChanged, OnCoerceAutoScaleWaveformCache));

		/// <summary>
		///     Identifies the <see cref="CenterLineBrush" /> dependency property.
		/// </summary>
		public static readonly DependencyProperty CenterLineBrushProperty = DependencyProperty.Register("CenterLineBrush",
			typeof(Brush), typeof(WaveformTimeline),
			new UIPropertyMetadata(new SolidColorBrush(Colors.Black), OnCenterLineBrushChanged, OnCoerceCenterLineBrush));

		/// <summary>
		///     Identifies the <see cref="CenterLineThickness" /> dependency property.
		/// </summary>
		public static readonly DependencyProperty CenterLineThicknessProperty =
			DependencyProperty.Register("CenterLineThickness", typeof(double), typeof(WaveformTimeline),
				new UIPropertyMetadata(1.0d, OnCenterLineThicknessChanged, OnCoerceCenterLineThickness));

		/// <summary>
		///     Identifies the <see cref="LeftLevelBrush" /> dependency property.
		/// </summary>
		public static readonly DependencyProperty LeftLevelBrushProperty = DependencyProperty.Register("LeftLevelBrush",
			typeof(Brush), typeof(WaveformTimeline),
			new UIPropertyMetadata(new SolidColorBrush(Colors.Blue), OnLeftLevelBrushChanged, OnCoerceLeftLevelBrush));

		/// <summary>
		///     Identifies the <see cref="ProgressBarBrush" /> dependency property.
		/// </summary>
		public static readonly DependencyProperty ProgressBarBrushProperty = DependencyProperty.Register("ProgressBarBrush",
			typeof(Brush), typeof(WaveformTimeline),
			new UIPropertyMetadata(new SolidColorBrush(Color.FromArgb(0xCD, 0xBA, 0x00, 0xFF)), OnProgressBarBrushChanged,
				OnCoerceProgressBarBrush));

		/// <summary>
		///     Identifies the <see cref="ProgressBarThickness" /> dependency property.
		/// </summary>
		public static readonly DependencyProperty ProgressBarThicknessProperty =
			DependencyProperty.Register("ProgressBarThickness", typeof(double), typeof(WaveformTimeline),
				new UIPropertyMetadata(2.0d, OnProgressBarThicknessChanged, OnCoerceProgressBarThickness));

		/// <summary>
		///     Identifies the <see cref="RepeatRegionBrush" /> dependency property.
		/// </summary>
		public static readonly DependencyProperty RepeatRegionBrushProperty = DependencyProperty.Register(
			"RepeatRegionBrush", typeof(Brush), typeof(WaveformTimeline),
			new UIPropertyMetadata(new SolidColorBrush(Color.FromArgb(0x81, 0xF6, 0xFF, 0x00)), OnRepeatRegionBrushChanged,
				OnCoerceRepeatRegionBrush));

		/// <summary>
		///     Identifies the <see cref="RightLevelBrush" /> dependency property.
		/// </summary>
		public static readonly DependencyProperty RightLevelBrushProperty = DependencyProperty.Register("RightLevelBrush",
			typeof(Brush), typeof(WaveformTimeline),
			new UIPropertyMetadata(new SolidColorBrush(Colors.Red), OnRightLevelBrushChanged, OnCoerceRightLevelBrush));

		/// <summary>
		///     Identifies the <see cref="TimelineTickBrush" /> dependency property.
		/// </summary>
		public static readonly DependencyProperty TimelineTickBrushProperty = DependencyProperty.Register(
			"TimelineTickBrush", typeof(Brush), typeof(WaveformTimeline),
			new UIPropertyMetadata(new SolidColorBrush(Colors.Black), OnTimelineTickBrushChanged, OnCoerceTimelineTickBrush));

		public static readonly DependencyProperty SoundSourceProperty = DependencyProperty.Register("SoundSource", typeof(IWaveformPlayer), typeof(WaveformTimeline), new UIPropertyMetadata(default(IWaveformPlayer), OnSoundEngineChanged));

		private static void OnSoundEngineChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var waveformTimeline = d as WaveformTimeline;
			if (waveformTimeline != null)
			{
				waveformTimeline.OnSoundSourcePropertyChange();
			}
		}

		private void OnSoundSourcePropertyChange()
		{
			SoundSource.PropertyChanged += soundPlayer_PropertyChanged;
			OnApplyTemplate();
		}
	}
}