﻿using System;
using System.Windows.Media;
using rage.ToolLib.Extensions;

namespace WPFSoundVisualizationLib.WaveForm_Timeline
{
	static class HsbHelper
	{
		public static Color GetColorFromDb(float dbValue, float minDbValue)
		{
			float hotness = Math.Max(Math.Min((dbValue - minDbValue) / 100, 1), 0);
			float hue = (float)(0.8 + (hotness * (1.17 - 0.8))) % 1.0f;
			Color color = HSBtoRGB(hue, 1, (float)Math.Pow(hotness+0.2, 3).Clamp(0,1));
			return color;
		}

		public static Color HSBtoRGB(float hue, float saturation, float brightness)
		{
			int r = 0, g = 0, b = 0;
			if (saturation == 0)
			{
				r = g = b = (int)(brightness * 255.0f + 0.5f);
			}
			else
			{
				float h = (hue - (float)Math.Floor(hue)) * 6.0f;
				float f = h - (float)Math.Floor(h);
				float p = brightness * (1.0f - saturation);
				float q = brightness * (1.0f - saturation * f);
				float t = brightness * (1.0f - (saturation * (1.0f - f)));
				switch ((int)h)
				{
					case 0:
						r = (int)(brightness * 255.0f + 0.5f);
						g = (int)(t * 255.0f + 0.5f);
						b = (int)(p * 255.0f + 0.5f);
						break;
					case 1:
						r = (int)(q * 255.0f + 0.5f);
						g = (int)(brightness * 255.0f + 0.5f);
						b = (int)(p * 255.0f + 0.5f);
						break;
					case 2:
						r = (int)(p * 255.0f + 0.5f);
						g = (int)(brightness * 255.0f + 0.5f);
						b = (int)(t * 255.0f + 0.5f);
						break;
					case 3:
						r = (int)(p * 255.0f + 0.5f);
						g = (int)(q * 255.0f + 0.5f);
						b = (int)(brightness * 255.0f + 0.5f);
						break;
					case 4:
						r = (int)(t * 255.0f + 0.5f);
						g = (int)(p * 255.0f + 0.5f);
						b = (int)(brightness * 255.0f + 0.5f);
						break;
					case 5:
						r = (int)(brightness * 255.0f + 0.5f);
						g = (int)(p * 255.0f + 0.5f);
						b = (int)(q * 255.0f + 0.5f);
						break;
				}
			}
			return Color.FromArgb(Convert.ToByte(255), Convert.ToByte(r), Convert.ToByte(g), Convert.ToByte(b));
		}
	}
}
