namespace Rave.NameValidation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;

    using Rave.Controls.Forms.Popups;
    using Rave.ObjectBrowser.Infrastructure.Interfaces;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    public class MarkerValidator : IRAVEReportPlugin
    {
        private string m_dir;
        private string m_name;
        private Dictionary<string, List<int>> m_namesUsed;
        private List<string> m_packs;
        private List<string> m_validNames;

        #region IRAVEReportPlugin Members

        public string GetName()
        {
            return this.m_name;
        }

        public bool Init(XmlNode settings)
        {
            this.m_packs = new List<string>();

            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.m_name = setting.InnerText;
                        break;
                        //directory containg wave name files
                    case "Dir":
                        this.m_dir = setting.InnerText;
                        break;
                        //packs to be checked
                    case "Pack":
                        this.m_packs.Add(setting.InnerText.ToUpper());
                        break;
                }
            }

            if (this.m_name == "" ||
                this.m_dir == "")
            {
                return false;
            }
            return true;
        }

        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            var sb = new StringBuilder();
            var dir = new DirectoryInfo(this.m_dir);

            //Build List of Valid Names
            this.m_validNames = new List<string>();
            foreach (var file in dir.GetFiles())
            {
                var fs = new FileStream(file.FullName, FileMode.Open);
                var sr = new StreamReader(fs);
                while (!sr.EndOfStream)
                {
                    var name = sr.ReadLine();
                    if (!this.m_validNames.Contains(name))
                    {
                        this.m_validNames.Add(name);
                    }
                }
                sr.Close();
                fs.Close();
            }

            //Process Packs
            var nodes = new ArrayList();
            foreach (EditorTreeNode etn in waveBrowser.GetEditorTreeView().GetNodes())
            {
                var pack = etn as PackNode;
                if (pack != null &&
                    this.m_packs.Contains(pack.GetObjectName()))
                {
                    //Process Banks
                    var banks = new ArrayList();
                    foreach (EditorTreeNode treeNode in pack.Nodes)
                    {
                        GetBanks(treeNode, ref banks);
                    }

                    //Need to check wave names on a per bank basis as different banks can have 
                    //same named waves

                    //No need to check for duplicate wave names per bank as Rave would not allow this

                    foreach (BankNode bank in banks)
                    {
                        nodes = bank.GetChildWaveNodes();

                        if (nodes.Count != 0)
                        {
                            //create new dictionary every bank
                            this.m_namesUsed = new Dictionary<string, List<int>>();

                            foreach (WaveNode wn in nodes)
                            {
                                this.IsNameValid(wn, sb);
                            }
                            //check dictionary for missing variations
                            this.CheckMissingVariations(sb, bank);
                        }
                    }
                }
            }

            //Write to temp file and display
            var fileName = Path.GetTempPath() + "output.txt";
            var filesteam = new FileStream(fileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);
            sw.Write(sb.ToString());
            sw.Close();
            filesteam.Close();

            var report = new frmReport(this.m_name + " Report");
            report.SetURL(fileName);
            report.Show();
        }

        #endregion

        public void Shutdown()
        {
        }

        private static void GetBanks(EditorTreeNode etn, ref ArrayList banks)
        {
            var bank = etn as BankNode;
            if (bank != null)
            {
                banks.Add(bank);
            }
            else
            {
                foreach (EditorTreeNode treeNode in etn.Nodes)
                {
                    GetBanks(treeNode, ref banks);
                }
            }
        }

        private void CheckMissingVariations(StringBuilder sb, BankNode bank)
        {
            foreach (var kvp in this.m_namesUsed)
            {
                for (var i = 1; i <= kvp.Value.Count; i++)
                {
                    if (!kvp.Value.Contains(i))
                    {
                        sb.AppendLine("Missing varaition " + kvp.Key + "_" + i + "in " + bank.FullPath);
                    }
                }
            }
        }

        private void IsNameValid(WaveNode wn, StringBuilder sb)
        {
            var waveName = wn.GetObjectName();
            waveName = waveName.Substring(0, waveName.Length - 4);
            var baseName = waveName.Substring(0, waveName.LastIndexOf("_"));

            //Chekc length of name
            if (waveName.Length > 63)
            {
                sb.AppendLine("Name longer than 63 characters: " + wn.FullPath);
                return;
            }

            //check for valid name
            if (!this.m_validNames.Contains(baseName))
            {
                sb.AppendLine("Base name not found: " + wn.FullPath);
                return;
            }

            //check if valid variation. Has to be two digits
            var index = waveName.LastIndexOf("_");
            if ((waveName.Length - (index + 1)) != 2)
            {
                sb.AppendLine("Invalid name format: " + wn.FullPath);
                return;
            }

            int variation;

            try
            {
                //try to parse
                var number = waveName.Substring(waveName.Length - 2);
                variation = Int32.Parse(number);
            }
            catch
            {
                sb.AppendLine("Invalid variation number: " + wn.FullPath);
                return;
            }

            if (!this.m_namesUsed.ContainsKey(baseName))
            {
                var temp = new List<int>();
                temp.Add(variation);
                this.m_namesUsed.Add(baseName, temp);
            }
            else
            {
                this.m_namesUsed[baseName].Add(variation);
            }
        }
    }
}