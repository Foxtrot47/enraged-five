﻿// -----------------------------------------------------------------------
// <copyright file="IXmlBankMetaDataCompiler.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Metadata.Infrastructure.Interfaces
{
    using System.Collections.Generic;

    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;

    using rage;

    /// <summary>
    /// XML bank metadata compiler.
    /// </summary>
    public interface IXmlBankMetaDataCompiler
    {
        IDictionary<string, IMetadataManager> MetadataManagers { get; }

        int RuntimeUniqueId { get; set; }

        audProjectSettings ProjectSettings { get; }

        bool CompileMetadata(IXmlBank bank);
    }
}
