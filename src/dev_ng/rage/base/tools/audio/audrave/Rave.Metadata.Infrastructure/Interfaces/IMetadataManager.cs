﻿// -----------------------------------------------------------------------
// <copyright file="IMetadataManager.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Metadata.Infrastructure.Interfaces
{
    using System.Xml;
    using System.Xml.Linq;

    using rage;
    using rage.Compiler;
    using rage.ToolLib.Writer;

    /// <summary>
    /// Metadata manager interface.
    /// </summary>
    public interface IMetadataManager
    {
        MetaDataCompiler Compiler { get; }

        int FindObjectSize(string objName);

        XDocument CompileObject(XmlNode sound, IWriter output, audMetadataFile metadataFile);

        uint ComputeHash(string str);

        void UpdateObjectSize(string name, int offset, int size);
    }
}
