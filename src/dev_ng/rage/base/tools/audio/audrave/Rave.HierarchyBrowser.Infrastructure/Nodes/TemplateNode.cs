﻿// -----------------------------------------------------------------------
// <copyright file="TemplateNode.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Nodes
{
    using System.Drawing;

    using Rave.HierarchyBrowser.Infrastructure.Annotations;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;

    /// <summary>
    /// The template node.
    /// </summary>
    public class TemplateNode : HierarchyNode
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateNode"/> class.
        /// </summary>
        /// <param name="proxyObject">
        /// The proxy object.
        /// </param>
        /// <param name="annotationNode">
        /// The annotation node.
        /// </param>
        public TemplateNode(ITemplateProxyObject proxyObject, ObjectAnnotationNode annotationNode)
        {
            this.ProxyObject = proxyObject;
            this.ImageIndex = 0;

            if (proxyObject == null)
            {
                this.ObjectName = InvalidReference;
                this.ForeColor = Color.Red;
            }
            else
            {
                this.ObjectName = proxyObject.Name;
            }

            this.AnnotationNode = annotationNode;
        }

        #region Public Properties

        /// <summary>
        /// Gets the proxy object.
        /// </summary>
        public ITemplateProxyObject ProxyObject { get; private set; }

        /// <summary>
        /// Gets the annotation node.
        /// </summary>
        public ObjectAnnotationNode AnnotationNode { get; private set; }

        #endregion
    }
}
