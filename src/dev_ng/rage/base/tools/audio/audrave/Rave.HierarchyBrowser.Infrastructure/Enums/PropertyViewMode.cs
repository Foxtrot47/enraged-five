﻿// -----------------------------------------------------------------------
// <copyright file="PropertyViewMode.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Enums
{
    /// <summary>
    /// Property view mode.
    /// </summary>
    public enum PropertyViewMode
    {
        /// <summary>
        /// Single property mode.
        /// </summary>
        Single,

        /// <summary>
        /// Inline mode.
        /// </summary>
        Inline,

        /// <summary>
        /// Nested mode.
        /// </summary>
        Nested
    }
}
