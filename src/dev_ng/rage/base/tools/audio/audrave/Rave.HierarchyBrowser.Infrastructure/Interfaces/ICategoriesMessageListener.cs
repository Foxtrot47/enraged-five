﻿// -----------------------------------------------------------------------
// <copyright file="ICategoryMessageListener.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Interfaces
{
    using System;
    using System.Collections.Generic;

    using Rave.RemoteControl.Infrastructure.Interfaces;

    /// <summary>
    /// Category message listener.
    /// </summary>
    public interface ICategoriesMessageListener : IRemoteMessageListener
    {
        /// <summary>
        /// The header received event.
        /// </summary>
        event Action HeaderReceived;

        /// <summary>
        /// The update received.
        /// </summary>
        event Action UpdateReceived;

        /// <summary>
        /// Gets the category properties.
        /// </summary>
        IDictionary<string, IDictionary<string, float>> CategoryProperties { get; }
    }
}
