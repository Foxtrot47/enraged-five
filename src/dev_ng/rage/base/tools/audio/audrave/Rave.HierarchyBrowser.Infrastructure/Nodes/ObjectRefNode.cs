// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectRefNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The object ref node.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Nodes
{
    using Rave.HierarchyBrowser.Infrastructure.Annotations;
    using Rave.Types.Infrastructure.Structs;

    /// <summary>
    /// The object ref node.
    /// </summary>
    public class ObjectRefNode : ObjectNode
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectRefNode"/> class.
        /// </summary>
        /// <param name="objectRef">
        /// The object ref.
        /// </param>
        /// <param name="annotationNode">
        /// The annotation node.
        /// </param>
        public ObjectRefNode(ObjectRef objectRef, ObjectAnnotationNode annotationNode)
            : base(objectRef.ObjectInstance, annotationNode)
        {
            this.ObjectReference = objectRef;
            if (null == objectRef.ObjectInstance && null != objectRef.Node)
            {
                this.ObjectName = objectRef.Node.InnerText + " " + InvalidReference;
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the object reference.
        /// </summary>
        public ObjectRef ObjectReference { get; private set; }

        #endregion
    }
}