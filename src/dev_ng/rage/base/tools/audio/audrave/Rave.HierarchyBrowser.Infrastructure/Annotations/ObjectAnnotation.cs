﻿// -----------------------------------------------------------------------
// <copyright file="ObjectAnnotation.cs" company="Rockstar North">
// Object annotation.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Annotations
{
    /// <summary>
    /// Object annotation.
    /// </summary>
    public class ObjectAnnotation
    {
        /// <summary>
        /// The value.
        /// </summary>
        private string value;

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectAnnotation"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="isDefaultValue">
        /// The is default value flag.
        /// </param>
        public ObjectAnnotation(string name, string value, bool isDefaultValue = false)
        {
            this.Name = name;
            this.Value = value;
            this.IsDefaultValue = isDefaultValue;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.value = value;
                this.FormattedValue = string.Format("({0}: {1})", this.Name, value);
            }
        }

        /// <summary>
        /// Gets the formatted value.
        /// </summary>
        public string FormattedValue { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether is default value.
        /// </summary>
        public bool IsDefaultValue { get; set; }
    }
}
