// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The object node.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Nodes
{
	using System.Drawing;

	using Rave.HierarchyBrowser.Infrastructure.Annotations;
	using Rave.Types.Infrastructure.Interfaces.Objects;

	/// <summary>
	/// The object node.
	/// </summary>
	public class ObjectNode : HierarchyNode
	{

		public static Font boldFont = new Font(System.Windows.Forms.TreeView.DefaultFont, FontStyle.Bold);
		public Brush Brush { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="ObjectNode"/> class.
		/// </summary>
		/// <param name="objectInstance">
		/// The object instance.
		/// </param>
		/// <param name="annotationNode">
		/// The annotation node.
		/// </param>
		public ObjectNode(IObjectInstance objectInstance, ObjectAnnotationNode annotationNode = null)
		{
			this.ObjectInstance = objectInstance;
			this.AnnotationNode = annotationNode;
			this.ImageIndex = 0;
			this.NodeFont = System.Windows.Forms.TreeView.DefaultFont;
			Brush = GetBrush();
			if (objectInstance == null)
			{
				this.ObjectName = InvalidReference;
			}
			else
			{
				this.ObjectName = objectInstance.Name;
			}
		}



		public Brush GetBrush()
		{
			Brush brush = SystemBrushes.WindowText;
			
			if (ObjectInstance == null)
			{

				this.ForeColor = Color.Red;
				brush = Brushes.Red;
			}
			else
			{

				if (this.ObjectInstance.IsMuted)
				{
					this.NodeFont = boldFont;
					this.ForeColor = Color.DarkOrange;
					brush = Brushes.DarkOrange;
				}
				else if (this.ObjectInstance.IsSoloed)
				{
					this.NodeFont = boldFont;
					this.ForeColor = Color.Blue;
					brush = Brushes.Blue;
				}
			}

			if (this.IsSelected)
			{
				brush = SystemBrushes.HighlightText;
			}
			return brush;
		}


		/// <summary>
		/// Gets or sets the object instance.
		/// </summary>
		public IObjectInstance ObjectInstance { get; set; }

		/// <summary>
		/// Gets or sets the annotation node.
		/// </summary>
		public ObjectAnnotationNode AnnotationNode { get; set; }

	}
}
