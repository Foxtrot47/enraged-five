﻿// -----------------------------------------------------------------------
// <copyright file="IHierarchyBrowser.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Interfaces
{
    using System.Collections.Generic;

    using Rave.HierarchyBrowser.Infrastructure.Enums;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.RemoteControl.Infrastructure.Interfaces;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;

    /// <summary>
    /// Hierarchy browser interface.
    /// </summary>
    public interface IHierarchyBrowser
    {
        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether can audition.
        /// </summary>
        bool CanAudition { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can check in out.
        /// </summary>
        bool CanCheckInOut { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can delete.
        /// </summary>
        bool CanDelete { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can find in browser.
        /// </summary>
        bool CanFindInBrowser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can insert wrapper.
        /// </summary>
        bool CanInsertWrapper { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can local check out.
        /// </summary>
        bool CanLocalCheckOut { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can make local.
        /// </summary>
        bool CanMakeLocal { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can navigate.
        /// </summary>
        bool CanNavigate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can remove reference.
        /// </summary>
        bool CanRemoveReference { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether should append type.
        /// </summary>
        bool ShouldAppendType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether should expand all.
        /// </summary>
        bool ShouldExpandAll { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sort tree.
        /// </summary>
        bool SortTree { get; set; }

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        HierarchyBrowserModes Mode { get; set; }

        /// <summary>
        /// Gets the property view mode.
        /// </summary>
        PropertyViewMode PropertyViewMode { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        void DeleteObjectInstance(IObjectInstance objectInstance);

        /// <summary>
        /// Delete a template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        void DeleteTemplate(ITemplate template);

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="remoteControl">
        /// The remote control.
        /// </param>
        void Init(string title);

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="externalEditors">
        /// The external editors.
        /// </param>
        /// <param name="title">
        /// The title.
        /// </param>
        void Init(IList<IRAVEEditorPlugin> externalEditors, string title);

        /// <summary>
        /// The notify audition.
        /// </summary>
        void NotifyAudition();

        /// <summary>
        /// The notify audition stop.
        /// </summary>
        void NotifyAuditionStop();

        /// <summary>
        /// The show hierarchy.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="updateHistory">
        /// The update History flag.
        /// </param>
        void ShowHierarchy(object parent, bool updateHistory = true);

        /// <summary>
        /// Clears the hierarchy tree view while updates are being made elsewhere.
        /// </summary>
        void BeginUpdate();

        /// <summary>
        /// Reloads the hierarchy tree view once updates completed elsewhere.
        /// </summary>
        void EndUpdate();

        /// <summary>
        /// The show node.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        void ShowNode(IObjectInstance obj);

        /// <summary>
        /// The show node.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        void ShowNode(ITemplate template);

        /// <summary>
        /// The try to rename.
        /// </summary>
        void TryToRename();

        #endregion
    }
}
