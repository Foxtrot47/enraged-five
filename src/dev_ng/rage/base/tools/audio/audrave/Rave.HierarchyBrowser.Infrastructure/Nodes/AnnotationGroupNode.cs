﻿// -----------------------------------------------------------------------
// <copyright file="AnnotationGroupNode.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Nodes
{
    using System.Windows.Forms;

    /// <summary>
    /// Annotation group node.
    /// </summary>
    public class AnnotationGroupNode : TreeNode
    {
    }
}
