﻿// -----------------------------------------------------------------------
// <copyright file="ObjectAnnotationNode.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Nodes
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;

    using Rave.HierarchyBrowser.Infrastructure.Annotations;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;

    /// <summary>
    /// Object annotation node.
    /// </summary>
    public class ObjectAnnotationNode
    {
        /// <summary>
        /// The type definition.
        /// </summary>
        private readonly ITypeDefinition typeDefinition;

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectAnnotationNode"/> class.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        public ObjectAnnotationNode(ObjectAnnotationNode parent, IObjectInstance objectInstance) 
            : this(parent)
        {
            this.ObjectInstance = objectInstance;

            if (null != objectInstance)
            {
                this.typeDefinition = objectInstance.TypeDefinitions.FindTypeDefinition(objectInstance.TypeName);
            }

            this.InitAnnotations();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectAnnotationNode"/> class.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="proxyObject">
        /// The proxy object.
        /// </param>
        public ObjectAnnotationNode(ObjectAnnotationNode parent, ITemplateProxyObject proxyObject)
            : this(parent)
        {
            this.TemplateProxyObject = proxyObject;

            // TODO

            this.InitAnnotations();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectAnnotationNode"/> class.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        private ObjectAnnotationNode(ObjectAnnotationNode parent)
        {
            if (null != parent)
            {
                this.Parent = parent;
                parent.Children.Add(this);
            }

            this.Annotations = new List<ObjectAnnotation>();
            this.GameAnnotations = new List<ObjectAnnotation>();
            this.Children = new List<ObjectAnnotationNode>();
        }

        /// <summary>
        /// Gets the parent.
        /// </summary>
        public ObjectAnnotationNode Parent { get; private set; }

        /// <summary>
        /// Gets the object instance.
        /// </summary>
        public IObjectInstance ObjectInstance { get; private set; }

        /// <summary>
        /// Gets the template proxy object.
        /// </summary>
        public ITemplateProxyObject TemplateProxyObject { get; private set; }

        /// <summary>
        /// Gets the annotations.
        /// </summary>
        public IList<ObjectAnnotation> Annotations { get; private set; }

        /// <summary>
        /// Gets the game annotations.
        /// </summary>
        public IList<ObjectAnnotation> GameAnnotations { get; private set; } 

        /// <summary>
        /// Gets the children.
        /// </summary>
        public IList<ObjectAnnotationNode> Children { get; private set; }

        /// <summary>
        /// Initialize the annotations.
        /// </summary>
        public void InitAnnotations()
        {
            this.Annotations.Clear();

            if (null == this.typeDefinition || null == this.ObjectInstance.Node)
            {
                return;
            }

            foreach (var fieldDef in this.typeDefinition.Fields)
            {
                var node = this.ObjectInstance.Node;
                var exposedAsAttr = node.Attributes["ExposeAs"];
                var name = (null == exposedAsAttr || string.IsNullOrEmpty(exposedAsAttr.Value))
                               ? fieldDef.Name
                               : exposedAsAttr.Value;

                var fieldNode = FindXmlNode(name, node);

                if (null != fieldNode)
                {
                    var value = fieldNode.InnerText;
                    if (string.IsNullOrEmpty(value))
                    {
                        value = fieldDef.DefaultValue;
                    }

                    if (!string.IsNullOrEmpty(value))
                    {
                        var isDefaultValue = fieldNode.InnerText == fieldDef.DefaultValue;
                        var annotation = new ObjectAnnotation(name, value, isDefaultValue);
                        this.Annotations.Add(annotation);
                    }
                }
            }
        }

        /// <summary>
        /// The find xml node.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <returns>
        /// The xml node <see cref="XmlNode"/>.
        /// </returns>
        private static XmlNode FindXmlNode(string name, XmlNode parent)
        {
            return parent == null ? null : parent.ChildNodes.Cast<XmlNode>().FirstOrDefault(n => n.Name == name);
        }

        /// <summary>
        /// Get an annotation.
        /// </summary>
        /// <param name="name">
        /// The annotation name.
        /// </param>
        /// <returns>
        /// The annotation <see cref="ObjectAnnotation"/>.
        /// </returns>
        public ObjectAnnotation GetAnnotation(string name)
        {
            return this.Annotations.FirstOrDefault(annotation => annotation.Name == name);
        }

        /// <summary>
        /// Get a game annotation.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The annotation <see cref="ObjectAnnotation"/>.
        /// </returns>
        public ObjectAnnotation GetGameAnnotation(string name)
        {
            return this.GameAnnotations.FirstOrDefault(annotation => annotation.Name == name);
        }

        /// <summary>
        /// Set a game annotation value.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="isDefaultValue">
        /// The is default value flag.
        /// </param>
        public void SetGameAnnotation(string name, string value, bool isDefaultValue)
        {
            ObjectAnnotation annotation = this.GameAnnotations.FirstOrDefault(a => a.Name == name);
            if (null != annotation)
            {
                annotation.Value = value;
                annotation.IsDefaultValue = annotation.IsDefaultValue;
            }
            else
            {
                var newAnnotation = new ObjectAnnotation(name, value, isDefaultValue);
                this.GameAnnotations.Add(newAnnotation);
            }
        }
    }
}
