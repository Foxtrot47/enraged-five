// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SoundHeirarchyNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The heirarchy node.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Nodes
{
    using System.Windows.Forms;

    /// <summary>
    /// The heirarchy node.
    /// </summary>
    public class HierarchyNode : TreeNode
    {
        #region Constants

        /// <summary>
        /// The invalid reference string.
        /// </summary>
        protected const string InvalidReference = "(invalid reference)";

        #endregion

        #region Fields

        /// <summary>
        /// The object name.
        /// </summary>
        private string objectName;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the object name.
        /// </summary>
        public string ObjectName
        {
            get
            {
                return this.objectName;
            }

            set
            {
                this.objectName = value;
                this.Text = this.objectName;
            }
        }

        #endregion
    }
}