﻿// -----------------------------------------------------------------------
// <copyright file="HierarchyBrowserModes.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Enums
{
    /// <summary>
    /// Hierarchy browser modes.
    /// </summary>
    public enum HierarchyBrowserModes
    {
        /// <summary>
        /// Default mode.
        /// </summary>
        Default,

        /// <summary>
        /// Categories mode.
        /// </summary>
        Categories,
    }
}
