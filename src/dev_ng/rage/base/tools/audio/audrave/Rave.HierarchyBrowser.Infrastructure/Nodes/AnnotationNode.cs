﻿// -----------------------------------------------------------------------
// <copyright file="AnnotationNode.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.HierarchyBrowser.Infrastructure.Nodes
{
    using System.Windows.Forms;

    /// <summary>
    /// Annotation node.
    /// </summary>
    public class AnnotationNode : TreeNode
    {
        /// <summary>
        /// The value.
        /// </summary>
        private string value;

        /// <summary>
        /// Gets or sets the property.
        /// </summary>
        public string Property { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.value = value;
                this.Text = string.Format("{0}: {1}", this.Property, value);
            }
        }

        public bool IsDefaultValue { get; set; }

        /// <summary>
        /// Gets or sets the unit.
        /// </summary>
        public string Unit { get; set; }
    }
}
