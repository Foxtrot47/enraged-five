namespace Rave.BuildManager.Infrastructure.Enums
{
    public enum MetadataBuildType
    {
        Code,
        Data
    }
}