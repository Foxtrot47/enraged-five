﻿namespace Rave.BuildManager.Infrastructure.Enums
{
    public enum ElementType
    {
        Exception,
        Error,
        Warning,
        Information,
    }
}