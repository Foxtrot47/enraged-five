namespace Rave.BuildManager.Infrastructure.Interfaces
{
    using System;
    using System.Collections.Generic;

    using Rave.BuildManager.Infrastructure.Enums;

    using rage;

    public interface IBuildManager
    {
        event Action SaveAllBanks;

        void Build(audBuildSet buildSet, string platformTag, ISet<string> packNames = null);

        void Build(MetadataBuildType metadataBuildType, audMetadataType metadataType);
    }
}