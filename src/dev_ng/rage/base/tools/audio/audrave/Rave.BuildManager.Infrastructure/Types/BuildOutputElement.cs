namespace Rave.BuildManager.Infrastructure.Types
{
    using System.Text;
    using System.Xml.Linq;

    using rage.ToolLib;

    using Rave.BuildManager.Infrastructure.Enums;

    public class BuildOutputElement
    {
        public BuildOutputElement(XElement node) : this(Enum<ElementType>.Parse(node.Name.ToString()), node.Value)
        {
            if (node.Attribute("context") != null)
            {
                this.Context = node.Attribute("context").Value;
            }
        }

        public BuildOutputElement(ElementType type, string msg, string context = null)
        {
            this.OutputElementType = type;
            this.Message = msg;
            this.Context = context;
        }

        public string Context { get; private set; }
        public string Message { get; private set; }
        public ElementType OutputElementType { get; private set; }

        public override string ToString()
        {
            switch (this.OutputElementType)
            {
                case ElementType.Information:
                    {
                        return this.Message;
                    }
                default:
                    {
                        return string.IsNullOrEmpty(this.Context)
                                   ? string.Format("{0}: {1}", this.OutputElementType, this.Message)
                                   : string.Format("{0}: {1} ({2})", this.OutputElementType, this.Message, this.GetFriendlyContext());
                    }
            }
        }

        public void GetFriendlyContextElements(out string file, out string metadataType, out string obj)
        {
            file = this.GetContextElement("File");
            metadataType = this.GetContextElement("MetadataType");
            obj = this.GetContextElement("Object");
        }

        private string GetFriendlyContext()
        {
            string file, metadataType, obj;
            this.GetFriendlyContextElements(out file, out metadataType, out obj);

            var builder = new StringBuilder();
            if (!string.IsNullOrEmpty(metadataType))
            {
                builder.Append(string.Format(" Type: {0}", metadataType));
            }

            if (!string.IsNullOrEmpty(file))
            {
                builder.Append(string.Format(" File: {0}", file));
            }

            if (!string.IsNullOrEmpty(obj))
            {
                builder.Append(string.Format(" Object: {0}", obj));
            }
            return builder.ToString();
        }

        private string GetContextElement(string name)
        {
            if (!string.IsNullOrEmpty(this.Context))
            {
                var typeStr = "[" + name + ":";
                var index = this.Context.IndexOf(typeStr);
                if (index != -1)
                {
                    var startIndex = index + typeStr.Length;
                    var endIndex = this.Context.IndexOf("]", startIndex);
                    return this.Context.Substring(startIndex, endIndex - startIndex);
                }
            }
            return string.Empty;
        }
    }
}