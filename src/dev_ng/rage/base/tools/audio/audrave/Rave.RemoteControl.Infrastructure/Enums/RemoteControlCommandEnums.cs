﻿// -----------------------------------------------------------------------
// <copyright file="RemoteControlCommandEnums.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Rave.RemoteControl.Infrastructure.Enums
{
    /// <summary>
    /// The remote control command enums.
    /// </summary>
    [Flags]
    public enum RemoteControlCommandEnums
    {
        /// <summary>
        /// TPlays the sound specified by name
        /// </summary>
        Playsound,

        /// <summary>
        /// Stops the last sound played
        /// </summary>
        Stopsound,

        /// <summary>
        /// No longer used; see WRITEMETADATACHUNK
        /// </summary>
        DeprecatedWritemetadata,

        /// <summary>
        /// No longer used; see OVERRIDEOBJECTCHUNK
        /// </summary>
        DeprecatedOverrideobject,

        /// <summary>
        /// Sent by the game on boot up to set RAVE's current platform
        /// </summary>
        Setruntimeplatform,

        /// <summary>
        /// No longer used; see REQUESTMETADATACHUNK
        /// </summary>
        DeprecatedRequestmetadata,

        /// <summary>
        /// Sent by the game to keep the connection alive
        /// </summary>
        Ping,

        /// <summary>
        /// Informs the game that an object has been viewed in RAVE
        /// PARAMS
        /// metadataTypeHash
        /// objectNameHash
        /// </summary>
        Viewobject,

        /// <summary>
        /// Toggles whether the runtime engine should do any rendering for viewed objects
        /// </summary>
        Toggleviewobject,

        /// <summary>
        /// Sent by the game to update RAVE with resolved category settings
        /// PARAMS
        /// u32 numCategories
        /// tCategorySettings categories[]
        /// </summary>
        Resolvedcategorydata,

        /// <summary>
        /// Sent by the remote client to RAVE to update an objects Xml
        /// PARAMS
        /// u32 stringLengthBytes
        /// char XmlString[stringLengthBytes]
        /// </summary>
        Editobject,

        /// <summary>
        /// Sent by the game to request latest in-memory metadata
        /// PARAMS
        /// metadataTypeHash
        /// chunkNameHash
        /// </summary>
        Requestmetadatachunk,

        /// <summary>
        /// Overrides an objects metadata - the data specified will be returned in place of
        /// the original metadata
        /// PARAMS
        /// metadataTypeHash
        /// chunkName/episode hash
        /// hash - the hashed name of the object 
        /// size (bytes) - the number of bytes to store
        /// data (byte[]) - data to store in metadata
        /// </summary>
        Overrideobjectchunk,

        /// <summary>
        /// Writes the specified data into metadata
        /// PARAMS
        /// metdataTypeHash
        /// chunkNameHash
        /// offset (bytes) - the offset into the metadata chunk to start writing
        /// size (bytes) - number of bytes to write
        /// data (byte[]) - data to store into metadata
        /// </summary>
        Writemetadatachunk,

        /// <summary>
        /// Sent from RAVE to the game to acknowledge succesful processing of an xml message
        /// </summary>
        Messagereceipt,

        /// <summary>
        /// TSent from RAVE to the game to request auditioning of the specified object
        /// </summary>
        AuditionStart,

        /// <summary>
        /// The audition stop.
        /// </summary>
        AuditionStop,

        /// <summary>
        /// Sent when a new object is created, or an existing object is modified
        /// PARAMS
        /// metadataTypeHash
        /// chunkName/episode hash
        /// objectName - the name of the object as a zero terminated string
        /// size (bytes) - the number of bytes to store
        /// data (byte[]) - data to store in metadata
        /// </summary>
        ObjectChanged,

        /// <summary>
        /// The category telemetry header.
        /// </summary>
        CategoryTelemetryHeader,

        /// <summary>
        /// The category telemetry update.
        /// </summary>
        CategoryTelemetryUpdate,
        
        ///<summary>
        ///The Mute command
        ///</summary>
        SetMuteList,

        /// <summary>
        /// The solo command
        /// </summary>
        SetSoloList,

		/// <summary>
		/// Audio Engine Loaded
		/// </summary>
		AudioEngineLoaded,
        
		/// <summary>
		/// Preview bank created, send for reload by game
		/// </summary>
		PrievewBank,
		/// <summary>
        /// The last command id.
        /// </summary>
        LastCommandId

    }
}
