// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRemoteControl.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the IRemoteControl type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.RemoteControl.Infrastructure.Interfaces
{
    using System;
    using System.Collections.Generic;

    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    /// The RemoteControl interface.
    /// </summary>
    public interface IRemoteControl
    {
        /// <summary>
        /// The on view object.
        /// </summary>
        event Action<IObjectInstance> OnViewObject;

        /// <summary>
        /// Gets the number of clients.
        /// </summary>
        int NumClients { get; }

        /// <summary>
        /// Gets the listeners.
        /// </summary>
        IList<IRemoteMessageListener> Listeners { get; }

        /// <summary>
        /// Abort the listener thread.
        /// </summary>
        void Abort();

        /// <summary>
        /// The object instance on object modified handler.
        /// </summary>
        /// <param name="modifiedObject">
        /// The modified object.
        /// </param>
        void ObjectInstance_OnObjectModified(IObjectInstance modifiedObject);

        /// <summary>
        /// Play a sound.
        /// </summary>
        /// <param name="soundName">
        /// The sound name.
        /// </param>
        void PlaySound(string soundName);

        /// <summary>
        /// Sets the mute list.
        /// </summary>
        /// <param name="typeNameObjectNameMutedList">A list of keyValuePair where typename is key and pair is value</param>
        void SetMuteList(List<KeyValuePair<string, string>> typeNameObjectNameMutedList);

        /// <summary>
        /// Sets the solo list.
        /// </summary>
        /// <param name="typeNameObjectNameMutedList">A list of keyValuePair where typename is key and pair is value</param>
        void SetSoloList(List<KeyValuePair<string, string>> typeNameObjectNameMutedList);

        /// <summary>
        /// Remove a client.
        /// </summary>
        /// <param name="client">
        /// The client.
        /// </param>
        void RemoveClient(IRemoteControlClientState client);

        /// <summary>
        /// Shutdown the remote control.
        /// </summary>
        void Shutdown();

        /// <summary>
        /// Start the remote control.
        /// </summary>
        void Start();

        /// <summary>
        /// Start auditioning an object.
        /// </summary>
        /// <param name="metadataTypeHash">
        /// The metadata type hash.
        /// </param>
        /// <param name="objectNameHash">
        /// The object name hash.
        /// </param>
        void StartAuditioningObject(uint metadataTypeHash, uint objectNameHash);

        /// <summary>
        /// Stop auditioning an object.
        /// </summary>
        /// <param name="metadataTypeHash">
        /// The metadata type hash.
        /// </param>
        /// <param name="objectNameHash">
        /// The object name hash.
        /// </param>
        void StopAuditioningObject(uint metadataTypeHash, uint objectNameHash);

        /// <summary>
        /// Stop current sound.
        /// </summary>
        void StopSound();

        /// <summary>
        /// View an object.
        /// </summary>
        /// <param name="metadataTypeHash">
        /// The metadata type hash.
        /// </param>
        /// <param name="objectNameHash">
        /// The object name hash.
        /// </param>
        void ViewObject(uint metadataTypeHash, uint objectNameHash);

        /// <summary>
        /// Add a remote message listener.
        /// </summary>
        /// <param name="listener">
        /// The listener.
        /// </param>
        void AddRemoteMessageListener(IRemoteMessageListener listener);

        /// <summary>
        /// Remove a remote message listener.
        /// </summary>
        /// <param name="listener">
        /// The listener.
        /// </param>
        void RemoveRemoteMessageListener(IRemoteMessageListener listener);


	    void SendBankPreviewUpdated(string bankPath);
    }
}
