﻿// -----------------------------------------------------------------------
// <copyright file="IRemoteControlClientState.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.RemoteControl.Infrastructure.Interfaces
{
    using System;
    using System.Net.Sockets;
    using System.Threading;

    using rage;

    using Rave.Metadata.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public interface IRemoteControlClientState
    {
        /// <summary>
        /// The on view object.
        /// </summary>
        event Action<IObjectInstance> OnViewObject;

        /// <summary>
        /// Gets the compiler.
        /// </summary>
        IXmlBankMetaDataCompiler Compiler { get; }

        /// <summary>
        /// Gets the platform settings.
        /// </summary>
        PlatformSetting PlatformSettings { get; }

        /// <summary>
        /// Gets the TCP client.
        /// </summary>
        TcpClient TcpClient { get; }

        /// <summary>
        /// Gets the thread.
        /// </summary>
        Thread Thread { get; }

        /// <summary>
        /// The abort.
        /// </summary>
        void Abort();

        /// <summary>
        /// The add object override.
        /// </summary>
        /// <param name="metadataTypeHash">
        /// The metadata type hash.
        /// </param>
        /// <param name="chunkNameHash">
        /// The chunk name hash.
        /// </param>
        /// <param name="objectNameHash">
        /// The object name hash.
        /// </param>
        /// <param name="dataSize">
        /// The data size.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        void AddObjectOverride(
            uint metadataTypeHash, uint chunkNameHash, uint objectNameHash, int dataSize, byte[] data);

        /// <summary>
        /// The on object modified.
        /// </summary>
        /// <param name="modifiedObject">
        /// The modified object.
        /// </param>
        void OnObjectModified(IObjectInstance modifiedObject);

        /// <summary>
        /// The play sound.
        /// </summary>
        /// <param name="soundName">
        /// The sound name.
        /// </param>
        void PlaySound(string soundName);

        /// <summary>
        /// The send object changed message.
        /// </summary>
        /// <param name="metadataTypeHash">
        /// The metadata type hash.
        /// </param>
        /// <param name="chunkNameHash">
        /// The chunk name hash.
        /// </param>
        /// <param name="objectName">
        /// The object name.
        /// </param>
        /// <param name="dataSize">
        /// The data size.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        void SendObjectChangedMessage(
            uint metadataTypeHash, uint chunkNameHash, string objectName, int dataSize, byte[] data);

        /// <summary>
        /// The start auditioning object.
        /// </summary>
        /// <param name="metadataTypeHash">
        /// The metadata type hash.
        /// </param>
        /// <param name="objectNameHash">
        /// The object name hash.
        /// </param>
        void StartAuditioningObject(uint metadataTypeHash, uint objectNameHash);

        /// <summary>
        /// The stop auditioning object.
        /// </summary>
        /// <param name="metadataTypeHash">
        /// The metadata type hash.
        /// </param>
        /// <param name="objectNameHash">
        /// The object name hash.
        /// </param>
        void StopAuditioningObject(uint metadataTypeHash, uint objectNameHash);

        /// <summary>
        /// The stop sound.
        /// </summary>
        void StopSound();

        /// <summary>
        /// The toggle view object.
        /// </summary>
        /// <param name="metadataTypeHash">
        /// The metadata type hash.
        /// </param>
        void ToggleViewObject(uint metadataTypeHash);

        /// <summary>
        /// The view object.
        /// </summary>
        /// <param name="metadataTypeHash">
        /// The metadata type hash.
        /// </param>
        /// <param name="objectNameHash">
        /// The object name hash.
        /// </param>
        void ViewObject(uint metadataTypeHash, uint objectNameHash);

        /// <summary>
        /// The write metadata.
        /// </summary>
        /// <param name="metadataTypeHash">
        /// The metadata type hash.
        /// </param>
        /// <param name="chunkNameHash">
        /// The chunk name hash.
        /// </param>
        /// <param name="objectNameHash">
        /// The object name hash.
        /// </param>
        /// <param name="dataSize">
        /// The data size.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        void WriteMetadata(
            uint metadataTypeHash, uint chunkNameHash, uint objectNameHash, int dataSize, byte[] data);

        void SetSoloList(System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<uint, uint>> uintList);

        void SetMuteList(System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<uint, uint>> uintList);
	    
		void SendBankPreviewUpdated(string path);
    }
}
