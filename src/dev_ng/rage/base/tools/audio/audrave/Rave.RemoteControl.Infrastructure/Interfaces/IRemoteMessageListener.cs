﻿// -----------------------------------------------------------------------
// <copyright file="IRemoteMessageListener.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.RemoteControl.Infrastructure.Interfaces
{
    using Rave.RemoteControl.Infrastructure.Enums;

    /// <summary>
    /// Remote message listener interface.
    /// </summary>
    public interface IRemoteMessageListener
    {
        /// <summary>
        /// Receive a message.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="isBigEndian">
        /// The is big endian flag.
        /// </param>
        void ReceiveMessage(RemoteControlCommandEnums command, byte[] payload, bool isBigEndian);
    }
}
