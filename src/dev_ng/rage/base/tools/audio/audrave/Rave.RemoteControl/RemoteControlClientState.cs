﻿// -----------------------------------------------------------------------
// <copyright file="RemoteControlClientState.cs" company="Rockstar North">
//
// </copyright>
// -----------------------------------------------------------------------

using rage.Fields;

namespace Rave.RemoteControl
{
	using rage;
	using rage.Compiler;
	using rage.ToolLib;
	using rage.ToolLib.Writer;
	using Rave.Controls.WPF.UXTimer;
	using Rave.Instance;
	using Rave.Metadata;
	using Rave.Metadata.Infrastructure.Interfaces;
	using Rave.RemoteControl.Infrastructure.Enums;
	using Rave.RemoteControl.Infrastructure.Interfaces;
	using Rave.TypeDefinitions.Infrastructure.Interfaces;
	using Rave.Types;
	using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
	using Rave.Types.Infrastructure.Interfaces.Objects;
	using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;
	using Rave.Types.Infrastructure.Structs;
	using Rave.Types.Objects.TypedObjects;
	using Rave.Utils;
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Net.Sockets;
	using System.Text;
	using System.Threading;
	using System.Windows.Forms;
	using System.Xml;
	using System.Xml.Linq;
	using BinaryWriter = rage.ToolLib.Writer.BinaryWriter;

	/// <summary>
	/// The remote control client state.
	/// </summary>
	public class RemoteControlClientState : IRemoteControlClientState
	{
		/// <summary>
		/// The remote control.
		/// </summary>
		private readonly RemoteControl remoteControl;

		/// <summary>
		/// The requested metadata types.
		/// </summary>
		private readonly Dictionary<uint, List<uint>> requestedMetadataTypes;

		/// <summary>
		/// The thread.
		/// </summary>
		private readonly Thread thread;

		/// <summary>
		/// Gets the compiler.
		/// </summary>
		public IXmlBankMetaDataCompiler Compiler { get; private set; }

		/// <summary>
		/// Gets the platform settings.
		/// </summary>
		public PlatformSetting PlatformSettings
		{
			get
			{
				return this.Compiler.ProjectSettings.GetCurrentPlatform();
			}
		}

		/// <summary>
		/// Gets the TCP client.
		/// </summary>
		public TcpClient TcpClient { get; private set; }

		/// <summary>
		/// Gets the thread.
		/// </summary>
		public Thread Thread
		{
			get
			{
				return this.thread;
			}
		}

		/// <summary>
		/// The on view object.
		/// </summary>
		public event Action<IObjectInstance> OnViewObject;

		/// <summary>
		/// Initializes a new instance of the <see cref="RemoteControlClientState"/> class.
		/// </summary>
		/// <param name="remoteControl">
		/// The remote control.
		/// </param>
		/// <param name="tcpClient">
		/// The TCP client.
		/// </param>
		/// <param name="compiler">
		/// The compiler.
		/// </param>
		public RemoteControlClientState(RemoteControl remoteControl, TcpClient tcpClient, IXmlBankMetaDataCompiler compiler)
		{
			this.remoteControl = remoteControl;
			this.TcpClient = tcpClient;
			this.thread = new Thread(this.ReceiveThreadMain)
							  {
								  Priority = ThreadPriority.BelowNormal,
								  IsBackground = true
							  };
			this.Compiler = compiler ?? new XmlBankMetaDataCompiler(RaveInstance.RaveLog, null);

			// set up default platform
			this.Compiler.ProjectSettings.SetCurrentPlatform(this.Compiler.ProjectSettings.getDefaultPlatform());

			this.requestedMetadataTypes = new Dictionary<uint, List<uint>>();
			this.thread.Start();
		}


		public void Abort()
		{
			if ((this.TcpClient != null) && this.TcpClient.Connected)
			{
				this.TcpClient.GetStream().Close();
				this.TcpClient.Close();
			}
		}

		/// <summary>
		/// The add object override.
		/// </summary>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="chunkNameHash">
		/// The chunk name hash.
		/// </param>
		/// <param name="objectNameHash">
		/// The object name hash.
		/// </param>
		/// <param name="dataSize">
		/// The data size.
		/// </param>
		/// <param name="data">
		/// The data.
		/// </param>
		public void AddObjectOverride(
			uint metadataTypeHash, uint chunkNameHash, uint objectNameHash, int dataSize, byte[] data)
		{
			this.SendMetadataMessage(
				RemoteControlCommandEnums.Overrideobjectchunk,
				metadataTypeHash,
				chunkNameHash,
				objectNameHash,
				dataSize,
				data);
		}

		/// <summary>
		/// The on object modified.
		/// </summary>
		/// <param name="modifiedObject">
		/// The modified object.
		/// </param>
		public void OnObjectModified(IObjectInstance modifiedObject)
		{
			if (this.TcpClient.Connected && modifiedObject.Node != null)
			{
				var metadataType = modifiedObject.Type;
				var metadataMgr = this.Compiler.MetadataManagers[metadataType];

				if (null != modifiedObject.Bank as ITemplateDummyBank)
				{
					if (RaveInstance.AllBankManagers[modifiedObject.Type + "_TEMPLATE"] != null)
					{
						// we're modifying a template definition - ensure the compiler is updated
						// with the latest template, but for now don't worry about updating any objects
						// that use this template
						metadataMgr.Compiler.ClearTemplates();
						foreach (var bank in RaveInstance.AllBankManagers[modifiedObject.Type + "_TEMPLATE"].Banks)
						{
							metadataMgr.Compiler.LoadTemplates(rage.ToolLib.Utility.ToXDoc(bank.Document));
						}
					}

					// we don't want to try and compile this template definition as an object so bail out
					return;
				}

				try
				{

					System.Diagnostics.Debug.WriteLine(modifiedObject.Name);
					var newDoc = new XmlDocument();
					newDoc.AppendChild(newDoc.CreateElement("Objects"));
					newDoc.DocumentElement.AppendChild(newDoc.ImportNode(modifiedObject.Node, true));

					var doc = rage.ToolLib.Utility.ToXDoc(newDoc);

					// Initialise document entries list
					var docEntry = new DocumentEntry { Document = new XDocument(doc) };

					// Get metadata file we require for modified object
					audMetadataFile metadataFile = null;
					var episodeLower = modifiedObject.Episode.ToLower();
					var metadataTypeLower = metadataType.ToLower();
					foreach (var file in Configuration.MetadataFiles)
					{
						if (file.Episode.ToLower().Equals(episodeLower) && file.Type.ToLower().Equals(metadataTypeLower))
						{
							metadataFile = file;
							break;
						}
					}

					// Check we found metadata file
					if (null == metadataFile)
					{
						throw new Exception("Failed to find metadata file for modified object");
					}

					var docEntries = new List<DocumentEntry> { docEntry };

					// Set editMode to true (only need to run generators required in edit mode)
					if (!metadataMgr.Compiler.Generate(docEntries, metadataFile, true))
					{
						throw new Exception("Failed to generate object");
					}

					// Compile and send each document entry in list
					foreach (var documentEntry in docEntries)
					{
						newDoc = rage.ToolLib.Utility.ToXmlDoc(documentEntry.Document);

						foreach (XmlNode node in newDoc.DocumentElement.ChildNodes)
						{
							var objName = node.Attributes["name"].Value;
							this.CompileAndSendObjectChangedMetaData(modifiedObject, metadataMgr, node, objName);
						}
					}
				}
				catch (Exception ex)
				{
					ErrorManager.HandleError(ex);
				}
			}
		}

		/// <summary>
		/// The play sound.
		/// </summary>
		/// <param name="soundName">
		/// The sound name.
		/// </param>
		public void PlaySound(string soundName)
		{
			if (this.TcpClient.Connected)
			{
				this.Send(this.GetBytes(RemoteControlCommandEnums.Playsound, Encoding.ASCII.GetBytes(soundName)));
			}
		}

		/// <summary>
		/// The send object changed message.
		/// </summary>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="chunkNameHash">
		/// The chunk name hash.
		/// </param>
		/// <param name="objectName">
		/// The object name.
		/// </param>
		/// <param name="dataSize">
		/// The data size.
		/// </param>
		/// <param name="data">
		/// The data.
		/// </param>
		public void SendObjectChangedMessage(
			uint metadataTypeHash, uint chunkNameHash, string objectName, int dataSize, byte[] data)
		{
			if (this.TcpClient.Connected && this.HasRequestedData(metadataTypeHash, chunkNameHash))
			{
				if (Monitor.TryEnter(this, 500))
				{
					try
					{
						using (var ms = new MemoryStream())
						{
							using (var writer = new BinaryWriter(ms, this.PlatformSettings.IsBigEndian))
							{
								writer.Write((byte)RemoteControlCommandEnums.ObjectChanged);
								using (new ChunkSizeWriter(writer))
								{
									writer.Write(metadataTypeHash);
									writer.Write(chunkNameHash);
									writer.Write(dataSize);
									writer.Write(Encoding.ASCII.GetBytes(objectName), false);
									writer.Write((byte)0);
									writer.Write(data, false);
								}
							}

							this.Send(ms.ToArray());
						}

					}
					finally
					{
						Monitor.Exit(this);
					}
				}
			}
		}

		public void SetMuteList(List<KeyValuePair<uint, uint>> typeNameObjectNameHashes)
		{
			this.SendMuteSoloMessage(RemoteControlCommandEnums.SetMuteList, typeNameObjectNameHashes);
		}

		public void SetSoloList(List<KeyValuePair<uint, uint>> typeNameObjectNameHashes)
		{
			this.SendMuteSoloMessage(RemoteControlCommandEnums.SetSoloList | RemoteControlCommandEnums.Stopsound, typeNameObjectNameHashes);
		}

        public void SendBankPreviewUpdated(string path)
        {
            if (this.TcpClient.Connected)
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    using (rage.ToolLib.Writer.BinaryWriter writer = new rage.ToolLib.Writer.BinaryWriter(stream, this.PlatformSettings.IsBigEndian))
                    {
                        writer.Write((byte)RemoteControlCommandEnums.PrievewBank);
                        using (new ChunkSizeWriter(writer))
                        {

                            writer.Write(path);
                        }
                    }
                    this.Send(stream.ToArray());
                }
            }
        }

		/// <summary>
		/// The start auditioning object.
		/// </summary>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="objectNameHash">
		/// The object name hash.
		/// </param>
		public void StartAuditioningObject(uint metadataTypeHash, uint objectNameHash)
		{
			this.SendTypeAndObjectNameHashMessage(RemoteControlCommandEnums.AuditionStart, metadataTypeHash, objectNameHash);
		}

		/// <summary>
		/// The stop auditioning object.
		/// </summary>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="objectNameHash">
		/// The object name hash.
		/// </param>
		public void StopAuditioningObject(uint metadataTypeHash, uint objectNameHash)
		{
			this.SendTypeAndObjectNameHashMessage(RemoteControlCommandEnums.AuditionStop, metadataTypeHash, objectNameHash);
		}

		/// <summary>
		/// The stop sound.
		/// </summary>
		public void StopSound()
		{
			if (this.TcpClient.Connected)
			{
				this.Send(this.GetBytes(RemoteControlCommandEnums.Stopsound, null));
			}
		}

		/// <summary>
		/// The toggle view object.
		/// </summary>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		public void ToggleViewObject(uint metadataTypeHash)
		{
			if (this.TcpClient.Connected && this.requestedMetadataTypes.ContainsKey(metadataTypeHash))
			{
				using (var ms = new MemoryStream())
				{
					using (var writer = new rage.ToolLib.Writer.BinaryWriter(ms, this.PlatformSettings.IsBigEndian))
					{
						writer.Write((byte)RemoteControlCommandEnums.Toggleviewobject);
						using (new ChunkSizeWriter(writer))
						{
							writer.Write(metadataTypeHash);
						}
					}

					this.Send(ms.ToArray());
				}
			}
		}

		/// <summary>
		/// The view object.
		/// </summary>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="objectNameHash">
		/// The object name hash.
		/// </param>
		public void ViewObject(uint metadataTypeHash, uint objectNameHash)
		{
			this.SendTypeAndObjectNameHashMessage(RemoteControlCommandEnums.Viewobject, metadataTypeHash, objectNameHash);
		}

		/// <summary>
		/// The write metadata.
		/// </summary>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="chunkNameHash">
		/// The chunk name hash.
		/// </param>
		/// <param name="objectNameHash">
		/// The object name hash.
		/// </param>
		/// <param name="dataSize">
		/// The data size.
		/// </param>
		/// <param name="data">
		/// The data.
		/// </param>
		public void WriteMetadata(
			uint metadataTypeHash, uint chunkNameHash, uint objectNameHash, int dataSize, byte[] data)
		{
			this.SendMetadataMessage(
				RemoteControlCommandEnums.Writemetadatachunk,
				metadataTypeHash,
				chunkNameHash,
				objectNameHash,
				dataSize,
				data);
		}

		/// <summary>
		/// Get the metadata file.
		/// </summary>
		/// <param name="metadataTypeName">
		/// The metadata type name.
		/// </param>
		/// <param name="chunkNameHash">
		/// The chunk name hash.
		/// </param>
		/// <returns>
		/// The metadata file <see cref="audMetadataFile"/>.
		/// </returns>
		private static audMetadataFile GetMetadataFile(string metadataTypeName, uint chunkNameHash)
		{
			foreach (var metadataFile in Configuration.MetadataFiles)
			{
				if (System.String.Compare(metadataFile.Type, metadataTypeName, StringComparison.OrdinalIgnoreCase) == 0)
				{
					var hash = new Hash { Value = metadataFile.Episode };
					if (hash.Key == chunkNameHash)
					{
						return metadataFile;
					}
				}
			}

			throw new ApplicationException(
				string.Format(
                    "Unable to find MetadataFile for type \"{0}\" and episode \"{1}\".",
					metadataTypeName,
					chunkNameHash));
		}

		/// <summary>
		/// Handle the metadata compile error.
		/// </summary>
		/// <param name="context">
		/// The context.
		/// </param>
		/// <param name="error">
		/// The error.
		/// </param>
		/// <param name="_">
		/// Not required.
		/// </param>
		private static void HandleMetadataCompileError(string context, string error, bool _)
		{
			UXTimer.Instance.AppendNewLine(string.Format("{0} - {1}", context, error));
		}

		/// <summary>
		/// Handle the metadata compile warning.
		/// </summary>
		/// <param name="context">
		/// The context.
		/// </param>
		/// <param name="exception">
		/// The exception.
		/// </param>
		private static void HandleMetadataCompileWarning(string context, Exception exception)
		{
			UXTimer.Instance.AppendNewLine(string.Format("{0} - {1}", context, exception.Message));
		}

		private void AppendLog(string message)
		{
			string messageToPrint = message;
			if (this.TcpClient.Connected)
			{
				messageToPrint = this.TcpClient.Client.RemoteEndPoint.ToString() + ": " + message;
			}
			UXTimer.Instance.AppendNewLine(messageToPrint);
		}

		/// <summary>
		/// The compile and send object changed meta data.
		/// </summary>
		/// <param name="modifiedObject">
		/// The modified object.
		/// </param>
		/// <param name="metadataMgr">
		/// The metadata manager.
		/// </param>
		/// <param name="node">
		/// The node.
		/// </param>
		/// <param name="name">
		/// The name.
		/// </param>
		private void CompileAndSendObjectChangedMetaData(
			IObjectInstance modifiedObject, IMetadataManager metadataMgr, XmlNode node, string name)
		{
			if (Monitor.TryEnter(this, 500))
			{
				try
				{
					var metadataFiles = new List<audMetadataFile> { modifiedObject.Bank.MetadataFile };
					if (modifiedObject.Bank.LinkedMetadataFiles != null)
					{
						// we also need to compile and send metadata for all of the other linked metadata files (if any)
						metadataFiles.AddRange(modifiedObject.Bank.LinkedMetadataFiles);
					}

					foreach (var metadataFile in metadataFiles)
					{
						var memoryStream = new MemoryStream();
						IWriter output = new rage.ToolLib.Writer.BinaryWriter(
							memoryStream, this.PlatformSettings.IsBigEndian);

						// Write 8 bytes to compensate for the normal 8 byte header in the metadata compiler
						output.Write((UInt32)0);
						output.Write((UInt32)0);

						try
						{
							metadataMgr.Compiler.LoggedError += HandleMetadataCompileError;
							metadataMgr.Compiler.LoggedException += HandleMetadataCompileWarning;

							var outputDoc = metadataMgr.CompileObject(node, output, metadataFile);

							var updatedObj = outputDoc.Root.Elements().FirstOrDefault();

							// Get name from updated object if it exists, otherwise use specified name
							var updatedName = name;
							if (null != updatedObj)
							{
								updatedName = updatedObj.Attribute("name").Value;
							}

							var objectSize = (int)output.Tell() - 8;
							var adjustedBuf = new byte[objectSize];
							Array.Copy(memoryStream.GetBuffer(), 8, adjustedBuf, 0, objectSize);

							this.SendObjectChangedMessage(
								metadataMgr.ComputeHash(metadataFile.Type.ToUpper()),
								metadataMgr.ComputeHash(metadataFile.Episode.ToUpper()),
								updatedName,
								objectSize,
								adjustedBuf);
						}
						catch (Exception ex)
						{
							ErrorManager.HandleException("Compile Error", ex);
							UXTimer.Instance.AppendNewLine(string.Format("Compile Error - {0}", ex));
						}
						finally
						{
							metadataMgr.Compiler.LoggedError -= HandleMetadataCompileError;
							metadataMgr.Compiler.LoggedException -= HandleMetadataCompileWarning;
						}
					}
				}
				finally
				{
					Monitor.Exit(this);

				}
			}

		}


		/// <summary>
		/// The get bytes.
		/// </summary>
		/// <param name="commandEnum">
		/// The command.
		/// </param>
		/// <param name="payload">
		/// The payload.
		/// </param>
		/// <returns>
		/// The bytes <see cref="byte[]"/>.
		/// </returns>
		private byte[] GetBytes(RemoteControlCommandEnums commandEnum, byte[] payload)
		{
			using (var ms = new MemoryStream())
			{
				using (var writer = new BinaryWriter(ms, this.PlatformSettings.IsBigEndian))
				{
					writer.Write((byte)commandEnum);
					using (new ChunkSizeWriter(writer))
					{
						if (payload != null)
						{
							writer.Write(payload, false);
						}
					}
				}

				return ms.ToArray();
			}
		}

		/// <summary>
		/// The handle received data.
		/// </summary>
		/// <param name="commandId">
		/// The command id.
		/// </param>
		/// <param name="payload">
		/// The payload.
		/// </param>
		/// <returns>
		/// The <see cref="bool"/>.
		/// </returns>
		private bool HandleReceivedData(byte commandId, byte[] payload)
		{
			var command = (RemoteControlCommandEnums)commandId;

			// Inform any listeners of received message
			if (Monitor.TryEnter(this.remoteControl.Listeners, 500))
			{

				try
				{
					foreach (var listener in this.remoteControl.Listeners)
					{
						listener.ReceiveMessage(command, payload, this.PlatformSettings.IsBigEndian);
					}
				}
				finally
				{
					Monitor.Exit(this.remoteControl.Listeners);
				}
			}
			else
			{
				return false;
			}

			switch (command)
			{
				case RemoteControlCommandEnums.Requestmetadatachunk:
					{
						var metadataTypeHash = BitConverter.ToUInt32(payload, 0);
						if (this.PlatformSettings.IsBigEndian)
						{
							metadataTypeHash = RaveUtils.SwapUInt32(metadataTypeHash);
						}

                        audMetadataType metadataType = RaveUtils.FindMetadataTypeFromHash(metadataTypeHash);
                        var metadataTypeName = metadataType.Type;

						var chunkNameHash = BitConverter.ToUInt32(payload, 4);
						if (this.PlatformSettings.IsBigEndian)
						{
							chunkNameHash = RaveUtils.SwapUInt32(chunkNameHash);
						}

						var codeSchemaVersion = BitConverter.ToUInt32(payload, 8);
						if (this.PlatformSettings.IsBigEndian)
						{
							codeSchemaVersion = RaveUtils.SwapUInt32(codeSchemaVersion);
						}

						// build a list of object stores based on the requested chunk name
						var bankList = new List<IObjectBank>();
						foreach (var bank in RaveInstance.AllBankManagers[metadataTypeName].ObjectBanks)
						{
							// match all BASE banks as well as all banks in the current episode
							var episodes = new List<string> { bank.Episode };
							if (bank.LinkedMetadataFiles != null)
							{
								episodes.AddRange(bank.LinkedMetadataFiles.Select(x => x.Episode.ToUpper()));
							}

							if (
								episodes.Select(episode => new Hash { Value = episode })
										.Any(hash => hash.Key == chunkNameHash))
							{
								bankList.Add(bank);
							}
						}

						var compiler = this.Compiler.MetadataManagers[metadataTypeName].Compiler;

						// If we have a more up to date schema version than code, we will fail quickly, so instead pull a previous version
						var ourSchemaVersion = compiler.Version;
						if (ourSchemaVersion < codeSchemaVersion)
						{
							MessageBox.Show(
								RaveInstance.ActiveWindow,
								string.Format(
									"Code schema version: {0} is greater than the latest definitions schema version: "
									+ "{1} for \"{2}\" metadata. Objects cannot be compiled by Rave.",
									codeSchemaVersion,
									ourSchemaVersion,
									metadataTypeName),
								"Error",
								MessageBoxButtons.OK);
							return false;
						}

						var currSchemaVersion = ourSchemaVersion;
						while (currSchemaVersion != codeSchemaVersion)
						{
							try
							{
								var typeDefinitionPaths = compiler.TypeDefinitionPaths;
								if (typeDefinitionPaths != null)
								{
									foreach (var typeDefPath in typeDefinitionPaths)
									{
										RaveInstance.AssetManager.GetPrevious(typeDefPath, true);
									}
								}
							}
							catch (Exception e)
							{
								ErrorManager.HandleError(e);
								break;
							}

							compiler.ReloadTypeDefinitions();
							currSchemaVersion = compiler.Version;
						}

						var metadataFile = GetMetadataFile(metadataTypeName, chunkNameHash);
                        bool isTaggingEnabled = (metadataType.SchemaPath != null && File.Exists(metadataType.SchemaPath) &&
                                    File.ReadAllText(metadataType.SchemaPath).IndexOf(FieldDefinitionTokens.Tagged.ToString(), StringComparison.OrdinalIgnoreCase) >= 0);
					    isTaggingEnabled = isTaggingEnabled && Configuration.ProjectSettings.MetadataTagsEnabled;

						AppendLog("Requested metadata - " + metadataTypeName + ", " + metadataFile.Episode);

						var ms = TypeUtils.CompileMetadataInMemory(
							this.Compiler,
							new List<IXmlBank>(bankList),
                            metadataFile,
                            isTaggingEnabled);
						if (ms != null)
						{
							// first 4 bytes are length
							var lengthInPlatformOrder = (uint)ms.Length;
							if (this.PlatformSettings.IsBigEndian)
							{
								lengthInPlatformOrder = RaveUtils.SwapUInt32(lengthInPlatformOrder);
							}

							this.TcpClient.GetStream().Write(BitConverter.GetBytes(lengthInPlatformOrder), 0, 4);
							this.TcpClient.GetStream().Write(ms.GetBuffer(), 0, (int)ms.Length);

							AppendLog(string.Format("Sent {0} bytes", ms.Length));
						}
						else
						{
							AppendLog("Failed to compile data");
						}

						if (!this.requestedMetadataTypes.ContainsKey(metadataTypeHash))
						{
							this.requestedMetadataTypes.Add(metadataTypeHash, new List<uint> { chunkNameHash });
						}
						else if (!this.requestedMetadataTypes[metadataTypeHash].Contains(chunkNameHash))
						{
							this.requestedMetadataTypes[metadataTypeHash].Add(chunkNameHash);
						}

						if (ourSchemaVersion != codeSchemaVersion)
						{
							// Restore latest schema version
							try
							{
								var typeDefinitionPaths = compiler.TypeDefinitionPaths;
								if (typeDefinitionPaths != null)
								{
									foreach (var typeDefPath in typeDefinitionPaths)
									{
										RaveInstance.AssetManager.GetLatest(typeDefPath, false);
									}
								}
							}
							catch (Exception ex)
							{
								ErrorManager.HandleError(ex);
							}
						}


						break;
					}

				case RemoteControlCommandEnums.Setruntimeplatform:
					{
						this.SetRuntimePlatform(payload);

						// game code waits for a receipt
						return true;
					}

				case RemoteControlCommandEnums.Editobject:
					{
						try
						{
							var xmlString = Encoding.ASCII.GetString(payload).Trim();
							return this.HandleXmlData(xmlString);
						}
						catch (Exception e)
						{
							ErrorManager.HandleError(e);
						}

						break;
					}
				case RemoteControlCommandEnums.AudioEngineLoaded:
					{

						if (this.remoteControl.CurrentMuteList != null)
						{
							SetMuteList(this.remoteControl.CurrentMuteList);
						}
						if (this.remoteControl.CurrentSoloList != null)
						{
							SetSoloList(this.remoteControl.CurrentSoloList);
						}

						break;
					}
			}

			// default to no receipt
			return false;
		}

		/// <summary>
		/// The handle xml data.
		/// </summary>
		/// <param name="xmlData">
		/// The xml data.
		/// </param>
		/// <returns>
		/// The <see cref="bool"/>.
		/// </returns>
		private bool HandleXmlData(string xmlData)
		{
			var doc = new XmlDocument();
			try
			{
				doc.LoadXml(xmlData);
			}
			catch (Exception e)
			{
				ErrorManager.HandleError(e);
				return false;
			}

			if (doc.DocumentElement.Name == "RAVEMessage")
			{
				var rootNode = doc.DocumentElement.ChildNodes[0];
				if (rootNode.Name == "EditObjects")
				{
					var metadataType = rootNode.Attributes["metadataType"].Value;

					//Search through all episodes for an object. If an object with the given name exist in more than one episode, 
					//take the one in the default store (DefaultModelPath) if present otherwise display an error message.
					foreach (XmlNode n in rootNode.ChildNodes)
					{
						string name = n.Attributes["name"].Value;
						IObjectInstance objInst = null;

						//see if object is in default store
						if (!string.IsNullOrEmpty(Configuration.DefaultModelPath))
						{
							var bankPath = RaveInstance.AssetManager.GetLocalPath(Configuration.DefaultModelPath);
							var bank = RaveInstance.AllBankManagers[metadataType].FindObjectBank(bankPath);
							if (bank != null && bank.Type.Equals(metadataType))
							{
								objInst = bank.FindObjectInstance(name);
							}
						}

						//if object hasn't been found in default store, search through all episodes to find objects with the given name
						if (objInst == null)
						{
							Dictionary<string, IObjectInstance> objectsPerEpisode =
								new Dictionary<string, IObjectInstance>();

							foreach (
								KeyValuePair<ObjectLookupId, Dictionary<string, IObjectInstance>> episodeObjects in
									RaveInstance.ObjectLookupTables.Where(
										x => x.Key.Type.Equals(metadataType, StringComparison.OrdinalIgnoreCase)))
							{
								if (episodeObjects.Value.ContainsKey(name))
								{
									objectsPerEpisode.Add(episodeObjects.Key.Episode, episodeObjects.Value[name]);
								}
							}


							if (objectsPerEpisode.Count == 1)
							{
								objInst = objectsPerEpisode.First().Value;
							}
							else if (objectsPerEpisode.Count > 1)
							{
								ErrorManager.HandleError("Object with name " + name +
															" found in different episodes, please specify which object to update by setting the default store");
								return false;
							}
						}

						if (objInst != null)
						{
							if (objInst.Bank.IsCheckedOut)
							{
								// Replace local node with remote node
								var originalNode = objInst.Node;
								var importedNode = objInst.Bank.Document.ImportNode(n, true);
								objInst.Bank.Document.DocumentElement.RemoveChild(objInst.Node);
								objInst.Bank.Document.DocumentElement.AppendChild(importedNode);
								objInst.Node = importedNode;

								var typedObjInstance = objInst as ITypedObjectInstance;

								if (null == typedObjInstance)
								{
									throw new Exception("Expected object instance of type ITypedObjectInstance");
								}

								// Update fields in new local node that are marked with 'ignore' flag
								// to preserve their original values
								if (
									!this.UpdateNodeIgnoredFieldsUpdateNode(
										typedObjInstance.TypeDefinition.Fields, objInst.Node, originalNode))
								{
									ErrorManager.HandleError(
										"Failed to update ignored fields in new local node, copied from remote node: "
										+ n.InnerText);
									return false;
								}

								objInst.ComputeReferences(true);
								objInst.MarkAsDirty();

								// object change was succesfully saved
								return true;
							}
						}
						else
						{
							if (string.IsNullOrEmpty(Configuration.DefaultModelPath))
							{
								ErrorManager.HandleError("Please specify a bank for the model");
								return false;
							}

							var bankPath = RaveInstance.AssetManager.GetLocalPath(Configuration.DefaultModelPath);
							var bank = RaveInstance.AllBankManagers[metadataType].FindObjectBank(bankPath);
							if (bank != null && bank.TryToCheckout())
							{
								if (string.Compare(bank.Type, metadataType, true) == 0)
								{
									var importedNode = bank.Document.ImportNode(n, true);
									bank.Document.DocumentElement.AppendChild(importedNode);

									var newObj = new TypedObjectInstance(
										importedNode,
										bank,
										bank.Type,
										bank.Episode,
										RaveInstance.ObjectLookupTables[
											new ObjectLookupId(metadataType, ObjectLookupId.Base)],
										RaveInstance.AllTypeDefinitions[metadataType]);

									newObj.ObjectLookup.Add(newObj.Name, newObj);
									bank.ObjectInstances.Add(newObj);
									newObj.ComputeReferences();
									bank.MarkAsDirty();
									TypeUtils.ReportNewObjectCreated(newObj);
								}
							}
						}
					}
				}
				else if (rootNode.Name == "ViewObject")
				{
					var name = rootNode.Attributes["name"].Value;
					var metadataType = rootNode.Attributes["metadataType"].Value;
					var obj =
						RaveInstance.ObjectLookupTables[new ObjectLookupId(metadataType, ObjectLookupId.Base)][name];
					if (obj != null)
					{
						this.OnViewObject.Raise(obj);
					}

					return true;
				}
				else if (rootNode.Name == "DuplicateObject")
				{
					var name = rootNode.Attributes["oldName"].Value;
					var metadataType = rootNode.Attributes["metadataType"].Value;
					var newName = rootNode.Attributes["newName"].Value;
					IObjectInstance obj =
						RaveInstance.ObjectLookupTables[new ObjectLookupId(metadataType, ObjectLookupId.Base)][name];

					if (obj != null)
					{
						if (obj.IsReadOnly)
						{
							obj.Bank.Checkout(false);
						}

						var newNode = obj.Node.Clone();
						if (newNode.Attributes != null && obj.Bank != null)
						{
							newNode.Attributes["name"].Value = newName;
							IObjectBank bank = obj.Bank;
							XmlNode importedNode = newNode;
							bank.Document.DocumentElement.AppendChild(importedNode);

							TypedObjectInstance newObj = new TypedObjectInstance(
								importedNode,
								bank,
								bank.Type,
								bank.Episode,
								RaveInstance.ObjectLookupTables[
									new ObjectLookupId(metadataType, ObjectLookupId.Base)],
								RaveInstance.AllTypeDefinitions[metadataType]);

							newObj.ObjectLookup.Add(newObj.Name, newObj);
							bank.ObjectInstances.Add(newObj);
							newObj.ComputeReferences();
							bank.MarkAsDirty();
							TypeUtils.ReportNewObjectCreated(newObj);
						}
					}
				}

				return true;
			}


			return false;
		}

		/// <summary>
		/// The has requested data.
		/// </summary>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="chunkNameHash">
		/// The chunk name hash.
		/// </param>
		/// <returns>
		/// Indication of success <see cref="bool"/>.
		/// </returns>
		private bool HasRequestedData(uint metadataTypeHash, uint chunkNameHash)
		{
			if (!this.requestedMetadataTypes.ContainsKey(metadataTypeHash))
			{
				return false;
			}

			return this.requestedMetadataTypes[metadataTypeHash].Contains(chunkNameHash);
		}

		/// <summary>
		/// The receive thread main.
		/// </summary>
		private void ReceiveThreadMain()
		{
			var headerBuffer = new byte[5];

			AppendLog("Received remote connection");

			while (this.TcpClient.Connected)
			{
				try
				{
					if (this.TcpClient.GetStream().DataAvailable)
					{
						if (Monitor.TryEnter(this, 1000))
						{
							try
							{
								// read command header  
								var bytesRead = this.TcpClient.GetStream().Read(headerBuffer, 0, 5);
								if (bytesRead != 5)
								{
									throw new ArgumentException("Failed to read command header from client stream");
								}

								var commandId = headerBuffer[0];
								if (commandId > (byte)RemoteControlCommandEnums.LastCommandId)
								{
									throw new ArgumentException("Invalid command id received from game");
								}

								var messageSize = BitConverter.ToUInt32(headerBuffer, 1);
								if (this.PlatformSettings.IsBigEndian || (RemoteControlCommandEnums)commandId == RemoteControlCommandEnums.Setruntimeplatform)
								{
									messageSize = RaveUtils.SwapUInt32(messageSize);
								}

								if (messageSize > 1024 * 1024)
								{
									throw new ArgumentException("Message received from game too large (>1MB)");
								}

								var messageBuffer = new byte[messageSize];

								// read payload
								bytesRead = 0;
								var numIterations = 0;
								while (bytesRead < messageSize)
								{
									var bytesReadThisTime = this.TcpClient.GetStream()
																.Read(
																	messageBuffer, bytesRead, (int)messageSize - bytesRead);
									if (bytesReadThisTime < 0 || numIterations++ > 1000)
									{
										break;
									}

									bytesRead += bytesReadThisTime;
								}

								if (bytesRead != messageSize)
								{
									throw new ArgumentException("Failed to read command payload");
								}

								var sendReceipt = this.HandleReceivedData(commandId, messageBuffer);
								if (sendReceipt)
								{
									// send receipt
									var buf = this.GetBytes(RemoteControlCommandEnums.Messagereceipt, null);
									try
									{
										this.TcpClient.GetStream().Write(buf, 0, buf.Length);
									}
									catch (Exception ex)
									{
										System.Diagnostics.Debug.WriteLine(ex.ToString());
									}
								}
							}
							finally
							{
								Monitor.Exit(this);
							}

						}
					}
					else
					{
						Thread.Sleep(100);
					}

				}
				catch (SocketException e)
				{
					Thread.Sleep(100);
					this.AppendLog(e.ToString());
					System.Diagnostics.Debug.WriteLine(e.ToString());
				}
				catch (ArgumentException ex)
				{
					System.Diagnostics.Debug.WriteLine(ex.ToString());
					this.AppendLog(ex.ToString());
					// clear out the incoming data
					while (this.TcpClient.GetStream().DataAvailable)
					{
						this.TcpClient.GetStream().ReadByte();
					}
				}
				catch (Exception ex)
				{
					UXTimer.Instance.AppendNewLine(ex.ToString());
					this.AppendLog(ex.ToString());
					System.Diagnostics.Debug.WriteLine(ex.ToString());
				}
			}

			try
			{
				if (this.TcpClient.Connected)
				{
					this.TcpClient.Close();
				}
			}
			catch (Exception ex)
			{
				this.AppendLog(ex.ToString());
				System.Diagnostics.Debug.WriteLine(ex.ToString());
			}

			RaveInstance.RemoteControl.RemoveClient(this);
		}

		/// <summary>
		/// The send.
		/// </summary>
		/// <param name="buffer">
		/// The buffer.
		/// </param>
		private void Send(byte[] buffer)
		{
			try
			{
				if (Monitor.TryEnter(this, 500))
				{
					try
					{
						this.TcpClient.GetStream().Write(buffer, 0, buffer.Length);
					}
					finally
					{
						Monitor.Exit(this);
					}
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
			}
		}

		/// <summary>
		/// The send metadata message.
		/// </summary>
		/// <param name="commandEnum">
		/// The command.
		/// </param>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="chunkNameHash">
		/// The chunk name hash.
		/// </param>
		/// <param name="objectNameHash">
		/// The object name hash.
		/// </param>
		/// <param name="dataSize">
		/// The data size.
		/// </param>
		/// <param name="data">
		/// The data.
		/// </param>
		private void SendMetadataMessage(
			RemoteControlCommandEnums commandEnum,
			uint metadataTypeHash,
			uint chunkNameHash,
			uint objectNameHash,
			int dataSize,
			byte[] data)
		{
			if (!this.TcpClient.Connected || !this.HasRequestedData(metadataTypeHash, chunkNameHash))
			{
				return;
			}

			using (var ms = new MemoryStream())
			{
				using (var writer = new BinaryWriter(ms, this.PlatformSettings.IsBigEndian))
				{
					writer.Write((byte)commandEnum);
					using (new ChunkSizeWriter(writer))
					{
						writer.Write(metadataTypeHash);
						writer.Write(chunkNameHash);
						writer.Write(objectNameHash);
						writer.Write(dataSize);
						writer.Write(data, false);
					}
				}

				this.Send(ms.ToArray());
			}
		}

		private void SendMuteSoloMessage(RemoteControlCommandEnums commandEnum, List<KeyValuePair<uint, uint>> typeNameObjectNameHashes)
		{
			if (this.TcpClient.Connected)
			{
				using (MemoryStream stream = new MemoryStream())
				{
					using (rage.ToolLib.Writer.BinaryWriter writer = new rage.ToolLib.Writer.BinaryWriter(stream, this.PlatformSettings.IsBigEndian))
					{
						writer.Write((byte)commandEnum);
						using (new ChunkSizeWriter(writer))
						{
							foreach (KeyValuePair<uint, uint> pair in typeNameObjectNameHashes)
							{
								writer.Write(pair.Key);
								writer.Write(pair.Value);
							}
						}
					}
					this.Send(stream.ToArray());
				}
			}
		}

		/// <summary>
		/// The send type and object name hash message.
		/// </summary>
		/// <param name="commandEnum">
		/// The command.
		/// </param>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="objectNameHash">
		/// The object name hash.
		/// </param>
		private void SendTypeAndObjectNameHashMessage(
			RemoteControlCommandEnums commandEnum, uint metadataTypeHash, uint objectNameHash)
		{
			if (this.TcpClient.Connected && this.requestedMetadataTypes.ContainsKey(metadataTypeHash))
			{
				using (var ms = new MemoryStream())
				{
					using (var writer = new BinaryWriter(ms, this.PlatformSettings.IsBigEndian))
					{
						writer.Write((byte)commandEnum);
						using (new ChunkSizeWriter(writer))
						{
							writer.Write(metadataTypeHash);
							writer.Write(objectNameHash);
						}
					}

					this.Send(ms.ToArray());
				}
			}
		}

		/// <summary>
		/// The set runtime platform.
		/// </summary>
		/// <param name="buffer">
		/// The buffer.
		/// </param>
		private void SetRuntimePlatform(byte[] buffer)
		{
			// last 4 bytes are client uid
			var platformTag = Encoding.ASCII.GetString(buffer, 0, buffer.Length - 4);
			var clientUid = BitConverter.ToInt32(buffer, buffer.Length - 4);

			// If the uid's don't match up, then we know this must be a fresh boot of the runtime, so we trash the compiler
			if (this.Compiler == null || this.Compiler.RuntimeUniqueId != clientUid)
			{
				this.Compiler = new XmlBankMetaDataCompiler(RaveInstance.RaveLog, null) { RuntimeUniqueId = clientUid };
			}
			AppendLog("Setting remote client platform: " + platformTag);
			this.Compiler.ProjectSettings.SetCurrentPlatformByTag(platformTag);
		}

		/// <summary>
		/// The update node ignored fields helper.
		/// </summary>
		/// <param name="fieldDefs">
		/// The field definitions.
		/// </param>
		/// <param name="localNodes">
		/// The local nodes.
		/// </param>
		/// <param name="originalNodes">
		/// The original nodes.
		/// </param>
		/// <returns>
		/// Indication of success <see cref="bool"/>.
		/// </returns>
		private bool UpdateNodeIgnoredFieldsHelper(
			IFieldDefinition[] fieldDefs, XmlNodeList localNodes, XmlNodeList originalNodes)
		{
			// Preserve original local node value if it has an 'ignore' flag
			foreach (XmlNode originalNode in originalNodes)
			{
				// Find corresponding local node for current remote node
				var matchingNodes =
					localNodes.Cast<XmlNode>().Where(newNode => newNode.Name.Equals(originalNode.Name)).ToList();

				// Failed to find corresponding local node
				if (!matchingNodes.Any())
				{
					continue;
				}

				if (matchingNodes.Count > 1)
				{
					// We can only update the node if we find one node with matching
					// name, otherwise it is not possible to make sure we are updating
					// the correct node.
					continue;
				}

				var localNode = matchingNodes[0];

				// Find corresponding field definition for current node
				var fieldDef = fieldDefs.FirstOrDefault(def => def.Name.Equals(originalNode.Name));

				if (null == fieldDef)
				{
					// Unable to update if field def not found
					continue;
				}

				var compositeFieldDef = fieldDef as ICompositeFieldDefinition;

				if (null != compositeFieldDef)
				{
					// Recursively process any children of current node
					this.UpdateNodeIgnoredFieldsHelper(
						compositeFieldDef.Fields, localNode.ChildNodes, originalNode.ChildNodes);
				}
				else
				{
					// Update local node value if marked as 'ignore' to preserver
					// original value
					if (fieldDef.Ignore)
					{
						localNode.InnerText = originalNode.InnerText;
					}
				}
			}

			return true;
		}

		/// <summary>
		/// The update node ignored fields update node.
		/// </summary>
		/// <param name="fieldDefs">
		/// The field definitions.
		/// </param>
		/// <param name="localNode">
		/// The local node.
		/// </param>
		/// <param name="originalNode">
		/// The original node.
		/// </param>
		/// <returns>
		/// Indication of success <see cref="bool"/>.
		/// </returns>
		private bool UpdateNodeIgnoredFieldsUpdateNode(
			IFieldDefinition[] fieldDefs, XmlNode localNode, XmlNode originalNode)
		{
			// Preserve attributes of the root node
			var localAttrs = localNode.Attributes;
			var originalAttrs = originalNode.Attributes;

			if (null != localAttrs && null != originalAttrs)
			{
				foreach (XmlAttribute originalAttr in originalAttrs)
				{
					// Find and update existing matching attribute
					var found = false;
					foreach (XmlAttribute localAttr in localAttrs)
					{
						if (localAttr.Name.Equals(originalAttr.Name))
						{
							localAttr.Value = originalAttr.Value;
							found = true;
							break;
						}
					}

					// If not found, create new attribute
					if (!found)
					{
						var newAttr = localNode.OwnerDocument.CreateAttribute(originalAttr.Name);
						newAttr.Value = originalAttr.Value;
						localAttrs.Append(newAttr);
					}
				}
			}

			return this.UpdateNodeIgnoredFieldsHelper(fieldDefs, localNode.ChildNodes, originalNode.ChildNodes);
		}
	}
}
