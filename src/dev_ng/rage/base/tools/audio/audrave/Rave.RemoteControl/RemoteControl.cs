// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RemoteControl.cs" company="Rockstar North">
//
// </copyright>
// <summary>
//   The remote control client state.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.RemoteControl
{
	using rage.ToolLib;
	using Rave.Metadata.Infrastructure.Interfaces;
	using Rave.RemoteControl.Infrastructure.Interfaces;
	using Rave.Types.Infrastructure.Interfaces.Objects;
	using Rave.Types.Objects;
	using Rave.Utils;
	using System;
	using System.Collections.Generic;
	using System.Net;
	using System.Net.Sockets;
	using System.Threading;
	using System.Threading.Tasks;

	/// <summary>
	/// Remote control.
	/// </summary>
	public class RemoteControl : IRemoteControl
	{
		/// <summary>
		/// The clients.
		/// </summary>
		private readonly Dictionary<IRemoteControlClientState, string> clients;

		public List<KeyValuePair<uint, uint>> CurrentMuteList { get; private set; }

		public List<KeyValuePair<uint, uint>> CurrentSoloList { get; private set; }

		/// <summary>
		/// The listener thread.
		/// </summary>
		private Thread listenerThread;

		/// <summary>
		/// The should thread be running.
		/// </summary>
		private bool shouldThreadBeRunning;

		/// <summary>
		/// Gets the listeners.
		/// </summary>
		public IList<IRemoteMessageListener> Listeners { get; private set; }

		/// <summary>
		/// Gets the number of clients.
		/// </summary>
		public int NumClients
		{
			get
			{
				return this.clients.Count;
			}
		}

		/// <summary>
		/// The on client connect.
		/// </summary>
		public static event Action OnClientConnect;

		/// <summary>
		/// The on client disconnect.
		/// </summary>
		public static event Action OnClientDisconnect;

		/// <summary>
		/// The on view object.
		/// </summary>
		public event Action<IObjectInstance> OnViewObject;

		/// <summary>
		/// Initializes a new instance of the <see cref="RemoteControl"/> class.
		/// </summary>
		public RemoteControl()
		{
			this.Listeners = new List<IRemoteMessageListener>();
			this.listenerThread = new Thread(this.ListenerThreadMain) { IsBackground = true };
			this.shouldThreadBeRunning = true;
			this.clients = new Dictionary<IRemoteControlClientState, string>();
			ObjectInstance.ObjectModified += this.ObjectInstance_OnObjectModified;
		}

		/// <summary>
		/// The abort.
		/// </summary>
		public void Abort()
		{
			shouldThreadBeRunning = false;
		}

		/// <summary>
		/// Add a remote message listener.
		/// </summary>
		/// <param name="listener">
		/// The listener.
		/// </param>
		public void AddRemoteMessageListener(IRemoteMessageListener listener)
		{
			if (Monitor.TryEnter(this.Listeners, 500))
			{
				try
				{
					if (!this.Listeners.Contains(listener))
					{
						this.Listeners.Add(listener);
					}
				}
				finally
				{
					Monitor.Exit(this.Listeners);
				}
			}
		}

		/// <summary>
		/// The object instance_ on object modified.
		/// </summary>
		/// <param name="modifiedObject">
		/// The modified object.
		/// </param>
		public void ObjectInstance_OnObjectModified(IObjectInstance modifiedObject)
		{
			Task task = new Task(() => OnObjectModified(modifiedObject));
			task.Start();
		}

		private void OnObjectModified(IObjectInstance modifiedObject)
		{
			if (Monitor.TryEnter(this.clients, 500))
			{
				try
				{
					foreach (IRemoteControlClientState state in this.clients.Keys)
					{
						state.OnObjectModified(modifiedObject);
					}
				}
				finally
				{
					Monitor.Exit(this.clients);
				}

			}
		}

		/// <summary>
		/// The play sound.
		/// </summary>
		/// <param name="soundName">
		/// The sound name.
		/// </param>
		public void PlaySound(string soundName)
		{
			if (Monitor.TryEnter(this.clients, 500))
			{
				try
				{
					foreach (IRemoteControlClientState state in this.clients.Keys)
					{
						state.PlaySound(soundName);
					}
				}
				finally
				{
					Monitor.Exit(this.clients);
				}
			}
		}

		/// <summary>
		/// The remove client.
		/// </summary>
		/// <param name="client">
		/// The client.
		/// </param>
		public void RemoveClient(IRemoteControlClientState client)
		{
			if (Monitor.TryEnter(this.clients, 500))
			{
				try
				{
					this.clients.Remove(client);
				}
				finally
				{
					Monitor.Exit(this.clients);
				}
			}

			if (OnClientDisconnect != null)
			{
				OnClientDisconnect();
			}
		}

		/// <summary>
		/// Remove a remote message listener.
		/// </summary>
		/// <param name="listener">
		/// The listener.
		/// </param>
		public void RemoveRemoteMessageListener(IRemoteMessageListener listener)
		{
			if (Monitor.TryEnter(this.Listeners, 500))
			{
				try
				{
					this.Listeners.Remove(listener);
				}
				finally
				{
					Monitor.Exit(this.Listeners);
				}
			}
		}

		public void SendBankPreviewUpdated(string path)
		{
			if (Monitor.TryEnter(this.Listeners, 500))
			{
				try
				{
					foreach (IRemoteControlClientState state in this.clients.Keys)
					{
						state.SendBankPreviewUpdated(path);
					}
				}
				finally
				{
					Monitor.Exit(this.Listeners);
				}
			}
			
		}
		public void SetMuteList(List<KeyValuePair<string, string>> typeNameObjectNameMutedList)
		{
			List<KeyValuePair<uint, uint>> list = new List<KeyValuePair<uint, uint>>();
			foreach (KeyValuePair<string, string> pair in typeNameObjectNameMutedList)
			{
				Hash hash = new Hash
				{
					Value = pair.Key
				};
				Hash hash2 = new Hash
				{
					Value = pair.Value
				};
				list.Add(new KeyValuePair<uint, uint>(hash.Key, hash2.Key));
			}

			CurrentMuteList = list;

			if (Monitor.TryEnter(this.clients, 500))
			{
				try
				{
					foreach (IRemoteControlClientState state in this.clients.Keys)
					{
						state.SetMuteList(list);
					}
				}
				finally
				{
					Monitor.Exit(this.clients);
				}
			}
		}

		public void SetSoloList(List<KeyValuePair<string, string>> typeNameObjectNameMutedList)
		{
			List<KeyValuePair<uint, uint>> list = new List<KeyValuePair<uint, uint>>();
			foreach (KeyValuePair<string, string> pair in typeNameObjectNameMutedList)
			{
				Hash hash = new Hash
				{
					Value = pair.Key
				};
				Hash hash2 = new Hash
				{
					Value = pair.Value
				};
				list.Add(new KeyValuePair<uint, uint>(hash.Key, hash2.Key));
			}

			CurrentSoloList = list;

			if (Monitor.TryEnter(this.clients, 500))
			{
				try
				{
					foreach (IRemoteControlClientState state in this.clients.Keys)
					{
						state.SetSoloList(list);
					}
				}
				finally
				{
					Monitor.Exit(this.clients);
				}
			}
		}

		/// <summary>
		/// The shutdown.
		/// </summary>
		public void Shutdown()
		{
			this.shouldThreadBeRunning = false;
			Thread.Sleep(100);
			this.Abort();
			if (Monitor.TryEnter(this.clients, 500))
			{
				try
				{
					foreach (IRemoteControlClientState state in this.clients.Keys)
					{
						state.Abort();
					}
				}
				finally
				{
					Monitor.Exit(this.clients);
				}
			}
		}

		/// <summary>
		/// The start.
		/// </summary>
		public void Start()
		{
			listenerThread.Start();
		}

		/// <summary>
		/// The start auditioning object.
		/// </summary>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="objectNameHash">
		/// The object name hash.
		/// </param>
		public void StartAuditioningObject(uint metadataTypeHash, uint objectNameHash)
		{
			if (Monitor.TryEnter(this.clients, 500))
			{
				try
				{
					foreach (IRemoteControlClientState state in this.clients.Keys)
					{
						state.StartAuditioningObject(metadataTypeHash, objectNameHash);
					}
				}
				finally
				{
					Monitor.Exit(this.clients);
				}
			}

		}

		/// <summary>
		/// The stop auditioning object.
		/// </summary>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="objectNameHash">
		/// The object name hash.
		/// </param>
		public void StopAuditioningObject(uint metadataTypeHash, uint objectNameHash)
		{
			if (Monitor.TryEnter(this.clients, 500))
			{
				try
				{
					foreach (IRemoteControlClientState state in this.clients.Keys)
					{
						state.StopAuditioningObject(metadataTypeHash, objectNameHash);
					}
				}
				finally
				{
					Monitor.Exit(this.clients);

				}
			}
		}

		/// <summary>
		/// The stop sound.
		/// </summary>
		public void StopSound()
		{
			if (Monitor.TryEnter(this.clients, 500))
			{
				try
				{

					foreach (IRemoteControlClientState state in this.clients.Keys)
					{
						state.StopSound();
					}
				}
				finally
				{
					Monitor.Exit(this.clients);

				}
			}
		}

		/// <summary>
		/// The view object.
		/// </summary>
		/// <param name="metadataTypeHash">
		/// The metadata type hash.
		/// </param>
		/// <param name="objectNameHash">
		/// The object name hash.
		/// </param>
		public void ViewObject(uint metadataTypeHash, uint objectNameHash)
		{
			if (Monitor.TryEnter(this.clients, 500))
			{
				try
				{
					foreach (IRemoteControlClientState state in this.clients.Keys)
					{
						state.ViewObject(metadataTypeHash, objectNameHash);
					}
				}
				finally
				{
					Monitor.Exit(this.clients);
				}
			}

		}

		/// <summary>
		/// The listener thread main.
		/// </summary>
		private void ListenerThreadMain()
		{
			var listener = new TcpListener(IPAddress.Any, 48000);
			try
			{
				listener.Start();
				while (shouldThreadBeRunning)
				{
					try
					{
						if (listener.Pending())
						{
							TcpClient clientSocket = listener.AcceptTcpClient();
							IPEndPoint socket = clientSocket.Client.RemoteEndPoint as IPEndPoint;
							string connectingClientAddress = socket.Address.ToString();
							OnClientConnect.Raise();
							IXmlBankMetaDataCompiler compiler = null;
							IPEndPoint remoteEndPoint = clientSocket.Client.RemoteEndPoint as IPEndPoint;
							IPEndPoint localEndPoint = clientSocket.Client.LocalEndPoint as IPEndPoint;

							if (remoteEndPoint.Address.ToString() != localEndPoint.Address.ToString())
							{
								if (Monitor.TryEnter(this.clients, 500))
								{
									try
									{
										foreach (IRemoteControlClientState state in this.clients.Keys)
										{
											string szEndPoint = this.clients[state];
											if (szEndPoint == connectingClientAddress)
											{
												System.Diagnostics.Debug.WriteLine("Reconnected client");
												compiler = state.Compiler;
												state.Abort();
												this.RemoveClient(state);
												break;
											}
										}
									}
									finally
									{
										Monitor.Exit(this.clients);
									}
								}
							}

							System.Diagnostics.Debug.WriteLine("Client connected!");
							RemoteControlClientState rccs = new RemoteControlClientState(this, clientSocket, compiler);
							rccs.OnViewObject += new Action<IObjectInstance>(this.RccsOnViewObject);
							this.clients.Add(rccs, ((IPEndPoint)rccs.TcpClient.Client.RemoteEndPoint).Address.ToString());
							continue;
						}
						else
						{
							Thread.Sleep(250);
						}
					}
					catch (Exception ex)
					{
						System.Diagnostics.Debug.WriteLine(ex.ToString());
						Thread.Sleep(100);
					}
				}

				listener.Stop();
			}
			catch (ThreadAbortException abortException)
			{
				listener.Stop();
				ErrorManager.HandleError(abortException);
			}
			catch (Exception otherException)
			{
				listener.Stop();
				ErrorManager.HandleError(otherException);
			}
		}

		/// <summary>
		/// The RCCS on view object.
		/// </summary>
		/// <param name="objectInstance">
		/// The object instance.
		/// </param>
		private void RccsOnViewObject(IObjectInstance objectInstance)
		{
			if (this.OnViewObject != null)
			{
				this.OnViewObject(objectInstance);
			}
		}
	}
}