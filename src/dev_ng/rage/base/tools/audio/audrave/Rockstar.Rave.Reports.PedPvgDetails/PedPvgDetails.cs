﻿// -----------------------------------------------------------------------
// <copyright file="PedPvgDetails.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.PedPvgDetails
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;

    using global::Rave.Plugins.Infrastructure.Interfaces;

    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// Report that generates a list of ped details.
    /// </summary>
    public class PedPvgDetails : IRAVEReportPlugin
    {
        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// The game objects depot path.
        /// </summary>
        private string gameObjectsDepotPath;

        /// <summary>
        /// The ped game data path.
        /// </summary>
        private string pedGameDataDepotPath;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private IAssetManager assetManager;

        /// <summary>
        /// The pvgs.
        /// </summary>
        private IDictionary<string, Pvg> pvgs;

        /// <summary>
        /// The race to pvgs.
        /// </summary>
        private IDictionary<string, RaceToPvg> raceToPvgs; 

        /// <summary>
        /// The peds.
        /// </summary>
        private IList<Ped> peds; 

        /// <summary>
        /// The get report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;

                    case "GameObjectsDepotPath":
                        this.gameObjectsDepotPath = setting.InnerText;
                        break;

                    case "PedGameDataDepotPath":
                        this.pedGameDataDepotPath = setting.InnerText;
                        break;
                }
            }

            // Validate args
            if (string.IsNullOrEmpty(this.name))
            {
                throw new Exception("Report name required");
            }

            if (string.IsNullOrEmpty(this.gameObjectsDepotPath))
            {
                throw new Exception("Game object depot path required");
            }

            if (string.IsNullOrEmpty(this.pedGameDataDepotPath))
            {
                throw new Exception("Ped game data depot path required");
            }

            // Get instance of asset manager
            this.assetManager = AssetManagerFactory.GetInstance(AssetManagerType.Perforce);

            if (null == this.assetManager)
            {
                throw new Exception("Failed to get instance of Perforce asset manager");
            }

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes (not required).
        /// </param>
        /// <param name="browsers">
        /// The sound browsers (not required).
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            // Get latest data
            if (!this.GetLatestData())
            {
                throw new Exception("Failed to get latest data required to generate report");
            }

            // Load latest data
            if (!this.LoadPvgData())
            {
                throw new Exception("Failed to load PVG data");
            }

            if (!this.LoadPedGameData())
            {
                throw new Exception("Failed to load ped data");
            }

            this.WriteToCsv();
            this.DisplayResults();
        }

        /// <summary>
        /// Get latest data from asset manager.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GetLatestData()
        {
            // Get latest ped game data file
            if (!this.assetManager.GetLatest(this.pedGameDataDepotPath, false))
            {
                throw new Exception("Failed to get latest ped game data: " + this.pedGameDataDepotPath);
            }

            // Get latest game object data files
            var gameObjectsFolder = this.gameObjectsDepotPath;
            if (!gameObjectsFolder.EndsWith("/"))
            {
                gameObjectsFolder += "/";
            }

            gameObjectsFolder += "...";

            if (!this.assetManager.GetLatest(gameObjectsFolder, false))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Load the PVG data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadPvgData()
        {
            this.pvgs = new Dictionary<string, Pvg>();
            this.raceToPvgs = new Dictionary<string, RaceToPvg>();

            // Get list of game object XML files
            var gameObjectsLocalPath = this.assetManager.GetLocalPath(this.gameObjectsDepotPath);

            var filePaths = Directory.GetFiles(gameObjectsLocalPath, "*.xml", SearchOption.AllDirectories);

            foreach (var filePath in filePaths)
            {
                var file = XDocument.Load(filePath);
                var currObjsElem = file.Element("Objects");

                if (null == currObjsElem)
                {
                    return false;
                }

                // Find and store names of all PVGs
                foreach (var pvgElem in currObjsElem.Descendants("PedVoiceGroups"))
                {
                    // Get PVG name
                    var nameAttr = pvgElem.Attribute("name");
                    if (null == nameAttr)
                    {
                        throw new Exception("<name> attribute required");
                    }

                    var pvg = new Pvg(nameAttr.Value);

                    // Get PVG voices
                    var voiceElems = pvgElem.Descendants("VoiceName");
                    foreach (var voiceElem in voiceElems)
                    {
                        pvg.Voices.Add(voiceElem.Value);
                    }

                    this.pvgs.Add(pvg.Name, pvg);
                }

                // Find and store races against PVGs
                foreach (var pedRaceToPvgElem in currObjsElem.Descendants("PedRaceToPVG"))
                {
                    var nameAttr = pedRaceToPvgElem.Attribute("name");
                    if (null == nameAttr)
                    {
                        throw new Exception("<name> attribute required");
                    }

                    var raceToPvg = new RaceToPvg(nameAttr.Value);

                    foreach (var pvgElem in pedRaceToPvgElem.Elements())
                    {
                        // Can assume that all child elements with no children contain a voice name
                        if (pvgElem.HasElements)
                        {
                            continue;
                        }

                        var race = pvgElem.Name.LocalName;
                        var pvgName = pvgElem.Value;
                        Pvg pvg;
                        this.pvgs.TryGetValue(pvgName, out pvg);
                        raceToPvg.Pvgs.Add(new KeyValuePair<string, Pvg>(race, pvg));
                    }

                    this.raceToPvgs.Add(nameAttr.Value, raceToPvg);
                }
            }

            return true;
        }

        /// <summary>
        /// Load the ped data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadPedGameData()
        {
            this.peds = new List<Ped>();

            var localPath = this.assetManager.GetLocalPath(this.pedGameDataDepotPath);

            if (string.IsNullOrEmpty(localPath))
            {
                throw new Exception("Failed to get local path for game file: " + this.pedGameDataDepotPath);
            }

            // Load the XML file
            var pedDoc = XDocument.Load(localPath);

            var modelInfoElem = pedDoc.Element("CPedModelInfo__InitDataList");
            var dataElem = modelInfoElem.Element("InitDatas");
            var pedElems = dataElem.Elements("Item");

            foreach (var pedElem in pedElems)
            {
                var nameElem = pedElem.Element("Name");
                if (null == nameElem)
                {
                    throw new Exception("<Name> element required");
                }

                var pvgElem = pedElem.Element("PedVoiceGroup");

                if (null == pvgElem)
                {
                    continue;
                }

                Pvg pvg;
                this.pvgs.TryGetValue(pvgElem.Value, out pvg);

                RaceToPvg raceToPvg;
                this.raceToPvgs.TryGetValue(pvgElem.Value, out raceToPvg);

                var ped = new Ped(nameElem.Value, pvgElem.Value, pvg, raceToPvg);
                this.peds.Add(ped);
            }

            return true;
        }

        /// <summary>
        /// Display the results to the user.
        /// </summary>
        private void DisplayResults()
        {
            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filestream = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filestream);

            sw.WriteLine("<html><head><title>Ped PVG Details</title></head><body>");

            var csvPath = Path.Combine(Path.GetTempPath(), this.name + "_report.csv");
            sw.WriteLine("<p><a href=\"" + csvPath + "\" target=\"_blank\">CSV Version (" + csvPath + ")</a></p>");

            sw.WriteLine("<table style=\"border: 1px solid #AAA;\">");
            sw.WriteLine("<tr style=\"background-color:#EEE;\">");
            sw.WriteLine("<th style=\"padding:10px;\">Ped Name</th>");
            sw.WriteLine("<th style=\"padding:10px;\">PVG</th>");
            sw.WriteLine("<th style=\"padding:10px;\">R2PVG</th>");
            sw.WriteLine("<th style=\"padding:10px;\">PVG(s)</th>");
            sw.WriteLine("<th style=\"padding:10px;\">Voices(s)</th>");
            sw.WriteLine("</tr>");

            foreach (var ped in this.peds)
            {
                sw.WriteLine("<tr style=\"font-size: 0.8em;\">");
                sw.WriteLine("<td style=\"padding:10px;\">" + ped.Name + "</td>");
                
                if (null != ped.Pvg)
                {
                    sw.WriteLine("<td style=\"padding:10px;\">" + ped.Pvg.Name + "</td>");
                }
                else
                {
                    sw.WriteLine("<td style=\"padding:10px;\"></td>");
                }

                if (null != ped.RaceToPvg)
                {
                    sw.WriteLine("<td style=\"padding:10px;\">" + ped.RaceToPvg.Name + "</td>");
                }
                else
                {
                    sw.WriteLine("<td style=\"padding:10px;\"></td>");
                }

                if (null != ped.Pvg)
                {
                    sw.WriteLine("<td style=\"padding:10px;\">");
                    sw.WriteLine("<td style=\"padding:10px;\">");
                    this.WriteVoices(sw, ped.Pvg.Voices, "<br />");
                    sw.WriteLine("</td>");
                }
                else if (null != ped.RaceToPvg)
                {
                    var count = 0;
                    foreach (var pvg in ped.RaceToPvg.Pvgs.Values)
                    {
                        if (count > 0)
                        {
                            sw.WriteLine("</tr>");
                            sw.WriteLine("<tr style=\"font-size: 0.8em;\">");
                            sw.WriteLine("<td style=\"padding:10px;\"></td>");
                            sw.WriteLine("<td style=\"padding:10px;\"></td>");
                            sw.WriteLine("<td style=\"padding:10px;\"></td>");
                        }

                        sw.WriteLine("<td style=\"padding:10px;\">" + pvg.Name + "</td>");
                        sw.WriteLine("<td style=\"padding:10px;\">");
                        this.WriteVoices(sw, pvg.Voices, "<br />");
                        sw.WriteLine("</td>");

                        count++;
                    }
                }
                else
                {
                    sw.WriteLine("<td style=\"padding:10px;\">");
                    sw.WriteLine("<td style=\"padding:10px;\">");
                }

                sw.WriteLine("</tr>");
            }

            sw.WriteLine("</table>");

            sw.WriteLine("</body></html>");

            sw.Close();
            filestream.Close();

            // Display report
            var report = new frmReport(this.name + " Report");
            report.SetURL(resultsFileName);
            report.Show();
        }

        /// <summary>
        /// Write the report to CSV.
        /// </summary>
        private void WriteToCsv()
        {
            // Write results to temp file
            var csvFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.csv");
            var filestream = new FileStream(csvFileName, FileMode.Create);
            var sw = new StreamWriter(filestream);

            sw.WriteLine("Ped Name,PVG,P2PVG,PVG,Voice");

            foreach (var ped in this.peds)
            {
                if (null != ped.Pvg)
                {
                    foreach (var voice in ped.Pvg.Voices)
                    {
                        sw.Write(ped.Name);
                        sw.Write(",");
                        sw.Write(ped.Pvg.Name);
                        sw.Write(",");
                        sw.Write(",");
                        sw.Write(ped.Pvg.Name);
                        sw.Write(",");
                        sw.WriteLine(voice);
                    }
                }
                else if (null != ped.RaceToPvg)
                {
                    foreach (var pvg in ped.RaceToPvg.Pvgs.Values)
                    {
                        foreach (var voice in pvg.Voices)
                        {
                            sw.Write(ped.Name);
                            sw.Write(",");
                            sw.Write(",");
                            sw.Write(ped.RaceToPvg.Name);
                            sw.Write(",");
                            sw.Write(pvg.Name);
                            sw.Write(",");
                            sw.WriteLine(voice);
                        }
                    }
                }
            }

            sw.Close();
            filestream.Close();
        }

        /// <summary>
        /// Helper method to write list of voices to stream writer.
        /// </summary>
        /// <param name="sw">
        /// The stream writer.
        /// </param>
        /// <param name="voices">
        /// The voices.
        /// </param>
        /// <param name="separator">
        /// The separator.
        /// </param>
        private void WriteVoices(StreamWriter sw, IList<string> voices, string separator)
        {
            for (var i = 0; i < voices.Count; i++)
            {
                sw.Write(voices[i]);
                if (i < voices.Count - 1)
                {
                    sw.Write(separator);
                }
            }
        }
    }
}
