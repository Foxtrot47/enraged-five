﻿// -----------------------------------------------------------------------
// <copyright file="Ped.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.PedPvgDetails
{
    /// <summary>
    /// Holds Ped details.
    /// </summary>
    public class Ped
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Ped"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="pvgName">
        /// The pvg name.
        /// </param>
        /// <param name="pvg">
        /// The pvg.
        /// </param>
        /// <param name="raceToPvg">
        /// The race to pvg.
        /// </param>
        public Ped(string name, string pvgName, Pvg pvg, RaceToPvg raceToPvg)
        {
            this.Name = name;
            this.PvgName = pvgName;
            this.Pvg = pvg;
            this.RaceToPvg = raceToPvg;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the pvg name.
        /// </summary>
        public string PvgName { get; private set; }

        /// <summary>
        /// Gets the pvg.
        /// </summary>
        public Pvg Pvg { get; private set; }

        /// <summary>
        /// Gets the race to pvg.
        /// </summary>
        public RaceToPvg RaceToPvg { get; private set; }
    }
}
