﻿// -----------------------------------------------------------------------
// <copyright file="RaceToPvg.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.PedPvgDetails
{
    using System.Collections.Generic;

    /// <summary>
    /// Holds RaceToPvg details.
    /// </summary>
    public class RaceToPvg
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RaceToPvg"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public RaceToPvg(string name)
        {
            this.Name = name;
            this.Pvgs = new Dictionary<string, Pvg>();
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the race -> pvgs.
        /// </summary>
        public IDictionary<string, Pvg> Pvgs { get; private set; }
    }
}
