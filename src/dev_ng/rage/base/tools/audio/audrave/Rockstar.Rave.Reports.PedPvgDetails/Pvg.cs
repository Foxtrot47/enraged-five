﻿// -----------------------------------------------------------------------
// <copyright file="Pvg.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.PedPvgDetails
{
    using System.Collections.Generic;

    /// <summary>
    /// Holds PVG details.
    /// </summary>
    public class Pvg
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pvg"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public Pvg(string name)
        {
            this.Name = name;
            this.Voices = new List<string>();
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the voices.
        /// </summary>
        public IList<string> Voices { get; private set; }
    }
}
