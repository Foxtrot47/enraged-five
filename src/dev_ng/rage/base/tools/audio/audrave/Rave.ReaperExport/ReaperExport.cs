﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReaperExport.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The reaper export.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.ReaperExport
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Instance;
    using Rave.Utils.Infrastructure.Interfaces;
    using Rave.Utils.Popups;

    using rage;
    using rage.ToolLib.CmdLine;

    using Rave.AssetManager;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.UserActions;

    using Rockstar.AssetManager.Interfaces;

    using Wavelib;

    /// <summary>
    /// The reaper export plugin.
    /// </summary>
    public class ReaperExport : IRAVEWaveBrowserPlugin
    {
        #region Constants

        /// <summary>
        /// The lipsync extension.
        /// </summary>
        private const string LipsyncExtension = ".lipsync";

        /// <summary>
        /// The xarb.
        /// </summary>
        private const string Xarb = "XARB";

        #endregion

        #region Fields

        /// <summary>
        /// The is x64 flag.
        /// </summary>
        private bool isX64;

        /// <summary>
        /// The name.
        /// </summary>
        private string name;

        /// <summary>
        /// The reaper is executing flag.
        /// </summary>
        private bool reaperIsExecuting;

        /// <summary>
        /// The reaper path.
        /// </summary>
        private string reaperPath;

        /// <summary>
        /// The remove visemes flag.
        /// </summary>
        private bool removeVisemes;

        /// <summary>
        /// The skip custom metadata flag.
        /// </summary>
        private bool skipCustomMetadata;

        /// <summary>
        /// The assets.
        /// </summary>
        private IList<IAsset> assets;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get plugin name.
        /// </summary>
        /// <returns>
        /// The plugin name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        /// Initialize the report speech export plugin.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            foreach (XmlNode child in settings.ChildNodes)
            {
                switch (child.Name)
                {
                    case "Name":
                        {
                            this.name = child.InnerText;
                            break;
                        }

                    case "x86":
                        {
                            if (File.Exists(child.InnerText))
                            {
                                this.reaperPath = child.InnerText;
                            }

                            break;
                        }

                    case "x64":
                        {
                            if (File.Exists(child.InnerText))
                            {
                                this.reaperPath = child.InnerText;
                                this.isX64 = true;
                            }

                            break;
                        }

                    case "SkipCustomMetadata":
                        {
                            this.skipCustomMetadata = bool.Parse(child.InnerText);
                            break;
                        }

                    case "RemoveVisemes":
                        {
                            this.removeVisemes = bool.Parse(child.InnerText);
                            break;
                        }
                }
            }

            return !string.IsNullOrEmpty(this.name) && !string.IsNullOrEmpty(this.reaperPath);
        }

        /// <summary>
        /// Process waves in Reaper.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        public void Process(IWaveBrowser waveBrowser, IActionLog actionLog, ArrayList nodes, IBusy busyFrm)
        {
            this.assets = new List<IAsset>();

            if (this.reaperIsExecuting || System.Diagnostics.Process.GetProcessesByName("reaper").Length != 0)
            {
                ErrorManager.HandleInfo(
                    "Reaper is currently executing - Please close Reaper before using this plugin");
                return;
            }

            // Check for any non-reaper related pending actions
            if (null != RaveInstance.RaveAssetManager.WaveChangeList && !RaveInstance.RaveAssetManager.WaveChangeList.Description.Equals("[Rave] - Reaper Wave Changes"))
            {
                ErrorManager.HandleInfo("Please commit pending non-Reaper related wave changes before using this plugin");
                return;
            }

            try
            {
                var packNames = new HashSet<string>();

                // Process each selected node in the wave browser
                var editorTreeNodes = new List<WaveNode>();
                foreach (EditorTreeNode node in nodes)
                {
                    var pack = EditorTreeNode.FindParentPackNode(node).GetObjectName();
                    packNames.Add(pack);

                    waveBrowser.GetEditorTreeView().GetSelectedWaveNodes(
                        node, this.skipCustomMetadata, editorTreeNodes);
                }

                if (editorTreeNodes.Count == 0)
                {
                    return;
                }

                // Get any pending wave files associated with bank(s) we're using
                var projectSettings = Configuration.ProjectSettings;
                var pendingWaveFiles = new HashSet<string>();

                foreach (PlatformSetting platform in projectSettings.GetPlatformSettings())
                {
                    foreach (var packName in packNames)
                    {
                        var packFileName = packName + ".XML";
                        var pendingWavesPath = Path.Combine(platform.BuildInfo, "PendingWaves");
                        var pendingWaveFile = Path.Combine(pendingWavesPath, packFileName);
                        var depotPath = RaveInstance.AssetManager.GetDepotPath(pendingWaveFile);

                        pendingWaveFiles.Add(depotPath);
                    }
                }

                // Check that there aren't any pending wave lists in a pending change list
                foreach (var pendingWaveFile in pendingWaveFiles)
                {
                    if (RaveInstance.AssetManager.ExistsInPendingChangeList(pendingWaveFile))
                    {
                        var fileName = Path.GetFileNameWithoutExtension(pendingWaveFile);
                        ErrorManager.HandleInfo("Pending wave list for " + fileName + " pack must be checked in/reverted before using this plugin");
                        return;
                    }
                }

                var checkedOutWaves = new List<string>();
                var checkedOutWavesWithLipsyncData = new List<string>();
                var selectedWaveNodes = editorTreeNodes.Cast<WaveNode>().ToList();

                if (!this.CheckOutWaves(busyFrm, selectedWaveNodes, checkedOutWaves, checkedOutWavesWithLipsyncData)
                    || checkedOutWaves.Count == 0)
                {
                    return;
                }

                var raveReaperWavePath = Path.GetTempPath() + "RaveReaperSourceWaves\\";
                if (Directory.Exists(raveReaperWavePath))
                {
                    Directory.Delete(raveReaperWavePath, true);
                }

                var reaperProject = new ReaperProject(checkedOutWaves);
                foreach (var checkedOutWave in checkedOutWaves)
                {
                    var destination = checkedOutWave.Replace(reaperProject.RenderPath, raveReaperWavePath);
                    var destinationDir = Path.GetDirectoryName(destination);
                    if (!Directory.Exists(destinationDir))
                    {
                        Directory.CreateDirectory(destinationDir);
                    }

                    File.Copy(checkedOutWave, destination);
                }

                var reaperProjectFileName = reaperProject.Generate(this.isX64, raveReaperWavePath);
                if (string.IsNullOrEmpty(reaperProjectFileName))
                {
                    ErrorManager.HandleInfo("Failed to generate Reaper project file...");
                    this.RevertWaves(busyFrm);
                    return;
                }

                this.ExecuteReaper(busyFrm, reaperProjectFileName);

                if (
                    ErrorManager.HandleQuestion(
                        "Reaper has finished executing. Would you like to keep your pending changes? "
                        + "These must be submitted manually in RAVE. Select 'No' to revert your changes.",
                        MessageBoxButtons.YesNo,
                        DialogResult.Yes) == DialogResult.Yes)
                {
                    this.AddPendingChanges(waveBrowser, busyFrm, checkedOutWaves, checkedOutWavesWithLipsyncData);
                }
                else
                {
                    this.RevertWaves(busyFrm);
                }
            }
            finally
            {
                busyFrm.Close();
            }
        }

        /// <summary>
        /// Override ToString to return plugin name.
        /// </summary>
        /// <returns>
        /// The plugin name <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.GetName();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add pending changes.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        /// <param name="checkedOutWaves">
        /// The checked out waves.
        /// </param>
        /// <param name="checkOutWavesWithLipsyncData">
        /// The check out waves with lipsync data.
        /// </param>
        private void AddPendingChanges(
            IWaveBrowser waveBrowser,
            IBusy busyFrm,
            IEnumerable<string> checkedOutWaves,
            ICollection<string> checkOutWavesWithLipsyncData)
        {
            busyFrm.SetUnknownDuration(true);
            busyFrm.SetStatusText("Adding pending wave changes...");

            var root = Configuration.PlatformWavePath.ToUpper();
            try
            {
                foreach (var wavePath in checkedOutWaves)
                {
                    if (checkOutWavesWithLipsyncData.Contains(wavePath))
                    {
                        // restore XARB chunk
                        byte[] lipsyncData;
                        using (
                            var lipsyncReader =
                                new BinaryReader(new FileStream(wavePath + LipsyncExtension, FileMode.Open)))
                        {
                            lipsyncData = lipsyncReader.ReadBytes((int)lipsyncReader.BaseStream.Length);
                        }

                        var waveFile = new bwWaveFile(wavePath);
                        if (waveFile.Format.SignificantBitsPerSample != 16)
                        {
                            throw new Exception(string.Format("Wave file {0} is not a 16Bit Wavefile", waveFile.FileName));
                        }
                        var lipsyncChunk = new bwRaveChunk(Xarb, lipsyncData);
                        waveFile.AddRaveChunk(lipsyncChunk);
                        waveFile.Save();
                    }

                    var pendingWaveLists = waveBrowser.GetPendingWaveLists();
                    var path = wavePath.ToUpper().Replace(root, string.Empty);

                    foreach (PlatformSetting ps in Configuration.ActivePlatformSettings)
                    {
                        // Mark waves as modified
                        pendingWaveLists.GetPendingWaveList(ps.PlatformTag).RecordModifyWave(path);
                        Application.DoEvents();
                    }

                    var fileName = Path.GetFileNameWithoutExtension(wavePath);

                    // Add pending edit wave action
                    var parameters = new ActionParams(
                        waveBrowser, null, null, wavePath, null, null, fileName, null, null, null);
                    IUserAction editWaveAction = waveBrowser.CreateAction(ActionType.EditWave, parameters);
                    if (editWaveAction.Action())
                    {
                        waveBrowser.GetActionLog().AddAction(editWaveAction);
                    }
                }
            }
            catch (Exception e)
            {
                ErrorManager.HandleError("Failed to add pending changes: " + e.Message);
                this.RevertWaves(busyFrm);
            }
        }

        /// <summary>
        /// Revert the wave files.
        /// </summary>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        private void RevertWaves(IBusy busyFrm)
        {
            busyFrm.SetStatusText("Reverting pending wave changes...");
            busyFrm.SetUnknownDuration(true);
            busyFrm.Show();
            Application.DoEvents();

            try
            {
                foreach (var asset in this.assets)
                {
                    asset.Revert();
                }

                if (null != RaveInstance.RaveAssetManager.WaveChangeList && !RaveInstance.RaveAssetManager.WaveChangeList.Assets.Any())
                {
                    RaveInstance.RaveAssetManager.WaveChangeList.Delete();
                    RaveInstance.RaveAssetManager.WaveChangeList = null;
                }
            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
            }

            busyFrm.Hide();
        }

        /// <summary>
        /// Check-out the waves.
        /// </summary>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        /// <param name="selectedWaveNodes">
        /// The selected wave nodes.
        /// </param>
        /// <param name="checkedOutWaves">
        /// The checked out waves.
        /// </param>
        /// <param name="checkedOutWavesWithLipsyncData">
        /// The checked out waves with lipsync data.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool CheckOutWaves(
            IBusy busyFrm,
            IList<WaveNode> selectedWaveNodes,
            ICollection<string> checkedOutWaves,
            ICollection<string> checkedOutWavesWithLipsyncData)
        {
            if (RaveInstance.RaveAssetManager.WaveChangeList == null)
            {
                try
                {
                    RaveInstance.RaveAssetManager.WaveChangeList =
                        RaveInstance.AssetManager.CreateChangeList("[Rave] - Reaper Wave Changes");
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                    return false;
                }
            }

            busyFrm.SetProgress(0);
            busyFrm.SetStatusText("Checking out Waves");
            busyFrm.Show();

            var errors = new StringBuilder();

            for (var i = 0; i < selectedWaveNodes.Count; ++i)
            {
                var waveNode = selectedWaveNodes[i];
                var waveObjectPath = waveNode.GetObjectPath();
                var waveAssetPath =
                    (Configuration.PlatformWavePath + waveObjectPath).Replace(
                        Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar).ToUpper();

                busyFrm.SetProgress((int)((i / (float)selectedWaveNodes.Count) * 100.0f));
                busyFrm.SetStatusText("Checking out " + waveObjectPath);

                var asset = RaveInstance.AssetManager.GetCheckedOutAsset(waveAssetPath);
                if (null != asset)
                {
                    if (asset.ChangeList != RaveInstance.RaveAssetManager.WaveChangeList)
                    {
                        ErrorManager.HandleInfo("Asset is already checked out in another change list; unable to edit: " + waveAssetPath);
                        return false;
                    }
                }

                try
                {
                    asset = RaveInstance.RaveAssetManager.WaveChangeList.CheckoutAsset(waveAssetPath, true);

                    // strip off XARB etc chunks since sound forge etc don't like them
                    var waveFile = new bwWaveFile(waveAssetPath, false);

                    if (!this.removeVisemes)
                    {
                        var xarbChunk = waveFile.GetChunk(Xarb);
                        if (xarbChunk != null)
                        {
                            var lipsyncChunk = xarbChunk as bwRaveChunk;
                            if (lipsyncChunk != null)
                            {
                                using (
                                    var lipsyncWriter =
                                        new BinaryWriter(
                                            new FileStream(waveAssetPath + LipsyncExtension, FileMode.CreateNew)))
                                {
                                    checkedOutWavesWithLipsyncData.Add(waveAssetPath);
                                    lipsyncWriter.Write(lipsyncChunk.Bytes);
                                    lipsyncWriter.Flush();
                                }
                            }
                        }
                    }

                    waveFile.RemoveRaveChunks();
                    waveFile.Save();

                    this.assets.Add(asset);
                    checkedOutWaves.Add(waveAssetPath);
                }
                catch (Exception e)
                {
                    errors.AppendLine(e.Message);
                }

                Application.DoEvents();
            }

            busyFrm.Hide();

            if (errors.Length != 0)
            {
                if (
                    ErrorManager.HandleQuestion(
                        string.Format(
                            "Rave Encountered the following errors during check out. Do you wish to continue?{0}{0}{1}",
                            Environment.NewLine,
                            errors),
                        MessageBoxButtons.YesNo,
                        DialogResult.No) == DialogResult.No)
                {
                    this.RevertWaves(busyFrm);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Execute the Reaper application.
        /// </summary>
        /// <param name="busyFrm">
        /// The busy form.
        /// </param>
        /// <param name="projectFileName">
        /// The project file name.
        /// </param>
        private void ExecuteReaper(IBusy busyFrm, string projectFileName)
        {
            busyFrm.SetUnknownDuration(true);
            busyFrm.SetStatusText("Waiting for Reaper to terminate...");
            busyFrm.Show();

            var commandExecutor = new NoOutputAsyncCommandExecutor();
            commandExecutor.Finished += this.OnReaperExecutionFinished;

            this.reaperIsExecuting = true;
            commandExecutor.Execute(this.reaperPath, string.Format("{0} -close:nosave", projectFileName));

            while (this.reaperIsExecuting)
            {
                Application.DoEvents();
                Thread.Sleep(20);
            }

            commandExecutor.Finished -= this.OnReaperExecutionFinished;
            busyFrm.Hide();
        }

        /// <summary>
        /// Change status of Reaper execution process to indicate it has finished.
        /// </summary>
        /// <param name="success">
        /// The success flag.
        /// </param>
        private void OnReaperExecutionFinished(bool success)
        {
            this.reaperIsExecuting = false;
        }

        #endregion
    }
}