﻿// -----------------------------------------------------------------------
// <copyright file="UnreferencedModSynths.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.UnreferencedModSynths
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.Instance;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;
    using global::Rave.Plugins.Infrastructure.Interfaces;
    using global::Rave.Types.Infrastructure.Interfaces.Objects;
    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    /// Report that generates a list of ModularSynth objects that
    /// have the ExportForGame flag set, but aren't referenced by
    /// a ModularSynthSound.
    /// </summary>
    public class UnreferencedModSynths : IRAVEReportPlugin
    {
        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// Get the report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;
                }
            }

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes (not required).
        /// </param>
        /// <param name="browsers">
        /// The sound browsers (not required).
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            var invalidObjs = new List<IObjectInstance>();

            foreach (var lookupTable in RaveInstance.ObjectLookupTables.Where(lookupTable => lookupTable.Key.Type.ToLower().Equals("modularsynth")))
            {
                foreach (var objInstance in lookupTable.Value.Values)
                {
                    foreach (XmlNode node in objInstance.Node.ChildNodes)
                    {
                        if (node.Name.ToLower().Equals("exportforgame"))
                        {
                            if (node.InnerText.ToLower().Equals("yes"))
                            {
                                var valid = objInstance.Referencers.OfType<IObjectInstance>().Any(
									refObjInstance => refObjInstance.Type.ToLower().Contains("sound") || refObjInstance.Type.ToUpper().Contains("GAMEOBJECTS"));

                                if (!valid)
                                {
                                    invalidObjs.Add(objInstance);
                                }
                            }
                        }
                    }
                }
            }

            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Invalid ModularSynth Objects</title></head><body>");
            sw.WriteLine("<h2>Invalid ModularSynth Objects</h2>");
            sw.WriteLine("<p>Description: ModularSynth objects that have the ExportForGame flag set, but aren't referenced by a ModularSynthSound.");

            if (!invalidObjs.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<ul>");

                foreach (var objInstance in invalidObjs)
                {
                    sw.WriteLine("<li>" + objInstance.Bank.FilePath + "/" + objInstance.Name + "</li>");
                }

                sw.WriteLine("</ul>");

                sw.WriteLine("</body></html>");

                sw.Close();
                filesteam.Close();

                // Display report
                var report = new frmReport(this.name + " Report");
                report.SetURL(resultsFileName);
                report.Show();
            }
        }
    }
}
