<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">   

<xsl:template match="@*|node()">
   <xsl:copy>
     <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 </xsl:template>

<xsl:variable name="numVariations" select="count(//Variation)"/>

<xsl:template match="//HistorySpace">
<!-- check that history space is no bigger than numVariations - 1 -->
	<xsl:choose>
		<xsl:when test=". &gt; $numVariations - 1">
		<!-- test for case where numVariations is 0 - dont want history space to go below 0 -->
			<xsl:choose>
				<xsl:when test="$numVariations = 0">
					<HistorySpace>0</HistorySpace>
				</xsl:when>
				<xsl:otherwise>
					<Warning><xsl:value-of select="//@name"/>: Randomized sound with history space greater or equal to number of variations</Warning>
					<HistorySpace><xsl:value-of select="$numVariations - 1"/></HistorySpace>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<HistorySpace><xsl:value-of select="."/></HistorySpace>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>