<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">   

<xsl:template match="@*|node()">
   <xsl:copy>
     <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 </xsl:template>

<xsl:template match="//preDelayVariance">
<!-- check that preDelayVariance is no bigger than preDelay -->
	<xsl:choose>
		<xsl:when test=". &gt; //preDelay">
			<Warning><xsl:value-of select="//@name"/>: preDelayVariance is greater than preDelay - clamping</Warning>
			<preDelayVariance><xsl:value-of select="//preDelay"/></preDelayVariance>
		</xsl:when>
		<xsl:otherwise>
			<preDelayVariance><xsl:value-of select="."/></preDelayVariance>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="//StartOffset">
<!-- check that StartOffset is [0,100] if in percentage -->
	<xsl:choose>
		<xsl:when test="//StartOffsetPercentage = 'yes'">
			<xsl:choose>
					<xsl:when test=". &gt; 100">
						<Warning><xsl:value-of select="//@name"/>: StartOffset in percent outwith valid range - ignoring</Warning>
						<StartOffset>0</StartOffset>
					</xsl:when>
					<xsl:otherwise>
						<StartOffset><xsl:value-of select="."/></StartOffset>
					</xsl:otherwise>
				</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<StartOffset><xsl:value-of select="."/></StartOffset>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="//StartOffsetVariance">
<!-- check that startOffsetVariance is no bigger than startOffset -->
	<xsl:choose>
		<xsl:when test=". &gt; //StartOffset">
			<Warning><xsl:value-of select="//@name"/>: StartOffsetVariance is greater than StartOffset - clamping</Warning>
			<StartOffsetVariance><xsl:value-of select="//StartOffset"/></StartOffsetVariance>
		</xsl:when>
		<xsl:otherwise>
			<StartOffsetVariance><xsl:value-of select="."/></StartOffsetVariance>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>