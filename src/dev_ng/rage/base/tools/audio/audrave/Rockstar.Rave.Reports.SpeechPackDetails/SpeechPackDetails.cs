﻿// -----------------------------------------------------------------------
// <copyright file="SpeechPackDetails.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.SpeechPackDetails
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Xml;
    using System.Xml.Linq;

    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;

    using global::Rave.Plugins.Infrastructure.Interfaces;

    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// Speech pack details report.
    /// </summary>
    public class SpeechPackDetails : IRAVEReportPlugin
    {
        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// The built waves depot path.
        /// </summary>
        private string builtWavesDepotPath;

        /// <summary>
        /// The built waves local path.
        /// </summary>
        private string builtWavesLocalPath;

        /// <summary>
        /// The built waves pack list file name.
        /// </summary>
        private string builtWavesPackListFileName;

        /// <summary>
        /// The built waves pack file names.
        /// </summary>
        private HashSet<string> builtWavesPackFileNames;

        /// <summary>
        /// The pack filters.
        /// </summary>
        private IList<string> packFilters; 

        /// <summary>
        /// The asset manager.
        /// </summary>
        private IAssetManager assetManager;

        /// <summary>
        /// The bank info entries.
        /// </summary>
        private IList<BankInfo> BankInfoEntries; 

        /// <summary>
        /// Get the report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            this.packFilters = new List<string>();

            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;

                    case "BuiltWaves":
                        foreach (XmlNode childNode in setting.ChildNodes)
                        {
                            switch (childNode.Name)
                            {
                                case "DepotPath":
                                    this.builtWavesDepotPath = childNode.InnerText;
                                    break;

                                case "PackListFileName":
                                    this.builtWavesPackListFileName = childNode.InnerText;
                                    break;
                            }
                        }

                        break;

                    case "PacksToInclude":
                        foreach (XmlNode childNode in setting.ChildNodes)
                        {
                            switch (childNode.Name)
                            {
                                case "PackFilter":
                                    this.packFilters.Add(childNode.InnerText);
                                    break;
                            }
                        }

                        break;
                }
            }

            if (string.IsNullOrEmpty(this.name))
            {
                throw new Exception("Report name required");
            }

            if (string.IsNullOrEmpty(this.builtWavesDepotPath))
            {
                throw new Exception("Built waves depot path required");
            }

            // Get instance of asset manager
            this.assetManager = AssetManagerFactory.GetInstance(AssetManagerType.Perforce);

            if (null == this.assetManager)
            {
                throw new Exception("Failed to get instance of Perforce asset manager");
            }

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes (not required).
        /// </param>
        /// <param name="browsers">
        /// The sound browsers (not required).
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            // Get latest data
            if (!this.GetLatestData())
            {
                throw new Exception("Failed to get latest data required to generate report");
            }

            this.LoadPackListNames();
            this.LoadData();
            this.WriteResults();
        }

        /// <summary>
        /// Get latest data from asset manager.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GetLatestData()
        {
            // Get latest built waves data files
            var builtWaveFolder = this.builtWavesDepotPath;
            if (!builtWaveFolder.EndsWith("/"))
            {
                builtWaveFolder += "/";
            }

            builtWaveFolder += "...";

            if (!this.assetManager.GetLatest(builtWaveFolder, false))
            {
                return false;
            }

            this.builtWavesLocalPath = this.assetManager.GetLocalPath(this.builtWavesDepotPath);

            return true;
        }

        /// <summary>
        /// Load the pack list names from the pack list file.
        /// </summary>
        private void LoadPackListNames()
        {
            this.builtWavesPackFileNames = new HashSet<string>();

            // Obtain pack list file path
            var packListPath = Path.Combine(this.builtWavesLocalPath, this.builtWavesPackListFileName);

            if (!File.Exists(packListPath))
            {
                throw new Exception("Failed to find built waves pack list file: " + packListPath);
            }

            // Load pack list file
            var file = XDocument.Load(packListPath);
            foreach (var packFileElem in file.Descendants("PackFile"))
            {
                this.builtWavesPackFileNames.Add(packFileElem.Value);
            }
        }

        /// <summary>
        /// Load the data.
        /// </summary>
        private void LoadData()
        {
            this.BankInfoEntries = new List<BankInfo>();

            foreach (var fileName in this.builtWavesPackFileNames)
            {
                if (!this.PackRequired(fileName))
                {
                    continue;
                }

                var filePath = Path.Combine(this.builtWavesLocalPath, fileName);
                var file = XDocument.Load(filePath);

                var packName = file.Descendants("Pack").First().Attribute("name").Value;

                // Locate bank elements
                foreach (var bankElem in file.Descendants("Bank"))
                {
                    var bankName = bankElem.Attribute("name").Value;
                    var headerSizeAttr = bankElem.Attribute("headerSize");
                    var headerSize = null != headerSizeAttr ? headerSizeAttr.Value : string.Empty;

                    var bankInfo = new BankInfo { PackName = packName, BankName = bankName, HeaderSize = headerSize };
                    this.BankInfoEntries.Add(bankInfo);
                }
            }
        }

        /// <summary>
        /// Write the results to file.
        /// </summary>
        private void WriteResults()
        {
            var entries = this.BankInfoEntries.OrderBy(entry => entry.PackName).ThenBy(entry => entry.BankName);
            var outputPath = Path.Combine(Path.GetTempPath(), "SpeechPackDetails.csv");
            using (var writer = new StreamWriter(outputPath))
            {
                writer.WriteLine("Pack Name,Bank Name,Header Size");

                foreach (var bankInfo in entries)
                {
                    writer.WriteLine("{0},{1},{2}", bankInfo.PackName, bankInfo.BankName, bankInfo.HeaderSize);
                }
            }

            MessageBox.Show(
                "Speech pack details saved to " + outputPath,
                "Success",
                MessageBoxButton.OK,
                MessageBoxImage.Information);
        }

        /// <summary>
        /// Determine if pack is required based on the pack filters.
        /// </summary>
        /// <param name="packFileName">
        /// The pack file name.
        /// </param>
        /// <returns>
        /// Value indicating if pack is required <see cref="bool"/>.
        /// </returns>
        private bool PackRequired(string packFileName)
        {
            return this.packFilters.Any(packFilter => Regex.IsMatch(packFileName, packFilter, RegexOptions.IgnoreCase));
        }

        /// <summary>
        /// The bank info struct.
        /// </summary>
        public struct BankInfo
        {
            /// <summary>
            /// Gets or sets the pack name.
            /// </summary>
            public string PackName { get; set; }

            /// <summary>
            /// Gets or sets the bank name.
            /// </summary>
            public string BankName { get; set; }

            /// <summary>
            /// Gets or sets the header size.
            /// </summary>
            public string HeaderSize { get; set; }
        }
    }
}
