namespace Rave.Plugins.Infrastructure.Interfaces
{
    using System;

    using Rave.Types.Infrastructure.Interfaces.Objects;

    public enum Mode
    {
        DEFAULT,
        TEMPLATE,
        MULTIEDIT
    } ;

    public interface IRAVEObjectEditorPlugin : IRAVEPlugin
    {
        string ObjectType { get; }

        event Action<IObjectInstance> OnObjectEditClick;

        event Action<IObjectInstance> OnObjectRefClick;

        event Action<string, string> OnWaveRefClick;

        event Action<string> OnWaveBankRefClick;

        void EditObject(IObjectInstance objectInstance, Mode mode);

        void Dispose();
    }
}