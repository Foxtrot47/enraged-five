namespace Rave.Plugins.Infrastructure.Interfaces
{
    using System.Xml;

    public interface IRAVEPlugin
    {
        string GetName();

        bool Init(XmlNode settings);
    }
}