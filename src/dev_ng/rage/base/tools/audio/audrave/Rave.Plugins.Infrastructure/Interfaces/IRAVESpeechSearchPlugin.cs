namespace Rave.Plugins.Infrastructure.Interfaces
{
    using System.Collections;

    using Rave.Utils.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Interfaces;

    public interface IRAVESpeechSearchPlugin : IRAVEPlugin
    {
        void Process(IWaveBrowser waveBrowser, IActionLog actionLog, ArrayList nodes, IBusy busyFrm);
    }
}
