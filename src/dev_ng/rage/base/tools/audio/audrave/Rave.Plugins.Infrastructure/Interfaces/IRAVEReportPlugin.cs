namespace Rave.Plugins.Infrastructure.Interfaces
{
    using System.Collections.Generic;

    using Rave.ObjectBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Interfaces;

    public interface IRAVEReportPlugin:IRAVEPlugin
    {
       void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string,IObjectBrowser> browsers);
    }
}
