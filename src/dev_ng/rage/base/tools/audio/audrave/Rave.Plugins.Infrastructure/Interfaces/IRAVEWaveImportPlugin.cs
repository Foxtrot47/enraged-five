namespace Rave.Plugins.Infrastructure.Interfaces
{
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    public interface IRAVEWaveImportPlugin : IRAVEPlugin
    {
        void HandleDroppedFiles(
            IWaveBrowser waveBrowser,
            EditorTreeNode selectedNode,
            IActionLog actionLog,
            string[] files);
    }
}