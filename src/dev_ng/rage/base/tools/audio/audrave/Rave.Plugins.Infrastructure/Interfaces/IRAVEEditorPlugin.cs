namespace Rave.Plugins.Infrastructure.Interfaces
{
    using System.Windows.Forms;

    public interface IRAVEEditorPlugin: IRAVEPlugin
    {
       
        void EditNodes(TreeView m_TreeView);

    }
}
