﻿// -----------------------------------------------------------------------
// <copyright file="IPluginManager.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.Plugins.Infrastructure.Interfaces
{
    using System.Collections.Generic;

    public interface IPluginManager
    {
        IList<IRAVEWaveImportPlugin> WaveImportPlugins { get; }

        IList<IRAVEWaveBrowserPlugin> WaveBrowserPlugins { get; }

        IList<IRAVESpeechSearchPlugin> SpeechSearchPlugins { get; }

        IList<IRAVEReportPlugin> ReportPlugins { get; }

        IList<IRAVEEditorPlugin> EditorPlugins { get; }

        IList<IRAVEObjectBrowserPlugin> ObjectBrowserPlugins { get; }

        IRAVEObjectEditorPlugin GetObjectEditorPlugin(string objectType);
    }
}
