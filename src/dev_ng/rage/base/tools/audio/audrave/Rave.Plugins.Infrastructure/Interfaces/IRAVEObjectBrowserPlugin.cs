namespace Rave.Plugins.Infrastructure.Interfaces
{
    using System.Collections;

    public interface IRAVEObjectBrowserPlugin : IRAVEPlugin
    {
        void Process(ArrayList nodes);
    }

}
