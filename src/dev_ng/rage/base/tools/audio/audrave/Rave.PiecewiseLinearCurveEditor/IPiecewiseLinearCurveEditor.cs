using System;
using System.ComponentModel;
using System.Windows;

namespace Rave.PiecewiseLinearCurveEditor
{
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public interface IPiecewiseLinearCurveEditor : IDisposable,
                                                   INotifyPropertyChanged
    {
        double MaxX { get; }

        double MaxY { get; }

        UIElement Control { get; }

        void EditObject(IObjectInstance instance);

        bool SetXAxisLength(string lengthText);

        bool SetYAxisLength(string lengthText);

        void SetSize(double size);
    }
}