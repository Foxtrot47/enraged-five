namespace Rave.PiecewiseLinearCurveEditor
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Xml;

    using rage.ToolLib;

    using Rave.CurveEditor.Curves;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public class PiecewiseLinearCurveSerializer : IPiecewiseLinearCurveSerializer
    {
        private bool m_isLoading;

        #region IPiecewiseLinearCurveSerializer Members

        public IList<Point> Load(IObjectInstance instance, out double max)
        {
            max = 1;

            var points = new List<Point>();
            if (instance != null)
            {
                try
                {
                    m_isLoading = true;

                    var pointNodes = GetNodes(instance, ParseTokens.Point);
                    foreach (var pointNode in pointNodes)
                    {
                        var point = DeserializePoint(pointNode);
                        if (point != null)
                        {
                            points.Add(point.Value);
                        }
                    }

                    max = GetMax(GetNode(instance, ParseTokens.MinInput), max);
                    max = GetMax(GetNode(instance, ParseTokens.MaxInput), max);
                }
                finally
                {
                    m_isLoading = false;
                }
            }
            return points;
        }

        public void Save(IObjectInstance instance, IList<ControlPoint> points)
        {
            if (!m_isLoading && instance != null &&
                instance.Node != null)
            {
                var pointNodes = GetNodes(instance, ParseTokens.Point);
                foreach (var node in pointNodes)
                {
                    instance.Node.RemoveChild(node);
                }

                if (points != null)
                {
                    foreach (var point in points)
                    {
                        var node = SerializePoint(point, instance.Node.OwnerDocument);
                        instance.Node.AppendChild(node);
                    }
                }
                instance.MarkAsDirty();
            }
        }

        #endregion

        private static IEnumerable<XmlNode> GetNodes(IObjectInstance instance, ParseTokens nodeName)
        {
            var nodes = new List<XmlNode>();
            foreach (XmlNode childNode in instance.Node.ChildNodes)
            {
                try
                {
                    var token = Enum<ParseTokens>.TryParse(childNode.Name, true, ParseTokens.Invalid);
                    if (token == nodeName)
                    {
                        nodes.Add(childNode);
                    }
                }
                catch
                {
                }
            }
            return nodes;
        }

        private static XmlNode GetNode(IObjectInstance instance, ParseTokens nodeName)
        {
            foreach (XmlNode childNode in instance.Node.ChildNodes)
            {
                try
                {
                    var token = Enum<ParseTokens>.TryParse(childNode.Name, true, ParseTokens.Invalid);
                    if (token == nodeName)
                    {
                        return childNode;
                    }
                }
                catch
                {
                }
            }
            return null;
        }

        #region Deserialization

        private static Point? DeserializePoint(XmlNode pointNode)
        {
            Point? point = null;
            XmlNode xNode, yNode;

            ParseXAndYNodes(pointNode, out xNode, out yNode);
            if (xNode != null &&
                yNode != null)
            {
                double x, y;
                if (double.TryParse(xNode.InnerText, out x) &&
                    double.TryParse(yNode.InnerText, out y))
                {
                    point = new Point(x, y);
                }
            }
            return point;
        }

        private static void ParseXAndYNodes(XmlNode pointNode, out XmlNode xNode, out XmlNode yNode)
        {
            xNode = null;
            yNode = null;

            foreach (XmlNode childNode in pointNode.ChildNodes)
            {
                try
                {
                    switch (Enum<ParseTokens>.Parse(childNode.Name))
                    {
                        case ParseTokens.x:
                            {
                                xNode = childNode;
                                break;
                            }
                        case ParseTokens.y:
                            {
                                yNode = childNode;
                                break;
                            }
                    }
                }
                catch
                {
                    break;
                }
            }
        }

        private static double GetMax(XmlNode node, double max)
        {
            double temp;
            if (node != null &&
                double.TryParse(node.InnerText, out temp))
            {
                temp = Math.Abs(temp);
                if (temp > max)
                {
                    max = temp;
                }
            }
            return max;
        }

        #endregion

        #region Serialization

        private static XmlNode SerializePoint(ControlPoint point, XmlDocument ownerDocument)
        {
            var pointNode = ownerDocument.CreateElement(ParseTokens.Point.ToString());

            var x = string.Format("{0:0.###}", point.X);
            var xNode = ownerDocument.CreateElement(ParseTokens.x.ToString());
            xNode.AppendChild(ownerDocument.CreateTextNode(x));
            pointNode.AppendChild(xNode);

            var y = string.Format("{0:0.###}", point.Y);
            var yNode = ownerDocument.CreateElement(ParseTokens.y.ToString());
            yNode.AppendChild(ownerDocument.CreateTextNode(y));
            pointNode.AppendChild(yNode);

            return pointNode;
        }

        #endregion

        #region Nested type: ParseTokens

        private enum ParseTokens
        {
            Invalid,
            Point,
            MinInput,
            MaxInput,
// ReSharper disable InconsistentNaming
            x,
            y
// ReSharper restore InconsistentNaming
        }

        #endregion
    }
}