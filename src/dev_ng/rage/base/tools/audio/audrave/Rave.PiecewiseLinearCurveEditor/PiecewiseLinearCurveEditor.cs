using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Rave.CurveEditor;
using Rave.CurveEditor.Curves;
using WPFToolLib.Extensions;

namespace Rave.PiecewiseLinearCurveEditor
{
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public class PiecewiseLinearCurveEditor : IPiecewiseLinearCurveEditor
    {
        private IObjectInstance m_instance;
        private double m_max;
        private double m_maxX;
        private double m_maxY;
        private IPiecewiseLinearCurveSerializer m_serializer;
        private ICurveEditorView m_curveEditorView;

        public PiecewiseLinearCurveEditor(ICurveEditorView view)
        {
            if (view == null)
            {
                throw new ArgumentNullException("view");
            }
            m_curveEditorView = view;
            m_curveEditorView.Dirty += OnDirty;
            Control = view as UIElement;
        }

        #region IPiecewiseLinearCurveEditor Members

        public void EditObject(IObjectInstance instance)
        {
            if (m_instance != null &&
                m_instance.Bank != null)
            {
                m_instance.Bank.BankStatusChanged -= OnBankStatusChanged;
            }

            Reset();

            var isNewInstance = m_instance != instance;

            m_instance = instance;
            if (m_instance == null)
            {
                return;
            }

            if (m_instance.Bank != null)
            {
                m_instance.Bank.BankStatusChanged += OnBankStatusChanged;
            }

            m_curveEditorView.IsCheckedOut = !m_instance.IsReadOnly;

            var points = m_serializer.Load(m_instance, out m_max);

            SetupAxesAndCreateCurve(m_max, points, isNewInstance);
        }

        public bool SetXAxisLength(string lengthText)
        {
            double length;
            if (double.TryParse(lengthText, out length))
            {
                var oldX = m_curveEditorView.XAxisLength;
                MaxX = length;
                m_curveEditorView.XAxisLength = length;
                m_curveEditorView.ViewModel.ScaleCurves(m_curveEditorView.XAxisLength / oldX);
                return true;
            }
            
            MessageBox.Show("Invalid X-Axis length specified.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            return false;
        }

        public bool SetYAxisLength(string lengthText)
        {
            double length;
            if (double.TryParse(lengthText, out length))
            {
                var oldY = m_curveEditorView.YAxisLength;
                MaxY = length;
                m_curveEditorView.YAxisLength = length;
                m_curveEditorView.ViewModel.ScaleCurves(1, m_curveEditorView.YAxisLength / oldY);
                return true;
            }
            
            MessageBox.Show("Invalid Y-Axis length specified.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            return false;
        }

        public void SetSize(double size)
        {
            m_curveEditorView.ViewModel.XAxis.Size = size;
            m_curveEditorView.ViewModel.YAxis.Size = size;
            m_curveEditorView.ViewModel.GridLines.Height = size;
            m_curveEditorView.ViewModel.GridLines.Width = size;
            m_curveEditorView.ViewModel.UpdateCurves();
        }

        public double MaxX
        {
            get { return m_maxX; }
            private set
            {
                if (m_maxX != value)
                {
                    m_maxX = value;
                    PropertyChanged.Raise(this, "MaxX");
                }
            }
        }

        public double MaxY
        {
            get { return m_maxY; }
            private set
            {
                if (m_maxY != value)
                {
                    m_maxY = value;
                    PropertyChanged.Raise(this, "MaxY");
                }
            }
        }

        public UIElement Control { get; private set; }

        public void Dispose()
        {
            if (m_instance != null && m_instance.Bank != null)
            {
                m_instance.Bank.BankStatusChanged -= OnBankStatusChanged;
            }

            if (m_curveEditorView != null)
            {
                m_curveEditorView.Dirty -= OnDirty;
                m_curveEditorView.Dispose();
                m_curveEditorView = null;
            }

            Control = null;
            m_serializer = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private double GetLargestX()
        {
            var x = m_max;
            if (m_curveEditorView.ViewModel.CurveEvaluators.Count > 0)
            {
                var curve = m_curveEditorView.ViewModel.CurveEvaluators[0].Curve;
                foreach (var controlPoint in curve.ControlPoints)
                {
                    var absX = Math.Abs(controlPoint.X);
                    if (absX > x)
                    {
                        x = absX;
                    }
                }
            }
            return x;
        }

        private double GetLargestY()
        {
            var y = 1.0;
            if (m_curveEditorView.ViewModel.CurveEvaluators.Count > 0)
            {
                var curve = m_curveEditorView.ViewModel.CurveEvaluators[0].Curve;
                foreach (var controlPoint in curve.ControlPoints)
                {
                    var absY = Math.Abs(controlPoint.Y);
                    if (absY > y)
                    {
                        y = absY;
                    }
                }
            }
            return y;
        }

        private void SetupAxesAndCreateCurve(double max, ICollection<Point> points, bool editingNewInstance)
        {
            if (editingNewInstance)
            {
                MaxX = max;
            }
            else
            {
                MaxX = m_curveEditorView.XAxisLength / 2;
            }
            MaxY = 1;

            if (points.Count > 1)
            {
                foreach (var point in points)
                {
                    var absX = Math.Abs(point.X);
                    if (absX > MaxX)
                    {
                        MaxX = absX;
                    }

                    var absY = Math.Abs(point.Y);
                    if (absY > MaxY)
                    {
                        MaxY = absY;
                    }
                }

                m_curveEditorView.XAxisLength = MaxX;
                m_curveEditorView.YAxisLength = MaxY;
                m_curveEditorView.AddCurve(CurveTypes.PiecewiseLinear, points);
            }
            else
            {
                m_curveEditorView.XAxisLength = MaxX;
                m_curveEditorView.YAxisLength = MaxY;
            }
        }

        private void OnDirty()
        {
            if (m_curveEditorView.IsCheckedOut)
            {
                if (m_curveEditorView.ViewModel.CurveEvaluators.Count > 0)
                {
                    var curve = m_curveEditorView.ViewModel.CurveEvaluators[0].Curve;
                    m_serializer.Save(m_instance, curve.ControlPoints);
                }
                else
                {
                    m_serializer.Save(m_instance, null);
                }
            }
        }

        private void Reset()
        {
            if (m_curveEditorView != null)
            {
                m_curveEditorView.RemoveCurves();
                m_curveEditorView.IsCheckedOut = false;
            }
            m_serializer = new PiecewiseLinearCurveSerializer();
        }

        private void OnBankStatusChanged()
        {
            if (m_instance != null &&
                m_instance.ObjectLookup.ContainsKey(m_instance.Name))
            {
                // Edit the newly loaded object instance
                EditObject(m_instance.ObjectLookup[m_instance.Name]);
            }
            else
            {
                Reset();
            }
        }
    }
}