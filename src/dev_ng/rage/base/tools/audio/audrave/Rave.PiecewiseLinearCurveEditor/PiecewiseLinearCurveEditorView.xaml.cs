﻿namespace Rave.PiecewiseLinearCurveEditor
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Xml;

    using Rave.CurveEditor;
    using Rave.CurveEditor.Axes;
    using Rave.CurveEditor.GridLines;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;

    /// <summary>
    ///   Interaction logic for PiecewiseLinearCurveEditorView.xaml
    /// </summary>
    public partial class PiecewiseLinearCurveEditorView : IRAVEObjectEditorPlugin
    {
        private IPiecewiseLinearCurveEditor m_editor;
        private readonly Brush m_errorBrush;
        private readonly Brush m_normalBrush;
        private IObjectInstance m_objectToEdit;

        public PiecewiseLinearCurveEditorView()
        {
            InitializeComponent();

            m_normalBrush = xAxisLength.Background;
            m_errorBrush = Brushes.Tomato;
        }

        #region IRAVEObjectEditorPlugin Members

#pragma warning disable 0067
        public event Action<IObjectInstance> OnObjectEditClick;
        public event Action<IObjectInstance> OnObjectRefClick;
        public event Action<string, string> OnWaveRefClick;
        public event Action<string> OnWaveBankRefClick;
#pragma warning restore 0067

        public string ObjectType
        {
            get { return "PiecewiseLinear"; }
        }

        public string GetName()
        {
            return "Piecewise Linear Curve Editor";
        }

        public bool Init(XmlNode settings)
        {
            return true;
        }
        
        public void EditObject(IObjectInstance objectInstance, Mode mode)
        {
            UserControl_Unloaded(null, null);
            m_objectToEdit = objectInstance;
            UserControl_Loaded(null, null);
        }

        public void Dispose()
        {
            // Do nothing
        }

        #endregion

        private void OnClickSetX(object sender, RoutedEventArgs e)
        {
            if (!m_editor.SetXAxisLength(xAxisLength.Text))
            {
                xAxisLength.Text = m_editor.MaxX.ToString("0.###");
            }
        }

        private void OnClickSetY(object sender, RoutedEventArgs e)
        {
            if (!m_editor.SetYAxisLength(yAxisLength.Text))
            {
                yAxisLength.Text = m_editor.MaxY.ToString("0.###");
            }
        }

        private void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox != null)
            {
                double temp;
                if (string.IsNullOrEmpty(textBox.Text) ||
                    (double.TryParse(textBox.Text, out temp) && temp > 0))
                {
                    textBox.Background = m_normalBrush;
                }
                else
                {
                    textBox.Background = m_errorBrush;
                }
                e.Handled = true;
            }
        }

        private static bool CanMoveControlPoint(Point? previousControlPoint,
                                                Point? nextControlPoint,
                                                Point currentControlPoint)
        {
            if (previousControlPoint != null)
            {
                if (currentControlPoint.X <=
                    previousControlPoint.Value.X)
                {
                    return false;
                }
            }

            if (nextControlPoint != null)
            {
                if (currentControlPoint.X >=
                    nextControlPoint.Value.X)
                {
                    return false;
                }
            }
            return true;
        }

        private void SizeSliderChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (m_editor != null)
            {
                m_editor.SetSize(e.NewValue);
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (m_editor != null)
            {
                root.Children.Clear();
                DataContext = null;
                m_editor.Dispose();
                m_editor = null;
            }

            if (e != null)
            {
                e.Handled = true;
            }
        }
        
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (m_objectToEdit != null)
            {
                var curveEditorModel = new CurveEditorModel(
                    1,
                    new AxisModel("X", 1, 260),
                    new AxisModel("Y", 1, 260),
                    new GridLinesModel(260, 260),
                    CanMoveControlPoint);
                var curveEditorViewModel = new CurveEditorViewModel(curveEditorModel);
                var curveEditorView = new CurveEditorView(curveEditorViewModel);
                m_editor = new PiecewiseLinearCurveEditor(curveEditorView);
                DataContext = m_editor;
                root.Children.Add(m_editor.Control);

                zoom.Value = 260;

                m_editor.EditObject(m_objectToEdit);
            }
        }
    }
}