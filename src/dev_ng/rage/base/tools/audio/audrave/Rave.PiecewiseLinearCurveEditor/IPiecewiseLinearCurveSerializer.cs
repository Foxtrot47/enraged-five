using System.Collections.Generic;
using System.Windows;
using Rave.CurveEditor.Curves;

namespace Rave.PiecewiseLinearCurveEditor
{
    using Rave.Types.Infrastructure.Interfaces.Objects;

    public interface IPiecewiseLinearCurveSerializer
    {
        IList<Point> Load(IObjectInstance instance, out double max);
        
        void Save(IObjectInstance instance, IList<ControlPoint> points);
    }
}