﻿// -----------------------------------------------------------------------
// <copyright file="IVisualAssetManager.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.AssetBrowser.Infrastructure.Interfaces
{
    using System.Collections.Generic;

    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Visual asset manager interface.
    /// </summary>
    public interface IVisualAssetManager
    {
        /// <summary>
        /// The diff checked in.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="bank">
        /// The bank.
        /// </param>
        void DiffCheckedIn(string path, IXmlBank bank);

        /// <summary>
        /// The diff saved.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="bank">
        /// The bank.
        /// </param>
        void DiffSaved(string path, IXmlBank bank);

        /// <summary>
        /// Initialize asset manager.
        /// </summary>
        void Init();

        /// <summary>
        /// The refresh view.
        /// </summary>
        void RefreshView();

        /// <summary>
        /// Check in a change list.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="selectedAssets">
        /// The selected assets.
        /// </param>
        void CheckIn(IChangeList changeList, IList<IAsset> selectedAssets);
    }
}
