namespace Rave.ObjectExport
{
    using System;
    using System.Collections;
    using System.Text;
    using System.Xml;

    using Rave.Instance;
    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.Utils;

    using rageUsefulCSharpToolClasses;

    using Rave.Plugins.Infrastructure.Interfaces;

    using Rockstar.AssetManager.Interfaces;

    public class ObjectExport: IRAVEObjectBrowserPlugin
    {
        private string m_Name,m_Command,m_Parameters,m_Delimiter;

        #region IRAVEPlugin Members

        public string GetName()
        {
            return m_Name;
        }

        public bool Init(XmlNode settings)
        {
            foreach (XmlNode setting in settings.ChildNodes)
            {
                switch (setting.Name)
                {
                    case "Name":
                        m_Name = setting.InnerText;
                        break;
                    case "Command":
                        m_Command = setting.InnerText;
                        break;
                    case "Parameters":
                        m_Parameters = setting.InnerText;
                        break;
                    case "Delimiter":
                        m_Delimiter = setting.InnerText;
                        if (m_Delimiter == "")
                        {
                            m_Delimiter = " ";
                        }
                        break;
                }
            }
            return true;
        }

        public void Shutdown()
        {
        }

        #endregion

        public void Process(ArrayList nodes)
        {
            if (nodes.Count == 0)
            {
                return;
            }

            //check out object banks
            StringBuilder sb = new StringBuilder();
            IChangeList c = RaveInstance.AssetManager.CreateChangeList("Object Export Plugin");
            foreach (BaseNode sn in nodes)
            {
                SoundBankNode sbn = sn as SoundBankNode;
                if (sbn != null)
                {
                    try
                    {
                        sbn.ObjectBank.Checkout(c.NumberAsString, false);
                        sb.Append(sbn.ObjectBank.FilePath);
                        sb.Append(m_Delimiter);
                    }
                    catch (Exception e)
                    {
                        ErrorManager.HandleError(e);
                    }
                }
            }

            rageStatus status;
            rageExecuteCommand cmd = new rageExecuteCommand(m_Command, m_Parameters + sb.ToString(), out status);

  
            if (status.Success())
            {
                ErrorManager.HandleInfo("Object Export plugin finished successfully");
            }

         }
    }
}
