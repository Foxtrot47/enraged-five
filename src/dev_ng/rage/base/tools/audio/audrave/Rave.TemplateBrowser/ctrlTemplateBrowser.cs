// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ctrlTemplateBrowser.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The template browser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.TemplateBrowser
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;
    using System.Xml;

    using rage.ToolLib;

    using Rave.Controls.Forms;
    using Rave.Controls.Forms.Popups;
    using Rave.HierarchyBrowser.Infrastructure.Nodes;
    using Rave.Instance;
    using Rave.Metadata.Infrastructure.Interfaces;
    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.TemplateBrowser.Infrastructure.Interfaces;
    using Rave.TemplateBrowser.Infrastructure.Nodes;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;
    using Rave.Types.ObjectBanks.Templates;
    using Rave.Types.Objects.Templates;
    using Rave.Utils;

    using TemplateNode = Rave.TemplateBrowser.Infrastructure.Nodes.TemplateNode;

    /// <summary>
    /// The template browser.
    /// </summary>
    public partial class ctrlTemplateBrowser : DockableWFControl, ITemplateBrowser
    {
        #region Fields

        /// <summary>
        /// The bankManager.
        /// </summary>
        private IXmlBankManager bankManager;

        /// <summary>
        /// The node state.
        /// </summary>
        private XmlDocument nodeState;

        /// <summary>
        /// The root.
        /// </summary>
        private TemplateBaseNode root;

        /// <summary>
        /// The template path.
        /// </summary>
        private string templatePath;

        /// <summary>
        /// The type.
        /// </summary>
        private string type;

        /// <summary>
        /// The type definitions.
        /// </summary>
        private ITypeDefinitions typeDefs;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ctrlTemplateBrowser"/> class.
        /// </summary>
        public ctrlTemplateBrowser()
        {
            this.MetadataManagers = RaveInstance.XmlBankCompiler.MetadataManagers;
            this.InitializeComponent();
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The change list modified action.
        /// </summary>
        public event Action ChangeListModified;

        /// <summary>
        /// The template deleted event.
        /// </summary>
        public event Action<ITemplate> TemplateDeleted;

        /// <summary>
        /// The find object event.
        /// </summary>
        public event Action<IObjectInstance> FindObject;

        /// <summary>
        /// The template selected event.
        /// </summary>
        public event Action<ITemplate> TemplateSelected;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the metadata managers.
        /// </summary>
        public IDictionary<string, IMetadataManager> MetadataManagers { get; private set; }

        /// <summary>
        /// Gets the base path.
        /// </summary>
        public string BasePath { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Add template bank nodes for a given bank.
        /// </summary>
        /// <param name="bank">
        /// The template bank.
        /// </param>
        /// <returns>
        /// The template bank node <see cref="TemplateBankNode"/>.
        /// </returns>
        public TemplateBankNode AddNodesForBank(ITemplateBank bank)
        {
            var path = bank.FilePath.ToUpper().Replace(@"/", "\\");

            // strip off the base path
            path = path.ToUpper().Replace(Configuration.ObjectXmlPath.ToUpper(), string.Empty);
            path = path.Replace(".XML", string.Empty);
            var pathElements = path.Split('\\');

            var nodes = this.treeView.Nodes;
            for (var i = 0; i < pathElements.Length; i++)
            {
                var found = false;
                foreach (TreeNode node in nodes)
                {
                    if (pathElements[i] != node.Text)
                    {
                        continue;
                    }

                    nodes = node.Nodes;
                    found = true;
                    if (i == pathElements.Length - 1)
                    {
                        return node as TemplateBankNode;
                    }

                    break;
                }

                if (found)
                {
                    continue;
                }

                // Create this node
                if (i == pathElements.Length - 1)
                {
                    var bankNode = new TemplateBankNode(bank);
                    this.treeView.BeginUpdate();
                    nodes.Add(bankNode);
                    this.treeView.EndUpdate();
                    return bankNode;
                }

                var folder = new TemplateFolderNode(pathElements[i], i == pathElements.Length - 2);
                this.treeView.BeginUpdate();
                nodes.Add(folder);
                folder.Expand();
                this.treeView.EndUpdate();
                nodes = folder.Nodes;
            }

            return null;
        }

        /// <summary>
        /// Create a new template bank.
        /// </summary>
        /// <param name="bankName">
        /// The bank name.
        /// </param>
        /// <returns>
        /// The new template bank <see cref="TemplateBank"/>.
        /// </returns>
        public ITemplateBank CreateNewBank(string bankName)
        {

            if (String.IsNullOrEmpty(bankName)) throw new Exception("No bank name specified");
            if (String.IsNullOrEmpty(this.templatePath)) throw new Exception("No templatepath specified");

            if (!Directory.Exists(this.BasePath))
            {
                Directory.CreateDirectory(this.BasePath);
            }

            TreeNode lastFolderNode = this.treeView.LastSelectedNode;
            while (!(lastFolderNode is TemplateFolderNode))
            {
                lastFolderNode = this.treeView.LastSelectedNode.Parent;
            }


            string path = Configuration.ObjectXmlPath + lastFolderNode.FullPath + "\\" + bankName + ".XML";

            var writer = new XmlTextWriter(path, Encoding.UTF8);
            writer.WriteElementString("Templates", string.Empty);
            writer.Close();

            try
            {
                var changeList = RaveInstance.AssetManager.CreateChangeList("[Rave] Initial import of " + path);
                changeList.MarkAssetForAdd(path);
                changeList.Submit();
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }

            var bank = new TemplateBank(path, this.type, this.typeDefs, this.MetadataManagers[type].Compiler);
            bank.Load();

            this.AddNodesForBank(bank).EnsureVisible();

            return bank;
        }

        /// <summary>
        /// Initialize the template browser.
        /// </summary>
        /// <param name="bankManager">
        /// The bankManager.
        /// </param>
        /// <param name="templatePath">
        /// The template path.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="typeDefinitions">
        /// The type definitions.
        /// </param>
        public void Init(IXmlBankManager bankManager, string templatePath, string type, ITypeDefinitions typeDefinitions)
        {
            this.typeDefs = typeDefinitions;
            this.templatePath = templatePath;
            this.type = type;
            this.root = new TemplateBaseNode("TEMPLATES");
            this.treeView.Nodes.Add(this.root);
            this.bankManager = bankManager;
            this.nodeState = new XmlDocument();
            this.nodeState.AppendChild(this.nodeState.CreateElement("State"));
            this.RefreshTree();
            this.BasePath = Configuration.ObjectXmlPath + this.templatePath;
        }

        /// <summary>
        /// Refresh the template browser tree.
        /// </summary>
        public void RefreshTree()
        {
            this.treeView.BeginUpdate();
            this.treeView.Nodes.Clear();
            foreach (var bank in this.bankManager.Banks)
            {
                var templateBank = bank as ITemplateBank;
                if (null == templateBank)
                {
                    throw new Exception("Loading " + this.type + " Templates. Non-template bank found");
                }

                this.AddNodesForBank(templateBank);
            }

            this.treeView.EndUpdate();
        }

        /// <summary>
        /// The find template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        public void FindTemplate(ITemplate template)
        {
            this.Content.Show();
            this.Content.IsSelected = true;
            var node = FindNodeWithTemplate(template, this.treeView.Nodes);
            if (node != null)
            {
                this.treeView.BeginUpdate();
                node.EnsureVisible();
                this.treeView.LastSelectedNode = node;
                this.treeView.EndUpdate();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Expand the template browser tree.
        /// </summary>
        /// <param name="nodeState">
        /// The node state.
        /// </param>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private static void ExpandTree(XmlNode nodeState, IList treeNodes)
        {
            foreach (XmlNode state in nodeState.ChildNodes)
            {
                foreach (TreeNode treeNode in treeNodes)
                {
                    var name = Regex.Replace(treeNode.Text, @"[^\w-]+", string.Empty);
                    if (state.Name != name)
                    {
                        continue;
                    }

                    if (state.HasChildNodes)
                    {
                        treeNode.Expand();
                        ExpandTree(state, treeNode.Nodes);
                    }

                    break;
                }
            }
        }

        /// <summary>
        /// Save the template browser tree state.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private static void SaveTreeState(XmlNode parentNode, IEnumerable treeNodes)
        {
            foreach (TreeNode treeNode in treeNodes)
            {
                var name = Regex.Replace(treeNode.Text, @"[^\w-]+", string.Empty);
                XmlNode node = parentNode.OwnerDocument.CreateElement(name);
                parentNode.AppendChild(node);
                if (treeNode.IsExpanded)
                {
                    SaveTreeState(node, treeNode.Nodes);
                }
            }
        }

        /// <summary>
        /// The find node with template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <returns>
        /// The template node <see cref="TemplateNode"/>
        /// </returns>
        private static TemplateNode FindNodeWithTemplate(ITemplate template, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                var templateNode = node as TemplateNode;
                if (templateNode != null && templateNode.Template == template)
                {
                    return templateNode;
                }

                templateNode = FindNodeWithTemplate(template, node.Nodes);
                if (templateNode != null && templateNode.Template == template)
                {
                    return templateNode;
                }
            }

            return null;
        }

        /// <summary>
        /// Set event of drag action.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private static void Treeview_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        /// <summary>
        /// Expand the template browser tree.
        /// </summary>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private void ExpandTree(IList treeNodes)
        {
            this.treeView.BeginUpdate();
            ExpandTree(this.nodeState.DocumentElement, treeNodes);
            this.treeView.EndUpdate();
        }

        /// <summary>
        /// Get latest data for a template folder node.
        /// </summary>
        /// <param name="folderNode">
        /// The folder node.
        /// </param>
        private void GetLatestFolder(TemplateFolderNode folderNode)
        {
            // Find path of folder
            var path = Configuration.ObjectXmlPath + folderNode.Path;
            var dir = new DirectoryInfo(path);

            // get a list of bankManager currently in the folder node
            List<TemplateBankNode> currBanks = folderNode.GetBankNodes();

            // create a list of checked out bankManager
            var checkedOutBanks = currBanks.Where(tbn => tbn.Bank.IsCheckedOut).ToList();

            // remove any checked out bankManager from bank list
            foreach (var checkedOutbank in checkedOutBanks)
            {
                currBanks.Remove(checkedOutbank);
            }

            // remove bankManager from bank list and sounds from sound list
            // so when we recreate/update the lists we do not get duplicates
            foreach (var currBank in currBanks)
            {
                this.bankManager.RemoveBank(currBank.Bank);
                currBank.Remove();
                foreach (var template in currBank.Bank.Templates)
                {
                    Template.TemplateLookupTables[this.type].Remove(template.Name);
                }
            }

            // load the bankManager in the directory corresponding to the folder
            this.bankManager.LoadBanks(dir, string.Empty, this.type, BankTypes.Template);

            // Update the object references
            this.bankManager.ComputeReferences();
            this.RefreshTree();
        }

        /// <summary>
        /// Raise FindObjectInstance action for selected output.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        private void Output_OnSelectObject(object obj)
        {
            var objectInst = obj as IObjectInstance;
            if (objectInst != null)
            {
                this.FindObject.Raise(objectInst);
            }
        }

        /// <summary>
        /// Save the template browser tree state.
        /// </summary>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private void SaveTreeState(TreeNodeCollection treeNodes)
        {
            this.nodeState.DocumentElement.RemoveAll();
            SaveTreeState(this.nodeState.DocumentElement, treeNodes);
        }

        /// <summary>
        /// Raise TemplateSelected action when tree view is clicked.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void TreeView_DoubleClick(object sender, EventArgs e)
        {
            var node = this.treeView.LastSelectedNode as TemplateNode;
            if (node != null)
            {
                this.TemplateSelected.Raise(node.Template);
            }
        }

        /// <summary>
        /// Keep track of last selected node when item dragged over tree view.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void TreeView_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
            var pointToClient = this.treeView.PointToClient(new Point(e.X, e.Y));
            this.treeView.LastSelectedNode = this.treeView.GetNodeAt(pointToClient.X, pointToClient.Y);
        }

        /// <summary>
        /// Handle drag+drop action on tree view.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void Treeview_DragDrop(object sender, DragEventArgs e)
        {
            IObjectInstance obj = null;
            if (e.Data.GetDataPresent(typeof(SoundNode[])))
            {
                var soundNode = ((SoundNode[])e.Data.GetData(typeof(SoundNode[])))[0];
                obj = soundNode.ObjectInstance;
            }
            else if (e.Data.GetDataPresent(typeof(ObjectNode[])))
            {
                var sn =
                    ((ObjectNode[])
                     e.Data.GetData(typeof(ObjectNode[])))[0];
                obj = sn.ObjectInstance;
            }
            else if (e.Data.GetDataPresent(typeof(ObjectNode)))
            {
                var sn =
                    (ObjectNode)
                    e.Data.GetData(typeof(ObjectNode));
                obj = sn.ObjectInstance;
            }

            if (obj != null && this.treeView.LastSelectedNode.GetType() == typeof(TemplateBankNode))
            {
                var bankNode = this.treeView.LastSelectedNode as TemplateBankNode;

                if (null == bankNode)
                {
                    ErrorManager.HandleError("Failed to get bank node");
                    return;
                }

                if (!bankNode.Bank.TryToCheckout())
                {
                    var status =
                        RaveInstance.AssetManager.GetFileStatus(
                            RaveInstance.AssetManager.GetLocalPath(bankNode.Bank.FilePath));

                    if (status.IsLocked && !status.IsLockedByMe)
                    {
                        ErrorManager.HandleInfo(
                            "Template bank " + bankNode.Bank.Name + " is currently locked by " + status.Owner);
                        return;
                    }

                    ErrorManager.HandleInfo("Failed to check out template bank " + bankNode.Bank.Name);
                    return;
                }

                if (obj.Type.ToUpper() == this.type.ToUpper())
                {
                    frmSimpleInput.InputResult input = frmSimpleInput.GetInput("Template name");
                    if (input.HasBeenCancelled) return;
                    string name = input.data;

                    while (Template.TemplateLookupTables[this.type].ContainsKey(name))
                    {
                        input = frmSimpleInput.GetInput(this.ParentForm, "Please enter a valid and unique template name", name);
                        if (input.HasBeenCancelled) return;
                        name = input.data;
                    }

                    try
                    {
                        var typedObjInstance = obj as ITypedObjectInstance;

                        if (null == typedObjInstance)
                        {
                            throw new Exception("New template instance requires original object of type ITypedObjectInstance");
                        }

                        var lookup = Template.TemplateLookupTables[bankNode.Bank.Type];
                        var template = new Template(name, bankNode.Bank, this.type, typedObjInstance, this.typeDefs, lookup);
                        template.ComputeReferences();
                        bankNode.Bank.AddTemplate(template);
                        var node = new TemplateNode(template);
                        bankNode.Nodes.Add(node);
                        node.EnsureVisible();
                        bankNode.Bank.MarkAsDirty();
                    }
                    catch (Exception ex)
                    {
                        ErrorManager.HandleError("Failed to create new template: " + ex.Message);
                        return;
                    }
                }
            }
            else
            {
                ErrorManager.HandleInfo("Object for templates can only be dropped onto a template bank");
            }

            this.ChangeListModified.Raise();
        }

        /// <summary>
        /// Checkout selected template bank.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void mnuCheckOut_Click(object sender, EventArgs e)
        {
            if (this.treeView.LastSelectedNode.GetType() == typeof(TemplateBankNode))
            {
                var bankNode = this.treeView.LastSelectedNode as TemplateBankNode;
                if (null != bankNode && bankNode.Bank.Checkout(false))
                {
                    var expanded = bankNode.IsExpanded;
                    bankNode.Update();
                    if (expanded)
                    {
                        bankNode.Expand();
                    }

                    this.ChangeListModified.Raise();
                }
            }
        }

        private void mnuRevert_Click(object sender, EventArgs e)
        {
            if (this.treeView.LastSelectedNode.GetType() == typeof(TemplateBankNode))
            {
                var bankNode = this.treeView.LastSelectedNode as TemplateBankNode;
                if (null != bankNode && bankNode.Bank.Revert())
                {
                    var expanded = bankNode.IsExpanded;
                    bankNode.Update();
                    if (expanded)
                    {
                        bankNode.Expand();
                    }

                    this.ChangeListModified.Raise();
                }
            }
        }

        /// <summary>
        /// Delete selected template bank.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void mnuDelete_Click(object sender, EventArgs e)
        {
            if (this.treeView.LastSelectedNode.GetType() == typeof(TemplateBankNode))
            {
                var bankNode = this.treeView.LastSelectedNode as TemplateBankNode;
                var canDelete = true;
                var allReferences = new List<IReferencable>();

                foreach (TemplateNode tn in bankNode.Nodes)
                {
                    if (tn.Template.Referencers.Count > 0)
                    {
                        allReferences.AddRange(tn.Template.Referencers);
                        canDelete = false;
                    }
                }

                if (!canDelete)
                {
                    var output = new frmSimpleOutput("Referencing Objects");
                    output.DisplayInfo(
                        "Cannot delete template bank as one or more of the templates are referenced by objects. Double click to view object in browser");
                    output.OnSelectObject += this.Output_OnSelectObject;
                    foreach (var templateInstance in allReferences)
                    {
                        output.AddObject(templateInstance);
                    }

                    output.Show(RaveInstance.ActiveWindow);
                }
                else
                {
                    if (ErrorManager.HandleQuestion(
                        "This action cannot be undone. Do you wish to continue?",
                        MessageBoxButtons.OKCancel,
                        DialogResult.OK) != DialogResult.OK)
                    {
                        return;
                    }

                    if (!bankNode.Bank.Delete(true))
                    {
                        ErrorManager.HandleInfo("Failed to delete template bank: " + bankNode.Bank.Name);
                        return;
                    }

                    bankNode.Nodes.Clear();
                    bankNode.Remove();
                    this.ChangeListModified.Raise();
                }
            }
            else if (this.treeView.LastSelectedNode.GetType() == typeof(TemplateNode))
            {
                var node = this.treeView.LastSelectedNode as TemplateNode;
                if (null == node)
                {
                    return;
                }

                var bankNode = node.Parent as TemplateBankNode;

                if (bankNode != null)
                {
                    var template = node.Template;
                    if (template.Referencers.Count > 0)
                    {
                        var output = new frmSimpleOutput("Referencing Objects");
                        output.DisplayInfo(
                            "Cannot delete template as it is referenced by objects. Double click to view object in browser");
                        output.OnSelectObject += this.Output_OnSelectObject;
                        foreach (var referencer in template.Referencers)
                        {
                            output.AddObject(referencer);
                        }

                        output.Show(RaveInstance.ActiveWindow);
                    }
                    else
                    {
                        if (template.Delete())
                        {
                            bankNode.Nodes.Remove(node);
                            this.TemplateDeleted.Raise(template);
                        }
                        else
                        {
                            ErrorManager.HandleInfo("Failed to delete template: " + template.Name);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get latest data for selected template bank.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void mnuGetLatest_Click(object sender, EventArgs e)
        {
            this.SaveTreeState(this.treeView.Nodes);
            if (this.treeView.LastSelectedNode.GetType() == typeof(TemplateBankNode))
            {
                var tbn = this.treeView.LastSelectedNode as TemplateBankNode;
                RaveInstance.AssetManager.GetLatest(tbn.Bank.FilePath, false);
                tbn.Bank.Reload();
            }
            else if (this.treeView.LastSelectedNode.GetType() == typeof(TemplateFolderNode))
            {
                this.GetLatestFolder(this.treeView.LastSelectedNode as TemplateFolderNode);
            }

            this.ExpandTree(this.treeView.Nodes);

        }


        private void mnuNewBank_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.templatePath))
            {
                ErrorManager.HandleInfo(
                    "Please specify a <TemplatePath> under the <MetadataType> in the project settings.\r\nThe path should be relative to the object xml path");
                return;
            }

            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "Template store name");
            if (input.HasBeenCancelled) return;
            string name = input.data.ToUpper();

            ITemplateBank newBank = this.CreateNewBank(name);
            newBank.Checkout(false);
        }

        private void mnuNewFolder_Click(object sender, System.EventArgs e)
        {
            if (this.treeView.LastSelectedNode == null)
            {
                return;
            }

            TreeNode baseNode = this.treeView.LastSelectedNode as TreeNode;
            List<string> nodeNames = (from TreeNode tn in baseNode.Nodes select tn.Text).ToList();

            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "Folder name");
            if (input.HasBeenCancelled) return;
            string folderName = input.data;

            while (folderName != null && nodeNames.Contains(folderName))
            {
                input = frmSimpleInput.GetInput(
                    this.ParentForm, string.Format("Name {0} is invalid. Folder name", folderName));
                if (input.HasBeenCancelled) return;
                folderName = input.data;
            }

            folderName = folderName.ToUpper();


            TemplateFolderNode newNode = new TemplateFolderNode(
     folderName, true);
            TreeNode lastFolderNode = this.treeView.LastSelectedNode;
            while (!(lastFolderNode is TemplateFolderNode))
            {
                lastFolderNode = this.treeView.LastSelectedNode.Parent;
            }
            lastFolderNode.Nodes.Add(newNode);
            string path = Configuration.ObjectXmlPath + "\\" + lastFolderNode.FullPath + "\\" + folderName;
            Directory.CreateDirectory(path);
        }
        /// <summary>
        /// Populate context menu for tree view.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void mnuPopUp_Opening(object sender, CancelEventArgs e)
        {
            this.mnuCheckOut.Visible = false;
            this.mnuRevert.Visible = false;
            this.mnuDelete.Visible = false;
            this.mnuSave.Visible = false;
            this.mnuNewBank.Visible = false;
            this.mnuGetLatest.Visible = false;
            this.mnuNewFolder.Visible = false;
            this.mnuNewFolder.Enabled = false;

            if (this.treeView.LastSelectedNode == null)
            {
                this.mnuNewBank.Visible = true;
            }
            else
            {
                this.mnuGetLatest.Visible = true;

                if (this.treeView.LastSelectedNode.GetType() == typeof(TemplateBankNode))
                {
                    this.mnuDelete.Visible = this.mnuDelete.Enabled = true;
                    this.mnuSave.Visible = this.mnuSave.Enabled = true;
                    this.mnuCheckOut.Visible = this.mnuCheckOut.Enabled = true;
                    this.mnuRevert.Visible = true;
                    this.mnuNewFolder.Visible = this.mnuNewFolder.Enabled = true;

                    var bankNode = this.treeView.LastSelectedNode as TemplateBankNode;
                    if (!bankNode.Bank.IsDirty)
                    {
                        this.mnuSave.Enabled = false;
                    }

                    if (bankNode.Bank.IsCheckedOut)
                    {
                        this.mnuCheckOut.Enabled = false;
                        this.mnuRevert.Enabled = true;
                        this.mnuGetLatest.Enabled = false;
                    }
                    else
                    {
                        this.mnuRevert.Enabled = false;
                        this.mnuGetLatest.Enabled = true;
                    }
                }
                else if (this.treeView.LastSelectedNode.GetType() == typeof(TemplateNode))
                {
                    this.mnuDelete.Visible = true;
                    this.mnuGetLatest.Visible = false;

                    var tn = this.treeView.LastSelectedNode as TemplateNode;
                    this.mnuDelete.Enabled = tn.Template.TemplateBank.IsCheckedOut;
                }
                else if (this.treeView.LastSelectedNode.GetType() == typeof(TemplateFolderNode))
                {
                    if (((TemplateFolderNode)this.treeView.LastSelectedNode).CanAddBank())
                    {
                        this.mnuNewBank.Visible = this.mnuNewBank.Enabled = true;
                        this.mnuNewFolder.Visible = this.mnuNewFolder.Enabled = true;
                    }
                    this.mnuGetLatest.Visible = this.mnuGetLatest.Enabled = true;
                }
            }
        }

        /// <summary>
        /// Save the selected template bank.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void mnuSave_Click(object sender, EventArgs e)
        {
            if (this.treeView.LastSelectedNode.GetType() == typeof(TemplateBankNode))
            {
                var tbn = this.treeView.LastSelectedNode as TemplateBankNode;
                tbn.Bank.Save();
            }
        }

        #endregion
    }
}