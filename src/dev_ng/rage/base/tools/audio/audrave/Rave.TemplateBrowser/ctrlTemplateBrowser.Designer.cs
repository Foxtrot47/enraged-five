namespace Rave.TemplateBrowser
{
    using Rave.Controls.Forms;

    partial class ctrlTemplateBrowser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctrlTemplateBrowser));
            this.m_Images = new System.Windows.Forms.ImageList(this.components);
            this.mnuPopUp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuNewBank = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCheckOut = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRevert = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSave = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuGetLatest = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.treeView = new ScrollableTreeView();
            this.mnuPopUp.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_Images
            // 
            this.m_Images.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("m_Images.ImageStream")));
            this.m_Images.TransparentColor = System.Drawing.Color.Transparent;
            this.m_Images.Images.SetKeyName(0, "CLSDFOLD.BMP");
            this.m_Images.Images.SetKeyName(1, "SoundStoreGrey.ico");
            this.m_Images.Images.SetKeyName(2, "SoundStoreGreyRed.ico");
            this.m_Images.Images.SetKeyName(3, "template.ico");
            // 
            // mnuPopUp
            // 
            this.mnuPopUp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNewBank,
            this.mnuCheckOut,
            this.mnuRevert,
            this.mnuDelete,
            this.mnuSave,
            this.mnuGetLatest,
            this.mnuNewFolder});
            this.mnuPopUp.Name = "mnuPopUp";
            this.mnuPopUp.ShowImageMargin = false;
            this.mnuPopUp.Size = new System.Drawing.Size(158, 136);
            this.mnuPopUp.Opening += new System.ComponentModel.CancelEventHandler(this.mnuPopUp_Opening);
            // 
            // mnuNewBank
            // 
            this.mnuNewBank.Name = "mnuNewBank";
            this.mnuNewBank.Size = new System.Drawing.Size(157, 22);
            this.mnuNewBank.Text = "New Template Store";
            this.mnuNewBank.Click += new System.EventHandler(this.mnuNewBank_Click);
            // 
            // mnuCheckOut
            // 
            this.mnuCheckOut.Name = "mnuCheckOut";
            this.mnuCheckOut.Size = new System.Drawing.Size(157, 22);
            this.mnuCheckOut.Text = "Check Out";
            this.mnuCheckOut.Click += new System.EventHandler(this.mnuCheckOut_Click);
            // 
            // mnuRevert
            // 
            this.mnuRevert.Name = "mnuRevert";
            this.mnuRevert.Size = new System.Drawing.Size(157, 22);
            this.mnuRevert.Text = "Revert";
            this.mnuRevert.Click += new System.EventHandler(this.mnuRevert_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Size = new System.Drawing.Size(157, 22);
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSave
            // 
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Size = new System.Drawing.Size(157, 22);
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuGetLatest
            // 
            this.mnuGetLatest.Name = "mnuGetLatest";
            this.mnuGetLatest.Size = new System.Drawing.Size(157, 22);
            this.mnuGetLatest.Text = "Get Latest";
            this.mnuGetLatest.Click += new System.EventHandler(this.mnuGetLatest_Click);
            //
            //mnuNewFolder
            //
            this.mnuNewFolder.Name = "mnuNewFolder";
            this.mnuNewFolder.Size = new System.Drawing.Size(157, 22);
            this.mnuNewFolder.Text = "New Folder";
            this.mnuNewFolder.Click += new System.EventHandler(this.mnuNewFolder_Click);
            // 
            // treeView
            // 
            this.treeView.AllowDrop = true;
            this.treeView.ContextMenuStrip = this.mnuPopUp;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.ImageIndex = 0;
            this.treeView.ImageList = this.m_Images;
            this.treeView.ItemHeight = 18;
            this.treeView.LastSelectedNode = null;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.SelectedImageIndex = 0;
            this.treeView.Size = new System.Drawing.Size(293, 370);
            this.treeView.Sorted = true;
            this.treeView.TabIndex = 0;
            this.treeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.Treeview_DragDrop);
            this.treeView.DragEnter += new System.Windows.Forms.DragEventHandler(Treeview_DragEnter);
            this.treeView.DragOver += new System.Windows.Forms.DragEventHandler(this.TreeView_DragOver);
            this.treeView.DoubleClick += new System.EventHandler(this.TreeView_DoubleClick);
            // 
            // ctrlTemplateBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeView);
            this.Name = "ctrlTemplateBrowser";
            this.Size = new System.Drawing.Size(293, 370);
            this.mnuPopUp.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ScrollableTreeView treeView;
        private System.Windows.Forms.ImageList m_Images;
        private System.Windows.Forms.ContextMenuStrip mnuPopUp;
        private System.Windows.Forms.ToolStripMenuItem mnuNewBank;
        private System.Windows.Forms.ToolStripMenuItem mnuDelete;
        private System.Windows.Forms.ToolStripMenuItem mnuCheckOut;
        private System.Windows.Forms.ToolStripMenuItem mnuRevert;
        private System.Windows.Forms.ToolStripMenuItem mnuSave;
        private System.Windows.Forms.ToolStripMenuItem mnuGetLatest;
        private System.Windows.Forms.ToolStripMenuItem mnuNewFolder;
    }
}
