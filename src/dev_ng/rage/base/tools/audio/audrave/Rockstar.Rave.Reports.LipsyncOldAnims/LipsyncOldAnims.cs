﻿// -----------------------------------------------------------------------
// <copyright file="LipsyncOldAnimsReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.LipsyncOldAnims
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using P4API;

    using Rockstar.AssetManager.Perforce;
    using Rockstar.AssetManager.Perforce.Interfaces;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;

    using global::Rave.Plugins.Infrastructure.Interfaces;

    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// <para>
    /// Report that generates a list of manually-created lipsync anim
    /// files that have a creation timestamp older than that of their
    /// corresponding wave file.
    /// </para>
    /// <para>
    /// Reason:
    /// We auto-generate facial animation for all speech waves - but
    /// also support manually created assets. This risks the wave files
    /// being updated without the manually-created anims, leaving them
    /// out of sync.
    /// </para>
    /// </summary>
    public class LipsyncOldAnims : IRAVEReportPlugin
    {
        /// <summary>
        /// The custom animation extension.
        /// </summary>
        private const string AnimExtension = ".anim";

        /// <summary>
        /// The wave extension.
        /// </summary>
        private const string WaveExtension = ".wav";

        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private IAssetManager assetManager;

        /// <summary>
        /// The request handler.
        /// </summary>
        private IP4RequestHandler requestHandler;

        /// <summary>
        /// The custom lipsync animations path.
        /// </summary>
        private string customAnimsPath;

        /// <summary>
        /// The waves path.
        /// </summary>
        private string wavesPath;

        /// <summary>
        /// The list of wave file last modified timestamps.
        /// </summary>
        private Dictionary<string, string> waveTimestamps;

        /// <summary>
        /// List of errors which will populate the report.
        /// mapping: error type -> list of errors
        /// </summary>
        private Dictionary<string, IList<string>> errors; 

        /// <summary>
        /// Get the report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            this.waveTimestamps = new Dictionary<string, string>();
            this.errors = new Dictionary<string, IList<string>>();

            this.errors["Missing Waves"] = new List<string>();
            this.errors["Out of Date Custom Anim Files"] = new List<string>();
            this.errors["General Errors"] = new List<string>();

            // Get instance of asset manager and request handler
            this.assetManager = AssetManagerFactory.GetInstance(AssetManagerType.Perforce);

            if (null == this.assetManager)
            {
                throw new Exception("Failed to get instance of Perforce asset manager");
            }

            this.requestHandler = (IP4RequestHandler)this.assetManager.RequestHandler;

            // Initialize report settings
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;

                    case "CustomAnimsPath":
                        this.customAnimsPath = this.assetManager.GetDepotPath(setting.InnerText);
                        break;

                    case "WavesPath":
                        this.wavesPath = this.assetManager.GetDepotPath(setting.InnerText);
                        break;
                }
            }

            // Report requires a name
            return !string.IsNullOrEmpty(this.name);
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes.
        /// </param>
        /// <param name="browsers">
        /// The sound browsers.
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            this.InitWaveList();
            this.CheckAnimFiles();
            this.DisplayResults();
        }

        /// <summary>
        /// Initialize the list of wave file last modified timestamps.
        /// </summary>
        private void InitWaveList()
        {
            // Get list of files in wave directory
            var request = new P4RunCmdRequest("files", this.wavesPath);
            this.requestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed)
            {
                throw new Exception("Failed to get wave file list: " + request.FormattedErrors);
            }

            // Populate list of wave files and their last modified timestamps
            foreach (P4Record record in request.RecordSet)
            {
                var action = record.Fields["action"].ToLower();
                if (action.Equals("delete"))
                {
                    // Ignore any deleted files
                    continue;
                }

                // Ignore files that aren't wave files
                var depotPath = record.Fields["depotFile"];
                var fileName = Path.GetFileName(depotPath).ToLower();
                if (!fileName.ToLower().EndsWith(WaveExtension))
                {
                    continue;
                }

                // Store last modified timestamp of current wave file against the wave file name
                this.waveTimestamps.Add(fileName, record.Fields["time"]);
            }
        }

        /// <summary>
        /// Check the last modified timestamp of all custom anim files against
        /// the corresponding wave file.
        /// </summary>
        private void CheckAnimFiles()
        {
            // Get list of files in custom anim directory
            var request = new P4RunCmdRequest("files", this.customAnimsPath);
            this.requestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed)
            {
                throw new Exception("Failed to get custom anim file list: " + request.FormattedErrors);
            }

            // Check each custom anim file
            foreach (P4Record record in request.RecordSet)
            {
                var action = record.Fields["action"].ToLower();
                if (action.Equals("delete"))
                {
                    // Ignore any deleted files
                    continue;
                }

                var depotPath = record.Fields["depotFile"];
                var ext = Path.GetExtension(depotPath);
                if (null == ext || !ext.ToLower().Equals(AnimExtension))
                {
                    // Only interested in custom anim files in this folder
                    continue;
                }

                // Determine wave file name
                var fileName = Path.GetFileNameWithoutExtension(depotPath).ToLower();
                var waveName = fileName + WaveExtension;

                // Check timestamp of custom anim file against wave file
                var timestamp = record.Fields["time"];
                string wavTimestamp;

                // Check for any potential issues
                if (!this.waveTimestamps.TryGetValue(waveName, out wavTimestamp))
                {
                    this.errors["Missing Waves"].Add(depotPath);
                }
                else
                {
                    uint animTimestamp_i;
                    if (!uint.TryParse(timestamp, out animTimestamp_i))
                    {
                        this.errors["General Errors"].Add("Failed to parse timestamp for custom anim file: " + depotPath);
                    }

                    uint wavTimestamp_i;
                    if (!uint.TryParse(wavTimestamp, out wavTimestamp_i))
                    {
                        this.errors["General Errors"].Add("Failed to parse timestamp for wave file: " + waveName);
                    }

                    if (animTimestamp_i < wavTimestamp_i)
                    {
                        this.errors["Out of Date Custom Anim Files"].Add(depotPath);
                    }
                }
            }
        }

        /// <summary>
        /// Display the results to the user.
        /// </summary>
        private void DisplayResults()
        {
            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Old Lipsync Anim Files</title></head><body>");

            // Output errors to file
            foreach (var errorList in this.errors)
            {
                sw.WriteLine("<h2>" + errorList.Key + "</h2>");

                if (errorList.Value.Any())
                {
                    sw.WriteLine("<ul>");
                    foreach (var error in errorList.Value)
                    {
                        sw.WriteLine("<li>" + error + "</li>");
                    }

                    sw.WriteLine("</ul>");
                }
                else
                {
                    sw.WriteLine("<p>None</p>");
                }
            }

            sw.WriteLine("</body></html>");

            sw.Close();
            filesteam.Close();

            // Display report
            var report = new frmReport(this.name + " Report");
            report.SetURL(resultsFileName);
            report.Show();
        }
    }
}
