namespace Rave.Plugins
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Xml;

    using Rave.Instance;
    using Rave.Plugins.Infrastructure.Interfaces;

    public class PluginManager : IPluginManager
    {
        private readonly List<IRAVEEditorPlugin> m_editorPlugins;
        private readonly List<IRAVEObjectBrowserPlugin> m_objectBrowserPlugins;
        private readonly Dictionary<string, Type> m_objectEditorPlugins;
        private readonly Dictionary<string, XmlNode> m_objectEditorPluginXml;
        private readonly List<IRAVEReportPlugin> m_reportPlugins;
        private readonly List<IRAVEWaveBrowserPlugin> m_waveBrowserPlugins;
        private readonly List<IRAVEWaveImportPlugin> m_waveImportPlugins;
        private readonly List<IRAVESpeechSearchPlugin> m_speechSearchPlugins;

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginManager"/> class.
        /// </summary>
        /// <param name="pluginNodes">
        /// The plugins.
        /// </param>
        public PluginManager(IEnumerable<XmlNode> pluginNodes)
        {
            this.m_editorPlugins = new List<IRAVEEditorPlugin>();
            this.m_objectEditorPlugins = new Dictionary<string, Type>();
            this.m_objectEditorPluginXml = new Dictionary<string, XmlNode>();
            this.m_objectBrowserPlugins = new List<IRAVEObjectBrowserPlugin>();
            this.m_reportPlugins = new List<IRAVEReportPlugin>();
            this.m_waveBrowserPlugins = new List<IRAVEWaveBrowserPlugin>();
            this.m_waveImportPlugins = new List<IRAVEWaveImportPlugin>();
            this.m_speechSearchPlugins = new List<IRAVESpeechSearchPlugin>();

            foreach (var pluginNode in pluginNodes)
            {
                var depotFilePath = pluginNode.Attributes["path"].Value;
                var filePath = RaveInstance.AssetManager.GetLocalPath(depotFilePath).ToUpper();
                if (!File.Exists(filePath) || string.Compare(Path.GetExtension(filePath), ".DLL") != 0)
                {
                    continue;
                }

                var assembly = Assembly.LoadFile(filePath);
                if (assembly == null)
                {
                    continue;
                }

                foreach (var type in assembly.GetExportedTypes())
                {
                    if (!type.IsClass || type.IsAbstract || !ImplementsInterface<IRAVEPlugin>(type))
                    {
                        // we are only looking for non-abstract classes which implement IRAVEPlugin
                        continue;
                    }

                    var plugin = CreateAndInitialize<IRAVEPlugin>(pluginNode, type);
                    if (null == plugin)
                    {
                        continue;
                    }

                    if (ImplementsInterface<IRAVEWaveImportPlugin>(type))
                    {
                        this.m_waveImportPlugins.Add(plugin as IRAVEWaveImportPlugin);
                    }

                    if (ImplementsInterface<IRAVEWaveBrowserPlugin>(type))
                    {
                        this.m_waveBrowserPlugins.Add(plugin as IRAVEWaveBrowserPlugin);
                    }

                    if (ImplementsInterface<IRAVESpeechSearchPlugin>(type))
                    {
                        this.m_speechSearchPlugins.Add(plugin as IRAVESpeechSearchPlugin);
                    }

                    if (ImplementsInterface<IRAVEReportPlugin>(type))
                    {
                        this.m_reportPlugins.Add(plugin as IRAVEReportPlugin);
                    }

                    if (ImplementsInterface<IRAVEEditorPlugin>(type))
                    {
                        this.m_editorPlugins.Add(plugin as IRAVEEditorPlugin);
                    }

                    if (ImplementsInterface<IRAVEObjectBrowserPlugin>(type))
                    {
                        this.m_objectBrowserPlugins.Add(plugin as IRAVEObjectBrowserPlugin);
                    }

                    if (ImplementsInterface<IRAVEObjectEditorPlugin>(type))
                    {
                        var objectEditorPlugin = plugin as IRAVEObjectEditorPlugin;
                        foreach (string objectType in objectEditorPlugin.ObjectType.Split('/'))
                        {
                            this.m_objectEditorPlugins[objectType.ToUpper()] = type;
                            this.m_objectEditorPluginXml[objectType.ToUpper()] = pluginNode;
                        }
                    }
                }
            }
        }

        public IList<IRAVEWaveImportPlugin> WaveImportPlugins
        {
            get { return this.m_waveImportPlugins.AsReadOnly(); }
        }

        public IList<IRAVEWaveBrowserPlugin> WaveBrowserPlugins
        {
            get { return this.m_waveBrowserPlugins.AsReadOnly(); }
        }

        public IList<IRAVESpeechSearchPlugin> SpeechSearchPlugins
        {
            get { return this.m_speechSearchPlugins.AsReadOnly(); }
        }

        public IList<IRAVEReportPlugin> ReportPlugins
        {
            get { return this.m_reportPlugins.AsReadOnly(); }
        }

        public IList<IRAVEEditorPlugin> EditorPlugins
        {
            get { return this.m_editorPlugins.AsReadOnly(); }
        }

        public IList<IRAVEObjectBrowserPlugin> ObjectBrowserPlugins
        {
            get { return this.m_objectBrowserPlugins.AsReadOnly(); }
        }

        public IRAVEObjectEditorPlugin GetObjectEditorPlugin(string objectType)
        {
            /*
             * For Testing Purpuses use this
             * 
                        if (objectType.Equals("SoundType", StringComparison.CurrentCultureIgnoreCase))
                        {
                            SoundTypeView  view = new  SoundTypeView();
                            view.Init(m_objectEditorPluginXml[objectType]);
                            return view;
                        }
            */
            if (this.m_objectEditorPlugins.ContainsKey(objectType))
            {
                return CreateAndInitialize<IRAVEObjectEditorPlugin>(this.m_objectEditorPluginXml[objectType], this.m_objectEditorPlugins[objectType]);
            }
            return null;
        }

        private static T CreateAndInitialize<T>(XmlNode pluginNode, Type type) where T : class, IRAVEPlugin
        {
            var plugin = Activator.CreateInstance(type) as T;
            if (null != plugin && plugin.Init(pluginNode))
            {
                return plugin;
            }

            return null;
        }

        private static bool ImplementsInterface<T>(Type type)
        {
            return type.FindInterfaces((x, y) => x.TypeHandle.Equals(typeof(T).TypeHandle), null).Length > 0;
        }
    }
}