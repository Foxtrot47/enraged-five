﻿using System;
using System.IO;
using System.Text;
using Rockstar.AssetManager.Interfaces;

namespace ProjectLoader {
    class Logger: ILogger {

        private String filePath;

        public Logger(string filePath) {
            this.filePath = filePath;
            if(File.Exists(filePath)) {
                File.Delete(filePath); 
            }
        }

        StringBuilder tempLogContent = new StringBuilder();

        public void writeLine(string line) {
            File.AppendAllText(filePath, line+Environment.NewLine);
        }
    }
}
