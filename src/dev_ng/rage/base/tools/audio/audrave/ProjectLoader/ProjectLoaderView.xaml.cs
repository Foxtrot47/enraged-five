﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using rage;

namespace ProjectLoader
{
    /// <summary>
    /// Interaction logic for ProjectLoaderView.xaml
    /// </summary>
    public partial class ProjectLoaderView
    {
        public ProjectLoaderView(ProjectLoaderViewModel projectList)
        {
            InitializeComponent();
            this.DataContext = projectList;
        }

        private void PasswordChanged(object sender, RoutedEventArgs e)
        {

            if (this.DataContext is ProjectLoaderViewModel)
            { (this.DataContext as ProjectLoaderViewModel).Project.Password = ((PasswordBox)sender).Password; }

        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = e.Source as ComboBox;

            ProjectLoaderViewModel plvm = this.DataContext as ProjectLoaderViewModel;
            if (comboBox != null && plvm != null)
            {
                plvm.Project = comboBox.SelectedItem as audProjectEntry;
            }
        }
    }
}
