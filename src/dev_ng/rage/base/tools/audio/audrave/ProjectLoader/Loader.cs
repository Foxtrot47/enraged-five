﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage;
using Rockstar.AssetManager.Interfaces;

namespace ProjectLoader
{
    public class Loader
    {

        private readonly ProjectLoaderViewModel _viewModel;
        private readonly string _autoSelectedProject;

        public Loader(string projectList, string autoSelectedProject)
        {

            audProjectList projectListObject = new audProjectList(projectList);
            _autoSelectedProject = autoSelectedProject;
            _viewModel = new ProjectLoaderViewModel(projectListObject);
            if (!string.IsNullOrEmpty(autoSelectedProject))
            {
                _viewModel.Project =
                    projectListObject.Projects.FirstOrDefault(
                        p => p.Name.Equals(autoSelectedProject, StringComparison.InvariantCultureIgnoreCase));
            }
            if (_viewModel.Project == null)
            {
                _viewModel.Project = projectListObject.Projects[0];
            }
        }

        public void ShowDialog()
        {
            ProjectLoaderView projectView = new ProjectLoaderView(_viewModel);
            projectView.ShowDialog();
        }

        public void CheckAutoSelectOrShow()
        {
            if ((string.IsNullOrEmpty(_autoSelectedProject) &&
                         (this.Project == null || string.IsNullOrEmpty(this.Project.Password)))
                        ||
                        (!string.IsNullOrEmpty(_autoSelectedProject) &&
                         String.Compare(this.Project.Name, _autoSelectedProject, StringComparison.OrdinalIgnoreCase) != 0))
            {
                ShowDialog();
            }
            else
            {
                _viewModel.IsCancelled = false;
            }
        }

        public bool IsCancelled
        {
            get { return _viewModel.IsCancelled; }

        }

        public IAssetManager AssetManager { get { return _viewModel.AssetManager; } }
        public audProjectEntry Project { get { return _viewModel.Project; } }
    }
}
