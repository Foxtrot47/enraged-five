﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ProjectLoader.ValidationRules
{
    public class ProjectSettingsValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string text = value as string;

            if (string.IsNullOrEmpty(text) ||
                !text.ToUpper().EndsWith(".XML"))
            {

                return new ValidationResult(false, "Project Settings must end with xml");
            }

            return new ValidationResult(true, "Project settings is valid");
        }
    }
}
