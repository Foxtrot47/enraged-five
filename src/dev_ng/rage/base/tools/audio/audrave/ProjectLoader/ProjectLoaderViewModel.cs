﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ProjectLoader.Annotations;
using rage;
using Rockstar.AssetManager.Interfaces;
using GalaSoft.MvvmLight.Command;
using NLog;
using Rockstar.AssetManager.Infrastructure.Enums;
using Rockstar.AssetManager.Main;
using RelayCommand = GalaSoft.MvvmLight.Command.RelayCommand;

namespace ProjectLoader
{
    public class ProjectLoaderViewModel : INotifyPropertyChanged
    {
        private IAssetManager _assetManager;
        private audProjectEntry _currentProject;

        public IAssetManager AssetManager
        {
            get { return _assetManager; }
        }

        public bool IsCancelled = true;
        private audProjectList _projects;

        public audProjectEntry Project
        {
            get { return _currentProject; }
            set
            {
                if (_currentProject != null)
                {
                    _currentProject.PropertyChanged -= OnProjectPropertyChanged;
                }
                _currentProject = value;
                ValidateAndCloseCommand.RaiseCanExecuteChanged();
                _currentProject.PropertyChanged += OnProjectPropertyChanged;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Project"));
                }
            }
        }

        private void OnProjectPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ValidateAndCloseCommand.RaiseCanExecuteChanged();
        }


        public RelayCommand<Window> ValidateAndCloseCommand { get; private set; }
        public RelayCommand<Window> CloseCommand { get; private set; }

        public RelayCommand NewCommand { get; private set; }

        public audProjectList Projects
        {
            get { return _projects; }
            set
            {
                _projects = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Projects"));
                }
            }
        }

        public ProjectLoaderViewModel(audProjectList projects)
        {
            CloseCommand = new RelayCommand<Window>(p => { IsCancelled = true; p.Close(); });
            ValidateAndCloseCommand = new RelayCommand<Window>(ValidateAndClose, IsValid);
            NewCommand = new RelayCommand(NewProject);
            Projects = projects;
          
        }

        private void NewProject()
        {
            if (Project != null)
            {
                Project = Projects.DuplicateProject(Project);
            }
            else
            {
                Project = Projects.CreateProject();
            }
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs("Projects")); }
        }

        private void ValidateAndClose(Window window)
        {
            ValidateAndCloseCommand.RaiseCanExecuteChanged();
            if (IsValid(window))
            {
                CreateAssetManager();
                if (AssetManager != null)
                {
                    IsCancelled = false;
                    Projects.Save();
                    window.Close();

                }
            }
        }


        private bool IsValid(DependencyObject obj)
        {

            return !Validation.GetHasError(obj) &&
            LogicalTreeHelper.GetChildren(obj)
            .OfType<DependencyObject>()
            .All(IsValid);
        }

        private void CreateAssetManager()
        {
            try
            {
                _assetManager = AssetManagerFactory.GetInstance(
                    AssetManagerType.Perforce,
                    null,
                    Project.AssetServer,
                    Project.AssetProject,
                    Project.UserName,
                    Project.Password,
                    Project.DepotRoot,
                    new Logger(
                                string.Concat(
                                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                @"\Rave\P4SessionLog.log")),
                    "Rave Audio Rockstar");

            }
            catch (Exception e)
            {
                if (null != _assetManager)
                {
                    try
                    {
                        _assetManager.Disconnect();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }

                MessageBox.Show(e.Message);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


    }
}
