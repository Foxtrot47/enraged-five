namespace Rave.ObjectBrowser
{
    using Rave.TemplateBrowser;

    partial class ctrlBrowserWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ctrlObjectBrowser1 = new ctrlObjectBrowser();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.m_ObjectBrowser = new ctrlObjectBrowser();
            this.m_TemplateBrowser = new ctrlTemplateBrowser();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ctrlObjectBrowser1
            // 
            this.ctrlObjectBrowser1.Location = new System.Drawing.Point(0, 0);
            this.ctrlObjectBrowser1.Name = "ctrlObjectBrowser1";
            this.ctrlObjectBrowser1.Size = new System.Drawing.Size(360, 442);
            this.ctrlObjectBrowser1.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.m_ObjectBrowser);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.m_TemplateBrowser);
            this.splitContainer1.Size = new System.Drawing.Size(354, 587);
            this.splitContainer1.SplitterDistance = 414;
            this.splitContainer1.TabIndex = 0;
            // 
            // m_ObjectBrowser
            // 
            this.m_ObjectBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ObjectBrowser.Location = new System.Drawing.Point(0, 0);
            this.m_ObjectBrowser.Name = "m_ObjectBrowser";
            this.m_ObjectBrowser.Size = new System.Drawing.Size(354, 414);
            this.m_ObjectBrowser.TabIndex = 0;
            // 
            // m_TemplateBrowser
            // 
            this.m_TemplateBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_TemplateBrowser.Location = new System.Drawing.Point(0, 0);
            this.m_TemplateBrowser.Name = "m_TemplateBrowser";
            this.m_TemplateBrowser.Size = new System.Drawing.Size(354, 169);
            this.m_TemplateBrowser.TabIndex = 0;
            // 
            // ctrlBrowserWindow
            // 
            this.Controls.Add(this.splitContainer1);
            this.Name = "BrowserWindow";
            this.Size = new System.Drawing.Size(354, 587);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ctrlObjectBrowser ctrlObjectBrowser1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private ctrlObjectBrowser m_ObjectBrowser;
        private ctrlTemplateBrowser m_TemplateBrowser;
    }
}
