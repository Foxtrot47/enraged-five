// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ctrlObjectBrowser.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Summary description for ctrlSoundBrowser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using Rave.ObjectBrowser.Functionality;

namespace Rave.ObjectBrowser
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Windows.Forms;
    using System.Xml;
    using System.Xml.Linq;

    using rage;
    using rage.ToolLib;

    using Rave.AssetBrowser;
    using Rave.Controls.Forms;
    using Rave.Controls.Forms.Popups;
    using Rave.Controls.Forms.Search;
    using Rave.Instance;
    using Rave.Metadata;
    using Rave.ObjectBrowser.Infrastructure.Interfaces;
    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.PropertiesEditor.Editors;
    using Rave.PropertiesEditor.Popups;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types;
    using Rave.Types.Infrastructure.Enums;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;
    using Rave.Types.Infrastructure.Structs;
    using Rave.Types.ObjectBanks.Objects;
    using Rave.Types.Objects;
    using Rave.Types.Objects.Templates;
    using Rave.Types.Objects.TypedObjects;
    using Rave.Utils;
    using Rave.Utils.Popups;
    using Rave.Utils.Reports;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    using Rockstar.AssetManager.Interfaces;
    using Rave.MuteSoloManager;

    using Template = Rave.Types.Objects.Templates.Template;
    using Utility = rage.ToolLib.Utility;
    using Rave.MuteSoloManager.Model;

    /// <summary>
    /// Summary description for ctrlSoundBrowser.
    /// </summary>
    public class ctrlObjectBrowser : DockableWFControl, IObjectBrowser
    {
        #region Fields

        /// <summary>
        /// The m_node state.
        /// </summary>
        private readonly XmlDocument m_nodeState;

        /// <summary>
        /// The btn collapse.
        /// </summary>
        private ToolBarButton btnCollapse;

        /// <summary>
        /// The btn copy.
        /// </summary>
        private ToolBarButton btnCopy;

        /// <summary>
        /// The btn delete.
        /// </summary>
        private ToolBarButton btnDelete;

        /// <summary>
        /// The btn expand folders.
        /// </summary>
        private ToolBarButton btnExpandFolders;

        /// <summary>
        /// The btn rename.
        /// </summary>
        private ToolBarButton btnRename;

        /// <summary>
        /// The components.
        /// </summary>
        private IContainer components;

        /// <summary>
        /// The icon list.
        /// </summary>
        private ImageList iconList;

        /// <summary>
        /// The m_collapse.
        /// </summary>
        private bool m_collapse;

        /// <summary>
        /// The m_finished.
        /// </summary>
        private bool m_finished;

        /// <summary>
        /// The m_object type icon table.
        /// </summary>
        private Hashtable m_objectTypeIconTable;

        /// <summary>
        /// The m_toolbar.
        /// </summary>
        private ToolBar m_toolbar;

        /// <summary>
        /// The m_tree view.
        /// </summary>
        private ScrollableTreeView m_treeView;

        /// <summary>
        /// The m_type.
        /// </summary>
        private string m_type;

        /// <summary>
        /// The m_type definitions.
        /// </summary>
        private ITypeDefinitions m_typeDefinitions;

        /// <summary>
        /// The menu item 4.
        /// </summary>
        private ToolStripSeparator menuItem4;

        /// <summary>
        /// The mnu audition.
        /// </summary>
        private ToolStripMenuItem mnuAudition;

        /// <summary>
        /// The mnu batch create.
        /// </summary>
        private ToolStripMenuItem mnuBatchCreate;

        /// <summary>
        /// The mnu check out.
        /// </summary>
        private ToolStripMenuItem mnuCheckOut;

        /// <summary>
        /// The mnu copy base settings.
        /// </summary>
        private ToolStripMenuItem mnuCopyBaseSettings;

        /// <summary>
        /// The mnu copy name.
        /// </summary>
        private ToolStripMenuItem mnuCopyName;

        /// <summary>
        /// The mnu copy path.
        /// </summary>
        private ToolStripMenuItem mnuCopyPath;

        /// <summary>
        /// The mnu delete.
        /// </summary>
        private ToolStripMenuItem mnuDelete;

        /// <summary>
        /// The insert context sound menu item.
        ///</summary>
        /// <summary>
        private ToolStripMenuItem mnuInsertContext;

        /// The mnu dup sound.
        /// </summary>
        private ToolStripMenuItem mnuDupSound;

        /// <summary>
        /// The mnu dup tree.
        /// </summary>
        private ToolStripMenuItem mnuDupTree;

        /// <summary>
        /// The mnu expand.
        /// </summary>
        private ToolStripMenuItem mnuExpand;

        /// <summary>
        /// The mnu expand checked out.
        /// </summary>
        private ToolStripMenuItem mnuExpandCheckedOut;

        /// <summary>
        /// The mnu expand object stores.
        /// </summary>
        private ToolStripMenuItem mnuExpandObjectStores;

        /// <summary>
        /// The mnu expand xml.
        /// </summary>
        private ToolStripMenuItem mnuExpandXml;

        /// <summary>
        /// The mnu get latest.
        /// </summary>
        private ToolStripMenuItem mnuGetLatest;

        /// <summary>
        /// The mnu global.
        /// </summary>
        private ToolStripMenuItem mnuGlobal;

        /// <summary>
        /// The mnu grid edit.
        /// </summary>
        private ToolStripMenuItem mnuGridEdit;

        /// <summary>
        /// The mnu local check out.
        /// </summary>
        private ToolStripMenuItem mnuLocalCheckOut;

        /// <summary>
        /// The mnu model.
        /// </summary>
        private ToolStripMenuItem mnuModel;

        /// <summary>
        /// The mnu multi edit.
        /// </summary>
        private ToolStripMenuItem mnuMultiEdit;

        /// <summary>
        /// The mnu new folder.
        /// </summary>
        private ToolStripMenuItem mnuNewFolder;

        /// <summary>
        /// The mnu new sound.
        /// </summary>
        private ToolStripMenuItem mnuNewSound;

        /// <summary>
        /// The mnu new sound bank.
        /// </summary>
        private ToolStripMenuItem mnuNewSoundBank;

        /// <summary>
        /// The mnu paste base settings.
        /// </summary>
        private ToolStripMenuItem mnuPasteBaseSettings;

        /// <summary>
        /// The mnu plugins.
        /// </summary>
        private ToolStripMenuItem mnuPlugins;

        /// <summary>
        /// The mnu populate missing fields.
        /// </summary>
        private ToolStripMenuItem mnuPopulateMissingFields;

        /// <summary>
        /// The mnu reload.
        /// </summary>
        private ToolStripMenuItem mnuReload;

        /// <summary>
        /// The mnu reload checked in.
        /// </summary>
        private ToolStripMenuItem mnuReloadCheckedIn;

        /// <summary>
        /// The mnu reload saved.
        /// </summary>
        private ToolStripMenuItem mnuReloadSaved;

        /// <summary>
        /// The mnu rename.
        /// </summary>
        private ToolStripMenuItem mnuRename;

        /// <summary>
        /// The mnu revert.
        /// </summary>
        private ToolStripMenuItem mnuRevert;

        /// <summary>
        /// The mnu right click.
        /// </summary>
        private ContextMenuStrip mnuRightClick;

        /// <summary>
        /// The mnu save.
        /// </summary>
        private ToolStripMenuItem mnuSave;

        /// <summary>
        /// The mnu submit.
        /// </summary>
        private ToolStripMenuItem mnuSubmit;

        /// <summary>
        /// The mnu search in store.
        /// </summary>
        private ToolStripMenuItem mnuSearchInStore;

        /// <summary>
        /// The mnu separator 4.
        /// </summary>
        private ToolStripSeparator mnuSeparator4;

        /// <summary>
        /// The mnu seperator 2.
        /// </summary>
        private ToolStripSeparator mnuSeperator2;

        /// <summary>
        /// The mnu seperator 3.
        /// </summary>
        private ToolStripSeparator mnuSeperator3;

        /// <summary>
        /// The mnu sound set report.
        /// </summary>
        private ToolStripMenuItem mnuSoundSetReport;

        /// <summary>
        /// The mnu invalid refs report.
        /// </summary>
        private ToolStripMenuItem mnuInvalidRefsReport;

        /// <summary>
        /// The mnu templates.
        /// </summary>
        private ToolStripMenuItem mnuTemplates;

        /// <summary>
        /// The mnu view hierarchy.
        /// </summary>
        private ToolStripMenuItem mnuViewHierarchy;

        /// <summary>
        /// The mnu view in P4.
        /// </summary>
        private ToolStripMenuItem mnuViewInP4;

        private ToolStripMenuItem mnuViewInExplorer;


        private ToolStripMenuItem mnuMute;

        private ToolStripMenuItem mnuSolo;

        /// <summary>
        /// The object bank manager.
        /// </summary>
        private IXmlBankManager xmlBankManager;


        /// <summary>
        /// The tool bar icons.
        /// </summary>
        private ImageList toolBarIcons;


        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="ctrlObjectBrowser"/> class.
        /// </summary>
        static ctrlObjectBrowser()
        {
            BusyText = "Working...";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ctrlObjectBrowser"/> class.
        /// </summary>
        public ctrlObjectBrowser()
        {
            // This call is required by the Windows.Forms Form Designer.
            this.InitializeComponent();
            this.m_objectTypeIconTable = new Hashtable();
            this.LoadDefaultIcons();
            TypeUtils.NewObjectCreated += this.Utility_NewObjectCreated;
            this.m_nodeState = new XmlDocument();
            this.m_nodeState.AppendChild(this.m_nodeState.CreateElement("State"));
            MuteSoloManager.Instance.MutedObjectsChanged -= MuteSoloObjects_CollectionChanged;
            MuteSoloManager.Instance.SoloedObjectsChanged -= MuteSoloObjects_CollectionChanged;
            MuteSoloManager.Instance.MutedObjectsChanged += MuteSoloObjects_CollectionChanged;
            MuteSoloManager.Instance.SoloedObjectsChanged += MuteSoloObjects_CollectionChanged;

        }

        #endregion

        #region Public Events

        /// <summary>
        /// The audition sound.
        /// </summary>
        public event Action<IObjectInstance> AuditionSound;

        /// <summary>
        /// The changelist modified.
        /// </summary>
        public event Action ChangelistModified;

        /// <summary>
        /// The delete object.
        /// </summary>
        public event Action<IObjectInstance> DeleteObject;

        /// <summary>
        /// The on sound select.
        /// </summary>
        public event Action<IObjectInstance> OnSoundSelect;

        /// <summary>
        /// The stop sound audition.
        /// </summary>
        public event Action<IObjectInstance> StopSoundAudition;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether busy known progress.
        /// </summary>
        public static bool BusyKnownProgress { get; set; }

        /// <summary>
        /// Gets or sets the busy progress.
        /// </summary>
        public static int BusyProgress { get; set; }

        /// <summary>
        /// Gets or sets the busy text.
        /// </summary>
        public static string BusyText { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether allow multi checkout.
        /// </summary>
        public bool AllowMultiCheckout { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can audition.
        /// </summary>
        public bool CanAudition { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can view hierarchy.
        /// </summary>
        public bool CanViewHierarchy { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The find object.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        public void FindObject(IObjectInstance obj)
        {
            if (obj == null)
            {
                return;
            }
            this.Content.Show();
            this.Content.IsSelected = true;
            SoundNode sn = FindObjectNodeWithObjectName(obj.Episode, obj.Bank.Name, obj.Name, this.m_treeView.Nodes);
            if (sn != null)
            {
                this.m_treeView.BeginUpdate();
                sn.EnsureVisible();
                this.m_treeView.LastSelectedNode = sn;
                this.m_treeView.EndUpdate();
            }
        }

        public void FindObject(string name, string episode)
        {
            this.Content.Show();
            this.Content.IsSelected = true;
            SoundNode sn = FindObjectNodeWithObjectName(name, episode, this.m_treeView.Nodes);
            if (sn != null)
            {
                this.m_treeView.BeginUpdate();
                sn.EnsureVisible();
                this.m_treeView.LastSelectedNode = sn;
                this.m_treeView.EndUpdate();
            }
        }

        /// <summary>
        /// The find object bank.
        /// </summary>
        /// <param name="objectBank">
        /// The object bank.
        /// </param>
        public void FindObjectBank(IObjectBank objectBank)
        {
            SoundBankNode soundBankNode = FindObjectBankNode(objectBank, this.m_treeView.Nodes);
            if (soundBankNode != null)
            {
                this.m_treeView.BeginUpdate();
                soundBankNode.EnsureVisible();
                this.m_treeView.LastSelectedNode = soundBankNode;
                this.m_treeView.EndUpdate();
            }
        }

        private void MuteSoloObjects_CollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            List<SoundNode> soundNodeList = new List<SoundNode>();
            IEnumerable<MuteSoloObject> oldItems = new List<MuteSoloObject>();

            if (e.OldItems != null)
            {
                oldItems = e.OldItems.Cast<MuteSoloObject>().Except<MuteSoloObject>(e.NewItems.Cast<MuteSoloObject>());



                foreach (MuteSoloObject msObject in oldItems)
                {
                    FindObjectNodeWithMuteSolo((MuteSoloObject)msObject, this.m_treeView.Nodes, soundNodeList);
                }
            }

            if (e.NewItems != null)
            {
                IEnumerable<MuteSoloObject> newItems;
                if (e.OldItems != null)
                {
                    newItems = e.NewItems.Cast<MuteSoloObject>().Except<MuteSoloObject>(e.OldItems.Cast<MuteSoloObject>());
                }
                else
                {
                    newItems = e.NewItems.Cast<MuteSoloObject>();
                }

                foreach (MuteSoloObject msObject in newItems)
                {
                    if (msObject is MuteSoloObject)
                    {
                        FindObjectNodeWithMuteSolo((MuteSoloObject)msObject, this.m_treeView.Nodes, soundNodeList);
                    }
                }
            }
            if (soundNodeList.Count > 10)
            {
                string nodeText = this.m_treeView.LastSelectedNode.Text;
                this.RefreshTree();
                TreeNode tn = FindTreeNode(this.m_treeView, nodeText);
                tn.EnsureVisible();
                tn.Expand();
                m_treeView.LastSelectedNode = tn;
            }
            else
            {
                foreach (SoundNode sn in soundNodeList)
                {
                    UpdateFont(sn);
                }
            }
        }

        private void UpdateFont(SoundNode sn)
        {
            sn.SetFont();
            sn.Text = sn.Text;
        }


        /// <summary>
        /// The get tree view.
        /// </summary>
        /// <returns>
        /// The <see cref="TreeView"/>.
        /// </returns>
        public TreeView GetTreeView()
        {
            return this.m_treeView;
        }

        /// <summary>
        /// Gets the type
        /// </summary>
        public string GetObjectBrowserType()
        {
            return m_type;
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="bankManager">
        /// The bank manager.
        /// </param>
        /// <param name="typeDefinitions">
        /// The type definitions.
        /// </param>
        public void Init(
            string type,
            string path,
            IXmlBankManager bankManager,
            ITypeDefinitions typeDefinitions)
        {
            this.m_typeDefinitions = typeDefinitions;
            this.Text = type + " Browser";
            this.xmlBankManager = bankManager;
            this.m_type = type;

            this.LoadMetadataTypeIcons();

            this.RefreshTree();
            RaveUtils.CreateObjectMenus(typeDefinitions, this.mnuNewSoundType_Click, this.mnuNewSound);
            this.mnuTemplates = new ToolStripMenuItem("Template ObjectNameBox");
            this.mnuNewSound.DropDownItems.Add(this.mnuTemplates);
            RaveUtils.CreateObjectMenus(typeDefinitions, this.mnuBatchCreate_Click, this.mnuBatchCreate);

            if (RaveInstance.PluginManager != null)
            {
                foreach (IRAVEObjectBrowserPlugin plugin in RaveInstance.PluginManager.ObjectBrowserPlugins)
                {
                    this.mnuPlugins.DropDownItems.Add(
                        new ToolStripMenuItem(plugin.GetName(), null, this.mnuPlugin_Click));
                }
            }
        }

        /// <summary>
        /// The show hierarchy.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        public void ShowHierarchy(SoundNode node)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<SoundNode>(this.ShowHierarchy), new object[] { node });
            }
            else
            {
                if (node != null)
                {
                    if (this.OnSoundSelect != null)
                    {
                        this.OnSoundSelect(node.ObjectInstance);
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// The are objects of same type.
        /// </summary>
        /// <param name="objects">
        /// The objects.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool AreObjectsOfSameType(List<IObjectInstance> objects)
        {
            bool result = true;

            if (objects.Count > 0)
            {
                string typeName = objects[0].TypeName;
                for (int i = 1; i < objects.Count; i++)
                {
                    if (objects[i].TypeName != typeName)
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// The build node list.
        /// </summary>
        /// <param name="nodeList">
        /// The node list.
        /// </param>
        /// <param name="soundBankLookup">
        /// The sound bank lookup.
        /// </param>
        /// <returns>
        /// The <see cref="List{T}"/>.
        /// </returns>
        private static List<SoundNode> BuildNodeList(
            List<SoundNode> nodeList, Dictionary<SoundNode, SoundBankNode> soundBankLookup)
        {
            var newNodeList = new List<SoundNode>();
            for (int i = 0; i < nodeList.Count; ++i)
            {
                SoundBankNode nodeSoundBank = soundBankLookup[nodeList[i]];
                SoundNode newNode = null;
                foreach (object node in nodeSoundBank.Nodes)
                {
                    if (node is VirtualFolderNode)
                    {
                        var virtualFolderNode = node as VirtualFolderNode;
                        foreach (SoundNode soundNode in virtualFolderNode.Nodes)
                        {
                            if (soundNode.ObjectName == nodeList[i].ObjectName)
                            {
                                newNode = soundNode;
                                break;
                            }
                        }

                        if (newNode != null)
                        {
                            break;
                        }
                    }
                    else
                    {
                        var soundNode = node as SoundNode;
                        if (soundNode != null && soundNode.ObjectName == nodeList[i].ObjectName)
                        {
                            newNode = soundNode;
                            break;
                        }
                    }
                }

                newNodeList.Add(newNode);
            }

            return newNodeList;
        }

        /// <summary>
        /// The check for dup tree name conflict.
        /// </summary>
        /// <param name="parentObj">
        /// The parent obj.
        /// </param>
        /// <param name="baseName">
        /// The base name.
        /// </param>
        /// <param name="newBaseName">
        /// The new base name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool CheckForDupTreeNameConflict(IObjectInstance parentObj, string baseName, string newBaseName)
        {
            string newName;
            if (parentObj.Name.Contains(baseName))
            {
                newName = parentObj.Name.Replace(baseName, newBaseName);
            }
            else
            {
                newName = newBaseName + "_" + parentObj.Name;
            }

            if (parentObj.ObjectLookup.ContainsKey(newName))
            {
                ErrorManager.HandleInfo("An object already exists named " + newName);
                return false;
            }

            return parentObj.References.All(
                or => CheckForDupTreeNameConflict(or.ObjectInstance, baseName, newBaseName));
        }

        /// <summary>
        /// The expand checked out stores.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private static void ExpandCheckedOutStores(TreeNode node)
        {
            var sbn = node as SoundBankNode;
            if (sbn != null)
            {
                if (sbn.ObjectBank.IsCheckedOut)
                {
                    sbn.EnsureVisible();
                }
            }

            foreach (TreeNode n in node.Nodes)
            {
                ExpandCheckedOutStores(n);
            }
        }

        /// <summary>
        /// The expand tree.
        /// </summary>
        /// <param name="nodeState">
        /// The node state.
        /// </param>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private static void ExpandTree(XmlNode nodeState, TreeNodeCollection treeNodes)
        {
            foreach (XmlNode state in nodeState.ChildNodes)
            {
                foreach (TreeNode treeNode in treeNodes)
                {
                    string name = Regex.Replace(treeNode.Text, @"[^\w-]+", String.Empty);
                    if (state.Name == name)
                    {
                        if (state.HasChildNodes)
                        {
                            treeNode.Expand();
                            ExpandTree(state, treeNode.Nodes);
                        }

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// The find object bank node.
        /// </summary>
        /// <param name="xmlBank">
        /// The object bank.
        /// </param>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <returns>
        /// The <see cref="SoundBankNode"/>.
        /// </returns>
        private static SoundBankNode FindObjectBankNode(IObjectBank xmlBank, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                var soundBankNode = node as SoundBankNode;
                if (soundBankNode != null && soundBankNode.ObjectBank == xmlBank)
                {
                    return soundBankNode;
                }

                soundBankNode = FindObjectBankNode(xmlBank, node.Nodes);
                if (soundBankNode != null && soundBankNode.ObjectBank == xmlBank)
                {
                    return soundBankNode;
                }
            }

            return null;
        }

        /// <summary>
        /// The find sound node.
        /// </summary>
        /// <param name="sbn">
        /// The sbn.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="SoundNode"/>.
        /// </returns>
        internal static SoundNode FindSoundNode(SoundBankNode sbn, string name)
        {
            // search for the new node representing this sound, it'll have changed during the reload
            foreach (TreeNode tn in sbn.Nodes)
            {
                if (tn.GetType() == typeof(SoundNode))
                {
                    var s = (SoundNode)tn;
                    if (s.ObjectInstance.Name == name)
                    {
                        return s;
                    }
                }
                else
                {
                    foreach (TreeNode tn2 in tn.Nodes)
                    {
                        if (tn2.GetType() == typeof(SoundNode))
                        {
                            var s = (SoundNode)tn2;
                            if (s.ObjectInstance.Name == name)
                            {
                                return s;
                            }
                        }
                    }
                }
            }

            ErrorManager.HandleInfo("Object " + name + " is no longer in " + sbn.ObjectBank.Name);
            return null;
        }

        /// <summary>
        /// The find sound node with object.
        /// </summary>
        /// <param name="objName">
        /// The object name.
        /// </param>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <returns>
        /// The <see cref="SoundNode"/>.
        /// </returns>
        private static SoundNode FindObjectNodeWithObjectName(string episode, string bank, string objName, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                var soundNode = node as SoundNode;
                if (soundNode != null && soundNode.ObjectInstance.Name.Equals(objName)
                    && soundNode.ObjectInstance.Bank.Name.Equals(bank)
                    && soundNode.ObjectInstance.Bank.Episode.Equals(episode))
                {
                    return soundNode;
                }

                soundNode = FindObjectNodeWithObjectName(episode, bank, objName, node.Nodes);
                if (soundNode != null && soundNode.ObjectInstance.Name.Equals(objName)
                    && soundNode.ObjectInstance.Bank.Name.Equals(bank)
                    && soundNode.ObjectInstance.Bank.Episode.Equals(episode))
                {
                    return soundNode;
                }
            }

            return null;
        }

        private static SoundNode FindObjectNodeWithObjectName(string name, string episode, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                SoundNode soundNode = node as SoundNode;
                if (soundNode != null && soundNode.ObjectInstance.Name.Equals(name) && soundNode.ObjectInstance.Episode.Equals(episode.Replace("_", string.Empty).Trim(), StringComparison.InvariantCultureIgnoreCase))
                {
                    return soundNode;
                }
                soundNode = FindObjectNodeWithObjectName(name, episode, node.Nodes);
                if (soundNode != null)
                {
                    return soundNode;
                }
            }
            return null;
        }

        private static SoundNode FindObjectNodeWithObjectName(string name, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                SoundNode soundNode = node as SoundNode;
                if (soundNode != null && soundNode.ObjectInstance.Name.Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase))
                {
                    return soundNode;
                }
                soundNode = FindObjectNodeWithObjectName(name, node.Nodes);
                if (soundNode != null)
                {
                    return soundNode;
                }
            }
            return null;
        }


        private static void FindObjectNodeWithMuteSolo(MuteSoloObject muteSolo, TreeNodeCollection nodes, List<SoundNode> soundNodeList)
        {
            foreach (TreeNode node in nodes)
            {
                SoundNode soundNode = node as SoundNode;
                if (soundNode != null && soundNode.ObjectInstance.Name.Equals(muteSolo.Name) && soundNode.ObjectInstance.TypeName.Equals(muteSolo.TypeName))
                {
                    soundNodeList.Add(soundNode);
                }
                FindObjectNodeWithMuteSolo(muteSolo, node.Nodes, soundNodeList);
            }

        }

        /// <summary>
        /// The find wave container node.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="WaveContainerNode"/>.
        /// </returns>
        private static WaveContainerNode FindWaveContainerNode(IDataObject data)
        {
            var types = new[] { typeof(BankFolderNode), typeof(BankNode), typeof(PackNode), typeof(WaveFolderNode) };
            return
                (from type in types where data.GetDataPresent(type) select (WaveContainerNode)data.GetData(type))
                    .FirstOrDefault();
        }

        /// <summary>
        /// Find first tree node with matching node text.
        /// </summary>
        /// <param name="tree">
        /// The tree.
        /// </param>
        /// <param name="nodeText">
        /// The node text.
        /// </param>
        /// <returns>
        /// The tree node <see cref="TreeNode"/>.
        /// </returns>
        public static TreeNode FindTreeNode(TreeView tree, string nodeText)
        {
            foreach (TreeNode node in tree.Nodes)
            {
                var foundNode = FindTreeNodeHelper(node, nodeText);
                if (null != foundNode)
                {
                    return foundNode;
                }
            }

            return null;
        }

        /// <summary>
        /// Helper method for finding first tree node with matching node text.
        /// </summary>
        /// <param name="root">
        /// The root.
        /// </param>
        /// <param name="nodeText">
        /// The node text.
        /// </param>
        /// <returns>
        /// The tree node <see cref="TreeNode"/>.
        /// </returns>
        private static TreeNode FindTreeNodeHelper(TreeNode root, string nodeText)
        {
            foreach (TreeNode node in root.Nodes)
            {
                if (node.Text == nodeText)
                {
                    return node;
                }

                var foundNode = FindTreeNodeHelper(node, nodeText);
                if (null != foundNode)
                {
                    return foundNode;
                }
            }

            return null;
        }

        /// <summary>
        /// Helper method for finding first tree node with matching node text.
        /// </summary>
        /// <param name="root">
        /// The root.
        /// </param>
        /// <param name="nodeText">
        /// The node text.
        /// </param>
        /// <param name="character"></param>
        /// <returns>
        /// The tree node <see cref="TreeNode"/>.
        /// </returns>
        private static TreeNode FindTreeNodeHelper(TreeNode root, char character)
        {
            if (root == null || root.Nodes == null)
            {
                return null;
            }
            foreach (TreeNode node in root.Nodes)
            {
                if (node.Text.First() == character || node.Text.First() == (char)(character - 32))
                {
                    return node;
                }

                var foundNode = FindTreeNodeHelper(node, character);
                if (null != foundNode)
                {
                    return foundNode;
                }
            }

            return null;
        }
        /// <summary>
        /// The make local on tree.
        /// </summary>
        /// <param name="parentObj">
        /// The parent obj.
        /// </param>
        /// <param name="baseName">
        /// The base name.
        /// </param>
        /// <param name="newBaseName">
        /// The new base name.
        /// </param>
        /// <param name="processed">
        /// The processed.
        /// </param>
        private static void MakeLocalOnTree(
            IObjectInstance parentObj, string baseName, string newBaseName, Hashtable processed)
        {
            var referencers = new List<ObjectRef>();
            referencers.AddRange(parentObj.References.ToArray());

            var virtualFolderName = parentObj.VirtualFolderName;
            if (virtualFolderName == null)
            {
                virtualFolderName = parentObj.Name + "_COMPONENTS";
            }

            foreach (var referencer in referencers)
            {
                // We do not want to attempt to change
                // references to objects of other metadata types
                if (referencer.ObjectInstance.TypeDefinitions != parentObj.TypeDefinitions)
                {
                    continue;
                }

                if (!processed.ContainsKey(referencer.ObjectInstance))
                {
                    string newName = String.Empty;
                    if (referencer.ObjectInstance.Name.Contains(baseName))
                    {
                        newName = referencer.ObjectInstance.Name.Replace(baseName, newBaseName);
                    }
                    else
                    {
                        newName = newBaseName + "_" + referencer.ObjectInstance.Name;
                    }

                    int i = 2;
                    string tempName = newName;
                    while (parentObj.ObjectLookup.ContainsKey(tempName))
                    {
                        tempName = newName + "_" + i;
                        i++;
                    }

                    newName = tempName;
                    string newFolderName = virtualFolderName;
                    if (referencer.ObjectInstance != null && !string.IsNullOrEmpty(referencer.ObjectInstance.VirtualFolderName))
                    {
                        string oldFolderName = referencer.ObjectInstance.VirtualFolderName;
                        if (oldFolderName.Contains(baseName))
                        {
                            newFolderName = oldFolderName.Replace(baseName, newBaseName);
                        }
                        else
                        {
                            newFolderName = string.Format("{0}_{1}", newBaseName, oldFolderName);
                        }
                    }

                    var s = TypeUtils.MakeReferenceLocal(referencer, parentObj, newName, newFolderName);
                    processed.Add(referencer.ObjectInstance, s);
                    MakeLocalOnTree(s, baseName, newBaseName, processed);
                }
                else
                {
                    // don't create a new object, use existing object instance
                    var s = (IObjectInstance)processed[referencer.ObjectInstance];
                    XmlNode n = referencer.Node;
                    parentObj.RemoveReference(referencer.ObjectInstance, referencer.Node);
                    referencer.ObjectInstance.RemoveReferencer(parentObj);
                    s.AddReferencer(parentObj);
                    n.InnerText = s.Name;
                    parentObj.AddReference(s, n);
                    parentObj.References.Remove(referencer);
                }
            }

            parentObj.MarkAsDirty();
        }

        /// <summary>
        /// The save tree state.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private static void SaveTreeState(XmlNode parentNode, TreeNodeCollection treeNodes)
        {
            foreach (TreeNode treeNode in treeNodes)
            {
                string name = Regex.Replace(treeNode.Text, @"[^\w-]+", String.Empty);
                XmlNode node = parentNode.OwnerDocument.CreateElement(name);
                parentNode.AppendChild(node);
                if (treeNode.IsExpanded)
                {
                    SaveTreeState(node, treeNode.Nodes);
                }
            }
        }

        /// <summary>
        /// The add nodes for bank.
        /// </summary>
        /// <param name="bank">
        /// The bank.
        /// </param>
        private void AddNodesForBank(IObjectBank bank)
        {
            string path = bank.FilePath.ToUpper().Replace(@"/", "\\");

            // strip off the base path
            path = path.ToUpper().Replace(Configuration.ObjectXmlPath.ToUpper(), String.Empty);

            string[] pathElements = path.Split('\\');

            TreeNodeCollection nodes = this.m_treeView.Nodes;
            for (int i = 0; i < pathElements.Length; i++)
            {
                bool found = false;
                foreach (TreeNode n in nodes)
                {
                    if (pathElements[i] == n.Text)
                    {
                        nodes = n.Nodes;
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    // create this node
                    if (i == pathElements.Length - 1)
                    {
                        var bn = new SoundBankNode(bank, this.m_objectTypeIconTable);
                        bn.BankStatusChanged += this.SoundBankNode_OnBankStatusChanged;
                        nodes.Add(bn);
                    }
                    else
                    {
                        string fullPath = string.Empty;
                        for (int j = 0; j <= i; j++)
                        {
                            fullPath += pathElements[j] + "\\";
                        }

                        var folder = new FolderNode(
                            pathElements[i], Configuration.ObjectXmlPath + fullPath, bank.Episode, bank.Type, this.m_objectTypeIconTable);
                        nodes.Add(folder);
                        nodes = folder.Nodes;
                    }
                }
            }

        }

        /// <summary>
        /// The check out.
        /// </summary>
        /// <param name="isLocal">
        /// The is local.
        /// </param>
        private void CheckOut(bool isLocal)
        {
            foreach (BaseNode bn in this.m_treeView.SelectedItems)
            {
                var sbn = bn as SoundBankNode;
                if (sbn != null)
                {
                    if (sbn.ObjectBank.IsCheckedOut)
                    {
                        // Already checked out
                        continue;
                    }

                    var isCurrentlyLocalCheckout = sbn.ObjectBank.IsLocalCheckout;

                    if (isCurrentlyLocalCheckout && !isLocal)
                    {
                        // Need to ensure that user has latest revision before locking file
                        var assetManager = RaveInstance.AssetManager;
                        if (!assetManager.HaveLatest(sbn.ObjectBank.FilePath))
                        {
                            MessageBox.Show(
                                "Bank has been updated remotely since it was locally locked. Please revert '" + sbn.ObjectBank.Name + "' before checking it out.",
                                "Cannot Lock Bank",
                                MessageBoxButtons.OK);
                            continue;
                        }

                        // Must save local changes as bank will be reloaded
                        if (sbn.ObjectBank.IsDirty)
                        {
                            if (MessageBox.Show(
                                "Changes to bank must be saved before checking out - save changes now?",
                                "Save Changes",
                                MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                            {
                                continue;
                            }

                            if (!sbn.ObjectBank.Save(true))
                            {
                                ErrorManager.HandleError("Failed to save changes to bank - unable to checkout: " + sbn.ObjectBank.FilePath);
                                continue;
                            }
                        }
                    }

                    var bankStatus = sbn.ObjectBank.GetLockedStatus();

                    if (bankStatus == null)
                    {
                        ErrorManager.HandleInfo(
                            "Failed to check out " + sbn.ObjectBank.Name + " bank - there was a Perforce error determining the locked status of the bank.");
                    }
                    else
                    {
                        if (!isLocal && bankStatus.IsLocked && !bankStatus.IsLockedByMe)
                        {
                            ErrorManager.HandleInfo(sbn.ObjectBank.Name + " bank is currently locked by " + bankStatus.Owner);
                        }
                        else
                        {
                            this.SaveTreeState(this.m_treeView.LastSelectedNode.Nodes);
                            if (isCurrentlyLocalCheckout && !isLocal)
                            {
                                // If we're going from a local to full lock, only need to lock asset
                                // and skip grab and reload (we've already checked that we have latest
                                // version above, so don't need to worry about data inconsistencies
                                if (!sbn.ObjectBank.Lock())
                                {
                                    return;
                                }
                            }
                            else
                            {
                                if (!sbn.ObjectBank.Checkout(isLocal))
                                {
                                    return;
                                }
                            }

                            sbn.RecreateSoundNodes();
                            this.ExpandTree(this.m_treeView.LastSelectedNode.Nodes);
                        }
                    }
                }
            }

            this.ChangelistModified.Raise();
        }

        /// <summary>
        /// The collapse names.
        /// </summary>
        /// <param name="showFullName">
        /// The show full name.
        /// </param>
        private void CollapseNames(bool showFullName)
        {
            this.m_collapse = showFullName;
            this.m_treeView.BeginUpdate();
            this.DoCollapse(this.m_treeView.Nodes);
            this.m_treeView.EndUpdate();
        }

        /// <summary>
        /// The delete items from tree.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        private void DeleteItemsFromTree(ArrayList nodes)
        {
            foreach (BaseNode baseNode in nodes)
            {
                // if bn.Parent is null then it's already been removed so ignore it - this happens if you multi-select
                // a bank and it's sounds
                if (baseNode.Parent != null)
                {
                    if (baseNode.GetType() == typeof(SoundNode))
                    {
                        var soundNode = (SoundNode)baseNode;

                        // Check if any other objects reference node
                        foreach (var referencer in soundNode.ObjectInstance.Referencers)
                        {
                            var objectInstance = referencer as IObjectInstance;
                            var template = referencer as ITemplate;

                            if ((null != objectInstance && !objectInstance.IsAutoGenerated) ||
                                null != template)
                            {
                                ErrorManager.HandleInfo(
                                "Other objects reference " + soundNode.ObjectInstance.Name + " - remove these references first");
                                return;
                            }
                        }

                        if (this.TryToCheckOutBank(ref soundNode) && soundNode != null)
                        {
                            soundNode.ObjectInstance.Delete();
                            soundNode.Remove();
                            this.DeleteObject.Raise(soundNode.ObjectInstance);
                        }
                    }
                    else if (baseNode.GetType() == typeof(SoundBankNode))
                    {
                        var sbn = (SoundBankNode)baseNode;
                        foreach (var s in sbn.ObjectBank.ObjectInstances)
                        {
                            foreach (var referencer in s.Referencers)
                            {
                                var objectInstance = referencer as IObjectInstance;
                                var template = referencer as ITemplate;

                                if (null != objectInstance && objectInstance.Bank != sbn.ObjectBank && !objectInstance.IsAutoGenerated)
                                {
                                    ErrorManager.HandleInfo(
                                        "The bank to be deleted contains at least one object that is referenced by an object in another bank - remove this reference first\n\n"
                                        + s.Name + " is referenced by " + objectInstance.Name + " in bank " + objectInstance.Bank.Name);
                                    return;
                                }

                                if (null != template)
                                {
                                    ErrorManager.HandleInfo(
                                        "The bank to be deleted contains at least one object that is referenced by a template - remove this reference first\n\n"
                                        + s.Name + " is referenced by " + template.Name + " in bank " + template.TemplateBank.Name);
                                    return;
                                }
                            }
                        }

                        if (sbn.ObjectBank.ObjectInstances.Count == 0
                            || ErrorManager.HandleQuestion(
                                "Are you sure you want to delete " + sbn.ObjectBank.Name
                                + " and all objects within it?",
                                MessageBoxButtons.YesNo,
                                DialogResult.Yes) == DialogResult.Yes)
                        {
                            sbn.ObjectBank.Delete(true);
                            sbn.Remove();
                            this.xmlBankManager.RemoveBank(sbn.ObjectBank);
                            this.ChangelistModified.Raise();
                        }
                    }
                    else if (baseNode.GetType() == typeof(FolderNode))
                    {
                        if (baseNode.Nodes.Count != 0)
                        {
                            ErrorManager.HandleInfo("Folder " + baseNode.Text + " is not empty");
                        }
                        else
                        {
                            var sfn = (FolderNode)baseNode;
                            try
                            {
                                Directory.Delete(RaveInstance.AssetManager.GetLocalPath(sfn.GetFullPath()));
                            }
                            catch (Exception e)
                            {
                                ErrorManager.HandleError(e);
                            }

                            sfn.Remove();
                        }
                    }
                    else if (baseNode.GetType() == typeof(VirtualFolderNode))
                    {
                        var vfn = (VirtualFolderNode)baseNode;
                        var sbn = (SoundBankNode)vfn.Parent;
                        var objectsToDelete = new List<IObjectInstance>(sbn.ObjectBank.ObjectInstances);
                        bool referencedSounds = false;

                        if (this.TryToCheckOutBank(sbn))
                        {
                            foreach (var obj in objectsToDelete)
                            {
                                if (obj.VirtualFolderName == vfn.ObjectName)
                                {
                                    bool canDelete = true;
                                    foreach (var refSound in obj.Referencers)
                                    {
                                        var objectInstance = refSound as IObjectInstance;

                                        if (null == objectInstance)
                                        {
                                            continue;
                                        }

                                        if (obj.Referencers.Count != 0 || objectInstance.Bank != sbn.ObjectBank
                                            || objectInstance.VirtualFolderName != vfn.ObjectName)
                                        {
                                            canDelete = false;
                                            break;
                                        }
                                    }

                                    if (canDelete)
                                    {
                                        obj.Delete();
                                        foreach (SoundNode sn in vfn.Nodes)
                                        {
                                            if (sn.ObjectInstance == obj)
                                            {
                                                sn.Remove();
                                                this.DeleteObject.Raise(sn.ObjectInstance);
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        referencedSounds = true;
                                    }
                                }
                            }

                            if (referencedSounds)
                            {
                                ErrorManager.HandleInfo(
                                    "Some objects within the virtual folder " + vfn.Text
                                    + " are referenced by other objects and/or templates; these objects have not been deleted");
                            }
                            else if (vfn.Nodes.Count == 0)
                            {
                                try
                                {
                                    vfn.Remove();
                                }
                                catch (Exception ex)
                                {
                                    Debug.WriteLine(ex.ToString());
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The do collapse.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        private void DoCollapse(TreeNodeCollection nodes)
        {
            foreach (TreeNode n in nodes)
            {
                if (n.Parent != null)
                {
                    var node = n as BaseNode;
                    string nodeName = node.ObjectName;
                    string parentName = ((BaseNode)node.Parent).ObjectName;

                    if (!this.m_collapse)
                    {
                        node.ResetText();
                    }
                    else if (nodeName.StartsWith(parentName) && nodeName != parentName)
                    {
                        node.Text = "..." + nodeName.Substring(parentName.Length);
                    }
                }

                this.m_treeView.BeginUpdate();
                this.DoCollapse(n.Nodes);
                this.m_treeView.EndUpdate();
            }
        }

        /// <summary>
        /// The expand everything except bank nodes.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        private void ExpandEverythingExceptBankNodes(TreeNodeCollection nodes)
        {
            this.m_treeView.BeginUpdate();

            foreach (TreeNode n in nodes)
            {
                if (n.GetType() != typeof(SoundBankNode))
                {
                    n.Expand();
                    this.ExpandEverythingExceptBankNodes(n.Nodes);
                }
            }

            this.m_treeView.EndUpdate();
        }

        /// <summary>
        /// Expand to checked out banks.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        private void ExpandToCheckedOutBanks(TreeNodeCollection nodes)
        {
            this.m_treeView.BeginUpdate();

            foreach (TreeNode node in nodes)
            {
                var bankNode = node as SoundBankNode;
                if (null != bankNode && !bankNode.ObjectBank.IsAutoGenerated &&
                    (bankNode.ObjectBank.IsCheckedOut || bankNode.ObjectBank.IsLocalCheckout))
                {
                    var currNode = node.Parent;
                    while (null != currNode)
                    {
                        currNode.Expand();
                        currNode = currNode.Parent;
                    }
                }

                this.ExpandToCheckedOutBanks(node.Nodes);
            }

            this.m_treeView.EndUpdate();
        }

        /// <summary>
        /// The expand tree.
        /// </summary>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private void ExpandTree(TreeNodeCollection treeNodes)
        {
            this.m_treeView.BeginUpdate();
            ExpandTree(this.m_nodeState.DocumentElement, treeNodes);
            this.m_treeView.EndUpdate();
        }

        /// <summary>
        /// The get latest folder.
        /// </summary>
        /// <param name="fn">
        /// The fn.
        /// </param>
        private void GetLatestFolder(FolderNode fn)
        {
            var banks = new ArrayList();
            var checkedOutBanks = new ArrayList();
            var referencingObjects = new ArrayList();
            this.SaveTreeState(this.m_treeView.Nodes);
            var t = new Thread(this.ShowBusy);
            t.Start();

            BusyText = "Getting Latest...";

            // Find path of folder
            string path = fn.GetFullPath();

            if (!RaveInstance.AssetManager.GetLatest(path, false))
            {
                ErrorManager.HandleError("Failed to get latest: " + path);
                return;
            }

            var dir = new DirectoryInfo(path);

            // get a list of bankManager currently in the folder node
            FolderNode.GetBanks(fn, banks);

            // create a list of checked out bankManager
            foreach (SoundBankNode sbn in banks)
            {
                if (sbn.ObjectBank.IsCheckedOut)
                {
                    checkedOutBanks.Add(sbn);
                }
                else
                {
                    foreach (IObjectInstance o in sbn.ObjectBank.ObjectInstances)
                    {
                        if (!referencingObjects.Contains(o))
                        {
                            referencingObjects.Add(o.Name);
                        }
                    }
                }
            }

            // remove any checked out bankManager from bank list
            foreach (SoundBankNode checkedOutbank in checkedOutBanks)
            {
                banks.Remove(checkedOutbank);
            }

            BusyText = "Reloading bankManager...";

            // remove bankManager from bank list and sounds from sound list
            // so when we recreate/update the lists we do not get duplicates
            foreach (SoundBankNode sbn in banks)
            {
                this.xmlBankManager.RemoveBank(sbn.ObjectBank);
                sbn.Remove();
            }

            // load the bankManager in the directory corresponding to the folder
            foreach (FileInfo fileInfo in dir.GetFiles("*.xml", SearchOption.AllDirectories))
            {
                if (!checkedOutBanks.OfType<SoundBankNode>().Any(p => p.ObjectBank.Name == Path.GetFileNameWithoutExtension(fileInfo.FullName)))
                {
                    this.xmlBankManager.LoadBank(fileInfo, fn.Episode, fn.Type, BankTypes.Object);
                }
            }
            BusyText = "Updating Object Referencers...";

            // Update the object references
            var id = new ObjectLookupId(fn.Type, fn.Episode);
            foreach (string s in referencingObjects)
            {
                if (RaveInstance.ObjectLookupTables[id].ContainsKey(s))
                {
                    BusyText = "Updating Object Referencers for " + s;
                    RaveInstance.ObjectLookupTables[id][s].ComputeReferences();
                }
            }

            BusyText = "Updating SoundBrowser...";

            // redraw tree
            this.RefreshTree();
            this.ExpandTree(this.m_treeView.Nodes);
            this.m_finished = true;
        }

        /// <summary>
        /// The handle copy.
        /// </summary>
        private void HandleCopy()
        {
            if (this.m_treeView.LastSelectedNode != null
                && this.m_treeView.LastSelectedNode.GetType() == typeof(SoundNode))
            {
                var sn = (SoundNode)this.m_treeView.LastSelectedNode;
                frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "Duplicated object name", sn.ObjectInstance.Name);
                if (input.HasBeenCancelled) return;
                string name = input.data;

                while (ObjectInstance.CheckForNameAndHashCollision(name, sn.ObjectInstance.Bank.Episode))
                {
                    input = frmSimpleInput.GetInput(this.ParentForm, "Name or namehash in use, name", name);
                    if (input.HasBeenCancelled) return;
                    name = input.data;
                }

                if (this.TryToCheckOutBank(ref sn) && sn != null)
                {
                    TypeUtils.CreateDuplicateSound(sn.ObjectInstance, sn.ObjectInstance.Bank, name);
                }
            }
        }

        /// <summary>
        /// The handle delete.
        /// </summary>
        private void HandleDelete()
        {
            if (this.m_treeView.SelectedItems.Count > 0)
            {
                if (ErrorManager.HandleQuestion(
                    "Are you sure you want to delete these objects?", MessageBoxButtons.YesNo, DialogResult.Yes)
                    == DialogResult.Yes)
                {
                    this.DeleteItemsFromTree(this.m_treeView.SelectedItems);
                }
            }
        }

        /// <summary>
        /// The handle dropped folder.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="folderNode">
        /// The folder node.
        /// </param>
        private void HandleDroppedFolder(TreeNode parent, FolderNode folderNode)
        {
            var parentNode = parent as BaseNode;
            var newFolder = new FolderNode(
                folderNode.Text, parentNode.FullPath + "\\" + folderNode.Text, folderNode.Episode, folderNode.Type, this.m_objectTypeIconTable);
            if (parentNode.Nodes.Cast<TreeNode>().Any(node => String.Compare(node.Text, newFolder.Text, true) == 0))
            {
                ErrorManager.HandleError("Cannot move folder " + folderNode.FullPath);
                return;
            }

            if (folderNode.Type == parentNode.Type && folderNode.Episode == parentNode.Episode)
            {
                parentNode.Nodes.Add(newFolder);

                foreach (TreeNode tn in folderNode.Nodes)
                {
                    if (tn.GetType() == typeof(FolderNode))
                    {
                        this.HandleDroppedFolder(newFolder, tn as FolderNode);
                    }

                    if (tn.GetType() == typeof(SoundBankNode))
                    {
                        this.HandleDroppedSoundBank(newFolder, tn as SoundBankNode);
                    }
                }

                folderNode.Remove();
            }
            else
            {
                ErrorManager.HandleError(
                    "Invalid move, folder must me moved to a folder with the same episode and type");
            }
        }

        /// <summary>
        /// The handle dropped sound bank.
        /// </summary>
        /// <param name="targetNode">
        /// The target node.
        /// </param>
        /// <param name="droppedSoundBankNode">
        /// The dropped sound bank node.
        /// </param>
        private void HandleDroppedSoundBank(TreeNode targetNode, SoundBankNode droppedSoundBankNode)
        {
            if (targetNode != null && targetNode.GetType() == typeof(FolderNode))
            {
                var targetFolderNode = targetNode as FolderNode;
                string targetFolderPath = targetFolderNode.FullPath;
                var objectBank = droppedSoundBankNode.ObjectBank;

                string srcFilePath = objectBank.FilePath;
                string destFilePath = string.Concat(
                    Configuration.ObjectXmlPath,
                    targetFolderPath,
                    Path.DirectorySeparatorChar,
                    Path.GetFileName(srcFilePath));

                if (string.Compare(srcFilePath, destFilePath, true) != 0
                    && targetFolderNode.Episode == droppedSoundBankNode.Episode
                    && targetFolderNode.Type == droppedSoundBankNode.Type)
                {
                    string destDirectory = Path.GetDirectoryName(destFilePath);
                    if (!Directory.Exists(destDirectory))
                    {
                        Directory.CreateDirectory(destDirectory);
                    }

                    File.Copy(srcFilePath, destFilePath, true);

                    var bankName = droppedSoundBankNode.ObjectBank.Name;

                    // Delete bank and remove node but do not delete asset
                    droppedSoundBankNode.ObjectBank.Delete(false);
                    droppedSoundBankNode.Remove();

                    var changelist = RaveInstance.AssetManager.CreateChangeList("[RAVE] Moving object bank - " + bankName);
                    changelist.MarkAssetForDelete(srcFilePath);
                    changelist.MarkAssetForAdd(destFilePath);
                    changelist.Submit();

                    if (File.Exists(srcFilePath))
                    {
                        File.Delete(srcFilePath);
                    }

                    var bank = new ObjectBank(
                        destFilePath, targetFolderNode.Episode, targetFolderNode.Type, this.m_typeDefinitions);
                    bank.Load();
                    bank.ComputeReferences();
                    this.xmlBankManager.Banks.Add(bank);

                    var newNode = new SoundBankNode(bank, this.m_objectTypeIconTable);
                    newNode.BankStatusChanged += this.SoundBankNode_OnBankStatusChanged;
                    this.m_treeView.BeginUpdate();
                    targetFolderNode.Nodes.Add(newNode);
                    this.m_treeView.EndUpdate();
                }
            }
        }

        /// <summary>
        /// The handle dropped sound node.
        /// </summary>
        /// <param name="targetNode">
        /// The target node.
        /// </param>
        /// <param name="droppedSoundNode">
        /// The dropped sound node.
        /// </param>
        /// <param name="updateUi">
        /// The update ui.
        /// </param>
        private void HandleDroppedSoundNode(TreeNode targetNode, SoundNode[] droppedSoundNodes)
        {
            if (targetNode != null)
            {
                var cleanedNodes = new List<SoundNode>(droppedSoundNodes);
                cleanedNodes.RemoveAll(x => x == targetNode);
                droppedSoundNodes = cleanedNodes.ToArray();
                if (targetNode.GetType() == typeof(SoundBankNode))
                {
                    this.ProcessSoundNodeDroppedOnSoundBankNode(targetNode as SoundBankNode, droppedSoundNodes);
                }
                else if (targetNode.GetType() == typeof(VirtualFolderNode))
                {
                    this.ProcessSoundNodeDroppedOnVirtualFolderNode(
                        targetNode as VirtualFolderNode, droppedSoundNodes);
                }
                else if (targetNode.GetType() == typeof(SoundNode))
                {
                    //Do nothing if dropped on the same node
                    if (droppedSoundNodes.Length < 1)
                    {
                        return;
                    }
                    this.ProcessSoundNodeDroppedOnSoundNode(targetNode as SoundNode, droppedSoundNodes);
                }
            }
        }

        /// <summary>
        /// The handle dropped virtual folder.
        /// </summary>
        /// <param name="targetSoundBankNode">
        /// The target sound bank node.
        /// </param>
        /// <param name="virtualFolderNode">
        /// The virtual folder node.
        /// </param>
        private void HandleDroppedVirtualFolder(SoundBankNode targetSoundBankNode, VirtualFolderNode virtualFolderNode)
        {
            // assume been checked out - update
            if (virtualFolderNode != null)
            {
                var newVirtualFolderNode = new VirtualFolderNode(
                    virtualFolderNode.Text, virtualFolderNode.Episode, virtualFolderNode.Type, this.m_objectTypeIconTable);
                targetSoundBankNode.Nodes.Add(newVirtualFolderNode);
                List<SoundNode> soundNodes =
                    (from SoundNode soundNode in virtualFolderNode.Nodes where soundNode != null select soundNode)
                        .ToList();

                this.HandleDroppedSoundNode(newVirtualFolderNode, soundNodes.ToArray());
            }
        }

        /// <summary>
        /// The handle dropped wave.
        /// </summary>
        /// <param name="wave">
        /// The wave.
        /// </param>
        private void HandleDroppedWave(WaveNode wave)
        {
            var node = this.m_treeView.LastSelectedNode;

            if (node == null)
            {
                return;
            }

            if (this.m_type.ToUpper() != "SOUNDS")
            {
                ErrorManager.HandleInfo("Can only perform this action on sounds");
                return;
            }

            if (node.GetType() == typeof(SoundBankNode))
            {
                var bank = (SoundBankNode)node;

                if (!this.TryToCheckOutBank(bank))
                {
                    return;
                }

                var newObject = TypeUtils.CreateWrapperSound(
                    bank.ObjectBank, Configuration.WaveWrapperSoundType, wave);
                TypeUtils.ReportNewObjectCreated(newObject);
            }
            else if (node.GetType() == typeof(VirtualFolderNode))
            {
                var bank = (SoundBankNode)node.Parent;
                var fullPath = node.FullPath;

                if (!this.TryToCheckOutBank(bank))
                {
                    return;
                }

                var newObject = TypeUtils.CreateWrapperSound(
                    bank.ObjectBank, Configuration.WaveWrapperSoundType, wave);
                newObject.VirtualFolderName = node.Text;
                TypeUtils.ReportNewObjectCreated(newObject);

                // parent lost after checkout - find updated folder node
                if (node.Parent == null)
                {
                    foreach (TreeNode tn in bank.Nodes)
                    {
                        if (tn.FullPath == fullPath)
                        {
                            this.m_treeView.LastSelectedNode = tn;
                            break;
                        }
                    }
                }
            }
            else if (node.GetType() == typeof(SoundNode))
            {
                var target = (SoundNode)node;

                if (!this.TryToCheckOutBank(ref target) || target == null)
                {
                    return;
                }

                target.ObjectInstance.HandleDroppedWave(wave);
            }
        }

        /// <summary>
        /// The init paste menu.
        /// </summary>
        private void InitPasteMenu()
        {
            if (TypeUtils.ObjectClipboard == null)
            {
                return;
            }

            this.mnuPasteBaseSettings.Visible = true;

            var typedObjectInstance = TypeUtils.ObjectClipboard as ITypedObjectInstance;

            if (null == typedObjectInstance)
            {
                throw new Exception("Unable to paste object of unknown type; expected ITypedObjectInstance");
            }

            var typeDef = typedObjectInstance.TypeDefinition;

            if (null != typeDef)
            {
                // Build up set of display groups of source object
                var tmp = new HashSet<string>();
                foreach (object item in this.m_treeView.SelectedItems)
                {
                    var sn = item as SoundNode;

                    if (null == sn)
                    {
                        continue;
                    }

                    var typedObjInstance = sn.ObjectInstance as ITypedObjectInstance;

                    if (null == typedObjInstance)
                    {
                        throw new Exception("Expected object instance of type IObjectBank");
                    }

                    if (null == sn || null == typedObjInstance.TypeDefinition)
                    {
                        continue;
                    }

                    foreach (string group in typedObjInstance.TypeDefinition.GetDisplayGroups())
                    {
                        tmp.Add(@group);
                    }
                }

                List<string> sourceGroups = tmp.ToList();
                List<string> destGroups = typeDef.GetDisplayGroups();

                // Filter destination group list to only show
                // those which exist in source group list
                destGroups.RemoveAll(item => !sourceGroups.Contains(item));

                // "Main" display group is currently made up of fields
                // that don't have a specified display group, so add
                // "Main" paste option explicitly.
                var mainItem = new ToolStripMenuItem("Main");
                mainItem.Click += this.PasteBaseSettings_Click;
                this.mnuPasteBaseSettings.DropDownItems.Add(mainItem);


                if (destGroups.Any())
                {
                    this.mnuPasteBaseSettings.DropDownItems.Add(new ToolStripSeparator());

                    // Add paste menu item for all display groups of
                    // destination object that exist in source display groups list
                    foreach (string group in destGroups)
                    {
                        var menuItem = new ToolStripMenuItem(@group);
                        menuItem.Click += this.PasteBaseSettings_Click;
                        this.mnuPasteBaseSettings.DropDownItems.Add(menuItem);
                    }
                }
                this.mnuPasteBaseSettings.DropDownItems.Add(new ToolStripSeparator());
                var allItem = new ToolStripMenuItem("All");
                allItem.Click += this.PasteBaseSettings_Click;
                this.mnuPasteBaseSettings.DropDownItems.Add(allItem);
            }

            // Only enable menu if it has any menu items
            this.mnuPasteBaseSettings.Enabled = this.mnuPasteBaseSettings.DropDownItems.Count > 0;
        }

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ctrlObjectBrowser));
            this.mnuRightClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuViewHierarchy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMute = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSolo = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAudition = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCopyPath = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCopyName = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuNewSound = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewSoundBank = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRename = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuInsertContext = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSeperator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuCheckOut = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRevert = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuLocalCheckOut = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSave = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSubmit = new ToolStripMenuItem();
            this.mnuReload = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuReloadCheckedIn = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuReloadSaved = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuGetLatest = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuModel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSeperator3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDupSound = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDupTree = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPopulateMissingFields = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSearchInStore = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuExpandObjectStores = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuExpandCheckedOut = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBatchCreate = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCopyBaseSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPasteBaseSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMultiEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuGlobal = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPlugins = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuGridEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExpand = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuExpandXml = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSoundSetReport = new ToolStripMenuItem();
            this.mnuInvalidRefsReport = new ToolStripMenuItem();
            this.mnuViewInP4 = new ToolStripMenuItem();
            this.mnuViewInExplorer = new ToolStripMenuItem();
            this.iconList = new System.Windows.Forms.ImageList(this.components);
            this.m_toolbar = new System.Windows.Forms.ToolBar();
            this.btnCopy = new System.Windows.Forms.ToolBarButton();
            this.btnDelete = new System.Windows.Forms.ToolBarButton();
            this.btnRename = new System.Windows.Forms.ToolBarButton();
            this.btnExpandFolders = new System.Windows.Forms.ToolBarButton();
            this.btnCollapse = new System.Windows.Forms.ToolBarButton();
            this.toolBarIcons = new System.Windows.Forms.ImageList(this.components);
            this.m_treeView = new ScrollableTreeView();
            this.mnuRightClick.SuspendLayout();
            this.SuspendLayout();

            // mnuRightClick
            this.mnuRightClick.Items.AddRange(
                new ToolStripItem[]
                    {
                        this.mnuViewHierarchy,  this.mnuAudition, this.mnuMute, this.mnuSolo, this.mnuCopyPath, this.mnuCopyName, this.menuItem4, 
                        this.mnuNewSound, this.mnuNewSoundBank, this.mnuNewFolder, this.mnuRename, this.mnuDelete, 
                        this.mnuSeperator2, this.mnuCheckOut, this.mnuRevert, this.mnuLocalCheckOut, this.mnuSave, this.mnuSubmit,
                        this.mnuReload, this.mnuGetLatest, this.mnuModel, this.mnuSeperator3, this.mnuDupSound, this.mnuInsertContext, 
                        this.mnuDupTree, this.mnuPopulateMissingFields, this.mnuSearchInStore, this.mnuExpandObjectStores, 
                        this.mnuExpandCheckedOut, this.mnuBatchCreate, this.mnuCopyBaseSettings, this.mnuPasteBaseSettings, 
                        this.mnuMultiEdit, this.mnuGlobal, this.mnuSoundSetReport, this.mnuInvalidRefsReport, this.mnuPlugins, 
                        this.mnuGridEdit, this.mnuSeparator4, this.mnuExpand, this.mnuExpandXml, this.mnuViewInP4, this.mnuViewInExplorer
                    });
            this.mnuRightClick.Name = "mnuRightClick";
            this.mnuRightClick.Size = new System.Drawing.Size(221, 732);
            this.mnuRightClick.Opening += this.mnuRightClick_Popup;

            // mnuViewHierarchy
            this.mnuViewHierarchy.Name = "mnuViewHierarchy";
            this.mnuViewHierarchy.Size = new System.Drawing.Size(220, 22);
            this.mnuViewHierarchy.Text = "View Hierarchy";
            this.mnuViewHierarchy.Click += this.mnuViewHierarchy_Click;

            // mnuAudition
            this.mnuAudition.Name = "mnuAudition";
            this.mnuAudition.Size = new System.Drawing.Size(220, 22);
            this.mnuAudition.Text = "Audition";
            this.mnuAudition.Click += this.mnuAudition_Click;

            // mnuMute
            this.mnuMute.Name = "mnuNewSound";
            this.mnuMute.Size = new System.Drawing.Size(220, 22);
            this.mnuMute.Text = "Mute";
            this.mnuMute.CheckOnClick = true;
            this.mnuMute.Click += this.mnuMute_Click;

            // mnuSolo
            this.mnuSolo.Name = "mnuNewSound";
            this.mnuSolo.Size = new System.Drawing.Size(220, 22);
            this.mnuSolo.Text = "Solo";
            this.mnuMute.CheckOnClick = true;
            this.mnuSolo.Click += this.mnuSolo_Click;

            // mnuCopyPath
            this.mnuCopyPath.Name = "mnuCopyPath";
            this.mnuCopyPath.Size = new System.Drawing.Size(220, 22);
            this.mnuCopyPath.Text = "Copy Object Path";
            this.mnuCopyPath.Click += this.mnuCopyPath_Click;

            // mnuCopyName
            this.mnuCopyName.Name = "mnuCopyName";
            this.mnuCopyName.Size = new System.Drawing.Size(220, 22);
            this.mnuCopyName.Text = "Copy Object Name";
            this.mnuCopyName.Click += this.mnuCopyName_Click;

            // menuItem4
            this.menuItem4.Name = "menuItem4";
            this.menuItem4.Size = new System.Drawing.Size(217, 6);

            // mnuNewSound
            this.mnuNewSound.Name = "mnuNewSound";
            this.mnuNewSound.Size = new System.Drawing.Size(220, 22);
            this.mnuNewSound.Text = "New";

            // mnuNewSoundBank
            this.mnuNewSoundBank.Name = "mnuNewSoundBank";
            this.mnuNewSoundBank.Size = new System.Drawing.Size(220, 22);
            this.mnuNewSoundBank.Text = "New Object Store";
            this.mnuNewSoundBank.Click += this.mnuNewSoundBank_Click;

            // mnuNewFolder
            this.mnuNewFolder.Name = "mnuNewFolder";
            this.mnuNewFolder.Size = new System.Drawing.Size(220, 22);
            this.mnuNewFolder.Text = "New Folder";
            this.mnuNewFolder.Click += this.mnuNewFolder_Click;

            // mnuRename
            this.mnuRename.Name = "mnuRename";
            this.mnuRename.Size = new System.Drawing.Size(220, 22);
            this.mnuRename.Text = "Rename";
            this.mnuRename.Click += this.mnuRename_Click;

            // mnuDelete
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Size = new System.Drawing.Size(220, 22);
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += this.mnuDeleteSound_Click;

            // mnuSeperator2
            this.mnuSeperator2.Name = "mnuSeperator2";
            this.mnuSeperator2.Size = new System.Drawing.Size(217, 6);

            // mnuCheckOut
            this.mnuCheckOut.Name = "mnuCheckOut";
            this.mnuCheckOut.Size = new System.Drawing.Size(220, 22);
            this.mnuCheckOut.Text = "Check Out Store";
            this.mnuCheckOut.Click += this.mnuCheckOut_Click;

            // mnuRevert
            this.mnuRevert.Name = "mnuRevert";
            this.mnuRevert.Size = new System.Drawing.Size(220, 22);
            this.mnuRevert.Text = "Revert Store";
            this.mnuRevert.Click += this.mnuRevert_Click;

            // mnuLocalCheckOut
            this.mnuLocalCheckOut.Name = "mnuLocalCheckOut";
            this.mnuLocalCheckOut.Size = new System.Drawing.Size(220, 22);
            this.mnuLocalCheckOut.Text = "Local Check Out";
            this.mnuLocalCheckOut.Click += this.mnuLocalCheckOut_Click;

            // mnuSave
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Size = new System.Drawing.Size(220, 22);
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += this.mnuSave_OnClick;

            // mnuSubmit
            this.mnuSubmit.Name = "mnuSubmit";
            this.mnuSubmit.Size = new System.Drawing.Size(220, 22);
            this.mnuSubmit.Text = "Submit";
            this.mnuSubmit.Click += this.mnuSubmit_OnClick;

            // mnuReload
            this.mnuReload.DropDownItems.AddRange(new ToolStripItem[] { this.mnuReloadCheckedIn, this.mnuReloadSaved });
            this.mnuReload.Name = "mnuReload";
            this.mnuReload.Size = new System.Drawing.Size(220, 22);
            this.mnuReload.Text = "Reload";

            // mnuReloadCheckedIn
            this.mnuReloadCheckedIn.Name = "mnuReloadCheckedIn";
            this.mnuReloadCheckedIn.Size = new System.Drawing.Size(177, 22);
            this.mnuReloadCheckedIn.Text = "Checked In Version";
            this.mnuReloadCheckedIn.Click += this.mnuReloadCheckedIn_OnClick;

            // mnuReloadSaved
            this.mnuReloadSaved.Name = "mnuReloadSaved";
            this.mnuReloadSaved.Size = new System.Drawing.Size(177, 22);
            this.mnuReloadSaved.Text = "Saved Version";
            this.mnuReloadSaved.Click += this.mnuReloadSaved_OnClick;

            // mnuGetLatest
            this.mnuGetLatest.Name = "mnuGetLatest";
            this.mnuGetLatest.Size = new System.Drawing.Size(220, 22);
            this.mnuGetLatest.Text = "Get Latest";
            this.mnuGetLatest.Click += this.mnuGetLatest_Click;

            // mnuModel
            this.mnuModel.Name = "mnuModel";
            this.mnuModel.Size = new System.Drawing.Size(220, 22);
            this.mnuModel.Text = "Set as Default Model Store";
            this.mnuModel.Click += this.mnuModel_Click;

            // mnuSeperator3
            this.mnuSeperator3.Name = "mnuSeperator3";
            this.mnuSeperator3.Size = new System.Drawing.Size(217, 6);

            // mnuDupSound
            this.mnuDupSound.Name = "mnuDupSound";
            this.mnuDupSound.Size = new System.Drawing.Size(220, 22);
            this.mnuDupSound.Text = "Duplicate Object";
            this.mnuDupSound.Click += this.mnuDupSound_Click;

            // mnuInsertContext
            this.mnuInsertContext.Name = "mnuInsertContext";
            this.mnuInsertContext.Size = new System.Drawing.Size(220, 22);
            this.mnuInsertContext.Text = "Insert Context Sound";
            this.mnuInsertContext.Click += this.mnuInsertContext_Click;

            // mnuDupTree
            this.mnuDupTree.Name = "mnuDupTree";
            this.mnuDupTree.Size = new System.Drawing.Size(220, 22);
            this.mnuDupTree.Text = "Duplicate Tree";
            this.mnuDupTree.Click += this.mnuDupTree_Click;

            // mnuPopulateMissingFields
            this.mnuPopulateMissingFields.Name = "mnuPopulateMissingFields";
            this.mnuPopulateMissingFields.Size = new System.Drawing.Size(220, 22);
            this.mnuPopulateMissingFields.Text = "Populate Missing Fields";
            this.mnuPopulateMissingFields.Click += this.mnuPopulateMissingFields_Click;

            // mnuSearchInStore
            this.mnuSearchInStore.Name = "mnuSearchInStore";
            this.mnuSearchInStore.Size = new System.Drawing.Size(220, 22);
            this.mnuSearchInStore.Text = "Search from here ...";
            this.mnuSearchInStore.Click += this.mnuSearchInStore_Click;

            // mnuExpandObjectStores
            this.mnuExpandObjectStores.Name = "mnuExpandObjectStores";
            this.mnuExpandObjectStores.Size = new System.Drawing.Size(220, 22);
            this.mnuExpandObjectStores.Text = "Expand Object Stores";
            this.mnuExpandObjectStores.Visible = false;
            this.mnuExpandObjectStores.Click += this.mnuExpandObjectStores_Click;

            // mnuExpandCheckedOut
            this.mnuExpandCheckedOut.Name = "mnuExpandCheckedOut";
            this.mnuExpandCheckedOut.Size = new System.Drawing.Size(220, 22);
            this.mnuExpandCheckedOut.Text = "Expand Checked Out Stores";
            this.mnuExpandCheckedOut.Click += this.mnuExpandCheckedOut_Click;

            // mnuBatchCreate
            this.mnuBatchCreate.Name = "mnuBatchCreate";
            this.mnuBatchCreate.Size = new System.Drawing.Size(220, 22);
            this.mnuBatchCreate.Text = "Batch Create";

            // mnuCopyBaseSettings
            this.mnuCopyBaseSettings.Name = "mnuCopyBaseSettings";
            this.mnuCopyBaseSettings.Size = new System.Drawing.Size(220, 22);
            this.mnuCopyBaseSettings.Text = "Copy Settings";
            this.mnuCopyBaseSettings.Click += this.menuCopyBaseSettings_Click;

            // mnuPasteBaseSettings
            this.mnuPasteBaseSettings.Name = "mnuPasteBaseSettings";
            this.mnuPasteBaseSettings.Size = new System.Drawing.Size(220, 22);
            this.mnuPasteBaseSettings.Text = "Paste ";

            // mnuMultiEdit
            this.mnuMultiEdit.Name = "mnuMultiEdit";
            this.mnuMultiEdit.Size = new System.Drawing.Size(220, 22);
            this.mnuMultiEdit.Text = "Edit Multi-Object Settings";
            this.mnuMultiEdit.Click += this.MultiEdit_Click;

            // mnuGlobal
            this.mnuGlobal.Name = "mnuGlobal";
            this.mnuGlobal.Size = new System.Drawing.Size(220, 22);
            this.mnuGlobal.Text = "Global Field Edit";
            this.mnuGlobal.Click += this.mnuGlobal_OnClick;

            // mnuPlugins
            this.mnuPlugins.Name = "mnuPlugins";
            this.mnuPlugins.Size = new System.Drawing.Size(220, 22);
            this.mnuPlugins.Text = "Plugins";

            // mnuSoundSetReport
            this.mnuSoundSetReport.Name = "mnuSoundSetReport";
            this.mnuSoundSetReport.Size = new System.Drawing.Size(220, 22);
            this.mnuSoundSetReport.Text = "Generate Report";
            this.mnuSoundSetReport.Click += this.mnuSoundSetReport_Click;

            // mnuInvalidRefsReport
            this.mnuInvalidRefsReport.Name = "mnuInvalidRefsReport";
            this.mnuInvalidRefsReport.Size = new System.Drawing.Size(220, 22);
            this.mnuInvalidRefsReport.Text = "Invalid References Report";
            this.mnuInvalidRefsReport.Click += this.mnuInvalidRefsReport_Click;

            // mnuViewInP4
            this.mnuViewInP4.Name = "mnuViewInP4";
            this.mnuViewInP4.Size = new System.Drawing.Size(220, 22);
            this.mnuViewInP4.Text = "View in P4";
            this.mnuViewInP4.Click += this.mnuViewInP4_Click;

            // mnuViewInExplorer
            this.mnuViewInExplorer.Name = "mnuViewInExplorer";
            this.mnuViewInExplorer.Size = new System.Drawing.Size(220, 22);
            this.mnuViewInExplorer.Text = "View in Explorer";
            this.mnuViewInExplorer.Click += this.mnuViewInExplorer_Click;

            // mnuGridEdit
            this.mnuGridEdit.Name = "mnuGridEdit";
            this.mnuGridEdit.Size = new System.Drawing.Size(220, 22);
            this.mnuGridEdit.Text = "Grid View Edit";
            this.mnuGridEdit.Click += this.mnuGridEdit_OnClick;

            // mnuSeparator4
            this.mnuSeparator4.Name = "mnuSeparator4";
            this.mnuSeparator4.Size = new System.Drawing.Size(217, 6);

            // mnuExpand
            this.mnuExpand.Name = "mnuExpand";
            this.mnuExpand.Size = new System.Drawing.Size(220, 22);
            this.mnuExpand.Text = "Expand to Objects";
            this.mnuExpand.Click += this.mnuExpand_Click;

            // mnuExpandXml
            this.mnuExpandXml.Name = "mnuExpandXml";
            this.mnuExpandXml.Size = new System.Drawing.Size(220, 22);
            this.mnuExpandXml.Text = "View Expanded XML";
            this.mnuExpandXml.Click += this.mnuExpandXml_Click;

            // iconList
            this.iconList.TransparentColor = Color.Transparent;

            // m_Toolbar
            this.m_toolbar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_toolbar.Buttons.AddRange(
                new[] { this.btnCopy, this.btnDelete, this.btnRename, this.btnExpandFolders, this.btnCollapse });
            this.m_toolbar.ButtonSize = new System.Drawing.Size(16, 16);
            this.m_toolbar.DropDownArrows = true;
            this.m_toolbar.ImageList = this.toolBarIcons;
            this.m_toolbar.Location = new System.Drawing.Point(0, 0);
            this.m_toolbar.Name = "m_toolbar";
            this.m_toolbar.ShowToolTips = true;
            this.m_toolbar.Size = new System.Drawing.Size(360, 29);
            this.m_toolbar.TabIndex = 4;
            this.m_toolbar.ButtonClick += this.Toolbar_ButtonClick;

            // btnCopy
            this.btnCopy.ImageIndex = 0;
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Tag = "copy";
            this.btnCopy.ToolTipText = "Duplicate";

            // btnDelete
            this.btnDelete.ImageIndex = 1;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Tag = "delete";
            this.btnDelete.ToolTipText = "Delete";

            // btnRename
            this.btnRename.ImageIndex = 2;
            this.btnRename.Name = "btnRename";
            this.btnRename.Tag = "rename";
            this.btnRename.ToolTipText = "Rename";

            // btnExpandFolders
            this.btnExpandFolders.ImageIndex = 3;
            this.btnExpandFolders.Name = "btnExpandFolders";
            this.btnExpandFolders.Tag = "expand";
            this.btnExpandFolders.ToolTipText = "Expand Folders";

            // btnCollapse
            this.btnCollapse.ImageIndex = 4;
            this.btnCollapse.Name = "btnCollapse";
            this.btnCollapse.Tag = "collapse";
            this.btnCollapse.ToolTipText = "Collapse Names";

            // toolBarIcons
            this.toolBarIcons.ImageStream =
                (System.Windows.Forms.ImageListStreamer)(resources.GetObject("toolBarIcons.ImageStream"));
            this.toolBarIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.toolBarIcons.Images.SetKeyName(0, String.Empty);
            this.toolBarIcons.Images.SetKeyName(1, String.Empty);
            this.toolBarIcons.Images.SetKeyName(2, String.Empty);
            this.toolBarIcons.Images.SetKeyName(3, String.Empty);
            this.toolBarIcons.Images.SetKeyName(4, String.Empty);

            // m_TreeView
            this.m_treeView.AllowDrop = true;
            this.m_treeView.ContextMenuStrip = this.mnuRightClick;
            this.m_treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_treeView.HideSelection = false;
            this.m_treeView.ImageIndex = 0;
            this.m_treeView.ImageList = this.iconList;
            this.m_treeView.ItemHeight = 18;
            this.m_treeView.LastSelectedNode = null;
            this.m_treeView.Location = new System.Drawing.Point(0, 29);
            this.m_treeView.Name = "m_treeView";
            this.m_treeView.SelectedImageIndex = 0;
            this.m_treeView.Size = new System.Drawing.Size(360, 371);
            this.m_treeView.Sorted = true;
            this.m_treeView.TabIndex = 2;
            this.m_treeView.DoubleClick += this.TreeView_DoubleClick;
            this.m_treeView.DragDrop += this.TreeView_DragDrop;
            this.m_treeView.DragEnter += this.TreeView_DragEnter;
            this.m_treeView.KeyDown += this.ctrlObjectBrowser_KeyDown;
            this.m_treeView.ItemDrag += this.TreeView_ItemDrag;
            this.m_treeView.DragOver += this.TreeView_DragOver;

            // ctrlObjectBrowser
            this.Controls.Add(this.m_treeView);
            this.Controls.Add(this.m_toolbar);
            this.Name = "ctrlObjectBrowser";
            this.Size = new System.Drawing.Size(360, 400);
            this.mnuRightClick.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        /// <summary>
        /// The load icons.
        /// </summary>
        private void LoadDefaultIcons()
        {
            // Load default store icons
            RaveUtils.LoadIcons(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "icons\\basefolder"), this.m_objectTypeIconTable, this.iconList);
            RaveUtils.LoadIcons(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "icons\\folder"), this.m_objectTypeIconTable, this.iconList);
            RaveUtils.LoadIcons(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "icons\\store"), this.m_objectTypeIconTable, this.iconList);
            RaveUtils.LoadIcons(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "icons\\template"), this.m_objectTypeIconTable, this.iconList);
        }

        private void LoadMetadataTypeIcons()
        {
            // Load metadata type icons
            foreach (audMetadataType metadataType in Configuration.MetadataTypes)
            {
                if (metadataType.Type == this.m_type)
                {
                    RaveUtils.LoadTypeIcons(
                        Path.Combine(AppDomain.CurrentDomain.BaseDirectory, metadataType.IconPath),
                        this.m_objectTypeIconTable,
                        this.iconList,
                        RaveInstance.AllTypeDefinitions[this.m_type],
                        true);

                    break;
                }
            }
        }

        /// <summary>
        /// The multi edit_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MultiEdit_Click(object sender, EventArgs e)
        {
            var objInstances = new List<IObjectInstance>();

            foreach (BaseNode bn in this.m_treeView.SelectedItems)
            {
                var sn = bn as SoundNode;
                if (sn != null && sn.ObjectInstance as ITemplateInstance == null)
                {
                    objInstances.Add(sn.ObjectInstance);
                }
            }

            if (objInstances.Count == 0)
            {
                MessageBox.Show(
                    "No object instances in selection",
                    "Invalid Selection",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            if (objInstances.Count < this.m_treeView.SelectedItems.Count)
            {
                MessageBox.Show(
                    RaveInstance.ActiveWindow,
                    string.Format(
                        "Warning: {0} non-object instance(s) in selection will not be included in group edit",
                        this.m_treeView.SelectedItems.Count - objInstances.Count),
                    "Warning",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }

            var multiEditor = new frmClipboardEditor();
            if (multiEditor.Init(objInstances))
            {
                multiEditor.Show();
            }
        }

        /// <summary>
        /// The paste base settings.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <param name="pasteType">
        /// The paste type.
        /// </param>
        private void PasteBaseSettings(ArrayList nodes, string pasteType)
        {
            foreach (TreeNode n in nodes)
            {
                var sn = n as SoundNode;
                if (null == sn)
                {
                    continue;
                }

                string name = sn.ObjectInstance.Name;
                var id = new ObjectLookupId(sn.Type, sn.Episode);
                TypeUtils.ApplyObjectClipboardToObject(sn.ObjectInstance, pasteType);

                // needed if bank is not checked out before paste
                sn.ObjectInstance = RaveInstance.ObjectLookupTables[id][name];
            }

            // force update of last selected node
            var soundNode = this.m_treeView.LastSelectedNode as SoundNode;
            if (soundNode != null)
            {
                if (this.OnSoundSelect != null)
                {
                    this.OnSoundSelect(soundNode.ObjectInstance);
                }
            }
        }

        /// <summary>
        /// The paste base settings_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void PasteBaseSettings_Click(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;
            string value = menuItem.Text.ToLower().Equals("main") ? null : menuItem.Text;
            value = menuItem.Text.ToLower().Equals("all") ? string.Empty : value;
            // paste base settings
            this.PasteBaseSettings(this.m_treeView.SelectedItems, value);
        }

        /// <summary>
        /// The process dropped sound node.
        /// </summary>
        /// <param name="targetSoundBankNode">
        /// The target sound bank node.
        /// </param>
        /// <param name="droppedSoundNode">
        /// The dropped sound node.
        /// </param>
        /// <param name="virtualFolderName">
        /// The virtual folder name.
        /// </param>
        /// <param name="updateUi">
        /// The update ui.
        /// </param>
        private void ProcessDroppedSoundNode(
            SoundBankNode targetSoundBankNode, SoundNode[] droppedSoundNodes, string virtualFolderName)
        {
            IObjectBank targetBank = targetSoundBankNode.ObjectBank;
            targetSoundBankNode.BeginUpdate();
            try
            {
                foreach (SoundNode droppedSoundNode in droppedSoundNodes)
                {
                    SoundBankNode droppedSoundBankNode = droppedSoundNode.Parent as SoundBankNode
                                                         ?? droppedSoundNode.Parent.Parent as SoundBankNode;

                    bool areSameType = droppedSoundNode.Type.ToUpper() == targetBank.Type.ToUpper();
                    bool areSameEpisodeOrBase = (droppedSoundNode.Episode == ObjectLookupId.Base)
                                                || (droppedSoundNode.Episode == targetBank.Episode);
                    bool areInTest = targetBank.Episode == ObjectLookupId.Test;
                    if (areSameType && (areSameEpisodeOrBase || areInTest))
                    {
                        if (
                            !droppedSoundNode.ObjectInstance.Type.Equals("Categories", StringComparison.InvariantCultureIgnoreCase) &&
                            droppedSoundNode.ObjectInstance.Referencers.OfType<IEpisodable>()
                                .Any(episodeInstance => episodeInstance.Episode != targetBank.Episode))
                        {
                            ErrorManager.HandleInfo(
                                "Sound node is referenced by sounds in another episode, cannot be moved");
                            return;
                        }

                        if (Control.ModifierKeys == Keys.Control)
                        {
                            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(
                                this.ParentForm, "New object name", droppedSoundNode.ObjectInstance.Name);
                            if (input.HasBeenCancelled) return;
                            string newName = input.data;

                            while (ObjectInstance.CheckForNameAndHashCollision(newName, targetBank.Episode))
                            {
                                input = frmSimpleInput.GetInput(this.ParentForm, "Name in use or hash collision, name", newName);
                                if (input.HasBeenCancelled) return;
                                newName = input.data;
                            }

                            if (TypeUtils.CopyObjectToBank(droppedSoundNode.ObjectInstance, targetBank, newName,
                                virtualFolderName))
                            {
                                targetSoundBankNode.RecreateSoundNodes();
                            }
                            else
                            {
                                //checkout cancelled
                                break;
                            }
                        }
                        else
                        {
                            if (TypeUtils.MoveObjectToBank(droppedSoundNode.ObjectInstance, targetBank,
                                virtualFolderName))
                            {
                                //update tree after all nodes have been moved
                                if (droppedSoundNodes.Last() == droppedSoundNode)
                                {
                                    targetSoundBankNode.RecreateSoundNodes();
                                    droppedSoundBankNode.RecreateSoundNodes();
                                }
                            }
                            else
                            {
                                //checkout cancelled
                                break;
                            }
                        }
                    }
                    else
                    {
                        ErrorManager.HandleInfo(
                            "Operation not allowed. Please check the object's type and episode properties");
                    }
                }
            }
            finally
            {
                targetSoundBankNode.EndUpdate();
            }
        }

        /// <summary>
        /// The process sound node dropped on sound bank node.
        /// </summary>
        /// <param name="targetSoundBankNode">
        /// The target sound bank node.
        /// </param>
        /// <param name="droppedSoundNode">
        /// The dropped sound node.
        /// </param>
        /// <param name="updateUi">
        /// The update ui.
        /// </param>
        private void ProcessSoundNodeDroppedOnSoundBankNode(
            SoundBankNode targetSoundBankNode, SoundNode[] droppedSoundNodes)
        {
            this.ProcessDroppedSoundNode(targetSoundBankNode, droppedSoundNodes, null);
        }

        /// <summary>
        /// The process sound node dropped on sound node.
        /// </summary>
        /// <param name="targetSoundNode">
        /// The target sound node.
        /// </param>
        /// <param name="droppedSoundNode">
        /// The dropped sound node.
        /// </param>
        private void ProcessSoundNodeDroppedOnSoundNode(SoundNode targetSoundNode, SoundNode[] droppedSoundNodes)
        {

            var targetBank = targetSoundNode.ObjectInstance.Bank;
            string targetSoundName = targetSoundNode.ObjectInstance.Name;

            if (!targetBank.TryToCheckout())
            {
                return;
            }

            foreach (SoundNode droppedSoundNode in droppedSoundNodes)
            {
                string droppedSoundName = droppedSoundNode.ObjectInstance.Name;
                var droppedBank = droppedSoundNode.ObjectInstance.Bank;

                // grab newly checked-out object instances
                IObjectInstance targetObject = targetBank.FindObjectInstance(targetSoundName);
                IObjectInstance droppedObject = droppedBank.FindObjectInstance(droppedSoundName);

                if (targetObject != null && droppedSoundNode.ObjectInstance != null)
                {
                    targetObject.HandleDroppedObject(droppedObject, this.m_typeDefinitions);
                }
            }
        }

        /// <summary>
        /// The process sound node dropped on virtual folder node.
        /// </summary>
        /// <param name="targetVirtualFolderNode">
        /// The target virtual folder node.
        /// </param>
        /// <param name="droppedSoundNode">
        /// The dropped sound node.
        /// </param>
        /// <param name="updateUi">
        /// The update ui.
        /// </param>
        private void ProcessSoundNodeDroppedOnVirtualFolderNode(
            VirtualFolderNode targetVirtualFolderNode, SoundNode[] droppedSoundNodes)
        {
            this.ProcessDroppedSoundNode(
                targetVirtualFolderNode.Parent as SoundBankNode,
                droppedSoundNodes,
                targetVirtualFolderNode.ObjectName);
        }

        /// <summary>
        /// The refresh tree.
        /// </summary>
        private void RefreshTree()
        {
            this.m_treeView.BeginUpdate();
            this.m_treeView.Nodes.Clear();

            foreach (var bank in this.xmlBankManager.ObjectBanks)
            {
                this.AddNodesForBank(bank);
            }

            foreach (FolderNode fn in FlattenCollection(m_treeView.Nodes).OfType<FolderNode>())
            {
                fn.SetBankFolderIcon();
            }
            this.m_treeView.EndUpdate();
        }

        public static IEnumerable<TreeNode> FlattenCollection(TreeNodeCollection coll)
        {
            return coll.Cast<TreeNode>().Concat(coll.Cast<TreeNode>().SelectMany(x => FlattenCollection(x.Nodes)));
        }
        /// <summary>
        /// The rename object.
        /// </summary>
        private void RenameObject()
        {
            var soundBankNodeLookup = new Dictionary<string, SoundBankNode>();
            foreach (TreeNode t in this.m_treeView.SelectedItems)
            {
                var sn = t as SoundNode;
                if (sn != null)
                {
                    SoundBankNode sbn = sn.Parent as SoundBankNode ?? sn.Parent.Parent as SoundBankNode;
                    soundBankNodeLookup.Add(sn.ObjectInstance.Name, sbn);
                }
            }

            foreach (var entry in soundBankNodeLookup)
            {
                if (this.TryToCheckOutBank(entry.Value))
                {
                    SoundNode sn = FindSoundNode(entry.Value, entry.Key);
                    frmSimpleInput.InputResult input = frmSimpleInput.GetInput(
                        this.ParentForm, "New object name for " + sn.ObjectName, sn.ObjectInstance.Name);
                    if (input.HasBeenCancelled) return;
                    string newName = input.data;

                    var id = new ObjectLookupId(entry.Value.Type, entry.Value.Episode);
                    while (newName != null && ObjectInstance.CheckForNameAndHashCollision(newName, sn.Episode))
                    {
                        input = frmSimpleInput.GetInput(this.ParentForm, "Name taken or hash collision, new name", newName);
                        if (input.HasBeenCancelled) return;
                        newName = input.data;
                    }

                    if (newName != null
                        && ErrorManager.HandleQuestion(
                            "Are you sure you want to rename this object?", MessageBoxButtons.YesNo, DialogResult.Yes)
                        == DialogResult.Yes)
                    {
                        try
                        {
                            sn.ObjectInstance.Rename(newName);
                            MuteSoloManager.Instance.Rename(sn.ObjectName, sn.ObjectInstance.TypeName, newName);
                            sn.ObjectName = newName;
                            entry.Value.RecreateSoundNodes();
                        }
                        catch (Exception ex)
                        {
                            ErrorManager.HandleInfo("Rename failed: " + ex.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The rename virtual folder.
        /// </summary>
        private void RenameVirtualFolder()
        {
            var vfn = this.m_treeView.LastSelectedNode as VirtualFolderNode;
            if (vfn != null)
            {
                var sbn = vfn.Parent as SoundBankNode;
                if (this.TryToCheckOutBank(sbn))
                {
                    string folderName = vfn.ObjectName;

                    List<string> virtualFolderNames =
                        (from BaseNode baseNode in sbn.Nodes
                         where baseNode is VirtualFolderNode
                         select baseNode.ObjectName).ToList();

                    frmSimpleInput.InputResult input = frmSimpleInput.GetInput(
                        this.ParentForm, "New virtual folder name for " + folderName, folderName);
                    if (input.HasBeenCancelled) return;
                    string newFolderName = input.data;

                    while (!string.IsNullOrEmpty(newFolderName) && virtualFolderNames.Contains(newFolderName))
                    {
                        input = frmSimpleInput.GetInput(
                            this.ParentForm, "Virtual folder exists already, choose a new name", newFolderName);
                        if (input.HasBeenCancelled) return;
                        newFolderName = input.data;
                    }


                    vfn = (from BaseNode baseNode in sbn.Nodes
                           where baseNode is VirtualFolderNode && baseNode.ObjectName == folderName
                           select baseNode as VirtualFolderNode).First();
                    foreach (SoundNode soundNode in vfn.Nodes)
                    {
                        soundNode.ObjectInstance.VirtualFolderName = newFolderName;
                    }

                    vfn.ObjectName = newFolderName;
                    vfn.Text = newFolderName;
                }
            }
        }

        /// <summary>
        /// The revert.
        /// </summary>
        private void Revert()
        {
            if (ErrorManager.HandleQuestion(
                "Are you sure you want to revert the selected object stores?", MessageBoxButtons.YesNo, DialogResult.Yes)
                == DialogResult.Yes)
            {
                foreach (BaseNode bn in this.m_treeView.SelectedItems)
                {
                    var sbn = bn as SoundBankNode;
                    if (sbn != null)
                    {

                        MuteSoloManager.Instance.OnRevert(sbn.ObjectBank);
                        if (!sbn.ObjectBank.Revert())
                        {
                            ErrorManager.HandleInfo("Failed to revert bank.");
                        }
                        else
                        {
                            sbn.RecreateSoundNodes();
                        }
                    }
                }

                this.ChangelistModified.Raise();
            }
        }

        /// <summary>
        /// The save tree state.
        /// </summary>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private void SaveTreeState(TreeNodeCollection treeNodes)
        {
            this.m_nodeState.DocumentElement.RemoveAll();
            SaveTreeState(this.m_nodeState.DocumentElement, treeNodes);
        }

        /// <summary>
        /// The search form_ on find object.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        private void SearchForm_OnFindObject(IObjectInstance obj)
        {
            this.FindObject(obj);
        }

        /// <summary>
        /// The show busy.
        /// </summary>
        private void ShowBusy()
        {
            var busy = new frmBusy();
            busy.SetStatusText(BusyText);
            busy.SetUnknownDuration(true);
            busy.Show();
            while (!this.m_finished)
            {
                if (BusyKnownProgress)
                {
                    busy.SetUnknownDuration(false);
                    busy.SetProgress(BusyProgress);
                }

                busy.SetStatusText(BusyText);
                Application.DoEvents();
            }

            busy.FinishedWorking();
            BusyText = "Working...";
            this.m_finished = false;
            BusyProgress = 0;
        }

        /// <summary>
        /// The sound bank node_ on bank status changed.
        /// </summary>
        private void SoundBankNode_OnBankStatusChanged()
        {
            // ChangeListModified.Raise();
        }

        /// <summary>
        /// The toolbar_ button click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Toolbar_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            switch (e.Button.Tag.ToString())
            {
                case "copy":
                    this.HandleCopy();
                    break;
                case "delete":
                    this.HandleDelete();
                    break;
                case "rename":
                    if ((this.m_treeView.LastSelectedNode as SoundNode) != null)
                    {
                        this.RenameObject();
                    }
                    else if ((this.m_treeView.LastSelectedNode as VirtualFolderNode) != null)
                    {
                        this.RenameVirtualFolder();
                    }

                    break;
                case "expand":
                    this.ExpandToCheckedOutBanks(this.m_treeView.Nodes);
                    break;
                case "collapse":
                    this.btnCollapse.Pushed = !this.m_collapse;
                    this.CollapseNames(!this.m_collapse);
                    break;
            }
        }

        /// <summary>
        /// The tree view_ double click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DoubleClick(object sender, EventArgs e)
        {
            var node = this.m_treeView.LastSelectedNode as SoundNode;

            if (node != null)
            {
                this.OnSoundSelect.Raise(node.ObjectInstance);
            }
        }

        /// <summary>
        /// The tree view_ drag drop.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DragDrop(object sender, DragEventArgs e)
        {
            if (this.m_treeView.LastSelectedNode != null)
            {
                if (e.Data.GetDataPresent(typeof(FolderNode[]))
                    && this.m_treeView.LastSelectedNode.GetType() == typeof(FolderNode))
                {
                    var folderNodes = (FolderNode[])e.Data.GetData(typeof(FolderNode[]));

                    var sb = new StringBuilder();
                    sb.AppendLine("Moving the following folders:");
                    foreach (FolderNode folderNode in folderNodes)
                    {
                        sb.AppendLine(folderNode.FullPath);
                    }

                    sb.AppendLine();
                    sb.AppendLine("to :");
                    sb.Append(this.m_treeView.LastSelectedNode.FullPath);

                    if (MessageBox.Show(
                        RaveInstance.ActiveWindow,
                        sb.ToString(),
                        "Warning",
                        MessageBoxButtons.OKCancel,
                        MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        RaveInstance.HierarchyBrowser.BeginUpdate();
                        foreach (FolderNode folderNode in folderNodes)
                        {
                            this.HandleDroppedFolder(this.m_treeView.LastSelectedNode, folderNode);
                        }

                        RaveInstance.HierarchyBrowser.EndUpdate();
                    }
                }
                else if (e.Data.GetDataPresent(typeof(VirtualFolderNode[]))
                         && this.m_treeView.LastSelectedNode.GetType() == typeof(SoundBankNode))
                {
                    var targetSoundBankNode = this.m_treeView.LastSelectedNode as SoundBankNode;
                    if (this.TryToCheckOutBank(targetSoundBankNode))
                    {
                        var vfns = (VirtualFolderNode[])e.Data.GetData(typeof(VirtualFolderNode[]));

                        var sb = new StringBuilder();
                        sb.AppendLine("Moving the following virtual folders: ");
                        foreach (VirtualFolderNode vfn in vfns)
                        {
                            sb.AppendLine(vfn.FullPath);
                        }

                        sb.AppendLine();
                        sb.AppendLine("to :");
                        sb.Append(this.m_treeView.LastSelectedNode.FullPath);

                        if (MessageBox.Show(
                            RaveInstance.ActiveWindow,
                            sb.ToString(),
                            "Warning",
                            MessageBoxButtons.OKCancel,
                            MessageBoxIcon.Warning) == DialogResult.OK)
                        {
                            RaveInstance.HierarchyBrowser.BeginUpdate();

                            List<SoundBankNode> sourceSoundBankNodes =
                                (from vfn in vfns select vfn.Parent as SoundBankNode).ToList();
                            foreach (VirtualFolderNode vfn in vfns)
                            {
                                var parent = vfn.Parent as SoundBankNode;
                                if (parent != null && this.TryToCheckOutBank(parent))
                                {
                                    foreach (TreeNode node in parent.Nodes)
                                    {
                                        if (node.GetType() == typeof(VirtualFolderNode) && node.Text == vfn.Text)
                                        {
                                            this.HandleDroppedVirtualFolder(
                                                targetSoundBankNode, node as VirtualFolderNode);
                                            break;
                                        }
                                    }
                                }
                            }

                            foreach (SoundBankNode sourceSoundBankNode in sourceSoundBankNodes)
                            {
                                sourceSoundBankNode.RecreateSoundNodes();
                            }

                            targetSoundBankNode.RecreateSoundNodes();

                            RaveInstance.HierarchyBrowser.EndUpdate();
                        }
                    }
                }
                else if (e.Data.GetDataPresent(typeof(SoundBankNode[]))
                         && this.m_treeView.LastSelectedNode.GetType() == typeof(FolderNode))
                {
                    var sbns = (SoundBankNode[])e.Data.GetData(typeof(SoundBankNode[]));

                    var sb = new StringBuilder();
                    sb.AppendLine("Moving the following sound bankManager: ");
                    foreach (SoundBankNode sbn in sbns)
                    {
                        sb.AppendLine(sbn.FullPath);
                    }

                    sb.AppendLine();
                    sb.AppendLine("to :");
                    sb.Append(this.m_treeView.LastSelectedNode.FullPath);

                    if (MessageBox.Show(
                        RaveInstance.ActiveWindow, sb.ToString(), "Warning", MessageBoxButtons.OKCancel)
                        == DialogResult.OK)
                    {
                        RaveInstance.HierarchyBrowser.BeginUpdate();

                        foreach (SoundBankNode sbn in sbns)
                        {
                            this.HandleDroppedSoundBank(this.m_treeView.LastSelectedNode, sbn);
                        }

                        RaveInstance.HierarchyBrowser.EndUpdate();
                    }
                }
                else if (e.Data.GetDataPresent(typeof(SoundNode)))
                {
                    var droppedNode = (SoundNode)e.Data.GetData(typeof(SoundNode));
                    this.HandleDroppedSoundNode(this.m_treeView.LastSelectedNode, new SoundNode[] { droppedNode });
                }
                else if (e.Data.GetDataPresent(typeof(SoundNode[])))
                {
                    var droppedNodes = (SoundNode[])e.Data.GetData(typeof(SoundNode[]));
                    var nodeList = new List<SoundNode>();

                    RaveInstance.HierarchyBrowser.BeginUpdate();

                    // Order Nodes so those with no referencing sounds are moved first.
                    // These should be the "root" of the object heirarchy
                    foreach (var droppedNode in droppedNodes)
                    {
                        if (droppedNode.ObjectInstance.Referencers.Count == 0)
                        {
                            nodeList.Insert(0, droppedNode);
                        }
                        else
                        {
                            nodeList.Add(droppedNode);
                        }
                    }

                    Dictionary<SoundNode, SoundBankNode> soundBankLookup =
                        nodeList.ToDictionary(
                            node => node,
                            node => node.Parent as SoundBankNode ?? node.Parent.Parent as SoundBankNode);
                    nodeList = BuildNodeList(nodeList, soundBankLookup);

                    this.HandleDroppedSoundNode(this.m_treeView.LastSelectedNode, nodeList.ToArray());

                    RaveInstance.HierarchyBrowser.EndUpdate();
                }
                else if (e.Data.GetDataPresent(typeof(WaveNode)))
                {
                    var w = (WaveNode)e.Data.GetData(typeof(WaveNode));
                    this.HandleDroppedWave(w);
                }
                else if (e.Data.GetDataPresent(typeof(WaveNode[])))
                {
                    RaveInstance.HierarchyBrowser.BeginUpdate();

                    var waves = (WaveNode[])e.Data.GetData(typeof(WaveNode[]));
                    foreach (WaveNode w in waves)
                    {
                        this.HandleDroppedWave(w);
                    }

                    RaveInstance.HierarchyBrowser.EndUpdate();
                }
                else
                {
                    WaveContainerNode container = FindWaveContainerNode(e.Data);
                    if (container != null)
                    {
                        RaveInstance.HierarchyBrowser.BeginUpdate();

                        foreach (WaveNode w in container.GetChildWaveNodes())
                        {
                            this.HandleDroppedWave(w);
                        }

                        RaveInstance.HierarchyBrowser.EndUpdate();
                    }
                }

                // update tree names
                this.m_treeView.BeginUpdate();
                this.DoCollapse(this.m_treeView.Nodes);
                this.m_treeView.EndUpdate();
            }
        }

        /// <summary>
        /// The tree view_ drag enter.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        /// <summary>
        /// The tree view_ drag over.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
            Point p = this.m_treeView.PointToClient(new Point(e.X, e.Y));
            this.m_treeView.LastSelectedNode = this.m_treeView.GetNodeAt(p.X, p.Y);
        }

        /// <summary>
        /// The tree view_ item drag.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            var al = new ArrayList();
            if (e.Item.GetType() == typeof(SoundBankNode))
            {
                foreach (TreeNode tn in this.m_treeView.SelectedItems)
                {
                    if (tn as SoundBankNode != null)
                    {
                        al.Add(tn);
                    }
                }

                this.DoDragDrop(al.ToArray(typeof(SoundBankNode)), DragDropEffects.Link);
            }
            else if (e.Item.GetType() == typeof(SoundNode))
            {
                foreach (TreeNode tn in this.m_treeView.SelectedItems)
                {
                    if (tn as SoundNode != null)
                    {
                        al.Add(tn);
                    }
                }
                //TODO: Create a helper method and change all drag and drop to this
                DataObject dataObject = new DataObject();
                dataObject.SetData(al.ToArray(typeof(SoundNode)));
                dataObject.SetData(new rage.ToolLib.DataContainer(al.ToArray(typeof(SoundNode))));
                this.DoDragDrop(dataObject, DragDropEffects.Link);
            }
            else if (e.Item.GetType() == typeof(FolderNode))
            {
                foreach (TreeNode tn in this.m_treeView.SelectedItems)
                {
                    if (tn as FolderNode != null)
                    {
                        al.Add(tn);
                    }
                }

                this.DoDragDrop(al.ToArray(typeof(FolderNode)), DragDropEffects.Link);
            }
            else if (e.Item.GetType() == typeof(VirtualFolderNode))
            {
                foreach (TreeNode tn in this.m_treeView.SelectedItems)
                {
                    if (tn as VirtualFolderNode != null)
                    {
                        al.Add(tn);
                    }
                }

                this.DoDragDrop(al.ToArray(typeof(VirtualFolderNode)), DragDropEffects.Link);
            }
        }

        /// <summary>
        /// The try to check out bank.
        /// </summary>
        /// <param name="sn">
        /// The sn.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool TryToCheckOutBank(ref SoundNode sn)
        {
            if (sn != null)
            {
                string name = sn.ObjectInstance.Name;
                SoundBankNode sbn = sn.Parent as SoundBankNode ?? sn.Parent.Parent as SoundBankNode;
                if (this.TryToCheckOutBank(sbn))
                {
                    sn = FindSoundNode(sbn, name);
                    this.ChangelistModified.Raise();
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// The try to check out bank.
        /// </summary>
        /// <param name="sbn">
        /// The sbn.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool TryToCheckOutBank(SoundBankNode sbn)
        {
            if (sbn == null)
            {
                return false;
            }

            if (sbn.ObjectBank.IsCheckedOut)
            {
                return true;
            }

            if (sbn.ObjectBank.TryToCheckout())
            {
                sbn.RecreateSoundNodes();
                this.ChangelistModified.Raise();
                return true;
            }

            return false;
        }

        /// <summary>
        /// The utility_ new object created.
        /// </summary>
        /// <param name="newSound">
        /// The new sound.
        /// </param>
        private void Utility_NewObjectCreated(IObjectInstance newSound)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<IObjectInstance>(this.Utility_NewObjectCreated), new object[] { newSound });
            }
            else
            {
                if (newSound.TypeDefinitions != this.m_typeDefinitions)
                {
                    // we don't deal with objects of this type family
                    return;
                }

                // figure out where this sound sits in our hierarchy and add it to the tree
                string path = newSound.Bank.FilePath.ToUpper().Replace(@"\\", "\\");

                // strip off the base path
                path = path.Replace(Configuration.ObjectXmlPath.ToUpper(), String.Empty);

                string[] pathElements = path.Split('\\');

                TreeNode currentNode = null;

                for (int i = 0; i < pathElements.Length; i++)
                {
                    if (currentNode == null)
                    {
                        foreach (TreeNode tn in this.m_treeView.Nodes)
                        {
                            if (i == pathElements.Length - 1)
                            {
                                if (tn.GetType() == typeof(SoundBankNode)
                                    && ((BaseNode)tn).ObjectName.ToUpper()
                                    == pathElements[i].ToUpper().Replace(".XML", string.Empty))
                                {
                                    currentNode = tn;
                                    break;
                                }
                            }
                            else
                            {
                                if (tn.GetType() == typeof(FolderNode)
                                    && ((BaseNode)tn).ObjectName.ToUpper() == pathElements[i])
                                {
                                    currentNode = tn;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (TreeNode tn in currentNode.Nodes)
                        {
                            if (i == pathElements.Length - 1)
                            {
                                if (tn.GetType() == typeof(SoundBankNode)
                                    && ((BaseNode)tn).ObjectName.ToUpper()
                                    == pathElements[i].ToUpper().Replace(".XML", string.Empty))
                                {
                                    currentNode = tn;
                                    break;
                                }
                            }
                            else
                            {
                                if (tn.GetType() == typeof(FolderNode)
                                    && ((BaseNode)tn).ObjectName.ToUpper() == pathElements[i])
                                {
                                    currentNode = tn;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (currentNode == null)
                {
                    // this happens since curves/sounds arent split properly (so a new sound
                    // path wont be found in the curve browser)
                    return;
                }

                if (newSound.VirtualFolderName != null)
                {
                    TreeNode folderNode = null;
                    foreach (TreeNode tn in currentNode.Nodes)
                    {
                        if (tn.GetType() == typeof(VirtualFolderNode) && tn.Text == newSound.VirtualFolderName)
                        {
                            folderNode = tn;
                            break;
                        }
                    }

                    if (folderNode == null)
                    {
                        folderNode = new VirtualFolderNode(newSound.VirtualFolderName, newSound.Episode, newSound.Type, this.m_objectTypeIconTable);
                        currentNode.Nodes.Add(folderNode);
                    }

                    currentNode = folderNode;
                }

                currentNode.Nodes.Add(new SoundNode(newSound, (int)this.m_objectTypeIconTable[newSound.TypeName], this.m_objectTypeIconTable));
                currentNode.Expand();
                this.m_treeView.BeginUpdate();
                this.DoCollapse(currentNode.Nodes);
                this.m_treeView.EndUpdate();
            }
        }

        /// <summary>
        /// The ctrl object browser_ key down.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ctrlObjectBrowser_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Left:
                    {
                        var sn = this.m_treeView.LastSelectedNode as BaseNode;
                        if (sn != null)
                        {
                            if (!sn.IsExpanded && sn.Parent != null)
                            {
                                var parent = sn.Parent as BaseNode;
                                parent.EnsureVisible();
                                this.m_treeView.LastSelectedNode = parent;
                            }
                        }
                    }

                    break;
                case Keys.Right:
                    {
                        var sn = this.m_treeView.LastSelectedNode as BaseNode;
                        if (sn != null)
                        {
                            if (sn.IsExpanded && sn.Nodes.Count > 0)
                            {
                                var child = sn.Nodes[0] as BaseNode;
                                child.EnsureVisible();
                                this.m_treeView.LastSelectedNode = child;
                            }
                        }
                    }

                    break;
                case Keys.Down:
                case Keys.Up:
                    {
                        var sn = this.m_treeView.LastSelectedNode as BaseNode;
                        if (sn != null)
                        {
                            sn.EnsureVisible();
                        }
                    }

                    break;

                case Keys.Space | Keys.Control:
                    {
                        if (this.StopSoundAudition != null)
                        {
                            var selectedSoundNode = this.m_treeView.LastSelectedNode as SoundNode;
                            if (selectedSoundNode != null)
                            {
                                this.StopSoundAudition(selectedSoundNode.ObjectInstance);
                            }
                        }

                        break;
                    }

                case Keys.Space:
                    {
                        this.mnuAudition_Click(null, null);
                        break;
                    }

                case Keys.Delete:
                    {
                        this.HandleDelete();
                        break;
                    }

                case Keys.F2:
                    {
                        if ((this.m_treeView.LastSelectedNode as SoundNode) != null)
                        {
                            this.RenameObject();
                        }
                        else if ((this.m_treeView.LastSelectedNode as VirtualFolderNode) != null)
                        {
                            this.RenameVirtualFolder();
                        }

                        break;
                    }

                case Keys.Enter:
                    {
                        var selectedSound = this.m_treeView.LastSelectedNode as SoundNode;
                        if (selectedSound != null)
                        {
                            if (this.OnSoundSelect != null)
                            {
                                this.OnSoundSelect(selectedSound.ObjectInstance);
                            }
                        }

                        break;
                    }

                case Keys.D | Keys.Control:
                    {
                        this.DuplicateObject();
                        break;
                    }
                case Keys.F | Keys.Control:
                    {
                        TreeNode node = this.m_treeView.LastSelectedNode;

                        if (node == null)
                        {
                            UIControls.AutoComplete.ObjectNameBox.Create(this.m_typeDefinitions.ObjectTypes.Select(p => p.Name),
                        p => FindObject(ObjectInstance.GetObjectMatchingName(p)));

                            break;
                        }

                        while (node.Parent != null)
                        {
                            node = node.Parent;
                        }

                        UIControls.AutoComplete.ObjectNameBox.Create(this.m_typeDefinitions.ObjectTypes.Select(p => p.Name), node.Text,
                            p => SelectObject(p, node)
                            );


                        break;
                    }
                default:
                    {
                        TreeNode topNode = m_treeView.LastSelectedNode ?? m_treeView.TopNode;

                        TreeNode node = FindTreeNodeHelper(topNode, GetChar(e));
                        node = node ?? FindTreeNodeHelper(topNode.Parent, GetChar(e));
                        if (node != null)
                        {
                            node.EnsureVisible();
                            m_treeView.LastSelectedNode = node;
                        }
                        break;
                    }
            }
        }

        private void SelectObject(string name, TreeNode node)
        {
            SoundNode x = FindObjectNodeWithObjectName(name, node.Nodes);
            if (x != null)
            {
                FindObject(x.ObjectInstance);
            }
        }

        private char GetChar(KeyEventArgs e)
        {
            int keyValue = e.KeyValue;
            if (keyValue >= (int)Keys.A && keyValue <= (int)Keys.Z)
                return (char)(keyValue + 32);
            return (char)keyValue;
        }
        /// <summary>
        /// The menu copy base settings_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void menuCopyBaseSettings_Click(object sender, EventArgs e)
        {
            // copy base settings
            if (this.m_treeView.SelectedItems.Count != 1)
            {
                ErrorManager.HandleInfo("Warning: Only the first selected node will be copied");
            }

            var current = ((SoundNode)this.m_treeView.SelectedItems[0]).ObjectInstance;

            TypeUtils.ObjectClipboard = new TypedObjectInstance(
                current.Node.Clone(),
                null,
                current.Bank.Type,
                current.Bank.Episode,
                current.ObjectLookup,
                current.TypeDefinitions);
        }

        /// <summary>
        /// The mnu audition_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuAudition_Click(object sender, EventArgs e)
        {
            var sn = this.m_treeView.LastSelectedNode as SoundNode;
            if (sn != null)
            {
                this.AuditionSound.Raise(sn.ObjectInstance);
            }
        }

        /// <summary>
        /// The mnu batch create_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuBatchCreate_Click(object sender, EventArgs e)
        {
            var m = (ToolStripMenuItem)sender;
            ITypeDefinition soundDef;
            if (m == this.mnuBatchCreate)
            {
                soundDef = this.m_typeDefinitions.ObjectTypes[0];
            }
            else
            {
                soundDef = this.m_typeDefinitions.FindTypeDefinition(m.Text);
            }

            SoundBankNode sbn;
            string virtualFolder;
            if (this.m_treeView.LastSelectedNode.GetType() == typeof(VirtualFolderNode))
            {
                virtualFolder = ((VirtualFolderNode)this.m_treeView.LastSelectedNode).ObjectName;
                sbn = (SoundBankNode)this.m_treeView.LastSelectedNode.Parent;
            }
            else
            {
                virtualFolder = null;
                sbn = (SoundBankNode)this.m_treeView.LastSelectedNode;
            }

            if (!this.TryToCheckOutBank(sbn))
            {
                return;
            }

            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(
                this.ParentForm,
                m.Text + " base name",
                sbn.ObjectBank.Name + "_" + (virtualFolder != null ? virtualFolder + "_" : string.Empty));
            if (input.HasBeenCancelled) return;
            string baseName = input.data;


            input = frmSimpleInput.GetInput(
                this.ParentForm, "Number of " + m.Text + "s to create");
            if (input.HasBeenCancelled) return;
            uint numObjectsToCreate = 0;
            try
            {
                numObjectsToCreate = uint.Parse(input.data);
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }

            if (numObjectsToCreate > 0)
            {
                var id = new ObjectLookupId(sbn.Type, sbn.Episode);
                for (uint i = 1; i <= numObjectsToCreate; i++)
                {
                    string soundName = baseName + "_" + i;
                    if (ObjectInstance.CheckForNameAndHashCollision(soundName, sbn.Episode))
                    {
                        ErrorManager.HandleInfo("An object named " + soundName + " already exists, or hash is in use.");
                        return;
                    }
                }

                for (uint i = 1; i <= numObjectsToCreate; i++)
                {
                    string soundName = baseName + "_" + i;
                    TypeUtils.GenerateNewObjectInstance(
                        sbn.ObjectBank,
                        soundName,
                        virtualFolder,
                        RaveInstance.ObjectLookupTables[id],
                        soundDef);
                }
            }
        }

        /// <summary>
        /// The mnu check out_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuCheckOut_Click(object sender, EventArgs e)
        {
            this.CheckOut(false);
        }

        /// <summary>
        /// The mnu copy name_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuCopyName_Click(object sender, EventArgs e)
        {
            var node = this.m_treeView.LastSelectedNode as BaseNode;
            Clipboard.Clear();
            Clipboard.SetDataObject(node.ObjectName);
        }

        /// <summary>
        /// The mnu copy path_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuCopyPath_Click(object sender, EventArgs e)
        {
            var node = this.m_treeView.LastSelectedNode as BaseNode;
            Clipboard.Clear();
            Clipboard.SetDataObject(node.GetObjectPath());
        }

        /// <summary>
        /// The mnu delete sound_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuDeleteSound_Click(object sender, EventArgs e)
        {
            if (this.m_treeView.SelectedItems.Count > 0)
            {
                if (ErrorManager.HandleQuestion(
                    "Are you sure you want to delete these objects?", MessageBoxButtons.YesNo, DialogResult.Yes)
                    == DialogResult.Yes)
                {
                    this.DeleteItemsFromTree(this.m_treeView.SelectedItems);
                }
            }
        }

        /// <summary>
        /// The mnu dup sound_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuDupSound_Click(object sender, EventArgs e)
        {
            this.DuplicateObject();
        }

        /// <summary>
        /// Duplicate selected object(s).
        /// </summary>
        private void DuplicateObject()
        {
            var soundBankNodes =
                this.m_treeView.SelectedItems.OfType<SoundNode>()
                    .ToDictionary(
                        sn => sn.ObjectInstance.Name,
                        sn => sn.Parent as SoundBankNode ?? sn.Parent.Parent as SoundBankNode);
            foreach (var entry in soundBankNodes)
            {
                if (!this.TryToCheckOutBank(entry.Value))
                {
                    return;
                }

                var sn = FindSoundNode(entry.Value, entry.Key);
                frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "New object name", sn.ObjectInstance.Name);
                if (input.HasBeenCancelled) continue;
                string name = input.data;

                while (ObjectInstance.CheckForNameAndHashCollision(name, entry.Value.Episode))
                {
                    input = frmSimpleInput.GetInput(this.ParentForm, "Name or Namehash in use, name", name);
                    if (input.HasBeenCancelled) return;
                    name = input.data;
                }

                var newSound = TypeUtils.CreateDuplicateSound(
                    sn.ObjectInstance, sn.ObjectInstance.Bank, name);
                TypeUtils.ReportNewObjectCreated(newSound);
            }
        }

        private void mnuInsertContext_Click(object sender, EventArgs e)
        {
            SoundNode soundNode = (SoundNode)m_treeView.LastSelectedNode;
            if (soundNode != null)
            {
                ContextSoundCreator contextSoundCreator = new ContextSoundCreator();
                IObjectInstance objectInstance = contextSoundCreator.ReplaceWithContextSound(soundNode);
                if (objectInstance != null)
                {
                    this.OnSoundSelect.Raise(objectInstance);
                }
            }
        }

        /// <summary>
        /// The mnu dup tree_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuDupTree_Click(object sender, EventArgs e)
        {
            var dictionary = new Dictionary<string, SoundBankNode>();

            for (int i = 0; i < this.m_treeView.SelectedItems.Count; i++)
            {
                var sn = this.m_treeView.SelectedItems[i] as SoundNode;
                if (sn != null)
                {
                    var sbn = sn.Parent as SoundBankNode;
                    if (sbn == null)
                    {
                        sbn = sn.Parent.Parent as SoundBankNode;
                    }

                    dictionary.Add(sn.ObjectInstance.Name, sbn);
                }
            }

            foreach (var kvp in dictionary)
            {
                if (this.TryToCheckOutBank(kvp.Value))
                {
                    SoundNode sn = FindSoundNode(kvp.Value, kvp.Key);
                    IObjectInstance parentObj = sn.ObjectInstance;

                    frmSimpleInput.InputResult input = frmSimpleInput.GetInput("Base name", parentObj.Name);
                    if (input.HasBeenCancelled) continue;
                    string baseName = input.data;


                    while (!sn.ObjectInstance.Name.Contains(baseName))
                    {
                        input = frmSimpleInput.GetInput("Invalid Base Name. Please enter base name", parentObj.Name);
                        if (input.HasBeenCancelled) continue;
                        baseName = input.data;
                    }
                    if (input.HasBeenCancelled) continue;

                    input = frmSimpleInput.GetInput("New base name (replaces " + baseName + ")");
                    if (input.HasBeenCancelled) continue;
                    string newBaseName = input.data;

                    if (CheckForDupTreeNameConflict(parentObj, baseName, newBaseName))
                    {
                        IObjectInstance newParentObj = TypeUtils.CreateDuplicateSound(
                            parentObj, parentObj.Bank, parentObj.Name.Replace(baseName, newBaseName));
                        TypeUtils.ReportNewObjectCreated(newParentObj);
                        var processed = new Hashtable();
                        MakeLocalOnTree(newParentObj, baseName, newBaseName, processed);
                    }
                }
            }
        }

        /// <summary>
        /// The mnu expand checked out_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuExpandCheckedOut_Click(object sender, EventArgs e)
        {
            ExpandCheckedOutStores(this.m_treeView.LastSelectedNode);
        }

        /// <summary>
        /// The mnu expand object stores_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuExpandObjectStores_Click(object sender, EventArgs e)
        {
            this.m_treeView.LastSelectedNode.Expand();
            this.ExpandEverythingExceptBankNodes(this.m_treeView.LastSelectedNode.Nodes);
        }

        /// <summary>
        /// The mnu expand xml_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuExpandXml_Click(object sender, EventArgs e)
        {
            var sn = this.m_treeView.LastSelectedNode as SoundNode;
            if (sn != null)
            {
                string templateKey = this.m_type + "_TEMPLATE";
                if (RaveInstance.AllBankManagers.ContainsKey(templateKey))
                {
                    var compiler = RaveInstance.XmlBankCompiler.MetadataManagers[this.m_type].Compiler;
                    var templateManager = compiler.CreateTemplateManager();
                    foreach (var tb in RaveInstance.AllBankManagers[templateKey].Banks)
                    {
                        templateManager.Load(Utility.ToXDoc(tb.Document));
                    }

                    var ti = sn.ObjectInstance as ITemplateInstance;
                    if (ti != null)
                    {
                        var temp = new XmlDocument();
                        temp.AppendChild(temp.CreateElement("Objects"));
                        XmlNode imported = temp.ImportNode(ti.Node, true);
                        temp.DocumentElement.AppendChild(imported);
                        XDocument tempXDoc = Utility.ToXDoc(temp);
                        templateManager.Process(tempXDoc);
                        temp = Utility.ToXmlDoc(tempXDoc);
                        var report = new frmReport("Expanded XML");
                        report.SetXml(temp);
                        report.Show(RaveInstance.ActiveWindow);
                    }
                }
            }
        }

        /// <summary>
        /// The mnu expand_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="Exception">
        /// </exception>
        private void mnuExpand_Click(object sender, EventArgs e)
        {
            var sn = this.m_treeView.LastSelectedNode as SoundNode;
            if (sn == null)
            {
                return;
            }

            TreeNode parent = sn.Parent;
            while (parent != null && parent.GetType() != typeof(SoundBankNode))
            {
                parent = parent.Parent;
            }

            var id = new ObjectLookupId(sn.Type, sn.Episode);
            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "Please enter a name");
            if (input.HasBeenCancelled) return;
            string soundName = input.data;

            while (ObjectInstance.CheckForNameAndHashCollision(soundName, sn.Episode))
            {
                input = frmSimpleInput.GetInput(
                    this.ParentForm, "Please choose another name (already in use/ hash collision):", soundName);
                if (input.HasBeenCancelled) return;
                soundName = input.data;
            }

            var obj = sn.ObjectInstance;
            string objName = obj.Name;
            var bank = obj.Bank;
            if (!bank.TryToCheckout())
            {
                return;
            }

            obj = RaveInstance.ObjectLookupTables[id][objName];
            RaveInstance.ObjectLookupTables[id].Remove(objName);
            obj.RemoveBankDelegate();

            // and remove all references
            foreach (var objref in obj.References)
            {
                if (objref.ObjectInstance != null)
                {
                    objref.ObjectInstance.RemoveReferencer(obj);
                }
            }

            bank.DeleteObjectInstance(obj);

            string templateKey = this.m_type + "_TEMPLATE";
            if (!RaveInstance.AllBankManagers.ContainsKey(templateKey))
            {
                return;
            }

            var compiler = RaveInstance.XmlBankCompiler.MetadataManagers[this.m_type].Compiler;
            var templateManager = compiler.CreateTemplateManager();
            foreach (var tb in RaveInstance.AllBankManagers[templateKey].Banks)
            {
                templateManager.Load(Utility.ToXDoc(tb.Document));
            }

            var ti = obj as ITemplateInstance;
            if (ti == null)
            {
                return;
            }

            var temp = new XmlDocument();
            temp.AppendChild(temp.CreateElement("Objects"));
            var tempNode = (XmlElement)temp.ImportNode(ti.Node, true);
            tempNode.SetAttribute("name", soundName);
            temp.DocumentElement.AppendChild(tempNode);

            var tempXDoc = Utility.ToXDoc(temp);
            templateManager.Process(tempXDoc);
            temp = Utility.ToXmlDoc(tempXDoc);

            foreach (XmlNode node in temp.DocumentElement.ChildNodes)
            {
                string name = node.Attributes["name"].Value;
                if (ObjectInstance.CheckForNameAndHashCollision(name, sn.Episode))
                {
                    // ensure no naming clashes
                    int counter = 2;
                    while (ObjectInstance.CheckForNameAndHashCollision(name, sn.Episode))
                    {
                        input = frmSimpleInput.GetInput(this.ParentForm, "Name in use or hash collision", name);
                        if (input.HasBeenCancelled) name = name + "_" + counter++;
                        else name = input.data;
                    }

                    node.Attributes["name"].Value = name;
                }

                if (node.Attributes["folder"] != null)
                {
                    // fix up historic folders
                    if (node.Attributes["name"].Value == soundName)
                    {
                        node.Attributes.RemoveNamedItem("folder");
                    }
                    else
                    {
                        node.Attributes["folder"].Value = soundName + "_COMPONENTS";
                    }
                }

                bank.Document.DocumentElement.AppendChild(ti.Bank.Document.ImportNode(node, true));
                var newObject = new TypedObjectInstance(
                    node,
                    bank,
                    bank.Type,
                    bank.Episode,
                    RaveInstance.ObjectLookupTables[id],
                    obj.TypeDefinitions);
                RaveInstance.ObjectLookupTables[id].Add(newObject.Name, newObject);
                bank.ObjectInstances.Add(newObject);
                newObject.MarkAsDirty();
            }

            bank.MarkAsDirty();
            bank.ComputeReferences();

            var sbn = parent as SoundBankNode;
            sbn.RecreateSoundNodes();
        }

        /// <summary>
        /// The mnu get latest_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuGetLatest_Click(object sender, EventArgs e)
        {
            this.SaveTreeState(this.m_treeView.Nodes);
            var sbn = this.m_treeView.LastSelectedNode as SoundBankNode;
            var fn = this.m_treeView.LastSelectedNode as FolderNode;
            if (fn != null)
            {
                try
                {
                    this.GetLatestFolder(fn);
                }
                catch (Exception ex)
                {
                    ErrorManager.HandleError(ex);
                }
            }
            else if (sbn != null)
            {
                if (!sbn.ObjectBank.GetLatest())
                {
                    ErrorManager.HandleInfo("Get Latest failed.");
                }

                sbn.ObjectBank.Reload();
                sbn.RecreateSoundNodes();
            }

            this.ExpandTree(this.m_treeView.Nodes);
        }

        /// <summary>
        /// The mnu global_ on click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuGlobal_OnClick(object sender, EventArgs e)
        {
            List<IObjectInstance> objectInstances =
                this.m_treeView.SelectedItems.OfType<SoundNode>()
                    .Select(sn => sn.ObjectInstance)
                    .Where(oi => oi.GetType() == typeof(IObjectInstance))
                    .ToList();
            if (objectInstances.Count > 1)
            {
                List<IObjectInstance> checkedOutObjectInstances =
                    objectInstances.Where(o => o.Bank.IsCheckedOut).ToList();
                List<IObjectInstance> notCheckedOutObjectInstances =
                    objectInstances.Where(o => !o.Bank.IsCheckedOut).ToList();
                var banksNotCheckedOut =
                    notCheckedOutObjectInstances.Select(o => o.Bank).Distinct().ToList();
                if (banksNotCheckedOut.Count > 0)
                {
                    foreach (var bank in banksNotCheckedOut)
                    {
                        if (!bank.TryToCheckout())
                        {
                            MessageBox.Show(
                                RaveInstance.ActiveWindow,
                                string.Format("Failed to check out bank for Global Field Edit - {0}", bank.Name),
                                "Checkout Failed",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                            return;
                        }
                    }

                    checkedOutObjectInstances.AddRange(
                        notCheckedOutObjectInstances.Select(
                            objectInstance => objectInstance.Bank.FindObjectInstance(objectInstance.Name)));
                }

                if (checkedOutObjectInstances.Count > 1)
                {
                    using (var frmGlobalEdit = new frmGlobalEdit(checkedOutObjectInstances))
                    {
                        frmGlobalEdit.ShowDialog();
                    }
                }
            }
        }

        /// <summary>
        /// The mnu grid edit_ on click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuGridEdit_OnClick(object sender, EventArgs e)
        {
            var gridEditor = new frmGridEditor();
            var objects = new List<IObjectInstance>();
            foreach (TreeNode tn in this.m_treeView.SelectedItems)
            {
                if (tn.GetType() == typeof(FolderNode))
                {
                    var fn = tn as FolderNode;
                    var soundBankNodes = new ArrayList();
                    FolderNode.GetBanks(fn, soundBankNodes);
                    foreach (SoundBankNode sbn in soundBankNodes)
                    {
                        if (sbn.ObjectBank.TryToCheckout())
                        {
                            foreach (var o in sbn.ObjectBank.ObjectInstances)
                            {
                                if (!objects.Contains(o))
                                {
                                    objects.Add(o);
                                }
                            }
                        }
                    }
                }

                if (tn.GetType() == typeof(VirtualFolderNode))
                {
                    var vfn = tn as VirtualFolderNode;
                    foreach (SoundNode sn in vfn.Nodes)
                    {
                        IObjectInstance objInst = sn.ObjectInstance;

                        string name = objInst.Name;
                        var id = new ObjectLookupId(objInst.Type, objInst.Episode);

                        if (sn.ObjectInstance.Bank.TryToCheckout())
                        {
                            objInst = RaveInstance.ObjectLookupTables[id][name];

                            // skip templates
                            if (objInst as ITemplateInstance == null && objInst.Bank.IsCheckedOut)
                            {
                                if (!objects.Contains(objInst))
                                {
                                    objects.Add(objInst);
                                }
                            }
                        }
                    }
                }

                if (tn.GetType() == typeof(SoundBankNode))
                {
                    var sbn = tn as SoundBankNode;
                    if (sbn.ObjectBank.TryToCheckout())
                    {
                        foreach (var o in sbn.ObjectBank.ObjectInstances)
                        {
                            if (!objects.Contains(o))
                            {
                                objects.Add(o);
                            }
                        }
                    }
                }
                else if (tn.GetType() == typeof(SoundNode))
                {
                    var sn = tn as SoundNode;
                    IObjectInstance objInst = sn.ObjectInstance;

                    string name = objInst.Name;
                    var id = new ObjectLookupId(objInst.Type, objInst.Episode);

                    objInst = RaveInstance.ObjectLookupTables[id][name];

                    // skip templates
                    if (objInst as ITemplateInstance == null)
                    {
                        if (!objects.Contains(objInst))
                        {
                            objects.Add(objInst);
                        }
                    }
                }
            }

            if (objects.Count == 0)
            {
                ErrorManager.HandleInfo("No object instances selected. Only template instances have been selected.");
                return;
            }

            ITypeDefinition baseType;
            if (AreObjectsOfSameType(objects))
            {
                baseType = objects[0].TypeDefinitions.FindTypeDefinition(objects[0].TypeName);
            }
            else
            {
                var toRemove = new List<IObjectInstance>();
                var typeHeirarchy = new List<ITypeDefinition>();

                ITypeDefinition baseTypeTemp;

                for (int i = 0; i < objects.Count; i++)
                {
                    string inheritsFromTemp = objects[i].TypeName;
                    bool foundBase = false;

                    // build up inheritance list
                    if (i == 0)
                    {
                        while (inheritsFromTemp != null)
                        {
                            baseTypeTemp = objects[i].TypeDefinitions.FindTypeDefinition(inheritsFromTemp);
                            inheritsFromTemp = baseTypeTemp.InheritsFrom;
                            typeHeirarchy.Add(baseTypeTemp);
                        }
                    }

                        // find first common parent
                    else
                    {
                        while (inheritsFromTemp != null)
                        {
                            baseTypeTemp = objects[i].TypeDefinitions.FindTypeDefinition(inheritsFromTemp);
                            inheritsFromTemp = baseTypeTemp.InheritsFrom;
                            if (typeHeirarchy.Contains(baseTypeTemp))
                            {
                                // remove everything below common parent
                                int index = typeHeirarchy.IndexOf(baseTypeTemp);
                                if (index > 0)
                                {
                                    typeHeirarchy.RemoveRange(0, index);
                                }

                                foundBase = true;
                                break;
                            }
                        }

                        if (!foundBase)
                        {
                            toRemove.Add(objects[i]);
                        }
                    }
                }

                var sb = new StringBuilder();
                sb.Append("The following objects do not share a common parent with ");
                sb.Append(objects[0].Name);
                sb.AppendLine(" and will therefore not be displayed: ");
                foreach (IObjectInstance o in toRemove)
                {
                    objects.Remove(o);
                    sb.AppendLine(o.Name);
                }

                MessageBox.Show(RaveInstance.ActiveWindow, sb.ToString());
                baseType = typeHeirarchy[0];
            }

            if (baseType.Fields.Length > 0)
            {
                gridEditor.Init(objects, baseType);
                gridEditor.Show(RaveInstance.ActiveWindow);
            }
            else
            {
                ErrorManager.HandleInfo("Selected objects have a common parent typedefinition that contains no fields");
            }
        }

        /// <summary>
        /// The mnu insert template_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="Exception">
        /// </exception>
        private void mnuInsertTemplate_Click(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;

            var template = Template.TemplateLookupTables[this.m_type][menuItem.Text];

            SoundBankNode bankNode;
            string virtualFolder;

            if (this.m_treeView.LastSelectedNode.GetType() == typeof(VirtualFolderNode))
            {
                virtualFolder = ((VirtualFolderNode)this.m_treeView.LastSelectedNode).ObjectName;
                bankNode = (SoundBankNode)this.m_treeView.LastSelectedNode.Parent;
            }
            else
            {
                virtualFolder = null;
                bankNode = (SoundBankNode)this.m_treeView.LastSelectedNode;
            }

            if (!this.TryToCheckOutBank(bankNode))
            {
                ErrorManager.HandleInfo("Check out failed - is someone else currently working on this bank?");
                return;
            }

            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(
                this.ParentForm,
                menuItem.Text + " name",
                bankNode.ObjectBank.Name + "_" + (virtualFolder != null ? virtualFolder + "_" : string.Empty));
            if (input.HasBeenCancelled) return;
            string templateInstanceName = input.data;

            var id = new ObjectLookupId(bankNode.Type, bankNode.Episode);

            while (ObjectInstance.CheckForNameAndHashCollision(templateInstanceName, bankNode.Episode))
            {
                input = frmSimpleInput.GetInput(
                    this.ParentForm, "Name in use or hash collision, " + menuItem.Text + " name", templateInstanceName);
                if (input.HasBeenCancelled) return;
                templateInstanceName = input.data;
            }

            var element = bankNode.ObjectBank.Document.CreateElement("TemplateInstance");
            element.SetAttribute("name", templateInstanceName);
            element.SetAttribute("template", template.Name);

            var templateInstance = new TemplateInstance(
                template,
                element,
                bankNode.ObjectBank,
                this.m_type,
                bankNode.ObjectBank.Episode,
                RaveInstance.ObjectLookupTables[id],
                this.m_typeDefinitions);

            templateInstance.ObjectLookup.Add(templateInstance.Name, templateInstance);
            templateInstance.VirtualFolderName = virtualFolder;
            templateInstance.MarkAsDirty();
            bankNode.ObjectBank.ObjectInstances.Add(templateInstance);
            bankNode.ObjectBank.Document.DocumentElement.AppendChild(element);
            TypeUtils.ReportNewObjectCreated(templateInstance);
        }

        /// <summary>
        /// The mnu local check out_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuLocalCheckOut_Click(object sender, EventArgs e)
        {
            this.CheckOut(true);
        }

        /// <summary>
        /// The mnu model_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuModel_Click(object sender, EventArgs e)
        {
            var bankNode = this.m_treeView.LastSelectedNode as SoundBankNode;
            if (bankNode != null)
            {
                Configuration.DefaultModelPath = bankNode.ObjectBank.FilePath;
            }
        }

        /// <summary>
        /// The mnu new folder_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewFolder_Click(object sender, EventArgs e)
        {
            if (this.m_treeView.LastSelectedNode == null)
            {
                return;
            }

            var baseNode = this.m_treeView.LastSelectedNode as BaseNode;
            List<string> nodeNames = (from TreeNode tn in baseNode.Nodes select tn.Text).ToList();

            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "Folder name");
            if (input.HasBeenCancelled) return;
            string folderName = input.data;

            while (folderName != null && nodeNames.Contains(folderName))
            {
                input = frmSimpleInput.GetInput(
                    this.ParentForm, string.Format("Name {0} is invalid. Folder name", folderName));
                if (input.HasBeenCancelled) return;
                folderName = input.data;
            }

            var sbn = this.m_treeView.LastSelectedNode as SoundBankNode;
            if (sbn != null)
            {
                if (!sbn.ObjectBank.TryToCheckout())
                {
                    ErrorManager.HandleInfo("Failed to check-out " + sbn.Name);
                    return;
                }

                var newNode = new VirtualFolderNode(
                    folderName, sbn.ObjectBank.Episode, sbn.ObjectBank.Type, this.m_objectTypeIconTable);
                this.m_treeView.LastSelectedNode.Nodes.Add(newNode);
            }
            else
            {
                string path = Configuration.ObjectXmlPath + baseNode.GetObjectPath() + "\\" + folderName;
                var newNode = new FolderNode(folderName, path, baseNode.Episode, baseNode.Type, this.m_objectTypeIconTable);
                this.m_treeView.LastSelectedNode.Nodes.Add(newNode);

                Directory.CreateDirectory(path);
            }
        }

        /// <summary>
        /// The mnu new sound bank_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewSoundBank_Click(object sender, EventArgs e)
        {
            if (this.m_treeView.LastSelectedNode == null)
            {
                return;
            }

            var bn = this.m_treeView.LastSelectedNode as BaseNode;
            List<string> nodeNames = (from TreeNode tn in bn.Nodes select tn.Text).ToList();

            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "Object store name");
            if (input.HasBeenCancelled) return;
            string bankName = input.data;
            while (bankName != null && nodeNames.Contains(bankName))
            {
                input = frmSimpleInput.GetInput(
                    this.ParentForm, string.Format("Name {0} is invalid. Object store name", bankName));
                if (input.HasBeenCancelled) return;
                bankName = input.data;
            }

            var sb = new StringBuilder();
            sb.Append(Configuration.ObjectXmlPath);
            sb.Append(((BaseNode)this.m_treeView.LastSelectedNode).GetObjectPath());
            sb.Append("\\");
            sb.Append(bankName);
            sb.Append(".XML");

            string path = sb.ToString();
            TreeNodeCollection collection = this.m_treeView.LastSelectedNode.Nodes;

            var xw = new XmlTextWriter(path, Encoding.UTF8);
            xw.WriteElementString("Objects", string.Empty);
            xw.Close();

            try
            {
                IChangeList clist =
                    RaveInstance.AssetManager.CreateChangeList("[Rave] Initial import of " + path);
                clist.MarkAssetForAdd(path);
                clist.Submit();
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }

            var bank = new ObjectBank(path, bn.Episode, bn.Type, this.m_typeDefinitions);
            bank.Load();
            this.xmlBankManager.Banks.Add(bank);

            var newNode = new SoundBankNode(bank, this.m_objectTypeIconTable);
            newNode.BankStatusChanged += this.SoundBankNode_OnBankStatusChanged;
            collection.Add(newNode);

            bank.Checkout(false);
            //newNode.RecreateSoundNodes();
            //this.ExpandTree(this.m_treeView.LastSelectedNode.Nodes);
            //this.ChangelistModified.Raise();
        }

        /// <summary>
        /// The mnu new sound type_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuNewSoundType_Click(object sender, EventArgs e)
        {
            var m = (ToolStripMenuItem)sender;
            ITypeDefinition soundDef;

            if (m == this.mnuNewSound)
            {
                soundDef = this.m_typeDefinitions.ObjectTypes[0];
            }
            else
            {
                soundDef = this.m_typeDefinitions.FindTypeDefinition(m.Text);
            }

            SoundBankNode sbn;
            string virtualFolder;

            if (this.m_treeView.LastSelectedNode.GetType() == typeof(VirtualFolderNode))
            {
                virtualFolder = ((VirtualFolderNode)this.m_treeView.LastSelectedNode).ObjectName;
                sbn = (SoundBankNode)this.m_treeView.LastSelectedNode.Parent;
            }
            else
            {
                virtualFolder = null;
                sbn = (SoundBankNode)this.m_treeView.LastSelectedNode;
            }

            if (!this.TryToCheckOutBank(sbn))
            {
                return;
            }

            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(
                this.ParentForm,
                m.Text + " name",
                sbn.ObjectBank.Name + "_" + (virtualFolder != null ? virtualFolder + "_" : string.Empty));
            if (input.HasBeenCancelled) return;
            string soundName = input.data;

            var id = new ObjectLookupId(sbn.Type, sbn.Episode);

            while (soundName != null && ObjectInstance.CheckForNameAndHashCollision(soundName, sbn.Episode))
            {
                input = frmSimpleInput.GetInput(this.ParentForm, "Name in use or hash collision, " + m.Text + " name", soundName);
                if (input.HasBeenCancelled) return;
                soundName = input.data;
            }

            TypeUtils.GenerateNewObjectInstance(
                sbn.ObjectBank, soundName, virtualFolder, RaveInstance.ObjectLookupTables[id], soundDef);
        }

        /// <summary>
        /// The mnu plugin_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuPlugin_Click(object sender, EventArgs e)
        {
            foreach (IRAVEObjectBrowserPlugin plugin in RaveInstance.PluginManager.ObjectBrowserPlugins)
            {
                if (((ToolStripMenuItem)sender).Text == plugin.GetName())
                {
                    var nodes = new ArrayList();

                    foreach (BaseNode bn in this.m_treeView.SelectedItems)
                    {
                        var sbn = bn as SoundBankNode;
                        if (sbn != null)
                        {
                            nodes.Add(sbn);
                        }
                    }

                    plugin.Process(nodes);
                }
            }
        }

        /// <summary>
        /// The mnu populate missing fields_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuPopulateMissingFields_Click(object sender, EventArgs e)
        {
            var soundNode = this.m_treeView.LastSelectedNode as SoundNode;
            var soundBankNode = this.m_treeView.LastSelectedNode as SoundBankNode;

            if (null != soundNode)
            {
                var typedObjectInstance = soundNode.ObjectInstance as ITypedObjectInstance;
                if (null != typedObjectInstance)
                {
                    typedObjectInstance.PopulateMissingFields();
                }
            }
            else if (soundBankNode != null)
            {
                if (this.TryToCheckOutBank(soundBankNode))
                {
                    foreach (var objInstance in soundBankNode.ObjectBank.ObjectInstances)
                    {
                        var typedObjectInstance = objInstance as ITypedObjectInstance;

                        if (null != typedObjectInstance)
                        {
                            typedObjectInstance.PopulateMissingFields();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The mnu reload checked in_ on click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuReloadCheckedIn_OnClick(object sender, EventArgs e)
        {
            var sbn = this.m_treeView.LastSelectedNode as SoundBankNode;
            if (sbn != null)
            {
                if (sbn.ObjectBank.LoadCheckedInVersion())
                {
                    sbn.RecreateSoundNodes();
                }
            }

            this.ChangelistModified.Raise();
        }

        /// <summary>
        /// The mnu reload saved_ on click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuReloadSaved_OnClick(object sender, EventArgs e)
        {
            var sbn = this.m_treeView.LastSelectedNode as SoundBankNode;
            if (sbn != null)
            {
                sbn.ObjectBank.LoadSavedVersion();
                sbn.RecreateSoundNodes();
            }

            this.ChangelistModified.Raise();
        }

        /// <summary>
        /// The mnu rename_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuRename_Click(object sender, EventArgs e)
        {
            if ((this.m_treeView.LastSelectedNode as SoundNode) != null)
            {
                this.RenameObject();
            }
            else if ((this.m_treeView.LastSelectedNode as VirtualFolderNode) != null)
            {
                this.RenameVirtualFolder();
            }
        }

        /// <summary>
        /// The mnu revert_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuRevert_Click(object sender, EventArgs e)
        {
            this.Revert();
        }

        class MenuEntry
        {
            public string Name;
            public List<MenuEntry> Children = new List<MenuEntry>();
        }

        private static string RemoveRootString(string rootString, string itemString)
        {
            if (itemString.StartsWith(rootString, StringComparison.OrdinalIgnoreCase))
            {
                return itemString.Substring(rootString.Length);
            }
            return itemString;
        }

        private static MenuEntry FindOrCreateEntry(MenuEntry root, string itemName)
        {
            MenuEntry newRoot = null;
            foreach (var c in root.Children)
            {
                if (c.Name.Equals(itemName, StringComparison.OrdinalIgnoreCase))
                {
                    newRoot = c;
                    break;
                }
            }
            if (newRoot == null)
            {
                newRoot = new MenuEntry() { Name = itemName };
                root.Children.Add(newRoot);
            }

            return newRoot;
        }

        private static void AddToMenuTree(string itemPath, string itemName, MenuEntry root)
        {
            if (itemPath.Length == 0)
            {
                // path is empty; we're now at the correct point in the tree to add the item
                root.Children.Add(new MenuEntry() { Name = itemName });
            }
            else
            {
                var index = itemPath.IndexOf('\\') + 1;
                if (index == 0)
                {
                    // if IndexOf returned -1 (ie index==0) then we're at the final path element, so treat the final character as the delimiter.
                    index = itemPath.Length;
                }

                // If index == 1 then the first character is '\', so simply skip that and try again

                if (index > 1)
                {
                    // Otherwise create a menu entry for this path element
                    var thisElem = itemPath.Substring(0, index);
                    root = FindOrCreateEntry(root, thisElem.Replace("\\", string.Empty));
                }
                AddToMenuTree(itemPath.Substring(index), itemName, root);
            }
        }

        private static void CreateToolStripItems(ToolStripMenuItem toolStripMenu, MenuEntry menuRoot, EventHandler clickHandler)
        {
            foreach (var e in menuRoot.Children.OrderBy(m => m.Name))
            {
                if (e.Children.Count > 0)
                {
                    var mnu = new ToolStripMenuItem(e.Name);
                    toolStripMenu.DropDownItems.Add(mnu);
                    CreateToolStripItems(mnu, e, clickHandler);
                }
                else
                {
                    var mnu = new ToolStripMenuItem(e.Name, null, clickHandler);
                    toolStripMenu.DropDownItems.Add(mnu);
                }
            }
        }

        private void PopulateTemplatesMenu(ToolStripMenuItem templatesMenu)
        {
            // Create menu hierarchy based on folder structure within the template browser
            // This is a little convoluted since we don't have access to the templates in a hierarchal form, so instead iterate over the flat list,
            // use the TemplateBank path to build a tree, then turn that into a tool strip menu hierarchy

            audMetadataType metadataType = null;
            foreach (var mt in Configuration.ProjectSettings.GetMetadataTypes())
            {
                if (mt.Type.Equals(m_type, StringComparison.OrdinalIgnoreCase))
                {
                    metadataType = mt;
                    break;
                }
            }
            if (metadataType == null)
            {
                return;
            }

            var templatePath = Path.Combine(Configuration.ObjectXmlPath, metadataType.TemplatePath);

            var root = new MenuEntry();

            foreach (var kvp in Template.TemplateLookupTables[this.m_type])
            {
                var itm = kvp.Value;

                var fileNameNoExt = System.IO.Path.GetFileNameWithoutExtension(itm.TemplateBank.FilePath);
                var filePathNoExt = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(itm.TemplateBank.FilePath), fileNameNoExt);

                var path = RemoveRootString(templatePath, filePathNoExt);

                AddToMenuTree(path, itm.Name, root);
            }

            CreateToolStripItems(templatesMenu, root, this.mnuInsertTemplate_Click);
        }

        /// <summary>
        /// The mnu right click_ popup.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuRightClick_Popup(object sender, CancelEventArgs e)
        {
            this.mnuDupTree.Visible = false;
            this.mnuViewHierarchy.Visible = false;
            this.mnuNewSound.Visible = false;
            this.mnuNewSoundBank.Visible = false;
            this.mnuNewFolder.Visible = false;
            this.mnuRename.Visible = false;
            this.mnuCheckOut.Visible = false;
            this.mnuRevert.Visible = false;
            this.mnuLocalCheckOut.Visible = false;
            this.mnuAudition.Visible = false;
            this.mnuDupSound.Visible = false;
            this.mnuInsertContext.Visible = false;
            this.mnuPopulateMissingFields.Visible = false;
            this.mnuGetLatest.Visible = false;
            this.mnuSearchInStore.Visible = true;
            this.mnuDelete.Visible = true;
            this.mnuExpandObjectStores.Visible = false;
            this.mnuExpandCheckedOut.Visible = false;
            this.mnuBatchCreate.Visible = false;
            this.mnuCopyBaseSettings.Visible = false;
            this.mnuPasteBaseSettings.Visible = false;
            this.menuItem4.Visible = true;
            this.mnuMultiEdit.Visible = false;
            this.mnuCopyPath.Visible = false;
            this.mnuCopyName.Visible = false;
            this.mnuSave.Visible = false;
            this.mnuSubmit.Visible = false;
            this.mnuSeperator3.Visible = true;
            this.mnuReload.Visible = false;
            this.mnuReloadSaved.Visible = false;
            this.mnuReloadCheckedIn.Visible = false;
            this.mnuGlobal.Visible = false;
            this.mnuTemplates.Visible = false;
            this.mnuExpand.Visible = false;
            this.mnuGridEdit.Visible = false;
            this.mnuExpandXml.Visible = false;
            this.mnuSeperator2.Visible = false;
            this.mnuModel.Visible = false;
            this.mnuSoundSetReport.Visible = false;
            this.mnuInvalidRefsReport.Visible = false;
            this.mnuViewInP4.Visible = false;
            this.mnuViewInExplorer.Visible = true;
            this.mnuMute.Visible = false;
            this.mnuSolo.Visible = false;

            var sn = this.m_treeView.LastSelectedNode as SoundNode;
            if (sn != null)
            {
                this.mnuSolo.Checked = sn.ObjectInstance.IsSoloed;
                this.mnuMute.Checked = sn.ObjectInstance.IsMuted;
                this.mnuSolo.Enabled = true;
                this.mnuMute.Enabled = true;
                if (!sn.ObjectInstance.CheckIfMuteSoloEnabled())
                {

                    this.mnuSolo.Enabled = false;
                    this.mnuMute.Enabled = false;
                }

            }

            this.mnuPasteBaseSettings.DropDownItems.Clear();

            if (this.m_treeView.LastSelectedNode == null)
            {
                this.mnuDelete.Visible = false;
                this.mnuNewFolder.Visible = true;
                this.mnuSearchInStore.Visible = false;
                this.menuItem4.Visible = false;
                return;
            }

            this.mnuCopyName.Visible = this.mnuCopyPath.Visible = true;

            var soundNode = this.m_treeView.LastSelectedNode as SoundNode;
            var soundBankNode = this.m_treeView.LastSelectedNode as SoundBankNode;
            var folderNode = this.m_treeView.LastSelectedNode as FolderNode;
            var virtualFolderNode = this.m_treeView.LastSelectedNode as VirtualFolderNode;

            if (null != soundNode)
            {
                this.mnuViewHierarchy.Visible = true;
                this.mnuRename.Visible = true;
                this.mnuAudition.Visible = true;

                this.mnuMute.Visible = true;
                this.mnuSolo.Visible = true;

                this.mnuDupSound.Visible = true;

                this.mnuDupTree.Visible = true;
                this.mnuCopyBaseSettings.Visible = true;
                this.mnuMultiEdit.Visible = true;
                this.mnuGlobal.Visible = true;
                this.mnuGridEdit.Visible = true;
                this.mnuPopulateMissingFields.Visible = true;
                this.mnuInvalidRefsReport.Visible = true;

                var templateInstance = soundNode.ObjectInstance as ITemplateInstance;
                if (null != templateInstance)
                {
                    this.mnuExpand.Visible = true;
                    this.mnuExpandXml.Visible = true;
                }

                if (soundNode.ObjectInstance.TypeName.Equals("SoundSet"))
                {
                    this.mnuSoundSetReport.Visible = true;
                }

                if (m_type.Equals("Sounds", StringComparison.InvariantCultureIgnoreCase) && m_typeDefinitions.FindTypeDefinition("ContextSound") != null)
                {
                    this.mnuInsertContext.Visible = true;
                }
            }
            else if (null != soundBankNode)
            {
                this.mnuNewSound.Visible = true;
                this.mnuGridEdit.Visible = true;
                this.mnuSeperator2.Visible = true;
                this.mnuInvalidRefsReport.Visible = true;
                this.mnuViewInP4.Visible = true;

                if (Template.TemplateLookupTables.ContainsKey(this.m_type) && Template.TemplateLookupTables[this.m_type].Count > 0)
                {
                    this.mnuTemplates.Visible = true;

                    this.mnuTemplates.DropDownItems.Clear();
                    PopulateTemplatesMenu(mnuTemplates);
                }

                this.mnuModel.Visible = true;

                this.mnuPopulateMissingFields.Visible = true;
                this.mnuBatchCreate.Visible = true;
                this.mnuNewFolder.Visible = true;

                if (soundBankNode.ObjectBank.IsCheckedOut ||
                    soundBankNode.ObjectBank.IsLocalCheckout)
                {
                    if (!soundBankNode.ObjectBank.IsAutoGenerated)
                    {
                        this.mnuSave.Visible = true;
                        this.mnuRevert.Visible = true;
                        this.mnuSave.Enabled = soundBankNode.ObjectBank.IsDirty;
                        this.mnuReloadCheckedIn.Visible = true;
                    }

                    this.mnuReload.Visible = true;
                    this.mnuReloadSaved.Visible = true;
                }
                else
                {
                    this.mnuSave.Visible = false;
                    this.mnuSubmit.Visible = false;
                    this.mnuRevert.Visible = false;
                    this.mnuCheckOut.Visible = true;
                    this.mnuLocalCheckOut.Visible = true;
                    this.mnuGetLatest.Visible = true;
                }

                if (soundBankNode.ObjectBank.IsCheckedOut)
                {
                    this.mnuSubmit.Visible = true;
                }
                else if (soundBankNode.ObjectBank.IsLocalCheckout)
                {
                    this.mnuCheckOut.Visible = true;
                }

                if (Configuration.MuteSoloEnabledTypes.Contains(this.GetObjectBrowserType().Replace("IES", "Y").TrimEnd("S".ToCharArray())))
                {
                    this.mnuMute.Visible = true;
                    this.mnuSolo.Visible = true;
                    this.mnuMute.Enabled = true;
                    this.mnuSolo.Enabled = true;
                }
            }
            else if (null != folderNode)
            {
                // Setup menu for folder node
                this.mnuGridEdit.Visible = true;
                this.mnuNewSoundBank.Visible = true;
                this.mnuNewFolder.Visible = true;
                this.mnuExpandObjectStores.Visible = true;
                this.mnuExpandCheckedOut.Visible = true;
                this.mnuGetLatest.Visible = true;
                this.mnuInvalidRefsReport.Visible = true;
                this.mnuViewInExplorer.Visible = false;
                if (Configuration.MuteSoloEnabledTypes.Contains(this.GetObjectBrowserType().Replace("IES", "Y").TrimEnd("S".ToCharArray())))
                {
                    this.mnuMute.Visible = true;
                    this.mnuSolo.Visible = true;
                    this.mnuMute.Enabled = true;
                    this.mnuSolo.Enabled = true;
                }
            }
            else if (null != virtualFolderNode)
            {
                // Setup menu for virtual folder node
                this.mnuGridEdit.Visible = true;
                this.mnuDelete.Visible = true;
                this.mnuNewSound.Visible = true;
                this.mnuRename.Visible = true;
                this.mnuInvalidRefsReport.Visible = true;

                if (Template.TemplateLookupTables.ContainsKey(this.m_type) && Template.TemplateLookupTables[this.m_type].Count > 0)
                {
                    this.mnuTemplates.Visible = true;
                    this.mnuTemplates.DropDownItems.Clear();
                    foreach (var kvp in Template.TemplateLookupTables[this.m_type])
                    {
                        var mnu = new ToolStripMenuItem(kvp.Key, null, this.mnuInsertTemplate_Click);
                        this.mnuTemplates.DropDownItems.Add(mnu);
                    }
                }

                this.mnuBatchCreate.Visible = true;
                this.mnuSeperator3.Visible = false;

                if (Configuration.MuteSoloEnabledTypes.Contains(this.GetObjectBrowserType().Replace("IES", "Y").TrimEnd("S".ToCharArray())))
                {
                    this.mnuMute.Visible = true;
                    this.mnuSolo.Visible = true;
                    this.mnuMute.Enabled = true;
                    this.mnuSolo.Enabled = true;
                }
            }

            // Setup paste menu
            if (null != soundNode)
            {
                this.InitPasteMenu();
            }

            // Finally, override with properties
            if (!this.CanAudition)
            {
                this.mnuAudition.Visible = false;
            }


            if (!this.CanViewHierarchy)
            {
                this.mnuViewHierarchy.Visible = false;
            }


            foreach (object obj in this.m_treeView.SelectedItems)
            {
                if (obj is SoundNode)
                {
                    SoundNode selsoundNode = (SoundNode)obj;
                    this.mnuMute.Visible = true;
                    this.mnuSolo.Visible = true;
                    if (selsoundNode.ObjectInstance.CheckIfMuteSoloEnabled())
                    {
                        this.mnuMute.Enabled = true;
                        this.mnuSolo.Enabled = true;
                    }

                }

            }
        }

        /// <summary>
        /// The mnu save_ on click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuSave_OnClick(object sender, EventArgs e)
        {
            var sbn = this.m_treeView.LastSelectedNode as SoundBankNode;
            if (sbn != null)
            {
                sbn.ObjectBank.Save();
            }
        }

        /// <summary>
        /// The mnu submit on click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuSubmit_OnClick(object sender, EventArgs e)
        {
            var sbn = this.m_treeView.LastSelectedNode as SoundBankNode;
            if (sbn != null)
            {
                if (sbn.ObjectBank.IsDirty)
                {
                    if (MessageBox.Show(
                        "Changes to bank must be saved before submit - save changes now?",
                        "Save Changes",
                        MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                    {
                        return;
                    }

                    if (!sbn.ObjectBank.Save())
                    {
                        ErrorManager.HandleError("Failed to save object bank " + sbn.ObjectBank.Name);
                        return;
                    }
                }

                if (sbn.ObjectBank.GetType() != typeof(ITemplateBank))
                {
                    var compiler = new XmlBankMetaDataCompiler(RaveInstance.RaveLog, null);
                    if (!compiler.CompileMetadata(sbn.ObjectBank))
                    {
                        return;
                    }
                }

                var assets = new List<IAsset> { sbn.ObjectBank.Asset };
                ctrlVisualAssetManager.Instance.CheckIn(sbn.ObjectBank.Asset.ChangeList, assets);
            }
        }

        /// <summary>
        /// The mnu search in store_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuSearchInStore_Click(object sender, EventArgs e)
        {
            var node = this.m_treeView.LastSelectedNode as BaseNode;

            if (null == node)
            {
                ErrorManager.HandleError("Failed to get last selected node");
            }

            var searchForm = new frmSearch(node.Text, null, node);
            searchForm.FindObject += this.SearchForm_OnFindObject;
            searchForm.Show(RaveInstance.ActiveWindow);
        }

        /// <summary>
        /// The mnu sound set report_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuSoundSetReport_Click(object sender, EventArgs e)
        {
            var node = this.m_treeView.LastSelectedNode as SoundNode;

            if (node != null && node.ObjectInstance.TypeName.Equals("SoundSet"))
            {
                var objs = new List<IObjectInstance> { node.ObjectInstance };
                var report = new SoundSetReport(null, objs);
                report.ShowReport();
            }
        }

        private void mnuInvalidRefsReport_Click(object sender, EventArgs e)
        {
            var node = this.m_treeView.LastSelectedNode;
            var report = new InvalidRefsReport();
            report.ShowReport(node);
        }

        /// <summary>
        /// The mnu view hierarchy_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuViewHierarchy_Click(object sender, EventArgs e)
        {
            var node = this.m_treeView.LastSelectedNode as SoundNode;

            if (node != null)
            {
                if (this.OnSoundSelect != null)
                {
                    this.OnSoundSelect(node.ObjectInstance);
                }
            }
        }

        /// <summary>
        /// The mnu view in P4 click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuViewInP4_Click(object sender, EventArgs e)
        {
            var node = this.m_treeView.LastSelectedNode as SoundBankNode;

            if (node != null)
            {
                var args = string.Format(
                    "-s {0} -p {1} -u {2} -c {3}",
                    node.ObjectBank.FilePath,
                    Configuration.AssetServer,
                    Configuration.AssetUser,
                    Configuration.AssetProject);

                ThreadPool.QueueUserWorkItem(
                    delegate { Process.Start(Configuration.ProjectSettings.P4vPath, args); });
            }
        }

        private void mnuViewInExplorer_Click(object sender, EventArgs e)
        {
            TreeNode treeNode = this.m_treeView.LastSelectedNode;
            var node = treeNode as SoundBankNode;

            while (node == null)
            {
                if (treeNode.Parent == null)
                {
                    return;
                }
                treeNode = treeNode.Parent;
                node = treeNode as SoundBankNode;
            }

            string path = node.ObjectBank.FilePath;

            ThreadPool.QueueUserWorkItem
                (
                    delegate { Process.Start("explorer.exe", "/select, " + path); }
                    );

        }
        //***EditXML for future use?**
        //private void mnuEditXML_Click(object sender, EventArgs e)
        //{
        //	TreeNode treeNode = this.m_treeView.LastSelectedNode;
        //	var node = treeNode as SoundBankNode;

        //	while (node == null)
        //	{
        //		if (treeNode.Parent == null)
        //		{
        //			return;
        //		}
        //		treeNode = treeNode.Parent;
        //		node = treeNode as SoundBankNode;
        //	}
        //	if (node.ObjectBank.IsCheckedOut)
        //	{
        //		node.ObjectBank.Save();
        //	}

        //	if (node.ObjectBank.Checkout(false))
        //	{

        //		editedObjectBank = node.ObjectBank;
        //		string path = node.ObjectBank.FilePath;

        //		ThreadPool.QueueUserWorkItem
        //			(
        //				delegate { Process.Start(path); }
        //			);
        //		RaveInstance.MainWindow.Activated += ReloadEditedObjectBank;
        //	}
        //}

        private IObjectBank editedObjectBank;

        private void ReloadEditedObjectBank(object sender, EventArgs e)
        {
            if (editedObjectBank != null)
            {
                try
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(editedObjectBank.FilePath);
                    RaveInstance.MainWindow.Activated -= ReloadEditedObjectBank;
                }
                catch (Exception exception)
                {
                    RaveInstance.MainWindow.Activated -= ReloadEditedObjectBank;
                    ErrorManager.HandleError(exception);
                    editedObjectBank.MarkAsDirty();
                    editedObjectBank.Save();
                    ErrorManager.HandleInfo(string.Format("Changes to bank:{0} done outside of rave have been reverted", editedObjectBank.Name));
                }
                editedObjectBank.Reload();
                editedObjectBank = null;

            }
        }



        private void mnuMute_Click(object sender, EventArgs e)
        {
            if (this.m_treeView.SelectedItems.Count == 1 && this.m_treeView.LastSelectedNode is SoundNode)
            {
                SoundNode sn = this.m_treeView.LastSelectedNode as SoundNode;
                if (sn != null)
                {
                    MuteSoloManager.Instance.MuteToggle(sn.ObjectInstance);
                }
            }
            else
            {
                Cursor.Current = Cursors.WaitCursor;
                List<IObjectInstance> objectList = new List<IObjectInstance>();
                foreach (TreeNode tn in this.m_treeView.SelectedItems)
                {
                    AddAllObjectNodesToList(tn, objectList);
                }

                MuteSoloManager.Instance.MuteSelection(objectList);
                Cursor.Current = Cursors.Default;
            }
        }


        private void AddAllObjectNodesToList(TreeNode tn, List<IObjectInstance> objectList)
        {
            if (tn is SoundNode)
            {
                SoundNode sn = (SoundNode)tn;
                if (sn != null)
                {
                    objectList.Add(sn.ObjectInstance);
                }
            }
            if (tn.Nodes.Count > 0)
            {
                foreach (TreeNode treeNode in tn.Nodes)
                {
                    AddAllObjectNodesToList(treeNode, objectList);
                }
            }

        }


        private void mnuSolo_Click(object sender, EventArgs e)
        {
            if (m_treeView.SelectedItems.Count > 1 || !(this.m_treeView.LastSelectedNode is SoundNode))
            {
                Cursor.Current = Cursors.WaitCursor;
                List<IObjectInstance> objectList = new List<IObjectInstance>();
                foreach (TreeNode tn in this.m_treeView.SelectedItems)
                {
                    AddAllObjectNodesToList(tn, objectList);
                }
                MuteSoloManager.Instance.SoloSelection(objectList);
                Cursor.Current = Cursors.Default;
            }
            else
            {
                SoundNode sn = this.m_treeView.LastSelectedNode as SoundNode;
                if (sn != null)
                {
                    MuteSoloManager.Instance.SoloToggle(sn.ObjectInstance);
                }
            }
        }
        #endregion
    }
}
