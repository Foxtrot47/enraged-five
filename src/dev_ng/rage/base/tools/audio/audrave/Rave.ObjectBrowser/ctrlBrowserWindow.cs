namespace Rave.ObjectBrowser
{
    using Rave.Controls.Forms;
    using Rave.ObjectBrowser.Infrastructure.Interfaces;
    using Rave.TemplateBrowser.Infrastructure.Interfaces;

    public partial class ctrlBrowserWindow : DockableWFControl, IBrowserWindow
    {
        public ctrlBrowserWindow()
        {
            this.InitializeComponent();
        }

        public IObjectBrowser ObjectBrowser
        {
            get { return this.m_ObjectBrowser; }
        }

        public ITemplateBrowser TemplateBrowser
        {
            get { return this.m_TemplateBrowser; }
        }
    }
}