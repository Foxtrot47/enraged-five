#region

using System;
using System.Windows.Controls.Primitives;
using ModelLib.Extentions;
using Models;
using Rave.Controls.Forms;
using Rave.Instance;
using Rave.ObjectBrowser.Infrastructure.Nodes;
using Rave.TypeDefinitions.Infrastructure.Interfaces;
using Rave.Types;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.Types.Infrastructure.Structs;
using Rave.Types.Objects;
using Rave.Utils;
using ObjectRef = ModelLib.ObjectRef;

#endregion

namespace Rave.ObjectBrowser.Functionality
{
    public class ContextSoundCreator
    {

        public IObjectInstance ReplaceWithContextSound(SoundNode soundNode)
        {
            SoundBankNode sbn = soundNode.Parent as SoundBankNode ?? soundNode.Parent.Parent as SoundBankNode;
            string oldName = soundNode.ObjectInstance.Name;

            if (!soundNode.ObjectInstance.Bank.IsCheckedOut)
            {
                soundNode.ObjectInstance.Bank.Checkout(false);
                soundNode = ctrlObjectBrowser.FindSoundNode(sbn, oldName);
            }

            string virtualFolder = null;
            VirtualFolderNode vfn = soundNode.Parent as VirtualFolderNode;

            if (vfn != null)
            {
                virtualFolder = vfn.ObjectName;
            }

            string newName = oldName + "_default";
            try
            {
                int version = 1;
                while (ObjectInstance.CheckForNameAndHashCollision(newName, sbn.Episode))
                {
                    if (version != 1)
                    {
                        newName = newName.Replace((version - 1).ToString("_00"), version.ToString("_00"));
                    }
                    else
                    {
                        newName = newName + version.ToString("_00");
                    }

                    version++;
                }
                soundNode.ObjectInstance.Rename(newName);
                MuteSoloManager.MuteSoloManager.Instance.Rename(soundNode.ObjectName, soundNode.ObjectInstance.TypeName, newName);
                soundNode.ObjectName = newName;

                sbn.RecreateSoundNodes();


                ObjectLookupId id = new ObjectLookupId(sbn.Type, sbn.Episode);
                string soundName = oldName;
                if (ObjectInstance.CheckForNameAndHashCollision(soundName, sbn.Episode))
                {
                    ErrorManager.HandleInfo("An object named " + soundName + " already exists, or hash is in use.");
                    return null;
                }

                ITypeDefinition soundDef =
                    soundNode.ObjectInstance.TypeDefinitions.FindTypeDefinition("ContextSound");


                IObjectInstance contextChildObject = TypeUtils.GenerateNewObjectInstance(
                    sbn.ObjectBank,
                    soundName,
                    virtualFolder,
                    RaveInstance.ObjectLookupTables[id],
                    soundDef);
                TypeUtils.ReportNewObjectCreated(contextChildObject);
                ContextSound contextSound = contextChildObject.Node.DeserializeToModel<ContextSound>();
                contextSound.DefaultSound = new ObjectRef { Reference = newName };
                contextChildObject.Node = contextSound.SerialializeToNode();
                contextChildObject.MarkAsDirty();
                sbn.RecreateSoundNodes();
                return contextChildObject;
            }
            catch (Exception ex)
            {
                ErrorManager.HandleInfo("Insert Context Sound Failed: " + ex.Message);
                return null;
            }

        }
    }
}