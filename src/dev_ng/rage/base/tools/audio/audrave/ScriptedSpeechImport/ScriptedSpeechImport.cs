// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScriptedSpeechImport.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The scripted speech import.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.ScriptedSpeechImport
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;
    using System.Xml;
    using System.Xml.Linq;

    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.UserActions;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    using Wavelib;

    class SpeechWave
    {
        public string VoiceName;
        public string WaveName;
    }

    /// <summary>
    /// The scripted speech import.
    /// </summary>
    public class ScriptedSpeechImport : IRAVEWaveImportPlugin
    {
        #region Fields

        /// <summary>
        /// The asset manager.
        /// </summary>
        private IAssetManager assetManager;

        /// <summary>
        /// The m_action log.
        /// </summary>
        private IActionLog actionLog;

        /// <summary>
        /// The m_actions.
        /// </summary>
        private ArrayList actions;

        /// <summary>
        /// The m_name.
        /// </summary>
        private string name;

        /// <summary>
        /// The built waves depot paths.
        /// </summary>
        private IList<string> builtWavesDepotPaths;

        /// <summary>
        /// The pending waves depot paths.
        /// </summary>
        private IList<string> pendingWavesDepotPaths;

        /// <summary>
        /// The built waves local paths.
        /// </summary>
        private IDictionary<string, IList<string>> builtWavesLocalPaths;

        /// <summary>
        /// The pending waves local paths.
        /// </summary>
        private IDictionary<string, IList<string>> pendingWavesLocalPaths;

        /// <summary>
        /// The m_platforms.
        /// </summary>
        private List<string> platforms;

        /// <summary>
        /// The m_wave browser.
        /// </summary>
        private IWaveBrowser waveBrowser;

        /// <summary>
        /// The existing waves.
        /// </summary>
        private IDictionary<string, ISet<SpeechWave>> existingWaves;

        /// <summary>
        /// The scripted speech packs.
        /// </summary>
        private IDictionary<BankFolderNode, IList<string>> bankFolderNodes; 

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.name;
        }

        /// <summary>
        /// Initialize the import plugin.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            this.builtWavesDepotPaths = new List<string>();
            this.pendingWavesDepotPaths = new List<string>();

            foreach (XmlNode setting in settings.ChildNodes)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;

                    case "BuiltPackFiles":
                        foreach (XmlNode childNode in setting.ChildNodes)
                        {
                            this.builtWavesDepotPaths.Add(childNode.InnerText);
                        }

                        break;

                    case "PendingPackFiles":
                        foreach (XmlNode childNode in setting.ChildNodes)
                        {
                            this.pendingWavesDepotPaths.Add(childNode.InnerText);
                        }

                        break;
                }
            }

            if (string.IsNullOrEmpty(this.name) || !this.builtWavesDepotPaths.Any() || !this.pendingWavesDepotPaths.Any())
            {
                return false;
            }

            this.assetManager = AssetManagerFactory.GetInstance(AssetManagerType.Perforce);

            if (null == this.assetManager)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The handle dropped files.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="selectedNode">
        /// The selected node.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="files">
        /// The files.
        /// </param>
        public void HandleDroppedFiles(
            IWaveBrowser waveBrowser, EditorTreeNode selectedNode, IActionLog actionLog, string[] files)
        {
            this.waveBrowser = waveBrowser;
            this.actionLog = actionLog;
            this.actions = new ArrayList();

            this.platforms = selectedNode.GetPlatforms();

            if (!this.InitData())
            {
                return;
            }

            if (!this.ValidateDroppedFiles(files))
            {
                return;
            }

            try
            {
                EditorTreeNode.AllowAutoLock = true;
                this.ImportFolder(files);
                if (this.actions.Count > 0)
                {
                    var actionParams = new ActionParams(
                        null, null, null, null, null, this.actionLog, null, null, null, this.actions);
                    var action = this.waveBrowser.CreateAction(ActionType.BulkAction, actionParams);
                    if (action.Action())
                    {
                        this.actionLog.AddAction(action);
                    }
                }
            }
            finally
            {
                EditorTreeNode.AllowAutoLock = false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Used to get newly created child node.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="name">
        /// The bank node name.
        /// </param>
        /// <returns>
        /// The bank node <see cref="BankNode"/>.
        /// </returns>
        private static BankNode GetChildNode(EditorTreeNode parentNode, string name)
        {
            for (var i = 0; i < parentNode.Nodes.Count; i++)
            {
                var bankNode = parentNode.Nodes[i] as BankNode;
                if (bankNode != null && bankNode.GetObjectName() == name)
                {
                    return bankNode;
                }
            }

            return null;
        }

        /// <summary>
        /// Initialize the required data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool InitData()
        {
            this.builtWavesLocalPaths = new Dictionary<string, IList<string>>();
            this.pendingWavesLocalPaths = new Dictionary<string, IList<string>>();
            this.existingWaves = new Dictionary<string, ISet<SpeechWave>>();

            // Locate built waves files
            foreach (var platform in this.platforms)
            {
                foreach (var builtWavesDepotPath in this.builtWavesDepotPaths)
                {
                    var builtDepotPath = Regex.Replace(builtWavesDepotPath, "{platform}", platform, RegexOptions.IgnoreCase);
                    var builtLocalPath = this.assetManager.GetLocalPath(builtDepotPath);
                    if (!File.Exists(builtLocalPath))
                    {
                        ErrorManager.HandleError("Failed to find built waves file: " + builtLocalPath);
                        return false;
                    }

                    if (!this.builtWavesLocalPaths.ContainsKey(platform))
                    {
                        this.builtWavesLocalPaths[platform] = new List<string>();
                    }

                    this.builtWavesLocalPaths[platform].Add(builtLocalPath);
                }

                foreach (var pendingWavesDepotPath in this.pendingWavesDepotPaths)
                {
                    var pendingDepotPath = Regex.Replace(pendingWavesDepotPath, "{platform}", platform, RegexOptions.IgnoreCase);
                    var pendingLocalPath = this.assetManager.GetLocalPath(pendingDepotPath);

                    // It's okay for the pending file not to exist
                    if (File.Exists(pendingLocalPath))
                    {
                        if (!this.pendingWavesLocalPaths.ContainsKey(platform))
                        {
                            this.pendingWavesLocalPaths[platform] = new List<string>();
                        }

                        this.pendingWavesLocalPaths[platform].Add(pendingLocalPath);
                    }
                }
            }

            try
            {
                // Populate list of existing wave names
                foreach (var platform in this.platforms)
                {
                    this.existingWaves[platform] = new HashSet<SpeechWave>();

                    var builtWavesPaths = this.builtWavesLocalPaths[platform];
                    foreach (var path in builtWavesPaths)
                    {
                        // Load built waves info
                        var builtDoc = XDocument.Load(path);
                        var builtBankFolderElem =
                            builtDoc.Root.Elements("BankFolder")
                                    .FirstOrDefault(n => n.Attribute("name").Value.ToUpper() == "SCRIPTED_SPEECH");
                        var builtWaveElems = builtBankFolderElem.Descendants("Wave");

                        // Add build wave info
                        foreach (var waveElem in builtWaveElems)
                        {
                            var nameElem = waveElem.Attribute("name");
                            if (null != nameElem && !string.IsNullOrEmpty(nameElem.Value))
                            {
                                string waveName = nameElem.Value.ToUpper();
                                var voiceTag = (from e in waveElem.Ancestors("WaveFolder").Descendants("Tag") where e.Attribute("name").Value == "voice" select e.Attribute("value").Value).First();
                                this.existingWaves[platform].Add(new SpeechWave{WaveName = waveName, VoiceName = voiceTag.ToUpper()});
                            }
                        }
                    }

                    // Check for any pending wave add/delete actions
                    /*
                    if (this.pendingWavesLocalPaths.ContainsKey(platform))
                    {
                        var pendingWavesPaths = this.pendingWavesLocalPaths[platform];
                        foreach (var path in pendingWavesPaths)
                        {
                            var pendingDoc = XDocument.Load(path);
                            var pendingPackElem =
                                pendingDoc.Root.Elements("Pack").FirstOrDefault();

                            if (null != pendingPackElem)
                            {
                                var pendingBankFolderElem =
                                    pendingPackElem.Elements("BankFolder")
                                                   .FirstOrDefault(
                                                       n => n.Attribute("name").Value.ToUpper() == "SCRIPTED_SPEECH");

                                if (null != pendingBankFolderElem)
                                {
                                    var pendingWaveElems = pendingBankFolderElem.Descendants("Wave");
                                    foreach (var waveElem in pendingWaveElems)
                                    {
                                        var nameElem = waveElem.Attribute("name");
                                        var name = nameElem.Value;

                                        var operationAttr = waveElem.Attribute("operation");
                                        if (null == operationAttr)
                                        {
                                            continue;
                                        }

                                        var operation = operationAttr.Value.ToLower();

                                        switch (operation)
                                        {
                                            case "add":
                                                this.existingWaves[platform].Add(name.ToUpper());
                                                break;

                                            case "remove":
                                                this.existingWaves[platform].Remove(name.ToUpper());
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }*/
                }
            }
            catch (Exception ex)
            {
                ErrorManager.HandleException("Scripted Speech Import - Failed to load wave data: ", ex);
                return false;
            }

            // Initialize list of scripted speech pack folders for dropping onto
            this.bankFolderNodes = new Dictionary<BankFolderNode, IList<string>>();
            foreach (var packNode in this.waveBrowser.GetEditorTreeView().GetPackNodes())
            {
                var packName = packNode.PackName.ToUpper();
                var allowedChars = new List<string>();
                if (!packName.StartsWith("SS_"))
                {
                    continue;
                }

                var chars = packName.Substring(3).ToCharArray();
                if (chars.Length != 2)
                {
                    ErrorManager.HandleError("Pack name not in expected format: " + packName);
                    return false;
                }

                allowedChars.Add(chars[0].ToString(CultureInfo.InvariantCulture));
                var currChar = chars[0];
                while (currChar != chars[1])
                {
                    allowedChars.Add((++currChar).ToString(CultureInfo.InvariantCulture));
                }

                BankFolderNode bankFolderNode = null;
                foreach (var node in packNode.Nodes)
                {
                    bankFolderNode = node as BankFolderNode;
                    if (null != bankFolderNode && bankFolderNode.Text.ToUpper() == "SCRIPTED_SPEECH")
                    {
                        break;
                    }
                }

                if (null == bankFolderNode)
                {
                    ErrorManager.HandleError("Failed to find SCRIPTED_SPEECH bank folder node in pack: " + packNode.PackName);
                    return false;
                }

                this.bankFolderNodes[bankFolderNode] = allowedChars;
            }

            return true;
        }

        /// <summary>
        /// Validate the dropped nodes.
        /// </summary>
        /// <param name="files">
        /// The files.
        /// </param>
        /// <returns>
        /// Indicates if dropped files are valid <see cref="bool"/>.
        /// </returns>
        private bool ValidateDroppedFiles(IEnumerable<string> files)
        {
            this.waveBrowser.GetEditorTreeView().SetText("Validating files...");

            var errors = new StringBuilder();

            foreach (var file in files)
            {
                // check that we are getting a folder
                var folderName = file.Substring(file.LastIndexOf("\\") + 1).ToUpper();

                // not folder
                if (folderName.Contains("."))
                {
                    // not a folder
                    ErrorManager.HandleInfo(folderName + " is not a folder so will not be imported");
                    return false;
                }

                // passed file is a folder so flatten and sort waves by mission
                var waves = new Dictionary<string, ArrayList>();
                var currentFolder = new DirectoryInfo(file);
                var waveNames = new ArrayList();
                
                this.FlattenFolder(currentFolder, waves, waveNames);
                
                var wavesToBeAdded = new HashSet<string>();

                foreach (var waveLists in waves)
                {
                    foreach (string wave in waveLists.Value)
                    {
                        var waveName = Path.GetFileName(wave);
                        var waveUpper = waveName.ToUpper();

                        // Check for valid sequence number
                        var nameNoExt = waveName.Substring(waveName.LastIndexOf("\\") + 1);

                        var pathElems = wave.Split('\\');
                        var parentFolder = pathElems[pathElems.Count() - 2].ToUpper();

                        // Ignore non-wav files
                        if (waveName.ToUpper().EndsWith("WAV") &&
                            Regex.Match(nameNoExt.ToUpper(), @"_\d{2}\.(WAV)").Length == 0)
                        {
                            errors.AppendLine(waveName
                                        + " is invalid: Filename must end with '_XX.wav' where X represents a digit."
                                        + Environment.NewLine);
                        }

                        // Check incoming lists for duplicates
                        if (wavesToBeAdded.Contains(waveUpper))
                        {
                            errors.AppendLine("Duplicate wave name in input list: " + waveUpper);
                        }

                        // Check existing waves for duplicates
                        // Ignore existing waves when the voice name matches, since that's just an update.
                        var foundForPlatforms = new List<string>();
                        foreach (var platformWaves in this.existingWaves)
                        {
                            var duplicates = (from w in platformWaves.Value where w.WaveName.Equals(waveUpper) && !w.VoiceName.Equals(folderName) select w);
                            if(duplicates.Count() > 0)
                            {
                                foundForPlatforms.Add(platformWaves.Key);
                            }
                        }

                        if (foundForPlatforms.Any())
                        {
                            errors.AppendLine(
                                "Duplicate wave name in existing waves: " + waveUpper +
                                " (" + string.Join(", ", foundForPlatforms) + ")");
                        }

                        wavesToBeAdded.Add(waveUpper);
                    }
                }
            }

            if (errors.Length > 0)
            {
                var errorMsg = errors.ToString();
                if (errorMsg.Length > 450)
                {
                    var tempFolder = Path.Combine(Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System)), "Temp");

                    if (!Directory.Exists(tempFolder))
                    {
                        Directory.CreateDirectory(tempFolder);
                    }

                    var tempPath = Path.Combine(tempFolder, "scripted_speech_failures.txt");
                    var writer = new StreamWriter(tempPath);
                    writer.Write(errorMsg);
                    writer.Close();

                    errorMsg = string.Format("{0}...{1}Full report saved to {2}", errorMsg.Substring(0, 450), Environment.NewLine + Environment.NewLine, tempPath);
                }

                ErrorManager.HandleInfo("Failed to import due to following error(s)" + Environment.NewLine + Environment.NewLine + errorMsg);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Takes one or more directories. For each directory it flattens the hierarchy and adds the waves, creating banks etc. where appropriate.
        /// </summary>
        /// <param name="files">
        /// The files.
        /// </param>
        private void ImportFolder(IEnumerable<string> files)
        {
            var errors = new StringBuilder();

            foreach (var file in files)
            {
                // check that we are getting a folder
                var folderName = file.Substring(file.LastIndexOf("\\") + 1).ToUpper();

                // passed file is a folder so flatten and sort waves by mission
                var waves = new Dictionary<string, ArrayList>();
                var currentFolder = new DirectoryInfo(file);
                var waveNames = new ArrayList();

                this.waveBrowser.GetEditorTreeView().SetText("Retrieving wave files...");
                this.FlattenFolder(currentFolder, waves, waveNames);

                foreach (var waveKvp in waves)
                {
                    var packFound = false;
                    foreach (var packKvp in this.bankFolderNodes)
                    {
                        var bankFolderNode = packKvp.Key;
                        var allowedChars = packKvp.Value;

                        if (!allowedChars.Any(waveKvp.Key.ToUpper().StartsWith))
                        {
                            continue;
                        }

                        packFound = true;

                        var bankExists = false;

                        foreach (var treeNode in bankFolderNode.Nodes)
                        {
                            var bank = treeNode as BankNode;
                            if (bank == null || bank.GetObjectName() != waveKvp.Key)
                            {
                                continue;
                            }

                            bankExists = true;
                            var folderExists = false;
                            foreach (TreeNode tn in bank.Nodes)
                            {
                                var wfn = tn as WaveFolderNode;
                                if (wfn != null && wfn.GetObjectName() == folderName)
                                {
                                    folderExists = true;
                                    this.ImportWaves(waveKvp.Value, wfn);
                                    break;
                                }
                            }

                            if (!folderExists)
                            {
                                var waveFolder = this.CreateFolder(folderName, bank);
                                if (waveFolder != null)
                                {
                                    this.CreateTagNode(waveFolder);
                                    this.ImportWaves(waveKvp.Value, waveFolder);
                                }
                                else
                                {
                                    ErrorManager.HandleError("Could not create wave folder " + folderName);
                                }
                            }

                            break;
                        }

                        if (!bankExists)
                        {
                            // create bank
                            this.waveBrowser.GetEditorTreeView().SetText("Creating Bank " + waveKvp.Key);
                            var action = this.waveBrowser.CreateAction(
                                ActionType.AddBank,
                                new ActionParams(
                                    this.waveBrowser,
                                    bankFolderNode,
                                    null,
                                    null,
                                    this.waveBrowser.GetEditorTreeView().GetTreeView(),
                                    this.actionLog,
                                    waveKvp.Key,
                                    new List<string>(bankFolderNode.GetPlatforms()),
                                    null,
                                    null));
                            if (action.Action())
                            {
                                this.actions.Add(action);
                            }
                            else
                            {
                                return;
                            }

                            // get added bank as node
                            var bnk = GetChildNode(bankFolderNode, waveKvp.Key);
                            if (bnk != null)
                            {
                                var waveFolder = this.CreateFolder(folderName, bnk);
                                if (waveFolder != null)
                                {
                                    this.CreateTagNode(waveFolder);
                                    this.ImportWaves(waveKvp.Value, waveFolder);
                                }
                                else
                                {
                                    ErrorManager.HandleError("Could not create wave folder " + folderName);
                                }
                            }
                            else
                            {
                                ErrorManager.HandleError("Could not create bank " + waveKvp.Key);
                            }
                        }

                        break;
                    }

                    if (!packFound)
                    {
                        errors.AppendLine("Failed to find pack for file: " + file);
                    }
                }
            }

            if (errors.Length > 0)
            {
                ErrorManager.HandleInfo("Failed to import the following wave(s):" + Environment.NewLine + Environment.NewLine + errors);
            }
        }

        /// <summary>
        /// Creates Wave Folder and returns it.
        /// </summary>
        /// <param name="folderName">
        /// The folder name.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <returns>
        /// The wave folder node <see cref="WaveFolderNode"/>.
        /// </returns>
        private WaveFolderNode CreateFolder(string folderName, BankNode parent)
        {
            var action = this.waveBrowser.CreateAction(
                ActionType.AddWaveFolder,
                new ActionParams(
                    this.waveBrowser,
                    parent,
                    null,
                    null,
                    null,
                    this.actionLog,
                    folderName,
                    new List<string>(parent.GetPlatforms()),
                    null,
                    null));
            if (action.Action())
            {
                this.actions.Add(action);
            }

            // get added bank as node
            return (WaveFolderNode)GetChildNode(parent, folderName);
        }

        /// <summary>
        /// Flattens the directory and return a flat list of waves and a Dictionary of the waves divided into appropriate banks
        /// Note: wave files dragged to the placeholder folder will not be added if they exist in the live folder
        /// </summary>
        /// <param name="dir">
        /// The directory.
        /// </param>
        /// <param name="waves">
        /// The waves.
        /// </param>
        /// <param name="waveNames">
        /// The wave names.
        /// </param>
        private void FlattenFolder(
            DirectoryInfo dir, IDictionary<string, ArrayList> waves, IList waveNames)
        {
            var childFiles = dir.GetFiles();
            foreach (var childFile in childFiles)
            {
                var fullFileName = childFile.FullName.ToUpper();
                if (fullFileName.EndsWith(".WAV"))
                {
                    var fileName = fullFileName.Substring(fullFileName.LastIndexOf('\\') + 1);
                    var mission = fileName.Substring(0, fileName.IndexOf('_'));
                    if (waves.ContainsKey(mission))
                    {
                        waveNames.Add(fileName);
                        waves[mission].Add(fullFileName);
                    }
                    else
                    {
                        waveNames.Add(fileName);
                        var al = new ArrayList { fullFileName };
                        waves.Add(mission, al);
                    }
                }
            }

            var childDirectories = dir.GetDirectories();
            foreach (var childDirectory in childDirectories)
            {
                this.FlattenFolder(childDirectory, waves, waveNames);
            }
        }

        /// <summary>
        /// The import waves.
        /// </summary>
        /// <param name="newWaves">
        /// The new waves.
        /// </param>
        /// <param name="waveFolder">
        /// The wave folder.
        /// </param>
        private void ImportWaves(IEnumerable newWaves, WaveFolderNode waveFolder)
        {
            foreach (string file in newWaves)
            {
                try
                {
                    // Ensures valid wave
                    new bwWaveFile(file, false);

                    var newWaveName = file.ToUpper().Substring(file.LastIndexOf("\\") + 1).Replace(" ", "_");
                    this.waveBrowser.GetEditorTreeView().SetText("Importing wave " + newWaveName);
                    WaveNode existingWave = null;
                    foreach (WaveNode wn in waveFolder.GetChildWaveNodes())
                    {
                        if (wn.GetObjectName().ToUpper() == newWaveName)
                        {
                            existingWave = wn;
                        }
                    }

                    if (existingWave == null)
                    {
                        // this is a new wave file
                        var action = this.waveBrowser.CreateAction(
                            ActionType.AddWave, 
                            new ActionParams(
                                this.waveBrowser, 
                                waveFolder, 
                                null, 
                                file, 
                                null, 
                                this.actionLog, 
                                null, 
                                new List<string>(waveFolder.GetPlatforms()), 
                                null, 
                                null));
                        if (action.Action())
                        {
                            this.actions.Add(action);
                        }
                    }
                    else
                    {
                        // this is an existing wave file
                        var action = this.waveBrowser.CreateAction(
                            ActionType.ChangeWave, 
                            new ActionParams(
                                this.waveBrowser, 
                                waveFolder, 
                                existingWave, 
                                file, 
                                null, 
                                this.actionLog, 
                                null, 
                                new List<string>(existingWave.GetPlatforms()), 
                                null, 
                                null));
                        if (action.Action())
                        {
                            this.actions.Add(action);
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                }
            }
        }

        /// <summary>
        /// The create tag node.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        private void CreateTagNode(BankNode parent)
        {
            var parentName = parent.GetObjectName();

            var values = this.platforms.ToDictionary(platform => platform, platform => parentName);

            var action = this.waveBrowser.CreateAction(
                ActionType.AddTag, 
                new ActionParams(
                    this.waveBrowser, parent, null, null, null, null, "voice", this.platforms, values, null));
            if (action.Action())
            {
                this.actions.Add(action);
            }
        }

        #endregion
    }
}