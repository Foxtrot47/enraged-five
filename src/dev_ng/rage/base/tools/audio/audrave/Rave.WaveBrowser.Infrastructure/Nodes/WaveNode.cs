// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WaveNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The wave node.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.WaveBrowser.Infrastructure.Nodes
{
    using System;
    using System.IO;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

	public enum WaveNodeType
	{
		Wave,
		Midi,
		Text,
		Grn
	}
    /// <summary>
    /// The wave node.
    /// </summary>
    public class WaveNode : EditorTreeNode
    {
        #region Constants

        /// <summary>
        /// The bas e_ imag e_ index.
        /// </summary>
        private const int BASE_IMAGE_INDEX = 16;

        #endregion
        
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveNode"/> class.
        /// </summary>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="strDisplayName">
        /// The display name.
        /// </param>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        public WaveNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
			switch (Path.GetExtension(this.GetObjectName().ToUpper()))
			{
				case ".WAV":
					this.Type = WaveNodeType.Wave;
					break;
				case ".MID": this.Type = WaveNodeType.Midi;
					break;
				case ".TXT": this.Type = WaveNodeType.Text;
					break;
				case ".GRN": this.Type = WaveNodeType.Grn;
					break;
				default:
					this.Type = WaveNodeType.Wave;
					break;
			}
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the type of the node.
        /// </summary>

		public WaveNodeType Type { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get bank path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetBankPath()
        {
            var bankNode = FindParentBankNode(this);
            if (null == bankNode)
            {
                throw new Exception("Failed to find parent bank node for wave node: " + this.Name);
            }

            return bankNode.GetBankPath();
        }

        /// <summary>
        /// The get base icon index.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return BASE_IMAGE_INDEX;
        }

        /// <summary>
        /// The get built path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string GetBuiltPath()
        {
            var editorTreeNode = this.Parent as EditorTreeNode;
            if (editorTreeNode != null)
            {
                var parentPath = editorTreeNode.GetBuiltPath();
                if (!string.IsNullOrEmpty(parentPath))
                {
                    return parentPath + "\\" + Path.GetFileNameWithoutExtension(this.GetObjectName());
                }
            }

            return Path.GetFileNameWithoutExtension(this.GetObjectName());
        }

        /// <summary>
        /// The get object path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public override string GetObjectPath()
        {
            var bankNode = this.Parent as BankNode;
            if (null != bankNode)
            {
                return Path.Combine(bankNode.GetObjectPath(), this.GetObjectName());
            }

            var bankFolderNode = this.Parent as BankFolderNode;
            if (null != bankFolderNode)
            {
                return Path.Combine(bankFolderNode.GetObjectPath(), this.GetObjectName());
            }

            throw new Exception("Unknown parent node type");
        }

        /// <summary>
        /// The get type description.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string GetTypeDescription()
        {
            return "Wave";
        }

        /// <summary>
        /// The get wave name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetWaveName()
        {
            return this.m_waveBrowser.FormatGameString(Path.GetFileNameWithoutExtension(this.GetObjectName()));
        }

        /// <summary>
        /// The is node type lockable.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool IsNodeTypeLockable()
        {
            return false;
        }

        /// <summary>
        /// The lock for editing.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool LockForEditing()
        {
            // search up for a bank and lock that
            var n = FindParentBankNode(this);
            if (n != null)
            {
                return n.LockForEditing();
            }

            return false;
        }

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.GetObjectPath();
        }

        /// <summary>
        /// The try to lock.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool TryToLock()
        {
            var bn = FindParentBankNode(this);
            return bn.TryToLock();
        }

        /// <summary>
        /// The unlock.
        /// </summary>
        public override void Unlock()
        {
            // nothing to do here
            return;
        }

        #endregion
    }
}
