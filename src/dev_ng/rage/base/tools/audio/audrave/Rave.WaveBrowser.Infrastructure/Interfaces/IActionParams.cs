﻿// -----------------------------------------------------------------------
// <copyright file="IActionParams.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.WaveBrowser.Infrastructure.Interfaces
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using Rave.WaveBrowser.Infrastructure.Nodes;

    public interface IActionParams
    {
        /// <summary>
        /// Gets the action log.
        /// </summary>
        IActionLog ActionLog { get; }

        /// <summary>
        /// Gets the actions.
        /// </summary>
        ArrayList Actions { get; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the node.
        /// </summary>
        EditorTreeNode Node { get; }

        /// <summary>
        /// Gets the parent node.
        /// </summary>
        EditorTreeNode ParentNode { get; }

        /// <summary>
        /// Gets the platforms.
        /// </summary>
        List<string> Platforms { get; }

        /// <summary>
        /// Gets the source path.
        /// </summary>
        string SourcePath
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the tree view.
        /// </summary>
        TreeView TreeView { get; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        Dictionary<string, string> Value { get; }

        /// <summary>
        /// Gets the wave browser.
        /// </summary>
        IWaveBrowser WaveBrowser { get; }

        /// <summary>
        /// Gets the other file extensions.
        /// </summary>
        IList<string> OtherFileExts { get; }
    }
}
