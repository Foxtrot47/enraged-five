using System.Windows.Forms;

namespace Rave.WaveBrowser.Infrastructure.Nodes
{
    using System;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    ///   Summary description for BankNode.
    /// </summary>
    public class BankNode : WaveContainerNode
    {
        private const int BASE_IMAGE_INDEX = 8;

        public BankNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
        }

        public override string GetObjectPath()
        {
            return ((PackNode)this.Parent).GetObjectPath() + "\\" + this.GetObjectName();
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return BASE_IMAGE_INDEX;
        }

        public override bool TryToLock()
        {
            this.UpdateLockedState();
            if (!this.IsLocked() &&
                !AllowAutoLock)
            {
                if (this.LockForEditing())
                {
                    return true;
                }
                return false;
            }
            return true;
        }

        public override bool LockForEditing()
        {
            if (!this.m_lockedLocally &&
                this.m_waveBrowser.RaveAssetManager.LockWavePath(this.GetObjectPath()))
            {
                this.m_lockedLocally = true;
                this.UpdateDisplay();
            }

            return this.m_lockedLocally;
        }

        public override void Unlock()
        {
            if (this.m_lockedLocally)
            {
                try
                {
                    this.m_waveBrowser.RaveAssetManager.UnlockWavePath(this.GetObjectPath());
                    this.m_lockedLocally = false;
                    this.UpdateDisplay();
                }
                catch (Exception e)
                {
                    this.m_waveBrowser.HandleError(e);
                }
            }
        }

        public override string GetBuiltPath()
        {
            if (this is WaveFolderNode)
            {
                return (this.Parent as EditorTreeNode).GetBuiltPath();
            }
            return (this.Parent as EditorTreeNode).GetBuiltPath() + "\\" + this.GetObjectName();
        }

        public override string GetTypeDescription()
        {
            return "Bank";
        }

        public string GetBankPath()
        {
            var pn = FindParentPackNode(this);
            return this.m_waveBrowser.FormatGameString(pn.GetObjectName() + "\\" + this.GetObjectName());
        }

        public string GetTreePath()
        {
            EditorTreeNode pn = FindParentPackNode(this);
            string path = string.Empty;
            EditorTreeNode node = this;
            while (node != pn)
            {
                path = path.Insert(0, node.GetObjectName() + "\\");

                node = (EditorTreeNode)node.Parent;
            }
            path = path.Insert(0, pn.GetObjectName() + "\\");
            path = path.TrimEnd('\\');

            return this.m_waveBrowser.FormatGameString(path);
        }
    }
}