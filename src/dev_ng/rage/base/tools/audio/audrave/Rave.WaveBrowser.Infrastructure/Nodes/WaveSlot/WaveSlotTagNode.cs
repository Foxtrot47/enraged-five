﻿// -----------------------------------------------------------------------
// <copyright file="WaveSlotTagNode.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.WaveBrowser.Infrastructure.Nodes.WaveSlot
{
    using System.Windows.Forms;
    using System.Xml;

    /// <summary>
    /// Wave slot tag node.
    /// </summary>
    public class WaveSlotTagNode : TreeNode
    {
        /// <summary>
        /// The value.
        /// </summary>
        private string value;

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveSlotTagNode"/> class.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        public WaveSlotTagNode(string key, string value, string platform)
        {
            this.Key = key;
            this.Platform = platform;
            this.Value = value;
        }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.value = value;
                this.UpdateDisplayText();
            }
        }

        /// <summary>
        /// Gets or sets the platform.
        /// </summary>
        public string Platform { get; set; }

        /// <summary>
        /// Serialise the tag node.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        public void Serialise(XmlNode parentNode)
        {
            XmlNode node = parentNode.OwnerDocument.CreateElement("Tag");
            node.Attributes.Append(parentNode.OwnerDocument.CreateAttribute("key"));
            node.Attributes.Append(parentNode.OwnerDocument.CreateAttribute("value"));
            node.Attributes.Append(parentNode.OwnerDocument.CreateAttribute("platform"));
            node.Attributes["key"].Value = this.Key;
            node.Attributes["value"].Value = this.Value;
            node.Attributes["platform"].Value = this.Platform;

            parentNode.AppendChild(node);
        }

        /// <summary>
        /// Update display text.
        /// </summary>
        private void UpdateDisplayText()
        {
            this.Text = string.Format("{0} ({1}) - {2}", this.Key, this.Platform, this.Value);
        }
    }
}
