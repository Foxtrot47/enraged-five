﻿// -----------------------------------------------------------------------
// <copyright file="ActionType.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.WaveBrowser.Infrastructure.Enums
{
    public enum ActionType
    {
        AddBank,
        AddBankFolder,
        AddPack,
        AddPackFolder,
        AddTag,
        AddWave,
        AddWaveFolder,
        ChangeWave,
        ChangeWaveFile,
        DeleteBank,
        DeleteBankFolder,
        DeletePack,
        DeleteTag,
        DeleteWaveFolder,
        EditWave,
        ModifyTag,
        BulkAction,
        DeleteWave,
        Dummy
    }
}
