namespace Rave.WaveBrowser.Infrastructure.Interfaces
{
    using System.Windows.Forms;

    public interface IPendingWaveLists
    {
        void LoadPack(string packName, IWaveBrowser browser);

        bool LoadLatestWaveLists();

        bool LockPendingWaveList();

        bool SerialisePendingWaveList();

        void ShowPendingChanges(TreeView treeView, IActionLog log);

        void ShowBuiltWaves(TreeView treeView, IActionLog log);

        void ShowBuiltWavesOfPack(string packName, TreeView treeView, IActionLog log);

        IPendingWaveList GetPendingWaveList(string platform);

        bool ArePendingWavelistsCheckedOut();
    }
}