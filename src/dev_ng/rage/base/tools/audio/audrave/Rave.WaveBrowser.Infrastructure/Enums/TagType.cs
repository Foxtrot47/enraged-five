﻿// -----------------------------------------------------------------------
// <copyright file="TagType.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.WaveBrowser.Infrastructure.Enums
{
    /// <summary>
    /// Tag type enums.
    /// </summary>
    public enum TagType
    {
        /// <summary>
        /// Default tag.
        /// </summary>
        Default,

        /// <summary>
        /// Voice tag.
        /// </summary>
        Voice,
    }
}
