using System;

namespace Rave.WaveBrowser.Infrastructure.Interfaces
{
    using System.Windows.Forms;
    using System.Collections.Generic;

    using Rave.WaveBrowser.Infrastructure.Nodes;

    public interface IWaveEditorTreeView
    {
        int GetIconIndex(string objName);

        void SetText(string text);

        void WavesContainString(string str, out IList<WaveNode> results);

        void WavesInFolder(string folderName, out IList<WaveNode> results);

        void SpeechToBeProcessed(string str, string mission, string character, bool includePlaceholder, bool searchConversationRoots, out IList<WaveNode> results);

        void SpeechToBeProcessed_OutOfDate(string str, string mission, string character, bool includePlaceholder, bool searchConversationRoots, out IList<WaveNode> results);

        void GetSelectedWaveNodes(TreeNode n, bool skipMetada, IList<WaveNode> nodeList, bool speechOnly = false);

        EditorTreeNode FindNode(string path, string waveName);

        EditorTreeNode FindNodeWithBankPath(string bankPath, string waveName);

        void SelectNode(string bankPath, string waveName);

        IList<PackNode> GetPackNodes();

        void AuditionWave(string path, string wave);

        TreeView GetTreeView();

        TreeNodeCollection GetNodes();

        void AddWaveNode(string path, EditorTreeNode node);

        bool DeleteWaveNode(string parentPath, string waveName, bool skipReferenceCheck = false);

        void DeleteNode(EditorTreeNode node);

        bool LoadWaveLists(bool runMetadataBuild = true);

        void LoadPackChildNodes(string packName);
        void HandleDriveFiles(string[] file, string rootDir);
        event Action<string> FolderCreated;

    }
}