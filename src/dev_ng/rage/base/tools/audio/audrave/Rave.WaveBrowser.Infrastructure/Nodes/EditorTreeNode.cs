// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EditorTreeNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The editor tree node.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.WaveBrowser.Infrastructure.Nodes
{
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml.Linq;

    using rage;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    /// The editor tree node.
    /// </summary>
    public abstract class EditorTreeNode : TreeNode
    {
        #region Static Fields

        /// <summary>
        /// The contexts requiring gestures.
        /// </summary>
        private static HashSet<string> contextsRequiringGestures;

        #endregion

        #region Fields

        /// <summary>
        /// The m_xml nodes.
        /// </summary>
        private readonly Dictionary<string, XElement> m_xmlNodes;

        /// <summary>
        /// The m_action log.
        /// </summary>
        protected IActionLog m_actionLog;

        /// <summary>
        /// The m_locked locally.
        /// </summary>
        protected bool m_lockedLocally;

        /// <summary>
        /// The m_obj name.
        /// </summary>
        protected string m_objName;

        /// <summary>
        /// The m_platform states.
        /// </summary>
        protected Dictionary<string, NodeDisplayState> m_platformStates;

        /// <summary>
        /// The m_platforms.
        /// </summary>
        protected ISet<string> m_platforms;

        /// <summary>
        /// The m_wave browser.
        /// </summary>
        protected IWaveBrowser m_waveBrowser;

        /// <summary>
        /// The m_active state.
        /// </summary>
        private NodeDisplayState m_activeState;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorTreeNode"/> class.
        /// </summary>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="strDisplayName">
        /// The str display name.
        /// </param>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        protected EditorTreeNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
        {
            this.m_xmlNodes = new Dictionary<string, XElement>();
            this.m_waveBrowser = waveBrowser;
            this.m_actionLog = actionLog;
            this.m_objName = strDisplayName;
            this.Text = this.m_objName;

            this.ImageIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.GreenIcon;
            this.SelectedImageIndex = this.ImageIndex;

            this.m_lockedLocally = false;

            this.m_platforms = new HashSet<string>();
            this.m_platformStates = new Dictionary<string, NodeDisplayState>();

            this.InitContextsRequiringGesturing();
        }

        #endregion

        #region Enums

        /// <summary>
        /// The node display state.
        /// </summary>
        public enum NodeDisplayState
        {
            /// <summary>
            /// The not present.
            /// </summary>
            NotPresent, 

            /// <summary>
            /// The already built.
            /// </summary>
            AlreadyBuilt, 

            /// <summary>
            /// The added in pending wave list.
            /// </summary>
            AddedInPendingWaveList, 

            /// <summary>
            /// The changed in pending wave list.
            /// </summary>
            ChangedInPendingWaveList, 

            /// <summary>
            /// The added locally.
            /// </summary>
            AddedLocally, 

            /// <summary>
            /// The changed locally.
            /// </summary>
            ChangedLocally, 

            /// <summary>
            /// The not present metadata.
            /// </summary>
            NotPresentMetadata, // not used can't tell if non present has metadata

            /// <summary>
            /// The already built metadata.
            /// </summary>
            AlreadyBuiltMetadata, 

            /// <summary>
            /// The added in pending wave list metadata.
            /// </summary>
            AddedInPendingWaveListMetadata, 

            /// <summary>
            /// The changed in pending wave list metadata.
            /// </summary>
            ChangedInPendingWaveListMetadata, 

            /// <summary>
            /// The added locally metadata.
            /// </summary>
            AddedLocallyMetadata, 

            /// <summary>
            /// The changed locally metadata.
            /// </summary>
            ChangedLocallyMetadata,

            /// <summary>
            /// The not loaded state.
            /// </summary>
            NotLoaded
        }

        /// <summary>
        /// The icon colours.
        /// </summary>
        private enum IconColours
        {
            /// <summary>
            /// The green icon.
            /// </summary>
            GreenIcon = 0, 

            /// <summary>
            /// The amber icon.
            /// </summary>
            AmberIcon, 

            /// <summary>
            /// The red icon.
            /// </summary>
            RedIcon, 

            /// <summary>
            /// The gray icon.
            /// </summary>
            GrayIcon, 

            /// <summary>
            /// The green metadata icon.
            /// </summary>
            GreenMetadataIcon, 

            /// <summary>
            /// The amber metadata icon.
            /// </summary>
            AmberMetadataIcon, 

            /// <summary>
            /// The red metadata icon.
            /// </summary>
            RedMetadataIcon,

            /// <summary>
            /// The red icon.
            /// </summary>
            WhiteIcon
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether allow auto lock.
        /// </summary>
        public static bool AllowAutoLock { get; set; }


        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The find parent bank node.
        /// </summary>
        /// <param name="child">
        /// The child.
        /// </param>
        /// <returns>
        /// The <see cref="BankNode"/>.
        /// </returns>
        public static BankNode FindParentBankNode(TreeNode child)
        {
            if (child == null)
            {
                return null;
            }

            if (child.GetType() == typeof(BankNode))
            {
                return child as BankNode;
            }

            return FindParentBankNode(child.Parent);
        }

        /// <summary>
        /// The find parent pack node.
        /// </summary>
        /// <param name="child">
        /// The child.
        /// </param>
        /// <returns>
        /// The <see cref="PackNode"/>.
        /// </returns>
        public static PackNode FindParentPackNode(TreeNode child)
        {
            if (child == null)
            {
                return null;
            }

            if (child.GetType() == typeof(PackNode))
            {
                return child as PackNode;
            }

            return FindParentPackNode(child.Parent);
        }

        /// <summary>
        /// Determine if a wave node requires gesture data.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// Value indicating whether wave node requires gesture data <see cref="bool"/>.
        /// </returns>
        public static bool WaveRequiresGestures(TreeNode node)
        {
            var waveNode = node as WaveNode;

            if (null == waveNode)
            {
                return false;
            }

            var packNode = FindParentPackNode(waveNode);
            if (null == packNode || !packNode.PackName.ToLower().Equals("speech"))
            {
                return false;
            }

            var nameLower = node.Text.ToLower();

            return contextsRequiringGestures.Where(nameLower.StartsWith).Any();
        }

        /// <summary>
        /// The add platform.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="state">
        /// The state.
        /// </param>
        public virtual void AddPlatform(string platform, NodeDisplayState state)
        {
            if (platform != "All")
            {
                foreach (var p in this.m_platforms)
                {
                    if (p == platform)
                    {
                        this.m_platformStates[platform] = state;
                        return;
                    }
                }

                this.m_platforms.Add(platform);
                this.m_platformStates[platform] = state;
            }
            else
            {
                // add all platforms
                foreach (PlatformSetting setting in this.m_waveBrowser.PlatformSettings)
                {
                    if (!this.m_platforms.Contains(setting.PlatformTag))
                    {
                        this.m_platforms.Add(setting.PlatformTag);
                        this.m_platformStates[setting.PlatformTag] = state;
                    }
                }
            }
        }

        public void SetPlatformXmlNode(string platform, XElement node)
        {
            if (this.m_xmlNodes.ContainsKey(platform))
            {
                this.m_xmlNodes[platform] = node;
            }
            else
            {
                this.m_xmlNodes.Add(platform, node);
            }
        }

        public XElement GetNodeXmlForPlatform(string platform)
        {
            if (this.m_xmlNodes.ContainsKey(platform))
            {
                return this.m_xmlNodes[platform];
            }

            return null;
        }

        /// <summary>
        /// The get base icon index.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public abstract int GetBaseIconIndex(IWaveBrowser waveBrowser);

        /// <summary>
        /// The get built path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public virtual string GetBuiltPath()
        {
            return string.Empty;
        }

        /// <summary>
        /// The get node display state.
        /// </summary>
        /// <returns>
        /// The <see cref="NodeDisplayState"/>.
        /// </returns>
        public NodeDisplayState GetNodeDisplayState()
        {
            return this.m_activeState;
        }

        /// <summary>
        /// The get node display state.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <returns>
        /// The <see cref="NodeDisplayState"/>.
        /// </returns>
        public NodeDisplayState GetNodeDisplayState(string platform)
        {
            if (!this.m_platforms.Contains(platform))
            {
                return NodeDisplayState.NotPresent;
            }

            return this.m_platformStates[platform];
        }

        /// <summary>
        /// The get object name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetObjectName()
        {
            return this.m_objName;
        }

        /// <summary>
        /// The get object path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public abstract string GetObjectPath();

        /// <summary>
        /// The get platforms.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<string> GetPlatforms()
        {
            return this.m_platforms.ToList();
        }

        /// <summary>
        /// The get platforms string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetPlatformsString()
        {
            var ret = string.Empty;
            if (this.m_platforms.Count != this.m_waveBrowser.PlatformSettings.Count)
            {
                var needComma = false;
                foreach (var platform in this.m_platforms)
                {
                    if (needComma)
                    {
                        ret += ",";
                    }

                    ret += platform;
                    needComma = true;
                }
            }
            else
            {
                ret = "All";
            }

            return ret;
        }

        /// <summary>
        /// The get state description.
        /// </summary>
        /// <param name="platformtag">
        /// The platformtag.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetStateDescription(string platformtag)
        {
            if (!this.m_platformStates.ContainsKey(platformtag))
            {
                return "Not Present";
            }

            return this.GetStateDescription(this.m_platformStates[platformtag]);
        }

        /// <summary>
        /// The get state description.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetStateDescription()
        {
            return this.GetStateDescription(this.m_activeState);
        }

        /// <summary>
        /// The get state description.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetStateDescription(NodeDisplayState state)
        {
            string desc;

            switch (state)
            {
                case NodeDisplayState.AlreadyBuilt:
                    desc = "Built";
                    break;
                case NodeDisplayState.AlreadyBuiltMetadata:
                    desc = "Built: Custom Metadata";
                    break;
                case NodeDisplayState.AddedInPendingWaveList:
                case NodeDisplayState.ChangedInPendingWaveList:
                    desc = "Pending";
                    break;
                case NodeDisplayState.AddedInPendingWaveListMetadata:
                case NodeDisplayState.ChangedInPendingWaveListMetadata:
                    desc = "Pending: Custom Metadata";
                    break;
                case NodeDisplayState.AddedLocally:
                    desc = "Added locally";
                    break;
                case NodeDisplayState.AddedLocallyMetadata:
                    desc = "Added locally: Cutsom Metadata";
                    break;
                case NodeDisplayState.ChangedLocally:
                    desc = "Changed locally";
                    break;
                case NodeDisplayState.ChangedLocallyMetadata:
                    desc = "Changed locally: Custom MetaData";
                    break;
                default:
                    desc = "Unknown";
                    break;
            }

            if (this.m_lockedLocally)
            {
                desc += " (locked locally)";
            }

            return desc;
        }

        /// <summary>
        /// The get type description.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public abstract string GetTypeDescription();

        /// <summary>
        /// The is locked.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsLocked()
        {
            return this.m_lockedLocally;
        }

        /// <summary>
        /// The is node type lockable.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool IsNodeTypeLockable()
        {
            return true;
        }

        /// <summary>
        /// The lock for editing.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public abstract bool LockForEditing();

        /// <summary>
        /// The remove platform.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        public virtual void RemovePlatform(string platform)
        {
            this.m_platforms.Remove(platform);
            this.m_platformStates.Remove(platform);
        }

        /// <summary>
        /// The set platform display state.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="state">
        /// The state.
        /// </param>
        public void SetPlatformDisplayState(string platform, NodeDisplayState state)
        {
            if (platform == "All")
            {
                foreach (var p in this.m_platforms)
                {
                    this.m_platformStates[p] = state;
                }
            }
            else
            {
                this.m_platformStates[platform] = state;
            }
        }

        /// <summary>
        /// The try to lock.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public abstract bool TryToLock();

        /// <summary>
        /// The unlock.
        /// </summary>
        public abstract void Unlock();

        /// <summary>
        /// The update display.
        /// </summary>
        public void UpdateDisplay()
        {
            var iconIndex = 0;
            this.m_activeState = this.GetActiveDisplayState();

            switch (this.m_activeState)
            {
                case NodeDisplayState.AlreadyBuilt:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.GreenIcon;
                    break;
                case NodeDisplayState.AlreadyBuiltMetadata:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.GreenMetadataIcon;
                    break;
                case NodeDisplayState.AddedInPendingWaveList:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.AmberIcon;
                    break;
                case NodeDisplayState.AddedInPendingWaveListMetadata:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.AmberMetadataIcon;
                    break;
                case NodeDisplayState.ChangedInPendingWaveList:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.AmberIcon;
                    break;
                case NodeDisplayState.ChangedInPendingWaveListMetadata:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.AmberMetadataIcon;
                    break;
                case NodeDisplayState.AddedLocally:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.RedIcon;
                    break;
                case NodeDisplayState.AddedLocallyMetadata:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.RedMetadataIcon;
                    break;
                case NodeDisplayState.ChangedLocally:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.RedIcon;
                    break;
                case NodeDisplayState.ChangedLocallyMetadata:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.RedMetadataIcon;
                    break;
                case NodeDisplayState.NotPresent:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.GrayIcon;
                    break;
                case NodeDisplayState.NotLoaded:
                    iconIndex = this.GetBaseIconIndex(this.m_waveBrowser) + (int)IconColours.WhiteIcon;
                    break;
            }

            // wave icon doesn't change for locally locked
            if (this.m_lockedLocally && this.GetType() != typeof(WaveNode))
            {
                iconIndex += 4;
            }

            this.SelectedImageIndex = this.ImageIndex = iconIndex;

            if (this.m_activeState == NodeDisplayState.NotPresent)
            {
                this.ForeColor = Color.Gray;
            }
            else
            {
                if (WaveRequiresGestures(this))
                {
                    this.ForeColor = Color.MediumBlue;
                }
                else
                {
                    this.ForeColor = Color.Black;
                }
            }

            this.UpdateDisplayName();
        }

        /// <summary>
        /// The update display name.
        /// </summary>
        public void UpdateDisplayName()
        {
            var name = this.m_objName;
            var activePlatformCount = this.m_waveBrowser.PlatformSettings.Cast<PlatformSetting>().Count(platformSetting => platformSetting.IsActive);

            if (this.m_platforms.Count != activePlatformCount)
            {
                name = this.m_objName + " (" + this.GetPlatformsString() + ")";
            }

            // Only want to set Text if value has changed (performance improvement)
            if (!this.Text.Equals(name))
            {
                this.Text = name;
            }
        }

        /// <summary>
        /// The update display state.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="bForce">
        /// The b force.
        /// </param>
        /// <param name="bUpdateParent">
        /// The b update parent.
        /// </param>
        public void UpdateDisplayState(NodeDisplayState state, string platform, bool bForce, bool bUpdateParent)
        {
            this.SetPlatformDisplayState(platform, state);

            if (bForce || this.m_activeState != state)
            {
                this.UpdateDisplay();
            }

            if (bUpdateParent)
            {
                if (this.m_activeState != state && state != NodeDisplayState.NotPresent && (int)this.m_activeState > 5)
                {
                    state = state - 6;
                }

                var parent = this.Parent as EditorTreeNode;
                if (parent != null)
                {
                    parent.UpdateDisplayState(state, platform, bForce, true);
                }
            }
        }

        /// <summary>
        /// The update locked state.
        /// </summary>
        public void UpdateLockedState()
        {
            this.m_lockedLocally = this.m_waveBrowser.RaveAssetManager.IsWavePathLocked(this.GetObjectPath());
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initialize the contexts requiring gesturing.
        /// </summary>
        private void InitContextsRequiringGesturing()
        {
            if (null != contextsRequiringGestures)
            {
                return;
            }

            contextsRequiringGestures = new HashSet<string>();
            var contextListPath = this.m_waveBrowser.WaveGestureContextList;

            if (!File.Exists(contextListPath))
            {
                return;
            }

            using (var reader = new StreamReader(contextListPath))
            {
                var line = reader.ReadLine();
                while (!string.IsNullOrEmpty(line))
                {
                    var context = line.Split(',')[0];
                    if (!string.IsNullOrEmpty(context))
                    {
                        contextsRequiringGestures.Add(context.ToLower());
                    }

                    line = reader.ReadLine();
                }
            }
        }

        /// <summary>
        /// The get active display state.
        /// </summary>
        /// <returns>
        /// The <see cref="NodeDisplayState"/>.
        /// </returns>
        private NodeDisplayState GetActiveDisplayState()
        {
            NodeDisplayState state;

            var packNode = this as PackNode;
            if (null != packNode && !packNode.IsLoaded)
            {
                state = NodeDisplayState.NotLoaded;
            }
            else
            {
                var activePlatform = this.m_waveBrowser.GetActivePlatform();
                if (activePlatform == "All")
                {
                    // find the 'dominant' state
                    var statenum = 0;
                    foreach (var platform in this.m_platforms)
                    {
                        // use mod 6 to distguish between states with metadata
                        if (this.m_platformStates.ContainsKey(platform)
                            && (int)this.m_platformStates[platform] > statenum)
                        {
                            statenum = (int)this.m_platformStates[platform];
                        }
                    }

                    state = (NodeDisplayState)statenum;
                }
                else
                {
                    state = this.m_platformStates.ContainsKey(this.m_waveBrowser.GetActivePlatform())
                                ? this.m_platformStates[this.m_waveBrowser.GetActivePlatform()]
                                : NodeDisplayState.NotPresent;
                }
            }

            return state;
        }

        #endregion
    }
}