namespace Rave.WaveBrowser.Infrastructure.Nodes
{
    using System;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    ///   Summary description for PackNode.
    /// </summary>
    public class PackNode : WaveContainerNode
    {
        private const int BASE_IMAGE_INDEX = 0;

        public PackNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser, string packName)
            : base(actionLog, strDisplayName, waveBrowser)
        {
            this.PackName = packName;
        }

        public string PackName { get; private set; }

        public bool IsLoaded { get; set; }

        public override string GetObjectPath()
        {
            if (this.Parent != null)
            {
                return ((PackFolderNode) this.Parent).GetObjectPath() + "\\" + this.GetObjectName();
            }
            return this.GetObjectName();
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return BASE_IMAGE_INDEX;
        }

        public override bool TryToLock()
        {
            this.UpdateLockedState();
            if (!this.IsLocked() &&
                !AllowAutoLock)
            {
                return this.LockForEditing();
            }
            return true;
        }

        public override bool LockForEditing()
        {
            if (!this.m_lockedLocally &&
                this.m_waveBrowser.RaveAssetManager.LockWavePath(this.GetObjectPath()))
            {
                this.m_lockedLocally = true;
                this.UpdateDisplay();
            }

            return this.m_lockedLocally;
        }

        public override void Unlock()
        {
            if (this.m_lockedLocally)
            {
                try
                {
                    this.m_waveBrowser.RaveAssetManager.UnlockWavePath(this.GetObjectPath());
                    this.m_lockedLocally = false;
                    this.UpdateDisplay();
                }
                catch (Exception e)
                {
                    this.m_waveBrowser.HandleError(e);
                }
            }
        }

        public override string GetBuiltPath()
        {
            if (this is BankFolderNode)
            {
                return (this.Parent as EditorTreeNode).GetBuiltPath();
            }
            return this.GetObjectName();
        }

        public override string GetTypeDescription()
        {
            return "Pack";
        }
    }
}