namespace Rave.WaveBrowser.Infrastructure.Interfaces
{
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    /// Summary description for UserAction.
    /// </summary>
    public interface IUserAction
    {
        // Fully commit the action
        bool Commit();
        
        // reset the UI state to undo this action 
        bool Undo();

        // update the UI state to show the action has been performed
        bool Action();

        // return a user-friendly summary string
        string GetSummary();

        // returns the asset path that this action affects
        string GetAssetPath();

        /// <summary>
        /// Gets the tree node associated with the action.
        /// </summary>
        EditorTreeNode Node { get; }

        /// <summary>
        /// Gets the action parameters.
        /// </summary>
        IActionParams ActionParameters { get; }

        bool Apply(IPendingWaveList wavelist, string platform);
    }
}
