namespace Rave.WaveBrowser.Infrastructure.Nodes
{
    using System;
    using System.IO;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    ///   Summary description for WaveFolderNode.
    /// </summary>
    public class BankFolderNode : PackNode
    {
        private const int BASE_IMAGE_INDEX = 23;

        public BankFolderNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser, null)
        {
        }

        public override string GetObjectPath()
        {
            var packNode = this.Parent as PackNode;
            if (null != packNode)
            {
                return Path.Combine(packNode.GetObjectPath(), this.GetObjectName());
            }

            var bankNode = this.Parent as BankNode;
            if (null != bankNode)
            {
                return Path.Combine(bankNode.GetObjectPath(), this.GetObjectName());
            }

            throw new Exception("Unknown parent node type");
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return BASE_IMAGE_INDEX;
        }

        public override bool TryToLock()
        {
            var pn = FindParentPackNode(this);
            return pn.TryToLock();
        }

        public override bool LockForEditing()
        {
            var pn = FindParentPackNode(this);
            if (pn != null)
            {
                return pn.LockForEditing();
            }
            return false;
        }

        public override void Unlock()
        {
            if (this.m_lockedLocally)
            {
                try
                {
                    this.m_waveBrowser.RaveAssetManager.UnlockWavePath(this.GetObjectPath());
                    this.m_lockedLocally = false;
                    this.UpdateDisplay();
                }
                catch (Exception e)
                {
                    this.m_waveBrowser.HandleError(e);
                }
            }
        }

        public override string GetTypeDescription()
        {
            return "Bank Folder";
        }
    }
}