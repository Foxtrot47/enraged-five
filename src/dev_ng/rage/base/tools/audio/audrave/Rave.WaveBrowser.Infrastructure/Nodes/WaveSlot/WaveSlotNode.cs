namespace Rave.WaveBrowser.Infrastructure.Nodes.WaveSlot
{
    using System.Windows.Forms;
    using System.Xml;

    /// <summary>
    ///   Summary description for WaveSlotNode.
    /// </summary>
    public class WaveSlotNode : TreeNode
    {
        #region SlotLoadType enum

        public enum SlotLoadType
        {
            BANK,
            WAVE,
            STREAM,
            BUFFER,
        }

        #endregion

        private readonly string m_Name;
        private readonly SlotLoadType m_Type;
        private int m_Size;

        public WaveSlotNode(string name, SlotLoadType type)
        {
            this.m_Name = this.Text = name;
            this.m_Type = type;

            switch (type)
            {
                case SlotLoadType.BANK:
                    this.ImageIndex = this.SelectedImageIndex = 0;
                    break;
                case SlotLoadType.WAVE:
                    this.ImageIndex = this.SelectedImageIndex = 1;
                    break;
                case SlotLoadType.STREAM:
                    this.ImageIndex = this.SelectedImageIndex = 5;
                    break;
                case SlotLoadType.BUFFER:
                    this.ImageIndex = this.SelectedImageIndex = 6;
                    break;
            }
        }

        public new string Name
        {
            get { return this.m_Name; }
        }

        public SlotLoadType Type
        {
            get { return this.m_Type; }
        }

        public int Size
        {
            get { return this.m_Size; }
        }

        public void SetSize(int size)
        {
            this.m_Size = size;
            this.Text = this.m_Name + " (" + size + " bytes)";
        }

        public void Serialise(XmlNode parentNode)
        {
            XmlNode node = parentNode.OwnerDocument.CreateElement("Slot");
            node.Attributes.Append(parentNode.OwnerDocument.CreateAttribute("name"));
            node.Attributes.Append(parentNode.OwnerDocument.CreateAttribute("loadType"));
            node.Attributes["name"].Value = this.m_Name;
            node.Attributes["loadType"].Value = this.Type.ToString();
            foreach (TreeNode childNode in this.Nodes)
            {
                var loadableNode = childNode as LoadableNode;
                var tagNode = childNode as WaveSlotTagNode;

                if (null != loadableNode)
                {
                    loadableNode.Serialise(node);
                }
                else if (null != tagNode)
                {
                    tagNode.Serialise(node);
                }
            }

            if (this.m_Type == SlotLoadType.BUFFER)
            {
                node.Attributes.Append(parentNode.OwnerDocument.CreateAttribute("size"));
                node.Attributes["size"].Value = this.Size.ToString();
            }

            parentNode.AppendChild(node);
        }
    }
}