using System.Collections.Generic;
using Rave.WaveBrowser.Infrastructure.Nodes;

namespace Rave.WaveBrowser.Infrastructure.Interfaces
{
    public interface IActionLog
    {
        void AddAction(IUserAction action);

        void Reset();

        IUserAction GetUndoAction();

        IUserAction GetRedoAction();

        void UndoLastAction();

        void RedoNextAction();

        bool Commit();

        string GetCommitSummary();

        int GetCommitLogCount();

        IUserAction SearchForAction(string wavePath);

        IUserAction SearchForWaveDeletion(string wavePath);

        IUserAction SearchForWaveAdd(string wavePath);

        IUserAction SearchForBankDeletion(string bankPath);

        IUserAction SearchForBankAdd(string bankPath);

        IUserAction SearchForPackDeletion(string packPath);

        IUserAction SearchForPackAdd(string packPath);

        IEnumerable<IUserAction> SearchForActions(BankNode banknode);
    }
}