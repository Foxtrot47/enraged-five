namespace Rave.WaveBrowser.Infrastructure.Nodes
{
    using System;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    ///   Summary description for WaveFolderNode.
    /// </summary>
    public class WaveFolderNode : BankNode
    {
        private const int BASE_IMAGE_INDEX = 23;

        public WaveFolderNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
        }

        public override string GetObjectPath()
        {
            return ((BankNode) this.Parent).GetObjectPath() + "\\" + this.GetObjectName();
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return BASE_IMAGE_INDEX;
        }

        public override bool TryToLock()
        {
            var bn = FindParentBankNode(this);
            return bn.TryToLock();
        }

        public override bool LockForEditing()
        {
            var bn = FindParentBankNode(this);
            if (bn != null)
            {
                return bn.LockForEditing();
            }
            return false;
        }

        public override void Unlock()
        {
            if (this.m_lockedLocally)
            {
                try
                {
                    this.m_waveBrowser.RaveAssetManager.UnlockWavePath(this.GetObjectPath());
                    this.m_lockedLocally = false;
                    this.UpdateDisplay();
                }
                catch (Exception e)
                {
                    this.m_waveBrowser.HandleError(e);
                }
            }
        }

        public override string GetTypeDescription()
        {
            return "Wave Folder";
        }
    }
}