namespace Rave.WaveBrowser.Infrastructure.Nodes
{
    using System.Collections.Generic;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    ///   Summary description for TagNode.
    /// </summary>
    public class PresetNode : EditorTreeNode
    {
        private readonly Dictionary<string, string> m_platformValues;
        private readonly string m_presetName;

        public PresetNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
            this.m_presetName = strDisplayName;
            this.m_platformValues = new Dictionary<string, string>();
        }

        public Dictionary<string, string> Values
        {
            get { return this.m_platformValues; }
        }

        public string GetPresetName()
        {
            return this.m_presetName;
        }

        public string GetCurrentValue(string platform)
        {
            if (!this.m_platformValues.ContainsKey(platform))
            {
                return null;
            }
            return this.m_platformValues[platform];
        }

        public override string GetObjectPath()
        {
            return ((EditorTreeNode) this.Parent).GetObjectPath() + "\\" + this.m_presetName;
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return 31;
        }

        public override bool LockForEditing()
        {
            return false;
        }

        public override bool TryToLock()
        {
            return false;
        }

        public override void Unlock()
        {
            //Nothing to do here
            return;
        }

        public override bool IsNodeTypeLockable()
        {
            return false;
        }

        public override string GetTypeDescription()
        {
            return "Preset";
        }
    }
}