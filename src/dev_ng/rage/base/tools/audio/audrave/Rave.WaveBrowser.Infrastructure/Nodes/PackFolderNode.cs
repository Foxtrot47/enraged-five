namespace Rave.WaveBrowser.Infrastructure.Nodes
{
    using System;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    ///   Summary description for PackNode.
    /// </summary>
    public class PackFolderNode : WaveContainerNode
    {
        private const int BASE_IMAGE_INDEX = 23;

        public PackFolderNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
        }

        public override string GetObjectPath()
        {
            return this.GetObjectName();
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return BASE_IMAGE_INDEX;
        }

        public override bool TryToLock()
        {
            this.UpdateLockedState();
            if (!this.IsLocked() &&
                !AllowAutoLock)
            {
                if (this.LockForEditing())
                {
                    return true;
                }
                return false;
            }
            return true;
        }

        public override bool LockForEditing()
        {
            if (!this.m_lockedLocally &&
                this.m_waveBrowser.RaveAssetManager.LockWavePath(this.GetObjectPath()))
            {
                this.m_lockedLocally = true;
                this.UpdateDisplay();
            }

            return this.m_lockedLocally;
        }

        public override void Unlock()
        {
            if (this.m_lockedLocally)
            {
                try
                {
                    this.m_waveBrowser.RaveAssetManager.UnlockWavePath(this.GetObjectPath());
                    this.m_lockedLocally = false;
                    this.UpdateDisplay();
                }
                catch (Exception e)
                {
                    this.m_waveBrowser.HandleError(e);
                }
            }
        }

        public override string GetTypeDescription()
        {
            return "Pack Folder";
        }
    }
}