namespace Rave.WaveBrowser.Infrastructure.Interfaces
{
    using System;
    using System.Collections;

    using Rave.AssetManager.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Enums;

    using Forms = System.Windows.Forms;
    using Wpf = System.Windows.Controls;

    public interface IWaveBrowser
    {
        #region Public Events

        /// <summary>
        ///     The changelist modified.
        /// </summary>
        event Action ChangelistModified;

        /// <summary>
        /// The get latest.
        /// </summary>
        event Action<string> GetLatest;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the menu.
        /// </summary>
        Forms.ContextMenuStrip Menu { get; }

        /// <summary>
        /// Gets the rave asset manager.
        /// </summary>
        IRaveAssetManager RaveAssetManager { get; }

        /// <summary>
        /// Gets the platform settings.
        /// </summary>
        ArrayList PlatformSettings { get; }

        /// <summary>
        /// Gets the wave gesture context list.
        /// </summary>
        string WaveGestureContextList { get; }

        #endregion
        
        /// <summary>
        /// The commit changes.
        /// </summary>
        /// <param name="showDialogue">
        /// The show dialogue.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool CommitChanges(bool showDialogue);

        /// <summary>
        /// The create action.
        /// </summary>
        /// <param name="actionType">
        /// The action type.
        /// </param>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        /// <returns>
        /// The <see cref="IUserAction"/>.
        /// </returns>
        IUserAction CreateAction(ActionType actionType, IActionParams parameters);

        /// <summary>
        /// The create pending wave lists.
        /// </summary>
        void CreatePendingWaveLists();

        /// <summary>
        /// The get action log.
        /// </summary>
        /// <returns>
        /// The <see cref="IActionLog"/>.
        /// </returns>
        IActionLog GetActionLog();

        /// <summary>
        /// The get active platform.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetActivePlatform();

        /// <summary>
        /// The get editor tree view.
        /// </summary>
        /// <returns>
        /// The <see cref="IWaveEditorTreeView"/>.
        /// </returns>
        IWaveEditorTreeView GetEditorTreeView();

        /// <summary>
        /// The get pending wave lists.
        /// </summary>
        /// <returns>
        /// The <see cref="IPendingWaveLists"/>.
        /// </returns>
        IPendingWaveLists GetPendingWaveLists();

        /// <summary>
        /// Load the wave lists (if the wave tree is visible).
        /// </summary>
        /// <param name="runMetadataBuild">
        /// Run metadata build flag.
        /// </param>
        void LoadWaveLists(bool runMetadataBuild = true);

        /// <summary>
        /// The on close.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool OnClose();

        /// <summary>
        /// The init main menu.
        /// </summary>
        /// <param name="waveBrowserMenu">
        /// The wave browser menu.
        /// </param>
        /// <param name="buildMenu">
        /// The build menu.
        /// </param>
        void InitMainMenu(Wpf.MenuItem waveBrowserMenu, Wpf.MenuItem buildMenu);

        /// <summary>
        /// The init.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool Init();

        /// <summary>
        /// The format game string.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string FormatGameString(string input);

        /// <summary>
        /// Handle an error.
        /// </summary>
        /// <param name="ex">
        /// The ex.
        /// </param>
        void HandleError(Exception ex);
    }
}