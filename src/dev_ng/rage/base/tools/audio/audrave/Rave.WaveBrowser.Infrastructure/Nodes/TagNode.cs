namespace Rave.WaveBrowser.Infrastructure.Nodes
{
    using System.Collections.Generic;

    using Rave.WaveBrowser.Infrastructure.Interfaces;

    /// <summary>
    ///   Summary description for TagNode.
    /// </summary>
    public class TagNode : EditorTreeNode
    {
        private readonly Dictionary<string, string> m_platformValues;
        private string m_tagName;

        public TagNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
            this.m_tagName = strDisplayName;
            this.m_platformValues = new Dictionary<string, string>();
        }

        public Dictionary<string, string> Values
        {
            get { return this.m_platformValues; }
        }

        public string GetTagName()
        {
            return this.m_tagName;
        }

        public void SetTagName(string tagName)
        {
            this.m_tagName = tagName;
        }

        public void RemoveValue(string platform)
        {
            if (this.m_platformValues.ContainsKey(platform))
            {
                this.m_platformValues.Remove(platform);
            }
            if (this.m_platforms.Contains(platform))
            {
                this.m_platforms.Remove(platform);
            }
        }

        public void SetValue(string platform, string val)
        {
            if (platform == null)
            {
                foreach (var s in this.m_platforms)
                {
                    this.m_platformValues[s] = val;
                }
            }
            else
            {
                if (!this.m_platforms.Contains(platform))
                {
                    this.m_platforms.Add(platform);
                }

                this.m_platformValues[platform] = val;
            }

            this.UpdateDisplayName();
        }

        public string GetCurrentValue(string platform)
        {
            if (!this.m_platformValues.ContainsKey(platform))
            {
                return null;
            }
            return this.m_platformValues[platform];
        }

        public override string GetObjectPath()
        {
            return ((EditorTreeNode) this.Parent).GetObjectPath() + "\\" + this.m_tagName;
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return waveBrowser.GetEditorTreeView().GetIconIndex(this.m_objName);
        }

        public override bool LockForEditing()
        {
            return false;
        }

        public override bool TryToLock()
        {
            return false;
        }

        public override void Unlock()
        {
            //Nothing to do here
            return;
        }

        public override bool IsNodeTypeLockable()
        {
            return false;
        }

        public override string GetTypeDescription()
        {
            return "Tag";
        }
    }
}