namespace Rave.WaveBrowser.Infrastructure.Nodes.WaveSlot
{
    using System.Windows.Forms;
    using System.Xml;

    /// <summary>
    ///   Summary description for LoadableNode.
    /// </summary>
    public class LoadableNode : TreeNode
    {
        #region LoadableType enum

        public enum LoadableType
        {
            BANK,
            PACK,
            BANKFOLDER
        }

        #endregion

        private readonly string m_Name;
        private readonly LoadableType m_Type;

        public LoadableNode(string name, LoadableType type)
        {
            this.Text = this.m_Name = name;
            this.m_Type = type;

            switch (type)
            {
                case LoadableType.BANK:
                    this.ImageIndex = this.SelectedImageIndex = 2;
                    break;
                case LoadableType.PACK:
                    this.ImageIndex = this.SelectedImageIndex = 3;
                    break;
                case LoadableType.BANKFOLDER:
                    this.ImageIndex = this.SelectedImageIndex = 4;
                    break;
            }
        }

        public new string Name
        {
            get { return this.m_Name; }
        }

        public LoadableType Type
        {
            get { return this.m_Type; }
        }

        public void Serialise(XmlNode parentNode)
        {
            XmlNode node = parentNode.OwnerDocument.CreateElement("Loadable");
            node.Attributes.Append(parentNode.OwnerDocument.CreateAttribute("type"));
            node.Attributes.Append(parentNode.OwnerDocument.CreateAttribute("name"));
            node.Attributes["type"].Value = this.Type.ToString();
            node.Attributes["name"].Value = this.Name;

            parentNode.AppendChild(node);
        }
    }
}