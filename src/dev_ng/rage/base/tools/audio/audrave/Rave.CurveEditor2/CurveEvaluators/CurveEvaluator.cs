using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Rave.CurveEditor2.Curves;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor2.CurveEvaluators
{
    public abstract class CurveEvaluator : INotifyPropertyChanged,
                                       IDisposable
    {
        private const int DEFAULT_CURVE_THICKNESS = 2;
        private const int SELECTED_CURVE_THICKNESS = 3;
        private string m_curveGeometry;
        private int m_curveThickness;

        protected CurveEvaluator(CurveEditorModel parent, string curveColor, int? maxControlPoints, Tuple<ExtendedLineMode, ExtendedLineMode> extendedLines = null)
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }
            Parent = parent;
            Curve = new Curve(this, curveColor, maxControlPoints, extendedLines);
            CurveThickness = DEFAULT_CURVE_THICKNESS;
            Curve.ControlPoints.ListChanged += OnListChanged;
           
            parent.XAxis.LengthChanged += OnAxisChangedDispatcher;
            parent.YAxis.LengthChanged += OnAxisChangedDispatcher;
            DashArray = "";
        }

        private void OnAxisChangedDispatcher()
        {
            Application.Current.Dispatcher.Invoke(AxisChanged);
        }

        protected virtual void AxisChanged()
        {
            Evaluate();
        }

        #region ICurveEvaluator Members

        public event PropertyChangedEventHandler PropertyChanged;

        public CurveEditorModel Parent { get; protected set; }

        public bool IsSelected
        {
            get { return Curve.IsSelected; }
            set
            {
                if (Curve.IsSelected != value)
                {
                    Curve.IsSelected = value;
                    CurveThickness = value ? SELECTED_CURVE_THICKNESS : DEFAULT_CURVE_THICKNESS;
                    PropertyChanged.Raise(this, "IsSelected");
                }
            }
        }

        public int CurveThickness
        {
            get { return m_curveThickness; }
            private set
            {
                if (m_curveThickness != value)
                {
                    m_curveThickness = value;
                    PropertyChanged.Raise(this, "CurveThickness");
                }
            }
        }

        public string CurveGeometry
        {
            get { return m_curveGeometry; }
            protected set
            {
                if (value != m_curveGeometry)
                {
                    m_curveGeometry = value;
                    PropertyChanged.Raise(this, "CurveGeometry");
                }
            }
        }

        public Curve Curve { get; private set; }

        private ICommand _mouseDown;

        public ICommand MouseDown
        {
            get { return _mouseDown ?? (_mouseDown = new RelayCommand(() => Console.WriteLine("down"))); }
        }

        private ICommand _mouseMove;

        public ICommand MouseMove
        {
            get { return _mouseMove ?? (_mouseMove = new RelayCommand(() => Console.WriteLine("moving"))); }
        }

        private ICommand _mouseUp;
        private string _dashArray;

        public ICommand MouseUp
        {
            get
            {
                return _mouseUp ?? (_mouseUp = new RelayCommand
                    (() => Console.WriteLine("up")));
            }
        }

        public void Dispose()
        {
            Curve.ControlPoints.ListChanged -= OnListChanged;
            Parent.XAxis.LengthChanged -= OnAxisChangedDispatcher;
            Parent.YAxis.LengthChanged -= OnAxisChangedDispatcher;
        }

        #endregion ICurveEvaluator Members

        protected abstract void Evaluate();

        protected virtual void OnListChanged(object sender, ListChangedEventArgs e)
        {
            Evaluate();
        }

        public bool mouseMoving { get; set; }

        public string DashArray
        {
            get { return _dashArray; }
            set
            {
                _dashArray = value;
                this.PropertyChanged.Raise(this, "DashArray");
            }
        }
    }
}