using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using GalaSoft.MvvmLight.Command;
using rage.ToolLib;
using Rave.CurveEditor2.Axes;
using Rave.CurveEditor2.CurveEvaluators;
using Rave.CurveEditor2.Curves;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor2
{
	public class CurveEditorViewModel : INotifyPropertyChanged,
											 IDisposable
	{
		private bool m_isCheckedOut;
		private CurveEditorModel m_model;
		private CurveEvaluator m_selectedCurve;

		public CurveEditorViewModel(CurveEditorModel model)
		{
			if (model == null)
			{
				throw new ArgumentNullException("model");
			}

		}

		#region ViewModel Members

		public CurveEvaluator SelectedCurve
		{
			get { return m_selectedCurve; }
			set
			{
				if (m_selectedCurve != value)
				{
					m_selectedCurve = value;
					CurveSelected.Raise(value);
					PropertyChanged.Raise(this, "SelectedCurve");
				}
			}
		}

		public BindingList<CurveEvaluator> CurveEvaluators
		{
			get
			{
			    if (m_model != null) return m_model.CurveEvaluators;
                return null;
			}
		}

		public AxisViewModel XAxis { get; private set; }

		public AxisViewModel YAxis { get; private set; }

		public bool IsCheckedOut
		{
			get { return m_isCheckedOut; }
			set
			{
				if (m_isCheckedOut != value)
				{
					m_isCheckedOut = value;
					PropertyChanged.Raise(this, "IsCheckedOut");
				}
			}
		}

		public bool CanAddCurve
		{
			get { return m_model.CanAddCurve; }
		}

		private ICommand _zoomIn;

		public ICommand ZoomIn
		{
			get
			{
				if (_zoomIn == null)
				{
					_zoomIn = new RelayCommand
						(
						() =>
						{
							this.XAxis.ZoomCentral(1.1);
							this.YAxis.ZoomCentral(1.1);
							_zoomOut.RaiseCanExecuteChanged();
						}
						);
				}
				return _zoomIn;
			}
		}

		private RelayCommand _zoomOut;

		public RelayCommand ZoomOut
		{
			get
			{
				if (_zoomOut == null)
				{
					_zoomOut = new RelayCommand
						(
						() =>
						{
							this.XAxis.ZoomCentral(1 / 1.1);
							this.YAxis.ZoomCentral(1 / 1.1);
							YAxis.Displacement = Math.Abs(YAxis.Displacement) < 0.1 ? 0 : YAxis.Displacement / 1.1;
							XAxis.Displacement = Math.Abs(XAxis.Displacement) < 0.1 ? 0 : XAxis.Displacement / 1.1;
						},
						CanZoomOut);
				}
				return _zoomOut;
			}
		}

		private bool CanZoomOut()
		{
		    if (XAxis == null || YAxis == null)
		    {
		        return false;
		    }
			return (XAxis.Zoom > 1 && YAxis.Zoom > 1);
		}

		private ICommand _resetZoom;
		private bool _allowContextMenu = true;
		private bool _showZoomControls;

		public CurveEditorViewModel(int? maxCurves, int? maxControlPoints, AxisModel xaxisModel, AxisModel yaxisModel,
			Func<Point?, Point?, Point, bool> canMoveControlPoint)
			: this(maxCurves, maxControlPoints, xaxisModel, yaxisModel, canMoveControlPoint, true, true)
		{
		}

		public CurveEditorViewModel(int? maxCurves, int? maxControlPoints, AxisModel xaxisModel, AxisModel yaxisModel,
			Func<Point?, Point?, Point, bool> canMoveControlPoint, bool xAxisVisibility, bool allowContextMenu)
			: this(maxCurves, maxControlPoints, xaxisModel, yaxisModel, canMoveControlPoint, true, true, true)
		{
			
		}
			

		public CurveEditorViewModel(int? maxCurves, int? maxControlPoints, AxisModel xaxisModel, AxisModel yaxisModel, Func<Point?, Point?, Point, bool> canMoveControlPoint, bool xAxisVisibility, bool allowContextMenu, bool showZoomControls, double initialDisplacement = -0.05)
		{
			m_model = new CurveEditorModel(maxCurves, maxControlPoints, xaxisModel, yaxisModel, canMoveControlPoint);
			m_model.CurveAdded += OnCurveAdded;
			m_model.CurveRemoved += OnCurveRemoved;
			m_model.CurveEdited += OnCurveEdited;
			m_model.CanAddCurveChanged += OnCanAddCurveChanged;

			XAxis = new AxisViewModel(m_model.XAxis, xAxisVisibility, initialDisplacement);
			YAxis = new AxisViewModel(m_model.YAxis, true, initialDisplacement);
			AllowContextMenu = allowContextMenu;
			ShowZoomControls = showZoomControls;
		}


		public ICommand ResetZoom
		{
			get
			{
				if (_resetZoom == null)
				{
					_resetZoom = new RelayCommand
						(
						() =>
						{
							this.XAxis.ResetZoom();
							this.YAxis.ResetZoom();
							ZoomOut.RaiseCanExecuteChanged();
						}
						);
				}
				return _resetZoom;
			}
		}

		public bool AllowContextMenu
		{
			get { return _allowContextMenu; }
			set { _allowContextMenu = value; }
		}

		public bool ShowZoomControls
		{
			get { return _showZoomControls; }
			set { _showZoomControls = value; }
		}

		public event Action<CurveEvaluator> CurveAdded;

		public event Action<CurveEvaluator> CurveRemoved;

		public event Action<CurveEvaluator> CurveSelected;

		public event Action CurveEdited;

		public Curve AddCurve(CurveTypes type)
		{
			return m_model.AddCurve(type);
		}

		public Curve AddCurve(CurveTypes type, IEnumerable<Point> points)
		{
			return m_model.AddCurve(type, points);
		}

		public Curve AddCurve(CurveTypes type, IEnumerable<Point3D> points, Tuple<ExtendedLineMode, ExtendedLineMode> extendedLines = null, string color = null)
		{
			return m_model.AddCurve(type, points, extendedLines, color);
		}

		public Curve AddCurve(CurveTypes type, IEnumerable<Point> points, IEnumerable<Point> bezierpoints, Tuple<ExtendedLineMode, ExtendedLineMode> extendedLines = null, string color = null)
		{
			return m_model.AddCurve(type, points, bezierpoints, extendedLines, color);
		}

		public Curve AddHorizontalLine(CurveTypes type, double y, double x1, double x2, string color = null)
		{
			return m_model.AddHorizontalLine(type, y, x1, x2, color);
		}

		public void AddPointToCurve(Point point, CurveEvaluator curveEvaluator)
		{
			m_model.AddPointToCurve(point, curveEvaluator.Curve);
		}

		public void UpdateCurves()
		{
			if (CurveEvaluators.Count > 0)
			{
				CurveEvaluators.ResetBindings();
				foreach (var curveEvaluator in CurveEvaluators)
				{
					curveEvaluator.Curve.ControlPoints.ResetBindings();
				}
			}
		}

		public void RemoveCurve(Curve curve)
		{
			m_model.RemoveCurve(curve);
		}

		public void RemoveCurves()
		{
			m_model.RemoveCurves();
		}

		public void Dispose()
		{
			m_model.CanAddCurveChanged -= OnCanAddCurveChanged;
			m_model.CurveAdded -= OnCurveAdded;
			m_model.CurveRemoved -= OnCurveRemoved;
			m_model.CurveEdited -= OnCurveEdited;
			m_model.Dispose();
			m_model = null;

			XAxis.Dispose();
			XAxis = null;
			YAxis.Dispose();
			YAxis = null;

			m_selectedCurve = null;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion ViewModel Members

		private void OnCanAddCurveChanged()
		{
			PropertyChanged.Raise(this, "CanAddCurve");
		}

		private void OnCurveAdded(CurveEvaluator curveEvaluator)
		{
			CurveAdded.Raise(curveEvaluator);
		}

		private void OnCurveEdited()
		{
			CurveEdited.Raise();
		}

		private void OnCurveRemoved(CurveEvaluator curveEvaluator)
		{
			CurveRemoved.Raise(curveEvaluator);
		}
	}
}