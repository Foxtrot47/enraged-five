using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media.Media3D;
using rage.ToolLib;
using Rave.CurveEditor2.Axes;
using Rave.CurveEditor2.CurveEvaluators;
using Rave.CurveEditor2.Curves;
using Rave.CurveEditor2.Helpers;

namespace Rave.CurveEditor2
{
    public class CurveEditorModel : IDisposable
    {
        private readonly Func<Point?, Point?, Point, bool> m_controlPointMovementFunc;
        private readonly int? m_maxControlPoints;
        private readonly int m_maxCurves;

        public CurveEditorModel(int? maxCurves,
                                int? maxControlPoints,
                                AxisModel xAxisModel,
                                AxisModel yAxisModel,
                                Func<Point?, Point?, Point, bool> controlPointMovementFunc)
        {
            if (maxCurves == null)
            {
                maxCurves = 0;
            }
            if (xAxisModel == null)
            {
                throw new ArgumentNullException("xAxisModel");
            }
            if (yAxisModel == null)
            {
                throw new ArgumentNullException("yAxisModel");
            }
            XAxis = xAxisModel;
            XAxis.MinMaxChanged += OnMinMaxChanged;
            YAxis = yAxisModel;
            YAxis.MinMaxChanged += OnMinMaxChanged;
            m_controlPointMovementFunc = controlPointMovementFunc;
            CurveColors = new CurveColors();

            m_maxCurves = maxCurves.Value;
            if (m_maxCurves <= 0)
            {
                m_maxCurves = CurveColors.MaxCurves;
            }

            m_maxControlPoints = maxControlPoints;
            if (m_maxControlPoints.HasValue &&
                m_maxControlPoints.Value < 2)
            {
                m_maxControlPoints = null;
            }

            CurveEvaluators = new BindingList<CurveEvaluator>();
            Curves = new List<Curve>();
        }

        private List<Curve> Curves { get; set; }

        private CurveColors CurveColors { get; set; }

        #region CurveEditorModel Members

        public AxisModel XAxis { get; private set; }

        public AxisModel YAxis { get; private set; }

        public BindingList<CurveEvaluator> CurveEvaluators { get; private set; }

        public bool CanAddCurve
        {
            get { return Curves.Count < m_maxCurves; }
        }

        public event Action CanAddCurveChanged;

        public event Action<CurveEvaluator> CurveAdded;

        public event Action<CurveEvaluator> CurveRemoved;

        public event Action CurveEdited;

        public Curve AddCurve(CurveTypes type)
        {
            if (CanAddCurve)
            {
                switch (type)
                {
                    case CurveTypes.PiecewiseLinear:
                        {
                            var curve =
                                AddCurve(new PiecewiseLinearCurveEvaluator(this,
                                                                           CurveColors.AcquireColor(),
                                                                           m_maxControlPoints));
                            PiecewiseLinearCurveEvaluator.PopulateCurve(curve, CanChangeValue, XAxis, YAxis);
                            return curve;
                        }
                    case CurveTypes.PiecewiseExponential:
                        {
                            var curve =
                                AddCurve(new PiecewiseExponentialCurveEvaluator(this,
                                                                           CurveColors.AcquireColor(),
                                                                           m_maxControlPoints));
                            PiecewiseLinearCurveEvaluator.PopulateCurve(curve, CanChangeValue, XAxis, YAxis);
                            return curve;
                        }
                    case CurveTypes.Bezier:
                        {
                            var curve =
                                AddCurve(new BezierCurveEvaluator(this,
                                                                           CurveColors.AcquireColor(),
                                                                           m_maxControlPoints));
                            BezierCurveEvaluator.PopulateCurve(curve, CanChangeValue, XAxis, YAxis);
                            return curve;
                        }
                }
            }
            return null;
        }

        public Curve AddCurve(CurveTypes type, IEnumerable<Point> points)
        {
            if (CanAddCurve)
            {
                Curve curve = null;
                switch (type)
                {
                    case CurveTypes.PiecewiseLinear:
                        {
                            curve =
                                AddCurve(new PiecewiseLinearCurveEvaluator(this,
                                                                           CurveColors.AcquireColor(),
                                                                           m_maxControlPoints));
                            break;
                        }
                }

                if (curve != null)
                {
                    foreach (var point in points)
                    {
                        curve.Add(new ControlPoint(curve, CanChangeValue) { X = point.X, Y = point.Y });
                    }
                    return curve;
                }
            }
            return null;
        }

        public Curve AddCurve(CurveTypes type, IEnumerable<Point3D> points, Tuple<ExtendedLineMode, ExtendedLineMode> extendedLines = null, string color = null)
        {
            if (color == null)
            {
                color = CurveColors.AcquireColor();
            }

            if (CanAddCurve)
            {
                Curve curve = null;
                switch (type)
                {
                    case CurveTypes.PiecewiseExponential:
                        {
                            curve =
                                AddCurve(new PiecewiseExponentialCurveEvaluator(this,
                                                                           color,
                                                                           m_maxControlPoints, extendedLines));
                            break;
                        }
                }

                if (curve != null)
                {
                    foreach (var point in points)
                    {
                        curve.Add(new ControlPoint(curve, CanChangeValue) { X = point.X, Y = point.Y, Exponent = point.Z });
                    }
                    return curve;
                }
            }
            return null;
        }

        public Curve AddHorizontalLine(CurveTypes type, double y, double x1, double x2, string color = null)
        {
            if (color == null)
            {
                color = CurveColors.AcquireColor();
            }

            if (CanAddCurve)
            {
                Curve curve = null;
                switch (type)
                {
                    case CurveTypes.HorizontalLine:
                        {
                            curve =
                                AddCurve(new HorizontalLineEvaluator(this,
                                                                           color));
                            break;
                        }
                }

                if (curve != null)
                {

                    curve.Add(new ControlPoint(curve, CanChangeValue) { X = x1, Y = y });
                    curve.Add(new ControlPoint(curve, CanChangeValue) { X = x2, Y = y });
                    return curve;
                }
            }
            return null;
        }

        public Curve AddCurve(CurveTypes type, IEnumerable<Point> points, IEnumerable<Point> bezierPoints, Tuple<ExtendedLineMode, ExtendedLineMode> extendedLines = null, string color = null)
        {
            if (color == null)
            {
                color = CurveColors.AcquireColor();
            }

            if (CanAddCurve)
            {
                Curve curve = null;
                switch (type)
                {
                    case CurveTypes.Bezier:
                        {
                            curve =
                                AddCurve(new BezierCurveEvaluator(this,
                                                                           color,
                                                                           m_maxControlPoints, extendedLines));
                            break;
                        }
                }

                if (curve != null)
                {

                    foreach (var point in points)
                    {
                        curve.Add(new ControlPoint(curve, CanChangeValue) { X = point.X, Y = point.Y });
                    }
                    curve.AddBezierPoints(bezierPoints);

                    
                    BezierHelper.CalculateScaling(curve);
                    return curve;
                }
            }
            return null;
        }

        public void AddPointToCurve(Point point, Curve curve)
        {
            var controlPoint = new ControlPoint(curve, CanChangeValue) { XPos = point.X, YPos = point.Y };

            var insertionIndex = 0;
            while (insertionIndex < curve.ControlPoints.Count - 1 &&
                curve.ControlPoints[insertionIndex].X < controlPoint.X)
            {
                insertionIndex += 1;
            }

            curve.Insert(insertionIndex, controlPoint);
        }


        public void StraightenBezier(Point point, Curve curve)
        {
            var controlPoint = new ControlPoint(curve, CanChangeValue) { XPos = point.X, YPos = point.Y };
            var indexToStraighten = 0;
            while (indexToStraighten < curve.ControlPoints.Count - 1 &&
                curve.ControlPoints[indexToStraighten].X < controlPoint.X)
            {
                indexToStraighten += 1;
            }
            indexToStraighten--;
            if (indexToStraighten < curve.BezierPoints.Count)
            {
                curve.BezierPoints[indexToStraighten] = new Point
                    (
                    ((curve.ControlPoints[indexToStraighten + 1].X - curve.ControlPoints[indexToStraighten].X) / 2) +
                    curve.ControlPoints[indexToStraighten].X,
                    ((curve.ControlPoints[indexToStraighten + 1].Y - curve.ControlPoints[indexToStraighten].Y) / 2) +
                    curve.ControlPoints[indexToStraighten].Y
                    );
                BezierHelper.CalculateScaling(curve);
            }
        }

        public void RemoveCurve(Curve curve)
        {
            var index = Curves.IndexOf(curve);
            var evaluator = curve.Parent;
            CurveRemoved.Raise(evaluator);

            curve.Dirty -= OnDirty;
            Curves.RemoveAt(index);
            CurveColors.ReleaseColor(curve.Color);

            CurveEvaluators.RemoveAt(index);
            evaluator.Dispose();

            CanAddCurveChanged.Raise();
        }

        public void RemoveCurves()
        {
            var evaluators = CurveEvaluators.ToList();
            foreach (var evaluator in evaluators)
            {
                RemoveCurve(evaluator.Curve);
            }
        }


        public void Dispose()
        {
            RemoveCurves();
            XAxis.MinMaxChanged -= OnMinMaxChanged;
            YAxis.MinMaxChanged -= OnMinMaxChanged;
        }

        #endregion CurveEditorModel Members

        private void OnMinMaxChanged()
        {
            foreach (var curve in Curves)
            {
                curve.Refresh();
            }
        }

        private Curve AddCurve(CurveEvaluator curveEvaluator)
        {
            CurveEvaluators.Add(curveEvaluator);
            Curves.Add(curveEvaluator.Curve);
            curveEvaluator.Curve.Dirty += OnDirty;
            CurveAdded.Raise(curveEvaluator);
            CanAddCurveChanged.Raise();
            return curveEvaluator.Curve;
        }

        private void OnDirty()
        {
            CurveEdited.Raise();
        }

        public bool CanChangeValue(ControlPoint controlPoint, double x, double y)
        {
            var canChangeValue = true;
            if (m_controlPointMovementFunc != null)
            {
                var controlPoints = controlPoint.Parent.ControlPoints;
                var controlPointIndex = controlPoints.IndexOf(controlPoint);
                if (controlPointIndex >= 0)
                {
                    var prevControlPointIndex = controlPointIndex - 1;
                    Point? prevControlPoint = null;
                    if (prevControlPointIndex >= 0 &&
                        controlPointIndex < controlPoints.Count)
                    {
                        prevControlPoint = new Point(controlPoints[prevControlPointIndex].X,
                                                     controlPoints[prevControlPointIndex].Y);
                    }

                    var nextControlPointIndex = controlPointIndex + 1;
                    Point? nextControlPoint = null;
                    if (nextControlPointIndex >= 0 &&
                        nextControlPointIndex < controlPoints.Count)
                    {
                        nextControlPoint = new Point(controlPoints[nextControlPointIndex].X,
                                                     controlPoints[nextControlPointIndex].Y);
                    }

                    canChangeValue = m_controlPointMovementFunc(prevControlPoint, nextControlPoint, new Point(x, y));
                }
            }
            return canChangeValue;
        }
    }
}