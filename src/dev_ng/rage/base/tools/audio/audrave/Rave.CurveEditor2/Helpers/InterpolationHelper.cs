using rage.ToolLib.Extensions;

namespace Rave.CurveEditor2.Helpers
{
    public static class InterpolationHelper
    {
        public static float Lerp(this float val, float min, float max)
        {
            return (val * (max - min)) + min;
        }

        public static float GetLinearRatio(this float val, float min, float max)
        {
            float x = ((val - min) / (max - min));
            return x.Clamp(0, 1);
        }

        public static double Lerp(this double val, double min, double max)
        {
            return (val * (max - min)) + min;
        }

        public static double GetLinearRatio(this double convertedValue, double min, double max)
        {
            double originalValue = (convertedValue - min) / (max - min);
            return originalValue;
        }
    }
}
