﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor2.Curves
{
	/// <summary>
	///   Interaction logic for EditControlPoint.xaml
	/// </summary>
	public partial class EditControlPoint
	{
		private readonly Brush m_errorForegroundBrush;
		private readonly Brush m_errorBackgroundBrush;
		private readonly Brush m_xValueForegroundBrush;
		private readonly Brush m_xValueBackgroundBrush;
		private readonly Brush m_yValueForegroundBrush;
		private readonly Brush m_yValueBackgroundBrush;

		public EditControlPoint()
		{
			InitializeComponent();

			m_xValueForegroundBrush = xValue.Foreground;
			m_xValueBackgroundBrush = xValue.Background;
			m_yValueForegroundBrush = yValue.Foreground;
			m_yValueBackgroundBrush = yValue.Background;

			m_errorForegroundBrush = Brushes.Wheat;
			m_errorBackgroundBrush = Brushes.Tomato;
		}

		private void OnOkClick(object sender, RoutedEventArgs e)
		{
			var controlPoint = this.FindDataContext<ControlPoint>();
			var xAxis = controlPoint.Parent.Parent.Parent.XAxis;
			var yAxis = controlPoint.Parent.Parent.Parent.YAxis;

			double xVal, yVal;
			if (double.TryParse(xValue.Text, out xVal) &&
				double.TryParse(yValue.Text, out yVal))
			{
				if ((xVal >= xAxis.Min && xVal <= xAxis.Max) &&
					(yVal >= yAxis.Min && yVal <= yAxis.Max))
				{
					controlPoint.XDisplay = xVal;
					controlPoint.YDisplay = yVal;
					controlPoint.IsEditing = false;
				}
				else
				{
					MessageBox.Show("Coordinate value is outside permitted range",
									"Error",
									MessageBoxButton.OK,
									MessageBoxImage.Error);
				}
			}
			else
			{
				MessageBox.Show("Invalid coordinate value", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		private void OnCancelClick(object sender, RoutedEventArgs e)
		{
			var controlPoint = this.FindDataContext<ControlPoint>();
			if (controlPoint != null)
			{
				controlPoint.IsEditing = false;
			}
		}

		private void OnXValueTextChanged(object sender, TextChangedEventArgs e)
		{
			var controlPoint = this.FindDataContext<ControlPoint>();
			var xAxis = controlPoint.Parent.Parent.Parent.XAxis;

			double temp;
			if (string.IsNullOrEmpty(xValue.Text) ||
				(double.TryParse(xValue.Text, out temp) && (temp >= xAxis.Min && temp <= xAxis.Max)))
			{
				xValue.Foreground = m_xValueForegroundBrush;
				xValue.Background = m_xValueBackgroundBrush;
			}
			else
			{
				xValue.Foreground = m_errorForegroundBrush;
				xValue.Background = m_errorBackgroundBrush;
			}
		}

		private void OnYValueTextChanged(object sender, TextChangedEventArgs e)
		{
			var controlPoint = this.FindDataContext<ControlPoint>();
			var yAxis = controlPoint.Parent.Parent.Parent.YAxis;

			double temp;
			if (string.IsNullOrEmpty(yValue.Text) ||
				(double.TryParse(yValue.Text, out temp) && (temp >= yAxis.Min && temp <= yAxis.Max)))
			{
				yValue.Foreground = m_yValueForegroundBrush;
				yValue.Background = m_yValueBackgroundBrush;
			}
			else
			{
				yValue.Foreground = m_errorForegroundBrush;
				yValue.Background = m_errorBackgroundBrush;
			}
		}

		private void OnUserControlLoaded(object sender, RoutedEventArgs e)
		{
			var controlPoint = this.FindDataContext<ControlPoint>();
			xValue.Text = Math.Round(controlPoint.XDisplay, 5).ToString();
			yValue.Text = Math.Round(controlPoint.YDisplay, 5).ToString();
		}
	}
}