﻿using System;
using rage.ToolLib.Extensions;

namespace Rave.CurveEditor2
{
    public class ParameterDestinationHelper
    {
        public static float SerializeOutputRangeValue(float val, ParameterDestinations destination)
        {
            switch (destination)
            {
                case ParameterDestinations.Volume:
                    {
                        return (float)Math.Pow(10, val / 20);
                    }
                case ParameterDestinations.Pitch:
                    {
                        return (float)Math.Pow(Math.Pow(2, val / 12) / 4, 0.5);
                    }
                case ParameterDestinations.Pan:
                    {
                        return val / 360;
                    }
                case ParameterDestinations.Lpf:
                case ParameterDestinations.Hpf:
                    {
                        return (float)(0.125306 * Math.Log(val.Clamp(8.1758f,23900f)) - 0.26329);
                    }
                default:
                    {
                        return val;
                    }
            }
        }

        public static float DeserializeOutputRangeValue(float val, ParameterDestinations destination)
        {
            switch (destination)
            {
                case ParameterDestinations.Volume:
                {
                    return ((float) (Math.Log10(val)*20)).Clamp(-100, 0);
                }
                case ParameterDestinations.Pitch:
                    {
                        return ((float)(Math.Log((Math.Pow(val, 2)) * 4, 2) * 12)).Clamp(-24,24);
                    }
                case ParameterDestinations.Pan:
                    {
                        return val * 360;
                    }
                case ParameterDestinations.Lpf:
                case ParameterDestinations.Hpf:
                    {
                        float freqInOctaves = (1.0f - val) * -11.51336667f;
                        return (float)(Math.Pow(2.0f, freqInOctaves) * 23900);
                    }
                default:
                    {
                        return val;
                    }
            }
        }
    }
}
