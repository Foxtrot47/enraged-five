using System;
using System.ComponentModel;
using rage.ToolLib;
using rage.ToolLib.Extensions;
using Rave.CurveEditor2.Axes;
using Rave.CurveEditor2.Helpers;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor2.Curves
{
    public class ControlPoint : INotifyPropertyChanged
    {
        private const int SIZE = 8;
        private const int HALF_SIZE = SIZE / 2;
        internal Func<ControlPoint, double, double, bool> _canChangeValueFunc;
        private string m_coordinates;
        private bool m_isEditing;
        internal double m_x;
        internal double m_y;
        private double m_exponent = 1;

        public bool IsBezier { get; set; }
        public ControlPoint(Curve parent, Func<ControlPoint, double, double, bool> canChangeValueFunc)
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }
            if (canChangeValueFunc == null)
            {
                throw new ArgumentNullException("canChangeValueFunc");
            }
            Parent = parent;
            _canChangeValueFunc = canChangeValueFunc;
            Parent.Parent.Parent.XAxis.MinMaxChanged += () =>
            {
                PropertyChanged.Raise(this, "XDisplay");
                PropertyChanged.Raise(this, "XPos");
                PropertyChanged.Raise(this, "YDisplay");
                PropertyChanged.Raise(this, "YPos");
            };
            Parent.Parent.Parent.XAxis.LengthChanged += () =>
            {
                PropertyChanged.Raise(this, "XDisplay");
                PropertyChanged.Raise(this, "XPos");
                PropertyChanged.Raise(this, "YDisplay");
                PropertyChanged.Raise(this, "YPos");
            };
            Parent.Parent.Parent.YAxis.MinMaxChanged += () =>
            {
                PropertyChanged.Raise(this, "YDisplay");
                PropertyChanged.Raise(this, "YPos");
                PropertyChanged.Raise(this, "XDisplay");
                PropertyChanged.Raise(this, "XPos");
            };
            Parent.Parent.Parent.YAxis.LengthChanged += () =>
            {
                PropertyChanged.Raise(this, "YDisplay");
                PropertyChanged.Raise(this, "YPos");
                PropertyChanged.Raise(this, "XDisplay");
                PropertyChanged.Raise(this, "XPos");
            };
        }

        public Curve Parent { get; private set; }

        public double X
        {
            get { return m_x; }
            set
            {
                if (value != m_x)
                {
                    var val = value.Clamp(0, 1);
                    if (_canChangeValueFunc(this, val, m_y))
                    {
                        m_x = val;
                        PropertyChanged.Raise(this, "X");
                        PropertyChanged.Raise(this, "XDisplay");
                        PropertyChanged.Raise(this, "XPos");
                        Dirty.Raise();
                    }
                }

                RefreshTooltip();
            }
        }

        public double XDisplay
        {
            get
            {
                AxisModel xAxis = Parent.Parent.Parent.XAxis;
                return GetDisplayFromX(X, xAxis);
            }
            set
            {
                var xAxis = Parent.Parent.Parent.XAxis;
                X = (value - xAxis.Min) / (xAxis.Max - xAxis.Min);
            }
        }

        public static double GetDisplayFromX(double x, AxisModel xAxis)
        {
            return xAxis.Min + (x * (xAxis.Max - xAxis.Min));
        }

        public double XPos
        {
            get
            {
                return GetScreenFromValue(X, Parent.Parent.Parent.XAxis);
            }
            set
            {
                X = GetValueFromScreen(value, Parent.Parent.Parent.XAxis);
            }
        }




        public double Exponent
        {
            get { return m_exponent; }
            set
            {
                m_exponent = value;
                PropertyChanged.Raise(this, "Exponent");
            }
        }

        public double Y
        {
            get { return m_y; }
            set
            {
                if (value != m_y)
                {
                    var val = value;
                    if (!IsBezier)
                    {
                        val = value.Clamp(0, 1);
                    }
                    if (_canChangeValueFunc(this, m_x, val))
                    {
                        m_y = val;
                        PropertyChanged.Raise(this, "Y");
                        PropertyChanged.Raise(this, "YDisplay");
                        PropertyChanged.Raise(this, "YPos");
                        Dirty.Raise();
                    }
                }
                RefreshTooltip();
            }
        }

        public double YDisplay
        {
            get
            {
                var yAxis = Parent.Parent.Parent.YAxis;
                var destination = yAxis.Destination ?? ParameterDestinations.Variable;
                var min = ParameterDestinationHelper.SerializeOutputRangeValue((float)yAxis.Min, destination);
                var max = ParameterDestinationHelper.SerializeOutputRangeValue((float)yAxis.Max, destination);
                var result = min + (Y * (max - min));
                return ParameterDestinationHelper.DeserializeOutputRangeValue((float)result, destination);
            }
            set
            {
                var yAxis = Parent.Parent.Parent.YAxis;
                var destination = yAxis.Destination ?? ParameterDestinations.Variable;
                var min = ParameterDestinationHelper.SerializeOutputRangeValue((float)yAxis.Min, destination);
                var max = ParameterDestinationHelper.SerializeOutputRangeValue((float)yAxis.Max, destination);
                var result = ParameterDestinationHelper.SerializeOutputRangeValue((float)value, destination);
                Y = (result - min) / (max - min);
            }
        }

        public double YPos
        {
            get
            {
                return GetScreenFromValue(Y, Parent.Parent.Parent.YAxis);
            }
            set
            {
                Y = GetValueFromScreen(value, Parent.Parent.Parent.YAxis);
            }
        }


        public static double GetValueFromScreen(double value, AxisModel axisModel)
        {
            return (value / axisModel.Scale) + axisModel.Displacement;
        }

        private double GetScreenFromValue(double value, AxisModel axisModel)
        {
          return (value - axisModel.Displacement) * axisModel.Scale;
        }


        public string Coordinates
        {
            get { return m_coordinates; }
            private set
            {
                m_coordinates = value;
                PropertyChanged.Raise(this, "Coordinates");
            }
        }

        public bool IsEditing
        {
            get { return m_isEditing; }
            set
            {
                m_isEditing = value;
                PropertyChanged.Raise(this, "IsEditing");
            }
        }

        public static double Size
        {
            get { return SIZE; }
        }

        public static double NegHalfSize
        {
            get { return -HALF_SIZE; }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        public event Action Dirty;

        public void RefreshTooltip()
        {
            Coordinates = string.Format("{0},{1}", Math.Round(XDisplay, 5), Math.Round(YDisplay, 5));
        }
    }
}