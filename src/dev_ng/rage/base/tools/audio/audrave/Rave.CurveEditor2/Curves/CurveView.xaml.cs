﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using Rave.CurveEditor2.CurveEvaluators;

namespace Rave.CurveEditor2.Curves
{
	/// <summary>
	///   Interaction logic for CurveView.xaml
	/// </summary>
	public partial class CurveView
	{
		public CurveView()
		{
			InitializeComponent();
		}

		public Point? InsertionPoint { get; set; }

		private void OnAddControlPointClick(object sender, RoutedEventArgs e)
		{
			var evaluator = DataContext as CurveEvaluator;
			if (evaluator != null &&
				evaluator.IsSelected)
			{
				CurveEditorModel model = evaluator.Parent;
				if (InsertionPoint.HasValue)
				{
					model.AddPointToCurve(InsertionPoint.Value, evaluator.Curve);
				}
			}
			e.Handled = true;
		}

		private void StraghtenLine(object sender, RoutedEventArgs e)
		{
			var evaluator = DataContext as CurveEvaluator;
			if (evaluator != null &&
				evaluator.IsSelected)
			{
				CurveEditorModel model = evaluator.Parent;
				if (InsertionPoint.HasValue)
				{
					model.StraightenBezier(InsertionPoint.Value, evaluator.Curve);
                }
            }
			e.Handled = true;
		}
	}
}