﻿// -----------------------------------------------------------------------
// <copyright file="ExtendedLine.cs" company="Rockstar North">
//
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using rage.ToolLib;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor2.Curves
{
	using System.Windows;

	/// <summary>
	/// Extended line.
	/// </summary>
	public class ExtendedLine : INotifyPropertyChanged
	{
		private Point _lineStart;
		private Point _lineEnd;


		public ExtendedLine(Curve parent, Point lineStart, Point lineEnd)
		{
			this.Parent = parent;
			this.LineStart = lineStart;
			this.LineEnd = lineEnd;
		}

		/// <summary>
		/// Gets the parent.
		/// </summary>
		public Curve Parent { get; private set; }

		/// <summary>
		/// Gets or sets the line start.
		/// </summary>
		public Point LineStart
		{
			get { return _lineStart; }
			set
			{
				_lineStart = value;
				PropertyChanged.Raise(this, "LineStart");
			}
		}

		/// <summary>
		/// Gets or sets the line end.
		/// </summary>
		public Point LineEnd
		{
			get { return _lineEnd; }
			set
			{
				_lineEnd = value;
				PropertyChanged.Raise(this, "LineEnd");
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}