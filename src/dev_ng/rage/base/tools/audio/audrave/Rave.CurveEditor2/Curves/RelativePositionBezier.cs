﻿using System;

namespace Rave.CurveEditor2.Curves
{
    public class RelativePositionBezier
    {
        public Tuple<float, float, float> MagAndAngle { get; set; }
        public double LerpValue { get; set; }
    }
}