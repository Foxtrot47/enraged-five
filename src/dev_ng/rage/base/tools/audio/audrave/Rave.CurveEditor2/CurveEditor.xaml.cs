﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Media3D;
using Rave.CurveEditor2.Axes;
using Rave.CurveEditor2.Curves;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor2
{
    /// <summary>
    /// Interaction logic for CurveEditor.xaml
    /// </summary>
    public partial class CurveEditor : IDisposable, INotifyPropertyChanged
    {
        private CurveEditorViewModel _viewModel;


        public CurveEditor()
        {
            InitializeComponent();
            DataContext = this;
            if (Border.ActualHeight > 200 && Border.ActualWidth > 200)
            {
                Init(new Size(Border.ActualWidth - 50, Border.ActualHeight - 50));
            }
            else
            {
                Init(new Size(300, 300));
            }
            Border.SizeChanged += OnSizeChanged;

        }

        public CurveEditor(int maxCurves, int maxControlPoints, float minX, float minY, float maxX, float maxY, bool showXAxis, bool zoomEnabled)
        {
            InitializeComponent();
            DataContext = this;
            if (Border.ActualHeight > 200 && Border.ActualWidth > 200)
            {
                Init(new Size(Border.ActualWidth - 50, Border.ActualHeight - 50), maxCurves, maxControlPoints, minX, minY, maxX, maxY, showXAxis, zoomEnabled);
            }
            else
            {
                Init(new Size(300, 300), maxCurves, maxControlPoints, minX, minY, maxX, maxY, showXAxis, zoomEnabled);
            }
            Border.SizeChanged += OnSizeChanged;

        }

        public void Init(Size size, int maxCurves, int maxControlPoints, float minX, float minY, float maxX, float maxY, bool showXAxis, bool zoomEnabled, double initialDisplacement = -0.05)
        {
            if (_viewModel != null)
            {
                _viewModel.Dispose();
            }
            _viewModel = new CurveEditorViewModel(maxCurves, maxControlPoints,
                new AxisModel("X", minX, maxX, 1, size.Width - 20),
                new AxisModel("Y", minY, maxY, 1, size.Height - 20),
                CanMoveControlPoint, showXAxis, false, zoomEnabled, initialDisplacement);
            CurveEditorViewInstance = new CurveEditorView(_viewModel);
        }

        public void Init(Size size)
        {
            Init(size, 100, 400, 0, 0, 1, 1, false, false, 0.0);
        }

        private CurveEditorView _curveEditorView;

        public CurveEditorView CurveEditorViewInstance
        {
            get
            {
                return _curveEditorView;
            }
            set
            {
                _curveEditorView = value;
                PropertyChanged.Raise(this, "CurveEditorViewInstance");
            }
        }

        public CurveEditorViewModel ViewModel
        {
            get { return _viewModel; }
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs sizeChangedEventArgs)
        {
            if (sizeChangedEventArgs.NewSize.Width > 10)
            {
                double newSize = Math.Min(900, Math.Max(sizeChangedEventArgs.NewSize.Width - 120, 150));
                _viewModel.XAxis.SetSize(newSize);
            }
            if (sizeChangedEventArgs.NewSize.Height > 10)
            {

                double newSize = Math.Min(700, Math.Max(sizeChangedEventArgs.NewSize.Height - 100, 150));
                _viewModel.YAxis.SetSize(newSize);
            }
        }


        public Curve AddCurve(IEnumerable<Point3D> points3D, Tuple<ExtendedLineMode, ExtendedLineMode> extendedLines, string color)
        {
            Curve curve = _viewModel.AddCurve(CurveTypes.PiecewiseExponential, points3D, extendedLines, color);
            PropertyChanged.Raise(this, "CurveEditorViewInstance");
            return curve;
        }

        public Curve AddCurve(IEnumerable<Point> points, IEnumerable<Point> bezierPoints, Tuple<ExtendedLineMode, ExtendedLineMode> extendedLines, string color)
        {
            Curve curve = _viewModel.AddCurve(CurveTypes.Bezier, points, bezierPoints, extendedLines, color);
            PropertyChanged.Raise(this, "CurveEditorViewInstance");
            return curve;
        }


        public Curve AddHorizontalLine(double y, double x1, double x2, string color)
        {
            Curve curve = _viewModel.AddHorizontalLine(CurveTypes.HorizontalLine, y, x1, x2, color);
            curve.Parent.DashArray = "1";
            PropertyChanged.Raise(this, "CurveEditorViewInstance");
            return curve;
        }

        public void SetDestination(ParameterDestinations destination)
        {
            ViewModel.YAxis.Destination = destination;
        }

        public void SetMax(double value)
        {
            ViewModel.YAxis.Max = value;
        }

        public void SetMin(double value)
        {
            ViewModel.YAxis.Min = value;
        }

        private static bool CanMoveControlPoint(Point? previousControlPoint,
                                              Point? nextControlPoint,
                                              Point currentControlPoint)
        {
            if (previousControlPoint != null)
            {
                if (currentControlPoint.X <=
                    previousControlPoint.Value.X)
                {
                    return false;
                }
            }

            if (nextControlPoint != null)
            {
                if (currentControlPoint.X >=
                    nextControlPoint.Value.X)
                {
                    return false;
                }
            }

            return true;
        }

        public void Dispose()
        {
            _viewModel.Dispose();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}