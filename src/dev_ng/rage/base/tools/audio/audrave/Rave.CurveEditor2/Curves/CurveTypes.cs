namespace Rave.CurveEditor2.Curves
{
	public enum CurveTypes
	{
		PiecewiseLinear,
		PiecewiseExponential,
		Bezier,
		HorizontalLine
	}
}