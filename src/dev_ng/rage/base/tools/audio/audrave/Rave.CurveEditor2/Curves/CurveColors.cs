using System;
using System.Collections.Generic;
using System.Linq;

namespace Rave.CurveEditor2.Curves
{
	public class CurveColors
	{
		private readonly Dictionary<string, bool> m_colors;

		public CurveColors()
		{
			m_colors = new Dictionary<string, bool>(16)
                           {
                               {"Orange", false},
                               {"LimeGreen", false},
                               {"Plum", false},
                               {"OrangeRed", false},
                               {"Olive", false},
                               {"BlueViolet", false},
                               {"Gray", false},
                               {"LightCoral", false},
                               {"CornflowerBlue", false},
                               {"Fuchsia", false},
                               {"Peru", false},
                               {"SeaGreen", false},
                               {"Goldenrod", false},
                               {"RoyalBlue", false},
                               {"DarkTurquoise", false},
                               {"LightSteelBlue", false},
                           };
		}

		public int MaxCurves
		{
			get { return m_colors.Count; }
		}

		public string AcquireColor()
		{
			try
			{
				var freeColour = (from color in m_colors where color.Value == false select color.Key).First();
				m_colors[freeColour] = true;
				return freeColour;
			}
			catch (Exception)
			{
				throw new ApplicationException(
					"Ran out of unique colours - please increase the number of unique colours");
			}
		}

		public void ReleaseColor(string color)
		{
			m_colors[color] = false;
		}
	}
}