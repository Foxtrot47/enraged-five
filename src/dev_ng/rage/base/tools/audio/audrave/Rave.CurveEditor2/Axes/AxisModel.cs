using System;
using System.ComponentModel;
using System.Dynamic;
using System.Windows;
using rage.ToolLib;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor2.Axes
{
	public class AxisModel : INotifyPropertyChanged
	{
		public const double MARK_PERCENT = 9;
		private readonly string m_defaultLabel;
		private double m_size;
		private string m_label;
		private double m_length;
		private double m_max;
		private double m_min;
		private double m_zoom = 1.00;

		public AxisModel(string label, double min, double max, double length, double size)
		{
			if (label == null)
			{
				throw new ArgumentNullException("label");
			}
			if (size <= 0)
			{
				throw new ArgumentException("size");
			}

			m_label = label;
			m_defaultLabel = label;
			m_min = min;
			m_max = max;
			m_length = length;
			m_size = size;
		}

		#region AxisModel Members

		public double Min
		{
			get { return m_min; }
			set
			{
				if (m_min != value)
				{
					m_min = value;
					MinMaxChanged.Raise();
				}
			}
		}

		public double Zoom
		{
			get { return m_zoom; }
			set
			{
				if (m_zoom != value)
				{
					m_zoom = value;
					this.PropertyChanged.Raise(this, "Zoom");
					LengthChanged.Raise();
				}
			}
		}

		public double Max
		{
			get { return m_max; }
			set
			{
				if (m_max != value)
				{
					m_max = value;
					MinMaxChanged.Raise();
				}
			}
		}

		public double Length
		{
			get { return m_length; }
			set
			{
				m_length = value;
				LengthChanged.Raise();
			}
		}

		public double ZoomedLength
		{
			get { return m_length / m_zoom; }
		}

		public double Displacement { get { return m_displacement; } set { m_displacement = value; MinMaxChanged.Raise(); } }

		public double MarkFrequency
		{
			get { return ZoomedLength / MARK_PERCENT; }
		}

		public double Scale
		{
			get { return m_size / ZoomedLength; }
		}

		public string Label
		{
			get { return m_label; }
			set
			{
				if (m_label != value)
				{
					m_label = string.IsNullOrEmpty(value) ? m_defaultLabel : value;
					LabelChanged.Raise();
				}
			}
		}

		public ParameterDestinations? Destination { get; set; }

		public event Action LengthChanged;

		public event Action LabelChanged;

		public event Action MinMaxChanged;

		public event PropertyChangedEventHandler PropertyChanged;

		private double m_displacement;

		public double Size
		{
			get
			{
				return m_size;
			}
			set
			{
				m_size = value;
				PropertyChanged.Raise(this, "Size");
			}
		}

		#endregion AxisModel Members
	}
}