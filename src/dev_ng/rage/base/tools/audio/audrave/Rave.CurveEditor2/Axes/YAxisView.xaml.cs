﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

using System.Windows.Shapes;
using Rave.CurveEditor2.Curves;
using Rave.CurveEditor2.Helpers;

namespace Rave.CurveEditor2.Axes
{
    /// <summary>
    ///   Interaction logic for YAxisView.xaml
    /// </summary>
    public partial class YAxisView
    {
        private AxisViewModel m_viewModel;

        public YAxisView()
        {
            InitializeComponent();
        }

        private void OnUserControlLoaded(object sender, RoutedEventArgs e)
        {
            m_viewModel = DataContext as AxisViewModel;
            if (m_viewModel != null)
            {
                m_viewModel.PropertyChanged += OnViewModelPropertyChanged;
                OnViewModelPropertyChanged(null, null);
            }
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            markings.Children.Clear();

            double initialDisplacement = (m_viewModel.Length * m_viewModel.InitialDisplacement);
            var current = m_viewModel.DrawingLength + initialDisplacement;
            var yAxis = m_viewModel;
            var destination = yAxis.Destination ?? ParameterDestinations.Variable;
            var min = ParameterDestinationHelper.SerializeOutputRangeValue((float)yAxis.Min, destination);
            var max = ParameterDestinationHelper.SerializeOutputRangeValue((float)yAxis.Max, destination);

            while (current - initialDisplacement  >= m_viewModel.MarkFrequency)
            {
                var result = (float)(min + (m_viewModel.GetValueFromScreen(Math.Max(m_viewModel.DrawingLength - current, 0.0)) * (max - min)));
                string value =

                    FormatNumber(ParameterDestinationHelper.DeserializeOutputRangeValue(result, destination));
                var textBlock = new TextBlock { Style = Resources["TextBlockStyle"] as Style, Text = value };

                Canvas.SetRight(textBlock, 7);
                Canvas.SetTop(textBlock, current);
                markings.Children.Add(textBlock);

                var line = new Line
                               {
                                   X1 = 0,
                                   Y1 = current,
                                   X2 = -5,
                                   Y2 = current,
                                   Style = Resources["AxisLineStyle"] as Style
                               };
                markings.Children.Add(line);

                current -= m_viewModel.MarkFrequency;


            }
        }

        private void OnUserControlUnloaded(object sender, RoutedEventArgs e)
        {
            if (m_viewModel != null)
            {
                m_viewModel.PropertyChanged -= OnViewModelPropertyChanged;
                m_viewModel = null;
            }
        }


        static string FormatNumber(float num)
        {
            if (float.IsInfinity(num) || float.IsNaN(num))
            {
                return "NaN";
            }
            if (Math.Abs(num) < 1)
            {
                return num.ToString("0.##");
            }
            else if (Math.Abs(num) < 10)
            {
                return num.ToString("#0.#");
            }
            else if (Math.Abs(num) >= 1000)
            {
                return FormatNumber(num / 1000) + "K";
            }

            return num.ToString("#0");
        }
    }
}