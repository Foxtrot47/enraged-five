namespace Rave.CurveEditor2
{
	public enum ParameterDestinations
	{
		Volume,
		Pitch,
		Pan,
		StartOffset,
		PreDelay,
		Lpf,
		Hpf,
		Variable,
	}
}