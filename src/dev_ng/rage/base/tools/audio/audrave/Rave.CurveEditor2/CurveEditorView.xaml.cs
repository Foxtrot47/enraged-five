﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using rage.ToolLib.Extensions;
using Rave.CurveEditor2.Axes;
using Rave.CurveEditor2.CurveEvaluators;
using Rave.CurveEditor2.Curves;
using Rave.CurveEditor2.Helpers;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor2
{
    /// <summary>
    ///   Interaction logic for CurveEditorViewInstance.xaml
    /// </summary>
    public partial class CurveEditorView
    {

        private ControlPointView m_draggedControlPoint;
        private ExtendedLineView _extendedLineDrag;
        private Point m_lastPosition;
        private ControlPoint _currentExponentMouseDown = null;
        private ControlPoint _nextExponentControlPoint = null;

        public ItemsControl Editor
        {
            get { return editor; }
        }

        public CurveEditorView(CurveEditorViewModel viewModel)
            : this()
        {

            if (viewModel == null)
            {
                throw new ArgumentNullException("viewModel");
            }
            ViewModel = viewModel;
            DataContext = ViewModel;
            if (!viewModel.AllowContextMenu)
            {
                editor.ContextMenu = null;
            }
        }

        public CurveEditorView()
        {
            InitializeComponent();
            DataContext = ViewModel;
        }

        private CurveEditorViewModel ViewModel { get; set; }

        public void SetDataContext()
        {
            DataContext = ViewModel;
        }

        private void OnUserControlLoaded(object sender, RoutedEventArgs e)
        {
            DataContext = ViewModel;
            e.Handled = true;
        }

        private void OnUserControlUnloaded(object sender, RoutedEventArgs e)
        {
            DataContext = null;
            e.Handled = true;
        }

        private void OnAddClicked(object sender, RoutedEventArgs e)
        {
            ViewModel.AddCurve(CurveTypes.Bezier);
            e.Handled = true;
        }

        private void OnRemoveClicked(object sender, RoutedEventArgs e)
        {
            var curveToRemove =
                (from curveEvaluator in ViewModel.CurveEvaluators
                 where curveEvaluator.IsSelected
                 select curveEvaluator.Curve).FirstOrDefault();
            if (curveToRemove != null)
            {
                ViewModel.RemoveCurve(curveToRemove);
            }
            e.Handled = true;
        }

        private void OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            m_lastPosition = editor.GetMousePosition();
            var originalSource = e.OriginalSource as DependencyObject;
            if (originalSource != null)
            {
                var curve = originalSource.FindAncestor<CurveView>();
                if (curve != null)
                {
                    CurveBezierEditor(curve);
                    HorizontalLineEdit(curve);
                }
            }
        }


        public void ChangeHorizontalLine(ControlPoint closestControlPoint, Point p)
        {
            double yinverse = closestControlPoint.Parent.Parent.Parent.YAxis.Size - p.Y;

            double y = ControlPoint.GetValueFromScreen(yinverse, closestControlPoint.Parent.Parent.Parent.YAxis) + closestControlPoint.Parent.Parent.Parent.YAxis.MarkFrequency;

            closestControlPoint.Parent.ControlPoints.First().Y = y;
            closestControlPoint.Parent.ControlPoints.Last().Y = y;
        }

        private void OnPreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton ==
                MouseButtonState.Pressed)
            {
                var originalSource = e.OriginalSource as DependencyObject;
                if (m_draggedControlPoint == null)
                {

                    var controlPointView = originalSource.FindAncestor<ControlPointView>();
                    if (controlPointView != null)
                    {
                        m_draggedControlPoint = controlPointView;
                        e.Handled = true;
                        return;
                    }

                }
                else
                {
                    DoDrag();
                    e.Handled = true;
                    return;
                }

                if (_extendedLineDrag == null)
                {

                    var extendedLineView = originalSource.FindAncestor<ExtendedLineView>();
                    if (extendedLineView != null)
                    {
                        this._extendedLineDrag = extendedLineView;
                        e.Handled = true;
                        return;
                    }

                }
                else
                {
                    MoveExtendedLine();
                    e.Handled = true;
                    return;
                }

                if (_currentExponentMouseDown == null)
                {

                    var curve = originalSource.FindAncestor<CurveView>();
                    if (curve != null)
                    {
                        CurveBezierEditor(curve);
                        HorizontalLineEdit(curve);
                        e.Handled = true;
                        return;
                    }
                }
                else if (_nextExponentControlPoint == null)
                {
                    var currentPoint = editor.GetMousePosition();
                    ChangeHorizontalLine(_currentExponentMouseDown, currentPoint);
                    m_lastPosition = currentPoint;
                    e.Handled = true;
                    return;
                }
                else
                {
                    var currentPoint = editor.GetMousePosition();
                    BezierHelper.ChangeBezier(_currentExponentMouseDown, currentPoint, _nextExponentControlPoint);
                    m_lastPosition = currentPoint;
                    e.Handled = true;
                    return;
                }
                if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control) && ViewModel.ShowZoomControls)
                {
                    var canvas = originalSource.FindAncestor<Canvas>();
                    if (canvas != null)
                    {
                        var currentPoint = canvas.GetMousePosition();
                        ViewModel.XAxis.Displacement -= (ViewModel.XAxis.GetValueFromScreen(currentPoint.X) -
                                                         ViewModel.XAxis.GetValueFromScreen(m_lastPosition.X));
                        ViewModel.YAxis.Displacement += (ViewModel.YAxis.GetValueFromScreen(currentPoint.Y) -
                                                         ViewModel.YAxis.GetValueFromScreen(m_lastPosition.Y));
                        m_lastPosition = currentPoint;
                        e.Handled = true;
                    }
                }
            }
        }

        private void MoveExtendedLine()
        {
            if (_extendedLineDrag != null)
            {
                ExtendedLine extendedLine = (ExtendedLine)_extendedLineDrag.DataContext;
                extendedLine.LineStart = new Point(editor.GetMousePosition().X, extendedLine.LineStart.Y);
                extendedLine.LineEnd = new Point(editor.GetMousePosition().X, extendedLine.LineEnd.Y);


                m_lastPosition = editor.GetMousePosition();
            }
        }

        private void OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //Send last action
            if (m_draggedControlPoint != null)
            {
                ControlPoint controlPoint = m_draggedControlPoint.DataContext as ControlPoint;
                if (controlPoint != null)
                    controlPoint.Parent.SendMissedDirty();
            }
            else if (_nextExponentControlPoint != null)
            {
                _nextExponentControlPoint.Parent.SendMissedDirty();
            }
            else if (_currentExponentMouseDown != null)
            {
                _currentExponentMouseDown.Parent.SendMissedDirty();
            }
            else if (_extendedLineDrag != null)
            {
                ExtendedLine extendedLine = _extendedLineDrag.DataContext as ExtendedLine;
                if (extendedLine != null) extendedLine.Parent.SendMissedDirty();
            }


            _nextExponentControlPoint = null;
            _currentExponentMouseDown = null;
            m_draggedControlPoint = null;
            _extendedLineDrag = null;
        }

        private void CurveBezierEditor(CurveView curveView)
        {
            BezierCurveEvaluator evaluator = curveView.DataContext as BezierCurveEvaluator;
            if (evaluator != null &&
                evaluator.IsSelected)
            {
                Curve curve = evaluator.Curve;
                int insertionIndex = 0;
                ControlPoint controlPoint = new ControlPoint(curve, (c, a, b) => true) { XPos = m_lastPosition.X, YPos = m_lastPosition.Y };
                while (insertionIndex < curve.ControlPoints.Count - 1 &&
                  curve.ControlPoints[insertionIndex].X < controlPoint.X)
                {
                    insertionIndex += 1;
                }
                insertionIndex = insertionIndex > 0 ? insertionIndex - 1 : 0;
                _currentExponentMouseDown = curve.ControlPoints[insertionIndex];
                _nextExponentControlPoint = curve.ControlPoints[insertionIndex + 1];
            }
        }



        private void HorizontalLineEdit(CurveView curveView)
        {
            HorizontalLineEvaluator evaluator = curveView.DataContext as HorizontalLineEvaluator;
            if (evaluator != null &&
                evaluator.IsSelected)
            {
                _currentExponentMouseDown = evaluator.Curve.ControlPoints[0];
            }
        }

        private void OnPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var originalSource = e.OriginalSource as DependencyObject;
            var curveView = originalSource.FindAncestor<CurveView>();
            if (curveView != null)
            {
                curveView.InsertionPoint = null;

                var curveEvaluator = curveView.FindDataContext<CurveEvaluator>();
                if (curveEvaluator != null && curveEvaluator.IsSelected)
                {
                    var point = editor.GetMousePosition();
                    var x = point.X;
                    var y = ViewModel.YAxis.DrawingLength - point.Y;

                    ClampXAndY(ref x, ref y);

                    point.X = x;
                    point.Y = y;

                    curveView.InsertionPoint = point;
                }
            }
        }

        private void OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var originalSource = e.OriginalSource as DependencyObject;
            var controlPointView = originalSource.FindAncestor<ControlPointView>();
            if (controlPointView == null)
            {
                var curveView = originalSource.FindAncestor<CurveView>();
                if (curveView != null)
                {
                    var curveEvaluator = curveView.FindDataContext<CurveEvaluator>();
                    if (curveEvaluator != null && curveEvaluator.IsSelected)
                    {
                        var point = editor.GetMousePosition();
                        var x = point.X;
                        var y = ViewModel.YAxis.DrawingLength - point.Y;

                        ClampXAndY(ref x, ref y);

                        point.X = x;
                        point.Y = y;

                        ViewModel.AddPointToCurve(point, curveEvaluator);
                    }
                }
            }
            e.Handled = true;
        }

        private void DoDrag()
        {
            var controlPoint = m_draggedControlPoint.DataContext as ControlPoint;
            if (controlPoint != null)
            {
                var currentPosition = editor.GetMousePosition();
                var diff = m_lastPosition - currentPosition;

                var x = controlPoint.XPos - diff.X;
                var y = controlPoint.YPos + diff.Y;

                ClampXAndY(ref x, ref y);

                controlPoint.XPos = x;
                controlPoint.YPos = y;

                m_lastPosition = currentPosition;
                if (controlPoint.Parent.Parent is BezierCurveEvaluator)
                {
                    BezierHelper.ApplyScaling(controlPoint);
                }
            }
        }

        private void ClampXAndY(ref double x, ref double y)
        {
            var halfXMarkFreq = ViewModel.XAxis.MarkFrequency / 2;
            var halfYMarkFreq = ViewModel.YAxis.MarkFrequency / 2;

            if (x < 0)
            {
                x = 0;
            }
            if (y < 0)
            {
                y = 0;
            }
            if (x > ViewModel.XAxis.Length + halfXMarkFreq)
            {
                x = ViewModel.XAxis.Length + halfXMarkFreq;
            }
            if (y > ViewModel.YAxis.Length + halfYMarkFreq)
            {
                y = ViewModel.YAxis.Length + halfYMarkFreq;
            }
        }

        private void CanvasMouseWheel(object sender, MouseWheelEventArgs wheel)
        {
            if (ViewModel != null && ViewModel.ShowZoomControls && Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
            {
                if (wheel.Delta > 0 && ViewModel.XAxis.Zoom < 100)
                {
                    ViewModel.XAxis.ZoomToCanvasPoint(wheel.GetPosition((UIElement)sender).X, 1.1);
                    ViewModel.YAxis.ZoomToCanvasPoint(((Canvas)sender).ActualHeight - wheel.GetPosition((UIElement)sender).Y, 1.1);
                    ViewModel.ZoomOut.RaiseCanExecuteChanged();
                }
                else
                {
                    ViewModel.ZoomOut.Execute(null);
                }
                wheel.Handled = true;
            }
        }
    }
}