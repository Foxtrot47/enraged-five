﻿using System;
using System.Text;

using Rave.CurveEditor2.Axes;
using Rave.CurveEditor2.Curves;

namespace Rave.CurveEditor2.CurveEvaluators
{
	public class PiecewiseLinearCurveEvaluator : CurveEvaluator
	{
		public PiecewiseLinearCurveEvaluator(CurveEditorModel parent, string curveColor, int? maxControlPoints)
			: base(parent, curveColor, maxControlPoints)
		{
		}

		protected override void Evaluate()
		{
			var pathBuilder = new StringBuilder();
			pathBuilder.Append("M ");

			var first = true;
			foreach (var controlPoint in Curve.ControlPoints)
			{
				if (first)
				{
					pathBuilder.Append(controlPoint.XPos);
					pathBuilder.Append(",");
					pathBuilder.Append(controlPoint.YPos);
					pathBuilder.Append(" L");
					first = false;
				}
				else
				{
					pathBuilder.Append(" ");
					pathBuilder.Append(controlPoint.XPos);
					pathBuilder.Append(",");
					pathBuilder.Append(controlPoint.YPos);
				}
			}

			CurveGeometry = pathBuilder.ToString();
		}

		public static void PopulateCurve(Curve curve,
										 Func<ControlPoint, double, double, bool> canChangeValueFunc,
										 AxisModel xAxis,
										 AxisModel yAxis)
		{
			curve.Clear();
			curve.Add(new ControlPoint(curve, canChangeValueFunc) { X = 0, Y = 0 });
			curve.Add(new ControlPoint(curve, canChangeValueFunc) { X = xAxis.Length, Y = yAxis.Length });
		}
	}
}