﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Windows;
using System.Windows.Input;
using rage.ToolLib.Extensions;
using Rave.CurveEditor2.CurveEvaluators;
using Rave.CurveEditor2.Curves;

namespace Rave.CurveEditor2.Helpers
{
    public static class BezierHelper
    {

        public static Vector2 GetBezierVector(Vector2 lowXPoint, Tuple<float, float, float> magAndAngle, Vector2 highXPoint)
        {
            float magBBe = magAndAngle.Item3;
            float angleR = magAndAngle.Item1;
            float magAb = magAndAngle.Item2;

            Vector2 newDirAb = lowXPoint - highXPoint;
            float newMagAb = newDirAb.Length();
            newDirAb = Vector2.Normalize(newDirAb);

            // Rotate that vector angleR radians to get the new direction to the bezier point.
            // get the sin and cos 
            float tsin, tcos;
            tsin = (float)Math.Sin(angleR);
            tcos = (float)Math.Cos(angleR);
            Vector2 newDirBBe;
            newDirBBe.X = newDirAb.X * tcos - newDirAb.Y * tsin;
            newDirBBe.Y = newDirAb.X * tsin + newDirAb.Y * tcos;

            Vector2 newBezierPoint = highXPoint + ((newMagAb / magAb) * magBBe) * newDirBBe; ;
            newBezierPoint.X = newBezierPoint.X.Clamp(lowXPoint.X + 0.01f, highXPoint.X - 0.01f);
            return newBezierPoint;
        }

        public static Tuple<float, float, float> GetMagAndAngle(Vector2 pointA, Vector2 pointB, Vector2 bezierPoint)
        {
            Vector2 dirAb = pointA - pointB;
            float magAb = dirAb.Length();
            dirAb = Vector2.Normalize(dirAb);


            Vector2 dirBBe = bezierPoint - pointB;
            float magBBe = dirBBe.Length();
            dirBBe = Vector2.Normalize(dirBBe);


            float angleR = (float)Math.Acos(FloatExtensions.Clamp(Vector2.Dot(dirAb, dirBBe), -1, 1f));

            if (dirBBe.Y > dirAb.Y)
            {
                angleR = angleR * -1;
            }

            return new Tuple<float, float, float>(angleR, magAb, magBBe);
        }

        public static double GetXLerp(this Tuple<float, float, float> magAndAngle)
        {
            double mag = Math.Cos(Math.Abs(magAndAngle.Item1 % Math.PI / 2)) * magAndAngle.Item3;

            double value = (magAndAngle.Item2 - mag) / magAndAngle.Item2;
            return value;
        }

        public static void ChangeBezier(ControlPoint closestControlPoint, Point p, ControlPoint nextPoint)
        {
            if (closestControlPoint.Parent.BezierPoints == null || closestControlPoint.Parent.BezierPoints.Count <= 0)
            {
                return;
            }

            double yinverse = closestControlPoint.Parent.Parent.Parent.YAxis.Size - p.Y;
            double y = ControlPoint.GetValueFromScreen(yinverse, closestControlPoint.Parent.Parent.Parent.YAxis) + closestControlPoint.Parent.Parent.Parent.YAxis.MarkFrequency;
            double x = ControlPoint.GetValueFromScreen(p.X, closestControlPoint.Parent.Parent.Parent.XAxis);
            p = new Point(x, y);
            Point p1 = new Point(closestControlPoint.X, closestControlPoint.Y);
            Point p2 = new Point(nextPoint.X, nextPoint.Y);


            Point bz = BezierCurveEvaluator.GetBezier(p, p1, p2);
            Point previousBz =
                closestControlPoint.Parent.BezierPoints[closestControlPoint.Parent.ControlPoints.IndexOf(closestControlPoint)];
            bool isBeyondY = (previousBz.Y < Math.Min(nextPoint.Y, closestControlPoint.Y) ||
                              previousBz.Y > Math.Max(nextPoint.Y, closestControlPoint.Y));

            if (!isBeyondY && !Keyboard.Modifiers.HasFlag(ModifierKeys.Alt) && Math.Abs(p1.Y - p2.Y) > 0.1)
            {
                bz.Y = bz.Y.Clamp(Math.Min(p1.Y, p2.Y), Math.Max(p1.Y, p2.Y));
            }
            bz.X = bz.X.Clamp(p1.X + 0.01, p2.X - 0.01);
            closestControlPoint.Parent.BezierPoints[closestControlPoint.Parent.ControlPoints.IndexOf(closestControlPoint)] = new Point(bz.X, bz.Y);
            CalculateScaling(closestControlPoint.Parent);
        }

        public static void CalculateScaling(Curve curve)
        {
            curve.BezierScaling = new List<RelativePositionBezier>();
            foreach (Point p in curve.BezierPoints)
            {
                if (curve.BezierPoints.IndexOf(p) + 1 < curve.ControlPoints.Count)
                {
                    Vector2 pointA = new Vector2((float)curve.ControlPoints[curve.BezierPoints.IndexOf(p)].X,
                        (float)curve.ControlPoints[curve.BezierPoints.IndexOf(p)].Y);
                    Vector2 pointB = new Vector2((float)curve.ControlPoints[curve.BezierPoints.IndexOf(p) + 1].X,
                        (float)curve.ControlPoints[curve.BezierPoints.IndexOf(p) + 1].Y);
                    Vector2 bezierPoint = new Vector2((float)p.X, (float)p.Y);

                    curve.BezierScaling.Add(new RelativePositionBezier
                    {
                        MagAndAngle = BezierHelper.GetMagAndAngle(pointA, pointB, bezierPoint),
                        LerpValue = (bezierPoint.X - pointA.X) / (pointB.X - pointA.X)
                    });
                }
            }
        }

        public static void ApplyScaling(ControlPoint cp)
        {
            Curve curve = cp.Parent;
            int index = curve.ControlPoints.IndexOf(cp);
            if (Keyboard.Modifiers.HasFlag(ModifierKeys.Alt))
            {
                if (index < curve.BezierPoints.Count && curve.ControlPoints.Count > index + 1)
                {
                    curve.BezierPoints[index] =
                        new Point(
                            curve.BezierScaling[index].LerpValue.Lerp(curve.ControlPoints[index].X,
                                curve.ControlPoints[index + 1].X),
                            curve.BezierPoints[index].Y);
                }
                if (index > 0)
                {
                    curve.BezierPoints[index - 1] =
                        new Point(
                            curve.BezierScaling[index - 1].LerpValue.Lerp(
                                curve.ControlPoints[index - 1].X, curve.ControlPoints[index].X),
                            curve.BezierPoints[index - 1].Y)
                        ;
                }
            }
            else
            {
                if (index > 0)
                {
                    Vector2 stationaryPoint = new Vector2((float)curve.ControlPoints[index - 1].X,
                        (float)curve.ControlPoints[index - 1].Y);
                    Vector2 movedPoint = new Vector2((float)cp.X, (float)cp.Y);
                    Vector2 bezierPoint =
                        BezierHelper.GetBezierVector(stationaryPoint,
                            curve.BezierScaling[index - 1].MagAndAngle, movedPoint);

                    curve.BezierPoints[index - 1] = new Point(bezierPoint.X, bezierPoint.Y);
                }
                if (index < curve.BezierPoints.Count && curve.ControlPoints.Count > index + 1)
                {
                    Vector2 movedPoint = new Vector2((float)cp.X, (float)cp.Y);
                    Vector2 stationaryPoint = new Vector2((float)curve.ControlPoints[index + 1].X,
                        (float)curve.ControlPoints[index + 1].Y);
                    Vector2 bezierPoint =
                        BezierHelper.GetBezierVector(movedPoint,
                            curve.BezierScaling[index].MagAndAngle, stationaryPoint);

                    curve.BezierPoints[index] = new Point(bezierPoint.X, bezierPoint.Y);
                }
             }
        }


    }

}
