﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using Rave.CurveEditor2.Curves;

namespace Rave.CurveEditor2.Axes
{
    /// <summary>
    ///   Interaction logic for XAxisView.xaml
    /// </summary>
    public partial class XAxisView
    {
        private AxisViewModel m_viewModel;

        public XAxisView()
        {
            InitializeComponent();
            DataContext = m_viewModel;
        }

        private void OnUserControlLoaded(object sender, RoutedEventArgs e)
        {
            m_viewModel = DataContext as AxisViewModel;
            if (m_viewModel != null)
            {
                m_viewModel.PropertyChanged += OnViewModelPropertyChanged;
                OnViewModelPropertyChanged(null, null);
            }
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            markings.Children.Clear();


            double displacement = (m_viewModel.Length* m_viewModel.InitialDisplacement);
            double current = displacement * -1;

            double max = m_viewModel.Max;
            double min = m_viewModel.Min;

            while (current + displacement <= m_viewModel.Length)
            {
                var actualCurrent = min + (m_viewModel.GetValueFromScreen(current) * (max - min));
                var value = actualCurrent.ToString("#0.##");
                var textBlock = new TextBlock { Style = Resources["XAxisTextBlockStyle"] as Style, Text = value, };
                Canvas.SetLeft(textBlock, current);
                Canvas.SetTop(textBlock, 7);
                markings.Children.Add(textBlock);

                var line = new Line { X1 = current, Y1 = 0, X2 = current, Y2 = 5, Style = Resources["AxisLineStyle"] as Style };
                markings.Children.Add(line);

                current += m_viewModel.MarkFrequency;

            }
        }

        private void OnUserControlUnloaded(object sender, RoutedEventArgs e)
        {
            if (m_viewModel != null)
            {
                m_viewModel.PropertyChanged -= OnViewModelPropertyChanged;
                m_viewModel = null;
            }
        }
    }
}