using System;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Rave.CurveEditor2.Axes;
using Rave.CurveEditor2.Curves;

namespace Rave.CurveEditor2.CurveEvaluators
{
	public class PiecewiseExponentialCurveEvaluator : CurveEvaluator
	{
		public PiecewiseExponentialCurveEvaluator(CurveEditorModel parent, string curveColor, int? maxControlPoints, Tuple<ExtendedLineMode, ExtendedLineMode> extendedLines = null)
			: base(parent, curveColor, maxControlPoints, extendedLines)
		{
		}

		protected override void Evaluate()
		{
			var pathBuilder = new StringBuilder();
			pathBuilder.Append("M ");
			bool first = true;

			for (int i = 0; i < Curve.ControlPoints.Count; i++)
			{
				var controlPoint = Curve.ControlPoints[i];

				if (first)
				{
					pathBuilder.Append(controlPoint.XPos);
					pathBuilder.Append(",");
					pathBuilder.Append(controlPoint.YPos);
					pathBuilder.Append(" L");
					first = false;
				}
				else
				{
					pathBuilder.Append(" ");
					pathBuilder.Append(controlPoint.XPos);
					pathBuilder.Append(",");
					pathBuilder.Append(controlPoint.YPos);
				}
				double size = Curve.Parent.Parent.XAxis.Scale * Curve.Parent.Parent.XAxis.ZoomedLength;
				double mark = Curve.Parent.Parent.XAxis.MarkFrequency * 2;
				if (Math.Abs(controlPoint.Exponent - 1.00) > 0.05)
				{
					if (i + 1 < Curve.ControlPoints.Count)
					{
						var nextControlPoint = Curve.ControlPoints[i + 1];
						var max = Math.Round((nextControlPoint.XPos - controlPoint.XPos) / 3);
						for (int j = 0; j < max; j++)
						{
							double x = controlPoint.XPos + (nextControlPoint.XPos - controlPoint.XPos) * (j / max);
							if (!(x > 0) || !(x < size + mark)) continue;
							//Don't add points if x is not visible (performance)
							double y = GetY(x, controlPoint.XPos, controlPoint.YPos, nextControlPoint.XPos, nextControlPoint.YPos,
								controlPoint.Exponent);

							pathBuilder.Append(" ");
							pathBuilder.Append(x);
							pathBuilder.Append(",");
							pathBuilder.Append(y);
						}
					}
				}
			}

			CurveGeometry = pathBuilder.ToString();
		}

		private double GetY(double x, double x1, double y1, double x2, double y2, double exponent)
		{

			double y = ((y2 - y1) / Math.Pow((x2 - x1), exponent)) * (Math.Pow((x - x1), exponent)) + y1;
			//if (exponent >= 1)
			//{
				
				return y;
			//}
			//else
			//{
			
			//	double a = (y2 - y1) / (x2 - x1);
			//	double c = y1 - (a * x1);
			//	double d = (x + (y - c) * a) / (1 + (Math.Pow(a, 2)));
			//	y = ((y2 - y1) / Math.Pow((x2 - x1), (1 / exponent))) * (Math.Pow((x - x1), (1 / exponent))) + y1;
			//	return 2 * d - (2 * d * a) - y + (2 * c);
			//}

		}

		public static void PopulateCurve(Curve curve,
										 Func<ControlPoint, double, double, bool> canChangeValueFunc,
										 AxisModel xAxis,
										 AxisModel yAxis)
		{
			curve.Clear();
			curve.Add(new ControlPoint(curve, canChangeValueFunc) { X = 0, Y = 0 });
			curve.Add(new ControlPoint(curve, canChangeValueFunc) { X = xAxis.Length, Y = yAxis.Length });
		}
	}
}