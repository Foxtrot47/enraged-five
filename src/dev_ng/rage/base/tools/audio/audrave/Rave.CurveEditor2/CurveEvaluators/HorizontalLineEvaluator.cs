﻿using System;
using System.Text;

using Rave.CurveEditor2.Axes;
using Rave.CurveEditor2.Curves;

namespace Rave.CurveEditor2.CurveEvaluators
{
	public class HorizontalLineEvaluator : CurveEvaluator
	{
		public HorizontalLineEvaluator(CurveEditorModel parent, string curveColor)
			: base(parent, curveColor, 2, new Tuple<ExtendedLineMode, ExtendedLineMode>(ExtendedLineMode.None, ExtendedLineMode.None))
		{
		}

		protected override void Evaluate()
		{
			var pathBuilder = new StringBuilder();
			pathBuilder.Append("M ");

			var first = true;
			foreach (var controlPoint in Curve.ControlPoints)
			{
				if (first)
				{
					pathBuilder.Append(controlPoint.XPos);
					pathBuilder.Append(",");
					pathBuilder.Append(Curve.ControlPoints[0].YPos);
					pathBuilder.Append(" L");
					first = false;
				}
				else
				{
					pathBuilder.Append(" ");
					pathBuilder.Append(controlPoint.XPos);
					pathBuilder.Append(",");
					pathBuilder.Append(Curve.ControlPoints[0].YPos);
				}
			}

			CurveGeometry = pathBuilder.ToString();
		}

		public static void PopulateCurve(Curve curve,
										 Func<ControlPoint, double, double, bool> canChangeValueFunc,
										 AxisModel xAxis,
										 AxisModel yAxis)
		{
			curve.Clear();
			curve.Add(new ControlPoint(curve, canChangeValueFunc) { X = 0, Y = 0 });
			curve.Add(new ControlPoint(curve, canChangeValueFunc) { X = xAxis.Length, Y = yAxis.Length });
		}
	}
}