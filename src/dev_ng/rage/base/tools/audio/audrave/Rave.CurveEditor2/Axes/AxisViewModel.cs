using System;
using System.ComponentModel;
using System.Dynamic;
using System.Security.Policy;
using System.Threading;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using rage.ToolLib.Extensions;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor2.Axes
{
    public class AxisViewModel : INotifyPropertyChanged
    {
        private readonly AxisModel m_model;
        public double InitialDisplacement = -0.05;
        public double Indent
        {
            get { return _indent; }
            set { _indent = value.Clamp(0, 1); }
        }
        public AxisViewModel(AxisModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            m_model = model;
            Displacement = InitialDisplacement;
            m_model.LengthChanged += OnLengthChanged;
            m_model.LabelChanged += OnLabelChanged;
            m_model.MinMaxChanged += OnMinMaxChanged;

        }

        public AxisViewModel(AxisModel model, bool axisVisibility, double initialDisplacement)
        {
            ShowMarking = axisVisibility;
            InitialDisplacement = initialDisplacement;
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            m_model = model;
            Displacement = InitialDisplacement;
            m_model.LengthChanged += OnLengthChanged;
            m_model.LabelChanged += OnLabelChanged;
            m_model.MinMaxChanged += OnMinMaxChanged;
        }

        public bool ShowMarking
        {
            get { return _showMarking; }
            set
            {
                _showMarking = value;
                PropertyChanged.Raise(this, "ShowMarking");
            }
        }

        #region IAxisViewModel Members

        public event PropertyChangedEventHandler PropertyChanged;

        public double Zoom
        {
            get { return m_model.Zoom; }
            set
            {
                m_model.Zoom = value;
                this.PropertyChanged.Raise(this, "Zoom");
            }
        }

        public double Min
        {
            get { return m_model.Min; }
            set { m_model.Min = value; }
        }

        public double Max
        {
            get { return m_model.Max; }
            set { m_model.Max = value; }
        }

        public double Length
        {
            get { return m_model.ZoomedLength * m_model.Scale; }
        }

        public double ActualLength
        {
            get { return m_model.Length; }
        }

        public double MarkFrequency
        {
            get { return m_model.MarkFrequency * m_model.Scale; }
        }

        public double ActualMarkFrequency
        {
            get { return m_model.MarkFrequency; }
        }

        public double Displacement { get { return m_model.Displacement; } set { m_model.Displacement = value; } }

        public double DrawingLength
        {
            get { return Length + MarkFrequency; }
        }

        public string Label
        {
            get { return m_model.Label; }
            set { m_model.Label = value; }
        }

        public ParameterDestinations? Destination
        {
            get { return m_model.Destination; }
            set
            {
                m_model.Destination = value;
                PropertyChanged.Raise(this, "Destination");
            }
        }

        public void Dispose()
        {
            m_model.LengthChanged -= OnLengthChanged;
            m_model.LabelChanged -= OnLabelChanged;
            m_model.MinMaxChanged -= OnMinMaxChanged;
        }

        #endregion IAxisViewModel Members

        private void OnMinMaxChanged()
        {
            PropertyChanged.Raise(this, "ActualMin");
            PropertyChanged.Raise(this, "ActualMax");
        }

        private void OnLengthChanged()
        {
            PropertyChanged.Raise(this, "Length");
            PropertyChanged.Raise(this, "ActualLength");
            PropertyChanged.Raise(this, "MarkFrequency");
            PropertyChanged.Raise(this, "ActualMarkFrequency");
            PropertyChanged.Raise(this, "DrawingLength");
        }

        private void OnLabelChanged()
        {
            PropertyChanged.Raise(this, "Label");
        }

        private ICommand _zoomCommand;
        private bool _showMarking = true;
        private double _indent;

        public ICommand ZoomCommand
        {
            get
            {
                if (_zoomCommand == null)
                {
                    _zoomCommand = new RelayCommand<MouseWheelEventArgs>
                        (
                        WheelZoom
                        );
                }
                return _zoomCommand;
            }
        }

        private void WheelZoom(MouseWheelEventArgs wheel)
        {
            if (wheel.Delta > 0 && Zoom < 2)
            {
                Zoom = Zoom * 1.1;
            }
            else if (Zoom > 0)
            {
                Zoom = Zoom / 1.1;
            }
        }

        public void ZoomToCanvasPoint(double mousePos, double zoomMultiplier)
        {
            double properValue = (mousePos / m_model.Scale) - (MarkFrequency / 2) + (m_model.Displacement / (m_model.Max - m_model.Min));

            Zoom = Zoom * zoomMultiplier;
            double positionOfValueAfterMove = (properValue + (MarkFrequency / 2) - (m_model.Displacement / (m_model.Max - m_model.Min))) * m_model.Scale;

            Displacement += ((positionOfValueAfterMove - mousePos) / m_model.Scale) * (m_model.Max - m_model.Min);
        }

        public double GetValueFromScreen(double value)
        {
            AxisModel axisModel = m_model;
            //double halfMarkFreq = axisModel.MarkFrequency / 2;
            return (value / axisModel.Scale) + axisModel.Displacement; // - halfMarkFreq;
        }

        public void ResetZoom()
        {
            Zoom = 1;
            Displacement = 0;
        }

        public void ZoomCentral(double multiplier)
        {
            double previousLength = m_model.ZoomedLength;
            Zoom = Zoom * multiplier;
            Displacement += (previousLength - m_model.ZoomedLength) / 2;
        }

        private double MinMax { get { return Math.Abs(Max - Min); } }

        public double ZoomMin { get { return Min + (Displacement * MinMax); } }
        public double ZoomMax { get { return Min + (Displacement * MinMax) + (m_model.Length * MinMax / Zoom); } }
        public void SetSize(double size)
        {
            m_model.Size = size - MarkFrequency;
            OnLengthChanged();
            Zoom = Zoom;
            Displacement = Displacement;
        }
    }
}