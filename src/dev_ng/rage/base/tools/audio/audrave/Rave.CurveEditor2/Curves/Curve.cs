using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Windows.Media;
using rage.ToolLib;
using Rave.CurveEditor2.CurveEvaluators;
using Rave.CurveEditor2.Helpers;
using WPFToolLib.Extensions;

namespace Rave.CurveEditor2.Curves
{
    using System.Linq;
    using System.Windows;

    public class Curve : INotifyPropertyChanged
    {
        private readonly int? m_maxControlPoints;
        private bool m_isSelected;
        private readonly Tuple<ExtendedLineMode, ExtendedLineMode> _extendedLines = new Tuple<ExtendedLineMode, ExtendedLineMode>(ExtendedLineMode.Horizontal, ExtendedLineMode.Horizontal);

        public Curve(CurveEvaluator parent, string color, int? maxControlPoints, Tuple<ExtendedLineMode, ExtendedLineMode> extendedLines = null)
        {

            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }
            if (extendedLines != null)
            {
                _extendedLines = extendedLines;
            }
            Parent = parent;
            Color = color;
            m_maxControlPoints = maxControlPoints;
            ControlPoints = new BindingList<ControlPoint>();
            BezierPoints = new BindingList<Point>();
            ExtendedLines = new BindingList<ExtendedLine>();
            parent.PropertyChanged += this.parent_PropertyChanged;
        }

        #region Curve Members

        public event Action Dirty;

        public CurveEvaluator Parent { get; private set; }

        public bool IsSelected
        {
            get { return m_isSelected; }
            set
            {
                if (m_isSelected != value)
                {
                    m_isSelected = value;
                    PropertyChanged.Raise(this, "IsSelected");
                }
            }
        }

        public BindingList<ControlPoint> ControlPoints { get; private set; }
        public BindingList<Point> BezierPoints { get; private set; }
        public List<RelativePositionBezier> BezierScaling;
        public BindingList<ExtendedLine> ExtendedLines { get; private set; }

        public bool CanAddControlPoint
        {
            get
            {
                if (m_maxControlPoints == null)
                {
                    return true;
                }
                if (ControlPoints.Count <
                    m_maxControlPoints.Value)
                {
                    return true;
                }
                return false;
            }
        }

        public string Color { get; private set; }

        public void Add(ControlPoint controlPoint)
        {
            if (controlPoint.Parent == this && CanAddControlPoint)
            {
                controlPoint.Dirty += OnDirty;
                ControlPoints.Add(controlPoint);
                PropertyChanged.Raise(this, "CanAddControlPoint");
                OnDirty();
            }
        }

        public void AddBezierPoints(IEnumerable<Point> bezierPoints)
        {
            Point[] pointsToPlot = bezierPoints as Point[] ?? bezierPoints.ToArray();

            for (int i = 0; i < pointsToPlot.Count(); i++)
            {
                if (BezierPoints.Count > i)
                {
                    BezierPoints[i] = pointsToPlot[i];
                }
                else
                {
                    BezierPoints.Add(pointsToPlot[i]);
                }

            }

        }

        public void Insert(int index, ControlPoint controlPoint)
        {
            if (controlPoint.Parent == this && CanAddControlPoint && index >= 0 &&
                index < ControlPoints.Count)
            {
                controlPoint.Dirty += OnDirty;
                ControlPoints.Insert(index, controlPoint);
                PropertyChanged.Raise(this, "CanAddControlPoint");
                OnDirty();
            }
        }

        public void Remove(ControlPoint controlPoint)
        {
            if (controlPoint.Parent ==
                this)
            {
                controlPoint.Dirty -= OnDirty;
                ControlPoints.Remove(controlPoint);
                PropertyChanged.Raise(this, "CanAddControlPoint");
                OnDirty();
            }
        }

        public void Clear()
        {
            foreach (var controlPoint in ControlPoints)
            {
                controlPoint.Dirty -= OnDirty;
            }
            ControlPoints.Clear();
            PropertyChanged.Raise(this, "CanAddControlPoint");
            OnDirty();
        }

        public void Refresh()
        {
            foreach (var controlPoint in ControlPoints)
            {
                controlPoint.RefreshTooltip();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Curve Members

        private void parent_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.UpdateExtendedLines();
        }

        private void UpdateExtendedLines()
        {
            this.ExtendedLines.Clear();
            if (!this.ControlPoints.Any())
            {
                return;
            }

            var orderedPoints = this.ControlPoints.OrderBy(p => p.X);
            var firstPoint = orderedPoints.First();
            var lastPoint = orderedPoints.Last();

            var maxY = this.Parent.Parent.YAxis.ZoomedLength * this.Parent.Parent.YAxis.Scale;
            if (firstPoint.XPos > 0 && (_extendedLines.Item1.Equals(ExtendedLineMode.Horizontal) || _extendedLines.Item1.Equals(ExtendedLineMode.Both)))
            {
                var line = new ExtendedLine(this, new Point(0, firstPoint.YPos), new Point(firstPoint.XPos, firstPoint.YPos));
                this.ExtendedLines.Add(line);
            }
            if (firstPoint.XPos > 0 && (_extendedLines.Item1.Equals(ExtendedLineMode.Vertical) || _extendedLines.Item1.Equals(ExtendedLineMode.Both)))
            {
                var line = new ExtendedLine(this, new Point(firstPoint.XPos, 0), new Point(firstPoint.XPos, maxY));
                ExtendedLines.Add(line);
            }

            var maxX = this.Parent.Parent.XAxis.ZoomedLength * this.Parent.Parent.XAxis.Scale;

            if (lastPoint.X < maxX && (_extendedLines.Item2.Equals(ExtendedLineMode.Horizontal) || _extendedLines.Item2.Equals(ExtendedLineMode.Both)))
            {
                var lineX = Math.Max(maxX, lastPoint.XPos);
                var line = new ExtendedLine(this, new Point(lastPoint.XPos, lastPoint.YPos), new Point(lineX, lastPoint.YPos));
                this.ExtendedLines.Add(line);
            }
            if (lastPoint.X < maxX && (_extendedLines.Item2.Equals(ExtendedLineMode.Vertical) || _extendedLines.Item2.Equals(ExtendedLineMode.Both)))
            {
                var line = new ExtendedLine(this, new Point(lastPoint.XPos, 0), new Point(lastPoint.XPos, maxY));
                ExtendedLines.Add(line);
            }
        }

        private double _lastSent = DateTime.Now.TimeOfDay.TotalMilliseconds;
        private Task OnDirtyTask;
        private bool _missed;

        internal void OnDirty()
        {
            if (Math.Abs(DateTime.Now.TimeOfDay.TotalMilliseconds - _lastSent) > 50)
            {
                Dirty.Raise();
                _lastSent = DateTime.Now.TimeOfDay.TotalMilliseconds;
                _missed = false;
            }
            else
            {
                _missed = true;
            }

        }

        public void SendMissedDirty()
        {
            if (_missed)
            {
                Dirty.Raise();
                _missed = false;
            }
        }

    }
}