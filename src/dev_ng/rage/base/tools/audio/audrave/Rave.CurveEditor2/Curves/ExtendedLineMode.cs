﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rave.CurveEditor2.Curves
{
	public enum ExtendedLineMode
	{
		Horizontal,
		Vertical,
		Both,
		None
	}
}
