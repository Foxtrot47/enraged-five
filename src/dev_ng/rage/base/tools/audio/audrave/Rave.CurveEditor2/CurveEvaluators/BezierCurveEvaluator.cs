#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Windows;
using rage.ToolLib.Extensions;
using Rave.CurveEditor2.Axes;
using Rave.CurveEditor2.Curves;
using Rave.CurveEditor2.Helpers;

#endregion

namespace Rave.CurveEditor2.CurveEvaluators
{
    public class BezierCurveEvaluator : CurveEvaluator
    {
        private bool _update = true;
        private Dictionary<ControlPoint, List<Point>> _controlPointAndActualPoints =
            new Dictionary<ControlPoint, List<Point>>();

        private Dictionary<ControlPoint, bool> _changedControlPoints = new Dictionary<ControlPoint, bool>();

        public BezierCurveEvaluator(CurveEditorModel parent, string curveColor, int? maxControlPoints,
            Tuple<ExtendedLineMode, ExtendedLineMode> extendedLines = null)
            : base(parent, curveColor, maxControlPoints, extendedLines)
        {
            Curve.BezierPoints.ListChanged += OnBezierListChanged;
        }

        private void OnBezierListChanged(object sender, ListChangedEventArgs e)
        {
            UpdateChangedControlPoints(e);

            Evaluate();
        }

        private void UpdateChangedControlPoints(ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemChanged)
            {
                _callDirty = true;
                _changedControlPoints[Curve.ControlPoints[e.NewIndex]] = true;
            }
            else if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                _callDirty = true;
                int index = e.NewIndex;
                if (index < Curve.ControlPoints.Count)
                {
                    _changedControlPoints[Curve.ControlPoints[index]] = true;
                }
                if (index > 0 && index - 1 < Curve.ControlPoints.Count)
                {
                    _changedControlPoints[Curve.ControlPoints[index - 1]] = true;
                }
                if (index + 1 < Curve.ControlPoints.Count)
                {
                    _changedControlPoints[Curve.ControlPoints[index + 1]] = true;
                }
            }
            else if (e.ListChangedType == ListChangedType.Reset || e.ListChangedType == ListChangedType.ItemDeleted)
            {
                _callDirty = true;
                _changedControlPoints = new Dictionary<ControlPoint, bool>();
                _controlPointAndActualPoints = new Dictionary<ControlPoint, List<Point>>();
            }
        }

        protected override void Evaluate()
        {
            if (!_update)
            {
                return;
            }
            NormalizeBezierPoints();
            PopulateActualPoints();


            List<Point> points = _controlPointAndActualPoints.Select(p => p.Value).SelectMany(i => i).ToList();
            points = points.GroupBy(p => p.X).Select(g => g.First()).ToList();
            points.Sort(Comparer<Point>.Create((point, point1) => point.X.CompareTo(point1.X)));

            StringBuilder pathBuilder = new StringBuilder();
            pathBuilder.Append("M ");

            bool first = true;
            foreach (Point controlPoint in points)
            {
                if (first)
                {
                    pathBuilder.Append(controlPoint.X);
                    pathBuilder.Append(",");
                    pathBuilder.Append(controlPoint.Y);
                    pathBuilder.Append(" L");
                    first = false;
                }
                else
                {
                    pathBuilder.Append(" ");
                    pathBuilder.Append(controlPoint.X);
                    pathBuilder.Append(",");
                    pathBuilder.Append(controlPoint.Y);
                }
            }

            CurveGeometry = pathBuilder.ToString();
        }

        protected override void AxisChanged()
        {
            List<ControlPoint> list = _changedControlPoints.Keys.ToList();
            foreach (ControlPoint changedControlPoint in list)
            {
                _changedControlPoints[changedControlPoint] = true;
            }
            Evaluate();
        }

        protected override void OnListChanged(object sender, ListChangedEventArgs e)
        {
            _update = false;
            if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                if (e.NewIndex > 0 && e.NewIndex < Curve.ControlPoints.Count - 1 &&
                    Curve.Parent is BezierCurveEvaluator)
                {
                    Vector2 pointA = new Vector2((float)Curve.ControlPoints[e.NewIndex - 1].X,
                        (float)Curve.ControlPoints[e.NewIndex - 1].Y);

                    Vector2 pointB = new Vector2((float)Curve.ControlPoints[e.NewIndex].X,
                        (float)Curve.ControlPoints[e.NewIndex].Y);

                    Vector2 bezierPoint = BezierHelper.GetBezierVector(pointA,
                        Curve.BezierScaling[e.NewIndex - 1].MagAndAngle, pointB);

                    Curve.BezierPoints[e.NewIndex - 1] = new Point(bezierPoint.X, bezierPoint.Y);

                    Vector2 pointC = new Vector2((float)Curve.ControlPoints[e.NewIndex + 1].X,
                        (float)Curve.ControlPoints[e.NewIndex + 1].Y);

                    Vector2 bezierPoint2 = BezierHelper.GetBezierVector(pointB,
                        Curve.BezierScaling[e.NewIndex - 1].MagAndAngle, pointC);

                    Curve.BezierPoints.Insert
                        (e.NewIndex,
                            new Point(bezierPoint2.X, bezierPoint2.Y)
                        );
                }
                BezierHelper.CalculateScaling(Curve);
            }

            if (e.ListChangedType == ListChangedType.ItemDeleted)
            {

                if (Curve.BezierPoints.Count > e.NewIndex)
                { Curve.BezierPoints.RemoveAt(e.NewIndex); }
                else
                {
                    Curve.BezierPoints.RemoveAt(e.NewIndex - 1);
                }

                BezierHelper.CalculateScaling(Curve);
            }


            UpdateChangedControlPoints(e);
            _update = true;
            Evaluate();

        }
        bool _callDirty = false;

        private void NormalizeBezierPoints()
        {
            _update = false;

            while (Curve.ControlPoints.Count <= Curve.BezierPoints.Count && Curve.BezierPoints.Count > 0)
            {
                Curve.BezierPoints.RemoveAt(Curve.BezierPoints.Count - 1);
                _callDirty = true;
            }
            while (Curve.ControlPoints.Count > Curve.BezierPoints.Count + 1)
            {
                ControlPoint cp = Curve.ControlPoints[Curve.BezierPoints.Count];
                ControlPoint np = Curve.ControlPoints[Curve.BezierPoints.Count + 1];
                _changedControlPoints[cp] = true;
                Curve.BezierPoints.Add(new Point(0.5d.Lerp(cp.X, np.X), 0.5d.Lerp(cp.Y, np.Y)));
                _callDirty = true;
            }

            for (int i = 0; i < Curve.BezierPoints.Count; i++)
            {
                ControlPoint cp = Curve.ControlPoints[i];
                ControlPoint np = Curve.ControlPoints[i + 1];
                double x = Curve.BezierPoints[i].X.Clamp(cp.X, np.X);
                if (Math.Abs(x - Curve.BezierPoints[i].X) > 0.001d)
                {
                    _changedControlPoints[cp] = true;
                    Curve.BezierPoints[i] = new Point(x, Curve.BezierPoints[i].Y);
                    _callDirty = true;
                }

            }

            _update = true;
            if (_callDirty)
            {
                Curve.OnDirty();
                _callDirty = false;
            }
        }
        private void PopulateActualPoints()
        {
            double size = Curve.Parent.Parent.XAxis.Scale * Curve.Parent.Parent.XAxis.ZoomedLength;


            for (int i = 0; i < Curve.ControlPoints.Count; i++)
            {
                //Last Control point doesn't have bezier
                ControlPoint controlPoint = Curve.ControlPoints[i];
                //Check if changed else skip:
                if (_changedControlPoints.ContainsKey(controlPoint) &&
                    _changedControlPoints[controlPoint] == false)
                {
                    continue;
                }


                _controlPointAndActualPoints[controlPoint] = new List<Point>();
                _controlPointAndActualPoints[controlPoint].Add(new Point(controlPoint.XPos, controlPoint.YPos));

                //Check if it's last point (last point does not have bezier)
                if (i == Curve.ControlPoints.Count - 1)
                {
                    continue;
                }


                //Get next control point
                ControlPoint nextControlPoint = Curve.ControlPoints[i + 1];

                if (i < Curve.BezierPoints.Count)
                {
                    Point bezierPoint = Curve.BezierPoints[i];
                    if (i + 1 < Curve.ControlPoints.Count)
                    {
                        double max = Math.Round((nextControlPoint.XPos - controlPoint.XPos) / ((nextControlPoint.XPos - controlPoint.XPos) > 30 ? 6 : 1));
                        for (int j = 0; j < max; j++)
                        {
                            float x = (float)(controlPoint.X + (nextControlPoint.X - controlPoint.X) * (j / max));

                            //Don'X add points if X is not visible (performance)
                            if (x < (0 - size / 10) || x > (size / 10 + size)) continue;

                            float y = GetY(x, (float)controlPoint.X, (float)controlPoint.Y, (float)nextControlPoint.X,
                                (float)nextControlPoint.Y,
                                (float)bezierPoint.X, (float)bezierPoint.Y);

                            ControlPoint thisPoint = new ControlPoint(Curve, Parent.CanChangeValue) { X = x, Y = y };
                            y = (float)thisPoint.YPos;
                            x = (float)thisPoint.XPos;

                            _controlPointAndActualPoints[controlPoint].Add(new Point(x, y));
                        }
                    }
                }

                _controlPointAndActualPoints[controlPoint].Add(new Point(nextControlPoint.XPos, nextControlPoint.YPos));



                _changedControlPoints[controlPoint] = false;
                _changedControlPoints[nextControlPoint] = false;
            }
        }

        private float GetY(float x, float x1, float y1, float x2, float y2, float bx, float by)
        {
            bx = bx.Clamp(x1 + float.Epsilon, x2 - float.Epsilon);

            float linearRatio = x.GetLinearRatio(x1, x2);

            Vector2 yInterp = Vector2.Lerp(new Vector2(y1, by), new Vector2(by, y2), linearRatio);
            Vector2 xInterp = Vector2.Lerp(new Vector2(x1, bx), new Vector2(bx, x2), linearRatio);

            float transformedRatio = x.GetLinearRatio(xInterp.X, xInterp.Y);

            float y = transformedRatio.Lerp(yInterp.X, yInterp.Y);

            return y;
        }

        public static Point GetBezier(Point currentpoint, Point previousPoint, Point nextPoint)
        {
            double x1 = currentpoint.X;
            double y1 = 2 * currentpoint.Y - previousPoint.Y / 2 - nextPoint.Y / 2;

            return new Point(x1, y1);
        }

        public static void PopulateCurve(Curve curve,
            Func<ControlPoint, double, double, bool> canChangeValueFunc,
            AxisModel xAxis,
            AxisModel yAxis)
        {
            curve.Clear();
            curve.Add(new ControlPoint(curve, canChangeValueFunc) { X = 0, Y = 0 });
            curve.Add(new ControlPoint(curve, canChangeValueFunc) { X = xAxis.Length, Y = yAxis.Length });
            curve.AddBezierPoints(new List<Point> { new Point(0.5, 0.5) });
            BezierHelper.CalculateScaling(curve);
        }
    }
}