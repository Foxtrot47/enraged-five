﻿// -----------------------------------------------------------------------
// <copyright file="DialogueIssues.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Rave.Reports.DialogueIssues
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    using global::Rave.Controls.Forms.Popups;
    using global::Rave.ObjectBrowser.Infrastructure.Interfaces;

    using global::Rave.Plugins.Infrastructure.Interfaces;

    using global::Rave.WaveBrowser.Infrastructure.Interfaces;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// Report to identify issues between DStar and RAVE dialogue data.
    /// </summary>
    public class DialogueIssues : IRAVEReportPlugin
    {
        /// <summary>
        /// The asset manager.
        /// </summary>
        private IAssetManager assetManager;

        /// <summary>
        /// The report name.
        /// </summary>
        private string name;

        /// <summary>
        /// The dstar config path.
        /// </summary>
        private string dstarConfigPath;

        /// <summary>
        /// The dstar root path.
        /// </summary>
        private string dstarRootPath;

        /// <summary>
        /// The built waves path.
        /// </summary>
        private string builtWavesPath;

        /// <summary>
        /// The dstar local config path.
        /// </summary>
        private string dstarLocalConfigPath;

        /// <summary>
        /// The dstar local root path.
        /// </summary>
        private string dstarLocalRootPath;

        /// <summary>
        /// The built local waves path.
        /// </summary>
        private string builtLocalWavesPath;

        /// <summary>
        /// The character names.
        /// </summary>
        private Dictionary<string, string> characterNames;

        /// <summary>
        /// The wave elements.
        /// </summary>
        private Dictionary<string, XElement> waveElems;

        /// <summary>
        /// Get the report name.
        /// </summary>
        /// <returns>
        /// The report name <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        ///  Initialize the report.
        /// </summary>
        /// <param name="settings">
        /// The settings for running the report.
        /// </param>
        /// <returns>
        /// Indicates if report generation was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            foreach (XmlNode setting in settings)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;

                    case "DstarConfigPath":
                        this.dstarConfigPath = setting.InnerText;
                        break;

                    case "DstarRootPath":
                        this.dstarRootPath = setting.InnerText;
                        break;

                    case "BuiltWavesPath":
                        this.builtWavesPath = setting.InnerText;
                        break;
                }
            }

            if (string.IsNullOrEmpty(this.name))
            {
                throw new Exception("Report name required");
            }

            if (string.IsNullOrEmpty(this.dstarConfigPath))
            {
                throw new Exception("DStar config path required");
            }

            if (string.IsNullOrEmpty(this.dstarRootPath))
            {
                throw new Exception("DStar root path required");
            }

            if (string.IsNullOrEmpty(this.builtWavesPath))
            {
                throw new Exception("Built waves path required");
            }

            this.assetManager = AssetManagerFactory.GetInstance(AssetManagerType.Perforce);

            if (null == this.assetManager)
            {
                throw new Exception("Failed to get instance of Perforce asset manager");
            }

            return true;
        }

        /// <summary>
        /// Generate and display the report data.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser containing wave nodes.
        /// </param>
        /// <param name="browsers">
        /// The sound browsers.
        /// </param>
        public void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string, IObjectBrowser> browsers)
        {
            // We get local paths at this point instead of in Init() in case 
            // user doesn't have required path mappings (which would cause an error when RAVE starts up)
            this.dstarLocalConfigPath = this.assetManager.GetLocalPath(this.dstarConfigPath);
            this.dstarLocalRootPath = this.assetManager.GetLocalPath(this.dstarRootPath);
            this.builtLocalWavesPath = this.assetManager.GetLocalPath(this.builtWavesPath);

            if (!this.GetLatestData())
            {
                throw new Exception("Failed to get latest data");
            }

            if (!this.LoadData())
            {
                throw new Exception("Failed to load required data");
            }

            this.DisplayResults();
        }

        /// <summary>
        /// Get the latest data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GetLatestData()
        {
            return this.assetManager.GetLatest(this.dstarConfigPath, true) 
                && this.assetManager.GetLatest(this.dstarRootPath, true) 
                && this.assetManager.GetLatest(this.builtWavesPath, true);
        }

        /// <summary>
        /// Load the data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadData()
        {
            this.characterNames = new Dictionary<string, string>();
            var configDoc = XDocument.Load(this.dstarLocalConfigPath);

            // Load the DStar character names
            foreach (var characterElem in configDoc.Descendants("Character"))
            {
                var guid = characterElem.Attribute("guid").Value.ToUpper();
                var characterName = characterElem.Attribute("name").Value.ToUpper();
                this.characterNames[guid] = characterName;
            }

            // Load the built waves into a single XML object
            var builtWavesDoc = new XDocument();
            var rootElem = new XElement("BuiltWaves");
            builtWavesDoc.Add(rootElem);

            foreach (var file in Directory.GetFiles(this.builtLocalWavesPath, "*.xml", SearchOption.AllDirectories))
            {
                var fileName = Path.GetFileName(file);
                if (null == fileName || !fileName.ToUpper().StartsWith("SS_"))
                {
                    continue;
                }

                var doc = XDocument.Load(file);
                rootElem.Add(doc.Root);
            }

            // Locate the wave elements (without sequence prefix).
            this.waveElems = new Dictionary<string, XElement>();
            foreach (var waveElem in builtWavesDoc.Descendants("Wave"))
            {
                var waveName = waveElem.Attribute("name").Value.ToUpper();
                var key = Path.GetFileNameWithoutExtension(waveName);
                if (this.waveElems.ContainsKey(key))
                {
                    throw new Exception("Duplicate wave name detected: " + key);
                }

                this.waveElems[key] = waveElem;
            }

            return true;
        }

        /// <summary>
        /// Display the results.
        /// </summary>
        private void DisplayResults()
        {
            // Write results to temp file
            var resultsFileName = Path.Combine(Path.GetTempPath(), this.name + "_report.html");
            var filesteam = new FileStream(resultsFileName, FileMode.Create);
            var sw = new StreamWriter(filesteam);

            sw.WriteLine("<html><head><title>Dialogue Issues</title></head><body>");

            var waveVoiceErrors = new List<string>();
            var missingLines = new List<XElement>();
            var extraSpeechWaves = this.waveElems.Where(
                waveElem => !waveElem.Key.Contains(".PROCESSED"))
                .ToDictionary(waveElem => waveElem.Key, waveElem => waveElem.Value);

            foreach (var file in Directory.GetFiles(this.dstarLocalRootPath, "*.dstar", SearchOption.AllDirectories))
            {
                var doc = XDocument.Load(file);
                foreach (var lineElem in doc.Descendants("Line"))
                {
                    if (IsCutsceneConversation(lineElem))
                    {
                        continue;
                    }

                    var guid = lineElem.Attribute("guid").Value.ToUpper();
                    var character = this.characterNames[guid];
                    var filename = lineElem.Attribute("filename").Value.ToUpper();
                    if (!IsConversationRandom(lineElem))
                    {
                        // Non-random lines don't include the default sequence number prefix
                        filename += "_01";
                    }

                    if (!this.waveElems.ContainsKey(filename))
                    {
                        if (!filename.StartsWith("SFX_"))
                        {
                            missingLines.Add(lineElem);
                        }

                        continue;
                    }

                    var waveElem = this.waveElems[filename];
                    extraSpeechWaves.Remove(filename);
                    var voiceTagElem = GetVoiceTagElem(waveElem);
                    var voiceName = voiceTagElem.Attribute("value").Value.ToUpper();
                    if (voiceName == character)
                    {
                        continue;
                    }

                    var msg = string.Format("Wave voice name '{0}' doesn't match DStar character {1}: {2}", voiceName, character, GetPath(waveElem));
                    waveVoiceErrors.Add(msg);
                }
            }

            // Part 1: Wave voice name inconsistencies
            sw.WriteLine("<h2>Wave Voice Name Inconsistencies ({0})</h2>", waveVoiceErrors.Count);
            if (!waveVoiceErrors.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<ul>");
                foreach (var error in waveVoiceErrors)
                {
                    sw.WriteLine("<li>" + error + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            // Part 2: Lines in DStar but not in built waves
            sw.WriteLine("<h2>Lines in DialogStar but not in Built Waves ({0})</h2>", missingLines.Count);
            if (!missingLines.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<ul>");
                foreach (var line in missingLines)
                {
                    sw.WriteLine("<li>" + this.GetLineInfo(line) + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            // Part 3: Speech waves in built waves but not in DStar
            sw.WriteLine("<h2>Speech Waves in Built Waves but not in DialogStar ({0})</h2>", extraSpeechWaves.Count);
            if (!extraSpeechWaves.Any())
            {
                sw.WriteLine("<p>None</p>");
            }
            else
            {
                sw.WriteLine("<ul>");
                foreach (var extraSpeechWave in extraSpeechWaves.Values)
                {
                    sw.WriteLine("<li>" + GetPath(extraSpeechWave) + "</li>");
                }

                sw.WriteLine("</ul>");
            }

            sw.WriteLine("</body></html>");

            sw.Close();
            filesteam.Close();

            // Display report
            var report = new frmReport(this.name + " Report");
            report.SetURL(resultsFileName);
            report.Show();
        }

        /// <summary>
        /// Determine if line is in a conversation that is random.
        /// </summary>
        /// <param name="lineElem">
        /// The line elemenet.
        /// </param>
        /// <returns>
        /// Flag indicating conversation is random <see cref="bool"/>.
        /// </returns>
        private static bool IsConversationRandom(XElement lineElem)
        {
            var convoElem = lineElem.Ancestors("Conversation").First();
            var randomAttr = convoElem.Attribute("random");
            return null == randomAttr || randomAttr.Value.ToUpper() == "TRUE";
        }

        /// <summary>
        /// Determine if line is in a conversation that is for a cutscene.
        /// </summary>
        /// <param name="lineElem">
        /// The line element.
        /// </param>
        /// <returns>
        /// Flag indicating conversation is for a cutscene <see cref="bool"/>.
        /// </returns>
        private static bool IsCutsceneConversation(XElement lineElem)
        {
            var convoElem = lineElem.Ancestors("Conversation").First();
            var cutsceneAttr = convoElem.Attribute("cutscene");
            return null == cutsceneAttr || cutsceneAttr.Value.ToUpper() == "TRUE";
        }

        /// <summary>
        /// Get the info for a particular DStar line.
        /// </summary>
        /// <param name="lineElem">
        /// The line element.
        /// </param>
        /// <returns>
        /// The line info <see cref="string"/>.
        /// </returns>
        private string GetLineInfo(XElement lineElem)
        {
            var dialogue = lineElem.Attribute("dialogue").Value;
            var guid = lineElem.Attribute("guid").Value.ToUpper();
            var character = this.characterNames[guid];
            var filename = lineElem.Attribute("filename").Value;
            var convoElem = lineElem.Ancestors("Conversation").First();
            var convoRoot = convoElem.Attribute("root").Value;
            return string.Format("{0}\\{1}\\{2} - \"{3}\"", convoRoot, character, filename, dialogue);
        }

        /// <summary>
        /// Get the voice tag element.
        /// </summary>
        /// <param name="waveElem">
        /// The wave element.
        /// </param>
        /// <returns>
        /// The voice tag element <see cref="XElement"/>.
        /// </returns>
        private static XElement GetVoiceTagElem(XElement waveElem)
        {
            var parent = waveElem.Parent;
            var tagElem = parent.Elements("Tag").FirstOrDefault(e => e.Attribute("name").Value.ToUpper() == "VOICE")
                          ?? waveElem.Ancestors("Tag").First(e => e.Attribute("name").Value.ToUpper() == "VOICE");

            return tagElem;
        }

        /// <summary>
        /// Get the path of an element using the 'name' attributes.
        /// </summary>
        /// <param name="elem">
        /// The element.
        /// </param>
        /// <returns>
        /// The element path <see cref="string"/>.
        /// </returns>
        private static string GetPath(XElement elem)
        {
            var name = elem.Attribute("name").Value;
            if (null == elem.Parent || elem.Name == "Pack")
            {
                return name;
            }

            return Path.Combine(GetPath(elem.Parent), name);
        }
    }
}
