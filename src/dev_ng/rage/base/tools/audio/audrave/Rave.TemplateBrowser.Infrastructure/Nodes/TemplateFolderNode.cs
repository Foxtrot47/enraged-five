// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TemplateFolderNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The template folder node.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.TemplateBrowser.Infrastructure.Nodes
{
    using System.Collections.Generic;
    using System.Windows.Forms;

    /// <summary>
    /// The template folder node.
    /// </summary>
    public class TemplateFolderNode : TemplateBaseNode
    {
        private bool canAddBank = false;

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateFolderNode"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public TemplateFolderNode(string name, bool canAddBank)
            : base(name)
        {
            this.ImageIndex = 0;
            this.canAddBank = canAddBank;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get bank nodes.
        /// </summary>
        /// <param name="bankNodes">
        /// The bank nodes.
        /// </param>
        public List<TemplateBankNode> GetBankNodes()
        {
            List<TemplateBankNode> bankNodes = new List<TemplateBankNode>();
            foreach (TreeNode node in this.Nodes)
            {
                if (node.GetType() == typeof(TemplateBankNode))
                {
                    bankNodes.Add(node as TemplateBankNode);
                }
                else if (node.GetType() == typeof(TemplateFolderNode))
                {
                    var folderNode = node as TemplateFolderNode;
                    bankNodes.AddRange(folderNode.GetBankNodes());
                }
            }
            return bankNodes;
        }

        public bool CanAddBank()
        {
            return canAddBank;
        }

        #endregion
    }
}