// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TemplateBankNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The template bank node.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.TemplateBrowser.Infrastructure.Nodes
{
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;

    /// <summary>
    /// The template bank node.
    /// </summary>
    public class TemplateBankNode : TemplateBaseNode
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateBankNode"/> class.
        /// </summary>
        /// <param name="bank">
        /// The bank.
        /// </param>
        public TemplateBankNode(ITemplateBank bank)
            : base(bank.Name)
        {
            this.Bank = bank;
            this.Bank.BankDeleted += this.OnBankDeleted;
            this.Bank.BankStatusChanged += this.OnBankStatusChanged;
            this.Update();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the bank.
        /// </summary>
        public ITemplateBank Bank { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The update.
        /// </summary>
        public void Update()
        {
            this.ImageIndex = this.Bank.IsCheckedOut ? 2 : 1;
            this.Nodes.Clear();
            this.CreateChildren();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create the children.
        /// </summary>
        private void CreateChildren()
        {
            foreach (var template in this.Bank.Templates)
            {
                this.Nodes.Add(new TemplateNode(template));
            }
        }

        /// <summary>
        /// The on bank deleted.
        /// </summary>
        /// <param name="deletedBank">
        /// The deleted bank.
        /// </param>
        private void OnBankDeleted(IXmlBank deletedBank)
        {
            deletedBank.BankDeleted -= this.OnBankDeleted;
            deletedBank.BankStatusChanged -= this.OnBankStatusChanged;
            this.Remove();
        }

        /// <summary>
        /// The on bank status changed.
        /// </summary>
        private void OnBankStatusChanged()
        {
            this.Update();
        }

        #endregion
    }
}