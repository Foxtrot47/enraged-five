// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TemplateBaseNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The template base node.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.TemplateBrowser.Infrastructure.Nodes
{
    using System.Windows.Forms;

    /// <summary>
    /// The template base node.
    /// </summary>
    public class TemplateBaseNode : TreeNode
    {
        #region Fields

        /// <summary>
        /// The name.
        /// </summary>
        private readonly string name;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateBaseNode"/> class.
        /// </summary>
        /// <param name="name">
        /// The template name.
        /// </param>
        public TemplateBaseNode(string name)
            : base(name)
        {
            this.name = name;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the path.
        /// </summary>
        public string Path
        {
            get
            {
                if (this.Parent == null)
                {
                    return this.name;
                }

                return ((TemplateBaseNode)this.Parent).Path + "\\" + this.name;
            }
        }

        #endregion
    }
}