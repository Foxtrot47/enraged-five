﻿// -----------------------------------------------------------------------
// <copyright file="ITemplateBrowser.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.TemplateBrowser.Infrastructure.Interfaces
{
    using System;
    using System.Collections.Generic;

    using Rave.Metadata.Infrastructure.Interfaces;
    using Rave.TemplateBrowser.Infrastructure.Nodes;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;

    /// <summary>
    /// Template browser interface.
    /// </summary>
    public interface ITemplateBrowser
    {
        #region Public Events

        /// <summary>
        /// The change list modified action.
        /// </summary>
        event Action ChangeListModified;

        /// <summary>
        /// The template deleted event.
        /// </summary>
        event Action<ITemplate> TemplateDeleted;

        /// <summary>
        /// The find object event.
        /// </summary>
        event Action<IObjectInstance> FindObject;

        /// <summary>
        /// The template selected event.
        /// </summary>
        event Action<ITemplate> TemplateSelected;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the metadata managers.
        /// </summary>
        IDictionary<string, IMetadataManager> MetadataManagers { get; }

        /// <summary>
        /// Gets the base path.
        /// </summary>
        string BasePath { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Add template bank nodes for a given bank.
        /// </summary>
        /// <param name="bank">
        /// The template bank.
        /// </param>
        /// <returns>
        /// The template bank node <see cref="TemplateBankNode"/>.
        /// </returns>
        TemplateBankNode AddNodesForBank(ITemplateBank bank);

        /// <summary>
        /// Create a new template bank.
        /// </summary>
        /// <param name="bankName">
        /// The bank name.
        /// </param>
        /// <returns>
        /// The new template bank <see cref="ITemplateBank"/>.
        /// </returns>
        ITemplateBank CreateNewBank(string bankName);

        /// <summary>
        /// Initialize the template browser.
        /// </summary>
        /// <param name="bankManager">
        /// The bankManager.
        /// </param>
        /// <param name="templatePath">
        /// The template path.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="typeDefinitions">
        /// The type definitions.
        /// </param>
        void Init(IXmlBankManager bankManager, string templatePath, string type, ITypeDefinitions typeDefinitions);

        /// <summary>
        /// Refresh the template browser tree.
        /// </summary>
        void RefreshTree();

        /// <summary>
        /// The find template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        void FindTemplate(ITemplate template);

        #endregion
    }
}
