// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TemplateNode.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The template node.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.TemplateBrowser.Infrastructure.Nodes
{
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;

    /// <summary>
    /// The template node.
    /// </summary>
    public class TemplateNode : TemplateBaseNode
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateNode"/> class.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        public TemplateNode(ITemplate template)
            : base(template.Name)
        {
            this.Template = template;
            this.ImageIndex = 3;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the template.
        /// </summary>
        public ITemplate Template { get; private set; }

        #endregion
    }
}