﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Controls;
using rage.ToolLib;
using Rave.Controls.WPF.UXTimer;
using System.Collections.Generic;

namespace Rave.Drive
{
	public class FileWatcher
	{
		public event Action<string[]> NewFileInDrive;
		private Thread _workerThread;
		private bool _shouldThreadBeRunning;
		private ConcurrentQueue<string> _processQue;
		private FileSystemWatcher _watcher;
		private HashSet<string> _pathsToProcess;

		public FileWatcher(DirectoryInfo root)
		{
			_pathsToProcess = new HashSet<string>();
			_processQue = new ConcurrentQueue<string>();
			this.StartWatcher(root);
		}

		private void OnChanged(object sender, FileSystemEventArgs e)
		{
			_processQue.Enqueue(e.FullPath);
		}


		private void WorkerThread()
		{
			bool started = true;
			while (_shouldThreadBeRunning)
			{

				while (_processQue.Count > 0)
				{
					started = true;
					string item;
					_processQue.TryDequeue(out item);
					if (item != null)
					{
						AddFilesToProcessingSet(item);
					}
					Thread.Sleep(5);
				}
				if (started && _pathsToProcess.All(HasBeenModified) && !_pathsToProcess.Any(IsFileLocked))
				{
					ProcessFiles();
					started = false;
				}

				if (!started) Thread.Sleep(450);
				
				Thread.Sleep(50);
			}
		}

		private bool HasBeenModified(string file)
		{
			return (DateTime.Now.Ticks - File.GetLastWriteTime(file).Ticks > 2500000);
		}

		private bool IsFileLocked(string file)
		{
			FileStream stream = null;
			FileInfo fileInfo = new FileInfo(file);
			try
			{
				fileInfo.IsReadOnly = false;
				stream = fileInfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
			}
			catch (Exception e)
			{
				if (e is UnauthorizedAccessException)
				{
					//the file is unavailable because it is:
					//still being written to
					//or being processed by another thread
					//or does not exist (has already been processed)
					return true;
				}
				else
				{
					UXTimer.Instance.AppendNewLine(string.Format("[Rave Drive] Error: {0}", e.Message));
				}
			}
			finally
			{
				if (stream != null)
					stream.Close();
			}

			//file is not locked
			return false;
		}

		private void AddFilesToProcessingSet(string path)
		{
			if (Directory.Exists(path))
			{
				foreach (string file in Directory.GetFiles(path))
				{
					AddFilesToProcessingSet(file);
				}
			}
			else
			{
				Console.WriteLine("{0} created.", path);
				string ext = Path.GetExtension(path);
				if (ext.Equals(".wav", StringComparison.CurrentCultureIgnoreCase) ||
					ext.Equals(".mid", StringComparison.CurrentCultureIgnoreCase))
				{
					_pathsToProcess.Add(path);
				}
			}
		}

		private void ProcessFiles()
		{
			if (_pathsToProcess.Count > 0)
			{
				NewFileInDrive(_pathsToProcess.ToArray<string>());
				_pathsToProcess.Clear();
			}
		}


		private void OnRenamed(object sender, RenamedEventArgs e)
		{
			string ext = Path.GetExtension(e.FullPath);
			if (string.IsNullOrEmpty(ext))
			{
				NewFileInDrive(new string[] { e.FullPath });
			}
		}

		private void StartWatcher(DirectoryInfo rootDir)
		{
			_watcher = new FileSystemWatcher();

			_watcher.Path = rootDir.FullName;
			_watcher.IncludeSubdirectories = true;
			_watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
								   | NotifyFilters.FileName | NotifyFilters.DirectoryName;
			_watcher.Created += new FileSystemEventHandler(OnChanged);
			_watcher.Error += WatcherOnError;
			_watcher.EnableRaisingEvents = true;
			_watcher.Disposed += WatcherDisposed;
			_shouldThreadBeRunning = true;
			_workerThread = new Thread(this.WorkerThread) { IsBackground = true };
			_workerThread.Start();

		}

		private void WatcherDisposed(object sender, EventArgs e)
		{
			_shouldThreadBeRunning = false;
		}

		private void WatcherOnError(object sender, ErrorEventArgs errorEventArgs)
		{
			UXTimer.Instance.AppendNewLine(string.Format("[Rave Drive] Error: {0}", errorEventArgs.GetException().Message));
		}
	}
}