﻿using System;
using System.Collections.Generic;
using System.IO;
using Rave.Controls.WPF.UXTimer;
using Rave.WaveBrowser.Infrastructure.Interfaces;
using System.Security.AccessControl;
using rage.ToolLib.Networking;
using Rave.Utils;
using System.Text.RegularExpressions;
using System.Linq;
using System.Windows.Media;

namespace Rave.Drive
{
	public class DriveWatcher : IDisposable
	{
		private readonly DirectoryInfo _rootDir;
		private FileWatcher _watcher;
		private readonly IWaveBrowser _waveBrowser;
		private readonly List<string[]> _failedList;

		public DriveWatcher(DirectoryInfo rootDir, IWaveBrowser waveBrowser)
		{
			this._rootDir = new DirectoryInfo(rootDir.FullName + "\\" + Regex.Replace(Configuration.ProjectName, @"\s+", "") + "\\");
			this._waveBrowser = waveBrowser;
			_failedList = new List<string[]>();
			WaveFolderReflector waveFolderReflector = new WaveFolderReflector();
			waveFolderReflector.CreateWavesFolderStructure(_rootDir);
			waveFolderReflector.ShareFolder();
			UXTimer.Instance.AppendNewLine(string.Format("[Rave Drive] Created folder structure under: {0}", _rootDir.FullName));
			_watcher = new FileWatcher(_rootDir);
			_watcher.NewFileInDrive += OnNewFilesInDirectory;
			this._waveBrowser.GetEditorTreeView().FolderCreated += CreateNewFolder;
			CheckForFiles();
		}

		public void Dispose()
		{

			_watcher = null;

			foreach (FileInfo fileInfo in this._rootDir.GetFiles("*.*", SearchOption.AllDirectories))
			{
				fileInfo.IsReadOnly = false;
				fileInfo.Delete();
			}
		}

		public void CheckForFiles()
		{
			string[] files = this._rootDir.GetFiles("*.wav", SearchOption.AllDirectories).Select(p => p.FullName).ToArray();
			if (files.Length > 0)
			{
				OnNewFilesInDirectory(files);
			}
		}

		private void CreateNewFolder(string folder)
		{
			string path = Path.Combine(_rootDir.FullName, folder);

			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
				DriveHelper.GrantFullAccess(path);
				
				UXTimer.Instance.AppendNewLine(string.Format("[Rave Drive] Created Folder: {0}", path));
			}
		}

		private void OnNewFilesInDirectory(string[] files)
		{
			if (!_failedList.Contains(files))
			{
				try
				{
					_waveBrowser.GetEditorTreeView().HandleDriveFiles(files, _rootDir.FullName);
					UXTimer.Instance.AppendNewLine(string.Format("[Rave Drive] Successfully imported : {0}", string.Join(", ", files)), Brushes.Green);
				}
				catch (Exception exception)
				{
					_failedList.Add(files);
					UXTimer.Instance.AppendNewLine(string.Format("[Rave Drive] Failed to import {0} due to error: {1}", string.Join(", ", files), exception.Message), Brushes.Red);
				}
			}
		}
	}
}