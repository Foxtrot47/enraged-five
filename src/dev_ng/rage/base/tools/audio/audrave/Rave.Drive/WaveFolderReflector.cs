﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Linq;
using Rave.Utils;
using System;
using rage.ToolLib.Networking;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Rave.Drive
{
	public class WaveFolderReflector
	{
		private const string BUILT_WAVES_DIR_NAME = "BuiltWaves";
		private const string BUILT_WAVES_PACK_LIST_FILENAME = "BuiltWaves_PACK_LIST.xml";
		private const string PENDING_WAVES_DIR_NAME = "PendingWaves";
		private const string WAVE_ELEMENT = "Wave";

		private DirectoryInfo _rootDirectory;

		public void BuiltWaves(rage.PlatformSetting platformSetting)
		{
			string packListPath = Path.Combine(platformSetting.BuildInfo, BUILT_WAVES_DIR_NAME, BUILT_WAVES_PACK_LIST_FILENAME);
			XDocument packListDoc = XDocument.Load(packListPath);

			foreach (var packFileElem in packListDoc.Descendants("PackFile"))
			{
				var packFilePath = Path.Combine(platformSetting.BuildInfo, BUILT_WAVES_DIR_NAME, packFileElem.Value);
				XDocument packDoc = XDocument.Load(packFilePath);
				CreateFolderStructure(packDoc);
			}
		}

		public void CreateWavesFolderStructure(DirectoryInfo rootDirectory)
		{
			this._rootDirectory = rootDirectory;
			if (!rootDirectory.Exists)
			{
				rootDirectory.Create();
			}
			
			foreach (rage.PlatformSetting platformSetting in Configuration.ActivePlatformSettings)
			{
				Task builtWavesTask = new Task(() => BuiltWaves(platformSetting));
				builtWavesTask.Start();

				Task pendingWavesTask = new Task(() => PendingWaves(platformSetting));
				pendingWavesTask.Start();

			}
		}
		public string GetPath(XElement element)
		{
			StringBuilder pathBuilder = new StringBuilder();
			foreach (var parent in element.AncestorsAndSelf().InDocumentOrder())
			{
				if (parent.Attribute("name") == null)
				{
					continue;
				}

				// Don't want backslash at start of path
				if (!string.IsNullOrEmpty(pathBuilder.ToString()))
				{
					pathBuilder.Append("\\");
				}

				pathBuilder.Append(parent.Attribute("name").Value);
			}
			return pathBuilder.ToString().ToUpper();
		}

		public void PendingWaves(rage.PlatformSetting platformSetting)
		{
			string packListPath = Path.Combine(platformSetting.BuildInfo, PENDING_WAVES_DIR_NAME);

			foreach (string file in Directory.GetFiles(Path.Combine(platformSetting.BuildInfo, PENDING_WAVES_DIR_NAME), "*.xml", SearchOption.TopDirectoryOnly))
			{
				XDocument packDoc = XDocument.Load(file);
				CreateFolderStructure(packDoc);
			}
		}

		private void CreateFolderStructure(XDocument doc)
		{
			IEnumerable<XElement> waveNodes = doc.Descendants(WAVE_ELEMENT);
			foreach (XElement element in waveNodes)
			{
				string folder = Path.Combine(_rootDirectory.FullName, Path.GetDirectoryName(GetPath(element)));
				if (!Directory.Exists(folder))
				{
					Directory.CreateDirectory(folder);
					DriveHelper.GrantFullAccess(folder);
				}
			}
		}


		public void ShareFolder()
		{
			string folder = _rootDirectory.FullName;
			if (!Directory.Exists(folder))
			{
				return;
			}
			DriveHelper.GrantFullAccess(folder);
			IList<WindowsShare> winshare = WindowsShare.GetAllShares();
			string shareName = "RaveDrive" + Regex.Replace(Configuration.ProjectName, @"\s+", "");
			if (WindowsShare.GetShareByName(shareName) == null)
			{
				WindowsShare.MethodStatus shareResult = WindowsShare.Create(folder.TrimEnd('\\'), shareName, WindowsShare.ShareType.DiskDrive, 3, "", "");
				if (shareResult == WindowsShare.MethodStatus.Success)
				{
					WindowsShare sharedFolder = WindowsShare.GetShareByName(shareName);
					sharedFolder.SetPermission(Environment.UserDomainName, Environment.UserName, WindowsShare.AccessMaskTypes.FullControl);
				}
			}
		}
	}
}