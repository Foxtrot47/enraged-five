﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using SHDocVw;

namespace Rave.Drive
{
	public class DriveHelper
	{
		public static void GrantFullAccess(string fullPath)
		{


			if (Directory.Exists(fullPath))
			{
				DirectoryInfo dInfo = new DirectoryInfo(fullPath);
				DirectorySecurity dSecurity = dInfo.GetAccessControl();
				dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
				dInfo.SetAccessControl(dSecurity);
			}
			if (File.Exists(fullPath))
			{
				FileInfo fInfo = new FileInfo(fullPath);
				FileSecurity fSecurity = fInfo.GetAccessControl();
				fSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
				fInfo.SetAccessControl(fSecurity);
			}

		}

		public static void CloseExplorerWindows(string dir)
		{
			ShellWindows _shellWindows = new SHDocVw.ShellWindows();
			string processType;

			foreach (InternetExplorer ie in _shellWindows)
			{
				//this parses the name of the process
				processType = Path.GetFileNameWithoutExtension(ie.FullName).ToLower();

				//this could also be used for IE windows with processType of "iexplore"
				if (processType.Equals("explorer") && ie.LocationURL.Contains(dir.Replace('\\', '/')))
				{
					ie.Quit();
				}
			}
		}
	}
}
