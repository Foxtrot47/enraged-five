﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rave.HierarchyBrowser.CategoriesBrowser
{
    public class CategoryDiff
    {
        public CategoryDiff(string propertyName, string valueInChild, string valueInParent)
        {
            PropertyName = propertyName;
            ValueInChild = valueInChild;
            ValueInParent = valueInParent;
        }

        public string PropertyName { get; set; }
        public string ValueInChild { get; set; }
        public string ValueInParent { get; set; }
    }
}
