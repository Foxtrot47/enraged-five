// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ctrlSoundHeirarchyViewer.cs" company="Rockstar North">
//
// </copyright>
// <summary>
//   The hierarchy viewer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Rave.HierarchyBrowser.CategoriesBrowser;
using Rave.HierarchyBrowser.Infrastructure.Annotations;
using Rave.RemoteControl.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
using Rockstar.AssetManager.Infrastructure.Data;

namespace Rave.HierarchyBrowser
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Xml;
    using rage;
    using rage.ToolLib;
    using Rave.Controls.Forms;
    using Rave.Controls.Forms.DoubleBufferedTreeView;
    using Rave.Controls.Forms.Popups;
    using Rave.HierarchyBrowser.Infrastructure.Enums;
    using Rave.HierarchyBrowser.Infrastructure.Interfaces;
    using Rave.HierarchyBrowser.Infrastructure.Nodes;
    using Rave.Instance;
    using Rave.MuteSoloManager;
    using Rave.MuteSoloManager.Model;
    using Rave.ObjectBrowser.Infrastructure.Nodes;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;
    using Rave.Types.Infrastructure.Structs;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Nodes;

    /// <summary>
    /// Summary description for ctrlSoundHierarchyViewer.
    /// </summary>
    public class HierarchyBrowser : DockableWFControl, IHierarchyBrowser
    {
        #region Fields

        /// <summary>
        /// The minimum refresh rate.
        /// </summary>
        private const int MinRefreshRate = 1000;

        /// <summary>
        /// The m_history.
        /// </summary>
        private readonly List<HistoryObject> m_history;

        /// <summary>
        /// The visible live properties.
        /// </summary>
        private readonly ISet<string> visibleLiveProperties;

        /// <summary>
        /// The update live properties.
        /// </summary>
        private bool updateLiveProperties;

        /// <summary>
        /// The bt collapse.
        /// </summary>
        private ToolBarButton btCollapse;

        /// <summary>
        /// The btn back.
        /// </summary>
        private ToolBarButton btnBack;

        /// <summary>
        /// The btn expand all.
        /// </summary>
        private ToolBarButton btnExpandAll;

        /// <summary>
        /// The btn forward.
        /// </summary>
        private ToolBarButton btnForward;

        /// <summary>
        /// The btn set snapback.
        /// </summary>
        private ToolBarButton btnSetSnapback;

        /// <summary>
        /// The btn snap back.
        /// </summary>
        private ToolBarButton btnSnapBack;

        /// <summary>
        /// The components.
        /// </summary>
        private IContainer components;

        /// <summary>
        /// The m_ image list.
        /// </summary>
        private ImageList m_ImageList;

        /// <summary>
        /// The m_ sound image list.
        /// </summary>
        private ImageList m_ObjectImageList;

        /// <summary>
        /// The m_collapse.
        /// </summary>
        private bool m_collapse;

        /// <summary>
        /// The m_current history index.
        /// </summary>
        private int m_currentHistoryIndex;

        /// <summary>
        /// The m_details panel.
        /// </summary>
        private Panel m_detailsPanel;

        /// <summary>
        /// The m_external editors.
        /// </summary>
        private IList<IRAVEEditorPlugin> m_externalEditors = new List<IRAVEEditorPlugin>();

        /// <summary>
        /// The m_in drag mode.
        /// </summary>
        private bool m_inDragMode;

        /// <summary>
        /// The m_lbl sound name.
        /// </summary>
        private Label m_lblObjectName;

        /// <summary>
        /// The m_lbl sound type.
        /// </summary>
        private Label m_lblObjectType;

        /// <summary>
        /// The m_menu.
        /// </summary>
        private ContextMenuStrip m_menu;

        /// <summary>
        /// The current object instance/template.
        /// </summary>
        private object currentObject;

        /// <summary>
        /// The m_snap back object look up.
        /// </summary>
        private IDictionary<string, IObjectInstance> m_snapBackObjectLookUp;

        /// <summary>
        /// The m_snapback sound.
        /// </summary>
        private string m_snapbackObject;

        /// <summary>
        /// The m_sound type icon.
        /// </summary>
        private PictureBox m_objectTypeIcon;

        /// <summary>
        /// The m_sound type icon table.
        /// </summary>
        private Hashtable typeIconTable;

        /// <summary>
        /// The m_title.
        /// </summary>
        private string m_title;

        /// <summary>
        /// The m_toolbar.
        /// </summary>
        private ToolBar m_toolbar;

        /// <summary>
        /// The m_tree view.
        /// </summary>
        private DoubleBufferedTreeView m_treeView;

        /// <summary>
        /// The mnu add child.
        /// </summary>
        private ToolStripMenuItem mnuAddChild;

        /// <summary>
        /// The mnu check out.
        /// </summary>
        private ToolStripMenuItem mnuCheckOut;

        /// <summary>
        /// The mnu delete.
        /// </summary>
        private ToolStripMenuItem mnuDelete;

        /// <summary>
        /// The mnu find in browser.
        /// </summary>
        private ToolStripMenuItem mnuFindInBrowser;

        /// <summary>
        /// The mnu find template in browser.
        /// </summary>
        private ToolStripMenuItem mnuFindTemplateInBrowser;

        /// <summary>
        /// The mnu insert wrapper.
        /// </summary>
        private ToolStripMenuItem mnuInsertWrapper;

        /// <summary>
        /// The mnu local check out.
        /// </summary>
        private ToolStripMenuItem mnuLocalCheckOut;

        /// <summary>
        /// The mnu make local.
        /// </summary>
        private ToolStripMenuItem mnuMakeLocal;

        /// <summary>
        /// The mnu move down.
        /// </summary>
        private ToolStripMenuItem mnuMoveDown;

        /// <summary>
        /// The mnu move up.
        /// </summary>
        private ToolStripMenuItem mnuMoveUp;

        /// <summary>
        /// The mnu plugin.
        /// </summary>
        private ToolStripMenuItem mnuPlugin;

        /// <summary>
        /// The mnu remove ref.
        /// </summary>
        private ToolStripMenuItem mnuRemoveRef;

        /// <summary>
        /// The mnu rename.
        /// </summary>
        private ToolStripMenuItem mnuRename;

        /// <summary>
        /// The mnu property selector.
        /// </summary>
        private ToolStripMenuItem mnuPropertySelector;

        /// <summary>
        /// The mnu live property selector.
        /// </summary>
        private ToolStripMenuItem mnuLivePropertySelector;

        /// <summary>
        /// The all menu item.
        /// </summary>
        private ToolStripMenuItem mnuAllProperties;

        /// <summary>
        /// The mnu expand all.
        /// </summary>
        private ToolStripMenuItem mnuExpandAll;

        /// <summary>
        /// The mnu expand all live properties.
        /// </summary>
        private ToolStripMenuItem mnuExpandAllLiveProperties;

        private ToolStripMenuItem mnuMute;

        private ToolStripMenuItem mnuSolo;

        private TreeNode _expandableNode = new TreeNode();

        /// <summary>
        /// The annotations root.
        /// </summary>
        private ObjectAnnotationNode annotationsRoot;

        private bool _autoUpdate = true;

        public bool AutoUpdate
        {
            get { return _autoUpdate; }
            set
            {
                _autoUpdate = value;
            }
        }


        private string _visibleProperty;

        /// <summary>
        /// The visible property.
        /// </summary>
        private string VisibleProperty
        {
            get
            {
                if (string.IsNullOrEmpty(_visibleProperty))
                {
                    m_treeView.DrawMode = TreeViewDrawMode.Normal;
                }
                return _visibleProperty;
            }
            set
            {
                _visibleProperty = value;
                if (string.IsNullOrEmpty(_visibleProperty))
                {
                    m_treeView.DrawMode = TreeViewDrawMode.Normal;
                }
                else
                {
                    m_treeView.DrawMode = TreeViewDrawMode.OwnerDrawText;
                }
            }
        }

        /// <summary>
        /// The categories message listener.
        /// </summary>
        private ICategoriesMessageListener categoriesMessageListener;

        /// <summary>
        /// The last refresh time.
        /// </summary>
        private DateTime lastRefreshTime;

        /// <summary>
        /// Object instance refresh pending flag.
        /// </summary>
        private bool objectInstanceRefreshPending;

        private bool isDisposing = false;
        private ToolBarButton btnAutoUpdate;

        private bool _isCategoryTop = false;

        #endregion Fields

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HierarchyBrowser" /> class.
        /// </summary>
        public HierarchyBrowser()
        {
            // This call is required by the Windows.Forms Form Designer.
            this.InitializeComponent();
            this.m_history = new List<HistoryObject>();
            this.Mode = HierarchyBrowserModes.Default;
            this.visibleLiveProperties = new HashSet<string>();
            MuteSoloManager.Instance.MutedObjects.CollectionChanged += MuteSoloObjects_CollectionChanged;
            MuteSoloManager.Instance.SoloedObjects.CollectionChanged += MuteSoloObjects_CollectionChanged;
        }

        private void MuteSoloObjects_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            List<ObjectNode> objectNodeList = new List<ObjectNode>();
            IEnumerable<MuteSoloObject> oldItems = new List<MuteSoloObject>();

            if (e.OldItems != null)
            {
                oldItems = e.OldItems.Cast<MuteSoloObject>().Except<MuteSoloObject>(e.NewItems.Cast<MuteSoloObject>());

                foreach (MuteSoloObject msObject in oldItems)
                {
                    FindObjectNodeWithMuteSolo((MuteSoloObject)msObject, this.m_treeView.Nodes, objectNodeList);
                }
            }

            if (e.NewItems != null)
            {
                IEnumerable<MuteSoloObject> newItems;
                if (e.OldItems != null)
                {
                    newItems = e.NewItems.Cast<MuteSoloObject>().Except<MuteSoloObject>(e.OldItems.Cast<MuteSoloObject>());
                }
                else
                {
                    newItems = e.NewItems.Cast<MuteSoloObject>();
                }

                foreach (MuteSoloObject msObject in newItems)
                {
                    if (msObject is MuteSoloObject)
                    {
                        FindObjectNodeWithMuteSolo((MuteSoloObject)msObject, this.m_treeView.Nodes, objectNodeList);
                    }
                }
            }
            foreach (ObjectNode sn in objectNodeList)
            {
                UpdateFont(sn);
            }
        }

        #endregion Constructors and Destructors

        #region Public Events

        /// <summary>
        /// The audition sound.
        /// </summary>
        public event Action<IObjectInstance> AuditionObject;

        /// <summary>
        /// The change list modified.
        /// </summary>
        public event Action ChangeListModified;

        /// <summary>
        /// The find object.
        /// </summary>
        public event Action<IObjectInstance> FindObject;

        /// <summary>
        /// The find template.
        /// </summary>
        public event Action<ITemplateProxyObject> FindTemplate;

        /// <summary>
        /// The sound selected.
        /// </summary>
        public event Action<HierarchyNode> NodeSelected;

        /// <summary>
        /// The stop audition sound.
        /// </summary>
        public event Action<IObjectInstance> StopAuditionObject;

        #endregion Public Events

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether can audition.
        /// </summary>
        public bool CanAudition { get; set; }

        public bool AlphabeticalSorted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can check in out.
        /// </summary>
        public bool CanCheckInOut { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can delete.
        /// </summary>
        public bool CanDelete { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can find in browser.
        /// </summary>
        public bool CanFindInBrowser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can insert wrapper.
        /// </summary>
        public bool CanInsertWrapper { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can local check out.
        /// </summary>
        public bool CanLocalCheckOut { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can make local.
        /// </summary>
        public bool CanMakeLocal { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can navigate.
        /// </summary>
        public bool CanNavigate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether can remove reference.
        /// </summary>
        public bool CanRemoveReference { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether should append type.
        /// </summary>
        public bool ShouldAppendType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether should expand all.
        /// </summary>
        public bool ShouldExpandAll { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sort tree.
        /// </summary>
        public bool SortTree { get; set; }

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        public HierarchyBrowserModes Mode { get; set; }

        private PropertyViewMode _propertyViewMode;

        /// <summary>
        /// Gets the property view mode.
        /// </summary>
        public PropertyViewMode PropertyViewMode
        {
            get
            {
                return _propertyViewMode;
            }
            private set
            {
                if (value == PropertyViewMode.Inline || value == PropertyViewMode.Nested)
                {
                    this.m_treeView.DrawMode = TreeViewDrawMode.OwnerDrawText;
                }
                m_treeView.Update();
                _propertyViewMode = value;
            }
        }

        #endregion Public Properties

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        public void DeleteObjectInstance(IObjectInstance objectInstance)
        {
            if (this.m_treeView.Nodes.Count != 0)
            {
                DeleteObjectFromTree(this.m_treeView.Nodes[0], objectInstance);
            }
        }

        /// <summary>
        /// Delete a template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        public void DeleteTemplate(ITemplate template)
        {
            if (this.m_treeView.Nodes.Count != 0)
            {
                DeleteTemplateFromTree(this.m_treeView.Nodes[0], template);
            }
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        public void Init(string title)
        {
            this.Text = this.m_title = title;
            this.m_treeView.Sorted = this.SortTree;
            this.LoadIcons();
            this.Disposed += HierarchyBrowser_Disposed;
        }

        private void HierarchyBrowser_Disposed(object sender, EventArgs e)
        {
            this.isDisposing = true;
            if (categoriesMessageListener != null)
            {
                categoriesMessageListener.HeaderReceived -= this.CategoryHeaderReceived;
                categoriesMessageListener.UpdateReceived -= this.CategoryUpdateReceived;
            }
        }

        /// <summary>
        /// The init.
        /// </summary>
        /// <param name="externalEditors">
        /// The external editors.
        /// </param>
        /// <param name="title">
        /// The title.
        /// </param>
        public void Init(IList<IRAVEEditorPlugin> externalEditors, string title)
        {
            this.m_externalEditors = externalEditors;
            this.Init(title);
        }

        /// <summary>
        /// The notify audition.
        /// </summary>
        public void NotifyAudition()
        {
            if (this.CanAudition && this.m_treeView.Nodes[0] != null)
            {
                ObjectNode objectNode = (this.m_treeView.SelectedNode ?? this.m_treeView.Nodes[0]) as ObjectNode;
                if (null != objectNode)
                {
                    this.AuditionObject.Raise(objectNode.ObjectInstance);
                }
            }
        }

        /// <summary>
        /// The notify audition stop.
        /// </summary>
        public void NotifyAuditionStop()
        {
            if (this.m_treeView.Nodes[0] != null)
            {
                ObjectNode objectNode = (this.m_treeView.SelectedNode ?? this.m_treeView.Nodes[0]) as ObjectNode;
                if (null != objectNode)
                {
                    this.StopAuditionObject.Raise(objectNode.ObjectInstance);
                }
            }
        }

        public void ShowNode(string path)
        {
            //int previouslySelectedNodeIndex = this.m_treeView.SelectedNode == null ? 0 : m_treeView.SelectedNode.Index;
            selectTreeNode(path, 0, this.m_treeView.Nodes);
        }

        /// <summary>
        /// The show hierarchy.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="updateHistory">
        /// The update History flag.
        /// </param>
        public void ShowHierarchy(object parent, bool updateHistory = true)
        {

            _isCategoryTop = false;

            if (null == parent)
            {
                if (this.currentObject != null)
                {
                    IObjectInstance currObjectInstance = this.currentObject as IObjectInstance;
                    if (null != currObjectInstance)
                    {
                        currObjectInstance.Bank.BankStatusChanged -= this.UpdateHierarchy;
                        currObjectInstance.ReferencesChanged -= this.UpdateHierarchy;
                    }
                }

                this.m_treeView.DoubleClick -= this.TreeView_DoubleClick;
                this.m_treeView.AllowDrop = false;

                this.m_treeView.Nodes.Clear();
                return;
            }

            IObjectInstance objectInstance = parent as IObjectInstance;
            ITemplate template = parent as ITemplate;



            this.mnuPropertySelector.DropDownItems.Clear();

            if (null != objectInstance)
            {
                this.m_treeView.BeforeExpand -= TreeView_BeforeExpand;
                _isCategoryTop = objectInstance.TypeName.ToUpper().Equals("CATEGORY");
                AlphabeticalSorted = Configuration.AlphabeticalSortHierarchyTpes.Contains(objectInstance.TypeName);
                this.ShowObjectInstanceHierarchy(objectInstance, updateHistory);
            }
            else if (null != template)
            {
                this.m_treeView.BeforeExpand -= TreeView_BeforeExpand;
                AlphabeticalSorted = Configuration.AlphabeticalSortHierarchyTpes.Contains(template.TypeName);
                this.ShowTemplateHierarchy(template, updateHistory);
            }
            if (this.Mode != HierarchyBrowserModes.Categories)
            {
                ApplyAlphabeticalSort(AlphabeticalSorted);
            }
            if (this.Mode == HierarchyBrowserModes.Categories)
            {
                // Link up to categories message listener
                foreach (IRemoteMessageListener listener in RaveInstance.RemoteControl.Listeners)
                {
                    ICategoriesMessageListener catListener = listener as ICategoriesMessageListener;
                    if (null != catListener)
                    {
                        this.categoriesMessageListener = catListener;
                        //make sure event handler is registered only once
                        catListener.HeaderReceived -= this.CategoryHeaderReceived;
                        catListener.UpdateReceived -= this.CategoryUpdateReceived;

                        catListener.HeaderReceived += this.CategoryHeaderReceived;
                        catListener.UpdateReceived += this.CategoryUpdateReceived;
                        break;
                    }
                }
                if (UserPreferences.UserPreferences.Instance.CategoriesCollapsed)
                {
                    this.m_treeView.CollapseAll();
                }

                UserPreferences.UserPreferences.Instance.PropertyChanged += (p, r) =>
                {
                    if (r.PropertyName == "CategoriesCollapsed")
                    {
                        if (UserPreferences.UserPreferences.Instance.CategoriesCollapsed)
                        {
                            this.m_treeView.CollapseAll();
                        }
                        else
                        {
                            this.m_treeView.ExpandAll();
                        }
                    }
                };
            }


        }

        /// <summary>
        /// Receive category data header.
        /// </summary>
        private void CategoryHeaderReceived()
        {
            if (this.IsDisposed || this.Disposing || this.isDisposing) return;

            if (this.InvokeRequired)
            {
                if (!this.Disposing && !this.isDisposing && this.IsHandleCreated)
                {
                    try
                    {
                        this.Invoke(new Action(this.CategoryHeaderReceived));
                    }
                    catch (Exception e)
                    {
                        //Curse microsoft for not having included TryInvoke() change this to a dynamic object in future
                        ErrorManager.WriteToLog("Error", e.Message, e.StackTrace);
                    }
                }
            }
            else
            {
                this.InitLivePropertySelector();
            }
        }

        /// <summary>
        /// Receive category data update.
        /// </summary>
        private void CategoryUpdateReceived()
        {
            if (this.IsDisposed || this.Disposing)
            {
                return;
            }

            if (this.InvokeRequired)
            {
                if (!this.Disposing && !this.isDisposing && this.IsHandleCreated)
                {
                    try
                    {
                        this.Invoke(new Action(this.CategoryUpdateReceived));
                    }
                    catch (Exception e)
                    {
                        ErrorManager.WriteToLog("Error", e.Message, e.StackTrace);
                    }
                }
            }
            else
            {
                if (null != this.categoriesMessageListener && null != this.annotationsRoot)
                {
                    IDictionary<string, IDictionary<string, float>> categoryProperties = this.categoriesMessageListener.CategoryProperties;

                    Task task = new Task(() => this.UpdateCategoryAnnotations(this.m_treeView.Nodes, categoryProperties));
                    task.Start();
                }

                DateTime now = DateTime.Now;
                if (now > this.lastRefreshTime.AddMilliseconds(MinRefreshRate) && this.visibleLiveProperties.Count > 0)
                {
                    this.m_treeView.BeginUpdate();
                    this.lastRefreshTime = DateTime.Now;
                    this.m_treeView.EndUpdate();
                }
            }
        }

        /// <summary>
        /// Update the category annotations.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <param name="properties">
        /// The properties.
        /// </param>
        private void UpdateCategoryAnnotations(
            TreeNodeCollection nodes, IDictionary<string, IDictionary<string, float>> properties)
        {
            if (this.visibleLiveProperties.Any())
            {
                this.updateLiveProperties = true;
            }

            if (!this.updateLiveProperties)
            {
                return;
            }

            this.StartUpdateCategoryAnnotations(nodes, properties);

            this.updateLiveProperties = this.visibleLiveProperties.Any();
        }

        /// <summary>
        /// The update category annotations helper.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <param name="properties">
        /// The properties.
        /// </param>
        private void StartUpdateCategoryAnnotations(
            TreeNodeCollection nodes, IDictionary<string, IDictionary<string, float>> properties)
        {
            if (this.PropertyViewMode == PropertyViewMode.Nested)
            {
                this.UpdateNestedCategoryAnnotations(nodes, properties);
            }
            else
            {
                this.UpdateInlineCategoryAnnotations(nodes, properties);
            }
        }

        /// <summary>
        /// The update nested category annotations.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <param name="properties">
        /// The properties.
        /// </param>
        private void UpdateNestedCategoryAnnotations(IEnumerable nodes, IDictionary<string, IDictionary<string, float>> properties)
        {
            foreach (TreeNode node in nodes)
            {
                if (!node.IsExpanded)
                {
                    continue;
                }

                ObjectNode objectNode = node as ObjectNode;
                if (null != objectNode)
                {
                    if (properties.ContainsKey(objectNode.ObjectInstance.Name))
                    {
                        IDictionary<string, float> objProperties = properties[objectNode.ObjectInstance.Name];
                        ITypedObjectInstance typedObjectInstance = objectNode.ObjectInstance as ITypedObjectInstance;

                        if (null != typedObjectInstance)
                        {
                            this.UpdateCategoryAnnotationsHelper(objectNode, typedObjectInstance, objProperties);
                        }
                    }
                }

                AnnotationNode annotationNode = node as AnnotationNode;

                if (null == annotationNode)
                {
                    this.StartUpdateCategoryAnnotations(node.Nodes, properties);
                }
            }
        }

        /// <summary>
        /// The update inline category annotations.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <param name="properties">
        /// The properties.
        /// </param>
        private void UpdateInlineCategoryAnnotations(TreeNodeCollection nodes, IDictionary<string, IDictionary<string, float>> properties)
        {
            this.DeleteAnnotationGroupNodes(nodes);

            foreach (TreeNode node in nodes)
            {
                ObjectNode objectNode = node as ObjectNode;
                if (null == objectNode)
                {
                    continue;
                }

                if (!properties.ContainsKey(objectNode.ObjectInstance.Name))
                {
                    continue;
                }

                IDictionary<string, float> objProperties = properties[objectNode.ObjectInstance.Name];

                foreach (KeyValuePair<string, float> property in objProperties)
                {
                    objectNode.AnnotationNode.SetGameAnnotation(property.Key, property.Value.ToString(CultureInfo.InvariantCulture), false);
                }

                this.UpdateInlineCategoryAnnotations(node.Nodes, properties);
            }
        }

        /// <summary>
        /// The update category annotations helper.
        /// </summary>
        /// <param name="objectNode">
        /// The object node.
        /// </param>
        /// <param name="objInstance">
        /// The object instance.
        /// </param>
        /// <param name="objProperties">
        /// The object properties.
        /// </param>
        private void UpdateCategoryAnnotationsHelper(
            ObjectNode objectNode, ITypedObjectInstance objInstance, IDictionary<string, float> objProperties)
        {
            foreach (IFieldDefinition field in objInstance.TypeDefinition.Fields)
            {
                string fieldName = field.Name;
                string fieldDefaultValue = field.DefaultValue;

                if (!objProperties.ContainsKey(fieldName))
                {
                    continue;
                }

                string propertyValue = objProperties[fieldName].ToString(CultureInfo.InvariantCulture);
                bool isPropertyVisible = this.visibleLiveProperties.Contains(fieldName);

                AnnotationGroupNode groupNode = null;
                foreach (object node in objectNode.Nodes)
                {
                    groupNode = node as AnnotationGroupNode;
                    if (null != groupNode)
                    {
                        break;
                    }
                }

                if (null != groupNode)
                {
                    if (!groupNode.IsExpanded)
                    {
                        continue;
                    }
                }
                else
                {
                    groupNode = new AnnotationGroupNode { Text = "Live Parameters" };
                    objectNode.Nodes.Insert(0, groupNode);
                }

                bool nodeFound = false;
                foreach (TreeNode childNode in groupNode.Nodes)
                {
                    AnnotationNode childAnnotationNode = childNode as AnnotationNode;
                    if (null != childAnnotationNode && childAnnotationNode.Property == fieldName)
                    {
                        if (isPropertyVisible)
                        {
                            childAnnotationNode.Value = propertyValue;
                            childAnnotationNode.IsDefaultValue = propertyValue.Equals(fieldDefaultValue);
                        }
                        else
                        {
                            childNode.Remove();
                        }

                        nodeFound = true;
                        break;
                    }
                }

                if (!nodeFound && isPropertyVisible)
                {
                    AnnotationNode childAnnotationNode = new AnnotationNode
                    {
                        Property = fieldName,
                        Value = propertyValue,
                        IsDefaultValue = fieldDefaultValue.Equals(propertyValue)
                    };

                    groupNode.Nodes.Add(childAnnotationNode);
                }
            }
        }

        /// <summary>
        /// Clears the hierarchy tree view while updates are being made elsewhere.
        /// </summary>
        public void BeginUpdate()
        {
            this.ShowHierarchy(null, false);
        }

        /// <summary>
        /// Reloads the hierarchy tree view once updates completed elsewhere.
        /// </summary>
        public void EndUpdate()
        {
            this.ShowHierarchy(this.currentObject, false);
        }

        /// <summary>
        /// Delete all annotation group nodes.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        private void DeleteAnnotationGroupNodes(IEnumerable nodes)
        {
            List<AnnotationGroupNode> nodesToDelete = new List<AnnotationGroupNode>();

            foreach (TreeNode node in nodes)
            {
                AnnotationGroupNode groupNode = node as AnnotationGroupNode;
                if (null != groupNode)
                {
                    nodesToDelete.Add(groupNode);
                }
                else
                {
                    this.DeleteAnnotationGroupNodes(node.Nodes);
                }
            }

            foreach (AnnotationGroupNode node in nodesToDelete)
            {
                node.Remove();
            }
        }

        /// <summary>
        /// The show node.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        public void ShowNode(IObjectInstance obj)
        {
            TreeNode node = FindNodeWithObject(obj, this.m_treeView.Nodes);
            if (node != null)
            {
                node.EnsureVisible();
                this.m_treeView.SelectedNode = node;
            }
        }

        public void ShowNode(ITemplate template)
        {
            TreeNode node = FindNodeWithTemplate(template, this.m_treeView.Nodes);
            if (node != null)
            {
                node.EnsureVisible();
                this.m_treeView.SelectedNode = node;
            }
        }

        /// <summary>
        /// The try to rename.
        /// </summary>
        public void TryToRename()
        {
            ObjectNode shn = this.m_treeView.SelectedNode as ObjectNode;
            if (shn == null
                || ErrorManager.HandleQuestion(
                    "Warning: although RAVE will automatically fix any references to this object from other objects any code that references this object directly will have to be updated.\n\nAre you sure you want to rename this object?",
                    MessageBoxButtons.YesNo,
                    DialogResult.Yes) != DialogResult.Yes)
            {
                return;
            }

            frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "New object name for " + shn.ObjectInstance.Name);
            if (input.HasBeenCancelled) return;
            string newName = input.data;

            while (shn.ObjectInstance.ObjectLookup.ContainsKey(newName))
            {
                input = frmSimpleInput.GetInput(this.ParentForm, "Name taken, new name", newName);
                if (input.HasBeenCancelled) return;
                newName = input.data;
            }

            if (ErrorManager.HandleQuestion(
                    "Are you sure you want to rename this object?", MessageBoxButtons.YesNo, DialogResult.Yes)
                == DialogResult.Yes)
            {
                try
                {
                   
                    shn.ObjectInstance.Rename(newName);
                    MuteSoloManager.Instance.Rename(shn.ObjectName, shn.ObjectInstance.TypeName, newName);
                    shn.ObjectName = newName;

                    this.NodeSelected.Raise(shn);
                    UpdateHierarchy();
                }
                catch (Exception ex)
                {
                    ErrorManager.HandleError(ex);
                }
            }
        }

        #endregion Public Methods and Operators

        #region Methods

        /// <summary>
        /// The collapse names.
        /// </summary>
        /// <param name="collapse">
        /// The collapse.
        /// </param>
        internal void CollapseNames(bool collapse)
        {
            this.m_collapse = collapse;
            this.DoCollapse(this.m_treeView.Nodes);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                HierarchyBrowser_Disposed(this, null);
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// The can add children.
        /// </summary>
        /// <param name="td">
        /// The td.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool CanAddChildren(ITypeDefinition td)
        {
            if (td != null)
            {
                return td.ContainsObjectRef;
            }

            return false;
        }

        /// <summary>
        /// The delete object from tree.
        /// </summary>
        /// <param name="treeNode">
        /// The tree node.
        /// </param>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        private static void DeleteObjectFromTree(TreeNode treeNode, IObjectInstance objectInstance)
        {
            ObjectNode objectNode = treeNode as ObjectNode;
            if (null == objectNode)
            {
                return;
            }

            if (objectNode.ObjectInstance == objectInstance)
            {
                treeNode.Remove();
            }
            else
            {
                foreach (TreeNode n in treeNode.Nodes)
                {
                    DeleteObjectFromTree(n, objectInstance);
                }
            }
        }

        /// <summary>
        /// The delete template from tree.
        /// </summary>
        /// <param name="treeNode">
        /// The tree node.
        /// </param>
        /// <param name="template">
        /// The template.
        /// </param>
        private static void DeleteTemplateFromTree(TreeNode treeNode, ITemplate template)
        {
            TemplateNode templateNode = treeNode as TemplateNode;
            if (null == templateNode)
            {
                return;
            }

            if (templateNode.ProxyObject == template.Parent)
            {
                treeNode.Remove();
            }
            else
            {
                foreach (TreeNode n in treeNode.Nodes)
                {
                    DeleteTemplateFromTree(n, template);
                }
            }
        }

        /// <summary>
        /// The expand tree.
        /// </summary>
        /// <param name="nodeState">
        /// The node state.
        /// </param>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private static void ExpandTree(XmlNode nodeState, TreeNodeCollection treeNodes)
        {
            foreach (XmlNode state in nodeState.ChildNodes)
            {
                foreach (TreeNode treeNode in treeNodes)
                {
                    string name = Regex.Replace(treeNode.Text, @"[^\w-]+", string.Empty);
                    if (state.Name != name)
                    {
                        continue;
                    }

                    if (state.HasChildNodes)
                    {
                        treeNode.Expand();

                        ExpandTree(state, treeNode.Nodes);
                    }

                    break;
                }
            }
        }

        /// <summary>
        /// The find node with object.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <returns>
        /// The <see cref="TreeNode"/>.
        /// </returns>
        private static TreeNode FindNodeWithObject(IObjectInstance obj, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                ObjectNode objectNode = node as ObjectNode;
                if (objectNode == null)
                {
                    continue;
                }

                if (objectNode.ObjectInstance == obj)
                {
                    return node;
                }

                TreeNode ret = FindNodeWithObject(obj, node.Nodes);
                if (ret != null)
                {
                    return ret;
                }
            }

            return null;
        }

        private static void FindObjectNodeWithMuteSolo(MuteSoloObject obj, TreeNodeCollection nodes, List<ObjectNode> list)
        {
            foreach (TreeNode node in nodes)
            {
                ObjectNode objectNode = node as ObjectNode;
                if (objectNode == null || objectNode.ObjectInstance == null)
                {
                    continue;
                }

                if (objectNode.ObjectInstance.Name == obj.Name && objectNode.ObjectInstance.TypeName == obj.TypeName)
                {
                    list.Add(objectNode);
                }
                if (node.Nodes != null)
                {
                    FindObjectNodeWithMuteSolo(obj, node.Nodes, list);
                }
            }
        }

        /// <summary>
        /// The find node with template.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        /// <returns>
        /// The <see cref="TreeNode"/>.
        /// </returns>
        private static TreeNode FindNodeWithTemplate(ITemplate template, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                TemplateNode templateNode = node as TemplateNode;
                if (templateNode == null)
                {
                    continue;
                }

                if (templateNode.ProxyObject.Template == template)
                {
                    return node;
                }

                TreeNode ret = FindNodeWithTemplate(template, node.Nodes);
                if (ret != null)
                {
                    return ret;
                }
            }

            return null;
        }

        /// <summary>
        /// The get child node index.
        /// </summary>
        /// <param name="srn">
        /// The srn.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private static int GetChildNodeIndex(ObjectRefNode srn)
        {
            return srn.Parent.Nodes.IndexOf(srn);
        }

        /// <summary>
        /// The remove reference.
        /// </summary>
        /// <param name="objectRef">
        /// The object reference.
        /// </param>
        /// <param name="parentObject">
        /// The parent object.
        /// </param>
        private static void RemoveReference(ObjectRef objectRef, IObjectInstance parentObject)
        {
            if (parentObject.Bank.IsCheckedOut)
            {
                parentObject.RemoveReference(objectRef.ObjectInstance, objectRef.Node);
                bool directChild = parentObject.Node.ChildNodes.Cast<XmlNode>().Any(n => n == objectRef.Node);
                ITypeDefinition typeDef = parentObject.TypeDefinitions.FindTypeDefinition(parentObject.TypeName);
                if (directChild || typeDef.SkipDeleteContainerOnRefDelete)
                {
                    if (directChild)
                    {
                        parentObject.Node.RemoveChild(objectRef.Node);
                    }
                    else
                    {
                        objectRef.Node.ParentNode.RemoveChild(objectRef.Node);
                    }
                }
                else
                {
                    try
                    {
                        parentObject.Node.RemoveChild(objectRef.Node.ParentNode);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                }

                // remove parent object as a referncing object
                if (objectRef.ObjectInstance != null)
                {
                    objectRef.ObjectInstance.RemoveReferencer(parentObject);
                }

                parentObject.ComputeReferences(true);
                parentObject.MarkAsDirty();
            }
            else
            {
                if (
                    ErrorManager.HandleQuestion("Object bank must be checked out first, do you want to check out?", MessageBoxButtons.YesNo,
                        DialogResult.No) == DialogResult.Yes)
                {
                    if (parentObject.Bank.Checkout(false))
                    {
                        parentObject = Types.Objects.ObjectInstance.GetObjectsMatchingName(parentObject.Name).First(p => p.Episode == parentObject.Episode);
                        objectRef = parentObject.References.First(p => p.ObjectInstance.Name.Equals(objectRef.ObjectInstance.Name));
                        RemoveReference(objectRef, parentObject);
                    }
                }
            }
        }

        /// <summary>
        /// The save tree state.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private void SaveTreeNodeStructuretoXmlNodes(XmlNode parentNode, TreeNodeCollection treeNodes)
        {
            foreach (TreeNode treeNode in treeNodes)
            {
                string name = Regex.Replace(treeNode.Text, @"[^\w-]+", string.Empty);
                XmlNode node = parentNode.OwnerDocument.CreateElement(name);
                parentNode.AppendChild(node);
                if (treeNode.IsExpanded)
                {
                    SaveTreeNodeStructuretoXmlNodes(node, treeNode.Nodes);
                }
            }
        }

        /// <summary>
        /// The swap child nodes.
        /// </summary>
        /// <param name="nodeToMoveBefore">
        /// The node to be placed before the next node (B).
        /// </param>
        /// <param name="nodeToMoveAfter">
        /// The node to be placed after the previous node (A).
        /// </param>
        private static void SwapChildNodes(ObjectRefNode nodeToMoveBefore, ObjectRefNode nodeToMoveAfter)
        {
            ObjectNode objectNodeA = nodeToMoveBefore as ObjectNode;
            ObjectNode objectNodeB = nodeToMoveAfter as ObjectNode;

            if (null == objectNodeA || null == objectNodeB)
            {
                // Can only swap object nodes
                return;
            }

            ObjectNode parentNode = objectNodeA.Parent as ObjectNode;
            if (null == parentNode)
            {
                return;
            }

            IObjectInstance parentObj = parentNode.ObjectInstance;

            Debug.Assert(nodeToMoveBefore.ObjectInstance.Node != null);
            Debug.Assert(nodeToMoveAfter.ObjectInstance.Node != null);

            if (nodeToMoveBefore.ObjectInstance.Node != null && nodeToMoveAfter.ObjectInstance.Node != null)
            {
                if (parentObj.Bank.IsCheckedOut)
                {
                    string objectA = nodeToMoveBefore.ObjectReference.Node.InnerText;
                    string objectB = nodeToMoveAfter.ObjectReference.Node.InnerText;

                    XmlNode aNode = null, bNode = null;
                    foreach (XmlNode child in parentObj.Node.ChildNodes)
                    {
                        foreach (XmlNode node in child.ChildNodes)
                        {
                            if (node.InnerText == objectA)
                            {
                                aNode = child;
                                continue;
                            }

                            if (node.InnerText == objectB)
                            {
                                bNode = child;
                            }
                        }
                    }

                    if (aNode != null && bNode != null)
                    {
                        parentObj.Node.InsertBefore(aNode, bNode);
                        parentObj.MarkAsDirty();
                        parentObj.ComputeReferences();
                    }
                }
                else
                {
                    ErrorManager.HandleInfo("Bank '" + parentObj.Bank.Name + "' must be checked out");
                }
            }
        }

        /// <summary>
        /// Add object instance children.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="parentAnnotation">
        /// The parent annotation.
        /// </param>
        private void AddObjectInstanceChildren(IObjectInstance objectInstance, XmlNode node, TreeNode parentNode, ObjectAnnotationNode parentAnnotation)
        {
            ObjectRef objectRef = new ObjectRef { ObjectInstance = objectInstance, Node = node };
            ObjectAnnotationNode annotationNode = new ObjectAnnotationNode(parentAnnotation, objectInstance);
            ObjectRefNode objectRefNode = new ObjectRefNode(objectRef, annotationNode);

            IObjectInstance referencedObject = objectRef.ObjectInstance;

            if (referencedObject != null)
            {
                objectRefNode.SelectedImageIndex =
                    objectRefNode.ImageIndex = (int)this.typeIconTable[referencedObject.TypeName];

                if (!objectInstance.TypeName.ToUpper().Equals("CATEGORY") || _isCategoryTop || Mode == HierarchyBrowserModes.Categories)
                {
                    foreach (var reference in referencedObject.References)
                    {
                        if (reference.ObjectInstance == null)
                        {
                            continue;
                        }

                        bool isDefault = false;
                        if (!reference.ObjectInstance.TypeName.ToUpper().Equals("CATEGORY") && Mode == HierarchyBrowserModes.Categories)
                        {
                            foreach (IFieldDefinition field in objectInstance.TypeDefinitions.FindTypeDefinition(objectInstance.TypeName).Fields)
                            {
                                if (field.DefaultValue == reference.ObjectInstance.Name)
                                {
                                    isDefault = true;
                                }
                            }
                        }
                        if (isDefault) continue;
                        if (Mode == HierarchyBrowserModes.Default)
                        {
                            objectRefNode.Nodes.Add(new TreeNode());
                        }
                        else
                        {
                            this.AddObjectInstanceChildren(reference.ObjectInstance, reference.Node, objectRefNode, annotationNode);
                        }
                    }
                }
            }
            parentNode.Nodes.Add(objectRefNode);
        }

        /// <summary>
        /// The add template children.
        /// </summary>
        /// <param name="proxyObject">
        /// The proxy object.
        /// </param>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="parentAnnotation">
        /// The parent annotation.
        /// </param>
        private void AddTemplateChildren(ITemplateProxyObject proxyObject, TreeNode parentNode, ObjectAnnotationNode parentAnnotation)
        {
            // Add external reference nodes at this level
            foreach (KeyValuePair<IObjectInstance, XmlNode> externalRef in proxyObject.ExternalReferences)
            {
                this.AddObjectInstanceChildren(externalRef.Key, externalRef.Value, parentNode, parentAnnotation);
            }

            foreach (ITemplateProxyObject child in proxyObject.Children)
            {
                this.AddTemplateChildrenHelper(child, parentNode, parentAnnotation);
            }
        }

        /// <summary>
        /// The add template children helper.
        /// </summary>
        /// <param name="proxyObject">
        /// The proxy object.
        /// </param>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="parentAnnotation">
        /// The parent annotation.
        /// </param>
        private void AddTemplateChildrenHelper(ITemplateProxyObject proxyObject, TreeNode parentNode, ObjectAnnotationNode parentAnnotation)
        {
            ObjectAnnotationNode annotationNode = new ObjectAnnotationNode(parentAnnotation, proxyObject);
            TemplateNode proxyNode = new TemplateNode(proxyObject, annotationNode);

            if (this.typeIconTable.ContainsKey(proxyObject.TypeName))
            {
                proxyNode.SelectedImageIndex = proxyNode.ImageIndex = (int)this.typeIconTable[proxyObject.TypeName];
            }
            else
            {
                proxyNode.SelectedImageIndex = proxyNode.ImageIndex = 0;
            }

            parentNode.Nodes.Add(proxyNode);

            // Add external reference nodes at this level
            foreach (KeyValuePair<IObjectInstance, XmlNode> externalRef in proxyObject.ExternalReferences)
            {
                this.AddObjectInstanceChildren(externalRef.Key, externalRef.Value, proxyNode, annotationNode);
            }

            // Continue to add nested children
            foreach (ITemplateProxyObject child in proxyObject.Children)
            {
                this.AddTemplateChildrenHelper(child, proxyNode, annotationNode);
            }
        }

        /// <summary>
        /// The do collapse.
        /// </summary>
        /// <param name="nodes">
        /// The nodes.
        /// </param>
        private void DoCollapse(TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                if (node == null)
                {
                    continue;
                }

                if (node.Parent != null)
                {


                    ObjectRefNode objectRefNode = node as ObjectRefNode;
                    if (null != objectRefNode)
                    {
                        string nodeName = objectRefNode.ObjectName;
                        if (!this.m_collapse)
                        {
                            //objectRefNode.Text = objectRefNode.ObjectName;
                        }
                        else if (objectRefNode.Parent != null)
                        {
                            ObjectNode snParent = objectRefNode.Parent as ObjectNode;
                            ObjectRefNode srnParent = objectRefNode.Parent as ObjectRefNode;

                            if (snParent != null && nodeName.StartsWith(snParent.ObjectName))
                            {
                                objectRefNode.Text = "..." + nodeName.Substring(snParent.ObjectName.Length);
                            }
                            else if (srnParent != null && nodeName.StartsWith(srnParent.ObjectName))
                            {
                                objectRefNode.Text = "..." + nodeName.Substring(srnParent.ObjectName.Length);
                            }
                        }
                    }
                    else
                    {
                        ObjectNode objectNode = node as ObjectNode;
                        if (objectNode != null)
                        {
                            string nodeName = objectNode.ObjectName;
                            if (!this.m_collapse)
                            {
                                //objectNode.Text = objectNode.ObjectName;
                            }
                            else if (objectNode.Parent != null)
                            {
                                ObjectNode snParent = objectNode.Parent as ObjectNode;
                                ObjectRefNode srnParent = objectNode.Parent as ObjectRefNode;

                                if (snParent != null && nodeName.StartsWith(snParent.ObjectName))
                                {
                                    objectNode.Text = "..." + nodeName.Substring(snParent.ObjectName.Length);
                                }

                                if (srnParent != null && nodeName.StartsWith(srnParent.ObjectName))
                                {
                                    objectNode.Text = "..." + nodeName.Substring(srnParent.ObjectName.Length);
                                }
                            }
                        }
                    }
                }

                this.DoCollapse(node.Nodes);
            }

        }

        /// <summary>
        /// The expand tree.
        /// </summary>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private void ExpandTree(XmlDocument treeState, TreeNodeCollection treeNodes)
        {
            this.m_treeView.CollapseAll();
            ExpandTree(treeState.DocumentElement, treeNodes);
        }

        /// <summary>
        /// Expand all property nodes.
        /// </summary>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        /// <param name="expand">
        /// The expand.
        /// </param>
        private void ExpandPropertyNodes(IEnumerable treeNodes, bool expand)
        {
            foreach (TreeNode treeNode in treeNodes)
            {
                AnnotationGroupNode annotationGroupNode = treeNode as AnnotationGroupNode;
                if (null != annotationGroupNode)
                {
                    if (expand)
                    {
                        annotationGroupNode.Expand();
                    }
                    else
                    {
                        annotationGroupNode.Collapse();
                    }
                }
                else
                {
                    this.ExpandPropertyNodes(treeNode.Nodes, expand);
                }
            }
        }

        /// <summary>
        /// The handle delete.
        /// </summary>
        private void HandleDelete()
        {
            if (this.CanDelete)
            {
                ObjectRefNode srn = this.m_treeView.SelectedNode as ObjectRefNode;
                if (srn != null)
                {
                    if (srn.ObjectInstance.Bank.IsCheckedOut)
                    {
                        ObjectNode parentObjectNode = this.m_treeView.SelectedNode.Parent as ObjectNode;

                        if (null == parentObjectNode)
                        {
                            ErrorManager.HandleInfo("Failed to get parent object node");
                            return;
                        }

                        if (this.Mode == HierarchyBrowserModes.Categories)
                        {
                            CategoryDeleteViewModel categoryDelete = new CategoryDeleteViewModel(srn.ObjectInstance, parentObjectNode.ObjectInstance);
                            categoryDelete.DeleteAction += DeleteCategory;
                            return;
                        }

                        IObjectInstance parentObjectInstance = parentObjectNode.ObjectInstance;
                        srn.ObjectReference.ObjectInstance.RemoveReferencer(parentObjectInstance);
                        parentObjectInstance.RemoveReference(srn.ObjectReference.ObjectInstance, srn.ObjectReference.Node);
                        bool directChild =
                            parentObjectInstance.Node.ChildNodes.Cast<XmlNode>().Any(n => n == srn.ObjectReference.Node);
                        parentObjectInstance.Node.RemoveChild(
                            directChild ? srn.ObjectReference.Node : srn.ObjectReference.Node.ParentNode);
                        parentObjectInstance.MarkAsDirty();

                        srn.ObjectInstance.Delete();
                        srn.Remove();
                    }
                    else
                    {
                        ErrorManager.HandleInfo(srn.ObjectInstance.Bank.Name + " needs to be checked out");
                    }
                }
            }
        }

        private void DeleteCategory(IObjectInstance objectNode)
        {
            if (this.CanDelete)
            {
                ObjectRefNode srn = FindNodeWithObject(objectNode, this.m_treeView.Nodes) as ObjectRefNode;

                if (srn != null)
                {
                    if (!srn.ObjectInstance.TypeName.Equals("category", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return;
                    }

                    if (srn.ObjectInstance.Bank.IsCheckedOut)
                    {
                        ObjectNode parentObjectNode = this.m_treeView.SelectedNode.Parent as ObjectNode;

                        if (null == parentObjectNode)
                        {
                            ErrorManager.HandleInfo("Failed to get parent object node");
                            return;
                        }

                        if (srn.Nodes.Count > 0)
                        {
                            IEnumerable<TreeNode> treeNodes = srn.Nodes.Cast<TreeNode>();
                            IEnumerable<ObjectRefNode> catNodes = treeNodes.OfType<ObjectRefNode>().ToList<ObjectRefNode>().Where(p => p.ObjectInstance.TypeName.Equals("category", StringComparison.InvariantCultureIgnoreCase));
                            foreach (ObjectRefNode catNode in catNodes)
                            {
                                DeleteCategory(catNode.ObjectInstance);
                            }
                        }

                        IObjectInstance parentObjectInstance = parentObjectNode.ObjectInstance;
                        srn.ObjectReference.ObjectInstance.RemoveReferencer(parentObjectInstance);
                        parentObjectInstance.RemoveReference(srn.ObjectReference.ObjectInstance, srn.ObjectReference.Node);
                        bool directChild =
                            parentObjectInstance.Node.ChildNodes.Cast<XmlNode>().Any(n => n == srn.ObjectReference.Node);
                        XmlNode nodeToRemove = directChild ? srn.ObjectReference.Node : srn.ObjectReference.Node.ParentNode;
                        if (parentObjectInstance.Node.ChildNodes.Cast<XmlNode>().Contains(nodeToRemove))
                        {
                            parentObjectInstance.Node.RemoveChild(nodeToRemove);
                            parentObjectInstance.MarkAsDirty();
                        }
                        srn.ObjectInstance.Delete();
                        srn.Remove();
                    }
                    else
                    {
                        ErrorManager.HandleInfo(srn.ObjectInstance.Bank.Name + " needs to be checked out");
                    }
                }
            }
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager resources = new ComponentResourceManager(typeof(HierarchyBrowser));
            this.m_menu = new ContextMenuStrip(this.components);
            this.mnuInsertWrapper = new ToolStripMenuItem();
            this.mnuMakeLocal = new ToolStripMenuItem();
            this.mnuRemoveRef = new ToolStripMenuItem();
            this.mnuAddChild = new ToolStripMenuItem();
            this.mnuDelete = new ToolStripMenuItem();

            this.mnuCheckOut = new ToolStripMenuItem();
            this.mnuLocalCheckOut = new ToolStripMenuItem();
            this.mnuFindInBrowser = new ToolStripMenuItem();
            this.mnuFindTemplateInBrowser = new ToolStripMenuItem();
            this.mnuMoveUp = new ToolStripMenuItem();
            this.mnuMoveDown = new ToolStripMenuItem();
            this.mnuRename = new ToolStripMenuItem();
            this.mnuPropertySelector = new ToolStripMenuItem();
            this.mnuLivePropertySelector = new ToolStripMenuItem();
            this.mnuExpandAllLiveProperties = new ToolStripMenuItem();
            this.mnuExpandAll = new ToolStripMenuItem();
            this.mnuMute = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSolo = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ObjectImageList = new ImageList(this.components);
            this.m_toolbar = new ToolBar();
            this.btnBack = new ToolBarButton();
            this.btnForward = new ToolBarButton();
            this.btnSnapBack = new ToolBarButton();
            this.btnSetSnapback = new ToolBarButton();
            this.btnExpandAll = new ToolBarButton();
            this.btCollapse = new ToolBarButton();
            this.btnAutoUpdate = new System.Windows.Forms.ToolBarButton();
            this.m_ImageList = new ImageList(this.components);
            this.m_detailsPanel = new Panel();
            this.m_lblObjectType = new Label();
            this.m_lblObjectName = new Label();
            this.m_objectTypeIcon = new PictureBox();
            this.m_treeView = new DoubleBufferedTreeView();
            this.m_menu.SuspendLayout();
            this.m_detailsPanel.SuspendLayout();
            ((ISupportInitialize)this.m_objectTypeIcon).BeginInit();
            this.SuspendLayout();

            // m_Menu
            this.m_menu.Items.AddRange(
                new ToolStripItem[]
                    {
                        this.mnuInsertWrapper, this.mnuMakeLocal, this.mnuRemoveRef, this.mnuAddChild, this.mnuDelete,
                        this.mnuInsertWrapper, this.mnuMakeLocal, this.mnuRemoveRef,  this.mnuMute, this.mnuSolo, this.mnuAddChild, this.mnuDelete,
                        this.mnuCheckOut, this.mnuLocalCheckOut, this.mnuFindInBrowser, this.mnuFindTemplateInBrowser,
                        this.mnuMoveUp, this.mnuMoveDown, this.mnuRename, this.mnuPropertySelector, this.mnuLivePropertySelector,
                        this.mnuExpandAll
                    });
            this.m_menu.Name = "m_menu";
            this.m_menu.Size = new Size(225, 246);
            this.m_menu.Opening += this.Menu_Popup;
            this.m_menu.Closing += this.m_Menu_Collapse;

            // mnuInsertWrapper
            this.mnuInsertWrapper.Name = "mnuInsertWrapper";
            this.mnuInsertWrapper.ShortcutKeys = (Keys.Control | Keys.W);
            this.mnuInsertWrapper.Size = new Size(224, 22);
            this.mnuInsertWrapper.Text = "Insert &Wrapper";
            this.mnuInsertWrapper.Click += this.mnuInsertWrapper_Click;

            // mnuMakeLocal
            this.mnuMakeLocal.Name = "mnuMakeLocal";
            this.mnuMakeLocal.ShortcutKeys = (Keys.Control | Keys.L);
            this.mnuMakeLocal.Size = new Size(224, 22);
            this.mnuMakeLocal.Text = "Make &Local";
            this.mnuMakeLocal.Click += this.mnuMakeLocal_Click;

            // mnuMute
            this.mnuMute.Name = "mnuNewSound";
            this.mnuMute.Size = new System.Drawing.Size(220, 22);
            this.mnuMute.Text = "Mute";
            this.mnuMute.CheckOnClick = true;
            this.mnuMute.Click += this.mnuMute_Click;

            // mnuSolo
            this.mnuSolo.Name = "mnuNewSound";
            this.mnuSolo.Size = new System.Drawing.Size(220, 22);
            this.mnuSolo.Text = "Solo";
            this.mnuMute.CheckOnClick = true;
            this.mnuSolo.Click += this.mnuSolo_Click;

            // mnuRemoveRef
            this.mnuRemoveRef.Name = "mnuRemoveRef";
            this.mnuRemoveRef.ShortcutKeys = (Keys.Control | Keys.Delete);
            this.mnuRemoveRef.Size = new Size(224, 22);
            this.mnuRemoveRef.Text = "&Remove Reference";
            this.mnuRemoveRef.Click += this.mnuRemoveRef_Click;

            // mnuAddChild
            this.mnuAddChild.Name = "mnuAddChild";
            this.mnuAddChild.Size = new Size(224, 22);
            this.mnuAddChild.Text = "&Create Child";

            // mnuDelete
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Size = new Size(224, 22);
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += this.mnuDelete_Click;

            // mnuCheckOut
            this.mnuCheckOut.Name = "mnuCheckOut";
            this.mnuCheckOut.ShortcutKeys = (Keys.Control | Keys.O);
            this.mnuCheckOut.Size = new Size(224, 22);
            this.mnuCheckOut.Text = "Check &Out Store";
            this.mnuCheckOut.Click += this.mnuCheckOut_Click;

            // mnuLocalCheckOut
            this.mnuLocalCheckOut.Name = "mnuLocalCheckOut";
            this.mnuLocalCheckOut.Size = new Size(224, 22);
            this.mnuLocalCheckOut.Text = "Local Check Out";
            this.mnuLocalCheckOut.Click += this.mnuLocalCheckOut_Click;

            // mnuFindInBrowser
            this.mnuFindInBrowser.Name = "mnuFindInBrowser";
            this.mnuFindInBrowser.ShortcutKeys = (Keys.Control | Keys.B);
            this.mnuFindInBrowser.Size = new Size(224, 22);
            this.mnuFindInBrowser.Text = "Find in &Browser";
            this.mnuFindInBrowser.Click += this.mnuFindInBrowser_Click;

            // mnuFindTemplateInBrowser
            this.mnuFindTemplateInBrowser.Name = "mnuFindTemplateInBrowser";
            this.mnuFindTemplateInBrowser.Size = new Size(224, 22);
            this.mnuFindTemplateInBrowser.Text = "Find Template in Browser";
            this.mnuFindTemplateInBrowser.Click += this.mnuFindInBrowser_Click;

            // mnuMoveUp
            this.mnuMoveUp.Name = "mnuMoveUp";
            this.mnuMoveUp.ShortcutKeys = (Keys.Control | Keys.U);
            this.mnuMoveUp.Size = new Size(224, 22);
            this.mnuMoveUp.Text = "Move &Up";
            this.mnuMoveUp.Click += this.mnuMoveUp_Click;

            // mnuMoveDown
            this.mnuMoveDown.Name = "mnuMoveDown";
            this.mnuMoveDown.ShortcutKeys = (Keys.Control | Keys.D);
            this.mnuMoveDown.Size = new Size(224, 22);
            this.mnuMoveDown.Text = "Move &Down";
            this.mnuMoveDown.Click += this.mnuMoveDown_Click;

            // mnuRename
            this.mnuRename.Name = "mnuRename";
            this.mnuRename.Size = new Size(224, 22);
            this.mnuRename.Text = "Rename";
            this.mnuRename.Click += this.mnuRename_Click;

            // mnuPropertySelector
            this.mnuPropertySelector.Name = "mnuPropertySelector";
            this.mnuPropertySelector.Size = new Size(224, 22);
            this.mnuPropertySelector.Text = "Show Property";

            // mnuLivePropertySelector
            this.mnuLivePropertySelector.Name = "mnuLivePropertySelector";
            this.mnuLivePropertySelector.Size = new Size(224, 22);
            this.mnuLivePropertySelector.Text = "Show Live Property";

            // mnuExpandAll
            this.mnuExpandAll.Name = "mnuExpandAll";
            this.mnuExpandAll.Size = new Size(224, 22);
            this.mnuExpandAll.Text = "Expand All";

            // mnuExpandAllLiveProperties
            this.mnuExpandAllLiveProperties.Name = "mnuExpandAllLiveProperties";
            this.mnuExpandAllLiveProperties.Size = new Size(224, 22);
            this.mnuExpandAllLiveProperties.Text = "Live Properties";
            this.mnuExpandAllLiveProperties.Click += this.ExpandAllLivePropertiesClick;
            this.mnuExpandAll.DropDownItems.Add(this.mnuExpandAllLiveProperties);

            // m_ObjectImageList
            this.m_ObjectImageList.ColorDepth = ColorDepth.Depth8Bit;
            this.m_ObjectImageList.ImageSize = new Size(16, 16);
            this.m_ObjectImageList.TransparentColor = Color.Transparent;

            // m_Toolbar
            this.m_toolbar.Appearance = ToolBarAppearance.Flat;
            this.m_toolbar.BorderStyle = BorderStyle.FixedSingle;
            this.m_toolbar.Buttons.AddRange(
                new[]
                    {
                        this.btnBack, this.btnForward, this.btnSnapBack, this.btnSetSnapback, this.btnExpandAll,
                        this.btCollapse, this.btnAutoUpdate
					});
            this.m_toolbar.ButtonSize = new Size(16, 16);
            this.m_toolbar.DropDownArrows = true;
            this.m_toolbar.ImageList = this.m_ImageList;
            this.m_toolbar.Location = new Point(0, 0);
            this.m_toolbar.Name = "m_toolbar";
            this.m_toolbar.ShowToolTips = true;
            this.m_toolbar.Size = new Size(288, 29);
            this.m_toolbar.TabIndex = 4;
            this.m_toolbar.ButtonClick += this.Toolbar_ButtonClick;

            // btnBack
            this.btnBack.Enabled = false;
            this.btnBack.ImageIndex = 0;
            this.btnBack.Name = "btnBack";
            this.btnBack.Tag = "back";
            this.btnBack.ToolTipText = "Back";

            // btnForward
            this.btnForward.Enabled = false;
            this.btnForward.ImageIndex = 1;
            this.btnForward.Name = "btnForward";
            this.btnForward.Tag = "forward";
            this.btnForward.ToolTipText = "Forward";

            // btnSnapBack
            this.btnSnapBack.Enabled = false;
            this.btnSnapBack.ImageIndex = 3;
            this.btnSnapBack.Name = "btnSnapBack";
            this.btnSnapBack.Tag = "snapback";
            this.btnSnapBack.ToolTipText = "Snap Back";

            // btnSetSnapback
            this.btnSetSnapback.Enabled = false;
            this.btnSetSnapback.ImageIndex = 2;
            this.btnSetSnapback.Name = "btnSetSnapback";
            this.btnSetSnapback.Tag = "setsnapback";
            this.btnSetSnapback.ToolTipText = "Set Snapback location";

            // btnExpandAll
            this.btnExpandAll.Enabled = false;
            this.btnExpandAll.ImageIndex = 4;
            this.btnExpandAll.Name = "btnExpandAll";
            this.btnExpandAll.Tag = "expandall";
            this.btnExpandAll.ToolTipText = "Expand all";

            // btCollapse
            this.btCollapse.ImageIndex = 5;
            this.btCollapse.Name = "btCollapse";
            this.btCollapse.Tag = "collapse";
            this.btCollapse.ToolTipText = "Collapse Names";
            // 
            // btnAutoUpdate
            // 
            this.btnAutoUpdate.ImageIndex = 7;
            this.btnAutoUpdate.Name = "btnAutoUpdate";
            this.btnAutoUpdate.Tag = "autoUpdate";
            this.btnAutoUpdate.ToolTipText = "Auto Update";
            // 


            // m_ImageList
            // 
            this.m_ImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("m_ImageList.ImageStream")));
            this.m_ImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.m_ImageList.Images.SetKeyName(0, "");
            this.m_ImageList.Images.SetKeyName(1, "");
            this.m_ImageList.Images.SetKeyName(2, "");
            this.m_ImageList.Images.SetKeyName(3, "");
            this.m_ImageList.Images.SetKeyName(4, "");
            this.m_ImageList.Images.SetKeyName(5, "elips.bmp");
            this.m_ImageList.Images.SetKeyName(6, "lightbolt.png");
            this.m_ImageList.Images.SetKeyName(7, "lightboltselect.png");
            this.m_ImageList.Images.SetKeyName(8, "");
            this.m_ImageList.Images.SetKeyName(9, "");
            // 

            // m_DetailsPanel
            this.m_detailsPanel.BackColor = SystemColors.Control;
            this.m_detailsPanel.BorderStyle = BorderStyle.FixedSingle;
            this.m_detailsPanel.Controls.Add(this.m_lblObjectType);
            this.m_detailsPanel.Controls.Add(this.m_lblObjectName);
            this.m_detailsPanel.Controls.Add(this.m_objectTypeIcon);
            this.m_detailsPanel.Dock = DockStyle.Bottom;
            this.m_detailsPanel.Location = new Point(0, 389);
            this.m_detailsPanel.Name = "m_detailsPanel";
            this.m_detailsPanel.Size = new Size(288, 48);
            this.m_detailsPanel.TabIndex = 5;

            // m_lblObjectType
            this.m_lblObjectType.Anchor = ((AnchorStyles.Bottom | AnchorStyles.Left) | AnchorStyles.Right);
            this.m_lblObjectType.Font = new Font("Microsoft Sans Serif", 7F, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.m_lblObjectType.Location = new Point(48, 22);
            this.m_lblObjectType.Name = "m_lblObjectType";
            this.m_lblObjectType.Size = new Size(230, 16);
            this.m_lblObjectType.TabIndex = 2;

            // m_lblObjectName
            this.m_lblObjectName.Anchor = ((AnchorStyles.Bottom | AnchorStyles.Left) | AnchorStyles.Right);
            this.m_lblObjectName.Font = new Font("Microsoft Sans Serif", 7F, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.m_lblObjectName.Location = new Point(48, 6);
            this.m_lblObjectName.Name = "m_lblObjectName";
            this.m_lblObjectName.Size = new Size(230, 16);
            this.m_lblObjectName.TabIndex = 1;

            // m_ObjectTypeIcon
            this.m_objectTypeIcon.Location = new Point(8, 8);
            this.m_objectTypeIcon.Name = "m_objectTypeIcon";
            this.m_objectTypeIcon.Size = new Size(32, 32);
            this.m_objectTypeIcon.SizeMode = PictureBoxSizeMode.StretchImage;
            this.m_objectTypeIcon.TabIndex = 0;
            this.m_objectTypeIcon.TabStop = false;

            // m_TreeView
            this.m_treeView.AllowDrop = true;
            this.m_treeView.BackColor = System.Drawing.Color.White;
            this.m_treeView.ContextMenuStrip = this.m_menu;
            this.m_treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_treeView.HideSelection = false;
            this.m_treeView.ImageIndex = 0;
            this.m_treeView.ImageList = this.m_ObjectImageList;
            this.m_treeView.ItemHeight = 18;
            this.m_treeView.Location = new Point(0, 29);
            this.m_treeView.Name = "m_treeView";
            this.m_treeView.SelectedImageIndex = 0;
            this.m_treeView.Size = new Size(288, 360);
            this.m_treeView.Sorted = true;
            this.m_treeView.TabIndex = 3;
            this.m_treeView.DragLeave += this.TreeView_DragLeave;
            this.m_treeView.DoubleClick += this.TreeView_DoubleClick;
            this.m_treeView.DragDrop += this.TreeView_DragDrop;
            this.m_treeView.AfterSelect += this.TreeView_AfterSelect;
            this.m_treeView.MouseMove += this.TreeView_MouseMove;
            this.m_treeView.MouseDown += this.TreeView_MouseDown;
            this.m_treeView.DragEnter += this.TreeView_DragEnter;
            this.m_treeView.KeyDown += this.TreeView_KeyDown;
            this.KeyDown += this.TreeView_KeyDown;
            this.m_treeView.ItemDrag += this.TreeView_ItemDrag;
            this.m_treeView.DragOver += this.TreeView_DragOver;
            this.m_treeView.DrawMode = TreeViewDrawMode.Normal;
            this.m_treeView.BackColor = Color.White;
            this.m_treeView.DrawNode += this.DrawNode;

            // ctrlSoundHierarchyViewer
            // 

            // HierarchyBrowser
            // 
            this.Controls.Add(this.m_treeView);
            this.Controls.Add(this.m_detailsPanel);
            this.Controls.Add(this.m_toolbar);
            this.Name = "HierarchyBrowser";
            this.Size = new Size(288, 437);
            this.m_menu.ResumeLayout(false);
            this.m_detailsPanel.ResumeLayout(false);
            ((ISupportInitialize)this.m_objectTypeIcon).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        /// <summary>
        /// The load icons.
        /// </summary>
        private void LoadIcons()
        {
            this.typeIconTable = new Hashtable();
            foreach (audMetadataType metadataType in Configuration.MetadataTypes)
            {
                // Load meta data type icons
                RaveUtils.LoadTypeIcons(
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, metadataType.IconPath),
                    this.typeIconTable,
                    this.m_ObjectImageList,
                    RaveInstance.AllTypeDefinitions[metadataType.Type],
                    true);

                // Add template image
                if (!this.typeIconTable.Contains("TemplateInstance"))
                {
                    int index = this.m_ObjectImageList.Images.Count;
                    //this.m_ObjectImageList.Images.Add(Resources.Template); // TODO REFACTOR ISSUE
                    this.typeIconTable.Add("TemplateInstance", index);
                }
            }
        }

        public void UpdateFont(ObjectNode node)
        {
            if (node.ObjectInstance == null)
            {
                node.ForeColor = Color.Red;
                node.NodeFont = TreeView.DefaultFont;
            }
            else
            {
                if (node.ObjectInstance.IsMuted)
                {
                    node.NodeFont = ObjectNode.boldFont;
                    node.ForeColor = Color.DarkOrange;
                    node.Brush = Brushes.DarkOrange;
                }
                else if (node.ObjectInstance.IsSoloed)
                {
                    node.NodeFont = ObjectNode.boldFont;
                    node.ForeColor = Color.Blue;
                    node.Brush = Brushes.Blue;
                }
                else
                {
                    node.NodeFont = TreeView.DefaultFont;
                    node.ForeColor = Color.Black;
                    node.Brush = Brushes.Black;
                }
            }
            node.Text = node.Text;
        }

        /// <summary>
        /// The menu_ popup.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Menu_Popup(object sender, CancelEventArgs e)
        {
            // Initialize the plugin menu
            if (this.mnuPlugin == null && this.m_externalEditors.Count != 0)
            {
                this.mnuPlugin = new ToolStripMenuItem("Launch Plugin");
                foreach (IRAVEEditorPlugin plugin in this.m_externalEditors)
                {
                    this.mnuPlugin.DropDownItems.Add(
                        new ToolStripMenuItem(plugin.GetName(), null, this.mnuPlugin_OnClick));
                }

                this.m_menu.Items.Add(this.mnuPlugin);
            }

            // Enable plugin menu if root node is an object node
            if (this.m_treeView.Nodes.Count > 0 && null != this.mnuPlugin)
            {
                ObjectNode objectNode = this.m_treeView.Nodes[0] as ObjectNode;
                if (null != objectNode && objectNode.ObjectInstance.Bank.IsCheckedOut)
                {
                    this.mnuPlugin.Enabled = true;
                }
                else
                {
                    this.mnuPlugin.Enabled = false;
                }
            }

            this.InitPropertySelector();

            // Reset menu
            this.mnuAddChild.Dispose();
            this.mnuAddChild = new ToolStripMenuItem("&Create Child");
            this.m_menu.Items.Insert(2, this.mnuAddChild);

            this.mnuAddChild.Visible = false;
            this.mnuDelete.Visible = false;
            this.mnuMoveUp.Visible = false;
            this.mnuMoveDown.Visible = false;
            this.mnuMakeLocal.Visible = false;
            this.mnuInsertWrapper.Visible = false;
            this.mnuRemoveRef.Visible = false;
            this.mnuLocalCheckOut.Visible = false;
            this.mnuRename.Visible = false;
            this.mnuFindInBrowser.Visible = false;
            this.mnuFindTemplateInBrowser.Visible = false;
            this.mnuCheckOut.Visible = false;
            this.mnuPropertySelector.Visible = false;
            this.mnuLivePropertySelector.Visible = false;
            this.mnuMute.Visible = false;
            this.mnuSolo.Visible = false;

            this.mnuAddChild.Enabled = false;
            this.mnuDelete.Enabled = false;
            this.mnuMoveUp.Enabled = false;
            this.mnuMoveDown.Enabled = false;
            this.mnuMakeLocal.Enabled = false;
            this.mnuInsertWrapper.Enabled = false;
            this.mnuRemoveRef.Enabled = false;
            this.mnuLocalCheckOut.Enabled = false;
            this.mnuRename.Enabled = false;
            this.mnuFindInBrowser.Enabled = false;
            this.mnuFindTemplateInBrowser.Enabled = false;
            this.mnuCheckOut.Enabled = false;

            TreeNode node = this.m_treeView.SelectedNode;

            if (null != node)
            {
                if (this.mnuPropertySelector.DropDownItems.Count > 0)
                {
                    this.mnuPropertySelector.Visible = true;
                }

                if (this.Mode == HierarchyBrowserModes.Categories)
                {
                    this.mnuLivePropertySelector.Visible = true;
                    this.mnuLivePropertySelector.Enabled = this.mnuLivePropertySelector.DropDownItems.Count > 0;
                    this.mnuExpandAll.Visible = true;
                    this.mnuExpandAllLiveProperties.Enabled = this.mnuLivePropertySelector.DropDownItems.Count > 0;
                }
            }

            ObjectRefNode objRefNode = node as ObjectRefNode;
            ObjectNode objNode = null;
            if (null == objRefNode)
            {
                // Required as ObjectRefNode extends ObjectNode
                objNode = node as ObjectNode;
            }

            // Create menu for node within template
            if (this.m_treeView.Nodes.Count > 0 && null != this.m_treeView.Nodes[0] as TemplateNode)
            {
                if (null != objRefNode)
                {
                    this.mnuFindInBrowser.Visible = true;
                    this.mnuFindInBrowser.Enabled = true;
                }
                else
                {
                    TemplateNode templateNode = node as TemplateNode;
                    if (null != templateNode)
                    {
                        this.mnuFindTemplateInBrowser.Visible = true;
                        this.mnuFindTemplateInBrowser.Enabled = true;
                    }
                }
            }
            else
            {
                if (null != node)
                {
                    // Initialize delete menu
                    this.mnuDelete.Visible = this.CanDelete;
                    this.mnuDelete.Enabled = node != null;
                }

                // Create menu for object reference node
                if (null != objRefNode)
                {
                    this.mnuMakeLocal.Visible = true;
                    this.mnuInsertWrapper.Visible = this.CanInsertWrapper;
                    this.mnuRemoveRef.Visible = this.CanRemoveReference;
                    this.mnuRemoveRef.Enabled = this.CanRemoveReference;

                    this.mnuFindInBrowser.Visible = this.CanFindInBrowser;
                    this.mnuFindInBrowser.Enabled = this.CanFindInBrowser;

                    if (objRefNode.ObjectReference.ObjectInstance != null)
                    {
                        ObjectNode parentNode = objRefNode.Parent as ObjectNode;
                        if (null != parentNode && parentNode.ObjectInstance.Bank.IsCheckedOut)
                        {
                            this.mnuMakeLocal.Enabled = true;
                            this.mnuInsertWrapper.Enabled = this.CanInsertWrapper;
                        }
                    }

                    if (null != objRefNode.ObjectInstance)
                    {
                        // Populate 'create child' menu
                        if (objRefNode.ObjectReference.ObjectInstance != null)
                        {
                            RaveUtils.CreateObjectMenus(
                                objRefNode.ObjectInstance.TypeDefinitions, this.mnuAddChild_Click, this.mnuAddChild);
                            ITypeDefinition typeDef =
                                objRefNode.ObjectInstance.TypeDefinitions.FindTypeDefinition(
                                    objRefNode.ObjectReference.ObjectInstance.TypeName);

                            if (objRefNode.ObjectInstance.TypeDefinitions
                                != RaveInstance.AllTypeDefinitions["GAMEOBJECTS"])
                            {
                                this.mnuAddChild.Visible = true;
                                this.mnuAddChild.Enabled = CanAddChildren(typeDef);
                            }

                            this.mnuSolo.Checked = objRefNode.ObjectInstance.IsSoloed;
                            this.mnuMute.Checked = objRefNode.ObjectInstance.IsMuted;
                            this.mnuMute.Visible = true;
                            this.mnuSolo.Visible = true;

                            this.mnuSolo.Enabled = true;
                            this.mnuMute.Enabled = true;
                            if (!objRefNode.ObjectInstance.CheckIfMuteSoloEnabled())
                            {
                                this.mnuSolo.Enabled = false;
                                this.mnuMute.Enabled = false;
                            }
                        }

                        if (objRefNode.ObjectInstance.Bank.IsCheckedOut)
                        {
                            this.mnuRename.Enabled = true;
                            this.mnuRename.Visible = true;
                        }

                        if (!objRefNode.ObjectInstance.Bank.IsCheckedOut && this.CanCheckInOut)
                        {
                            this.mnuCheckOut.Visible = true;
                            this.mnuCheckOut.Enabled = true;
                            if (this.CanLocalCheckOut)
                            {
                                this.mnuLocalCheckOut.Visible = true;
                                this.mnuLocalCheckOut.Enabled = true;
                            }
                        }
                    }

                    // Enable move up/down menu items if required
                    this.mnuMoveUp.Visible = true;
                    this.mnuMoveDown.Visible = true;

                    int nodeIndex = GetChildNodeIndex(objRefNode);

                    if (nodeIndex >= 1 && !this.AlphabeticalSorted)
                    {
                        this.mnuMoveUp.Enabled = true;
                    }

                    if (nodeIndex < objRefNode.Parent.Nodes.Count - 1 && !this.AlphabeticalSorted)
                    {
                        this.mnuMoveDown.Enabled = true;
                    }
                }

                // Create menu for object node
                if (null != objNode && null != objNode.ObjectInstance)
                {
                    this.mnuFindInBrowser.Visible = this.CanFindInBrowser;
                    this.mnuFindInBrowser.Enabled = this.CanFindInBrowser;
                    this.mnuInsertWrapper.Visible = this.CanInsertWrapper;

                    ObjectNode parentNode = objNode.Parent as ObjectNode;
                    if (null != parentNode && parentNode.ObjectInstance.Bank.IsCheckedOut)
                    {
                        this.mnuMakeLocal.Enabled = true;
                        this.mnuInsertWrapper.Enabled = this.CanInsertWrapper;
                    }

                    if (objNode.ObjectInstance.TypeDefinitions != RaveInstance.AllTypeDefinitions["GAMEOBJECTS"])
                    {
                        RaveUtils.CreateObjectMenus(
                            objNode.ObjectInstance.TypeDefinitions, this.mnuAddChild_Click, this.mnuAddChild);
                        ITypeDefinition typeDef =
                            objNode.ObjectInstance.TypeDefinitions.FindTypeDefinition(objNode.ObjectInstance.TypeName);

                        this.mnuAddChild.Visible = true;
                        this.mnuAddChild.Enabled = CanAddChildren(typeDef);
                    }

                    if (objNode.ObjectInstance.Bank.IsCheckedOut)
                    {
                        this.mnuRename.Visible = true;
                        this.mnuRename.Enabled = true;
                    }
                    else if (this.CanCheckInOut)
                    {
                        this.mnuCheckOut.Visible = true;
                        this.mnuCheckOut.Enabled = true;
                        if (this.CanLocalCheckOut)
                        {
                            this.mnuLocalCheckOut.Visible = true;
                            this.mnuLocalCheckOut.Enabled = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The save tree state.
        /// </summary>
        /// <param name="treeNodes">
        /// The tree nodes.
        /// </param>
        private XmlDocument getTreeStateAsXml(TreeNodeCollection treeNodes)
        {
            XmlDocument result = new XmlDocument();
            result.AppendChild(result.CreateElement("State"));
            SaveTreeNodeStructuretoXmlNodes(result.DocumentElement, treeNodes);
            return result;
        }

        /// <summary>
        /// The show details.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        private void ShowDetails(HierarchyNode node)
        {
            ObjectNode objectNode = node as ObjectNode;
            TemplateNode templateNode = node as TemplateNode;

            if (null != objectNode)
            {
                IObjectInstance obj = objectNode.ObjectInstance;
                if (obj == null)
                {
                    this.m_lblObjectName.Text = string.Empty;
                    this.m_lblObjectType.Text = string.Empty;
                    this.m_objectTypeIcon.Image = null;
                }
                else
                {
                    this.m_lblObjectName.Text = obj.Name;
                    this.m_lblObjectType.Text = obj.TypeName;
                    this.m_objectTypeIcon.Image =
                        this.m_ObjectImageList.Images[(int)this.typeIconTable[obj.TypeName]];
                }
            }
            else if (null != templateNode)
            {
                this.m_lblObjectName.Text = string.Empty;
                this.m_lblObjectType.Text = string.Empty;
                this.m_objectTypeIcon.Image = null;
            }

            this.m_detailsPanel.Invalidate();
        }

        /// <summary>
        /// The show hierarchy.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="updateHistory">
        /// The update history.
        /// </param>
        private void ShowObjectInstanceHierarchy(IObjectInstance objectInstance, bool updateHistory)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(
                    new Action<IObjectInstance, bool>(this.ShowObjectInstanceHierarchy),
                    new object[] { objectInstance, updateHistory });
            }
            else
            {
                this.m_treeView.BeginUpdate();
                if (this.currentObject != null)
                {
                    IObjectInstance currObjectInstance = this.currentObject as IObjectInstance;
                    if (null != currObjectInstance)
                    {
                        currObjectInstance.Bank.BankStatusChanged -= this.UpdateHierarchy;
                        currObjectInstance.ReferencesChanged -= this.UpdateHierarchy;
                    }
                }

                this.m_treeView.DoubleClick -= this.TreeView_DoubleClick;
                this.m_treeView.AllowDrop = false;

                this.m_treeView.Nodes.Clear();

                if (objectInstance == null)
                {
                    return;
                }

                this.currentObject = objectInstance;

                this.m_treeView.DoubleClick += this.TreeView_DoubleClick;
                this.m_treeView.AllowDrop = true;

                if (updateHistory)
                {
                    this.UpdateHistory();
                }

                this.m_title = "Hierarchy Viewer";
                this.mnuRename.Visible = false;

                this.InitControls(objectInstance.Name);
                this.InitAnnotationsTree(objectInstance);

                ObjectNode objectNode = new ObjectNode(objectInstance, this.annotationsRoot);
                if (this.typeIconTable.ContainsKey(objectInstance.TypeName))
                {
                    objectNode.SelectedImageIndex =
                        objectNode.ImageIndex = (int)this.typeIconTable[objectInstance.TypeName];
                }
                else
                {
                    objectNode.SelectedImageIndex = objectNode.ImageIndex = 0;
                }

                objectInstance.Bank.BankStatusChanged += this.UpdateHierarchy;
                objectInstance.ReferencesChanged += this.UpdateHierarchy;

                foreach (ObjectRef reference in objectInstance.References)
                {
                    this.AddObjectInstanceChildren(reference.ObjectInstance, reference.Node, objectNode, this.annotationsRoot);
                }

                this.AddNodeToTree(objectNode);
                this.m_treeView.BeforeExpand += TreeView_BeforeExpand;
                m_treeView.EndUpdate();
            }
        }

        private void TreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            e.Node.Nodes.Clear();
            if (e.Node is ObjectNode)
            {
                foreach (ObjectRef reference in ((ObjectNode)e.Node).ObjectInstance.References)
                {
                    this.AddObjectInstanceChildren(reference.ObjectInstance, reference.Node, e.Node, this.annotationsRoot);
                }
            }
        }

        /// <summary>
        /// Show the template hierarchy.
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        /// <param name="updateHistory">
        /// The update history flag.
        /// </param>
        private void ShowTemplateHierarchy(ITemplate template, bool updateHistory)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(
                    new Action<ITemplate, bool>(this.ShowTemplateHierarchy),
                    new object[] { template, updateHistory });
            }
            else
            {
                this.m_treeView.BeginUpdate();
                if (this.currentObject != null)
                {
                    IObjectInstance currObjectInstance = this.currentObject as IObjectInstance;
                    if (null != currObjectInstance)
                    {
                        currObjectInstance.Bank.BankStatusChanged -= this.UpdateHierarchy;
                        currObjectInstance.ReferencesChanged -= this.UpdateHierarchy;
                    }
                }

                this.m_treeView.DoubleClick -= this.TreeView_DoubleClick;
                this.m_treeView.AllowDrop = false;

                this.m_treeView.Nodes.Clear();

                if (template == null)
                {
                    return;
                }

                this.currentObject = template;

                this.m_treeView.DoubleClick += this.TreeView_DoubleClick;
                this.m_treeView.AllowDrop = false;

                if (updateHistory)
                {
                    this.UpdateHistory();
                }

                this.m_title = "Template Hierarchy Viewer";
                this.mnuRename.Visible = true;

                this.InitControls(template.Name);

                ITemplateProxyObject parentProxyObject = template.Parent;
                TemplateNode templateNode = new TemplateNode(parentProxyObject, this.annotationsRoot);

                if (this.typeIconTable.ContainsKey(parentProxyObject.TypeName))
                {
                    templateNode.SelectedImageIndex =
                        templateNode.ImageIndex = (int)this.typeIconTable[parentProxyObject.TypeName];
                }
                else
                {
                    templateNode.SelectedImageIndex = templateNode.ImageIndex = 0;
                }

                this.AddTemplateChildren(parentProxyObject, templateNode, this.annotationsRoot);
                this.AddNodeToTree(templateNode);
                this.m_treeView.EndUpdate();
            }
        }

        /// <summary>
        /// Initialize the property selector context menu.
        /// </summary>
        private void InitPropertySelector()
        {
            if (this.mnuPropertySelector.DropDownItems.Count > 0)
            {
                // We've already initiliazed the menu for current hierarchy.
                return;
            }

            if (null == this.annotationsRoot)
            {
                return;
            }

            IDictionary<string, ISet<string>> allProperties = this.InitPropertySelectorHelper(this.annotationsRoot);

            foreach (KeyValuePair<string, ISet<string>> kvp in allProperties)
            {
                string typeName = kvp.Key;
                List<string> properties = kvp.Value.ToList();
                properties.Sort();

                ToolStripMenuItem subMenu = new ToolStripMenuItem { Text = typeName };
                this.mnuPropertySelector.DropDownItems.Add(subMenu);

                foreach (string property in properties)
                {
                    ToolStripMenuItem menuItem = new ToolStripMenuItem
                    {
                        Text = property,
                        Tag = property,
                        CheckState = this.VisibleProperty == property ? CheckState.Checked : CheckState.Unchecked
                    };

                    menuItem.Click += this.PropertyItemClick;
                    subMenu.DropDownItems.Add(menuItem);
                }
            }
        }

        /// <
        /// <summary>
        /// Initialize the live property selector context menu.
        /// </summary>
        private void InitLivePropertySelector()
        {
            this.mnuLivePropertySelector.DropDownItems.Clear();

            if (null == this.categoriesMessageListener.CategoryProperties
                || !this.categoriesMessageListener.CategoryProperties.Any())
            {
                return;
            }

            this.mnuAllProperties = new ToolStripMenuItem { Text = "All", Tag = "All" };
            this.mnuAllProperties.Click += this.LivePropertyItemClick;
            this.mnuLivePropertySelector.DropDownItems.Add(this.mnuAllProperties);
            this.mnuLivePropertySelector.DropDownItems.Add(new ToolStripSeparator());

            foreach (KeyValuePair<string, float> propertyEntry in this.categoriesMessageListener.CategoryProperties.First().Value)
            {
                string property = propertyEntry.Key;

                ToolStripMenuItem menuItem = new ToolStripMenuItem { Text = property, Tag = property };
                menuItem.Click += this.LivePropertyItemClick;
                this.mnuLivePropertySelector.DropDownItems.Add(menuItem);
            }
        }

        /// <summary>
        /// Helper method to initialize the property selector values.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// The properties <see cref="HashSet{T}"/>.
        /// </returns>
        private IDictionary<string, ISet<string>> InitPropertySelectorHelper(ObjectAnnotationNode node)
        {
            Dictionary<string, ISet<string>> allProperties = new Dictionary<string, ISet<string>>();

            if (null != node.ObjectInstance)
            {
                if (!allProperties.ContainsKey(node.ObjectInstance.Type))
                {
                    allProperties[node.ObjectInstance.Type] = new HashSet<string>();
                }

                ISet<string> properties = allProperties[node.ObjectInstance.Type];

                foreach (ObjectAnnotation annotation in node.Annotations)
                {
                    properties.Add(annotation.Name);
                }
            }

            foreach (ObjectAnnotationNode childNode in node.Children)
            {
                foreach (KeyValuePair<string, ISet<string>> property in this.InitPropertySelectorHelper(childNode))
                {
                    string key = property.Key;
                    ISet<string> childProperties = property.Value;

                    if (allProperties.ContainsKey(key))
                    {
                        foreach (string childProperty in childProperties)
                        {
                            allProperties[key].Add(childProperty);
                        }
                    }
                    else
                    {
                        allProperties[key] = childProperties;
                    }
                }
            }

            return allProperties;
        }

        /// <summary>
        /// Update the history.
        /// </summary>
        private void UpdateHistory()
        {
            // clear out forward history
            while (this.m_history.Count - 1 > this.m_currentHistoryIndex)
            {
                this.m_history.RemoveAt(this.m_history.Count - 1);
            }

            IObjectInstance objectInstance = this.currentObject as IObjectInstance;
            ITemplate template = this.currentObject as ITemplate;

            if (null != objectInstance)
            {
                this.m_history.Add(new HistoryObject(objectInstance.Name, objectInstance.Bank));
            }
            else if (null != template)
            {
                this.m_history.Add(new HistoryObject(template.Name, template.TemplateBank));
            }

            this.m_currentHistoryIndex = this.m_history.Count - 1;
        }

        /// <summary>
        /// Initialize the controls.
        /// </summary>
        /// <param name="name">
        /// The object instance/template name.
        /// </param>
        private void InitControls(string name)
        {
            this.m_toolbar.Visible = this.CanNavigate;
            this.btnExpandAll.Enabled = true;
            this.m_detailsPanel.Visible = this.ShouldAppendType;
            this.btnBack.Enabled = this.m_currentHistoryIndex > 0;
            this.btnForward.Enabled = this.m_currentHistoryIndex < this.m_history.Count - 1;
            this.btnSetSnapback.Enabled = this.m_snapbackObject != name;
            this.btnSnapBack.Enabled = !string.IsNullOrEmpty(this.m_snapbackObject)
                                       && this.m_snapbackObject != name;
            this.Text = this.m_title + " - " + name;
        }

        /// <summary>
        /// Initialize the annotations tree.
        /// </summary>
        /// <param name="objectInstance">
        /// The object instance.
        /// </param>
        /// <param name="parent">
        /// The parent annotation.
        /// </param>
        private void InitAnnotationsTree(IObjectInstance objectInstance, ObjectAnnotationNode parent = null)
        {
            // Reset annotation root if first iteration
            if (null == parent)
            {
                this.annotationsRoot = null;
            }

            if (null == objectInstance)
            {
                return;
            }

            ObjectAnnotationNode annotation = new ObjectAnnotationNode(parent, objectInstance);
            if (null == parent)
            {
                this.annotationsRoot = annotation;
            }

            foreach (ObjectRef objectRef in objectInstance.References)
            {
                this.InitAnnotationsTree(objectRef.ObjectInstance, annotation);
            }
        }

        /// <summary>
        /// Add a node to the tree.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private void AddNodeToTree(HierarchyNode node)
        {
            //do not fire after selction event on tree creation
            this.m_treeView.AfterSelect -= TreeView_AfterSelect;

            this.m_treeView.Nodes.Add(node);

            if (this.ShouldExpandAll)
            {
                this.m_treeView.ExpandAll();
            }
            else
            {
                // expand 1 level
                this.m_treeView.Nodes[0].Expand();
            }

            this.DoCollapse(this.m_treeView.Nodes);

            this.m_treeView.AfterSelect += TreeView_AfterSelect;
        }

        /// <summary>
        /// The toolbar_ button click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Toolbar_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            switch ((string)e.Button.Tag)
            {
                case "back":
                    if (this.m_currentHistoryIndex > 0)
                    {
                        HistoryObject hb = this.m_history[--this.m_currentHistoryIndex];
                        if (null == hb.GetObject())
                        {
                            this.m_history.RemoveRange(0, this.m_currentHistoryIndex + 1);
                            this.m_currentHistoryIndex = 0;
                        }
                        else
                        {
                            this.ShowHierarchy(hb.GetObject(), false);
                        }
                    }
                    break;

                case "forward":
                    if (this.m_currentHistoryIndex < this.m_history.Count - 1)
                    {
                        HistoryObject hb = this.m_history[++this.m_currentHistoryIndex];
                        if (null == hb.GetObject())
                        {
                            this.m_history.RemoveRange(
                                this.m_currentHistoryIndex, this.m_history.Count - this.m_currentHistoryIndex);
                            this.m_currentHistoryIndex = this.m_history.Count - 1;
                        }

                        this.ShowHierarchy(hb.GetObject(), false);
                    }

                    break;

                case "snapback":
                    this.ShowHierarchy(this.m_snapBackObjectLookUp[this.m_snapbackObject]);
                    break;

                case "setsnapback":
                    this.btnSetSnapback.Enabled = this.btnSnapBack.Enabled = false;

                    IObjectInstance objectInstance = this.currentObject as IObjectInstance;
                    ITemplate template = this.currentObject as ITemplate;
                    if (null != objectInstance)
                    {
                        this.m_snapbackObject = objectInstance.Name;
                        this.m_snapBackObjectLookUp = objectInstance.ObjectLookup;
                    }
                    else if (null != template)
                    {
                        this.m_snapbackObject = template.Name;
                        this.m_snapBackObjectLookUp = null;
                    }
                    else
                    {
                        ErrorManager.HandleError("Object type not recognised");
                    }

                    break;

                case "expandall":
                    this.m_treeView.ExpandAll();
                    break;

                case "collapse":
                    this.btCollapse.Pushed = !this.m_collapse;
                    this.CollapseNames(!this.m_collapse);
                    break;
                case "autoUpdate":
                    AutoUpdate = !AutoUpdate;
                    this.btnAutoUpdate.ImageIndex = AutoUpdate ? 7 : 6;
                    break;
            }
        }

        /// <summary>
        /// The tree view_ after select.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private bool initialSelection = true;

        private void TreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            HierarchyNode shn = this.m_treeView.SelectedNode as HierarchyNode;
            if (this.m_treeView.SelectedNode != null && !this.m_inDragMode)
            {
                if (!initialSelection)
                {
                    this.NodeSelected.Raise(shn);
                }
                else
                {
                    initialSelection = false;
                }
            }
        }

        /// <summary>
        /// The tree view_ double click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DoubleClick(object sender, EventArgs e)
        {
            if (!this.CanNavigate)
            {
                return;
            }

            if (null == this.m_treeView.SelectedNode)
            {
                // double click white space to go back
                if (this.m_currentHistoryIndex > 0)
                {
                    HistoryObject hb = this.m_history[--this.m_currentHistoryIndex];
                    this.ShowHierarchy(hb.GetObject(), false);
                }
            }
            else
            {
                ObjectNode objectNode = this.m_treeView.SelectedNode as ObjectNode;
                if (null != objectNode)
                {
                    this.ShowHierarchy(objectNode.ObjectInstance);
                }
            }
        }

        /// <summary>
        /// The tree view_ drag drop.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DragDrop(object sender, DragEventArgs e)
        {
            m_treeView.BeginUpdate();
            XmlDocument treeState = this.getTreeStateAsXml(this.m_treeView.Nodes);
            this.m_inDragMode = false;
            try
            {
                ObjectNode objectNode = this.m_treeView.SelectedNode as ObjectNode;
                if (null != objectNode)
                {
                    this.HandleDropOnObject(objectNode, e.Data);
                }

                this.ShowHierarchy(this.currentObject);
            }
            catch (Exception ex)
            {
                ErrorManager.HandleError(ex);
            }

            this.ExpandTree(treeState, this.m_treeView.Nodes);
            m_treeView.EndUpdate();
        }

        /// <summary>
        /// Handle dropped node onto object node.
        /// </summary>
        /// <param name="objectNode">
        /// The destination object node.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        private void HandleDropOnObject(ObjectNode objectNode, IDataObject data)
        {
            if (null == objectNode)
            {
                return;
            }

            IObjectBank bank = objectNode.ObjectInstance.Bank;
            if (!bank.IsCheckedOut && !bank.IsLocalCheckout)
            {
                if (!objectNode.ObjectInstance.Bank.TryToCheckout())
                {
                    return;
                }

                // Need to find updated version of ObjectInstance, as the bank is usually
                // reloaded during checkout, and we will need to update the ref from ObjectNode
                IObjectInstance updateObjInstance;
                if (
                    !objectNode.ObjectInstance.Bank.ObjectLookup.TryGetValue(
                        objectNode.ObjectInstance.Name, out updateObjInstance))
                {
                    ErrorManager.HandleError("Failed to find object instance in reloaded bank: " + objectNode.ObjectInstance.Name);
                    return;
                }

                this.InitAnnotationsTree(updateObjInstance);
                objectNode.ObjectInstance = updateObjInstance;
                objectNode.AnnotationNode = this.annotationsRoot;
            }

            if (data.GetDataPresent(typeof(SoundNode)))
            {
                IObjectInstance s = ((SoundNode)
                     data.GetData(typeof(SoundNode))).ObjectInstance;
                objectNode.ObjectInstance.HandleDroppedObject(s, s.TypeDefinitions);
            }
            else if (data.GetDataPresent(typeof(SoundNode[])))
            {
                SoundNode[] sounds =
                    (SoundNode[])
                    data.GetData(typeof(SoundNode[]));
                foreach (SoundNode s in sounds)
                {
                    objectNode.ObjectInstance.HandleDroppedObject(
                        s.ObjectInstance, s.ObjectInstance.TypeDefinitions);
                }
            }
            else if (data.GetDataPresent(typeof(WaveNode)))
            {
                WaveNode w = (WaveNode)data.GetData(typeof(WaveNode));
                objectNode.ObjectInstance.HandleDroppedWave(w);
            }
            else if (data.GetDataPresent(typeof(WaveNode[])))
            {
                if (objectNode.ObjectInstance.Type.ToUpper() != "SOUNDS")
                {
                    ErrorManager.HandleInfo("Can only perform this action on sounds");
                    return;
                }

                WaveNode[] waves = (WaveNode[])data.GetData(typeof(WaveNode[]));
                foreach (WaveNode w in waves)
                {
                    objectNode.ObjectInstance.HandleDroppedWave(w);
                }
            }
            else if (data.GetDataPresent(typeof(ObjectRefNode)))
            {
                ObjectRefNode draggedNode = (ObjectRefNode)data.GetData(typeof(ObjectRefNode));
                if (objectNode != draggedNode.Parent && objectNode != draggedNode)
                {
                    if (objectNode.ObjectInstance.Bank.IsCheckedOut)
                    {
                        if (
                            objectNode.ObjectInstance.HandleDroppedObject(
                                draggedNode.ObjectReference.ObjectInstance,
                                draggedNode.ObjectReference.ObjectInstance.TypeDefinitions))
                        {
                            ObjectNode parentObjectNode = draggedNode.Parent as ObjectNode;
                            if (null != parentObjectNode)
                            {
                                RemoveReference(draggedNode.ObjectReference, parentObjectNode.ObjectInstance);
                            }

                            draggedNode.Remove();
                        }
                    }
                    else
                    {
                        string bankName = !objectNode.ObjectInstance.Bank.IsCheckedOut ?
                                    objectNode.ObjectInstance.Bank.Name :
                                    draggedNode.ObjectInstance.Bank.Name;

                        ErrorManager.HandleInfo("Bank '" + bankName + "' must be checked out");
                    }
                }
            }
        }

        private void ApplyAlphabeticalSort(bool alphabetical)
        {
            this.m_treeView.Sorted = alphabetical;
        }

        /// <summary>
        /// Show property.
        /// </summary>
        /// <param name="property">
        /// The property.
        /// </param>
        private void ShowProperty(string property)
        {
            this.m_treeView.BeginUpdate();
            this.VisibleProperty = property;
            this.m_treeView.Refresh();
            this.m_treeView.EndUpdate();
        }

        /// <summary>
        /// Draw the tree node text.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            if (!e.Node.IsVisible) return;

            if ((e.State & TreeNodeStates.Selected) != 0)
            {
                Rectangle bounds = e.Bounds;
                bounds.Width = Convert.ToInt32(e.Graphics.MeasureString(e.Node.Text, e.Node.NodeFont).Width);
                bounds.Height = Convert.ToInt32(e.Graphics.MeasureString(e.Node.Text, e.Node.NodeFont).Height);
                e.Graphics.FillRectangle(Brushes.White, e.Bounds);
                e.Graphics.FillRectangle(SystemBrushes.Highlight, bounds);
            }

            switch (this.PropertyViewMode)
            {
                case PropertyViewMode.Single:
                    {
                        this.DrawNodeSingleProperty(e);
                        break;
                    }

                case PropertyViewMode.Inline:
                    {
                        this.DrawNodeInlineProperties(e);
                        break;
                    }

                case PropertyViewMode.Nested:
                    {
                        this.DrawNodeNestedProperties(e);
                        break;
                    }
            }
        }

        /// <summary>
        /// Draw node with single property.
        /// </summary>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void DrawNodeSingleProperty(DrawTreeNodeEventArgs e)
        {
            ObjectNode objNode = e.Node as ObjectNode;

            if (null == objNode)
            {
                e.DrawDefault = true;
                return;
            }

            Rectangle bounds = e.Bounds;
            if (objNode.NodeFont == null)
            {
                objNode.NodeFont = TreeView.DefaultFont;
            }
            bounds.Width = Convert.ToInt32(e.Graphics.MeasureString(objNode.ObjectName, objNode.NodeFont).Width) + 10;

            e.Graphics.DrawString(objNode.ObjectName, objNode.NodeFont, objNode.GetBrush(), bounds);

            if (string.IsNullOrEmpty(this.VisibleProperty))
            {
                return;
            }

            ObjectAnnotation annotation = objNode.AnnotationNode.GetAnnotation(this.VisibleProperty);
            if (null == annotation)
            {
                return;
            }

            Brush brush = annotation.IsDefaultValue ? Brushes.Green : Brushes.Red;
            e.Graphics.DrawString(
                annotation.FormattedValue,
                TreeView.DefaultFont,
                brush,
                bounds.Left + bounds.Width,
                bounds.Top);
        }

        /// <summary>
        /// Draw node with inline properties.
        /// </summary>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void DrawNodeInlineProperties(DrawTreeNodeEventArgs e)
        {
            ObjectNode objNode = e.Node as ObjectNode;

            if (null == objNode)
            {
                e.DrawDefault = true;
                return;
            }

            List<ObjectAnnotation> annotations = this.visibleLiveProperties.Select(
                property => objNode.AnnotationNode.GetGameAnnotation(property)).Where(
                annotation => null != annotation).ToList();

            if (objNode.NodeFont == null)
            {
                objNode.NodeFont = TreeView.DefaultFont;
            }

            Rectangle bounds = e.Bounds;
            bounds.Width = Convert.ToInt32(e.Graphics.MeasureString(objNode.ObjectName, objNode.NodeFont).Width) + 10;
            e.Graphics.DrawString(objNode.ObjectName, objNode.NodeFont, objNode.GetBrush(), bounds);

            StringBuilder annotationBuilder = new StringBuilder();
            foreach (ObjectAnnotation annotation in annotations)
            {
                if (annotationBuilder.Length == 0)
                {
                    annotationBuilder.Append(annotation.FormattedValue);
                }
                else
                {
                    annotationBuilder.AppendFormat(", {0}", annotation.FormattedValue);
                }
            }

            string annotationText = annotationBuilder.ToString();

            e.Graphics.DrawString(
                annotationText,
                TreeView.DefaultFont,
                Brushes.Gray,
                bounds.Left + bounds.Width,
                bounds.Top);
        }

        /// <summary>
        /// Draw node with nested properties.
        /// </summary>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void DrawNodeNestedProperties(DrawTreeNodeEventArgs e)
        {
            AnnotationNode annotationNode = e.Node as AnnotationNode;

            if (null == annotationNode)
            {
                e.DrawDefault = true;
                return;
            }

            string text = string.Format("{0}: ", annotationNode.Property);

            Rectangle bounds = e.Bounds;
            bounds.Width = Convert.ToInt32(e.Graphics.MeasureString(text, TreeView.DefaultFont).Width) + 10;
            e.Graphics.DrawString(text, TreeView.DefaultFont, SystemBrushes.WindowText, bounds);

            Brush brush = annotationNode.IsDefaultValue ? Brushes.Green : Brushes.Red;
            e.Graphics.DrawString(
                annotationNode.Value,
                TreeView.DefaultFont,
                brush,
                bounds.Left + bounds.Width,
                bounds.Top);
        }

        /// <summary>
        /// The tree view_ drag enter.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
            this.m_inDragMode = true;
        }

        /// <summary>
        /// The tree view_ drag leave.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DragLeave(object sender, EventArgs e)
        {
            this.m_inDragMode = false;
        }

        /// <summary>
        /// The tree view_ drag over.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
            Point p = this.m_treeView.PointToClient(new Point(e.X, e.Y));
            this.m_treeView.SelectedNode = this.m_treeView.GetNodeAt(p.X, p.Y);
        }

        /// <summary>
        /// The tree view_ item drag.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            DataObject dataObject = new DataObject();
            dataObject.SetData(this.m_treeView.SelectedNode);
            dataObject.SetData(new rage.ToolLib.DataContainer(this.m_treeView.SelectedNode));
            this.DoDragDrop(dataObject, DragDropEffects.Move);
        }

        /// <summary>
        /// The tree view_ key down.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_KeyDown(object sender, KeyEventArgs e)
        {
            if (m_treeView != null && m_treeView.Nodes.Count > 0)
            {
                switch (e.KeyCode)
                {
                    case Keys.Space:
                        {
                            if (!e.Control)
                            {
                                this.NotifyAudition();
                            }
                            else
                            {
                                if (this.StopAuditionObject != null)
                                    this.NotifyAuditionStop();
                            }
                            break;
                        }

                    case Keys.Delete:
                        {
                            this.HandleDelete();
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// The tree view_ mouse down.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_MouseDown(object sender, MouseEventArgs e)
        {
            TreeNode n = this.m_treeView.GetNodeAt(e.X, e.Y);
            if (n != null)
            {
                n.EnsureVisible();

                // only update selection if the user clicks on a node
                this.m_treeView.SelectedNode = n;
            }
        }

        /// <summary>
        /// The tree view_ mouse move.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TreeView_MouseMove(object sender, MouseEventArgs e)
        {
            TreeNode tn = this.m_treeView.GetNodeAt(e.X, e.Y);
            if (tn != null)
            {
                HierarchyNode sn = tn as HierarchyNode;
                if (null != sn)
                {
                    this.ShowDetails(sn);
                }
                else
                {
                    this.ShowDetails(null);
                }
            }
            else
            {
                this.ShowDetails(null);
            }
        }

        /// <summary>
        /// The update hierarchy.
        /// </summary>
        private void UpdateHierarchy()
        {
            if (this.InvokeRequired)
            {
                try
                {
                    this.Invoke(new Action(this.UpdateHierarchy));
                }
                catch (Exception e)
                {
                    ErrorManager.WriteToLog("Error", e.Message, e.StackTrace);
                }
            }
            else
            {
                if (RaveInstance.MainWindow.IsActive)
                {
                    this.m_treeView.SuspendLayout();
                    XmlDocument treeState = this.getTreeStateAsXml(this.m_treeView.Nodes);
                    string previouslySelectedNodeFullPath = this.m_treeView.SelectedNode == null ? null : m_treeView.SelectedNode.FullPath;
                    int previouslySelectedNodeIndex = this.m_treeView.SelectedNode == null ? 0 : m_treeView.SelectedNode.Index;
                    Point previousScrollPosition = m_treeView.GetScrollPos();

                    IObjectInstance objectInstance = this.currentObject as IObjectInstance;
                    ITemplate template = this.currentObject as ITemplate;

                    if (null != objectInstance)
                    {
                        IObjectInstance newObject = objectInstance.Bank.FindObjectInstance(objectInstance.Name);
                        this.ShowHierarchy(newObject, false);
                    }
                    else if (null != template)
                    {
                        ITemplate newTemplate = template.TemplateBank.FindTemplate(template.Name);
                        this.ShowHierarchy(newTemplate, false);
                    }

                    this.ExpandTree(treeState, this.m_treeView.Nodes);
                    if (previouslySelectedNodeFullPath != null)
                    {
                        //do not raise event on automatic reselect
                        this.m_treeView.AfterSelect -= this.TreeView_AfterSelect;
                        selectTreeNode(previouslySelectedNodeFullPath, previouslySelectedNodeIndex, this.m_treeView.Nodes);
                        this.m_treeView.AfterSelect += this.TreeView_AfterSelect;
                    }
                    this.m_treeView.SetScrollPos(previousScrollPosition);

                    this.objectInstanceRefreshPending = false;
                    this.m_treeView.ResumeLayout();
                }
                else
                {
                    this.objectInstanceRefreshPending = true;
                }
            }
        }

        private bool selectTreeNode(string fullPath, int index, TreeNodeCollection nodes)
        {
            foreach (TreeNode n in nodes)
            {
                if (n.FullPath == fullPath)
                {
                    if (n.Index == index)
                    {
                        this.m_treeView.SelectedNode = n;
                        return true;
                    }
                }
                if (selectTreeNode(fullPath, index, n.Nodes)) return true;
            }
            return false;
        }

        /// <summary>
        /// The m_ menu_ collapse.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void m_Menu_Collapse(object sender, ToolStripDropDownClosingEventArgs e)
        {
            // update menu options to allow shortcuts
            this.mnuInsertWrapper.Enabled = this.CanInsertWrapper;
            this.mnuMakeLocal.Enabled = this.CanMakeLocal;
            this.mnuRemoveRef.Enabled = this.CanRemoveReference;
            this.mnuFindInBrowser.Enabled = this.CanFindInBrowser;
        }

        /// <summary>
        /// The mnu add child_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuAddChild_Click(object sender, EventArgs e)
        {

            m_treeView.BeginUpdate();
            try
            {
                XmlDocument treeState = this.getTreeStateAsXml(this.m_treeView.Nodes);

                IObjectInstance parentObject = this.m_treeView.SelectedNode.GetType() == typeof(ObjectNode)
                    ? ((ObjectNode)this.m_treeView.SelectedNode).ObjectInstance
                    : ((ObjectRefNode)this.m_treeView.SelectedNode).ObjectReference
                        .ObjectInstance;
                IObjectInstance newObject = null;

                ToolStripMenuItem m = (ToolStripMenuItem)sender;

                ITypeDefinition objectDef = m == this.mnuAddChild
                    ? parentObject.TypeDefinitions.ObjectTypes[0]
                    : parentObject.TypeDefinitions.FindTypeDefinition(m.Text);

                string virtualFolder = parentObject.VirtualFolderName ?? parentObject.Name + "_COMPONENTS";

                string parentObjName = parentObject.Name;
                IObjectBank parentBank = parentObject.Bank;

                if (parentBank.TryToCheckout())
                {
                    // need to grab parentObject again since it'll have changed if the object store was just checked out
                    parentObject = parentBank.FindObjectInstance(parentObjName);
                    if (parentObject != null)
                    {
                        frmSimpleInput.InputResult input = frmSimpleInput.GetInput(
                            this.ParentForm, m.Text + " name", parentObject.Name + "_");

                        if (input.HasBeenCancelled) return;

                        string objectName = input.data;

                        while (objectName != null && parentObject.ObjectLookup.ContainsKey(objectName))
                        {
                            input = frmSimpleInput.GetInput(
                                this.ParentForm, "Name in use, " + m.Text + " name", objectName);
                            if (input.HasBeenCancelled) return;

                            objectName = input.data;
                        }

                        if (objectName != null)
                        {
                            newObject = TypeUtils.GenerateNewObjectInstance(
                                parentObject.Bank, objectName, virtualFolder, parentObject.ObjectLookup, objectDef);
                            parentObject.HandleDroppedObject(newObject, newObject.TypeDefinitions);
                            ShowNode(newObject);
                        }
                    }
                }


                this.ExpandTree(treeState, this.m_treeView.Nodes);
                FindNodeWithObject(parentObject, this.m_treeView.Nodes).Expand();
                if (newObject != null)
                { ShowNode(newObject); }
            }
            finally
            {
                m_treeView.EndUpdate();
            }


        }

        /// <summary>
        /// The mnu check out_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuCheckOut_Click(object sender, EventArgs e)
        {
            this.visibleLiveProperties.Clear();
            ObjectNode objectNode = this.m_treeView.SelectedNode as ObjectNode;
            if (objectNode != null && objectNode.ObjectInstance != null)
            {
                FileStatus bankStatus = objectNode.ObjectInstance.Bank.GetLockedStatus();
                if (bankStatus != null && bankStatus.IsLocked && !bankStatus.IsLockedByMe)
                {
                    ErrorManager.HandleInfo("This bank is currently locked by " + bankStatus.Owner);
                }
                else
                {
                    if (objectNode.ObjectInstance.Bank.Checkout(false))
                    {
                        this.ChangeListModified.Raise();
                    }
                    else
                    {
                        ErrorManager.HandleInfo("Check out of " + objectNode.ObjectInstance.Bank.Name + " failed");
                    }
                }
            }
        }

        /// <summary>
        /// The mnu delete_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuDelete_Click(object sender, EventArgs e)
        {
            this.HandleDelete();
        }

        /// <summary>
        /// The mnu find in browser_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuFindInBrowser_Click(object sender, EventArgs e)
        {
            if (this.m_treeView.SelectedNode != null)
            {
                ObjectNode objectNode = this.m_treeView.SelectedNode as ObjectNode;
                TemplateNode templateNode = this.m_treeView.SelectedNode as TemplateNode;

                if (null != objectNode)
                {
                    this.FindObject.Raise(objectNode.ObjectInstance);
                }
                else if (null != templateNode)
                {
                    this.FindTemplate.Raise(templateNode.ProxyObject);
                }
            }
        }

        /// <summary>
        /// The mnu insert wrapper_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuInsertWrapper_Click(object sender, EventArgs e)
        {
            ObjectRefNode srn = this.m_treeView.SelectedNode as ObjectRefNode;
            if (srn == null)
            {
                return;
            }

            ObjectRef sref = ((ObjectRefNode)this.m_treeView.SelectedNode).ObjectReference;

            ObjectNode parentObjectNode = srn.Parent as ObjectNode;
            if (null != parentObjectNode)
            {
                IObjectInstance parentObject = parentObjectNode.ObjectInstance;

                if (parentObject != null)
                {
                    if (!parentObject.Type.Equals(sref.ObjectInstance.Type))
                    {
                        ErrorManager.HandleInfo(
                            "Cannot insert wrapper as the the types of the object and parent differ");
                        return;
                    }

                    if (parentObject.Bank.IsCheckedOut)
                    {
                        IObjectInstance newObject = TypeUtils.CreateSoundWrapperObject(
                            sref.ObjectInstance, parentObject.Bank);
                        string virtualFolder;

                        if (parentObject.VirtualFolderName != null)
                        {
                            virtualFolder = parentObject.VirtualFolderName;
                        }
                        else
                        {
                            virtualFolder = parentObject.Name + "_COMPONENTS";
                        }

                        newObject.VirtualFolderName = virtualFolder;
                        TypeUtils.ReportNewObjectCreated(newObject);

                        XmlNode n = sref.Node;
                        sref.ObjectInstance.RemoveReferencer(parentObject);
                        int index = parentObject.FindReferenceListIndex(sref.ObjectInstance, sref.Node);
                        parentObject.RemoveReference(sref.ObjectInstance, sref.Node);
                        n.InnerText = newObject.Name;
                        newObject.AddReferencer(parentObject);
                        if (index != int.MaxValue)
                        {
                            parentObject.AddReference(newObject, n, index);
                        }
                        else
                        {
                            parentObject.AddReference(newObject, n);
                        }

                        parentObject.MarkAsDirty();
                    }
                    else
                    {
                        ErrorManager.HandleInfo("Bank '" + parentObject.Bank.Name + "' must be checked out");
                    }
                }
                else
                {
                    ErrorManager.HandleInfo("Object has no parent to insert wrapper into");
                }
            }
            else
            {
                ErrorManager.HandleInfo("Object has no parent to insert wrapper into");
            }
        }

        /// <summary>
        /// The mnu local check out_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuLocalCheckOut_Click(object sender, EventArgs e)
        {
            this.visibleLiveProperties.Clear();
            ObjectNode objectNode = this.m_treeView.SelectedNode as ObjectNode;

            if (objectNode != null)
            {
                if (!objectNode.ObjectInstance.Bank.Checkout(true))
                {
                    ErrorManager.HandleInfo("Check out of " + objectNode.ObjectInstance.Bank.Name + " failed");
                }

                this.ChangeListModified.Raise();
            }
        }

        /// <summary>
        /// The mnu make local_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuMakeLocal_Click(object sender, EventArgs e)
        {
            ObjectRefNode srn = this.m_treeView.SelectedNode as ObjectRefNode;
            if (srn == null)
            {
                return;
            }

            ObjectRef sref = ((ObjectRefNode)this.m_treeView.SelectedNode).ObjectReference;

            ObjectNode parentNode = srn.Parent as ObjectNode;
            if (null == parentNode)
            {
                return;
            }

            IObjectInstance parentObject = parentNode.ObjectInstance;

            /*only create local if parent is of same type i.e don't create a local
                copy of a referenced child object inside a game object store*/
            if (parentObject == null || parentObject.Type != srn.ObjectInstance.Type)
            {
                ErrorManager.HandleInfo("Cannot make object local as the the types of the object and parent differ");
                return;
            }

            if (
                ErrorManager.HandleQuestion(
                    "This will create a copy of the referenced object and update this reference to point to that - "
                    + "are you sure you wish to do this?",
                    MessageBoxButtons.YesNo,
                    DialogResult.Yes) != DialogResult.Yes)
            {
                return;
            }

            if (!parentObject.Bank.IsCheckedOut)
            {
                ErrorManager.HandleInfo("Bank '" + parentObject.Bank.Name + "' must be checked out");
                return;
            }

            string name = sref.ObjectInstance.Name + "_";
            do
            {
                frmSimpleInput.InputResult input = frmSimpleInput.GetInput(this.ParentForm, "New object name", name);
                if (input.HasBeenCancelled) return;
                name = input.data;
            }
            while (parentObject.ObjectLookup.ContainsKey(name));

            // new object should be created in parent bank
            TypeUtils.MakeReferenceLocal(sref, parentObject, name, parentObject.VirtualFolderName);
            parentObject.MarkAsDirty();
        }

        /// <summary>
        /// The mnu move down_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuMoveDown_Click(object sender, EventArgs e)
        {
            if (AlphabeticalSorted)
            {
                return;
            }
            ObjectRefNode srn = this.m_treeView.SelectedNode as ObjectRefNode;

            if (srn != null)
            {
                int nodeIndex = GetChildNodeIndex(srn);

                if (nodeIndex < srn.Parent.Nodes.Count - 1)
                {
                    ObjectRefNode selNode = (ObjectRefNode)this.m_treeView.SelectedNode;
                    ObjectRefNode swapNode = (ObjectRefNode)selNode.Parent.Nodes[selNode.Parent.Nodes.IndexOf(selNode) + 1];

                    SwapChildNodes(swapNode, selNode);
                }
            }
        }

        /// <summary>
        /// The mnu move up_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuMoveUp_Click(object sender, EventArgs e)
        {
            if (AlphabeticalSorted)
            {
                return;
            }
            ObjectRefNode srn = this.m_treeView.SelectedNode as ObjectRefNode;

            if (srn != null)
            {
                int nodeIndex = GetChildNodeIndex(srn);

                if (nodeIndex >= 1)
                {
                    ObjectRefNode selNode = (ObjectRefNode)this.m_treeView.SelectedNode;
                    ObjectRefNode swapNode = (ObjectRefNode)selNode.Parent.Nodes[selNode.Parent.Nodes.IndexOf(selNode) - 1];

                    SwapChildNodes(selNode, swapNode);
                }
            }
        }

        /// <summary>
        /// The mnu plugin_ on click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuPlugin_OnClick(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if (null == menuItem)
            {
                return;
            }

            string name = menuItem.Text;
            foreach (IRAVEEditorPlugin plugin in this.m_externalEditors)
            {
                if (plugin.GetName() == name)
                {
                    plugin.EditNodes(this.m_treeView);
                }
            }
        }

        /// <summary>
        /// The mnu remove ref_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuRemoveRef_Click(object sender, EventArgs e)
        {
            ObjectRefNode objectRefNode = this.m_treeView.SelectedNode as ObjectRefNode;
            if (null == objectRefNode)
            {
                return;
            }

            ObjectNode parentObjectNode = objectRefNode.Parent as ObjectNode;
            if (null != parentObjectNode)
            {
                RemoveReference(objectRefNode.ObjectReference, parentObjectNode.ObjectInstance);
            }
        }

        /// <summary>
        /// The mnu rename_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mnuRename_Click(object sender, EventArgs e)
        {
            this.TryToRename();
        }

        /// <summary>
        /// The property item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void PropertyItemClick(object sender, EventArgs e)
        {
            this.PropertyViewMode = PropertyViewMode.Single;

            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if (null == menuItem)
            {
                return;
            }

            foreach (ToolStripMenuItem dropDownItem in this.mnuPropertySelector.DropDownItems)
            {
                dropDownItem.CheckState = CheckState.Unchecked;
            }

            string property = menuItem.Tag as string;
            if (string.IsNullOrEmpty(property) || property == this.VisibleProperty)
            {
                this.ShowProperty(null);
            }
            else
            {
                this.ShowProperty(property);
                menuItem.CheckState = CheckState.Checked;
            }
        }

        /// <summary>
        /// The live property item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void LivePropertyItemClick(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if (null == menuItem)
            {
                return;
            }

            if (menuItem == this.mnuAllProperties)
            {
                this.ViewAllLiveProperties(menuItem);
            }
            else
            {
                this.ViewSelectedLiveProperties(menuItem);
            }

            if (visibleLiveProperties.Count() > 0)
            {
                this.m_treeView.DrawMode = TreeViewDrawMode.OwnerDrawText;
            }
            else
            {
                this.m_treeView.DrawMode = TreeViewDrawMode.Normal;
            }
        }

        /// <summary>
        /// View all live properties.
        /// </summary>
        /// <param name="menuItem">
        /// The menu item.
        /// </param>
        private void ViewAllLiveProperties(ToolStripMenuItem menuItem)
        {
            this.PropertyViewMode = PropertyViewMode.Nested;
            menuItem.CheckState = menuItem.CheckState == CheckState.Checked
                                      ? CheckState.Unchecked
                                      : CheckState.Checked;

            foreach (object item in this.mnuLivePropertySelector.DropDownItems)
            {
                ToolStripMenuItem currItem = item as ToolStripMenuItem;
                if (null == currItem)
                {
                    continue;
                }

                string tag = currItem.Tag as string;
                if (null == tag || tag == "All")
                {
                    continue;
                }

                if (menuItem.CheckState == CheckState.Checked)
                {
                    this.visibleLiveProperties.Add(tag);
                    currItem.CheckState = CheckState.Unchecked;
                }
                else
                {
                    this.visibleLiveProperties.Remove(tag);
                }
            }
        }

        /// <summary>
        /// View selected live properties.
        /// </summary>
        /// <param name="menuItem">
        /// The menu item.
        /// </param>
        private void ViewSelectedLiveProperties(ToolStripMenuItem menuItem)
        {
            if (this.mnuAllProperties.CheckState == CheckState.Checked)
            {
                this.visibleLiveProperties.Clear();
                this.mnuAllProperties.CheckState = CheckState.Unchecked;
            }

            string property = menuItem.Tag as string;
            if (string.IsNullOrEmpty(property))
            {
                return;
            }

            if (menuItem.CheckState == CheckState.Checked)
            {
                this.visibleLiveProperties.Remove(property);
                menuItem.CheckState = CheckState.Unchecked;
            }
            else
            {
                this.visibleLiveProperties.Add(property);
                menuItem.CheckState = CheckState.Checked;
            }

            this.PropertyViewMode = this.visibleLiveProperties.Count < 3
                ? PropertyViewMode.Inline
                : PropertyViewMode.Nested;
        }

        /// <summary>
        /// Expand all live properties click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ExpandAllLivePropertiesClick(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if (null == menuItem)
            {
                return;
            }

            bool expand = menuItem.CheckState != CheckState.Checked;

            this.ExpandPropertyNodes(this.m_treeView.Nodes, expand);

            menuItem.CheckState = expand ? CheckState.Checked : CheckState.Unchecked;
        }

        private void mnuMute_Click(object sender, EventArgs e)
        {
            ObjectRefNode objectRefNode = this.m_treeView.SelectedNode as ObjectRefNode;
            if (objectRefNode != null)
            {
                MuteSoloManager.Instance.MuteToggle(objectRefNode.ObjectInstance);
            }
        }

        private void mnuSolo_Click(object sender, EventArgs e)
        {
            ObjectRefNode objectRefNode = this.m_treeView.SelectedNode as ObjectRefNode;
            if (objectRefNode != null)
            {
                MuteSoloManager.Instance.SoloToggle(objectRefNode.ObjectInstance);
            }
        }

        #endregion Methods

    }
}
