﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.AvalonDock.Controls;

namespace Rave.HierarchyBrowser.CategoriesBrowser
{
    /// <summary>
    /// Interaction logic for CategoryDeleteView.xaml
    /// </summary>
    public partial class CategoryDeleteView : Window
    {
        public CategoryDeleteView()
        {
            InitializeComponent();
        }

        public CategoryDeleteView(CategoryDeleteViewModel categoryDelete)
        {
            InitializeComponent();
            this.DataContext = categoryDelete;
           
        }


        private void CloseWindow(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
