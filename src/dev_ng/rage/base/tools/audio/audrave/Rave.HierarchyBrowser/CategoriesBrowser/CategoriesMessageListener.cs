﻿// -----------------------------------------------------------------------
// <copyright file="CategoriesMessageListener.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rave.HierarchyBrowser.CategoriesBrowser
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;

    using rage.ToolLib;

    using Rave.HierarchyBrowser.Infrastructure.Interfaces;
    using Rave.Instance;
    using Rave.RemoteControl.Infrastructure.Enums;
    using Rave.TypeDefinitions.Infrastructure.Interfaces;
    using Rave.Types.Infrastructure.Interfaces.Objects;
    using Rave.Types.Infrastructure.Interfaces.Objects.TypedObjects;
    using Rave.Types.Infrastructure.Structs;

    using BinaryReader = rage.ToolLib.Reader.BinaryReader;

    /// <summary>
    /// Categories message listener.
    /// </summary>
    public class CategoriesMessageListener : ICategoriesMessageListener
    {
        /// <summary>
        /// The lookup.
        /// </summary>
        private readonly IDictionary<string, IObjectInstance> lookup;

        /// <summary>
        /// The category name hashes.
        /// </summary>
        private readonly IDictionary<uint, string> categoryNameHashes;

        /// <summary>
        /// The property name hashes.
        /// </summary>
        private readonly IDictionary<uint, string> propertyNameHashes;

        /// <summary>
        /// The buffer.
        /// </summary>
        private byte[] buffer;

        /// <summary>
        /// The category names.
        /// </summary>
        private string[] categoryNames;

        /// <summary>
        /// The property names.
        /// </summary>
        private string[] propertyNames;

        /// <summary>
        /// The static category count.
        /// </summary>
        private uint staticCategoryCount;

        /// <summary>
        /// The property count.
        /// </summary>
        private uint propertyCount;

        /// <summary>
        /// The is running flag.
        /// </summary>
        private bool isRunning;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoriesMessageListener"/> class.
        /// </summary>
        public CategoriesMessageListener()
        {
            this.CategoryProperties = new Dictionary<string, IDictionary<string, float>>();
            this.categoryNameHashes = new Dictionary<uint, string>();
            this.propertyNameHashes = new Dictionary<uint, string>();

            ObjectLookupId id = new ObjectLookupId("CATEGORIES", ObjectLookupId.Base);
            this.lookup = RaveInstance.ObjectLookupTables[id];

            // Initialize category name hashes
            foreach (string categoryName in this.lookup.Keys)
            {
                Hash hash = new Hash { Value = categoryName };
                uint key = hash.Key;
                this.categoryNameHashes[key] = categoryName;
            }

            // Initialize property name hashes
            ITypedObjectInstance obj = this.lookup.Values.First() as ITypedObjectInstance;
            if (null == obj)
            {
                throw new Exception("Expected object of type ITypedObjectInstance");
            }

            foreach (IFieldDefinition field in obj.TypeDefinition.Fields)
            {
                ISimpleFieldDefinition basicField = field as ISimpleFieldDefinition;
                if (null != basicField)
                {
                    string fieldName = basicField.Name;
                    Hash hash = new Hash { Value = fieldName };
                    this.propertyNameHashes[hash.Key] = fieldName;
                }
            }
        }

        /// <summary>
        /// The header received event.
        /// </summary>
        public event Action HeaderReceived;

        /// <summary>
        /// The update received event.
        /// </summary>
        public event Action UpdateReceived;

        /// <summary>
        /// Gets the category properties.
        /// </summary>
        public IDictionary<string, IDictionary<string, float>> CategoryProperties { get; private set; }

        /// <summary>
        /// Receive a remote message.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="isBigEndian">
        /// The is big endian flag.
        /// </param>
        public void ReceiveMessage(RemoteControlCommandEnums command, byte[] payload, bool isBigEndian)
        {
            if (command != RemoteControlCommandEnums.CategoryTelemetryHeader
                && command != RemoteControlCommandEnums.CategoryTelemetryUpdate)
            {
                return;
            }

            if (Monitor.TryEnter(this, 3000))
            {
                try
                {
                    if (this.isRunning)
                    {
                        // Okay to skip latest message if still processing previous one.
                        return;
                    }

                    this.isRunning = true;
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }
            else
            {
                return;
            }

            this.buffer = new byte[payload.Length];
            Array.Copy(payload, this.buffer, payload.Length);

            Thread thread = new Thread(() => this.ProcessData(command, isBigEndian));
            thread.Start();
        }

        /// <summary>
        /// Process the data.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="isBigEndian">
        /// The is big endian flag.
        /// </param>
        private void ProcessData(RemoteControlCommandEnums command, bool isBigEndian)
        {
            switch (command)
            {
                case RemoteControlCommandEnums.CategoryTelemetryHeader:
                    {
                        this.ReceiveHeader(this.buffer, isBigEndian);
                        break;
                    }

                case RemoteControlCommandEnums.CategoryTelemetryUpdate:
                    {
                        this.ReceiveUpdate(this.buffer, isBigEndian);
                        break;
                    }
            }

            this.isRunning = false;
        }

        /// <summary>
        /// Receive header message.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="isBigEndian">
        /// The is big endian flag.
        /// </param>
        private void ReceiveHeader(byte[] payload, bool isBigEndian)
        {
            this.CategoryProperties.Clear();

            BinaryReader reader = new BinaryReader(new MemoryStream(payload), isBigEndian);
            this.staticCategoryCount = reader.ReadUInt();
            this.propertyCount = reader.ReadUInt();

            this.categoryNames = new string[this.staticCategoryCount];
            this.propertyNames = new string[this.propertyCount];

            Dictionary<string, float> tempProperties = new Dictionary<string, float>();

            // Initalize property names
            for (int propertyIndex = 0; propertyIndex < this.propertyCount; propertyIndex++)
            {
                uint propertyNameHash = reader.ReadUInt();
                string propertyName;
                if (!this.propertyNameHashes.TryGetValue(propertyNameHash, out propertyName))
                {
                    throw new Exception("Failed to find property name matching hash: " + propertyNameHash);
                }

                this.propertyNames[propertyIndex] = propertyName;
                tempProperties.Add(propertyName, 0);
            }

            // Initialize category names
            for (int categoryIndex = 0; categoryIndex < this.staticCategoryCount; categoryIndex++)
            {
                uint categoryHash = reader.ReadUInt();
                string categoryName;
                if (!this.categoryNameHashes.TryGetValue(categoryHash, out categoryName))
                {
                    //throw new Exception("Failed to find category name matching hash: " + categoryHash);
                    categoryName = "(unknown)";
                }

                this.categoryNames[categoryIndex] = categoryName;
                this.CategoryProperties[categoryName] = new Dictionary<string, float>(tempProperties, StringComparer.OrdinalIgnoreCase);
            }

            this.HeaderReceived.Raise();
        }

        /// <summary>
        /// Receive update message.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="isBigEndian">
        /// The is big endian flag.
        /// </param>
        private void ReceiveUpdate(byte[] payload, bool isBigEndian)
        {
            BinaryReader reader = new BinaryReader(new MemoryStream(payload), isBigEndian);

            for (int categoryIndex = 0; categoryIndex < this.staticCategoryCount; categoryIndex++)
            {
                string categoryName = this.categoryNames[categoryIndex];
                IDictionary<string, float> categoryProperties = this.CategoryProperties[categoryName];

                for (int propertyIndex = 0; propertyIndex < this.propertyCount; propertyIndex++)
                {
                    float propertyValue = reader.ReadFloat();
                    string propertyName = this.propertyNames[propertyIndex];

                    // Update latest property value
                    categoryProperties[propertyName] = propertyValue;
                }
            }

            this.UpdateReceived.Raise();
        }
    }
}
