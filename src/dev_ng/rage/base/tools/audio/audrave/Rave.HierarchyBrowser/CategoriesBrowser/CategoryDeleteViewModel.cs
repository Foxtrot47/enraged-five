﻿using GalaSoft.MvvmLight.Command;
using rage.Compiler;
using rage.Templates;
using rage.ToolLib;
using Rave.Instance;
using Rave.TypeDefinitions.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.ObjectBanks;
using Rave.Types.Infrastructure.Interfaces.Objects;
using Rave.Types.Infrastructure.Interfaces.Objects.Templates;
using Rave.Types.Infrastructure.Structs;
using Rave.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using System.Xml.Linq;

namespace Rave.HierarchyBrowser.CategoriesBrowser
{
	public class CategoryDeleteViewModel
	{
		public Action<IObjectInstance> DeleteAction;
		private Dictionary<IObjectInstance, double> _categoryVolumeDictionary;
		private ICommand _closeCommand;
		private ICommand _deleteCommand;
		private List<IObjectInstance> _objectInstanceNodes = new List<IObjectInstance>();
		private CategoryDeleteView _view;

		public IObjectInstance Category { get; private set; }

		public ObservableCollection<CategoryDiff> CategoryDiffs { get; private set; }

		public IObjectInstance ParentCategory { get; private set; }

		public ICommand CloseCommand
		{
			get
			{
				return _closeCommand ?? (_closeCommand = new RelayCommand(
					this.Close));
			}
		}

		public ICommand DeleteCommand
		{
			get
			{
				return _deleteCommand ?? (_deleteCommand = new RelayCommand(
					this.Delete,
					() => !this.InvalidDelete
					));
			}
		}
		public string Info { get; private set; }

		public bool InvalidDelete { get; private set; }

		public Dictionary<string, double[]> VolumeChanges { get; private set; }

		public CategoryDeleteViewModel(IObjectInstance categoryInstance, IObjectInstance parentCategoryCategoryInstance)
		{
			InvalidDelete = false;
			Category = categoryInstance;
			ParentCategory = parentCategoryCategoryInstance;
			VolumeChanges = new Dictionary<string, double[]>();
			_categoryVolumeDictionary = new Dictionary<IObjectInstance, double>();
			GetReferenceNodes(categoryInstance, parentCategoryCategoryInstance);
			GetCategoryDiffs(categoryInstance, parentCategoryCategoryInstance);
			LaunchWindow();
		}

		public void Delete()
		{
			try
			{
				_view.Close();
				CheckOutAllReferenceBanks();
				_objectInstanceNodes.Clear();
				//We need to get reference nodes again after checkout
				GetReferenceNodes(Category, ParentCategory);

				UpdateVolumesAndReferences();
				DeleteAction.Raise(this.Category);

				MessageBox.Show(Application.Current.MainWindow, "Category " + Category.Name + " deleted");
			}
			catch (Exception e)
			{
				ErrorManager.HandleInfo(e.Message);
			}
		}

		public bool LaunchWindow()
		{
			_view = new CategoryDeleteView(this) { Owner = Application.Current.MainWindow };
			_view.Show();
			return true;
		}

		public void UpdateVolumesAndReferences()
		{
			foreach (IObjectInstance objectInstance in _objectInstanceNodes)
			{
				UpdateReferenceVolume(objectInstance);
				UpdateReferenceToParent(objectInstance);
			}
		}

		private bool CheckIfTypeSound(IObjectInstance objectInstance)
		{
			string typeName = objectInstance.TypeName;
			const string allowedType = "SOUND";

			if (typeName == "TemplateInstance")
			{
				string templateKey = objectInstance.Type + "_TEMPLATE";
				if (RaveInstance.AllBankManagers.ContainsKey(templateKey))
				{
					MetaDataCompiler compiler =
						RaveInstance.XmlBankCompiler.MetadataManagers[objectInstance.Type].Compiler;
					ITemplateManager templateManager = compiler.CreateTemplateManager();

					foreach (IXmlBank bank in RaveInstance.AllBankManagers[templateKey].Banks)
					{
						templateManager.Load(Utility.ToXDoc(bank.Document));
					}

					ITemplateInstance templateInstance = objectInstance as ITemplateInstance;
					if (templateInstance != null)
					{
						XmlDocument temp = new XmlDocument();
						temp.AppendChild(temp.CreateElement("Objects"));
						XmlNode imported = temp.ImportNode(templateInstance.Node, true);
						if (temp.DocumentElement != null) temp.DocumentElement.AppendChild(imported);
						XDocument expandedDoc = Utility.ToXDoc(temp);
						templateManager.Process(expandedDoc);
						if (expandedDoc.Root != null) typeName = expandedDoc.Root.Descendants().First().Name.ToString();
					}
				}
			}

			while (typeName != null)
			{
				if (allowedType.ToUpper() == typeName.ToUpper())
				{
					return true;
				}

				ITypeDefinition type = objectInstance.TypeDefinitions.FindTypeDefinition(typeName);
				if (type == null)
				{
					break;
				}

				typeName = type.InheritsFrom;
			}
			return false;
		}

		private void CheckOutAllReferenceBanks()
		{
			HashSet<IObjectBank> banks = new HashSet<IObjectBank>();
			foreach (IObjectInstance refobjectInstance in _objectInstanceNodes)
			{
				if ((null != refobjectInstance && !refobjectInstance.IsAutoGenerated &&
						 !refobjectInstance.Equals(ParentCategory)))
				{
					if (CheckIfTypeSound(refobjectInstance))
					{
						banks.Add(refobjectInstance.Bank);
					}
				}
			}
			foreach (IObjectBank objectBank in banks)
			{
				objectBank.Checkout(false);
			}
		}

		private void Close()
		{
			_view.Close();
			this._objectInstanceNodes = null;
			this.Category = null;
			this.CategoryDiffs = null;
			this.DeleteAction = null;
		}

		private void GetCategoryDiffs(IObjectInstance category, IObjectInstance categoryParent)
		{
			CategoryDiffs = new ObservableCollection<CategoryDiff>();
			foreach (XmlNode xmlNode in category.Node.ChildNodes)
			{
				XmlNode parentNode =
					categoryParent.Node.ChildNodes.Cast<XmlNode>().FirstOrDefault(n => n.Name == xmlNode.Name);

				if (parentNode == null)
				{
					if (!xmlNode.Name.ToUpper().Contains("COMMENT"))
					{
						string defaultValue = category.TypeDefinitions.FindTypeDefinition(category.TypeName).FindFieldDefinition(xmlNode.Name).DefaultValue;
						if (defaultValue != null && !defaultValue.Equals(xmlNode.InnerText))
						{
							CategoryDiffs.Add(new CategoryDiff(xmlNode.Name, xmlNode.InnerText, defaultValue));
						}
					}
				}
				else
				{
					if (!parentNode.Name.ToUpper().Contains("COMMENT") &&
				   !parentNode.InnerText.Equals(xmlNode.InnerText, StringComparison.InvariantCulture))
					{
						CategoryDiffs.Add(new CategoryDiff(xmlNode.Name, xmlNode.InnerText, parentNode.InnerText));
					}
				}
			}
		}


		private void GetCategoryVolume(IObjectInstance categoryObjectInstance, IObjectInstance parentCategoryInstance)
		{
			XmlNode volumeNode = categoryObjectInstance.Node.ChildNodes.Cast<XmlNode>().FirstOrDefault(n => n.Name == "Volume");
			if (volumeNode != null)
			{
				if (volumeNode.Attributes != null && volumeNode.Attributes["overrideParent"] != null &&
					volumeNode.Attributes["overrideParent"].Value == "yes")
				{
					InvalidDelete = true;
					this.Info = Info + "Category " + categoryObjectInstance.Name + " is set to override parent volume\n";
				}
				if (_categoryVolumeDictionary.ContainsKey(parentCategoryInstance))
				{
					_categoryVolumeDictionary[categoryObjectInstance] = Convert.ToDouble(volumeNode.InnerText) + _categoryVolumeDictionary[parentCategoryInstance];
				}
				else
				{
					_categoryVolumeDictionary[categoryObjectInstance] = Convert.ToDouble(volumeNode.InnerText);
				}

			}
		}

		private void GetReferenceNodes(IObjectInstance categoryInstance, IObjectInstance parentCategoryInstance)
		{
			GetCategoryVolume(categoryInstance, parentCategoryInstance);
			foreach (IReferencable referencer in categoryInstance.Referencers)
			{
				IObjectInstance refobjectInstance = referencer as IObjectInstance;

				if ((null != refobjectInstance && !refobjectInstance.IsAutoGenerated &&
					 !refobjectInstance.Equals(parentCategoryInstance)))
				{
					if (!CheckIfTypeSound(refobjectInstance))
					{
						Info = Info + "Category " + categoryInstance.Name + " has reference " + refobjectInstance.Name + " which is of type " + refobjectInstance.TypeName + "\n";
						InvalidDelete = true;
					}
					CheckForNestedCategoryReference(refobjectInstance, categoryInstance);
					_objectInstanceNodes.Add(refobjectInstance);

					XmlNode volumeNode =
					  refobjectInstance.Node.ChildNodes.Cast<XmlNode>().FirstOrDefault(node => node.Name == "Volume");

					if (volumeNode != null)
					{
						double currentVolume = Convert.ToDouble(volumeNode.InnerText);
						if (!VolumeChanges.ContainsKey(refobjectInstance.Name))
						{
							VolumeChanges.Add(refobjectInstance.Name,
								new[] { currentVolume, currentVolume + _categoryVolumeDictionary[categoryInstance] });
						}
					}
				}
			}
			foreach (XmlNode xmlNode in categoryInstance.Node.ChildNodes)
			{
				if (xmlNode.Name == "CategoryRef" && !string.IsNullOrEmpty(xmlNode.InnerText))
				{
					Info = Info + "Child category " + xmlNode.InnerText + "will be deleted\n";
					GetReferenceNodes(categoryInstance.ObjectLookup[xmlNode.InnerText], categoryInstance);
				}
			}
		}

		private void CheckForNestedCategoryReference(IObjectInstance objectInstance, IObjectInstance categoryInstance)
		{
			foreach (ObjectRef referencer in objectInstance.References)
			{
				IObjectInstance refobjectInstance = referencer.ObjectInstance;
				if (null != refobjectInstance && !refobjectInstance.IsAutoGenerated)
				{
					if (refobjectInstance.References.Select(p => p.ObjectInstance).Contains(categoryInstance))
					{

						Info = Info + "Category has reference " + objectInstance.Name + " which has " + refobjectInstance.Name + " referencing the same category\n";
						InvalidDelete = true;
						return;
					}
				}
			}
			return;
		}
		private void UpdateReferenceToParent(IObjectInstance refobjectInstance)
		{
			XmlNode categoryNode =
					   refobjectInstance.Node.ChildNodes.Cast<XmlNode>().FirstOrDefault(node => node.Name == "Category");
			if (categoryNode == null)
			{
				return;
			}
			categoryNode.InnerText = ParentCategory.Name;

			refobjectInstance.MarkAsDirty();
		}

		private void UpdateReferenceVolume(IObjectInstance refobjectInstance)
		{
			XmlNode volumeNode =
					  refobjectInstance.Node.ChildNodes.Cast<XmlNode>().FirstOrDefault(node => node.Name == "Volume");

			if (volumeNode == null)
			{

				return;
			}
			double volume = VolumeChanges[refobjectInstance.Name][1];
			volumeNode.InnerText = volume.ToString(CultureInfo.InvariantCulture);
			refobjectInstance.MarkAsDirty();
		}
	}
}