// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CutsceneImport.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The cutscene import plugin.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rave.CutsceneImport
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;

    using Rave.Instance;
    using Rave.Plugins.Infrastructure.Interfaces;
    using Rave.Utils;
    using Rave.WaveBrowser.Infrastructure.Enums;
    using Rave.WaveBrowser.Infrastructure.Interfaces;
    using Rave.WaveBrowser.Infrastructure.Nodes;
    using Rave.WaveBrowser.UserActions;

    using Wavelib;
    using System.Diagnostics;

    /// <summary>
    /// The cutscene import plugin.
    /// </summary>
    public class CutsceneImport : IRAVEWaveImportPlugin
    {
        #region Constants

        /// <summary>
        /// The wave extension.
        /// </summary>
        private const string WaveExtension = ".WAV";

        #endregion

        #region Static Fields

        /// <summary>
        /// The channels.
        /// </summary>
        private static readonly string[] Channels = new[]
        {
            "_LEFT", "_RIGHT", "_CENTRE", "_CENTER", ".L", ".R", ".C", 
            ".LF", ".RF", ".LS", ".RS", ".MONO"
        };

        #endregion

        #region Fields

        /// <summary>
        /// The plugin name.
        /// </summary>
        private string name;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetName()
        {
            return this.name;
        }

        /// <summary>
        /// Handle the dropped files.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="selectedNode">
        /// The selected node.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="files">
        /// The files.
        /// </param>
        public void HandleDroppedFiles(
            IWaveBrowser waveBrowser, EditorTreeNode selectedNode, IActionLog actionLog, string[] files)
        {
            try
            {
                var packNode = selectedNode as PackNode;
                if (packNode == null || !packNode.FullPath.ToUpper().Contains("CUTSCENE"))
                {
                    MessageBox.Show(
                        RaveInstance.ActiveWindow,
                        "You may only drop waves on to the CUTSCENE Pack or Bank Folder using this plugin");
                    return;
                }

                if (!ValidateFiles(files))
                {
                    return;
                }

                EditorTreeNode.AllowAutoLock = true;

                ArrayList actions = Import(waveBrowser, actionLog, selectedNode, files);
                if (actions.Count > 0)
                {
                    if (actions.Count > 1)
                    {
                        IUserAction action = waveBrowser.CreateAction(
                            ActionType.BulkAction,
                            new ActionParams(
                                null, null, null, null, null, actionLog, "Cutscene Import", null, null, actions));

                        if (action.Action())
                        {
                            actionLog.AddAction(action);
                        }
                    }
                    else
                    {
                        actionLog.AddAction((IUserAction)actions[0]);
                    }
                }


            }
            catch (Exception e)
            {
                ErrorManager.HandleError(e);
            }
            finally
            {
                EditorTreeNode.AllowAutoLock = false;
            }
        }

        /// <summary>
        /// Initialize the plugin.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Init(XmlNode settings)
        {
            this.name = null;

            foreach (XmlNode setting in settings.ChildNodes)
            {
                switch (setting.Name)
                {
                    case "Name":
                        this.name = setting.InnerText;
                        break;
                }
            }

            return !string.IsNullOrEmpty(this.name);
        }

        /// <summary>
        /// Override to string method.
        /// </summary>
        /// <returns>
        /// The plugin name <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.GetName();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the bank name.
        /// </summary>
        /// <param name="waveName">
        /// The wave name.
        /// </param>
        /// <returns>
        /// The bank name <see cref="string"/>.
        /// </returns>
        private static string GetBankName(string waveName)
        {
            var bankName = waveName.ToUpper();

            foreach (var channel in Channels)
            {
                if (bankName.EndsWith(channel))
                {
                    bankName = bankName.Remove(bankName.Length - channel.Length);
                    break;
                }
            }

            return bankName;
        }

        /// <summary>
        /// Get the child node.
        /// </summary>
        /// <param name="parentNode">
        /// The parent node.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The child node <see cref="EditorTreeNode"/>.
        /// </returns>
        private static EditorTreeNode GetChildNode(EditorTreeNode parentNode, string name)
        {
            return parentNode.Nodes.Cast<EditorTreeNode>().FirstOrDefault(node => node.GetObjectName() == name);
        }

        /// <summary>
        /// Validate the dropped files.
        /// </summary>
        /// <param name="files">
        /// The files.
        /// </param>
        /// <returns>
        /// Flag indicating whether files are valid <see cref="bool"/>.
        /// </returns>
        private static bool ValidateFiles(string[] files)
        {
            var errors = new StringBuilder();

            foreach (var file in files)
            {
                var fileUpper = file.ToUpper();

                if (!fileUpper.EndsWith(".WAV"))
                {
                    errors.AppendLine("File must have .wav extension: " + file);
                    continue;
                }

                var fileName = Path.GetFileNameWithoutExtension(file);
                if (!Channels.Any(fileName.EndsWith))
                {
                    errors.AppendLine("Invalid filename - must end with valid channel identifier (e.g. file_left.wav): " + file);
                }
            }

            if (errors.Length > 0)
            {
                ErrorManager.HandleInfo("Invalid file(s):" + Environment.NewLine + errors);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Import the dropped files.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="files">
        /// The files.
        /// </param>
        /// <returns>
        /// The resulting actions <see cref="ArrayList"/>.
        /// </returns>
        private static ArrayList Import(
            IWaveBrowser waveBrowser, IActionLog actionLog, EditorTreeNode parent, IEnumerable<string> files)
        {
            var actions = new ArrayList();
            foreach (var file in files)
            {
                if (!file.ToUpper().EndsWith(WaveExtension))
                {
                    continue;
                }

                ImportWave(waveBrowser, parent, actionLog, actions, file);
            }

            return actions;
        }

        /// <summary>
        /// Import a cutscene wave.
        /// </summary>
        /// <param name="waveBrowser">
        /// The wave browser.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="actionLog">
        /// The action log.
        /// </param>
        /// <param name="actions">
        /// The actions.
        /// </param>
        /// <param name="file">
        /// The wave file.
        /// </param>
        private static void ImportWave(
            IWaveBrowser waveBrowser, EditorTreeNode parent, IActionLog actionLog, ArrayList actions, string file)
        {
            string waveName = Path.GetFileName(file).ToUpper();
            string waveNameNoExt = Path.GetFileNameWithoutExtension(file).ToUpper();
            string bankName = GetBankName(waveNameNoExt);
            ImportWave(waveBrowser, parent, actionLog, actions, file, bankName, waveName);

            if (bankName.EndsWith("_MASTERED"))
            {
                foreach (KeyValuePair<string, string> masteredLocation in Configuration.ProjectSettings.MasteredCutsceneLocations)
                {
                    // The .Replace('\\', '/') are there for inconsistencies between path names
                    if (parent.FullPath.Replace('\\', '/').Equals(masteredLocation.Key))
                    {
                        EditorTreeNode masteredNode = waveBrowser.GetEditorTreeView().FindNode(masteredLocation.Value.Replace('/', '\\'), null);
                        if (masteredNode == null)
                        {
                            MessageBox.Show(RaveInstance.ActiveWindow, masteredLocation.Value +
                                " cannot be found. Please make sure projectSettings.XML and your wave browser are up to date");
                            return;
                        }

                        string masteredBankName = bankName + "_ONLY";
                        string masteredWaveName =
                       //HACK FOR FILES STRUCTURED AS WAVENAME.(CHANNEL)_ONLY.WAV
                        waveName.Replace(Path.GetExtension(waveName), "_ONLY" + Path.GetExtension(waveName));
                        string newFile = Path.GetTempPath() + masteredWaveName;
                        File.Copy(file, newFile);
                        ImportWave(waveBrowser, masteredNode, actionLog, actions, newFile, masteredBankName, masteredWaveName);
                    }

                }
            }
        }


        private static void ImportWave(IWaveBrowser waveBrowser, EditorTreeNode parent, IActionLog actionLog, ArrayList actions, string file, string bankName, string waveName)
        {
            var editorTreeView = waveBrowser.GetEditorTreeView();
            editorTreeView.SetText("Importing: " + waveName);

            PackNode packNode = EditorTreeNode.FindParentPackNode(parent);

            if (!packNode.IsLoaded)
            {
                waveBrowser.GetEditorTreeView().LoadPackChildNodes(packNode.PackName);
            }

            var waveFile = new bwWaveFile(file, false);

            uint numOfChannels = waveFile.Format.NumChannels;

            // make sure this is a mono wave
            if (numOfChannels != 1)
            {
                MessageBox.Show(
                    RaveInstance.ActiveWindow, numOfChannels + " waves not supported", "Error", MessageBoxButtons.OK);
                return;
            }



            // check if bank already exists
            var bankNode = GetChildNode(parent, bankName) as BankNode;
            if (bankNode == null)
            {
                editorTreeView.SetText("Adding Bank " + bankName);

                // create a new bank
                var action = waveBrowser.CreateAction(
                    ActionType.AddBank,
                    new ActionParams(
                        waveBrowser,
                        parent,
                        null,
                        null,
                        editorTreeView.GetTreeView(),
                        actionLog,
                        bankName,
                        parent.GetPlatforms(),
                        null,
                        null));
                if (action.Action())
                {
                    actions.Add(action);
                }

                bankNode = GetChildNode(parent, bankName) as BankNode;
            }

            WaveNode existingWave = null;
            for (var i = 0; i < bankNode.Nodes.Count; i++)
            {
                var waveNode = bankNode.Nodes[i] as WaveNode;
                if (waveNode != null)
                {
                    if (waveNode.GetObjectName().ToUpper() == waveName)
                    {
                        existingWave = waveNode;
                        break;
                    }
                }
            }

            if (existingWave == null)
            {
                // this is a new wave file
                var action = waveBrowser.CreateAction(
                    ActionType.AddWave,
                    new ActionParams(
                        waveBrowser,
                        bankNode,
                        null,
                        file,
                        editorTreeView.GetTreeView(),
                        actionLog,
                        null,
                        bankNode.GetPlatforms(),
                        null,
                        null));
                if (action.Action())
                {
                    actions.Add(action);
                }
            }
            else
            {
                // this is an existing wave file
                var action = waveBrowser.CreateAction(
                    ActionType.ChangeWave,
                    new ActionParams(
                        waveBrowser,
                        bankNode,
                        existingWave,
                        file,
                        editorTreeView.GetTreeView(),
                        actionLog,
                        null,
                        existingWave.GetPlatforms(),
                        null,
                        null));
                if (action.Action())
                {
                    actions.Add(action);
                }
            }

        }


        private static void CreateNodesWithPath(TreeNodeCollection nodeList, string path)
        {
            TreeNode node = null;

            string folder = string.Empty;

            int p = path.IndexOf('\\');

            if (p == -1)
            {
                folder = path;
                path = "";
            }
            else
            {
                folder = path.Substring(0, p);
                path = path.Substring(p + 1, path.Length - (p + 1));
            }

            node = null;

            foreach (TreeNode item in nodeList)
            {
                if (item.Text == folder)
                {
                    node = item;
                }
            }

            if (node == null)
            {
                node = new TreeNode(folder);
                nodeList.Add(node);
            }

            if (path != "")
            {
                CreateNodesWithPath(node.Nodes, path);
            }
        }
        #endregion
    }
}