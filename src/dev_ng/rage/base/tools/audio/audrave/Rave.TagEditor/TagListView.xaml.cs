﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using Rave.TypeDefinitions.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.Objects;
using UserControl = System.Windows.Controls.UserControl;

namespace Rave.TagEditor
{
	/// <summary>
	/// Interaction logic for TagListView.xaml
	/// </summary>
	public partial class TagListView : UserControl
	{
		public TagListView(IObjectInstance objectInstance, string fieldAlias, XmlNode node)
		{
			InitializeComponent();
			ObjectInstance = objectInstance;
			FieldName = fieldAlias;
			Node = node;
			SetupDataContext();
		}

		public TagListView()
		{
			InitializeComponent();
			SetupDataContext();
		}

		private void OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			TagListViewModel viewmodel = SetupDataContext();
			if (viewmodel != null && viewmodel.IsCheckedOut)
			{
				NewTagTextBox.Focus();
				NewTagTextBox.SelectAll();
				e.Handled = true;
			}
		}
		public IObjectInstance ObjectInstance
		{
			get { return (IObjectInstance)GetValue(ObjectInstanceProperty); }
			set { SetValue(ObjectInstanceProperty, value); }
		}


		public string FieldName
		{
			get { return (string)GetValue(FieldNameProperty); }
			set
			{
				SetValue(FieldNameProperty, value);
			}
		}

		public XmlNode Node
		{
			get { return (XmlNode)GetValue(NodeProperty); }
			set { SetValue(NodeProperty, value); }
		}

		public static readonly DependencyProperty ObjectInstanceProperty =
			DependencyProperty.Register("ObjectInstance", typeof(IObjectInstance), typeof(TagListView), new PropertyMetadata(default(IObjectInstance), DependencyPropertyChanged));
		public static readonly DependencyProperty FieldNameProperty = DependencyProperty.Register("FieldName", typeof(string), typeof(TagListView), new PropertyMetadata(default(string), DependencyPropertyChanged));
		public static readonly DependencyProperty NodeProperty = DependencyProperty.Register("Node", typeof(XmlNode), typeof(TagListView), new PropertyMetadata(default(XmlNode), DependencyPropertyChanged));

		private static void DependencyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			TagListView control = (TagListView)d;
			control.SetupDataContext();
		}


		private void NewTagTextBox_OnKeyUP(object sender, KeyEventArgs e)
		{
			TagListViewModel viewmodel = SetupDataContext();

			if (viewmodel != null && viewmodel.IsCheckedOut)
			{
				viewmodel.OnKeyDown(e);
			}
		}


		private TagListViewModel SetupDataContext()
		{
			TagListViewModel viewmodel = null;
			if (!(DataContext is TagListViewModel))
			{
				if (ObjectInstance != null && FieldName != null && Node != null)
				{
					viewmodel = new TagListViewModel(ObjectInstance, FieldName, Node);
					DataContext = viewmodel;
				}
			}
			else
			{
				return (TagListViewModel)DataContext;
			}
			return viewmodel;
		}

		private void OnLostFocus(object sender, RoutedEventArgs e)
		{
			TagListViewModel viewmodel = SetupDataContext();

			if (viewmodel != null && viewmodel.IsCheckedOut)
			{
				viewmodel.AddTag(NewTagTextBox.Text);
			}
			NewTagTextBox.Clear();
		}
	}
}
