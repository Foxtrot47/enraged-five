﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml;
using GalaSoft.MvvmLight.Command;

using Rave.TypeDefinitions.Infrastructure.Interfaces;
using Rave.Types.Infrastructure.Interfaces.Objects;
using WPFToolLib.Extensions;

namespace Rave.TagEditor
{
	public class TagListViewModel : INotifyPropertyChanged
	{

		public ObservableCollection<string> Tags { get; private set; }
		public event PropertyChangedEventHandler PropertyChanged;
		private IObjectInstance _objectInstance;
		private string _alias;

		public bool IsCheckedOut { get { return ObjectInstance.Bank.IsCheckedOut; } }

		public TagListViewModel(IObjectInstance objectInstance, string alias, XmlNode node)
		{
			ObjectInstance = objectInstance;
			ObjectInstance.ReferencesChanged += OnObjectReferencesChanged;
			_alias = alias;
			if (node == null)
			{
				Node = ObjectInstance.Node == null
					? null
					: ObjectInstance.Node.ChildNodes.Cast<XmlNode>().FirstOrDefault(n => n.Name == _alias);
			}
			else
			{
				Node = node.ChildNodes.Cast<XmlNode>().FirstOrDefault(n => n.Name == _alias);
			}
			Tags = new ObservableCollection<string>();

			Tags.CollectionChanged += (p, j) => PropertyChanged.Raise(this, "Tags");
			if (GetListValue(Node, "tags") != null)
			{
				foreach (string tag in GetListValue(Node, "tags"))
				{
					if (!string.IsNullOrWhiteSpace(tag))
					{
						Tags.Add(tag);
					}
				}
			}
			RemoveTag = new RelayCommand<string>(p => { Tags.Remove(p); SetList(); });
		}


		private void OnObjectReferencesChanged()
		{
			if (Node == null)
			{
				Node = ObjectInstance.Node == null
					? null
					: ObjectInstance.Node.ChildNodes.Cast<XmlNode>().FirstOrDefault(n => n.Name == _alias);
			}
			if (IsCheckedOut)
			{
				SetList();
			}
		}

		internal void OnKeyDown(KeyEventArgs p)
		{
			if (p.Key.Equals(Key.Enter) || p.Key.Equals(Key.OemComma))
			{
				TextBox textBox = p.Source as TextBox;
				if (textBox != null)
				{
					if (!Tags.Contains(textBox.Text) && !string.IsNullOrWhiteSpace(textBox.Text))
					{
						AddTag(textBox.Text);
						SetList();
					}
					textBox.Clear();
				}
			}
			if (p.Key.Equals(Key.Back))
			{
				TextBox textBox = p.Source as TextBox;
				if (textBox != null && textBox.Text.Length == 0 && Tags.Count > 0)
				{
					Tags.RemoveAt(Tags.Count - 1);
					SetList();
				}
			}
		}

		public static string RemoveSpecialCharacters(string str)
		{
			StringBuilder sb = new StringBuilder();
			foreach (char c in str)
			{
				if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_')
				{
					sb.Append(c);
				}
			}
			return sb.ToString();
		}


		public RelayCommand<string> RemoveTag { get; private set; }

		public IObjectInstance ObjectInstance
		{
			get { return _objectInstance; }
			set
			{
				_objectInstance = value;
				PropertyChanged.Raise(this, "ObjectInstance");
			}
		}
		public XmlNode Node
		{
			get;
			private set;
		}

		private IList<string> GetListValue(XmlNode node, string valName)
		{

			if (node != null && node.Attributes != null && node.Attributes[valName] != null)
			{
				return node.Attributes[valName].Value.Trim().Split(',').ToList();
			}
			return null;
		}

		private void SetList(XmlNode node, string valName)
		{
			if (node == null)
			{
				return;
			}
			XmlAttribute valAttribute = null;
			if (node.Attributes != null)
			{
				valAttribute = node.Attributes[valName];
			}
			if (valAttribute == null)
			{
				valAttribute = node.OwnerDocument.CreateAttribute(valName);
				node.Attributes.Append(valAttribute);
			}

			if (Tags.Count == 0)
			{
				node.Attributes.Remove(valAttribute);
			}
			else
			{
				valAttribute.Value = String.Join(",", Tags);
			}

			ObjectInstance.MarkAsDirty();

		}

		private void SetList()
		{
			SetList(Node, "tags");
			RemoveTag.RaiseCanExecuteChanged();
		}

		public void AddTag(string text)
		{
			if (!Tags.Contains(text) && !string.IsNullOrWhiteSpace(text))
			{
				text = RemoveSpecialCharacters(text.Replace(" ", "_"));
				Tags.Add(text);
				SetList();
			}
		}
	}
}
