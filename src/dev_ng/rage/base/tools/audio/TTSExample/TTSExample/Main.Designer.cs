﻿namespace TTS
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.voicesCmb = new System.Windows.Forms.ComboBox();
            this.saveToFile = new System.Windows.Forms.CheckBox();
            this.dialogueText = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.operationsTabCtrl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.sayAsTypesCmb = new System.Windows.Forms.ComboBox();
            this.generateSayAsBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.volumeSettingsCmb = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.generateVolumeBtn = new System.Windows.Forms.Button();
            this.volumeTxt = new System.Windows.Forms.TextBox();
            this.volumeTrackBar = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.breakTypesCmb = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.generateBreakBtn = new System.Windows.Forms.Button();
            this.breakDurationTxt = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.subTxt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.generateSubBtn = new System.Windows.Forms.Button();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.pitchSettingsCmb = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.generatePitchBtn = new System.Windows.Forms.Button();
            this.pitchTxt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.phonemeTxt = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.generatePhonemeBtn = new System.Windows.Forms.Button();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.emphasisTypesCmb = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.generateEmphasisBtn = new System.Windows.Forms.Button();
            this.playOrSaveBtn = new System.Windows.Forms.Button();
            this.saveWavFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.label9 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.trackBar4 = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.rateSettingsCmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.generateRateBtn = new System.Windows.Forms.Button();
            this.rateMultiplierTxt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.operationsTabCtrl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Voices:";
            // 
            // voicesCmb
            // 
            this.voicesCmb.FormattingEnabled = true;
            this.voicesCmb.Location = new System.Drawing.Point(59, 25);
            this.voicesCmb.Name = "voicesCmb";
            this.voicesCmb.Size = new System.Drawing.Size(223, 21);
            this.voicesCmb.TabIndex = 1;
            this.voicesCmb.SelectionChangeCommitted += new System.EventHandler(this.voicesCmb_SelectionChangeCommitted);
            // 
            // saveToFile
            // 
            this.saveToFile.AutoSize = true;
            this.saveToFile.Checked = true;
            this.saveToFile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.saveToFile.Location = new System.Drawing.Point(323, 25);
            this.saveToFile.Name = "saveToFile";
            this.saveToFile.Size = new System.Drawing.Size(79, 17);
            this.saveToFile.TabIndex = 3;
            this.saveToFile.Text = "Save to file";
            this.saveToFile.UseVisualStyleBackColor = true;
            this.saveToFile.CheckedChanged += new System.EventHandler(this.saveToFile_CheckedChanged);
            // 
            // dialogueText
            // 
            this.dialogueText.Location = new System.Drawing.Point(16, 77);
            this.dialogueText.Name = "dialogueText";
            this.dialogueText.Size = new System.Drawing.Size(400, 121);
            this.dialogueText.TabIndex = 5;
            this.dialogueText.Text = "";
            this.dialogueText.SelectionChanged += new System.EventHandler(this.dialogueText_SelectionChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Dialogue:";
            // 
            // operationsTabCtrl
            // 
            this.operationsTabCtrl.Controls.Add(this.tabPage1);
            this.operationsTabCtrl.Controls.Add(this.tabPage2);
            this.operationsTabCtrl.Controls.Add(this.tabPage3);
            this.operationsTabCtrl.Controls.Add(this.tabPage6);
            this.operationsTabCtrl.Controls.Add(this.tabPage4);
            this.operationsTabCtrl.Controls.Add(this.tabPage5);
            this.operationsTabCtrl.Controls.Add(this.tabPage7);
            this.operationsTabCtrl.Controls.Add(this.tabPage8);
            this.operationsTabCtrl.Location = new System.Drawing.Point(432, 15);
            this.operationsTabCtrl.Name = "operationsTabCtrl";
            this.operationsTabCtrl.SelectedIndex = 0;
            this.operationsTabCtrl.Size = new System.Drawing.Size(588, 224);
            this.operationsTabCtrl.TabIndex = 9;
            this.operationsTabCtrl.Selected += new System.Windows.Forms.TabControlEventHandler(this.operationsTabCtrl_Selected);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.sayAsTypesCmb);
            this.tabPage1.Controls.Add(this.generateSayAsBtn);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(580, 198);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Say-As";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // sayAsTypesCmb
            // 
            this.sayAsTypesCmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.sayAsTypesCmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.sayAsTypesCmb.FormattingEnabled = true;
            this.sayAsTypesCmb.Location = new System.Drawing.Point(254, 64);
            this.sayAsTypesCmb.Name = "sayAsTypesCmb";
            this.sayAsTypesCmb.Size = new System.Drawing.Size(113, 21);
            this.sayAsTypesCmb.TabIndex = 10;
            // 
            // generateSayAsBtn
            // 
            this.generateSayAsBtn.Location = new System.Drawing.Point(253, 112);
            this.generateSayAsBtn.Name = "generateSayAsBtn";
            this.generateSayAsBtn.Size = new System.Drawing.Size(75, 23);
            this.generateSayAsBtn.TabIndex = 4;
            this.generateSayAsBtn.Text = "&Generate";
            this.generateSayAsBtn.UseVisualStyleBackColor = true;
            this.generateSayAsBtn.Click += new System.EventHandler(this.generateSayAsBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(214, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Type:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.volumeSettingsCmb);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.generateVolumeBtn);
            this.tabPage2.Controls.Add(this.volumeTxt);
            this.tabPage2.Controls.Add(this.volumeTrackBar);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(580, 198);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Volume";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // volumeSettingsCmb
            // 
            this.volumeSettingsCmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.volumeSettingsCmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.volumeSettingsCmb.FormattingEnabled = true;
            this.volumeSettingsCmb.Location = new System.Drawing.Point(181, 113);
            this.volumeSettingsCmb.Name = "volumeSettingsCmb";
            this.volumeSettingsCmb.Size = new System.Drawing.Size(114, 21);
            this.volumeSettingsCmb.TabIndex = 11;
            this.volumeSettingsCmb.Click += new System.EventHandler(this.volumeSettingsCmb_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(80, 116);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 13);
            this.label18.TabIndex = 7;
            this.label18.Text = "Setting:";
            // 
            // generateVolumeBtn
            // 
            this.generateVolumeBtn.Location = new System.Drawing.Point(277, 157);
            this.generateVolumeBtn.Name = "generateVolumeBtn";
            this.generateVolumeBtn.Size = new System.Drawing.Size(75, 23);
            this.generateVolumeBtn.TabIndex = 6;
            this.generateVolumeBtn.Text = "&Generate";
            this.generateVolumeBtn.UseVisualStyleBackColor = true;
            this.generateVolumeBtn.Click += new System.EventHandler(this.generateVolumeBtn_Click);
            // 
            // volumeTxt
            // 
            this.volumeTxt.Location = new System.Drawing.Point(181, 19);
            this.volumeTxt.Name = "volumeTxt";
            this.volumeTxt.Size = new System.Drawing.Size(58, 20);
            this.volumeTxt.TabIndex = 5;
            this.volumeTxt.TextChanged += new System.EventHandler(this.volumeTxt_TextChanged);
            // 
            // volumeTrackBar
            // 
            this.volumeTrackBar.LargeChange = 10;
            this.volumeTrackBar.Location = new System.Drawing.Point(181, 45);
            this.volumeTrackBar.Maximum = 100;
            this.volumeTrackBar.Name = "volumeTrackBar";
            this.volumeTrackBar.Size = new System.Drawing.Size(320, 45);
            this.volumeTrackBar.TabIndex = 4;
            this.volumeTrackBar.Scroll += new System.EventHandler(this.volumeTrackBar_Scroll);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Volume (0 - 100):";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.rateSettingsCmb);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.generateRateBtn);
            this.tabPage3.Controls.Add(this.rateMultiplierTxt);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(580, 198);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Rate";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.breakTypesCmb);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.generateBreakBtn);
            this.tabPage4.Controls.Add(this.breakDurationTxt);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(580, 198);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Break";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // breakTypesCmb
            // 
            this.breakTypesCmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.breakTypesCmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.breakTypesCmb.FormattingEnabled = true;
            this.breakTypesCmb.Location = new System.Drawing.Point(299, 65);
            this.breakTypesCmb.Name = "breakTypesCmb";
            this.breakTypesCmb.Size = new System.Drawing.Size(115, 21);
            this.breakTypesCmb.TabIndex = 13;
            this.breakTypesCmb.Click += new System.EventHandler(this.breakTypeCmb_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(167, 68);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 13);
            this.label17.TabIndex = 11;
            this.label17.Text = "Type:";
            // 
            // generateBreakBtn
            // 
            this.generateBreakBtn.Location = new System.Drawing.Point(255, 108);
            this.generateBreakBtn.Name = "generateBreakBtn";
            this.generateBreakBtn.Size = new System.Drawing.Size(75, 23);
            this.generateBreakBtn.TabIndex = 10;
            this.generateBreakBtn.Text = "&Generate";
            this.generateBreakBtn.UseVisualStyleBackColor = true;
            this.generateBreakBtn.Click += new System.EventHandler(this.generateBreakBtn_Click);
            // 
            // breakDurationTxt
            // 
            this.breakDurationTxt.Location = new System.Drawing.Point(299, 25);
            this.breakDurationTxt.Name = "breakDurationTxt";
            this.breakDurationTxt.Size = new System.Drawing.Size(115, 20);
            this.breakDurationTxt.TabIndex = 9;
            this.breakDurationTxt.TextChanged += new System.EventHandler(this.breakDurationTxt_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(167, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(126, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Duration (in milliseconds):";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.subTxt);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Controls.Add(this.generateSubBtn);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(580, 198);
            this.tabPage5.TabIndex = 8;
            this.tabPage5.Text = "Sub";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // subTxt
            // 
            this.subTxt.Location = new System.Drawing.Point(77, 50);
            this.subTxt.Name = "subTxt";
            this.subTxt.Size = new System.Drawing.Size(476, 20);
            this.subTxt.TabIndex = 14;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(28, 50);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 13);
            this.label16.TabIndex = 13;
            this.label16.Text = "Text:";
            // 
            // generateSubBtn
            // 
            this.generateSubBtn.Location = new System.Drawing.Point(253, 83);
            this.generateSubBtn.Name = "generateSubBtn";
            this.generateSubBtn.Size = new System.Drawing.Size(75, 23);
            this.generateSubBtn.TabIndex = 12;
            this.generateSubBtn.Text = "Substitute";
            this.generateSubBtn.UseVisualStyleBackColor = true;
            this.generateSubBtn.Click += new System.EventHandler(this.generateSubBtn_Click);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.pitchSettingsCmb);
            this.tabPage6.Controls.Add(this.label11);
            this.tabPage6.Controls.Add(this.generatePitchBtn);
            this.tabPage6.Controls.Add(this.pitchTxt);
            this.tabPage6.Controls.Add(this.label12);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(580, 198);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Pitch";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // pitchSettingsCmb
            // 
            this.pitchSettingsCmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.pitchSettingsCmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.pitchSettingsCmb.FormattingEnabled = true;
            this.pitchSettingsCmb.Location = new System.Drawing.Point(269, 83);
            this.pitchSettingsCmb.Name = "pitchSettingsCmb";
            this.pitchSettingsCmb.Size = new System.Drawing.Size(115, 21);
            this.pitchSettingsCmb.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(196, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Setting:";
            // 
            // generatePitchBtn
            // 
            this.generatePitchBtn.Location = new System.Drawing.Point(250, 127);
            this.generatePitchBtn.Name = "generatePitchBtn";
            this.generatePitchBtn.Size = new System.Drawing.Size(75, 23);
            this.generatePitchBtn.TabIndex = 19;
            this.generatePitchBtn.Text = "&Generate";
            this.generatePitchBtn.UseVisualStyleBackColor = true;
            this.generatePitchBtn.Click += new System.EventHandler(this.generatePitchBtn_Click);
            // 
            // pitchTxt
            // 
            this.pitchTxt.Location = new System.Drawing.Point(269, 49);
            this.pitchTxt.Name = "pitchTxt";
            this.pitchTxt.Size = new System.Drawing.Size(58, 20);
            this.pitchTxt.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(196, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Pitch (in Hz):";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.phonemeTxt);
            this.tabPage7.Controls.Add(this.label14);
            this.tabPage7.Controls.Add(this.generatePhonemeBtn);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(580, 198);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Phoneme";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // phonemeTxt
            // 
            this.phonemeTxt.Location = new System.Drawing.Point(83, 70);
            this.phonemeTxt.Name = "phonemeTxt";
            this.phonemeTxt.Size = new System.Drawing.Size(476, 20);
            this.phonemeTxt.TabIndex = 16;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(22, 70);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Phoneme:";
            // 
            // generatePhonemeBtn
            // 
            this.generatePhonemeBtn.Location = new System.Drawing.Point(253, 106);
            this.generatePhonemeBtn.Name = "generatePhonemeBtn";
            this.generatePhonemeBtn.Size = new System.Drawing.Size(75, 23);
            this.generatePhonemeBtn.TabIndex = 12;
            this.generatePhonemeBtn.Text = "&Generate";
            this.generatePhonemeBtn.UseVisualStyleBackColor = true;
            this.generatePhonemeBtn.Click += new System.EventHandler(this.generatePhonemeBtn_Click);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.emphasisTypesCmb);
            this.tabPage8.Controls.Add(this.label15);
            this.tabPage8.Controls.Add(this.generateEmphasisBtn);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(580, 198);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Emphasis";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // emphasisTypesCmb
            // 
            this.emphasisTypesCmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.emphasisTypesCmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.emphasisTypesCmb.FormattingEnabled = true;
            this.emphasisTypesCmb.Location = new System.Drawing.Point(258, 49);
            this.emphasisTypesCmb.Name = "emphasisTypesCmb";
            this.emphasisTypesCmb.Size = new System.Drawing.Size(105, 21);
            this.emphasisTypesCmb.TabIndex = 12;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(218, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "Type:";
            // 
            // generateEmphasisBtn
            // 
            this.generateEmphasisBtn.Location = new System.Drawing.Point(253, 85);
            this.generateEmphasisBtn.Name = "generateEmphasisBtn";
            this.generateEmphasisBtn.Size = new System.Drawing.Size(75, 23);
            this.generateEmphasisBtn.TabIndex = 1;
            this.generateEmphasisBtn.Text = "Emphasize";
            this.generateEmphasisBtn.UseVisualStyleBackColor = true;
            this.generateEmphasisBtn.Click += new System.EventHandler(this.generateEmphasisBtn_Click);
            // 
            // playOrSaveBtn
            // 
            this.playOrSaveBtn.Location = new System.Drawing.Point(173, 216);
            this.playOrSaveBtn.Name = "playOrSaveBtn";
            this.playOrSaveBtn.Size = new System.Drawing.Size(75, 23);
            this.playOrSaveBtn.TabIndex = 10;
            this.playOrSaveBtn.Text = "Save";
            this.playOrSaveBtn.UseVisualStyleBackColor = true;
            this.playOrSaveBtn.Click += new System.EventHandler(this.playOrSaveBtn_Click);
            // 
            // saveWavFileDialog
            // 
            this.saveWavFileDialog.DefaultExt = "wav";
            this.saveWavFileDialog.Filter = "All files (*.*)|*.*|WAV files (*.wav)|*.wav";
            this.saveWavFileDialog.FilterIndex = 2;
            this.saveWavFileDialog.RestoreDirectory = true;
            this.saveWavFileDialog.Title = "Save text to speech WAV file";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(495, 66);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(59, 17);
            this.checkBox1.TabIndex = 15;
            this.checkBox1.Text = "Enable";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(495, 12);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(59, 17);
            this.checkBox2.TabIndex = 14;
            this.checkBox2.Text = "Enable";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(123, 66);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(58, 20);
            this.textBox1.TabIndex = 13;
            // 
            // trackBar1
            // 
            this.trackBar1.Enabled = false;
            this.trackBar1.LargeChange = 3;
            this.trackBar1.Location = new System.Drawing.Point(192, 66);
            this.trackBar1.Minimum = -10;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(296, 45);
            this.trackBar1.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Speed (+/- 10):";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(253, 117);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "&Generate";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(123, 12);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(58, 20);
            this.textBox2.TabIndex = 9;
            // 
            // trackBar2
            // 
            this.trackBar2.Enabled = false;
            this.trackBar2.LargeChange = 3;
            this.trackBar2.Location = new System.Drawing.Point(192, 12);
            this.trackBar2.Minimum = -10;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(292, 45);
            this.trackBar2.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "AbsSpeed (+/- 10):";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(495, 66);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(59, 17);
            this.checkBox3.TabIndex = 15;
            this.checkBox3.Text = "Enable";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(495, 12);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(59, 17);
            this.checkBox4.TabIndex = 14;
            this.checkBox4.Text = "Enable";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(123, 66);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(58, 20);
            this.textBox3.TabIndex = 13;
            // 
            // trackBar3
            // 
            this.trackBar3.Enabled = false;
            this.trackBar3.LargeChange = 3;
            this.trackBar3.Location = new System.Drawing.Point(192, 66);
            this.trackBar3.Minimum = -10;
            this.trackBar3.Name = "trackBar3";
            this.trackBar3.Size = new System.Drawing.Size(296, 45);
            this.trackBar3.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Speed (+/- 10):";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(253, 117);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "&Generate";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(123, 12);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(58, 20);
            this.textBox4.TabIndex = 9;
            // 
            // trackBar4
            // 
            this.trackBar4.Enabled = false;
            this.trackBar4.LargeChange = 3;
            this.trackBar4.Location = new System.Drawing.Point(192, 12);
            this.trackBar4.Minimum = -10;
            this.trackBar4.Name = "trackBar4";
            this.trackBar4.Size = new System.Drawing.Size(292, 45);
            this.trackBar4.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 12);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "AbsSpeed (+/- 10):";
            // 
            // rateSettingsCmb
            // 
            this.rateSettingsCmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.rateSettingsCmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.rateSettingsCmb.FormattingEnabled = true;
            this.rateSettingsCmb.Location = new System.Drawing.Point(274, 83);
            this.rateSettingsCmb.Name = "rateSettingsCmb";
            this.rateSettingsCmb.Size = new System.Drawing.Size(115, 21);
            this.rateSettingsCmb.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(191, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Setting:";
            // 
            // generateRateBtn
            // 
            this.generateRateBtn.Location = new System.Drawing.Point(255, 127);
            this.generateRateBtn.Name = "generateRateBtn";
            this.generateRateBtn.Size = new System.Drawing.Size(75, 23);
            this.generateRateBtn.TabIndex = 24;
            this.generateRateBtn.Text = "&Generate";
            this.generateRateBtn.UseVisualStyleBackColor = true;
            this.generateRateBtn.Click += new System.EventHandler(this.generateRateBtn_Click);
            // 
            // rateMultiplierTxt
            // 
            this.rateMultiplierTxt.Location = new System.Drawing.Point(274, 49);
            this.rateMultiplierTxt.Name = "rateMultiplierTxt";
            this.rateMultiplierTxt.Size = new System.Drawing.Size(58, 20);
            this.rateMultiplierTxt.TabIndex = 23;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(191, 52);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 13);
            this.label19.TabIndex = 22;
            this.label19.Text = "Rate Multiplier:";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 255);
            this.Controls.Add(this.playOrSaveBtn);
            this.Controls.Add(this.operationsTabCtrl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dialogueText);
            this.Controls.Add(this.saveToFile);
            this.Controls.Add(this.voicesCmb);
            this.Controls.Add(this.label1);
            this.Name = "Main";
            this.Text = "Text To Speech";
            this.Load += new System.EventHandler(this.Main_Load);
            this.operationsTabCtrl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox voicesCmb;
        private System.Windows.Forms.CheckBox saveToFile;
        private System.Windows.Forms.RichTextBox dialogueText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl operationsTabCtrl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button generateSayAsBtn;
        private System.Windows.Forms.Button playOrSaveBtn;
        private System.Windows.Forms.SaveFileDialog saveWavFileDialog;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox volumeTxt;
        private System.Windows.Forms.TrackBar volumeTrackBar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button generateVolumeBtn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button generatePitchBtn;
        private System.Windows.Forms.TextBox pitchTxt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TrackBar trackBar3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TrackBar trackBar4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button generateBreakBtn;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Button generatePhonemeBtn;
        private System.Windows.Forms.Button generateEmphasisBtn;
        private System.Windows.Forms.ComboBox emphasisTypesCmb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox subTxt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button generateSubBtn;
        private System.Windows.Forms.ComboBox breakTypesCmb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox breakDurationTxt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox volumeSettingsCmb;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox pitchSettingsCmb;
        private System.Windows.Forms.ComboBox sayAsTypesCmb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox phonemeTxt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox rateSettingsCmb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button generateRateBtn;
        private System.Windows.Forms.TextBox rateMultiplierTxt;
        private System.Windows.Forms.Label label19;
    }
}

