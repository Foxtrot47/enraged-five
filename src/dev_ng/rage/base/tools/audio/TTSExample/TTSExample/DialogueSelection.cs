using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Xml.Linq;

namespace TTS
{
    public class DialogueSelection
    {
        private const string SSML_XMLNS = "xmlns=\"http://www.w3.org/2001/10/synthesis\"";
        private readonly PromptBuilder m_builder;
        private string m_selectedText;

        public DialogueSelection()
        {
            m_builder = new PromptBuilder(CultureInfo.GetCultureInfo("en-US"));
        }

        public string SelectedText
        {
            get { return m_selectedText; }
            set { m_selectedText = value.Trim(); }
        }

        public int SelectionStart { get; set; }

        public int SelectionLength { get; set; }

        public string ApplySayAs(string type, string text)
        {
            m_builder.ClearContent();
            SayAs value;
            if (type.TryParseEnum(true, out value))
            {
                m_builder.AppendTextWithHint(SelectedText, value);
                return InsertText(GetXmlTextFromPromptBuilder(), text);
            }
            return text;
        }

        public string ApplyVolume(int value, string text)
        {
            var xmlText = GenerateXmlText("prosody", SelectedText, new XAttribute("volume", value.ToString()));
            return InsertText(xmlText, text);
        }

        public string ApplyVolume(string setting, string text)
        {
            m_builder.ClearContent();
            PromptVolume volume;
            if (setting.TryParseEnum(true, out volume))
            {
                m_builder.AppendText(SelectedText, volume);
                return InsertText(GetXmlTextFromPromptBuilder(), text);
            }
            return text;
        }

        public string ApplyRate(float value, string text)
        {
            var xmlText = GenerateXmlText("prosody", SelectedText, new XAttribute("rate", value));
            return InsertText(xmlText, text);
        }

        public string ApplyRate(string setting, string text)
        {
            m_builder.ClearContent();
            PromptRate rate;
            if (setting.TryParseEnum(true, out rate))
            {
                m_builder.AppendText(SelectedText, rate);
                return InsertText(GetXmlTextFromPromptBuilder(), text);
            }
            return text;
        }

        public string ApplyPitch(uint value, string text)
        {
            var xmlText = GenerateXmlText("prosody", SelectedText, new XAttribute("pitch", string.Format("{0}Hz", value)));
            return InsertText(xmlText, text);
        }

        public string ApplyPitch(string setting, string text)
        {
            var xmlText = GenerateXmlText("prosody",
                                          SelectedText,
                                          new XAttribute("pitch", TranslatePromptPitchRange(setting)));
            return InsertText(xmlText, text);
        }

        private static string TranslatePromptPitchRange(string setting)
        {
            switch (setting)
            {
                case "ExtraLow":
                    {
                        return "x-low";
                    }
                case "ExtraHigh":
                    {
                        return "x-high";
                    }
                default:
                    {
                        return setting.ToLower();
                    }
            }
        }

        public string ApplyBreak(string type, string text)
        {
            m_builder.ClearContent();
            PromptBreak value;
            if (type.TryParseEnum(true, out value))
            {
                m_builder.AppendBreak(value);
                return text.Insert(SelectionStart, GetXmlTextFromPromptBuilder());
            }
            return text;
        }

        public string ApplyBreak(uint durationInMsec, string text)
        {
            m_builder.ClearContent();
            m_builder.AppendBreak(TimeSpan.FromMilliseconds(durationInMsec));
            return text.Insert(SelectionStart, GetXmlTextFromPromptBuilder());
        }

        public string ApplySub(string subText, string text)
        {
            m_builder.ClearContent();
            m_builder.AppendTextWithAlias(SelectedText, subText);
            return InsertText(GetXmlTextFromPromptBuilder(), text);
        }

        public string ApplyPhoneme(string phonemeText, string text)
        {
            var xmlText = GenerateXmlText("phoneme",
                                          SelectedText,
                                          new XAttribute("alphabet", "ipa"),
                                          new XAttribute("ph", phonemeText));
            return InsertText(xmlText, text);
        }

        public string ApplyEmphasis(string type, string text)
        {
            m_builder.ClearContent();
            PromptEmphasis value;
            if (type.TryParseEnum(true, out value))
            {
                m_builder.AppendText(SelectedText, value);
                return InsertText(GetXmlTextFromPromptBuilder(), text);
            }
            return text;
        }

        private string InsertText(string textToInsert, string text)
        {
            var leadingText = text.Substring(0, SelectionStart);
            var trailingText = text.Substring(SelectionStart + SelectionLength);
            return string.Concat(leadingText, textToInsert, trailingText);
        }

        private string GetXmlTextFromPromptBuilder()
        {
            try
            {
                var xmlStr = m_builder.ToXml();
                using (var strReader = new StringReader(xmlStr))
                {
                    var doc = XDocument.Load(strReader);

                    var element = doc.Root.Elements().First();
                    return element.ToString().Replace(SSML_XMLNS, string.Empty);
                }
            }
            catch
            {
            }
            return SelectedText;
        }

        private static string GenerateXmlText(string elementName, string content, params XAttribute[] attributes)
        {
            var builder = new StringBuilder();
            builder.Append("<");
            builder.Append(elementName);

            foreach (var attribute in attributes)
            {
                builder.Append(" ");
                builder.Append(attribute.Name);
                builder.Append("=\"");
                builder.Append(attribute.Value);
                builder.Append("\"");
            }

            if (string.IsNullOrEmpty(content))
            {
                builder.Append("/>");
            }
            else
            {
                builder.Append(">");
                builder.Append(content);
                builder.Append("</");
                builder.Append(elementName);
                builder.Append(">");
            }
            return builder.ToString();
        }
    }
}