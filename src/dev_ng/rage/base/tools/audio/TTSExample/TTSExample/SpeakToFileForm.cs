﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using SpeechGenLib.Processing;

namespace TTS
{
    public partial class SpeakToFileForm : Form
    {
        private readonly ITextToSpeechProcessor m_speakToFile;
        private readonly string m_speechText;

        public SpeakToFileForm(ITextToSpeechProcessor speakToFile, string speechText)
        {
            if (speakToFile == null)
            {
                throw new ArgumentNullException("speakToFile");
            }
            if (string.IsNullOrEmpty(speechText))
            {
                throw new ArgumentNullException("speechText");
            }
            m_speakToFile = speakToFile;
            m_speechText = speechText;

            InitializeComponent();
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            m_speakToFile.Process(m_speechText);
            if (m_speakToFile.Error != null)
            {
                throw m_speakToFile.Error;
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressTimer.Stop();
            progressBar.Value = 100;

            if (e.Error != null)
            {
                MessageBox.Show("Error: " + e.Error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Close();
        }

        private void progressTimer_Tick(object sender, EventArgs e)
        {
            var value = progressBar.Value + 10;
            if (value > 100)
            {
                value -= 100;
            }
            progressBar.Value = value;
        }

        private void SpeakToFileForm_Load(object sender, EventArgs e)
        {
            progressBar.Value = 0;
            progressTimer.Start();
            backgroundWorker.RunWorkerAsync();
        }
    }
}