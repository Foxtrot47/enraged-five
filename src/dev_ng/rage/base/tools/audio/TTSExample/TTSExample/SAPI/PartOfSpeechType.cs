namespace TTS.SAPI
{
    public enum PartOfSpeechType
    {
// ReSharper disable InconsistentNaming
        unknown,
        noun,
        verb,
        modifier,
        function,
        interjection
// ReSharper restore InconsistentNaming
    }
}