namespace TTS.SAPI
{
    public enum ContextTypes
    {
// ReSharper disable InconsistentNaming
        date_mdy,
        date_dmy,
        date_ymd,
        date_ym,
        date_my,
        date_dm,
        date_md,
        date_year,
        time,
        number_cardinal,
        number_digit,
        number_fraction,
        number_decimal,
        phone_number,
        currency,
        web_url,
        address,
        address_postal
// ReSharper restore InconsistentNaming
    }
}