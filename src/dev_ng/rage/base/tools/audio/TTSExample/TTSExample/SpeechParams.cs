namespace TTS
{
    public class SpeechParams
    {
        public string Voice { get; set; }
        public string Text { get; set; }
    }
}