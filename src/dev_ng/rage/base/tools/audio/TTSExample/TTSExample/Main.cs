﻿using System;
using System.Speech.Synthesis;
using System.Threading;
using System.Windows.Forms;
using rage.ToolLib.Logging;
using SpeechGenLib.Processing;
using SpeechGenLib.Voices;
using TTS.SSML;

namespace TTS
{
    public partial class Main : Form
    {
        private readonly IVoiceRepository m_availableVoices;
        private readonly DialogueSelection m_selection;
        private string m_currentVoice;

        public Main()
        {
            m_selection = new DialogueSelection();
            m_availableVoices = new VoiceRepository(new TextLog(new NullLogWriter(), new ContextStack()));
            InitializeComponent();
        }

        #region Main

        private void InitializeVoices()
        {
            voicesCmb.Items.Clear();
            foreach (var voiceName in m_availableVoices.VoiceNames)
            {
                voicesCmb.Items.Add(voiceName);
            }
            voicesCmb.SelectedIndex = 0;
            m_currentVoice = voicesCmb.Items[0] as string;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            InitializeVoices();
            InitialiseSayAsTypes();
            InitialiseVolumeSettings();
            InitializeRateSettings();
            InitializePitchSettings();
            InitialiseBreakTypes();
            InitialiseEmphasisTypes();
        }

        private void voicesCmb_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var voice = voicesCmb.SelectedItem as string;
            if (m_currentVoice != voice)
            {
                m_currentVoice = voice;
            }
        }

        private void dialogueText_SelectionChanged(object sender, EventArgs e)
        {
            m_selection.SelectionStart = dialogueText.SelectionStart;
            m_selection.SelectedText = dialogueText.SelectedText;
            m_selection.SelectionLength = dialogueText.SelectionLength;
        }

        private void saveToFile_CheckedChanged(object sender, EventArgs e)
        {
            playOrSaveBtn.Text = saveToFile.Checked ? "Save" : "Play";
        }

        private void playOrSaveBtn_Click(object sender, EventArgs e)
        {
            if (saveToFile.Checked)
            {
                if (saveWavFileDialog.ShowDialog() ==
                    DialogResult.OK)
                {
                    using (
                        var speakToFileForm =
                            new SpeakToFileForm(
                                new TextToSpeechFileProcessor(m_currentVoice, saveWavFileDialog.FileName),
                                dialogueText.Text))
                    {
                        speakToFileForm.ShowDialog();
                    }
                }
            }
            else
            {
                var thread = new Thread(Speak) {IsBackground = true};
                thread.Start(new SpeechParams {Voice = m_currentVoice, Text = dialogueText.Text});
            }
        }

        private static void Speak(object param)
        {
            var speechParams = param as SpeechParams;
            var player = new TextToSpeechSpeakerProcessor(speechParams.Voice);
            if (!player.Process(speechParams.Text))
            {
                MessageBox.Show("Error: " + player.Error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void operationsTabCtrl_Selected(object sender, TabControlEventArgs e)
        {
            if (e.Action ==
                TabControlAction.Selected)
            {
                switch (e.TabPageIndex)
                {
                    case 1: // Volume tab
                        {
                            volumeTrackBar.Value = 100;
                            volumeTxt.Text = "100";
                            break;
                        }
                }
            }
        }

        #endregion

        #region Say As

        private void InitialiseSayAsTypes()
        {
            var enumValues = Enum.GetNames(typeof (SayAs));

            sayAsTypesCmb.AutoCompleteCustomSource.Clear();
            sayAsTypesCmb.AutoCompleteCustomSource.AddRange(enumValues);

            sayAsTypesCmb.Items.Clear();
            sayAsTypesCmb.Items.AddRange(enumValues);
        }

        private void generateSayAsBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(m_selection.SelectedText))
            {
                if (sayAsTypesCmb.SelectedIndex >= 0)
                {
                    dialogueText.Text = m_selection.ApplySayAs(sayAsTypesCmb.SelectedItem as string, dialogueText.Text);
                }
            }
        }

        #endregion

        #region Volume

        private void InitialiseVolumeSettings()
        {
            var enumValues = Enum.GetNames(typeof (PromptVolume));

            volumeSettingsCmb.AutoCompleteCustomSource.Clear();
            volumeSettingsCmb.AutoCompleteCustomSource.AddRange(enumValues);

            volumeSettingsCmb.Items.Clear();
            volumeSettingsCmb.Items.AddRange(enumValues);
        }

        private void volumeTrackBar_Scroll(object sender, EventArgs e)
        {
            volumeTxt.Text = volumeTrackBar.Value.ToString();
        }

        private void volumeTxt_TextChanged(object sender, EventArgs e)
        {
            int value;
            if (int.TryParse(volumeTxt.Text, out value))
            {
                if (value < 0)
                {
                    volumeTrackBar.Value = 0;
                    volumeTrackBar_Scroll(null, null);
                }
                else if (value > 100)
                {
                    volumeTrackBar.Value = 100;
                    volumeTrackBar_Scroll(null, null);
                }
                else
                {
                    volumeTrackBar.Value = value;
                }
            }
        }

        private void volumeSettingsCmb_Click(object sender, EventArgs e)
        {
            volumeTxt.Text = "0";
            volumeTxt_TextChanged(null, null);
            volumeTxt.Text = string.Empty;
        }

        private void generateVolumeBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(m_selection.SelectedText))
            {
                if (!string.IsNullOrEmpty(volumeTxt.Text))
                {
                    dialogueText.Text = m_selection.ApplyVolume(volumeTrackBar.Value, dialogueText.Text);
                }
                else
                {
                    dialogueText.Text = m_selection.ApplyVolume(volumeSettingsCmb.SelectedItem as string,
                                                                dialogueText.Text);
                }
            }
        }

        #endregion

        #region Rate

        private void InitializeRateSettings()
        {
            var enumValues = Enum.GetNames(typeof (PromptRate));

            rateSettingsCmb.AutoCompleteCustomSource.Clear();
            rateSettingsCmb.AutoCompleteCustomSource.AddRange(enumValues);

            rateSettingsCmb.Items.Clear();
            rateSettingsCmb.Items.AddRange(enumValues);
        }

        private void generateRateBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(m_selection.SelectedText))
            {
                float rateMultiplier;
                if (!string.IsNullOrEmpty(rateMultiplierTxt.Text) &&
                    float.TryParse(rateMultiplierTxt.Text, out rateMultiplier))
                {
                    dialogueText.Text = m_selection.ApplyRate(rateMultiplier, dialogueText.Text);
                }
                else
                {
                    dialogueText.Text = m_selection.ApplyRate(rateSettingsCmb.SelectedItem as string, dialogueText.Text);
                }
            }
        }

        #endregion

        #region Pitch

        private void InitializePitchSettings()
        {
            var enumValues = Enum.GetNames(typeof (PromptPitchRange));

            pitchSettingsCmb.AutoCompleteCustomSource.Clear();
            pitchSettingsCmb.AutoCompleteCustomSource.AddRange(enumValues);

            pitchSettingsCmb.Items.Clear();
            pitchSettingsCmb.Items.AddRange(enumValues);
        }

        private void generatePitchBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(m_selection.SelectedText))
            {
                uint pitch;
                if (!string.IsNullOrEmpty(pitchTxt.Text) &&
                    uint.TryParse(pitchTxt.Text, out pitch))
                {
                    dialogueText.Text = m_selection.ApplyPitch(pitch, dialogueText.Text);
                }
                else
                {
                    dialogueText.Text = m_selection.ApplyPitch(pitchSettingsCmb.SelectedItem as string,
                                                               dialogueText.Text);
                }
            }
        }

        #endregion

        #region Break

        private void InitialiseBreakTypes()
        {
            var enumValues = Enum.GetNames(typeof (PromptBreak));

            breakTypesCmb.AutoCompleteCustomSource.Clear();
            breakTypesCmb.AutoCompleteCustomSource.AddRange(enumValues);

            breakTypesCmb.Items.Clear();
            breakTypesCmb.Items.AddRange(enumValues);
        }

        private void generateBreakBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(breakDurationTxt.Text))
            {
                uint breakDuration;
                if (uint.TryParse(breakDurationTxt.Text, out breakDuration))
                {
                    var result = DialogResult.Yes;
                    if (breakDuration > 10000)
                    {
                        result =
                            MessageBox.Show(
                                "You have specified a large break duration of " + breakDuration +
                                " milliseconds.\n Are you sure you want to use this value?",
                                "Large Break Duration",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Exclamation,
                                MessageBoxDefaultButton.Button2);
                    }
                    if (result == DialogResult.Yes)
                    {
                        dialogueText.Text = m_selection.ApplyBreak(breakDuration, dialogueText.Text);
                    }
                }
            }
            else
            {
                dialogueText.Text = m_selection.ApplyBreak(breakTypesCmb.SelectedItem as string, dialogueText.Text);
            }
        }

        private void breakTypeCmb_Click(object sender, EventArgs e)
        {
            breakDurationTxt.Text = string.Empty;
        }

        private void breakDurationTxt_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(breakDurationTxt.Text))
            {
                uint breakDuration;
                if (!uint.TryParse(breakDurationTxt.Text, out breakDuration))
                {
                    breakDurationTxt.Text = "0";
                    breakDurationTxt.Select(1, 0);
                }
            }
        }

        #endregion

        #region Sub

        private void generateSubBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(m_selection.SelectedText) &&
                !string.IsNullOrEmpty(subTxt.Text))
            {
                dialogueText.Text = m_selection.ApplySub(subTxt.Text, dialogueText.Text);
            }
        }

        #endregion

        #region Phoneme

        private void generatePhonemeBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(m_selection.SelectedText) &&
                !string.IsNullOrEmpty(phonemeTxt.Text))
            {
                dialogueText.Text = m_selection.ApplyPhoneme(phonemeTxt.Text, dialogueText.Text);
            }
        }

        #endregion

        #region Emphasis

        private void InitialiseEmphasisTypes()
        {
            var enumValues = Enum.GetNames(typeof (PromptEmphasis));

            emphasisTypesCmb.AutoCompleteCustomSource.Clear();
            emphasisTypesCmb.AutoCompleteCustomSource.AddRange(enumValues);

            emphasisTypesCmb.Items.Clear();
            emphasisTypesCmb.Items.AddRange(enumValues);
        }

        private void generateEmphasisBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(m_selection.SelectedText))
            {
                dialogueText.Text = m_selection.ApplyEmphasis(emphasisTypesCmb.SelectedItem as string, dialogueText.Text);
            }
        }

        #endregion
    }
}