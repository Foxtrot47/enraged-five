﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="audProjectSettings.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Parses RAGE Audio projectSettings.xml files
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace rage
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Xml;

    /// <summary>
    ///     Parses RAGE Audio projectSettings.xml files
    /// </summary>
	public class audProjectSettings
	{
        #region Fields

        /// <summary>
        /// The m_asset project.
        /// </summary>
        private string m_assetProject;

        /// <summary>
        /// The m_asset server.
        /// </summary>
        private string m_assetServer;

        /// <summary>
        /// The m_backup working path.
        /// </summary>
        private string m_backupWorkingPath;

        /// <summary>
        /// The m_build asset client.
        /// </summary>
        private string m_buildAssetClient;

        /// <summary>
        /// The m_build asset password.
        /// </summary>
        private string m_buildAssetPassword;

        /// <summary>
        /// The m_build asset user.
        /// </summary>
        private string m_buildAssetUser;

        // build pipeline

        // build Modules
        /// <summary>
        /// The m_build path.
        /// </summary>
        private string m_buildPath;

        /// <summary>
        /// The m_build tools path.
        /// </summary>
        private string m_buildToolsPath;

        /// <summary>
        /// The m_builder list.
        /// </summary>
        private ArrayList m_builderList;

        /// <summary>
        /// The m_current platform.
        /// </summary>
        private PlatformSetting m_currentPlatform;

        /// <summary>
        /// The m_metadata compiler path.
        /// </summary>
        private string m_metadataCompilerPath;

        /// <summary>
        /// The m_metadata settings.
        /// </summary>
        private audMetadataFile[] m_metadataSettings;

        /// <summary>
        /// The m_metatdata types.
        /// </summary>
        private audMetadataType[] m_metatdataTypes;

        /// <summary>
        /// The m_ped voice settings.
        /// </summary>
        private List<audPedVoiceSettings> m_pedVoiceSettings;

        /// <summary>
        /// The m_platform settings.
        /// </summary>
        private ArrayList m_platformSettings;

        /// <summary>
        /// The m_post builder list.
        /// </summary>
        private ArrayList m_postBuilderList;

        /// <summary>
        /// The m_pre builder list.
        /// </summary>
        private ArrayList m_preBuilderList;

        /// <summary>
        /// The m_preset path.
        /// </summary>
        private string m_presetPath;

        /// <summary>
        /// The m_project name.
        /// </summary>
        private string m_projectName;

        /// <summary>
        /// The m_rave plugins.
        /// </summary>
        private XmlNode[] m_ravePlugins;

        /// <summary>
        /// The m_server host.
        /// </summary>
        private string m_serverHost;

        /// <summary>
        /// The m_server port.
        /// </summary>
        private int m_serverPort;

        /// <summary>
        /// The m_should encrypt.
        /// </summary>
        private bool m_shouldEncrypt;

        /// <summary>
        /// The m_sound xml.
        /// </summary>
        private string m_soundXml;

        /// <summary>
        /// The m_static wave slot settings.
        /// </summary>
        private Dictionary<string, string> m_staticWaveSlotSettings;

        /// <summary>
        /// The m_wave attributes.
        /// </summary>
        private string[] m_waveAttributes;

        /// <summary>
        /// The m_wave slot attributes.
        /// </summary>
        private IList<string> m_waveSlotAttributes;

        /// <summary>
        /// The m_wave input.
        /// </summary>
        private string m_waveInput;

        /// <summary>
        /// The m_wave refs to skip.
        /// </summary>
        private List<string> m_waveRefsToSkip;

        /// <summary>
        /// The m_wave slot settings.
        /// </summary>
        private string m_waveSlotSettings;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="audProjectSettings"/> class.
        /// </summary>
        /// <param name="projectSettingsPath">
        /// The project settings path.
        /// </param>
		public audProjectSettings(string projectSettingsPath)
		{
            this.BatchBuilds = new List<audBuildSet>();
			this.MuteSoloEnabledObjectTypes = new List<string>();
			this.AlphabeticalSortHierarchyTypes = new List<string>();
			this.DialoguePaths = new List<string>();
			if (!this.LoadSettings(projectSettingsPath))
			{
                throw new Exception("Couldn't load project settings from " + projectSettingsPath);
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the audio data builder settings.
        /// </summary>
        public audDataBuilderSettings AudioDataBuilderSettings { get; private set; }

        /// <summary>
        /// Gets the batch builds.
        /// </summary>
        public List<audBuildSet> BatchBuilds { get; private set; }

        /// <summary>
        /// Gets the build modules.
        /// </summary>
        public XmlNode[] BuildModules { get; private set; }

        /// <summary>
        /// Gets or sets the default model path.
        /// </summary>
        public string DefaultModelPath { get; set; }

        /// <summary>
        /// Gets the fail build at stage.
        /// </summary>
        public string FailBuildAtStage { get; private set; }

        /// <summary>
        /// Gets the global variables list.
        /// </summary>
        public string GlobalVariablesList { get; private set; }

        /// <summary>
        /// Gets the metadata compiler jit paths.
        /// </summary>
        public string[] MetadataCompilerJitPaths { get; private set; }

        public audRpfGeneratorSettings RpfGeneratorSettings { get; private set; }

        /// <summary>
        /// Gets the path.
        /// </summary>
        public string Path { get; private set; }

        private List<audPlaceholderSpeechGeneration> placeholderSpeechGenerationSettings = new List<audPlaceholderSpeechGeneration>();
        /// <summary>
        /// Gets the placeholder speech generation settings.
        /// </summary>
		public List<audPlaceholderSpeechGeneration> PlaceholderSpeechGenerationSettings
		{
			get
			{
                return placeholderSpeechGenerationSettings;
            }
        }


        /// <summary>
        /// Gets the wave gesture context list.
        /// </summary>
        public string WaveGestureContextList { get; private set; }

        /// <summary>
        /// Gets the P4V path.
        /// </summary>
        public string P4vPath { get; private set; }

        /// <summary>
        /// Gets the wave slots xml path.
        /// </summary>
        public string WaveSlotsXmlPath { get; private set; }

        /// <summary>
        /// Gets the wave slot checker settings.
        /// </summary>
        public WaveSlotCheckerSettings WaveSlotCheckerSettings { get; private set; }

        /// <summary>
        /// Gets the wave pack groups.
        /// </summary>
        public IList<IList<string>> WavePackGroups { get; private set; }

        //TODO: remove that property and use LipsyncDialogueSettings.LipsyncTextPath in applications (e.g. audWaveBuildModule)
        /// <summary>
        /// Gets the LipsyncText path.
        /// </summary>
        public string LipsyncTextPath { get; private set; }

        /// <summary>
        /// Gets the lipsync dialogue settings.
        /// </summary>
        public audLipSyncSettings LipsyncDialogueSettings { get; private set; }

		public Dictionary<string, string> MasteredCutsceneLocations { get; private set; }

		public List<string> MuteSoloEnabledObjectTypes { get; private set; }
		
		public List<string> AlphabeticalSortHierarchyTypes { get; private set; }
		
		public string RaveDriveRoot { get; private set; }

		public string PreviewFolder { get; private set; }

        public bool MetadataTagsEnabled { get; private set; }

		public List<string> DialoguePaths { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The parse bool value.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
		public static bool ParseBoolValue(XmlNode node)
		{
			if (string.Compare(node.InnerText, "yes", true) == 0)
			{
                return true;
            }

			if (string.Compare(node.InnerText, "no", true) == 0)
			{
                return false;
            }

            throw new FormatException(
                string.Format(
                    "Invalid value for boolean field {0} ({1}): expected yes or no", node.Name, node.InnerText));
        }

        /// <summary>
        /// The debug print asset paths.
        /// </summary>
        [Conditional("DEBUG")]
		public void DebugPrintAssetPaths()
		{
            Debug.WriteLine("Asset Paths:");
            Debug.WriteLine("Asset Server: " + this.m_assetServer);
            Debug.WriteLine("Asset Project:" + this.m_assetProject);
            Debug.WriteLine("Sound Xml: " + this.m_soundXml);
            Debug.WriteLine("Wave Input: " + this.m_waveInput);
        }

        /// <summary>
        /// The debug print build server.
        /// </summary>
        [Conditional("DEBUG")]
		public void DebugPrintBuildServer()
		{
            Debug.WriteLine("Build Server:");
            Debug.WriteLine("Host: " + this.m_serverHost);
            Debug.WriteLine("Port: " + this.m_serverPort);
            Debug.WriteLine("UserName: " + this.m_buildAssetUser);
            Debug.WriteLine("Password: " + this.m_buildAssetPassword);
        }

        /// <summary>
        /// The debug print platform settings.
        /// </summary>
        [Conditional("DEBUG")]
		public void DebugPrintPlatformSettings()
		{
			foreach (PlatformSetting p in this.m_platformSettings)
			{
                Debug.WriteLine("Platform Settings:");
                Debug.WriteLine("\tTag: " + p.PlatformTag);
                Debug.WriteLine("\tBigEndian: " + p.IsBigEndian);
                Debug.WriteLine("\tBuildOutput: " + p.BuildOutput);
                Debug.WriteLine("\tBuildInfo: " + p.BuildInfo);
            }
        }

        /// <summary>
        /// The debug print settings.
        /// </summary>
        [Conditional("DEBUG")]
		public void DebugPrintSettings()
		{
            this.DebugPrintPlatformSettings();
            this.DebugPrintAssetPaths();
            this.DebugPrintBuildServer();
        }

        /// <summary>
        /// The get asset project.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetAssetProject()
		{
            return this.m_assetProject;
        }

        /// <summary>
        /// The get asset server.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetAssetServer()
		{
            return this.m_assetServer;
        }

        /// <summary>
        /// The get backup working path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetBackupWorkingPath()
		{
            return this.m_backupWorkingPath;
        }

        /// <summary>
        /// The get build asset client.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetBuildAssetClient()
		{
            return this.m_buildAssetClient;
        }

        /// <summary>
        /// The get build asset password.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetBuildAssetPassword()
		{
            return this.m_buildAssetPassword;
        }

        /// <summary>
        /// The get build asset user.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetBuildAssetUser()
		{
            return this.m_buildAssetUser;
        }

        /// <summary>
        /// The get build info path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetBuildInfoPath()
		{
            return this.m_currentPlatform.BuildInfo;
        }

        /// <summary>
        /// The get build output path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetBuildOutputPath()
		{
            return this.m_currentPlatform.BuildOutput;
        }

        /// <summary>
        /// The get build path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetBuildPath()
		{
            return this.m_buildPath;
        }

        /// <summary>
        /// The get build tools path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetBuildToolsPath()
		{
            return this.m_buildToolsPath;
        }

        /// <summary>
        /// The get builders.
        /// </summary>
        /// <returns>
        /// The <see cref="ArrayList"/>.
        /// </returns>
		public ArrayList GetBuilders()
		{
            return this.m_builderList;
        }

        /// <summary>
        /// The get current platform.
        /// </summary>
        /// <returns>
        /// The <see cref="PlatformSetting"/>.
        /// </returns>
		public PlatformSetting GetCurrentPlatform()
		{
            return this.m_currentPlatform;
        }

        /// <summary>
        /// The get metadata compiler path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetMetadataCompilerPath()
		{
            return this.m_metadataCompilerPath;
        }

        /// <summary>
        /// The get metadata settings.
        /// </summary>
        /// <returns>
        /// The <see cref="audMetadataFile[]"/>.
        /// </returns>
		public audMetadataFile[] GetMetadataSettings()
		{
            return this.m_metadataSettings;
        }

        /// <summary>
        /// The get metadata types.
        /// </summary>
        /// <returns>
        /// The <see cref="audMetadataType[]"/>.
        /// </returns>
		public audMetadataType[] GetMetadataTypes()
		{
            return this.m_metatdataTypes;
        }

        /// <summary>
        /// The get ped voice settings.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
		public List<audPedVoiceSettings> GetPedVoiceSettings()
		{
            return this.m_pedVoiceSettings ?? (this.m_pedVoiceSettings = new List<audPedVoiceSettings>());
        }

        /// <summary>
        /// The get platform settings.
        /// </summary>
        /// <returns>
        /// The <see cref="ArrayList"/>.
        /// </returns>
		public ArrayList GetPlatformSettings()
		{
            return this.m_platformSettings;
        }

        /// <summary>
        /// The get platform tag.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetPlatformTag()
		{
            return this.m_currentPlatform.PlatformTag;
        }

        /// <summary>
        /// The get post builders.
        /// </summary>
        /// <returns>
        /// The <see cref="ArrayList"/>.
        /// </returns>
		public ArrayList GetPostBuilders()
		{
            return this.m_postBuilderList;
        }

        /// <summary>
        /// The get pre builders.
        /// </summary>
        /// <returns>
        /// The <see cref="ArrayList"/>.
        /// </returns>
		public ArrayList GetPreBuilders()
		{
            return this.m_preBuilderList;
        }

        /// <summary>
        /// The get preset path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetPresetPath()
		{
            return this.m_presetPath;
        }

        /// <summary>
        /// The get project name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetProjectName()
		{
            return this.m_projectName;
        }

        /// <summary>
        /// The get rave plugins.
        /// </summary>
        /// <returns>
        /// The <see cref="XmlNode[]"/>.
        /// </returns>
		public XmlNode[] GetRavePlugins()
		{
            return this.m_ravePlugins;
        }

        /// <summary>
        /// The get server host.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetServerHost()
		{
            return this.m_serverHost;
        }

        /// <summary>
        /// The get server port.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
		public int GetServerPort()
		{
            return this.m_serverPort;
        }

        /// <summary>
        /// The get sound xml path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetSoundXmlPath()
		{
            return this.m_soundXml;
        }

        /// <summary>
        /// The get static wave slot paths.
        /// </summary>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
		public Dictionary<string, string> GetStaticWaveSlotPaths()
		{
            return this.m_staticWaveSlotSettings;
        }

        /// <summary>
        /// The get wave attributes.
        /// </summary>
        /// <returns>
        /// The <see cref="string[]"/>.
        /// </returns>
		public string[] GetWaveAttributes()
		{
            return this.m_waveAttributes;
        }

        /// <summary>
        /// The get wave slot attributes.
        /// </summary>
        /// <returns>
        /// The <see cref="string[]"/>.
        /// </returns>
		public IList<string> GetWaveSlotAttributes()
		{
            return this.m_waveSlotAttributes;
        }

        /// <summary>
        /// The get wave input path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetWaveInputPath()
		{
            return this.m_waveInput;
        }

        /// <summary>
        /// The get wave slot settings.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string GetWaveSlotSettings()
		{
            return this.m_waveSlotSettings;
        }

        /// <summary>
        /// The is big endian.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
		public bool IsBigEndian()
		{
            return this.m_currentPlatform.IsBigEndian;
        }

        /// <summary>
        /// The load settings.
        /// </summary>
        /// <param name="projectSettingsPath">
        /// The project settings path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
		public bool LoadSettings(string projectSettingsPath)
		{
            this.Path = projectSettingsPath;
            var doc = new XmlDocument();
            doc.Load(projectSettingsPath);
			if (doc.DocumentElement.Name != "ProjectSettings" || doc.DocumentElement.Attributes["name"] == null)
			{
                return false;
            }

            this.m_projectName = doc.DocumentElement.Attributes["name"].Value;

			foreach (XmlNode node in doc.DocumentElement.ChildNodes)
			{
				switch (node.Name)
				{
                    case "PlatformSettings":
                        this.ParsePlatformSettings(node);
                        break;
                    case "AssetPaths":
                        this.ParseAssetPaths(node);
                        break;
                    case "StaticWaveSlotSettings":
                        this.ParseWaveSlotSettings(node);
                        break;
                    case "BuildServer":
                        this.ParseBuildServer(node);
                        break;
                    case "RpfGenerator":
				        this.ParseRpfGenerator(node);
				        break;
                    case "BuildPipeline":
                        this.ParseBuildPipeline(node);
                        break;
                    case "WaveAttributes":
                        this.ParseWaveAttributes(node);
                        break;
                    case "WaveSlotAttributes":
                        this.ParseWaveSlotAttributes(node);
                        break;
                    case "MetadataSettings":
                        this.ParseMetadataSettings(node);
                        break;
                    case "RAVEPlugins":
                        this.ParseRavePlugins(node);
                        break;
                    case "BuildModules":
                        this.ParseBuildModules(node);
                        break;
                    case "WaveRefs":
                        this.ParseWaveRefs(node);
                        break;
                    case "DefaultModelPath":
                        this.DefaultModelPath = node.InnerText;
                        break;
                    case "PedVoiceSettings":
                        this.ParsePedVoices(node);
                        break;
                    case "BatchBuilds":
                        this.ParseBatchBuilds(node);
                        break;
                    case "PlaceholderSpeechGenerationSettings":
                        this.ParsePlaceholderSpeechGeneration(node);
                        break;
                    case "GlobalVariablesList":
                        this.GlobalVariablesList = node.InnerText;
                        break;
                    case "AudioDataBuilderSettings":
                        this.AudioDataBuilderSettings = new audDataBuilderSettings(node);
                        break;
                    case "FailBuildAtStage":
                        this.FailBuildAtStage = node.InnerText;
                        break;
                    case "WaveGestureContextList":
                        this.WaveGestureContextList = node.InnerText;
                        break;
                    case "P4vPath":
                        this.P4vPath = node.InnerText;
                        break;
                    case "WaveSlotsXmlPath":
                        this.WaveSlotsXmlPath = node.InnerText;
                        break;
                    case "WaveSlotChecker":
                        this.WaveSlotCheckerSettings = new WaveSlotCheckerSettings(node);
                        break;
                    case "WavePackGroups":
                        this.ParseWavePackGroupsSettings(node);
                        break;
                    case "LipsyncTextPath":
                        this.LipsyncTextPath = node.InnerText;
                        break;
                    case "LipsyncSettings":
                        this.ParseLipsyncSetting(node);
                        break;
					case "MasteredCutsceneLocations":
						this.ParseMasteredCutsceneLocations(node);
						break;
					case "MuteSoloEnabledObjectTypes":
						this.ParseMutedSoloEnabledObjectTypes(node);
						break;
					case "AlphabeticalSortHierarchyTypes":
						this.ParseAlphabeticalSortHierarchyTypes(node);
						break;
					case "RaveDriveRootDirectory":
						this.RaveDriveRoot = node.InnerText;
						break;
					case "PreviewFolder":
						this.PreviewFolder = node.InnerText.Trim();
						break;
                    case "MetadataTagsEnabled":
				        this.MetadataTagsEnabled = node.InnerText.ToLower().Equals("true");
				        break;

				}
            }

            this.DebugPrintSettings();

            return true;
        }



        /// <summary>
        /// The resolve path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
		public string ResolvePath(string path)
		{
            var newpath = path;

			if (newpath.ToUpper().Contains("{PLATFORM}"))
			{
                newpath = newpath.ToUpper().Replace("{PLATFORM}", this.m_currentPlatform.PlatformTag);
            }

            return newpath;
        }

        /// <summary>
        /// The set current platform.
        /// </summary>
        /// <param name="s">
        /// The s.
        /// </param>
		public void SetCurrentPlatform(PlatformSetting s)
		{
            this.m_currentPlatform = s;
        }

        /// <summary>
        /// The set current platform by tag.
        /// </summary>
        /// <param name="platformTag">
        /// The platform tag.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool SetCurrentPlatformByTag(string platformTag)
        {
            foreach (PlatformSetting s in this.m_platformSettings)
            {
                if (s.PlatformTag == platformTag)
                {
                    this.SetCurrentPlatform(s);
                    return true;
                }
            }

            return false;
        }

        public PlatformSetting GetPlatformByTag(string platformTag) 
        {
            foreach (PlatformSetting s in this.m_platformSettings)
            {
                if (s.PlatformTag.ToUpper().Equals(platformTag.ToUpper()))
                {
                    return s;
                }
            }
            throw new Exception("No platform found with platform tag "+platformTag);
        }

        /// <summary>
        /// The should encrypt.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
		public bool ShouldEncrypt()
		{
            return this.m_shouldEncrypt;
        }

        /// <summary>
        /// The wave refs to skip.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
		public List<string> WaveRefsToSkip()
		{
            return this.m_waveRefsToSkip;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The parse platform setting.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// The <see cref="PlatformSetting"/>.
        /// </returns>
		private static PlatformSetting ParsePlatformSetting(XmlNode node)
		{
            var setting = new PlatformSetting { Name = node.Attributes["name"].Value };
			foreach (XmlNode child in node.ChildNodes)
			{
				switch (child.Name)
				{
                    case "Tag":
                        setting.PlatformTag = child.InnerText;
                        break;
                    case "BigEndian":
                        setting.IsBigEndian = ParseBoolValue(child);
                        break;
                    case "BuildOutput":
                        setting.BuildOutput = child.InnerText;
                        break;
                    case "LiveOutput":
                        setting.LiveOutput = child.InnerText;
                        break;
                    case "BuildInfo":
                        setting.BuildInfo = child.InnerText;
                        break;
                    case "WaveMemory":
                        setting.MaxWaveMemory = int.Parse(child.InnerText);
                        break;
                    case "IsActive":
                        setting.IsActive = ParseBoolValue(child);
                        break;
                    case "BuildEnabled":
                        setting.BuildEnabled = ParseBoolValue(child);
                        break;
                    case "GetLatestEnabled":
                        setting.GetLatestEnabled = ParseBoolValue(child);
                        break;
                    case "AlignmentSamples":
                        setting.AlignmentSamples = int.Parse(child.InnerText);
                        break;
                    case "Encoder":
                        setting.Encoder = child.InnerText;
                        break;
                    case "StreamingPacketBytes":
                        setting.StreamingPacketBytes = int.Parse(child.InnerText);
                        break;
                    case "EncryptionKey":
                        setting.EncryptionKey = child.InnerText;
                        break;
					case "CodeSymbol":
						setting.CodeSymbol = child.InnerText;
						break;
                }
            }

            return setting;
        }

        /// <summary>
        /// The parse asset paths.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseAssetPaths(XmlNode node)
		{
			foreach (XmlNode child in node.ChildNodes)
			{
				switch (child.Name)
				{
                    case "AssetServer":
                        this.m_assetServer = child.InnerText;
                        break;
                    case "AssetProject":
                        this.m_assetProject = child.InnerText;
                        break;
                    case "SoundXml":
                        this.m_soundXml = child.InnerText;
                        break;
                    case "WaveInput":
                        this.m_waveInput = child.InnerText;
                        break;
                    case "WaveSlotSettings":
                        this.m_waveSlotSettings = child.InnerText;
                        break;
                }
            }
        }

        /// <summary>
        /// The parse batch builds.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseBatchBuilds(XmlNode node)
		{
            this.BatchBuilds = new List<audBuildSet>();
			foreach (XmlNode c in node.ChildNodes)
			{
				if (c.Name == "BuildSet")
				{
                    this.BatchBuilds.Add(new audBuildSet(c));
                }
            }
        }

        /// <summary>
        /// The parse build modules.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseBuildModules(XmlNode node)
		{
            this.BuildModules = node.ChildNodes.Cast<XmlNode>().Where(child => child.Name == "BuildModule").ToArray();
        }

        /// <summary>
        /// The parse build pipeline.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseBuildPipeline(XmlNode node)
		{
            this.m_preBuilderList = new ArrayList();
            this.m_builderList = new ArrayList();
            this.m_postBuilderList = new ArrayList();

			foreach (XmlNode child in node.ChildNodes)
			{
				switch (child.Name)
				{
                    case "PreBuilder":
                        this.m_preBuilderList.Add(new audBuildComponent(child));
                        break;
                    case "Builder":
                        this.m_builderList.Add(new audBuildComponent(child));
                        break;
                    case "PostBuilder":
                        this.m_postBuilderList.Add(new audBuildComponent(child));
                        break;
                }
            }
        }

        /// <summary>
        /// The parse build server.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseBuildServer(XmlNode node)
		{
			foreach (XmlNode child in node.ChildNodes)
			{
				switch (child.Name)
				{
                    case "Path":
                        this.m_buildPath = child.InnerText;
                        break;
                    case "BuildToolsPath":
                        this.m_buildToolsPath = child.InnerText;
                        break;
                    case "ServerHost":
                        this.m_serverHost = child.InnerText;
                        break;
                    case "ServerPort":
                        this.m_serverPort = int.Parse(child.InnerText);
                        break;
                    case "AssetUser":
                        this.m_buildAssetUser = child.InnerText;
                        break;
                    case "AssetPassword":
                        this.m_buildAssetPassword = child.InnerText;
                        break;
                    case "AssetClient":
                        this.m_buildAssetClient = child.InnerText;
                        break;
                    case "EnableEncryption":
                        this.m_shouldEncrypt = ParseBoolValue(child);
                        break;
                }
            }
        }

        private void ParseRpfGenerator(XmlNode node)
        {
            string p4IntegrateExe=null;
            string p4IntegrateWorkingPath=null;
            string rpfRootPath=null; 
            List<string> devIngnorePackList = new List<string>();
            Dictionary<string, List<string>> releaseIgnorePackList = new Dictionary<string, List<string>>();
            List<string> ignoreExceptionPackList = new List<string>();

            foreach (XmlNode child in node.ChildNodes)
			{
				switch (child.Name)
				{
                    case "P4IntegrateExe":
                        p4IntegrateExe = child.InnerText;
                        break;
                    case "P4IntegrateWorkingPath":
                        p4IntegrateWorkingPath = child.InnerText;
                        break;
                    case "RpfRootPath":
                        rpfRootPath = child.InnerText;
                        break;
                    case "DevIgnorePackList":
				        foreach (XmlNode devIgnorePack in child.ChildNodes)
				        {
				            devIngnorePackList.Add(devIgnorePack.InnerText);
				        }
				        break;
                    case "ReleaseIgnorePackList":
                        foreach (XmlNode relIgnorePack in child.ChildNodes)
						{
						    if (relIgnorePack.Attributes["platform"] != null)
						    {
                                string platform=relIgnorePack.Attributes["platform"].Value;
						        if(!releaseIgnorePackList.ContainsKey(platform))
						            releaseIgnorePackList.Add(platform, new List<string>());
						        releaseIgnorePackList[platform].Add(relIgnorePack.InnerText);
						    }
						}
                        break;
                    case "IgnoreExceptionPackList":
				        foreach (XmlNode ignoreExceptionPack in child.ChildNodes)
				        {
				            ignoreExceptionPackList.Add(ignoreExceptionPack.InnerText);
				        }
				        break;
                }
            }
            this.RpfGeneratorSettings = new audRpfGeneratorSettings(p4IntegrateExe, p4IntegrateWorkingPath, rpfRootPath, devIngnorePackList, releaseIgnorePackList, ignoreExceptionPackList);
        }

        /// <summary>
        /// The parse metadata compiler jit paths.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseMetadataCompilerJitPaths(XmlNode node)
		{
            var jitPaths =
                (from XmlNode childNode in node.ChildNodes
                 where childNode.Name == "MetadataCompilerJitPath"
                 select childNode.InnerText).ToArray();

			if (jitPaths.Length > 0)
			{
                this.MetadataCompilerJitPaths = jitPaths;
            }
        }

        /// <summary>
        /// The parse metadata settings.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseMetadataSettings(XmlNode node)
		{
            var metadataFiles = new List<audMetadataFile>();
            var metadataTypes = new List<audMetadataType>();

			foreach (XmlNode child in node.ChildNodes)
			{
				switch (child.Name)
				{
                    case "MetadataFile":
                        metadataFiles.Add(new audMetadataFile(child));
                        break;
                    case "MetadataType":
                        metadataTypes.Add(new audMetadataType(child));
                        break;
                    case "MetadataCompilerPath":
                        this.m_metadataCompilerPath = child.InnerText;
                        break;
                    case "BackupWorkingPath":
                        this.m_backupWorkingPath = child.InnerText;
                        break;
                    case "MetadataCompilerJitPaths":
                        this.ParseMetadataCompilerJitPaths(child);
                        break;
                }
            }

            this.m_metadataSettings = metadataFiles.ToArray();
            this.m_metatdataTypes = metadataTypes.ToArray();
        }

        /// <summary>
        /// The parse ped voices.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParsePedVoices(XmlNode node)
		{
			if (this.m_pedVoiceSettings == null)
			{
                this.m_pedVoiceSettings = new List<audPedVoiceSettings>();
            }

			foreach (XmlNode pedVoiceSetting in node.ChildNodes)
			{
                this.m_pedVoiceSettings.Add(new audPedVoiceSettings(pedVoiceSetting));
            }
        }

        /// <summary>
        /// The parse placeholder speech generation.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseLipsyncSetting(XmlNode node)
		{

            string lipsyncTextPath = null;
            string lipsyncBuildXMLPath = null;
            string customAnimationDir = null;
            string wavesBaseDir = null;
            string lipsyncAnimsBaseDir = null;
            List<audScriptedSpeechConfig> scriptedSpeechConfigs = new List<audScriptedSpeechConfig>();
            List<audAmbientSpeechConfig> ambientSpeechConfigs = new List<audAmbientSpeechConfig>();

            foreach (XmlNode childNode in node.ChildNodes)
            {
                switch (childNode.Name)
                {
                    case "LipsyncTextPath":
                        lipsyncTextPath = childNode.InnerText;
                        break;
                    case "LipsyncBuildXMLPath":
                        lipsyncBuildXMLPath = childNode.InnerText;
                        break;
                    case "CustomAnimationDir":
                        customAnimationDir = childNode.InnerText;
                        break;
                    case "WavesBaseDir":
                        wavesBaseDir = childNode.InnerText;
                        break;
                    case "LipsyncAnimsBaseDir":
                        lipsyncAnimsBaseDir = childNode.InnerText;
                        break;
                    case "ScriptedSpeechConfig":
                        string ssPath = null;
                        string dialogueFilesFile = null;
                        string dialogueFile = null;
						string customAnimDir = null;
                        if(childNode.Attributes["Path"]!=null)ssPath =childNode.Attributes["Path"].Value;
                        if(childNode.Attributes["DialogueFilesFile"]!=null)dialogueFilesFile = childNode.Attributes["DialogueFilesFile"].Value;
                        if( childNode.Attributes["DialogueFile"]!=null)dialogueFile = childNode.Attributes["DialogueFile"].Value;
						if (childNode.Attributes["CustomAnimationDir"] != null) customAnimDir = childNode.Attributes["CustomAnimationDir"].Value;
						scriptedSpeechConfigs.Add(new audScriptedSpeechConfig(ssPath, dialogueFilesFile, dialogueFile, customAnimDir));
                        break;
                    case "AmbientSpeechConfig":
                        string asPath = null;
                        if(childNode.Attributes["Path"]!=null)asPath =childNode.Attributes["Path"].Value;
                        ambientSpeechConfigs.Add(new audAmbientSpeechConfig(asPath));
                        break;
                }
            }
            this.LipsyncDialogueSettings = new audLipSyncSettings(lipsyncTextPath, lipsyncBuildXMLPath, customAnimationDir, wavesBaseDir, lipsyncAnimsBaseDir, scriptedSpeechConfigs, ambientSpeechConfigs);
        }

        /// <summary>
        /// The parse placeholder speech generation.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParsePlaceholderSpeechGeneration(XmlNode node)
		{
			foreach (XmlNode childNode in node.ChildNodes)
			{
                string enabled = null;
				if (childNode.Name.Equals("SpeechGeneration"))
				{
                    enabled = childNode.Attributes["enabled"].Value;
					if (enabled != null && enabled.Equals("True"))
					{
                        string name = childNode.Attributes["name"].Value;
                        bool excludePlaceholderConversation = false;
                        if (childNode.Attributes["excludePlaceholderConversation"] != null)
                        {
                            excludePlaceholderConversation =
                                childNode.Attributes["excludePlaceholderConversation"].Value.Equals("True", StringComparison.InvariantCultureIgnoreCase);
                        }
                        string dialoguePath = "";
                        string characterVoiceConfig = "";
                        List<audPlaceholderSpeechPack> placeholderSpeechPacks = new List<audPlaceholderSpeechPack>();
						foreach (XmlNode speechGenerationContent in childNode.ChildNodes)
						{
							switch (speechGenerationContent.Name)
							{
								case "DialoguePath":
									{
                                        dialoguePath = speechGenerationContent.InnerText;

                                        break;
                                    }
								case "CharacterVoiceConfig":
									{
                                        characterVoiceConfig = speechGenerationContent.InnerText;
                                        break;
                                    }
								case "PlaceholderSpeechPacks":
									{
										foreach (XmlNode placeholderPack in speechGenerationContent.ChildNodes)
										{
                                            string maxFileSize = null;
                                            string bankFolder = null;
                                            string associatedSpeechPack = null;
                                            string associatedSpeechBankfolder = null;
                                            if(placeholderPack.Attributes["maxFileSize"]!=null) maxFileSize=placeholderPack.Attributes["maxFileSize"].Value;
                                            if(placeholderPack.Attributes["bankFolder"]!=null) bankFolder=placeholderPack.Attributes["bankFolder"].Value;
                                            if (placeholderPack.Attributes["associatedSpeechPack"] != null) associatedSpeechPack = placeholderPack.Attributes["associatedSpeechPack"].Value;
                                            if (placeholderPack.Attributes["associatedSpeechBankfolder"] != null) associatedSpeechBankfolder = placeholderPack.Attributes["associatedSpeechBankfolder"].Value;
                                            placeholderSpeechPacks.Add(new audPlaceholderSpeechPack(placeholderPack.InnerText, maxFileSize, bankFolder, associatedSpeechPack, associatedSpeechBankfolder));
                                        }
                                        break;
                                    }
                            }
                        }
                        this.PlaceholderSpeechGenerationSettings.Add(new audPlaceholderSpeechGeneration(
                            name, excludePlaceholderConversation, dialoguePath, characterVoiceConfig, placeholderSpeechPacks));
                    }

					//Add all paths to DstarPaths
					foreach (XmlNode speechGenerationContent in childNode.ChildNodes)
					{
						if (speechGenerationContent.Name.Equals("DialoguePath", StringComparison.InvariantCultureIgnoreCase))
						{

							DialoguePaths.Add(speechGenerationContent.InnerText);

                }
            }
        }
			}
		}

        /// <summary>
        /// The parse platform settings.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParsePlatformSettings(XmlNode node)
		{
            this.m_platformSettings = new ArrayList();

			foreach (XmlNode n in node)
			{
				if (n.Name == "Platform")
				{
                    PlatformSetting platform = ParsePlatformSetting(n);
                    this.m_platformSettings.Add(platform);
                }
            }

            m_currentPlatform = this.getDefaultPlatform();
        }

        //returns the first platform with BuildEnabled = yes or the first platform in the config file
		public PlatformSetting getDefaultPlatform()
		{
            PlatformSetting result = (PlatformSetting)this.m_platformSettings[0];
			foreach (PlatformSetting s in this.m_platformSettings)
			{
				if (s.BuildEnabled)
				{
                    result = s;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// The parse rave plugins.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseRavePlugins(XmlNode node)
		{
            this.m_ravePlugins = node.ChildNodes.Cast<XmlNode>().Where(child => child.Name == "Plugin").ToArray();
        }

        /// <summary>
        /// The parse wave attributes.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseWaveAttributes(XmlNode node)
		{
            var waveAttrs = new List<string>();
			foreach (XmlNode child in node.ChildNodes)
			{
				switch (child.Name)
				{
                    case "WaveAttribute":
                        waveAttrs.Add(child.InnerText);
                        break;
                    case "PresetPath":
						try
						{
                            this.m_presetPath = child.InnerText;
                        }
						catch (Exception e)
						{
                            Debug.WriteLine(e.Message);
                        }

                        break;
                }
            }

            this.m_waveAttributes = waveAttrs.ToArray();
        }

        /// <summary>
        /// Parse the wave slot attributes.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseWaveSlotAttributes(XmlNode node)
		{
            this.m_waveSlotAttributes = new List<string>();
			foreach (XmlNode child in node.ChildNodes)
			{
				switch (child.Name)
				{
                    case "WaveAttribute":
                        this.m_waveSlotAttributes.Add(child.InnerText);
                        break;
                }
            }
        }

        /// <summary>
        /// The parse wave refs.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseWaveRefs(XmlNode node)
		{
            this.m_waveRefsToSkip = new List<string>();
			foreach (XmlNode child in node.ChildNodes)
			{
				if (child.Name == "Skip")
				{
                    this.m_waveRefsToSkip.Add(child.InnerText);
                }
            }
        }

        /// <summary>
        /// The parse wave slot settings.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseWaveSlotSettings(XmlNode node)
		{
            this.m_staticWaveSlotSettings = new Dictionary<string, string>();

			foreach (XmlNode setting in node.ChildNodes)
			{
                this.m_staticWaveSlotSettings.Add(setting.Attributes["platform"].Value, setting.InnerText);
            }
        }

        /// <summary>
        /// Parse the wave pack groups settings.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
		private void ParseWavePackGroupsSettings(XmlNode node)
		{
            this.WavePackGroups = new List<IList<string>>();
			foreach (XmlNode groupNode in node.ChildNodes)
			{
                var items = new List<string>();
                this.WavePackGroups.Add(items);

                items.AddRange(from XmlNode itemNode in groupNode.ChildNodes select itemNode.InnerText);
            }
        }

		private void ParseMasteredCutsceneLocations(XmlNode node)
		{
			this.MasteredCutsceneLocations = new Dictionary<string, string>();

			foreach (XmlNode childNode in node.ChildNodes)
			{
				if (childNode.Attributes["Development"] == null || childNode.Attributes["Release"] == null)
				{
					continue;
				}

				string originalPath = childNode.Attributes["Development"].Value;
				string releasePath = childNode.Attributes["Release"].Value;


				if (originalPath != null && releasePath != null)
				{
					MasteredCutsceneLocations.Add(originalPath, releasePath);
				}
			}
		}

		private void ParseMutedSoloEnabledObjectTypes(XmlNode node)
		{
			this.MuteSoloEnabledObjectTypes = new List<string>();
			foreach (XmlNode childNode in node.ChildNodes)
			{
				this.MuteSoloEnabledObjectTypes.Add(childNode.InnerText);
			}

		}
        #endregion





		private void ParseAlphabeticalSortHierarchyTypes(XmlNode node)
		{
			this.AlphabeticalSortHierarchyTypes = new List<string>();
			foreach (XmlNode childNode in node.ChildNodes)
			{
				this.AlphabeticalSortHierarchyTypes.Add(childNode.InnerText);
			}
		}
    }
}
