﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace rage
{
    public class audProjectList
    {
        private readonly XmlDocument m_Document;
        private readonly string m_DocumentPath;
        private readonly List<audProjectEntry> m_Projects;

        public audProjectList(string filename)
        {
            m_Projects = new List<audProjectEntry>();
            m_Document = new XmlDocument();
            m_DocumentPath = filename;

            var dir = Path.GetDirectoryName(filename);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            if (!File.Exists(filename))
            {
                //create new XmlDocument                
                m_Document.AppendChild(m_Document.CreateElement("Projects"));
                CreateProject();
            }
            else
            {
                m_Document.Load(filename);

                foreach (XmlNode project in m_Document.DocumentElement.ChildNodes)
                {
                    Projects.Add(new audProjectEntry(project));
                }
            }
        }

        public List<audProjectEntry> Projects
        {
            get { return m_Projects; }
        }

        public void RemeberProject(string name)
        {
            if (m_Document.DocumentElement.ChildNodes.Count > 1)
            {
                XmlNode projectNode = null;
                foreach (XmlNode project in m_Document.DocumentElement.ChildNodes)
                {
                    if (String.Compare(project.Attributes["name"].Value, name, true) == 0)
                    {
                        projectNode = project;
                        break;
                    }
                }

                if (projectNode != null)
                {
                    m_Document.DocumentElement.RemoveChild(projectNode);
                    m_Document.DocumentElement.InsertBefore(projectNode, m_Document.DocumentElement.ChildNodes[0]);
                }
            }
        }

        public audProjectEntry CreateProject()
        {
            XmlElement project = m_Document.CreateElement("Project");
            string projectName = "New Project";
            int i = 2;
            while (ProjectNameExists(projectName))
            {
                projectName = projectName + i;
                i++;
            }
            project.SetAttribute("name", projectName);
            m_Document.DocumentElement.AppendChild(project);
            var pe = new audProjectEntry(project);
            Projects.Add(pe);
            return pe;
        }

        public audProjectEntry DuplicateProject(audProjectEntry projectEntry)
        {
            XmlElement project = m_Document.CreateElement("Project");
            string projectName = "New Project";
            int i = 2;
            while (ProjectNameExists(projectName))
            {
                projectName = projectName + i;
                i++;
            }
           
            project = (XmlElement)projectEntry.Node.Clone();
            project.SetAttribute("name", projectName);
            m_Document.DocumentElement.AppendChild(project);
            audProjectEntry pe = new audProjectEntry(project);
            pe.DepotRoot = string.Empty;
            pe.ProjectSettings = string.Empty;
            Projects.Add(pe);
            return pe;
        }

        private bool ProjectNameExists(string projectName)
        {
            if (m_Document.DocumentElement.ChildNodes.Count > 1)
            {
                XmlNode projectNode = null;
                foreach (XmlNode project in m_Document.DocumentElement.ChildNodes)
                {
                    if (project.Attributes["name"] != null && String.Compare(project.Attributes["name"].Value, projectName, true) == 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void Save()
        {
            m_Document.Save(m_DocumentPath);
        }
    }
}