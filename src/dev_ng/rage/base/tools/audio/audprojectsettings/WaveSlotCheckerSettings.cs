﻿// -----------------------------------------------------------------------
// <copyright file="WaveSlotCheckerSettings.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace rage
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    /// <summary>
    /// Wave slot checker settings
    /// </summary>
    public class WaveSlotCheckerSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WaveSlotCheckerSettings"/> class.
        /// </summary>
        /// <param name="settingsNode">
        /// The node.
        /// </param>
        public WaveSlotCheckerSettings(XmlNode settingsNode)
        {
            if (settingsNode == null)
            {
                throw new ArgumentNullException("settingsNode");
            }

            this.NotificationRecipients = new HashSet<string>();
            this.WaveSlotLimits = new Dictionary<string, IList<KeyValuePair<string, uint>>>(StringComparer.OrdinalIgnoreCase);

            foreach (XmlNode node in settingsNode.ChildNodes)
            {
                switch (node.Name)
                {
                    case "NotificationRecipients":
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name == "Recipient")
                                {
                                    this.NotificationRecipients.Add(childNode.InnerText);
                                }
                            }

                            break;
                        }

                    case "NotificationSender":
                        {
                            this.NotificationSender = node.InnerText;
                            break;
                        }

                    case "SmtpHost":
                        {
                            this.SmtpHost = node.InnerText;
                            break;
                        }

                    case "SmtpPort":
                        {
                            int port;
                            if (!int.TryParse(node.InnerText, out port))
                            {
                                throw new Exception("Failed to get SMTP port: " + node.InnerText);
                            }

                            this.SmtpPort = port;

                            break;
                        }

                    case "Limits":
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name != "Limit")
                                {
                                    continue;
                                }

                                if (null == childNode.Attributes)
                                {
                                    throw new Exception("<Limit> element expected to have attributes");
                                }

                                var slotAttr = childNode.Attributes["slot"];
                                if (null == slotAttr || string.IsNullOrEmpty(slotAttr.Value))
                                {
                                    throw new Exception("<Limit> element requires 'slot' attribute");
                                }

                                var maxSizeAttr = childNode.Attributes["maxSize"];
                                if (null == maxSizeAttr || string.IsNullOrEmpty(maxSizeAttr.Value))
                                {
                                    throw new Exception("<Limit> element requires 'maxSize' attribute");
                                }

                                var platformAttr = childNode.Attributes["platform"];
                                if (null == platformAttr || string.IsNullOrEmpty(platformAttr.Value))
                                {
                                    throw new Exception("<Limit> element requires 'platform' attribute");
                                }

                                var slot = slotAttr.Value;
                                uint maxSize;

                                if (!uint.TryParse(maxSizeAttr.Value, out maxSize))
                                {
                                    throw new Exception("Failed to parse limit 'maxSize' value: " + maxSizeAttr.Value);
                                }

                                var platforms = platformAttr.Value.Split(';');
                                foreach (var platform in platforms)
                                {
                                    if (string.IsNullOrEmpty(platform))
                                    {
                                        continue;
                                    }

                                    if (!this.WaveSlotLimits.ContainsKey(platform.ToUpper()))
                                    {
                                        this.WaveSlotLimits[platform.ToUpper()] = new List<KeyValuePair<string, uint>>();
                                    }

                                    var kvp = new KeyValuePair<string, uint>(slot.ToUpper(), maxSize);
                                    this.WaveSlotLimits[platform.ToUpper()].Add(kvp);
                                }
                            }

                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Gets the wave slot limits for PS3.
        /// </summary>
        public IDictionary<string, IList<KeyValuePair<string, uint>>> WaveSlotLimits { get; private set; }

        /// <summary>
        /// Gets the notification recipients.
        /// </summary>
        public HashSet<string> NotificationRecipients { get; private set; }

        /// <summary>
        /// Gets the notification sender.
        /// </summary>
        public string NotificationSender { get; private set; }

        /// <summary>
        /// Gets the SMTP host.
        /// </summary>
        public string SmtpHost { get; private set; }

        /// <summary>
        /// Gets the SMTP port.
        /// </summary>
        public int SmtpPort { get; private set; }
    }
}
