using System;
using System.Collections.Generic;

namespace rage
{
    public class audPlaceholderSpeechGeneration
    {
        public audPlaceholderSpeechGeneration(string name, bool excludePlaceholderConversation, string dialoguePath,
                                                      string characterVoiceConfig, List<audPlaceholderSpeechPack> placeholderSpeechPacks)
        {
            if(string.IsNullOrEmpty(name)) {
                throw new ArgumentNullException("name");
            }
            if (string.IsNullOrEmpty(dialoguePath))
            {
                throw new ArgumentNullException("dialoguePath");
            }
            if (string.IsNullOrEmpty(characterVoiceConfig))
            {
                throw new ArgumentNullException("characterVoiceConfig");
            }
            if(!(placeholderSpeechPacks!=null && placeholderSpeechPacks.Count>0)) {
                throw new ArgumentNullException("placeholderSpeechPacks");
            }
            Name = name;
            ExcludePlaceholderConversation = excludePlaceholderConversation;
            DialoguePath = dialoguePath;
            CharacterVoiceConfig = characterVoiceConfig;
            PlaceholderSpeechPacks = placeholderSpeechPacks;
        }

        public string Name { get; private set; }
        public bool ExcludePlaceholderConversation { get; private set; }
        public string DialoguePath { get; private set; }
        public string CharacterVoiceConfig { get; private set; }
        public List<audPlaceholderSpeechPack> PlaceholderSpeechPacks { get; private set; }
    }
}