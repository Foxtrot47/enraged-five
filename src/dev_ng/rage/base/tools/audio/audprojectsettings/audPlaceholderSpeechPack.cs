﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage {
    public class audPlaceholderSpeechPack  {

        private string defaultMaxFileSize = "716800";
        private string defaultBankFolder = "PLACEHOLDER_SPEECH";
        private string defaultAssociatedSpeechBankfolder = "SCRIPTED_SPEECH";

        public audPlaceholderSpeechPack(string name, string maxFileSize, string bankFolder, string associatedSpeechPack, string associatedSpeechBankfolder)
        {
            if(string.IsNullOrEmpty(name)) {
                throw new ArgumentNullException("name");
            }
            Name = name;
            MaxFileSize = uint.Parse(string.IsNullOrEmpty(maxFileSize)?defaultMaxFileSize:maxFileSize);
            BankFolder = string.IsNullOrEmpty(bankFolder)?defaultBankFolder:bankFolder;
            AssociatedSpeechPack = string.IsNullOrEmpty(associatedSpeechPack) ? name : associatedSpeechPack;
            AssociatedSpeechBankfolder = string.IsNullOrEmpty(associatedSpeechBankfolder) ? defaultAssociatedSpeechBankfolder : associatedSpeechBankfolder;
        }

        public string Name { get; private set; }
        public uint MaxFileSize { get; private set; }
        public string BankFolder { get; private set; }
        public string AssociatedSpeechPack { get; private set; }
        public string AssociatedSpeechBankfolder { get; private set; }
    }

}
