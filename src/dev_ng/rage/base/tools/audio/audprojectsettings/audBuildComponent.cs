﻿using System.Collections.Generic;
using System.Xml;

namespace rage
{
    public class audBuildComponent
    {
        public readonly bool EnabledStrictMode;
        public readonly string InputPath;
        public readonly string InstanceType;
        public readonly string LibraryPath;
        public readonly string OutputPath;
        public readonly string SecondOutputPath;
        public readonly string ThirdOutputPath;
        public readonly List<string> XmlPaths;

        public audBuildComponent(XmlNode node)
        {
            XmlPaths = new List<string>();

            foreach (XmlNode n in node.ChildNodes)
            {
                switch (n.Name)
                {
                    case "LibraryPath":
                        LibraryPath = n.InnerText;
                        break;
                    case "InstanceType":
                        InstanceType = n.InnerText;
                        break;
                    case "InputPath":
                        InputPath = n.InnerText;
                        break;
                    case "OutputPath":
                        OutputPath = n.InnerText;
                        break;
                    case "EnableStrictMode":
                        EnabledStrictMode = audProjectSettings.ParseBoolValue(n);
                        break;
                    case "XmlPath":
                        XmlPaths.Add(n.InnerText);
                        break;
                    case "SecondOutputPath":
                        SecondOutputPath = n.InnerText;
                        break;
                    case "ThirdOutputPath":
                        ThirdOutputPath = n.InnerText;
                        break;
                }
            }
        }

        public override string ToString()
        {
            return LibraryPath;
        }
    }
}