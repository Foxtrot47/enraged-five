﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage {
    public class audLipSyncSettings {

        public audLipSyncSettings(string lipsyncTextPath, string lipsyncBuildXMLPath, string customAnimationDir, string wavesBaseDir, string lipsyncAnimsBaseDir, List<audScriptedSpeechConfig> scriptedSpeechConfigs, List<audAmbientSpeechConfig> ambientSpeechConfigs)
        {
            if(string.IsNullOrEmpty(lipsyncTextPath)) {
                throw new ArgumentNullException("lipsyncTextPath");
            }
            if(string.IsNullOrEmpty(lipsyncBuildXMLPath)) {
                throw new ArgumentNullException("lipsyncBuildXMLPath");
            }
            if(string.IsNullOrEmpty(customAnimationDir)) {
                throw new ArgumentNullException("customAnimationDir");
            }
            if(string.IsNullOrEmpty(wavesBaseDir)) {
                throw new ArgumentNullException("wavesBaseDir");
            }
            if(string.IsNullOrEmpty(lipsyncAnimsBaseDir)) {
                throw new ArgumentNullException("lipsyncAnimsBaseDir");
            }
            if(scriptedSpeechConfigs==null) {
                throw new ArgumentNullException("scriptedSpeechConfigs");
            }
            if(ambientSpeechConfigs==null) {
                throw new ArgumentNullException("ambientSpeechConfigs");
            }

            this.LipsyncTextPath = lipsyncTextPath;
            this.LipsyncBuildXMLPath = lipsyncBuildXMLPath;
            this.CustomAnimationDir = customAnimationDir;
            this.WavesBaseDir = wavesBaseDir;
            this.LipsyncAnimsBaseDir = lipsyncAnimsBaseDir;
            this.ScriptedSpeechConfigs = scriptedSpeechConfigs;
            this.AmbientSpeechConfigs = ambientSpeechConfigs;
        }

        public string LipsyncTextPath { get; private set; }
        public string LipsyncBuildXMLPath { get; private set; }
        public string CustomAnimationDir { get; private set; }
        public string WavesBaseDir { get; private set; }
        public string LipsyncAnimsBaseDir { get; private set; }
        public List<audScriptedSpeechConfig> ScriptedSpeechConfigs { get; private set; }
        public List<audAmbientSpeechConfig> AmbientSpeechConfigs { get; private set; }
    }
}
