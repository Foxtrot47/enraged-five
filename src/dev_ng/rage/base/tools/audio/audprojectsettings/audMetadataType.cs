﻿using System.Collections;
using System.Xml;

namespace rage
{
    public class audMetadataType
    {
        public readonly bool AllowMultiCheckout;
        public readonly bool BuildOnCheckin;
        public readonly bool CanAudition;
        public readonly bool CanViewHeirarchy;
        public readonly string ClassFactoryCodePath;
        public readonly string ClassFactoryFunctionName;
        public readonly string ClassFactoryFunctionReturnType;
        public readonly string CodePath;
        public readonly string DataPath;
        public readonly bool DisableAutoCheckOut;
        public readonly string Guid;
        public readonly string IconPath;
        public readonly string NameSpaceName;
        public readonly audObjectDefinition[] ObjectDefinitions;
        public readonly string SchemaPath;
        public readonly bool SideBySideVersioning;
        public readonly string Path;
        public readonly string TemplatePath;
        public readonly string Type;
        public readonly string TypeNotUpper;
        public readonly bool UseHeirarchy;
        public readonly bool UsesBucketAllocator;

        public audMetadataType(XmlNode node)
        {
            var al = new ArrayList();
            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "Type":
                        Type = child.InnerText.ToUpper();
                        TypeNotUpper = child.InnerText;
                        break;
                    case "NameSpace":
                        NameSpaceName = child.InnerText;
                        break;
                    case "CodePath":
                        CodePath = child.InnerText;
                        break;
                    case "ObjectDefinition":
                        al.Add(new audObjectDefinition(child));
                        break;
                    case "ClassFactoryFunctionName":
                        ClassFactoryFunctionName = child.InnerText;
                        break;
                    case "ClassFactoryFunctionReturnType":
                        ClassFactoryFunctionReturnType = child.InnerText;
                        break;
                    case "ClassFactoryCodePath":
                        ClassFactoryCodePath = child.InnerText;
                        break;
                    case "BuildOnCheckIn":
                        BuildOnCheckin = audProjectSettings.ParseBoolValue(child);
                        break;
                    case "SideBySideVersioning":
                        SideBySideVersioning = audProjectSettings.ParseBoolValue(child);
                        break;
                    case "Path":
                        Path = child.InnerText;
                        break;
                    case "UsesBucketAllocator":
                        UsesBucketAllocator = audProjectSettings.ParseBoolValue(child);
                        break;
                    case "Guid":
                        Guid = child.InnerText;
                        break;
                    case "DataPath":
                        DataPath = child.InnerText;
                        break;
                    case "CanAudition":
                        CanAudition = audProjectSettings.ParseBoolValue(child);
                        break;
                    case "UseHeirarchy":
                        UseHeirarchy = audProjectSettings.ParseBoolValue(child);
                        break;
                    case "CanViewHeirarchy":
                        CanViewHeirarchy = audProjectSettings.ParseBoolValue(child);
                        break;
                    case "IconPath":
                        IconPath = child.InnerText;
                        break;
                    case "TemplatePath":
                        TemplatePath = child.InnerText;
                        break;
                    case "SchemaPath":
                        SchemaPath = child.InnerText;
                        break;
                    case "DisableAutoCheckOut":
                        DisableAutoCheckOut = audProjectSettings.ParseBoolValue(child);
                        break;
                    case "AllowMultiCheckout":
                        AllowMultiCheckout = audProjectSettings.ParseBoolValue(child);
                        break;
                }
            }

            ObjectDefinitions = (audObjectDefinition[]) al.ToArray(typeof (audObjectDefinition));
        }
    }
}