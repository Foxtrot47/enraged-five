﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rage
{
    public class audRpfGeneratorSettings
    {
        public audRpfGeneratorSettings(string p4IntegrateExe, string p4IntegrateWorkingPath, string rpfRootPath, List<string> devIngnorePackList, Dictionary<string, List<string>> releaseIgnorePackList, List<string> ignoreExceptionPackList)
        {
            if (string.IsNullOrEmpty(p4IntegrateExe)) throw new ArgumentNullException("p4IntegrateExe");
            if (string.IsNullOrEmpty(p4IntegrateWorkingPath)) throw new ArgumentNullException("p4IntegrateWorkingPath");
            if (string.IsNullOrEmpty(rpfRootPath)) throw new ArgumentNullException("rpfRootPath");
            if (devIngnorePackList == null) throw new ArgumentException("devIngnorePackList");
            if (releaseIgnorePackList == null) throw new ArgumentException("releaseIgnorePackList");
            if (ignoreExceptionPackList == null) throw new ArgumentException("ignoreExceptionPackList");

            P4IntegrateExe = p4IntegrateExe;
            P4IntegrateWorkingPath = p4IntegrateWorkingPath;
            RpfRootPath = rpfRootPath;
            DevIgnorePackList = devIngnorePackList;
            ReleaseIgnorePackList = releaseIgnorePackList;
            IgnoreExceptionPackList = ignoreExceptionPackList;
        }
        public string P4IntegrateExe { get; set; }
        public string P4IntegrateWorkingPath { get; private set; }
        public string RpfRootPath { get; private set; }
        public List<string> DevIgnorePackList { get; private set; }
        public Dictionary<string, List<string>> ReleaseIgnorePackList { get; private set; }
        public List<string> IgnoreExceptionPackList { get; private set; }
    }
}
