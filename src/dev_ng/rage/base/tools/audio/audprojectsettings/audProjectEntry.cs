﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml;

namespace rage
{
    public class audProjectEntry : INotifyPropertyChanged
    {
        private readonly XmlNode m_enableProgrammerSupport;
        private readonly XmlNode m_projectNode;
        private XmlNode m_assetManager;
        private XmlNode m_assetProject;
        private XmlNode m_assetServer;
        private XmlNode m_depotRoot;
        private XmlNode m_grabLatestPC;
        private XmlNode m_grabLatestx64XMA;
        private XmlNode m_grabLatestPS4;
        private XmlNode m_projectSettings;
        private XmlNode m_softRoot;
        private XmlNode m_workingPath;
        private XmlNode _userName;
        private XmlNode m_waveEditor;
        private string _password;

        public audProjectEntry(XmlNode node)
        {
            m_projectNode = node;

            foreach (XmlNode n in node.ChildNodes)
            {
                switch (n.Name)
                {
                    case "UserName":
                        _userName = n;
                        break;
                    case "Password":
                        Password = n.InnerText;
                        break;
                    case "AssetManager":
                        m_assetManager = n;
                        break;
                    case "AssetServer":
                        m_assetServer = n;
                        break;
                    case "AssetProject":
                        m_assetProject = n;
                        break;
                    case "ProjectSettings":
                        m_projectSettings = n;
                        break;
                    case "DepotRoot":
                        m_depotRoot = n;
                        break;
                    case "SoftRoot":
                        m_softRoot = n;
                        break;
                    case "WorkingPath":
                        m_workingPath = n;
                        break;
                    case "WaveEditor":
                        m_waveEditor = n;
                        break;
                   case "GrabLatestPC":
                        m_grabLatestPC = n;
                        break;
                    case "GrabLatestx64XMA":
                        m_grabLatestx64XMA = n;
                        break;
                    case "GrabLatestPS4":
                        this.m_grabLatestPS4 = n;
                        break;
                    case "EnableProgrammerSupport":
                        m_enableProgrammerSupport = n;
                        break;
                }
            }
        }

        public XmlNode Node
        {
            get { return m_projectNode; }
        }
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                RaisePropertyChanged("Password");
            }
        }

        public string UserName
        {
            get
            {
                if (_userName == null)
                {
                    return string.Empty;
                }
                return _userName.InnerText;
            }
            set
            {
                if (_userName == null)
                {
                    _userName = m_projectNode.OwnerDocument.CreateElement("UserName");
                    m_projectNode.AppendChild(_userName);
                }

                _userName.InnerText = value;
                RaisePropertyChanged("UserName");
            }
        }

        public string AssetManager
        {
            get
            {
                if (m_assetManager == null)
                {
                    m_assetManager = m_projectNode.OwnerDocument.CreateElement("AssetManager");
                    m_projectNode.AppendChild(m_assetManager);
                    //default perforce
                    m_assetManager.InnerText = "2";
                }

                return m_assetManager.InnerText;
            }
            set
            {
                if (m_assetManager == null)
                {
                    m_assetManager = m_projectNode.OwnerDocument.CreateElement("AssetManager");
                    m_projectNode.AppendChild(m_assetManager);
                }

                m_assetManager.InnerText = value;
                RaisePropertyChanged("AssetManager");
            }
        }

        public string AssetServer
        {
            get
            {
                if (m_assetServer == null)
                {
                    return string.Empty;
                }
                return m_assetServer.InnerText;
            }
            set
            {
                if (m_assetServer == null)
                {
                    m_assetServer = m_projectNode.OwnerDocument.CreateElement("AssetServer");
                    m_projectNode.AppendChild(m_assetServer);
                }

                m_assetServer.InnerText = value;
                RaisePropertyChanged("AssetServer");
            }
        }

        public string AssetProject
        {
            get
            {
                if (m_assetProject == null)
                {
                    return string.Empty;
                }
                return m_assetProject.InnerText;
            }
            set
            {
                if (m_assetProject == null)
                {
                    m_assetProject = m_projectNode.OwnerDocument.CreateElement("AssetProject");
                    m_projectNode.AppendChild(m_assetProject);
                }

                m_assetProject.InnerText = value;
                RaisePropertyChanged("AssetProject");
            }
        }

        public string ProjectSettings
        {
            get
            {
                if (m_projectSettings == null)
                {
                    m_projectSettings = m_projectNode.OwnerDocument.CreateElement("ProjectSettings");
                    m_projectNode.AppendChild(m_projectSettings);
                    m_projectSettings.InnerText = "ProjectSettings.xml";
                }
                return m_projectSettings.InnerText;
            }
            set
            {
                if (m_projectSettings == null)
                {
                    m_projectSettings = m_projectNode.OwnerDocument.CreateElement("ProjectSettings");
                    m_projectNode.AppendChild(m_projectSettings);
                }

                m_projectSettings.InnerText = value;
                RaisePropertyChanged("ProjectSettings");
            }
        }

        public string DepotRoot
        {
            get
            {
                if (m_depotRoot == null)
                {
                    return string.Empty;
                }
                return m_depotRoot.InnerText;
            }
            set
            {
                if (m_depotRoot == null)
                {
                    m_depotRoot = m_projectNode.OwnerDocument.CreateElement("DepotRoot");
                    m_projectNode.AppendChild(m_depotRoot);
                }

                m_depotRoot.InnerText = value;
                RaisePropertyChanged("DepotRoot");
            }
        }

        public string SoftRoot
        {
            get
            {
                if (m_softRoot == null)
                {
                    return string.Empty;
                }
                return m_softRoot.InnerText;
            }
            set
            {
                if (m_softRoot == null)
                {
                    m_softRoot = m_projectNode.OwnerDocument.CreateElement("SoftRoot");
                    m_projectNode.AppendChild(m_softRoot);
                }

                m_softRoot.InnerText = value;
                RaisePropertyChanged("SoftRoot");
            }
        }

        public string WorkingPath
        {
            get
            {
                if (null == m_workingPath)
                {
                    return string.Empty;
                }

                return m_workingPath.InnerText;
            }

            set
            {
                if (null == m_workingPath)
                {
                    m_workingPath = m_projectNode.OwnerDocument.CreateElement("WorkingPath");
                    m_projectNode.AppendChild(m_workingPath);
                }

                m_workingPath.InnerText = value;
                RaisePropertyChanged("WorkingPath");

            }
        }

        public string WaveEditor
        {
            get
            {
                if (m_waveEditor == null)
                {
                    m_waveEditor = m_projectNode.OwnerDocument.CreateElement("WaveEditor");
                    m_projectNode.AppendChild(m_waveEditor);
                    m_waveEditor.InnerText = @"C:\Program Files\Sony\Sound Forge 8.0\forge80.exe";
                }
                return m_waveEditor.InnerText;
            }
            set
            {
                if (m_waveEditor == null)
                {
                    m_waveEditor = m_projectNode.OwnerDocument.CreateElement("WaveEditor");
                    m_projectNode.AppendChild(m_waveEditor);
                }

                m_waveEditor.InnerText = value;
                RaisePropertyChanged("WaveEditor");
            }
        }

        public string Name
        {
            get
            {
                if (m_projectNode.Attributes["name"] != null)
                {
                    return m_projectNode.Attributes["name"].Value;
                }
                return string.Empty;
            }

            set
            {
                ((XmlElement)m_projectNode).SetAttribute("name", value);
                RaisePropertyChanged("Name");
            }
        }

        public bool GrabLatestPC
        {
            get
            {
                if (m_grabLatestPC == null)
                {
                    m_grabLatestPC = m_projectNode.OwnerDocument.CreateElement("GrabLatestPC");
                    m_projectNode.AppendChild(m_grabLatestPC);
                    m_grabLatestPC.InnerText = "false";
                }

                return bool.Parse(m_grabLatestPC.InnerText);
            }
            set
            {
                if (m_grabLatestPC == null)
                {
                    m_grabLatestPC = m_projectNode.OwnerDocument.CreateElement("GrabLatestPC");
                    m_projectNode.AppendChild(m_grabLatestPC);
                }

                m_grabLatestPC.InnerText = value.ToString();
                RaisePropertyChanged("GrabLatestPC");
            }
        }

        public bool GrabLatestx64XMA
        {
            get
            {
                if (m_grabLatestx64XMA == null)
                {
                    m_grabLatestx64XMA = m_projectNode.OwnerDocument.CreateElement("GrabLatestx64XMA");
                    m_projectNode.AppendChild(m_grabLatestx64XMA);
                    m_grabLatestx64XMA.InnerText = "false";
                }

                return bool.Parse(m_grabLatestx64XMA.InnerText);
            }
            set
            {
                if (m_grabLatestx64XMA == null)
                {
                    m_grabLatestx64XMA = m_projectNode.OwnerDocument.CreateElement("GrabLatestx64XMA");
                    m_projectNode.AppendChild(m_grabLatestx64XMA);
                }

                m_grabLatestx64XMA.InnerText = value.ToString();
                RaisePropertyChanged("GrabLatestx64XMA");
            }
        }

        public bool GrabLatestPS4
        {
            get
            {
                if (this.m_grabLatestPS4 == null)
                {
                    this.m_grabLatestPS4 = m_projectNode.OwnerDocument.CreateElement("GrabLatestPS4");
                    m_projectNode.AppendChild(this.m_grabLatestPS4);
                    this.m_grabLatestPS4.InnerText = "false";
                }

                return bool.Parse(this.m_grabLatestPS4.InnerText);
            }
            set
            {
                if (this.m_grabLatestPS4 == null)
                {
                    this.m_grabLatestPS4 = m_projectNode.OwnerDocument.CreateElement("GrabLatestPS4");
                    m_projectNode.AppendChild(this.m_grabLatestPS4);
                }

                this.m_grabLatestPS4.InnerText = value.ToString();

                RaisePropertyChanged("GrabLatestPS4");
            }
        }

        private void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public bool IsProgrammerSupportEnabled
        {
            get
            {
                //  We don't bother to create this in the xml, since this is a "hidden" feature
                return m_enableProgrammerSupport != null && bool.Parse(m_enableProgrammerSupport.InnerText);
            }
        }

        public override string ToString()
        {
            return Name;
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}