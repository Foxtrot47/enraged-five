﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using rage.ToolLib;

namespace rage
{
    public class audMetadataFile
    {
        public audMetadataFile(XmlNode node)
        {
            ShouldTransform = true;
            ShowInBrowser = true;
            GeneralTransformerTargets = new List<string>();

            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "Name":
                        Name = child.InnerText;
                        break;
                    case "Type":
                        Type = child.InnerText;
                        break;
                    case "OutputFile":
                        OutputFile = child.InnerText;
                        break;
                    case "ReleaseOutputFileExt":
                        ReleaseOutputFileExt = child.InnerText;
                        break;
                    case "DataPath":
                        DataPath = child.InnerText;
                        break;
                    case "Episode":
                        Episode = child.InnerText;
                        break;
                    case "Plugins":
                        PluginPaths = new List<string>();
                        foreach (XmlNode childNode in node.ChildNodes)
                        {
                            if (childNode.Name == "Path")
                            {
                                PluginPaths.Add(childNode.InnerText);
                            }
                        }
                        break;
                    case "TransformedXmlPath":
                        TransformedXmlPath = child.InnerText;
                        break;
                    case "ShouldTransform":
                        ShouldTransform = bool.Parse(child.InnerText);
                        break;
                    case "ShowInBrowser":
                        ShowInBrowser = bool.Parse(child.InnerText);
                        break;
                    case "GenerateMetadata":
                        if (GenerateMetadataElements == null)
                        {
                            GenerateMetadataElements = new List<XElement>();
                        }
                        GenerateMetadataElements.Add(Utility.ToXElement(child));
                        break;
                    case "GeneralTransformer":
                        if(child.Attributes["target"]!=null && !string.IsNullOrEmpty(child.Attributes["target"].Value))
                            GeneralTransformerTargets.Add(child.Attributes["target"].Value);
						break;
                    case "RestrictToRuntimeMode":
                        if(RestrictToRuntimeMode == null) {
                            RestrictToRuntimeMode = new List<string>();
                        }
                        RestrictToRuntimeMode.AddRange(child.InnerText.Split(';'));
                        break;
                }
            }
        }

        public string DataPath { get; private set; }
        public string Episode { get; private set; }
        public string Name { get; private set; }
        public string OutputFile { get; private set; }
        public string ReleaseOutputFileExt { get; private set; }
        public string TransformedXmlPath { get; private set; }
        public bool ShouldTransform { get; private set; }
        public bool ShowInBrowser { get; private set; }
        public List<string> RestrictToRuntimeMode { get; private set; }
        public List<string> PluginPaths { get; private set; }
        public string Type { get; private set; }
        public List<XElement> GenerateMetadataElements { get; private set; }
        public List<string> GeneralTransformerTargets { get; private set; }
    }
}
