﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage {
    public class audScriptedSpeechConfig {

        public audScriptedSpeechConfig(string path, string dialogueFilesFile, string dialogueFile, string customAnimDir)
        {
            if(string.IsNullOrEmpty(path)) {
                throw new ArgumentNullException("path");
            }
            if(string.IsNullOrEmpty(dialogueFilesFile)) {
                throw new ArgumentNullException("dialogueFilesFile");
            }
            if(string.IsNullOrEmpty(dialogueFile)) {
                throw new ArgumentNullException("dialogueFile");
            }

            this.Path = path;
            this.DialogueFile = dialogueFile;
            this.DialogueFilesFile = dialogueFilesFile;
            this.CustomAnimationDir = customAnimDir;
        }

        public string Path { get; private set; }
        public string DialogueFilesFile { get; private set; }
        public string DialogueFile { get; private set; }
        public string CustomAnimationDir { get; private set; }
    }
}
