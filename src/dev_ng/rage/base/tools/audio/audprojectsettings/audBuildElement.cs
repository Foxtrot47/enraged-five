﻿using System.Xml;
using rage.ToolLib;

namespace rage
{
    public enum audBuildElementType
    {
        Metadata = 0,
        Waves,
    }

    public class audBuildElement
    {
        public audBuildElement(XmlNode n)
        {
            if (n.Attributes != null)
            {
                if (n.Attributes["name"] != null)
                {
                    Name = n.Attributes["name"].InnerText;
                }
                if (n.Attributes["type"] != null)
                {
                    Type = Enum<audBuildElementType>.Parse(n.Attributes["type"].InnerText);
                }
            }
        }

        public audBuildElement(audBuildElementType type, string name = null)
        {
            Name = name;
            Type = type;
        }

        public string Name { get; private set; }
        public audBuildElementType Type { get; private set; }
    }
}