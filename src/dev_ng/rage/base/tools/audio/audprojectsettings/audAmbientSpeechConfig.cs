﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rage {
    public class audAmbientSpeechConfig {

        public audAmbientSpeechConfig(string path)
        {
            if(string.IsNullOrEmpty(path)) {
                throw new ArgumentNullException("path");
            }

            this.Path = path;
        }

        public string Path { get; private set; }
    }
}
