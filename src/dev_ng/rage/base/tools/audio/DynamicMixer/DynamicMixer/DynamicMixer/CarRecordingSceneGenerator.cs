﻿// -----------------------------------------------------------------------
// <copyright file="CarRecordingSceneGenerator.cs" company="Rockstar North">
// Car recording scene generator.
// </copyright>
// -----------------------------------------------------------------------

namespace rage.DynamicMixer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using rage;
    using rage.Generator;
    using rage.ToolLib.Logging;
    using rage.Types;

    /// <summary>
    /// Car recording scene generator.
    /// </summary>
    public class CarRecordingSceneGenerator : IGenerator
    {
        /// <summary>
        /// The type definitions manager.
        /// </summary>
        private ITypeDefinitionsManager typeDefManager;

        /// <summary>
        /// The car recording script directory.
        /// </summary>
        private string carRecordingScriptDir;

        /// <summary>
        /// The script document.
        /// </summary>
        private XDocument carRecordingsDoc;

        /// <summary>
        /// Initialize the generator.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="workingPath">
        /// The working path.
        /// </param>
        /// <param name="projectSettings">
        /// The project settings.
        /// </param>
        /// <param name="typeDefManager">
        /// The type definitions manager.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Init(
            ILog log,
            string workingPath,
            audProjectSettings projectSettings,
            ITypeDefinitionsManager typeDefManager,
            params string[] args)
        {
            this.typeDefManager = typeDefManager;

            if (!args.Any())
            {
                log.Error("Missing CarRecordingSceneGenerator argument(s)");
                return false;
            }

            this.carRecordingScriptDir = args[0];

            if (!Directory.Exists(this.carRecordingScriptDir))
            {
                log.Error("Car recording script directory doesn't exist: " + this.carRecordingScriptDir);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Generate the mix group car recording data.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="inputDoc">
        /// The input doc.
        /// </param>
        /// <returns>
        /// The output <see cref="XDocument"/>.
        /// </returns>
        public XDocument Generate(ILog log, XDocument inputDoc)
        {
            var objsElem = inputDoc.Element("Objects");

            var outputDoc = this.InitOutputDoc();

            if (null == outputDoc)
            {
                log.Error("Failed to initialize the output document");
                return null;
            }

            if (!this.LoadCarRecordingsDoc(log))
            {
                log.Error("Failed to load script data");
                return null;
            }

            var newScenes = this.GenerateCarRecordingScenes(log, objsElem);
            if (null == newScenes)
            {
                log.Error("Failed to generate new car recording scenes");
                return null;
            }

            // Add new scene elements to output document
            foreach (var newScene in newScenes)
            {
                outputDoc.Root.Add(newScene);
            }

            return outputDoc;
        }

        /// <summary>
        /// Perform any actions on shutdown.
        /// </summary>
        public void Shutdown()
        {
        }

        /// <summary>
        /// Initialize the output document.
        /// </summary>
        /// <returns>
        /// The output document <see cref="XDocument"/>.
        /// </returns>
        private XDocument InitOutputDoc()
        {
            var outputDoc = new XDocument();
            var objsElem = new XElement("Objects");
            outputDoc.Add(objsElem);
            return outputDoc;
        }

        /// <summary>
        /// Load the car recordings document.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadCarRecordingsDoc(ILog log)
        {
            this.carRecordingsDoc = new XDocument();
            var objsElem = new XElement("Objects");
            this.carRecordingsDoc.Add(objsElem);

            var filePaths = Directory.GetFiles(this.carRecordingScriptDir, "*.xml", SearchOption.AllDirectories);
            foreach (var filePath in filePaths)
            {
                try
                {
                    var doc = XDocument.Load(filePath);
                    foreach (var childElem in doc.Element("Objects").Elements())
                    {
                        this.carRecordingsDoc.Root.Add(childElem);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Failed to load script data from " + filePath + ": " + ex.Message);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Generate new scenes for car recordings to allow each car recording
        /// to have its own instance of particular scenes.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="objsElem">
        /// The input object elements containing existing scene data.
        /// </param>
        /// <returns>
        /// List of new car recording scenes <see cref="IList"/>.
        /// </returns>
        private IList<XElement> GenerateCarRecordingScenes(ILog log, XElement objsElem)
        {
            var sceneElems = new List<XElement>();

            foreach (var carRecordingElem in this.carRecordingsDoc.Root.Elements("CarRecordingAudioSettings"))
            {
                var carRecordingName = carRecordingElem.Attribute("name").Value;

                // Process OneShotScene elements
                foreach (var eventElem in carRecordingElem.Elements("Event"))
                {
                    var oneShotSceneElem = eventElem.Element("OneShotScene");

                    if (null == oneShotSceneElem)
                    {
                        continue;
                    }

                    var newSceneElems = this.GenerateSceneObject(log, objsElem, carRecordingName, oneShotSceneElem.Value);

                    if (null != newSceneElems)
                    {
                        sceneElems.AddRange(newSceneElems);
                    }
                    else
                    {
                        log.Error("Failed to create OneShotScene for car recording: " + carRecordingName);
                        return null;
                    }
                }

                // Process PersistentMixerScenes elements
                foreach (var persistentScenesElem in carRecordingElem.Elements("PersistentMixerScenes"))
                {
                    var sceneElem = persistentScenesElem.Element("Scene");

                    if (null == sceneElem)
                    {
                        continue;
                    }

                    var newSceneElems = this.GenerateSceneObject(log, objsElem, carRecordingName, sceneElem.Value);

                    if (null != newSceneElems)
                    {
                        sceneElems.AddRange(newSceneElems);
                    }
                    else
                    {
                        log.Error("Failed to create persistent scene for car recording: " + carRecordingName);
                    }
                }
            }

            return sceneElems;
        }

        /// <summary>
        /// Generate a new scene object based on an existing scene with same name.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="objsElem">
        /// The input object elements containing existing scene data.
        /// </param>
        /// <param name="carRecordingName">
        /// The car recording name.
        /// </param>
        /// <param name="sceneName">
        /// The scene name.
        /// </param>
        /// <returns>
        /// The new scene object element <see cref="XElement"/>.
        /// </returns>
        private IList<XElement> GenerateSceneObject(ILog log, XElement objsElem, string carRecordingName, string sceneName)
        {
            var newElems = new List<XElement>();

            // Find existing Scene element with matching name
            var foundSceneElem = (
                from existingSceneElem in objsElem.Elements("MixerScene") 
                let existingSceneName = existingSceneElem.Attribute("name").Value 
                where sceneName.Equals(existingSceneName) 
                select existingSceneElem).FirstOrDefault();

            if (null == foundSceneElem)
            {
                log.Error("Failed to find existing Scene with name: " + sceneName);
                return null;
            }

            // Duplicate existing Scene element
            var newSceneElem = new XElement(foundSceneElem);
            newSceneElem.SetAttributeValue("name", carRecordingName + "_" + sceneName);
            newElems.Add(newSceneElem);

            // Duplicate existing MixGroup element
            foreach (var patchGroupElem in newSceneElem.Elements("PatchGroup"))
            {
                var mixGroupElem = patchGroupElem.Element("MixGroup");
                if (null == mixGroupElem)
                {
                    continue;
                }

                var mixGroupName = mixGroupElem.Value;

                // Find existing MixGroup element with matching name
                var foundMixGroupElem = (
                    from existingMixGroupElem in objsElem.Elements("MixGroup")
                    let existingMixGroupName = existingMixGroupElem.Attribute("name").Value
                    where mixGroupName.Equals(existingMixGroupName)
                    select existingMixGroupElem).FirstOrDefault();

                if (null == foundMixGroupElem)
                {
                    log.Error("Failed to find existing MixGroup with name: " + mixGroupName);
                    return null;
                }

                // Duplicate existing MixGroup element
                var newMixGroupElem = new XElement(foundMixGroupElem);
                newMixGroupElem.SetAttributeValue("name", carRecordingName + "_MG");
                newElems.Add(newMixGroupElem);

                // Update MixGroup reference
                mixGroupElem.SetValue(carRecordingName + "_MG");
            }

            return newElems;
        }
    }
}
