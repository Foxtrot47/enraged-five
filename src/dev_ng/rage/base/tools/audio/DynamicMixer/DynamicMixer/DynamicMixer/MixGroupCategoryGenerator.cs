﻿// -----------------------------------------------------------------------
// <copyright file="MixGroupCategoryGenerator.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace rage.DynamicMixer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using rage;
    using rage.Compiler;
    using rage.Generator;
    using rage.ToolLib.DataStructures;
    using rage.ToolLib.Logging;
    using rage.Types;

    /// <summary>
    /// Dynamic mixer map generator class.
    /// </summary>
    public class MixGroupCategoryGenerator : IGenerator
    {
        /// <summary>
        /// The categories xml path.
        /// </summary>
        private string categoriesXmlPath;
        /// <summary>
        /// The type definitions manager.
        /// </summary>
        private ITypeDefinitionsManager typeDefManager;

        /// <summary>
        /// The category XDocument.
        /// </summary>
        private Tree<string> categoryTree;  

        /// <summary>
        /// The mix group patches.
        /// </summary>
        private Dictionary<string, HashSet<string>> mixGroupPatches;
        /// <summary>
        /// The mix group names.
        /// </summary>
        private List<string> mixGroupsNames;

        /// <summary>
        /// The patch categories.
        /// </summary>
        private Dictionary<string, IList<string>> patchCategories;

        /// <summary>
        /// The mix group categories.
        /// </summary>
        private Dictionary<string, HashSet<string>> mixGroupCategories;

        /// <summary>
        /// The mix group category trees (i.e. cut down version of categoryTree
        /// that contains just the categories and structure relevant to
        /// each mix group).
        /// </summary>
        private Dictionary<string, Tree<string>> mixGroupCategoryTrees; 

        /// <summary>
        /// Initialize the dynamic mixer map generator.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="workingPath">
        /// The working path.
        /// </param>
        /// <param name="projectSettings">
        /// The project settings.
        /// </param>
        /// <param name="typeDefManager">
        /// The type definitions manager.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// Indication of whether method was successful <see cref="bool"/>.
        /// </returns>
        public bool Init(
            ILog log,
            string workingPath,
            audProjectSettings projectSettings,
            ITypeDefinitionsManager typeDefManager,
            params string[] args)
        {
            if (!args.Any())
            {
                log.Error("Missing MixGroupCategoryGenerator argument(s)");
                return false;
            }

            this.categoriesXmlPath = args[0];
            // Check categories file path is valid
            if (!File.Exists(this.categoriesXmlPath))
            {
                log.Error("Categories XML file not found: " + this.categoriesXmlPath);
                return false;
            }

            this.typeDefManager = typeDefManager;

            return true;
        }

        /// <summary>
        /// generate the dynamic mixer map from an input XDocument.
        /// </summary>
        /// <param name="log">
        /// The log to output to.
        /// </param>
        /// <param name="inputDoc">
        /// The input XDocument.
        /// </param>
        /// <returns>
        /// The dynamic mixer map output <see cref="XDocument"/>.
        /// </returns>
        public XDocument Generate(ILog log, XDocument inputDoc)
        {
            // We need to run the composite field instance aggregator on the
            // input XDocument to instantiate all the data we require
            var compositeFieldInstanceAggregator = new CompositeFieldInstanceAggregator(this.typeDefManager.FindTypeDefinition);
            compositeFieldInstanceAggregator.Process(inputDoc);

            var objsElem = inputDoc.Element("Objects");

            // Check that objects list exists in input
            if (objsElem == null)
            {
                log.Error("'Objects' element not found in input XDocument.");
                return null;
            }

            if (!this.InitCategoryTree())
            {
                log.Error("Failed to initialize category tree");
                return null;
            }

            if (!this.InitMixGroupPatchList(log, objsElem))
            {
                log.Error("Failed to initialize mix group patch list");
                return null;
            }

            if (!this.InitPatchCategories(log, objsElem))
            {
                log.Error("Failed to initialize patch categories list");
                return null;
            }
            
            if (!this.InitMixGroupCategories())
            {
                log.Error("Failed to initialize mix group categories list");
                return null;
            }

            if (!this.InitMixGroupCategoryTrees(log))
            {
                log.Error("Failed to initialize mix group category XDocuments list");
                return null;
            }

            // Create output data
            var outputDoc = this.GenerateMixGroupCategoryMaps();

            if (null == outputDoc)
            {
                log.Error("Failed to generate output XDocument");
                return null;
            }

            return outputDoc;
        }

        /// <summary>
        /// Initialize the main category tree structure (loaded from XML file).
        /// </summary>
        /// <returns>
        /// Indication of whether method was successful <see cref="bool"/>.
        /// </returns>
        private bool InitCategoryTree()
        {
            var categoryDoc = XDocument.Load(this.categoriesXmlPath);
            this.categoryTree = new Tree<string>();

            var categoryElems = categoryDoc.Root.Elements("Category").ToArray();

            // Get the base element
            var baseElem = categoryElems.FirstOrDefault(elem => elem.Attribute("name").Value.Equals("BASE"));

            // Init tree root
            var baseTreeNode = new TreeNode<string>(null) { Data = "BASE" };
            this.categoryTree.Root = baseTreeNode;

            // Populate the category tree
            foreach (var childCategoryElem in baseElem.Elements("CategoryRef"))
            {
                this.InitCategoryTreeHelper(categoryDoc.Root, baseTreeNode, childCategoryElem.Value);
            }

            return true;
        }

        /// <summary>
        /// Helper method for initializing the main category tree.
        /// </summary>
        /// <param name="root">
        /// The root of the source XDocument.
        /// </param>
        /// <param name="parent">
        /// The current parent tree node.
        /// </param>
        /// <param name="categoryName">
        /// The current category name.
        /// </param>
        private void InitCategoryTreeHelper(XElement root, TreeNode<string> parent, string categoryName)
        {
            var treeNode = new TreeNode<string>(parent) { Data = categoryName };
            parent.Children.Add(treeNode);

            var categoryElem =
                root.Elements("Category").FirstOrDefault(elem => elem.Attribute("name").Value.Equals(categoryName));

            foreach (var childCategoryElem in categoryElem.Elements("CategoryRef"))
            {
                this.InitCategoryTreeHelper(root, treeNode, childCategoryElem.Value);
            }
        }

        /// <summary>
        /// Initialize the list of mix group patches.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="objsElem">
        /// The objects element containing the MixerScene we require.
        /// </param>
        /// <returns>
        /// Indication of whether method was successful <see cref="bool"/>.
        /// </returns>
        private bool InitMixGroupPatchList(ILog log, XElement objsElem)
        {
            this.mixGroupPatches = new Dictionary<string, HashSet<string>>();
            this.mixGroupsNames = new List<string>();

            // First of all store the name of the existing mixgroups in the data.
            foreach (var obj in objsElem.Elements("MixGroup"))
            {
                if (obj != null && obj.Attribute("name") != null) 
                {
                    this.mixGroupsNames.Add(obj.Attribute("name").Value);
                }
            }
            // The MixerScene contains a list of patch groups
            foreach (var obj in objsElem.Elements("MixerScene"))
            {
                var patchGroupElem = obj.Element("PatchGroup");

                if (null == patchGroupElem)
                {
                    continue;
                }

                foreach (var patchGroup in patchGroupElem.Elements("Instance"))
                {
                    // Get patch name from patch group
                    var patch = patchGroup.Element("Patch");
                    if (null == patch)
                    {
                        log.Error("Failed to find Patch element in PatchGroup: " + patchGroup);
                        return false;
                    }

                    var patchName = patch.Value;

                    // Get mix group name from patch group
                    var mixGroup = patchGroup.Element("MixGroup");
                    if (null == mixGroup)
                    {
                        // A patch group doesn't have to have a mix group
                        continue;
                    }

                    var mixGroupName = mixGroup.Value;

                    if (string.IsNullOrEmpty(mixGroupName))
                    {
                        // Mix group may be blank - safe to skip
                        continue;
                    }

                    // Store patch name against mix group name for later use
                    if (!this.mixGroupPatches.ContainsKey(mixGroupName))
                    {
                        this.mixGroupPatches[mixGroupName] = new HashSet<string>();
                    }

                    this.mixGroupPatches[mixGroupName].Add(patchName);
                }
            }

            // Also need to generate a category map for MixGroup elements not associated with a scene
            foreach (var mixGroupElem in objsElem.Elements("MixGroup"))
            {
                var mixGroupNameAttr = mixGroupElem.Attribute("name");
                if (null == mixGroupNameAttr)
                {
                    throw new Exception("Failed to find MixGroup name attribute: " + mixGroupElem);
                }

                var mixGroupName = mixGroupNameAttr.Value;

                if (!this.mixGroupPatches.ContainsKey(mixGroupName))
                {
                    this.mixGroupPatches[mixGroupName] = new HashSet<string>();
                }
            }

            return true;
        }

        /// <summary>
        /// Initialize the patch categories list.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <param name="objsElem">
        /// The objects element containing the MixerPatch we require.
        /// </param>
        /// <returns>
        /// Indication of whether method was successful <see cref="bool"/>.
        /// </returns>
        private bool InitPatchCategories(ILog log, XElement objsElem)
        {
            var settingsElems = objsElem.Elements("MixerPatch");
            this.patchCategories = new Dictionary<string, IList<string>>();

            foreach (var settingsElem in settingsElems)
            {
                // Get the patch settings name
                var settingsNameAttr = settingsElem.Attribute("name");
                if (null == settingsNameAttr)
                {
                    log.Error("Failed to find MixerPatch name attribute: " + settingsElem);
                    return false;
                }

                var settingsName = settingsNameAttr.Value;

                // We expect patch settings name to be unique
                if (this.patchCategories.ContainsKey(settingsName))
                {
                    log.Error("Duplicate patch settings name detected: " + settingsName);
                    return false;
                }

                var categoryNames = new List<string>();

                var mixCategoriesElem = settingsElem.Element("MixCategories");

                if (null == mixCategoriesElem)
                {
                    continue;
                }

                foreach (var categoryElem in mixCategoriesElem.Elements("Instance"))
                {
                    var categoryNameElem = categoryElem.Element("Name");

                    if (null == categoryNameElem)
                    {
                        log.Error("Failed to find Name element in MixCategory");
                        return false;
                    }

                    var categoryName = categoryNameElem.Value;

                    // Add category name to list of names
                    categoryNames.Add(categoryName);
                }

                // Record list of Mixcategory elements against patch setting name
                this.patchCategories.Add(settingsName, categoryNames);
            }

            return true;
        }

        /// <summary>
        /// Initialize the mix group categories from the mix group patches list.
        /// </summary>
        /// <returns>
        /// Indication of whether method was successful <see cref="bool"/>.
        /// </returns>
        private bool InitMixGroupCategories()
        {
            this.mixGroupCategories = new Dictionary<string, HashSet<string>>();

            // Build up list of distinct category names against corresponding mix groups
            foreach (var entry in this.mixGroupPatches)
            {
                var mixGroupName = entry.Key;
                var patchNames = entry.Value;

                // Initialize mix group categories list
                if (!this.mixGroupCategories.ContainsKey(mixGroupName))
                {
                    this.mixGroupCategories[mixGroupName] = new HashSet<string>();
                }

                foreach (var patchName in patchNames)
                {
                    IList<string> categoryNames;
                    if (this.patchCategories.TryGetValue(patchName, out categoryNames))
                    {
                        foreach (var categoryName in categoryNames)
                        {
                            this.mixGroupCategories[mixGroupName].Add(categoryName);
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Initialize the mix group category trees for each mix group.
        /// Each mix group only requires a reduced version of the main category
        /// tree, which includes just the categories required by the
        /// mix group.
        /// </summary>
        /// <param name="log">
        /// The log.
        /// </param>
        /// <returns>
        /// Indication of whether method was successful <see cref="bool"/>.
        /// </returns>
        private bool InitMixGroupCategoryTrees(ILog log)
        {
            this.mixGroupCategoryTrees = new Dictionary<string, Tree<string>>();

            foreach (var entry in this.mixGroupCategories)
            {
                var mixGroupName = entry.Key;
                var categories = entry.Value;

                // We should only have one instance of each mix group tree, but check to be safe
                if (this.mixGroupCategoryTrees.ContainsKey(mixGroupName))
                {
                    log.Error("Duplicate mix group name detected in mixGroupCategoryTrees: " + mixGroupName);
                    return false;
                }

                // Create category tree for mix group
                var tree = new Tree<string>();
                var root = new TreeNode<string>(null) { Data = "BASE" };
                tree.Root = root;

                if (categories.Any())
                {
                    this.InitMixGroupCategoryTree(root, this.categoryTree.Root, categories);

                    // Determine tree root
                    if (root.Children.Count == 1 && !categories.Contains("BASE"))
                    {
                        // No need for "BASE" node if it only has one
                        // child and "BASE" not directly referenced - 
                        // We can cut out the "BASE" node
                        tree.Root = root.Children[0];
                    }
                }

                this.mixGroupCategoryTrees[mixGroupName] = tree;
            }

            return true;
        }

        /// <summary>
        /// Initialize the mix group category tree for a mix group in a recursive manner.
        /// The resulting tree is a cut down version of the main category tree, with only
        /// the category nodes specified in the categoriesNeeded list.
        /// </summary>
        /// <param name="parent">
        /// The current parent tree node.
        /// </param>
        /// <param name="originalNode">
        /// The original tree node used as a reference for the tree structure required.
        /// </param>
        /// <param name="categoriesNeeded">
        /// The categories needed.
        /// </param>
        private void InitMixGroupCategoryTree(TreeNode<string> parent, TreeNode<string> originalNode, HashSet<string> categoriesNeeded)
        {
            foreach (var childNode in originalNode.Children)
            {
                // We only want to add new tree node if category is required
                if (categoriesNeeded.Contains(childNode.Data))
                {
                    var newNode = new TreeNode<string>(parent) { Data = childNode.Data };
                    parent.Children.Add(newNode);
                    this.InitMixGroupCategoryTree(newNode, childNode, categoriesNeeded);
                }
                else
                {
                    this.InitMixGroupCategoryTree(parent, childNode, categoriesNeeded);
                }
            }
        }

        /// <summary>
        /// Generate mix group category maps for our mix groups.
        /// </summary>
        /// <returns>
        /// The XDocument containing the mix group category maps <see cref="XDocument"/>.
        /// </returns>
        private XDocument GenerateMixGroupCategoryMaps()
        {
            var outputDoc = new XDocument();
            var objsElem = new XElement("Objects");
            outputDoc.Add(objsElem);

             foreach (var entry in this.mixGroupCategoryTrees)
             {
                 var mixGroupName = entry.Key;
                 var tree = entry.Value;
                 if (this.mixGroupsNames.Contains(mixGroupName))
                 {
                     var mapElem = new XElement("MixGroupCategoryMap");
                     mapElem.SetAttributeValue("name", mixGroupName + "_MAP");
                     objsElem.Add(mapElem);
 
                     this.GenerateMixGroupCategoryMapEntries(mapElem, tree.Root);
                 }
             }

            return outputDoc;
        }

        /// <summary>
        /// Generate the mix group category map entries for a given node and it's child nodes.
        /// Entries are added to as children to the specified map element.
        /// </summary>
        /// <param name="mapElem">
        /// The parent map element.
        /// </param>
        /// <param name="currentNode">
        /// The current node (should be tree root node for first call to method).
        /// </param>
        private void GenerateMixGroupCategoryMapEntries(XElement mapElem, TreeNode<string> currentNode)
        {
            // Create Entry element and add to map element
            var entryElem = new XElement("Entry");
            mapElem.Add(entryElem);

            // Create category and parent elements and add to Entry element
            var categoryNameElem = new XElement("CategoryHash") { Value = currentNode.Data };
            entryElem.Add(categoryNameElem);

            var parentName = null != currentNode.Parent ? currentNode.Parent.Data : "NULL";
            var parentNameElem = new XElement("ParentHash") { Value = parentName };
            entryElem.Add(parentNameElem);

            // Recursively process remaining child nodes of current tree node
            foreach (var childNode in currentNode.Children)
            {
                this.GenerateMixGroupCategoryMapEntries(mapElem, childNode);
            }
        }

        /// <summary>
        /// Perform any actions on shutdown.
        /// </summary>
        public void Shutdown()
        {
            // Do nothing
        }
    }
}
