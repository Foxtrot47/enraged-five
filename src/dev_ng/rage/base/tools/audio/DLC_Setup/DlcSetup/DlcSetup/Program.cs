﻿// Copyright: Rockstar North 2017

#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using Rockstar.AssetManager.Interfaces;
using Rockstar.AssetManager.P4API;

#endregion

namespace DlcSetup
{
    class Program
    {
        #region Private Methods

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                throw new Exception("Please drag content to setup");
            }
            if (!File.Exists(args[0]))
            {
                throw new Exception("The content file does not exist");
            }

            //gta5_dlc/mpPacks/mp{dlcName}/build/dev_ng/content.xml

            string dlcName = args[0].Split('/', '\\').Where(s => s.Contains("mp"))
                .Select(smp => smp.Replace("mp", string.Empty)).Last();

            if (string.IsNullOrEmpty(dlcName))
            {
                throw new Exception("The content file seems to be in a bad location.");
            }

            string packname = GetSetting("packname");
            if (string.IsNullOrEmpty(packname))
            {
                throw new Exception("Enter dlc name in the exe config file");
            }

            string[] platforms = GetSetting("platforms").Split(',');

            Console.WriteLine("Connecting to perforce.");

            P4AssetManager p4AssetManager = new P4AssetManager(GetSetting("p4host"), GetSetting("p4port"),
                GetSetting("p4workspace"), GetSetting("p4username"), GetSetting("p4password"),
                GetSetting("p4depotroot"), false, null, null);
            IChangeList changelist = p4AssetManager.CreateChangeList($"{packname} setup");
            Console.WriteLine($"Created changelist {changelist.Number}");
            List<string> builtWavesLocs = platforms
                .Select(platform => GetSetting("builtWavesLoc").Replace("{platform}", platform)).ToList();
            foreach (string builtWavesLoc in builtWavesLocs)
            {
                Console.WriteLine($"Getting latest on {builtWavesLoc}");

                p4AssetManager.GetLatest(builtWavesLoc, false);
                string builtWavesLocal = p4AssetManager.GetLocalPath(builtWavesLoc);
                XmlDocument xmlDocument = new XmlDocument();
                string builtPacklist = Path.Combine(builtWavesLocal, "BuiltWaves_Pack_List.xml");
                IAsset asset = changelist.CheckoutAsset(builtPacklist, true);
                xmlDocument.Load(builtPacklist);
                Console.WriteLine($"Writing packlist {builtPacklist}");
                string dlcPackFileName = $"DLC_{packname.ToUpper()}_PACK_FILE.xml";
                if (xmlDocument.DocumentElement.ChildNodes.OfType<XmlElement>()
                    .Any(n => n.InnerText.Contains(dlcPackFileName)))
                {
                    asset.Revert();
                    Console.WriteLine("Builtwaves seems to exist in BuiltWaves_Pack_List");
                    continue;
                }
                XmlNode node = xmlDocument.DocumentElement.AppendChild(xmlDocument.CreateElement("PackFile"));

                node.InnerText = dlcPackFileName;
                xmlDocument.Save(builtPacklist);


                XmlDocument newBuiltWavesXML = new XmlDocument();
                XmlNode packNode = newBuiltWavesXML.AppendChild(newBuiltWavesXML.CreateElement("Pack"));
                XmlAttribute nameAttribute = packNode.Attributes.Append(newBuiltWavesXML.CreateAttribute("name"));
                nameAttribute.Value = $"DLC_{packname.ToUpper()}";
                string newBuiltWaves = Path.Combine(builtWavesLocal, dlcPackFileName);
                Console.WriteLine($"Creating packfile {newBuiltWaves}");
                newBuiltWavesXML.Save(newBuiltWaves);
                changelist.MarkAssetForAdd(newBuiltWaves);
            }
            //add project settings
            XmlTextToAdd xmlTextToAdd = new XmlTextToAdd {dlcName = packname};
            string newText = xmlTextToAdd.TransformText();
            p4AssetManager.GetLatest(GetSetting("projectSettings"), false);
            string projectSettingsFile = p4AssetManager.GetLocalPath(GetSetting("projectSettings"));
            IAsset projectAsset = changelist.CheckoutAsset(projectSettingsFile, true);
            Console.WriteLine($"Adding {packname} to project settings {projectSettingsFile}");
            string textReader = File.ReadAllText(projectSettingsFile);
            bool alreadyIn = textReader.Contains(newText);
            if (!alreadyIn)
            {
                int indexInserstion = textReader.IndexOf("</MetadataSettings>");
                string textWriter = textReader.Insert(indexInserstion - 1, newText + "\n");
                File.WriteAllText(projectSettingsFile, textWriter);
            }
            else
            {
                Console.WriteLine($"Already found {packname} in project settings {projectSettingsFile}, reverting");
                projectAsset.Revert();
            }

            //add dummy files
            string[] objectTypes = GetSetting("objectTypes").Split(',');
            List<string> dummyXmlFiles = objectTypes
                .Select(objectType => Path.Combine(GetSetting("objectLocation").Replace("{dlcName}", packname),
                    objectType, "dummy.xml")).ToList();
            foreach (string dummyXmlFile in dummyXmlFiles)
            {
                if (p4AssetManager.ExistsAsAsset(dummyXmlFile))
                {
                    continue;
                }
                XmlDocument dummyObjectBank = new XmlDocument();
                dummyObjectBank.AppendChild(dummyObjectBank.CreateElement("Objects"));
                Directory.CreateDirectory(Path.GetDirectoryName(dummyXmlFile));
                dummyObjectBank.Save(dummyXmlFile);
                changelist.MarkAssetForAdd(dummyXmlFile);
                Console.WriteLine($"Adding {dummyXmlFile} to {changelist.Number}");
            }



            //Content XML SETUP
            IChangeList contentXmlChangelist = p4AssetManager.CreateChangeList("Audio Setup for content.xml");
            contentXmlChangelist.CheckoutAsset(args[0], false);
            string contentFile = args[0];
            string contentXml = File.ReadAllText(contentFile);

            DataFiles dataFiles = new DataFiles(dlcName, packname);
            FilesToEnable filesToEnable = new FilesToEnable(dlcName, packname);

            string dataFilesText = dataFiles.TransformText();
            if (!contentXml.Contains(dataFilesText))
            {
                int indexInserstion = contentXml.IndexOf("</dataFiles>");
                contentXml = contentXml.Insert(indexInserstion - 1, dataFilesText + "\n");
            }

            string filesEnabledText = filesToEnable.TransformText();
            if (!contentXml.Contains(filesEnabledText))
            {
                int indexInserstion = contentXml.IndexOf("</filesToEnable>");
                contentXml = contentXml.Insert(indexInserstion - 1, filesEnabledText + "\n");
            }
            Console.WriteLine($"Updating {contentFile} to Changelist {contentXmlChangelist.Number}");

            File.WriteAllText(contentFile, contentXml);
            
            Console.WriteLine($"Updating {contentFile} to Changelist {contentXmlChangelist.Number}");

            Console.WriteLine();
            Console.WriteLine($"Successfully created {packname} dlc pack on rave.\n" +
                              $"Test in rave and submit CL:{changelist.Number}.\n" +
                              $"Changelist: {contentXmlChangelist.Number} is for content XML, submit once we have data.");

            Console.WriteLine();
            Console.WriteLine("New branch mapping is being displayed, please copy and paste to new branch.");
            string branching = Path.ChangeExtension(Path.GetTempFileName(), ".txt");
            File.WriteAllText(branching, new AudioMoveBranch(dlcName, packname).TransformText());
            Process.Start(branching);

            Console.ReadKey();
        }

        static string GetSetting(string setting)
        {
            return ConfigurationManager.AppSettings[setting];
        }

        #endregion
    }

    public partial class DataFiles
    {
        public string dlcName = "DlcName";
        public string packName = "PackName";

        public DataFiles(string dlcName, string packName)
        {
            this.dlcName = dlcName;
            this.packName = packName;
        }
    }

    public partial class FilesToEnable
    {
        public string dlcName;
        public string packName;

        public FilesToEnable(string dlcName, string packName)
        {
            this.dlcName = dlcName;
            this.packName = packName;
        }
    }

    public partial class AudioMoveBranch
    {
        public string dlcName;
        public string packName;

        public AudioMoveBranch(string dlcName, string packName)
        {
            this.dlcName = dlcName;
            this.packName = packName;
        }
    }
}