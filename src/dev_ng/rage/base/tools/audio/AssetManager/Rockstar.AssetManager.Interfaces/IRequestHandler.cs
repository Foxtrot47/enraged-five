﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRequestHandler.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the IRequestHandler type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Interfaces
{
    using System.Threading;

    /// <summary>
    /// The RequestHandler interface.
    /// </summary>
    public interface IRequestHandler
    {
        #region Properties

        /// <summary>
        /// Gets the asset manager.
        /// </summary>
        IAssetManager AssetManager { get; }

        /// <summary>
        /// Gets the host.
        /// </summary>
        string Host { get; }

        /// <summary>
        /// Gets the port.
        /// </summary>
        string Port { get; }

        /// <summary>
        /// Gets the workspace.
        /// </summary>
        string Workspace { get; }

        /// <summary>
        /// Gets the username.
        /// </summary>
        string Username { get; }

        /// <summary>
        /// Gets the password.
        /// </summary>
        string Password { get; }

        /// <summary>
        /// Gets a value indicating whether to use login functionality.
        /// </summary>
        bool UseLogin { get; }

        /// <summary>
        /// Gets the pending request count.
        /// </summary>
        int PendingCount { get; }

        #endregion

        #region Connection

        /// <summary>
        /// Connect to the asset manager.
        /// </summary>
        /// <returns>
        /// Indication of connection success <see cref="bool"/>.
        /// </returns>
        bool Connect();

        /// <summary>
        /// Disconnect from the asset manager.
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Reconnect to the asset manager.
        /// </summary>
        /// <returns>
        /// Indication of connection success <see cref="bool"/>.
        /// </returns>
        bool Reconnect();

        /// <summary>
        /// Get asset manager connection status.
        /// </summary>
        /// <returns>
        /// Indication of connection status <see cref="bool"/>.
        /// </returns>
        bool IsConnected();

        #endregion

        #region Requests

        /// <summary>
        /// Actions a request to the asset manager (synchronously).
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        void ActionRequest(IRequest request);

        /// <summary>
        /// Actions a request to the asset manager (asynchronously).
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// Semaphore of the asset manager request <see cref="Semaphore"/>.
        /// </returns>
        Semaphore ActionRequestAsync(IRequest request);

        #endregion
    }
}
