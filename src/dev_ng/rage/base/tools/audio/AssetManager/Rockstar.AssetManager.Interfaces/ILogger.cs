﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rockstar.AssetManager.Interfaces {
    public interface ILogger {
        void writeLine(String line);
    }
}
