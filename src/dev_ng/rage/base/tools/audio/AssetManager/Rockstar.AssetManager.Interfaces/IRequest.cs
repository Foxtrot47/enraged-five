﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRequest.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the IRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Interfaces
{
    using System.Collections.Generic;
    using System.Threading;

    using Rockstar.AssetManager.Infrastructure.Enums;

    /// <summary>
    /// The Request interface.
    /// </summary>
    public interface IRequest
    {
        /// <summary>
        /// Gets the semaphore.
        /// </summary>
        Semaphore Semaphore { get; }

        /// <summary>
        /// Gets or sets the request status.
        /// </summary>
        RequestStatus Status { get; set; }

        /// <summary>
        /// Gets the messages generated when action the request.
        /// </summary>
        List<string> Messages { get; }

        /// <summary>
        /// Gets the formatted messages.
        /// </summary>
        string FormattedMessages { get; }

        /// <summary>
        /// Gets the warnings generated when action the request.
        /// </summary>
        List<string> Warnings { get; }

        /// <summary>
        /// Gets the formatted warnings.
        /// </summary>
        string FormattedWarnings { get; }

        /// <summary>
        /// Gets the errors generated when action the request.
        /// </summary>
        List<string> Errors { get; }

        /// <summary>
        /// Gets the formatted errors.
        /// </summary>
        string FormattedErrors { get; }

        /// <summary>
        /// Indicates whether there were errors when the request was performed.
        /// </summary>
        /// <returns>
        /// Error indicator <see cref="bool"/>.
        /// </returns>
        bool HasErrors();

        /// <summary>
        /// Indicates whether there were warnings when the request was performed.
        /// </summary>
        /// <returns>
        /// Warning indicator <see cref="bool"/>.
        /// </returns>
        bool HasWarnings();
    }
}
