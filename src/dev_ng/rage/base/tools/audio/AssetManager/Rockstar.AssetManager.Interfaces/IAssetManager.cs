﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAssetManager.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the IAssetManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Interfaces
{
    using System;
    using System.Collections.Generic;

    using Rockstar.AssetManager.Infrastructure.Data;
    using Rockstar.AssetManager.Infrastructure.Enums;

    /// <summary>
    /// The AssetManager interface.
    /// </summary>
    public interface IAssetManager
    {
        #region Properties

        /// <summary>
        /// Gets the request handler.
        /// </summary>
        IRequestHandler RequestHandler { get; }

        /// <summary>
        /// Gets the host.
        /// </summary>
        string Host { get; }

        /// <summary>
        /// Gets the port.
        /// </summary>
        string Port { get; }

        /// <summary>
        /// Gets the workspace.
        /// </summary>
        string Workspace { get; }

        /// <summary>
        /// Gets the username.
        /// </summary>
        string Username { get; }

        /// <summary>
        /// Gets the password.
        /// </summary>
        string Password { get; }

        /// <summary>
        /// Gets the depot root.
        /// </summary>
        string DepotRoot { get; }

        /// <summary>
        /// Gets the root.
        /// </summary>
        string Root { get; }

        /// <summary>
        /// Gets a value indicating whether to use login functionality.
        /// </summary>
        bool UseLogin { get; }

        /// <summary>
        /// Gets the change lists.
        /// </summary>
        Dictionary<string, IChangeList> ChangeLists { get; }

        #endregion

        #region Connection

        /// <summary>
        /// Connect to the asset manager.
        /// </summary>
        /// <returns>
        /// Indication of connection success <see cref="bool"/>.
        /// </returns>
        bool Connect();

        /// <summary>
        /// Disconnect from the asset manager.
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Reconnect to the asset manager.
        /// </summary>
        /// <returns>
        /// Indication of connection success <see cref="bool"/>.
        /// </returns>
        bool Reconnect();

        /// <summary>
        /// Get asset manager connection status.
        /// </summary>
        /// <returns>
        /// Indication of connection status <see cref="bool"/>.
        /// </returns>
        bool IsConnected();

        #endregion

        #region View

        /// <summary>
        /// Refresh the asset manager view.
        /// </summary>
        void RefreshView();

        #endregion

        #region Client

        /// <summary>
        /// Load the client views (i.e. mappings between local and depot paths).
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool LoadClientViews();

        #endregion

        #region Change Lists

        /// <summary>
        /// Create a new change list.
        /// </summary>
        /// <returns>
        /// New change list (null if unsuccessful) <see cref="IChangeList"/>.
        /// </returns>
        IChangeList CreateChangeList();

        /// <summary>
        /// Create a new change list with specified description.
        /// </summary>
        /// <param name="description">
        /// Change list description.
        /// </param>
        /// <returns>
        /// New change list (null if unsuccessful) <see cref="IChangeList"/>.
        /// </returns>
        IChangeList CreateChangeList(string description);

        /// <summary>
        /// Initialize the asset manager by populating change lists and
        /// assets using data from asset manager.
        /// </summary>
        /// <returns>
        /// The loaded change lists <see cref="Dictionary"/>.
        /// </returns>
        Dictionary<string, IChangeList> LoadChangeLists();

        /// <summary>
        /// Get change list with specified change list number.
        /// </summary>
        /// <param name="changeListNumber">
        /// Change list number of change list to return.
        /// </param>
        /// <param name="allowAllUsers">
        /// flag to allow viewing all changelists by all users
        /// </param>
        /// <returns>
        /// Change list matching change list number (null if not found) <see cref="IChangeList"/>.
        /// </returns>
        IChangeList GetChangeList(string changeListNumber, bool allowAllUsers = false);

        /// <summary>
        /// Get change list with specified change list number.
        /// </summary>
        /// <param name="changeListNumber">
        /// Change list number of change list to return.
        /// </param>
        /// <param name="allowAllUsers">
        /// flag to allow viewing all changelists by all users
        /// </param>
        /// <returns>
        /// Change list matching change list number (null if not found) <see cref="IChangeList"/>.
        /// </returns>
        IChangeList GetChangeList(uint changeListNumber, bool allowAllUsers = false);

        /// <summary>
        /// Get Change list files from a changelist
        /// </summary>
        /// <param name="changeListNumber">
        ///  Change list number of change list.
        /// </param>
        /// <param name="exludeDeleted">
        /// parameter to only show added and changed
        /// </param>
        /// <returns>
        /// list of paths
        /// </returns>
        IList<string> GetChangeListFiles(string changeListNumber, bool exludeDeleted = false);

         /// <summary>
        /// Delete a change list.
        /// </summary>
        /// <param name="changeList">
        /// The change list to delete.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool DeleteChangeList(IChangeList changeList);

        /// <summary>
        /// Get the latest change list number.
        /// </summary>
        /// <returns>
        /// Latest change list number <see cref="uint"/>.
        /// </returns>
        uint GetLatestChangeListNumber();

        #endregion

        
            
            #region Assets

        /// <summary>
        /// Move an asset to a new change list.
        /// </summary>
        /// <param name="asset">
        /// The asset to move.
        /// </param>
        /// <param name="newChangeList">
        /// Change list to move asset to.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool MoveAsset(IAsset asset, IChangeList newChangeList);

        /// <summary>
        /// Get latest on a specified asset.
        /// </summary>
        /// <param name="asset">
        /// The asset.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool GetLatest(IAsset asset, bool force);

        /// <summary>
        /// Get latest on a specified asset.
        /// </summary>
        /// <param name="asset">
        /// The asset.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <param name="changeListNumber">
        /// Change list number to get latest from.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool GetLatest(IAsset asset, bool force, string changeListNumber);

        /// <summary>
        /// Get latest on a specified path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool GetLatest(string path, bool force);

        /// <summary>
        /// Get latest on a specified path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <param name="changeListNumber">
        /// Change list number to get latest from.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool GetLatest(string path, bool force, string changeListNumber);

        /// <summary>
        /// Get latest on a specified path filtered by specified file type.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <param name="fileType">
        /// The file type.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool GetLatestOnFileType(string path, bool force, string fileType);

        /// <summary>
        /// Get latest on a specified path and force get latest on any files
        /// that can't be clobbered first time round.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool GetLatestForceErrors(string path);

        /// <summary>
        /// Get the previous revision of specified file path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool GetPrevious(string path, bool force);

        /// <summary>
        /// Get the specified revision of the file path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="revision">
        /// The revision number.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool GetRevision(string path, uint revision, bool force = false);

        /// <summary>
        /// Resolve file path using specified resolve action.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="resolveAction">
        /// The resolve action.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Resolve(string path, ResolveAction resolveAction);

        /// <summary>
        /// Determine whether specified path exists as an asset in asset manager.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="includeDeleted">
        /// The include deleted action flag.
        /// </param>
        /// <returns>
        /// Indication of whether path exists as asset <see cref="bool"/>.
        /// </returns>
        bool ExistsAsAsset(string path, bool includeDeleted = false);

        /// <summary>
        /// Determine whether specified path exists in a local pending change list.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Indication of whether path exists in a local pending change list <see cref="bool"/>.
        /// </returns>
        bool ExistsInPendingChangeList(string path);

        /// <summary>
        /// Get checked out asset, if it exists.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// The asset, if found, otherwise null <see cref="IAsset"/>.
        /// </returns>
        IAsset GetCheckedOutAsset(string path);

        /// <summary>
        /// Get depot path from local path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Depot path <see cref="string"/>.
        /// </returns>
        string GetDepotPath(string path);

        /// <summary>
        /// Get local path from depot path.
        /// </summary>
        /// <param name="path">
        /// The depot path.
        /// </param>
        /// <returns>
        /// Local path <see cref="string"/>.
        /// </returns>
        string GetLocalPath(string path);

        /// <summary>
        /// Indicates whether file is checked out locally.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="safeCheck">
        /// Safe check flag - indicates whether to just check in-memory data
        /// or to query P4 to ensure we have accurate information about the asset.
        /// </param>
        /// <returns>
        /// Indication of whether file is checked out locally <see cref="bool"/>.
        /// </returns>
        bool IsCheckedOut(string path, bool safeCheck = false);

        /// <summary>
        /// Indicates whether file is marked for add.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Indication of whether file is marked for add <see cref="bool"/>.
        /// </returns>
        bool IsMarkedForAdd(string path);

        /// <summary>
        /// Indicates whether file is resolved.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Indication of whether file is resolved <see cref="bool"/>.
        /// </returns>
        bool IsResolved(string path);

        /// <summary>
        /// Indicates whether client has latest version of file.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Indication of whether client has latest version of file <see cref="bool"/>.
        /// </returns>
        bool HaveLatest(string path);

        /// <summary>
        /// Indicates whether file is marked as readonly.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// Indication of whether file is marked as readonly <see cref="bool"/>.
        /// </returns>
        bool IsReadOnly(string path);

        /// <summary>
        /// Get the file lock info for a specified file.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// File lock info <see cref="FileStatus"/>.
        /// </returns>
        FileStatus GetFileStatus(string path);

        /// <summary>
        /// Get the latest check-in time for a specified file.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// A DateTime representing the last time the file was checked in
        /// </returns>
        DateTime GetLatestCheckinTime(string path);

        DateTime GetTimeOfLastSubmitToDir(string path);

        /// <summary>
        /// Gets the revision history of a file
        /// </summary>
        /// <param name="path">the path to the file</param>
        /// <param name="maxHistoryLength">maximum history  length</param>
        /// <param name="latestChangelist">latest changelist to take care of</param>
        /// <param name="oldestRevision">the oldest revision to take into consideration</param>
        /// <param name="newestRevision">the newest revision to take into consideration</param>
        /// <returns></returns>
        IList<RevisionInfo> GetRevisionHistory(string path, int maxHistoryLength = 100, int latestChangelist = 0,
            int oldestRevision = 0, int newestRevision = int.MaxValue);
        
        /// <summary>
        /// Gets revision info of a file
        /// </summary>
        /// <param name="path">path to file</param>
        /// <param name="revision">revision number</param>
        /// <returns></returns>
        RevisionInfo GetRevisionInfo(string path, int revision = 0);

        /// <summary>
        /// Gets the number of revisions available.
        /// </summary>
        /// <param name="path">path to file</param>
        /// <returns></returns>
        int GetNumberOfRevisions(string path);

        /// <summary>
        /// Get available assets from server, deleted assets are ignored.
        /// </summary>
        /// <param name="path">A depot or local path</param>
        /// <returns></returns>
        IEnumerable<IAsset> GetAssets(string path);
        #endregion
    }
}
