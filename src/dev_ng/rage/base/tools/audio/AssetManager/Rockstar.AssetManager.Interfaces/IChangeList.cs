﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IChangeList.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the IChangeList type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Interfaces
{
    using System;
    using System.Collections.Generic;

    using Rockstar.AssetManager.Infrastructure.Enums;

    /// <summary>
    /// The ChangeList interface.
    /// </summary>
    public interface IChangeList
    {
        #region Properties

        /// <summary>
        /// Gets the asset manager.
        /// </summary>
        IAssetManager AssetManager { get; }

        /// <summary>
        /// Gets the change list number.
        /// </summary>
        uint Number { get; }

        /// <summary>
        /// Gets the change list number as a string.
        /// </summary>
        string NumberAsString { get; }

        /// <summary>
        /// Gets the change list created timestamp.
        /// </summary>
        DateTime Created { get; }

        /// <summary>
        /// Gets the change list username.
        /// </summary>
        string Username { get; }

        /// <summary>
        /// Gets or sets the change list workspace.
        /// </summary>
        string Workspace { get; set; }

        /// <summary>
        /// Gets or sets the change list description.
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Gets the change list state.
        /// </summary>
        ChangeListState State { get; }

        /// <summary>
        /// Gets the change list assets.
        /// </summary>
        IList<IAsset> Assets { get; }

        /// <summary>
        /// Gets the asset local paths.
        /// </summary>
        HashSet<string> AssetLocalPaths { get; }

        /// <summary>
        /// Gets the asset depot paths.
        /// </summary>
        HashSet<string> AssetDepotPaths { get; }

        #endregion

        #region Actions

        /// <summary>
        /// Refresh the change list assets using the versions in asset manager.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Refresh();

        /// <summary>
        /// Checks if the changelist is available. Could have been deleted from elsewhere or reverted.
        /// </summary>
        /// <returns>True if is available</returns>
        bool IsLocalPending();

        /// <summary>
        /// Rename the change list.
        /// </summary>
        /// <param name="description">
        /// The new name.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool UpdateDescription(string description);

        /// <summary>
        /// Submit the change list.
        /// </summary>
        /// <param name="description">
        /// The change list description.
        /// </param>
        /// <param name="submittedChangeListNumber">
        /// The submitted change list number.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Submit(string description, out string submittedChangeListNumber);

        /// <summary>
        /// Submit the change list.
        /// </summary>
        /// <param name="description">
        /// The change list description.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Submit(string description);

        /// <summary>
        /// Submit the change list using the current/default description.
        /// </summary>
        /// <param name="submittedChangeListNumber">
        /// The submitted change list number.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Submit(out string submittedChangeListNumber);

        /// <summary>
        /// Submit the change list using the current/default description and
        /// without providing the submitted change list number.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Submit();

        /// <summary>
        /// Revert all assets in change list.
        /// </summary>
        /// <param name="delete">
        /// Flag to indicate whether to delete change list after successful revert.
        /// </param>
        /// <param name="unchangedOnly">
        /// Revert unchanged files only.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Revert(bool delete, bool unchangedOnly = false);

        /// <summary>
        /// Revert asset.
        /// </summary>
        /// <param name="asset">
        /// The asset to revert.
        /// </param>
        /// <param name="removeFromChangeList">
        /// Flag to indicate whether to remove asset from change list on successful revert.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool RevertAsset(IAsset asset, bool removeFromChangeList);

        /// <summary>
        /// Revert any unchanged assets in change list.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool RevertUnchanged();

        /// <summary>
        /// Delete the change list and its pending asset changes/actions.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Delete();

        #endregion

        #region Assets

        /// <summary>
        /// Add an asset to the change list.
        /// </summary>
        /// <param name="asset">
        /// The asset to add.
        /// </param>
        void AddAsset(IAsset asset);

        /// <summary>
        /// Remove an asset from the change list.
        /// </summary>
        /// <param name="asset">
        /// The asset to remove.
        /// </param>
        /// <returns>
        /// Indication of success of operation (false if not found) <see cref="bool"/>.
        /// </returns>
        bool RemoveAsset(IAsset asset);

        /// <summary>
        /// Return string containing list of asset paths separated by spaces.
        /// </summary>
        /// <returns>
        /// Asset paths as string (space separated) <see cref="string"/>.
        /// </returns>
        string AssetsAsString();

        /// <summary>
        /// Get asset object in change list matching specified path.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// Matching asset (null if not found) <see cref="IAsset"/>.
        /// </returns>
        IAsset GetAsset(string path);

        /// <summary>
        /// Checkout asset into change list.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <param name="lockAsset">
        /// Indicates whether to lock the asset on checkout.
        /// </param>
        /// <returns>
        /// The checked-out asset (null on failure) <see cref="IAsset"/>.
        /// </returns>
        IAsset CheckoutAsset(string path, bool lockAsset);

        /// <summary>
        /// Mark an asset for add in change list.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <param name="type">
        /// The asset type.
        /// </param>
        /// <returns>
        /// Asset marked for add (null on failure) <see cref="IAsset"/>.
        /// </returns>
        IAsset MarkAssetForAdd(string path, string type);

        /// <summary>
        /// Mark an asset for add in change list.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// Asset marked for add (null on failure) <see cref="IAsset"/>.
        /// </returns>
        IAsset MarkAssetForAdd(string path);

        /// <summary>
        /// Mark an asset for delete in change list.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// Asset to be deleted <see cref="IAsset"/>.
        /// </returns>
        IAsset MarkAssetForDelete(string path);

        /// <summary>
        /// Mark an asset for delete in change list.
        /// </summary>
        /// <param name="asset">
        /// The asset.
        /// </param>
        /// <returns>
        /// Asset to be deleted <see cref="IAsset"/>.
        /// </returns>
        IAsset MarkAssetForDelete(IAsset asset);

        /// <summary>
        /// Lock an asset.
        /// </summary>
        /// <param name="asset">
        /// The asset.
        /// </param>
        /// <param name="revertOnFailure">
        /// The revert On failure flag.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool LockAsset(IAsset asset, bool revertOnFailure = true);

        /// <summary>
        /// Lock an asset.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool LockAsset(string path);

        /// <summary>
        /// Move asset to new file path.
        /// </summary>
        /// <param name="asset">
        /// The asset.
        /// </param>
        /// <param name="newFilePath">
        /// The new file path.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool MoveAsset(IAsset asset, string newFilePath);

        /// <summary>
        /// Returns value indicating whether change list has shelved files.
        /// </summary>
        /// <returns>
        /// Value indicating  whether change list has shelved files <see cref="bool"/>.
        /// </returns>
        bool HasShelvedAssets();

        #endregion
    }
}
