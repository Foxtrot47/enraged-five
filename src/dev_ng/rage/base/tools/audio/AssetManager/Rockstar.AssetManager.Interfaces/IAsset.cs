﻿// Copyright: Rockstar North 2014


using System.Collections.Generic;
using Rockstar.AssetManager.Infrastructure.Data;

namespace Rockstar.AssetManager.Interfaces
{
    /// <summary>
    /// The Asset interface.
    /// </summary>
    public interface IAsset
    {
        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        string Action { get; set; }

        /// <summary>
        /// Gets the asset manager.
        /// </summary>
        IAssetManager AssetManager { get; }

        /// <summary>
        /// Gets or sets the change list.
        /// </summary>
        IChangeList ChangeList { get; set; }

        /// <summary>
        /// Gets or sets the depot path.
        /// </summary>
        string DepotPath { get; set; }

        /// <summary>
        /// Gets or sets the have revision.
        /// </summary>
        int Revision { get; set; }

        /// <summary>
        /// Gets or sets the local path.
        /// </summary>
        string LocalPath { get; set; }

        #region Public methods

        /// <summary>
        /// Gets the latest version of the asset from the asset manager.
        /// </summary>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation  <see cref="bool"/>.
        /// </returns>
        bool GetLatest(bool force);

        /// <summary>
        /// Checkout the asset from the asset manager.
        /// </summary>
        /// <param name="lockAsset">
        /// Indicates whether to lock the asset on checkout.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Checkout(bool lockAsset);

        /// <summary>
        /// Lock the asset.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool Lock();

        // <summary>
        /// more info about the file status
        /// </summary>
        FileStatus FileStatus {get; }

        /// <summary>
        /// Revert the local version of the asset.
        /// </summary>
        /// <param name="removeFromChangeList">
        /// Flag to indicate whether to remove asset from change list on successful revert.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Revert(bool removeFromChangeList = true);

        /// <summary>
        /// Mark the asset for add in the change list.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool MarkForAdd();

        /// <summary>
        /// Mark the asset for add in the change list.
        /// </summary>
        /// <param name="type">
        /// The asset type.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool MarkForAdd(string type);

        /// <summary>
        /// Move the asset to another change list.
        /// </summary>
        /// <param name="newChangeList">
        /// The new change list.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Move(IChangeList newChangeList);

        /// <summary>
        /// Move the asset to another change list with specified change list number.
        /// </summary>
        /// <param name="newChangeListNumber">
        /// The number of the new change list.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool Move(uint newChangeListNumber);

        /// <summary>
        /// Move asset file location.
        /// </summary>
        /// <param name="newFilePath">
        /// The new file path.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        bool MoveAsset(string newFilePath);

        /// <summary>
        /// Mark the asset for delete in the change list.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool MarkForDelete();

        /// <summary>
        /// Get the previous revision of asset.
        /// </summary>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool GetPrevious(bool force);

        /// <summary>
        /// Indicates if local file is readonly.
        /// </summary>
        /// <returns>
        /// Readonly status <see cref="bool"/>.
        /// </returns>
        bool IsReadonly();

        /// <summary>
        /// Set readonly file attribute on local version of asset.
        /// </summary>
        /// <param name="flag">
        /// Flag indicating readonly value to set.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        bool SetReadonly(bool flag);

        /// <summary>
        /// Gets the history of the file
        /// </summary>
        /// <param name="maxHistoryLength">the max number of results</param>
        /// <param name="latestChangelist">the latest changelist considered</param>
        /// <param name="oldestRevision">the oldest revision</param>
        /// <param name="newestRevision">the newest revision</param>
        /// <returns></returns>
        IList<RevisionInfo> GetHistory(int maxHistoryLength = 100, int latestChangelist = 0, int oldestRevision = 0,
            int newestRevision = int.MaxValue);

        /// <summary>
        /// Get revision info
        /// </summary>
        /// <param name="revision">specify revision number for info</param>
        /// <returns></returns>
        RevisionInfo GetRevisionInfo(int revision = 0);

        #endregion
    }
}
