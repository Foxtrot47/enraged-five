﻿// -----------------------------------------------------------------------
// <copyright file="P4FetchFormRequest.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce
{
    using P4API;

    using Rockstar.AssetManager.Perforce.Interfaces;

    /// <summary>
    /// The P4 fetch form request.
    /// </summary>
    public class P4FetchFormRequest : P4BaseRequest, IP4FetchFormRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="P4FetchFormRequest"/> class.
        /// </summary>
        /// <param name="formName">
        /// The form name.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        public P4FetchFormRequest(string formName, params string[] args)
        {
            this.FormName = formName;
            this.Args = args;
        }

        /// <summary>
        /// Gets the form name.
        /// </summary>
        public string FormName { get; private set; }

        /// <summary>
        /// Gets the args.
        /// </summary>
        public string[] Args { get; private set; }

        /// <summary>
        /// Gets or sets the form.
        /// </summary>
        public P4Form Form { get; set; }
    }
}
