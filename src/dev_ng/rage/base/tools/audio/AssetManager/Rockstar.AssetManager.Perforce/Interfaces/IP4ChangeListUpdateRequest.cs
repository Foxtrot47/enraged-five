﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IP4ChangeListUpdateRequest.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce.Interfaces
{
    using P4API;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The IP4ChangeListUpdateRequest interface.
    /// </summary>
    public interface IP4ChangeListUpdateRequest : IRequest
    {
        /// <summary>
        /// Gets the change list number.
        /// </summary>
        string ChangeListNumber { get; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets or sets the record set.
        /// </summary>
        P4UnParsedRecordSet RecordSet { get; set; }
    }
}
