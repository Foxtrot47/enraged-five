﻿// -----------------------------------------------------------------------
// <copyright file="IP4RunCmdRequest.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce.Interfaces
{
    using P4API;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The P4RunCmdRequest interface.
    /// </summary>
    public interface IP4RunCmdRequest : IRequest
    {
        /// <summary>
        /// Gets the command.
        /// </summary>
        string Command { get; }

        /// <summary>
        /// Gets the args.
        /// </summary>
        string[] Args { get; }

        /// <summary>
        /// Gets or sets the record set.
        /// </summary>
        P4RecordSet RecordSet { get; set; }
    }
}
