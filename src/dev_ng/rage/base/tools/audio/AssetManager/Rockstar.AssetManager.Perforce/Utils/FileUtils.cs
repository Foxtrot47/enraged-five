﻿// -----------------------------------------------------------------------
// <copyright file="FileUtils.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce.Utils
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// General file utils.
    /// </summary>
    public class FileUtils
    {
        /// <summary>
        /// Safe check a list of file paths by replacing special symbols with their code representation.
        /// </summary>
        /// <param name="paths">
        /// The paths.
        /// </param>
        /// <returns>
        /// Safe-checked version of file paths <see cref="IList"/>.
        /// </returns>
        public static IList<string> SafeCheckPaths(IList<string> paths)
        {
            return paths.Select(SafeCheckPath).ToList();
        }

        /// <summary>
        /// Safe check a file path by replacing special symbols with their code representation.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Safe-checked version of file path <see cref="string"/>.
        /// </returns>
        public static string SafeCheckPath(string path)
        {
            var result = path;
            result = result.Replace("@", "%40");
            result = result.Replace("#", "%23");
            result = result.Replace("*", "%2A");
            return result;
        }
    }
}
