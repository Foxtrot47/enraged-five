﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="P4Request.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the P4Request type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce
{
    using System.Collections.Generic;

    using P4API;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Perforce.Interfaces;

    /// <summary>
    /// P4 request.
    /// </summary>
    public class P4RunCmdRequest : P4BaseRequest, IP4RunCmdRequest
    {
        /// <summary>
        /// The P4RecordSet.
        /// </summary>
        private P4RecordSet recordSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="P4RunCmdRequest"/> class.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        public P4RunCmdRequest(string command, List<string> args) :
            this(command, args.ToArray())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="P4RunCmdRequest"/> class.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        public P4RunCmdRequest(string command, params string[] args) :
            base(args)
        {
            this.Command = command;
            this.Args = args;
        }

        /// <summary>
        /// Gets the command.
        /// </summary>
        public string Command { get; private set; }

        /// <summary>
        /// Gets the args.
        /// </summary>
        public string[] Args { get; private set; }

        /// <summary>
        /// Gets or sets the record set.
        /// </summary>
        public P4RecordSet RecordSet
        {
            get
            {
                return this.recordSet;
            }

            set
            {
                this.recordSet = value;
                this.Messages.AddRange(this.recordSet.Messages);
                this.Status = RequestStatus.Completed;

                if (this.recordSet.HasWarnings())
                {
                    this.Warnings.AddRange(this.recordSet.Warnings);
                    this.Status = RequestStatus.CompletedWithWarnings;
                }

                if (this.recordSet.HasErrors())
                {
                    this.Errors.AddRange(this.recordSet.Errors);
                    this.Status = RequestStatus.Failed;
                }
            }
        }
    }
}
