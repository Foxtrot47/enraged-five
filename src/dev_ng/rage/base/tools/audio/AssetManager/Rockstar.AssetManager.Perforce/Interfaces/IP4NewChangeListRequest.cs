﻿// -----------------------------------------------------------------------
// <copyright file="IP4NewChangeListRequest.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce.Interfaces
{
    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The P4NewChangeListRequest interface.
    /// </summary>
    public interface IP4NewChangeListRequest : IRequest
    {
        /// <summary>
        /// Gets the description for the new change list.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets or sets the change list.
        /// </summary>
        IChangeList ChangeList { get; set; }
    }
}
