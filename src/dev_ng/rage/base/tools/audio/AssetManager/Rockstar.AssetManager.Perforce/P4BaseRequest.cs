﻿// -----------------------------------------------------------------------
// <copyright file="P4BaseRequest.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce
{
    using System.Collections.Generic;
    using System.Threading;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The P4 base request.
    /// </summary>
    public class P4BaseRequest : IRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="P4BaseRequest"/> class.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public P4BaseRequest(List<string> args) :
            this(args.ToArray())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="P4BaseRequest"/> class.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public P4BaseRequest(params string[] args)
        {
            this.Semaphore = new Semaphore(0, 1);
            this.Status = RequestStatus.New;

            this.Messages = new List<string>();
            this.Warnings = new List<string>();
            this.Errors = new List<string>();
        }

        /// <summary>
        /// Gets the semaphore.
        /// </summary>
        public Semaphore Semaphore { get; private set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public RequestStatus Status { get; set; }

        /// <summary>
        /// Gets the messages.
        /// </summary>
        public List<string> Messages { get; private set; }

        /// <summary>
        /// Gets the formatted messages.
        /// </summary>
        public string FormattedMessages
        {
            get { return string.Join("\n", this.Messages.ToArray()); }
        }

        /// <summary>
        /// Gets the warnings.
        /// </summary>
        public List<string> Warnings { get; private set; }

        /// <summary>
        /// Gets the formatted warnings.
        /// </summary>
        public string FormattedWarnings
        {
            get { return string.Join("\n", this.Warnings.ToArray()); }
        }

        /// <summary>
        /// Gets the errors.
        /// </summary>
        public List<string> Errors { get; private set; }

        /// <summary>
        /// Gets the formatted errors.
        /// </summary>
        public string FormattedErrors
        {
            get { return string.Join("\n", this.Errors.ToArray()); }
        }

        /// <summary>
        /// Indicates whether there were errors when the request was performed.
        /// </summary>
        /// <returns>
        /// Error indicator <see cref="bool"/>.
        /// </returns>
        public bool HasErrors()
        {
            return this.Errors.Count > 0;
        }

        /// <summary>
        /// Indicates whether there were warnings when the request was performed.
        /// </summary>
        /// <returns>
        /// Warning indicator <see cref="bool"/>.
        /// </returns>
        public bool HasWarnings()
        {
            return this.Warnings.Count > 0;
        }
    }
}
