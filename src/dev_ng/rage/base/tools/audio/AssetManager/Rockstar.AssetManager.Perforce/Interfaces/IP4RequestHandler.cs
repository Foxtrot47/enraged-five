﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IP4RequestHandler.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the IP4RequestHandler type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce.Interfaces
{
    using P4API;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The P4RequestHandler interface.
    /// </summary>
    public interface IP4RequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the P4 connection.
        /// </summary>
        P4Connection Connection { get; }
    }
}
