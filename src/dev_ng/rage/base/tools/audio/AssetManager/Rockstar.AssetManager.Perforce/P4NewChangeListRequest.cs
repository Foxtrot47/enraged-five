﻿// -----------------------------------------------------------------------
// <copyright file="P4NewChangeListRequest.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce
{
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Perforce.Interfaces;

    /// <summary>
    /// The P4 new change list request.
    /// </summary>
    public class P4NewChangeListRequest : P4BaseRequest, IP4NewChangeListRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="P4NewChangeListRequest"/> class.
        /// </summary>
        /// <param name="description">
        /// The new change list description.
        /// </param>
        public P4NewChangeListRequest(string description)
        {
            this.Description = description;
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets or sets the change list.
        /// </summary>
        public IChangeList ChangeList { get; set; }
    }
}
