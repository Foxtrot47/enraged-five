﻿// -----------------------------------------------------------------------
// <copyright file="RecordParser.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce.Utils
{
    using System;
    using P4API;

    /// <summary>
    /// P4 Record parser utils.
    /// </summary>
    public class RecordParserUtils
    {
        /// <summary>
        /// Return whether a field is set to "1" in a record.
        /// </summary>
        /// <param name="record">
        /// The record.
        /// </param>
        /// <param name="field">
        /// The field.
        /// </param>
        public static bool IsFlagSet(P4Record record, string field)
        {
            return record.Fields.ContainsKey(field) && (int.Parse(record.Fields[field]) == 1);
        }

        /// <summary>
        /// Return whether a field is present in a record.
        /// </summary>
        /// <param name="record">
        /// The record.
        /// </param>
        /// <param name="field">
        /// The field.
        /// </param>
        public static bool IsFlagPresent(P4Record record, string field)
        {
            return record.Fields.ContainsKey(field);
        }

        /// <summary>
        /// Return integer field.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int GetIntField(P4Record record, string field, int defaultValue = 0)
        {
            return record.Fields.ContainsKey(field) ? int.Parse(record.Fields[field]) : defaultValue;
        }

        /// <summary>
        /// Return unsigned integer field.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static uint GetUIntField(P4Record record, string field, uint defaultValue = 0)
        {
            return record.Fields.ContainsKey(field) ? uint.Parse(record.Fields[field]) : defaultValue;
        }

        /// <summary>
        /// Return changelist field.
        /// <remarks>handles the default changelist</remarks>
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static uint GetChangelistField(P4Record record, string field, uint defaultValue = 0)
        {
            if (record.Fields.ContainsKey(field))
            {
                uint result;

                if (record.Fields[field] != "default" &&
                    uint.TryParse(record.Fields[field], out result))
                {
                    return result;
                }
            }

            return defaultValue;
        }


        /// <summary>
        /// Return string field.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetStringField(P4Record record, string field, string defaultValue = "")
        {
            return record.Fields.ContainsKey(field) ? record.Fields[field] : defaultValue;
        }

        /// <summary>
        /// Return a DateTime object for a time field.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="record"></param>
        /// <param name="field"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static DateTime GetDateTimeField(P4Connection p4, P4Record record, string field, DateTime defaultValue)
        {
            var datetime = GetStringField(record, field);
            if (string.IsNullOrEmpty(datetime))
            {
                return defaultValue;
            }

            var utcTicks = int.Parse(datetime);
            return p4.ConvertDate(utcTicks);
        }
    }
}
