﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="P4AssetManager.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the P4AssetManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;

    using P4API;

    using Rockstar.AssetManager.Infrastructure.Data;
    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Perforce.Interfaces;
    using Rockstar.AssetManager.Perforce.Utils;

    /// <summary>
    /// The P4 asset manager.
    /// </summary>
    public class P4AssetManager : IAssetManager
    {
        /// <summary>
        /// The epoch timestamp.
        /// </summary>
        public static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// The default change list description.
        /// </summary>
        public const string DefaultChangeListDescription = "New Change List";

        /// <summary>
        /// The error message header.
        /// </summary>
        private const string ErrorMessageHeader = "error running perforce command!";

        /// <summary>
        /// The 'no such file' text.
        /// </summary>
        private const string NoSuchFileText = "no such file(s)";

        /// <summary>
        /// The 'cant clobber file' text.
        /// </summary>
        private const string CantClobberText = "can't clobber writable file";

        /// <summary>
        /// The wildcards.
        /// </summary>
        private readonly IList<string> wildcards = new List<string> { "...", "?" };

        /// <summary>
        /// The client views.
        /// </summary>
        private IList<ClientView> clientViews;

        /// <summary>
        /// The root.
        /// </summary>
        private string root;

        /// <summary>
        /// The fixed root.
        /// </summary>
        private string fixedRoot;

        /// <summary>
        /// Initializes a new instance of the <see cref="P4AssetManager"/> class.
        /// </summary>
        /// <param name="host">
        /// The host.
        /// </param>
        /// <param name="port">
        /// The port.
        /// </param>
        /// <param name="workspace">
        /// The workspace.
        /// </param>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="depotRoot">
        /// The depot Root.
        /// </param>
        /// <param name="useLogin">
        /// Indicates whether to use login functionality.
        /// </param>
        /// <exception cref="ArgumentException">
        /// Thrown if useLogin is set but no password is specified.
        /// </exception>
        public P4AssetManager(
            string host, string port, string workspace, string username, string password, string depotRoot, bool useLogin, ILogger logger, string description)
        {
            this.Host = host;
            this.Port = port;
            this.Workspace = workspace;
            this.Username = username;
            this.Password = password;
            this.DepotRoot = depotRoot;
            this.UseLogin = useLogin;

            if (useLogin && string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Password required if useLogin is enabled");
            }

            this.ChangeLists = new Dictionary<string, IChangeList>();

            this.RequestHandler = new P4RequestHandler(
                this, this.Host, this.Port, this.Workspace, this.Username, this.Password, this.UseLogin, logger, description);

            if (!this.Connect())
            {
                throw new Exception("Failed to connect to Perforce");
            }

            this.Init();
        }

        #region Properties

        /// <summary>
        /// Gets the request handler.
        /// </summary>
        public IRequestHandler RequestHandler { get; private set; }

        /// <summary>
        /// Gets the host.
        /// </summary>
        public string Host { get; private set; }

        /// <summary>
        /// Gets the port.
        /// </summary>
        public string Port { get; private set; }

        /// <summary>
        /// Gets the workspace.
        /// </summary>
        public string Workspace { get; private set; }

        /// <summary>
        /// Gets the username.
        /// </summary>
        public string Username { get; private set; }

        /// <summary>
        /// Gets the password.
        /// </summary>
        public string Password { get; private set; }

        /// <summary>
        /// Gets the depot root.
        /// </summary>
        public string DepotRoot { get; private set; }

        /// <summary>
        /// Gets the root.
        /// </summary>
        public string Root { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to use login functionality.
        /// </summary>
        public bool UseLogin { get; private set; }

        /// <summary>
        /// Gets the change lists.
        /// </summary>
        public Dictionary<string, IChangeList> ChangeLists { get; private set; }

        #endregion

        #region Connection

        /// <summary>
        /// Connect to the asset manager.
        /// </summary>
        /// <returns>
        /// Indication of connection success <see cref="bool"/>.
        /// </returns>
        public bool Connect()
        {
            return this.RequestHandler.Connect();
        }

        /// <summary>
        /// Disconnect from the asset manager.
        /// </summary>
        public void Disconnect()
        {
            this.RequestHandler.Disconnect();
        }

        /// <summary>
        /// Reconnect to the asset manager.
        /// </summary>
        /// <returns>
        /// Indication of connection success <see cref="bool"/>.
        /// </returns>
        public bool Reconnect()
        {
            return this.RequestHandler.Reconnect();
        }

        /// <summary>
        /// Get asset manager connection status.
        /// </summary>
        /// <returns>
        /// Indication of connection status <see cref="bool"/>.
        /// </returns>
        public bool IsConnected()
        {
            return this.RequestHandler.IsConnected();
        }

        #endregion

        #region View

        /// <summary>
        /// Refresh the asset manager view.
        /// </summary>
        public void RefreshView()
        {
            this.LoadChangeLists();

            // TODO - raise action event
        }

        #endregion

        #region Client

        /// <summary>
        /// Load the client views (i.e. mappings between local and depot paths).
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool LoadClientViews()
        {
            this.clientViews = new List<ClientView>();

            var request = new P4FetchFormRequest("client");
            this.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed || null == request.Form)
            {
                throw new Exception("Failed to get client view info: " + request.FormattedErrors);
            }

            var fields = request.Form.Fields;
            var arrayFields = request.Form.ArrayFields;

            // Check that we have the info we need
            if (!fields.ContainsKey("Client") || !fields.ContainsKey("Root") || !arrayFields.ContainsKey("View"))
            {
                throw new Exception("Failed to get client view info");
            }

            var clientName = fields["Client"];
            var localRoot = fields["Root"].Replace('/', '\\').TrimEnd('\\');

            // Store the client view mappings
            foreach (var view in arrayFields["View"])
            {
                var tmpView = view;

                var isActive = true;
                if (view.StartsWith("-"))
                {
                    isActive = false;
                    tmpView = view.Substring(1);
                }

                // Replace * with ... to have consistent wildcards
                tmpView = Regex.Replace(tmpView, "\\*", "...");

                var quoteCount = 0;
                var splitIndex = 0;
                char? prevChar = null;
                var split = new List<string> { string.Empty };
                foreach (var c in tmpView.ToCharArray())
                {
                    switch (c)
                    {
                        case '\"':
                            quoteCount++;
                            break;
                        case ' ':
                            if (quoteCount % 2 == 0)
                            {
                                if (null != prevChar && prevChar != ' ')
                                {
                                    split.Add(string.Empty);
                                    splitIndex++;
                                }
                            }
                            else
                            {
                                split[splitIndex] += c;
                            }

                            break;
                        default:
                            split[splitIndex] += c;
                            break;
                    }

                    prevChar = c;
                }

                // Check view info is parsable
                if (split.Count != 2)
                {
                    throw new Exception("Unable to parse client view info: " + view);
                }

                var depotPath = split[0];
                var clientPath = split[1];

                // Remove trailing ...
                if (depotPath.EndsWith("/..."))
                {
                    depotPath = depotPath.Substring(0, depotPath.Length - 3);
                }

                if (clientPath.EndsWith("/..."))
                {
                    clientPath = clientPath.Substring(0, clientPath.Length - 3);
                }

                // Check for paths containing wildcards (can't support these easily)
                if (this.wildcards.Any(wildcard => depotPath.Contains(wildcard) || clientPath.Contains(wildcard)))
                {
                    continue;
                }

                // Remove '//' at start of client path and use \ instead of /
                var localPath = clientPath.TrimStart('/').Replace('/', '\\');

                // Replace client name with local root
                localPath = Regex.Replace(localPath, clientName, localRoot, RegexOptions.IgnoreCase);

                // Create new client view mapping value
                var clientView = new ClientView
                    {
                        DepotPath = depotPath,
                        ClientPath = clientPath,
                        LocalPath = localPath,
                        IsActive = isActive
                    };

                this.clientViews.Add(clientView);
            }

            return true;
        }

        #endregion

        #region Change Lists

        /// <summary>
        /// Create a new change list.
        /// </summary>
        /// <returns>
        /// New change list (null if unsuccessful) <see cref="IChangeList"/>.
        /// </returns>
        public IChangeList CreateChangeList()
        {
            return this.CreateChangeList(DefaultChangeListDescription);
        }

        /// <summary>
        /// Create a new change list with specified description.
        /// </summary>
        /// <param name="description">
        /// Change list description.
        /// </param>
        /// <returns>
        /// New change list (null if unsuccessful) <see cref="IChangeList"/>.
        /// </returns>
        public IChangeList CreateChangeList(string description)
        {
            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException("description");
            }

            var request = new P4NewChangeListRequest(description);
            this.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed)
            {
                throw new Exception("Failed to create new changelist: " + request.FormattedErrors);
            }

            // Check that change list number is unique locally
            var changeList = request.ChangeList;
            if (this.ChangeLists.ContainsKey(changeList.NumberAsString))
            {
                throw new Exception("Change list with change list number already in list: " + changeList.NumberAsString);
            }

            // Add new change list to list of changes lists
            this.ChangeLists[changeList.NumberAsString] = changeList;

            return changeList;
        }

        /// <summary>
        /// Initialize the asset manager by populating change lists and
        /// assets using data from asset manager.
        /// </summary>
        /// <returns>
        /// The loaded change lists <see cref="Dictionary"/>.
        /// </returns>
        public Dictionary<string, IChangeList> LoadChangeLists()
        {
            // Get files opened by current user under user's workspace
            var args = new[]
                {
                    "-C" + this.Workspace
                };
            var request = new P4RunCmdRequest("opened", args);
            this.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed || null == request.RecordSet)
            {
                throw new Exception("Failed to get opened file info: " + request.FormattedErrors);
            }

            var tmpChangeListNums = new HashSet<string>();
            var tmpAssetDepotPaths = new Dictionary<string, IList<string>>(); // change list number -> asset depot paths 

            // Add default change list manually
            IChangeList defaultChangeList;
            if (!this.ChangeLists.TryGetValue("default", out defaultChangeList))
            {
                defaultChangeList = new P4ChangeList(this, string.Empty);
                this.ChangeLists.Add("default", defaultChangeList);
            }

            tmpChangeListNums.Add("default");

            // Initialize asset objects
            foreach (P4Record record in request.RecordSet)
            {
                // Get change list and asset if we already have objects locally
                var changeList = this.GetChangeList(record.Fields["change"]);
                IAsset asset = null;
                if (null != changeList)
                {
                    asset = changeList.GetAsset(record.Fields["depotFile"]);
                }

                // Otherwise, create new asset object and get reference to its change list
                if (null == asset)
                {
                    asset = this.InitAsset(record);
                    if (null == changeList)
                    {
                        changeList = asset.ChangeList;
                    }
                }

                // Add change list to dictionary if not already added
                this.ChangeLists[changeList.NumberAsString] = changeList;

                tmpChangeListNums.Add(asset.ChangeList.NumberAsString);

                if (!tmpAssetDepotPaths.ContainsKey(changeList.NumberAsString))
                {
                    tmpAssetDepotPaths[changeList.NumberAsString] = new List<string>();
                }

                tmpAssetDepotPaths[changeList.NumberAsString].Add(asset.DepotPath);
            }

            // Delete any local change lists that are no longer checked out
            var removeChangeLists = (
                from changeList in this.ChangeLists
                where !tmpChangeListNums.Contains(changeList.Key)
                select changeList).Distinct().ToList();

            foreach (var changeList in removeChangeLists)
            {
                this.ChangeLists.Remove(changeList.Key);
            }

            // Delete any local assets that are no longer checked out
            var removeAssets = new List<IAsset>();

            foreach (var changeList in this.ChangeLists.Values)
            {
                foreach (var asset in changeList.Assets)
                {
                    if (!tmpAssetDepotPaths.ContainsKey(changeList.NumberAsString)
                        || !tmpAssetDepotPaths[changeList.NumberAsString].Contains(asset.DepotPath))
                    {
                        removeAssets.Add(asset);
                    }
                }
            }

            foreach (var asset in removeAssets)
            {
                asset.ChangeList.RemoveAsset(asset);
            }

            // Return updated list of change lists
            return this.ChangeLists;
        }

        /// <summary>
        /// Get change list with specified change list number.
        /// </summary>
        /// <param name="changeListNumber">
        /// Change list number of change list to return.
        /// </param>
        /// <param name="allowAllUsers">
        /// flag to allow viewing all changelists by all users
        /// </param>
        /// <returns>
        /// Change list matching change list number (null if not found) <see cref="IChangeList"/>.
        /// </returns>
        public IChangeList GetChangeList(string changeListNumber, bool allowAllUsers = false)
        {
            if (string.IsNullOrEmpty(changeListNumber))
            {
                throw new ArgumentNullException("changeListNumber");
            }

            // First check if we already have the change list in our list
            var changeList = this.ChangeLists.FirstOrDefault(cl => cl.Key == changeListNumber).Value;

            if (null != changeList)
            {
                return changeList;
            }

            // If not, check to see if it exist on P4 server but is not yet known about locally
            Console.WriteLine("Checking P4 for change list " + changeListNumber);
            var args = new List<string> { "-O", changeListNumber };
            var request = new P4RunCmdRequest("describe", args);
            this.RequestHandler.ActionRequest(request);

            if (request.RecordSet.Records.Any())
            {
                var record = request.RecordSet[0];

                // Check that change list is in scope of current user and workspace if not allowAllUsers
                if (allowAllUsers || (record.Fields.ContainsKey("user")
                    && record.Fields["user"].ToUpper() == this.Username.ToUpper()
                    && record.Fields.ContainsKey("client")
                    && record.Fields["client"].ToUpper() == this.Workspace.ToUpper()))
                {
                    changeList = new P4ChangeList(this, record);

                    // Add new change list to list of changes lists
                    this.ChangeLists[changeList.NumberAsString] = changeList;
                }
            }

            return changeList;
        }

        /// <summary>
        /// Get change list with specified change list number.
        /// </summary>
        /// <param name="changeListNumber">
        /// Change list number of change list to return
        /// </param>
        /// <param name="allowAllUsers">
        /// flag to allow viewing all changelists by all users
        /// </param>
        /// <returns></returns>
        public IChangeList GetChangeList(uint changeListNumber, bool allowAllUsers = false)
        {
            return this.GetChangeList(changeListNumber.ToString(CultureInfo.InvariantCulture), allowAllUsers);
        }

        /// <summary>
        /// Get list of files in a change list (pending or submitted).
        /// </summary>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        /// <returns>
        /// The list of files in a change list <see cref="IList"/>.
        /// </returns>
        public IList<string> GetChangeListFiles(string changeListNumber, bool exludeDeleted = false)
        {
            var args = new List<string> { "-s", "-O", changeListNumber };
            var request = new P4RunCmdRequest("describe", args);
            this.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed
                || null == request.RecordSet)
            {
                throw new Exception("Failed to get change list files: " + request.FormattedErrors);
            }

            var files = new List<string>();
            foreach (var record in request.RecordSet.Records)
            {
                if (record.ArrayFields.ContainsKey("depotFile"))
                {
                    if (exludeDeleted)
                    {
                        for (int i = 0; i < record.ArrayFields["action"].Length; i++)
                        {
                            string fileIndex = record.ArrayFields["action"][i];

                            if (!fileIndex.Contains("delete"))
                            {
                                files.Add(record.ArrayFields["depotFile"][i]);
                            }
                        }

                    }
                    else
                    {
                        files.AddRange(record.ArrayFields["depotFile"]);
                    }
                }
            }

            return files;
        }

        /// <summary>
        /// Delete a change list.
        /// </summary>
        /// <param name="changeList">
        /// The change list to delete.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool DeleteChangeList(IChangeList changeList)
        {
            if (null == changeList)
            {
                throw new ArgumentNullException("changeList");
            }

            var args = new List<string>
                {
                    "-d",
                    changeList.NumberAsString
                };
            var request = new P4RunCmdRequest("change", args);
            this.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed
                || null == request.RecordSet)
            {
                throw new Exception("Failed to delete change list: " + request.FormattedErrors);
            }

            if (request.FormattedWarnings.ToLower().Contains("open file(s) associated with it and can't be deleted"))
            {
                throw new Exception("Failed to delete change list: " + request.FormattedWarnings);
            }

            changeList.Assets.Clear();
            changeList.AssetLocalPaths.Clear();
            changeList.AssetDepotPaths.Clear();
            this.ChangeLists.Remove(changeList.NumberAsString);

            return true;
        }

        /// <summary>
        /// Get the latest change list number.
        /// </summary>
        /// <returns>
        /// Latest change list number <see cref="uint"/>.
        /// </returns>
        public uint GetLatestChangeListNumber()
        {
            var args = new List<string>
                {
                    "-ssubmitted",
                    "-m1"
                };
            var request = new P4RunCmdRequest("changes", args);
            this.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed
                || null == request.RecordSet)
            {
                throw new Exception("Failed to get latest change list number: " + request.FormattedErrors);
            }

            // Otherwise safe to assume request has completed
            var changeStr = request.RecordSet.Records[0].Fields["change"];
            uint change;
            if (!uint.TryParse(changeStr, out change))
            {
                throw new Exception("Invalid change list number: " + changeStr);
            }

            return change;
        }

        #endregion

        #region Assets

        /// <summary>
        /// Move an asset to a new change list.
        /// </summary>
        /// <param name="asset">
        /// The asset to move.
        /// </param>
        /// <param name="newChangeList">
        /// Change list to move asset to.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool MoveAsset(IAsset asset, IChangeList newChangeList)
        {
            if (null == asset)
            {
                throw new ArgumentNullException("asset");
            }

            if (null == newChangeList)
            {
                throw new ArgumentNullException("newChangeList");
            }

            var args = new List<string>
                {
                    "-c" + newChangeList.NumberAsString,
                    asset.LocalPath
                };
            var request = new P4RunCmdRequest("reopen", args);
            this.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed)
            {
                throw new Exception("Failed to move asset: " + request.FormattedErrors);
            }

            if (request.FormattedWarnings.ToLower().Contains("file(s) not opened on this client"))
            {
                throw new Exception("Failed to move asset: " + request.FormattedWarnings);
            }

            // Otherwise safe to assume request has completed successfully
            // Remove asset from current change list and add to new change list
            if (!asset.ChangeList.RemoveAsset(asset))
            {
                throw new Exception("Failed to remove asset from its current change list");
            }

            newChangeList.AddAsset(asset);

            return true;
        }

        /// <summary>
        /// Get latest on a specified asset.
        /// </summary>
        /// <param name="asset">
        /// The asset.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool GetLatest(IAsset asset, bool force)
        {
            return this.GetLatest(asset, force, null);
        }

        /// <summary>
        /// Get latest on a specified asset.
        /// </summary>
        /// <param name="asset">
        /// The asset.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <param name="changeListNumber">
        /// Change list number to get latest from.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool GetLatest(IAsset asset, bool force, string changeListNumber)
        {
            var safePath = FileUtils.SafeCheckPath(asset.LocalPath);
            return this.GetLatest(safePath, force, changeListNumber);
        }

        /// <summary>
        /// Get latest on a specified path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool GetLatest(string path, bool force)
        {
            return this.GetLatest(path, force, null);
        }

        /// <summary>
        ///  Get latest on a specified path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <param name="changeListNumber">
        /// Change list number to get latest from.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool GetLatest(string path, bool force, string changeListNumber)
        {
            var request = this.GetLatestHelper(path, force, changeListNumber, null);

            if (false)
            {
                throw new Exception("Failed to get latest: " + request.FormattedErrors);
            }

            return true;
        }

        /// <summary>
        /// Get latest on a specified path filtered by specified file type.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <param name="fileType">
        /// The file type.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool GetLatestOnFileType(string path, bool force, string fileType)
        {
            var request = this.GetLatestHelper(path, force, null, fileType);

            if (request.HasErrors())
            {
                throw new Exception("Failed to get latest: " + request.FormattedErrors);
            }

            return true;
        }

        /// <summary>
        /// Get latest on a specified path and force get latest on any files
        /// that can't be clobbered first time round.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool GetLatestForceErrors(string path)
        {
            var request = this.GetLatestHelper(path, false, null, null);

            if (request.HasErrors())
            {
                if (!request.Errors.Any())
                {
                    throw new Exception("Get latest failed but no errors returned: " + path);
                }

                // Get paths of files unable to sync from error message and force sync individually
                foreach (var error in request.Errors)
                {
                    var errorLower = error.ToLower();
                    foreach (var line in errorLower.Split('\n'))
                    {
                        if (string.IsNullOrEmpty(line) || line.ToLower() == ErrorMessageHeader)
                        {
                            continue;
                        }

                        if (!line.ToLower().Contains(CantClobberText))
                        {
                            throw new Exception("Error when forcing sync: " + request.FormattedErrors);
                        }

                        var file = Regex.Replace(line, CantClobberText, string.Empty).Trim();

                        // Check we have a valid file path and not another error
                        if (!File.Exists(file))
                        {
                            throw new Exception("Failed to get file path from P4 request response error: " + line);
                        }

                        // Force grab file
                        if (!this.GetLatest(file, true))
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Get the previous revision of specified file path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool GetPrevious(string path, bool force)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var safePath = FileUtils.SafeCheckPath(path);

            if (this.IsCheckedOut(safePath))
            {
                throw new Exception("File is already checked out; can't get previous: " + path);
            }

            var request = this.RunCmdFStat(path);

            // Determine what file revision we currently have
            var currRevStr = request.RecordSet[0].Fields["haveRev"];
            uint currRev;
            if (!uint.TryParse(currRevStr, out currRev))
            {
                throw new Exception("Invalid revision number from P4: " + currRevStr);
            }

            if (currRev <= 1)
            {
                throw new Exception("There is no previous revision of file: " + path);
            }

            var prevRev = currRev - 1;

            // Get previous revision of file
            var args = new List<string>();

            // Add force flag if required
            if (force)
            {
                args.Add("-f");
            }

            // Add path and revision to get
            args.Add(safePath + "#" + prevRev);

            request = new P4RunCmdRequest("sync", args);
            this.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed)
            {
                throw new Exception("Failed to get revision " + prevRev + ": " + path);
            }

            return true;
        }

        /// <summary>
        /// Get the specified revision of the file path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="revision">
        /// The revision number.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool GetRevision(string path, uint revision, bool force = false)
        {
            var safePath = FileUtils.SafeCheckPath(path);

            // Get previous revision of file
            var args = new List<string>();

            // Add force flag if required
            if (force)
            {
                args.Add("-f");
            }

            // Add path and revision to get
            args.Add(safePath + "#" + revision);

            var request = new P4RunCmdRequest("sync", args);
            this.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed)
            {
                throw new Exception("Failed to get revision " + revision + ": " + path);
            }

            return true;
        }

        /// <summary>
        /// Resolve file path using specified resolve action.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="resolveAction">
        /// The resolve action.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Resolve(string path, ResolveAction resolveAction)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var safePath = FileUtils.SafeCheckPath(path);
            var depotPath = this.GetDepotPath(safePath);

            // Determine resolve action flag
            string resolveActionFlag;
            switch (resolveAction)
            {
                case ResolveAction.AcceptYours:
                    {
                        resolveActionFlag = "-ay";
                        break;
                    }

                case ResolveAction.AcceptTheirs:
                    {
                        resolveActionFlag = "-at";
                        break;
                    }

                case ResolveAction.AcceptMerged:
                    {
                        resolveActionFlag = "-am";
                        break;
                    }

                case ResolveAction.AcceptAuto:
                    {
                        resolveActionFlag = "-a";
                        break;
                    }

                default:
                    {
                        throw new Exception("Resolve action not supported: " + resolveAction);
                    }
            }

            // Perform resolve request
            var args = new List<string>
                {
                    depotPath,
                    resolveActionFlag
                };
            var request = new P4RunCmdRequest("resolve", args);
            this.RequestHandler.ActionRequest(request);
            if (request.HasErrors())
            {
                throw new Exception("Failed to resolve: " + path);
            }

            this.RefreshView();
            return true;
        }

        /// <summary>
        /// Determine whether specified path exists as an asset in asset manager.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="includeDeleted">
        /// The include deleted action flag.
        /// </param>
        /// <returns>
        /// Indication of whether path exists as asset <see cref="bool"/>.
        /// </returns>
        public bool ExistsAsAsset(string path, bool includeDeleted = false)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var request = this.RunCmdFStat(path);

            if (!request.RecordSet.Cast<P4Record>().Any())
            {
                return false;
            }

            // Check for valid asset (i.e. hasn't been deleted)
            var record = request.RecordSet[0];
            if (record.Fields.ContainsKey("headAction"))
            {
                var headAction = record.Fields["headAction"];
                if (headAction.Equals("delete"))
                {
                    return includeDeleted;
                }

                if (headAction.Equals("move/add") || headAction.Equals("add") ||
                    headAction.Equals("edit") || headAction.Equals("branch") ||
                    headAction.Equals("integrate"))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determine whether specified path exists in a local pending change list.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Indication of whether path exists in a local pending change list <see cref="bool"/>.
        /// </returns>
        public bool ExistsInPendingChangeList(string path)
        {
            return this.ChangeLists.Any(changeList => null != changeList.Value.GetAsset(path));
        }

        /// <summary>
        /// Get checked out asset, if it exists.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// The asset, if found, otherwise null <see cref="IAsset"/>.
        /// </returns>
        public IAsset GetCheckedOutAsset(string path)
        {
            return this.ChangeLists.Select(
                changeList => changeList.Value.GetAsset(path)).FirstOrDefault(asset => null != asset);
        }

        /// <summary>
        /// Get depot path from local path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Depot path <see cref="string"/>.
        /// </returns>
        public string GetDepotPath(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var safePath = FileUtils.SafeCheckPath(path);

            if (safePath.EndsWith("/") || safePath.EndsWith("\\"))
            {
                safePath += "...";
            }

            if (!safePath.Contains(":"))
            {
                safePath = safePath.Replace("\\", "/");

                // Add depot root if not already a valid depot path (i.e. starting with //)
                if (!path.StartsWith("//"))
                {
                    // Partial path provided without root
                    if (safePath.StartsWith("/"))
                    {
                        safePath = safePath.Substring(1);
                    }

                    safePath = Path.Combine(this.DepotRoot, safePath);
                }

                safePath = safePath.Replace("\\", "/");

                // Already got a depot path so safe to return
                return safePath;
            }

            var safePathUpper = safePath.ToUpper();

            // Attempt to convert path to depot path using client view mappings
            var depotPath = string.Empty;
            var currMatchChars = 0;
            foreach (var view in this.clientViews.Where(v => v.IsActive))
            {
                // Check for local path match
                var viewLocalPath = view.LocalPath.ToUpper();
                if (safePathUpper.StartsWith(viewLocalPath) &&
                    viewLocalPath.Length > currMatchChars)
                {
                    depotPath = view.DepotPath + safePathUpper.Substring(viewLocalPath.Length);
                    currMatchChars = viewLocalPath.Length;
                    continue;
                }

                // Check for client path match
                var viewClientPath = view.ClientPath.ToUpper();
                if (safePathUpper.StartsWith(viewClientPath) &&
                    viewClientPath.Length > currMatchChars)
                {
                    depotPath = view.DepotPath + safePathUpper.Substring(viewClientPath.Length);
                    currMatchChars = viewClientPath.Length;
                }
            }

            if (string.IsNullOrEmpty(depotPath))
            {
               // throw new Exception("Failed to determine depot path: " + path);
            }

            depotPath = depotPath.Replace('\\', '/').ToUpper();
            return depotPath;
        }

        /// <summary>
        /// Get local path from depot path.
        /// </summary>
        /// <param name="path">
        /// The depot path.
        /// </param>
        /// <returns>
        /// Local path <see cref="string"/>.
        /// </returns>
        public string GetLocalPath(string path)
        {
            var safePath = FileUtils.SafeCheckPath(path);

            if (string.IsNullOrEmpty(safePath))
            {
                return this.fixedRoot;
            }

            // Convert to depot path first to handle any path conversions
            var depotPath = this.GetDepotPath(safePath);
            var safePathUpper = depotPath.ToUpper();

            // Attempt to convert path to local path using client view mappings
            var localPath = string.Empty;
            var currMatchChars = 0;
            foreach (var view in this.clientViews.Where(v => v.IsActive))
            {
                // Check for local path match
                var viewDepotPath = view.DepotPath.ToUpper();
                if (safePathUpper.StartsWith(viewDepotPath) &&
                    viewDepotPath.Length > currMatchChars)
                {
                    localPath = view.LocalPath + safePathUpper.Substring(viewDepotPath.Length);
                    currMatchChars = viewDepotPath.Length;
                }
            }

            if (string.IsNullOrEmpty(localPath))
            {
                throw new Exception("Failed to determine local path: " + path);
            }

            localPath = localPath.Trim('.');
            localPath = localPath.Replace('/', '\\').ToUpper();
            return localPath;
        }

        /// <summary>
        /// Indicates whether file is checked out locally.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="safeCheck">
        /// Safe check flag - indicates whether to just check in-memory data
        /// or to query P4 to ensure we have accurate information about the asset.
        /// </param>
        /// <returns>
        /// Indication of whether file is checked out locally <see cref="bool"/>.
        /// </returns>
        public bool IsCheckedOut(string path, bool safeCheck = false)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            // Do safe check using P4 server info
            if (safeCheck)
            {
                var status = this.GetFileStatus(path);
                return status.IsLockedByMe && status.Owner == this.Workspace;
            }

            // Otherwise, just check local in-memory data if we are happy
            // that file hasn't been modfied outside of RAVE
            var safePathUpper = FileUtils.SafeCheckPath(path).ToUpper();

            // Search all change lists for matching local path
            var found =
                this.ChangeLists.SelectMany(changeList => changeList.Value.AssetLocalPaths)
                    .Any(assetLocalPath => assetLocalPath.ToUpper().Equals(safePathUpper));

            // Return true if found matching local path
            if (found)
            {
                return true;
            }

            // Otherwise, search all change lists for matching depot path
            found =
                this.ChangeLists.SelectMany(changeList => changeList.Value.AssetDepotPaths)
                    .Any(depotPath => depotPath.ToUpper().Equals(safePathUpper));

            return found;
        }

        /// <summary>
        /// Indicates whether file is marked for add.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Indication of whether file is marked for add <see cref="bool"/>.
        /// </returns>
        public bool IsMarkedForAdd(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var request = this.RunCmdFStat(path);

            if (!request.RecordSet.Records.Any())
            {
                return false;
            }

            var fields = request.RecordSet[0].Fields;
            return fields.ContainsKey("action") && fields["action"].Equals("add");
        }

        /// <summary>
        /// Indicates whether file is resolved.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Indication of whether file is resolved <see cref="bool"/>.
        /// </returns>
        public bool IsResolved(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var request = this.RunCmdFStat(path);

            var fields = request.RecordSet[0].Fields;
            return !fields.ContainsKey("unresolved");
        }

        /// <summary>
        /// Indicates whether client has latest version of file.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Indication of whether client has latest version of file <see cref="bool"/>.
        /// </returns>
        public bool HaveLatest(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var depotPath = this.GetDepotPath(path);

            var args = new[] { "-n", depotPath };
            var request = new P4RunCmdRequest("sync", args);
            this.RequestHandler.ActionRequest(request);
            if (request.HasErrors())
            {
                throw new Exception("Failed to check status of file: " + request.FormattedErrors);
            }

            return request.Warnings.Any(msg => msg.ToLower().Contains("up-to-date"));
        }

        /// <summary>
        /// Indicates whether file is marked as readonly.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// Indication of whether file is marked as readonly <see cref="bool"/>.
        /// </returns>
        public bool IsReadOnly(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            return new FileInfo(path).IsReadOnly;
        }

        /// <summary>
        /// Get the file status for a specified file.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// File lock info <see cref="FileStatus"/>.
        /// </returns>
        public FileStatus GetFileStatus(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var request = this.RunCmdFStat(path);

            // Check that we managed to get file info from P4
            if (!request.RecordSet.Cast<P4Record>().Any())
            {
                throw new Exception("Failed to get file status: " + path);
            }

            var fields = request.RecordSet[0].Fields;
            var arrayFields = request.RecordSet[0].ArrayFields;

            var isOpen = false;
            var isLocked = false;
            var isLockedByMe = fields.ContainsKey("ourLock");
            var owner = string.Empty;

            // Determine "open" status of file
            if (fields.ContainsKey("action"))
            {
                var action = fields["action"];
                if (action.Equals("delete") || action.Equals("add") || action.Equals("edit"))
                {
                    isOpen = true;
                }
            }

            // Determine owner
            if (isLockedByMe)
            {
                owner = this.Workspace;
            }

            // File is not locked by me, check if locked by another user
            else if (fields.ContainsKey("otherLock") && !string.IsNullOrEmpty(fields["otherLock"]))
            {
                owner = fields["otherLock"];
            }
            else if (fields.ContainsKey("otherLock1") && !string.IsNullOrEmpty(fields["otherLock1"]))
            {
                owner = fields["otherLock1"];
            }
            else if (arrayFields.ContainsKey("otherLock") && arrayFields["otherLock"].Any())
            {
                owner = string.Join(", ", arrayFields["otherLock"]);
            }
            else if (arrayFields.ContainsKey("otherLock1") && arrayFields["otherLock1"].Any())
            {
                owner = string.Join(", ", arrayFields["otherLock1"]);
            }

            // Determine lock status
            if (!string.IsNullOrEmpty(owner))
            {
                isLocked = true;
            }

            // Determine last modification time
            DateTime? lastModTime = null;
            if (fields.ContainsKey("headModTime"))
            {
                try
                {
                    var seconds = double.Parse(fields["headModTime"]);
                    lastModTime = Epoch.AddSeconds(seconds);
                }
                catch (Exception ex)
                {
                    request.Warnings.Add("Failed to parse file last mod time: " + ex.Message);
                }
            }

            return new FileStatus(isOpen, isLocked, isLockedByMe, owner, lastModTime);
        }

        /// <summary>
        /// Get the latest checkin time of a file.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The latest checkin time <see cref="DateTime"/>.
        /// </returns>
        public DateTime GetLatestCheckinTime(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var request = this.RunCmdFStat(path);

            var dateTime = request.RecordSet[0].Fields["headTime"];
            var utcTicks = int.Parse(dateTime);
            var p4ReqHandler = this.RequestHandler as P4RequestHandler;
            return p4ReqHandler.Connection.ConvertDate(utcTicks);
        }

        public DateTime GetTimeOfLastSubmitToDir(string path) 
        {
            path = this.GetDepotPath(path);
            if (!path.EndsWith("/...")) path += path.EndsWith("/") ? "..." : "/...";
            var request = new P4RunCmdRequest("changes", new string[]{"-t", "-ssubmitted", "-m1", path});
            this.RequestHandler.ActionRequest(request);
            if (request.HasErrors())
            {
                throw new Exception("Failed to get time of last submit to "+path+": " + request.FormattedErrors);
            }
            //if the path doesn't exist the call will return no records
            if (request.RecordSet.Records.Count() == 0) throw new Exception("Path "+path+" could not be found.");

            var dateTime = request.RecordSet[0].Fields["time"];
            var utcTicks = int.Parse(dateTime);
            var p4ReqHandler = this.RequestHandler as P4RequestHandler;
            return p4ReqHandler.Connection.ConvertDate(utcTicks);
        }

        public IList<RevisionInfo> GetRevisionHistory(string path, int maxHistoryLength = 100, int latestChangelist = 0, int oldestRevision = 0,
            int newestRevision = Int32.MaxValue)
        {
            throw new NotImplementedException();
        }

        public RevisionInfo GetRevisionInfo(string path, int revision = 0)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfRevisions(string path)
        {
            throw new NotImplementedException();
        }

        #endregion

        /// <summary>
        /// Initialize the asset manager.
        /// </summary>
        private void Init()
        {
            // Use P4 'info' to obtain workspace information
            var request = new P4RunCmdRequest("info");
            this.RequestHandler.ActionRequest(request);
            if (request.HasErrors())
            {
                throw new Exception("Failed to get workspace info: " + request.FormattedErrors);
            }

            // Set workspace root and fixed root
            this.root = request.RecordSet[0].Fields["clientRoot"];
            this.fixedRoot = this.root.Replace("/", "\\").TrimEnd('\\');
            this.Root = fixedRoot;
            // Load the client views (mappings between local and depot paths).
            this.LoadClientViews();

            // Load change lists from asset manager
            this.LoadChangeLists();
        }

        /// <summary>
        /// Initialize an asset from its P4Record.
        /// </summary>
        /// <param name="record">
        /// The P4 record.
        /// </param>
        /// <returns>
        /// The asset object <see cref="IAsset"/>.
        /// </returns>
        private IAsset InitAsset(P4Record record)
        {
            if (null == record)
            {
                throw new ArgumentNullException("record");
            }

            var isDefault = record["change"].Equals("default");

            IChangeList changeList;

            if (isDefault)
            {
                changeList = this.GetChangeList("default");

                if (null == changeList)
                {
                    throw new SystemException("Default change list expected to be initialized at this point");
                }
            }
            else
            {
                changeList = this.GetChangeList(record["change"]);

                // Create new change list for asset if required
                if (null == changeList)
                {
                    var request = new P4RunCmdRequest("describe", record["change"]);
                    this.RequestHandler.ActionRequest(request);
                    if (request.Status == RequestStatus.Failed || null == request.RecordSet)
                    {
                        throw new Exception("Failed to get change list for depot file: " + record["depotFile"]);
                    }

                    changeList = new P4ChangeList(this, request.RecordSet[0]);
                }
            }

            // Create and return new asset
            return new P4Asset(changeList, record);
        }

        /// <summary>
        /// GetLatest helper method.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        /// <param name="fileType">
        /// The file type.
        /// </param>
        /// <returns>
        /// The P4RunCmdRequest containing P4 request info <see cref="P4RunCmdRequest"/>.
        /// </returns>
        private P4RunCmdRequest GetLatestHelper(string path, bool force, string changeListNumber, string fileType)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var safePath = FileUtils.SafeCheckPath(path);
            var depotPath = this.GetDepotPath(safePath);

            var args = new List<string>();

            // Add force flag if required
            if (force)
            {
                args.Add("-f");
            }

            // Add file type to path
            if (!string.IsNullOrEmpty(fileType))
            {
                if (!depotPath.EndsWith("..."))
                {
                    throw new Exception("Depot path must end with '...' when file type is specified: " + depotPath);
                }

                depotPath += fileType;
            }

            // Add path to get latest on
            if (!string.IsNullOrEmpty(changeListNumber))
            {
                args.Add(depotPath + "@" + changeListNumber);
            }
            else
            {
                args.Add(depotPath);
            }

            var request = new P4RunCmdRequest("sync", args);
            this.RequestHandler.ActionRequest(request);

            // If not able to find file, this comes back as warning, so add it as error
            if (request.HasWarnings())
            {
                foreach (var warning in request.Warnings.Where(warning => warning.ToLower().Contains(NoSuchFileText)))
                {
                    request.Errors.Add(warning);
                }
            }

            return request;
        }

        /// <summary>
        /// Helper method to run P4 fstat command.
        /// </summary>
        /// <param name="path">
        /// The file path.
        /// </param>
        /// <returns>
        /// The completed request <see cref="IP4RunCmdRequest"/>.
        /// </returns>
        private IP4RunCmdRequest RunCmdFStat(string path)
        {
            var safePath = FileUtils.SafeCheckPath(path);
            safePath = this.GetDepotPath(safePath);

            var args = new[]
            {
                "-Op", // Include 'path' (i.e. local path) in output
                safePath
            };
            var request = new P4RunCmdRequest("fstat", args);
            this.RequestHandler.ActionRequest(request);
            if (request.HasErrors())
            {
                throw new Exception("Failed to get file info: " + request.FormattedErrors);
            }

            return request;
        }

        #region Structs

        /// <summary>
        /// The client view.
        /// </summary>
        public struct ClientView
        {
            /// <summary>
            /// Gets or sets the depot path.
            /// </summary>
            public string DepotPath { get; set; }

            /// <summary>
            /// Gets or sets the client path.
            /// </summary>
            public string ClientPath { get; set; }

            /// <summary>
            /// Gets or sets the local path.
            /// </summary>
            public string LocalPath { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether is active.
            /// </summary>
            public bool IsActive { get; set; }
        }

        #endregion
    }
}
