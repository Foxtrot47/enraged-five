﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="P4Asset.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the P4Asset type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using Rockstar.AssetManager.Infrastructure.Data;

namespace Rockstar.AssetManager.Perforce
{
    using System;
    using System.IO;

    using P4API;

    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Perforce.Utils;

    /// <summary>
    /// The P4 asset.
    /// </summary>
    public class P4Asset : IAsset
    {
        /// <summary>
        /// The local path.
        /// </summary>
        private string localPath;

        /// <summary>
        /// Initializes a new instance of the <see cref="P4Asset"/> class.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="record">
        /// The P4Record.
        /// </param>
        public P4Asset(IChangeList changeList, P4Record record)
        {
            if (null == changeList)
            {
                throw new ArgumentException("changelist");
            }

            if (null == changeList.AssetManager)
            {
                throw new ArgumentException("assetmanager");
            }

            this.AssetManager = changeList.AssetManager;
            this.ChangeList = changeList;

            if (null != record)
            {
                this.Init(record);
            }
            else
            {
                this.Action = "add";
                this.HaveRevision = 0;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="P4Asset"/> class.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        public P4Asset(IChangeList changeList) : this(changeList, null)
        {
        }

        /// <summary>
        /// Gets the asset manager.
        /// </summary>
        public IAssetManager AssetManager { get; private set; }

        /// <summary>
        /// Gets or sets the change list.
        /// </summary>
        public IChangeList ChangeList { get; set; }

        /// <summary>
        /// Gets or sets the local path.
        /// </summary>
        public string LocalPath
        {
            get
            {
                if (null == this.localPath)
                {
                    this.localPath = this.AssetManager.GetLocalPath(this.DepotPath);
                }

                return this.localPath;
            }

            set
            {
                this.localPath = value;
            }
        }

        /// <summary>
        /// Gets or sets the depot path.
        /// </summary>
        public string DepotPath { get; set; }

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Gets or sets the have revision.
        /// </summary>
        public uint HaveRevision { get; set; }

        /// <summary>
        /// Gets the latest version of the asset from the asset manager.
        /// </summary>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation  <see cref="bool"/>.
        /// </returns>
        public bool GetLatest(bool force)
        {
            return this.AssetManager.GetLatest(this, force);
        }

        /// <summary>
        /// Checkout the asset from the asset manager.
        /// </summary>
        /// <param name="lockAsset">
        /// Indicates whether to lock the asset on checkout.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Checkout(bool lockAsset)
        {
            return null != this.ChangeList.CheckoutAsset(this.LocalPath, lockAsset);
        }

        /// <summary>
        /// Lock the asset.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool Lock()
        {
            return this.ChangeList.LockAsset(this, false);
        }

        public FileStatus FileStatus { get; }

        /// <summary>
        /// Revert the local version of the asset.
        /// </summary>
        /// <param name="removeFromChangeList">
        /// Flag to indicate whether to remove asset from change list on successful revert.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Revert(bool removeFromChangeList = true)
        {
            if (this.ChangeList != null)
            {
                return this.ChangeList.RevertAsset(this, removeFromChangeList);
            }

            return true;
        }

        /// <summary>
        /// Mark the asset for add in the change list.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool MarkForAdd()
        {
            return null != this.ChangeList.MarkAssetForAdd(this.LocalPath);
        }

        /// <summary>
        /// Mark the asset for add in the change list.
        /// </summary>
        /// <param name="type">
        /// The asset type.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool MarkForAdd(string type)
        {
            return null != this.ChangeList.MarkAssetForAdd(this.LocalPath, type);
        }

        /// <summary>
        /// Move the asset to another change list.
        /// </summary>
        /// <param name="newChangeList">
        /// The new change list.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Move(IChangeList newChangeList)
        {
            return this.AssetManager.MoveAsset(this, newChangeList);
        }

        /// <summary>
        /// Move the asset to another change list with specified change list number.
        /// </summary>
        /// <param name="newChangeListNumber">
        /// The number of the new change list.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Move(uint newChangeListNumber)
        {
            foreach (var changeList in this.AssetManager.ChangeLists.Values)
            {
                if (changeList.Number == newChangeListNumber)
                {
                    return this.Move(changeList);
                }
            }

            throw new Exception("Failed to move asset to change list " + newChangeListNumber + ": " + this.LocalPath);
        }

        /// <summary>
        /// Move asset to new file path.
        /// </summary>
        /// <param name="newFilePath">
        /// The new file path.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool MoveAsset(string newFilePath)
        {
            return this.ChangeList.MoveAsset(this, newFilePath);
        }

        /// <summary>
        /// Mark the asset for delete in the change list.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool MarkForDelete()
        {
            return null != this.ChangeList.MarkAssetForDelete(this);
        }

        /// <summary>
        /// Get the previous revision of asset.
        /// </summary>
        /// <param name="force">
        /// Flag indicating whether operation should be forced if required.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool GetPrevious(bool force)
        {
            return this.AssetManager.GetPrevious(this.LocalPath, force);
        }

        /// <summary>
        /// Indicates if local file is readonly.
        /// </summary>
        /// <returns>
        /// Readonly status <see cref="bool"/>.
        /// </returns>
        public bool IsReadonly()
        {
            if (!File.Exists(this.LocalPath))
            {
                throw new FileNotFoundException(this.LocalPath);
            }

            return new FileInfo(this.LocalPath).IsReadOnly;
        }

        /// <summary>
        /// Set readonly file attribute on local version of asset.
        /// </summary>
        /// <param name="flag">
        /// Flag indicating readonly value to set.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool SetReadonly(bool flag)
        {
            if (!File.Exists(this.LocalPath))
            {
                throw new FileNotFoundException(this.LocalPath);
            }

            var currAttrs = File.GetAttributes(this.LocalPath);
            if (flag)
            {
                File.SetAttributes(this.LocalPath, currAttrs & FileAttributes.ReadOnly);
            }
            else
            {
                File.SetAttributes(this.LocalPath, currAttrs & ~FileAttributes.ReadOnly);
            }

            return true;
        }

        public IList<RevisionInfo> GetHistory(int maxHistoryLength = 100, int latestChangelist = 0, int oldestRevision = 0,
            int newestRevision = Int32.MaxValue)
        {
            throw new NotImplementedException();
        }

        public RevisionInfo GetRevisionInfo(int revision = 0)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Initialize asset from P4Record.
        /// </summary>
        /// <param name="record">
        /// The P4 record.
        /// </param>
        private void Init(P4Record record)
        {
            // Set depot path
            this.DepotPath = RecordParserUtils.GetStringField(record, "depotFile");

            if (string.IsNullOrEmpty(this.DepotPath))
            {
                throw new Exception("Depot path not found in P4Record");
            }

            // Set local path
            if (record.Fields.ContainsKey("file"))
            {
                this.LocalPath = RecordParserUtils.GetStringField(record, "file");
            }
            else
            {
                this.LocalPath = this.AssetManager.GetLocalPath(this.DepotPath);
            }

            if (string.IsNullOrEmpty(this.LocalPath))
            {
                throw new Exception("Local path not set");
            }
            
            // Set action and revision number
            this.Action = RecordParserUtils.GetStringField(record, "action");
            if (record.Fields.ContainsKey("haveRev") && !record.Fields["haveRev"].Equals("none"))
            {
                this.HaveRevision = RecordParserUtils.GetUIntField(record, "haveRev");
            }

            // Add asset to change list
            this.ChangeList.AddAsset(this);
        }
    }
}
