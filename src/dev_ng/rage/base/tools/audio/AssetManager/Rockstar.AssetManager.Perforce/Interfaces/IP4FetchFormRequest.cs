﻿// -----------------------------------------------------------------------
// <copyright file="IP4FetchFormRequest.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce.Interfaces
{
    using P4API;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The IP4FetchFormRequest interface.
    /// </summary>
    public interface IP4FetchFormRequest : IRequest
    {
        /// <summary>
        /// Gets the form name.
        /// </summary>
        string FormName { get; }

        /// <summary>
        /// Gets the args.
        /// </summary>
        string[] Args { get; }

        /// <summary>
        /// Gets or sets the form.
        /// </summary>
        P4Form Form { get; set; }
    }
}
