﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="P4ChangeList.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the P4ChangeList type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using P4API;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Perforce.Attributes;
    using Rockstar.AssetManager.Perforce.Interfaces;
    using Rockstar.AssetManager.Perforce.Utils;
    
    /// <summary>
    /// The P4 change list.
    /// </summary>
    public class P4ChangeList : IChangeList
    {
        private const string Default = "default";

        /// <summary>
        /// The request handler.
        /// </summary>
        private IP4RequestHandler requestHandler;

        /// <summary>
        /// Initializes a new instance of the <see cref="P4ChangeList"/> class.
        /// </summary>
        /// <param name="assetManager">
        /// The asset Manager.
        /// </param>
        /// <param name="description">
        /// The change list description.
        /// </param>
        /// <param name="state">
        /// The change list state.
        /// </param>
        public P4ChangeList(IAssetManager assetManager, string description, ChangeListState state = ChangeListState.Pending)
        {
            this.Init(assetManager, description, state);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="P4ChangeList"/> class.
        /// </summary>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        /// <param name="record">
        /// The P4 record containing change list info from asset manager.
        /// </param>
        /// <exception cref="ArgumentException">
        /// Thrown if invalid request handler detected in asset manager.
        /// </exception>
        public P4ChangeList(IAssetManager assetManager, P4Record record)
        {
            if (record == null)
            {
                throw new ArgumentNullException("record");
            }

            this.Init(assetManager, null, ChangeListState.Unknown, record);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="P4ChangeList"/> class.
        /// </summary>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        /// <param name="changeList">
        /// The P4 change list.
        /// </param>
        public P4ChangeList(IAssetManager assetManager, P4PendingChangelist changeList)
        {
            if (changeList == null)
            {
                throw new ArgumentNullException("changeList");
            }

            this.Init(assetManager, null, ChangeListState.Unknown, null, changeList);
        }

        #region Properties

        /// <summary>
        /// Gets the asset manager.
        /// </summary>
        public IAssetManager AssetManager { get; private set; }

        /// <summary>
        /// Gets the change list number.
        /// </summary>
        public uint Number { get; private set; }

        /// <summary>
        /// Gets the change list number as a string.
        /// </summary>
        public string NumberAsString
        {
            get
            {
                return this.Number > 0 ? this.Number.ToString(CultureInfo.InvariantCulture) : "default";
            }
        }

        /// <summary>
        /// Gets the change list created timestamp.
        /// </summary>
        public DateTime Created { get; private set; }

        /// <summary>
        /// Gets the change list username.
        /// </summary>
        public string Username { get; private set; }

        /// <summary>
        /// Gets or sets the change list workspace.
        /// </summary>
        public string Workspace { get; set; }

        /// <summary>
        /// Gets or sets the change list description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets the change list state.
        /// </summary>
        public ChangeListState State { get; private set; }

        /// <summary>
        /// Gets the change list assets.
        /// </summary>
        public IList<IAsset> Assets { get; private set; }

        /// <summary>
        /// Gets the asset local paths.
        /// </summary>
        public HashSet<string> AssetLocalPaths { get; private set; }

        /// <summary>
        /// Gets the asset depot paths.
        /// </summary>
        public HashSet<string> AssetDepotPaths { get; private set; } 

        #endregion

        #region Actions

        /// <summary>
        /// Refresh the change list assets using the versions in asset manager.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Refresh()
        {
            // Get files opened in change list by current user under user's workspace
            var args = new[]
                {
                    "-u" + this.AssetManager.Username,
                    "-C" + this.Workspace,
                    "-c" + this.NumberAsString
                };
            var request = new P4RunCmdRequest("opened", args);
            this.requestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed || null == request.RecordSet)
            {
                throw new Exception("Failed to get opened file info: " + request.FormattedErrors);
            }

            // Initialize asset objects
            var assetDepotPaths = new HashSet<string>();

            foreach (P4Record record in request.RecordSet)
            {
                var depotPath = record.Fields["depotFile"];
                if (!this.AssetDepotPaths.Contains(depotPath))
                {
                    this.AddAsset(new P4Asset(this, record));
                }

                assetDepotPaths.Add(depotPath.ToUpper());
            }

            // Delete any local asset objects that are no longer checked out in change list
            var assetsToDelete = this.Assets.Where(
                asset => !assetDepotPaths.Contains(asset.DepotPath.ToUpper())).Distinct().ToList();

            foreach (var asset in assetsToDelete)
            {
                this.RemoveAsset(asset);
            }

            return true;
        }

        /// <summary>
        /// Update the change list' description.
        /// </summary>
        /// <param name="description">
        /// The new description.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool UpdateDescription(string description)
        {
            if (string.IsNullOrEmpty(this.Description))
            {
                throw new Exception("Default change list cannot be renamed");
            }

            if (string.IsNullOrEmpty(description))
            {
                throw new Exception("New description cannot be empty");
            }

            var updateRequest = new P4ChangeListUpdateRequest(this.NumberAsString, description);
            this.requestHandler.ActionRequest(updateRequest);
            if (updateRequest.HasErrors())
            {
                throw new Exception("Failed to update change list: " + updateRequest.FormattedErrors);
            }

            this.Description = description;

            return true;
        }

        /// <summary>
        /// Submit the change list.
        /// </summary>
        /// <param name="description">
        /// The change list description.
        /// </param>
        /// <param name="submittedChangeListNumber">
        /// The submitted change list number.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Submit(string description, out string submittedChangeListNumber)
        {
            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException("description");
            }

            // Update change list description in case it has changed locally
            if (!this.UpdateDescription(description))
            {
                throw new Exception("Failed to update the change list description");
            }

            // Submit change list
            var args = new List<string>
                {
                    "-c" + this.NumberAsString
                };
            var request = new P4RunCmdRequest("submit", args);
            this.requestHandler.ActionRequest(request);
            if (request.HasErrors())
            {
                throw new Exception("Failed to submit change list: " + request.FormattedErrors);
            }

            // Remove change list from local list in asset manager
            this.AssetManager.ChangeLists.Remove(this.NumberAsString);

            submittedChangeListNumber = (
                from record in request.RecordSet.Records 
                where record.Fields.ContainsKey("submittedChange") 
                select record.Fields["submittedChange"]).FirstOrDefault();

            return true;
        }

        /// <summary>
        /// Submit the change list.
        /// </summary>
        /// <param name="description">
        /// The change list description.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Submit(string description)
        {
            string _;
            return this.Submit(description, out _);
        }

        /// <summary>
        /// Submit the change list using the current/default description.
        /// </summary>
        /// <param name="submittedChangeListNumber">
        /// The submitted change list number.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Submit(out string submittedChangeListNumber)
        {
            if (string.IsNullOrEmpty(this.Description))
            {
                throw new Exception("Change list description must be set before submit");
            }

            return this.Submit(this.Description, out submittedChangeListNumber);
        }

        /// <summary>
        /// Submit the change list using the current/default description and
        /// without providing the submitted change list number.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Submit()
        {
            string _;
            return this.Submit(out _);
        }

        /// <summary>
        /// Revert all assets in change list.
        /// </summary>
        /// <param name="delete">
        /// Flag to indicate whether to delete change list after successful revert.
        /// </param>
        /// <param name="unchangedOnly">
        /// Revert unchanged files only.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Revert(bool delete, bool unchangedOnly = false)
        {
            if (unchangedOnly)
            {
                return this.RevertUnchanged();
            }

            if (!this.RevertHelper())
            {
                throw new Exception("Failed to revert change list files");
            }

            return !delete || this.Delete();
        }

        /// <summary>
        /// Revert asset.
        /// </summary>
        /// <param name="asset">
        /// The asset to revert.
        /// </param>
        /// <param name="removeFromChangeList">
        /// Flag to indicate whether to remove asset from change list on successful revert.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool RevertAsset(IAsset asset, bool removeFromChangeList)
        {
            if (null == asset)
            {
                throw new ArgumentNullException("asset");
            }

            var safePath = FileUtils.SafeCheckPath(asset.LocalPath);

            var args = new List<string>
                {
                    "-c" + this.NumberAsString,
                    safePath
                };
            var request = new P4RunCmdRequest("revert", args);
            this.requestHandler.ActionRequest(request);
            if (request.HasErrors())
            {
                throw new Exception("Failed to revert asset: " + request.FormattedErrors);
            }

            this.RemoveAsset(asset);

            return true;
        }

        /// <summary>
        /// Revert any unchanged assets in change list.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool RevertUnchanged()
        {
            if (!this.RevertHelper("-a"))
            {
                throw new Exception("Failed to revert unchanged files in change list " + this.NumberAsString);
            }

            if (!this.Refresh())
            {
                throw new Exception("Failed to refresh change list asset list: " + this.NumberAsString);
            }

            return true;
        }

        /// <summary>
        /// Delete the change list and its pending asset changes/actions.
        /// </summary>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool Delete()
        {
            return this.AssetManager.DeleteChangeList(this);
        }

        #endregion

        #region Assets

        /// <summary>
        /// Add an asset to the change list.
        /// </summary>
        /// <param name="asset">
        /// The asset to add.
        /// </param>
        public void AddAsset(IAsset asset)
        {
            if (this.Assets.Contains(asset) || 
                null != this.GetAsset(asset.LocalPath))
            {
                // Already have asset in change list
                return;
            }

            // Add asset to change list
            asset.ChangeList = this;
            this.Assets.Add(asset);
            this.AssetLocalPaths.Add(asset.LocalPath);
            this.AssetDepotPaths.Add(asset.DepotPath);
        }

        /// <summary>
        /// Remove an asset from the change list.
        /// </summary>
        /// <param name="asset">
        /// The asset to remove.
        /// </param>
        /// <returns>
        /// Indication of success of operation (false if not found) <see cref="bool"/>.
        /// </returns>
        public bool RemoveAsset(IAsset asset)
        {
            if (!this.Assets.Contains(asset))
            {
                return false;
            }

            asset.ChangeList = null;
            this.AssetLocalPaths.Remove(asset.LocalPath);
            this.AssetDepotPaths.Remove(asset.DepotPath);
            this.Assets.Remove(asset);
            return true;
        }

        /// <summary>
        /// Return string containing list of asset paths separated by spaces.
        /// </summary>
        /// <returns>
        /// Asset paths as string (space separated) <see cref="string"/>.
        /// </returns>
        public string AssetsAsString()
        {
            var paths = new HashSet<string>();
            foreach (var asset in this.Assets)
            {
                paths.Add(asset.LocalPath);
            }

            return string.Join(" ", paths.ToArray());
        }

        /// <summary>
        /// Get asset object in change list matching specified path.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// Matching asset (null if not found) <see cref="IAsset"/>.
        /// </returns>
        public IAsset GetAsset(string path)
        {
            var safePath = FileUtils.SafeCheckPath(path);
            var safePathUpper = safePath.ToUpper();

            return this.Assets.FirstOrDefault(
                asset => asset.DepotPath.ToUpper().Equals(safePathUpper) ||
                    asset.LocalPath.ToUpper().Equals(safePathUpper));
        }

        /// <summary>
        /// Checkout asset into change list.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <param name="lockAsset">
        /// Indicates whether to lock the asset on checkout.
        /// </param>
        /// <returns>
        /// The checked-out asset (null on failure) <see cref="IAsset"/>.
        /// </returns>
        public IAsset CheckoutAsset(string path, bool lockAsset)
        {
            var safePath = FileUtils.SafeCheckPath(path);
            var depotPath = this.AssetManager.GetDepotPath(safePath);

            // Need to get latest if we have revision 0 or checkout fails
            this.AssetManager.GetLatest(path, true);

            var asset = this.GetAsset(path);

            if (null == asset)
            {
                var args = new List<string> { "-c" + this.NumberAsString, depotPath };

                var request = new P4RunCmdRequest("edit", args);
                this.AssetManager.RequestHandler.ActionRequest(request);
                if (request.Status == RequestStatus.Failed)
                {
                    throw new Exception("Failed to checkout asset: " + request.FormattedErrors);
                }

                if (request.FormattedWarnings.ToLower().Contains("file(s) not on client"))
                {
                    throw new Exception("Failed to checkout asset: " + request.FormattedWarnings);
                }

                if (request.FormattedMessages.ToLower().Contains("can't change from"))
                {
                    throw new Exception("File already checked out in another change list: " + request.FormattedMessages);
                }

                // Create new asset for file being edited
                asset = new P4Asset(this, request.RecordSet[0]);

                this.AddAsset(asset);
            }

            if (lockAsset)
            {
                if (!this.LockAsset(asset))
                {
                    throw new Exception("Failed to lock asset: " + asset.LocalPath);
                }
            }

            return asset;
        }

        /// <summary>
        /// Mark an asset for add in change list.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <param name="type">
        /// The asset type.
        /// </param>
        /// <returns>
        /// Asset marked for add (null on failure) <see cref="IAsset"/>.
        /// </returns>
        public IAsset MarkAssetForAdd(string path, string type)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var safePath = FileUtils.SafeCheckPath(path);

            var args = new List<string>
                {
                    "-c" + this.NumberAsString
                };


            // Add file type if specified
            if (!string.IsNullOrEmpty(type))
            {
                args.Add("-t" + type);
            }

            args.Add(safePath);

            var request = new P4RunCmdRequest("add", args);
            this.AssetManager.RequestHandler.ActionRequest(request);

            if (request.Status == RequestStatus.Failed)
            {
                throw new Exception("failed to mark asset for add: " + request.FormattedErrors);
            }

            // Get asset or create new if not in change list
            var asset = this.GetAsset(path);
            if (null == asset)
            {
                asset = new P4Asset(this)
                    {
                        LocalPath = path,
                        DepotPath = this.AssetManager.GetDepotPath(path)
                    };
                this.AddAsset(asset);
            }

            return asset;
        }

        /// <summary>
        /// Mark an asset for add in change list.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// Asset marked for add (null on failure) <see cref="IAsset"/>.
        /// </returns>
        public IAsset MarkAssetForAdd(string path)
        {
            return this.MarkAssetForAdd(path, null);
        }

        /// <summary>
        /// Mark an asset for delete in change list.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// Asset to be deleted <see cref="IAsset"/>.
        /// </returns>
        public IAsset MarkAssetForDelete(string path)
        {
            var safePath = FileUtils.SafeCheckPath(path);
            this.AssetManager.GetLatest(safePath, true);

            var args = new List<string>
                {
                    "-c" + this.NumberAsString,
                    safePath
                };
            var request = new P4RunCmdRequest("delete", args);
            this.AssetManager.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed)
            {
                throw new Exception("Failed to mark asset for delete: " + request.FormattedErrors);
            }

            // Get asset or create new if not in change list
            var asset = this.GetAsset(path);
            if (null == asset)
            {
                asset = new P4Asset(this)
                    {
                        LocalPath = path,
                        DepotPath = this.AssetManager.GetDepotPath(path)
                    };
            }

            // Lock asset
            if (!this.LockAsset(asset))
            {
                throw new Exception("Failed to lock asset: " + asset.LocalPath);
            }

            this.AddAsset(asset);
            return asset;
        }

        /// <summary>
        /// Mark an asset for delete in change list.
        /// </summary>
        /// <param name="asset">
        /// The asset.
        /// </param>
        /// <returns>
        /// Asset to be deleted <see cref="IAsset"/>.
        /// </returns>
        public IAsset MarkAssetForDelete(IAsset asset)
        {
            if (null == asset)
            {
                throw new ArgumentNullException("asset");
            }

            return this.MarkAssetForDelete(asset.LocalPath);
        }

        /// <summary>
        /// Lock an asset.
        /// </summary>
        /// <param name="asset">
        /// The asset.
        /// </param>
        /// <param name="revertOnFailure">
        /// The revert On failure flag.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        public bool LockAsset(IAsset asset, bool revertOnFailure = true)
        {
            var safePath = FileUtils.SafeCheckPath(asset.DepotPath);

            if (string.IsNullOrEmpty(safePath))
            {
                throw new Exception("Path required to lock asset");
            }

            // Lock asset
            var request = new P4RunCmdRequest("lock", safePath);
            this.AssetManager.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed)
            {
                throw new Exception("Failed to lock asset: " + request.FormattedErrors);
            }

            // Perforce only gives info message if it can't lock file, not an error,
            // so need to do an extra bit of checking here
            var lockStatus = this.AssetManager.GetFileStatus(safePath);
            if (!lockStatus.IsLocked || !lockStatus.IsLockedByMe)
            {
                if (revertOnFailure)
                {
                    asset.Revert(false);
                }

                throw new Exception(asset.LocalPath + " currently locked by " + lockStatus.Owner);
            }

            return true;
        }

        /// <summary>
        /// Lock an asset.
        /// </summary>
        /// <param name="path">
        /// The asset path.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool LockAsset(string path)
        {
            var asset = this.GetAsset(path);
            var checkedOut = false;

            if (null == asset)
            {
                asset = this.CheckoutAsset(path, true);
                checkedOut = true;
            }

            if (null == asset)
            {
                throw new Exception("Failed to checkout asset before locking: " + path);
            }

            return this.LockAsset(asset, !checkedOut);
        }

        /// <summary>
        /// Move asset to new file path.
        /// </summary>
        /// <param name="asset">
        /// The asset.
        /// </param>
        /// <param name="newFilePath">
        /// The new file path.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        public bool MoveAsset(IAsset asset, string newFilePath)
        {
            if (!this.AssetManager.IsCheckedOut(asset.LocalPath))
            {
                throw new Exception("Asset must be checked out for it to be moved: " + asset.LocalPath);
            }

            var depotPath = this.AssetManager.GetDepotPath(newFilePath);

            var args = new List<string> { asset.DepotPath, depotPath };
            var request = new P4RunCmdRequest("move", args);
            this.AssetManager.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed)
            {
                throw new Exception("Failed to move asset: " + request.FormattedErrors);
            }

            if (request.Warnings.Any(warning => warning.ToLower().Contains("file(s) not in client view")))
            {
                throw new Exception(string.Format("Failed to move asset - file not in client view: {0} --> {1}", asset.DepotPath, depotPath));
            }

            if (request.Messages.Any(msg => msg.ToLower().Contains("can't move to an existing file")))
            {
                throw new Exception("Failed to move asset - destination file already exists: " + depotPath);
            }

            return true;
        }

        /// <summary>
        /// Returns value indicating whether change list has shelved files.
        /// </summary>
        /// <returns>
        /// Value indicating  whether change list has shelved files <see cref="bool"/>.
        /// </returns>
        public bool HasShelvedAssets()
        {
            var args = new List<string>
                {
                    "-S",
                    this.NumberAsString
                };
            var request = new P4RunCmdRequest("describe", args);
            this.AssetManager.RequestHandler.ActionRequest(request);
            if (request.Status == RequestStatus.Failed)
            {
                throw new Exception("Failed to get change list description: " + request.FormattedErrors);
            }

            return request.RecordSet.Records[0].ArrayFields.Count > 0;
        }

        #endregion

        /// <summary>
        /// Initialize the change list.
        /// </summary>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="record">
        /// The P4 record.
        /// </param>
        /// <param name="changeList">
        /// The P4 change list.
        /// </param>
        private void Init(
            IAssetManager assetManager,
            string description = null,
            ChangeListState state = ChangeListState.Unknown,
            P4Record record = null,
            P4PendingChangelist changeList = null)
        {
            if (assetManager == null)
            {
                throw new ArgumentNullException("assetManager");
            }

            this.AssetManager = assetManager;
            this.requestHandler = assetManager.RequestHandler as IP4RequestHandler;

            if (null == this.requestHandler)
            {
                throw new ArgumentException("Expected request handler to be of type IP4RequestHandler");
            }

            this.Description = description;
            this.State = state;
            this.Assets = new List<IAsset>();
            this.AssetLocalPaths = new HashSet<string>();
            this.AssetDepotPaths = new HashSet<string>();

            // Initialize from P4 record/new change list
            if (null != record)
            {
                this.InitFromP4Record(record);
            }
            else if (null != changeList)
            {
                this.InitFromP4ChangeList(changeList);
            }
        }

        /// <summary>
        /// Initialize change list and its assets from P4Record.
        /// </summary>
        /// <param name="record">
        /// The P4 record.
        /// </param>
        private void InitFromP4Record(P4Record record)
        {
            this.Number = RecordParserUtils.GetChangelistField(record, "change");
            this.Created = RecordParserUtils.GetDateTimeField(this.requestHandler.Connection, record, "time", DateTime.MinValue);
            this.Username = RecordParserUtils.GetStringField(record, "user");
            this.Workspace = RecordParserUtils.GetStringField(record, "client");
            this.Description = RecordParserUtils.GetStringField(record, "desc");

            var status = RecordParserUtils.GetStringField(record, "status");
            this.State = P4ConstantAttribute.GetEnumValueFromString(status, ChangeListState.Unknown);
        }

        /// <summary>
        /// Initialize change list and its assets from P4PendingChangelist.
        /// </summary>
        /// <param name="changeList">
        /// The P4 change list.
        /// </param>
        private void InitFromP4ChangeList(P4PendingChangelist changeList)
        {
            this.Number = (uint)changeList.Number;
            this.Description = changeList.Description;
            this.State = ChangeListState.Pending;
        }

        /// <summary>
        /// Helper method for performing Perforce revert requests.
        /// </summary>
        /// <param name="additionalArgs">
        /// The additional args for revert command.
        /// </param>
        /// <returns>
        /// Indication of success of operation <see cref="bool"/>.
        /// </returns>
        private bool RevertHelper(params string[] additionalArgs)
        {
            // Revert everything filtered by change list number
            var args = new List<string>
                {
                    "-c" + this.NumberAsString
                };

            if (null != additionalArgs && additionalArgs.Length > 0)
            {
                args.AddRange(new List<string>(additionalArgs));
            }

            // File path must come after args
            args.Add("//...");

            var request = new P4RunCmdRequest("revert", args);
            this.requestHandler.ActionRequest(request);
            if (request.HasErrors())
            {
                foreach (var error in request.Errors)
                {
                    var tmpError = error.Trim().ToLower();

                    // These errors just mean the change list is empty, so okay to ignore
                    if (!tmpError.Equals("file(s) not opened on this client") &&
                        !tmpError.Equals("file(s) not opened in that changelist"))
                    {
                        throw new Exception("Failed to revert change list: " + request.FormattedErrors);
                    }
                }

                // If exception not thrown at this point, then change list is empty
                return true;
            }

            // Make sure files are set as readonly after revert
            foreach (var asset in this.Assets)
            {
                // Check file wasn't deleted in process
                if (File.Exists(asset.LocalPath))
                {
                    if (!asset.IsReadonly())
                    {
                        asset.SetReadonly(true);
                    }
                }
            }

            return true;
        }
    }
}
