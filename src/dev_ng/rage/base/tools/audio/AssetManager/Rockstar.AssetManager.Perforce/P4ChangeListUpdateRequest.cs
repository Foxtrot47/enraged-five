﻿// -----------------------------------------------------------------------
// <copyright file="P4ChangeListUpdateRequest.cs" company="Rockstar North">
// 
// </copyright>
// <summary>
//   Defines the P4ChangeListUpdateRequest type.
// </summary>
// -----------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce
{
    using System;

    using P4API;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Perforce.Interfaces;

    /// <summary>
    /// P4 form request.
    /// </summary>
    public class P4ChangeListUpdateRequest : P4BaseRequest, IP4ChangeListUpdateRequest
    {
        /// <summary>
        /// The P4 unparsed record set.
        /// </summary>
        private P4UnParsedRecordSet recordSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="P4ChangeListUpdateRequest"/> class.
        /// </summary>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        public P4ChangeListUpdateRequest(string changeListNumber, string description)
        {
            if (string.IsNullOrEmpty(changeListNumber))
            {
                throw new ArgumentNullException("changeListNumber");
            }

            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException("description");
            }

            this.ChangeListNumber = changeListNumber;
            this.Description = description;
        }

        /// <summary>
        /// Gets the change list number.
        /// </summary>
        public string ChangeListNumber { get; private set; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets or sets the record set.
        /// </summary>
        public P4UnParsedRecordSet RecordSet
        {
            get
            {
                return this.recordSet;
            }

            set
            {
                this.recordSet = value;
                this.Messages.AddRange(this.recordSet.Messages);
                this.Status = RequestStatus.Completed;

                if (this.recordSet.HasWarnings())
                {
                    this.Warnings.AddRange(this.recordSet.Warnings);
                    this.Status = RequestStatus.CompletedWithWarnings;
                }

                if (this.recordSet.HasErrors())
                {
                    this.Errors.AddRange(this.recordSet.Errors);
                    this.Status = RequestStatus.Failed;
                }
            }
        }
    }
}
