﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="P4RequestHandler.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The P4 request handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Runtime.Remoting.Lifetime;
using System.Text;

namespace Rockstar.AssetManager.Perforce
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading;

    using P4API;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Perforce.Interfaces;

    /// <summary>
    /// The P4 request handler.
    /// </summary>
    public class P4RequestHandler : IP4RequestHandler
    {
        /// <summary>
        /// Thread-safe queue of pending requests.
        /// </summary>
        private readonly ConcurrentQueue<IRequest> pendingRequests = new ConcurrentQueue<IRequest>();
        private ILogger logger = null;

        /// <summary>
        /// The queue processing thread.
        /// </summary>
        //private Thread queueProcessingThread;

        /// <summary>
        /// Initializes a new instance of the <see cref="P4RequestHandler"/> class.
        /// </summary>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        /// <param name="host">
        /// The host. Not required for P4.
        /// </param>
        /// <param name="port">
        /// The port. A P4 port comprises server host and port.
        /// </param>
        /// <param name="workspace">
        /// The workspace.
        /// </param>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="useLogin">
        /// Indicates whether to use login functionality.
        /// </param>
        /// <exception cref="ArgumentException">
        /// Thrown if any arguments are invalid.
        /// </exception>
        public P4RequestHandler(
            IAssetManager assetManager, string host, string port, string workspace, string username, string password, bool useLogin, string description)
        {
            if (null == assetManager)
            {
                throw new ArgumentNullException("assetManager");
            }

            // Host isn't required here; port is string comprising server host+port
            if (string.IsNullOrEmpty(port))
            {
                throw new ArgumentNullException("port");
            }

            if (string.IsNullOrEmpty(workspace))
            {
                throw new ArgumentNullException("workspace");
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException("username");
            }

            if (useLogin && string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Password required if useLogin is enabled");
            }

            this.AssetManager = assetManager;
            this.Host = host;
            this.Port = port;
            this.Workspace = workspace;
            this.Username = username;
            this.Password = password;
            this.UseLogin = useLogin;
            this.Description = description;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="P4RequestHandler"/> class.
        /// </summary>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        /// <param name="host">
        /// The host. Not required for P4.
        /// </param>
        /// <param name="port">
        /// The port. A P4 port comprises server host and port.
        /// </param>
        /// <param name="workspace">
        /// The workspace.
        /// </param>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="useLogin">
        /// Indicates whether to use login functionality.
        /// </param>
        /// /// <param name="logger">
        /// Logger to log timestamps of P4 API calls.
        /// </param>
        /// <exception cref="ArgumentException">
        /// Thrown if any arguments are invalid.
        /// </exception>
        public P4RequestHandler(
            IAssetManager assetManager, string host, string port, string workspace, string username, string password, bool useLogin, ILogger logger, string description) 
            : this(assetManager, host, port, workspace, username, password, useLogin, description)
        {
            this.logger = logger;
        }

        #region Properties

        /// <summary>
        /// Gets the P4 connection.
        /// </summary>
        public P4Connection Connection { get; private set; }

        /// <summary>
        /// Gets the asset manager.
        /// </summary>
        public IAssetManager AssetManager { get; private set; }

        /// <summary>
        /// Gets the host.
        /// </summary>
        public string Host { get; private set; }

        /// <summary>
        /// Gets the port.
        /// </summary>
        public string Port { get; private set; }

        /// <summary>
        /// Gets the workspace.
        /// </summary>
        public string Workspace { get; private set; }

        /// <summary>
        /// Gets the username.
        /// </summary>
        public string Username { get; private set; }

        /// <summary>
        /// Gets the password.
        /// </summary>
        public string Password { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to use login functionality.
        /// </summary>
        public bool UseLogin { get; private set; }

        /// <summary>
        /// Gets the description of the connection.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the pending request count.
        /// </summary>
        public int PendingCount
        {
            get { return this.pendingRequests.Count; }
        }

        #endregion

        #region Connection

        /// <summary>
        /// Connect to the asset manager.
        /// </summary>
        /// <returns>
        /// Indication of connection success <see cref="bool"/>.
        /// </returns>
        public bool Connect()
        {
            lock (this)
            {
                if (this.IsConnected())
                {
                    return true;
                }

                this.Connection = new P4Connection { Port = this.Port, User = this.Username, Client = this.Workspace};
                if(!string.IsNullOrEmpty(this.Description))this.Connection.CallingProgram = this.Description + " " + this.Connection.CallingProgram;



                if (!this.UseLogin)
                {
                    this.Connection.Password = this.Password;
                }

                this.Connection.Connect();

                if (this.UseLogin)
                {
                    this.Connection.Login(this.Password);
                }

                return this.IsConnected();
            }
        }

        /// <summary>
        /// Disconnect from the asset manager.
        /// </summary>
        public void Disconnect()
        {
            lock (this)
            {
                if (null != this.Connection)
                {
                    this.Connection.Disconnect();
                }
            }
        }

        /// <summary>
        /// Reconnect to the asset manager.
        /// </summary>
        /// <returns>
        /// Indication of connection success <see cref="bool"/>.
        /// </returns>
        public bool Reconnect()
        {
            lock (this)
            {
                this.Disconnect();
                return this.Connect();
            }
        }

        /// <summary>
        /// Get asset manager connection status.
        /// </summary>
        /// <returns>
        /// Indication of connection status <see cref="bool"/>.
        /// </returns>
        public bool IsConnected()
        {
            lock (this)
            {
                return null != this.Connection && this.Connection.IsValidConnection(true, true);
            }
        }

        #endregion

        #region Requests


        public void ActionRequest(IRequest request)
        {
            ActionRequest(request, true);
        }

        /// <summary>
        /// Actions a request to the asset manager (synchronously).
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        public void ActionRequest(IRequest request, bool initialRequest)
        {
            /* USING SINGLE THREAD SUPPORT ONLY FOR NOW
            
            // Add request to queue and wait for it to be actioned
            var semaphore = this.ActionRequestAsync(request);
            semaphore.WaitOne();
            */

            lock (this)
            {
                var cmdRequest = request as IP4RunCmdRequest;
                var changeListRequest = request as IP4NewChangeListRequest;
                var changeListUpdateRequest = request as IP4ChangeListUpdateRequest;
                var fetchFormRequest = request as IP4FetchFormRequest;

                try
                {
                    if (null != cmdRequest)
                    {
                        // Process P4 run command request
                        cmdRequest.RecordSet = this.Connection.Run(cmdRequest.Command, cmdRequest.Args);
                        try
                        {
                            if (logger != null)
                            {
                                var args = new StringBuilder();
                                var firstArg = true;
                                foreach (String arg in cmdRequest.Args)
                                {
                                    if (firstArg)
                                    {
                                        firstArg = false;
                                        args.Append(arg);
                                    }
                                    else
                                    {
                                        args.AppendFormat(", {0}", arg);
                                    }
                                }
                                
                                logger.writeLine(DateTime.Now.ToString("dd MMM HH:mm:ss.f")+" P4 API call: "+cmdRequest.Command+" args: " + args.ToString());
                            }
                        }
                        catch (Exception e)
                        {
                            //ignore logger exceptions    
                        }
                    }
                    else if (null != changeListRequest)
                    {
                        // Process P4 new change list request
                        var p4ChangeList = this.Connection.CreatePendingChangelist(changeListRequest.Description);
                        var changeList = new P4ChangeList(this.AssetManager, p4ChangeList) {Workspace = this.Workspace};
                        changeListRequest.ChangeList = changeList;
                    }
                    else if (null != changeListUpdateRequest)
                    {
                        // Process P4 form request
                        var form = this.Connection.Fetch_Form("change", changeListUpdateRequest.ChangeListNumber);
                        form["Description"] = changeListUpdateRequest.Description;
                        changeListUpdateRequest.RecordSet = this.Connection.Save_Form(form);
                    }
                    else if (null != fetchFormRequest)
                    {
                        fetchFormRequest.Form = this.Connection.Fetch_Form(
                            fetchFormRequest.FormName, fetchFormRequest.Args);
                        if (null == fetchFormRequest.Form)
                        {
                            fetchFormRequest.Errors.Add("Failed to fetch form: " + fetchFormRequest.FormName);
                            fetchFormRequest.Status = RequestStatus.Failed;
                        }
                    }
                    else
                    {
                        request.Errors.Add("P4 request type not recognised");
                        request.Status = RequestStatus.Failed;
                    }
                }
                catch (Exception ex)
                {
                    if (initialRequest)
                    {
                        try
                        {
                            // see if error is due to lost connection
                            // -> reconnect and try again
                            if (!this.IsConnected())
                            {
                                if (!this.Connect())
                                {
                                    request.Status = RequestStatus.Failed;
                                    request.Errors.Add("Failed to connect to Perforce server");
                                    return;
                                }
                            }
                            ActionRequest(request, false);
                        }
                        catch (Exception exc)
                        {
                            cmdRequest.Status = RequestStatus.Failed;
                            cmdRequest.Errors.Add(ex.Message);
                        }
                    }
                    else
                    {
                        cmdRequest.Status = RequestStatus.Failed;
                        cmdRequest.Errors.Add(ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Actions a request to the asset manager (asynchronously).
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// Semaphore of the asset manager request <see cref="Semaphore"/>.
        /// </returns>
        public Semaphore ActionRequestAsync(IRequest request)
        {
            throw new NotSupportedException("Only single threaded synchronous requests supported at the moment");

            /* USING SINGLE THREAD SUPPORT ONLY FOR NOW
            if (null == request)
            {
                throw new ArgumentNullException("request");
            }

            // Add incoming P4 request to queue
            if (null == request)
            {
                throw new ArgumentException("Request must be of type IP4Request");
            }
            request.Status = RequestStatus.Pending;
            this.pendingRequests.Enqueue(request);

            // Signal that queue has pending requests
            this.ProcessQueue();
            return request.Semaphore;
            */
        }

        #endregion

        /* USING SINGLE THREAD SUPPORT ONLY FOR NOW
        /// <summary>
        /// Process any pending requests in queue.
        /// </summary>
        private void ProcessQueue()
        {
            // Only spawn one instance of queue processing thread
            if (null != this.queueProcessingThread && this.queueProcessingThread.IsAlive)
            {
                return;
            }

            this.queueProcessingThread = new Thread(this.ProcessQueueWorker);
            this.queueProcessingThread.Start();
        }
        */

        /* USING SINGLE THREAD SUPPORT ONLY FOR NOW
        /// <summary>
        /// Worker method to action pending requests.
        /// </summary>
        private void ProcessQueueWorker()
        {
            // Use do-while instead of while in case
            // _pendingRequests is updated during loop
            do
            {
                IRequest request;
                if (!this.pendingRequests.TryDequeue(out request))
                {
                    return;
                }

                // Connect if required
                if (!this.IsConnected())
                {
                    if (!this.Connect())
                    {
                        request.Status = RequestStatus.Failed;
                        request.Errors.Add("Failed to connect to Perforce server");
                        return;
                    }
                }

                var cmdRequest = request as IP4RunCmdRequest;
                var changeListRequest = request as IP4NewChangeListRequest;
                var changeListUpdateRequest = request as IP4ChangeListUpdateRequest;

                if (null != cmdRequest)
                {
                    // Process P4 run command request
                    try
                    {
                        cmdRequest.RecordSet = this.Connection.Run(cmdRequest.Command, cmdRequest.Args);
                    }
                    catch (Exception ex)
                    {
                        cmdRequest.Status = RequestStatus.Failed;
                        cmdRequest.Errors.Add(ex.Message);
                    }
                }
                else if (null != changeListRequest)
                {
                    // Process P4 new change list request
                    var p4ChangeList = this.Connection.CreatePendingChangelist(changeListRequest.Description);
                    var changeList = new P4ChangeList(this.AssetManager, p4ChangeList)
                        {
                            Workspace = this.Workspace
                        };
                    changeListRequest.ChangeList = changeList;
                }
                else if (null != changeListUpdateRequest)
                {
                    // Process P4 form request
                    var form = this.Connection.Fetch_Form("change", changeListUpdateRequest.ChangeListNumber);
                    form["Description"] = changeListUpdateRequest.Description;
                    changeListUpdateRequest.RecordSet = this.Connection.Save_Form(form);
                }
                else
                {
                    request.Errors.Add("P4 request type not recognised");
                    request.Status = RequestStatus.Failed;
                }
                
                request.Semaphore.Release();
            }
            while (!this.pendingRequests.IsEmpty);
        }
        */
    }
}
