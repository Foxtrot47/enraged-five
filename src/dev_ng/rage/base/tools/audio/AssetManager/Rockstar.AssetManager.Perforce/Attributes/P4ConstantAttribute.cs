﻿// -----------------------------------------------------------------------
// <copyright file="P4ConstantAttributes.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.AssetManager.Perforce.Attributes
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Attribute that provides a way of tagging enums with String constants
    /// that appear in the Perforce API.
    /// </summary>
    public class P4ConstantAttribute
    {
        #region Constructor(s)

        /// <summary>
        /// Initializes a new instance of the <see cref="P4ConstantAttribute"/> class.
        /// </summary>
        /// <param name="constant">
        /// The constant.
        /// </param>
        public P4ConstantAttribute(string constant)
        {
            this.Constant = constant;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the constant.
        /// </summary>
        public string Constant
        {
            get;
            private set;
        }

        #endregion

        /// <summary>
        /// Return an enum value for the specified String; assuming the enum
        /// values are decorated with the PerforceConstant attribute.
        /// </summary>
        /// <typeparam name="T">
        /// The type param.
        /// </typeparam>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <param name="defaultValue">
        /// The default Value.
        /// </param>
        /// <returns>
        /// The enum value <see cref="T"/>.
        /// </returns>
        public static T GetEnumValueFromString<T>(string input, T defaultValue) 
            where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("Generic 'T' must be an enumerated type.");
            }

            var values = (T[])Enum.GetValues(typeof(T));
            foreach (var value in values)
            {
                var fi = typeof(T).GetField(value.ToString(CultureInfo.InvariantCulture));
                var attributes =
                    fi.GetCustomAttributes(typeof(P4ConstantAttribute), false) as P4ConstantAttribute[];

                if (null == attributes)
                {
                    throw new Exception("Attributes are null");
                }

                if (0 == attributes.Length)
                {
                    continue; // Skip enum value (no attribute)
                }

                var attribute = attributes.Length > 0 ? attributes[0].Constant : string.Empty;
                if (0 == string.CompareOrdinal(attribute, input))
                {
                    return value;
                }
            }

            return defaultValue;
        }
    }
}
