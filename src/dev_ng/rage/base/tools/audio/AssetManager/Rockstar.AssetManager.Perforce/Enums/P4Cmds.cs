﻿namespace Rockstar.AssetManager.Perforce.Enums
{
    public enum P4Cmds
    {
        CreateChangeList,
        RemoveChangeList,
        GetChangeList,
        GetPendingChangeLists,
        GetLatestChangeListNumber,
        MoveAsset,
        GetLatest,
        GetLatestOnFileType,
        GetPrevious,
        Integrate,
        Resolve,
        ExistsAsAsset,
        SetAssetReadOnly,
        GetWorkingPath,
        IsCheckedOut,
        IsMarkedForAdd,
        IsResolved,
        HaveLatest,
        IsReadOnly,
        LockedStatus
    }
}
