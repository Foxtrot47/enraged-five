﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssetManagerFactory.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the AssetManagerFactory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Main
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// The asset manager factory.
    /// </summary>
    public class AssetManagerFactory
    {
        /// <summary>
        /// The asset manager instances.
        /// </summary>
        private static readonly Dictionary<AssetManagerType, IAssetManager> Instances = new Dictionary<AssetManagerType, IAssetManager>();

        /// <summary>
        /// The platform code.
        /// </summary>
        private static string platformCode;

        public static IAssetManager GetInstance(
            AssetManagerType type,
            string host,
            string port,
            string workspace,
            string username,
            string password,
            string depotRoot)
        {
            return GetInstance(type, host, port, workspace, username, password, depotRoot, null);
        }


        /// <summary>
        /// Method to get an instance of an asset manager.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="host">
        /// The host.
        /// </param>
        /// <param name="port">
        /// The port.
        /// </param>
        /// <param name="workspace">
        /// The workspace.
        /// </param>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="depotRoot">
        /// The depot Root.
        /// </param>
        /// <returns>
        /// Asset manager instance. <see cref="IAssetManager"/>.
        /// </returns>
        /// <exception cref="NotSupportedException">
        /// Exception thrown if asset manager type not supported.
        /// </exception>
        public static IAssetManager GetInstance(
            AssetManagerType type,
            string host,
            string port,
            string workspace,
            string username,
            string password,
            string depotRoot,
            ILogger logger = null,
            string description = "")
        {
            switch (type)
            {
                case AssetManagerType.Perforce:
                case AssetManagerType.P4Api:
                    {
                        if (!Instances.ContainsKey(type))
                        {
                            var useLogin = !string.IsNullOrEmpty(password);
                            var p4api = new P4API.P4AssetManager(
                              host, port, workspace, username, password, depotRoot, useLogin, logger, description);

                            Instances.Add(type, p4api);
                           
                        }
                        return Instances[type];
                    }

                default:
                    {
                        throw new NotSupportedException("Asset manager type not supported: " + type);
                    }
            }
        }

        /// <summary>
        /// Get an existing instance of an asset manager of specified type.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The asset manager instance <see cref="IAssetManager"/>.
        /// </returns>
        public static IAssetManager GetInstance(AssetManagerType type)
        {
            if (!Instances.ContainsKey(type))
            {
                throw new Exception(
                    "Instance of " + type + " asset manager not found. "
                    + "Use GetInstance with connection params to create new instance.");
            }

            return Instances[type];
        }

        /// <summary>
        /// Create an instance of P4 asset manager.
        /// </summary>
        /// <param name="host">
        /// The host.
        /// </param>
        /// <param name="port">
        /// The port.
        /// </param>
        /// <param name="workspace">
        /// The workspace.
        /// </param>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="depotRoot">
        /// The depot root.
        /// </param>
        /// <param name="useLogin">
        /// The use login flag.
        /// </param>
        /// <returns>
        /// The asset manager instance <see cref="IAssetManager"/>.
        /// </returns>
        private static IAssetManager CreateP4AssetManagerInstance(
            string host, string port, string workspace, string username, string password, string depotRoot, bool useLogin, ILogger logger, string description)
        {

            // Load correct project for platform
            platformCode = (IntPtr.Size == 8) ? "64" : "86";

            var fileName = "Rockstar.AssetManager.Perforce" + platformCode + ".dll";
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);

            // Load assembly and create P4AssetManager instance
            // We use the assembly resolver for projects that run under AnyCPU, where
            // we require platform-specific versions of the P4 DLLs
            AppDomain.CurrentDomain.AssemblyResolve += AppDomain_AssemblyResolve;
            Console.WriteLine("Loading file: " + path);
            var assembly = Assembly.LoadFile(path);
            var type = assembly.GetType("Rockstar.AssetManager.Perforce.P4AssetManager");
            IAssetManager assetManager = null;
            try
            {
                assetManager = (IAssetManager)Activator.CreateInstance(type, host, port, workspace, username, password, depotRoot,
                    useLogin, logger, description);
            }
            catch (TargetInvocationException e)
            {
                throw e.InnerException;
            }
            AppDomain.CurrentDomain.AssemblyResolve -= AppDomain_AssemblyResolve;

            return assetManager;
        }

        /// <summary>
        /// Assembly resolve method to load assembly files not in default directory.
        /// If running as AnyCPU, make sure the P4API.dll and P4dn.dll files exist
        /// in relative paths /x64 and /x86 in the working directory.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The resolved assembly <see cref="Assembly"/>.
        /// </returns>
        private static Assembly AppDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            var platformTag = "x" + platformCode;

            var fileName = args.Name.Split(',')[0] + ".dll";
            var path = Path.Combine(Environment.CurrentDirectory, platformTag, fileName);

            Console.WriteLine("Loading file: " + path);

            if (File.Exists(path))
            {
                // Load assembly and create P4AssetManager instance
                return Assembly.LoadFile(path);
            }

            return null;
        }
    }
}
