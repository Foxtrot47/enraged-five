﻿// Copyright: Rockstar North 2016

using System;
using System.Collections.Generic;
using System.Linq;
using Perforce.P4;
using Rockstar.AssetManager.Interfaces;

namespace Rockstar.AssetManager.P4API
{
    public class PerforceCache : IDisposable
    {
        private readonly P4AssetManager _assetManager;

        private PerforceCache(P4AssetManager assetManager, IEnumerable<string> locations)
        {
            _assetManager = assetManager;
            PopulateCache(locations);
        }


        /// <summary>
        /// Perforce cache to be used in a using satatement
        /// using(PerforceCache.Instantiate(assetmanager, locations)
        /// {//list of methods}
        /// </summary>
        /// <param name="assetManager">P4 Asset manager</param>
        /// <param name="locations">a list of locations</param>
        public static PerforceCache Instantiate(P4AssetManager assetManager, IEnumerable<string> locations)
        {
            assetManager.PerforceCache = new PerforceCache(assetManager, locations);
            return assetManager.PerforceCache;
        }

        private void PopulateCache(IEnumerable<string> locations)
        {
            List<FileMetaData> metadataList = new List<FileMetaData>();
            foreach (string location in locations)
            {
                var files = _assetManager.Repository.GetFileMetaData(null, _assetManager.GetFileSpec(location));
                if (files?.Count > 0)
                {
                    metadataList.AddRange(files);
                }
            }

            FileMetaDataCache = metadataList.ToDictionary(p => p.LocalPath.Path.ToLower(), r => r);
            DepotToLocalPath = metadataList.ToDictionary(p => p.DepotPath.Path.ToLower(), r => r.LocalPath.Path.ToLower());
        }

        public Dictionary<string, string> DepotToLocalPath { get; set; }

        private Dictionary<string, FileMetaData> FileMetaDataCache { get; set; }

        public void Dispose()
        {
            _assetManager.InvalidateCache();
        }

        public FileMetaData GetCache(string path)
        {
            if (FileMetaDataCache == null)
            {
                return null;
            }

            bool isLocal = path.IsLocalPath();

            string pathToLower = path.ToLower();


            if (isLocal)
            {
                if (FileMetaDataCache.ContainsKey(pathToLower))
                {
                    return FileMetaDataCache[pathToLower];
                }
            }
            else
            {
                if (!DepotToLocalPath.ContainsKey(pathToLower))
                {
                    return null;
                }
                string localPath = DepotToLocalPath[pathToLower];
                if (FileMetaDataCache.ContainsKey(localPath))
                {
                    return FileMetaDataCache[localPath];
                }
            }

            return null;
        }


    }
}