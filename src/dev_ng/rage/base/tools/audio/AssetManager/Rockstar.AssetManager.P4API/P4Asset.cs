﻿// Copyright: Rockstar North 2016

#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Perforce.P4;
using Rockstar.AssetManager.Infrastructure.Data;
using Rockstar.AssetManager.Interfaces;
using File = System.IO.File;

#endregion

namespace Rockstar.AssetManager.P4API
{
    /// <summary>
    /// The P4 asset.
    /// </summary>
    public class P4Asset : IAsset
    {
        private const int Retry = 10;
        private IChangeList _changeList;
        private string _depotPath;
        private string _localPath;

        public FileSpec FileSpec
        {
            get
            {
                return
                    new FileSpec(new DepotPath(DepotPath), null, new LocalPath(LocalPath), null);
            }
            private set
            {
                if (value.DepotPath != null)
                {
                    DepotPath = value.DepotPath.Path;
                }
                if (value.DepotPath != null)
                {
                    LocalPath = value.LocalPath.Path;
                }
            }
        }

        public FileStatus FileStatus => P4AssetManager.GetFileStatus(DepotPath);

        private P4AssetManager P4AssetManager
        {
            get { return AssetManager as P4AssetManager; }
        }

        internal P4Asset(FileMetaData fileMeta, IAssetManager assetManager, IChangeList changelist)
        {
            if (fileMeta.LocalPath != null)
            {
                LocalPath = fileMeta.LocalPath.Path;
            }
            if (fileMeta.DepotPath != null)
            {
                DepotPath = fileMeta.DepotPath.Path;
            }
            Action = fileMeta.Action.ToString();

            Revision = fileMeta.HeadRev;
            AssetManager = assetManager;
            _changeList = changelist;

        }

        public IList<RevisionInfo> GetHistory(int maxHistoryLength = 100, int latestChangelist = 0, int oldestRevision = 0, int newestRevision = int.MaxValue)
        {
            FileSpec filespec = this.FileSpec.StripVersion();
            filespec.Version = new VersionRange(oldestRevision, newestRevision);
            IList<FileHistory> histories = RunWithRetry(() =>
                P4AssetManager.Repository.GetFileHistory(
                    new GetFileHistoryCmdOptions(GetFileHistoryCmdFlags.IncludeInherited, latestChangelist, maxHistoryLength), filespec),
                    "Could not get history for asset");
            return histories.Select(p => p.ToRevisionInfo()).ToList();
        }


        public RevisionInfo GetRevisionInfo(int revision = 0)
        {
            if (revision == 0)
            {
                revision = (int)Revision;
            }

            IList<RevisionInfo> history = GetHistory(1, 0, revision, revision);

            if (history?.Count > 0)
            {
                return history[0];
            }

            return null;
        }







        public bool Checkout(bool lockAsset)
        {
            GetLatest(true);
            RunWithRetry(() => P4AssetManager.Client.EditFiles(new Options(), FileSpec),
                $"Could not check out {DepotPath}");
            if (lockAsset)
            {
                RunWithRetry(() => P4AssetManager.Client.LockFiles(new Options(), FileSpec),
                    $"Could lock {DepotPath}");
            }
            return true;
        }

        public bool GetLatest(bool force)
        {
            return
                RunWithRetry(() => P4AssetManager.Client.SyncFiles(
                    new SyncFilesCmdOptions(force ? SyncFilesCmdFlags.Force : SyncFilesCmdFlags.None, 1), FileSpec)
                    .Any(), $"Could not get latest on {DepotPath}");
        }

        public bool GetPrevious(bool force)
        {
            return RunWithRetry(() => P4AssetManager.GetPrevious(LocalPath, force),
                $"Could not get previous on {DepotPath}");
        }

        public bool IsReadonly()
        {
            if (!File.Exists(LocalPath))
            {
                throw new FileNotFoundException(LocalPath);
            }

            return new FileInfo(LocalPath).IsReadOnly;
        }

        public bool Lock()
        {
            RunWithRetry(() => P4AssetManager.Client.LockFiles(new Options(), FileSpec), $"Could not lock {DepotPath}");
            return true;
        }

        public bool MarkForAdd()
        {
            RunWithRetry(() => P4AssetManager.Client.AddFiles(new Options(), FileSpec),
                $"Could not mark {DepotPath} for add");
            return true;
        }

        public bool MarkForAdd(string type)
        {
            RunWithRetry(() => ChangeList.MarkAssetForAdd(LocalPath, type),
                $"Could not mark {DepotPath} for add as {type}");
            return true;
        }

        public bool MarkForDelete()
        {
            RunWithRetry(() => P4AssetManager.Client.DeleteFiles(null, FileSpec),
                $"Could not mark {DepotPath} for delete");
            return true;
        }

        public bool Move(IChangeList newChangeList)
        {
            RunWithRetry(
                () =>
                    P4AssetManager.Client.ReopenFiles(new ReopenCmdOptions((int)newChangeList.Number, null), FileSpec),
                $"Could not move {DepotPath}");
            return true;
        }

        public bool Move(uint newChangeListNumber)
        {
            RunWithRetry(
                () => P4AssetManager.Client.ReopenFiles(new ReopenCmdOptions((int)newChangeListNumber, null), FileSpec),
                $"Could not move {DepotPath} to {newChangeListNumber}");
            return true;
        }

        public bool MoveAsset(string newFilePath)
        {
            FileSpec newFileSpec = new FileSpec();
            if (newFilePath.IsLocalPath())
            {
                newFileSpec.LocalPath = new LocalPath(newFilePath);
            }
            else
            {
                newFileSpec.DepotPath = new DepotPath(newFilePath);
            }
            RunWithRetry(() => P4AssetManager.Client.MoveFiles(FileSpec, newFileSpec, null),
                $"Could not move asset to {newFilePath}");
            return true;
        }

        public bool Revert(bool removeFromChangeList = true)
        {
            RunWithRetry(() => P4AssetManager.Client.RevertFiles(new Options(), FileSpec),
                "Could not revert asset {DepotPath}");
            return true;
        }

        public bool SetReadonly(bool flag)
        {
            if (!File.Exists(LocalPath))
            {
                throw new FileNotFoundException(LocalPath);
            }

            FileAttributes currAttrs = File.GetAttributes(LocalPath);
            if (flag)
            {
                File.SetAttributes(LocalPath, currAttrs & FileAttributes.ReadOnly);
            }
            else
            {
                File.SetAttributes(LocalPath, currAttrs & ~FileAttributes.ReadOnly);
            }

            return true;
        }

        public string Action { get; set; }

        public IAssetManager AssetManager { get; }

        public IChangeList ChangeList
        {
            get
            {
                if (_changeList == null)
                {
                    _changeList =
                        AssetManager.ChangeLists.Values.FirstOrDefault(
                            p => p.AssetLocalPaths.Contains(LocalPath, StringComparer.CurrentCultureIgnoreCase) ||
                                 p.AssetDepotPaths.Contains(DepotPath, StringComparer.CurrentCultureIgnoreCase));
                }
                return _changeList;
            }
            set { _changeList = value; }
        }

        public string DepotPath
        {
            get
            {
                if (string.IsNullOrEmpty(_depotPath))
                {
                    return LocalPath.ToDepotPath(AssetManager as P4AssetManager);
                }
                return _depotPath;
            }
            set { _depotPath = value; }
        }

        public int Revision { get; set; }

        public string LocalPath
        {
            get
            {
                if (string.IsNullOrEmpty(_localPath))
                {
                    return DepotPath.ToLocalPath(AssetManager as P4AssetManager);
                }
                return _localPath;
            }
            set { _localPath = value; }
        }

        #region Private Methods

        private T RunWithRetry<T>(Func<T> func, string exceptionMessage)
        {
            return CommandRunner.RunWithRetry(func, () => AssetManager.Connect(), exceptionMessage, Retry);
        }

        private void RunWithRetry(Action func, string exceptionMessage)
        {
            CommandRunner.RunWithRetry(func, () => AssetManager.Connect(), exceptionMessage, Retry);
        }

        #endregion
    }
}