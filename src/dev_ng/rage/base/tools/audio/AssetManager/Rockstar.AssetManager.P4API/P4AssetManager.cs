﻿// Copyright: Rockstar North 2016

#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Text;
using Perforce.P4;
using Rockstar.AssetManager.Infrastructure.Data;
using Rockstar.AssetManager.Interfaces;
using File = Perforce.P4.File;
using ResolveAction = Rockstar.AssetManager.Infrastructure.Enums.ResolveAction;

#endregion

namespace Rockstar.AssetManager.P4API
{
    /// <summary>
    /// The P4 asset manager.
    /// </summary>
    public class P4AssetManager : IAssetManager
    {
        private static readonly string DefaultDescription = "New Changelist";
        private Connection _connection;
        private Credential _credential;
        private int _retry = 10;
        public Action<string[]> CannotClobberFiles;

        public Func<string> GetNewPassword;
        private Dictionary<string, IChangeList> _changeLists;
        public Client Client { get; set; }
        public Repository Repository { get; set; }


        public PerforceCache PerforceCache = null;

        public int Retry
        {
            get { return _retry; }
            set { _retry = value > 0 ? value : 1; }
        }

        public P4AssetManager(
            string host, string port, string workspace, string username, string password, string depotRoot,
            bool useLogin, ILogger logger, string description)
        {
            Host = host;
            Port = port;
            Workspace = workspace;
            Username = username;
            Password = password;
            DepotRoot = depotRoot;

            UseLogin = useLogin;

            if (useLogin && string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Password required if useLogin is enabled");
            }


            ChangeLists = new Dictionary<string, IChangeList>();


            if (!Connect())
            {
                throw new Exception("Failed to connect to Perforce");
            }

            ClientMetadata metadata = RunWithRetry(() =>
                 Repository.GetClientMetadata(), "Could not get client information");
            try
            {
                Root = metadata.Root.Replace("/", "\\").TrimEnd('\\');
            }
            catch
            {
                throw new Exception($"Could not determine root of workspace: {Workspace}");
            }

            //TODO: implement and call LoadClientViews

            LoadChangeLists();
        }


        public void InvalidateCache()
        {
            PerforceCache = null;
        }


        public bool Connect()
        {
            for (int i = 0; i < Retry; i++)
            {
                try
                {
                    ServerAddress serverAddress = null;
                    if (Port.Contains(":"))
                    {
                        serverAddress = new ServerAddress(Port);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Host) && !string.IsNullOrEmpty(Port))
                        {
                            serverAddress = new ServerAddress(string.Format("{0}:{1}", Host, Port));
                        }
                    }


                    Server server = new Server(serverAddress);
                    Repository = new Repository(server);
                    _connection = Repository.Connection;

                    // use the connection variables for this connection
                    _connection.UserName = Username;

                    _connection.Client = new Client();
                    _connection.Client.Name = Workspace;
                    _connection.CommandTimeout = TimeSpan.FromHours(1.0);

                    if (!_connection.Connect(null))
                    {
                        return false;
                    }

                    _credential = string.IsNullOrEmpty(Password)
                        ? _connection.Login(null, new LoginCmdOptions(LoginCmdFlags.DisplayStatus, null))
                        : _connection.Login(Password);

                    while (_credential == null && GetNewPassword != null && i < Retry)
                    {
                        Password = GetNewPassword();
                        try
                        {
                            _credential = _connection.Login(Password);
                        }
                        catch
                        {
                            // will try to get password in next try else there will be a connection error.
                        }

                        i++;
                    }

                    if (_credential == null)
                    {
                        return false;
                    }


                    Client = _connection.Client;
                    return true;
                }
                catch (Exception e)
                {
                    if (i == Retry - 1)
                    {
                        if (_credential?.Ticket == null)
                        {
                            throw new Exception(
                                "Could not connect to Perforce, your password might be invalid, please contact IT.", e);
                        }
                        throw new Exception("Could not connect to Perforce", e);
                    }
                }
            }
            throw new Exception("Could not connect to Perforce");
        }


        public IChangeList CreateChangeList()
        {
            return CreateChangeList(DefaultDescription);
        }

        public IChangeList CreateChangeList(string description)
        {
            return P4ChangeList.CreateNew(this, description);
        }

        public bool DeleteChangeList(IChangeList changeList)
        {
            P4ChangeList p4Changelist = changeList as P4ChangeList;
            if (p4Changelist == null)
            {
                return false;
            }
            RunWithRetry(() => Repository.DeleteChangelist(p4Changelist.GetChangelist(), null),
                "Could not delete changelist");

            //Remove from changelists dictionary
            var changelist = ChangeLists.FirstOrDefault(p => p.Value == changeList).Key;
            if (!string.IsNullOrEmpty(changelist))
            {
                ChangeLists.Remove(changelist);
            }

            return true;
        }

        public void Disconnect()
        {
            RunWithRetry(() => _connection.Disconnect(), "Could not disconnect");
        }


        public bool ExistsAsAsset(string path, bool includeDeleted = false)
        {
            if (!Path.HasExtension(path))
            {
                return false;
            }
            FileSpec filespec = GetFileSpec(path);
            try
            {
                IList<File> files = Repository.GetFiles(
                    new GetDepotFilesCmdOptions(
                        !includeDeleted ? GetDepotFilesCmdFlags.NotDeleted : GetDepotFilesCmdFlags.None, 1),
                   filespec);
            return files != null && files.Count > 0;
        }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool ExistsInPendingChangeList(string path)
        {
            return ChangeLists.Any(changeList => null != changeList.Value.GetAsset(path));
        }

        public IChangeList GetChangeList(string changeListNumber, bool allowAllUsers = false)
        {
            if (ChangeLists.ContainsKey(changeListNumber))
            {
                return ChangeLists[changeListNumber];
            }

            if (changeListNumber == "default")
            {
                changeListNumber = "0";
            }
            return GetChangeList(uint.Parse(changeListNumber), allowAllUsers);
        }

        public IList<RevisionInfo> GetRevisionHistory(string path, int maxHistoryLength = 100, int latestChangelist = 0, int oldestRevision = 0, int newestRevision = int.MaxValue)
        {
            FileSpec filespec = GetFileSpec(path);
            if (filespec == null)
            {
                return new List<RevisionInfo>();
            }
            filespec.Version = new VersionRange(oldestRevision, newestRevision);
            IList<FileHistory> histories = RunWithRetry(() =>
               Repository.GetFileHistory(
                   new GetFileHistoryCmdOptions(GetFileHistoryCmdFlags.IncludeInherited, latestChangelist, maxHistoryLength), filespec), $"Could not get file history for {path}");
            return histories.Select(p => p.ToRevisionInfo()).ToList();
        }


        public RevisionInfo GetRevisionInfo(string path, int revision = 0)
        {
            if (revision == 0)
            {
                FileSpec fileSpec = GetFileSpec(path);
                GetDepotFilesCmdOptions opts =
    new GetDepotFilesCmdOptions(GetDepotFilesCmdFlags.None, 0);
                IList<File> files = RunWithRetry(() => Repository.GetFiles(opts, fileSpec), $"Could not get file {path} from depot");

                if (files.Count > 0)
                {
                    revision = ((Revision)(files[0]).Version).Rev;
                }

            }

            IList<RevisionInfo> history = GetRevisionHistory(path, 1, 0, revision, revision);

            if (history?.Count > 0)
            {
                return history[0];
            }

            return null;
        }

        public IChangeList GetChangeList(uint changeListNumber, bool allowAllUsers = false)
        {
            IChangeList changeList = P4ChangeList.GetChangeList(changeListNumber, this);
            if (!allowAllUsers)
            {
                if (changeList.Username != Username)
                {
                    return null;
                }
            }
            return changeList;
        }

        public IList<string> GetChangeListFiles(string changeListNumber, bool exludeDeleted = false)
        {
            return GetChangeList(changeListNumber, true).AssetDepotPaths.ToList();
        }

        public IAsset GetCheckedOutAsset(string path)
        {
            RefreshView();
            return ChangeLists.Select(
                changeList => changeList.Value.GetAsset(path)).FirstOrDefault(asset => null != asset);
        }

        public string GetDepotPath(string path)
        {
            string localRoot = Client.Root.Replace('/', '\\').EndsWith("\\")
                ? Client.Root.Replace('/', '\\')
                : Client.Root.Replace('/', '\\') + "\\";


            IList<FileSpec> preDepot =
                 RunWithRetry(() => Repository.GetDepotFiles(new List<FileSpec> { new FileSpec { DepotPath = new DepotPath(path) } },
                    null), $"Could not get depot path for {path}")
                    ;
            if (preDepot != null && preDepot.Count > 0)
            {
                return path;
            }
            path = path.Replace('/', '\\');
            if (!path.StartsWith(localRoot, StringComparison.InvariantCultureIgnoreCase))
            {
                if (path.StartsWith("\\"))
                {
                    path = path.TrimStart('\\');
                }
                path = path.Insert(0, localRoot);
            }

            if (!Path.HasExtension(path))
            {
                if (!path.EndsWith("\\"))
                {
                    path += "\\";
                }
                path += "...";
            }
            IList<FileSpec> depotfile =
                RunWithRetry(() =>
                    Repository.GetDepotFiles(
                        new List<FileSpec> { new FileSpec { LocalPath = new LocalPath(path) } },
                        new GetDepotFilesCmdOptions(GetDepotFilesCmdFlags.AllRevisions, 1)), $"Could not get depot path for {path}");
            if (depotfile != null && depotfile.Count > 0)
            {
                if (path.EndsWith("..."))
                {
                    string directoryName = Path.GetDirectoryName(depotfile[0].DepotPath.Path).Replace("\\", "/");
                    var pathSplit = path.Split('\\');
                    var beforeLast = pathSplit.Count() - 2;
                    var originalDirectoryName = pathSplit[beforeLast];
                    var directorySplit = directoryName.Split('/').Where(p => !string.IsNullOrEmpty(p));
                    if (!directorySplit.Last().Equals(originalDirectoryName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        var directorySplitArray = directorySplit.ToArray();
                        var directoryToReturn = "//";
                        for (int i = 0; i < directorySplit.Count(); i++)
                        {
                            directoryToReturn += directorySplitArray[i] + "/";
                            if (directorySplitArray[i].Equals(originalDirectoryName, StringComparison.CurrentCultureIgnoreCase))
                            {
                                return directoryToReturn;
                            }

                        }
                    }


                    if (directoryName != null)
                        return directoryName.Replace("\\", "/");
                }
                return depotfile[0].DepotPath.Path;
            }

            throw new Exception($"Could not get depot path for {path}");
        }

        private FileMetaData GetCache(string path)
        {
            return PerforceCache?.GetCache(path);
        }

        public string GetUserEmailAddress(string userName)
        {
            return Repository.GetUser(userName).EmailAddress;
        }

        public FileStatus GetFileStatus(string path)
        {
            FileMetaData fileMetaData = GetCache(path);
            if (fileMetaData == null)
            {
                IList<FileMetaData> fmds =
                    RunWithRetry(() =>
                        Repository.GetFileMetaData(null, GetFileSpec(path)), $"Could not get file status for {path}");

                if (fmds == null || fmds.Count == 0)
                {
                    return null;
                }
                fileMetaData = fmds[0];
                if (fileMetaData == null)
                {
                    return null;
                }
            }


            bool isOpen = fileMetaData.OtherOpen > 0 || ChangeLists.Values.Select(
                    p => p.Assets)
                    .SelectMany(z => z)
                    .Any(a => a.LocalPath.Equals(path, StringComparison.InvariantCultureIgnoreCase) ||
                                         a.DepotPath.Equals(path, StringComparison.CurrentCultureIgnoreCase));

            string user = fileMetaData.OtherLockUserClients != null && fileMetaData.OtherLockUserClients.Count > 0
                ? fileMetaData.OtherLockUserClients[0].Split('@')[0]
                : fileMetaData.ActionOwner;

            FileStatus fs = new FileStatus(isOpen, fileMetaData.OtherLock | fileMetaData.OurLock, fileMetaData.OurLock,
                user, fileMetaData.HeadModTime);

            return fs;
        }

        public bool GetLatest(IAsset asset, bool force)
        {
            RunWithRetry(() =>
                Client.SyncFiles(
                    new SyncFilesCmdOptions(force ? SyncFilesCmdFlags.Force : SyncFilesCmdFlags.None, 1),
                    GetFileSpec(asset)), $"Couldn't get latest on {asset.LocalPath}");
            return true;
        }

        public int GetNumberOfRevisions(string path)
        {
            return GetRevisionHistory(path).Count;
        }

        public bool GetLatest(IAsset asset, bool force, string changeListNumber)
        {
            if (string.IsNullOrEmpty(changeListNumber)) return GetLatest(asset, force);
            VersionSpec changeListLevel = new ChangelistIdVersion(int.Parse(changeListNumber));
            FileSpec fs = GetFileSpec(asset);
            fs.Version = changeListLevel;
            RunWithRetry(() =>
                Client.SyncFiles(
                    new SyncFilesCmdOptions(force ? SyncFilesCmdFlags.Force : SyncFilesCmdFlags.None, 1),
                    fs), string.Format("Could not get revision with changelist {1} on path {0}", asset.LocalPath,
                        changeListNumber));
            return true;
        }


        public bool GetLatest(string path, bool force)
        {
            SyncFilesCmdFlags flags = force ? SyncFilesCmdFlags.Force : SyncFilesCmdFlags.None;
            return GetLatest(path, flags);
        }

        public bool GetLatest(string path, bool force, string changeListNumber)
        {
            if (string.IsNullOrEmpty(changeListNumber)) return GetLatest(path, force);
            VersionSpec changeListLevel = new ChangelistIdVersion(int.Parse(changeListNumber));
            FileSpec fs = GetFileSpec(path);
            fs.Version = changeListLevel;
            RunWithRetry(() => Client.SyncFiles(
                new SyncFilesCmdOptions(force ? SyncFilesCmdFlags.Force : SyncFilesCmdFlags.None, -1),
                fs), string.Format("Could not get revision with changelist {1} on path {0}", path,
                    changeListNumber));
            return true;
        }

        public uint GetLatestChangeListNumber()
        {
            ChangesCmdOptions opts = new ChangesCmdOptions(ChangesCmdFlags.FullDescription, null, 1,
                ChangeListStatus.Submitted, null);

            return RunWithRetry(() => (uint)Repository.GetChangelists(opts, null)[0].Id,
                "Could not get latest changelist number");
        }

        public DateTime GetLatestCheckinTime(string path)
        {
            return RunWithRetry(() => Repository.GetFileMetaData(null, GetFileSpec(path))[0].HeadTime,
                "Could not get latest checkin time on path: " + path);
        }

        public bool GetLatestForceErrors(string path)
        {
            CommandRunner.RunWithRetry(() =>
                Client.SyncFiles(
                    null,
                    GetFileSpec(path)), () => GetLatest(path, true), $"Couldn't get latest on {path}", 2);
            return true;
        }

        public bool GetLatestOnFileType(string path, bool force, string fileType)
        {
            if (path.EndsWith("..."))
            {
                return GetLatest(path + fileType, force);
            }
            throw new Exception("Path needs to end with ... to get latest with filetype");
        }

        public string GetLocalPath(string path)
        {
            if (PerforceCache != null && PerforceCache.DepotToLocalPath.ContainsKey(path.ToLower()))
            {
                return PerforceCache.DepotToLocalPath[path.ToLower()];
            }


            if (!path.IsLocalPath())
            {
                if (string.IsNullOrEmpty(path))
                {
                    return string.Empty;
                }
                path = FixPath(path);


                GetFileMetaDataCmdOptions opts =
                    new GetFileMetaDataCmdOptions(GetFileMetadataCmdFlags.None,
                        null, null, 1, null, null, null);
                FileSpec filespec = GetFileSpec(path.Trim().ToLower());
                IList<FileMetaData> files = null;
                var file = GetCache(path);
                if (file != null)
                {
                    files = new[] { file };
                }
                else
                {
                    files = RunWithRetry(() => Repository.GetFileMetaData(opts, filespec), $"Could not get metadata for path {path}");

                }

                if (files == null || files.Count == 0)
                {
                    string localRoot = Client.Root.Replace('/', '\\').EndsWith("\\")
                ? Client.Root.Replace('/', '\\')
                 : Client.Root.Replace('/', '\\') + "\\";
                    return Path.Combine(localRoot,
                        path.Replace('/', '\\').TrimStart('\\'));
                }
                if (Path.HasExtension(path))
                {
                    return files[0].LocalPath.Path;
                }
                else
                {
                    var directoryName = path.Split('/').Last(p => !string.IsNullOrEmpty(p)).ToLower();
                    var directoryIndex = files[0].LocalPath.Path.ToLower().IndexOf(directoryName);
                    var directoryPath = files[0].LocalPath.Path.Substring(0,
                        directoryIndex + directoryName.Length + 1);
                    return directoryPath;
                }
            }
            return path;


        }


        public bool GetPrevious(string path, bool force)
        {

            FileSpec fileSpec = GetFileSpec(path);

            IList<FileMetaData> files = null;
            var file = GetCache(path);
            if (file != null)
            {
                files = new[] { file };
            }
            else
            {
                files = RunWithRetry(() => Repository.GetFileMetaData(null, fileSpec), $"Could not get file meta data on {path}");

            }

            if (files[0].HaveRev > 1)
            {
                return GetRevision(path, (uint)(files[0].HaveRev - 1), force);
            }
            return false;


        }

        public bool GetRevision(string path, uint revision, bool force = false)
        {

            FileSpec fileSpec = GetFileSpec(path);
            fileSpec.Version = new Revision((int)revision);
            RunWithRetry(() => Client.SyncFiles(
                new SyncFilesCmdOptions(force ? SyncFilesCmdFlags.Force : SyncFilesCmdFlags.None, 1),
                fileSpec), string.Format("Could not get revision {0} for path {1}", revision, path));
            return true;

        }

        public DateTime GetTimeOfLastSubmitToDir(string path)
        {

            if (Path.HasExtension(path))
            {
                throw new Exception("Path cannot be a file");
            }

            ChangesCmdOptions opts = new ChangesCmdOptions(ChangesCmdFlags.FullDescription, null, 1,
                ChangeListStatus.Submitted, null);

            return RunWithRetry(() => Repository.GetChangelists(opts, GetFileSpec(path))[0].ModifiedDate,
                $"Could not get time of last submit to dir {path}");
        }

        public bool HaveLatest(string path)
        {

            if (path.IsLocalPath())
            {
                path = GetDepotPath(path);
            }
            FileSpec fileSpec = GetFileSpec(path);
            IList<FileMetaData> files = null;
            var cachedFile = GetCache(path);
            if (cachedFile != null)
            {
                files = new[] { cachedFile };
            }
            else
            {
                files = RunWithRetry(() => Repository.GetFileMetaData(null, fileSpec), $"Could not check if have latest on {path}");

            }

            foreach (FileMetaData file in files)
            {
                if (file.HaveRev != file.HeadRev &&
                    !(file.HeadAction.Equals(FileAction.Delete) && file.HaveRev == -1))
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsCheckedOut(string path, bool safeCheck = false)
        {
            var cachedMetadata = GetCache(path);
            if (cachedMetadata != null)
            {
                var filestatus = GetFileStatus(cachedMetadata);

                if (safeCheck)
                {
                    return filestatus.IsLockedByMe;
                }
                return filestatus.IsOpen && filestatus.Owner?.ToLower() == Username.ToLower();
            }
            FileSpec fs = GetFileSpec(path);

            GetOpenedFilesOptions opts =
            new GetOpenedFilesOptions(GetOpenedFilesCmdFlags.AllClients,
            null, Client.Name, Username, 1);

            IList<File> target = RunWithRetry(() => Repository.GetOpenedFiles(new List<FileSpec> { fs }, opts), $"Could not check if {path} is checked out");


            if (target == null || target.Count == 0)
            {
                return false;
            }
            File file = target[0];

            if (file == null)
            {
                return false;
            }

            if (safeCheck)
            {
                IList<FileMetaData> metadata = RunWithRetry(() => Repository.GetFileMetaData(null, fs), $"Could not check if {path} is locked by me");
                return (metadata != null) && (metadata.Count > 0) && GetFileStatus(metadata[0]).IsLockedByMe;
            }

            return true;
        }

        private void UpdateChangelists(Dictionary<string, IChangeList> changeLists)
        {
            List<string> changelistsToRemove = new List<string>();
            foreach (KeyValuePair<string, IChangeList> changeList in changeLists)
            {
                var p4ChangeList = changeList.Value as P4ChangeList;
                if (p4ChangeList == null)
                {
                    changelistsToRemove.Add(changeList.Key);
                    continue;
                }
                if (!p4ChangeList.IsLocalPending())
                {
                    changelistsToRemove.Add(changeList.Key);
                }
            }
            foreach (string changelistNumber in changelistsToRemove)
            {
                changeLists.Remove(changelistNumber);
            }
        }

        public bool IsConnected()
        {
            return _connection.Status.Equals(ConnectionStatus.Connected);
        }

        public bool IsMarkedForAdd(string path)
        {
            if (Path.IsPathRooted(path) && !System.IO.File.Exists(path))
            {
                return false;
            }

            try
            {
                var fileMetaData = GetCache(path) ?? RunWithRetry(() =>
                    Repository.GetFileMetaData(null, GetFileSpec(path))[0],
                    $"Could not check if is mark for add on {path}");

                return fileMetaData.Action.HasFlag(FileAction.Add);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsReadOnly(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException(nameof(path));
            }

            return new FileInfo(path).IsReadOnly;
        }

        public bool IsResolved(string path)
        {
            FileMetaData fmd = RunWithRetry(() =>
                Repository.GetFileMetaData(null, GetFileSpec(path))[0], $"Could not check if is resolved {path}");

            return !fmd.Action.HasFlag(FileAction.Unresolved);
        }

        public Dictionary<string, IChangeList> LoadChangeLists()
        {
            Dictionary<string, IChangeList> changelistDictionary = new Dictionary<string, IChangeList>();
            ChangesCmdOptions opts = new ChangesCmdOptions(ChangesCmdFlags.FullDescription, Client.Name, 1000,
                ChangeListStatus.Pending, Username);

            Changelist defaultChange = RunWithRetry(() => Repository.GetChangelist(0, null),
                "Cannot get default changelist");

            if (defaultChange != null)
            {
                changelistDictionary["default"] = P4ChangeList.GetChangeList((uint)defaultChange.Id, this);
            }

            IList<Changelist> changelists = RunWithRetry(() => Repository.GetChangelists(opts, null),
                "Could not get changelists");

            if (changelists != null && changelists.Count > 0)
            {
                foreach (Changelist changelist in changelists)
                {
                    changelistDictionary[changelist.Id.ToString()] = P4ChangeList.GetChangeList(
                        (uint)changelist.Id, this);
                }
            }
            ChangeLists = changelistDictionary;
            return changelistDictionary;
        }

        public bool LoadClientViews()
        {
            throw new NotImplementedException();
        }

        public bool MoveAsset(IAsset asset, IChangeList newChangeList)
        {
            if (null == asset)
            {
                throw new ArgumentNullException(nameof(asset));
            }

            if (null == newChangeList)
            {
                throw new ArgumentNullException(nameof(newChangeList));
            }

            FileSpec filespec = GetFileSpec(asset);

            RunWithRetry(() => Client.ReopenFiles(new ReopenCmdOptions((int)newChangeList.Number, null), filespec),
                $"Cannot reopen files in changelist {newChangeList}");
            return true;
        }

        public bool Reconnect()
        {
            _connection.Disconnect();
            return Connect();
        }

        public void RefreshView()
        {
            LoadChangeLists();
        }

        public bool Resolve(string path, ResolveAction resolveAction)
        {
            ResolveFilesCmdFlags resolve;
            switch (resolveAction)
            {
                case ResolveAction.AcceptAuto:
                    resolve = ResolveFilesCmdFlags.AFlags;
                    break;
                case ResolveAction.AcceptMerged:
                    resolve = ResolveFilesCmdFlags.AutomaticForceMergeMode;
                    break;
                case ResolveAction.AcceptTheirs:
                    resolve = ResolveFilesCmdFlags.AutomaticTheirsMode;
                    break;
                case ResolveAction.AcceptYours:
                    resolve = ResolveFilesCmdFlags.AutomaticYoursMode;
                    break;
                default:
                    {
                        throw new Exception("Resolve action not supported: " + resolveAction);
                    }
            }

            RunWithRetry(() => Client.ResolveFiles(new ResolveCmdOptions(resolve, -1), GetFileSpec(path)),
                "Resolve action didn't work");
            return true;
        }

        public Dictionary<string, IChangeList> ChangeLists
        {
            get
            {
                if (PerforceCache == null)
                {
                UpdateChangelists(_changeLists);
                }
                return _changeLists;
            }
            private set { _changeLists = value; }
        }

        public string DepotRoot { get; }
        public string Host { get; }
        public string Password { get; private set; }
        public string Port { get; }
        public IRequestHandler RequestHandler { get; private set; }
        public string Root { get; private set; }
        public bool UseLogin { get; }
        public string Username { get; }
        public string Workspace { get; }

        #region Public methods

        public void CopyFile(string depotfile, string targetLocation, int changelist)
        {
            FileSpec fileSpec = new FileSpec(new DepotPath(depotfile), new ChangelistIdVersion(changelist));
            Options opts = new GetFileContentsCmdOptions(GetFileContentsCmdFlags.None, targetLocation);
            Repository.GetFileContents(opts, fileSpec);
        }


        public string FixPath(string path)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (!path.ToLower().StartsWith(DepotRoot.ToLower()))
            {
                stringBuilder.Append(DepotRoot);
            }
            else
            {
                stringBuilder.Append("//");
            }
            bool isChar = false;
            for (int i = 0; i < path.Length; i++)
            {
                if (char.IsLetterOrDigit(path[i]) && !isChar)
                {
                    isChar = true;
                }
                if (isChar)
                {
                    stringBuilder.Append(path[i]);
                }
            }
            return stringBuilder.Replace('\\', '/').ToString();
        }

        #endregion

        #region Private Methods

        private T RunWithRetry<T>(Func<T> func, string exceptionMessage)
        {
            return CommandRunner.RunWithRetry(func, () => Connect(), exceptionMessage, Retry);
        }

        private void RunWithRetry(Action func, string exceptionMessage)
        {
            CommandRunner.RunWithRetry(func, () => Connect(), exceptionMessage, Retry);
        }

        public IEnumerable<IAsset> GetAssets(string path)
        {
            IEnumerable<IAsset> files = RunWithRetry( ()=> Repository.
            GetFileMetaData(null, GetFileSpec(path)).
            Where(f=> f.HeadAction != FileAction.Delete  && f.HeadAction != FileAction.DeleteFrom &&
            f.HeadAction != FileAction.DeleteInto && f.HeadAction != FileAction.MoveDelete).
            Select(file => file.ToIAsset(this, null)), $"Could not get assets from {path}");
            return files;
        } 

        internal FileSpec GetFileSpec(string path)
        {
            FileSpec filespec;

            if (!Path.HasExtension(path) && !path.EndsWith("..."))
            {
                if (path.Contains("\\") && !path.EndsWith("\\"))
                {
                    path += "\\";
                }
                else if (path.Contains("/") && !path.EndsWith("/"))
                {
                    path += "/";
                }
                path = path + "...";
            }

            if (path.IsLocalPath())
            {
                filespec = FileSpec.LocalSpec(path);
            }
            else
            {

                path = FixPath(path);

                filespec = FileSpec.DepotSpec(path);
            }
            return filespec;
        }

        private static FileSpec GetFileSpec(IAsset asset)
        {
            FileSpec filespec = new FileSpec(new DepotPath(asset.DepotPath), null, new LocalPath(asset.LocalPath), null);
            return filespec;
        }

        private static FileStatus GetFileStatus(FileMetaData fileMetaData)
        {
            bool isOpen = fileMetaData.OtherOpen > 0 ||
                fileMetaData.Action.HasFlag(FileAction.Add) || fileMetaData.Action.HasFlag(FileAction.Delete) ||
                          fileMetaData.Action.HasFlag(FileAction.Edit);
            string user = fileMetaData.OtherLockUserClients != null && fileMetaData.OtherLockUserClients.Count > 0
                ? fileMetaData.OtherLockUserClients[0].Split('@')[0]
                : fileMetaData.ActionOwner;

            FileStatus fs = new FileStatus(isOpen, fileMetaData.OtherLock | fileMetaData.OurLock, fileMetaData.OurLock,
                user, fileMetaData.HeadModTime);
            return fs;
        }

        private bool GetLatest(string path, SyncFilesCmdFlags flags)
        {
            CommandRunner.RunWithRetry(() =>
                Client.SyncFiles(
                    new SyncFilesCmdOptions(flags, -1),
                    GetFileSpec(path)), CheckClobber, $"Could not get latest on path {path}", Retry);
            return true;
        }

        private void CheckClobber(Exception e)
        {
            Connect();
            if (e.Message.ToLower().Contains("clobber") && CannotClobberFiles != null)
            {
                List<string> clobberedPaths = new List<string>();
                clobberedPaths.AddRange(e.Message.Split(' ').Select(z => z.Trim()).Where(Path.IsPathRooted));
                P4Exception p4Exception = e as P4Exception;
                if (p4Exception != null)
                {
                    P4Exception nexterror = p4Exception.NextError;
                    while (nexterror != null)
                    {
                        clobberedPaths.AddRange(nexterror.Message.Split(' ').Select(z => z.Trim()).Where(Path.IsPathRooted));
                        nexterror = nexterror.NextError;
                    }
                }

                CannotClobberFiles(clobberedPaths.ToArray());
            }

        }

        #endregion
    }
}