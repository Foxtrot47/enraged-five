﻿// Copyright: Rockstar North 2016

#region

using System;
using System.Collections.Generic;
using System.Linq;
using Perforce.P4;
using Rockstar.AssetManager.Infrastructure.Data;
using Rockstar.AssetManager.Infrastructure.Enums;
using Rockstar.AssetManager.Interfaces;

#endregion

namespace Rockstar.AssetManager.P4API
{
    /// <summary>
    /// The P4 change list.
    /// </summary>
    public class P4ChangeList : IChangeList
    {
        private const int Retry = 10;
        private Changelist _changelist;
        private Client Client => ((P4AssetManager)AssetManager).Client;
        private Repository Repository => ((P4AssetManager)AssetManager).Repository;



        public P4ChangeList(P4AssetManager assetManager)
        {
            AssetManager = assetManager;
        }

        public P4ChangeList(uint id, P4AssetManager assetManager, string description = null)
        {
            AssetManager = assetManager;
            CommandRunner.RunWithRetry(() => GetOrCreateChangelist(id, description), () => assetManager.Connect(),
                $"Could not get or create changelist {id}", Retry);
            Refresh();
        }

        public void AddAsset(IAsset asset)
        {
            if (Assets.Contains(asset) ||
                null != GetAsset(asset.LocalPath))
            {
                // Already have asset in change list
                return;
            }

            // Add asset to change list
            asset.ChangeList = this;
            P4Asset p4Asset = asset as P4Asset;
            CommandRunner.RunWithRetry(() => AddP4Asset(p4Asset), () => AssetManager.Connect(),
                $"Cound not add asset to changelist {Number}", Retry);
        }

        public string AssetsAsString()
        {
            return string.Join(" ", AssetLocalPaths);
        }

        public IAsset CheckoutAsset(string path, bool lockAsset)
        {
            FileSpec file = CheckOut(path, lockAsset);
            Refresh();
            return new P4Asset(file, AssetManager, this);
        }

        public bool Delete()
        {
            CommandRunner.RunWithRetry
                (
                    () => Repository.DeleteChangelist(_changelist, null),
                    () => AssetManager.Connect(),
                    $"Could not delete changelist {Number}",
                    Retry);
            return true;
        }


        public IAsset GetAsset(string path)
        {
            Refresh();
            return
                Assets.FirstOrDefault(
                    p =>
                        p.LocalPath.Equals(path, StringComparison.CurrentCultureIgnoreCase) ||
                        p.DepotPath.Equals(path, StringComparison.CurrentCultureIgnoreCase));
        }

        public bool HasShelvedAssets()
        {
            Refresh();
            return _changelist.Shelved;
        }

        public bool LockAsset(IAsset asset, bool revertOnFailure = true)
        {
            try
            {
                asset.Lock();
                if (!asset.Lock() && revertOnFailure)
                {
                    asset.Revert();
                }
                Refresh();
                return asset.Lock();
            }
            catch (Exception)
            {
                if (revertOnFailure)
                {
                    asset.Revert();
                }
                Refresh();
                throw;
            }
        }

        public bool LockAsset(string path)
        {

            return LockAsset(GetAsset(path));
        }

        public IAsset MarkAssetForAdd(string path, string type)
        {

            P4Asset p4Asset = CommandRunner.RunWithRetry(() => MarkForAdd(path, type), () => AssetManager.Connect(),
                "Could not mark asset for add", Retry);
            Refresh();
            return p4Asset;
        }

        public IAsset MarkAssetForAdd(string path)
        {

            P4Asset p4Asset = CommandRunner.RunWithRetry(() => MarkForAdd(path), () => AssetManager.Connect(),
                "Could not mark asset for add", Retry);
            Refresh();
            return p4Asset;
        }

        public IAsset MarkAssetForDelete(string path)
        {

            P4Asset p4Asset = ((P4Asset)GetAsset(path)) ??
                              new P4Asset(new FileMetaData { LocalPath = new LocalPath(path) }, AssetManager, this);
            MarkAssetForDelete(p4Asset);
            return p4Asset;
        }

        public IAsset MarkAssetForDelete(IAsset asset)
        {

            P4Asset p4Asset = ((P4Asset)asset);
            CommandRunner.RunWithRetry(
                () => Client.DeleteFiles(new DeleteFilesCmdOptions(DeleteFilesCmdFlags.DeleteUnsynced, (int)Number),
                    p4Asset.FileSpec), () => AssetManager.Connect(), "Could not delete file", Retry);
            Refresh();
            return p4Asset;
        }

        public bool MoveAsset(IAsset asset, string newFilePath)
        {
            CommandRunner.RunWithRetry(() => MoveP4Asset(asset, newFilePath), () => AssetManager.Connect(),
                $"Could not move asset {asset.DepotPath} to {newFilePath}", Retry);
            MoveP4Asset(asset, newFilePath);
            return true;
        }

        public bool Refresh()
        {
            _changelist = CommandRunner.RunWithRetry(() => Repository.GetChangelist((int)Number),
                () => AssetManager.Connect(),
                $"Could not refresh changelist {Number}", Retry);

            return _changelist != null;
        }

        public bool RemoveAsset(IAsset asset)
        {
            throw new NotImplementedException();
        }

        public bool Revert(bool delete, bool unchangedOnly = false)
        {
            try
            {

                CommandRunner.RunWithRetry(
                    () => Client.RevertFiles(new List<FileSpec> { new FileSpec(new DepotPath("//..."), null, null, null) }, new RevertCmdOptions(unchangedOnly ? RevertFilesCmdFlags.UnchangedOnly : RevertFilesCmdFlags.None, (int)Number)),
                    () => AssetManager.Connect(), $"Could not revert changelist {Number}", Retry);
                Refresh();
                if (delete && Assets.Count == 0)
                {
                    AssetManager.DeleteChangeList(this);
                }

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool RevertAsset(IAsset asset, bool removeFromChangeList)
        {
            try
            {
                CommandRunner.RunWithRetry(
                    () => Client.RevertFiles(new RevertCmdOptions(RevertFilesCmdFlags.None, (int)Number),
                        new FileSpec { LocalPath = new LocalPath(asset.LocalPath) }), () => AssetManager.Connect(),
                    $"Could not revert {asset.DepotPath}", Retry);
                Refresh();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RevertUnchanged()
        {
            try
            {
                CommandRunner.RunWithRetry(
                    () => Client.RevertFiles(new RevertCmdOptions(RevertFilesCmdFlags.UnchangedOnly, (int)Number)),
                    () => AssetManager.Connect(), $"Could not revert unchanged in changelist {Number}", Retry);
                Refresh();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Submit(string description, out string submittedChangeListNumber)
        {
            Refresh();
            Description = description;
            submittedChangeListNumber =
                CommandRunner.RunWithRetry(() => _changelist.Submit(null).ChangeIdAfterSubmit.ToString(),
                    () => AssetManager.Connect(), $"Could not submit changelist {Number}", Retry);
            return true;
        }

        public bool Submit(string description)
        {
            Refresh();
            Description = description;
            return Submit();
        }

        public bool Submit(out string submittedChangeListNumber)
        {

            return Submit(Description, out submittedChangeListNumber);
        }

        public bool Submit()
        {
            try
            {
                CommandRunner.RunWithRetry(() => _changelist.Submit(null), ()
                     => AssetManager.Connect(),
                     $"Could not submit changelist {Number}", Retry);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateDescription(string description)
        {
            Description = description;
            return true;
        }

        public HashSet<string> AssetDepotPaths
        {
            get
            {
                HashSet<string> hashset = new HashSet<string>();
                IEnumerable<string> localfiles =
                    _changelist.Files.Select(
                        p =>
                            p.DepotPath != null
                                ? p.DepotPath.Path
                                : p.LocalPath.Path.ToDepotPath(AssetManager as P4AssetManager));
                foreach (string file in localfiles)
                {
                    hashset.Add(file);
                }
                return hashset;
            }
        }

        public HashSet<string> AssetLocalPaths
        {
            get
            {
                HashSet<string> hashset = new HashSet<string>();
                IEnumerable<string> localfiles =
                    _changelist.Files.Select(
                        p =>
                            p.LocalPath != null
                                ? p.LocalPath.Path
                                : p.DepotPath.Path.ToLocalPath(AssetManager as P4AssetManager));
                foreach (string file in localfiles)
                {
                    hashset.Add(file);
                }
                return hashset;
            }
        }

        public IAssetManager AssetManager { get; private set; }

        public IList<IAsset> Assets => _changelist.Files.ToIAsset(AssetManager, this);

        public DateTime Created => _changelist.ModifiedDate;

        public string Description
        {
            get { return _changelist.Description; }
            set
            {
                Refresh();
                if (value != _changelist.Description)
                {
                    _changelist.Description = value;
                    Update();
                }
            }
        }

        public uint Number => (uint)_changelist.Id;

        public string NumberAsString => _changelist.Id > 0 ? _changelist.Id.ToString() : "default";

        public ChangeListState State
        {
            get
            {
                if (_changelist.ShelvedFiles?.Count > 0)
                {
                    return ChangeListState.Shelved;
                }
                return _changelist.Pending ? ChangeListState.Pending : ChangeListState.Submitted;

            }
        }

        public string Username => _changelist.OwnerName;

        public string Workspace
        {
            get { return AssetManager.Workspace; }
            set { throw new NotImplementedException(); }
        }

        public bool IsLocalPending()
        {
            try
            {
                if (!Refresh())
                {
                    return false;
                }
                return State == ChangeListState.Pending;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Public methods

        public static P4ChangeList CreateNew(P4AssetManager p4AssetManager, string description)
        {
            return CreateNewChangelist(p4AssetManager, description);
        }

        public static P4ChangeList GetChangeList(uint id, P4AssetManager assetManager)
        {
            return CommandRunner.RunWithRetry(() => GetChangelist(id, assetManager),
                () => assetManager.Connect(),
                $"Couldn't get changelist number: {id}",
                Retry);
        }

        #endregion

        #region Private Methods

        private P4Asset MarkForAdd(string path, string type = null)
        {
            P4Asset p4Asset = ((P4Asset)GetAsset(path));
            if (p4Asset == null)
            {
                p4Asset = new P4Asset(new FileMetaData { LocalPath = new LocalPath(path) }, AssetManager, this);
            }
            AddFilesCmdFlags flag = AddFilesCmdFlags.None;

            if (path.Contains('@') || path.Contains('#') || path.Contains('%'))
            {
                flag = AddFilesCmdFlags.KeepWildcards;
            }

            AddFilesCmdOptions commandOptions = new AddFilesCmdOptions(flag, (int)Number,
                !string.IsNullOrEmpty(type) ? new FileType(type) : null);
            CommandRunner.RunWithRetry(() => Client.AddFiles(commandOptions,
                p4Asset.FileSpec), () => AssetManager.Connect(), $"Could not mark {path} for add in changelist {Number}",
                Retry);
            return p4Asset;
        }

        private FileSpec CheckOut(string path, bool lockAsset)
        {
            if (lockAsset)
            {
                FileStatus filestatus = AssetManager.GetFileStatus(path);
                if (filestatus.IsLocked && !filestatus.IsLockedByMe)
                {
                    throw new Exception($"Cannot lock {path} as it is already locked by {filestatus.Owner}");
                }
            }

            FileSpec file = new FileSpec();
            if (path.IsLocalPath())
            {
                file.LocalPath = new LocalPath(path);
            }
            else
            {
                file.DepotPath = new DepotPath(path);
            }


            //We get latest before checkout.
            AssetManager.GetLatest(path, true);


            CommandRunner.RunWithRetry(
                () => Client.EditFiles(new EditCmdOptions(EditFilesCmdFlags.None, (int)Number, null), file),
                () => AssetManager.Connect(), $"Could not checkout {path} in changelist {Number}", Retry);
            if (lockAsset)
            {
                CommandRunner.RunWithRetry(() => Client.LockFiles(null, file), () => AssetManager.Connect(),
                    $"Could not lock file {path}", Retry);
            }
            return file;
        }

        private void GetOrCreateChangelist(uint id, string description)
        {
            _changelist = CommandRunner.RunWithRetry(() => Repository.GetChangelist((int)id),
                () => AssetManager.Connect(), "Could not get or create changelist", Retry) ??
                          new Changelist((int)id, true);
            Description = description;
        }

        private void AddP4Asset(P4Asset p4Asset)
        {
            FileSpec fileSpec =
                CommandRunner.RunWithRetry(() => Repository.GetFileMetaData(null, p4Asset.FileSpec).FirstOrDefault(),
                    () => AssetManager.Connect(), "Could not get file metadata to add asset to changelist", Retry);
            if (fileSpec == null)
            {
                throw new Exception("Could not get filespec for asset");
            }
            _changelist.Files.Add(fileSpec);
            Repository.UpdateChangelist(_changelist);
        }

        private static P4ChangeList CreateNewChangelist(P4AssetManager p4AssetManager, string description)
        {
            P4ChangeList changelist = new P4ChangeList(p4AssetManager);

            Changelist newChangelist = new Changelist();

            newChangelist.Description = description;
            newChangelist.Files = new List<FileMetaData>();

            changelist._changelist =
                CommandRunner.RunWithRetry(() =>
                    changelist.Repository.CreateChangelist(newChangelist),
                    () => changelist.AssetManager.Connect(),
                    "Could not create changelist", Retry);
          
            return changelist;
        }

        private static P4ChangeList GetChangelist(uint id, P4AssetManager assetManager)
        {
            P4ChangeList changelist = new P4ChangeList(assetManager);

            changelist._changelist = CommandRunner.RunWithRetry(() => changelist.Repository.GetChangelist((int)id),
                () => changelist.AssetManager.Connect(), $"Could not  get changelist {id}", Retry);
            return changelist;
        }

        private void MoveP4Asset(IAsset asset, string newFilePath)
        {
            P4Asset p4Asset = ((P4Asset)asset);
            FileSpec newFileSpec = new FileSpec();
            if (newFilePath.IsLocalPath())
            {
                newFileSpec.LocalPath = new LocalPath(newFilePath);
            }
            else
            {
                newFileSpec.DepotPath = new DepotPath(newFilePath);
            }
            CommandRunner.RunWithRetry(() => Client.MoveFiles(p4Asset.FileSpec, newFileSpec,
                new MoveCmdOptions(MoveFileCmdFlags.Force, (int)Number, null)), () => AssetManager.Connect(),
                $"Could not move asset to {newFilePath}", Retry);
            Refresh();
        }

        private void Update()
        {
            CommandRunner.RunWithRetry(
                () => Repository.UpdateChangelist(_changelist),
                () => AssetManager.Connect(),
                $"Cannot update changelist {Number}",
                Retry);
        }

        #endregion

        internal Changelist GetChangelist()
        {
            return _changelist;
        }
    }
}