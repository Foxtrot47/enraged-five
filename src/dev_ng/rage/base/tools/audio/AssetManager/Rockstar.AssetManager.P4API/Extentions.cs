﻿// Copyright: Rockstar North 2016

#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using Perforce.P4;
using Rockstar.AssetManager.Infrastructure.Data;
using Rockstar.AssetManager.Interfaces;

#endregion

namespace Rockstar.AssetManager.P4API
{
    public static class Extentions
    {
        public static bool IsLocalPath(this string path)
        {
            return path.Contains(":");
        }

        public static string ToDepotPath(this string path, P4AssetManager assetManager)
        {
            return Path.Combine(assetManager.DepotRoot,
                path.TrimStart(assetManager.Client.Root.Replace('/', '\\').ToCharArray()).Replace('\\', '/'));
        }

        public static IList<IAsset> ToIAsset(this IList<FileMetaData> fileMetaDataList, IAssetManager assetManager,
            IChangeList changelist)
        {
            return fileMetaDataList.Select(fmd => fmd.ToIAsset(assetManager, changelist)).ToList();
        }

        public static IAsset ToIAsset(this FileMetaData fileMeta, IAssetManager assetManager, IChangeList changelist)
        {
            return new P4Asset(fileMeta, assetManager, changelist);
        }

        public static string ToLocalPath(this string path, P4AssetManager assetManager)
        {
            //var files = assetManager.Repository1.GetFileMetaData(
            //    new GetFileMetaDataCmdOptions(GetFileMetadataCmdFlags.LocalPath, null, null, 1, null, null, null),
            //    new FileSpec(new DepotPath(path), null, null, null));
            //if (files.Count > 0)
            //{
            //    return files[0].LocalPath.Path;
            //}
            return string.Concat(MakeSureIsFolder(assetManager.Client.Root.Replace('/', '\\')),
                path.TrimStart(assetManager.DepotRoot.ToCharArray()).Replace('/', '\\'));
        }

        public static string MakeSureIsFolder(this string path)
        {
            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }

            return path;
        }


        public static RevisionInfo ToRevisionInfo(this FileHistory fileHistory)
        {
            RevisionInfo revisionInfo = new RevisionInfo
            {
                Action = (RevisionInfo.FileAction) (long) fileHistory.Action,
                ChangelistId = fileHistory.ChangelistId,
                ClientName = fileHistory.ClientName,
                Date = fileHistory.Date,
                DepotPath = fileHistory.DepotPath.Path,
                Description = fileHistory.Description,
                Digest = fileHistory.Digest,
                FileSize = fileHistory.FileSize,
                Revision = fileHistory.Revision,
                
                UserName = fileHistory.UserName
            };
            return revisionInfo;
        }

       

    }


}