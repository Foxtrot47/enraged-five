﻿// Copyright: Rockstar North 2016

#region

using System;

#endregion

namespace Rockstar.AssetManager.P4API
{
    public class CommandRunner
    {
        #region Public methods

        /// <summary>
        /// Runs command with retries, runs command on first retry, it is suggested you use lambda expressions {() => Func(params)) and throws exception at the end if it fails a set number of times.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command">The first command</param>
        /// <param name="actionAtFirstRetry">Action to perform if it failed onthe first retry</param>
        /// <param name="finalExceptionMessage">Exception message to throw at <value>numberOfRetries</value> </param>
        /// <param name="numberOfRetries">number of retries</param>
        /// <returns></returns>
        public static T RunWithRetry<T>(Func<T> command, Action actionAtFirstRetry, string finalExceptionMessage,
            int numberOfRetries)
        {
            for (int i = 0; i < numberOfRetries; i++)
            {
                try
                {
                    return command();
                }
                catch (Exception e)
                {
                    if (i == 0)
                    {
                        actionAtFirstRetry();
                    }
                    else if (i == numberOfRetries - 1)
                    {
                        throw new Exception(finalExceptionMessage, e);
                    }
                }
            }
            throw new Exception(finalExceptionMessage);
        }


        /// <summary>
        /// Runs command with no parameters and no returns, runs action on first retry, and throws exception at the end if it fails a set number of times.
        /// </summary>
        /// <param name="command">The first command</param>
        /// <param name="actionAtFirstRetry">Action to perform if it failed onthe first retry</param>
        /// <param name="finalExceptionMessage">Exception message to throw at <value>numberOfRetries</value> </param>
        /// <param name="numberOfRetries">number of retries</param>
        /// <returns></returns>
        public static void RunWithRetry(Action command, Action actionAtFirstRetry, string finalExceptionMessage,
            int numberOfRetries)
        {
            for (int i = 0; i < numberOfRetries; i++)
            {
                try
                {
                    command();
                    return;
                }
                catch (Exception e)
                {
                    if (i == 0)
                    {
                        actionAtFirstRetry();
                    }
                    else if (i == numberOfRetries - 1)
                    {
                        throw new Exception(finalExceptionMessage, e);
                    }
                }
            }
            throw new Exception(finalExceptionMessage);
        }
        
        /// <summary>
        /// Runs command with no parameters and no returns, runs action on first retry, and throws exception at the end if it fails a set number of times.
        /// </summary>
        /// <param name="command">The first command</param>
        /// <param name="actionAtFirstRetry">Action to perform if it failed onthe first retry with exception as parameter</param>
        /// <param name="finalExceptionMessage">Exception message to throw at <value>numberOfRetries</value> </param>
        /// <param name="numberOfRetries">number of retries</param>
        /// <returns></returns>
        public static void RunWithRetry(Action command, Action<Exception> actionAtFirstRetry, string finalExceptionMessage,
            int numberOfRetries)
        {
            for (int i = 0; i < numberOfRetries; i++)
            {
                try
                {
                    command();
                    return;
                }
                catch (Exception e)
                {
                    if (i == 0)
                    {
                        actionAtFirstRetry(e);
                    }
                    else if (i == numberOfRetries - 1)
                    {
                        throw new Exception(finalExceptionMessage, e);
                    }
                }
            }
            throw new Exception(finalExceptionMessage);
        }

        #endregion
    }
}