﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Test class for Asset Manager.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Test
{
    using System;
    using System.Collections.Generic;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// Test class for Asset Manager.
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            //Console.Write("Enter P4 password: ");
            //var password = ReadPassword();
            //Console.WriteLine();
            var assetManager = AssetManagerFactory.GetInstance(
                AssetManagerType.Perforce, "rsgedip4s1", "1666", "EDIW-DSCHEMBR", "danjeli.schembri", "", "//", null);
            Console.WriteLine(assetManager.IsConnected());
            try
            {
                Console.WriteLine("Connecting to asset manager...");
                assetManager.Connect();
                Console.WriteLine("Connected");
                var assets = assetManager.GetAssets("//rdr3/audio/audio_assets_from_ny/dialogue_mastered/");
                foreach (var asset in assets)
                {
                    Console.WriteLine(asset.DepotPath);
                }
                Console.WriteLine(assetManager.ExistsAsAsset("X:\\gta5\\audio\\dev\\assets\\Waves\\PAIN\\FEMALE\\PAIN_FEMALE_01\\DEATH_LOW_GENERIC_01.WAV"));
              assetManager.GetPrevious(
                        @"x:\gta5\AUDIO\DEV\ASSETS\WAVES\WAVESLOTSETTINGS.XML",
                        true);
                Console.WriteLine(assetManager.GetLatest("X:\\gta5\\audio\\dev\\assets\\Waves\\PAIN\\FEMALE\\PAIN_FEMALE_01\\DEATH_LOW_GENERIC_01.WAV", false));
                Console.WriteLine(assetManager.GetDepotPath("X:\\gta5\\audio\\dev\\assets\\Waves\\PAIN\\FEMALE\\PAIN_FEMALE_01\\DEATH_LOW_GENERIC_01.WAV"));
                 Console.WriteLine(assetManager.GetDepotPath("\\gta5\\audio\\dev\\assets\\Waves\\PAIN\\FEMALE\\PAIN_FEMALE_01\\DEATH_LOW_GENERIC_01.WAV"));
                 Console.WriteLine(assetManager.GetDepotPath("X:\\gta5\\audio\\dev\\assets\\Waves\\PAIN\\FEMALE\\PAIN_FEMALE_01"));
               Console.WriteLine(  assetManager.GetDepotPath("X:\\gta5\\audio\\dev\\assets\\Waves\\PAIN\\FEMALE\\PAIN_FEMALE_01\\"));

                var path = assetManager.GetDepotPath("X:\\gta5\\audio\\dev\\assets\\Waves\\PAIN\\FEMALE\\PAIN_FEMALE_01\\DEATH_LOW_GENERIC_01.WAV");
                Console.WriteLine(path);
                Console.WriteLine(assetManager.IsResolved("X:\\gta5\\audio\\dev\\projectSettings.xml"));
             

                Console.WriteLine(assetManager.GetLocalPath("asdfsfsdf"));
            }
            catch (Exception ex)
            {
                var curr = ex;
                while (null != curr)
                {
                    Console.WriteLine(curr.Message);
                    curr = curr.InnerException;
                }
                Console.WriteLine(ex.StackTrace);
            }
            Console.Write("Press any key to finish...");
            Console.ReadKey();
        }

        public static string ReadPassword()
        {
            var passbits = new Stack<string>();
            //keep reading
            for (var cki = Console.ReadKey(true); cki.Key != ConsoleKey.Enter; cki = Console.ReadKey(true))
            {
                if (cki.Key == ConsoleKey.Backspace)
                {
                    //rollback the cursor and write a space so it looks backspaced to the user
                    Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                    Console.Write(" ");
                    Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                    passbits.Pop();
                }
                else
                {
                    Console.Write("*");
                    passbits.Push(cki.KeyChar.ToString());
                }
            }
            var pass = passbits.ToArray();
            Array.Reverse(pass);
            return string.Join(string.Empty, pass);
        }
    }
}
