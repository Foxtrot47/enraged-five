﻿// -----------------------------------------------------------------------
// <copyright file="ChangeListState.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.AssetManager.Infrastructure.Enums
{
    /// <summary>
    /// Change list state.
    /// </summary>
    public enum ChangeListState
    {
        /// <summary>
        /// Pending state.
        /// </summary>
        Pending,

        /// <summary>
        /// Submitted state.
        /// </summary>
        Submitted,

        /// <summary>
        /// Shelved state.
        /// </summary>
        Shelved,

        /// <summary>
        /// Unknown state.
        /// </summary>
        Unknown
    }
}
