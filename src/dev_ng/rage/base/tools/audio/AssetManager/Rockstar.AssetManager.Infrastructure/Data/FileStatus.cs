﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileStatus.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the FileStatus type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Infrastructure.Data
{
    using System;

    /// <summary>
    /// The file status.
    /// </summary>
    public class FileStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileStatus"/> class.
        /// </summary>
        /// <param name="isOpen">
        /// isOpen flag.
        /// </param>
        /// <param name="isLocked">
        /// isLocked flag.
        /// </param>
        /// <param name="isLockedByMe">
        /// isLockedByMe flag.
        /// </param>
        /// <param name="owner">
        /// The owner.
        /// </param>
        /// <param name="lastModTime">
        /// The last modification time.
        /// </param>
        public FileStatus(bool isOpen, bool isLocked, bool isLockedByMe, string owner, DateTime? lastModTime)
        {
            this.IsOpen = isOpen;
            this.IsLocked = isLocked;
            this.IsLockedByMe = isLockedByMe;
            this.Owner = owner;
            this.LastModTime = lastModTime;
        }

        #region Properties

        /// <summary>
        /// Gets a value indicating whether is file is open.
        /// </summary>
        public bool IsOpen { get; private set; }

        /// <summary>
        /// Gets a value indicating whether file is locked.
        /// </summary>
        public bool IsLocked { get; private set; }

        /// <summary>
        /// Gets a value indicating whether file is locked by me.
        /// </summary>
        public bool IsLockedByMe { get; private set; }

        /// <summary>
        /// Gets the file owner.
        /// </summary>
        public string Owner { get; private set; }

        /// <summary>
        /// Gets the last mod time.
        /// </summary>
        public DateTime? LastModTime { get; private set; }

        #endregion
    }
}
