﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssetManagerType.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the AssetManagerType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Infrastructure.Enums
{
    /// <summary>
    /// Asset manager type.
    /// </summary>
    public enum AssetManagerType
    {
        /// <summary>
        /// Perforce type.
        /// </summary>
        Perforce,

        /// <summary>
        /// Local type.
        /// </summary>
        Local,
        
        P4Api
    }
}
