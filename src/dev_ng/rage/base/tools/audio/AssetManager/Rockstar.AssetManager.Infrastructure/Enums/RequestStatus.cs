﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequestStatus.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the RequestStatus type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Infrastructure.Enums
{
    /// <summary>
    /// Asset manager request status.
    /// </summary>
    public enum RequestStatus
    {
        /// <summary>
        /// New request.
        /// </summary>
        New,

        /// <summary>
        /// Pending request.
        /// </summary>
        Pending,

        /// <summary>
        /// Completed request.
        /// </summary>
        Completed,

        /// <summary>
        /// Completed request with warnings.
        /// </summary>
        CompletedWithWarnings,

        /// <summary>
        /// Failed request.
        /// </summary>
        Failed
    }
}
