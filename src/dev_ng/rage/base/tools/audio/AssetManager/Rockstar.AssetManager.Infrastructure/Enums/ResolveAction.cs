﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResolveAction.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the ResolveAction type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.AssetManager.Infrastructure.Enums
{
    /// <summary>
    /// The asset resolve actions.
    /// </summary>
    public enum ResolveAction
    {
        /// <summary>
        /// Unknown.
        /// </summary>
        Unknown,

        /// <summary>
        /// "Accept yours" action.
        /// </summary>
        AcceptYours,

        /// <summary>
        /// "Accept theirs" action.
        /// </summary>
        AcceptTheirs,

        /// <summary>
        /// "Accept merged" action.
        /// </summary>
        AcceptMerged,

        /// <summary>
        /// "Accept auto" action.
        /// </summary>
        AcceptAuto
    }
}
