﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rage.ToolLib.CmdLine;
using System.ComponentModel;

namespace LipsyncRebuildChecker
{
    public class ProgramConfiguration : CommandLineConfiguration
    {
        public ProgramConfiguration(string[] args)
            : base(args)
        {
        }

        [Description("CommitWatcher settings")]
        [OptionalParameter]
        [DefaultValue("Settings.xml")]
        public string Settings { get; set; }

        [Description("Rebuild mode: Lipsync or Gesture")]
        public string Mode { get; set; }
    }
}
