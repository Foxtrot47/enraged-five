﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using System.IO;
using System.Xml;
using CommitWatcher;
using Rockstar.AssetManager.Main;
using Rockstar.AssetManager.Interfaces;
using Rockstar.AssetManager.Infrastructure.Enums;
using Rockstar.AssetManager.Infrastructure.Data;
using CommitWatcher.Reports;

namespace LipsyncRebuildChecker
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static int Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                logger.Error(eventArgs.ExceptionObject.ToString());
                Environment.Exit(-1);
            };

            ProgramConfiguration config;
            try
            {
                config = new ProgramConfiguration(args);
            }
            catch (rage.ToolLib.CmdLine.CommandLineConfiguration.ParameterException ex)
            {
                logger.Error(ex.Message);
                logger.Info(ex.Usage);
                return -1;
            }


            if (!File.Exists(config.Settings))
            {
                logger.Error("No commit watcher settings found [settings.xml]");
                return -1;
            }

            var settingsXml = new XmlDocument();
            settingsXml.Load(config.Settings);
            Settings settings = new Settings(settingsXml);

            IAssetManager assetManager = AssetManagerFactory.GetInstance(
                    AssetManagerType.Perforce,
                    settings.P4Host,
                    settings.P4Port,
                    settings.P4Workspace,
                    settings.P4User,
                    settings.P4Password,
                    settings.P4DepotRoot);

            if (config.Mode.ToUpper().Equals("LIPSYNC"))
            {
                try
                {
                    CommitWatcher.Reports.LipsyncAudioRebuildReport lipsyncAudioRebuildReport = new LipsyncAudioRebuildReport(settings, assetManager);
                    lipsyncAudioRebuildReport.Run();
                }
                catch (Exception ex)
                {
                    logger.Error("An error occurred while checking for lipsync audio to rebuild: " + ex.ToString());
                    return -1;
                }
            }
            else if (config.Mode.ToUpper().Equals("GESTURE"))
            {
                try
                {
                    CommitWatcher.Reports.GestureAudioRebuildReport gestureReport = new GestureAudioRebuildReport(settings, assetManager);
                    gestureReport.Run();
                }
                catch (Exception ex)
                {
                    logger.Error("An error occurred while checking for gesture audio to rebuild: " + ex.ToString());
                    return -1;
                }
            }
            else 
            {
                logger.Error("Mode " + config.Mode+" unknown");
                return -1;
            }

            return 0;
        }
    }
}
