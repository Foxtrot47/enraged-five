﻿// -----------------------------------------------------------------------
// <copyright file="LipsyncRegenReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using NLog;


    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Perforce;
    using Rockstar.AssetManager.Infrastructure.Enums;
    using rage;

    /// <summary>
    /// Collision settings report.
    /// </summary>
    public class LipsyncRegenReport
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// A list of custom anims that may need to be checked.
        /// </summary>
        List<string> alteredCustomAnimFiles = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="LipsyncRegenReport"/> class.
        /// </summary>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public LipsyncRegenReport(Settings settings, IAssetManager assetManager)
        {
            this.assetManager = assetManager;
            this.settings = settings;
        }

        /// <summary>
        /// Run the lipsync regen report.
        /// </summary>
        /// <param name="clFiles">
        /// The change list files.
        /// </param>
        /// <param name="lipsyncTextPath">
        /// The lipsync text path.
        /// </param>
        public void Run(Data.P4ChangeList.ChangeListFile[] clFiles, string lipsyncTextPath, IList<MailAddress> mailAddresses)
        {
            if (clFiles.Length == 0)
            {
                return;
            }

            if (!this.RequiresRun(clFiles))
            {
                return;
            }

            logger.Info("Running lipsync regen report");

            if (!this.WriteToLipsyncTextFile(clFiles, lipsyncTextPath + DateTime.Now.ToFileTime() + ".txt"))
            {
                logger.Error("Failed to write output to lipsync text file");
            }

            if (alteredCustomAnimFiles.Count() != 0)
            {
                try
                {
                    var sb = new StringBuilder();

                    var email = new MailMessage
                    {
                        From = this.settings.From,
                        Subject = string.Format("Custom Lipsync Alert - WAV file or files have changed."),
                        IsBodyHtml = true
                    };

                    foreach (var mailAddress in mailAddresses)
                    {
                        email.To.Add(mailAddress);
                    }

                    // Output header HTML
                    sb.AppendLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
                    sb.AppendLine("<html>");
                    sb.AppendLine("<head>");
                    sb.AppendLine("<style type=\"text/css\">");
                    sb.AppendLine("<!--");
                    sb.AppendLine("body, table");
                    sb.AppendLine("{");
                    sb.AppendLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
                    sb.AppendLine("}");
                    sb.AppendLine("div.indented");
                    sb.AppendLine("{");
                    sb.AppendLine("padding-left: 50pt;");
                    sb.AppendLine("padding-right: 50pt;");
                    sb.AppendLine("}");
                    sb.AppendLine("table");
                    sb.AppendLine("{");
                    sb.AppendLine("border-collapse: collapse;");
                    sb.AppendLine("margin: 8px 0px;");
                    sb.AppendLine("}");
                    sb.AppendLine("table td, table th");
                    sb.AppendLine("{");
                    sb.AppendLine("padding: 2px 15px;");
                    sb.AppendLine("}");
                    sb.AppendLine("table th");
                    sb.AppendLine("{");
                    sb.AppendLine("border-top: 1px solid #FB7A31;");
                    sb.AppendLine("border-bottom: 1px solid #FB7A31;");
                    sb.AppendLine("background: #FFC;");
                    sb.AppendLine("font-weight: bold;");
                    sb.AppendLine("}");
                    sb.AppendLine("table td");
                    sb.AppendLine("{");
                    sb.AppendLine("border-bottom: 1px solid #CCC;");
                    sb.AppendLine("padding: 0 0.5em;");
                    sb.AppendLine("}");
                    sb.AppendLine("-->");
                    sb.AppendLine("</style>");
                    sb.AppendLine("</head>");

                    // Output body HTML
                    sb.AppendLine("<body>");
                    sb.AppendLine("<p>");
                    sb.AppendLine("The following wave files with custom lipsync have changed and may need adjusting:");
                    sb.AppendLine("</p>");

                    foreach (string line in alteredCustomAnimFiles)
                        sb.AppendLine(line);

                    sb.AppendLine("</body>");
                    sb.AppendLine("</html>");

                    // Send email
                    email.Body = sb.ToString();
                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
                    {
                        smtp.Send(email);
                    };
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }

            alteredCustomAnimFiles.Clear();
        }

        /// <summary>
        /// Returns value indicating whether report is required to run.
        /// </summary>
        /// <param name="clFiles">
        /// The change list files.
        /// </param>
        /// <returns>
        /// Value indicating whether report is required to run <see cref="bool"/>.
        /// </returns>
        private bool RequiresRun(Data.P4ChangeList.ChangeListFile[] clFiles)
        {
            bool containsWavs = clFiles.Select(
                file => Path.GetExtension(file.File)).Any(
                ext => !string.IsNullOrEmpty(ext) && ext.ToUpper() == ".WAV");

            bool containsDstar = clFiles.Select(
               file => Path.GetExtension(file.File)).Any(
               ext => !string.IsNullOrEmpty(ext) && ext.ToUpper() == ".DSTAR");

            bool containsAnim = clFiles.Select(
               file => Path.GetExtension(file.File)).Any(
               ext => !string.IsNullOrEmpty(ext) && ext.ToUpper() == ".ANIM");

            return containsWavs || containsDstar || containsAnim;
        }

        /// <summary>
        /// Write the lipsync text file.
        /// </summary>
        /// <param name="clFiles">
        /// The change list files.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool WriteToLipsyncTextFile(IEnumerable<Data.P4ChangeList.ChangeListFile> clFiles, string path)
        {
            try
            {
                logger.Info("Lipysnc regen update path: " + path);

                List<string> banksToAdd = new List<string>();


                assetManager.GetLatest(settings.ProjectSettings.LipsyncDialogueSettings.LipsyncAnimsBaseDir, false);
                assetManager.GetLatest(settings.ProjectSettings.LipsyncDialogueSettings.CustomAnimationDir, false);
                foreach (audScriptedSpeechConfig ssConfig in settings.ProjectSettings.LipsyncDialogueSettings.ScriptedSpeechConfigs)
                {
                    if (!string.IsNullOrEmpty(ssConfig.CustomAnimationDir)) assetManager.GetLatest(ssConfig.CustomAnimationDir, false);
                }

                CheckForWaveFiles(clFiles, banksToAdd, this.alteredCustomAnimFiles);
                CheckForDStarFiles(clFiles, banksToAdd);
                CheckForCustomAnimFiles(clFiles, banksToAdd);

                if (!banksToAdd.Any())
                {
                    logger.Info("No Bank with Lipsync relevant changes found. Nothing needs to be regenerated");
                    return true;
                }

                var localPath = this.assetManager.GetLocalPath(path);
                Directory.CreateDirectory(Path.GetDirectoryName(localPath));
                var fs = File.Create(localPath);
                var writer = new StreamWriter(fs);

                foreach (var line in banksToAdd)
                {
                    writer.WriteLine(line);
                }

                writer.Close();
                fs.Close();

                var changeList = this.assetManager.CreateChangeList("Updating Lipsync Regen File");
                changeList.MarkAssetForAdd(localPath);

                // Revert unchanged and submit if file has new changes
                changeList.RevertUnchanged();
                if (!changeList.Assets.Any())
                {
                    logger.Info("Lipysnc regen report: No Lipsync changes present - update file will not be created.");
                    changeList.Delete();
                }
                else
                {
                    return changeList.Submit();
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Exception while generating lipsync regen file: " + ex);
                return false;
            }
            
        }


        public static audScriptedSpeechConfig GetScriptedSpeechConfigForWaveDepotPath(string waveDepotPath, Settings settings) {
            return settings.ProjectSettings.LipsyncDialogueSettings.ScriptedSpeechConfigs.FirstOrDefault(
                scriptedSpeechConfig => waveDepotPath.ToUpper().Contains(Path.Combine(settings.ProjectSettings.LipsyncDialogueSettings.WavesBaseDir, scriptedSpeechConfig.Path).ToUpper())
                );
        }

        public static audAmbientSpeechConfig GetAmbientSpeechConfigForWaveDepotPath(string waveDepotPath, Settings settings) {
            return settings.ProjectSettings.LipsyncDialogueSettings.AmbientSpeechConfigs.FirstOrDefault(
                ambientSpeechConfig => waveDepotPath.ToUpper().Contains(Path.Combine(settings.ProjectSettings.LipsyncDialogueSettings.WavesBaseDir, ambientSpeechConfig.Path).ToUpper())
            );
        }

        private void CheckForWaveFiles(IEnumerable<Data.P4ChangeList.ChangeListFile> clFiles, List<string> banksToAdd, List<string> alteredCustomAnimFiles)
        {
            foreach (var file in clFiles)
            {
                if(Path.GetExtension(file.File).ToUpper() != ".WAV")
                    continue;

                //only process waves in the WaveBaseDir
                //there shouldn't really be any wave commits outside of that directory
                if(!file.File.ToUpper().Contains(settings.ProjectSettings.LipsyncDialogueSettings.WavesBaseDir.ToUpper()))
                {
                    continue;
                }

                //do not process anim files which are not in the configured structure

                //do not process anim files which are not in the configured structure
                audScriptedSpeechConfig correspondingScriptedSpeechConfig = GetScriptedSpeechConfigForWaveDepotPath(file.File, settings);
                audAmbientSpeechConfig correspondingAmbientSpeechConfig = GetAmbientSpeechConfigForWaveDepotPath(file.File, settings);
                if (correspondingAmbientSpeechConfig == null && correspondingScriptedSpeechConfig == null)
                {
                    logger.Warn("No scripted speech configuration or ambient speech configuration found for " + file.File);
                    logger.Warn("Please check the LipsyncSettings section in projectSettings.xml");
                    continue;
                }

                string relativeLocalWaveDirPath = Path.GetDirectoryName(assetManager.GetLocalPath(file.File)).ToUpper().Replace(assetManager.GetLocalPath(settings.ProjectSettings.LipsyncDialogueSettings.WavesBaseDir).ToUpper(), "");

                string customAnimDir = settings.ProjectSettings.LipsyncDialogueSettings.CustomAnimationDir;
                if (correspondingScriptedSpeechConfig != null && !string.IsNullOrEmpty(correspondingScriptedSpeechConfig.CustomAnimationDir))
                    customAnimDir = correspondingScriptedSpeechConfig.CustomAnimationDir;

                string localPathToCustomAnimation = Path.Combine(
                    Path.Combine(assetManager.GetLocalPath(customAnimDir), relativeLocalWaveDirPath),
                    Path.GetFileNameWithoutExtension(file.File) + ".ANIM");

                if (File.Exists(localPathToCustomAnimation))
                {
                    if (file.Revision > 1)
                    {
                        List<String> args = new List<String>();
                        args.Add("-Ol");
                        args.Add(file.File + "#" + file.Revision);
                        var requestNew = new P4RunCmdRequest("fstat", args);
                        this.assetManager.RequestHandler.ActionRequest(requestNew);

                        args.Clear();
                        args.Add("-Ol");
                        args.Add(file.File + "#" + (file.Revision - 1).ToString());
                        var requestPrevious = new P4RunCmdRequest("fstat", args);
                        this.assetManager.RequestHandler.ActionRequest(requestPrevious);
                        if (requestNew.Status != RequestStatus.Failed &&
                            requestPrevious.Status != RequestStatus.Failed &&
                            requestNew.RecordSet.Records.Length > 0 && requestPrevious.RecordSet.Records.Length > 0)
                        {
                            if (requestNew.RecordSet.Records[0].Fields["fileSize"] !=
                                requestPrevious.RecordSet.Records[0].Fields["fileSize"])
                            {
                                alteredCustomAnimFiles.Add(localPathToCustomAnimation);
                            }
                        }
                    }
                    else
                    {
                        alteredCustomAnimFiles.Add(localPathToCustomAnimation);
                    }
                }

                string pathToBanks = correspondingScriptedSpeechConfig != null ? correspondingScriptedSpeechConfig.Path : correspondingAmbientSpeechConfig.Path;
                if (string.IsNullOrEmpty(pathToBanks))
                {
                    logger.Warn("No Path attribute specified in speech configuration. Bank of file "+file.File+" could not be determined and won't be added to the report.");
                    continue;
                }

                DirectoryInfo ssBaseDir = new DirectoryInfo(assetManager.GetLocalPath(settings.ProjectSettings.LipsyncDialogueSettings.WavesBaseDir + (settings.ProjectSettings.LipsyncDialogueSettings.WavesBaseDir.EndsWith("/") ? "" : "/") + pathToBanks));
                DirectoryInfo pathToBank = Directory.GetParent(Path.GetDirectoryName(assetManager.GetLocalPath(file.File)));

                try
                {
                    while (!pathToBank.Parent.FullName.Equals(ssBaseDir.FullName))
                    {
                        pathToBank = pathToBank.Parent;
                    }

                    string relativeBankPath = pathToBank.FullName.ToUpper().Replace(assetManager.GetLocalPath(settings.ProjectSettings.LipsyncDialogueSettings.WavesBaseDir).ToUpper(), "");

                    if (!banksToAdd.Contains(relativeBankPath))
                    {
                        banksToAdd.Add(relativeBankPath);
                    }
                }
                catch (Exception e) {
                    logger.Warn("Bank of file " + file.File + " could not be determined using " + ssBaseDir.FullName + " as base directory. Bank won't be added to the report.");
                    continue;
                }
            }
        }

        private void CheckForDStarFiles(IEnumerable<Data.P4ChangeList.ChangeListFile> clFiles, List<string> banksToAdd)
        {
            foreach (Data.P4ChangeList.ChangeListFile file in clFiles)
            {
                String dstarFileName = file.File;
                if (Path.GetExtension(dstarFileName).ToUpper() != ".DSTAR")
                    continue;

                var doc = new XmlDocument();
                if (!assetManager.ExistsAsAsset(dstarFileName) || file.Revision <= 1)
                    continue;

                assetManager.GetLatest(dstarFileName, true);

                bool shouldProcessThisFile = false;
                //diff the files
                ProcessStartInfo procInfo = new ProcessStartInfo("p4");
                procInfo.RedirectStandardOutput = true;
                procInfo.UseShellExecute = false;
                procInfo.CreateNoWindow = true;
                procInfo.WindowStyle = ProcessWindowStyle.Hidden;

                int revision = file.Revision;
                int lastRevision = file.Revision - 1;
                procInfo.Arguments = "diff2 " + dstarFileName + "#" + revision.ToString() + " " + dstarFileName + "#" + lastRevision.ToString();
                
                Process proc = new Process();
                proc.StartInfo = procInfo;
                proc.Start();
                String diffOutput = proc.StandardOutput.ReadToEnd();
                proc.WaitForExit();

                String[] outputLines = Regex.Split(diffOutput, "\r\n");

                Dictionary<string, string> filenameToTextMapping = new Dictionary<string, string>();
                foreach (string line in outputLines)
                {
                    Match thisLineFileName = Regex.Match(line, "filename=\".*?\"");
                    Match thisLineDialogue = Regex.Match(line, "dialogue=\".*?\"");
                    if (thisLineFileName.Success && thisLineDialogue.Success)
                    {
                        if (filenameToTextMapping.ContainsKey(thisLineFileName.Value))
                        {
                            if (String.Compare(filenameToTextMapping[thisLineFileName.Value], thisLineDialogue.Value) != 0)
                            {
                                shouldProcessThisFile = true;
                                break;
                            }
                        }
                        else
                            filenameToTextMapping.Add(thisLineFileName.Value, thisLineDialogue.Value);
                    }
                }

                if (shouldProcessThisFile)
                {
                    bool fileFound = false;
                    doc.Load(assetManager.GetLocalPath(dstarFileName));
                    foreach (XmlElement line in doc.SelectNodes("/MissionDialogue/Conversations/Conversation/Lines/Line"))
                    {
                        String wavName = line.GetAttribute("filename");
                        if (wavName.ToUpper().StartsWith("SFX_"))
                        {
                            continue;
                        }
                        String[] wavNameSplit = wavName.Split('_');
                        if (wavNameSplit.Length == 0)
                        {
                            continue;
                        }
                        String missionName = wavNameSplit[0];

                        //d* files are only associated with scripted speech, so check whether bank containing the mission needs to be added
                        foreach (audScriptedSpeechConfig ssConfig in settings.ProjectSettings.LipsyncDialogueSettings.ScriptedSpeechConfigs)
                        {
                            foreach (string missionSubDir in Directory.GetDirectories(assetManager.GetLocalPath(
                                            Path.Combine(settings.ProjectSettings.LipsyncDialogueSettings.LipsyncAnimsBaseDir, ssConfig.Path))))
                            {
                                if (missionSubDir.EndsWith(missionName))
                                {
                                    string relativeBankPath = missionSubDir.ToUpper().Replace(
                                        assetManager.GetLocalPath(settings.ProjectSettings.LipsyncDialogueSettings.LipsyncAnimsBaseDir).ToUpper(), "");

                                    if(!banksToAdd.Contains(relativeBankPath)) {
                                        banksToAdd.Add(relativeBankPath);
                                    }
                                    fileFound = true;
                                    break;

                                }
                            }
                            if(fileFound)
                                break;
                        }
                    }
                }
            }
        }


        private audScriptedSpeechConfig getScriptedSpeechConfigForCustomAnimDepotPath(string animDepotPath) {
            return settings.ProjectSettings.LipsyncDialogueSettings.ScriptedSpeechConfigs.FirstOrDefault(
                scriptedSpeechConfig => 
                    !string.IsNullOrEmpty(scriptedSpeechConfig.CustomAnimationDir)? animDepotPath.ToUpper().Contains(Path.Combine(scriptedSpeechConfig.CustomAnimationDir, scriptedSpeechConfig.Path).ToUpper()) :false
                    ||
                    animDepotPath.ToUpper().Contains(Path.Combine(settings.ProjectSettings.LipsyncDialogueSettings.CustomAnimationDir, scriptedSpeechConfig.Path).ToUpper())
                );
        }

        private audAmbientSpeechConfig getAmbientSpeechConfigForCustomAnimDepotPath(string animDepotPath) {
            return settings.ProjectSettings.LipsyncDialogueSettings.AmbientSpeechConfigs.FirstOrDefault(
                ambientSpeechConfig => animDepotPath.ToUpper().Contains(Path.Combine(settings.ProjectSettings.LipsyncDialogueSettings.CustomAnimationDir, ambientSpeechConfig.Path).ToUpper())
            );
        }

        private void CheckForCustomAnimFiles(IEnumerable<Data.P4ChangeList.ChangeListFile> clFiles, List<string> banksToAdd)
        {
            foreach (var file in clFiles)
            {
                var ext = Path.GetExtension(file.File);

                if (string.IsNullOrEmpty(ext) || ext.ToUpper() != ".ANIM")
                {
                    continue;
                }

                //do not process anim files which are not in the configured structure
                audScriptedSpeechConfig correspondingScriptedSpeechConfig = getScriptedSpeechConfigForCustomAnimDepotPath(file.File);
                audAmbientSpeechConfig correspondingAmbientSpeechConfig = getAmbientSpeechConfigForCustomAnimDepotPath(file.File);
                if (correspondingAmbientSpeechConfig == null && correspondingScriptedSpeechConfig == null)
                {
                    logger.Warn("No scripted speech configuration or ambient speech configuration found for " + file.File);
                    logger.Warn("Please check the LipsyncSettings section in projectSettings.xml");
                    continue;
                }

                string pathToBanks = correspondingScriptedSpeechConfig!=null? correspondingScriptedSpeechConfig.Path : correspondingAmbientSpeechConfig.Path;
                if (string.IsNullOrEmpty(pathToBanks))
                {
                    logger.Warn("No Path attribute specified in speech configuration. Bank of file " + file.File + " could not be determined and won't be added to the report.");
                    continue;
                }

                //only process animations in the CustomAnimationDir
                if (!(
                        file.File.ToUpper().Contains(settings.ProjectSettings.LipsyncDialogueSettings.CustomAnimationDir.ToUpper())
                        ||
                        (correspondingScriptedSpeechConfig!=null && 
                         !string.IsNullOrEmpty(correspondingScriptedSpeechConfig.CustomAnimationDir) && 
                         file.File.ToUpper().Contains(correspondingScriptedSpeechConfig.CustomAnimationDir.ToUpper())
                        )
                    )
                   )
                {
                    continue;
                }


                string customAnimDir = settings.ProjectSettings.LipsyncDialogueSettings.CustomAnimationDir;
                if (correspondingScriptedSpeechConfig != null && !string.IsNullOrEmpty(correspondingScriptedSpeechConfig.CustomAnimationDir))
                    customAnimDir = correspondingScriptedSpeechConfig.CustomAnimationDir;

                DirectoryInfo ssBaseDir = new DirectoryInfo(assetManager.GetLocalPath(customAnimDir + (customAnimDir.EndsWith("/") ? "" : "/") + pathToBanks));
                DirectoryInfo pathToBank = Directory.GetParent(Path.GetDirectoryName(assetManager.GetLocalPath(file.File)));

                try
                {
                    while (!pathToBank.Parent.FullName.Equals(ssBaseDir.FullName))
                    {
                        pathToBank = pathToBank.Parent;
                    }

                    string relativeBankPath = pathToBank.FullName.ToUpper().Replace(assetManager.GetLocalPath(customAnimDir).ToUpper(), "");

                    if (!banksToAdd.Contains(relativeBankPath))
                    {
                        banksToAdd.Add(relativeBankPath);
                    }
                }
                catch (Exception e)
                {
                    logger.Warn("Bank of file " + file.File + " could not be determined using " + ssBaseDir.FullName + " as base directory. Bank won't be added to the report.");
                    continue;
                }

            }
        }

    }
}
