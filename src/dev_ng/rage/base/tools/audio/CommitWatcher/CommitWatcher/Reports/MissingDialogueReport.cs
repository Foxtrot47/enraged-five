﻿// -----------------------------------------------------------------------
// <copyright file="MissingDialogueReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;
    using global::CommitWatcher.Data;
    using global::CommitWatcher.Enums;
    using Rockstar.AssetManager.Interfaces;
    using NLog;

    /// <summary>
    /// Missing Dialogue report generator.
    /// </summary>
    public class MissingDialogueReport
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The platform tags.
        /// </summary>
        private readonly string[] platformTags =
            {
                "PC",
                "PS3",
                "360",
                "XBOX360"
            };

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The build path.
        /// </summary>
        private readonly string buildPath;

        /// <summary>
        /// The wave root path.
        /// </summary>
        private readonly string waveRootPath;

        /// <summary>
        /// The error list.
        /// </summary>
        private List<string> errorList;

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveDepotReport"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public MissingDialogueReport(Settings settings, IAssetManager assetManager)
        {
            this.settings = settings;
            this.assetManager = assetManager;

            this.buildPath = assetManager.GetLocalPath(settings.AssetBuildPath);
            this.waveRootPath = settings.WaveRootPath;
        }

        /// <summary>
        /// Initialize the report generator.
        /// </summary>
        /// <param name="packListDepotPath">
        /// The pack list depot path.
        /// </param>
        /// <param name="mailAddress">
        /// The mail address.
        /// </param>
        public void Run(string packListDepotPath, List<MailAddress> mailAddresses)
        {
            int totalMissing = 0;
            logger.Info("Running missing dialogue report on "+packListDepotPath);

            var packListFileName = Path.GetFileName(packListDepotPath);

            // We're expecting a pack file
            if (null == packListFileName || !packListFileName.ToLower().EndsWith("_pack_list.xml"))
            {
                return;
            }

            // Get the platform from the file name
            var splitFileName = packListDepotPath.Split('\\', '/');
            var platform = splitFileName[splitFileName.Length - 3];

            if (string.IsNullOrEmpty(platform))
            {
                // Platform required and should be present in file path
                return;
            }

            // Sync built waves files
            if (!this.assetManager.GetLatest(this.buildPath, false))
            {
                logger.Error("Failed to get latest build data: " + this.buildPath);
                return;
            }

            // Get local pack list file path
            var packListPath = Path.Combine(this.buildPath, platform, "BuiltWaves", packListFileName);

            if (!File.Exists(packListPath))
            {
                // Invalid path / file doesn't exist
                return;
            }

            // Load the pack file to get the corresponding wave dir name
            var doc = XDocument.Load(packListPath);
            ICollection<String> packfileFilenames = new List<String>();
            foreach (XElement element in doc.Descendants("PackFile"))
            {
                String dir = Path.GetDirectoryName(packListPath);
                String path = Path.Combine(dir,element.Value);

                if (!File.Exists(path))
                {
                    logger.Error("Error: Pack file missing: "+ path);
                    continue;
                }

                packfileFilenames.Add(path);
            }

            IDictionary<String, ICollection<String>> taggedWavDict = new Dictionary<String, ICollection<String>>();
            IDictionary<String, ICollection<String>> wavInPackfileDict = new Dictionary<String, ICollection<String>>();
            IDictionary<String, ICollection<String>> wavInBanksDict = new Dictionary<String, ICollection<String>>();

            Regex regex = new Regex(@"^(.*)_(\d\d).wav$", RegexOptions.IgnoreCase);

            // Load and process each packfile
            foreach (String packfileFilename in packfileFilenames)
            {
                XDocument packfileDoc = XDocument.Load(packfileFilename);

                // Get all the voice tags.
                IEnumerable<XElement> tags = packfileDoc.Root.Descendants("Tag").Where(x => x.Attribute("name").Value == "voice" && x.Attribute("value") != null);

                // Get all the banks - so we can know what banks a wav belongs to .
                IEnumerable<XElement> banks = packfileDoc.Root.Descendants("Bank");
                foreach (XElement bank in banks)
                {
                    String bankName = bank.Attribute("name").Value;
                    IEnumerable<XElement> wavsInBank = bank.Descendants("Wave");
                    foreach (XElement wavInBank in wavsInBank)
                    {
                        String wavName;

                        if (wavInBank.Attribute("builtName") != null)
                        {
                            wavName = wavInBank.Attribute("builtName").Value;
                        }
                        else
                        {
                            wavName = wavInBank.Attribute("name").Value;
                        }


                        // does it match the NAME_DD.wav format?
                        Match match = regex.Match(wavName);
                        if (match.Success && match.Groups.Count == 3)
                        {
                            String shortWavName = match.Groups[1].Value;

                            if (wavInBanksDict.ContainsKey(shortWavName))
                            {
                                if (!wavInBanksDict[shortWavName].Contains(bankName))
                                {
                                    wavInBanksDict[shortWavName].Add(bankName);
                                }
                            }
                            else
                            {
                                wavInBanksDict.Add(shortWavName, new List<String>() { bankName });
                            }  
                        }
                    }
                }

                // Populate Dictionary
                foreach (XElement tag in tags)
                {
                    String tagName = tag.Attribute("value").Value;

                    foreach (XElement wav in tag.Parent.Descendants("Wave"))
                    {
                        IEnumerable<XElement> doNotBuilds = wav.Descendants("Tag").Where(x => x.Attribute("name").Value == "DoNotBuild");
                        
                        if (doNotBuilds.Count() > 0)
                            continue;

                        String wavName;

                        if (wav.Attribute("builtName") != null)
                        {
                            wavName = wav.Attribute("builtName").Value;
                        }
                        else 
                        { 
                            wavName = wav.Attribute("name").Value;
                        }

                        // does it match the NAME_DD.wav format?
                        Match match = regex.Match(wavName);
                        if (match.Success && match.Groups.Count == 3)
                        {
                            if (taggedWavDict.ContainsKey(tagName))
                            {
                                taggedWavDict[tagName].Add(wavName);
                            }
                            else
                            {
                                taggedWavDict.Add(tagName, new List<String>() { wavName } );
                            }

                            String shortWavName = match.Groups[1].Value;

                            if (wavInPackfileDict.ContainsKey(shortWavName))
                            {
                                wavInPackfileDict[shortWavName].Add(packfileFilename);
                            }
                            else
                            {
                                wavInPackfileDict.Add(shortWavName, new HashSet<String>() { packfileFilename });
                            }  
                        }
                    }
                }                    
            }

            String format = "\t{0,-30}{1,-5}{2,-5}{3,-30}{4,-10}{5,-10}\n";
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("\n===========================================================================================================================================\n");
            sb.AppendFormat("Missing Dialogue Report for {0}\n", packListDepotPath);
            sb.AppendFormat("\t----------------------------------------------------------------------------------------------------------------------------------\n");
            sb.AppendFormat(format, "Wav", "Min", "Max", "Missing", "Packfile", "Bank");
            sb.AppendFormat("\t----------------------------------------------------------------------------------------------------------------------------------\n");
            foreach (KeyValuePair<String, ICollection<String>> taggedWav in taggedWavDict)
            {
                IEnumerable<String> wavs = taggedWav.Value;
                IDictionary<String,ICollection<int>> wavsOfSameName = new Dictionary<String,ICollection<int>>();

                foreach (String wav in wavs)
                {
                    Match match = regex.Match(wav);
                    if (match.Success && match.Groups.Count == 3)
                    {
                        String wavName = match.Groups[1].Value;
                        int wavNum = int.Parse(match.Groups[2].Value);

                        if (wavsOfSameName.ContainsKey(wavName))
                        {
                            wavsOfSameName[wavName].Add(wavNum);
                        }
                        else
                        {
                            wavsOfSameName.Add(wavName, new List<int>() { wavNum } );
                        }              
                    }
                }

                foreach (KeyValuePair<String,ICollection<int>> wavRange in wavsOfSameName)
                {
                    int lowestWavNum = int.MaxValue;
                    int highestWavNum = int.MinValue;
                    String wavName = wavRange.Key;
                    IEnumerable<int> wavNums = wavRange.Value;
                    
                    bool[] vals = new bool[100];
                    for (int i=0; i<100; i++)
                        vals[i] = false;

                    foreach (int wavNum in wavNums)
                    {
                        vals[wavNum] = true;

                        if (wavNum > highestWavNum)
                        {
                            highestWavNum = wavNum;
                        }
                    
                        if (wavNum < lowestWavNum)
                        {
                            lowestWavNum = wavNum;
                        }                
                    }

                    int idx = 1;
                    StringBuilder missingVals = new StringBuilder();
                    int numMissing = 0;
                    foreach (bool val in vals.Skip(1))
                    {
                        if (idx > highestWavNum)
                            break;

                        if (!val)
                        {
                            if (numMissing>=1)
                                missingVals.Append(",");
                            missingVals.Append(idx);
                            numMissing += 1;
                        }

                        idx += 1;
                    }

                    if (numMissing > 0)                    
                    {
                        totalMissing += numMissing;

                        StringBuilder sbPackfile = new StringBuilder();
                        ICollection<String> packfiles = wavInPackfileDict[wavName]; 
                        foreach (String packfile in packfiles)
                        {
                            String prettyPackfile = Path.GetFileNameWithoutExtension(packfile).Replace("_PACK_FILE", "");
                            sbPackfile.Append(" " + prettyPackfile);
                        }

                        String bank = "???";
                        ICollection<String> banks = wavInBanksDict[wavName];
                        if (banks.Count > 1)
                        {
                            bank = String.Format("In {0} banks", banks.Count);
                        }
                        else if (banks.Count == 1)
                        {
                            bank = banks.First();
                        }

                        sb.AppendFormat(format, wavName, lowestWavNum, highestWavNum, missingVals, sbPackfile, bank);
                    }
                }
            }
            sb.AppendFormat("\t----------------------------------------------------------------------------------------------------------------------------------\n");

            sb.AppendFormat("\n===========================================================================================================================================\n", totalMissing);
            sb.AppendFormat("Total missing : {0}\n", totalMissing);
            sb.AppendFormat("===========================================================================================================================================\n");

            logger.Info(sb);

            foreach (MailAddress address in mailAddresses)
            {
                this.SendReport(address, sb.ToString(), platform, packListDepotPath);
            }
        }

        /// <summary>
        /// Send the report, if required.
        /// </summary>
        /// <param name="mailAddress">
        /// The mail address.
        /// </param>
        /// <param name="pack">
        /// The pack.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        private void SendReport(MailAddress mailAddress, String message, string platform, string packListDepotPath)
        {
            var subject = string.Format(
                "[{0}] Missing Dialogue Report - {1} ({2})", this.settings.Project, platform.ToUpper(), packListDepotPath);

            var email = new MailMessage
                {
                    From = this.settings.From,
                    Subject = subject,
                    IsBodyHtml = true,
                    BodyEncoding = Encoding.UTF8
                };
            
            email.To.Add(mailAddress);

            email.Body = String.Format("<pre>{0}</pre>", message);

            using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
            {
                smtp.Send(email);
            }
        }
    }
}
