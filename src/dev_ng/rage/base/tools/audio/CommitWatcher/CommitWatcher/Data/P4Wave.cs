﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="P4Wave.cs" company="">
//   
// </copyright>
// <summary>
//   The P4 wave.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CommitWatcher.Data
{
    using global::CommitWatcher.Enums;

    /// <summary>
    /// The P4 wave.
    /// </summary>
    public class P4Wave
    {
        #region Fields

        /// <summary>
        /// The asset name.
        /// </summary>
        public readonly string AssetName;

        /// <summary>
        /// The depot path.
        /// </summary>
        public readonly string DepotPath;

        /// <summary>
        /// The state.
        /// </summary>
        public readonly P4State State;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="P4Wave"/> class.
        /// </summary>
        /// <param name="depotPath">
        /// The depot path.
        /// </param>
        /// <param name="assetName">
        /// The asset name.
        /// </param>
        /// <param name="state">
        /// The state.
        /// </param>
        public P4Wave(string depotPath, string assetName, P4State state)
        {
            this.DepotPath = depotPath;
            this.State = state;
            this.AssetName = assetName;
        }

        #endregion
    }
}