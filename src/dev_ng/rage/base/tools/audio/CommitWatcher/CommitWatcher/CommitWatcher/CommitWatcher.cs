// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommitWatcher.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The commit watcher.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Text.RegularExpressions;

namespace CommitWatcher.CommitWatcher
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.ServiceProcess;
    using System.Threading;
    using System.Xml;
    using NLog;

    using global::CommitWatcher.Data;

    using global::CommitWatcher.Reports;

    using Microsoft.Win32;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// The commit watcher.
    /// </summary>
    public partial class CommitWatcher : ServiceBase
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();


        #region Fields

        /// <summary>
        /// Flag indicating if we are in command line mode.
        /// </summary>
        private readonly bool commandLineMode;

        private readonly IList<FileSystemWatcher> fileSystemWatchers;

        private Settings settings;

        private string settingsPath;

        private CommitReport commitReport;
        private PedPsoReport pedpsoReport;
        private ContentXmlReport contentXmlReport;
        private WaveDepotReport waveDepotReport;
        private CollisionSettingsReport collisionSettingsReport;
        private LipSyncReport lipsyncReport;
        private LipsyncRegenReport lipsyncRegenReport;
        private LipsyncAudioRebuildReport lipsyncAudioRebuildReport;
        private GestureAudioRebuildReport gestureAudioRebuildReport;
        private PendingWaveReport pendingWaveReport;
        private WaveslotReport waveslotReport;
        private MetadataObjectSizeReport metadataObjectSizeReport;
        private MissingDialogueReport missingDialogueReport;
        private DeliveredDialogueReport deliveredDialogueReport;

        private IAssetManager assetManager;

        private IList<string> filePaths;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CommitWatcher"/> class.
        /// </summary>
        /// <param name="commandLineMode">
        /// Flag indicating if we are in command line mode.
        /// </param>
        public CommitWatcher(bool commandLineMode)
        {
            this.commandLineMode = commandLineMode;
            this.fileSystemWatchers = new List<FileSystemWatcher>();
            this.ServiceName = "Commit Watcher";
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            logger.Fatal(e.ExceptionObject.ToString());
            Environment.Exit(-1);
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Initialize the commit watcher.
        /// </summary>
        public void Run()
        {
            logger.Info("Running the CommitWatcher service");

            if (!this.InitSettings())
            {
                return;
            }

            if (!this.InitAssetManager())
            {
                return;
            }

            this.commitReport = new CommitReport(this.settings, this.assetManager);
            this.pedpsoReport = new PedPsoReport(this.settings, this.assetManager);
            this.contentXmlReport = new ContentXmlReport(this.settings, this.assetManager);
            this.waveDepotReport = new WaveDepotReport(this.settings, this.assetManager);
            this.collisionSettingsReport = new CollisionSettingsReport(this.settings, this.assetManager);
            this.lipsyncReport = new LipSyncReport(this.settings, this.assetManager);
            this.lipsyncRegenReport = new LipsyncRegenReport(this.settings, this.assetManager);
            this.lipsyncAudioRebuildReport = new LipsyncAudioRebuildReport(this.settings, this.assetManager);
            this.gestureAudioRebuildReport = new GestureAudioRebuildReport(this.settings, this.assetManager);
            this.pendingWaveReport = new PendingWaveReport(this.settings, this.assetManager);
            this.waveslotReport = new WaveslotReport(this.settings, this.assetManager);
            this.metadataObjectSizeReport = new MetadataObjectSizeReport(this.settings, this.assetManager);
            this.missingDialogueReport = new MissingDialogueReport(this.settings, this.assetManager);
            this.deliveredDialogueReport = new DeliveredDialogueReport(this.settings, this.assetManager);
            this.InitP4EnvVars();
            this.ProcessPendingFiles();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Perform actions on service start.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnStart(string[] args)
        {
            this.Run();
        }

        /// <summary>
        /// Actions performed on service stop.
        /// </summary>
        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }

        /// <summary>
        /// Handle file system watcher error.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The error event args.
        /// </param>
        private static void FileSystemWatcherError(object sender, ErrorEventArgs e)
        {
            throw new Exception("Commit Watch Error! " + e.GetException().Message);
        }

        /// <summary>
        /// Load the commit watcher settings.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool InitSettings()
        {
            if (!File.Exists("Settings.xml"))
            {
                logger.Error("No settings.xml for commit watcher found");
                return false;
            }

            // Load settings
            try
            {
                this.settingsPath = "Settings.xml";
                if (this.commandLineMode)
                {
                    logger.Info("Settings path " + this.settingsPath);
                }

                var settingsXml = new XmlDocument();
                settingsXml.Load(this.settingsPath);
                this.settings = new Settings(settingsXml);

                this.filePaths = new List<string>();

                // Initialize file system watchers
                foreach (var filePath in this.settings.FilePaths)
                {
                    if (!Directory.Exists(filePath))
                    {
                        logger.Warn("Path doesn't exist, unable to initialize file watcher: " + filePath);
                        continue;
                    }

                    // Add valid file paths
                    this.filePaths.Add(filePath);

                    logger.Info("Initializing file watcher for " + filePath);

                    var fileSystemWatcher = new TimeOutFileSystemWatcher();
                    ((System.ComponentModel.ISupportInitialize)fileSystemWatcher).BeginInit();

                    // FileSystemWatcher
                    fileSystemWatcher.Path = filePath;
                    fileSystemWatcher.EnableRaisingEvents = true;
                    fileSystemWatcher.Created += this.FileSystemWatcher_FileCreated;
                    fileSystemWatcher.Error += FileSystemWatcherError;

                    // CommitWatcher
                    ((System.ComponentModel.ISupportInitialize)fileSystemWatcher).EndInit();

                    this.fileSystemWatchers.Add(fileSystemWatcher);
                }
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
                return false;
            }

            return true;
        }

        /// <summary>
        /// Initialize the asset manager.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool InitAssetManager()
        {
            if (null == this.assetManager)
            {
                this.assetManager = AssetManagerFactory.GetInstance(
                    AssetManagerType.Perforce,
                    this.settings.P4Host,
                    this.settings.P4Port,
                    this.settings.P4Workspace,
                    this.settings.P4User,
                    this.settings.P4Password,
                    this.settings.P4DepotRoot);
            }

            bool success = null != this.assetManager && this.assetManager.Connect();
            if (!success) logger.Error("Asset manager could not be initialised.");
            return success;
        }

        /// <summary>
        /// Initialize the P4 environment variables.
        /// </summary>
        private void InitP4EnvVars()
        {
            Utils.RunCommand("p4", "set P4USER=" + this.settings.P4User, false);
            Utils.RunCommand("p4", "set P4CLIENT=" + this.settings.P4Workspace, false);
            Utils.RunCommand("p4", "set P4PORT=" + this.settings.P4Port, false);

            // To aid debugging, print out perforce environment:
            Utils.RunCommand("p4", "set", false);
        }

        /// <summary>
        /// Process any pending files.
        /// </summary>
        private void ProcessPendingFiles()
        {
            foreach (var filepath in this.filePaths)
            {
                var files = Directory.GetFiles(filepath);
                foreach (var file in files)
                {
                    this.ProcessFile(file);
                }
            }
        }

        /// <summary>
        /// Actions performed when file system watcher is created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The file system event args.
        /// </param>
        private void FileSystemWatcher_FileCreated(object sender, FileSystemEventArgs e)
        {
            var fileSystemWatcher = sender as FileSystemWatcher;

            if (null == fileSystemWatcher)
            {
                throw new Exception("Sender not of expected type: FileSystemWatcher");
            }

            Debug.WriteLine(fileSystemWatcher.InternalBufferSize);
            Debug.WriteLine(e.FullPath);
            Thread.Sleep(1000);
            this.ProcessFile(e.FullPath);
        }

        /// <summary>
        /// Process a file picked up by the file system watcher.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        private void ProcessFile(string filePath)
        {
            bool processed = false;
            string lockfilePath = assetManager.Root.EndsWith(Path.DirectorySeparatorChar.ToString()) ? assetManager.Root + "workspace.lock" : assetManager.Root + Path.DirectorySeparatorChar + "workspace.lock";
            while (!processed)
            {
                try
                {
                    using (FileStream lockFile =
                        new FileStream(lockfilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Delete))
                    {
                        try
                        {
                            processFileImpl(filePath);
                        }
                        finally
                        {
                            File.Delete(lockfilePath);
                            processed = true;
                        }
                    }
                }
                catch (IOException ex)
                {
                    //lock already taken
                    Thread.Sleep(1000);
                }
            }


        }

        private void processFileImpl(string filePath)
        {
            lock (this)
            {
                logger.Info("Processing file " + filePath);

                var changelistNumber = Path.GetFileName(filePath);

                // Retrieve the change list from P4
                P4ChangeList.ChangeList changeList;
                try
                {
                    changeList = Utils.RetrieveChangeList(changelistNumber);
                }
                catch (Exception ex)
                {
                    var errorMsg = "Failed to retrieve change list: " + ex.Message;
                    logger.Error(errorMsg);

                    return;
                }


                try
                {
                    this.RunCommitReport(changeList, filePath);
                    this.RunPedPsoReport(changeList);
                    this.RunContentXmlReport(changeList);
                    this.RunWaveDepotReport(changeList);
                    this.RunCollisionSettingsReport(changeList);
                    this.RunLipsyncRegenReport(changeList);
                    this.RunPendingWaveReport(changeList);
                    this.RunWaveslotReport(changeList, filePath);
                    this.RunMetadataObjectSizesReport(changeList);
                    this.RunMissingDialogueReport(changeList, filePath, this.settings.Platforms.ToArray());
                    this.RunDeliveredDialogueReport(changeList);
                    if (this.settings.RunLipsyncAudioRebuildReport) this.lipsyncAudioRebuildReport.Run(changeList.Files);
                    if (this.settings.RunGestureAudioRebuildReport) this.gestureAudioRebuildReport.Run(changeList.Files);
                }
                catch (Exception ex) 
                {
                    logger.Error("Error while processing reports for changelist " + changelistNumber+": "+ex.ToString());
                }

                // Delete file once we've generated the required reports
                logger.Info("Deleting processed file " + filePath);
                try
                {
                    File.Delete(filePath);
                }
                catch (Exception ex)
                {
                    logger.Error("Failed to delete file " + filePath + ": " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Run the commit report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        private void RunCommitReport(P4ChangeList.ChangeList changeList, string filePath)
        {
            // Get list of mail addresses for commit report
            var mailAddresses = new HashSet<MailAddress>();
            foreach (var kvp in
                    from file in changeList.Files
                    from kvp in this.settings.CommitReportWatches
                    where Regex.IsMatch(file.File, kvp.Key, RegexOptions.IgnoreCase)
                    select kvp)
            {
                foreach (MailAddress address in kvp.Value)
                {
                    mailAddresses.Add(address);
                }
            }

            // Generate report if required
            if (mailAddresses.Any())
            {
                this.commitReport.Run(filePath, changeList, mailAddresses.ToList());
            }
        }

        /// <summary>
        /// Run the ped pso report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        private void RunPedPsoReport(P4ChangeList.ChangeList changeList)
        {
            // Get list of mail addresses for commit report
            var mailAddresses = new HashSet<MailAddress>();
            List<P4ChangeList.ChangeListFile> pedPsoFiles = new List<P4ChangeList.ChangeListFile>();
            foreach (P4ChangeList.ChangeListFile clFile in changeList.Files) 
            {
                bool fileAdded = false;
                foreach (KeyValuePair<string, List<MailAddress>> pathWatch in this.settings.PedPsoReportWatches) 
                {
                    if (Regex.IsMatch(clFile.File, pathWatch.Key, RegexOptions.IgnoreCase)) 
                    {
                        if (pathWatch.Value.Any() && !fileAdded)
                        {
                            pedPsoFiles.Add(clFile);
                            fileAdded = true;
                        }
                        foreach (MailAddress address in pathWatch.Value)
                        {
                            mailAddresses.Add(address);
                        }
                    }
                }
            }

            // Generate report if required
            if (mailAddresses.Any())
            {
                this.pedpsoReport.Run(pedPsoFiles.ToArray(), mailAddresses.ToList());
            }
        }

        /// <summary>
        /// Run the content xml report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        private void RunContentXmlReport(P4ChangeList.ChangeList changeList)
        {
            // Get list of mail addresses for commit report
            var mailAddresses = new HashSet<MailAddress>();
            List<P4ChangeList.ChangeListFile> contentXmlFiles = new List<P4ChangeList.ChangeListFile>();
            foreach (P4ChangeList.ChangeListFile clFile in changeList.Files)
            {
                bool fileAdded = false;
                foreach (KeyValuePair<string, List<MailAddress>> pathWatch in this.settings.ContentXmlWatches)
                {
                    if (Regex.IsMatch(clFile.File, pathWatch.Key, RegexOptions.IgnoreCase))
                    {
                        if (pathWatch.Value.Any() && !fileAdded)
                        {
                            contentXmlFiles.Add(clFile);
                            fileAdded = true;
                        }
                        foreach (MailAddress address in pathWatch.Value)
                        {
                            mailAddresses.Add(address);
                        }
                    }
                }
            }

            // Generate report if required
            if (mailAddresses.Any())
            {
                this.contentXmlReport.Run(contentXmlFiles.ToArray(), mailAddresses.ToList());
            }
        }


        /// <summary>
        /// Run the delivered dialogue report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// </param>
        private void RunDeliveredDialogueReport(P4ChangeList.ChangeList changeList)
        {
            HashSet<string> depothPaths = new HashSet<string>();
            // Get list of mail addresses for commit report
            HashSet<MailAddress> mailAddresses = new HashSet<MailAddress>();

            foreach (KeyValuePair<string, List<MailAddress>> kvp in this.settings.DeliveredDialogueReportWatches)
            {
                foreach (P4ChangeList.ChangeListFile file in changeList.Files)
                {
                    if (file.File.ToUpper().Contains(kvp.Key.ToUpper()))
                    {
                        this.deliveredDialogueReport.Run(changeList, kvp.Value, kvp.Key);
                        break;
                    }
                }
            }
        }


        /// <summary>
        /// Run the wave depot report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        private void RunWaveDepotReport(P4ChangeList.ChangeList changeList)
        {
            foreach (var file in changeList.Files)
            {
                var fileNameLower = file.File.ToLower();
                foreach (var kvp in this.settings.WaveDepotReportWatches)
                {
                    var depotPathLower = kvp.Key.ToLower();
                    if (fileNameLower.StartsWith(depotPathLower))
                    {
                        this.waveDepotReport.Run(file.File, kvp.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Run the collision settings report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        private void RunCollisionSettingsReport(P4ChangeList.ChangeList changeList)
        {
            bool runReport = false;
            // Get list of mail addresses for collision settings report errors
            var mailAddresses = new HashSet<MailAddress>();
            foreach (var kvp in
                    from file in changeList.Files
                    from kvp in this.settings.CollisionSettingsReportWatches
                    where Regex.IsMatch(file.File, kvp.Key, RegexOptions.IgnoreCase)
                    select kvp)
            {
                runReport = true;
                foreach (MailAddress address in kvp.Value)
                {
                    mailAddresses.Add(address);
                }
            }

            if (runReport) this.collisionSettingsReport.Run(mailAddresses.ToList());
        }

        /// <summary>
        /// Run the lipsync regen report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        private void RunLipsyncRegenReport(P4ChangeList.ChangeList changeList)
        {
            bool runReport = false;
            // Get list of mail addresses for commit report
            var mailAddresses = new HashSet<MailAddress>();
            foreach (var kvp in
                    from file in changeList.Files
                    from kvp in this.settings.LipsyncRegenReportWatches
                    where Regex.IsMatch(file.File, kvp.Key, RegexOptions.IgnoreCase)
                    select kvp)
            {
                runReport = true;
                foreach (MailAddress address in kvp.Value)
                {
                    mailAddresses.Add(address);
                }
            }

            if (runReport) this.lipsyncRegenReport.Run(changeList.Files, this.settings.LipsyncUpdateFilePath, mailAddresses.ToList());
        }

        /// <summary>
        /// Run the pending wave list report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        private void RunPendingWaveReport(P4ChangeList.ChangeList changeList)
        {
            bool runReport = false;
            // Get list of mail addresses for commit report
            var mailAddresses = new HashSet<MailAddress>();
            foreach (var kvp in
                    from file in changeList.Files
                    from kvp in this.settings.PendingWaveReportWatches
                    where Regex.IsMatch(file.File, kvp.Key, RegexOptions.IgnoreCase)
                    select kvp)
            {
                runReport = true;
                foreach (MailAddress address in kvp.Value)
                {
                    mailAddresses.Add(address);
                }
            }

            if (runReport) this.pendingWaveReport.Run(changeList.Files, mailAddresses.ToList(), changeList.ChangeListNumber, this.settings.Platforms.ToArray());
        }

        /// <summary>
        /// Run the waveslot report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        private void RunWaveslotReport(P4ChangeList.ChangeList changeList, string filePath)
        {
            // Get list of mail addresses for wave slot report
            var mailAddresses = new HashSet<MailAddress>();
            foreach (var kvp in
                    from file in changeList.Files
                    from kvp in this.settings.WaveslotReportWatches
                    where Regex.IsMatch(file.File, kvp.Key, RegexOptions.IgnoreCase)
                    select kvp)
            {
                foreach (MailAddress address in kvp.Value)
                {
                    mailAddresses.Add(address);
                }
            }

            // Check if report needs to run
            if (!mailAddresses.Any())
            {
                return;
            }

            this.waveslotReport.Run(
                filePath,
                changeList,
                mailAddresses.ToList());
        }

        /// <summary>
        /// Run the metadata object sizes report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        private void RunMetadataObjectSizesReport(P4ChangeList.ChangeList changeList)
        {
            // Get list of mail addresses for wave slot report
            var mailAddresses = new HashSet<MailAddress>();
            foreach (var kvp in
                    from file in changeList.Files
                    from kvp in this.settings.MetadataObjectSizesReportWatches
                    where Regex.IsMatch(file.File, kvp.Key, RegexOptions.IgnoreCase)
                    select kvp)
            {
                foreach (MailAddress address in kvp.Value)
                {
                    mailAddresses.Add(address);
                }
            }

            // Check if report needs to run
            if (!mailAddresses.Any())
            {
                return;
            }

            this.metadataObjectSizeReport.Run(changeList, mailAddresses);
        }


        /// <summary>
        /// Run the waveslot report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        private void RunMissingDialogueReport(P4ChangeList.ChangeList changeList, string filePath, String[] platforms)
        {
            // Get list of mail addresses for missing dialog report
            var mailAddresses = new HashSet<MailAddress>();
            foreach (var kvp in
                    from file in changeList.Files
                    from kvp in this.settings.MissingDialogueReportWatches
                    where Regex.IsMatch(file.File, kvp.Key, RegexOptions.IgnoreCase)
                    select kvp)
            {
                foreach (MailAddress address in kvp.Value)
                {
                    mailAddresses.Add(address);
                }
            }

            // Check if report needs to run
            if (!mailAddresses.Any())
            {
                return;
            }

            String builtWavePath = this.settings.GenericBuiltWavesPackListFilePath;
            ICollection<String> packListDepotPaths = new List<String>();
            foreach (String platform in platforms)
            {
                packListDepotPaths.Add(builtWavePath.Replace("{platform}", platform));
            }

            foreach (String packListDepotPath in packListDepotPaths)
            {
                this.missingDialogueReport.Run(packListDepotPath, mailAddresses.ToList());
            }

            this.missingDialogueReport.Run(this.settings.GenericBuiltWavesPackListFilePath,
                mailAddresses.ToList());
        }

        #endregion
    }
}
