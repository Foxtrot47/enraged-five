﻿// -----------------------------------------------------------------------
// <copyright file="LipsyncRegenReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using NLog;


    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Perforce;
    using Rockstar.AssetManager.Infrastructure.Enums;
    using rage;
    using System.Globalization;

    /// <summary>
    /// Ped pso report. This report sends out an email if a new file gets added to the watched location or if
    /// a pedXml_audioID or pedXml_audioID2 xml entry in one of the files in the watched location changes.
    /// </summary>
    public class PedPsoReport
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// Initializes a new instance of the <see cref="LipsyncRegenReport"/> class.
        /// </summary>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public PedPsoReport(Settings settings, IAssetManager assetManager)
        {
            this.assetManager = assetManager;
            this.settings = settings;
        }

        /// <summary>
        /// Run the lipsync regen report.
        /// </summary>
        /// <param name="clFiles">
        /// The change list files.
        /// </param>
        /// <param name="lipsyncTextPath">
        /// The lipsync text path.
        /// </param>
        public void Run(Data.P4ChangeList.ChangeListFile[] clFiles, IList<MailAddress> mailAddresses)
        {
            if (clFiles.Length == 0)
            {
                return;
            }

            logger.Info("Running ped pso report");

            Dictionary<string, string> fileChanges = new Dictionary<string, string>();

            foreach (Data.P4ChangeList.ChangeListFile file in clFiles)
            {
                if (file.Revision > 1)
                {
                    List<String> args = new List<String>();
                    args.Add("-Ol");
                    args.Add(file.File + "#" + file.Revision);
                    var requestNew = new P4RunCmdRequest("fstat", args);
                    this.assetManager.RequestHandler.ActionRequest(requestNew);

                    args.Clear();
                    args.Add("-Ol");
                    args.Add(file.File + "#" + (file.Revision - 1).ToString());
                    var requestPrevious = new P4RunCmdRequest("fstat", args);
                    this.assetManager.RequestHandler.ActionRequest(requestPrevious);
                    if (requestNew.Status != RequestStatus.Failed &&
                        requestPrevious.Status != RequestStatus.Failed &&
                        requestNew.RecordSet.Records.Length > 0 && requestPrevious.RecordSet.Records.Length > 0)
                    {
                        if (requestNew.RecordSet.Records[0].Fields["fileSize"] !=
                            requestPrevious.RecordSet.Records[0].Fields["fileSize"])
                        {
                            //file has changed, diff the files to see if the change 
                            //affetcts pedXml_audioID or pedXml_audioID2
                            ProcessStartInfo procInfo = new ProcessStartInfo("p4");
                            procInfo.RedirectStandardOutput = true;
                            procInfo.UseShellExecute = false;
                            procInfo.CreateNoWindow = true;
                            procInfo.WindowStyle = ProcessWindowStyle.Hidden;

                            int revision = file.Revision;
                            int lastRevision = file.Revision - 1;
                            procInfo.Arguments = "diff2 " + file.File + "#" + revision.ToString() + " " + file.File + "#" + lastRevision.ToString();

                            Process proc = new Process();
                            proc.StartInfo = procInfo;
                            proc.Start();
                            String diffOutput = proc.StandardOutput.ReadToEnd();
                            proc.WaitForExit();

                            String[] outputLines = Regex.Split(diffOutput, "\r\n");

                            foreach (string line in outputLines)
                            { 
                                if(CultureInfo.InvariantCulture.CompareInfo.IndexOf(line, "pedXml_audioID", CompareOptions.IgnoreCase) >= 0 ||
                                    CultureInfo.InvariantCulture.CompareInfo.IndexOf(line, "pedXml_audioID2", CompareOptions.IgnoreCase) >= 0)
                                {
                                    if(!fileChanges.ContainsKey(file.File)) fileChanges.Add(file.File, "Value of a pedXml_audioID entry changed. "+line.Substring(1).Trim());
                                }
                            }
                        }
                    }
                }
                else
                {
                    fileChanges.Add(file.File, "File has been added to perforce.");
                }
            }

            if (fileChanges.Count() != 0)
            {
                try
                {
                    var sb = new StringBuilder();

                    var email = new MailMessage
                    {
                        From = this.settings.From,
                        Subject = string.Format("Ped Pso changes"),
                        IsBodyHtml = true
                    };

                    foreach (var mailAddress in mailAddresses)
                    {
                        email.To.Add(mailAddress);
                    }

                    // Output header HTML
                    sb.AppendLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
                    sb.AppendLine("<html>");
                    sb.AppendLine("<head>");
                    sb.AppendLine("<style type=\"text/css\">");
                    sb.AppendLine("<!--");
                    sb.AppendLine("body, table");
                    sb.AppendLine("{");
                    sb.AppendLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
                    sb.AppendLine("}");
                    sb.AppendLine("div.indented");
                    sb.AppendLine("{");
                    sb.AppendLine("padding-left: 50pt;");
                    sb.AppendLine("padding-right: 50pt;");
                    sb.AppendLine("}");
                    sb.AppendLine("table");
                    sb.AppendLine("{");
                    sb.AppendLine("border-collapse: collapse;");
                    sb.AppendLine("margin: 8px 0px;");
                    sb.AppendLine("}");
                    sb.AppendLine("table td, table th");
                    sb.AppendLine("{");
                    sb.AppendLine("padding: 2px 15px;");
                    sb.AppendLine("}");
                    sb.AppendLine("table th");
                    sb.AppendLine("{");
                    sb.AppendLine("border-top: 1px solid #FB7A31;");
                    sb.AppendLine("border-bottom: 1px solid #FB7A31;");
                    sb.AppendLine("background: #FFC;");
                    sb.AppendLine("font-weight: bold;");
                    sb.AppendLine("}");
                    sb.AppendLine("table td");
                    sb.AppendLine("{");
                    sb.AppendLine("border-bottom: 1px solid #CCC;");
                    sb.AppendLine("padding: 0 0.5em;");
                    sb.AppendLine("}");
                    sb.AppendLine("-->");
                    sb.AppendLine("</style>");
                    sb.AppendLine("</head>");

                    // Output body HTML
                    sb.AppendLine("<body>");
                    sb.AppendLine("<p>");
                    sb.AppendLine("The following ped pso files have changed:");
                    sb.AppendLine("</p>");

                    foreach (var filePathAndChangeDescription in fileChanges)
                        sb.AppendLine(filePathAndChangeDescription.Key+": "+filePathAndChangeDescription.Value);

                    sb.AppendLine("</body>");
                    sb.AppendLine("</html>");

                    // Send email
                    email.Body = sb.ToString();
                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
                    {
                        smtp.Send(email);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }
        }

    }
}
