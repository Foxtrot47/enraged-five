﻿// -----------------------------------------------------------------------
// <copyright file="BankGroupDetails.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Data
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Bank group details.
    /// </summary>
    public class BankGroupDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BankGroupDetails"/> class.
        /// </summary>
        public BankGroupDetails()
        {
            this.Banks = new List<BankDetails>();
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the platform.
        /// </summary>
        public string Platform { get; set; }

        /// <summary>
        /// Gets the banks.
        /// </summary>
        public IList<BankDetails> Banks { get; private set; } 

        /// <summary>
        /// Gets the original size.
        /// </summary>
        public long OriginalSize
        {
            get
            {
                return this.Banks.Sum(bank => (long)bank.OriginalSize);
            }
        }

        /// <summary>
        /// Gets the current size.
        /// </summary>
        public long CurrentSize
        {
            get
            {
                return this.Banks.Sum(bank => (long)bank.CurrentSize);
            }
        }

        /// <summary>
        /// Gets the size delta.
        /// </summary>
        public long SizeDelta
        {
            get
            {
                return this.CurrentSize - this.OriginalSize;
            }
        }
    }
}
