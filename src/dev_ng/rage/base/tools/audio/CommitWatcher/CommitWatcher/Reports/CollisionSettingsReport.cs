﻿// -----------------------------------------------------------------------
// <copyright file="CollisionSettingsReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Xml.Linq;
    using NLog;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Collision settings report.
    /// </summary>
    public class CollisionSettingsReport
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The errors.
        /// </summary>
        private readonly IList<string> errors; 

        /// <summary>
        /// Initializes a new instance of the <see cref="CollisionSettingsReport"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public CollisionSettingsReport(Settings settings, IAssetManager assetManager)
        {
            this.settings = settings;
            this.assetManager = assetManager;
            this.errors = new List<string>();
        }

        /// <summary>
        /// Run the collision settings report.
        /// </summary>
        /// <param name="allGameObjectsPaths">
        /// The game objects path.
        /// </param>
        /// <param name="mailAddresses">
        /// The mail addresses to send error report to.
        /// </param>
        /// <param name="csvPath">
        /// Path to CSV output file.
        /// </param>
        public void Run(IList<MailAddress> mailAddresses)
        {
            logger.Info("Running collision settings report");

            ICollection<string> allGameObjectsPaths = this.settings.GameObjectsPaths;
            string csvPath = this.settings.ModelAudioCollisionListPath;

            if (allGameObjectsPaths.Count==0)
            {
                return;
            }

            foreach (string gameObjectsPath in allGameObjectsPaths)
            {
                // Get latest game object data
                if (!this.assetManager.GetLatest(gameObjectsPath, false))
                {
                    this.LogError("Failed to get latest game object data "+gameObjectsPath);
                    this.EmailError(mailAddresses);
                    return;
                }
            }

            // Get latest CSV file (may have been updated remotely)
            if (this.assetManager.ExistsAsAsset(csvPath) && !this.assetManager.GetLatest(csvPath, false))
            {
                this.LogError("Failed to get latest CSV file: " + csvPath);
                this.EmailError(mailAddresses);
                return;
            }

            HashSet<string> gameObjects = new HashSet<string>();
            foreach (string gameObjectsPath in allGameObjectsPaths)
            {
                var localGameObjectPath = this.assetManager.GetLocalPath(gameObjectsPath);
                if (string.IsNullOrEmpty(localGameObjectPath))
                {
                    this.LogError("Failed to get local path: " + allGameObjectsPaths);
                    this.EmailError(mailAddresses);
                    return;
                }

                try
                {
                    gameObjects.UnionWith(this.LoadData(localGameObjectPath));
                    
                }
                catch (Exception ex)
                {
                    this.LogError("Failed to load data from "+localGameObjectPath+Environment.NewLine+ex.ToString());
                    this.EmailError(mailAddresses);
                    return;
                }
            }

            try
            {
                this.WriteToCsv(csvPath, gameObjects);
            }
            catch (Exception ex)
            {
                this.LogError(ex.Message);
                this.LogError("Failed to write data to "+csvPath);
                this.EmailError(mailAddresses);
                return;
            }
        }

        /// <summary>
        /// Load the game object data.
        /// </summary>
        /// <param name="gameObjectsPath">
        /// The game objects path.
        /// </param>
        /// <returns>
        /// The list of game objects (key: game name, value: RAVE name).
        /// </returns>
        private HashSet<string> LoadData(string gameObjectsPath)
        {
            HashSet<string> gameObjects = new HashSet<string>();
            var files = Directory.GetFiles(gameObjectsPath, "*.xml", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var doc = XDocument.Load(file);

                if (null == doc.Root)
                {
                    throw new Exception("Invalid game object XML file: " + file);
                }

                var collisionSettingsElems = doc.Root.Descendants("ModelAudioCollisionSettings");
                foreach (var elem in collisionSettingsElems)
                {
                    var nameAttr = elem.Attribute("name");
                    if (null != nameAttr)
                    {
                        gameObjects.Add(nameAttr.Value);
                    }
                    else
                    {
                        throw new Exception("Failed to find name attribute in element: " + elem.Name.LocalName);
                    }
                }
            }

            return gameObjects;
        }

        /// <summary>
        /// Write the game object names to CSV.
        /// </summary>
        /// <param name="path">
        /// The CSV output file path.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool WriteToCsv(string path, ICollection<string> gameObjects)
        {
            try
            {
                var localPath = this.assetManager.GetLocalPath(path);

                var changeList = this.assetManager.CreateChangeList("ModelAudioCollisionSettings List");
                var requiresAdd = true;

                if (this.assetManager.ExistsAsAsset(localPath))
                {
                    requiresAdd = false;
                    if (null == changeList.CheckoutAsset(localPath, true))
                    {
                        this.LogError("Failed to checkout CSV output file: " + localPath);
                        return false;
                    }
                }

                // Check that asset correctly checked out into change list
                if (!requiresAdd && !changeList.Assets.Any())
                {
                    this.LogError(
                        "Failed to checkout CSV output file into change list - is it already checked out in another change list?");
                    return false;
                }

                // Ensure CSV file is writable (it should be from checkout, but just to make sure)
                if (File.Exists(localPath))
                {
                    var fileInfo = new FileInfo(localPath);
                    fileInfo.IsReadOnly = false;
                }

                // Add data to CSV file
                var filestream = new FileStream(localPath, FileMode.Create);
                var writer = new StreamWriter(filestream);

                writer.WriteLine("Game Name,RAVE Name");

                var items = gameObjects.ToList();
                items.Sort();

                foreach (var raveName in items)
                {
                    var gameName = raveName;
                    if (gameName.ToUpper().StartsWith("MOD_"))
                    {
                        gameName = raveName.Substring(4);
                    }

                    writer.WriteLine(gameName + "," + raveName);
                }

                writer.Close();
                filestream.Close();

                // Mark file for add if first time added to P4
                if (requiresAdd)
                {
                    changeList.MarkAssetForAdd(localPath);
                }

                // Revert unchanged and submit if file has new changes
                changeList.RevertUnchanged();
                if (!changeList.Assets.Any())
                {
                    changeList.Delete();
                }
                else
                {
                    return changeList.Submit();
                }
            }
            catch (Exception ex)
            {
                this.LogError("Failed to write collision settings report to file: " + ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Email errors.
        /// </summary>
        /// <param name="mailAddresses">
        /// The mail addresses.
        /// </param>
        private void EmailError(IList<MailAddress> mailAddresses)
        {
            if (!mailAddresses.Any())
            {
                return;
            }

            var subject = "Audio Collision Settings Failure";

            var email = new MailMessage
            {
                From = this.settings.From,
                Subject = subject,
                IsBodyHtml = false
            };

            foreach (var mailAddress in mailAddresses)
            {
                email.To.Add(mailAddress);
            }
            
            var messageBuilder = new StringBuilder();
            foreach (var error in this.errors)
            {
                messageBuilder.AppendLine(error);
            }

            email.Body = messageBuilder.ToString();

            using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
            {
                smtp.Send(email);
            }
        }

        /// <summary>
        /// The log error.
        /// </summary>
        /// <param name="error">
        /// The error.
        /// </param>
        private void LogError(string error)
        {
            logger.Error(error);
            this.errors.Add(error);
        }
    }
}
