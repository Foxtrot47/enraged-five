﻿// -----------------------------------------------------------------------
// <copyright file="Utils.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher
{
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using global::CommitWatcher.Data;
    using global::CommitWatcher.Enums;

    /// <summary>
    /// Commit watcher utils.
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// Run a command with specified args.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <param name="needOutput">
        /// Flag specifying if output is needed.
        /// </param>
        /// <returns>
        /// The output <see cref="string"/>.
        /// </returns>
        public static string RunCommand(string command, string args, bool needOutput = true)
        {
            var proc = new Process { StartInfo = { FileName = command }, EnableRaisingEvents = false };

            proc.StartInfo.Arguments = args;
            proc.StartInfo.ErrorDialog = false;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardOutput = needOutput;
            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.CreateNoWindow = false;

            Console.WriteLine("Command: {0} {1}", command, args);
            proc.Start();

            string output = null;
            if (needOutput)
            {
                output = proc.StandardOutput.ReadToEnd();
            }

            var error = proc.StandardError.ReadToEnd();

            proc.WaitForExit();

            if (proc.ExitCode != 0)
            {
                throw new Exception("Command failed with exit code " + proc.ExitCode + ": " + error);
            }

            return output;
        }

        public static int? GetFileSize(string file)
        {
            var output = RunCommand("p4", String.Format("sizes {0}", file));
            var split = output.Split(' ');
            if (split.Length < 2)
            {
                return null;
            }

            return Int32.Parse(split[1]);
        }

        /// <summary>
        /// Get file size change.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="rev0">
        /// The previous revision.
        /// </param>
        /// <param name="rev1">
        /// The current revision.
        /// </param>
        /// <returns>
        /// The difference in file size between the two revisions <see cref="int?"/>.
        /// </returns>
        public static int? GetFileSizeChange(string file, int rev0, int rev1)
        {
            var output = RunCommand("p4", String.Format("sizes {0}#{1} {0}#{2}", file, rev0, rev1));
            var lines = output.Split('\n');

            if (lines.Length < 2)
            {
                return null;
            }

            var line0 = lines[0].Trim().Split(' ');
            var line1 = lines[1].Trim().Split(' ');

            if (line0.Length < 3 || line1.Length < 3)
            {
                return null;
            }

            var size0 = Int32.Parse(line0[1]);
            var size1 = Int32.Parse(line1[1]);

            return size1 - size0;
        }

        /// <summary>
        /// Retrieve change list with specified change list number.
        /// </summary>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        /// <returns>
        /// The change list <see cref="P4ChangeList.ChangeList"/>.
        /// </returns>
        public static P4ChangeList.ChangeList RetrieveChangeList(string changeListNumber)
        {
            var changeList = new P4ChangeList.ChangeList();
            var output = RunCommand("p4", "describe -s " + changeListNumber);
            var lines = output.Split('\n');

            var state = ChangeParseState.ChangeHeader;
            changeList.Comment = string.Empty;
            changeList.ChangeListNumber = changeListNumber;
            var fileList = new ArrayList();

            foreach (var line in lines.Select(l => l.Trim()))
            {
                switch (state)
                {
                    case ChangeParseState.ChangeHeader:
                        changeList.Header = line;
                        state = ChangeParseState.ChangeComment;
                        break;

                    case ChangeParseState.ChangeComment:
                        if (line.StartsWith("Affected files ..."))
                        {
                            state = ChangeParseState.ChangeFilelist;
                        }
                        else
                        {
                            changeList.Comment += line;
                        }

                        break;

                    case ChangeParseState.ChangeFilelist:
                        var tokens = line.Split(' ');
                        if (tokens.Length == 3)
                        {
                            var file = new P4ChangeList.ChangeListFile { Operation = tokens[2] };
                            var tokens2 = tokens[1].Split('#');
                            if (tokens2.Length == 2)
                            {
                                file.File = tokens2[0];
                                file.Revision = int.Parse(tokens2[1]);
                                fileList.Add(file);
                            }
                        }

                        break;
                }
            }

            changeList.Files = (P4ChangeList.ChangeListFile[])fileList.ToArray(typeof(P4ChangeList.ChangeListFile));
            return changeList;
        }

        /// <summary>
        /// Make a file name 'pretty'.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <returns>
        /// The prettified file name <see cref="string"/>.
        /// </returns>
        public static string MakePrettyFileName(string fileName)
        {
            var elems = fileName.Split('/');
            var ret = String.Empty;
            var startIncludingPath = false;
            foreach (var e in elems)
            {
                if (e.Equals("Objects", StringComparison.OrdinalIgnoreCase)
                    || e.Equals("Waves", StringComparison.OrdinalIgnoreCase))
                {
                    startIncludingPath = true;
                }
                else if (e == elems[elems.Length - 1])
                {
                    ret += e;
                }
                else if (startIncludingPath)
                {
                    ret += e + "/";
                }
            }

            return ret;
        }

        /// <summary>
        /// Format a number of bytes into a readable size format.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The formatted size <see cref="string"/>.
        /// </returns>
        public static string FormatBytes(long input)
        {
            var sign = input < 0 ? "-" : string.Empty;
            var value = (double)input;
            if (value < 0)
            {
                value = value * -1;
            }

            // Default to KB (even if small number)
            value = value / 1024.0;
            var unit = "KB";

            if (value > 1024)
            {
                value = value / 1024.0;
                unit = "MB";
            }

            if (value > 1024)
            {
                value = value / 1024.0;
                unit = "GB";
            }

            return string.Format("{0}{1:0.00}{2}", sign, value, unit);
        }

        /// <summary>
        /// Format a decimal value.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="maxDecimalPlaces">
        /// The max decimal places.
        /// </param>
        /// <returns>
        /// The formatted value <see cref="string"/>.
        /// </returns>
        public static string FormatDecimalValue(double value, uint maxDecimalPlaces = 2)
        {
            if (maxDecimalPlaces == 0)
            {
                return ((int)value).ToString(CultureInfo.InvariantCulture);
            }

            var format = "{0:0.";
            for (var i = 0; i < maxDecimalPlaces; i++)
            {
                format += "0";
            }

            format += "}";

            var formatted = string.Format(format, value);
            formatted = formatted.TrimEnd('0');
            formatted = formatted.TrimEnd('.');
            return formatted;
        }

        /// <summary>
        /// Get platform from file.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <returns>
        /// The platform <see cref="string"/>.
        /// </returns>
        public static string GetPlatfromFromFile(P4ChangeList.ChangeListFile file, Settings commitWatcherSettings)
        {
            foreach (string platform in commitWatcherSettings.Platforms) {
                if (file.File.ToUpper().Contains(platform.ToUpper())) return platform;
            }
            throw new Exception("The platform for file "+file.File+" could not be found in the commit watcher settings!");
        }

        public static string GetPackNameFromAwcPath(string awcPath)
        {
            var upperPath = awcPath.ToUpper();
            if (!upperPath.Contains("SFX"))
            {
                return null;
            }

            upperPath = upperPath.Replace('/', '\\');
            var subPath = upperPath.Substring(upperPath.IndexOf("SFX") + 4);
            return subPath.Substring(0, subPath.IndexOf('\\'));
        }
    }
}
