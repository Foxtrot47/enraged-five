﻿// -----------------------------------------------------------------------
// <copyright file="WaveslotReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------


namespace CommitWatcher.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;
    using NLog;

    using global::CommitWatcher.Data;

    using Rockstar.AssetManager.Interfaces;

    using WaveSlotDataLib;

    /// <summary>
    /// Wave slot report.
    /// </summary>
    public class WaveslotReport
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The audio config paths.
        /// </summary>
        private readonly IDictionary<string, string> audioConfigPaths;

        /// <summary>
        /// The name table paths.
        /// </summary>
        private readonly IDictionary<string, string> nameTablePaths;

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveslotReport"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public WaveslotReport(Settings settings, IAssetManager assetManager)
        {
            this.settings = settings;
            this.assetManager = assetManager;
            this.audioConfigPaths = new Dictionary<string, string>();
            this.nameTablePaths = new Dictionary<string, string>();
        }

        /// <summary>
        /// Run the report generator.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <param name="changeList">
        /// The change List.
        /// </param>
        /// <param name="mailAddresses">
        /// The mail addresses to send report to.
        /// </param>
        public void Run(string filePath, P4ChangeList.ChangeList changeList, IList<MailAddress> mailAddresses)
        {
            if (!mailAddresses.Any())
            {
                // No one to send report to, no point generating it
                return;
            }

            logger.Info("Running waveslot report on " + filePath);

            try
            {
                var sr = new StreamReader(filePath);
                var elems = sr.ReadLine().Split(':');
                sr.Close();


                // Check that change list wasn't submitted by same P4 user as that running
                // the commit watcher (could result in infinite loop)
                var client = elems[0];
                if (client.ToUpper() == this.settings.P4User.ToUpper())
                {
                    return;
                }

                var sb = new StringBuilder();

                var email = new MailMessage
                {
                    From = this.settings.From,
                    Subject = string.Format("[{0}] Rave Commit by {1}", this.settings.Project, client),
                    IsBodyHtml = true
                };

                foreach (var mailAddress in mailAddresses)
                {
                    email.To.Add(mailAddress);
                }

                // Output header HTML
                sb.AppendLine("<html>");
                sb.AppendLine("<header>");
                sb.AppendLine("<style type=\"text/css\">");
                sb.AppendLine("<!--");
                sb.AppendLine("body, table");
                sb.AppendLine("{");
                sb.AppendLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
                sb.AppendLine("}");
                sb.AppendLine("div.indented");
                sb.AppendLine("{");
                sb.AppendLine("padding-left: 50pt;");
                sb.AppendLine("padding-right: 50pt;");
                sb.AppendLine("}");
                sb.AppendLine("table");
                sb.AppendLine("{");
                sb.AppendLine("border-collapse: collapse;");
                sb.AppendLine("margin: 8px 0px;");
                sb.AppendLine("}");
                sb.AppendLine("table td, table th");
                sb.AppendLine("{");
                sb.AppendLine("padding: 2px 15px;");
                sb.AppendLine("}");
                sb.AppendLine("table th");
                sb.AppendLine("{");
                sb.AppendLine("border-top: 1px solid #FB7A31;");
                sb.AppendLine("border-bottom: 1px solid #FB7A31;");
                sb.AppendLine("background: #FFC;");
                sb.AppendLine("font-weight: bold;");
                sb.AppendLine("}");
                sb.AppendLine("table td");
                sb.AppendLine("{");
                sb.AppendLine("border-bottom: 1px solid #CCC;");
                sb.AppendLine("padding: 0 0.5em;");
                sb.AppendLine("}");
                sb.AppendLine("-->");
                sb.AppendLine("</style>");
                sb.AppendLine("</header>");

                // Output body HTML
                sb.AppendLine("<body>");
                sb.AppendLine("<p>");
                sb.AppendLine("<strong>" + changeList.Header + "</strong>");
                sb.AppendLine("</p>");
                sb.AppendLine("<p>");
                sb.AppendLine(changeList.Comment);
                sb.AppendLine("</p>");

                bool newChanges;
                if (!this.DescribeWaveslotChanges(filePath, changeList, sb, mailAddresses, out newChanges))
                {
                    logger.Error("Failed to describe wave slot changes");
                    return;
                }

                if (!newChanges)
                {
                    return;
                }

                sb.AppendLine("</body>");
                sb.AppendLine("</html>");

                // Send email
                email.Body = sb.ToString();
                using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
                {
                    smtp.Send(email);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        /// <summary>
        /// Parse wave slots.
        /// </summary>
        /// <param name="doc">
        /// The document containing wave slots.
        /// </param>
        /// <returns>
        /// The list of waves slots info <see cref="List{T}"/>.
        /// </returns>
        private static List<WaveSlot.WaveslotInfo> ParseWaveslots(XDocument doc)
        {
            var slots = new List<WaveSlot.WaveslotInfo>();

            if (null == doc)
            {
                return slots;
            }

            foreach (var elem in doc.Element("WaveSlots").Elements())
            {
                if (!elem.Name.LocalName.Equals("Slot"))
                {
                    continue;
                }

                var info = new WaveSlot.WaveslotInfo();
                foreach (var child in elem.Elements())
                {
                    switch (child.Name.LocalName)
                    {
                        case "Name":
                            info.Name = child.Value;
                            break;
                        case "MaxHeaderSize":
                            info.HeaderSizeBytes = int.Parse(child.Attribute("value").Value);
                            break;
                        case "Size":
                            info.SizeBytes = int.Parse(child.Attribute("value").Value);
                            break;
                    }
                }

                slots.Add(info);
            }

            return slots;
        }

        /// <summary>
        /// Compute wave slot changes.
        /// </summary>
        /// <param name="oldSlots">
        /// The old slots.
        /// </param>
        /// <param name="newSlots">
        /// The new slots.
        /// </param>
        /// <returns>
        /// List of wave slot changes <see cref="List"/>.
        /// </returns>
        private static IEnumerable<WaveSlot.WaveslotChange> ComputeWaveslotChanges(
            List<WaveSlot.WaveslotInfo> oldSlots, List<WaveSlot.WaveslotInfo> newSlots)
        {
            var changes = new List<WaveSlot.WaveslotChange>();

            foreach (var oldSlot in oldSlots)
            {
                var newSlotNullable = FindSlot(newSlots, oldSlot.Name);
                if (newSlotNullable == null)
                {
                    // slot has been deleted
                    var change = new WaveSlot.WaveslotChange
                    {
                        SlotName = oldSlot.Name,
                        HeaderSizeDeltaBytes = -oldSlot.HeaderSizeBytes,
                        SizeDeltaBytes = -oldSlot.SizeBytes,
                        State = WaveSlot.WaveslotChange.ChangeState.Deleted
                    };
                    changes.Add(change);
                }
                else
                {
                    var newSlot = newSlotNullable.Value;
                    if (newSlot.SizeBytes != oldSlot.SizeBytes || newSlot.HeaderSizeBytes != oldSlot.HeaderSizeBytes)
                    {
                        // slot has been changed
                        var change = new WaveSlot.WaveslotChange
                        {
                            SlotName = oldSlot.Name,
                            HeaderSizeDeltaBytes = newSlot.HeaderSizeBytes - oldSlot.HeaderSizeBytes,
                            SizeDeltaBytes = newSlot.SizeBytes - oldSlot.SizeBytes,
                            State = WaveSlot.WaveslotChange.ChangeState.Existing,
                            SizeBytes = newSlot.SizeBytes
                        };
                        changes.Add(change);
                    }
                }
            }

            // add any new slots
            foreach (var newSlot in newSlots)
            {
                var oldSlotNullable = FindSlot(oldSlots, newSlot.Name);
                if (oldSlotNullable == null)
                {
                    // slot has been deleted
                    var change = new WaveSlot.WaveslotChange
                    {
                        SlotName = newSlot.Name,
                        HeaderSizeDeltaBytes = newSlot.HeaderSizeBytes,
                        SizeDeltaBytes = newSlot.SizeBytes,
                        SizeBytes = newSlot.SizeBytes,
                        State = WaveSlot.WaveslotChange.ChangeState.New
                    };
                    changes.Add(change);
                }
            }

            return changes;
        }

        /// <summary>
        /// Find a wave slot.
        /// </summary>
        /// <param name="slots">
        /// The slots.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The wave slot info <see cref="WaveSlot.WaveslotInfo"/>.
        /// </returns>
        private static WaveSlot.WaveslotInfo? FindSlot(IEnumerable<WaveSlot.WaveslotInfo> slots, string name)
        {
            foreach (var slot in slots.Where(slot => slot.Name.Equals(name)))
            {
                return slot;
            }

            return null;
        }

        /// <summary>
        /// Get the action from a state.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <returns>
        /// The action <see cref="string"/>.
        /// </returns>
        private static string GetActionFromState(WaveSlot.WaveslotChange.ChangeState state)
        {
            switch (state)
            {
                case WaveSlot.WaveslotChange.ChangeState.Existing:
                    return "Modified";

                case WaveSlot.WaveslotChange.ChangeState.New:
                    return "Added";

                case WaveSlot.WaveslotChange.ChangeState.Deleted:
                    return "Deleted";
            }

            return string.Empty;
        }

        /// <summary>
        /// Describe the wave slot changes.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="sb">
        /// The string builder.
        /// </param>
        /// <param name="mailAddresses">
        /// The mail addresses.
        /// </param>
        /// <param name="newChanges">
        /// Flag indicating if new changes are detected in wave slots.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool DescribeWaveslotChanges(string filePath, P4ChangeList.ChangeList changeList, StringBuilder sb, IList<MailAddress> mailAddresses, out bool newChanges)
        {
            newChanges = false;

            // Determine which platform(s) we need to check waveslots for
            var platforms = new HashSet<string>();
            foreach (var file in changeList.Files)
            {
                var fileUpper = file.File.ToUpper();
                if (fileUpper.Contains("WAVESLOTSETTINGS.XML"))
                {
                    foreach (string platform in settings.Platforms) platforms.Add(platform.ToUpper());
                }
                else
                {

                    foreach (string platform in settings.Platforms)
                    {
                        if (fileUpper.Contains(platform.ToUpper())) platforms.Add(platform.ToUpper());
                    }
                }
            }

            if (!this.InitPaths(platforms))
            {
                return false;
            }

            var changeListSyncAt = Path.GetFileName(filePath);
            if (!this.GetLatestData(changeListSyncAt, platforms))
            {
                return false;
            }

            // Check each platform's waveslot data
            foreach (var platform in platforms)
            {
                if (!this.settings.ProjectSettings.SetCurrentPlatformByTag(platform))
                {
                    logger.Warn("Failed to set platform by tag: " + platform);
                    continue;
                }

                var audioConfigPath = this.audioConfigPaths[platform];
                if (string.IsNullOrEmpty(audioConfigPath))
                {
                    return false;
                }

                if (!File.Exists(audioConfigPath))
                {
                    logger.Warn("Failed to find audio config file: " + audioConfigPath);
                    continue;
                }

                if (!this.GenerateMetadata(platform, changeListSyncAt))
                {
                    return false;
                }

                var prettyFileName = Utils.MakePrettyFileName(audioConfigPath);

                bool platformNewChanges;
                if (!this.DescribeWaveslotChanges(platform, audioConfigPath, prettyFileName, sb, mailAddresses, out platformNewChanges))
                {
                    return false;
                }

                newChanges = newChanges || platformNewChanges;
            }

            return true;
        }

        /// <summary>
        /// Get the latest data.
        /// </summary>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GetLatestData(string changeListNumber, IEnumerable<string> platforms)
        {
            foreach (string platform in platforms) 
            {
                string buildWavesPath = Regex.Replace(this.settings.BuiltWavePath, "{platform}", platform, RegexOptions.IgnoreCase);
                if (!this.assetManager.GetLatest(buildWavesPath, false, changeListNumber))
                {
                    logger.Error("Failed to get latest built waves data: " + buildWavesPath);
                    return false;
                }
            }

            //Get latest project settings as waveslot allocation might have changed. (change d: to x: for perforce to find the file) 
            this.assetManager.GetLatest(this.settings.ProjectSettingsPath.Replace("d:","x:"), false);
           
            if (!this.assetManager.GetLatest(this.settings.AudioConfigDataPath, false, changeListNumber))
            {
                logger.Error("Failed to get latest audio config data: " + this.settings.AudioConfigDataPath);
                return false;
            }

            if (!this.assetManager.GetLatest(this.settings.AudioDefinitionsPath, false, changeListNumber))
            {
                logger.Error("Failed to get latest audio definitions data: " + this.settings.AudioDefinitionsPath);
                return false;
            }

            if (!this.assetManager.GetLatest(this.settings.AssetBuildPath, false, changeListNumber))
            {
                logger.Error("Failed to get latest asset build data: " + this.settings.AssetBuildPath);
                return false;
            }

            if (!this.assetManager.GetLatest(this.settings.WaveSlotSettingsPath, false, changeListNumber))
            {
                logger.Error("Failed to get latest waveslot settings file: " + this.settings.WaveSlotSettingsPath);
                return false;
            }

            foreach (string platform in platforms) 
            {
                if (this.audioConfigPaths.ContainsKey(platform))
                {
                    if (!this.assetManager.GetLatest(this.audioConfigPaths[platform], false, changeListNumber))
                    {
                        logger.Error("Failed to get latest audio config path: " + audioConfigPaths[platform]);
                        return false;
                    }
                }
            }

            foreach (string platform in platforms) 
            {
                if (this.nameTablePaths.ContainsKey(platform))
                {
                    if (this.assetManager.ExistsAsAsset(this.nameTablePaths[platform], true)
                        && !this.assetManager.GetLatest(this.nameTablePaths[platform], false, changeListNumber))
                    {
                        logger.Error("Failed to get latest name table path: " + this.nameTablePaths[platform]);
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Initialize the paths.
        /// </summary>
        /// <param name="platforms">
        /// The platforms.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool InitPaths(IEnumerable<string> platforms)
        {
            this.audioConfigPaths.Clear();
            this.nameTablePaths.Clear();

            foreach (var platform in platforms)
            {
                var audioConfigPath = this.GetAudioConfigPath(platform);

                if (string.IsNullOrEmpty(audioConfigPath))
                {
                    return false;
                }

                this.audioConfigPaths[platform] = audioConfigPath;
                this.nameTablePaths[platform] = audioConfigPath + ".nametable";
            }

            return true;
        }

        /// <summary>
        /// Get the audio config path.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <returns>
        /// The audio config path <see cref="string"/>.
        /// </returns>
        private string GetAudioConfigPath(string platform)
        {
            // Get the schema version
            var typeDefLocalPath = this.assetManager.GetLocalPath(this.settings.SimpleTypesDefinitionsPath);
            var doc = XDocument.Load(typeDefLocalPath);
            var typeDefsElem = doc.Element("TypeDefinitions");
            if (null == typeDefsElem)
            {
                logger.Error("Failed to find TypeDefinitions element in XML: " + typeDefLocalPath);
                return null;
            }

            var versionAttr = typeDefsElem.Attribute("version");
            if (null == versionAttr)
            {
                logger.Error("Failed to find version attribute in TypeDefinitions Element: " + typeDefLocalPath);
                return null;
            }

            var version = versionAttr.Value;

            var runtimePath = Regex.Replace(
                this.settings.RuntimeConfigPath, "{platform}", platform, RegexOptions.IgnoreCase);
            var runtimeLocalPath = this.assetManager.GetLocalPath(runtimePath);
            var audioConfigPath = Path.Combine(runtimeLocalPath, "audioconfig.dat" + version);

            return audioConfigPath;
        }

        /// <summary>
        /// Generate the metadata.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="changeListSyncAt">
        /// The change list number to sync at.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GenerateMetadata(string platform, string changeListSyncAt)
        {
            var audioConfigPath = this.audioConfigPaths[platform];
            var nameTablePath = this.nameTablePaths[platform];

            var requiresAdd = true;
            var changeList = this.assetManager.CreateChangeList(settings.Project + "  " + platform + " AudioConfig Data Build @ CL " + changeListSyncAt);
           
            if (this.assetManager.ExistsAsAsset(audioConfigPath))
            {
                if (null == changeList.CheckoutAsset(audioConfigPath, true))
                {
                    logger.Error("Failed to checkout audio config file: " + audioConfigPath);
                    return false;
                }
                requiresAdd = false;
            }

            // checkout nametable file, if it exists
            if (File.Exists(nameTablePath))
            {
                if (null == changeList.CheckoutAsset(nameTablePath, true))
                {
                    logger.Error("Failed to checkout audio config nametable file: " + nameTablePath);
                    return false;
                }
            }

            // Run audioconfig data build
            if (!this.RunMetadataBuild(platform, changeListSyncAt))
            {
                changeList.Revert(true);
                return false;
            }

            // Commit changes
            if (requiresAdd)
            {
                changeList.MarkAssetForAdd(audioConfigPath);
            }

            changeList.RevertUnchanged();

            if (changeList.Assets.Any())
            {
                if (!changeList.Submit())
                {
                    // We don't return false here because it doesn't matter if
                    // we're unable to submit, as long as we have the changes locally
                    logger.Warn("Failed to commit pending change list " + changeList.NumberAsString);
                }
            }
            else
            {
                changeList.Delete();
            }

            return true;
        }

        /// <summary>
        /// Run the metadata build.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool RunMetadataBuild(string platform, string changeListNumber)
        {
            string localExePath = this.assetManager.GetLocalPath(this.settings.MetadataCompilerExePath);

            var projectSettingsPath = this.settings.ProjectSettingsPath;
            if (projectSettingsPath.ToUpper().StartsWith(this.settings.WorkingPath.ToUpper()))
            {
                projectSettingsPath = projectSettingsPath.Substring(this.settings.WorkingPath.Length);
            }

            var args = string.Format(
                "-project {0} -metadata AUDIOCONFIG -platform {1} -workingpath {2} -logformat XML -cl {3} -runtimemode dev",
                projectSettingsPath,
                platform,
                this.settings.WorkingPath,
                changeListNumber);

            try
            {
                ProcessStartInfo info = new ProcessStartInfo();
                info.FileName = localExePath;
                info.WorkingDirectory = Path.GetDirectoryName(localExePath);
                info.RedirectStandardOutput = true;
                info.UseShellExecute = false;
                info.Arguments = args;

                Process process = Process.Start(info);
                string metadataCompilerOutput = process.StandardOutput.ReadToEnd().Trim();
                process.WaitForExit();
                
                if (metadataCompilerOutput.Contains("<Exceptions>0</Exceptions>") && metadataCompilerOutput.Contains("<Errors>0</Errors>"))
                {
                    logger.Info(metadataCompilerOutput);
                }
                else 
                {
                    logger.Error("Metadata build failed" + Environment.NewLine + metadataCompilerOutput);
                }
                

                if (process.ExitCode != 0)
                {
                    logger.Info(process.StandardError.ReadToEnd().Trim());
                    logger.Error("Failed to run metadata build - exit code " + process.ExitCode);
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Failed to run metadata build: " + ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Describe the wave slot changes.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="newDocPath">
        /// The new doc path.
        /// </param>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <param name="sb">
        /// The string builder.
        /// </param>
        /// <param name="newChanges">
        /// Flag indicating if new changes are detected in wave slots.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool DescribeWaveslotChanges(string platform, string newDocPath, string fileName, StringBuilder sb, IList<MailAddress> mailAddresses, out bool newChanges)
        {
            // We keep a local version of audio config to compare future commits against for diffing
            var localAudioConfigPath = Path.Combine(Path.GetTempPath(), "LocalMetadataBuild",settings.Project, platform, "waveslots.xml");

            newChanges = false;
            //check if settings are set
            if (string.IsNullOrEmpty(settings.SimpleTypesDefinitionsPath)) throw new Exception("SimpleTypesDefinitionsPath is not configured");
            if (string.IsNullOrEmpty(settings.GenericBuiltWavesPackListFilePath)) throw new Exception("GenericBuiltWavesPackListFilePath is not configured");

            bool isBigEndian = true;
            foreach (rage.PlatformSetting platformsetting in this.settings.ProjectSettings.GetPlatformSettings())
            {
                if (platformsetting.PlatformTag.ToUpper().Equals(platform.ToUpper()))
                {
                    isBigEndian = platformsetting.IsBigEndian;
                }
            }
            var generator = new WaveSlotDataGenerator(assetManager.GetLocalPath(settings.SimpleTypesDefinitionsPath), assetManager.GetLocalPath(settings.GenericBuiltWavesPackListFilePath.Replace("{platform}", platform)), isBigEndian, newDocPath);
            var newDoc = generator.Run();
            this.CheckWaveSlotSizes(newDoc, platform, mailAddresses);

            if (!File.Exists(localAudioConfigPath))
            {
                // First time run - just copy audio config file for next run
                // Always replace local audio config file with latest version
                Directory.CreateDirectory(Path.GetDirectoryName(localAudioConfigPath));

                if (File.Exists(localAudioConfigPath))
                {
                    new FileInfo(localAudioConfigPath).IsReadOnly = false;
                }

                newDoc.Save(localAudioConfigPath);
            }
            else
            {
                var oldDoc = XDocument.Load(localAudioConfigPath);

                try
                {
                    sb.AppendLine("<p>Waveslot Changes:</p>");
                    sb.AppendLine("<div class=\"indented\">");
                    sb.AppendLine("<table>");
                    sb.AppendLine("<tr>");
                    sb.AppendLine("<th>Slot Name</th>");
                    sb.AppendLine("<th>Action</th>");
                    sb.AppendLine("<th>Platform</th>");
                    sb.AppendLine("<th>File Name</th>");
                    sb.AppendLine("<th>Size &#916;</th>");
                    sb.AppendLine("<th>Header &#916;</th>");
                    sb.AppendLine("</tr>");

                    var delta = 0;

                    var oldSlots = ParseWaveslots(oldDoc);
                    var newSlots = ParseWaveslots(newDoc);

                    var changes = ComputeWaveslotChanges(oldSlots, newSlots).ToList();
                    if (changes.Any())
                    {
                        newChanges = true;
                    }

                    foreach (var change in changes)
                    {
                        delta += change.SizeDeltaBytes;

                        var action = GetActionFromState(change.State);

                        sb.AppendLine("<tr>");
                        sb.AppendLine("<td>" + change.SlotName + "</td>");
                        sb.AppendLine("<td>" + action + "</td>");
                        sb.AppendLine("<td>" + platform + "</td>");
                        sb.AppendLine("<td>" + fileName + "</td>");
                        if (change.SizeDeltaBytes != 0)
                        {
                            sb.AppendLine("<td>" + Utils.FormatBytes(change.SizeDeltaBytes) + "</td>");
                        }
                        else
                        {
                            sb.AppendLine("<td>-</td>");
                        }

                        if (change.HeaderSizeDeltaBytes != 0)
                        {
                            sb.AppendLine("<td>" + Utils.FormatBytes(change.HeaderSizeDeltaBytes) + "</td>");
                        }
                        else
                        {
                            sb.AppendLine("<td>-</td>");
                        }

                        sb.AppendLine("</tr>");
                    }

                    sb.AppendLine("</table>");
                    sb.AppendLine("</div>");

                    sb.AppendLine(
                        "<p>Total <strong>" + platform + "</strong> waveslot size change: " + Utils.FormatBytes(delta));

                    var sizeBytes = newSlots.Where(slot => slot.Name != "TEST").Sum(slot => slot.SizeBytes);
                    sb.AppendLine(
                        "<p> Total <strong>" + platform + "</strong> waveslot size: " + Utils.FormatBytes(sizeBytes));
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                    return false;
                }
                finally
                {
                    // Always replace local audio config file with latest version
                    if (File.Exists(localAudioConfigPath))
                    {
                        new FileInfo(localAudioConfigPath).IsReadOnly = false;
                    }

                    newDoc.Save(localAudioConfigPath);
                }
            }

            return true;
        }

        /// <summary>
        /// Check the wave slot sizes.
        /// </summary>
        /// <param name="doc">
        /// The wave slot XML.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="mailAddresses">
        /// The mail addresses.
        /// </param>
        /// ///
        private void CheckWaveSlotSizes(XDocument doc, string platform, IList<MailAddress> mailAddresses)
        {
            var errors = string.Empty;

            foreach (var slotElem in doc.Descendants("Slot"))
            {
                var nameElem = slotElem.Element("Name");
                if (null == nameElem)
                {
                    throw new Exception("Failed to find Name element");
                }

                // Remove trailing "_XX" digits, if they exist
                var name = nameElem.Value.ToUpper();
                var shortName = Regex.Replace(name, "\\d*$", string.Empty).TrimEnd('_');

                var sizeElem = slotElem.Element("Size");
                if (null == sizeElem)
                {
                    throw new Exception("Failed to find Size element");
                }

                uint size;
                if (!uint.TryParse(sizeElem.Attribute("value").Value, out size))
                {
                    throw new Exception("Failed to parse Size value: " + sizeElem.Value);
                }

                // Find and check the max limit for slot, if one is set
                if (!this.settings.ProjectSettings.WaveSlotCheckerSettings.WaveSlotLimits.ContainsKey(platform.ToUpper()))
                {
                    throw new Exception("There is no Wave slot limit configured in the project settings for platform " + platform.ToUpper());
                }

                foreach (var kvp in this.settings.ProjectSettings.WaveSlotCheckerSettings.WaveSlotLimits[platform.ToUpper()])
                {
                    if (kvp.Key.ToUpper() != shortName)
                    {
                        continue;
                    }

                    if (size > kvp.Value)
                    {
                        var error = "WARNING! Slot size has been exceeded. Slot: " + name +
                                    " Limit: " + kvp.Value + " Size: " + size + Environment.NewLine;
                        errors += error;
                    }

                    break;
                }
            }

            if (!string.IsNullOrEmpty(errors))
            {
                errors += Environment.NewLine + "Check the Wave Slot Inspector for further info";
                var subject = string.Format("Audio Wave Slot Limit(s) Exceeded! - {0}", platform);
                this.EmailErrors(errors, subject, mailAddresses);
            }
        }

        /// <summary>
        /// Email details of errors.
        /// </summary>
        /// <param name="errors">
        /// The errors.
        /// </param>
        /// <param name="subject">
        /// The subject.
        /// </param>
        /// <param name="mailAddresses">
        /// The mail addresses.
        /// </param>
        private void EmailErrors(string errors, string subject, IEnumerable<MailAddress> mailAddresses)
        {
            var email = new MailMessage
            {
                From = this.settings.From,
                Subject = subject,
                IsBodyHtml = true
            };

            foreach (var mailAddress in mailAddresses)
            {
                email.To.Add(mailAddress);
            }

            // Preserve new line character for email format
            var body = Regex.Replace(errors, "\n", Environment.NewLine);
            email.Body = body;
            using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
            {
                smtp.Send(email);
            }
        }
    }
}
