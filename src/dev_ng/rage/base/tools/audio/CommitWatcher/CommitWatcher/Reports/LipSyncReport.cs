﻿// -----------------------------------------------------------------------
// <copyright file="LipSyncReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;
    using NLog;

    using global::CommitWatcher.Data;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Lip sync report.
    /// </summary>
    public class LipSyncReport
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The pack names.
        /// </summary>
        private string[] packNames = {"SPEECH"};

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The built wave depot paths.
        /// </summary>
        private readonly IDictionary<string, string> builtWaveDepotPaths;

        /// <summary>
        /// The built wave local paths.
        /// </summary>
        private readonly IDictionary<string, string> builtWaveLocalPaths;

        /// <summary>
        /// The wave elements missing lipsync tags for each platform.
        /// </summary>
        private readonly IDictionary<string, IList<XElement>> waveElemsMissingLipsync;

        /// <summary>
        /// The lipsync chunk elements.
        /// </summary>
        private readonly IDictionary<string, IList<XElement>> lipsyncChunkElems;

        /// <summary>
        /// Initializes a new instance of the <see cref="LipSyncReport"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public LipSyncReport(Settings settings, IAssetManager assetManager)
        {
            this.settings = settings;
            this.assetManager = assetManager;

            this.builtWaveDepotPaths = new Dictionary<string, string>();
            this.builtWaveLocalPaths = new Dictionary<string, string>();
            this.waveElemsMissingLipsync = new Dictionary<string, IList<XElement>>();
            this.lipsyncChunkElems = new Dictionary<string, IList<XElement>>();

            foreach (var platform in this.settings.Platforms)
            {
                var depotPath = Regex.Replace(this.settings.BuiltWavePath, "{platform}", platform, RegexOptions.IgnoreCase);
                this.builtWaveDepotPaths[platform] = depotPath;

                var localPath = this.assetManager.GetLocalPath(depotPath);

                if (null == localPath)
                {
                    throw new Exception("Failed to get local path: " + depotPath);
                }

                this.builtWaveLocalPaths[platform] = localPath;
            }
        }

        /// <summary>
        /// Run the report generator.
        /// </summary>
        /// <param name="filePath">
        /// The file Path.
        /// </param>
        /// <param name="changeList">
        /// The change List.
        /// </param>
        /// <param name="mailAddresses">
        /// The mail addresses to send report to.
        /// </param>
        public void Run(string filePath, P4ChangeList.ChangeList changeList, IList<MailAddress> mailAddresses)
        {
            if (!mailAddresses.Any())
            {
                // No one to send report to, no point generating it
                return;
            }

            logger.Info("Running lipsync report");

            if (!this.GetLatestData())
            {
                logger.Error("Failed to get latest data");
                return;
            }

            // Load the built waves data
            if (!this.LoadData())
            {
                logger.Error("Failed to load data");
                return;
            }

            var htmlBody = this.GenerateReport();
            this.SendEmail(mailAddresses, htmlBody);
        }

        /// <summary>
        /// Get latest data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool GetLatestData()
        {
            foreach (var path in this.builtWaveDepotPaths.Values)
            {
                if (!this.assetManager.GetLatest(path, true))
                {
                    logger.Error("Failed to get latest: " + path);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Load the data.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadData()
        {
            foreach (var localPathEntry in this.builtWaveLocalPaths)
            {
                foreach (var packName in this.packNames)
                {
                    var platform = localPathEntry.Key;
                    var path = localPathEntry.Value + packName + "_PACK_FILE.XML";

                    logger.Info("Loading data: " + path);

                    this.waveElemsMissingLipsync[platform] = new List<XElement>();
                    this.lipsyncChunkElems[platform] = new List<XElement>();

                    var doc = XDocument.Load(path);
                    
                    foreach (var waveElem in doc.Root.Descendants("Wave"))
                    {
                        var lipsyncChunkElem =
                            waveElem.Descendants("Chunk").FirstOrDefault(chunk => chunk.Attribute("name").Value.ToUpper() == "LIPSYNC");

                        if (null == lipsyncChunkElem)
                        {
                            this.waveElemsMissingLipsync[platform].Add(waveElem);
                        }
                        else
                        {
                            this.lipsyncChunkElems[platform].Add(lipsyncChunkElem);
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Generate the report.
        /// </summary>
        /// <returns>
        /// The report HTML <see cref="string"/>.
        /// </returns>
        private string GenerateReport()
        {
            var builder = new StringBuilder();

            builder.AppendLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            builder.AppendLine("<html>");
            builder.AppendLine("<head>");
            builder.AppendLine("<style type=\"text/css\">");
            builder.AppendLine("<!--");
            builder.AppendLine("body, table");
            builder.AppendLine("{");
            builder.AppendLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
            builder.AppendLine("}");
            builder.AppendLine("div.indented");
            builder.AppendLine("{");
            builder.AppendLine("padding-left: 50pt;");
            builder.AppendLine("padding-right: 50pt;");
            builder.AppendLine("}");
            builder.AppendLine("table");
            builder.AppendLine("{");
            builder.AppendLine("border-collapse: collapse;");
            builder.AppendLine("margin: 8px 0px;");
            builder.AppendLine("}");
            builder.AppendLine("table td, table th");
            builder.AppendLine("{");
            builder.AppendLine("padding: 2px 15px;");
            builder.AppendLine("}");
            builder.AppendLine("table th");
            builder.AppendLine("{");
            builder.AppendLine("border-top: 1px solid #FB7A31;");
            builder.AppendLine("border-bottom: 1px solid #FB7A31;");
            builder.AppendLine("background: #FFC;");
            builder.AppendLine("font-weight: bold;");
            builder.AppendLine("}");
            builder.AppendLine("table td");
            builder.AppendLine("{");
            builder.AppendLine("border-bottom: 1px solid #CCC;");
            builder.AppendLine("padding: 0 0.5em;");
            builder.AppendLine("}");
            builder.AppendLine("-->");
            builder.AppendLine("</style>");
            builder.AppendLine("</head>");
            builder.AppendLine("<body>");

            // Waves missing lipsync chunk
            foreach (var entry in this.waveElemsMissingLipsync)
            {
                var platform = entry.Key;
                var waveElems = entry.Value;

                if (!waveElems.Any())
                {
                    continue;
                }

                builder.AppendLine("<p>Waves Missing Lipsync Chunk - " + platform + ":</p>");
                builder.AppendLine("<table>");
                builder.AppendLine("<tr>");
                builder.AppendLine("<th>Pack</th>");
                builder.AppendLine("<th>Bank</th>");
                builder.AppendLine("<th>Wave</th>");
                builder.AppendLine("</tr>");
                
                foreach (var waveElem in waveElems)
                {
                    var waveNameAttr = waveElem.Attribute("name");
                    var bankNode = this.GetParentNode(waveElem, "Bank");
                    var packNode = this.GetParentNode(bankNode, "Pack");
                    var bankNameAttr = bankNode.Attribute("name");
                    var packNameAttr = packNode.Attribute("name");

                    builder.AppendLine("<tr>");
                    builder.AppendLine("<td>" + packNameAttr.Value + "</td>");
                    builder.AppendLine("<td>" + bankNameAttr.Value + "</td>");
                    builder.AppendLine("<td>" + waveNameAttr.Value + "</td>");
                    builder.AppendLine("</tr>");
                }

                builder.AppendLine("</table>");
            }

            // Overall lipsync stats
            builder.AppendLine("<p>Lipsync Tag Stats</p>");
            builder.AppendLine("<table>");
            builder.AppendLine("<tr>");
            builder.AppendLine("<th>Platform</th>");
            builder.AppendLine("<th>Total Tags</th>");
            builder.AppendLine("<th>Total Size</th>");
            builder.AppendLine("<th>Mean Size</th>");
            builder.AppendLine("<th>Min Size</th>");
            builder.AppendLine("<th>Max Size</th>");
            builder.AppendLine("</tr>");

            foreach (var entry in this.lipsyncChunkElems)
            {
                var platform = entry.Key;
                var lipsyncChunks = entry.Value;

                var min = 0U;
                var max = 0U;
                var sum = 0U;

                foreach (var chunk in lipsyncChunks)
                {
                    var currSize = uint.Parse(chunk.Attribute("size").Value);
                    sum += currSize;
                    min = min == 0 ? currSize : Math.Min(min, currSize);
                    max = Math.Max(max, currSize);
                }

                var mean = sum / lipsyncChunks.Count;

                builder.AppendLine("<tr>");
                builder.AppendLine("<td>" + platform + "</td>");
                builder.AppendLine("<td>" + lipsyncChunks.Count + "</td>");
                builder.AppendLine("<td>" + sum + "</td>");
                builder.AppendLine("<td>" + mean + "</td>");
                builder.AppendLine("<td>" + min + "</td>");
                builder.AppendLine("<td>" + max + "</td>");
                builder.AppendLine("</tr>");
            }

            builder.AppendLine("</table>");

            builder.AppendLine("</body>");
            builder.AppendLine("</html>");

            return builder.ToString();
        }

        /// <summary>
        /// Get parent node with specified name,
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="nodeName">
        /// The node name.
        /// </param>
        /// <returns>
        /// The found node <see cref="XElement"/>.
        /// </returns>
        private XElement GetParentNode(XElement element, string nodeName)
        {
            var currElem = element.Parent;
            while (null != currElem)
            {
                if (currElem.Name == nodeName)
                {
                    return currElem;
                }

                currElem = currElem.Parent;
            }

            return null;
        }

        /// <summary>
        /// Send email.
        /// </summary>
        /// <param name="mailAddresses">
        /// The mail addresses.
        /// </param>
        /// <param name="htmlBody">
        /// The body.
        /// </param>
        private void SendEmail(IEnumerable<MailAddress> mailAddresses, string htmlBody)
        {
            var email = new MailMessage
            {
                From = this.settings.From,
                Subject = string.Format("[{0}] Lipsync Report", this.settings.Project),
                IsBodyHtml = true
            };

            foreach (var mailAddress in mailAddresses)
            {
                email.To.Add(mailAddress);
            }

            email.Body = htmlBody;
            using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
            {
                smtp.Send(email);
            }
        }
    }
}
