﻿namespace CommitWatcher.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using global::CommitWatcher.Enums;

    /// <summary>
    /// The P4 wave list.
    /// </summary>
    public class P4WaveList
    {
        /// <summary>
        /// The wave list.
        /// </summary>
        private readonly Dictionary<string, P4Wave> waveList;

        /// <summary>
        /// Initializes a new instance of the <see cref="P4WaveList"/> class.
        /// </summary>
        /// <param name="fileList">
        /// The file list.
        /// </param>
        /// <param name="assetRootDepotPath">
        /// The asset root depot path.
        /// </param>
        public P4WaveList(string fileList, string assetRootDepotPath)
        {
            this.waveList = new Dictionary<string, P4Wave>(StringComparer.OrdinalIgnoreCase);

            var lines = fileList.Split('\n');
            foreach (var rawLine in lines)
            {
                var line = rawLine.Trim();
                if (line.Length == 0)
                {
                    continue;
                }

                var hashIndex = line.IndexOf('#');
                if (hashIndex == -1)
                {
                    Console.Error.WriteLine("Unexpected output from p4: '{0}' (missing #)", line);
                    continue;
                }

                var file = line.Substring(0, hashIndex);
                var endOfLine = line.Substring(hashIndex);

                var tokens = endOfLine.Split(' ');
                if (tokens.Length != 6)
                {
                    Console.Error.WriteLine("Unexpected output from p4: '{0}' (expected 6 tokens from '{1}'", line, endOfLine);
                    continue;
                }

                if (!file.StartsWith(assetRootDepotPath, StringComparison.CurrentCultureIgnoreCase))
                {
                    Console.Error.WriteLine("Unexpected output from p4: '{0}' (expected depot path to start with '{1}')", line, assetRootDepotPath);
                    continue;
                }

                if (!IsWaveAsset(file))
                {
                    // we only care about wave files
                    continue;
                }

                var assetName = file.Substring(assetRootDepotPath.Length);
                
                var stateString = tokens[2].Replace('/', '_');
                var state = (P4State)Enum.Parse(typeof(P4State), stateString, true);

                this.waveList.Add(assetName, new P4Wave(file, assetName, state));
            }
        }

        /// <summary>
        /// Find a P4 asset.
        /// </summary>
        /// <param name="assetName">
        /// The asset name.
        /// </param>
        /// <returns>
        /// The P4 wave if found, null otherwise <see cref="P4Wave"/>.
        /// </returns>
        public P4Wave Find(string assetName)
        {
            return this.waveList.ContainsKey(assetName) ? this.waveList[assetName] : null;
        }

        /// <summary>
        /// Get a hashset of asset names.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public HashSet<string> GetAssetNames()
        {
            var assetNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (var wave in this.waveList.Values)
            {
                if (wave.State != P4State.Delete && wave.State != P4State.Move_Delete)
                {
                    assetNames.Add(wave.AssetName);
                }
            }

            return assetNames;
        }

        /// <summary>
        /// Get flag indicating if asset a wave asset.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <returns>
        /// Flag indicating if asset is a wave asset <see cref="bool"/>.
        /// </returns>
        private static bool IsWaveAsset(string fileName)
        {
            string[] extensions = { ".wav", ".mid" };
            return extensions.Any(s => fileName.EndsWith(s, StringComparison.CurrentCultureIgnoreCase));
        }
    }
}
