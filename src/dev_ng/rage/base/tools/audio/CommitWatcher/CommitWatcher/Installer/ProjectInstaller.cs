// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProjectInstaller.cs" company="">
//   
// </copyright>
// <summary>
//   The project installer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CommitWatcher.Installer
{
    using System.ComponentModel;
    using System.Configuration.Install;
    using System.ServiceProcess;

    /// <summary>
    /// The project installer.
    /// </summary>
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        #region Fields

        /// <summary>
        /// The process.
        /// </summary>
        private readonly ServiceProcessInstaller process;

        /// <summary>
        /// The service.
        /// </summary>
        private readonly ServiceInstaller service;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectInstaller"/> class.
        /// </summary>
        public ProjectInstaller()
        {
            this.InitializeComponent();
            this.process = new ServiceProcessInstaller
                {
                    Account = ServiceAccount.LocalSystem
                };

            this.service = new ServiceInstaller
                {
                    ServiceName = "Commit Watcher"
                };

            this.Installers.Add(this.process);
            this.Installers.Add(this.service);
        }

        #endregion
    }
}