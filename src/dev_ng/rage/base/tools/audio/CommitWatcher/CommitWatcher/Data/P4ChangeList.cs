﻿// -----------------------------------------------------------------------
// <copyright file="P4ChangeList.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Data
{
    /// <summary>
    /// P4 Change List data structures.
    /// </summary>
    public class P4ChangeList
    {
        /// <summary>
        /// Change list struct.
        /// </summary>
        public struct ChangeList
        {
            #region Fields

            /// <summary>
            /// The change list number.
            /// </summary>
            public string ChangeListNumber;

            /// <summary>
            /// The comment.
            /// </summary>
            public string Comment;

            /// <summary>
            /// The files.
            /// </summary>
            public ChangeListFile[] Files;

            /// <summary>
            /// The header.
            /// </summary>
            public string Header;

            #endregion
        }

        /// <summary>
        /// Change list file struct.
        /// </summary>
        public struct ChangeListFile
        {
            #region Fields

            /// <summary>
            /// The file.
            /// </summary>
            public string File;

            /// <summary>
            /// The operation.
            /// </summary>
            public string Operation;

            /// <summary>
            /// The revision.
            /// </summary>
            public int Revision;

            #endregion
        }
    }
}
