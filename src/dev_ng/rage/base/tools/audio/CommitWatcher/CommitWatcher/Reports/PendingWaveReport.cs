﻿// -----------------------------------------------------------------------
// <copyright file="PendingWaveReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using NLog;

    using global::CommitWatcher.Data;

    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Perforce;
    using Rockstar.AssetManager.Infrastructure.Enums;

    /// <summary>
    /// Collision settings report.
    /// </summary>
    public class PendingWaveReport
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;


        /// <summary>
        /// Initializes a new instance of the <see cref="PendingWaveReport"/> class.
        /// </summary>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public PendingWaveReport(Settings settings, IAssetManager assetManager)
        {
            this.assetManager = assetManager;
            this.settings = settings;
        }

        /// <summary>
        /// Run the pending wave report.
        /// </summary>
        /// <param name="clFiles">
        /// The change list files.
        /// </param>
        public void Run(Data.P4ChangeList.ChangeListFile[] clFiles, IList<MailAddress> mailAddresses, String clNumber, String[] platforms)
        {
            if (clFiles.Length == 0)
            {
                return;
            }

            if (!this.RequiresRun(clFiles))
            {
                return;
            }

            int numPlatforms = platforms.Length;

            List<String> filesThatHaveShrunk = new List<String>();
            List<String>[] addedOrModifiedFiles = new List<String>[numPlatforms];
            List<String>[] deletedFiles = new List<String>[numPlatforms];
            List<String>[] outputCurrPrintLinesTotal = new List<String>[numPlatforms];
            List<String>[] outputPrevPrintLinesTotal = new List<String>[numPlatforms];

            for (int i = 0; i < numPlatforms; i++)
            {
                addedOrModifiedFiles[i] = new List<String>();
                deletedFiles[i] = new List<String>();
                outputCurrPrintLinesTotal[i] = new List<String>();
                outputPrevPrintLinesTotal[i] = new List<String>();

                foreach (Data.P4ChangeList.ChangeListFile file in clFiles)
                {
                    if (!file.File.ToUpper().Contains(".WAV") && !file.File.ToUpper().Contains(".GESTURE"))
                        continue;

                    int currentRevision = file.Revision;

                    List<String> args = new List<String>();
                    args.Add("-Ol");
                    args.Add(file.File + "#" + file.Revision);
                    var requestNew = new P4RunCmdRequest("fstat", args);
                    this.assetManager.RequestHandler.ActionRequest(requestNew);

                    if (requestNew.Status != RequestStatus.Failed)
                    {
                        if (requestNew.RecordSet.Records[0].Fields["headAction"] == "add" || requestNew.RecordSet.Records[0].Fields["headAction"] == "modify")
                        {
                            addedOrModifiedFiles[i].Add(Path.GetFileName(file.File).ToUpper());
                        }
                        else if (requestNew.RecordSet.Records[0].Fields["headAction"] == "delete")
                        {
                            deletedFiles[i].Add(Path.GetFileName(file.File).ToUpper());
                        }
                    }
                }

                foreach (Data.P4ChangeList.ChangeListFile file in clFiles)
                {
                    if ((!file.File.ToUpper().Contains("\\PENDINGWAVES\\") && !file.File.ToUpper().Contains("/PENDINGWAVES/")) ||
                        (!file.File.ToUpper().Contains("\\" + platforms[i] + "\\") && !file.File.ToUpper().Contains("/" + platforms[i] + "/")))
                        continue;

                    int currentRevision = file.Revision;
                    int lastRevision = currentRevision - 1;

                    if (lastRevision > 0)
                    {
                        List<String> args = new List<String>();
                        args.Add("-Ol");
                        args.Add(file.File + "#" + file.Revision);
                        var requestNew = new P4RunCmdRequest("fstat", args);
                        this.assetManager.RequestHandler.ActionRequest(requestNew);

                        args.Clear();
                        args.Add("-Ol");
                        args.Add(file.File + "#" + (file.Revision - 1).ToString());
                        var requestPrevious = new P4RunCmdRequest("fstat", args);
                        this.assetManager.RequestHandler.ActionRequest(requestPrevious);
                        if (requestNew.Status != RequestStatus.Failed && requestPrevious.Status != RequestStatus.Failed)
                        {
                            if (requestPrevious.RecordSet.Records[0].Fields["headAction"] != "delete" && requestNew.RecordSet.Records[0].Fields["headAction"] != "delete")
                            {
                                int newFileSize = requestNew.RecordSet.Records[0].Fields["fileSize"] != String.Empty ? int.Parse(requestNew.RecordSet.Records[0].Fields["fileSize"]) : 0;
                                int previousFileSize = requestNew.RecordSet.Records[0].Fields["fileSize"] != String.Empty ? int.Parse(requestPrevious.RecordSet.Records[0].Fields["fileSize"]) : 0;
                                if (newFileSize != 0 && newFileSize < previousFileSize)
                                {
                                    filesThatHaveShrunk.Add(file.File);
                                }

                                ProcessStartInfo procInfo = new ProcessStartInfo("p4");
                                procInfo.RedirectStandardOutput = true;
                                procInfo.UseShellExecute = false;
                                procInfo.CreateNoWindow = true;
                                procInfo.WindowStyle = ProcessWindowStyle.Hidden;

                                procInfo.Arguments = "print " + file.File + "#" + currentRevision.ToString();

                                Process procPrintCurr = new Process();
                                procPrintCurr.StartInfo = procInfo;
                                procPrintCurr.Start();
                                String currentRevisionPrintOutput = procPrintCurr.StandardOutput.ReadToEnd();
                                procPrintCurr.WaitForExit();

                                procInfo.Arguments = "print " + file.File + "#" + lastRevision.ToString();

                                Process procPrintPrev = new Process();
                                procPrintPrev.StartInfo = procInfo;
                                procPrintPrev.Start();
                                String previousRevisionPrintOutput = procPrintPrev.StandardOutput.ReadToEnd();
                                procPrintPrev.WaitForExit();

                                List<String> outputCurrPrintLines = Regex.Split(currentRevisionPrintOutput, "\r\n").ToList();
                                List<String> outputPrevPrintLines = Regex.Split(previousRevisionPrintOutput, "\r\n").ToList();

                                int numCurrPrintLines = outputCurrPrintLines.Count();
                                int numPrevPrintLines = outputPrevPrintLines.Count();
                                for (int currLineIndex = 0; currLineIndex < numCurrPrintLines; currLineIndex++)
                                {
                                    String currLine = outputCurrPrintLines[currLineIndex];
                                    Match currWaveMatch = Regex.Match(currLine, "Wave name=\"(.*?)\".*?operation");
                                    if (currWaveMatch.Success)
                                    {
                                        String waveMatchValue = currWaveMatch.Groups[1].Value;
                                        bool found = false;
                                        for (int prevLineIndex = 0; prevLineIndex < numPrevPrintLines && !found; prevLineIndex++)
                                        {
                                            String prevLine = outputPrevPrintLines[prevLineIndex];
                                            Match prevWaveMatch = Regex.Match(prevLine, "Wave name=\"(.*?)\".*?operation");
                                            if (prevWaveMatch.Success && waveMatchValue == prevWaveMatch.Groups[1].Value)
                                            {
                                                outputCurrPrintLines.Remove(currLine);
                                                outputPrevPrintLines.Remove(prevLine);
                                                numCurrPrintLines--;
                                                numPrevPrintLines--;
                                                currLineIndex--;
                                                found = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        outputCurrPrintLines.Remove(currLine);
                                        numCurrPrintLines--;
                                        currLineIndex--;
                                    }
                                }

                                numCurrPrintLines = outputCurrPrintLines.Count();
                                numPrevPrintLines = outputPrevPrintLines.Count();
                                for (int prevLineIndex = 0; prevLineIndex < numPrevPrintLines; prevLineIndex++)
                                {
                                    String prevLine = outputPrevPrintLines[prevLineIndex];
                                    Match prevWaveMatch = Regex.Match(prevLine, "Wave name=\"(.*?)\".*?operation");
                                    if (prevWaveMatch.Success)
                                    {
                                        String waveMatchValue = prevWaveMatch.Groups[1].Value;
                                        bool found = false;
                                        for (int currLineIndex = 0; currLineIndex < numCurrPrintLines && !found; currLineIndex++)
                                        {
                                            String currLine = outputCurrPrintLines[currLineIndex];
                                            Match currWaveMatch = Regex.Match(currLine, "Wave name=\"(.*?)\".*?operation");
                                            if (currWaveMatch.Success && waveMatchValue == currWaveMatch.Groups[1].Value)
                                            {
                                                outputCurrPrintLines.Remove(currLine);
                                                outputPrevPrintLines.Remove(prevLine);
                                                numCurrPrintLines--;
                                                numPrevPrintLines--;
                                                currLineIndex--;
                                                found = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        outputPrevPrintLines.Remove(prevLine);
                                        numPrevPrintLines--;
                                        prevLineIndex--;
                                    }
                                }

                                numCurrPrintLines = outputCurrPrintLines.Count();
                                for (int currLineIndex = 0; currLineIndex < numCurrPrintLines; currLineIndex++)
                                {
                                    String currLine = outputCurrPrintLines[currLineIndex];
                                    Match currWaveMatch = Regex.Match(currLine, "Wave name=\"(.*?)\".*?operation");
                                    if (currWaveMatch.Success)
                                    {
                                        bool noMatch = true;
                                        foreach (Data.P4ChangeList.ChangeListFile waveOrGestureFile in clFiles)
                                        {
                                            if (waveOrGestureFile.File.ToUpper().Contains(currWaveMatch.Groups[1].Value.ToUpper().Replace(".WAV", "")))
                                            {
                                                noMatch = false;
                                            }
                                        }
                                        if (!noMatch)
                                        {
                                            outputCurrPrintLines.Remove(currLine);
                                            numCurrPrintLines--;
                                            currLineIndex--;
                                        }
                                    }
                                }

                                numPrevPrintLines = outputPrevPrintLines.Count();
                                for (int prevLineIndex = 0; prevLineIndex < numPrevPrintLines; prevLineIndex++)
                                {
                                    String prevLine = outputPrevPrintLines[prevLineIndex];
                                    Match prevWaveMatch = Regex.Match(prevLine, "Wave name=\"(.*?)\".*?operation");
                                    if (prevWaveMatch.Success)
                                    {
                                        bool noMatch = true;
                                        foreach (Data.P4ChangeList.ChangeListFile waveOrGestureFile in clFiles)
                                        {
                                            if (waveOrGestureFile.File.ToUpper().Contains(prevWaveMatch.Groups[1].Value.ToUpper().Replace(".WAV", "")))
                                            {
                                                noMatch = false;
                                            }
                                        }
                                        if (!noMatch)
                                        {
                                            outputPrevPrintLines.Remove(prevLine);
                                            numPrevPrintLines--;
                                            prevLineIndex--;
                                        }
                                    }
                                }

                                foreach (String leftoverCurr in outputCurrPrintLines)
                                {
                                    outputCurrPrintLinesTotal[i].Add(leftoverCurr);
                                }
                                foreach (String leftoverPrev in outputPrevPrintLines)
                                {
                                    outputPrevPrintLinesTotal[i].Add(leftoverPrev);
                                }
                            }

                            if (requestPrevious.RecordSet.Records[0].Fields["headAction"] == "delete")
                            {
                                RemoveWavesInCurrentRevisionFromLists(file.File, currentRevision, ref addedOrModifiedFiles[i], ref deletedFiles[i]);
                            }
                            else
                            {
                                //diff the files
                                ProcessStartInfo procInfo = new ProcessStartInfo("p4");
                                procInfo.RedirectStandardOutput = true;
                                procInfo.UseShellExecute = false;
                                procInfo.CreateNoWindow = true;
                                procInfo.WindowStyle = ProcessWindowStyle.Hidden;

                                procInfo.Arguments = "diff2 " + file.File + "#" + currentRevision.ToString() + " " + file.File + "#" + lastRevision.ToString();

                                Process proc = new Process();
                                proc.StartInfo = procInfo;
                                proc.Start();
                                String diffOutput = proc.StandardOutput.ReadToEnd();
                                proc.WaitForExit();

                                String[] outputLines = Regex.Split(diffOutput, "\r\n");
                                foreach (String line in outputLines)
                                {
                                    if (line.Length > 0 && line[0] == '<')
                                    {
                                        String waveAddMatchValue = String.Empty;
                                        String waveModifyMatchValue = String.Empty;
                                        String waveRemoveMatchValue = String.Empty;
                                        Match waveAddMatch = Regex.Match(line, "Wave name=\"(.*?)\".*?operation=\"add\"");
                                        if (waveAddMatch.Success)
                                        {
                                            waveAddMatchValue = waveAddMatch.Groups[1].Value;
                                            addedOrModifiedFiles[i].Remove(waveAddMatchValue.ToUpper());
                                            addedOrModifiedFiles[i].Remove(waveAddMatchValue.ToUpper().Replace(".WAV", ".GESTURE"));
                                        }
                                        else
                                        {
                                            Match waveModifyMatch = Regex.Match(line, "Wave name=\"(.*?)\".*?operation=\"modify\"");
                                            if (waveModifyMatch.Success)
                                            {
                                                waveModifyMatchValue = waveModifyMatch.Groups[1].Value;
                                                addedOrModifiedFiles[i].Remove(waveModifyMatchValue.ToUpper());
                                                addedOrModifiedFiles[i].Remove(waveModifyMatchValue.ToUpper().Replace(".WAV", ".GESTURE"));
                                            }
                                            else
                                            {
                                                Match waveRemoveMatch = Regex.Match(line, "Wave name=\"(.*?)\".*?operation=\"remove\"");
                                                if (waveRemoveMatch.Success)
                                                {
                                                    waveRemoveMatchValue = waveRemoveMatch.Groups[1].Value;
                                                    deletedFiles[i].Remove(waveRemoveMatchValue.ToUpper());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RemoveWavesInCurrentRevisionFromLists(file.File, currentRevision, ref addedOrModifiedFiles[i], ref deletedFiles[i]);
                        }
                    }
                    else
                    {
                        RemoveWavesInCurrentRevisionFromLists(file.File, currentRevision, ref addedOrModifiedFiles[i], ref deletedFiles[i]);
                    }
                }
            }

            logger.Info("Running pending wave report");

            for (int i = 0; i < numPlatforms; i++)
            {
                for (int index = 0; index < addedOrModifiedFiles[i].Count(); index++)
                {
                    if (!addedOrModifiedFiles[i][index].ToUpper().Contains(".WAV"))
                    {
                        addedOrModifiedFiles[i].Remove(addedOrModifiedFiles[i][index]);
                        index--;
                    }
                }

                for (int index = 0; index < deletedFiles[i].Count(); index++)
                {
                    if (!deletedFiles[i][index].ToUpper().Contains(".WAV"))
                    {
                        deletedFiles[i].Remove(deletedFiles[i][index]);
                        index--;
                    }
                }
            }

            bool shouldSendEmail = filesThatHaveShrunk.Count() != 0;
            for (int i = 0; i < numPlatforms && !shouldSendEmail; i++)
            {
                shouldSendEmail |= addedOrModifiedFiles[i].Count != 0;
                shouldSendEmail |= deletedFiles[i].Count != 0;
                shouldSendEmail |= outputCurrPrintLinesTotal[i].Count != 0;
                shouldSendEmail |= outputPrevPrintLinesTotal[i].Count != 0;
            }

            if (shouldSendEmail)
            {
                try
                {
                    var sb = new StringBuilder();

                    var email = new MailMessage
                    {
                        From = this.settings.From,
                        Subject = string.Format("PendingWave file commit error report for CL " +  clNumber),
                        IsBodyHtml = true
                    };

                    foreach (var mailAddress in mailAddresses)
                    {
                        email.To.Add(mailAddress);
                    }

                    // Output header HTML
                    sb.AppendLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
                    sb.AppendLine("<html>");
                    sb.AppendLine("<head>");
                    sb.AppendLine("<style type=\"text/css\">");
                    sb.AppendLine("<!--");
                    sb.AppendLine("body, table");
                    sb.AppendLine("{");
                    sb.AppendLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
                    sb.AppendLine("}");
                    sb.AppendLine("div.indented");
                    sb.AppendLine("{");
                    sb.AppendLine("padding-left: 50pt;");
                    sb.AppendLine("padding-right: 50pt;");
                    sb.AppendLine("}");
                    sb.AppendLine("table");
                    sb.AppendLine("{");
                    sb.AppendLine("border-collapse: collapse;");
                    sb.AppendLine("margin: 8px 0px;");
                    sb.AppendLine("}");
                    sb.AppendLine("table td, table th");
                    sb.AppendLine("{");
                    sb.AppendLine("padding: 2px 15px;");
                    sb.AppendLine("}");
                    sb.AppendLine("table th");
                    sb.AppendLine("{");
                    sb.AppendLine("border-top: 1px solid #FB7A31;");
                    sb.AppendLine("border-bottom: 1px solid #FB7A31;");
                    sb.AppendLine("background: #FFC;");
                    sb.AppendLine("font-weight: bold;");
                    sb.AppendLine("}");
                    sb.AppendLine("table td");
                    sb.AppendLine("{");
                    sb.AppendLine("border-bottom: 1px solid #CCC;");
                    sb.AppendLine("padding: 0 0.5em;");
                    sb.AppendLine("}");
                    sb.AppendLine("-->");
                    sb.AppendLine("</style>");
                    sb.AppendLine("</head>");

                    // Output body HTML
                    sb.AppendLine("<body>");

                    if (filesThatHaveShrunk.Count() != 0)
                    {
                        sb.AppendLine("<p>");
                        sb.AppendLine("The following pendingwave files have gone down in size:");
                        sb.AppendLine("<br>");

                        foreach (string shrunkFiles in filesThatHaveShrunk)
                        {
                            sb.AppendLine(shrunkFiles);
                            sb.AppendLine("<br>");
                        }
                    }

                    for(int i=0; i<numPlatforms; i++)
                    {
                        if (addedOrModifiedFiles[i].Count != 0)
                        {
                            sb.AppendLine("<p>");
                            sb.AppendLine("The following wave files have been added/modified with no match in the pendingWave file:");
                            sb.AppendLine("<br>");
                            sb.AppendLine("PLATFORM: " + platforms[i]);
                            sb.AppendLine("<br>");

                            foreach (string addededWaves in addedOrModifiedFiles[i])
                            {
                                sb.AppendLine(addededWaves);
                                sb.AppendLine("<br>");
                            }
                        }

                        if (deletedFiles[i].Count != 0)
                        {
                            sb.AppendLine("<p>");
                            sb.AppendLine("The following wave files have been deleted with no match in the pendingWave file:");
                            sb.AppendLine("</br>");
                            sb.AppendLine("PLATFORM: " + platforms[i]);
                            sb.AppendLine("<br>");

                            foreach (string deletedWaves in deletedFiles[i])
                            {
                                sb.AppendLine(deletedWaves);
                                sb.AppendLine("<br>");
                            }
                        }

                        if (outputCurrPrintLinesTotal[i].Count != 0)
                        {
                            sb.AppendLine("<p>");
                            sb.AppendLine("The following wave files have been added to the pendingWave file with no matching wave or gesture file in the CL:");
                            sb.AppendLine("<br>");
                            sb.AppendLine("PLATFORM: " + platforms[i]);
                            sb.AppendLine("<br>");

                            foreach (string outputCurrPrintLine in outputCurrPrintLinesTotal[i])
                            {
                                Match waveMatch = Regex.Match(outputCurrPrintLine, "Wave name=\"(.*?)\".*?operation");
                                sb.AppendLine(waveMatch.Groups[1].Value.ToString());
                                sb.AppendLine("<br>");
                            }
                        }

                        if (outputPrevPrintLinesTotal[i].Count != 0)
                        {
                            sb.AppendLine("<p>");
                            sb.AppendLine("The following wave files have been removed from the pendingWave file with no matching wave or gesture file in the CL:");
                            sb.AppendLine("PLATFORM: " + platforms[i]);
                            sb.AppendLine("</br>");

                            foreach (string outputPrevPrintLine in outputPrevPrintLinesTotal[i])
                            {
                                Match waveMatch = Regex.Match(outputPrevPrintLine, "Wave name=\"(.*?)\".*?operation");
                                sb.AppendLine(waveMatch.Groups[1].Value.ToString());
                                sb.AppendLine("<br>");
                            }
                        }

                    }

                    sb.AppendLine("</body>");
                    sb.AppendLine("</html>");

                    // Send email
                    email.Body = sb.ToString();
                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
                    {
                        smtp.Send(email);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }
        }

        /// <summary>
        /// Returns value indicating whether report is required to run.
        /// </summary>
        /// <param name="clFiles">
        /// The change list files.
        /// </param>
        /// <returns>
        /// Value indicating whether report is required to run <see cref="bool"/>.
        /// </returns>
        private bool RequiresRun(Data.P4ChangeList.ChangeListFile[] clFiles)
        {
            return clFiles.Select(file => file.File).Any(ext => !string.IsNullOrEmpty(ext) && ext.ToUpper().Contains("PENDINGWAVES"));
        }

        private void RemoveWavesInCurrentRevisionFromLists(String fileName, int revision, ref List<string> addedFileList, ref List<string> deletedFileList)
        {
            ProcessStartInfo procInfo = new ProcessStartInfo("p4");
            procInfo.RedirectStandardOutput = true;
            procInfo.UseShellExecute = false;
            procInfo.CreateNoWindow = true;
            procInfo.WindowStyle = ProcessWindowStyle.Hidden;

            procInfo.Arguments = "print " + fileName + "#" + revision.ToString();

            Process procPrintCurr = new Process();
            procPrintCurr.StartInfo = procInfo;
            procPrintCurr.Start();
            String currentRevisionPrintOutput = procPrintCurr.StandardOutput.ReadToEnd();
            procPrintCurr.WaitForExit();

            List<String> outputCurrPrintLines = Regex.Split(currentRevisionPrintOutput, "\r\n").ToList();

            foreach (String line in outputCurrPrintLines)
            {
                Match waveAddMatch = Regex.Match(line, "Wave name=\"(.*?)\".*?operation=\"add\"");
                if (waveAddMatch.Success)
                {
                    String waveAddMatchValue = waveAddMatch.Groups[1].Value;
                    addedFileList.Remove(waveAddMatchValue.ToUpper());
                    addedFileList.Remove(waveAddMatchValue.ToUpper().Replace(".WAV", ".GESTURE"));
                }
                else
                {
                    Match waveRemoveMatch = Regex.Match(line, "Wave name=\"(.*?)\".*?operation=\"remove\"");
                    if (waveRemoveMatch.Success)
                    {
                        String waveRemoveMatchValue = waveRemoveMatch.Groups[1].Value;
                        deletedFileList.Remove(waveRemoveMatchValue.ToUpper());
                    }
                }
            }
        }



    }
}
