﻿// -----------------------------------------------------------------------
// <copyright file="ChangeParseStateEnums.cs" company="">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Enums
{
    /// <summary>
    /// Change parse state enums.
    /// </summary>
    public enum ChangeParseState
    {
        /// <summary>
        /// The change header.
        /// </summary>
        ChangeHeader,

        /// <summary>
        /// The change comment.
        /// </summary>
        ChangeComment,

        /// <summary>
        /// The change file list.
        /// </summary>
        ChangeFilelist,
    }
}
