﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using Rockstar.AssetManager.Interfaces;
using rage;
using System.IO;
using System.Xml.Linq;

namespace CommitWatcher.Reports
{
    /**
     * This report will check for any wave assets that have to be marked for rebuild when a custom animation has been submitted
     **/
    public class LipsyncAudioRebuildReport
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly IAssetManager assetManager;
        private audProjectSettings projectSettings;
        private readonly Settings settings;
        string localLipsyncAnimsBaseDir;
        string localWavesBaseDir;
        


        public LipsyncAudioRebuildReport(Settings settings, IAssetManager assetManager) 
        {
            this.assetManager = assetManager;
            this.settings = settings;
            this.projectSettings = settings.ProjectSettings;
            this.localLipsyncAnimsBaseDir = assetManager.GetLocalPath(projectSettings.LipsyncDialogueSettings.LipsyncAnimsBaseDir);
            this.localWavesBaseDir = assetManager.GetLocalPath(projectSettings.GetWaveInputPath());
        }

        public void Run() 
        {
            logger.Info("Running lipsync audio rebuild report - Checking for any outdated awc file");
            assetManager.GetLatest(localLipsyncAnimsBaseDir, false);

            Dictionary<string, AudioRebuildInfo> buildInfoLookupTable = new Dictionary<string, AudioRebuildInfo>();

            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            CreateBuildInfoLookupTableForAllFiles(localLipsyncAnimsBaseDir, buildInfoLookupTable, new BuiltWavesXmlCacher(this.assetManager, this.projectSettings));
            sw.Stop();
            logger.Info(sw.Elapsed + " needed to generate buildInfoLookupTable");
            BuildOutdatedAWCs(buildInfoLookupTable);
        }
        
        public void Run(Data.P4ChangeList.ChangeListFile[] clFiles)
        {
            if (clFiles.Length == 0)
            {
                return;
            }

            List<string> lipsyncFiles = new List<string>();
            foreach (Data.P4ChangeList.ChangeListFile clFile in clFiles) 
            {
                if (clFile.File.ToUpper().Contains(projectSettings.LipsyncDialogueSettings.LipsyncAnimsBaseDir.ToUpper()))
                {
                    lipsyncFiles.Add(clFile.File);
                }
            }
            if (lipsyncFiles.Count==0) return;

            logger.Info("Running lipsync audio rebuild report - Checking newly submitted Lipsync animations");
            assetManager.GetLatest(localLipsyncAnimsBaseDir, false);

            Dictionary<string,AudioRebuildInfo> buildInfoLookupTable = new Dictionary<string,AudioRebuildInfo>();
            
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            BuiltWavesXmlCacher xmlCacher = new BuiltWavesXmlCacher(this.assetManager, this.projectSettings);
            foreach(string lipsyncFilePath in lipsyncFiles)
            {
                AddBuildInfoLookupTableEntryforFile(this.assetManager.GetLocalPath(lipsyncFilePath), buildInfoLookupTable, xmlCacher);
            }
            sw.Stop();
            logger.Info(sw.Elapsed + " needed to generate buildInfoLookupTable");
            BuildOutdatedAWCs(buildInfoLookupTable);
        }

        public void BuildOutdatedAWCs(Dictionary<string, AudioRebuildInfo> buildInfoLookupTable) 
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            HashSet<string> bankPathsToRebuild = new HashSet<string>();
            foreach (var kvp in buildInfoLookupTable)
            {
                //use platforms from projectSettings.xml, we probably could have also used the platforms from commitwatcher settings.xml
                foreach (PlatformSetting platformSetting in projectSettings.GetPlatformSettings())
                {
                    if (!platformSetting.IsActive) continue;
                    if (bankPathsToRebuild.Contains(kvp.Value.BankPath)) continue;
                    string localAwcPath = Path.Combine(settings.ProjectSettings.GetBackupWorkingPath() + platformSetting.BuildOutput, "SFX", kvp.Value.PackName, kvp.Value.BankName + ".AWC");
                    if (kvp.Value.LatestDataUpdate > assetManager.GetLatestCheckinTime(localAwcPath)) bankPathsToRebuild.Add(kvp.Value.BankPath);
                }
            }
            sw.Stop();
            logger.Info(sw.Elapsed + " needed to find banks to rebuild");
            List<string> platforms = new List<string>();
            foreach (PlatformSetting platformSetting in projectSettings.GetPlatformSettings()) platforms.Add(platformSetting.PlatformTag);
            BankRebuilder.MarkBanksForRebuildAndSubmit(bankPathsToRebuild.ToList(), platforms, assetManager, settings);
        }

        public class AudioRebuildInfo: PackBankInfo
        {
            public string WaveName;
            public DateTime LatestDataUpdate;
        }

        private void CreateBuildInfoLookupTableForAllFiles(string directory, Dictionary<string, AudioRebuildInfo> buildInfoLookupTable, BuiltWavesXmlCacher builtWavesXmlCacher) 
        {
            foreach (string entry in Directory.GetFileSystemEntries(directory)) 
            {
                if (Directory.Exists(entry))
                {
                    CreateBuildInfoLookupTableForAllFiles(entry, buildInfoLookupTable, builtWavesXmlCacher);
                }
                else 
                {
                    AddBuildInfoLookupTableEntryforFile(entry, buildInfoLookupTable, builtWavesXmlCacher);
                }
            }
        }

        private void AddBuildInfoLookupTableEntryforFile(string lipsyncFilePath, Dictionary<string, AudioRebuildInfo> buildInfoLookupTable, BuiltWavesXmlCacher builtWavesXmlCacher) 
        {
            string correspondingWaveFile = Path.Combine(Path.GetDirectoryName(lipsyncFilePath), Path.GetFileNameWithoutExtension(lipsyncFilePath) + ".WAV").Replace(localLipsyncAnimsBaseDir, localWavesBaseDir);
            DateTime submitDate = assetManager.GetLatestCheckinTime(lipsyncFilePath);
            if (!buildInfoLookupTable.ContainsKey(correspondingWaveFile))
            {
                AudioRebuildInfo lookupTableEntry = new AudioRebuildInfo();
                PackBankInfo packBankInfo = builtWavesXmlCacher.GetPackAndBankName(correspondingWaveFile);
                lookupTableEntry.PackName = packBankInfo.PackName;
                lookupTableEntry.BankName = packBankInfo.BankName;
                lookupTableEntry.WaveName = correspondingWaveFile;
                lookupTableEntry.BankPath = packBankInfo.BankPath;
                lookupTableEntry.LatestDataUpdate = submitDate;
                buildInfoLookupTable.Add(correspondingWaveFile, lookupTableEntry);
            }
            else
            {
                if (buildInfoLookupTable[correspondingWaveFile].LatestDataUpdate < submitDate)
                {
                    buildInfoLookupTable[correspondingWaveFile].LatestDataUpdate = submitDate;
                }
            }
        }

        public class PackBankInfo { public string PackName; public string BankName; public string BankPath; }

        public class BuiltWavesXmlCacher
        {
            private Dictionary<string, XElement> rootElementPerPackLookupTable = new Dictionary<string, XElement>();
            IAssetManager assetManager;
            audProjectSettings projectSettings;

            public BuiltWavesXmlCacher(IAssetManager assetManager, audProjectSettings projectSettings) 
            { 
                this.assetManager = assetManager; 
                this.projectSettings = projectSettings; 
            }

            public PackBankInfo GetPackAndBankName(string wavePath)
            {
                string localWavesBaseDir = assetManager.GetLocalPath(projectSettings.GetWaveInputPath());
                string relativeWavePath = wavePath.Replace(localWavesBaseDir, "");
                List<string> wavePathElements = new List<string>(relativeWavePath.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries));
                string packName = wavePathElements.First();
                wavePathElements.RemoveAt(0);


                if (!rootElementPerPackLookupTable.ContainsKey(packName))
                {
                    string builtWavesName = Path.Combine(this.assetManager.GetLocalPath(projectSettings.getDefaultPlatform().BuildInfo), "BuiltWaves", packName + "_PACK_FILE.XML");
                    XDocument builtWavesDoc = XDocument.Load(builtWavesName);
                    rootElementPerPackLookupTable.Add(packName, builtWavesDoc.Root);
                }


                var currElem = rootElementPerPackLookupTable[packName];
                string currElemPath = "";
                foreach (string name in wavePathElements)
                {
                    currElem = currElem.Elements().FirstOrDefault(e => e.Attribute("name").Value.ToUpper() == name.ToUpper());
                    currElemPath = Path.Combine(currElemPath, name);
                    if (null == currElem) throw new Exception("Failed to find element " + wavePath + " in BuiltWaves of " + packName);
                    if (currElem.Name.LocalName.Equals("Bank"))
                    {
                        PackBankInfo result = new PackBankInfo();
                        result.PackName = packName;
                        result.BankName = currElem.Attribute("name").Value;
                        result.BankPath = Path.Combine(packName, currElemPath);
                        return result;
                    }
                }
                throw new Exception("Failed to find Bank name for " + wavePath);
            }
        }

    }
}
