﻿// -----------------------------------------------------------------------
// <copyright file="MetadataObjectSizeReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using NLog;

    using global::CommitWatcher.Data;

    using MetadataLib;

    using rage;
    using rage.ToolLib.DataStructures;

    using Rockstar.AssetManager.Interfaces;
    using System.Net.Mail;

    /// <summary>
    /// Metadata object size report.
    /// </summary>
    public class MetadataObjectSizeReport
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The types to process.
        /// </summary>
        private readonly string[] typesToProcess = { "GAME", "SOUNDS", "OPTAMP", "DYNAMIX" };

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The object xml path.
        /// </summary>
        private readonly string objectXmlPath;

        /// <summary>
        /// The object size report path.
        /// </summary>
        private string objectSizeReportPath;

        /// <summary>
        /// The folder names.
        /// </summary>
        private IList<string> folderNames;

        /// <summary>
        /// The row timestamps.
        /// </summary>
        private IList<string> rowTimestamps; 

        /// <summary>
        /// The folder sizes.
        /// </summary>
        private IDictionary<string, IList<string>> folderSizes;

        /// <summary>
        /// The run time stamp.
        /// </summary>
        private DateTime runTimeStamp;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataObjectSizeReport"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public MetadataObjectSizeReport(Settings settings, IAssetManager assetManager)
        {
            this.settings = settings;
            this.assetManager = assetManager;
            this.objectXmlPath = this.assetManager.GetLocalPath(this.settings.ProjectSettings.GetSoundXmlPath())
                        .Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
        }

        /// <summary>
        /// Run the report.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        public void Run(P4ChangeList.ChangeList changeList, IEnumerable<MailAddress> mailAddresses)
        {
            if (!this.InitPaths())
            {
                return;
            }

            var errors = new StringBuilder();
            this.runTimeStamp = DateTime.Now;

            foreach (var file in changeList.Files)
            {
                var typeName = GetTypeNameFromPath(file.File);

                if (null == typeName || !this.typesToProcess.Contains(typeName.ToUpper()))
                {
                    // Not a file we need to process
                    continue;
                }

                var platform = Utils.GetPlatfromFromFile(file, this.settings);

                if (string.IsNullOrEmpty(platform))
                {
                    errors.AppendLine("Failed to parse platform from file path: " + file.File);
                    continue;
                }

                if (!this.LoadData(typeName, platform))
                {
                    errors.AppendLine(
                        string.Format("Failed to load data for type {0} on platform {1}", typeName, platform));
                    continue;
                }

                var localFilePath = this.assetManager.GetLocalPath(file.File);

                var schemaPath = this.GetSchemaPath(typeName);
                if (string.IsNullOrEmpty(schemaPath))
                {
                    errors.AppendLine(string.Format("Failed to get schema path for type {0}", typeName));
                    continue;
                }

                var assetPath = this.GetAssetPath(typeName);
                if (string.IsNullOrEmpty(assetPath))
                {
                    errors.AppendLine(string.Format("Failed to get asset path for type {0}", typeName));
                    continue;
                }

                if (!this.UpdateData(localFilePath, platform, schemaPath, assetPath, changeList.ChangeListNumber))
                {
                    errors.AppendLine(
                        string.Format("Failed to update data from file: {0}", file.File));
                    continue;
                }

                if (!this.SaveData(typeName, platform))
                {
                    errors.AppendLine(
                        string.Format("Failed to save data for type {0} on platform {1}", typeName, platform));
                }
            }

            if (errors.Length > 0)
            {
                logger.Warn(errors);
                EmailError(errors, mailAddresses);
            }
        }

        /// <summary>
        /// Email errors.
        /// </summary>
        /// <param name="mailAddresses">
        /// The mail addresses.
        /// </param>
        private void EmailError(StringBuilder errors, IEnumerable<MailAddress> mailAddresses)
        {
            if (!mailAddresses.Any())
            {
                return;
            }

            var subject = "Metadata Object Size Report Failure";

            var email = new MailMessage
            {
                From = this.settings.From,
                Subject = subject,
                IsBodyHtml = false
            };

            foreach (var mailAddress in mailAddresses)
            {
                email.To.Add(mailAddress);
            }

            email.Body = errors.ToString();

            using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
            {
                smtp.Send(email);
            }
        }

        /// <summary>
        /// Parse out the type name from a DAT file path and return it if
        /// it is a type we need to process.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The type name <see cref="string"/>.
        /// </returns>
        private static string GetTypeNameFromPath(string filePath)
        {
            var filePathUpper = filePath.ToUpper();
            var ext = Path.GetExtension(filePathUpper);
            if (string.IsNullOrEmpty(ext) || !ext.StartsWith(".DAT"))
            {
                return null;
            }

            return Path.GetFileNameWithoutExtension(filePathUpper);
        }

        /// <summary>
        /// Initialize the data paths.
        /// </summary>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool InitPaths()
        {
            if (!this.assetManager.GetLatest(this.settings.ObjectSizeReportPath, false))
            {
                return false;
            }

            this.objectSizeReportPath = this.assetManager.GetLocalPath(this.settings.ObjectSizeReportPath);
            return true;
        }

        /// <summary>
        /// Update object sizes details for given DAT file.
        /// </summary>
        /// <param name="datFilePath">
        /// The DAT file path.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="schemaPath">
        /// The schema path.
        /// </param>
        /// <param name="assetPath">
        /// The asset path.
        /// </param>
        /// <param name="changeListNumber">
        /// The change list number.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool UpdateData(string datFilePath, string platform, string schemaPath, string assetPath, string changeListNumber)
        {
            this.settings.ProjectSettings.SetCurrentPlatformByTag(platform);
            var isBigEndian = this.settings.ProjectSettings.IsBigEndian();

            // Sync data at change list number
            if (!this.assetManager.GetLatest(datFilePath, true, changeListNumber) ||
                !this.assetManager.GetLatest(schemaPath, true, changeListNumber) ||
                !this.assetManager.GetLatest(assetPath, true, changeListNumber))
            {
                return false;
            }

            if (!File.Exists(datFilePath))
            {
                return false;
            }

            var nameTablePath = datFilePath + ".nametable";
            if (this.assetManager.ExistsAsAsset(nameTablePath) && !this.assetManager.GetLatest(nameTablePath, true, changeListNumber))
            {
                return false;
            }

            MetadataFile datFile;

            try
            {
                datFile = new MetadataFile(datFilePath, isBigEndian, schemaPath);
            }
            catch (Exception ex)
            {
                return false;
            }

            var tree = this.BuildDataTree(datFile, assetPath);

            // Store details of top-level folders
            foreach (var node in tree.Children)
            {
                var folderName = node.Data.Name;
                var size = node.Data.Size;

                if (!this.folderSizes.ContainsKey(folderName))
                {
                    this.folderSizes[folderName] = new List<string>();
                }

                this.folderSizes[folderName].Add(size.ToString(CultureInfo.InvariantCulture));
            }

            // Add current timestamp to list
            this.rowTimestamps.Add(this.runTimeStamp.ToString("yy-MM-dd HH:mm:ss"));

            return true;
        }

        /// <summary>
        /// Build the data tree.
        /// </summary>
        /// <param name="metadataFile">
        /// The metadata file.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <returns>
        /// The root node <see cref="TreeNode"/>.
        /// </returns>
        private TreeNode<ObjectSizeInfo> BuildDataTree(MetadataFile metadataFile, string path, TreeNode<ObjectSizeInfo> parent = null)
        {
            if (null == parent)
            {
                // First iteration creates tree root
                var root = new TreeNode<ObjectSizeInfo>(null);
                return this.BuildDataTree(metadataFile, path, root);
            }

            foreach (var dirPath in Directory.GetDirectories(path))
            {
                var dirName = dirPath.Substring(dirPath.LastIndexOf("\\", System.StringComparison.Ordinal) + 1);
                var node = new TreeNode<ObjectSizeInfo>(parent);
                parent.Children.Add(node);

                // Populate child nodes
                this.BuildDataTree(metadataFile, dirPath, node);

                var totalSize = node.Children.Aggregate(0U, (current, childNode) => current + childNode.Data.Size);

                var info = new ObjectSizeInfo { Name = dirName, Size = totalSize };
                node.Data = info;
            }

            foreach (var file in Directory.GetFiles(path, "*.xml"))
            {
                var fileNameNoExt = Path.GetFileNameWithoutExtension(file);
                var node = new TreeNode<ObjectSizeInfo>(parent);
                parent.Children.Add(node);

                this.AddObjects(file, node, metadataFile);

                var totalSize = node.Children.Aggregate(0U, (current, childNode) => current + childNode.Data.Size);

                var info = new ObjectSizeInfo { Name = fileNameNoExt, Size = totalSize };
                node.Data = info;
            }

            return parent;
        }

        /// <summary>
        /// The add objects.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <param name="parent">
        /// The parent node.
        /// </param>
        /// <param name="metadataFile">
        /// The metadata file.
        /// </param>
        private void AddObjects(string fileName, TreeNode<ObjectSizeInfo> parent, MetadataFile metadataFile)
        {
            var doc = XDocument.Load(fileName);
            foreach (var elem in doc.Root.Elements())
            {
                var node = new TreeNode<ObjectSizeInfo>(parent);
                var name = elem.Attribute("name").Value;
                parent.Children.Add(node);

                uint size = 0;
                if (metadataFile.ObjectLookup.ContainsKey(name))
                {
                    size = metadataFile.ObjectLookup[name].Size;
                }

                var info = new ObjectSizeInfo { Name = name, Size = size };
                node.Data = info;
            }
        }

        /// <summary>
        /// Load any existing data.
        /// </summary>
        /// <param name="typeName">
        /// The type name.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadData(string typeName, string platform)
        {
            var filePath = Path.Combine(this.objectSizeReportPath, platform, typeName) + ".csv";

            this.rowTimestamps = new List<string>();
            this.folderNames = new List<string>();
            this.folderSizes = new Dictionary<string, IList<string>>();

            if (!File.Exists(filePath))
            {
                return true;
            }

            using (var reader = new StreamReader(filePath))
            {
                string currLine;
                var firstLine = true;
                while ((currLine = reader.ReadLine()) != null)
                {
                    currLine = currLine.Trim();
                    if (currLine == string.Empty)
                    {
                        continue;
                    }

                    var split = currLine.Split(',');
                    var columnIndex = 0;
                    foreach (var value in split)
                    {
                        if (firstLine)
                        {
                            if (columnIndex > 0)
                            {
                                this.folderNames.Add(value);
                                this.folderSizes[value] = new List<string>();
                            }
                        }
                        else
                        {
                            if (columnIndex == 0)
                            {
                                this.rowTimestamps.Add(value);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(value))
                                {
                                    var folderName = this.folderNames[columnIndex - 1];
                                    this.folderSizes[folderName].Add(value);
                                }
                            }
                        }

                        columnIndex++;
                    }

                    firstLine = false;
                }
            }

            return true;
        }

        /// <summary>
        /// Save data to file.
        /// </summary>
        /// <param name="typeName">
        /// The type name.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool SaveData(string typeName, string platform)
        {
            var dirPath = Path.Combine(this.objectSizeReportPath, platform);
            var filePath = Path.Combine(dirPath, typeName) + ".csv";

            var changeList =
                this.assetManager.CreateChangeList(
                    string.Format("[Commit Watcher] Metadata object size report for {0}", typeName));

            if (File.Exists(filePath))
            {
                if (this.assetManager.ExistsAsAsset(filePath))
                {
                    changeList.CheckoutAsset(filePath, true);
                }
            }
            else if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            using (var writer = new StreamWriter(filePath, false))
            {
                writer.Write("Timestamp,");

                // Write folder names in first row
                writer.WriteLine(string.Join(",", this.folderSizes.Keys));

                // Write folder sizes in subsequent rows
                var totalRowCount = this.folderSizes.Values.Select(values => values.Count).Concat(new[] { 0 }).Max();
                for (var row = 0; row < totalRowCount; row++)
                {
                    writer.Write(string.Format("{0},", this.rowTimestamps[row]));

                    foreach (var values in this.folderSizes.Values)
                    {
                        if (values.Count > row)
                        {
                            writer.Write(values[row]);
                        }
                        
                        writer.Write(",");
                    }

                    writer.WriteLine();
                }
            }

            // Submit changes
            if (!this.assetManager.ExistsAsAsset(filePath))
            {
                changeList.MarkAssetForAdd(filePath);
            }

            changeList.RevertUnchanged();
            if (changeList.Assets.Any())
            {
                if (!changeList.Submit())
                {
                    return false;
                }
            }
            else
            {
                changeList.Delete();
            }

            return true;
        }

        /// <summary>
        /// Get the schema path.
        /// </summary>
        /// <param name="typeName">
        /// The type name.
        /// </param>
        /// <returns>
        /// The schema path <see cref="string"/>.
        /// </returns>
        private string GetSchemaPath(string typeName)
        {
            var metadataFile = this.GetMetadataFileSettings(typeName);
            if (null != metadataFile)
            {
                var typeLookup = metadataFile.Type.ToUpper();
                foreach (var metadataType in this.settings.MetadataTypes)
                {
                    if (metadataType.Type.ToUpper() == typeLookup)
                    {
                        var objectDef = metadataType.ObjectDefinitions.FirstOrDefault();

                        if (null != objectDef && !string.IsNullOrEmpty(objectDef.DefinitionsFile))
                        {
                            return Path.Combine(this.objectXmlPath, objectDef.DefinitionsFile);
                        }

                        return null;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Get the asset path.
        /// </summary>
        /// <param name="typeName">
        /// The type name.
        /// </param>
        /// <returns>
        /// The asset path <see cref="string"/>.
        /// </returns>
        private string GetAssetPath(string typeName)
        {
            var metadataFile = this.GetMetadataFileSettings(typeName);
            if (null != metadataFile)
            {
                var path = Path.Combine(this.objectXmlPath, metadataFile.DataPath);
                path = path.Trim().Replace('/', '\\');
                if (!path.EndsWith("\\"))
                {
                    path += "\\";
                }

                return path;
            }

            return null;
        }

        /// <summary>
        /// Get the metadata file settings.
        /// </summary>
        /// <param name="typeName">
        /// The type name.
        /// </param>
        /// <returns>
        /// The metadata file settings <see cref="audMetadataFile"/>.
        /// </returns>
        private audMetadataFile GetMetadataFileSettings(string typeName)
        {
            var typeNameUpper = typeName.ToUpper();

            foreach (var metadataFile in this.settings.MetadataFiles)
            {
                if (string.IsNullOrEmpty(metadataFile.OutputFile))
                {
                    continue;
                }

                var outputFileName = Path.GetFileNameWithoutExtension(metadataFile.OutputFile).ToUpper();

                if (outputFileName == typeNameUpper)
                {
                    return metadataFile;
                }
            }

            return null;
        }

        /// <summary>
        /// The object size info.
        /// </summary>
        public struct ObjectSizeInfo
        {
            /// <summary>
            /// Gets or sets the name.
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets the size.
            /// </summary>
            public uint Size { get; set; }
        }
    }
}
