﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CommitWatcher.CommitWatcher
{
    public class TimeOutFileSystemWatcher: FileSystemWatcher
    {
        System.Timers.Timer timeOutTimer;
        bool isNetworkUnavailable = false;

        public TimeOutFileSystemWatcher(int timeOutInMilisec = 1000) 
        {
            timeOutTimer = new System.Timers.Timer(timeOutInMilisec);
            timeOutTimer.Elapsed += new System.Timers.ElapsedEventHandler(timeOutTimer_Elapsed);
            timeOutTimer.Start();
        }

        void timeOutTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {  
            try
            {
                DirectoryInfo myFolder = new DirectoryInfo(base.Path.ToString());
                if (!isNetworkUnavailable)
                {
                    if (!myFolder.Exists)
                    {
                        isNetworkUnavailable = true;
                        //todo raise network unavailable event
                        //raise the error event for now
                        base.OnError(new ErrorEventArgs(new Exception("The location "+base.Path+" is not available")));
                    }
                }
                else
                {
                    if (myFolder.Exists)
                    {
                        isNetworkUnavailable = false;
                        //todo raise network available event
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
