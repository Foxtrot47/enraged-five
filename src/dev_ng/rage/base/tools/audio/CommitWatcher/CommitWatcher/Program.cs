namespace CommitWatcher
{
    using System;
    using System.IO;
    using System.ServiceProcess;

    /// <summary>
    /// The commit watcher program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        static void Main(string[] args)
        {
            if (args.Length > 0 && String.Compare(args[0], "-CommandLine", StringComparison.OrdinalIgnoreCase) == 0)
            {
                Console.Out.WriteLine("Running in command line mode");
                var cw = new CommitWatcher.CommitWatcher(true);
                cw.Run();

                while (true)
                {
                    System.Threading.Thread.Sleep(100);
                }
            }
            else
            {
                // More than one user Service may run within the same process. To add
                // another service to this process, change the following line to
                // create a second service object. For example:
                //  - ServicesToRun = new ServiceBase[] {new Service1(), new MySecondUserService()};
                var location = System.Reflection.Assembly.GetExecutingAssembly().Location;
                var path = Path.GetDirectoryName(location);
                Directory.SetCurrentDirectory(path);
                var servicesToRun = new ServiceBase[] { new CommitWatcher.CommitWatcher(false) };
                ServiceBase.Run(servicesToRun);
            }
        }
    }
}