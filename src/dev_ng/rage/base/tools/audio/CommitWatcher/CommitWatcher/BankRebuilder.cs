﻿using NLog;
using Rockstar.AssetManager.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CommitWatcher
{
    public class BankRebuilder
    {

        public static void MarkBanksForRebuildAndSubmit(List<string> banksToRebuild, IEnumerable<string> platforms, IAssetManager assetManager, Settings settings)
        {
            var banksInPacks = new Dictionary<string, List<List<string>>>();

            // Group banks by pack
            foreach (var bank in banksToRebuild)
            {
                List<string> wavePathElements = new List<string>(bank.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries));
                string packName = wavePathElements.First();
                wavePathElements.RemoveAt(0);
                if (!banksInPacks.ContainsKey(packName)) banksInPacks[packName] = new List<List<string>>();
                banksInPacks[packName].Add(wavePathElements);
            }

            // Mark up banks for rebuild
            foreach (var banksInPack in banksInPacks)
            {
                var pack = banksInPack.Key;
                var banks = banksInPack.Value;
                foreach (string platform in platforms)
                {
                    string pendingWavesLocalPath = Path.Combine(assetManager.GetLocalPath(Regex.Replace(settings.PendingWavePath, "{platform}", platform, RegexOptions.IgnoreCase)), pack + ".xml");
                    string builtWavesLocalPath = Path.Combine(assetManager.GetLocalPath(Regex.Replace(settings.BuiltWavePath, "{platform}", platform, RegexOptions.IgnoreCase)), pack + "_pack_file.xml");

                    if (!File.Exists(builtWavesLocalPath))
                    {
                        throw new Exception("Failed to find file " + builtWavesLocalPath);
                    }

                    var changeList = assetManager.CreateChangeList("Mark pack " + pack + " for rebuild [" + platform + "]");

                    // Mark next batch of banks for rebuild
                    try
                    {
                        BankRebuilder.MarkBanksForRebuildHelper(changeList, platform, pack, banks, pendingWavesLocalPath, builtWavesLocalPath, assetManager);
                    }
                    catch(Exception ex)
                    {
                        changeList.Revert(true);
                        throw new Exception("Failed to mark banks for rebuild: " + pack, ex);
                    }

                    changeList.RevertUnchanged();

                    if (changeList.Assets.Any())
                    {
                        // Submit pending wave changes
                        if (!changeList.Submit())
                        {
                            throw new Exception("Failed to submit change list: " + changeList.NumberAsString);
                        }
                    }
                    else changeList.Delete();
                }

            }
        }

        private static void MarkBanksForRebuildHelper(IChangeList changeList, string platform, string packName, List<List<string>> paths, string pendingWavesPath, string builtWavesPath, IAssetManager assetManager)
        {
            var builtWavesDoc = XDocument.Load(builtWavesPath);
            var banksElemsToRebuild = new List<XElement>();

            foreach (var path in paths)
            {
                var currElem = builtWavesDoc.Root;
                foreach(string name in path)
                {
                    currElem = currElem.Elements().FirstOrDefault(e => e.Attribute("name").Value.ToUpper() == name.ToUpper());
                    if (null == currElem)
                    {
                        throw new Exception("Failed to find element to mark for rebuild: " + path);
                    }
                }

                if (null != currElem) banksElemsToRebuild.Add(currElem);
            }

            if (banksElemsToRebuild.Any())
            {
                if (assetManager.ExistsAsAsset(pendingWavesPath))
                {
                    var asset = changeList.CheckoutAsset(pendingWavesPath, true);
                    if (null == asset)
                    {
                        throw new Exception("Failed to check out and lock pending waves file: " + pendingWavesPath);
                    }

                }
                else
                {
                    changeList.MarkAssetForAdd(pendingWavesPath);
                }

                try
                {
                    BankRebuilder.WritePendingWaves(banksElemsToRebuild, pendingWavesPath);
                }
                catch(Exception ex)
                {
                    throw new Exception("Failed to write pending wave changes to file: " + pendingWavesPath, ex);
                }
            }
        }

        /// <summary>
        /// Write bank rebuild entries into pending waves.
        /// </summary>
        /// <param name="bankElems">
        /// The bank elements.
        /// </param>
        /// <param name="pendingWavesPath">
        /// The pending waves path.
        /// </param>
        private static void WritePendingWaves(IEnumerable<XElement> bankElems, string pendingWavesPath)
        {
            var doc = new XDocument();
            if (File.Exists(pendingWavesPath))
            {
                doc = XDocument.Load(pendingWavesPath);
            }
            else
            {
                var pendingWavesElem = new XElement("PendingWaves");
                doc.Add(pendingWavesElem);
            }

            foreach (var bankElem in bankElems)
            {
                // Get hierarchy structure of bank
                var parentList = new List<XElement> { bankElem };
                var currParent = bankElem.Parent;
                while (null != currParent)
                {
                    // Stop when we hit PendingWaves element
                    if (currParent.Name.LocalName != "PendingWaves")
                    {
                        parentList.Insert(0, currParent);
                    }

                    currParent = currParent.Parent;
                }

                // Insert cloned bank element into pending waves
                var currElem = doc.Root;
                foreach (var elem in parentList)
                {
                    var existingElem = BankRebuilder.FindExistingElement(currElem, elem);
                    if (null != existingElem)
                    {
                        // Check that existing element not marked for remove
                        var opAttr = existingElem.Attribute("operation");
                        if (null != opAttr && opAttr.Value.ToLower() == "remove")
                        {
                            // We don't want to mark an element for rebuild if it is
                            // in a path that contains an element marked for remove
                            continue;
                        }
                    }
                    else
                    {
                        existingElem = new XElement(elem);
                        existingElem.RemoveNodes();

                        var isBankElem = false;

                        if (existingElem.Name.LocalName == "Bank")
                        {
                            isBankElem = true;

                            // Add rebuild tag to Bank
                            var tagElem = new XElement("Tag");
                            tagElem.SetAttributeValue("name", "rebuild");
                            tagElem.SetAttributeValue("operation", "add");
                            existingElem.Add(tagElem);
                        }

                        currElem.Add(existingElem);

                        if (isBankElem)
                        {
                            // We've found the bank and marked for rebuild,
                            // no need to continue any further
                            break;
                        }
                    }

                    currElem = existingElem;
                }
            }

            doc.Save(pendingWavesPath);
        }

        /// <summary>
        /// Determine whether element already exists in new structure.
        /// (based on element type and name).
        /// </summary>
        /// <param name="parentElem">
        /// The parent element.
        /// </param>
        /// <param name="elem">
        /// The element to find.
        /// </param>
        /// <returns>
        /// Existing element <see cref="bool"/>.
        /// </returns>
        private static XElement FindExistingElement(XElement parentElem, XElement elem)
        {
            foreach (var childElem in parentElem.Elements())
            {
                if (childElem.Name.LocalName != elem.Name.LocalName)
                {
                    continue;
                }

                var childNameAttr = childElem.Attribute("name");
                var elemNameAttr = elem.Attribute("name");

                if (null == childNameAttr && null == elemNameAttr)
                {
                    return childElem;
                }

                if (null == childNameAttr || null == elemNameAttr)
                {
                    continue;
                }

                var childName = childNameAttr.Value;
                var elemName = elemNameAttr.Value;

                if (childName == elemName)
                {
                    return childElem;
                }
            }

            return null;
        }
    }
}
