// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Settings.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Settings.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CommitWatcher
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Xml;

    using rage;

    /// <summary>
    /// Settings class.
    /// </summary>
    public class Settings
    {
        #region Fields

        /// <summary>
        /// The asset build path.
        /// </summary>
        public readonly string AssetBuildPath;

        /// <summary>
        /// The wave root path.
        /// </summary>
        public readonly string WaveRootPath;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        /// <param name="doc">
        /// The document containing the settings.
        /// </param>
        public Settings(XmlDocument doc)
        {
            this.CommitReportWatches = new Dictionary<string, List<MailAddress>>();
            this.PedPsoReportWatches = new Dictionary<string, List<MailAddress>>();
            this.ContentXmlWatches = new Dictionary<string, List<MailAddress>>();
            this.WaveDepotReportWatches = new Dictionary<string, List<MailAddress>>();
            this.CollisionSettingsReportWatches = new Dictionary<string, List<MailAddress>>();
            this.WaveslotReportWatches = new Dictionary<string, List<MailAddress>>();
            this.MetadataObjectSizesReportWatches = new Dictionary<string, List<MailAddress>>();
            this.LipsyncRegenReportWatches = new Dictionary<string, List<MailAddress>>();
            this.PendingWaveReportWatches = new Dictionary<string, List<MailAddress>>();
            this.MissingDialogueReportWatches = new Dictionary<string, List<MailAddress>>();
            this.DeliveredDialogueReportWatches = new Dictionary<string, List<MailAddress>>();

            this.FilePaths = new List<string>();
            this.GameObjectsPaths = new List<string>();

            foreach (XmlNode child in doc.DocumentElement.ChildNodes)
            {
                switch (child.Name)
                {
                    case "Platforms":
                        this.ParsePlatformSettings(child);
                        break;
                    case "FilePath":
                        this.FilePaths.Add(child.InnerText);
                        break;
                    case "Project":
                        this.Project = child.InnerText;
                        break;
                    case "SMTP":
                        this.SmtpHost = child.InnerText;
                        break;
                    case "Sender":
                        this.From = new MailAddress(child.InnerText);
                        break;
                    case "AssetManager":
                        this.ParseAssetSettings(child);
                        break;
                    case "Watch":
                        this.ParseWatchSettings(child);
                        break;
                    case "Report":
                        this.ParseReportSettings(child);
                        break;
                    case "AssetBuildPath":
                        this.AssetBuildPath = child.InnerText;
                        break;
                    case "WaveRootPath":
                        this.WaveRootPath = child.InnerText;
                        break;
                    case "GameObjectsPaths":
                        foreach (XmlNode path in child.ChildNodes)
                        {
                            if (path.Name.Equals("Path")) this.GameObjectsPaths.Add(path.InnerText);
                        }
                        break;
                    case "ModelAudioCollisionList":
                        this.ModelAudioCollisionListPath = child.InnerText;
                        break;
                    case "LipsyncUpdateFilePath":
                        this.LipsyncUpdateFilePath = child.InnerText;
                        break;
                    case "ProjectSettingsPath":
                        this.ProjectSettingsPath = child.InnerText;
                        this.InitProjectSettings(child.InnerText);
                        break;
                    case "WorkingPath":
                        this.WorkingPath = child.InnerText;
                        break;
                    case "BuiltWavePath":
                        this.BuiltWavePath = child.InnerText;
                        break;
                    case "PendingWavePath":
                        this.PendingWavePath = child.InnerText;
                        break;
                    case "AudioConfigDataPath":
                        this.AudioConfigDataPath = child.InnerText;
                        break;
                    case "AudioDefinitionsPath":
                        this.AudioDefinitionsPath = child.InnerText;
                        break;
                    case "SimpleTypesDefinitionsPath":
                        this.SimpleTypesDefinitionsPath = child.InnerText;
                        break;
                    case "RuntimeConfigPath":
                        this.RuntimeConfigPath = child.InnerText;
                        break;
                    case "RuntimeSfxPath":
                        this.RuntimeSfxPath = child.InnerText;
                        break;
                    case "MetadataCompilerExePath":
                        this.MetadataCompilerExePath = child.InnerText;
                        break;
                    case "WaveSlotSettingsPath":
                        this.WaveSlotSettingsPath = child.InnerText;
                        break;
                    case "GenericBuiltWavesPackListFilePath":
                        this.GenericBuiltWavesPackListFilePath = child.InnerText;
                        break;
                    case "ObjectSizeReportPath":
                        this.ObjectSizeReportPath = child.InnerText;
                        break;
                    case "BankSizePath":
                        this.BankSizePath = child.InnerText;
                        break;
                    case "WaveBankGroups":
                        this.ParseWaveBankGroups(child);
                        break;
                    case "GrabP4ApplicationPath":
                        this.GrabP4ApplicationPath = child.InnerText;
                        break;
                    case "DialogueDeliveryPath":
                        this.DialogueDeliveryPath = child.InnerText;
                        break;
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the platforms.
        /// </summary>
        public ISet<string> Platforms { get; private set; }

        /// <summary>
        /// Gets the file path.
        /// </summary>
        public IList<string> FilePaths { get; private set; }

        /// <summary>
        /// Gets the from.
        /// </summary>
        public MailAddress From { get; private set; }

        /// <summary>
        /// Gets the project.
        /// </summary>
        public string Project { get; private set; }

        /// <summary>
        /// Gets the project settings.
        /// </summary>
        public audProjectSettings ProjectSettings { get; private set; }

        /// <summary>
        /// Gets the metadata types.
        /// </summary>
        public IEnumerable<audMetadataType> MetadataTypes
        {
            get
            {
                return null != this.ProjectSettings ? this.ProjectSettings.GetMetadataTypes() : null;
            }
        }

        /// <summary>
        /// Gets the metadata files.
        /// </summary>
        public IEnumerable<audMetadataFile> MetadataFiles
        {
            get
            {
                return null != this.ProjectSettings ? this.ProjectSettings.GetMetadataSettings() : null;
            }
        }

        /// <summary>
        /// Gets the SMTP client.
        /// </summary>
        public string SmtpHost { get; private set; }

        /// <summary>
        /// Gets the game objects path.
        /// </summary>
        public List<string> GameObjectsPaths { get; private set; }

        /// <summary>
        /// Gets the model audio collision list path.
        /// </summary>
        public string ModelAudioCollisionListPath { get; private set; }

        /// <summary>
        /// Gets the lipsync update file path.
        /// </summary>
        public string LipsyncUpdateFilePath { get; private set; }

        /// <summary>
        /// Gets the P4 host.
        /// </summary>
        public string P4Host { get; private set; }

        /// <summary>
        /// Gets the P4 port.
        /// </summary>
        public string P4Port { get; private set; }

        /// <summary>
        /// Gets the P4 workspace.
        /// </summary>
        public string P4Workspace { get; private set; }

        /// <summary>
        /// Gets the P4 user.
        /// </summary>
        public string P4User { get; private set; }

        /// <summary>
        /// Gets the P4 password.
        /// </summary>
        public string P4Password { get; private set; }

        /// <summary>
        /// Gets the P4 depot root.
        /// </summary>
        public string P4DepotRoot { get; private set; }

        /// <summary>
        /// Gets the watches for the commit report.
        /// </summary>
        public Dictionary<string, List<MailAddress>> CommitReportWatches { get; private set; }

        /// <summary>
        /// Gets the watches for the ped pso report.
        /// </summary>
        public Dictionary<string, List<MailAddress>> PedPsoReportWatches { get; private set; }

        /// <summary>
        /// Gets the watches for the content xml report.
        /// </summary>
        public Dictionary<string, List<MailAddress>> ContentXmlWatches { get; private set; }

        /// <summary>
        /// Gets the watches for the wave depot report.
        /// </summary>
        public Dictionary<string, List<MailAddress>> WaveDepotReportWatches { get; private set; }

        /// <summary>
        /// Gets the collision settings report watches.
        /// </summary>
        public Dictionary<string, List<MailAddress>> CollisionSettingsReportWatches { get; private set; }

        /// <summary>
        /// Gets the waveslot report watches.
        /// </summary>
        public Dictionary<string, List<MailAddress>> WaveslotReportWatches { get; private set; }

        /// <summary>
        /// Gets the missing dialog report watches.
        /// </summary>
        public Dictionary<string, List<MailAddress>> MissingDialogueReportWatches { get; private set; }

        /// <summary>
        /// Gets the delivered dialogue report watches
        /// </summary>
        public Dictionary<string, List<MailAddress>> DeliveredDialogueReportWatches { get; private set; }


        /// <summary>
        /// Gets the metadata object sizes report watches.
        /// </summary>
        public Dictionary<string, List<MailAddress>> MetadataObjectSizesReportWatches { get; private set; }

        /// <summary>
        /// Gets the  lipsync regen report watches.
        /// </summary>
        public Dictionary<string, List<MailAddress>> LipsyncRegenReportWatches { get; private set; }

        private bool runLipsyncAudioRebuildReport = false;
        public bool RunLipsyncAudioRebuildReport 
        { 
            get { return runLipsyncAudioRebuildReport; } 
            private set { runLipsyncAudioRebuildReport = value; } 
        }

        private bool runGestureAudioRebuildReport = false;
        public bool RunGestureAudioRebuildReport 
        { 
            get { return runGestureAudioRebuildReport; } 
            private set { runGestureAudioRebuildReport = value; } 
        }

        /// <summary>
        /// Gets the  pending wave report watches.
        /// </summary>
        public Dictionary<string, List<MailAddress>> PendingWaveReportWatches { get; private set; }

        /// <summary>
        /// Gets the project settings path.
        /// </summary>
        public string ProjectSettingsPath { get; private set; }

        /// <summary>
        /// Gets the working path.
        /// </summary>
        public string WorkingPath { get; private set; }

        /// <summary>
        /// Gets the built wave path.
        /// </summary>
        public string BuiltWavePath { get; private set; }

        /// <summary>
        /// Gets the pending wave path.
        /// </summary>
        public string PendingWavePath { get; private set; }

        /// <summary>
        /// Gets the audio config data path.
        /// </summary>
        public string AudioConfigDataPath { get; private set; }

        /// <summary>
        /// Gets the audio definitions path.
        /// </summary>
        public string AudioDefinitionsPath { get; private set; }

        /// <summary>
        /// Gets the simple types definitions path.
        /// </summary>
        public string SimpleTypesDefinitionsPath { get; private set; }

        /// <summary>
        /// Gets the runtime config path.
        /// </summary>
        public string RuntimeConfigPath { get; private set; }

        /// <summary>
        /// Gets the runtime SFX path.
        /// </summary>
        public string RuntimeSfxPath { get; private set; }

        /// <summary>
        /// Gets the metadata compiler exe path.
        /// </summary>
        public string MetadataCompilerExePath { get; private set; }

        /// <summary>
        /// Gets the wave slot settings path.
        /// </summary>
        public string WaveSlotSettingsPath { get; private set; }

        /// <summary>
        /// Gets the object size report path.
        /// </summary>
        public string ObjectSizeReportPath { get; private set; }

        /// <summary>
        /// Gets the bank size path.
        /// </summary>
        public string BankSizePath { get; private set; }

        /// <summary>
        /// Packlist for missing dialog.
        /// </summary>
        public string GenericBuiltWavesPackListFilePath { get; private set; }

        /// <summary>
        /// Gets the wave bank groups.
        /// </summary>
        public IDictionary<string, IList<string>> WaveBankGroups { get; private set; }

        /// <summary>
        /// Grab P4 Application path
        /// </summary>
        public string GrabP4ApplicationPath { get; private set; }

        /// <summary>
        ///  Dialogue Delivery Path
        /// </summary>
        public string DialogueDeliveryPath { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Parse platform settings.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private void ParsePlatformSettings(XmlNode node)
        {
            this.Platforms = new HashSet<string>();

            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "Platform":
                        this.Platforms.Add(child.InnerText);
                        break;
                }
            }
        }

        /// <summary>
        /// Parse asset settings.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private void ParseAssetSettings(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "P4Host":
                        this.P4Host = child.InnerText;
                        break;
                    case "P4Port":
                        this.P4Port = child.InnerText;
                        break;
                    case "P4Workspace":
                        this.P4Workspace = child.InnerText;
                        break;
                    case "P4User":
                        this.P4User = child.InnerText;
                        break;
                    case "P4Password":
                        this.P4Password = child.InnerText;
                        break;
                    case "P4DepotRoot":
                        this.P4DepotRoot = child.InnerText;
                        break;
                }
            }
        }

        /// <summary>
        /// Parse Report settings.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private void ParseReportSettings(XmlNode node)
        {
            string report = node.InnerText;
            if (report.Contains("LipsyncAudioRebuildReport")) 
            {
                this.RunLipsyncAudioRebuildReport = true;
            }
            else if (report.Contains("GestureAudioRebuildReport")) 
            {
                this.RunGestureAudioRebuildReport = true;
            }
        }

        /// <summary>
        /// Parse watch settings.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private void ParseWatchSettings(XmlNode node)
        {
            string depotpath = string.Empty;
            var reports = new List<string>();
            var emailAddresses = new List<MailAddress>();

            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "To":
                        emailAddresses.Add(new MailAddress(child.InnerText));
                        break;

                    case "DepotPath":
                        depotpath = child.InnerText;
                        break;

                    case "Report":
                        reports.Add(child.InnerText);
                        break;
                }
            }

            if (emailAddresses.Count == 0 || string.IsNullOrEmpty(depotpath) || !reports.Any())
            {
                throw new Exception("Invalid watch object");
            }

            // Add watch to the necessary list(s)
            if (reports.Contains("CommitReport"))
            {
                this.CommitReportWatches.Add(depotpath, emailAddresses);
            }

            if (reports.Contains("PedPsoReport"))
            {
                this.PedPsoReportWatches.Add(depotpath, emailAddresses);
            }

            if (reports.Contains("ContentXmlReport"))
            {
                this.ContentXmlWatches.Add(depotpath, emailAddresses);
            }

            if (reports.Contains("WaveDepotReport"))
            {
                this.WaveDepotReportWatches.Add(depotpath, emailAddresses);
            }

            if (reports.Contains("CollisionSettingsReport"))
            {
                this.CollisionSettingsReportWatches.Add(depotpath, emailAddresses);
            }

            if (reports.Contains("WaveslotReport"))
            {
                this.WaveslotReportWatches.Add(depotpath, emailAddresses);
            }

            if (reports.Contains("MetadataObjectSizesReport"))
            {
                this.MetadataObjectSizesReportWatches.Add(depotpath, emailAddresses);
            }
            if (reports.Contains("LipsyncRegenReport"))
            {
                this.LipsyncRegenReportWatches.Add(depotpath, emailAddresses);
            }
            if (reports.Contains("PendingWaveReport"))
            {
                this.PendingWaveReportWatches.Add(depotpath, emailAddresses);
            }

            if (reports.Contains("MissingDialogueReport"))
            {
                this.MissingDialogueReportWatches.Add(depotpath, emailAddresses);
            }

            if (reports.Contains("DeliveredDialogueReport"))
            {
                this.DeliveredDialogueReportWatches.Add(depotpath, emailAddresses);
            }
        }

        /// <summary>
        /// Parse the wave bank groups.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        private void ParseWaveBankGroups(XmlNode node)
        {
            this.WaveBankGroups = new Dictionary<string, IList<string>>();

            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "Group":
                        var name = child.Attributes["name"].Value;
                        var patterns = new List<string>();

                        foreach (XmlNode innerChild in child.ChildNodes)
                        {
                            switch (innerChild.Name)
                            {
                                case "Pattern":
                                    patterns.Add(innerChild.InnerText);
                                    break;
                            }
                        }

                        this.WaveBankGroups[name] = patterns;

                        break;
                }
            }
        }

        /// <summary>
        /// Initialize project settings.
        /// </summary>
        /// <param name="projectSettingsPath">
        /// The project settings path.
        /// </param>
        private void InitProjectSettings(string projectSettingsPath)
        {
            if (!File.Exists(projectSettingsPath))
            {
                throw new Exception("Failed to find project settings file: " + projectSettingsPath);
            }

            this.ProjectSettings = new audProjectSettings(projectSettingsPath);
        }

        #endregion
    }
}
