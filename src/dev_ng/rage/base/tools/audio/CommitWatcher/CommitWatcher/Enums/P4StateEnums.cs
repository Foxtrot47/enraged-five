﻿// -----------------------------------------------------------------------
// <copyright file="P4StateEnums.cs" company="">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Enums
{
    /// <summary>
    /// P4 state.
    /// </summary>
    public enum P4State
    {
        /// <summary>
        /// branch state.
        /// </summary>
        Branch,

        /// <summary>
        /// Delete state.
        /// </summary>
        Delete,

        /// <summary>
        /// Edit state.
        /// </summary>
        Edit,

        /// <summary>
        /// Add state.
        /// </summary>
        Add,

        /// <summary>
        /// Move add state.
        /// </summary>
        Move_Add,

        /// <summary>
        /// Move delete state.
        /// </summary>
        Move_Delete,
    }
}
