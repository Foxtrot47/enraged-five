﻿// -----------------------------------------------------------------------
// <copyright file="BankDetails.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Data
{
    /// <summary>
    /// Class to hold details of a particular bank.
    /// </summary>
    public class BankDetails
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the pack name.
        /// </summary>
        public string PackName { get; set; }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Gets or sets the platform.
        /// </summary>
        public string Platform { get; set; }

        /// <summary>
        /// Gets or sets the bank group name.
        /// </summary>
        public string BankGroupName { get; set; }

        /// <summary>
        /// Gets or sets the original size.
        /// </summary>
        public int OriginalSize { get; set; }

        /// <summary>
        /// Gets or sets the current size.
        /// </summary>
        public int CurrentSize { get; set; }
    }
}
