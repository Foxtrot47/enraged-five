﻿namespace CommitWatcher.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// Built wave list.
    /// </summary>
    public class BuiltWaveList
    {
        /// <summary>
        /// The document.
        /// </summary>
        private readonly XDocument document;

        /// <summary>
        /// Initializes a new instance of the <see cref="BuiltWaveList"/> class.
        /// </summary>
        /// <param name="path">
        /// The wave list path.
        /// </param>
        public BuiltWaveList(string path)
        {
            this.document = XDocument.Load(path);
        }

        /// <summary>
        /// Get the list of waves in the document.
        /// </summary>
        /// <returns>
        /// The list of waves <see cref="List"/>.
        /// </returns>
        public List<string> GetWaves()
        {
            var waveElements = this.document.Descendants("Wave");
            return waveElements.Select(GetAssetName).ToList();
        }

        /// <summary>
        /// Get the name of an asset.
        /// </summary>
        /// <param name="waveElement">
        /// The wave element.
        /// </param>
        /// <returns>
        /// The asset name <see cref="string"/>.
        /// </returns>
        private static string GetAssetName(XElement waveElement)
        {
            var nameElements = new List<string>();
            var currentNode = waveElement;

            do
            {
                nameElements.Add(currentNode.Attribute("name").Value);
                currentNode = currentNode.Parent;
            }
            while (currentNode != null);

            var sb = new StringBuilder();
            nameElements.Reverse();
            var first = true;

            foreach (var name in nameElements)
            {
                if (!first)
                {
                    sb.Append('/');
                }

                first = false;
                sb.Append(name);
            }

            return sb.ToString();
        }
    }
}
