﻿// -----------------------------------------------------------------------
// <copyright file="WaveDepotReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Xml.Linq;
    using NLog;

    using global::CommitWatcher.Data;

    using global::CommitWatcher.Enums;

    using Rockstar.AssetManager.Interfaces;

    /// <summary>
    /// Wave depot report generator.
    /// </summary>
    public class WaveDepotReport
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The build path.
        /// </summary>
        private readonly string buildPath;

        /// <summary>
        /// The wave root path.
        /// </summary>
        private readonly string waveRootPath;

        /// <summary>
        /// The error list.
        /// </summary>
        private List<string> errorList;

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveDepotReport"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public WaveDepotReport(Settings settings, IAssetManager assetManager)
        {
            this.settings = settings;
            this.assetManager = assetManager;

            this.buildPath = assetManager.GetLocalPath(settings.AssetBuildPath);
            this.waveRootPath = settings.WaveRootPath;
        }

        /// <summary>
        /// Initialize the report generator.
        /// </summary>
        /// <param name="packListDepotPath">
        /// The pack list depot path.
        /// </param>
        /// <param name="mailAddress">
        /// The mail address.
        /// </param>
        public void Run(string packListDepotPath, List<MailAddress> mailAddresses)
        {
            logger.Info("Running wave depot report on " + packListDepotPath);

            var packListFileName = Path.GetFileName(packListDepotPath);

            // We're expecting a pack file
            if (null == packListFileName || !packListFileName.ToLower().EndsWith("_pack_file.xml"))
            {
                return;
            }

            // Get the platform from the file name
            var splitFileName = packListDepotPath.Split('\\', '/');
            var platform = splitFileName[splitFileName.Length - 3];

            if (string.IsNullOrEmpty(platform))
            {
                // Platform required and should be present in file path
                return;
            }

            // Sync built waves files
            if (!this.assetManager.GetLatest(this.buildPath, false))
            {
                logger.Error("Failed to get latest build data: " + this.buildPath);
                return;
            }

            // Get local pack list file path
            var packListPath = Path.Combine(this.buildPath, platform, "BuiltWaves", packListFileName);

            if (!File.Exists(packListPath))
            {
                // Invalid path / file doesn't exist
                return;
            }

            // Load the pack file to get the corresponding wave dir name
            var doc = XDocument.Load(packListPath);
            var packElem = doc.Element("Pack");
            if (null == packElem)
            {
                // Invalid pack file
                return;
            }

            var packNameAttr = packElem.Attribute("name");
            if (null == packNameAttr)
            {
                // Invalid pack file
                return;
            }

            var packName = packNameAttr.Value;

            this.RunDepotCheck(packListPath, packName, platform);
            foreach (MailAddress address in mailAddresses)
            {
                this.SendReport(address, packName, platform);
            }
        }

        /// <summary>
        /// Run the depot check for the specified pack.
        /// </summary>
        /// <param name="packListPath">
        /// The pack list path.
        /// </param>
        /// <param name="packName">
        /// The pack name.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        private void RunDepotCheck(string packListPath, string packName, string platform)
        {
            this.errorList = new List<string>();

            var waveRootDepotPath = Path.Combine(this.waveRootPath, packName, "...").Replace("\\", "/");

            var fileList = Utils.RunCommand("p4", "files " + waveRootDepotPath);
            var p4WaveList = new P4WaveList(fileList, waveRootPath);

            logger.Info("p4 fileList len: "+fileList.Length);

            var waveList = new List<string>();
            var p4AssetNames = p4WaveList.GetAssetNames();

            var builtWaves = new BuiltWaveList(packListPath);
            waveList.AddRange(builtWaves.GetWaves());

            logger.Info("Total "+platform+" waves: "+waveList.Count);

            // Check that waves exist in Perforce
            foreach (var assetName in waveList)
            {
                var p4Wave = p4WaveList.Find(assetName);

                if (p4Wave == null)
                {
                    // Wave file not in perforce
                    this.errorList.Add(string.Format("{1}: Wave file not in perforce: {0}", assetName, platform));
                }
                else
                {
                    switch (p4Wave.State)
                    {
                        case P4State.Delete:
                            // Wave file deleted from Perforce
                            this.errorList.Add(
                                string.Format("{1}: Wave file deleted from perforce: {0}", assetName, platform));
                            break;

                        case P4State.Move_Delete:
                            // Wave file moved in Perforce
                            this.errorList.Add(
                                string.Format("{1}: Wave file moved in perforce: {0}", assetName, platform));
                            break;
                    }
                }

                p4AssetNames.Remove(assetName);
            }

            // Remove any assets that are platform specific and aren't for current platform
            var assetsToRemove = new List<string>();
            foreach (var assetName in p4AssetNames)
            {
                var assetNameNoExt = Path.GetFileNameWithoutExtension(assetName);
                if (string.IsNullOrEmpty(assetNameNoExt) || !assetNameNoExt.Contains("_"))
                {
                    continue;
                }

                var possiblePlatform = assetNameNoExt.Substring(assetNameNoExt.LastIndexOf('_') + 1).ToUpper();

                if (possiblePlatform == platform)
                {
                    continue;
                }

                assetsToRemove.AddRange(
                    from currPlatform in this.settings.Platforms
                    where currPlatform.ToUpper() == possiblePlatform 
                    select assetName);
            }

            foreach (var assetName in assetsToRemove)
            {
                p4AssetNames.Remove(assetName);
            }

            // Update error list if errors found
            if (p4AssetNames.Count > 0)
            {
                this.errorList.Add(string.Format("{0} asset(s) in perforce not in {1} built waves:", p4AssetNames.Count, platform));
                this.errorList.AddRange(p4AssetNames);
            }

            if (this.errorList.Count > 0)
            {
                string errorText = string.Format("Depot check completed; found {0} errors", this.errorList.Count);
                foreach (string error in errorList) errorText += Environment.NewLine + error;
                logger.Warn(errorText);
            }
            else 
            {
                logger.Info("Depot check completed, no errors found");
            }
        }

        /// <summary>
        /// Send the report, if required.
        /// </summary>
        /// <param name="mailAddress">
        /// The mail address.
        /// </param>
        /// <param name="pack">
        /// The pack.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        private void SendReport(MailAddress mailAddress, string pack, string platform)
        {
            if (this.errorList.Count <= 0)
            {
                return;
            }

            var subject = string.Format(
                "[{0}] Wave Depot Report - {1} ({2})", this.settings.Project, pack, platform.ToUpper());

            var email = new MailMessage
                {
                    From = this.settings.From,
                    Subject = subject,
                    IsBodyHtml = false
                };

            email.To.Add(mailAddress);

            var messageBuilder = new StringBuilder();
            foreach (var error in this.errorList)
            {
                messageBuilder.AppendLine(error);
            }

            email.Body = messageBuilder.ToString();

            using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
            {
                smtp.Send(email);
            }
        }
    }
}
