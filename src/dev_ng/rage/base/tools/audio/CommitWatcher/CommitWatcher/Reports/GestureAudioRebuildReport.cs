﻿using NLog;
using rage;
using Rockstar.AssetManager.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommitWatcher.Reports
{
    public class GestureAudioRebuildReport
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly IAssetManager assetManager;
        private audProjectSettings projectSettings;
        private readonly Settings settings;
        string localWavesBaseDir;



        public GestureAudioRebuildReport(Settings settings, IAssetManager assetManager) 
        {
            this.assetManager = assetManager;
            this.settings = settings;
            this.projectSettings = settings.ProjectSettings;
            this.localWavesBaseDir = assetManager.GetLocalPath(projectSettings.GetWaveInputPath());
        }

        public void Run() 
        {
            logger.Info("Running Gesture audio rebuild report - Checking for any outdated awc file");
            assetManager.GetLatest(localWavesBaseDir, false);

            Dictionary<string, LipsyncAudioRebuildReport.AudioRebuildInfo> buildInfoLookupTable = new Dictionary<string, LipsyncAudioRebuildReport.AudioRebuildInfo>();

            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            CreateBuildInfoLookupTableForAllFiles(localWavesBaseDir, buildInfoLookupTable, new LipsyncAudioRebuildReport.BuiltWavesXmlCacher(this.assetManager, this.projectSettings));
            sw.Stop();
            logger.Info(sw.Elapsed + " needed to generate buildInfoLookupTable");
            BuildOutdatedAWCs(buildInfoLookupTable);
        }

        public void Run(Data.P4ChangeList.ChangeListFile[] clFiles)
        {
            if (clFiles.Length == 0)
            {
                return;
            }

            List<string> gestureFiles = new List<string>();
            foreach (Data.P4ChangeList.ChangeListFile clFile in clFiles)
            {
                if (clFile.File.ToUpper().Contains(".GESTURE"))
                {
                    gestureFiles.Add(clFile.File);
                }
            }
            if (gestureFiles.Count == 0) return;

            logger.Info("Running gesture audio rebuild report - Checking newly submitted gesture files");

            Dictionary<string, LipsyncAudioRebuildReport.AudioRebuildInfo> buildInfoLookupTable = new Dictionary<string, LipsyncAudioRebuildReport.AudioRebuildInfo>();

            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            LipsyncAudioRebuildReport.BuiltWavesXmlCacher xmlCacher = new LipsyncAudioRebuildReport.BuiltWavesXmlCacher(this.assetManager, this.projectSettings);
            foreach (string gestureFilePath in gestureFiles)
            {
                AddBuildInfoLookupTableEntryforFile(this.assetManager.GetLocalPath(gestureFilePath), buildInfoLookupTable, xmlCacher);
            }
            sw.Stop();
            logger.Info(sw.Elapsed + " needed to generate buildInfoLookupTable");
            BuildOutdatedAWCs(buildInfoLookupTable);
        }

        private void CreateBuildInfoLookupTableForAllFiles(string directory, Dictionary<string, LipsyncAudioRebuildReport.AudioRebuildInfo> buildInfoLookupTable, LipsyncAudioRebuildReport.BuiltWavesXmlCacher builtWavesXmlCacher)
        {
            foreach (string entry in Directory.GetFileSystemEntries(directory))
            {
                if (Directory.Exists(entry))
                {
                    CreateBuildInfoLookupTableForAllFiles(entry, buildInfoLookupTable, builtWavesXmlCacher);
                }
                else
                {
                    if (entry.ToUpper().EndsWith(".GESTURE"))
                    {
                        AddBuildInfoLookupTableEntryforFile(entry, buildInfoLookupTable, builtWavesXmlCacher);
                    }
                }
            }
        }

        private void AddBuildInfoLookupTableEntryforFile(string gestureFilePath, Dictionary<string, LipsyncAudioRebuildReport.AudioRebuildInfo> buildInfoLookupTable, LipsyncAudioRebuildReport.BuiltWavesXmlCacher builtWavesXmlCacher)
        {
            string correspondingWaveFile = Path.Combine(Path.GetDirectoryName(gestureFilePath), Path.GetFileNameWithoutExtension(gestureFilePath) + ".WAV");
            DateTime submitDate = assetManager.GetLatestCheckinTime(gestureFilePath);
            if (!buildInfoLookupTable.ContainsKey(correspondingWaveFile))
            {
                LipsyncAudioRebuildReport.AudioRebuildInfo lookupTableEntry = new LipsyncAudioRebuildReport.AudioRebuildInfo();
                LipsyncAudioRebuildReport.PackBankInfo packBankInfo = builtWavesXmlCacher.GetPackAndBankName(correspondingWaveFile);
                lookupTableEntry.PackName = packBankInfo.PackName;
                lookupTableEntry.BankName = packBankInfo.BankName;
                lookupTableEntry.WaveName = correspondingWaveFile;
                lookupTableEntry.BankPath = packBankInfo.BankPath;
                lookupTableEntry.LatestDataUpdate = submitDate;
                buildInfoLookupTable.Add(correspondingWaveFile, lookupTableEntry);
            }
            else
            {
                if (buildInfoLookupTable[correspondingWaveFile].LatestDataUpdate < submitDate)
                {
                    buildInfoLookupTable[correspondingWaveFile].LatestDataUpdate = submitDate;
                }
            }
        }

        public void BuildOutdatedAWCs(Dictionary<string, LipsyncAudioRebuildReport.AudioRebuildInfo> buildInfoLookupTable)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            HashSet<string> bankPathsToRebuild = new HashSet<string>();
            foreach (var kvp in buildInfoLookupTable)
            {
                //use platforms from projectSettings.xml, we probably could have also used the platforms from commitwatcher settings.xml
                foreach (PlatformSetting platformSetting in projectSettings.GetPlatformSettings())
                {
                    if (!platformSetting.IsActive) continue;
                    if (bankPathsToRebuild.Contains(kvp.Value.BankPath)) continue;
                    string localAwcPath = Path.Combine(settings.ProjectSettings.GetBackupWorkingPath() + platformSetting.BuildOutput, "SFX", kvp.Value.PackName, kvp.Value.BankName + ".AWC");
                    if (kvp.Value.LatestDataUpdate > assetManager.GetLatestCheckinTime(localAwcPath)) bankPathsToRebuild.Add(kvp.Value.BankPath);
                }
            }
            sw.Stop();
            logger.Info(sw.Elapsed + " needed to find banks to rebuild");
            List<string> platforms = new List<string>();
            foreach (PlatformSetting platformSetting in projectSettings.GetPlatformSettings()) platforms.Add(platformSetting.PlatformTag);
            BankRebuilder.MarkBanksForRebuildAndSubmit(bankPathsToRebuild.ToList(), platforms, assetManager, settings);
        }

    }
}
