﻿// -----------------------------------------------------------------------
// <copyright file="CommitReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Reports
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;

    using global::CommitWatcher.Data;

    using rage;
    using rage.Compiler;
    using rage.Fields;
    using rage.Reflection;
    using rage.ToolLib.Logging;
    using rage.Types;
    using NLog;

    using Rockstar.AssetManager.Interfaces;

    using ContextStack = rage.ToolLib.Logging.ContextStack;

    /// <summary>
    /// Generate and send commit reports.
    /// </summary>
    public class CommitReport
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The DLL extension.
        /// </summary>
        private const string DllExtension = ".dll";

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;

        /// <summary>
        /// The type definitions managers.
        /// </summary>
        private IDictionary<audMetadataType, TypeDefinitionsManager> typeDefinitionsManagers;

        /// <summary>
        /// The JIT paths.
        /// </summary>
        private IList<string> jitPaths;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommitReport"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public CommitReport(Settings settings, IAssetManager assetManager)
        {
            this.settings = settings;
            this.assetManager = assetManager;
        }

        /// <summary>
        /// Run the report generator.
        /// </summary>
        /// <param name="filePath">
        /// The file Path.
        /// </param>
        /// <param name="changeList">
        /// The change List.
        /// </param>
        /// <param name="mailAddresses">
        /// The mail addresses to send report to.
        /// </param>
        public void Run(string filePath, P4ChangeList.ChangeList changeList, IList<MailAddress> mailAddresses)
        {
            if (!mailAddresses.Any())
            {
                // No one to send report to, no point generating it
                return;
            }

            logger.Info("Running commit report on " + filePath);

            this.InitTypeDefinitionsLoader();

            try
            {
                var sr = new StreamReader(filePath);
                var elems = sr.ReadLine().Split(':');
                sr.Close();

                var client = elems[0];
                var sb = new StringBuilder();

                // Output header HTML
                sb.AppendLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">"); 
                sb.AppendLine("<html>");
                sb.AppendLine("<head>");
                sb.AppendLine("<style type=\"text/css\">");
                sb.AppendLine("<!--");
                sb.AppendLine("body, table");
                sb.AppendLine("{");
                sb.AppendLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
                sb.AppendLine("}");
                sb.AppendLine("div.indented");
                sb.AppendLine("{");
                sb.AppendLine("padding-left: 50pt;");
                sb.AppendLine("padding-right: 50pt;");
                sb.AppendLine("}");
                sb.AppendLine("table");
                sb.AppendLine("{");
                sb.AppendLine("border-collapse: collapse;");
                sb.AppendLine("margin: 8px 0px;");
                sb.AppendLine("}");
                sb.AppendLine("table td, table th");
                sb.AppendLine("{");
                sb.AppendLine("padding: 2px 15px;");
                sb.AppendLine("}");
                sb.AppendLine("table th");
                sb.AppendLine("{");
                sb.AppendLine("border-top: 1px solid #FB7A31;");
                sb.AppendLine("border-bottom: 1px solid #FB7A31;");
                sb.AppendLine("background: #FFC;");
                sb.AppendLine("font-weight: bold;");
                sb.AppendLine("}");
                sb.AppendLine("table td");
                sb.AppendLine("{");
                sb.AppendLine("border-bottom: 1px solid #CCC;");
                sb.AppendLine("padding: 0 0.5em;");
                sb.AppendLine("}");
                sb.AppendLine("-->");
                sb.AppendLine("</style>");
                sb.AppendLine("</head>");

                // Output body HTML
                sb.AppendLine("<body>");
                sb.AppendLine("<p>");
                sb.AppendLine("<strong>" + changeList.Header + "</strong>");
                sb.AppendLine("</p>");
                sb.AppendLine("<p>");
                sb.AppendLine(changeList.Comment);
                sb.AppendLine("</p>");

                // Only send email if report isn't empty
                if (this.DescribeChangedFiles(changeList, sb))
                {
                    sb.AppendLine("</body>");
                    sb.AppendLine("</html>");

                    var email = new MailMessage
                                    {
                                        From = this.settings.From,
                                        Subject =
                                            string.Format(
                                                "[{0}] Rave Commit by {1}", this.settings.Project, client),
                                        IsBodyHtml = true
                                    };

                    foreach (var mailAddress in mailAddresses)
                    {
                        email.To.Add(mailAddress);
                    }

                    // Send email
                    email.Body = sb.ToString();
                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
                    {
                        smtp.Send(email);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        /// <summary>
        /// Get action from an operation.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The action <see cref="string"/>.
        /// </returns>
        private static string GetActionFromOperation(string operation)
        {
            switch (operation.ToLower())
            {
                case "edit":
                    return "Modified";
                case "add":
                    return "Added";
                case "delete":
                    return "Deleted";
            }

            return string.Empty;
        }

        /// <summary>
        /// Get specified field definition.
        /// </summary>
        /// <param name="typeDef">
        /// The type definition.
        /// </param>
        /// <param name="fieldType">
        /// The field type.
        /// </param>
        /// <returns>
        /// The field definition <see cref="IBasicFieldDefinition"/>.
        /// </returns>
        private static ICommonFieldDefinition GetFieldDefinition(ITypeDefinition typeDef, string fieldType)
        {
            if (null == typeDef)
            {
                return null;
            }

            var fieldTypeLower = fieldType.ToLower();

            ICommonFieldDefinition fieldDef = null;
            var currTypeDef = typeDef;
            while (null != currTypeDef && null == fieldDef)
            {
                fieldDef = currTypeDef.AllFields.FirstOrDefault(f => f.Name.ToLower() == fieldTypeLower);
                currTypeDef = currTypeDef.InheritsFrom;
            }

            if (fieldDef == null)
            {
                // Check for composite field types
                foreach (var currFieldDef in typeDef.AllFields)
                {
                    var compFieldDef = currFieldDef as ICompositeFieldDefinition;
                    if (null != compFieldDef)
                    {
                        fieldDef = compFieldDef.AllFields.FirstOrDefault(f => f.Name.ToLower() == fieldTypeLower);
                        if (null != fieldDef)
                        {
                            break;
                        }
                    }
                }
            }

            return fieldDef;
        }

        /// <summary>
        /// Initialize the type definitions loader.
        /// </summary>
        private void InitTypeDefinitionsLoader()
        {
            var log = new TextLog(new NullLogWriter(), new ContextStack());

            this.jitPaths = new List<string>();

            foreach (var path in this.settings.ProjectSettings.MetadataCompilerJitPaths)
            {
                var localPath = this.assetManager.GetLocalPath(path);
                this.jitPaths.Add(localPath);
            }

            this.typeDefinitionsManagers = new Dictionary<audMetadataType, TypeDefinitionsManager>();

            AppDomain.CurrentDomain.AssemblyResolve += this.OnAssemblyResolve;

            foreach (var metadataType in this.settings.ProjectSettings.GetMetadataTypes())
            {
                var reflector = new Reflector(log, this.jitPaths.ToArray());
                var lookup = new CompiledObjectLookup();
                var stringTable = new StringTable();
                var localXmlPath = this.assetManager.GetLocalPath(this.settings.ProjectSettings.GetSoundXmlPath())
                                       .Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);

                var manager = new TypeDefinitionsManager(log, reflector, lookup, stringTable);
                this.typeDefinitionsManagers.Add(metadataType, manager);

                if (!manager.Load(localXmlPath, metadataType.ObjectDefinitions))
                {
                    logger.Warn("Failed to load type definitions: " + metadataType.Type);
                }
            }

            logger.Info("Loaded {0} type definition manager(s)", this.typeDefinitionsManagers.Count);

            AppDomain.CurrentDomain.AssemblyResolve -= this.OnAssemblyResolve;
        }

        /// <summary>
        /// JIT assembly resolver.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The resolved assembly <see cref="Assembly"/>.
        /// </returns>
        private Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (null != this.jitPaths)
            {
                var name = new AssemblyName(args.Name);
                foreach (var jitPath in this.jitPaths)
                {
                    var assemblyFile = Path.Combine(jitPath, string.Concat(name.Name, DllExtension));
                    if (File.Exists(assemblyFile))
                    {
                        return Assembly.LoadFile(assemblyFile);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Describe changed files.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="sb">
        /// The string builder.
        /// </param>
        /// <returns>
        /// Value indicating whether report needs to be sent (i.e. not empty/no failures) <see cref="bool"/>.
        /// </returns>
        private bool DescribeChangedFiles(P4ChangeList.ChangeList changeList, StringBuilder sb)
        {
            if (!changeList.Files.Any())
            {
                return false;
            }

            var xmlFiles = new List<P4ChangeList.ChangeListFile>();
            var bankFiles = new List<P4ChangeList.ChangeListFile>();
            var otherFiles = new List<P4ChangeList.ChangeListFile>();

            // Initialize file lists
            foreach (var file in changeList.Files)
            {
                var ext = Path.GetExtension(file.File);

                if (ext.Equals(".XML", StringComparison.OrdinalIgnoreCase)
                    && !file.File.ToLower().Contains("builtwaves") 
                    && !file.File.ToLower().Contains("pendingwaves"))
                {
                    // Wave slot or general XML file
                    if (!file.File.ToLower().Contains("waveslots"))
                    {
                        xmlFiles.Add(file);
                    }
                }
                else if (ext.Equals(".AWC", StringComparison.OrdinalIgnoreCase))
                {
                    // Bank file
                    bankFiles.Add(file);
                }
                else if (!file.Operation.Equals("edit", StringComparison.OrdinalIgnoreCase)
                    && !file.File.Contains("PendingWaves"))
                {
                    // Other file (we ignore file edits with no additional info and pending wave changes)
                    otherFiles.Add(file);
                }
            }

            this.DescribeFileActions(otherFiles, sb);
            this.DescribeXmlFiles(xmlFiles, sb);
            this.DescribeWaveBankGroups(bankFiles, sb);
            this.DescribeWaveBanks(bankFiles, sb);

            return xmlFiles.Any() || bankFiles.Any() || otherFiles.Any();
        }

        /// <summary>
        /// Describe file actions.
        /// </summary>
        /// <param name="files">
        /// The change list files.
        /// </param>
        /// <param name="sb">
        /// The string builder.
        /// </param>
        private void DescribeFileActions(IList<P4ChangeList.ChangeListFile> files, StringBuilder sb)
        {
            if (!files.Any())
            {
                return;
            }

            sb.AppendLine("<p>Files:</p>");
            sb.AppendLine("<div class=\"indented\">");
            sb.AppendLine("<table>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<th>File Name</th>");
            sb.AppendLine("<th>Action</th>");
            sb.AppendLine("</tr>");

            foreach (var file in files)
            {
                sb.AppendLine("<tr>");
                sb.AppendLine("<td>" + Utils.MakePrettyFileName(file.File) + "</td>");
                sb.AppendLine("<td>" + GetActionFromOperation(file.Operation) + "</td>");
                sb.AppendLine("</tr>");
            }

            sb.AppendLine("</table>");
            sb.AppendLine("</div>");
        }

        /// <summary>
        /// Describe the wave bank groups/
        /// </summary>
        /// <param name="files">
        /// The files.
        /// </param>
        /// <param name="sb">
        /// The string builder.
        /// </param>
        private void DescribeWaveBankGroups(IList<P4ChangeList.ChangeListFile> files, StringBuilder sb)
        {
            if (!files.Any())
            {
                return;
            }

            // Sort files into groups based on their platform
            var banksByPlatform = new Dictionary<string, IList<BankDetails>>();
            foreach (var file in files)
            {
                var platform = Utils.GetPlatfromFromFile(file, this.settings).ToUpper();
                if (!banksByPlatform.ContainsKey(platform))
                {
                    banksByPlatform[platform] = new List<BankDetails>();
                }

                var bankName = Path.GetFileNameWithoutExtension(file.File);
                var packName = Utils.GetPackNameFromAwcPath(file.File);

                if (string.IsNullOrEmpty(packName))
                {
                    // Just in case we end up with a non-AWC file at this point
                    continue;
                }

                var bankDetails = new BankDetails { Name = bankName, FilePath = file.File, Platform = platform, PackName = packName };
                banksByPlatform[platform].Add(bankDetails);
            }

            // Process each group of banks by platform
            foreach (var platformBanks in banksByPlatform)
            {
                var platform = platformBanks.Key;
                var banks = platformBanks.Value;

                // Check if first time running bank group sizes checker for current platform
                var bankSizePath = Regex.Replace(
                    this.settings.BankSizePath, "{platform}", platform, RegexOptions.IgnoreCase);
                if (!File.Exists(bankSizePath))
                {
                    this.InitBankSizesList(platform);
                }

                IList<BankDetails> bankDetails;
                if (!this.LoadBankDetails(out bankDetails, platform))
                {
                    return;
                }

                // Determine current bank sizes based on submitted files and existing data
                foreach (var bank in banks)
                {
                    var details = bankDetails.FirstOrDefault(b => string.Compare(b.FilePath, bank.FilePath, StringComparison.OrdinalIgnoreCase) == 0);
                    if (null == details)
                    {
                        details = bank;
                        bankDetails.Add(details);
                    }

                    details.PackName = bank.PackName;

                    // Check if file has been deleted
                    if (!this.assetManager.ExistsAsAsset(bank.FilePath))
                    {
                        details.CurrentSize = 0;
                    }
                    else
                    {
                        details.CurrentSize = (int)Utils.GetFileSize(bank.FilePath);
                    }
                }

                var bankGroupDetails = this.GetBankGroupSizes(bankDetails);

                sb.AppendLine("<p>Wave Bank Groups:</p>");
                sb.AppendLine("<div class=\"indented\">");
                sb.AppendLine("<table>");
                sb.AppendLine("<tr>");
                sb.AppendLine("<th>Group Name</th>");
                sb.AppendLine("<th>Platform</th>");
                sb.AppendLine("<th>Size</th>");
                sb.AppendLine("<th>Size &#916;</th>");
                sb.AppendLine("</tr>");

                foreach (var groupDetails in bankGroupDetails)
                {
                    var sizedDelta = groupDetails.SizeDelta != 0 ? Utils.FormatBytes(groupDetails.SizeDelta) : "-";

                    sb.AppendLine("<tr>");
                    sb.AppendLine("<td>" + groupDetails.Name + "</td>");
                    sb.AppendLine("<td>" + groupDetails.Platform + "</td>");
                    sb.AppendLine("<td>" + Utils.FormatBytes(groupDetails.CurrentSize) + "</td>");
                    sb.AppendLine("<td>" + sizedDelta + "</td>");
                    sb.AppendLine("</tr>");
                }

                sb.AppendLine("</table>");
                sb.AppendLine("</div>");

                this.SaveBankSizes(bankDetails, platform);
            }
        }

        /// <summary>
        /// Initialize the list of bank sizes and save to file.
        /// </summary>
        /// <param name="platform">
        /// The platform.
        /// </param>
        private void InitBankSizesList(string platform)
        {
            var sfxPath = Regex.Replace(this.settings.RuntimeSfxPath, "{platform}", platform, RegexOptions.IgnoreCase);
            if (!sfxPath.EndsWith("/"))
            {
                sfxPath += "/";
            }

            sfxPath += "...";

            var filesList = Utils.RunCommand("p4", "files " + sfxPath);
            var banks = new List<BankDetails>();

            foreach (var p4File in filesList.Split('\n'))
            {
                var split = p4File.Split(' ');
                var path = split[0].ToUpper();
                if (path.Contains("#"))
                {
                    path = path.Substring(0, path.IndexOf("#", StringComparison.Ordinal));
                }

                // Only interested in AWC files
                if (!path.EndsWith(".AWC"))
                {
                    continue;
                }

                // Skip file if it's deleted at head revision
                var headAction = split[2].ToUpper();
                if (headAction.Contains("DELETE"))
                {
                    continue;
                }

                var bankName = Path.GetFileNameWithoutExtension(path);
                var packName = Utils.GetPackNameFromAwcPath(path);

                // Get size of AWC file
                var bankSize = Utils.GetFileSize(path);

                var details = new BankDetails
                {
                    Name = bankName,
                    CurrentSize = (int)bankSize,
                    PackName = packName,
                    Platform = platform,
                    FilePath = path
                };

                banks.Add(details);
            }

            // Save bank details to file
            this.SaveBankSizes(banks, platform);
        }

        /// <summary>
        /// Load the bank sizes.
        /// </summary>
        /// <param name="bankDetails">
        /// The bank details.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <returns>
        /// Indication of success <see cref="bool"/>.
        /// </returns>
        private bool LoadBankDetails(out IList<BankDetails> bankDetails, string platform)
        {
            bankDetails = new List<BankDetails>();

            var path = Regex.Replace(this.settings.BankSizePath, "{platform}", platform);
            if (!File.Exists(path))
            {
                // No previous info saved, so return with empty list
                return true;
            }

            using (var file = new StreamReader(path))
            {
                string line;
                while (null != (line = file.ReadLine()))
                {
                    if (string.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }

                    var elems = line.Split(',');
                    var bankName = elems[0].ToUpper();
                    var packName = elems[1].ToUpper();
                    var bankFilePath = elems[2];
                    int bankSize;
                    if (!int.TryParse(elems[3], out bankSize))
                    {
                        return false;
                    }

                    var details = new BankDetails
                                      {
                                          Name = bankName,
                                          FilePath = bankFilePath,
                                          Platform = platform,
                                          PackName = packName,
                                          OriginalSize = bankSize,
                                          CurrentSize = bankSize,
                                      };
                    bankDetails.Add(details);
                }
            }

            return true;
        }

        /// <summary>
        /// Save the bank sizes.
        /// </summary>
        /// <param name="bankDetails">
        /// The bank details.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        private void SaveBankSizes(IEnumerable<BankDetails> bankDetails, string platform)
        {
            var path = Regex.Replace(this.settings.BankSizePath, "{platform}", platform);
            var dirPath = Path.GetDirectoryName(path);
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            using (var writer = new StreamWriter(path))
            {
                foreach (var bank in bankDetails)
                {
                    writer.WriteLine("{0},{1},{2},{3}", bank.Name, bank.PackName, bank.FilePath, bank.CurrentSize);
                }
            }
        }

        /// <summary>
        /// Get bank group sizes and their deltas.
        /// </summary>
        /// <param name="bankDetails">
        /// The bank details.
        /// </param>
        /// <returns>
        /// The bank group details <see cref="IDictionary"/>.
        /// </returns>
        private IEnumerable<BankGroupDetails> GetBankGroupSizes(IEnumerable<BankDetails> bankDetails)
        {
            var results = new List<BankGroupDetails>();

            foreach (var details in bankDetails)
            {
                var groupName = this.GetBankGroup(details.PackName);
                var group = results.FirstOrDefault(b => b.Name == groupName && b.Platform == details.Platform);
                if (null == group)
                {
                    group = new BankGroupDetails { Name = groupName, Platform = details.Platform };
                    results.Add(group);
                }

                group.Banks.Add(details);
            }

            return results;
        }

        /// <summary>
        /// Get the bank group for a given bank name.
        /// </summary>
        /// <param name="bankName">
        /// The bank name.
        /// </param>
        /// <returns>
        /// The bank group <see cref="string"/>.
        /// </returns>
        private string GetBankGroup(string bankName)
        {
            foreach (var kvp in this.settings.WaveBankGroups)
            {
                var name = kvp.Key;
                var patterns = kvp.Value;

                if (patterns.Any(pattern => Regex.IsMatch(bankName, pattern, RegexOptions.IgnoreCase)))
                {
                    return name;
                }
            }

            return null;
        }

        /// <summary>
        /// Describe the wave banks.
        /// </summary>
        /// <param name="files">
        /// The files.
        /// </param>
        /// <param name="sb">
        /// The string builder.
        /// </param>
        private void DescribeWaveBanks(IList<P4ChangeList.ChangeListFile> files, StringBuilder sb)
        {
            if (!files.Any())
            {
                return;
            }

            sb.AppendLine("<p>Wave Bank Changes:</p>");
            sb.AppendLine("<div class=\"indented\">");
            sb.AppendLine("<table>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<th>Bank Name</th>");
            sb.AppendLine("<th>Action</th>");
            sb.AppendLine("<th>Platform</th>");
            sb.AppendLine("<th>Size</th>");
            sb.AppendLine("<th>Size &#916;</th>");
            sb.AppendLine("</tr>");

            foreach (var file in files)
            {
                var action = GetActionFromOperation(file.Operation);
                var platform = Utils.GetPlatfromFromFile(file, this.settings);
                var size = Utils.GetFileSize(file.File);
                var sizeChangeBytesNullable = Utils.GetFileSizeChange(file.File, file.Revision - 1, file.Revision);

                sb.AppendLine("<tr>");
                sb.AppendLine("<td>" + Utils.MakePrettyFileName(file.File) + "</td>");
                sb.AppendLine("<td>" + action + "</td>");
                sb.AppendLine("<td>" + platform + "</td>");
                if (null != size)
                {
                    sb.AppendLine("<td>" + Utils.FormatBytes(size.Value) + "</td>");
                }
                else
                {
                    sb.AppendLine("<td>-</td>");
                }

                if (null != sizeChangeBytesNullable)
                {
                    sb.AppendFormat("<td>" + Utils.FormatBytes(sizeChangeBytesNullable.Value) + "</td>");
                }
                else
                {
                    sb.AppendLine("<td>-</td>");
                }

                sb.AppendLine("</tr>");
            }

            sb.AppendLine("</table>");
            sb.AppendLine("</div>");
        }

        /// <summary>
        /// Describe the xml files.
        /// </summary>
        /// <param name="files">
        /// The change list files.
        /// </param>
        /// <param name="sb">
        /// The string builder.
        /// </param>
        private void DescribeXmlFiles(IList<P4ChangeList.ChangeListFile> files, StringBuilder sb)
        {
            var filesAdded = new List<P4ChangeList.ChangeListFile>();
            var filesDeleted = new List<P4ChangeList.ChangeListFile>();

            foreach (var file in files)
            {
                var oldFile = Utils.RunCommand("p4", "print " + file.File + "#" + (file.Revision - 1));
                var newFile = Utils.RunCommand("p4", "print " + file.File + "#" + file.Revision);

                if (oldFile.Length > 0 && newFile.Length > 0)
                {
                    // Skip over the header line
                    if (oldFile.IndexOf('<') == -1) oldFile = "";
                    else oldFile = oldFile.Substring(oldFile.IndexOf('<')).Trim();

                    if (newFile.IndexOf('<') == -1) newFile = "";
                    else newFile = newFile.Substring(newFile.IndexOf('<')).Trim();
                }

                // Keep track of files deleted/added
                if (oldFile.Length == 0)
                {
                    filesAdded.Add(file);
                }
                else if (newFile.Length == 0)
                {
                    filesDeleted.Add(file);
                }
                else
                {
                    sb.Append("<p>Changed File " + Utils.MakePrettyFileName(file.File) + ":");
                    sb.Append("<div class=\"indented\">");

                    try
                    {
                        var oldDoc = new XmlDocument();
                        oldDoc.LoadXml(oldFile);
                        var newDoc = new XmlDocument();
                        newDoc.LoadXml(newFile);

                        var oldObjects = new Hashtable();
                        foreach (XmlNode node in oldDoc.DocumentElement.ChildNodes)
                        {
                            oldObjects.Add(node.Attributes["name"].Value, node);
                        }

                        var newObjects = new Hashtable();
                        foreach (XmlNode node in newDoc.DocumentElement.ChildNodes)
                        {
                            newObjects.Add(node.Attributes["name"].Value, node);
                        }

                        foreach (DictionaryEntry entry in oldObjects)
                        {
                            var oldNode = (XmlNode)entry.Value;
                            var newNode = (XmlNode)newObjects[entry.Key];

                            if (newNode == null)
                            {
                                sb.AppendFormat("<p>Deleted {0} object <strong>{1}</strong></p>", oldNode.Name, entry.Key);
                            }
                            else
                            {
                                if (oldNode.InnerText != newNode.InnerText)
                                {
                                    sb.AppendFormat("<p>Changed {0} object <strong>{1}</strong></p>", newNode.Name, entry.Key);

                                    if (!newNode.Name.Equals("ModularSynth", StringComparison.OrdinalIgnoreCase))
                                    {
                                        sb.AppendLine("<table>");
                                        sb.AppendLine("<tr>");
                                        sb.AppendLine("<th>Field</th>");
                                        sb.AppendLine("<th>Action</th>");
                                        sb.AppendLine("<th>Old Value</th>");
                                        sb.AppendLine("<th>New Value</th>");
                                        sb.AppendLine("<th>Delta</th>");
                                        sb.AppendLine("</tr>");
                                        this.CompareXmlObjects(oldNode, newNode, sb, string.Empty);
                                        sb.AppendLine("</table>");
                                    }
                                    else
                                    {
                                        sb.AppendLine("<br />");
                                    }
                                }
                            }
                        }

                        foreach (DictionaryEntry entry in newObjects)
                        {
                            if (oldObjects[entry.Key] == null)
                            {
                                var name = (entry.Value as XmlNode).Name;
                                sb.AppendFormat("<p>Added {0} object <strong>{1}</strong></p>", name, entry.Key);
                            }
                        }

                        sb.AppendLine("</div>");
                    }
                    catch (Exception ex)
                    {
                        sb.AppendLine("<p><b>Parse error: </b>" + ex.Message + "</p>");
                    }
                }
            }
        }

        /// <summary>
        /// The compare xml objects.
        /// </summary>
        /// <param name="oldNode">
        /// The old node.
        /// </param>
        /// <param name="newNode">
        /// The new node.
        /// </param>
        /// <param name="sb">
        /// The string builder.
        /// </param>
        /// <param name="fieldBase">
        /// The field base.
        /// </param>
        /// <param name="parentFieldDefinition">
        /// The parent field definition.
        /// </param>
        private void CompareXmlObjects(XmlNode oldNode, XmlNode newNode, StringBuilder sb, string fieldBase, ICompositeFieldDefinition parentFieldDefinition = null)
        {
            // Find the type definition for the current node
            ITypeDefinition typeDef = null;
            foreach (var manager in this.typeDefinitionsManagers.Values)
            {
                typeDef = manager.FindTypeDefinition(newNode.Name);
                if (null != typeDef)
                {
                    break;
                }
            }

            var matchedFields = new ArrayList();
            foreach (XmlNode oldField in oldNode.ChildNodes)
            {
                var fieldDef = null != parentFieldDefinition ? 
                                    GetFieldDefinition(parentFieldDefinition.TypeDefinition, oldField.Name) : 
                                    GetFieldDefinition(typeDef, oldField.Name);

                if (null == fieldDef)
                {
                    logger.Warn("Failed to find field definition: " + oldField.Name);
                }

                var fieldName = fieldBase + oldField.Name;
                XmlNode newField = null;
                foreach (XmlNode node in newNode.ChildNodes)
                {
                    if (node.Name == oldField.Name && !matchedFields.Contains(node))
                    {
                        newField = node;
                        break;
                    }
                }

                if (newField != null)
                {
                    matchedFields.Add(newField);
                    if (newField.InnerText != oldField.InnerText)
                    {
                        if (newField.ChildNodes.Count > 1 || oldField.ChildNodes.Count > 1)
                        {
                            this.CompareXmlObjects(oldField, newField, sb, fieldName + "/", fieldDef as ICompositeFieldDefinition);
                        }
                        else
                        {
                            sb.AppendLine("<tr>");
                            sb.AppendLine("<td>" + fieldName + "</td>");
                            sb.AppendLine("<td>Modified</td>");
                            sb.AppendLine("<td>" + this.GetPrettyValue(oldField, fieldDef) + "</td>");
                            sb.AppendLine("<td>" + this.GetPrettyValue(newField, fieldDef) + "</td>");
                            sb.AppendLine("<td>" + this.GetFormattedValueDiff(oldField.InnerText, newField.InnerText, fieldDef) + "</td>");
                            sb.AppendLine("</tr>");
                        }
                    }
                }
                else
                {
                    sb.AppendLine("<tr>");
                    sb.AppendLine("<td>" + fieldName + "</td>");
                    sb.AppendLine("<td>Deleted</td>");
                    sb.AppendLine("<td>" + this.GetPrettyValue(oldField, fieldDef) + "</td>");
                    sb.AppendLine("<td>-</td>");
                    sb.AppendLine("<td>-</td>");
                    sb.AppendLine("</tr>");
                }
            }

            foreach (XmlNode newField in newNode.ChildNodes)
            {
                var fieldName = fieldBase + newField.Name;
                if (matchedFields.Contains(newField))
                {
                    continue;
                }

                var fieldDef = null != parentFieldDefinition ?
                                    GetFieldDefinition(parentFieldDefinition.TypeDefinition, newField.Name) :
                                    GetFieldDefinition(typeDef, newField.Name);

                if (null == fieldDef)
                {
                    logger.Warn("Failed to find field definition: " + newField.Name);
                }

                sb.AppendLine("<tr>");
                sb.AppendLine("<td>" + fieldName + "</td>");
                sb.AppendLine("<td>Added</td>");
                sb.AppendLine("<td>-</td>");
                sb.AppendLine("<td>" + this.GetPrettyValue(newField, fieldDef) + "</td>");
                sb.AppendLine("<td>-</td>");
                sb.AppendLine("</tr>");
            }
        }

        /// <summary>
        /// Get the pretty value.
        /// </summary>
        /// <param name="fieldNode">
        /// The field node.
        /// </param>
        /// <param name="fieldDef">
        /// The field definition.
        /// </param>
        /// <param name="includeFieldName">
        /// The include field name flag.
        /// </param>
        /// <returns>
        /// The prettified value <see cref="string"/>.
        /// </returns>
        private string GetPrettyValue(XmlNode fieldNode, ICommonFieldDefinition fieldDef, bool includeFieldName = false)
        {
            var compositeFieldDef = fieldDef as ICompositeFieldDefinition;

            if (null == compositeFieldDef)
            {
                // Simple field type
                var formattedValue = this.GetFormattedValue(fieldNode.InnerText, fieldDef);
                if (includeFieldName)
                {
                    formattedValue = string.Format("{0}: {1}", fieldDef.Name, formattedValue);
                }

                return formattedValue;
            }
            
            // Format composite field values nicely
            var builder = new StringBuilder();
            foreach (var childFieldDef in compositeFieldDef.AllFields)
            {
                XmlNode foundNode = null;
                foreach (XmlNode childNode in fieldNode.ChildNodes)
                {
                    if (childNode.LocalName == childFieldDef.Name)
                    {
                        foundNode = childNode;
                        break;
                    }
                }

                if (null != foundNode)
                {
                    builder.AppendLine(this.GetPrettyValue(foundNode, childFieldDef, true));
                    builder.AppendLine("<br />");
                }
            }

            return builder.ToString();
        }

        /// <summary>
        /// Get formatted value.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="fieldDefinition">
        /// The field definition.
        /// </param>
        /// <returns>
        /// The formatted value <see cref="string"/>.
        /// </returns>
        private string GetFormattedValue(string value, ICommonFieldDefinition fieldDefinition)
        {
            if (null == fieldDefinition)
            {
                return value;
            }

            switch (fieldDefinition.Unit)
            {
                case Units.Mb:
                    {
                        // Convert mB to decibels
                        double valueMb;
                        if (double.TryParse(value, out valueMb))
                        {
                            var valueDb = valueMb / 100.0;
                            return Utils.FormatDecimalValue(valueDb) + "dB";
                        }

                        return value;
                    }

                case Units.Decibels:
                    {
                        double valueDb;
                        if (double.TryParse(value, out valueDb))
                        {
                            return Utils.FormatDecimalValue(valueDb) + "dB";
                        }

                        return value;
                    }

                case Units.Cents:
                    {
                        // Convert cents to semitones
                        double valueCents;
                        if (double.TryParse(value, out valueCents))
                        {
                            var valueSemitones = valueCents / 100.0;
                            return Utils.FormatDecimalValue(valueSemitones) + " semitones";
                        }

                        return value;
                    }

                case Units.FixedPoint:
                    {
                        double valueFp;
                        if (double.TryParse(value, out valueFp))
                        {
                            var realVal = valueFp / 100.0;
                            return Utils.FormatDecimalValue(realVal);
                        }

                        return value;
                    }

                case Units.Seconds:
                    {
                        double valueS;
                        if (double.TryParse(value, out valueS))
                        {
                            return Utils.FormatDecimalValue(valueS) + "s";
                        }

                        return value;
                    }

                case Units.Ms:
                    {
                        double valueMs;
                        if (double.TryParse(value, out valueMs))
                        {
                            if (Math.Abs(valueMs) >= 1000)
                            {
                                var valueS = valueMs / 1000.0;
                                return Utils.FormatDecimalValue(valueS, 3) + "s";
                            }

                            return value + "ms";
                        }

                        return value;
                    }

                case Units.Percent:
                    {
                        return string.Format("{0}%", value);
                    }
            }

            return value;
        }

        /// <summary>
        /// The get formatted value difference.
        /// </summary>
        /// <param name="oldValue">
        /// The old value.
        /// </param>
        /// <param name="newValue">
        /// The new value.
        /// </param>
        /// <param name="fieldDefinition">
        /// The field definition.
        /// </param>
        /// <returns>
        /// The formatted value difference <see cref="string"/>.
        /// </returns>
        private string GetFormattedValueDiff(string oldValue, string newValue, ICommonFieldDefinition fieldDefinition)
        {
            double oldValueDouble;
            double newValueDouble;
            if (double.TryParse(oldValue, out oldValueDouble) && double.TryParse(newValue, out newValueDouble))
            {
                var diff = newValueDouble - oldValueDouble;
                var sign = diff > 0 ? "+" : string.Empty;
                return sign + this.GetFormattedValue(diff.ToString(CultureInfo.InvariantCulture), fieldDefinition);
            }

            return "-";
        }
    }
}
