﻿// -----------------------------------------------------------------------
// <copyright file="DeliveredDialogueReport.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using NLog;

    using global::CommitWatcher.Data;

    using rage.Fields;
    using rage.Types;

    using Rockstar.AssetManager.Interfaces;


    /// <summary>
    /// Generate a DeliveredDialogueReport and copy the files to a particular location
    /// </summary>
    public class DeliveredDialogueReport
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The settings.
        /// </summary>
        private readonly Settings settings;

        /// <summary>
        /// The asset manager.
        /// </summary>
        private readonly IAssetManager assetManager;


        /// <summary>
        /// Initializes a new instance of the <see cref="DeliveredDialogueReport"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="assetManager">
        /// The asset manager.
        /// </param>
        public DeliveredDialogueReport(Settings settings, IAssetManager assetManager)
        {
            this.settings = settings;
            this.assetManager = assetManager;
        }

        /// <summary>
        /// Run the report generator.
        /// </summary>
        /// <param name="filePath">
        /// The file Path.
        /// </param>
        /// <param name="changeList">
        /// The change List.
        /// </param>
        /// <param name="mailAddresses">
        /// The mail addresses to send report to.
        /// </param>
        public void Run(P4ChangeList.ChangeList changeList, IList<MailAddress> mailAddresses, string deliveredDialogueBasePath)
        {
            try
            {
                ProcessStartInfo grabP4ProcessInfo = new ProcessStartInfo();
                grabP4ProcessInfo.FileName = assetManager.GetLocalPath(settings.GrabP4ApplicationPath);
                grabP4ProcessInfo.UseShellExecute = false;
                grabP4ProcessInfo.Arguments = "-changelist=" + changeList.ChangeListNumber + " -exportpath=" + settings.DialogueDeliveryPath + changeList.ChangeListNumber + "\\ " +
                    "-basepath=" + deliveredDialogueBasePath + " -username=" + settings.P4User + " -password=" + settings.P4Password + " -server=" + settings.P4Port +
                    " -workspace=" + settings.P4Workspace;
                grabP4ProcessInfo.RedirectStandardError = true;
                Process grabP4Process = Process.Start(grabP4ProcessInfo);

                grabP4Process.WaitForExit();
                if (grabP4Process.ExitCode != 0)
                {
                    logger.Error(grabP4Process.StandardError.ReadToEnd());
                }

            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            if (!mailAddresses.Any())
            {
                // No one to send report to, no point generating it
                return;
            }

            logger.Info("Running Delivered Dialogue Report on changelist" + changeList.ChangeListNumber);

            try
            {
                var sb = new StringBuilder();

                // Output header HTML
                sb.AppendLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
                sb.AppendLine("<html>");
                sb.AppendLine("<head>");
                sb.AppendLine("<style type=\"text/css\">");
                sb.AppendLine("<!--");
                sb.AppendLine("body, table");
                sb.AppendLine("{");
                sb.AppendLine("font: 11px/24px Verdana, Arial, Helvetica, sans-serif;");
                sb.AppendLine("}");
                sb.AppendLine("div.indented");
                sb.AppendLine("{");
                sb.AppendLine("padding-left: 50pt;");
                sb.AppendLine("padding-right: 50pt;");
                sb.AppendLine("}");
                sb.AppendLine("table");
                sb.AppendLine("{");
                sb.AppendLine("border-collapse: collapse;");
                sb.AppendLine("margin: 8px 0px;");
                sb.AppendLine("}");
                sb.AppendLine("table td, table th");
                sb.AppendLine("{");
                sb.AppendLine("padding: 2px 15px;");
                sb.AppendLine("}");
                sb.AppendLine("table th");
                sb.AppendLine("{");
                sb.AppendLine("border-top: 1px solid #FB7A31;");
                sb.AppendLine("border-bottom: 1px solid #FB7A31;");
                sb.AppendLine("background: #FFC;");
                sb.AppendLine("font-weight: bold;");
                sb.AppendLine("}");
                sb.AppendLine("table td");
                sb.AppendLine("{");
                sb.AppendLine("border-bottom: 1px solid #CCC;");
                sb.AppendLine("padding: 0 0.5em;");
                sb.AppendLine("}");
                sb.AppendLine("-->");
                sb.AppendLine("</style>");
                sb.AppendLine("</head>");

                // Output body HTML
                sb.AppendLine("<body>");
                sb.AppendLine("<strong>" + "Getting changes made to: " + deliveredDialogueBasePath + "</strong>");
                sb.AppendLine("<p>");
                sb.AppendLine("<strong>" + changeList.Header + "</strong>");
                sb.AppendLine("</p>");
                sb.AppendLine("<p>");
                sb.AppendLine(changeList.Comment);
                sb.AppendLine("</p>");


                bool filesChanged = this.DescribeChangedFiles(changeList.ChangeListNumber, sb, deliveredDialogueBasePath);

                sb.AppendLine("<h4>  Please find all dialogue in " + settings.DialogueDeliveryPath + changeList.ChangeListNumber + "\\ </h4>");
                sb.AppendLine("</body>");
                sb.AppendLine("</html>");

                // Don't send reports with no files attached.
                if (!filesChanged)
                {
                    return;
                }

                var email = new MailMessage
                {
                    From = this.settings.From,
                    Subject =
                        string.Format(
                            "[{0}] DeliveredDialogueReport", this.settings.Project),

                    IsBodyHtml = true
                };


                foreach (var mailAddress in mailAddresses)
                {
                    email.To.Add(mailAddress);
                }

                // Send email
                email.Body = sb.ToString();

                using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(this.settings.SmtpHost))
                {
                    smtp.Send(email);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        /// <summary>
        /// Get specified field definition.
        /// </summary>
        /// <param name="typeDef">
        /// The type definition.
        /// </param>
        /// <param name="fieldType">
        /// The field type.
        /// </param>
        /// <returns>
        /// The field definition <see cref="IBasicFieldDefinition"/>.
        /// </returns>
        private static ICommonFieldDefinition GetFieldDefinition(ITypeDefinition typeDef, string fieldType)
        {
            if (null == typeDef)
            {
                return null;
            }

            var fieldTypeLower = fieldType.ToLower();

            ICommonFieldDefinition fieldDef = null;
            var currTypeDef = typeDef;
            while (null != currTypeDef && null == fieldDef)
            {
                fieldDef = currTypeDef.AllFields.FirstOrDefault(f => f.Name.ToLower() == fieldTypeLower);
                currTypeDef = currTypeDef.InheritsFrom;
            }

            if (fieldDef == null)
            {
                // Check for composite field types
                foreach (var currFieldDef in typeDef.AllFields)
                {
                    var compFieldDef = currFieldDef as ICompositeFieldDefinition;
                    if (null != compFieldDef)
                    {
                        fieldDef = compFieldDef.AllFields.FirstOrDefault(f => f.Name.ToLower() == fieldTypeLower);
                        if (null != fieldDef)
                        {
                            break;
                        }
                    }
                }
            }

            return fieldDef;
        }


        /// <summary>
        /// Describe changed files.
        /// </summary>
        /// <param name="changeList">
        /// The change list.
        /// </param>
        /// <param name="sb">
        /// The string builder.
        /// </param>
        /// <returns>
        /// Value indicating whether report needs to be sent (i.e. not empty/no failures) <see cref="bool"/>.
        /// </returns>
        private bool DescribeChangedFiles(string changeList, StringBuilder sb, string basePath)
        {

            var allFiles = assetManager.GetChangeListFiles(changeList, true);

            this.DescribeFileActions(allFiles, sb, basePath);

            return allFiles.Any();
        }

        /// <summary>
        /// Describe file actions.
        /// </summary>
        /// <param name="files">
        /// The change list files.
        /// </param>
        /// <param name="sb">
        /// The string builder.
        /// </param>
        private void DescribeFileActions(IList<string> files, StringBuilder sb, string basePath)
        {
            if (!files.Any())
            {
                return;
            }

            sb.AppendLine("<p>Files Added/Edited:</p>");
            sb.AppendLine("<div class=\"indented\">");
            sb.AppendLine("<table>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<th>Relative File Path</th>");
            sb.AppendLine("</tr>");

            foreach (var file in files)
            {
                if (file.Contains(basePath))
                {
                    sb.AppendLine("<tr>");
                    sb.AppendLine("<td>" + file.Substring(basePath.Length - 1) + "</td>");
                    sb.AppendLine("</tr>");
                }
            }

            sb.AppendLine("</table>");
            sb.AppendLine("</div>");
        }
    }
}
