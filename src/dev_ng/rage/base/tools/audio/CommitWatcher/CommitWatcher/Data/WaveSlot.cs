﻿// -----------------------------------------------------------------------
// <copyright file="WaveSlot.cs" company="">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcher.Data
{
    /// <summary>
    /// Wave Slot Structures.
    /// </summary>
    public class WaveSlot
    {
        /// <summary>
        /// The wave slot change.
        /// </summary>
        public struct WaveslotChange
        {
            #region Fields

            /// <summary>
            /// The header size delta bytes.
            /// </summary>
            public int HeaderSizeDeltaBytes;

            /// <summary>
            /// The size bytes.
            /// </summary>
            public int SizeBytes;

            /// <summary>
            /// The size delta bytes.
            /// </summary>
            public int SizeDeltaBytes;

            /// <summary>
            /// The slot name.
            /// </summary>
            public string SlotName;

            /// <summary>
            /// The state.
            /// </summary>
            public ChangeState State;

            #endregion

            #region Enums

            /// <summary>
            /// The change state.
            /// </summary>
            public enum ChangeState
            {
                /// <summary>
                /// Existing item.
                /// </summary>
                Existing = 0,

                /// <summary>
                /// Deleted item.
                /// </summary>
                Deleted,

                /// <summary>
                /// New item.
                /// </summary>
                New
            }

            #endregion
        }

        /// <summary>
        /// The wave slot info.
        /// </summary>
        public struct WaveslotInfo
        {
            #region Fields

            /// <summary>
            /// The header size bytes.
            /// </summary>
            public int HeaderSizeBytes;

            /// <summary>
            /// The name.
            /// </summary>
            public string Name;

            /// <summary>
            /// The size bytes.
            /// </summary>
            public int SizeBytes;

            #endregion
        }
    }
}
