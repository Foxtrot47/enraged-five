﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CommitWatcherTest
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net.Mail;
    using System.Xml;

    using CommitWatcher;
    using CommitWatcher.Reports;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// The program.
    /// </summary>
    public class WaveDepotReportTest
    {
        /// <summary>
        /// Run test.
        /// </summary>
        public static void Run()
        {
            /*var packListDepotPath = "//depot/gta5/audio/dev/build/xbox360/BuiltWaves/CUTSCENE_PACK_FILE.xml";
            var mailAddress = new MailAddress("matt.hailey@rockstarnorth.com");

            var eventLog = new EventLog("Application", Environment.MachineName, "Commit Watcher");
            var settingsXml = new XmlDocument();
            settingsXml.Load("Settings.xml");
            var settings = new Settings(settingsXml);
            var report = new WaveDepotReport(eventLog, settings);
            report.Run(packListDepotPath, mailAddress);*/

            var mailAddress = new MailAddress("matt.hailey@rockstarnorth.com");
            var mailAddresses = new List<MailAddress> { mailAddress };

            var eventLog = new EventLog("Application", Environment.MachineName, "Commit Watcher");
            var settingsXml = new XmlDocument();
            settingsXml.Load("Settings.xml");

            var settings = new Settings(settingsXml);

            var assetManager = AssetManagerFactory.GetInstance(
                AssetManagerType.Perforce,
                settings.P4Host,
                settings.P4Port,
                settings.P4Workspace,
                settings.P4User,
                settings.P4Password,
                settings.P4DepotRoot);

            var report = new CollisionSettingsReport(settings, eventLog, assetManager);
            //report.Run(settings.GameObjectsPaths, mailAddresses, settings.ModelAudioCollisionListPath);
        }
    }
}
