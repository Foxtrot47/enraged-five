﻿// -----------------------------------------------------------------------
// <copyright file="CommitReportTest.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

namespace CommitWatcherTest
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net.Mail;
    using System.Xml;

    using CommitWatcher;
    using CommitWatcher.Data;
    using CommitWatcher.Reports;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// Lipsync report test.
    /// </summary>
    public class PendingWaveReportTest
    {
        /// <summary>
        /// Run test.
        /// </summary>
        public static void Run()
        {
            var eventLog = new EventLog("Application", Environment.MachineName, "Lipsync Report");
            var settingsXml = new XmlDocument();
            settingsXml.Load("Settings.xml");

            var settings = new Settings(settingsXml);

            var mailAddress = new MailAddress("robert.katz@rockstarsandiego.com");
            var mailAddresses = new List<MailAddress> { new MailAddress("robert.katz@rockstarsandiego.com"), new MailAddress("christian.kjeldsen@rockstarsandiego.com") };

            var assetManager = AssetManagerFactory.GetInstance(
                AssetManagerType.Perforce,
                settings.P4Host,
                settings.P4Port,
                settings.P4Workspace,
                settings.P4User,
                settings.P4Password,
                settings.P4DepotRoot);

            var report = new PendingWaveReport(eventLog, settings, assetManager);

            var changeList = Utils.RetrieveChangeList("4696063");

            report.Run(changeList.Files, mailAddresses, changeList.ChangeListNumber, settings.Platforms.ToArray());
        }
    }
}
