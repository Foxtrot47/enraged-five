﻿// -----------------------------------------------------------------------
// <copyright file="WaveslotReportTest.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace CommitWatcherTest
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net.Mail;
    using System.Xml;

    using CommitWatcher;
    using CommitWatcher.Reports;

    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// Wave slot report test.
    /// </summary>
    public class WaveslotReportTest
    {
        /// <summary>
        /// Run test.
        /// </summary>
        public static void Run()
        {
            var mailAddress = new MailAddress("matt.hailey@rockstarnorth.com");
            var mailAddresses = new List<MailAddress> { mailAddress };

            var eventLog = new EventLog("Application", Environment.MachineName, "Commit Watcher");
            var settingsXml = new XmlDocument();
            settingsXml.Load("Settings.xml");

            var settings = new Settings(settingsXml);

            var assetManager = AssetManagerFactory.GetInstance(
                AssetManagerType.Perforce,
                settings.P4Host,
                settings.P4Port,
                settings.P4Workspace,
                settings.P4User,
                settings.P4Password,
                settings.P4DepotRoot);

            var changeList = Utils.RetrieveChangeList("4206849");

            var report = new WaveslotReport(eventLog, settings, assetManager);
            report.Run("C:\\temp\\4206849", changeList, mailAddresses);
        }
    }
}
