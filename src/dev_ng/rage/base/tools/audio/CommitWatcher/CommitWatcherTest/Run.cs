﻿// -----------------------------------------------------------------------
// <copyright file="Run.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

using CommitWatcher.Reports;
namespace CommitWatcherTest
{
    /// <summary>
    /// Main entry point.
    /// </summary>
    public class Run
    {
        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            DialogueDeliveredReportTest.Run();
        }
    }
}
