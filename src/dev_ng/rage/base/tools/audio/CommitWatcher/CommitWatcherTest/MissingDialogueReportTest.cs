﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CommitWatcherTest
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Net.Mail;
    using System.Xml;
    using CommitWatcher;
    using CommitWatcher.Reports;
    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// The program.
    /// </summary>
    public class MissingDialogueReportTest
    {
        /// <summary>
        /// Run test.
        /// </summary>
        public static void Run()
        {
            MailAddress mailAddress = new MailAddress("derek@rockstarnorth.com");
            List<MailAddress> mailAddresses = new List<MailAddress> { mailAddress };

            EventLog eventLog = new EventLog("Application", Environment.MachineName, "Commit Watcher");
            XmlDocument settingsXml = new XmlDocument();
            settingsXml.Load("Settings.xml");

            Settings settings = new Settings(settingsXml);

            IAssetManager assetManager = AssetManagerFactory.GetInstance(
                AssetManagerType.Perforce,
                settings.P4Host,
                settings.P4Port,
                settings.P4Workspace,
                settings.P4User,
                settings.P4Password,
                settings.P4DepotRoot);
   
            String[] platforms = {  "xbox360", "ps3" };
            String builtWavePath = settings.BuiltWavePath;
            ICollection<String> packListDepotPaths = new List<String>();
            foreach (String platform in platforms)
            {
                packListDepotPaths.Add(Path.Combine(builtWavePath.Replace("{platform}",platform), "builtWaves_PACK_LIST.xml"));
            }

            foreach (String packListDepotPath in packListDepotPaths)
            {
                MissingDialogueReport report = new MissingDialogueReport(eventLog, settings, assetManager);
                report.Run(packListDepotPath, mailAddresses);
            }
        }
    }
}
