classdef PitchTracker < handle

properties (SetAccess = 'private', GetAccess='private')
    corrPanelHandle;
    pitchPanelHandle;
    editPanelHandle;
    corrAxesHandle;
    pitchAxesHandle;
    comboXcorrMethodHandle;
    comboStepByHandle;
    textboxStepsizeHandle;
    textboxFramesizeHandle;
    checkboxShowProcessing;
    buttonXcorrHandle;
    buttonSaveResultsHandle;
    buttonZeroCrossCorrHandle;
    textboxUpperboundHandle;
    textboxLowerboundHandle;
    
    engineSignal;
    Fs;
    engineStartIndex;
    startFreq;
    
    xCorrMethod = 4;
    upperBound = 3;
    lowerBound = 3;
    displayProcessing = 0;
    method = 1;
    stepBy = 0;
    stepSize = 480;
    FrameSizeMs = 80;
    
    PitchMap;
    PitchMapOffsets;
    PitchMapValues;
    
    zcCrossCorrHandle;
end
  
  methods
    function this = PitchTracker()
        format long g;
        
        % Create figure
        this.FigureHandle = figure( ...
                    'Menubar','none', ...
                    'Toolbar','none', ...
                    'Position', [535,210,850,500], ...
                    'IntegerHandle', 'off', ...
                    'NumberTitle', 'off', ...
                    'Visible', 'off', ...
                    'Name', 'GEARS: Pitch Tracker',...
                    'CloseRequestFcn', @(hObj, evd) figureCloseCallback(this));

warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
jframe=get(this.FigureHandle,'javaframe');
jIcon=javax.swing.ImageIcon('icons\icon.gif');
jframe.setFigureIcon(jIcon);

        this.corrPanelHandle = uipanel('Parent',this.FigureHandle,'Title','Pitch Tracking',...
              'Position',[.01 .52 .75 .48]);
        this.pitchPanelHandle = uipanel('Parent',this.FigureHandle,'Title','Pitch Map',...
              'Position',[.01 .02 .75 .48]);
        this.editPanelHandle = uipanel('Parent',this.FigureHandle,'Title','Options',...
              'Position',[.77 .02 .22 .98]);
        this.corrAxesHandle = axes('DataAspectRatio',[1 1 1],...
            'Position',[.1  .1  .8  .8], ...
            'XLimMode','manual','YLimMode','manual',...
            'DrawMode','fast',...
            'Parent',this.corrPanelHandle);
        this.pitchAxesHandle = axes('DataAspectRatio',[1 1 1],...
            'Position',[.1  .1  .8  .8], ...
            'XLimMode','manual','YLimMode','manual',...
            'DrawMode','fast',...
            'Parent',this.pitchPanelHandle);
        textLabel = uicontrol('Style','text',...
               'Parent', this.editPanelHandle,...
               'string','Pitch Tracking Method:',...
               'Units','normalized',...
               'position',[0.11 0.93 0.7 0.05]);
        this.comboXcorrMethodHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.11 0.84 0.7 0.1],...
                     'HandleVisibility','callback', ...
                     'String',{'ACF','AMDF','Residual','xCorr'},...
                     'Style','popupmenu',...  
                     'Value', this.xCorrMethod,...
                     'Callback',@(src,event)xCorrMethodChange_cb(this));
        textLabel2 = uicontrol('Style','text',... 
               'Parent', this.editPanelHandle,...
               'string','Step Method:',...
               'Units','normalized',...
               'position',[0.11 0.82 0.7 0.05]);
        this.comboStepByHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.11 0.73 0.7 0.1],...
                     'HandleVisibility','callback', ...
                     'String',{'Step by value','Step by pitch'},...
                     'Value', this.stepBy,...
                     'Style','popupmenu',...   
                     'Callback',@(src,event)stepByChange_cb(this));
        textLabel3 = uicontrol('Style','text',...
               'Parent', this.editPanelHandle,...
               'string','Step Size (Samples):',...
               'Units','normalized',...
               'position',[0.05 0.65 0.5 0.1]);
        this.textboxStepsizeHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.50 0.68 0.3 0.05],...
                     'HandleVisibility','callback', ...
                     'String',this.stepSize,...
                     'Style','edit',...   
                     'Callback',@(src,event)stepSizeChange_cb(this));
        textLabel4 = uicontrol('Style','text',...
               'Parent', this.editPanelHandle,...
               'string','Frame Size (Ms):',...
               'Units','normalized',...
               'position',[0.1 0.55 0.4 0.1]); 
        this.textboxFramesizeHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.50 0.58 0.3 0.05],...
                     'HandleVisibility','callback', ...
                     'String',this.FrameSizeMs,...
                     'Style','edit',...   
                     'Callback',@(src,event)frameSizeChange_cb(this));
        textLabel5 = uicontrol('Style','text',...
               'Parent', this.editPanelHandle,...
               'string','UpperBound +(Hz):',...
               'Units','normalized',...
               'position',[0.05 0.45 0.5 0.1]);
        this.textboxUpperboundHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.50 0.48 0.3 0.05],...
                     'HandleVisibility','callback', ...
                     'String',this.upperBound,...
                     'Style','edit',...   
                     'Callback',@(src,event)upperBoundChange_cb(this));
        textLabel6 = uicontrol('Style','text',...
               'Parent', this.editPanelHandle,...
               'string','LowerBound -(Hz):',...
               'Units','normalized',...
               'position',[0.1 0.35 0.4 0.1]); 
        this.textboxLowerboundHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.50 0.38 0.3 0.05],...
                     'HandleVisibility','callback', ...
                     'String',this.lowerBound,...
                     'Style','edit',...   
                     'Callback',@(src,event)lowerBoundChange_cb(this));
        this.checkboxShowProcessing = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.1 0.29 0.8 0.07],...
                     'HandleVisibility','callback', ...
                     'String','Display Processing',...
                     'Style','checkbox',...  
                     'Callback',@(src,event)DisplayProcessing_cb(this));   
        this.buttonXcorrHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.1 0.20 0.8 0.07],...
                     'HandleVisibility','callback', ...
                     'String','Calculate Pitch',...
                     'Style','pushbutton',...  
                     'Callback',@(src,event)RecalculatePitch_cb(this));         
        this.buttonSaveResultsHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.1 0.11 0.8 0.07],...
                     'HandleVisibility','callback', ...
                     'String','Save Results',...
                     'Style','pushbutton',...   
                     'Callback',@(src,event)SaveResults_cb(this));
        this.buttonZeroCrossCorrHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.1 0.02 0.8 0.07],...
                     'HandleVisibility','callback', ...
                     'String','Calculate Grain Boundaries',...
                     'Style','pushbutton',...   
                     'Callback',@(src,event)ZeroCrossCorr_cb(this));
                 
    	this.zcCrossCorrHandle = zcCrossCorr;
    end
    
    function xCorrMethodChange_cb(this)
        this.xCorrMethod = get(this.comboXcorrMethodHandle,'Value');
    end
    
    function stepByChange_cb(this)
        this.stepBy = get(this.comboStepByHandle,'Value');
    end
    
    function stepSizeChange_cb(this)
        this.stepSize = str2num(get(this.textboxStepsizeHandle,'String'));
    end
    
    function frameSizeChange_cb(this)
        this.FrameSizeMs = str2num(get(this.textboxFramesizeHandle,'String'));
    end
    
    function upperBoundChange_cb(this)
        this.upperBound = str2num(get(this.textboxUpperboundHandle,'String'));
    end
    
    function lowerBoundChange_cb(this)
        this.lowerBound = str2num(get(this.textboxLowerboundHandle,'String'));
    end
    
    function DisplayProcessing_cb(this) 
        this.displayProcessing = ~this.displayProcessing;
    end
    
    function RecalculatePitch_cb(this)
        figure(this.FigureHandle);
        set(this.FigureHandle, 'HandleVisibility', 'on');
        
        set(this.buttonXcorrHandle,'Enable','off');
        set(this.buttonSaveResultsHandle,'Enable','off');
        set(this.buttonZeroCrossCorrHandle,'Enable','off');
        pause(0.1);
        switch this.xCorrMethod 
        case 1
            trackPitchACF(this, this.engineSignal, this.Fs, this.engineStartIndex, this.startFreq);
        case 2
            trackPitchAMDF(this, this.engineSignal, this.Fs, this.engineStartIndex, this.startFreq);
        case 3
            trackPitchResidual(this, this.engineSignal, this.Fs, this.engineStartIndex, this.startFreq);
        case 4
            trackPitchXcorr(this, this.engineSignal, this.Fs, this.engineStartIndex, this.startFreq);
        otherwise
        end
        set(this.buttonXcorrHandle, 'Enable', 'on');
        set(this.buttonSaveResultsHandle,'Enable','on');
        set(this.buttonZeroCrossCorrHandle,'Enable','on');
    end
    
    function SaveResults_cb(this)
        this.PitchMap = [];
        this.PitchMap = [this.PitchMapOffsets' this.PitchMapValues'];
        
        [file,path] = uiputfile('*.xls','Save PitchMap As');
        xlswrite([path file], this.PitchMap, 'Pitchdata');
    end
    
    function ZeroCrossCorr_cb(this)
        this.PitchMap = [];
        this.PitchMap = [this.PitchMapOffsets' this.PitchMapValues'];
        if isvalid(this.zcCrossCorrHandle)
        else
            this.zcCrossCorrHandle = zcCrossCorr;
        end
        this.zcCrossCorrHandle.DrawPitchMap(this.engineSignal, this.Fs, this.PitchMap, this.PitchMapOffsets, this.PitchMapValues)
    end
    
    function trackPitch(this, engineSignal, Fs, engineStartIndex, startFreq)
        figure(this.FigureHandle);
        set(this.FigureHandle, 'HandleVisibility', 'on');

        this.engineSignal = engineSignal;
        this.Fs = Fs;
        this.engineStartIndex = engineStartIndex;
        this.startFreq = startFreq;
        
        set(this.buttonXcorrHandle, 'Enable', 'off');
        set(this.buttonSaveResultsHandle,'Enable','off');
        set(this.buttonZeroCrossCorrHandle,'Enable','off');
        pause(0.1);
        switch this.xCorrMethod 
        case 1
            trackPitchACF(this, engineSignal, Fs, engineStartIndex, startFreq);
        case 2
            trackPitchAMDF(this, engineSignal, Fs, engineStartIndex, startFreq);
        case 3
            trackPitchResidual(this, engineSignal, Fs, engineStartIndex, startFreq);
        case 4
            trackPitchXcorr(this, engineSignal, Fs, engineStartIndex, startFreq);
        otherwise
        end
        set(this.buttonXcorrHandle, 'Enable', 'on');
        set(this.buttonSaveResultsHandle,'Enable','on');
        set(this.buttonZeroCrossCorrHandle,'Enable','on');
    end
    
    function trackPitchACF(this, engineSignal, Fs, engineStartIndex, startFreq)
        this.PitchMapOffsets = [];
        this.PitchMapValues = [];
        
        frameSize=Fs*(this.FrameSizeMs/1000); %80 millisecond window;
        startIndex = engineStartIndex;

        endIndex = startIndex + frameSize;
        startIndex2 = 0;

        hannWindow = hann(frameSize + 1);

        maxShift = round(Fs/startFreq)*2;
        maxFreq = round(Fs/(startFreq + this.upperBound));
        minFreq = round(Fs/(startFreq - this.lowerBound));

        pitchmapACF1 = [];
        pitchmapACF2 = [];

        while (startIndex) >= 1

            data = engineSignal(startIndex:endIndex);
            data = data.*hannWindow;

            frame = data;

            [acfFreq1, acfFreq2, acfOffset1, acfOffset2] = pitchestimateACF(frame, Fs, maxShift, minFreq, maxFreq, this.method, this.displayProcessing, this.corrAxesHandle);
            pitchmapACF1 = [round2(acfFreq1, 0.01) pitchmapACF1];
            pitchmapACF2 = [round2(acfFreq2, 0.01) pitchmapACF2];

            if startIndex2 == 0
                startIndex2 = startIndex;
            else
                %fprintf('%d\n', startIndex);
                %fprintf('%d, %f\n', startIndex, round2(acfFreq1, 0.01));
                this.PitchMapOffsets = [startIndex this.PitchMapOffsets];
                this.PitchMapValues = [round2(acfFreq1, 0.01) this.PitchMapValues];
            end

            if this.stepBy == 0
                startIndex = startIndex - this.stepSize;
            else
                startIndex = startIndex - acfOffset1;
            end
            endIndex = startIndex + frameSize;
            maxShift = round(Fs/round2(acfFreq1, 0.1))*4;
            maxFreq = round(Fs/round(acfFreq1 + this.upperBound));
            minFreq = round(Fs/round(acfFreq1 - this.lowerBound));

        end

        %startIndex = startIndex2;
        startIndex = engineStartIndex;
        endIndex = startIndex + frameSize;
        maxShift = round(Fs/startFreq)*2;
        maxFreq = round(Fs/(startFreq + this.upperBound));
        minFreq = round(Fs/(startFreq - this.lowerBound));

        while (endIndex) < length(engineSignal)

            data = engineSignal(startIndex:endIndex);
            data = data.*hannWindow;

            frame = data;

            [acfFreq1, acfFreq2, acfOffset1, acfOffset2] = pitchestimateACF(frame, Fs, maxShift, minFreq, maxFreq, this.method, this.displayProcessing, this.corrAxesHandle);
            pitchmapACF1 = [pitchmapACF1 round2(acfFreq1, 0.01)];
            pitchmapACF2 = [pitchmapACF2 round2(acfFreq2, 0.01)];

            %fprintf('%d\n', startIndex);
            %fprintf('%d, %f\n', startIndex, round2(acfFreq1, 0.01));

            this.PitchMapOffsets = [this.PitchMapOffsets startIndex];
            this.PitchMapValues = [this.PitchMapValues round2(acfFreq1, 0.01)];
            
            if this.stepBy == 0
                startIndex = startIndex + this.stepSize;
            else
                startIndex = startIndex + acfOffset1;
            end
            endIndex = startIndex + frameSize;
            maxShift = round(Fs/round2(acfFreq1, 0.1))*4;
            maxFreq = round(Fs/round(acfFreq1 + this.upperBound));
            minFreq = round(Fs/round(acfFreq1 - this.lowerBound));
        end

        % plot pitchmapACF trace
        %t=(0:length(pitchmapACF1)-1) * 0.01;
        % plot pitchmapACF trace
        t=(0: round(length(engineSignal)/Fs)/length(pitchmapACF1) :round(length(engineSignal)/Fs));
        if(length(t) ~= length(pitchmapACF1))
            if(length(t) > length(pitchmapACF1))
                pitchmapACF1 = [pitchmapACF1 zeros(1, length(t)-length(pitchmapACF1))];
            end
            if(length(t) < length(pitchmapACF1))
                pitchmapACF1(end-(length(pitchmapACF1)-length(t)):end) = [];
            end
        end
        plot(this.pitchAxesHandle, t, pitchmapACF1);
    end
    
    function trackPitchAMDF(this, engineSignal, Fs, engineStartIndex, startFreq)
        this.PitchMapOffsets = [];
        this.PitchMapValues = [];
        
        frameSize=Fs*(this.FrameSizeMs/1000); %80 millisecond window;
        startIndex = engineStartIndex;

        endIndex = startIndex + frameSize;
        startIndex2 = 0;

        hannWindow = hann(frameSize + 1);

        maxShift = round(Fs/startFreq)*2;
        maxFreq = round(Fs/(startFreq + this.upperBound));
        minFreq = round(Fs/(startFreq - this.lowerBound));

        pitchmapAMDF = [];

        while (startIndex) >= 1

            data = engineSignal(startIndex:endIndex);
            data = data.*hannWindow;

            frame = data;

            [amdfFreq, amdfOffset] = pitchestimateAMDF(frame, Fs, maxShift, minFreq, maxFreq, this.method, this.displayProcessing, this.corrAxesHandle);
            pitchmapAMDF = [round2(amdfFreq, 0.01) pitchmapAMDF];

            if startIndex2 == 0
                startIndex2 = startIndex;
            else
                %fprintf('%d\n', startIndex);
                %fprintf('%d, %f\n', startIndex, round2(amdfFreq, 0.01));
                this.PitchMapOffsets = [startIndex this.PitchMapOffsets];
                this.PitchMapValues = [round2(amdfFreq, 0.01) this.PitchMapValues];
            end

            if this.stepBy == 0
                startIndex = startIndex - this.stepSize;
            else
                startIndex = startIndex - amdfOffset;
            end
            endIndex = startIndex + frameSize;
            maxShift = round(Fs/round2(amdfFreq, 0.1))*4;
            maxFreq = round(Fs/round(amdfFreq + this.upperBound));
            minFreq = round(Fs/round(amdfFreq - this.lowerBound));

        end

        %startIndex = startIndex2;
        startIndex = engineStartIndex;
        endIndex = startIndex + frameSize;
        maxShift = round(Fs/startFreq)*2;
        maxFreq = round(Fs/(startFreq + this.upperBound));
        minFreq = round(Fs/(startFreq - this.lowerBound));

        while (endIndex) < length(engineSignal)

            data = engineSignal(startIndex:endIndex);
            data = data.*hannWindow;

            frame = data;

            [amdfFreq, amdfOffset] = pitchestimateAMDF(frame, Fs, maxShift, minFreq, maxFreq, this.method, this.displayProcessing, this.corrAxesHandle);
            pitchmapAMDF = [pitchmapAMDF round2(amdfFreq, 0.01)];

            %fprintf('%d\n', startIndex);
            %fprintf('%d, %f\n', startIndex, round2(amdfFreq, 0.01));
            
            this.PitchMapOffsets = [this.PitchMapOffsets startIndex];
            this.PitchMapValues = [this.PitchMapValues round2(amdfFreq, 0.01)];

            if this.stepBy == 0
                startIndex = startIndex + this.stepSize;
            else
                startIndex = startIndex + amdfOffset;
            end
            endIndex = startIndex + frameSize;
            maxShift = round(Fs/round2(amdfFreq, 0.1))*4;
            maxFreq = round(Fs/round(amdfFreq + this.upperBound));
            minFreq = round(Fs/round(amdfFreq - this.lowerBound));
        end

        % plot pitchmapACF trace
        %t=(0:length(pitchmapAMDF)-1) * 0.01;
        t=(0: round(length(engineSignal)/Fs)/length(pitchmapAMDF) :round(length(engineSignal)/Fs));
        if(length(t) ~= length(pitchmapAMDF))
            if(length(t) > length(pitchmapAMDF))
                pitchmapAMDF = [pitchmapAMDF zeros(1, length(t)-length(pitchmapAMDF))];
            end
            if(length(t) < length(pitchmapAMDF))
                pitchmapAMDF(end-(length(pitchmapAMDF)-length(t)):end) = [];
            end
        end
        plot(this.pitchAxesHandle, t, pitchmapAMDF);
    end
    
    function trackPitchXcorr(this, engineSignal, Fs, engineStartIndex, startFreq)
        this.PitchMapOffsets = [];
        this.PitchMapValues = [];
        
        frameSize=Fs*(this.FrameSizeMs/1000); %80 millisecond window;
        startIndex = engineStartIndex;

        endIndex = startIndex + frameSize;
        startIndex2 = 0;

        hannWindow = hann(frameSize + 1);

        maxShift = round(Fs/startFreq)*2;
        maxFreq = round(Fs/(startFreq + this.upperBound));
        minFreq = round(Fs/(startFreq - this.lowerBound));

        pitchmapXcorr = [];

        while (startIndex) >= 1

            data = engineSignal(startIndex:endIndex);
            data = data.*hannWindow;

            frame = data;

            [xcorrFreq, xcorrOffset] = pitchestimateXcorr(frame, Fs, maxShift, minFreq, maxFreq, this.displayProcessing, this.corrAxesHandle);
            pitchmapXcorr = [round2(xcorrFreq, 0.01) pitchmapXcorr];

            if startIndex2 == 0
                startIndex2 = startIndex;
            else
                %fprintf('%d\n', startIndex);
                %fprintf('%d, %f\n', startIndex, round2(xcorrFreq, 0.01));
                this.PitchMapOffsets = [startIndex this.PitchMapOffsets];
                this.PitchMapValues = [round2(xcorrFreq, 0.01) this.PitchMapValues];
            end

            if this.stepBy == 0
                startIndex = startIndex - this.stepSize;
            else
                startIndex = startIndex - xcorrOffset;
            end
            endIndex = startIndex + frameSize;
            maxShift = round(Fs/round2(xcorrFreq, 0.1))*4;
            maxFreq = round(Fs/round(xcorrFreq + this.upperBound));
            minFreq = round(Fs/round(xcorrFreq - this.lowerBound));

        end

        %startIndex = startIndex2;
        startIndex = engineStartIndex;
        endIndex = startIndex + frameSize;
        maxShift = round(Fs/startFreq)*2;
        maxFreq = round(Fs/(startFreq + this.upperBound));
        minFreq = round(Fs/(startFreq - this.lowerBound));

        while (endIndex) < length(engineSignal)

            data = engineSignal(startIndex:endIndex);
            data = data.*hannWindow;

            frame = data;

            [xcorrFreq, xcorrOffset] = pitchestimateXcorr(frame, Fs, maxShift, minFreq, maxFreq, this.displayProcessing, this.corrAxesHandle);
            pitchmapXcorr = [pitchmapXcorr round2(xcorrFreq, 0.01)];

            %fprintf('%d\n', startIndex);
            %fprintf('%d, %f\n', startIndex, round2(xcorrFreq, 0.01));
            
            this.PitchMapOffsets = [this.PitchMapOffsets startIndex];
            this.PitchMapValues = [this.PitchMapValues round2(xcorrFreq, 0.01)];

            if this.stepBy == 0
                startIndex = startIndex + this.stepSize;
            else
                startIndex = startIndex + xcorrOffset;
            end
            endIndex = startIndex + frameSize;
            maxShift = round(Fs/round2(xcorrFreq, 0.1))*4;
            maxFreq = round(Fs/round(xcorrFreq + this.upperBound));
            minFreq = round(Fs/round(xcorrFreq - this.lowerBound));
        end

        % plot pitchmapACF trace
        %t=(0:length(pitchmapXcorr)-1) * 0.01;
        t=(0: round(length(engineSignal)/Fs)/length(pitchmapXcorr) :round(length(engineSignal)/Fs));
        if(length(t) ~= length(pitchmapXcorr))
            if(length(t) > length(pitchmapXcorr))
                pitchmapXcorr = [pitchmapXcorr zeros(1, length(t)-length(pitchmapXcorr))];
            end
            if(length(t) < length(pitchmapXcorr))
                pitchmapXcorr(end-(length(pitchmapXcorr)-length(t)):end) = [];
            end
        end
        plot(this.pitchAxesHandle, t, pitchmapXcorr);
    end
    
    function trackPitchResidual(this, engineSignal, Fs, engineStartIndex, startFreq)
        this.PitchMapOffsets = [];
        this.PitchMapValues = [];
        
        frameSize=Fs*(this.FrameSizeMs/1000); %80 millisecond window;
        startIndex = engineStartIndex;

        endIndex = startIndex + frameSize;
        startIndex2 = 0;

        hannWindow = hann(frameSize + 1);

        param.minpitch           = startFreq - this.lowerBound;
        param.maxpitch           = startFreq + this.upperBound;

        pitchmap = [];

        while (startIndex) >= 1

            frame = engineSignal(startIndex:endIndex);

            if startIndex > 87360
                [t,pitch,F,R,path] = pitchestimateResidual(frame, Fs, param);

                tempPitch = pitch(1);
                pitchmap = [round2(tempPitch, 0.1) pitchmap];

                if startIndex2 == 0
                    startIndex2 = startIndex;
                else
                    %fprintf('%d, %f\n', startIndex, round2(tempPitch, 0.01));
                    %fprintf('%d\n', startIndex);
                    this.PitchMapOffsets = [startIndex this.PitchMapOffsets];
                    this.PitchMapValues = [round2(tempPitch, 0.01) this.PitchMapValues];
                end

                offset = floor(Fs/tempPitch);
            else
                pitchmap = [0 pitchmap];
            end  

            if this.stepBy == 0
                startIndex = startIndex - this.stepSize;
            else
                startIndex = startIndex - offset;
            end
            endIndex = startIndex + frameSize;
            param.maxpitch = round(tempPitch + this.upperBound);
            param.minpitch = round(tempPitch - this.lowerBound);

        end

        %startIndex = startIndex2;
        startIndex = engineStartIndex;
        endIndex = startIndex + frameSize;
        param.minpitch           = startFreq - this.lowerBound;
        param.maxpitch           = startFreq + this.upperBound;

        while (endIndex) < length(engineSignal)

            frame = engineSignal(startIndex:endIndex);

            [t,pitch,F,R,path] = pitchestimateResidual(frame, Fs, param);
    
            tempPitch = pitch(1);
            pitchmap = [pitchmap round2(tempPitch, 0.1)];

            offset = floor(Fs/tempPitch);

            %fprintf('%d, %f \n', startIndex, round2(tempPitch, 0.1));
            %fprintf('%d \n', startIndex);
            
            this.PitchMapOffsets = [this.PitchMapOffsets startIndex];
            this.PitchMapValues = [this.PitchMapValues round2(tempPitch, 0.01)];

            if this.stepBy == 0
                startIndex = startIndex + this.stepSize;
            else
                startIndex = startIndex + offset;
            end
            endIndex = startIndex + frameSize;
            param.maxpitch = round(tempPitch + this.upperBound);
            param.minpitch = round(tempPitch - this.lowerBound);
        end

        % plot pitchmapACF trace
        %t=(0:length(pitchmap)-1) * 0.01;
        t=(0: round(length(engineSignal)/Fs)/length(pitchmap) :round(length(engineSignal)/Fs));
        if(length(t) ~= length(pitchmap))
            if(length(t) > length(pitchmap))
                pitchmap = [pitchmap zeros(1, length(t)-length(pitchmap))];
            end
            if(length(t) < length(pitchmap))
                pitchmap(end-(length(pitchmap)-length(t)):end) = [];
            end
        end
        plot(this.pitchAxesHandle, t, pitchmap);
    end
    
    function figureCloseCallback(this)
        delete(this.FigureHandle);
        delete(this);
    end

    function delete(this)
        if ishandle(this.FigureHandle)
            delete(this.FigureHandle);
        end
    end
  end

  methods (Static)
    function name = getName()
        name = 'GEARS Pitch Tracker';
    end
    % This can be called for any change in cursor position
    function isd = isDynamic()
        isd = false;
    end
  end

  properties
      FigureHandle
  end

end
