classdef AboutGEARS < handle

properties (SetAccess = 'private', GetAccess='private')
    AxesHandle;
end
  
  methods
    function this = AboutGEARS()
        % Create figure
        this.FigureHandle = figure( ...
                    'Menubar','none', ...
                    'Toolbar','none', ...
                    'Position', [700,350,500,500], ...
                    'IntegerHandle', 'off', ...
                    'NumberTitle', 'off', ...
                    'Visible', 'on', ...
                    'Name', 'About GEARS',...
                    'CloseRequestFcn', @(hObj, evd) figureCloseCallback(this));

movegui(this.FigureHandle,'center') ;
                
warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
jframe=get(this.FigureHandle,'javaframe');
jIcon=javax.swing.ImageIcon('icons\icon.gif');
jframe.setFigureIcon(jIcon);

        this.AxesHandle = axes('DataAspectRatio',[1 1 1],...
            'Position',[0  0  1  1], ...
            'XLimMode','manual','YLimMode','manual',...
            'DrawMode','fast',...
            'Parent',this.FigureHandle);
          
%load the background image into Matlab
%if image is not in the same directory as the GUI files, you must use the 
%full path name of the iamge file
backgroundImage = importdata('icons\Render_RlondonBed03.jpg');
%select the axes
axes(this.AxesHandle);
%place image onto the axes
image(backgroundImage);
%remove the axis tick marks
axis off

textLabel = uicontrol('Style','text',...
               'Parent', this.FigureHandle,...
               'string','Granular Engine Analysis Research System (GEARS) is an analysis system for tracking the pitch of engine recordings for the purpose of slicing the recording up into grains that match the pitch. GEARS was developed by John Murray and Lewis Griffin at Rockstar Studios London.',...
               'Units','normalized',...
               'position',[0.1,0.8,0.8,0.15]);

%parentColor = get(get(this.AxesHandle, 'parent'), 'color');
set(textLabel,'foregroundcolor', [0 0 0], ...
      'backgroundcolor', [1 1 1]);
    end
    
    function figureCloseCallback(this)
        delete(this.FigureHandle);
        delete(this);
    end

    function delete(this)
        if ishandle(this.FigureHandle)
            delete(this.FigureHandle);
        end
    end
  end

  methods (Static)
    function name = getName()
        name = 'About GEARS';
    end
    % This can be called for any change in cursor position
    function isd = isDynamic()
        isd = false;
    end
  end

  properties
      FigureHandle
  end

end
