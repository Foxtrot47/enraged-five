function [xcorrPitch, xcorrOffset] = pitchestimateXcorr(frame, Fs, maxShift, minFreq, maxFreq, plotOpt, axesHandle)

if nargin < 6, plotOpt = 0; end

    % calculate autocorrelation? Cross-Correlation
    [r, lags] = xcorr(frame, maxShift, 'coeff');

    r = r(maxShift + 1:2 * maxShift + 1);

    [maxValueXcorr, maxIndexXcorr] = max(r(maxFreq:minFreq));
    xcorrOffset = maxFreq + maxIndexXcorr - 1;
    xcorrPitch = Fs / xcorrOffset;
    
    if plotOpt
        %set(axesHandle,'NextPlot','add')
        plot(axesHandle, r);
        set(axesHandle,'NextPlot','add')
        hold on
        stem(axesHandle, maxIndexXcorr + maxFreq, maxValueXcorr, 'ro');
        hold off
        set(axesHandle,'NextPlot','replace')
        pause(0.01);
    end
return