function [amdfPitch, amdfOffset] = pitchestimateAMDF(frame, Fs, maxShift, minFreq, maxFreq, method, plotOpt, axesHandle)

if nargin < 6, method = 1; end
if nargin < 7, plotOpt = 0; end
    
    % calculate AMDF
    amdf = frame2amdf(frame, maxShift, method, 0);
    
    [maxValueAMDF, maxIndexAMDF] = min(amdf(maxFreq:minFreq));
    
    amdfOffset = maxIndexAMDF + maxFreq - 1;
    amdfPitch = Fs / amdfOffset;
    
    if plotOpt
        %subplot(2,1,1);
        %plot(axesHandle, frame);
        %set(gca, 'xlim', [-inf inf]);
        %subplot(2,1,2);
        plot(axesHandle, amdf);
        set(axesHandle,'NextPlot','add')
        hold on
        stem(axesHandle, maxIndexAMDF + maxFreq, maxValueAMDF, 'ro');
        hold off
        set(axesHandle,'NextPlot','replace')
        pause(0.01);
    end
return