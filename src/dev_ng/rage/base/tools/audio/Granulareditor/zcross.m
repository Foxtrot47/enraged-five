function [I] = zcross(data, range_limit)
if (nargin<2)
    range_limit = 1;
end
ii=0;  II=1;  I(1) = 0;
for ix=2:length(data),
  if (sign(data(ix))~=sign(data(ix-1)) & sign(data(ix))~=0),
    if (ix-II>=range_limit | II<range_limit), ii=ii+1;
      %if (exact)
      %I(ii)=data(ix-1)/(data(ix-1)-data(ix))+ix-1;
      %else
      II=ix-1; [dummy inx]=min(abs(data(II:ix))); I(ii)=II+inx-1; 
      %end;
    end;
  end;
end;

