function [t,pitch,F,R,path] = pitchestimateResidual(x,fs,param)
% PITCHESTIMATE High-accuracy pitch estimation
%   [T,P] = PITCHESTIMATE(X,Fs) computes a pitch estimate P at times T
%   for the signal X sampled at Fs Hz using default parameters.
%
%   [T,P] = PITCHESTIMATE(X,Fs,PARAM) uses the parameter values specified
%   in the parameter structure PARAM, which must have the following fields
%   (units in parentheses, default values in brackets)
%
%      minpitch           minimum detected pitch (Hz) [75]
%      maxpitch           maximum detected pitch (Hz) [400]
%      timestep           window hop (s) [0.01]
%      voicingthreshold   signal level required for voicing detection [0.4]
%      silencethreshold   signal level required for silence detection [0.05]
%      maxcandidates      number of pitch candidates to consider per frame [4]
%      octavecost         penalty for pitch halving [0.04]
%      voicedunvoicedcost penalty for switching voiced/unvoiced between frames [0.2]
%      octavejumpcost     penalty for switching octaves between frames [0.2]
%
%   The pitch estimation algorithm is based on the Praat pitch estimator described
%   in [1].
%
%   References:
%   [1]  P. Boersma, "Accurate Short-Term Analysis of the Fundamental Frequency and
%        the Harmonics-to-Noise Ratio of Sampled Sounds", IFA Proceedings 17, 1993
%

% check number of input arguments
error(nargchk(2,3,nargin));

% setup default parameter structure if none specified
if nargin<3,
  % define default algorithm parameters, see [1] for details
  param.minpitch           = 10;   % Hz
  param.maxpitch           = 30;   % Hz
  param.timestep           = 0.01; % seconds
  param.voicingthreshold   = 0.4;
  param.silencethreshold   = 0.05;
  param.maxcandidates      = 4;
  param.octavecost         = 0.04;
  param.voicedunvoicedcost = 0.2;
  param.octavejumpcost     = 0.2;
else
  param.timestep           = 0.01; % seconds
  param.voicingthreshold   = 0.4;
  param.silencethreshold   = 0.05;
  param.maxcandidates      = 4;
  param.octavecost         = 0.04;
  param.voicedunvoicedcost = 0.2;
  param.octavejumpcost     = 0.2;
end;

% compute size, skip, overlap and nfft in samples
%winsize = round(fs/(param.minpitch/3)); % use 3 for pitch detection, use 6 for HNR
winsize = length(x);
winnfft = 2^nextpow2(winsize * 1.5);

% define window
win = hanning(winsize);

% define vector to demean local segments
demean = ones(winnfft,1); demean(1)=0; 

% compute auto-correlation of window, and its reciprocal
rwin = real(ifft(abs(fft(win,winnfft)).^2));
rwinrecip = 1./rwin;

% step 1. determine windowed frames of speech
xbuf = x;
r = diag(sparse(win)) * xbuf;

% step 2. compute auto-correlation of all segments
r = real(ifft((diag(sparse(demean))*abs(fft(r,winnfft,1))).^2,[],1));

% step 3. divide by auto-correlation of the window
r = diag(sparse(rwinrecip)) * r;

% flip matrix top/bottom halves
r = fftshift(r,1);

% define time lag, and time lags within our pitch range
tau = (-winnfft/2:winnfft/2-1)'/fs;
mintau = 1/param.maxpitch;
maxtau = 1/param.minpitch;
goodtau = tau>=mintau & tau<=maxtau;

% preallocate output matrices
F = zeros(param.maxcandidates,size(r,2));
R = F;
t = (0:size(r,2)-1)*param.timestep + (winsize-1)/fs;

% compute unvoiced candidate strength
globalabsmax = max(abs(x));
localabsmax  = max(abs(xbuf));
R(1,:) = param.voicingthreshold + max([zeros(1,size(R,2)); 2-(localabsmax/globalabsmax)/(param.silencethreshold/(1+param.voicingthreshold))]);

% find suitable high-accuracy local maxima in individual frames
for i = 1:size(r,2),
    
    % skip frame if it has 0 energy
    if r(winnfft/2+1,i)==0, continue; end;

    % normalize autocorrelation
    rr = r(:,i)/r(winnfft/2+1,i);
    
    % find local maxima of frame
    localmax = [false; rr(2:end-1)>rr(1:end-2) & rr(2:end-1)>rr(3:end) & goodtau(2:end-1); false];
    idxmax = find(localmax);
    
    % skip frame if no local maxima found
    if isempty(idxmax), continue; end;
    
    rrmax  = rr(localmax);
    taumax = tau(localmax);
    localstrength = rrmax - param.octavecost * log2(param.minpitch * taumax);
    [lsval,lsidx] = sort(-localstrength);
    lsidx = lsidx(1:min(length(lsidx),param.maxcandidates-1));
    
    % interpolate results for higher accuracy
    for j=1:length(lsidx),
        % TODO: the constant "40" is not in accordance with the Praat algorithm, but
        % was easier to implement and seemed to give reasonable accuracy
        [fmax,fval] = fminbnd(@negsincinterp,...
                             tau(idxmax(lsidx(j))-1),...
                             tau(idxmax(lsidx(j))+1),...
                             [],tau(1),1/fs,rr,40);
        F(j+1,i) = 1/fmax;
        R(j+1,i) = -fval - param.octavecost * log2(param.minpitch * fmax);
    end;
end;

% global optimum requested?
if param.voicedunvoicedcost==0 && param.octavejumpcost==0,
    
    % if not, just determine the locally optimal pitch estimate
    path = maxidx(R);
    
else
    
    % find globally optimal path through local pitch estimates
    cost = zeros(size(F));
    prev = cost;
    cost(:,1) = -R(:,1);

    % determine cost of optimal path through local pitch estimates
    for i=2:size(F,2),
        for j=1:size(F,1),
            [cost(j,i),prev(j,i)] = bestcost(cost(:,i-1),F(:,i-1),F(j,i),R(j,i),...
                param.voicedunvoicedcost, param.octavejumpcost);
        end;
    end;
    
    % trace back optimal path
    path = zeros(1,size(F,2));
    path(end) = minidx(cost(:,end));
    for i=size(F,2)-1:-1:1,
        path(i) = prev(path(i+1),i+1);
    end;
end;

pitch = F(path+(0:size(F,2)-1)*param.maxcandidates);
return

%%% end of main function

%%% subfunction %%%%%%%%%%
function v = negsincinterp(tau,t0,T,y,N)
nu = 1 + (tau-t0)/T;                % convert tau to index nu in y
n1 = floor(nu)-(N-1);               % take +/- N samples around nu
n2 = floor(nu)+1+(N-1);
n = (n1:n2)';
x = nu - n;                         % negative distance from nu to sample indices
w = sinc(x);                        % compute sinc weights
h = (1/2) + (1/2) * cos(-pi*x/N);   % compute hanning window weights
r = y(n);                           % get samples
v = -sum(r .* w .* h);              % weigh, sum and negate the samples

%%% subfunction %%%%%%%%%%
function [mincost,minidx] = bestcost(cost,F1,F2,Rnp,vucost,ojcost)
F1zero = (F1==0);
if F2==0,
    localcost = cost + vucost.*(~F1zero) - Rnp;
else
    localcost(F1zero)  = cost(F1zero) + vucost - Rnp;
    localcost(~F1zero) = cost(~F1zero) + ojcost.*abs(log2(F1(~F1zero)/F2)) - Rnp;
end;
[mincost,minidx]=min(localcost);

%%% subfunction %%%%%%%%%%
function idx = maxidx(varargin)
[dummy,idx] = max(varargin{:});

%%% subfunction %%%%%%%%%%
function idx = minidx(varargin)
[dummy,idx] = min(varargin{:});


