function [acfPitch1, acfPitch2, acfOffset1, acfOffset2] = pitchestimateACF(frame, Fs, maxShift, minFreq, maxFreq, method, plotOpt, axesHandle)

if nargin < 6, method = 1; end
if nargin < 7, plotOpt = 0; end

    % calculate ACF
    acf = frame2acf(frame, maxShift, method);
    [maxValueACF1, maxIndexACF1] = max(acf(maxFreq:length(acf)));
    
    acfOffset1 = maxFreq + maxIndexACF1 - 1;
    acfPitch1 = Fs / acfOffset1;
    
    acf2 = acf;
    acf2(1:maxFreq) = min(acf);
    acf2(minFreq:end) = min(acf);
    [maxValueACF2, maxIndexACF2] = max(acf2);
    
    %acfOffset2 = maxIndexACF2 - 1;
    acfOffset2 = maxIndexACF2;
    acfPitch2 = Fs / acfOffset2;
    
    if plotOpt
        %subplot(2,1,1);
        %plot(axesHandle, frame); axis tight; title('Input frame');
        %subplot(2,1,2);
        xVec=1:length(acf);
        plot(axesHandle, xVec, acf, xVec, acf2, acfOffset1, maxValueACF1, 'ro');
        pause(0.01);
    end
return