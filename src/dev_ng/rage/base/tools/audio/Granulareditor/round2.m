function z = round2(x,y)
error(nargchk(2,2,nargin))
error(nargoutchk(0,1,nargout))
if prod(size(y))>1
  error('n must be scalar')
end
z = round(x/y)*y;

