classdef GranularEditor < handle

  properties (SetAccess = 'private', GetAccess='private')
    WaveFigureHandle     % Handle to wave image
    AxesHandles          % Handles to each axes. Each channel is in one axis
    ChannelHandles       % Handles to each channel's line plots
    Filename = '*.wav';  % File name of Wave file being analysed
    AudioData            % Audio data being edited
    iOutliers            % Used for down sampled plot
    jOutliers            % Used for down sampled plot
    Fs = 8000;           % Sample time
    FileReaders = [];    % File reader plugins
    FileFilterSpec = {}; % Spec used to filter files for uigetfile
    SelectorLines        % The lines used to select audio data in the editor
    AudioPlayer          % audioplayer object.
    Analyzer             % Object which manages all analyzers
    Position = 0;
    fftSize = 2048;
    EditTextBoxHandle;
    xd1;
    xd2;
    engineSignalNotSet = 1;
    AboutUs;
  end

  methods
    function this = GranularEditor(varargin)
      addpath(fileparts(mfilename('fullpath')));
      createWaveFigure(this);
    end
  end

  methods (Access = 'private')

    function createWaveFigure(this)
      fPos = [360,500,850,350];
      this.WaveFigureHandle = figure( ...
                    'Visible','off', ...
                    'Menubar','none', ...
                    'Toolbar','figure', ...
                    'Position', fPos, ...
                    'IntegerHandle', 'off', ...
                    'Color',    get(0, 'defaultuicontrolbackgroundcolor'), ...
                    'NumberTitle', 'off', ...
                    'Name', 'GEARS: Audio Editor', ...
                    'WindowButtonUpFcn', @(hobj, evd) figureButtonUpCallback(this), ...
                    'ResizeFcn', @(hObj, evd) resizeFcnCallback(this), ...
                    'CloseRequestFcn', @(hObj, evd) figureCloseCallback(this));
                
warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
jframe=get(this.WaveFigureHandle,'javaframe');
jIcon=javax.swing.ImageIcon('icons\icon.gif');
jframe.setFigureIcon(jIcon);

      hZoom = zoom(this.WaveFigureHandle);
      hPan  = pan(this.WaveFigureHandle);
      zoomFcn = @(hObject, eventdata) zoomCallback(this, hObject, eventdata);
      set(hZoom, 'Motion', 'horizontal', ...
                 'ActionPostCallback', zoomFcn);
      set(hPan , 'ActionPostCallback', zoomFcn);

      adjustFigure(this);
      this.AxesHandles = axes( ...
                  'Units','pixels', ...
                  'Box', 'on', ...
                  'ButtonDownFcn', @(hobj, evd) axesButtonDownCallback(this));
              
        this.EditTextBoxHandle = uicontrol('Style','edit',...
               'Parent',this.WaveFigureHandle,...
               'String',num2str(this.fftSize),...
               'Position',[.22 .7724 74 20],...
               'Callback',@(src,event)editbox_cb(this));
           
        textLabel = uicontrol('Style','text',... % Add labels to sliders 
               'Parent',this.WaveFigureHandle,...
               'string','FFT Samples',... % Text only, so 
               'position',[50 0 70 20]); % no callback needed 
      hold on;

      set(this.WaveFigureHandle, 'Name', ['GEARS: Audio Editor: ' this.Filename]);
      movegui(this.WaveFigureHandle, 'north');
      set(this.WaveFigureHandle,'Visible','on');
      
      figure(this.WaveFigureHandle);
      xlabel('Time (s)')

      loadFileReaders(this);
    end
    
    function editbox_cb(this)
        tempVal = str2num(get(this.EditTextBoxHandle,'String'));
        
        %Take the value of log2(x) and check to see if the result is an
        %integer. We mod the value of log2(x) and check to see if the remainder is
        %greater than 0. We will store this value in the variable y
        y = mod(log2(tempVal),1);
        %Take the result and convert to a text based response using a simple
        %if logic test combined with some concatentation of strings.
        if (y==0)
            this.fftSize = tempVal;
            data = getCurrentData(this, this.xd1(1), this.xd2(1));
            pastdata = getPastData(this, this.xd1(1), this.xd2(1));
            this.Position = this.xd1(1) / this.Fs;
            this.Analyzer.analyze(data, pastdata, this.Fs, this.Position, 'Audio Fourier Analysis');
        else
            set(this.EditTextBoxHandle, 'String', num2str(this.fftSize));
        end
    end
    
    % Scan +filereaders directory for file reading objects
    function loadFileReaders(this)
      this.FileFilterSpec{1, 1} = '*.*';
      this.FileFilterSpec{1, 2} = 'All Files (*.*)';
      thisFileDir = fileparts(mfilename('fullpath'));
      readerPackage = 'filereaders';
      readers = what([thisFileDir '/+' readerPackage]);
      if isempty(readers), return, end
      if isempty(this.FileReaders)
        this.FileReaders = struct('Reader', [], 'Ext', {});
      end
      for i=1:length(readers.m)
        try
          h = eval([readerPackage '.' readers.m{i}(1:end-2)]);
          ext = h.getFileExtensions();
          if ~isempty(ext)
            this.FileReaders(end+1) = struct('Reader', h, 'Ext', {ext});
          end
          spec = '';
          for j=1:length(ext)
            spec = [spec '*.' ext{j} ';'];
          end
          this.FileFilterSpec{end+1, 1} = spec;
          this.FileFilterSpec{end, 2} = ''; % For protection if next call fails
          this.FileFilterSpec{end, 2} = h.getExtDescription();
        catch me
          warning(me.identifier, me.message);
        end
      end
    end

    % Things which need to be emptied when reading a new file
    function clearHandles(this)
      this.AxesHandles = [];
      this.ChannelHandles = [];
      this.SelectorLines = [];
    end

    % updates audio data in the plots. Also called for zoom callbacks.
    function updatePlot(this, range)
      range = floor(range * this.Fs);
      numPoints = 50000;
      len = length(this.ChannelHandles);
      x = (1:size(this.AudioData, 1))';
      id = find(x >= range(1) & x <= range(2));
      if length(id) > numPoints/len
        idx = round(linspace(id(1), id(end), round(numPoints/len)))';
        ol = this.iOutliers > id(1) & this.iOutliers < id(end);
        for i=1:len
          idxi = unique([idx; this.iOutliers(ol & this.jOutliers == i)]);
          set(this.ChannelHandles(i), 'XData', idxi/this.Fs, ...
                                      'YData', this.AudioData(idxi));
        end
      else
        for i=1:len
          set(this.ChannelHandles(i), 'XData', id/this.Fs, ...
                                      'YData', this.AudioData(id));
        end
      end
    end

    % Plots downsampled data. This has much faster response time. This is
    % based on dsplot by Jiro Doke in file exchange.
    % TODO Add data cursor callbacks
    function dsplot(this)
      len = length(this.AudioData);
      x = 1:len;
      % Find outliers
      filterWidth = min([50, ceil(length(x)/10)]); % max window size of 50
      a  = this.AudioData - ...
                filter(ones(filterWidth,1)/filterWidth, 1, this.AudioData);
      [this.iOutliers, this.jOutliers] = ...
             find(abs(a - repmat(mean(a), size(a, 1), 1)) > ...
                    repmat(4 * std(a), size(a, 1), 1));
      updatePlot(this, [0 len/this.Fs]);
    end

    function loadAudioFile(this, filename)
      this.engineSignalNotSet = 1;
      [data, Fs, errFlag] = readAudioFile(this, filename);
      if ~errFlag
        this.Filename = filename;
        plotData(this, data, Fs);
      end
    end

    % Plot audio data.
    function plotData(this, data, Fs)
      this.AudioData = data;
      this.Fs = Fs;
      sz = size(this.AudioData);
      clearHandles(this);
      set(this.WaveFigureHandle, 'HandleVisibility', 'on');
      for i=1:sz(2)
        subplot(sz(2), 1, i),
        this.ChannelHandles(i) = plot(nan);   % Get a handle for plot
        this.AxesHandles(i) = get(this.ChannelHandles(i), 'Parent');
        set(this.AxesHandles(i), ...
              'ButtonDownFcn', @(hobj, evd) axesButtonDownCallback(this), ...
              'Units', 'Pixels');
        set(this.ChannelHandles(i), ...
              'ButtonDownFcn', @(hobj, evd) axesButtonDownCallback(this));
        axis([0 sz(1)/this.Fs -1 1]);
        zoom reset;     % Remember original zoom position
      end
      
      dsplot(this);
      addSelectorTool(this);
      set(this.WaveFigureHandle, 'Name', ['GEARS: Audio Editor: ' this.Filename], ...
                             'HandleVisibility', 'off');
                         
      % If the number of axes changes we need to resize the axes
      resizeFcnCallback(this);
      linkaxes(this.AxesHandles); % link axes for zooming
    end

    % Read audio data from given file.
    function [data, Fs, errFlag] = readAudioFile(this, filename)
      reader = findReader(this, filename);
      if ~isempty(reader)
        try
            setFilename(reader, filename);
            data = getData(reader);
            Fs = getSampleRate(reader);
            errFlag = 0;
        catch ME
            errordlg(ME.message, 'GranularEditor:FileReader');
            data = [];
            Fs = [];
            errFlag = 1;
        end
      else
        data = [];
        Fs = [];
        errFlag = 1;
      end
    end

    % Find the audio file reader which will read the given filename.
    % The search is based on the filename extension.
    function reader = findReader(this, filename)
      reader = [];
      [p,n,ext] = fileparts(filename);
      ext = ext(2:end); % Remove .
      for i=1:length(this.FileReaders)
        if ismember(ext, this.FileReaders(i).Ext)
          reader = this.FileReaders(i).Reader;
          break;
        end
      end
      if isempty(reader)
        errordlg('This file extension is not supported.', ...
                 'GranularEditor:cantFindReader');
      end
    end

    function createToolbar(this)
      audiotb = uitoolbar(this.WaveFigureHandle);
      addPlaybackControls(this, audiotb);

      tth = findall(this.WaveFigureHandle, 'Type', 'uitoolbar');
      ttb = findall(tth, 'Type', 'uipushtool');

      % Set callback for file open button
      fo = findobj(ttb, 'Tag', 'Standard.FileOpen');
      set(fo, 'Separator', 'on', 'ClickedCallback', ...
              @(hobj, evd) fileOpenCallback(this));
      copyobj(fo, audiotb);

      % copy standard buttons to our toolbar
      ttb = findall(tth, 'Type', 'uitoggletool');
      copyobj(findobj(ttb, 'Tag', 'Exploration.ZoomIn'), audiotb);
      copyobj(findobj(ttb, 'Tag', 'Exploration.ZoomOut'), audiotb);
      copyobj(findobj(ttb, 'Tag', 'Exploration.DataCursor'), audiotb);

      % Add zoom to selection button
      [cd,m,alpha] = imread('icons/ae_zoom_select.png');
      cd = double(cd) / 255;
      cd(~alpha) = NaN;
      uipushtool(audiotb, 'CData', cd, ...
           'TooltipString', 'Fit selection to axes', ...
           'HandleVisibility','off',...
           'ClickedCallback', @(hobj, evd) zoomSelectionCallback(this));

      % Add zoom out back to full signal button
      [cd,m,alpha] = imread('icons/ae_zoom_outfull.png');
      cd = double(cd) / 255;
      cd(~alpha) = NaN;
      uipushtool(audiotb, 'CData', cd, ...
           'TooltipString', 'Fit entire signal to axes', ...
           'HandleVisibility','off',...
           'ClickedCallback', @(hobj, evd) zoomOutfullCallback(this));

      % Move data cursor button to the end
      ttc = allchild(audiotb);
      ttc(1:3) = ttc([3 1 2]);
      set(audiotb, 'Children', ttc);
      set(this.WaveFigureHandle, 'Toolbar', 'none');
    end

    function createMenubar(this)
      mhData = uimenu(this.WaveFigureHandle, 'Label', 'Data');
      uimenu(mhData, 'Label', 'Read From File...', 'Callback', ...
             @(hobj, evd) fileOpenCallback(this));
      createAnalyzeMenu(this);
      mhData = uimenu(this.WaveFigureHandle, 'Label', 'About');
      uimenu(mhData, 'Label', 'About GEARS', 'Callback', ...
             @(hobj, evd) aboutUsOpenCallback(this));
    end

    function createAnalyzeMenu(this)
      hAnalyze = uimenu(this.WaveFigureHandle, 'Label', 'Analyze');
      names = loadAnalyzers(this);
      for i=1:length(names)
        uimenu(hAnalyze, 'Label', names{i}, 'Callback', ...
               @(obj, eventdata) analyzerCallback(this, obj));
      end
    end
  
    function names = loadAnalyzers(this)
      this.Analyzer = AnalysisManager;
      names = this.Analyzer.loadAnalyzers();
    end

    function adjustFigure(this)
      createToolbar(this);
      createMenubar(this);
    end

    % Add audio play back controls.
    function addPlaybackControls(this, parent)
      % Add playback controls
      load audiotoolbaricons;
      uipushtool(parent, 'CData', play_on,...
                   'TooltipString', 'Play',...
                   'HandleVisibility','off',...
                   'ClickedCallback', @(hobj, evd) playCallback(this));
      uipushtool(parent, 'CData', pause_default,...
                   'TooltipString', 'Pause',...
                   'HandleVisibility','off',...
                   'ClickedCallback', @(hobj, evd) pauseCallback(this));
      uipushtool(parent, 'CData', stop_default,...
                   'TooltipString', 'Stop',...
                   'HandleVisibility','off',...
                   'ClickedCallback', @(hobj, evd) stopCallback(this));
    end

    % Add the selector lines and the gray selection highlight area.
    function addSelectorTool(this)
      cbFcn = @(hobj, evd) selectButtonDownCallback(this, hobj);
      for i=1:length(this.AxesHandles)
        axes(this.AxesHandles(i));
        this.SelectorLines(1, i) = line([0 0], [-1 1], ...
                                        'ButtonDownFcn', cbFcn, ...
                                        'Parent', this.AxesHandles(i));
        this.SelectorLines(2, i) = line([0 0], [-1 1], ...
                                        'ButtonDownFcn', cbFcn, ...
                                        'Parent', this.AxesHandles(i));
      end
    end

    % Return the points where the selector lines are.
    % The first point is always smaller.
    function [xd1, xd2] = getSelectionPoints(this)
      xd1 = get(this.SelectorLines(1), 'XData');
      xd2 = get(this.SelectorLines(2), 'XData');
      if xd1(1) > xd2(1) % Make xd1 the smaller
        temp = xd1;
        xd1 = xd2;
        xd2 = temp;
      end
      
      %fprintf('GetSelectorPoints 2\n');
    end

    function [xd1, xd2] = getSelectionSampleNumbers(this)
      [xd1, xd2] = getSelectionPoints(this);
      xd1 = floor(xd1*this.Fs);
      xd2 = floor(xd2*this.Fs);
      
      %fprintf('GetSelectorSampleNumbers 3\n');
    end

    function stopAudioPlayer(this)
      if ~isempty(this.AudioPlayer)
        stop(this.AudioPlayer); % StopFcn is not called when player is paused
        h = this.AudioPlayer.UserData;
        if ishandle(h)
          delete(h);
        end
      end
    end

    function data = getCurrentData(this, x1, x2)
      if x1 ~= x2
        % Get selected data
        data = this.AudioData(x1(1)+1:x2(1), :);
      else
        % Get fftSize samples around cursor
        % 1048 on the left side and 1047 on the right. If the cursor is
        % close to the extremes the first or the last fftSize samples are
        % returned.
        audioLen = size(this.AudioData, 1);
        fftLength = this.fftSize;
        fftLengthHalf = fftLength/2;
        if x1 - (fftLengthHalf) >= 1 && x1+(fftLengthHalf) < audioLen
          data = this.AudioData(x1-(fftLengthHalf):x1+((fftLengthHalf)-1), :);
        elseif x1 - (fftLengthHalf) >= 1 && audioLen >= fftLength
          data = this.AudioData(end-(fftLength-1):end, :);
        elseif x1+(fftLengthHalf) < audioLen && audioLen >= fftLength
          data = this.AudioData(1:fftLength, :);
        else % audioLen < fftSize
          data = [this.AudioData; zeros(fftLength-audioLen ,size(this.AudioData, 2))];
        end
      end
    end
    
    function pastdata = getPastData(this, x1, x2)
      if x1 ~= x2
        % Get selected data
        pastdata = this.AudioData(x1(1)+1:x2(1), :);
      else
        fftLength = this.fftSize;
        fftLengthHalf = fftLength/2;
        % Get fftSize samples before fftSize/2 on the left side of the cursor
        if x1 - (fftLength + (fftLengthHalf)) >= 1
          pastdata = this.AudioData(x1-(fftLength + (fftLengthHalf)):x1-((fftLengthHalf)+1), :);
        else % audioLen < fftSize
          pastdata = zeros(fftLength,1);
        end
      end
    end

  end % Private methods

  methods (Access = 'public', Hidden = true) % Callback methods

    function resizeFcnCallback(this)
      fPos = get(this.WaveFigureHandle, 'Position');
      len = length(this.AxesHandles);
      h = round((fPos(4)-45-5*len)/len); % Height of each axis
      for i=0:len-1
        set(this.AxesHandles(len-i), 'Position', [35 45+h*i+5*i fPos(3)-60 h]);
        set(this.AxesHandles(len-i), 'plotBoxAspectRatio', [(fPos(3)-60)/h 1 1]);
        % Setting PlotBoxAspectRatio keeps the axis box on
        if i ~= 0
          % Need XTicks only for bottom axis
          set(this.AxesHandles(len-i), 'XTick', []);
        end
      end
      
      %fprintf('ResizeFcnCallback\n');
    end

    function fileOpenCallback(this)
      [filename, pathname] = ...
               uigetfile(this.FileFilterSpec, 'Audio file to read', ...
                         this.Filename);
      if ~(isequal(filename,0) || isequal(pathname,0))
        loadAudioFile(this, [pathname filename]);
      end
    end
    
    function aboutUsOpenCallback(this)
        this.AboutUs = AboutGEARS;
    end

    % Play audio
    function playCallback(this)
      stopAudioPlayer(this);
      [xd1, xd2] = getSelectionSampleNumbers(this);
      if xd1(1) == xd2(1)
        xd2(1) = size(this.AudioData, 1);
      end
      hl = zeros(length(this.AxesHandles), 1);
      for i=1:length(this.AxesHandles)
        axes(this.AxesHandles(i)); % Make this current axis
        hl(i) = line([xd1(1) xd1(1)]/this.Fs, [-1 1], 'Color', [1 0 0], ...
                     'Parent', this.AxesHandles(i));
      end
      this.AudioPlayer = audioplayer(this.AudioData(xd1(1)+1:xd2(1), :), this.Fs);
      set(this.AudioPlayer, 'TimerPeriod', 0.05, 'UserData', hl, ...
          'TimerFcn', @(ap, evd) audioPlayerCallbackTimer(this, ap, hl, xd1(1)), ...
          'StopFcn',  @(ap, evd) audioPlayerCallbackStop(this, ap, hl));
      play(this.AudioPlayer);
    end

    % Pause audio playback
    function pauseCallback(this)
      if ~isempty(this.AudioPlayer)
        if isplaying(this.AudioPlayer)
          pause(this.AudioPlayer);
        elseif this.AudioPlayer.CurrentSample ~= 1
          % resume only if it is paused
          resume(this.AudioPlayer);
        end
      end
    end

    % Stop audio playback
    function stopCallback(this)
      stopAudioPlayer(this);
    end

    % Position both selection lines at the mouse click point
    function axesButtonDownCallback(this)
      maxxd = size(this.AudioData, 1)/this.Fs;
      cp = get(this.AxesHandles(1), 'CurrentPoint');
      if cp(1) > maxxd || cp(1) < 0
          return;
      end
      if strcmp(get(this.WaveFigureHandle,'SelectionType'),'normal')
        % Move selector lines to this point
        set(this.SelectorLines, 'XData', [cp(1) cp(1)]);
        set(this.SelectorLines,'Color',[1,0.4,0.6],'LineWidth',2);
        hLine = this.SelectorLines(1,:);
        set(this.WaveFigureHandle, 'WindowButtonMotionFcn', ...
            @(hobj, evd) selectLineDragCallback(this, hLine));
      elseif strcmp(get(this.WaveFigureHandle,'SelectionType'),'alt') % right click
        xd1 = get(this.SelectorLines(1), 'XData');
        xd2 = get(this.SelectorLines(2), 'XData');
        % Find closest line and move that to this point
        if abs(xd1(1)-cp(1)) < abs(xd2(1)-cp(1))
          set(this.SelectorLines(1,:), 'XData', [cp(1) cp(1)]);
        else
          set(this.SelectorLines(2,:), 'XData', [cp(1) cp(1)]);
        end
      end
    end

    % Enable dragging of selection lines at the mouse click point
    function selectButtonDownCallback(this, hObject)
      if strcmp(get(this.WaveFigureHandle,'SelectionType'),'normal')
        [r, c] = find(this.SelectorLines == hObject);
        hLine = this.SelectorLines(1,:);
        set(this.WaveFigureHandle, 'WindowButtonMotionFcn', ...
            @(hobj, evd) selectLineDragCallback(this, hLine));
      end
      %fprintf('SelectButtonDownCallback\n');
    end

    % Drag selection lines
    function selectLineDragCallback(this, hLine)
      maxxd = size(this.AudioData, 1)/this.Fs;
      cp = get(this.AxesHandles(1), 'CurrentPoint');
      if cp(1) > maxxd || cp(1) < 0
          return;
      end
      if strcmp(get(this.WaveFigureHandle,'SelectionType'),'normal')
        % Move selector lines to this point
        set(this.SelectorLines, 'XData', [cp(1) cp(1)]);
      end
      [xd1, xd2] = getSelectionSampleNumbers(this);
      data = getCurrentData(this, xd1(1), xd2(1));
      pastdata = getPastData(this, xd1(1), xd2(1));
      this.Position = xd1(1) / this.Fs;
      this.Analyzer.analyze(data, pastdata, this.Fs, this.Position, 'Audio Fourier Analysis');
      
      %fprintf('SelectLineDragCallback 6\n');
    end

    function selectAllCallback(this)
      set(this.SelectorLines(1, :), 'XData', [0 0]);
      len = size(this.AudioData, 1)/this.Fs;
      set(this.SelectorLines(2, :), 'XData', [len len]);
    end

    % Stop selection drag
    function figureButtonUpCallback(this)
      set(this.WaveFigureHandle, 'WindowButtonMotionFcn', '');
      [this.xd1, this.xd2] = getSelectionSampleNumbers(this);
      data = getCurrentData(this, this.xd1(1), this.xd2(1));
      pastdata = getPastData(this, this.xd1(1), this.xd2(1));
      this.Position = this.xd1(1) / this.Fs;
      this.Analyzer.analyze(data, pastdata, this.Fs, this.Position, 'Audio Fourier Analysis');
      if this.engineSignalNotSet == 1
          this.Analyzer.setEngineSignal(this.AudioData);
          this.engineSignalNotSet = 0;
      end
    end

    % zoom
    function zoomCallback(this, hFig, eventdata)
      switch get(hFig, 'SelectionType')
        case {'normal', 'alt'}
          xlimits = xlim(eventdata.Axes);
        case 'open'
          xlimits = [0 size(this.AudioData, 1)/this.Fs];
      end
      updatePlot(this, xlimits);
    end

    % Callback for fit selection to the axis button
    function zoomSelectionCallback(this)
      [xd1, xd2] = getSelectionPoints(this);
      if xd1(1) ~= xd2(1)
        updatePlot(this, [xd1(1) xd2(1)]);
        set(this.AxesHandles, 'XLim', [xd1(1) xd2(1)]);
      end
    end

    % Callback for fit entire signal to the axis button
    function zoomOutfullCallback(this)
      xlimits = [0 size(this.AudioData, 1)/this.Fs];
      updatePlot(this, xlimits);
      set(this.AxesHandles, 'XLim', xlimits);
    end

    % Callback from analysis menu
    function analyzerCallback(this, obj)
      aName = get(obj, 'Label');
      [xd1, xd2] = getSelectionSampleNumbers(this);
      %data = getCurrentData(this, xd1(1), xd2(1));
      %this.Analyzer.analyze(data, this.Fs, aName);
    end

    function audioPlayerCallbackTimer(this, ap, hl, offset)
      if ~ishandle(hl), return, end
      x = get(ap, 'CurrentSample') + offset;
      set(hl, 'XData', [x x]/this.Fs);
      %data = getCurrentData(this, x, x);
      %this.Analyzer.analyze(data, this.Fs);
    end

    function audioPlayerCallbackStop(this, ap, hl)
      % This is also called when player is paused
      if ishandle(hl(1)) && (get(ap, 'CurrentSample') == 1 || ...
         get(ap, 'CurrentSample') == get(ap, 'TotalSamples'))
        delete(hl);
      end
    end

    function figureCloseCallback(this)
      stopAudioPlayer(this);
      delete(this.WaveFigureHandle);
      delete(this);
    end
    
  end

end
