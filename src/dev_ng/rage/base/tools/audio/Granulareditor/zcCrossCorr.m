classdef zcCrossCorr < handle

properties (SetAccess = 'private', GetAccess='private')
    pitchPanelHandle;
    grainPanelHandle;
    zeroCrossingsPanelHandle;
    editPanelHandle;
    
    pitchAxesHandle;
    grainAxesHandle;
    zeroCrossingsAxesHandle;
    
    textboxFramesizeHandle;
    textboxStepsizeHandle;

    buttonBeginingHandle;
    buttonEndHandle;
    buttonBackHandle;
    buttonNextHandle;
    buttonZeroCrossCorrHandle;
    buttonComputeGrainHandle;
    buttonSetStepToWindowHandle;
    buttonZeroCrossingHandle;
    buttonSetStartMarkerCurrentHandle;
    
    labelSampleHandle;
    labelPitchHandle;
    
    checkBoxPitchSyncHandle;
    
    engineSignal;
    Fs;
    TimeLine;
    StartIndex = 1;
    EndIndex;

    FrameSizeMs = 80;
    StepSize = 480;
    
    FirstGrainLength = 0;
    
    PitchMap;
    PitchMapOffsets;
    PitchMapValues;
    
    Grain;
    
    currentPositionStartHandle;
    currentPositionEndHandle;
    engineSignalBeginHandle;
    engineSignalEndHandle;
    
    engineSignalBegin = 1;
    engineSignalEnd = 1;
    
    GrainStart;
    GrainEnd;
    
    CrossCorrResults = [];
    zcCrossCorrResults = [];
end
  
  methods
    function this = zcCrossCorr()
        format long g;
        
        % Create figure
        this.FigureHandle = figure( ...
                    'Menubar','none', ...
                    'Toolbar','none', ...
                    'Position', [535,210,850,800], ...
                    'IntegerHandle', 'off', ...
                    'NumberTitle', 'off', ...
                    'Visible', 'off', ...
                    'Name', 'GEARS: Zero Crossing Analyser',...
                    'CloseRequestFcn', @(hObj, evd) figureCloseCallback(this));

warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
jframe=get(this.FigureHandle,'javaframe');
jIcon=javax.swing.ImageIcon('icons\icon.gif');
jframe.setFigureIcon(jIcon);

    
        this.pitchPanelHandle = uipanel('Parent',this.FigureHandle,'Title','Pitch Tracking',...
              'Position',[.01 .65 .75 .30]);
        this.grainPanelHandle = uipanel('Parent',this.FigureHandle,'Title','Wave View',...
              'Position',[.01 .35 .75 .30]);
        this.zeroCrossingsPanelHandle = uipanel('Parent',this.FigureHandle,'Title','Grain Data',...
              'Position',[.01 .05 .75 .30]);
        this.editPanelHandle = uipanel('Parent',this.FigureHandle,'Title','Options',...
              'Position',[.77 .05 .22 .90]);
    
        this.pitchAxesHandle = axes('DataAspectRatio',[1 1 1],...
            'Position',[.1  .1  .8  .8], ...
            'XLimMode','manual','YLimMode','manual',...
            'DrawMode','fast',...
            'Parent',this.pitchPanelHandle,...
            'ButtonDownFcn', @(hobj, evd) axesButtonDownCallback(this));
        this.grainAxesHandle = axes('DataAspectRatio',[1 1 1],...
            'Position',[.1  .1  .8  .8], ...
            'XLimMode','manual','YLimMode','manual',...
            'DrawMode','fast',...
            'Parent',this.grainPanelHandle);
        this.zeroCrossingsAxesHandle = axes('DataAspectRatio',[1 1 1],...
            'Position',[.1  .1  .8  .8], ...
            'XLimMode','manual','YLimMode','manual',...
            'DrawMode','fast',...
            'Parent',this.zeroCrossingsPanelHandle);
        
        textLabel4 = uicontrol('Style','text',...
               'Parent', this.editPanelHandle,...
               'string','Frame Size (Ms):',...
               'Units','normalized',...
               'position',[0.1 0.60 0.4 0.1]); 
        this.textboxFramesizeHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.50 0.63 0.3 0.05],...
                     'HandleVisibility','callback', ...
                     'String',this.FrameSizeMs,...
                     'Style','edit',...   
                     'Callback',@(src,event)frameSizeChange_cb(this));
                 
                 
        textLabel5 = uicontrol('Style','text',...
               'Parent', this.editPanelHandle,...
               'string','Step Size (Samples):',...
               'Units','normalized',...
               'position',[0.1 0.70 0.4 0.1]); 
        this.textboxStepsizeHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.50 0.73 0.3 0.05],...
                     'HandleVisibility','callback', ...
                     'String',this.StepSize,...
                     'Style','edit',...   
                     'Callback',@(src,event)stepSizeChange_cb(this));
                 
        this.checkBoxPitchSyncHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Style','checkbox',...
                     'Units','normalized',...
                     'String','Set Window Length to Pitch',...
                     'Value',0,'Position',[0.10 0.53 0.9 0.1],...   
                     'Callback',@(src,event)PitchSync_cb(this));
                 
        this.buttonBeginingHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.02 0.92 0.2 0.05],...
                     'HandleVisibility','callback', ...
                     'Style','pushbutton',...   
                     'Callback',@(src,event)Begin_cb(this));
        this.buttonBackHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.28 0.92 0.2 0.05],...
                     'HandleVisibility','callback', ...
                     'Style','pushbutton',...   
                     'Callback',@(src,event)Back_cb(this));
        this.buttonNextHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.53 0.92 0.2 0.05],...
                     'HandleVisibility','callback', ...
                     'Style','pushbutton',...   
                     'Callback',@(src,event)Next_cb(this));   
        this.buttonEndHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.78 0.92 0.2 0.05],...
                     'HandleVisibility','callback', ...
                     'Style','pushbutton',...   
                     'Callback',@(src,event)End_cb(this));
                 
beginImage = importdata('icons\begin.jpg');
set(this.buttonBeginingHandle, 'CDATA', beginImage);
backImage = importdata('icons\back.jpg');
set(this.buttonBackHandle, 'CDATA', backImage);
nextImage = importdata('icons\next.jpg');
set(this.buttonNextHandle, 'CDATA', nextImage);
endImage = importdata('icons\end.jpg');
set(this.buttonEndHandle, 'CDATA', endImage);
   

        this.buttonSetStepToWindowHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.1 0.41 0.8 0.07],...
                     'HandleVisibility','callback', ...
                     'String','Set Step Size To Frame Size',...
                     'Style','pushbutton',...   
                     'Callback',@(src,event)SetStepToWindow_cb(this));
        this.buttonSetStartMarkerCurrentHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.1 0.31 0.8 0.07],...
                     'HandleVisibility','callback', ...
                     'String','Start Boundary = Current Position',...
                     'Style','pushbutton',...   
                     'Callback',@(src,event)SetStartBoundary_cb(this));
        this.buttonZeroCrossingHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.1 0.21 0.8 0.07],...
                     'HandleVisibility','callback', ...
                     'String','Calculate Zero Crossings',...
                     'Style','pushbutton',...   
                     'Callback',@(src,event)ZeroCrossing_cb(this));
        this.buttonComputeGrainHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.1 0.11 0.8 0.07],...
                     'HandleVisibility','callback', ...
                     'String','Compute Grain',...
                     'Style','pushbutton',...   
                     'Callback',@(src,event)ComputeGrain_cb(this));
        this.buttonZeroCrossCorrHandle = uicontrol(...
                     'Parent', this.editPanelHandle, ...
                     'Units','normalized',...
                     'Position',[0.1 0.02 0.8 0.07],...
                     'HandleVisibility','callback', ...
                     'String','Calculate Cross Correlation',...
                     'Style','pushbutton',...   
                     'Callback',@(src,event)ZeroCrossCorr_cb(this));
                  
    end
    
    function PitchSync_cb(this)
        if (get(this.checkBoxPitchSyncHandle,'Value') == get(this.checkBoxPitchSyncHandle,'Max'))
            % Checkbox is checked-take approriate action
            % Get Closest Pitch value from Pitch Map
            %find current StartIndex pitch from pitchmap
            temp = min(this.PitchMap);
            if(this.StartIndex < temp)
              %this.StartIndex = temp
              currentPitch = temp(1,2)
            else
                [i,cv] = searchclosest(this.PitchMap, this.StartIndex);
                currentPitch = this.PitchMap(i,2);
            end
            %this.FrameSizeMs = round(this.Fs/(currentPitch*1000));
            this.FrameSizeMs = round(1000/currentPitch);
            set(this.textboxFramesizeHandle,'String', num2str(this.FrameSizeMs));
        else
            % Checkbox is not checked-take approriate action
            this.FrameSizeMs = str2num(get(this.textboxFramesizeHandle,'String'));
            set(this.textboxFramesizeHandle,'String', num2str(this.FrameSizeMs));
        end
        DrawLines(this);
        DrawBoundaries(this);
        DrawGrain(this);
    end
    
    function SetStartBoundary_cb(this) 
      %HL: Swapped this so it moves the grain marker instead
      %this.engineSignalBegin = this.StartIndex/this.Fs;
      this.StartIndex = this.engineSignalBegin * this.Fs;
      
      axes(this.pitchAxesHandle);
      set(this.pitchAxesHandle,'NextPlot','replace');
      DrawLines(this);
      DrawBoundaries(this);
      DrawGrain(this);
    end
    
    function axesButtonDownCallback(this)
      maxxd = size(this.engineSignal, 1)/this.Fs;
      cp = get(this.pitchAxesHandle, 'CurrentPoint');
      if cp(1) > maxxd || cp(1) < 0
          return;
      end
      if strcmp(get(this.FigureHandle,'SelectionType'),'normal')
        this.engineSignalBegin = cp(1);
        %HL: Added this so that left clicking just moves the grain selector
        SetStartBoundary_cb(this);
      elseif strcmp(get(this.FigureHandle,'SelectionType'),'alt') % right click
        this.engineSignalEnd = cp(1);
      end
      axes(this.pitchAxesHandle);
      set(this.pitchAxesHandle,'NextPlot','replace');
      DrawLines(this);
      DrawBoundaries(this);
    end
    
    function DrawBoundaries(this)
        y = [0 max(this.PitchMapValues)];
        x = [this.engineSignalBegin this.engineSignalBegin];
        %plot(this.pitchAxesHandle, x, y);
        this.engineSignalBeginHandle = line(x,y);
        set(this.engineSignalBeginHandle, 'HandleVisibility', 'on');
        set(this.engineSignalBeginHandle,'Color',[0.3,0.3,0.3],'LineWidth',2);
        
        x = [this.engineSignalEnd this.engineSignalEnd];
        this.engineSignalEndHandle = line(x,y);
        set(this.engineSignalEndHandle, 'HandleVisibility', 'on');
        set(this.engineSignalEndHandle,'Color',[0.3,0.3,0.3],'LineWidth',2);
         
        set(this.pitchPanelHandle,'Position',[.01 .65 .75 .30]);
    end
    
    function Begin_cb(this)
        this.StartIndex = 1;
        %this.StartIndex = this.engineSignalBegin;
        DrawLines(this);
        DrawBoundaries(this);
        DrawGrain(this);
    end
    
    function Back_cb(this)
        %this.StartIndex = this.StartIndex - (this.Fs*(this.FrameSizeMs/1000));
        this.StartIndex = this.StartIndex - this.StepSize;
        if this.StartIndex < 1
            this.StartIndex = 1;
        end
        DrawLines(this);
        DrawBoundaries(this);
        DrawGrain(this);
    end
    
    function Next_cb(this)
        %this.StartIndex = this.StartIndex + (this.Fs*(this.FrameSizeMs/1000));
        %if this.StartIndex > length(this.engineSignal) - (this.Fs*(this.FrameSizeMs/1000))
        %    this.StartIndex = length(this.engineSignal) - (this.Fs*(this.FrameSizeMs/1000));
        %end
        this.StartIndex = this.StartIndex + this.StepSize;
        if this.StartIndex > length(this.engineSignal) - this.StepSize
            this.StartIndex = length(this.engineSignal) - this.StepSize;
        end
        DrawLines(this);
        DrawBoundaries(this);
        DrawGrain(this);
    end  
    
    function End_cb(this)
        this.StartIndex = length(this.engineSignal) - (this.Fs*(this.FrameSizeMs/1000));
        %this.StartIndex = this.engineSignalEnd;
        DrawLines(this);
        DrawBoundaries(this);
        DrawGrain(this);
    end
                 
    function frameSizeChange_cb(this)
        this.FrameSizeMs = str2num(get(this.textboxFramesizeHandle,'String'));
        DrawLines(this);
        DrawBoundaries(this);
        DrawGrain(this);
    end
    
     function stepSizeChange_cb(this)
        this.StepSize = str2num(get(this.textboxStepsizeHandle,'String'));
        DrawLines(this);
        DrawBoundaries(this);
        DrawGrain(this);
     end
    
     function SetStepToWindow_cb(this)
         this.StepSize = this.Fs*(this.FrameSizeMs/1000);
         set(this.textboxStepsizeHandle,'String', num2str(this.StepSize));
     end
    
    function DrawGrain(this)
        this.Grain = this.engineSignal(this.StartIndex : this.EndIndex);
        
        dt = 1/this.Fs;      % sampling interval in seconds
        tmax = (this.EndIndex - this.StartIndex)/this.Fs;      % number of seconds to run each sine test
        tstart = (this.StartIndex-1)/this.Fs;
        tend = tstart + tmax;
        t = tstart:dt:tend;  % sampled time axis

        %t=(0:length(this.Grain)-1) * 0.01;
        ylim(this.grainAxesHandle, 'manual');
        ylim(this.grainAxesHandle, [-1 1]);
        plot(this.grainAxesHandle, t, this.engineSignal(this.StartIndex : this.EndIndex));
    end
    
    function DrawPitchMap(this, engineSignal, Fs, PitchMap, PitchMapOffsets, PitchMapValues)
        figure(this.FigureHandle);
        set(this.FigureHandle, 'HandleVisibility', 'on');
        
        this.PitchMap = PitchMap;
        this.PitchMapOffsets = PitchMapOffsets;
        this.PitchMapValues = PitchMapValues;
        this.engineSignal = engineSignal;
        this.Fs = Fs;
    
        %t=(0:length(PitchMapValues)-1) * 0.01;
        t=(0: round(length(engineSignal)/Fs)/length(PitchMapValues) :round(length(engineSignal)/Fs));
        this.TimeLine = t;
        if(length(t) ~= length(PitchMapValues))
            if(length(t) > length(PitchMapValues))
                PitchMapValues = [PitchMapValues zeros(1, length(t)-length(PitchMapValues))];
                this.PitchMapValues = PitchMapValues;
            end
            if(length(t) < length(PitchMapValues))
                PitchMapValues(end-(length(PitchMapValues)-length(t)):end) = [];
                this.PitchMapValues = PitchMapValues;
            end
        end

        DrawLines(this);
        DrawBoundaries(this);
        DrawGrain(this);
    end
    
    function DrawLines(this)
        this.EndIndex = this.StartIndex + (this.Fs*(this.FrameSizeMs/1000));
        
        axes(this.pitchAxesHandle);
        plot(this.pitchAxesHandle, this.TimeLine, this.PitchMapValues);
        
        set(this.pitchAxesHandle,'NextPlot','add');
        hold on;

        y = [0 max(this.PitchMapValues)];
        x = [(this.StartIndex/this.Fs) (this.StartIndex/this.Fs)];
        %plot(this.pitchAxesHandle, x, y);
        this.currentPositionStartHandle = line(x,y);
        set(this.currentPositionStartHandle, 'HandleVisibility', 'on');
        set(this.currentPositionStartHandle,'Color',[1,0.4,0.6],'LineWidth',2);

        x = [(this.EndIndex/this.Fs) (this.EndIndex/this.Fs)];
        this.currentPositionEndHandle = line(x,y);
        set(this.currentPositionEndHandle, 'HandleVisibility', 'on');
        set(this.currentPositionEndHandle,'Color',[1,0.3,0.9],'LineWidth',2);
        
        y = [max(this.PitchMapValues) max(this.PitchMapValues)];
        x = [(this.StartIndex/this.Fs) (this.EndIndex/this.Fs)];
        ln = line(x,y);
        set(ln, 'HandleVisibility', 'on');
        set(ln,'Color',[1,0.3,0.9],'LineWidth',2);
        
        hold off;
        
        set(this.pitchAxesHandle, 'ButtonDownFcn', @(hobj, evd) axesButtonDownCallback(this));
        
        %get(gca,'Children')
    end
    
    function ZeroCrossing_cb(this)
        axes(this.grainAxesHandle);
        set(this.grainAxesHandle,'NextPlot','replace');
        this.Grain = this.engineSignal(this.StartIndex : this.EndIndex);
        
        dt = 1/this.Fs;      % sampling interval in seconds
        tmax = (this.EndIndex - this.StartIndex)/this.Fs;      % number of seconds to run each sine test
        tstart = (this.StartIndex-1)/this.Fs;
        tend = tstart + tmax;
        t = tstart:dt:tend;  % sampled time axis

        %t=(0:length(this.Grain)-1) * 0.01;
        plot(this.grainAxesHandle, t, this.engineSignal(this.StartIndex : this.EndIndex));
        ylim(this.grainAxesHandle, [-1 1]);
        
        %get zero crossings and display
        count = zcr(this.Grain);
        [I] = zcross(this.Grain, 10);
        xaxis = this.StartIndex:this.EndIndex;
        plot(this.grainAxesHandle, xaxis, this.Grain);
        set(this.grainAxesHandle,'NextPlot','add');
        hold on;
        for i=1:length(I)
            stem(this.grainAxesHandle, this.StartIndex + I(i), 1,'red');
        end
        hold off;
        
        this.GrainStart = this.StartIndex + I(1);
        this.GrainEnd = this.StartIndex + I(end);
    end
    
    function ComputeGrain_cb(this)
        axes(this.zeroCrossingsAxesHandle);
        set(this.zeroCrossingsAxesHandle,'NextPlot','replace');
        this.Grain = this.engineSignal(this.GrainStart : this.GrainEnd);
        xaxis = this.GrainStart:this.GrainEnd;
        dt = 1/this.Fs;      % sampling interval in seconds
        tmax = (this.EndIndex - this.StartIndex)/this.Fs;      % number of seconds to run each sine test
        tstart = (this.StartIndex-1)/this.Fs;
        tend = tstart + tmax;
        t = tstart:dt:tend;  % sampled time axis
        plot(this.zeroCrossingsAxesHandle, xaxis, this.Grain);
        
        this.CrossCorrResults = [];
        this.CrossCorrResults = [this.CrossCorrResults this.GrainStart];
    end
    
    function ZeroCrossCorr_cb(this)
        % TODO: Look at using the rate of change of pitch to get a better
        % estimate of end of cycle
        
        %TODO: Look at using fundamental to work out where to make slice
        %instead of using zero crossings
        
        while(this.GrainEnd < this.engineSignalEnd * this.Fs)
        
        restofsignal = this.engineSignal(this.GrainEnd + 1 : this.GrainEnd + 1 + (length(this.Grain) * 2));
        [results,lags] = xcorr(this.Grain,restofsignal);
        [maxResult,maxIndex] = max(results);
        xcorr_lag = lags(maxIndex);
        
        nextStart = this.GrainEnd + 1 - xcorr_lag;
        [i,cv] = searchclosest(this.PitchMap, nextStart);
        currentPitch = this.PitchMap(i,2);
        nextEnd = nextStart + (this.Fs*(round(1000/currentPitch)/1000));
        nextGrain = this.engineSignal(nextStart : nextEnd);
        
        if(this.FirstGrainLength == 0)
            this.FirstGrainLength = nextStart - this.GrainStart;
        end
        
        if(length(nextGrain) > this.FirstGrainLength)
            xcorr_lag = lags(maxIndex(2));
            
            nextStart = this.GrainEnd + 1 - xcorr_lag;
            [i,cv] = searchclosest(this.PitchMap, nextStart);
            currentPitch = this.PitchMap(i,2);
            nextEnd = nextStart + (this.Fs*(round(1000/currentPitch)/1000));
            nextGrain = this.engineSignal(nextStart : nextEnd);
        end
        
        this.Grain = nextGrain;
        this.GrainStart = nextStart;
        this.GrainEnd = nextEnd;
        
        figure(13)
        subplot(2,1,1);
        plot(this.Grain);
        legend('CurrentGrain')
        %subplot(3,1,2);
        %plot(restofsignal);
        %legend('Rest of Signal')
        subplot(2,1,2);
        plot(nextGrain);
        legend('Next Grain')
        
        this.CrossCorrResults = [this.CrossCorrResults nextStart];
        
        pause(0.1);
        end
        
%         [file,path] = uiputfile('*.xls','Save CrossCorr Results As');
%         xlswrite([path file], this.CrossCorrResults', 'CrossCorrResults');
%        
%         this.zcCrossCorrResults = [];
%         for i=1:length(this.CrossCorrResults)-1
%             tempStart = this.CrossCorrResults(i);
%             tempEnd = this.CrossCorrResults(i+1)-1;
%             tempGrain = this.engineSignal(tempStart : tempEnd);
%             [I] = zcross(tempGrain, 10);
%             tempzcorrStart = tempStart + I(1);
%             this.zcCrossCorrResults = [this.zcCrossCorrResults tempzcorrStart];
%             
%             xaxis = tempStart:tempEnd;
%             figure(33)
%             plot(xaxis, tempGrain);
%             hold on;
%                 stem(tempzcorrStart, 1,'red');
%             hold off
%             
%             pause(0.1)
%         end
%         
%         [file,path] = uiputfile('*.xls','Save ZeroCrossings CrossCorr Results As');
%         xlswrite([path file], this.zcCrossCorrResults', 'zcCrossCorrResults');

    end
    
    function figureCloseCallback(this)
        delete(this.FigureHandle);
        delete(this);
    end

    function delete(this)
        if ishandle(this.FigureHandle)
            delete(this.FigureHandle);
        end
    end
  end

  methods (Static)
    function name = getName()
        name = 'GEARS Zero Crossing Analyser';
    end
    % This can be called for any change in cursor position
    function isd = isDynamic()
        isd = false;
    end
  end

  properties
      FigureHandle
  end

end
