classdef AudioFourierAnalysis < handle

properties (SetAccess = 'private', GetAccess='private')
    AxesHandles;         % Handles to each axes. Each channel is in one axis
    SelectorLines;       % The lines used to select audio data in the editor
    FourierSignal;
    SampleFrequency;
    FFTPoints;
    Cylinders = 8;
    PitchResolution;
    RPM;
    Position = 0;
    fftSlider = [];
    phaseSlider = [];
    Axes = [];
    Image = [];
    CLimit = [];
    EditTextBox = [];
    EditTextBoxNFFT = [];
    fftScaleFactor = 1;
    phaseScaleFactor = 1;
    Data;
    PastData;
    Fs;
    tempPoint;
    gridDrawn = 0;
    PitchTrack;
    engineSignal;
end
  
  methods
    function this = AudioFourierAnalysis()
        format long g;
        
        % Create figure
        this.FigureHandle = figure( ...
                    'Menubar','none', ...
                    'Toolbar','figure', ...
                    'Position', [535,210,850,500], ...
                    'IntegerHandle', 'off', ...
                    'NumberTitle', 'off', ...
                    'Name', 'GEARS: Audio Fourier Analysis',...
                    'WindowButtonUpFcn', @(hobj, evd) figureButtonUpCallback(this), ...
                    'CloseRequestFcn', @(hObj, evd) figureCloseCallback(this));

warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
jframe=get(this.FigureHandle,'javaframe');
jIcon=javax.swing.ImageIcon('icons\icon.gif');
jframe.setFigureIcon(jIcon);

         % Remove all buttons except zoom in, out and data cursor
         tb = uitoolbar(this.FigureHandle);
         tth = findall(this.FigureHandle, 'Type', 'uitoolbar');
         ttb = findall(tth, 'Type', 'uitoggletool');
         copyobj(findobj(ttb, 'Tag', 'Exploration.ZoomIn'), tb);
         copyobj(findobj(ttb, 'Tag', 'Exploration.ZoomOut'), tb);
         set(this.FigureHandle, 'Toolbar', 'none', 'HandleVisibility', 'off');
         this.Axes = axes('DataAspectRatio',[1 1 1],...
            'XLimMode','manual','YLimMode','manual',...
            'DrawMode','fast',...
            'Parent',this.FigureHandle);
         this.CLimit = get(this.Axes,'CLim');
         this.fftSlider = uicontrol('Style','slider',...
            'Parent',this.FigureHandle,...
            'Max',this.CLimit(2),...
            'Min',this.CLimit(1),...
            'Value',0,...
            'Units','normalized',...
            'Position',[.9286 .1724 .0257 .6897],...
            'SliderStep',[.005 .002],...        
            'BackgroundColor','red',...   
            'Callback',@(src,event)fftslider_cb(this));
        this.phaseSlider = uicontrol('Style','slider',...
            'Parent',this.FigureHandle,...
            'Max',this.CLimit(2),...
            'Min',this.CLimit(1),...
            'Value',0,...
            'Units','normalized',...
            'Position',[.01 .1724 .0257 .6897],...
            'SliderStep',[.005 .002],...        
            'BackgroundColor','blue',...   
            'Callback',@(src,event)phaseslider_cb(this));
        this.EditTextBox = uicontrol('Style','edit',...
               'Parent',this.FigureHandle,...
               'String',num2str(this.Cylinders),...
               'Position',[.22 .7724 54 30],...
               'Callback',@(src,event)editbox_cb(this));
        textLabel = uicontrol('Style','text',... % Add labels to sliders 
               'Parent',this.FigureHandle,...
               'string','Number of Cylindars',... % Text only, so 
               'position',[1 30 100 20]); % no callback needed 
        buttonTemp = uicontrol('Style', 'pushbutton',...
                'Parent',this.FigureHandle,...
                'String', 'Track Pitch',...
                'Position', [100 2 70 30],...
                'Callback', @(src,event)LoadPitchTracker(this));
            
            this.PitchTrack = PitchTracker;
    end
    
    function LoadPitchTracker(this)
      %if ishandle(this.PitchTrack)
      if isvalid(this.PitchTrack)
      else
        this.PitchTrack = PitchTracker;
      end
      
      if isempty(this.PitchResolution)
          msgBox('No base frequency selected', 'Error');
      else
        %this.Position = Fs * 9.4 seconds;
        this.PitchTrack.trackPitch(this.engineSignal, this.Fs, this.Fs * this.Position, this.PitchResolution);
      end
    end
    
    function fftslider_cb(AudioFourierAnalysis)
         min_val = get(AudioFourierAnalysis.fftSlider,'Value');
         AudioFourierAnalysis.fftScaleFactor = (min_val * 10) + 1;
         analyze(AudioFourierAnalysis, AudioFourierAnalysis.Data, AudioFourierAnalysis.PastData, AudioFourierAnalysis.Fs, AudioFourierAnalysis.Position);
    end
    
    function phaseslider_cb(AudioFourierAnalysis)
         min_val = get(AudioFourierAnalysis.phaseSlider,'Value');
         AudioFourierAnalysis.phaseScaleFactor = (min_val * 10) + 1;
         analyze(AudioFourierAnalysis, AudioFourierAnalysis.Data, AudioFourierAnalysis.PastData, AudioFourierAnalysis.Fs, AudioFourierAnalysis.Position);
    end
    
    function editbox_cb(AudioFourierAnalysis)
        AudioFourierAnalysis.Cylinders = str2num(get(AudioFourierAnalysis.EditTextBox,'String'));
        recalcPitchGrid(AudioFourierAnalysis);
    end
    
    function editboxNFFT_cb(AudioFourierAnalysis)
        AudioFourierAnalysis.Nres = str2num(get(AudioFourierAnalysis.EditTextBoxNFFT,'String'));
    end
    
    function setEngineSignal(this, signal)
        this.engineSignal = signal;
    end
      
    function analyze(this, data, pastdata, Fs, position)
      
      figure(this.FigureHandle);
      set(this.FigureHandle, 'HandleVisibility', 'on');
      this.Position = position;
      this.Data = data;
      this.PastData = pastdata;
      this.Fs = Fs;

      if size(data, 2) == 1
            subplot(1,1,1);  
            Nfft = Fs;     %number of points in fft
            Nfft = 2^nextpow2(Nfft); % Next power of 2 from length of y
            fourierTimeline = Fs*(0:(Nfft/2)-1)/Nfft; % Calculate the frequency axis

            hannWindow = hann(length(data));
            data = data.*hannWindow;
            pastdata = pastdata.*hannWindow;

            %Fourier Analysis
            fourierSignal = fft(data,Nfft) ; %Performing Nfft point fft
            YfourierSignal=abs(fourierSignal(1:length(fourierSignal)/2));
            YfourierSignal_inverse = YfourierSignal .* -2;
            YfourierSignal = YfourierSignal .* 2;
            YfourierSignal_inverse = YfourierSignal_inverse .* this.fftScaleFactor;
            YfourierSignal = YfourierSignal .* this.fftScaleFactor;

            %Phase Analysis
            phaseSignal = fourierSignal(1:length(fourierSignal)/2);
            m = abs(phaseSignal);           % m = magnitude of sinusoids

            ph=angle(fourierSignal(1:length(fourierSignal)/2));

            %phold=zeros(1,length(fourierSignal)/2);
            %>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            %Fourier Analysis
            fourierSignalPast = fft(pastdata,Nfft) ; %Performing Nfft point fft

            phold = angle(fourierSignalPast(1:length(fourierSignalPast)/2));
            %>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            
            all2pi=2*pi*(0:100);
            dtin = length(data)/Fs;
            
            % find peaks to define spectral mapping
            peaks=findPeaks(m, 50, 0.005);
            [dummy,inds]=sort(m(peaks(:,2)));
            peaksort=peaks(inds,:);
            pc=peaksort(:,2);

            bestf=zeros(size(pc));
            
            for tk=1:length(pc)                % estimate frequency using PV strategy  
                dtheta=(ph(pc(tk))-phold(pc(tk)))+all2pi;
                fest=dtheta./(2*pi*dtin);      % see pvanalysis.m for same idea
                [~,indf]=min(abs(fourierTimeline(pc(tk))-fest));
                bestf(tk)=fest(indf);          % find best freq estimate for each row
            end
            
            % find troughs for interpolation
            troughs=findTroughs(m, 50, 0.005);
            [dummy2,inds2]=sort(m(troughs(:,2)));
            troughsort=troughs(inds2,:);
            tc=troughsort(:,2);

            worstf=zeros(size(tc));
            for tk=1:length(tc)                % estimate frequency using PV strategy  
                dtheta=(ph(tc(tk))-phold(tc(tk)))+all2pi;
                fest=dtheta./(2*pi*dtin);      % see pvanalysis.m for same idea
                [er,indf]=min(abs(fourierTimeline(tc(tk))-fest));
                worstf(tk)=fest(indf);          % find best freq estimate for each row
            end
            
            bestf = [0 bestf' worstf'];
            dummy = [0 dummy' dummy2'];

            [sortedFrequ,IX]  = sort(bestf);
            %for index=1:length(IX)
            %    sortedMag(index,:) = dummy(IX(index));
            %end
            sortedMag = dummy(IX);

            sortedMag = sortedMag .* 1;
            inv_sortedMag = sortedMag .* -1;

            pp = interp1(sortedFrequ,sortedMag','cubic','pp');
            yi = ppval(pp, fourierTimeline);
            yi2 = yi .* -1;
            yi = yi .* this.phaseScaleFactor;
            yi2 = yi2 .* this.phaseScaleFactor;

            tempPlotHandle = plot(fourierTimeline, YfourierSignal,'-r', fourierTimeline, YfourierSignal_inverse, '-r', fourierTimeline, yi, 'b-', fourierTimeline, yi2, 'b-');
            %tempPlotHandle = plot(fourierTimeline, YfourierSignal,'-r', fourierTimeline, YfourierSignal_inverse, '-r');
            legend('FFT', 'invFFT', 'Phase', 'invPhase')
            set(tempPlotHandle, 'HandleVisibility', 'off');
           
            ylim([-800 800]) % Set the y-axis limit
            xlim([0 1000]) % Set the x-axis so we can see the peaks
            xlabel('Frequency (Hz)')
            ylabel('Amplitude')
            title('CZT and Fourier Transform')

            %_Correlation estimation of Pitch for Grid______>
%             maxFreq = round(Fs/(position * 8)); %50Hz
%             minFreq = round(Fs/position * 3); %30Hz
%             maxShift = round(minFreq*2);    %100 Hz
%             [r, lags] = xcorr(data, maxShift, 'coeff');
% 
%             r = r(maxShift + 1:2 * maxShift + 1);
% 
%             [maxValue, maxIndex] = max(r(maxFreq:maxShift));
%             xcorrFreq = round2(Fs/(maxFreq + maxIndex - 1), 0.1);
%             
%             this.PitchResolution = xcorrFreq;
            %_______________________________________________>
 
            if this.PitchResolution > 0             
                hold on;

                for k = 0:this.PitchResolution:1000
                    x = [k k];
                    y = [-1000 1000];
                    ln = line(x,y);
                    set(ln, 'HandleVisibility', 'on');
                    if k == this.PitchResolution * this.Cylinders
                        set(ln,'Color',[1,0.4,0.6],'LineWidth',2);
                    else
                        set(ln,'Color',[0.5,0.2,0.8]);
                    end
                end

                legend(['Pitch Resolution = ',num2str(this.PitchResolution), ' - RPM = ', num2str(this.RPM)])

                hold off;
            end
      else
          subplot(2,1,1);
          spectrogram(data(:,1), 256, 128, 256, Fs, 'yaxis');
          subplot(2,1,2);
          spectrogram(data(:,2), 256, 128, 256, Fs, 'yaxis');
      end
      %set(this.FigureHandle, 'HandleVisibility', 'off');
    end
    
    function figureButtonUpCallback(this)
        cla;
        ca = get(this.FigureHandle(1), 'CurrentAxes');
        this.tempPoint = get(ca, 'CurrentPoint');
      
        hold on;

        currentHarmonics = this.Cylinders;
        currentFrequPoint = this.tempPoint(1);
        this.PitchResolution = currentFrequPoint/currentHarmonics;
        for k = 0:this.PitchResolution:1000
            x = [k k];
            y = [-1000 1000];
            ln = line(x,y);
            set(ln, 'HandleVisibility', 'on');
            if k == this.PitchResolution * currentHarmonics
                set(ln,'Color',[1,0.4,0.6],'LineWidth',2);
            else
                set(ln,'Color',[0.5,0.2,0.8]);
            end
        end
  
        this.RPM = 120 * this.PitchResolution;
        legend(['Pitch Resolution = ',num2str(this.PitchResolution), ' - RPM = ', num2str(this.RPM)])
          
        hold off;
        this.gridDrawn = 1;
    end
    
    function recalcPitchGrid(this)
        if(this.gridDrawn == 1)
            cla;
            hold on;

            currentHarmonics = this.Cylinders;
            currentFrequPoint = this.tempPoint(1);
            this.PitchResolution = currentFrequPoint/currentHarmonics;
            for k = 0:this.PitchResolution:1000
                x = [k k];
                y = [-1000 1000];
                ln = line(x,y);
                set(ln, 'HandleVisibility', 'on');
                if k == this.PitchResolution * currentHarmonics
                    set(ln,'Color',[1,0.4,0.6],'LineWidth',2);
                else
                    set(ln,'Color',[0.5,0.2,0.8]);
                end
            end

            this.RPM = 120 * this.PitchResolution;
            legend(['Pitch Resolution = ',num2str(this.PitchResolution), ' - RPM = ', num2str(this.RPM)])

            hold off;
        end
    end

%     % Drag selection lines
%     function selectLineDragCallback(this, hLine)
%       maxxd = size(this.AudioData, 1)/this.Fs;
%       cp = get(this.AxesHandles(1), 'CurrentPoint');
%       if cp(1) > maxxd || cp(1) < 0
%           return;
%       end
%       if strcmp(get(this.FigureHandle,'SelectionType'),'normal')
%         % Move selector lines to this point
%         set(this.SelectorLines, 'XData', [cp(1) cp(1)]);
%       end
%     end
    
    % Return the points where the selector lines are.
    % The first point is always smaller.
    function [xd1, xd2] = getSelectionPoints(this)
      xd1 = get(this.SelectorLines(1), 'XData');
      xd2 = get(this.SelectorLines(2), 'XData');
      if xd1(1) > xd2(1) % Make xd1 the smaller
        temp = xd1;
        xd1 = xd2;
        xd2 = temp;
      end
    end

    function [xd1, xd2] = getSelectionSampleNumbers(this)
      [xd1, xd2] = getSelectionPoints(this);
      xd1 = floor(xd1*this.Fs);
      xd2 = floor(xd2*this.Fs);
    end
    
    function figureCloseCallback(this)
        delete(this.FigureHandle);
        delete(this);
    end

    function delete(this)
        if ishandle(this.FigureHandle)
            delete(this.FigureHandle);
        end
    end
  end

  methods (Static)
    function name = getName()
        name = 'Audio Fourier Analysis';
    end
    % This can be called for any change in cursor position
    function isd = isDynamic()
        isd = false;
    end
  end

  properties
      FigureHandle
  end

end
