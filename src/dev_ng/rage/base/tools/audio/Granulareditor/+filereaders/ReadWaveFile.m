classdef ReadWaveFile < handle

  methods
    function this = ReadWaveFile(varargin)
    end

    function ext = getFileExtensions(this)
        ext = {'wav'};
    end

    function desc = getExtDescription(this)
        desc = 'WAV Files (*.wav)';
    end

    function setFilename(this, filename)
        this.Filename = filename;
    end

    function data = getData(this)
        try
            data = wavread(this.Filename);
        catch ME
            rethrow(ME);
        end
    end

    function Fs = getSampleRate(this)
        [data, Fs] = wavread(this.Filename, 1);
    end
  end

  properties
    Filename
  end

end
