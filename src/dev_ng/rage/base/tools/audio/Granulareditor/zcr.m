function count = zcr(sig)
% ZCR number of zero crossings in x
%    [n] = zc(x) calculates the number of zero crossings in x
    thresh = 0;
    N = length(sig);
    zc = (sig >= thresh) - (sig < thresh);
    count = sum((zc(1:N-1) - zc(2:N)) ~= 0);
return

