using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Xsl;

namespace audxmltransform
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Usage: axt.exe <transform.xsl> <file1.xml> ... <filen.xml>");
            }
            else
            {
                XslCompiledTransform xslt = new XslCompiledTransform();
                // first argument is xsl file
                xslt.Load(args[0]);

                for (int i = 1; i < args.Length; i++)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(args[i]);
                    XmlTextWriter results = new XmlTextWriter(args[i], null);
                    results.Formatting = Formatting.Indented;
                                    
                    xslt.Transform(doc, results);
                    results.Close();
                }
            }
        }
    }
}
