﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using Wavelib;
using System.Xml.Serialization;

namespace GranularPlayback2
{
	public class GranularMix
	{
		enum audGrainCrossfadeStyle
		{
			CrossfadeStyleEqualPower,
			CrossfadeStyleLinear,
		};

		public float s_GranularLoopBelowBias = 1.5f;
		audGrainCrossfadeStyle m_CrossfadeStyle = audGrainCrossfadeStyle.CrossfadeStyleEqualPower;

		List<GranularSubmix> m_SynchronisedLoops;
		GranularSubmix m_defaultGranularSubmix;
		GranularSubmix m_debugGranularSubmix;
		public bwWaveFile m_WaveData;
		public GranularSubmix.audGrainDataStruct m_GrainDataStruct;

		/// <summary>
		/// Granular mix constructor
		/// </summary>
		public GranularMix(string waveFileName, string grainFileName)
		{
			m_WaveData = new bwWaveFile(waveFileName);

			if (m_WaveData.Format.NumChannels > 1)
			{
				MessageBox.Show("The selected file has " + m_WaveData.Format.NumChannels + " channels. Only mono assets are supported", "Invalid Format");
				m_WaveData = null;
				return;
			}

			if (m_WaveData.Data == null)
			{
				MessageBox.Show("The selected file could not be loaded. It does not appear to contain valid audio data", "Unexpected Error");
				return;
			}

			List<GranularSubmix.audGrainData> grainData = new List<GranularSubmix.audGrainData>();

			if (System.IO.File.Exists(grainFileName))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(GranularSubmix.audGrainDataStruct));
				TextReader textReader = new StreamReader(grainFileName);
				m_GrainDataStruct = (GranularSubmix.audGrainDataStruct)serializer.Deserialize(textReader);
				grainData = m_GrainDataStruct.grainData;
				textReader.Close();
			}

			m_SynchronisedLoops = new List<GranularSubmix>();
			m_defaultGranularSubmix = new GranularSubmix(m_WaveData, grainData, GranularSubmix.audGranularSubmixType.SubmixTypePureGranular, GranularSubmix.audGrainPlaybackOrder.Sequential);
			m_debugGranularSubmix = new GranularSubmix(m_WaveData, grainData, GranularSubmix.audGranularSubmixType.SubmixTypeDebugSingleGranule, GranularSubmix.audGrainPlaybackOrder.Sequential);

			if (m_GrainDataStruct.loopDefinitions != null)
			{
				for(int loop = 0; loop < m_GrainDataStruct.loopDefinitions.Count; loop++)
				{
					GranularSubmix submix = new GranularSubmix(m_WaveData, grainData, GranularSubmix.audGranularSubmixType.SubmixTypeSynchronisedLoop, m_GrainDataStruct.loopDefinitions[loop].playbackOrder);
					submix.SetValidGrains(m_GrainDataStruct.loopDefinitions[loop].validGrains);
					submix.SetSubmixIdentifier(m_GrainDataStruct.loopDefinitions[loop].submixIdentifier);
					m_SynchronisedLoops.Add(submix);
				}
			}
		}

		/// <summary>
		/// Add a new loop
		/// </summary
		public void AddLoop(GranularSubmix loop)
		{
			m_SynchronisedLoops.Add(loop);
		}

		/// <summary>
		/// Remove a loop
		/// </summary
		public void RemoveLoop(int index)
		{
			if(index < m_SynchronisedLoops.Count)
				m_SynchronisedLoops.RemoveAt(index);
		}

		/// <summary>
		/// Get the granular submix
		/// </summary>
		public GranularSubmix GetGranularSubmix()
		{
			return m_defaultGranularSubmix;
		}

		/// <summary>
		/// Get the debug submix
		/// </summary>
		public GranularSubmix GetDebugSubmix()
		{
			return m_debugGranularSubmix;
		}

		/// <summary>
		/// Get the number of loops
		/// </summary>
		/// <returns></returns>
		public int GetNumSynchronisedLoops()
		{
			return m_SynchronisedLoops.Count;
		}

		/// <summary>
		/// Get a synchronised loop
		/// </summary>
		public GranularSubmix GetSynchronisedLoop(int index)
		{
			return m_SynchronisedLoops[index];
		}

		/// <summary>
		/// Generate a frame of data
		/// </summary>
		public void GenerateFrame(float volumeScale, 
								  GrainPlayer.audGrainPlaybackStyle playbackStyle,
								  float[] destBuffer,
								  float currentRateHz,
								  float loopVolumeScaleIn,
								  float grainVolumeScaleIn,
								  float currentGranularFraction,
								  float granuleFractionPerSample)
		{
			if(playbackStyle == GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug)
			{
				m_debugGranularSubmix.GenerateFrame(destBuffer, 0.0f, currentRateHz, 1.0f, currentGranularFraction, granuleFractionPerSample);
				return;
			}
			
			int nearestLoopAbove = -1;
			int nearestLoopBelow = -1;
			float nearestLoopAboveVolumeScale = 1.0f;
			float nearestLoopBelowVolumeScale = 1.0f;

			if (m_SynchronisedLoops.Count > 0)
			{
				CalculateNearestLoops(currentRateHz, out nearestLoopAbove, out nearestLoopBelow, out nearestLoopAboveVolumeScale, out nearestLoopBelowVolumeScale);
			}
			else
			{
				loopVolumeScaleIn = 0.0f;
			}

			float loopVolumeScale = loopVolumeScaleIn;
			float grainVolumeScale = grainVolumeScaleIn;

			if (playbackStyle == GrainPlayer.audGrainPlaybackStyle.PlaybackStyleGrainsOnly)
			{
				loopVolumeScale = 0.0f;
				grainVolumeScale = 1.0f;
			}
			else if (playbackStyle == GrainPlayer.audGrainPlaybackStyle.PlaybackStyleLoopsOnly)
			{
				loopVolumeScale = 1.0f;
				grainVolumeScale = 0.0f;
			}

			if (m_CrossfadeStyle == audGrainCrossfadeStyle.CrossfadeStyleEqualPower)
			{
				const float PI_over_2 = (float)(Math.PI / 2.0f);
				loopVolumeScale = (float)Math.Sin(loopVolumeScaleIn * PI_over_2);
				grainVolumeScale = (float)Math.Sin((1.0f - loopVolumeScaleIn) * PI_over_2);
			}

			loopVolumeScale *= volumeScale;
			grainVolumeScale *= volumeScale;

			// Debug - just make sure we set a 0.0 volume for the rendering
			for (int loop = 0; loop < m_SynchronisedLoops.Count; loop++)
			{
				if (loop != nearestLoopBelow && loop != nearestLoopAbove)
				{
					m_SynchronisedLoops[loop].m_LastVolumeScale = 0.0f;
				}
			}

			// Mix in the closest two synchronised loops above and below
			if (loopVolumeScale > 0.0f)
			{
				if (nearestLoopAbove != -1)
				{
					m_SynchronisedLoops[nearestLoopAbove].GenerateFrame(destBuffer, -1.0f, currentRateHz, nearestLoopAboveVolumeScale * loopVolumeScale, currentGranularFraction, granuleFractionPerSample);
				}

				if (nearestLoopBelow != -1 && nearestLoopBelow != nearestLoopAbove)
				{
					m_SynchronisedLoops[nearestLoopBelow].GenerateFrame(destBuffer, -1.0f, currentRateHz, nearestLoopBelowVolumeScale * loopVolumeScale, currentGranularFraction, granuleFractionPerSample);
				}
			}

			if (grainVolumeScale > 0.0f)
			{
				float nextGrainAsFraction = -1.0f;

				// In order to blend smoothly between the loops and grains, the pure granular submix need to be playing back from within the set of grains bounded
				// by the two closest loops (ie. the two that would start playing if the change rate drops to zero). Therefore work out what the current hertz value
				// corresponds to as a fraction between the two loops, and use that value as our current X position
				if (nearestLoopAbove >= 0 && nearestLoopBelow >= 0)
				{
					float fractionBetween = (currentRateHz - m_SynchronisedLoops[nearestLoopBelow].GetAverageValidGrainHertz()) / (m_SynchronisedLoops[nearestLoopAbove].GetAverageValidGrainHertz() - m_SynchronisedLoops[nearestLoopBelow].GetAverageValidGrainHertz());
					nextGrainAsFraction = (m_SynchronisedLoops[nearestLoopBelow].GetAverageValidGrainFraction() + ((m_SynchronisedLoops[nearestLoopAbove].GetAverageValidGrainFraction() - m_SynchronisedLoops[nearestLoopBelow].GetAverageValidGrainFraction()) * fractionBetween));
				}
				else if (nearestLoopAbove >= 0)
				{
					nextGrainAsFraction = m_SynchronisedLoops[nearestLoopAbove].GetAverageValidGrainFraction();
				}
				else if (nearestLoopBelow >= 0)
				{
					nextGrainAsFraction = m_SynchronisedLoops[nearestLoopBelow].GetAverageValidGrainFraction();
				}

				m_defaultGranularSubmix.GenerateFrame(destBuffer, nextGrainAsFraction, currentRateHz, grainVolumeScale, currentGranularFraction, granuleFractionPerSample);
			}
			else
			{
				m_defaultGranularSubmix.m_LastVolumeScale = 0.0f;
			}
		}

		void CalculateNearestLoops(float currentRateHz, out int nearestLoopAboveOut, out int nearestLoopBelowOut, out float nearestLoopAboveVolumeScaleOut, out float nearestLoopBelowVolumeScaleOut)
		{
			int nearestLoopAbove = -1;
			float nearestLoopAboveXValue = 10000.0f;
			float nearestLoopAboveVolumeScale = 1.0f;
			int nearestLoopBelow = -1;
			float nearestLoopBelowXValue = -10000.0f;
			float nearestLoopBelowVolumeScale = 1.0f;

			for (int loop = 0; loop < m_SynchronisedLoops.Count(); loop++)
			{
				if (m_SynchronisedLoops[loop].IsEnabled())
				{
					float loopXValue = m_SynchronisedLoops[loop].GetAverageValidGrainHertz();

					if (loopXValue <= currentRateHz &&
						loopXValue > nearestLoopBelowXValue)
					{
						nearestLoopBelowXValue = loopXValue;
						nearestLoopBelow = loop;
					}
					else if (loopXValue > currentRateHz &&
						loopXValue < nearestLoopAboveXValue)
					{
						nearestLoopAboveXValue = loopXValue;
						nearestLoopAbove = loop;
					}
				}
			}

			// If we found a loop both above and below, we need to crossfade between them
			if (nearestLoopAbove >= 0 &&
				nearestLoopBelow >= 0)
			{
				float nearestLoopBelowVolumeScaleLin = 1.0f - (currentRateHz - m_SynchronisedLoops[nearestLoopBelow].GetAverageValidGrainHertz()) / (m_SynchronisedLoops[nearestLoopAbove].GetAverageValidGrainHertz() - m_SynchronisedLoops[nearestLoopBelow].GetAverageValidGrainHertz());

				// Apply a bias towards the loop below, on the basis that pitched up granular loops are more natural sounding than
				// pitched down ones. Initial tests seem good - seems to provide smoother pure-loop playback
				nearestLoopBelowVolumeScaleLin *= s_GranularLoopBelowBias;
				nearestLoopBelowVolumeScaleLin = GrainPlayer.Clamp(nearestLoopBelowVolumeScaleLin, 0.0f, 1.0f);

				if (m_CrossfadeStyle == audGrainCrossfadeStyle.CrossfadeStyleEqualPower)
				{
					const float PI_over_2 = (float)(Math.PI / 2.0f);
					nearestLoopAboveVolumeScale = (float)Math.Sin((1.0f - nearestLoopBelowVolumeScaleLin) * PI_over_2);
					nearestLoopBelowVolumeScale = (float)Math.Sin(nearestLoopBelowVolumeScaleLin * PI_over_2);
				}
				else
				{
					nearestLoopBelowVolumeScale = nearestLoopBelowVolumeScaleLin;
					nearestLoopAboveVolumeScale = 1.0f - nearestLoopBelowVolumeScaleLin;
				}
			}

			nearestLoopAboveVolumeScaleOut = nearestLoopAboveVolumeScale;
			nearestLoopBelowVolumeScaleOut = nearestLoopBelowVolumeScale;
			nearestLoopAboveOut = nearestLoopAbove;
			nearestLoopBelowOut = nearestLoopBelow;
		}
	}
}
