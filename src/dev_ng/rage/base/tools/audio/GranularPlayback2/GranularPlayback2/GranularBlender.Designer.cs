﻿namespace GranularPlayback2
{
	partial class GranularBlender
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button4 = new System.Windows.Forms.Button();
			this.loadSource1Btn = new System.Windows.Forms.Button();
			this.grainSource1Pic = new System.Windows.Forms.PictureBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.button2 = new System.Windows.Forms.Button();
			this.loadSource2Btn = new System.Windows.Forms.Button();
			this.grainSource2Pic = new System.Windows.Forms.PictureBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.button3 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.blendedGrainPic = new System.Windows.Forms.PictureBox();
			this.loadWaveFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.redrawTimer = new System.Windows.Forms.Timer(this.components);
			this.GenerateBlendBtn = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.grainSource1Pic)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.grainSource2Pic)).BeginInit();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.blendedGrainPic)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.button4);
			this.groupBox1.Controls.Add(this.loadSource1Btn);
			this.groupBox1.Controls.Add(this.grainSource1Pic);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(1004, 140);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Granular Source 1";
			// 
			// button4
			// 
			this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button4.Location = new System.Drawing.Point(881, 50);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(117, 25);
			this.button4.TabIndex = 2;
			this.button4.Text = "Play";
			this.button4.UseVisualStyleBackColor = true;
			// 
			// loadSource1Btn
			// 
			this.loadSource1Btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.loadSource1Btn.Location = new System.Drawing.Point(881, 19);
			this.loadSource1Btn.Name = "loadSource1Btn";
			this.loadSource1Btn.Size = new System.Drawing.Size(117, 25);
			this.loadSource1Btn.TabIndex = 1;
			this.loadSource1Btn.Text = "Load";
			this.loadSource1Btn.UseVisualStyleBackColor = true;
			this.loadSource1Btn.Click += new System.EventHandler(this.loadSource1Btn_Click);
			// 
			// grainSource1Pic
			// 
			this.grainSource1Pic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.grainSource1Pic.Location = new System.Drawing.Point(6, 20);
			this.grainSource1Pic.Name = "grainSource1Pic";
			this.grainSource1Pic.Size = new System.Drawing.Size(869, 114);
			this.grainSource1Pic.TabIndex = 0;
			this.grainSource1Pic.TabStop = false;
			this.grainSource1Pic.Paint += new System.Windows.Forms.PaintEventHandler(this.grainSource1Pic_Paint);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.button2);
			this.groupBox2.Controls.Add(this.loadSource2Btn);
			this.groupBox2.Controls.Add(this.grainSource2Pic);
			this.groupBox2.Location = new System.Drawing.Point(12, 304);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(1004, 143);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Granular Source 2";
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.Location = new System.Drawing.Point(881, 50);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(117, 25);
			this.button2.TabIndex = 4;
			this.button2.Text = "Play";
			this.button2.UseVisualStyleBackColor = true;
			// 
			// loadSource2Btn
			// 
			this.loadSource2Btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.loadSource2Btn.Location = new System.Drawing.Point(881, 19);
			this.loadSource2Btn.Name = "loadSource2Btn";
			this.loadSource2Btn.Size = new System.Drawing.Size(117, 25);
			this.loadSource2Btn.TabIndex = 3;
			this.loadSource2Btn.Text = "Load";
			this.loadSource2Btn.UseVisualStyleBackColor = true;
			this.loadSource2Btn.Click += new System.EventHandler(this.loadSource2Btn_Click);
			// 
			// grainSource2Pic
			// 
			this.grainSource2Pic.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.grainSource2Pic.Location = new System.Drawing.Point(6, 19);
			this.grainSource2Pic.Name = "grainSource2Pic";
			this.grainSource2Pic.Size = new System.Drawing.Size(869, 114);
			this.grainSource2Pic.TabIndex = 1;
			this.grainSource2Pic.TabStop = false;
			this.grainSource2Pic.Paint += new System.Windows.Forms.PaintEventHandler(this.grainSource2Pic_Paint);
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.GenerateBlendBtn);
			this.groupBox3.Controls.Add(this.button3);
			this.groupBox3.Controls.Add(this.button5);
			this.groupBox3.Controls.Add(this.blendedGrainPic);
			this.groupBox3.Location = new System.Drawing.Point(12, 158);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(1004, 140);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Blended Output";
			// 
			// button3
			// 
			this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button3.Location = new System.Drawing.Point(881, 81);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(117, 25);
			this.button3.TabIndex = 4;
			this.button3.Text = "Play";
			this.button3.UseVisualStyleBackColor = true;
			// 
			// button5
			// 
			this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button5.Location = new System.Drawing.Point(881, 50);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(117, 25);
			this.button5.TabIndex = 3;
			this.button5.Text = "Save";
			this.button5.UseVisualStyleBackColor = true;
			// 
			// blendedGrainPic
			// 
			this.blendedGrainPic.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.blendedGrainPic.Location = new System.Drawing.Point(6, 20);
			this.blendedGrainPic.Name = "blendedGrainPic";
			this.blendedGrainPic.Size = new System.Drawing.Size(869, 114);
			this.blendedGrainPic.TabIndex = 1;
			this.blendedGrainPic.TabStop = false;
			this.blendedGrainPic.Paint += new System.Windows.Forms.PaintEventHandler(this.blendedGrainPic_Paint);
			// 
			// loadWaveFileDialog
			// 
			this.loadWaveFileDialog.Filter = "Wave files (*.wav)|*.wav";
			this.loadWaveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.loadWaveFileDialog_FileOk);
			// 
			// redrawTimer
			// 
			this.redrawTimer.Enabled = true;
			this.redrawTimer.Interval = 10;
			this.redrawTimer.Tick += new System.EventHandler(this.redrawTimer_Tick);
			// 
			// GenerateBlendBtn
			// 
			this.GenerateBlendBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.GenerateBlendBtn.Location = new System.Drawing.Point(881, 19);
			this.GenerateBlendBtn.Name = "GenerateBlendBtn";
			this.GenerateBlendBtn.Size = new System.Drawing.Size(117, 25);
			this.GenerateBlendBtn.TabIndex = 5;
			this.GenerateBlendBtn.Text = "Generate Blend";
			this.GenerateBlendBtn.UseVisualStyleBackColor = true;
			this.GenerateBlendBtn.Click += new System.EventHandler(this.GenerateBlendBtn_Click);
			// 
			// GranularBlender
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1028, 459);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "GranularBlender";
			this.Text = "Grain Blender";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.window_Closed);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.grainSource1Pic)).EndInit();
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.grainSource2Pic)).EndInit();
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.blendedGrainPic)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button loadSource1Btn;
		private System.Windows.Forms.PictureBox grainSource1Pic;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button loadSource2Btn;
		private System.Windows.Forms.PictureBox grainSource2Pic;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.PictureBox blendedGrainPic;
		private System.Windows.Forms.OpenFileDialog loadWaveFileDialog;
		private System.Windows.Forms.Timer redrawTimer;
		private System.Windows.Forms.Button GenerateBlendBtn;
	}
}