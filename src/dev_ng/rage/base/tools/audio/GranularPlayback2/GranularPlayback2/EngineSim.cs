﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Wavelib;
using System.IO;
using System.Xml.Serialization;

namespace GranularPlayback2
{
	public partial class EngineSim : Form
	{
		GrainPlayer m_GrainPlayer;
		int m_LastLoadButtonClicked;
		int[] m_SubmixIndexes = new int[4];
		Bitmap[] m_WaveImages = new Bitmap[4];
		PictureBox[] m_PictureBoxes = new PictureBox[4];
		GroupBox[] m_GroupBoxes = new GroupBox[4];
		float[] m_Xscale = new float[4];

		private float m_Xzoom = 1.0f;
		private float m_Yscale = 1.0f;
		private float m_Yzoom = 1.0f;
		private int m_Xoffset = 0;

		Thread m_EngineSimThread;		
		float m_CurrentGamepadThrottle;
		float m_CurrentRevs = 0.0f;
		float m_PrevRevs = 0.0f;
		float m_RevChangeFactor = 400.0f;
		float m_LastThrottleValue = 0.0f;
		int m_MaxRPMValues = 100;
		bool m_PlayingTestSweep = false;
		int m_TestSweepIndex = 0;

		List<float> m_TestSweepThrottle;
		List<float> m_TestSweepXVal;
		List<float> m_RPMValues;

		public EngineSim()
		{
			InitializeComponent();			
			m_GrainPlayer = new GrainPlayer(this);
			m_GrainPlayer.m_GrainPlaybackStyle = GrainPlayer.audGrainPlaybackStyle.PlaybackStyleLoopsAndGrains;

			for(int loop = 0; loop < m_SubmixIndexes.Length; loop++)
			{
				m_SubmixIndexes[loop] = -1;
				m_WaveImages[loop] = new Bitmap(slot1WaveView.Width, slot1WaveView.Height);
			}

			m_PictureBoxes[0] = slot1WaveView;
			m_PictureBoxes[1] = slot2WaveView;
			m_PictureBoxes[2] = slot3WaveView;
			m_PictureBoxes[3] = slot4WaveView;

			m_GroupBoxes[0] = groupBox1;
			m_GroupBoxes[1] = groupBox2;
			m_GroupBoxes[2] = groupBox3;
			m_GroupBoxes[3] = groupBox4;

			for (int loop = 0; loop < 4; loop++)
			{
				m_Xscale[loop] = 1.0f;
			}

			m_TestSweepXVal = new List<float>();
			m_TestSweepThrottle = new List<float>();

			m_RPMValues = new List<float>();
			m_EngineSimThread = new Thread(new ThreadStart(ThreadedEngineSim));
			m_EngineSimThread.Start();
		}

		private void window_Closed(object sender, FormClosedEventArgs e)
		{
			m_GrainPlayer.Kill();
			m_EngineSimThread.Abort();
		}

		private void slot1Load_Click(object sender, EventArgs e)
		{
			m_LastLoadButtonClicked = 0;
			loadWaveFileDialog.ShowDialog();
		}

		private void slot2Load_Click(object sender, EventArgs e)
		{
			m_LastLoadButtonClicked = 1;
			loadWaveFileDialog.ShowDialog();
		}

		private void slot3Load_Click(object sender, EventArgs e)
		{
			m_LastLoadButtonClicked = 2;
			loadWaveFileDialog.ShowDialog();
		}

		private void slot4Load_Click(object sender, EventArgs e)
		{
			m_LastLoadButtonClicked = 3;
			loadWaveFileDialog.ShowDialog();
		}

		private void loadWaveFileDialog_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;

			if (dialog != null)
			{
				string waveFileName = dialog.FileName;
				string grainFileName = waveFileName.Replace(Path.GetExtension(waveFileName), ".grn");

				m_GrainPlayer.InitMix(waveFileName, grainFileName);
				m_GrainPlayer.GetGranularMix(m_GrainPlayer.GetNumGranularMixes() - 1).GetGranularSubmix().m_SlidingGrainWindowSize = (float)granularWindowSizeUpDown.Value;
				m_GrainPlayer.GetGranularMix(m_GrainPlayer.GetNumGranularMixes() - 1).GetGranularSubmix().m_MinGrainRepeateRate = (int)minRepeatRateUpDown.Value;

				m_SubmixIndexes[m_LastLoadButtonClicked] = m_GrainPlayer.GetNumGranularMixes() - 1;
				RenderWaveView(m_SubmixIndexes[m_LastLoadButtonClicked], m_LastLoadButtonClicked);
				m_PictureBoxes[m_LastLoadButtonClicked].Image = m_WaveImages[m_LastLoadButtonClicked];
				m_PictureBoxes[m_LastLoadButtonClicked].Refresh();

				m_GroupBoxes[m_LastLoadButtonClicked].Text = dialog.FileName;
			}
		}

		private void WaveViewOnPaint(Graphics g, int submixIndex)
		{
			if (m_GrainPlayer.m_IsPlaying)
			{
				List<GranularSubmix.audGrainData> grainData = m_GrainPlayer.GetGranularMix(submixIndex).GetGranularSubmix().m_GrainTable;

				int grainIndex = 0;
				float volumeScale = 1.0f;

				if (m_GrainPlayer.m_GrainPlaybackStyle == GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug)
				{
					grainIndex = m_GrainPlayer.GetGranularMix(submixIndex).GetDebugSubmix().m_PrevSelectedGrain;
				}
				else
				{
					grainIndex = m_GrainPlayer.GetGranularMix(submixIndex).GetGranularSubmix().m_PrevSelectedGrain;
					volumeScale = m_GrainPlayer.GetGranularMix(submixIndex).GetGranularSubmix().m_LastVolumeScale;
				}

				if (grainIndex + 1 < grainData.Count)
				{
					g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb((int)(200 * volumeScale), 0, 255, 0)),
						(grainData[grainIndex].startSample - m_Xoffset) * m_Xscale[submixIndex],
						0,
						(grainData[grainIndex + 1].startSample - grainData[grainIndex].startSample) * m_Xscale[submixIndex],
						m_PictureBoxes[submixIndex].Height);
				}

				if (m_GrainPlayer.m_GrainPlaybackStyle != GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug)
				{
					for (int loop = 0; loop < m_GrainPlayer.GetGranularMix(submixIndex).GetNumSynchronisedLoops(); loop++)
					{
						grainIndex = m_GrainPlayer.GetGranularMix(submixIndex).GetSynchronisedLoop(loop).m_PrevSelectedGrain;
						volumeScale = m_GrainPlayer.GetGranularMix(submixIndex).GetSynchronisedLoop(loop).m_LastVolumeScale;

						if (grainIndex + 1 < grainData.Count)
						{
							g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb((int)(200 * volumeScale), 0, 0, 255)),
								(grainData[grainIndex].startSample - m_Xoffset) * m_Xscale[submixIndex],
								0,
								(grainData[grainIndex + 1].startSample - grainData[grainIndex].startSample) * m_Xscale[submixIndex],
								m_PictureBoxes[submixIndex].Height);
						}
					}
				}
			}
		}

		private void RenderWaveView(int submixIndex, int waveSlotIndex)
		{
			Graphics g = Graphics.FromImage(m_WaveImages[waveSlotIndex]);

			Int32 lastx = 0, lasty = 0;
			var gp = new GraphicsPath();
			var p = Pens.DarkBlue;

			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(submixIndex).m_WaveData;
			List<GranularSubmix.audGrainData> grainData = m_GrainPlayer.GetGranularMix(submixIndex).GetGranularSubmix().m_GrainTable;

			// scale x so that the entire sample fits in the visbounds
			m_Xscale[waveSlotIndex] = (m_WaveImages[waveSlotIndex].Width / (float)waveData.Data.NumSamples) * m_Xzoom;

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
							0,
							0,
							m_WaveImages[waveSlotIndex].Width,
							m_WaveImages[waveSlotIndex].Height);

			// Default step rate
			int stepRate = 30;

			float zoomPercentage = (m_Xzoom - 100) / 100.0f;

			if (zoomPercentage > 1.0f)
				zoomPercentage = 1.0f;

			if (zoomPercentage < 0.0f)
				zoomPercentage = 0.0f;

			// As we zoom in more, increase the accuracy
			stepRate -= (int)((stepRate - 1) * zoomPercentage);

			for (var i = m_Xoffset; i < waveData.Data.NumSamples; i += stepRate)
			{
				float sample = waveData.Data.ChannelData[0, i];

				// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
				sample *= -1.0f;
				sample += 1.0f;
				sample /= 2.0f;

				// now 0 -> 1.0f
				var y = (Int32)(sample * m_WaveImages[waveSlotIndex].Height);
				var x = i;

				x = (Int32)((x - m_Xoffset) * m_Xscale[waveSlotIndex]);

				y = (Int32)(y * m_Yscale);

				if (x > m_WaveImages[waveSlotIndex].Width &&
				   lastx > m_WaveImages[waveSlotIndex].Width)
				{
					break;
				}

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
					//g.DrawLine(p, lastx, lasty, x, y);
				}

				lastx = x;
				lasty = y;
			}

			// draw path
			g.DrawPath(p, gp);

			for (var grainLoop = 0; grainLoop < grainData.Count; grainLoop++)
			{
				var x = (Int32)((grainData[grainLoop].startSample - m_Xoffset) * m_Xscale[waveSlotIndex]);

				if (x > m_WaveImages[waveSlotIndex].Width)
				{
					break;
				}

				g.DrawLine(Pens.HotPink, x, m_WaveImages[waveSlotIndex].Height, x, 0);
			}

			for (int loop = 0; loop < m_GrainPlayer.GetGranularMix(submixIndex).GetNumSynchronisedLoops(); loop++)
			{
				GranularSubmix syncLoop = m_GrainPlayer.GetGranularMix(submixIndex).GetSynchronisedLoop(loop);

				for (int grainLoop = 0; grainLoop < syncLoop.m_ValidGrains.Count; grainLoop++)
				{
					if (syncLoop.m_ValidGrains[grainLoop] + 1 < grainData.Count)
					{
						g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 0, 0, 255)),
						(grainData[syncLoop.m_ValidGrains[grainLoop]].startSample - m_Xoffset) * m_Xscale[waveSlotIndex],
						0,
						(grainData[syncLoop.m_ValidGrains[grainLoop] + 1].startSample - grainData[syncLoop.m_ValidGrains[grainLoop]].startSample) * m_Xscale[waveSlotIndex],
						m_WaveImages[waveSlotIndex].Height);
					}
				}
			}

			// draw x axis
			g.DrawLine(p, 0, m_WaveImages[waveSlotIndex].Height / 2, m_WaveImages[waveSlotIndex].Width, m_WaveImages[waveSlotIndex].Height / 2);

			// draw a line to show the end of the wave
			g.DrawLine(p, lastx, 0, lastx, m_WaveImages[waveSlotIndex].Height);

			var zoom = (int)(m_Xzoom * 100);
		}

		private void redrawTimer_Tick(object sender, EventArgs e)
		{
			engineSimView.Refresh();
			slot1WaveView.Refresh();
			slot2WaveView.Refresh();
			slot3WaveView.Refresh();
			slot4WaveView.Refresh();
		}

		private void slot1WaveView_Paint(object sender, PaintEventArgs e)
		{
			if(m_SubmixIndexes[0] != -1)
			{
				WaveViewOnPaint(e.Graphics, m_SubmixIndexes[0]);
			}
		}

		private void slot2WaveView_Paint(object sender, PaintEventArgs e)
		{
			if (m_SubmixIndexes[1] != -1)
			{
				WaveViewOnPaint(e.Graphics, m_SubmixIndexes[1]);
			}
		}

		private void slot3WaveView_Paint(object sender, PaintEventArgs e)
		{
			if (m_SubmixIndexes[2] != -1)
			{
				WaveViewOnPaint(e.Graphics, m_SubmixIndexes[2]);
			}
		}

		private void slot4WaveView_Paint(object sender, PaintEventArgs e)
		{
			if (m_SubmixIndexes[3] != -1)
			{
				WaveViewOnPaint(e.Graphics, m_SubmixIndexes[3]);
			}
		}

		private void engineSimView_Paint(object sender, PaintEventArgs e)
		{
			var p = new Pen(new SolidBrush(Color.DarkBlue), 1.0f);

			var g = e.Graphics;
			var visBounds = g.VisibleClipBounds;

			int lastx = 0, lasty = 0;
			var gp = new GraphicsPath();

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(240, 240, 255)),
							0,
							0,
							visBounds.Width,
							visBounds.Height);

			p = Pens.DarkBlue;

			for (var i = 0; i < m_MaxRPMValues && i < m_RPMValues.Count; i++)
			{
				var y = visBounds.Height * (1.0f - (m_RPMValues[i] / 10000));
				var x = visBounds.Width * (i / (float)m_MaxRPMValues);

				if (x > visBounds.Width &&
				   lastx > visBounds.Width)
				{
					break;
				}

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
				}

				lastx = (int)x;
				lasty = (int)y;
			}

			// draw path
			g.DrawPath(p, gp);

			Font arialBold = new Font(new FontFamily("Arial"), 20, FontStyle.Bold);
			SolidBrush textBrush = new SolidBrush(Color.Black);
			g.DrawString(((int)m_CurrentRevs).ToString() + " RPM", arialBold, textBrush, new PointF(0.0f, 0.0f));

			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			float changeRate = m_CurrentRevs - m_PrevRevs;
			float chageRatePerSec = changeRate / 0.016f;
			string changeRateString = String.Format("{0:0.#}", chageRatePerSec);

			if (changeRate > 0)
			{
				changeRateString = "+" + changeRateString;
			}

			g.DrawString("Change Rate: " + changeRateString, arial, textBrush, new PointF(0.0f, 30.0f));
		}

		private void ThreadedEngineSim()
		{
			while (true)
			{
				if (m_GrainPlayer.GetNumGranularMixes() > 0)
				{
					float maxRPM = 0;
					float minRPM = 0;

					if (m_GrainPlayer.GetNumGranularMixes() > 0)
					{
						maxRPM = Math.Max(m_GrainPlayer.m_HertzStart, m_GrainPlayer.m_HertzEnd) * 120.0f;
						minRPM = Math.Min(m_GrainPlayer.m_HertzStart, m_GrainPlayer.m_HertzEnd) * 120.0f;
					}

					m_PrevRevs = m_CurrentRevs;

					if (m_PlayingTestSweep)
					{
						m_CurrentRevs = minRPM + ((maxRPM - minRPM) * m_TestSweepXVal[m_TestSweepIndex]);
						m_GrainPlayer.SetXValue(m_TestSweepXVal[m_TestSweepIndex], 0.033f);

						float accelVolume = 0.2f + (0.8f * m_TestSweepThrottle[m_TestSweepIndex]);
						float decelVolume = 1.0f - accelVolume;
						m_GrainPlayer.m_AccelVolume = accelVolume;
						m_GrainPlayer.m_DecelVolume = decelVolume;


						m_TestSweepIndex++;

						if (m_TestSweepIndex >= m_TestSweepXVal.Count)
						{
							m_TestSweepIndex = 0;
						}
					}
					else
					{
						if (throttleSlider.Value > 100)
						{
							throttleSlider.Value = 100;
						}

						float newThrottleValue = (float)(100.0f - throttleSlider.Value) / 100.0f;
						float throttleValue = m_LastThrottleValue + ((newThrottleValue - m_LastThrottleValue) * 0.1f);
						float inertiaValue = (float)(100.0f - intertiaSlider.Value) / 100.0f;

						float desiredRevs = minRPM + ((maxRPM - minRPM) * throttleValue);
						float oldRevs = m_CurrentRevs;
						m_LastThrottleValue = throttleValue;

						if (m_CurrentRevs < desiredRevs)
						{
							m_CurrentRevs += inertiaValue * m_RevChangeFactor;

							if (m_CurrentRevs > desiredRevs)
							{
								m_CurrentRevs = desiredRevs;
							}
						}
						else if (m_CurrentRevs > desiredRevs)
						{
							m_CurrentRevs -= inertiaValue * m_RevChangeFactor;

							if (m_CurrentRevs < desiredRevs)
							{
								m_CurrentRevs = desiredRevs;
							}
						}

						if (m_CurrentRevs < minRPM)
						{
							m_CurrentRevs = minRPM;
						}
						if (m_CurrentRevs > maxRPM)
						{
							m_CurrentRevs = maxRPM;
						}

						float currentXValue = (m_CurrentRevs - minRPM) / (maxRPM - minRPM);
						m_GrainPlayer.SetXValue(currentXValue, 0.033f);

						float accelVolume = 0.2f + (0.8f * throttleValue);
						float decelVolume = 1.0f - accelVolume;
						m_GrainPlayer.m_AccelVolume = accelVolume;
						m_GrainPlayer.m_DecelVolume = decelVolume;
					}

					m_RPMValues.Add(m_CurrentRevs);

					if (m_RPMValues.Count > m_MaxRPMValues)
					{
						m_RPMValues.RemoveAt(0);
					}
				}

				Thread.Sleep(33);
			}
		}

		private void simulateEngineButton_Click(object sender, EventArgs e)
		{
			if (simulateEngineButton.Text == "Stop")
			{
				m_GrainPlayer.m_IsPlaying = false;
				simulateEngineButton.Text = "Play";
			}
			else
			{
				m_GrainPlayer.m_IsPlaying = true;
				simulateEngineButton.Text = "Stop";
				playTestSweepButton.Text = "Play Test Sweep";
				m_PlayingTestSweep = false;
			}
		}

		private void playTestSweepButton_Click(object sender, EventArgs e)
		{
			if (m_GrainPlayer.m_IsPlaying && m_PlayingTestSweep)
			{
				m_PlayingTestSweep = false;
				m_GrainPlayer.m_IsPlaying = false;
			}
			else
			{
				if (m_TestSweepXVal.Count == 0)
				{
					string fileName = "testSweep.swp";

					if (System.IO.File.Exists(fileName))
					{
						System.IO.StreamReader textReader = new System.IO.StreamReader(fileName);
						string line;

						while ((line = textReader.ReadLine()) != null)
						{
							m_TestSweepThrottle.Add(Convert.ToSingle(line));

							line = textReader.ReadLine();
							m_TestSweepXVal.Add(Convert.ToSingle(line));
						}

						textReader.Close();
					}
				}

				if (m_TestSweepXVal.Count > 0)
				{
					if(playTestSweepButton.Text == "Stop")
					{
						m_GrainPlayer.m_IsPlaying = false;
						playTestSweepButton.Text = "Play Test Sweep";
					}
					else
					{
						playTestSweepButton.Text = "Stop";
						simulateEngineButton.Text = "Play";
						m_GrainPlayer.m_GrainPlaybackStyle = (GrainPlayer.audGrainPlaybackStyle)playbackStyleCombo.SelectedIndex;
						m_PlayingTestSweep = true;
						m_GrainPlayer.m_IsPlaying = true;
						m_TestSweepIndex = 0;
					}
				}
			}
		}

		private void pitchScaleUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_PitchScale = (float)pitchScaleUpDown.Value;
		}

		private void granularWindowSizeUpDown_ValueChanged(object sender, EventArgs e)
		{
			for(int loop = 0; loop < m_GrainPlayer.GetNumGranularMixes(); loop++)
			{
				m_GrainPlayer.GetGranularMix(loop).GetGranularSubmix().m_SlidingGrainWindowSize = (float)granularWindowSizeUpDown.Value;
			}
		}

		private void minRepeatRateUpDown_ValueChanged(object sender, EventArgs e)
		{
			for (int loop = 0; loop < m_GrainPlayer.GetNumGranularMixes(); loop++)
			{
				m_GrainPlayer.GetGranularMix(loop).GetGranularSubmix().m_MinGrainRepeateRate = (int)minRepeatRateUpDown.Value;
			}
		}

		private void MinGranularOffset_ValueChanged(object sender, EventArgs e)
		{
			for (int loop = 0; loop < m_GrainPlayer.GetNumGranularMixes(); loop++)
			{
				m_GrainPlayer.GetGranularMix(loop).GetGranularSubmix().m_MinGranularWindowOffset = (int)MinGranularOffset.Value;
			}
		}

		private void MaxGranularOffset_ValueChanged(object sender, EventArgs e)
		{
			for (int loop = 0; loop < m_GrainPlayer.GetNumGranularMixes(); loop++)
			{
				m_GrainPlayer.GetGranularMix(loop).GetGranularSubmix().m_MaxGranularWindowOffset = (int)MaxGranularOffset.Value;
			}
		}

		private void label5_Click(object sender, EventArgs e)
		{

		}

		private void label10_Click(object sender, EventArgs e)
		{

		}
	}
}
