﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Xml.Serialization;

using Wavelib;

namespace GranularPlayback2
{
	public partial class GrainView : Form
	{
		GrainPlayer m_GrainPlayer;
		Bitmap m_WaveImage;
		Bitmap m_AxisImage;
		string m_CurrentFileName;
		bool m_FileLoaded = false;

		private float m_Xscale = 1.0f;
		private float m_Xzoom = 1.0f;
		private float m_Yscale = 1.0f;
		private float m_Yzoom = 1.0f;
		private int m_Xoffset = 0;
		private int m_PlaybackCursor = 0;
		private List<int> m_SelectedGrains;
		private int m_SelectedLoop = -1;

		private bool m_LeftMouseDown = false;
		private int m_LeftMouseXStart = 0;
		private bool m_MouseOverWaveView = false;

		struct GranularVolumeInfo
		{
			public float m_RPM;
			public float m_Volume;
		}

		enum GranularPitchStyle
		{
			PitchStyleNative,
			PitchStyleFlattened
		}

		GranularPitchStyle m_PitchStyle = GranularPitchStyle.PitchStyleNative;
		Thread m_EngineSimThread;
		float m_CurrentGamepadThrottle;
		float m_CurrentRevs = 0.0f;
		float m_PrevRevs = 0.0f;
		float m_RevChangeFactor = 400.0f;
		float m_LastThrottleValue = 0.0f;
		int m_MaxRPMValues = 100;
		bool m_SkipSaveGrainPrompt = false;
		
		List<float> m_RPMValues;
		List<GranularVolumeInfo> m_GranularVolumes;
		List<List<float>> m_LoopVolumes;
		public Point m_LastMousePoint;
		bool m_GrainsHaveBeenSaved;

		public GrainView(string waveFile, string grainFile, bool grainsHaveBeenSaved)
		{
			m_SkipSaveGrainPrompt = false;
			m_GrainsHaveBeenSaved = grainsHaveBeenSaved;
			m_GrainPlayer = new GrainPlayer(this);
			m_GrainPlayer.m_VolumeScale = 0.7f;

			InitializeComponent();

			m_WaveImage = new Bitmap(waveView.Width, waveView.Height);
			waveView.Image = m_WaveImage;

			m_AxisImage = new Bitmap(waveViewAxis.Width, waveViewAxis.Height);
			waveViewAxis.Image = m_AxisImage;

			m_SelectedGrains = new List<int>();

			m_RPMValues = new List<float>();
			m_LoopVolumes = new List<List<float>>();

			for (int loop = 0; loop < 20; loop++)
			{
				m_LoopVolumes.Add(new List<float>());

				for (int loop2 = 0; loop2 < m_MaxRPMValues; loop2++)
				{
					m_LoopVolumes[loop].Add(0.0f);
				}
			}

			m_GranularVolumes = new List<GranularVolumeInfo>();
			m_EngineSimThread = new Thread(new ThreadStart(ThreadedEngineSim));
			m_EngineSimThread.Start();

			m_GrainPlayer.InitMix(waveFile, grainFile);
			m_FileLoaded = true;
			RenderWaveView();
			RenderWaveAxis();
			//loopGridView.DataSource = GetLoopDataTable();

			//for (int loop = 0; loop < loopGridView.Columns.Count; loop++)
			//{
				//loopGridView.Columns[loop].Width = 72;
			//}
			InitLoopTable();
			PopulateLoopTable();

			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(0).m_WaveData;
			waveScrollBar.Minimum = 0;
			waveScrollBar.Maximum = 0;
			waveScrollBar.Value = m_Xoffset;
			this.Text = "Grain Viewer - " + waveFile;

			grainSelectionStyle.SelectedIndex = 2;
		}

		public void HideRecalculateGrainButton()
		{
			recalcGrainBtn.Enabled = false;
		}

		void InitLoopTable()
		{
			DataGridViewTextBoxColumn loopColumn = new DataGridViewTextBoxColumn();
			loopColumn.HeaderText = "Loop";
			loopColumn.ReadOnly = true;
			loopGridView.Columns.Add(loopColumn);

			DataGridViewTextBoxColumn rpmColumn = new DataGridViewTextBoxColumn();
			rpmColumn.HeaderText = "RPM";
			rpmColumn.ReadOnly = true;
			loopGridView.Columns.Add(rpmColumn);

			DataGridViewComboBoxColumn comboBoxColumm = new DataGridViewComboBoxColumn();
			comboBoxColumm.HeaderText = "Style";

			for (int loop = 0; loop <= (int)GranularSubmix.audGrainPlaybackOrder.Mixed; loop++)
			{
				comboBoxColumm.Items.Add(((GranularSubmix.audGrainPlaybackOrder)loop).ToString());
			}

			loopGridView.Columns.Add(comboBoxColumm);

			DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
			checkBoxColumn.HeaderText = "Enabled";
			loopGridView.Columns.Add(checkBoxColumn);

			DataGridViewTextBoxColumn idColumn = new DataGridViewTextBoxColumn();
			idColumn.HeaderText = "Identifier";
			loopGridView.Columns.Add(idColumn);

			loopGridView.Columns[0].Width = 48;
			loopGridView.Columns[1].Width = 48;
			loopGridView.Columns[2].Width = 90;
			loopGridView.Columns[3].Width = 60;
			loopGridView.Columns[4].Width = 90;
			loopGridView.EditingControlShowing += loopGridView_EditingControlShowing;
			loopGridView.CurrentCellDirtyStateChanged += loopGridView_CurrentCellDirtyStateChanged;
		}

		private void loopGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
		{
			if(loopGridView.IsCurrentCellDirty)
			{
				if(loopGridView.CurrentCell.ColumnIndex == 3)
				{
					string value = loopGridView[0, loopGridView.CurrentCell.RowIndex].Value.ToString();
					m_SelectedLoop = System.Convert.ToInt32(value) - 1;

					DataGridViewCheckBoxCell checkCell = loopGridView[loopGridView.CurrentCell.ColumnIndex, loopGridView.CurrentCell.RowIndex] as DataGridViewCheckBoxCell;
					bool enabled = (bool)checkCell.EditingCellFormattedValue;
					m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(m_SelectedLoop).m_Enabled = enabled;
				}
				else if (loopGridView.CurrentCell.ColumnIndex == 4)
				{
					string value = loopGridView[0, loopGridView.CurrentCell.RowIndex].Value.ToString();
					m_SelectedLoop = System.Convert.ToInt32(value) - 1;

					DataGridViewCell cell = loopGridView[loopGridView.CurrentCell.ColumnIndex, loopGridView.CurrentCell.RowIndex] as DataGridViewCell;
					m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(m_SelectedLoop).SetSubmixIdentifier((string)cell.EditedFormattedValue);
				}

				loopGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
			}
		}

		private void loopGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (loopGridView.CurrentCell.ColumnIndex == 2 && e.Control is ComboBox)
			{
				ComboBox cmbox = e.Control as ComboBox;
				cmbox.SelectedValueChanged -= new EventHandler(cmbox_SelectedValueChanged);
				cmbox.SelectedValueChanged += new EventHandler(cmbox_SelectedValueChanged);
			}
		}

		private void cmbox_SelectedValueChanged(object sender, EventArgs e)
		{
			DataGridViewComboBoxEditingControl editingControl = sender as DataGridViewComboBoxEditingControl;
			string newStyle = (string)editingControl.EditingControlFormattedValue;
			string selectedLoopString = loopGridView[0, loopGridView.CurrentRow.Index].Value.ToString();
			int selectedLoop = System.Convert.ToInt32(selectedLoopString) - 1;

			GranularSubmix.audGrainPlaybackOrder newOrder = (GranularSubmix.audGrainPlaybackOrder)Enum.Parse(typeof(GranularSubmix.audGrainPlaybackOrder), newStyle);
			m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(selectedLoop).m_PlaybackOrder = newOrder;
		}

		void PopulateLoopTable()
		{
			loopGridView.Rows.Clear();

			for (int loop = 0; loop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); loop++)
			{
				loopGridView.Rows.Add(1);

				GranularSubmix submix = m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(loop);
				loopGridView.Rows[loop].Cells[0].Value = (loop + 1).ToString();
				loopGridView.Rows[loop].Cells[1].Value = (int)submix.GetAverageValidGrainHertz() * 120;
				loopGridView.Rows[loop].Cells[2].Value = submix.m_PlaybackOrder.ToString();
				loopGridView.Rows[loop].Cells[3].Value = submix.m_Enabled;
				loopGridView.Rows[loop].Cells[4].Value = submix.m_SubmixIdentifierString;
			}
		}

		private void loadToolStripMenuItem_Click(object sender, EventArgs e)
		{

		}

		private void window_Closed(object sender, FormClosedEventArgs e)
		{
			if (!m_SkipSaveGrainPrompt)
			{
				if (MessageBox.Show("Would you like to save loop/grain data before exiting?", "Save grain data", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					SaveGrainData(m_GrainPlayer.GetGranularMix(0).m_WaveData.FileName, true);
				}
			}

			m_GrainPlayer.Kill();
			m_EngineSimThread.Abort();
		}

		private void granularDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			loadWaveFileDialog.ShowDialog();
		}

		private void loadWaveFileDialog_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;

			if (dialog != null)
			{
			}
		}

		private void ThreadedEngineSim()
		{
			while(true)
			{
				if(m_FileLoaded)
				{
					m_PrevRevs = m_CurrentRevs;

					if (throttleSlider.Value > 100)
					{
						throttleSlider.Value = 100;
					}

					float newThrottleValue = (float)(100.0f - throttleSlider.Value) / 100.0f;
					float throttleValue = m_LastThrottleValue + ((newThrottleValue - m_LastThrottleValue) * 0.1f);
					float inertiaValue = (float)(100.0f - intertiaSlider.Value) / 100.0f;

					float maxRPM = Math.Max(m_GrainPlayer.m_HertzStart, m_GrainPlayer.m_HertzEnd) * 120.0f;
					float minRPM = Math.Min(m_GrainPlayer.m_HertzStart, m_GrainPlayer.m_HertzEnd) * 120.0f;

					float desiredRevs = minRPM + ((maxRPM - minRPM) * throttleValue);
					float oldRevs = m_CurrentRevs;
					m_LastThrottleValue = throttleValue;

					if (m_CurrentRevs < desiredRevs)
					{
						m_CurrentRevs += inertiaValue * m_RevChangeFactor;

						if (m_CurrentRevs > desiredRevs)
						{
							m_CurrentRevs = desiredRevs;
						}
					}
					else if (m_CurrentRevs > desiredRevs)
					{
						m_CurrentRevs -= inertiaValue * m_RevChangeFactor;

						if (m_CurrentRevs < desiredRevs)
						{
							m_CurrentRevs = desiredRevs;
						}
					}

					if (m_CurrentRevs < minRPM)
					{
						m_CurrentRevs = minRPM;
					}
					if (m_CurrentRevs > maxRPM)
					{
						m_CurrentRevs = maxRPM;
					}

					float currentXValue = (m_CurrentRevs - minRPM) / (maxRPM - minRPM);
					m_GrainPlayer.SetXValue(currentXValue, 0.033f);
					
					m_RPMValues.Add(m_CurrentRevs);

					if (m_RPMValues.Count > m_MaxRPMValues)
					{
						m_RPMValues.RemoveAt(0);
					}

					GranularVolumeInfo granularVolumeInfo;
					granularVolumeInfo.m_RPM = m_CurrentRevs;
					granularVolumeInfo.m_Volume = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_LastVolumeScale;

					m_GranularVolumes.Add(granularVolumeInfo);

					if (m_GranularVolumes.Count > m_MaxRPMValues)
					{
						m_GranularVolumes.RemoveAt(0);
					}

					for (int loop = 0; loop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); loop++)
					{
						m_LoopVolumes[loop].Add(m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(loop).m_LastVolumeScale);

						if (m_LoopVolumes[loop].Count > m_MaxRPMValues)
						{
							m_LoopVolumes[loop].RemoveAt(0);
						}
					}
				}

				Thread.Sleep(33);
			}
		}

		private void engineSimView_Paint(object sender, PaintEventArgs e)
		{
			if (!m_FileLoaded)
				return;

			var p = new Pen(new SolidBrush(Color.DarkBlue), 1.0f);

			var g = e.Graphics;
			var visBounds = g.VisibleClipBounds;

			int lastx = 0, lasty = 0;
			var gp = new GraphicsPath();

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(255, 255, 255)),
							0,
							0,
							visBounds.Width,
							visBounds.Height);

			p = Pens.DarkBlue;			
			PictureBox picBox = sender as System.Windows.Forms.PictureBox;
			g.DrawRectangle(p, new Rectangle(0, 0, picBox.Width - 1, picBox.Height - 1));

			for (int loop = 0; loop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); loop++)
			{
				var y = visBounds.Height * (1.0f - ((m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(loop).GetAverageValidGrainHertz() * 120.0f) / 10000.0f));

				for (var i = 0; i < m_MaxRPMValues && i < m_RPMValues.Count; i++)
				{
					var x = visBounds.Width * (i / (float)m_MaxRPMValues);

					if (m_LoopVolumes[loop][i] != 0.0f)
					{
						g.FillEllipse(new System.Drawing.SolidBrush(Color.FromArgb((int)(255 * m_LoopVolumes[loop][i]), 255, 0, 0)), x - 2, y - 2, 4, 4);
					}
				}
			}

			for (var i = 0; i < m_MaxRPMValues && i < m_RPMValues.Count; i++)
			{
				var x = visBounds.Width * (i / (float)m_MaxRPMValues);
				var y = visBounds.Height * (1.0f - (m_GranularVolumes[i].m_RPM / 10000.0f));

				g.FillEllipse(new System.Drawing.SolidBrush(Color.FromArgb((int)(255 * m_GranularVolumes[i].m_Volume), 0, 255, 0)), x - 2, y - 2, 5, 5);
			}

			for (var i = 0; i < m_MaxRPMValues && i < m_RPMValues.Count; i++)
			{
				var y = visBounds.Height * (1.0f - (m_RPMValues[i] / 10000));
				var x = visBounds.Width * (i / (float)m_MaxRPMValues);

				if (x > visBounds.Width &&
				   lastx > visBounds.Width)
				{
					break;
				}

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
				}

				lastx = (int)x;
				lasty = (int)y;
			}

			// draw path
			g.DrawPath(p, gp);

			Font arialBold = new Font(new FontFamily("Arial"), 20, FontStyle.Bold);
			SolidBrush textBrush = new SolidBrush(Color.Black);
			g.DrawString(((int)m_CurrentRevs).ToString() + " RPM", arialBold, textBrush, new PointF(0.0f, 0.0f));

			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			float changeRate = m_CurrentRevs - m_PrevRevs;
			float chageRatePerSec = changeRate / 0.016f;
			string changeRateString = String.Format("{0:0.#}", chageRatePerSec);

			if (changeRate > 0)
			{
				changeRateString = "+" + changeRateString;
			}

			g.DrawString("Change Rate: " + changeRateString, arial, textBrush, new PointF(5.0f, 30.0f));
		}
		
		private void waveView_Paint(object sender, PaintEventArgs e)
		{
			if (!m_FileLoaded)
				return;

			Graphics g = e.Graphics;

			var p = Pens.DarkBlue;
			PictureBox picBox = sender as System.Windows.Forms.PictureBox;
			g.DrawRectangle(p, new Rectangle(0, 0, picBox.Width - 1, picBox.Height - 1));

			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(0).m_WaveData;
			List<GranularSubmix.audGrainData> grainData = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_GrainTable;
			// draw selected grain
			if (grainData.Count > 2)
			{
				if (!IsEngineSimPlaying() && m_SelectedGrains.Count > 0)
				{
					for (int loop = 0; loop < m_SelectedGrains.Count; loop++)
					{
						if (m_SelectedGrains[loop] + 1 < grainData.Count)
						{
							g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 0, 255, 0)),
								(grainData[m_SelectedGrains[loop]].startSample - m_Xoffset) * m_Xscale,
								0,
								(grainData[m_SelectedGrains[loop] + 1].startSample - grainData[m_SelectedGrains[loop]].startSample) * m_Xscale,
								m_WaveImage.Height);
						}
					}
				}

				if (m_GrainPlayer.m_IsPlaying)
				{
					int grainIndex = 0;
					float volumeScale = 1.0f;
					
					if(m_GrainPlayer.m_GrainPlaybackStyle == GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug)
					{
						grainIndex = m_GrainPlayer.GetGranularMix(0).GetDebugSubmix().m_PrevSelectedGrain;
					}
					else
					{
						grainIndex = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_PrevSelectedGrain;
						volumeScale = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_LastVolumeScale;
					}

					if (grainIndex + 1 < grainData.Count)
					{
						g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb((int)(200 * volumeScale), 0, 255, 0)),
							(grainData[grainIndex].startSample - m_Xoffset) * m_Xscale,
							0,
							(grainData[grainIndex + 1].startSample - grainData[grainIndex].startSample) * m_Xscale,
							m_WaveImage.Height);
					}

					if (m_GrainPlayer.m_GrainPlaybackStyle != GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug)
					{
						for(int loop = 0; loop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); loop++)
						{
							grainIndex = m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(loop).m_PrevSelectedGrain;
							volumeScale = m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(loop).m_LastVolumeScale;

							if (grainIndex + 1 < grainData.Count)
							{
								g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb((int)(200 * volumeScale), 0, 0, 255)),
									(grainData[grainIndex].startSample - m_Xoffset) * m_Xscale,
									0,
									(grainData[grainIndex + 1].startSample - grainData[grainIndex].startSample) * m_Xscale,
									m_WaveImage.Height);
							}
						}
					}
				}

				int selectedGrainIndex = -1;
				
				if (!m_GrainPlayer.m_IsPlaying && m_SelectedGrains.Count > 0)
				{
					selectedGrainIndex = m_SelectedGrains[0];
				}
				else if(m_GrainPlayer.m_IsPlaying && m_GrainPlayer.m_GrainPlaybackStyle == GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug)
				{
					selectedGrainIndex = m_GrainPlayer.GetGranularMix(0).GetDebugSubmix().m_PrevSelectedGrain;
				}
				
				if (selectedGrainIndex >= 0 &&
					selectedGrainIndex + 1 < grainData.Count)
				{
					Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
					SolidBrush textBrush = new SolidBrush(Color.Black);

					g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(210, 255, 255, 255)),
							0,
							0,
							420.0f,
							17.0f);

					int samplesThisGranule = grainData[selectedGrainIndex + 1].startSample - grainData[selectedGrainIndex].startSample;
					float granulesPerSecond = (waveData.Format.SampleRate / (float)samplesThisGranule);
					float rpm = (int)(granulesPerSecond * 2.0f * 60.0f);
					string rpmString = rpm.ToString() + " RPM";

					float grainSamples = grainData[selectedGrainIndex + 1].startSample - grainData[selectedGrainIndex].startSample;
					float milliseconds = grainSamples / m_GrainPlayer.GetGranularMix(0).m_WaveData.Format.SampleRate;
					milliseconds *= 1000;

					g.DrawString("Grain " + selectedGrainIndex + "/" + grainData.Count + ": " + grainSamples.ToString() + " samples " + String.Format("{0:0.###}", milliseconds) + "ms " + rpmString, arial, textBrush, new PointF(0.0f, 0.0f));
				}
			}
		}

		private void RenderWaveView()
		{
			if (!m_FileLoaded)
				return;

			Graphics g = Graphics.FromImage(m_WaveImage);

			Int32 lastx = 0, lasty = 0;
			var gp = new GraphicsPath();
			var p = Pens.DarkBlue;

			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(0).m_WaveData;
			List<GranularSubmix.audGrainData> grainData = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_GrainTable;

			// scale x so that the entire sample fits in the visbounds
			m_Xscale = (m_WaveImage.Width / (float) waveData.Data.NumSamples) * m_Xzoom;

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
							0,
							0,
							m_WaveImage.Width,
							m_WaveImage.Height);

			// Default step rate
			int stepRate = 10;

			float zoomPercentage = (m_Xzoom - 100) / 100.0f;

			if (zoomPercentage > 1.0f)
				zoomPercentage = 1.0f;

			if (zoomPercentage < 0.0f)
				zoomPercentage = 0.0f;

			// As we zoom in more, increase the accuracy
			stepRate -= (int)((stepRate - 1) * zoomPercentage);

			for (var i = m_Xoffset; i < waveData.Data.NumSamples; i += stepRate)
			{
				float sample = waveData.Data.ChannelData[0, i];

				// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
				sample *= -1.0f;
				sample += 1.0f;
				sample /= 2.0f;

				// now 0 -> 1.0f
				var y = (Int32)(sample * m_WaveImage.Height);
				var x = i;

				x = (Int32)((x - m_Xoffset) * m_Xscale);

				y = (Int32)(y * m_Yscale);

				if (x > m_WaveImage.Width &&
				   lastx > m_WaveImage.Width)
				{
					break;
				}

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
					//g.DrawLine(p, lastx, lasty, x, y);
				}

				if (drawSamplesCheck.Checked)
				{
					g.FillEllipse(new SolidBrush(Color.Black), x - 2, y - 2, 4, 4);
				}

				lastx = x;
				lasty = y;
			}

			// draw path
			g.DrawPath(p, gp);

			if (drawGrainsCheck.Checked)
			{
				for (var grainLoop = 0; grainLoop < grainData.Count; grainLoop++)
				{
					var x = (Int32)((grainData[grainLoop].startSample - m_Xoffset) * m_Xscale);

					if (x > m_WaveImage.Width)
					{
						break;
					}

					g.DrawLine(Pens.HotPink, x, m_WaveImage.Height, x, 0);
				}
			}

			if (drawLoopsCheck.Checked)
			{
				for (int loop = 0; loop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); loop++)
				{
					GranularSubmix syncLoop = m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(loop);
					
					for (int grainLoop = 0; grainLoop < syncLoop.m_ValidGrains.Count; grainLoop++)
					{
						if (syncLoop.m_ValidGrains[grainLoop] + 1 < grainData.Count)
						{
							if (loop == m_SelectedLoop)
							{
								g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 255, 0, 0)),
								(grainData[syncLoop.m_ValidGrains[grainLoop]].startSample - m_Xoffset) * m_Xscale,
								0,
								(grainData[syncLoop.m_ValidGrains[grainLoop] + 1].startSample - grainData[syncLoop.m_ValidGrains[grainLoop]].startSample) * m_Xscale,
								m_WaveImage.Height);
							}
							else
							{
								g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 0, 0, 255)),
								(grainData[syncLoop.m_ValidGrains[grainLoop]].startSample - m_Xoffset) * m_Xscale,
								0,
								(grainData[syncLoop.m_ValidGrains[grainLoop] + 1].startSample - grainData[syncLoop.m_ValidGrains[grainLoop]].startSample) * m_Xscale,
								m_WaveImage.Height);
							}
						}
					}
				}
			}

			// draw cursor position
			if (grainData.Count <= 2)
			{
				g.DrawLine(Pens.Black,
					   (m_PlaybackCursor - m_Xoffset) * m_Xscale,
					   0,
					   (m_PlaybackCursor - m_Xoffset) * m_Xscale,
					   m_WaveImage.Height);
			}

			// draw selected grain
			if (grainData.Count > 2)
			{
				if (m_SelectedGrains.Count > 0)
				{
					/*
					for (int loop = 0; loop < m_SelectedGrains.Count; loop++)
					{
						if (m_SelectedGrains[loop] + 1 < grainData.Count)
						{
							g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 0, 255, 0)),
								(grainData[m_SelectedGrains[loop]] - m_Xoffset) * m_Xscale,
								0,
								(grainData[m_SelectedGrains[loop] + 1] - grainData[m_SelectedGrains[loop]]) * m_Xscale,
								m_WaveImage.Height);
						}
					}
					*/

					/*
					if (!m_SimulatorActive)
					{
						selectedGrainIndex = GetSelectedGrain(m_debugSubmix.m_SelectedGrainIndex);
					}
					else
					{
						selectedGrainIndex = m_GranularSubmix.m_PrevSelectedGrain;
					}

					if (selectedGrainIndex >= 0 &&
						selectedGrainIndex + 1 < grainData.Count)
					{
						if (IsPlayingGranular() &&
							selectedGrainIndex + 1 < grainData.Count)
						{
							g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(140, 0, 255, 0)),
								(grainData[selectedGrainIndex] - m_Xoffset) * m_Xscale,
								0,
								(grainData[selectedGrainIndex + 1] - grainData[selectedGrainIndex]) * m_Xscale,
								m_WaveImage.Height);
						}

						Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
						SolidBrush textBrush = new SolidBrush(Color.Black);

						g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(210, 255, 255, 255)),
								0,
								0,
								420.0f,
								17.0f);

						int samplesThisGranule = grainData[selectedGrainIndex + 1] - grainData[selectedGrainIndex];
						float granulesPerSecond = (waveData.Format.SampleRate / (float)samplesThisGranule);
						float rpm = granulesPerSecond * 2.0f * 60.0f;
						string rpmString = rpm.ToString() + " RPM";

						float grainSamples = grainData[selectedGrainIndex + 1] - grainData[selectedGrainIndex];
						float milliseconds = grainSamples / m_WaveFile.Format.SampleRate;
						milliseconds *= 1000;

						g.DrawString("Grain " + selectedGrainIndex + "/" + grainData.Count + ": " + grainSamples.ToString() + " samples " + String.Format("{0:0.###}", milliseconds) + "ms " + rpmString, arial, textBrush, new PointF(0.0f, 0.0f));
					}*/
				}
			}

			// draw x axis
			g.DrawLine(p, 0, m_WaveImage.Height / 2, m_WaveImage.Width, m_WaveImage.Height / 2);

			// draw a line to show the end of the wave
			g.DrawLine(p, lastx, 0, lastx, m_WaveImage.Height);

			var zoom = (int)(m_Xzoom * 100);
		}

		private void RenderWaveAxis()
		{
			if (!m_FileLoaded)
				return;

			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(0).m_WaveData;
			var grfx = Graphics.FromImage(m_AxisImage);
			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromKnownColor(KnownColor.Control)),
				0,
				0,
				m_AxisImage.Width,
				m_AxisImage.Height);

			float startSampleTime = 0.0f;
			float endSampleTime = 10.0f;

			int startSampleIndex = (int)(m_Xoffset);
			int endSampleIndex = (int)(m_Xoffset + (int)(waveData.Data.NumSamples / m_Xzoom));

			startSampleTime = startSampleIndex / (float)waveData.Format.SampleRate;
			endSampleTime = endSampleIndex / (float)waveData.Format.SampleRate;

			for (int loop = 0; loop < 10; loop++)
			{
				float xCoord = m_AxisImage.Width / 10 * loop;
				float thisTime = startSampleTime + ((endSampleTime - startSampleTime) / 10) * loop;

				string timeString = String.Format("{0:0.###}", thisTime);
				grfx.DrawString(timeString, arial, textBrush, new PointF(xCoord + 1, 3));
				grfx.DrawLine(Pens.Black, xCoord, m_AxisImage.Height / 2, xCoord, 0);
			}
		}

		private void redrawTimer_Tick(object sender, EventArgs e)
		{
			if (waveView.Width > 0 && waveView.Height > 0)
			{
				if (m_WaveImage.Width != waveView.Width ||
					m_WaveImage.Height != waveView.Height)
				{
					m_WaveImage = new Bitmap(waveView.Width, waveView.Height);
					waveView.Image = m_WaveImage;
					RenderWaveView();
				}

				if (m_AxisImage.Width != waveViewAxis.Width ||
					m_AxisImage.Height != waveViewAxis.Height)
				{
					m_AxisImage = new Bitmap(waveViewAxis.Width, waveViewAxis.Height);
					waveViewAxis.Image = m_AxisImage;
					RenderWaveAxis();
				}

				waveView.Refresh();
				waveViewAxis.Refresh();
				engineSimView.Refresh();
			}
		}

		private void UpdateDebugValidGrains()
		{
			if (m_SelectedGrains.Count > 0)
			{
				m_SelectedGrains.Sort();
				m_GrainPlayer.GetGranularMix(0).GetDebugSubmix().SetValidGrains(m_SelectedGrains);
			}
		}

		private void waveView_MouseDown(object sender, MouseEventArgs e)
		{
			waveView.Focus();

			if (!m_FileLoaded)
				return;

			if (e.Button == MouseButtons.Left)
			{
				m_GrainPlayer.m_GrainPlaybackStyle = GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug;
				m_LeftMouseDown = true;

				if (Control.ModifierKeys != Keys.Shift &&
					Control.ModifierKeys != Keys.Control)
				{
					m_SelectedGrains.Clear();
				}

				if(Control.ModifierKeys == Keys.Shift)
				{
					SelectGrainRange(e.X);
				}
				else
				{
					SelectGrain(e.X, Control.ModifierKeys == Keys.Control);
				}

				UpdateDebugValidGrains();
			}

			if(e.Button == MouseButtons.Right)
			{
				grainViewContextMenu.Show(e.Location.X + this.Location.X + 300, e.Location.Y + this.Location.Y + 60);
			}
		}

		private void waveView_MouseUp(object sender, MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Left)
			{
				m_LeftMouseDown = false;
			}
		}

		private void waveView_MouseEnter(object sender, EventArgs e)
		{
			m_MouseOverWaveView = true;
		}

		private void waveView_MouseLeave(object sender, EventArgs e)
		{
			m_LeftMouseDown = false;
			m_MouseOverWaveView = false;
		}

		private void waveView_MouseMove(object sender, MouseEventArgs e)
		{
			if (Control.ModifierKeys != Keys.Shift &&
				Control.ModifierKeys != Keys.Control)
			{
				if (m_LeftMouseDown)
				{
					m_SelectedGrains.Clear();

					SelectGrain(e.X, false);
					UpdateDebugValidGrains();
				}
			}

			m_LastMousePoint = e.Location;
		}

		private void waveView_MouseWheel(object sender, MouseEventArgs e)
		{
			if(m_FileLoaded)
			{
				if (m_MouseOverWaveView)
				{
					if (e.Delta > 0)
					{
						m_Xzoom *= 1.1f;
					}
					else
					{
						m_Xzoom *= 0.9f;

						if (m_Xzoom < 1.0f)
						{
							m_Xzoom = 1.0f;
						}
					}

					RefocusOnMousePos();
					
					bwWaveFile waveData = m_GrainPlayer.GetGranularMix(0).m_WaveData;
					int samplesInWindow = (int)(waveData.Data.NumSamples / m_Xzoom);
					waveScrollBar.Maximum = (int)((waveData.Data.NumSamples - samplesInWindow)/1000.0f);
					waveScrollBar.Value = (int)(m_Xoffset / 1000.0f);

					RenderWaveView();
					RenderWaveAxis();
				}
			}
		}

		/// <summary>
		/// Refocus on where the mouse currently is
		/// </summary>
		void RefocusOnMousePos()
		{
			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(0).m_WaveData;
				
			int samplesInWindow = (int)(waveData.Data.NumSamples / m_Xzoom);
			float zoomTarget = m_LastMousePoint.X / (float)waveView.Width;

			// This is a bit rubbish - zooms to centre - should really zoom to the mouse cursor pos
			float zoomTargetSamples = m_Xoffset + (samplesInWindow * zoomTarget) - (samplesInWindow / 2);
			m_Xoffset = (int)(zoomTargetSamples);

			if (m_Xoffset + samplesInWindow > waveData.Data.NumSamples)
			{
				m_Xoffset = (int)(waveData.Data.NumSamples - samplesInWindow);
			}

			if (m_Xoffset < 0)
			{
				m_Xoffset = 0;
			}
		}

		private void SelectGrain(int xCoord, bool toggle)
		{
			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(0).m_WaveData;
			List<GranularSubmix.audGrainData> grainData = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_GrainTable;

			if (waveData != null)
			{
				int samplesInWindow = (int)(waveData.Data.NumSamples / m_Xzoom);
				float fractionSelected = xCoord / (float)m_WaveImage.Width;
				int newGrainSelectionCursor = (int)(m_Xoffset + (samplesInWindow * fractionSelected));

				for (int loop = 0; loop < grainData.Count; loop++)
				{
					if (grainData[loop].startSample > newGrainSelectionCursor &&
						loop > 0)
					{
						bool alreadyAdded = false;
						int alreadyAddedIndex = -1;
						int grainToAdd = loop - 1;
						
						for (int selectedGrainLoop = 0; selectedGrainLoop < m_SelectedGrains.Count; selectedGrainLoop++)
						{
							if (m_SelectedGrains[selectedGrainLoop] == grainToAdd)
							{
								alreadyAddedIndex = selectedGrainLoop;
								alreadyAdded = true;
								break;
							}
						}

						if (!alreadyAdded)
						{
							m_SelectedGrains.Add(loop - 1);
						}
						else if (toggle)
						{
							m_SelectedGrains.RemoveAt(alreadyAddedIndex);
						}
						
						break;
					}
				}
			}
		}

		private void SelectGrainRange(int xCoord)
		{
			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(0).m_WaveData;
			List<GranularSubmix.audGrainData> grainData = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_GrainTable;

			if (waveData != null && m_SelectedGrains.Count > 0)
			{
				int samplesInWindow = (int)(waveData.Data.NumSamples / m_Xzoom);
				float fractionSelected = xCoord / (float)m_WaveImage.Width;
				int newGrainSelectionCursor = (int)(m_Xoffset + (samplesInWindow * fractionSelected));

				int minCurrentlySelectedGrain = m_SelectedGrains.Min();
				int maxCurrentlySelectedGrain = m_SelectedGrains.Max();

				int chosenGrain = -1;
				for (int loop = 0; loop < grainData.Count; loop++)
				{
					if (grainData[loop].startSample > newGrainSelectionCursor &&
						loop > 0)
					{
						chosenGrain = loop - 1;
						break;
					}
				}

				if(chosenGrain == -1)
				{
					return;
				}

				int startIndex = -1;
				int endIndex = -1;

				if(chosenGrain >= maxCurrentlySelectedGrain)
				{
					startIndex = maxCurrentlySelectedGrain;
					endIndex = chosenGrain;
				}
				else
				{
					startIndex = chosenGrain;
					endIndex = minCurrentlySelectedGrain;
				}

				for (int loop = startIndex; loop <= endIndex; loop++)
				{
					bool alreadyAdded = false;

					for (int selectedGrainLoop = 0; selectedGrainLoop < m_SelectedGrains.Count; selectedGrainLoop++)
					{
						if (m_SelectedGrains[selectedGrainLoop] == loop)
						{
							alreadyAdded = true;
							break;
						}
					}

					if (!alreadyAdded)
					{
						m_SelectedGrains.Add(loop);
					}
				}
			}
		}

		private bool IsDebugSubmixPlaying()
		{
			return m_GrainPlayer.m_IsPlaying && m_GrainPlayer.m_GrainPlaybackStyle == GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug;
		}

		private bool IsEngineSimPlaying()
		{
			return m_GrainPlayer.m_IsPlaying && m_GrainPlayer.m_GrainPlaybackStyle != GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug;
		}

		private void drawGrainsCheck_CheckedChanged(object sender, EventArgs e)
		{
			RenderWaveView();
		}

		private void drawLoopsCheck_CheckedChanged(object sender, EventArgs e)
		{
			RenderWaveView();
		}

		private void drawSamplesCheck_CheckedChanged(object sender, EventArgs e)
		{
			RenderWaveView();
		}

		public DataTable GetLoopDataTable()
		{
			// Create the output table.
			DataTable d = new DataTable();
			d.Columns.Add("Loop");
			d.Columns.Add("RPM");
			d.Columns.Add("Style");

			for(int loop = 0; loop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); loop++)
			{
				GranularSubmix submix = m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(loop);
				d.Rows.Add();

				d.Rows[loop][0] = (loop + 1).ToString();
				d.Rows[loop][1] = (int)submix.GetAverageValidGrainHertz() * 120;
				d.Rows[loop][2] = submix.m_PlaybackOrder;
			}

			return d;
		}

		private void loopGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			if(e.RowIndex >= 0)
			{
				string value = loopGridView[0, e.RowIndex].Value.ToString();
				m_SelectedLoop = System.Convert.ToInt32(value) - 1;
			}
			else
			{
				m_SelectedLoop = 0;
			}
			
			RenderWaveView();
		}

		private void simulateEngineButton_Click(object sender, EventArgs e)
		{
			if (simulateEngineButton.Text == "Play")
			{
				m_GrainPlayer.m_IsPlaying = true;				
			}
			else
			{
				m_GrainPlayer.m_IsPlaying = false;
			}

			simulateEngineButton.Text = m_GrainPlayer.m_IsPlaying ? "Stop" : "Play";
		}

		private void changeRateForMaxLoops_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_ChangeRateForMaxLoops = (float)changeRateForMaxLoops.Value;
		}

		private void changeRateForMaxGrains_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_ChangeRateForMaxGrains = (float)changeRateForMaxGrains.Value;
		}

		private void loopBelowBiasUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.GetGranularMix(0).s_GranularLoopBelowBias = (float)loopBelowBiasUpDown.Value;
		}

		private void playbackStyleCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(!(m_GrainPlayer.m_IsPlaying && m_GrainPlayer.m_GrainPlaybackStyle == GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug))
			{
				m_GrainPlayer.m_GrainPlaybackStyle = (GrainPlayer.audGrainPlaybackStyle)playbackStyleCombo.SelectedIndex;
			}
		}

		private void selectAllGrainsToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			if(m_FileLoaded)
			{
				m_SelectedGrains.Clear();

				for (int loop = 0; loop < m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_GrainTable.Count; loop++)
				{
					m_SelectedGrains.Add(loop);
				}

				UpdateDebugValidGrains();
			}
		}

		private void grainSelectionStyle_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(m_FileLoaded)
			{
				m_GrainPlayer.GetGranularMix(0).GetDebugSubmix().m_PlaybackOrder = (GranularSubmix.audGrainPlaybackOrder)grainSelectionStyle.SelectedIndex;
			}
		}

		/// <summary>
		/// Reduce the number of selected grains by chopping out every Xth one
		/// </summary>
		public void ReduceGrainSelection(List<int> grains, int grainsToRemove, int stepRate)
		{
			if (grains != null)
			{
				for (int loop = 0; loop < grains.Count; loop += stepRate)
				{
					for (int grainRemoveLoop = 0; grainRemoveLoop < grainsToRemove && loop < grains.Count; grainRemoveLoop++, loop++)
					{
						grains.RemoveAt(loop);
						loop--;
					}
				}
			}

			UpdateDebugValidGrains();
		}

		private void reduceSelectionBy75ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ReduceGrainSelection(m_SelectedGrains, 3, 1);

		}

		private void reduceSelectionBy66ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ReduceGrainSelection(m_SelectedGrains, 2, 1);
		}

		private void reduceSelectionBy50ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ReduceGrainSelection(m_SelectedGrains, 1, 1);
		}

		private void reduceSelectionBy33ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ReduceGrainSelection(m_SelectedGrains, 1, 2);
		}

		private void reduceSelectionBy25ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ReduceGrainSelection(m_SelectedGrains, 1, 3);
		}

		private void waveScrollBar_Scroll(object sender, ScrollEventArgs e)
		{
			m_Xoffset = waveScrollBar.Value * 1000;
			RenderWaveAxis();
			RenderWaveView();
		}

		private void granularWindowSizeUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_SlidingGrainWindowSize = (float)granularWindowSizeUpDown.Value;
		}

		private void minRepeatRateUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_MinGrainRepeateRate = (int)minRepeatRateUpDown.Value;
		}

		private void createLoopToolStripMenuItem_Click(object sender, EventArgs e)
		{
			GranularSubmix submix = new GranularSubmix(m_GrainPlayer.GetGranularMix(0).m_WaveData, m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_GrainTable, GranularSubmix.audGranularSubmixType.SubmixTypeSynchronisedLoop, (GranularSubmix.audGrainPlaybackOrder)grainSelectionStyle.SelectedIndex);
			submix.SetValidGrains(m_SelectedGrains);
			m_GrainPlayer.GetGranularMix(0).AddLoop(submix);
			RenderWaveView();
			PopulateLoopTable();
			//loopGridView.DataSource = GetLoopDataTable();
		}

		private void pitchStyleCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			m_PitchStyle = (GranularPitchStyle)pitchStyleCombo.SelectedIndex;
			m_GrainPlayer.GetGranularMix(0).GetDebugSubmix().m_DebugPitchFlatten = (m_PitchStyle == GranularPitchStyle.PitchStyleFlattened);
		}
		
		private void selectLoopButton_Click(object sender, EventArgs e)
		{
			if(m_SelectedLoop >= 0)
			{
				GranularSubmix submix = m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(m_SelectedLoop);

				m_SelectedGrains.Clear();

				for(int loop = 0; loop < submix.m_ValidGrains.Count; loop++)
				{
					m_SelectedGrains.Add(submix.m_ValidGrains[loop]);
				}

				m_GrainPlayer.GetGranularMix(0).GetDebugSubmix().m_PlaybackOrder = submix.m_PlaybackOrder;
				grainSelectionStyle.SelectedIndex = (int)submix.m_PlaybackOrder;

				UpdateDebugValidGrains();
			}
		}

		private void deleteLoopBtn_Click(object sender, EventArgs e)
		{
			if (m_SelectedLoop >= 0)
			{
				m_GrainPlayer.GetGranularMix(0).RemoveLoop(m_SelectedLoop);
				RenderWaveView();
				//loopGridView.DataSource = GetLoopDataTable();
				PopulateLoopTable();
			}
		}

		// taken from rage atl/map.cpp
		public static uint ComputeHash(string str)
		{
			if (str == null || str.Equals(""))
			{
				return 0;
			}

			//Convert .Net Wave name to a system string and then to a Rage hash code.
			byte[] utf8 = System.Text.Encoding.UTF8.GetBytes(str);


			// This is the one-at-a-time hash from this page:
			// http://burtleburtle.net/bob/hash/doobs.html
			// (borrowed from /soft/swat/src/swcore/string2key.cpp)
			uint key = 0;

			for (int i = 0; i < str.Length; i++)
			{
				byte character = utf8[i];
				if (character >= 'A' && character <= 'Z')
					character += 'a' - 'A';
				else if (character == '\\')
					character = (byte)'/';

				key += character;
				key += (key << 10);	//lint !e701
				key ^= (key >> 6);	//lint !e702
			}
			key += (key << 3);	//lint !e701
			key ^= (key >> 11);	//lint !e702
			key += (key << 15);	//lint !e701

			// The original swat code did several tests at this point to make
			// sure that the resulting value was representable as a valid
			// floating-point number (not a negative zero, or a NaN or Inf)
			// and also reserved a nonzero value to indicate a bad key.
			// We don't do this here for generality but the caller could
			// obviously add such checks again in higher-level code.
			return key;
		}

		private void removeGrainsBtn_Click(object sender, EventArgs e)
		{
			string extension = Path.GetExtension(m_GrainPlayer.GetGranularMix(0).m_WaveData.FileName);
			saveChoppedGrainDialog.FileName = m_GrainPlayer.GetGranularMix(0).m_WaveData.FileName.Replace(extension, "_chopped" + extension);
			saveChoppedGrainDialog.ShowDialog();
		}

		public void CreateNPCVersion(string outputFile)
		{
			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(0).m_WaveData;
			List<float> rawData = new List<float>((int)waveData.Data.NumSamples);
			List<GranularSubmix.audGrainData> grainData = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_GrainTable;
			List<int> selectedGrains = new List<int>();

			for (int loop = 0; loop < grainData.Count; loop++)
			{
				bool isBeingUsedByLoop = false;

				for (int syncLoop = 0; syncLoop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); syncLoop++)
				{
					if (m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains.Contains(loop))
					{
						isBeingUsedByLoop = true;

						if (!selectedGrains.Contains(loop))
						{
							selectedGrains.Add(loop);
						}

						break;
					}
				}
			}

			for (int loop = 0; loop < waveData.Data.NumSamples; loop++)
			{
				rawData.Add(waveData.Data.ChannelData[0, loop]);
			}

			int grainsRemoved = 0;

			for (int grainLoop = 0; grainLoop < grainData.Count - 1; )
			{
				bool grainIsValid = false;

				for (int selectedGrainLoop = 0; selectedGrainLoop < selectedGrains.Count; selectedGrainLoop++)
				{
					if (selectedGrains[selectedGrainLoop] - grainsRemoved == grainLoop)
					{
						grainIsValid = true;
						break;
					}
				}

				if (!grainIsValid)
				{
					int dataToTrim = grainData[grainLoop + 1].startSample - grainData[grainLoop].startSample;

					if (grainData[grainLoop].startSample + dataToTrim < rawData.Count)
					{
						int grainLength = grainData[grainLoop + 1].startSample - grainData[grainLoop].startSample;
						int samplesInCrossfade = (int)(grainLength * 0.05f);

						// Do a quick crossfade between the data we're removing and the new grain
						for (int crossfadeLoop = 0; crossfadeLoop < samplesInCrossfade; crossfadeLoop++)
						{
							float crossfadeVol = crossfadeLoop / (float)samplesInCrossfade;
							int sampleIndex = grainData[grainLoop + 1].startSample + crossfadeLoop;

							if(sampleIndex < rawData.Count)
							{
								rawData[grainData[grainLoop + 1].startSample + crossfadeLoop] *= crossfadeVol;
								rawData[grainData[grainLoop + 1].startSample + crossfadeLoop] += rawData[grainData[grainLoop].startSample + crossfadeLoop] * (1.0f - crossfadeVol);
							}
						}

						rawData.RemoveRange(grainData[grainLoop].startSample, dataToTrim);
					}

					grainData.RemoveAt(grainLoop);

					for (int remainingGrains = grainLoop; remainingGrains < grainData.Count; remainingGrains++)
					{
						grainData[remainingGrains].startSample -= dataToTrim;
					}

					for (int syncLoop = 0; syncLoop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); syncLoop++)
					{
						for (int validGrainLoop = 0; validGrainLoop < m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains.Count; )
						{
							if (m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains[validGrainLoop] == grainLoop)
							{
								m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains.RemoveAt(validGrainLoop);
							}
							else
							{
								if (m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains[validGrainLoop] > grainLoop)
								{
									m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains[validGrainLoop]--;
								}

								validGrainLoop++;
							}
						}
					}

					grainsRemoved++;
				}
				else
				{
					grainLoop++;
				}
			}

			byte[] byteData = new byte[rawData.Count * sizeof(Int16)];
			int byteIndex = 0;

			for (int loop = 0; loop < rawData.Count; loop++)
			{
				byte[] sampleAsByte = BitConverter.GetBytes((Int16)(rawData[loop] * Int16.MaxValue));

				for (int byteLoop = 0; byteLoop < sampleAsByte.Length; byteLoop++)
				{
					byteData[byteIndex] = sampleAsByte[byteLoop];
					byteIndex++;
				}
			}

			waveData.Data.ChannelData = new float[1, rawData.Count];

			for (int loop = 0; loop < rawData.Count; loop++)
			{
				waveData.Data.ChannelData[0, loop] = rawData[loop];
			}

			waveData.Data.RawData = byteData;
			waveData.Data.NumSamples = (uint)rawData.Count;

			waveData.Save(outputFile);
			SaveGrainData(outputFile, false);
		}

		private void saveChoppedGrainDialog_FileOk(object sender, CancelEventArgs e)
		{
			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(0).m_WaveData;
			List<float> rawData = new List<float>((int)waveData.Data.NumSamples);
			List<GranularSubmix.audGrainData> grainData = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_GrainTable;
			List<int> selectedGrains = new List<int>();
			bool preserveLoops = preserveLoopsCheck.Checked;

			for (int loop = 0; loop < grainData.Count; loop++)
			{
				selectedGrains.Add(loop);
			}

			if(grainReductionCombo.SelectedIndex == 0)
			{
				ReduceGrainSelection(selectedGrains, 1, 0);
			}
			else if (grainReductionCombo.SelectedIndex == 1)
			{
				ReduceGrainSelection(selectedGrains, 3, 1);
			}
			else if (grainReductionCombo.SelectedIndex == 2)
			{
				ReduceGrainSelection(selectedGrains, 2, 1);
			}
			else if (grainReductionCombo.SelectedIndex == 3)
			{
				ReduceGrainSelection(selectedGrains, 1, 1);
			}
			else if (grainReductionCombo.SelectedIndex == 4)
			{
				ReduceGrainSelection(selectedGrains, 1, 2);
			}
			else if (grainReductionCombo.SelectedIndex == 5)
			{
				ReduceGrainSelection(selectedGrains, 1, 3);
			}
			else if (grainReductionCombo.SelectedIndex == 6)
			{
				ReduceGrainSelection(selectedGrains, 1, 4);
			}
			else if (grainReductionCombo.SelectedIndex == 7)
			{
				ReduceGrainSelection(selectedGrains, 1, 9);
			}
			else if (grainReductionCombo.SelectedIndex == 8)
			{
				ReduceGrainSelection(selectedGrains, 1, 19);
			}

			if (preserveLoops)
			{
				for (int loop = 0; loop < grainData.Count; loop++)
				{
					bool isBeingUsedByLoop = false;

					for (int syncLoop = 0; syncLoop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); syncLoop++)
					{
						if (m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains.Contains(loop))
						{
							isBeingUsedByLoop = true;

							if (!selectedGrains.Contains(loop))
							{
								selectedGrains.Add(loop);
							}

							break;
						}
					}
				}
			}

			for (int loop = 0; loop < waveData.Data.NumSamples; loop++)
			{
				rawData.Add(waveData.Data.ChannelData[0, loop]);
			}

			int grainsRemoved = 0;

			for (int grainLoop = 0; grainLoop < grainData.Count - 1; )
			{
				bool grainIsValid = false;

				for (int selectedGrainLoop = 0; selectedGrainLoop < selectedGrains.Count; selectedGrainLoop++)
				{
					if (selectedGrains[selectedGrainLoop] - grainsRemoved == grainLoop)
					{
						grainIsValid = true;
						break;
					}
				}

				if (!grainIsValid)
				{
					int dataToTrim = grainData[grainLoop + 1].startSample - grainData[grainLoop].startSample;

					if (grainData[grainLoop].startSample + dataToTrim < rawData.Count)
					{
						int grainLength = grainData[grainLoop + 1].startSample - grainData[grainLoop].startSample;
						int samplesInCrossfade = (int)(grainLength * 0.05f);

						// Do a quick crossfade between the data we're removing and the new grain
						for(int crossfadeLoop = 0; crossfadeLoop < samplesInCrossfade; crossfadeLoop++)
						{
							float crossfadeVol = crossfadeLoop/(float)samplesInCrossfade;
							rawData[grainData[grainLoop + 1].startSample + crossfadeLoop] *= crossfadeVol;
							rawData[grainData[grainLoop + 1].startSample + crossfadeLoop] += rawData[grainData[grainLoop].startSample + crossfadeLoop] * (1.0f - crossfadeVol);
						}
						
						rawData.RemoveRange(grainData[grainLoop].startSample, dataToTrim);
					}

					grainData.RemoveAt(grainLoop);

					for (int remainingGrains = grainLoop; remainingGrains < grainData.Count; remainingGrains++)
					{
						grainData[remainingGrains].startSample -= dataToTrim;
					}

					for (int syncLoop = 0; syncLoop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); syncLoop++)
					{
						for (int validGrainLoop = 0; validGrainLoop < m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains.Count; )
						{
							if (m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains[validGrainLoop] == grainLoop)
							{
								m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains.RemoveAt(validGrainLoop);
							}
							else
							{
								if (m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains[validGrainLoop] > grainLoop)
								{
									m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains[validGrainLoop]--;
								}

								validGrainLoop++;
							}
						}
					}

					grainsRemoved++;
				}
				else
				{
					grainLoop++;
				}
			}

			byte[] byteData = new byte[rawData.Count * sizeof(Int16)];
			int byteIndex = 0;

			for (int loop = 0; loop < rawData.Count; loop++)
			{
				byte[] sampleAsByte = BitConverter.GetBytes((Int16)(rawData[loop] * Int16.MaxValue));

				for (int byteLoop = 0; byteLoop < sampleAsByte.Length; byteLoop++)
				{
					byteData[byteIndex] = sampleAsByte[byteLoop];
					byteIndex++;
				}
			}

			waveData.Data.ChannelData = new float[1, rawData.Count];

			for (int loop = 0; loop < rawData.Count; loop++)
			{
				waveData.Data.ChannelData[0, loop] = rawData[loop];
			}

			waveData.Data.RawData = byteData;
			waveData.Data.NumSamples = (uint)rawData.Count;

			SaveFileDialog dialog = sender as SaveFileDialog;
			waveData.Save(dialog.FileName);

			SaveGrainData(dialog.FileName, true);

			RenderWaveAxis();
			RenderWaveView();

			string grainFileName = dialog.FileName.Replace(Path.GetExtension(dialog.FileName), ".grn");
			GrainView grainView = new GrainView(dialog.FileName, grainFileName, true);
			grainView.Show();

			m_SkipSaveGrainPrompt = true;
			Close();
		}

		private void pitchScaleUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.m_PitchScale = (float)pitchScaleUpDown.Value;
		}

		private void label14_Click(object sender, EventArgs e)
		{

		}

		private void label13_Click(object sender, EventArgs e)
		{

		}

		private void MaxGranularOffset_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_MaxGranularWindowOffset = (int)MaxGranularOffset.Value;
		}

		private void MinGranularOffset_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_MinGranularWindowOffset = (int)MinGranularOffset.Value;
		}

		private void numericUpDown1_ValueChanged(object sender, EventArgs e)
		{
			m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().SetCrossfadeAmount((float)crossfadeUpDown1.Value);
		}

		private void deleteSelectedGrainsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (m_GrainPlayer.m_IsPlaying)
			{
				MessageBox.Show("Please stop granular playback before deleting grains.", "Playback in progress", MessageBoxButtons.OK);
				return;
			}

			string extension = Path.GetExtension(m_GrainPlayer.GetGranularMix(0).m_WaveData.FileName);
			saveRemovedGrainDialog.FileName = m_GrainPlayer.GetGranularMix(0).m_WaveData.FileName.Replace(extension, "_chopped" + extension);
			saveRemovedGrainDialog.ShowDialog();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			SaveGrainData(m_GrainPlayer.GetGranularMix(0).m_WaveData.FileName, true);
		}

		bool SaveGrainData(string fileName, bool showSuccessMsg)
		{
			bool success = false;
			string grainFileName = fileName.Replace(Path.GetExtension(fileName), ".grn");

			try
			{
				GranularSubmix.audGrainDataStruct grainDataStruct = new GranularSubmix.audGrainDataStruct();
				grainDataStruct.grainData = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_GrainTable;
				grainDataStruct.originalPitchData = m_GrainPlayer.GetGranularMix(0).m_GrainDataStruct.originalPitchData;
				grainDataStruct.loopDefinitions = new List<GranularSubmix.audLoopDefinition>();

				for (int loop = 0; loop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); loop++)
				{
					GranularSubmix.audLoopDefinition loopDef = new GranularSubmix.audLoopDefinition();
					GranularSubmix submix = m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(loop);

					loopDef.playbackOrder = submix.m_PlaybackOrder;
					loopDef.submixIdentifier = submix.m_SubmixIdentifierString;
					loopDef.validGrains = new List<int>();

					for (int grainIndex = 0; grainIndex < submix.m_ValidGrains.Count; grainIndex++)
					{
						loopDef.validGrains.Add(submix.m_ValidGrains[grainIndex]);
					}

					grainDataStruct.loopDefinitions.Add(loopDef);
				}

				XmlSerializer serializer = new XmlSerializer(typeof(GranularSubmix.audGrainDataStruct));
				TextWriter textWriter = new StreamWriter(grainFileName);
				serializer.Serialize(textWriter, grainDataStruct);
				textWriter.Close();

				if (showSuccessMsg)
					MessageBox.Show("Grain data has been saved successfully.", "Save Complete", MessageBoxButtons.OK);

				success = true;
				m_GrainsHaveBeenSaved = true;
			}
			catch (System.UnauthorizedAccessException exception)
			{
				MessageBox.Show("Could not get permission to write to grain file " + grainFileName + ". If the file already exists, ensure that it has been checked out in Perforce, and does not have the read-only flag set.", "Error saving grain data", MessageBoxButtons.OK);
			}

			return success;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			GrainGenerator grainGenerator = new GrainGenerator(m_GrainPlayer.GetGranularMix(0).m_WaveData, m_GrainPlayer.GetGranularMix(0).m_GrainDataStruct.originalPitchData, m_GrainPlayer.GetGranularMix(0).m_GrainDataStruct.grainData, m_GrainPlayer.GetGranularMix(0).m_GrainDataStruct.loopDefinitions, 4);
			grainGenerator.Show();
			this.Close();
		}

		private void grainReductionCombo_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void saveRemovedGrainDialog_FileOk(object sender, CancelEventArgs e)
		{
			m_SelectedGrains.Sort();
			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(0).m_WaveData;
			List<float> rawData = new List<float>((int)waveData.Data.NumSamples);
			List<GranularSubmix.audGrainData> grainData = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_GrainTable;
			List<int> selectedGrains = new List<int>(m_SelectedGrains);

			for (int loop = 0; loop < waveData.Data.NumSamples; loop++)
			{
				rawData.Add(waveData.Data.ChannelData[0, loop]);
			}

			int grainsRemoved = 0;

			for (int grainLoop = 0; grainLoop < grainData.Count - 1; )
			{
				bool grainIsValid = true;

				for (int selectedGrainLoop = 0; selectedGrainLoop < selectedGrains.Count; selectedGrainLoop++)
				{
					if (selectedGrains[selectedGrainLoop] - grainsRemoved == grainLoop)
					{
						grainIsValid = false;
						break;
					}
				}

				if (!grainIsValid)
				{
					int dataToTrim = grainData[grainLoop + 1].startSample - grainData[grainLoop].startSample;

					if (grainData[grainLoop].startSample + dataToTrim < rawData.Count)
					{
						int grainLength = grainData[grainLoop + 1].startSample - grainData[grainLoop].startSample;
						int samplesInCrossfade = (int)(grainLength * 0.05f);

						// Do a quick crossfade between the data we're removing and the new grain
						for (int crossfadeLoop = 0; crossfadeLoop < samplesInCrossfade; crossfadeLoop++)
						{
							float crossfadeVol = crossfadeLoop / (float)samplesInCrossfade;
							rawData[grainData[grainLoop + 1].startSample + crossfadeLoop] *= crossfadeVol;
							rawData[grainData[grainLoop + 1].startSample + crossfadeLoop] += rawData[grainData[grainLoop].startSample + crossfadeLoop] * (1.0f - crossfadeVol);
						}

						rawData.RemoveRange(grainData[grainLoop].startSample, dataToTrim);
					}

					grainData.RemoveAt(grainLoop);

					for (int remainingGrains = grainLoop; remainingGrains < grainData.Count; remainingGrains++)
					{
						grainData[remainingGrains].startSample -= dataToTrim;
					}

					for (int syncLoop = 0; syncLoop < m_GrainPlayer.GetGranularMix(0).GetNumSynchronisedLoops(); syncLoop++)
					{
						for (int validGrainLoop = 0; validGrainLoop < m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains.Count; )
						{
							if (m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains[validGrainLoop] == grainLoop)
							{
								m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains.RemoveAt(validGrainLoop);
							}
							else
							{
								if (m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains[validGrainLoop] > grainLoop)
								{
									m_GrainPlayer.GetGranularMix(0).GetSynchronisedLoop(syncLoop).m_ValidGrains[validGrainLoop]--;
								}

								validGrainLoop++;
							}
						}
					}

					grainsRemoved++;
				}
				else
				{
					grainLoop++;
				}
			}

			byte[] byteData = new byte[rawData.Count * sizeof(Int16)];
			int byteIndex = 0;

			for (int loop = 0; loop < rawData.Count; loop++)
			{
				byte[] sampleAsByte = BitConverter.GetBytes((Int16)(rawData[loop] * Int16.MaxValue));

				for (int byteLoop = 0; byteLoop < sampleAsByte.Length; byteLoop++)
				{
					byteData[byteIndex] = sampleAsByte[byteLoop];
					byteIndex++;
				}
			}

			waveData.Data.ChannelData = new float[1, rawData.Count];

			for (int loop = 0; loop < rawData.Count; loop++)
			{
				waveData.Data.ChannelData[0, loop] = rawData[loop];
			}

			waveData.Data.RawData = byteData;
			waveData.Data.NumSamples = (uint)rawData.Count;

			SaveFileDialog dialog = sender as SaveFileDialog;
			waveData.Save(dialog.FileName);

			SaveGrainData(dialog.FileName, true);

			RenderWaveAxis();
			RenderWaveView();

			string grainFileName = dialog.FileName.Replace(Path.GetExtension(dialog.FileName), ".grn");
			GrainView grainView = new GrainView(dialog.FileName, grainFileName, true);
			grainView.Show();

			m_SkipSaveGrainPrompt = true;
			Close();
		}

		private void waveView_Click(object sender, EventArgs e)
		{			
			m_GrainPlayer.m_GrainPlaybackStyle = GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug;			
		}

		private void throttleSlider_Scroll(object sender, ScrollEventArgs e)
		{
			m_GrainPlayer.m_GrainPlaybackStyle = (GrainPlayer.audGrainPlaybackStyle)playbackStyleCombo.SelectedIndex;
		}

		private void label10_Click(object sender, EventArgs e)
		{

		}

		private void label15_Click(object sender, EventArgs e)
		{

		}

		private void label12_Click(object sender, EventArgs e)
		{

		}

		private void waveViewAxis_Click(object sender, EventArgs e)
		{

		}
	}
}
