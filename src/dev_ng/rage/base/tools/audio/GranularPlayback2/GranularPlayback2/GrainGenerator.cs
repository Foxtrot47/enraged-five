﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using Wavelib;

namespace GranularPlayback2
{
	public partial class GrainGenerator : Form
	{
		List<GranularSubmix.audPitchData> m_OriginalPitchData;
		List<GranularSubmix.audPitchData> m_PitchData;
		List<GranularSubmix.audPitchData> m_OldPitchData;
		List<GranularSubmix.audGrainData> m_BlendedGrainData;
		List<GranularSubmix.audLoopDefinition> m_OriginalLoopData;
		List<GranularSubmix.audGrainData> m_GrainData;

		bwWaveFile m_BlendedWaveFile;

		float m_MaxPitch;
		string m_PitchDataFilename;
		bwWaveFile m_WaveFile;
		int m_GranuleBarProgress;
		Thread m_GenerationThread;
		bool m_ThreadCompleted;
		bool m_DataSaved;

		// Rendering stuff
		float m_Yscale = 1.0f;
		float m_Yzoom = 1.0f;
		float m_Xscale = 1.0f;
		float m_Xzoom = 1.0f;
		int m_Xoffset = 0;
		float m_WindowWidth = 0.0f;
		int m_PitchSelectionCursor = 0;
		float m_GranuleGenerationCursor = 0.0f;
		int m_LongestGrainSoFar = 0;
		float m_SpectrogramWidth = -1;
		float m_SpectrogramHeight = -1;
		int m_SpectrogramCylinders = -1;

		// Selection stuff
		bool m_SelectingRegion = false;
		int m_RegionSelectCursorStart = 0;
		int m_RegionSelectCursorEnd = 0;
		RectangleF m_MainDisplayVisBounds;

		public GrainGenerator(bwWaveFile waveFile, List<GranularSubmix.audPitchData> pitchData, List<GranularSubmix.audGrainData> grainData, List<GranularSubmix.audLoopDefinition> originalLoopData, int numCylinders)
		{
			InitializeComponent();
			m_WaveFile = waveFile;
			m_PitchData = pitchData;
			m_OriginalPitchData = new List<GranularSubmix.audPitchData>();
			m_OldPitchData = new List<GranularSubmix.audPitchData>();
			m_GrainData = new List<GranularSubmix.audGrainData>();
			m_GranuleBarProgress = 0;
			generateGranulesBtn.Enabled = false;
			m_PitchDataFilename = null;
			m_ThreadCompleted = false;
			m_DataSaved = false;
			m_OriginalLoopData = originalLoopData;
			grainPreviewLabel.Text = "";
			numCylindersUpDown.Value = numCylinders;

			m_MaxPitch = 0;

			if (grainData != null)
			{
				for(int loop = 0; loop < grainData.Count; loop++)
				{
					m_GrainData.Add(grainData[loop]);
				}
			}

			for (int loop = 0; loop < m_PitchData.Count; loop++ )
			{
				GranularSubmix.audPitchData copiedData = new GranularSubmix.audPitchData();
				copiedData.pitch = m_PitchData[loop].pitch;
				copiedData.sampleIndex = m_PitchData[loop].sampleIndex;
				m_OriginalPitchData.Add(copiedData);

				if(m_PitchData[loop].pitch > m_MaxPitch)
				{
					m_MaxPitch = m_PitchData[loop].pitch;
				}
			}

			Backup();

			this.KeyPreview = true;
			this.KeyDown += new KeyEventHandler(GrainGenerator_KeyDown);
			this.Text = "Grain Generator - " + m_WaveFile.FileName;
		}

		void GrainGenerator_KeyDown(object sender, KeyEventArgs e)
		{
			switch(e.KeyCode)
			{
				case Keys.Oemplus:
					ModifySelection(1);
					break;

				case Keys.OemMinus:
					ModifySelection(-1);
					break;
			}
		}

		private void loadPitchDataBtn_Click(object sender, EventArgs e)
		{
			loadPitchDataDlg.ShowDialog();
		}

		public bool IsPitchDataLoaded()
		{
			return m_PitchData.Count > 0;
		}

		private void loadPitchDataDlg_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;
			m_PitchDataFilename = dialog.FileName;
			LoadPitchData(m_PitchDataFilename);
		}

		private void generateGranulesBtn_Click(object sender, EventArgs e)
		{
			if (m_GenerationThread != null &&
				m_GenerationThread.IsAlive)
			{
				m_GenerationThread.Abort();
				m_GenerationThread = null;
			}
			else
			{
				if(generateFromCursorPos.Checked)
				{
					if(m_GranuleGenerationCursor < m_PitchData.Count)
					{
						int grainIndex = GetGrainIndexForSample(m_PitchData[(int)m_GranuleGenerationCursor].sampleIndex) - 1;

						if (grainIndex > 2)
						{
							m_GrainData.RemoveRange(grainIndex, m_GrainData.Count - grainIndex);
							m_GenerationThread = new Thread(new ThreadStart(GenerateGranules));
							m_GenerationThread.Start();
						}
					}
				}
				else
				{
					m_GrainData.Clear();
					m_GenerationThread = new Thread(new ThreadStart(GenerateGranules));
					m_GenerationThread.Start();
				}
			}
		}

		private void prevGrainView_Paint(object sender, PaintEventArgs pea)
		{
			var g = pea.Graphics;
			var visBounds = g.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);

			Int32 lastx = 0, lasty = (Int32)visBounds.Height/2;
			var gp = new GraphicsPath();
			var p = Pens.DarkBlue;
			int numGrains = m_GrainData.Count;

			if (m_GenerationThread == null || !m_GenerationThread.IsAlive)
			{
				if (m_PitchData.Count > m_PitchSelectionCursor)
				{
					numGrains = GetGrainIndexForSample(m_PitchData[m_PitchSelectionCursor].sampleIndex) - 1;
				}
			}

			if(numGrains > 2)
			{
				int grainStart = m_GrainData[numGrains - 2].startSample;
				int grainEnd = m_GrainData[numGrains - 1].startSample;
				int numSamples = grainEnd - grainStart;
				int stepRate = 10;
				int xIndex = 0;

				// Try to keep the preview window the same so we can visualise the granules
				// getting shorter as the pitch increases
				if (numSamples > m_LongestGrainSoFar)
				{
					m_LongestGrainSoFar = numSamples;
				}

				bool drawFixedWidth = fixedWidthComboBox.Checked;
				bool drawScaled = scaleToFitCheckBox.Checked;

				float scalingFactor = 1.0f;

				if(drawScaled)
				{
					float maxSample = float.MinValue;

					for (var i = grainStart; i < grainEnd && i < m_WaveFile.Data.ChannelData.Length; i += stepRate)
					{
						float sample = Math.Abs(m_WaveFile.Data.ChannelData[0, i]);

						if(sample > maxSample)
						{
							maxSample = sample;
						}
					}

					scalingFactor = 1.0f / maxSample;
				}

				/*
				if (drawFixedWidth)
				{
					stepRate = 2;
				}
				*/

				float hertz = 1.0f / ((grainEnd - grainStart) / (float)m_WaveFile.Format.SampleRate);
				float rpm = hertz * 120.0f;

				grainPreviewLabel.Text = (grainEnd - grainStart).ToString() + " samples / " + String.Format("{0:0.##}", hertz) + "Hz / " + String.Format("{0:0}", rpm) + " RPM";

				for (var i = grainStart; i < grainEnd && i < m_WaveFile.Data.ChannelData.Length; i += stepRate, xIndex += stepRate)
				{
					float sample = m_WaveFile.Data.ChannelData[0, i] * scalingFactor;

					// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
					sample *= -1.0f;
					sample += 1.0f;
					sample /= 2.0f;

					// now 0 -> 1.0f
					var y = (Int32)(sample * visBounds.Height);
					var x = 0;

					if(drawFixedWidth)
						x = (Int32)(((float)xIndex / (float)numSamples) * visBounds.Width);
					else
						x = (Int32)(((float)xIndex / (float)m_LongestGrainSoFar) * visBounds.Width);

					if (x > visBounds.Width &&
					   lastx > visBounds.Width)
					{
						break;
					}

					if (i >= 1)
					{
						gp.AddLine(lastx, lasty, x, y);
						//g.DrawLine(p, lastx, lasty, x, y);
					}

					lastx = x;
					lasty = y;
				}

				// draw path
				g.DrawPath(p, gp);

				g.DrawLine(Pens.Blue, 0, visBounds.Height / 2, visBounds.Width, visBounds.Height / 2);
			}
		}

		/// <summary>
		/// Get the grain index for the given sample
		/// </summary>
		/// <returns></returns>
		public int GetGrainIndexForSample(int sampleIndex)
		{
			int prevGrainData = -1;
			for (int loop = 0; loop < m_GrainData.Count; loop++)
			{
				if (prevGrainData < sampleIndex &&
					m_GrainData[loop].startSample >= sampleIndex)
				{
					return loop;
				}

				prevGrainData = m_GrainData[loop].startSample;
			}

			return -1;
		}

		private void pitchView_Paint(object sender, PaintEventArgs e)
		{
			if (m_PitchData.Count == 0)
			{
				return;
			}

			float maxPitch = 100;

			if (m_PitchData.Count != 0 &&
				m_WaveFile != null)
			{
				maxPitch = m_MaxPitch + 20;
			}

			var p = new Pen(new SolidBrush(Color.DarkBlue), 1.0f);

			var g = e.Graphics;
			var visBounds = g.VisibleClipBounds;
			m_MainDisplayVisBounds = visBounds;
			m_WindowWidth = visBounds.Width;

			m_Yscale = 1.0f * m_Yzoom;
			// scale x so that the entire sample fits in the visbounds
			m_Xscale = ((visBounds.Width - 40)/ m_PitchData.Count) * m_Xzoom;

			Int32 lastx = 0, lasty = 0;
			var gp = new GraphicsPath();

			/*
			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(255, 255, 255)),
							40,
							0,
							(Int32)((m_PitchData.Count - m_Xoffset) * m_Xscale),
							visBounds.Height - 40);
			*/

			Drawing.DrawMillisecondsAxis(g, sender as System.Windows.Forms.PictureBox, m_WaveFile.Data.NumSamples, false, m_WaveFile.Format.SampleRate);
			Drawing.DrawFrequencyAxis(g, sender as System.Windows.Forms.PictureBox, (uint)maxPitch * 2 * (uint)numCylindersUpDown.Value, true, (int)numCylindersUpDown.Value);
			
			if (visBounds.Width != m_SpectrogramWidth ||
				visBounds.Height != m_SpectrogramHeight ||
				m_SpectrogramCylinders != (int)numCylindersUpDown.Value)
			{
				Drawing.DrawSpectrogram(PitchAnalysis.s_AnalysisFrames, pitchView, m_WaveFile.Format.SampleRate, (uint)maxPitch * (uint)numCylindersUpDown.Value, 1.0f);
				m_SpectrogramWidth = visBounds.Width;
				m_SpectrogramHeight = visBounds.Height;
				m_SpectrogramCylinders = (int)numCylindersUpDown.Value;
			}			

			p = Pens.DarkBlue;

			if (displayCalculatedPitches.Checked)
			{
				for (var i = m_Xoffset; i < m_PitchData.Count; i++)
				{
					float pitchValue = m_PitchData[i].pitch;

					// scale from 0.0f -> 1.0f
					float pitchProportion = 1.0f - pitchValue / maxPitch;

					// now 0 -> 1.0f
					var y = (Int32)(pitchProportion * (visBounds.Height - 40));
					var x = i;

					x = (Int32)((x - m_Xoffset) * m_Xscale);
					x += 40;

					y = (Int32)(y * m_Yscale);

					if (x > visBounds.Width &&
					   lastx > visBounds.Width)
					{
						break;
					}

					if (i >= 1)
					{
						gp.AddLine(lastx, lasty, x, y);
						//g.DrawLine(p, lastx, lasty, x, y);
					}

					//g.FillEllipse(new SolidBrush(Color.Black), x, y, 3, 3);

					lastx = x;
					lasty = y;
				}
			}

			// draw path
			g.DrawPath(p, gp);

			lastx = 0;
			lasty = 0;

			if(m_GrainData.Count > 2 && displayGrainPitches.Checked)
			{
				gp = new GraphicsPath();

				p = Pens.CornflowerBlue;

				for (var i = 2; i < m_GrainData.Count; i++)
				{
					int grainLength = m_GrainData[i].startSample - m_GrainData[i - 1].startSample;
					float pitchValue = 1.0f / (grainLength / (float)m_WaveFile.Format.SampleRate);

					// scale from 0.0f -> 1.0f
					float pitchProportion = 1.0f - (pitchValue / maxPitch);

					// now 0 -> 1.0f
					var y = (Int32)(pitchProportion * (visBounds.Height - 40));
					var x = (Int32)((m_GrainData[i - 1].startSample / (float)m_PitchData.Last().sampleIndex) * (visBounds.Width - 40)) + 40;

					y = (Int32)(y * m_Yscale);

					if (x > visBounds.Width &&
					   lastx > visBounds.Width)
					{
						break;
					}

					if (i > 2)
					{
						gp.AddLine(lastx, lasty, x, y);
						//g.DrawLine(p, lastx, lasty, x, y);
					}

					//g.FillEllipse(new SolidBrush(Color.Black), x, y, 3, 3);

					lastx = x;
					lasty = y;
				}

				// draw path
				g.DrawPath(p, gp);
			}

			if (m_GrainData.Count > 2 && displaySmoothedPitches.Checked)
			{
				gp = new GraphicsPath();

				p = Pens.Green;

				for (var i = 2; i < m_GrainData.Count; i++)
				{
					int grainLength = m_GrainData[i].startSample - m_GrainData[i - 1].startSample;
					float pitchValue = m_GrainData[i].pitch;

					// scale from 0.0f -> 1.0f
					float pitchProportion = 1.0f - (pitchValue / maxPitch);

					// now 0 -> 1.0f
					var y = (Int32)(pitchProportion * (visBounds.Height - 40));
					var x = (Int32)((m_GrainData[i - 1].startSample / (float)m_PitchData.Last().sampleIndex) * (visBounds.Width - 40)) + 40;

					y = (Int32)(y * m_Yscale);

					if (x > visBounds.Width &&
					   lastx > visBounds.Width)
					{
						break;
					}

					if (i > 2)
					{
						gp.AddLine(lastx, lasty, x, y);
						//g.DrawLine(p, lastx, lasty, x, y);
					}

					//g.FillEllipse(new SolidBrush(Color.Black), x, y, 3, 3);

					lastx = x;
					lasty = y;
				}

				// draw path
				g.DrawPath(p, gp);
			}

			// draw cursor position
			g.DrawLine(Pens.Black,
					   ((m_PitchSelectionCursor - m_Xoffset) * m_Xscale) + 40,
					   0,
					   ((m_PitchSelectionCursor - m_Xoffset) * m_Xscale) + 40,
					   visBounds.Height - 40);

			// progress cursor
			if (m_GenerationThread != null &&
				m_GenerationThread.IsAlive)
			{
				int samplesInWindow = (int)m_PitchData.Last().sampleIndex;
				float fractionSelected = m_GranuleGenerationCursor/ (float)samplesInWindow;
				int finalCursorPos = (int)((m_WindowWidth - 40) * fractionSelected) + 40;

				g.DrawLine(Pens.Red,
						   finalCursorPos,
						   0,
						   finalCursorPos,
						   visBounds.Height - 40);
			}
			else
			{
				// draw cursor position
				g.DrawLine(Pens.Blue,
						   ((m_GranuleGenerationCursor - m_Xoffset) * m_Xscale) + 40,
						   0,
						   ((m_GranuleGenerationCursor - m_Xoffset) * m_Xscale) + 40,
						   visBounds.Height - 40);

				int boxWidth = Math.Abs(m_RegionSelectCursorEnd - m_RegionSelectCursorStart);
				int boxStart = Math.Min(m_RegionSelectCursorStart, m_RegionSelectCursorEnd);

				g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 255, 0, 0)),
						(boxStart * m_Xscale) + 40,
						0,
						(boxWidth * m_Xscale),
						visBounds.Height - 40);
			}
			
			Font arial = new Font(new FontFamily("Arial"), 16, FontStyle.Bold);
			SolidBrush textBrush = new SolidBrush(Color.Black);
			int rpm = (int)(m_PitchData[m_PitchSelectionCursor].pitch * 60 * 2.0f);
			string rpmString = rpm.ToString() + " RPM";

			float sampleIndex = m_PitchData[m_PitchSelectionCursor].sampleIndex;
			float milliseconds = sampleIndex / m_WaveFile.Format.SampleRate;
			milliseconds *= 1000;

			g.DrawString(rpmString, arial, textBrush, new PointF(45.0f, 5.0f));
		}

		private void pitchView_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_SelectingRegion = true;
				
				if (m_WindowWidth > 0)
				{
					int pitchValuesInWindow = (int)(m_PitchData.Count / m_Xzoom);
					float fractionSelected = Math.Max(0, (e.Location.X - 40) / (m_WindowWidth - 40));
					m_RegionSelectCursorStart = (int)(m_Xoffset + (pitchValuesInWindow * fractionSelected));
					m_RegionSelectCursorEnd = m_RegionSelectCursorStart;

					if(m_GenerationThread == null || !m_GenerationThread.IsAlive)
					{
						m_GranuleGenerationCursor = m_RegionSelectCursorStart;
					}
				}
			}
			else if(e.Button == MouseButtons.Right)
			{
				int offset = 50;
				contextMenuStrip.Show(e.Location.X + this.Location.X + offset, e.Location.Y + this.Location.Y + offset);
			}
		}

		private void pitchView_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_SelectingRegion = false;
				
				if (m_WindowWidth > 0)
				{
					int pitchValuesInWindow = (int)(m_PitchData.Count / m_Xzoom);
					float fractionSelected = Math.Max(0, (e.Location.X - 40) / (m_WindowWidth - 40));
					m_RegionSelectCursorEnd = (int)(m_Xoffset + (pitchValuesInWindow * fractionSelected));
				}
			}
		}

		private void pitchView_MouseMove(object sender, MouseEventArgs e)
		{
			if(m_WindowWidth > 0)
			{
				int pitchValuesInWindow = (int)(m_PitchData.Count / m_Xzoom);
				float fractionSelected = Math.Max(0, (e.Location.X - 40) / (m_WindowWidth - 40));
				m_PitchSelectionCursor = (int)(m_Xoffset + (pitchValuesInWindow * fractionSelected));

				if (m_PitchSelectionCursor >= m_PitchData.Count)
				{
					m_PitchSelectionCursor = m_PitchData.Count - 1;
				}

				if (m_PitchSelectionCursor < 0)
					m_PitchSelectionCursor = 0;

				if(m_SelectingRegion)
				{
					m_RegionSelectCursorEnd = m_PitchSelectionCursor;
				}
			}
		}

		private void GenerateGranules()
		{
			CalculateZeroCrossings();
			m_GenerationThread.Abort();
			m_GenerationThread = null;
		}

		private void LoadPitchData(string grainFileName)
		{
			if(File.Exists(grainFileName))
			{
				TextReader textReader = new StreamReader(grainFileName);
				GranularSubmix.audGrainDataStruct saveData = new GranularSubmix.audGrainDataStruct();
				XmlSerializer serializer = new XmlSerializer(typeof(GranularSubmix.audGrainDataStruct));
				saveData = (GranularSubmix.audGrainDataStruct)serializer.Deserialize(textReader);
				textReader.Close();

				m_PitchData.Clear();
				m_OriginalPitchData.Clear();
				m_MaxPitch = 0.0f;

				for (int loop = 0; loop < saveData.originalPitchData.Count; loop++)
				{
					GranularSubmix.audPitchData copiedData = new GranularSubmix.audPitchData();
					copiedData.pitch = saveData.originalPitchData[loop].pitch;
					copiedData.sampleIndex = saveData.originalPitchData[loop].sampleIndex;
					m_OriginalPitchData.Add(copiedData);

					GranularSubmix.audPitchData copiedData2 = new GranularSubmix.audPitchData();
					copiedData2.pitch = saveData.originalPitchData[loop].pitch;
					copiedData2.sampleIndex = saveData.originalPitchData[loop].sampleIndex;
					m_PitchData.Add(copiedData2);

					if (m_PitchData[loop].pitch > m_MaxPitch)
					{
						m_MaxPitch = m_PitchData[loop].pitch;
					}
				}

				Backup();
			}
		}

		public void Reset()
		{
			m_PitchSelectionCursor = 0;
			m_PitchData.Clear();
			m_OldPitchData.Clear();
			m_MaxPitch = 0.0f;
		}

		void LoadPitchDataTextFile()
		{
			System.IO.StreamReader textReader = new System.IO.StreamReader(m_PitchDataFilename);
			string line;

			while ((line = textReader.ReadLine()) != null)
			{
				GranularSubmix.audPitchData newPitchData = new GranularSubmix.audPitchData();
				newPitchData.sampleIndex = Convert.ToInt32(line);
				line = textReader.ReadLine();
				newPitchData.pitch = Convert.ToSingle(line);
				m_PitchData.Add(newPitchData);

				if (newPitchData.pitch > m_MaxPitch)
				{
					m_MaxPitch = newPitchData.pitch;
				}
			}

			textReader.Close();
		}

		/// <summary>
		/// Get the final grain start position in the sequence
		/// </summary>
		/// <returns></returns>
		public int GetFinalGrainPos()
		{
			if (m_GrainData.Count > 0)
			{
				return m_GrainData.Last().startSample;
			}

			return 0;
		}

		/// <summary>
		/// Read the sample at the given sample index
		/// </summary>
		/// <param name="sampleIndex"></param>
		/// <returns></returns>
		public float ReadSampleFromSampleIndex(int sampleIndex)
		{
			if (sampleIndex >= 0 &&
				sampleIndex < m_WaveFile.Data.ChannelData.Length)
			{
				return m_WaveFile.Data.ChannelData[0, sampleIndex];
			}

			return 0;
		}

		float ReadSampleFromGranularFraction(int grainStart, int grainEnd, float granularFraction, bwWaveFile waveData)
		{
			int grainLength = grainEnd - grainStart;
			float sampleIndex = grainStart + (grainLength * granularFraction);
			int floorSample = (int)Math.Floor(sampleIndex);

			if (floorSample + 1 >= waveData.Data.NumSamples)
			{
				return waveData.Data.ChannelData[0, waveData.Data.NumSamples - 1];
			}
			else
			{
				float prevSample16 = waveData.Data.ChannelData[0, floorSample];
				float nextSample16 = waveData.Data.ChannelData[0, floorSample + 1];
				return (prevSample16 + ((nextSample16 - prevSample16) * (sampleIndex - floorSample)));
			}
		}

		private void window_Closed(object sender, FormClosedEventArgs e)
		{
			if (m_GenerationThread != null &&
				m_GenerationThread.IsAlive)
			{
				m_GenerationThread.Abort();
			}

			if (m_ThreadCompleted && !m_DataSaved)
			{
				if (MessageBox.Show("Grain data has not been saved. Would you like to save before exiting?", "Save grain data", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					SaveData(false);
				}
			}
		}

		void CalculateZeroCrossings()
		{
			m_LongestGrainSoFar = 0;
			m_GranuleBarProgress = 0;

			float firstGrainPitch = FindNearestPitch(0);
			int firstBlockLengthMs = (int)Math.Round(1000.0f / firstGrainPitch);
			int firstBlockLength = (int)((m_WaveFile.Format.SampleRate / 1000) * firstBlockLengthMs);
			double leastError = float.MaxValue;
			int bestInitialMatch = 0;

			if (m_GrainData.Count == 0)
			{
				for (int loop = 0; loop < firstBlockLength; loop++)
				{
					double error = ((float)ReadSampleFromSampleIndex(loop) * (float)ReadSampleFromSampleIndex(loop));

					if (error < leastError)
					{
						leastError = error;
						bestInitialMatch = loop;
					}
				}

				GranularSubmix.audGrainData bestInitialMatchData = new GranularSubmix.audGrainData();
				bestInitialMatchData.startSample = bestInitialMatch;
				bestInitialMatchData.pitch = 0.0f;

				m_GrainData.Add(bestInitialMatchData);
				m_GrainData.Add(bestInitialMatchData);
			}

			float searchPercentage = (float)searchPercentageUpDown.Value; // How far we can deviate from the expected block end position
			float startEndFactor = (float)startEndBiasCombo.Value;        // How much significance to place on matching start/end DC values
			float zeroCrossingFactor = (float)zeroCrossingBiasCombo.Value;    // How much significance to place on start/ends being zero crossings

			while (m_GrainData.Last().startSample * sizeof(short) < m_WaveFile.Data.RawData.Length)
			{
				leastError = float.MaxValue;
				int prevGrainStart = m_GrainData[m_GrainData.Count - 2].startSample;
				int prevGrainEnd = m_GrainData[m_GrainData.Count - 1].startSample;
				int prevGrainLength = prevGrainEnd - prevGrainStart;

				float pitchScale = (float)pitchScaleUpDown.Value;
				float nearestPitch = FindNearestPitch(prevGrainEnd) * pitchScale;
				m_GranuleGenerationCursor = m_GrainData.Last().startSample;

				int nearestBlendedGrain = -1;
				float minPitchDiff = float.MaxValue;
				int blendedGrainIndex = 0;

				if (m_BlendedGrainData != null)
				{
					foreach (GranularSubmix.audGrainData grainData in m_BlendedGrainData)
					{
						float pitchDiff = Math.Abs(grainData.pitch - nearestPitch);

						if (pitchDiff < minPitchDiff)
						{
							minPitchDiff = pitchDiff;
							nearestBlendedGrain = blendedGrainIndex;
						}

						blendedGrainIndex++;
					}
				}

				float blockLengthMs = 1000.0f / nearestPitch;
				int blockLength = (int)((m_WaveFile.Format.SampleRate / 1000) * blockLengthMs);
				int searchRange = (int)((blockLength * searchPercentage * pitchScale) / 100);

				int probableMatch = prevGrainEnd + blockLength;
				int minMatch = probableMatch - searchRange;
				int maxMatch = probableMatch + searchRange;
				int bestMatch = probableMatch;

				float previousMatchBias = (float)previousMatchBiasUpDown.Value;
				float previousMatchStartPreferenceBias = (float)previousMatchStartPreferenceBiasUpDown.Value;

				for (int match = minMatch; match < maxMatch; match++)
				{
					float expectedMatchBias = 1.0f + Math.Abs((float)expectedMatchBiasUpDown.Value * (1.0f - Math.Abs((probableMatch - match) / (float)(probableMatch - minMatch))));
					double error = 0;

					if(m_GrainData.Count > 2)
					{
						int thisGrainLength = match - prevGrainEnd;
						float percentageDiff = (((float)Math.Max(thisGrainLength, prevGrainLength) / (float)Math.Min(thisGrainLength, prevGrainLength)) - 1.0f) * 100.0f;
						percentageDiff /= (float)Math.Min(thisGrainLength, prevGrainLength); // Scale this so it take into account long and short grains
						percentageDiff *= 100.0f;

						if(percentageDiff > (float)interGrainPitchDifference.Value)
						{
							continue;
						}
					}

					// Look for zero crossings
					float zeroCrossingError = ((float)ReadSampleFromSampleIndex(match) * (float)ReadSampleFromSampleIndex(match)) * zeroCrossingFactor;

					// Add a bias towards ending on the same value as we started ...
					float startEndError = ((float)ReadSampleFromSampleIndex(prevGrainEnd) - (float)ReadSampleFromSampleIndex(match)) * startEndFactor;

					if (m_GrainData.Count > 2)
					{
						for (int loop = 0; loop < prevGrainLength; loop++)
						{
							float granularFraction = loop / (float)prevGrainLength;
							double prevSample = (double)ReadSampleFromGranularFraction(prevGrainStart, prevGrainEnd, granularFraction, m_WaveFile);
							double newSample = (double)ReadSampleFromGranularFraction(prevGrainEnd, match, granularFraction, m_WaveFile);
							float previousMatchBiasThisSample = previousMatchBias + (previousMatchStartPreferenceBias * (1.0f - granularFraction));

							error -= expectedMatchBias;
							error -= previousMatchBiasThisSample * (prevSample * newSample);
							error += zeroCrossingError;
							error += startEndError;

							/*
							if (nearestBlendedGrain >= 0)
							{
								int blendedGrainStart = m_BlendedGrainData[nearestBlendedGrain].startSample;
								int blendedGrainEnd = m_BlendedGrainData[nearestBlendedGrain + 1].startSample;
								double blendedGrainSample = (double)ReadSampleFromGranularFraction(blendedGrainStart, blendedGrainEnd, granularFraction, m_BlendedWaveFile);
								error -= ((double)blendedDataUpdDown.Value) * (newSample * blendedGrainSample);
							}
							*/
						}
					}
					else
					{
						error -= expectedMatchBias * 10.0f; // Bump this up to start with
						error += zeroCrossingError;
						error += startEndError;
					}

					if (error < leastError)
					{
						leastError = error;
						bestMatch = match;
					}
				}

				if (bestMatch == prevGrainEnd)
				{
					break;
				}

				if (bestMatch * sizeof(short) < m_WaveFile.Data.RawData.Length)
				{
					GranularSubmix.audGrainData bestMatchData = new GranularSubmix.audGrainData();
					bestMatchData.startSample = bestMatch;
					bestMatchData.pitch = 0.0f;
					m_GrainData.Add(bestMatchData);
				}
				else
				{
					break;
				}

				m_GranuleBarProgress = (int)(100 * ((float)GetFinalGrainPos() * sizeof(short) / (float)m_WaveFile.Data.RawData.Length));
			}

			m_ThreadCompleted = true;
			m_DataSaved = false;
			m_GranuleBarProgress = 100;

			// Get rid of the very first position, as its just a dummy marker to get us started
			m_GrainData.RemoveAt(0);


			// OLD STYLE
			/*
			m_LongestGrainSoFar = 0;
			m_GranuleBarProgress = 0;

			m_GrainData.Clear();

			float searchPercentage = (float)searchPercentageUpDown.Value; // How far we can deviate from the expected block end position
			float startEndFactor = (float)startEndBiasCombo.Value;        // How much significance to place on matching start/end DC values
			float zeroCrossingFactor = (float)zeroCrossingBiasCombo.Value;    // How much significance to place on start/ends being zero crossings

			while (GetFinalGrainPos() * sizeof(short) < m_WaveFile.Data.RawData.Length)
			{
				int previous = GetFinalGrainPos();

				float nearestPitch = FindNearestPitch(previous) * (float)pitchScaleUpDown.Value;
				m_GranuleGenerationCursor = previous;

				int blockLengthMs = (int)Math.Round(1000.0f / nearestPitch);
				int blockLength = (int)((m_WaveFile.Format.SampleRate / 1000) * blockLengthMs);
				int searchRange = (int)((blockLength * searchPercentage) / 100);

				int probableMatch = previous + blockLength;
				int minMatch = probableMatch - searchRange;
				int maxMatch = probableMatch + searchRange;
				int bestMatch = probableMatch;
				double leastError = float.MaxValue;

				int nearestBlendedGrain = -1;
				float minPitchDiff = float.MaxValue;
				int blendedGrainIndex = 0;

				if(m_BlendedGrainData != null)
				{
					foreach(GranularSubmix.audGrainData grainData in m_BlendedGrainData)
					{
						float pitchDiff = Math.Abs(grainData.pitch - nearestPitch);

						if(pitchDiff < minPitchDiff)
						{
							minPitchDiff = pitchDiff;
							nearestBlendedGrain = blendedGrainIndex;
						}

						blendedGrainIndex++;
					}
				}

				for (int match = minMatch; match < maxMatch; match++)
				{
					float expectedMatchBias = 1.0f + Math.Abs((float)expectedMatchBiasUpDown.Value * (1.0f - Math.Abs((probableMatch - match) / (float)(probableMatch - minMatch))));

					double error = 0;
					int convolutionPeriod = match - previous;

					for (int i = 0; i < convolutionPeriod; i++)
					{
						double prevSample = (double)ReadSampleFromSampleIndex(previous + i);
						double newSample = (double)ReadSampleFromSampleIndex(match + i);

						error -= expectedMatchBias * (prevSample * newSample);

						if (nearestBlendedGrain >= 0)
						{
							float granularFraction = i / (float)convolutionPeriod;
							int grainStart = m_BlendedGrainData[nearestBlendedGrain].startSample;
							int grainEnd = m_BlendedGrainData[nearestBlendedGrain + 1].startSample;
							double blendedGrainSample = (double)ReadSampleFromGranularFraction(grainStart, grainEnd, granularFraction, m_BlendedWaveFile);
							error -= ((double)blendedDataUpdDown.Value) * (newSample * blendedGrainSample);
						}
					}

					// Get error per sample
					error /= (float)convolutionPeriod;

					// Add a bias towards ending on the same value as we started ...
					float diff = (float)ReadSampleFromSampleIndex(previous) - (float)ReadSampleFromSampleIndex(match);
					float diff2 = diff * diff; // Kind of in the same sort of units as the 'error' as that is now per sample
					error += diff2 * startEndFactor;

					// Look for zero crossings?
					error += ((float)ReadSampleFromSampleIndex(match) * (float)ReadSampleFromSampleIndex(match)) * zeroCrossingFactor;

					if (error < leastError)
					{
						leastError = error;
						bestMatch = match;
					}
				}

				if (bestMatch == previous)
				{
					break;
				}

				if (bestMatch * sizeof(short) < m_WaveFile.Data.RawData.Length)
				{
					m_GrainData.Add(bestMatch);
				}
				else
				{
					break;
				}
				
				m_GranuleBarProgress = (int)(100 * ((float)GetFinalGrainPos() * sizeof(short) / (float)m_WaveFile.Data.RawData.Length));
			}

			m_ThreadCompleted = true;
			m_DataSaved = false;
			m_GranuleBarProgress = 100;
			*/
		}

		public float FindNearestPitch(int sampleIndex)
		{
			// Todo - can this be improved? Maybe work out average pitch across the 
			// entire grain?
			if(m_PitchData.Count > 0)
			{
				for (int loop = 0; loop < m_PitchData.Count - 1; loop++)
				{
					if (loop == 0 && sampleIndex < m_PitchData[loop].sampleIndex)
					{
						return m_PitchData[loop].pitch;
					}
					else
					{
						if (sampleIndex >= m_PitchData[loop].sampleIndex &&
							sampleIndex < m_PitchData[loop + 1].sampleIndex)
						{
							float proportion = (sampleIndex - m_PitchData[loop].sampleIndex) / (float)(m_PitchData[loop + 1].sampleIndex - m_PitchData[loop].sampleIndex);

							// Linear interpolation
							//return m_PitchData[loop].pitch + ((m_PitchData[loop + 1].pitch - m_PitchData[loop].pitch) * proportion);

							// Cosine interpolation
							float prevPrevPitch = m_PitchData[loop].pitch;
							float prevPitch = m_PitchData[loop].pitch;
							float nextPitch = m_PitchData[loop + 1].pitch;
							float nextNextPitch = m_PitchData[loop + 1].pitch;
								
							if(loop > 0)
							{
								prevPrevPitch = m_PitchData[loop - 1].pitch;
							}
							
							if(loop + 2 < m_PitchData.Count)
							{
								nextNextPitch = m_PitchData[loop + 2].pitch;
							}

							double mu = proportion;
							double mu2 = mu * mu;
							double a0 = nextNextPitch - nextPitch - prevPrevPitch + prevPitch;
							double a1 = prevPrevPitch - prevPitch - a0;
							double a2 = nextPitch - prevPrevPitch;
							double a3 = prevPitch;

							return (float)(a0 * mu * mu2 + a1 * mu2 + a2 * mu + a3);
						}
					}
				}

				return m_PitchData.Last().pitch;
			}

			return 0.0f;
		}

		public void AddPitchDataFront(int sampleIndex, float pitchValue)
		{
			GranularSubmix.audPitchData data = new GranularSubmix.audPitchData();
			data.pitch = pitchValue;
			data.sampleIndex = sampleIndex;
			m_PitchData.Insert(0, data);

			if (data.pitch > m_MaxPitch)
			{
				m_MaxPitch = data.pitch;
			}
		}

		public void AddPitchDataBack(int sampleIndex, float pitchValue)
		{
			GranularSubmix.audPitchData data = new GranularSubmix.audPitchData();
			data.pitch = pitchValue;
			data.sampleIndex = sampleIndex;
			m_PitchData.Add(data);

			if (data.pitch > m_MaxPitch)
			{
				m_MaxPitch = data.pitch;
			}
		}

		private void releaseObject(object obj)
		{
			System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
			obj = null;
		}

		private void redrawTimer_Tick(object sender, EventArgs e)
		{
			granuleProgressBar.Value = m_GranuleBarProgress;
			granuleProgressBar.Refresh();

			prevGrainView.Refresh();
			pitchView.Refresh();

			if(m_GenerationThread != null &&
				m_GenerationThread.IsAlive)
			{
				generateGranulesBtn.Text = "Cancel";
				searchPercentageUpDown.Enabled = false;
				startEndBiasCombo.Enabled = false;
				expectedMatchBiasUpDown.Enabled = false;
				previousMatchStartPreferenceBiasUpDown.Enabled = false;
				pitchScaleUpDown.Enabled = false;
				previousMatchBiasUpDown.Enabled = false;
				zeroCrossingBiasCombo.Enabled = false;
				interGrainPitchDifference.Enabled = false;
				previewGrainsBtn.Enabled = false;
				AutoSmoothPitchBtn.Enabled = false;
				saveAndQuitBtn.Enabled = false;
				generateFromCursorPos.Enabled = false;
			}
			else
			{
				generateGranulesBtn.Text = "Generate Granules";

				if (m_PitchData.Count > 0)
				{
					generateGranulesBtn.Enabled = true;
				}
				else
				{
					generateGranulesBtn.Enabled = false;
				}

				searchPercentageUpDown.Enabled = true;
				interGrainPitchDifference.Enabled = true;
				startEndBiasCombo.Enabled = true;
				zeroCrossingBiasCombo.Enabled = true;
				previousMatchBiasUpDown.Enabled = true;
				previousMatchStartPreferenceBiasUpDown.Enabled = true;
				expectedMatchBiasUpDown.Enabled = true;
				pitchScaleUpDown.Enabled = true;
				previewGrainsBtn.Enabled = true;
				AutoSmoothPitchBtn.Enabled = true;
				saveAndQuitBtn.Enabled = true;
				generateFromCursorPos.Enabled = true;
				m_GranuleBarProgress = 0;
			}
		}

		private void loadPitchDataToolStripMenuItem1_Click(object sender, EventArgs e)
		{
			loadPitchDataDlg.ShowDialog();
		}

		private void flattenSelectionToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Backup();
			SmoothSelection(1.0f);
		}

		private void smoothSelectionToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Backup();
			SmoothSelection(0.1f);
		}

		private void smoothMoreToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Backup();
			SmoothSelection(0.5f);
		}

		void SmoothSelection(float severity)
		{
			int regionStart = Math.Min(m_RegionSelectCursorStart, m_RegionSelectCursorEnd);
			int regionEnd = Math.Max(m_RegionSelectCursorStart, m_RegionSelectCursorEnd);

			if(regionEnd >= m_PitchData.Count)
			{
				regionEnd = m_PitchData.Count - 1;
			}

			if(regionStart < 0)
			{
				regionStart = 0;
			}

			int regionLength = regionEnd - regionStart;
			int index = 0;

			for (int loop = regionStart; loop < regionEnd; loop++)
			{
				float desiredPitch = m_PitchData[regionStart].pitch +
										 (m_PitchData[regionEnd].pitch - m_PitchData[regionStart].pitch) * (index / (float)regionLength);

				float pitchDifference = m_PitchData[loop].pitch - desiredPitch;

				m_PitchData[loop].pitch -= (pitchDifference * severity);

				index++;
			}
		}

		void ModifySelection(float amount)
		{
			Backup();

			int regionStart = Math.Min(m_RegionSelectCursorStart, m_RegionSelectCursorEnd);
			int regionEnd = Math.Max(m_RegionSelectCursorStart, m_RegionSelectCursorEnd);

			if (regionEnd > m_PitchData.Count)
			{
				regionEnd = m_PitchData.Count;
			}

			if (regionStart < 0)
			{
				regionStart = 0;
			}

			for (int loop = regionStart; loop < regionEnd; loop++)
			{
				m_PitchData[loop].pitch += amount;
			}
		}

		private void quitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		void Backup()
		{
			m_OldPitchData.Clear();
			foreach (GranularSubmix.audPitchData pitchData in m_PitchData)
			{
				GranularSubmix.audPitchData copiedData = new GranularSubmix.audPitchData();
				copiedData.pitch = pitchData.pitch;
				copiedData.sampleIndex = pitchData.sampleIndex;
				m_OldPitchData.Add(copiedData);
			}
		}

		void Undo()
		{
			if (m_OldPitchData.Count > 0)
			{
				m_PitchData.Clear();

				foreach (GranularSubmix.audPitchData pitchData in m_OldPitchData)
				{
					GranularSubmix.audPitchData copiedData = new GranularSubmix.audPitchData();
					copiedData.pitch = pitchData.pitch;
					copiedData.sampleIndex = pitchData.sampleIndex;
					m_PitchData.Add(copiedData);
				}

				m_OldPitchData.Clear();
			}
		}

		private void undoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Undo();
		}

		private void lowerPitchToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ModifySelection(-1);
		}

		private void raisePitchToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ModifySelection(1);
		}

		private bool SaveData(bool temp)
		{
			bool success = false;

			if(m_GrainData.Count() == 0)
			{
				MessageBox.Show("There is no grain data!", "Error!", MessageBoxButtons.OK);
				return success;
			}

			List<GranularSubmix.audGrainData> grainDataList = new List<GranularSubmix.audGrainData>();

			for (int loop = 0; loop < m_GrainData.Count; loop++)
			{
				GranularSubmix.audGrainData grainData = new GranularSubmix.audGrainData();
				grainData.startSample = m_GrainData[loop].startSample;

				if (m_GrainData[loop].startSample < m_PitchData[0].sampleIndex)
				{
					grainData.pitch = m_PitchData[0].pitch;
				}
				else if (m_GrainData[loop].startSample >= m_PitchData[m_PitchData.Count - 1].sampleIndex)
				{
					grainData.pitch = m_PitchData[m_PitchData.Count - 1].pitch;
				}
				else
				{
					for (int pitchLoop = 0; pitchLoop < m_PitchData.Count - 1; pitchLoop++)
					{
						if (m_GrainData[loop].startSample >= m_PitchData[pitchLoop].sampleIndex &&
						   m_GrainData[loop].startSample < m_PitchData[pitchLoop + 1].sampleIndex)
						{
							float fractionBetween = (m_GrainData[loop].startSample - m_PitchData[pitchLoop].sampleIndex) / (float)(m_PitchData[pitchLoop + 1].sampleIndex - m_PitchData[pitchLoop].sampleIndex);
							float actualPitch = m_PitchData[pitchLoop].pitch + ((m_PitchData[pitchLoop + 1].pitch - m_PitchData[pitchLoop].pitch) * fractionBetween);
							grainData.pitch = actualPitch;
							break;
						}
					}
				}

				grainDataList.Add(grainData);
			}

			string grainFileName = m_WaveFile.FileName.Replace(Path.GetExtension(m_WaveFile.FileName), ".grn");

			if (temp)
			{
				grainFileName = m_WaveFile.FileName.Replace(Path.GetExtension(m_WaveFile.FileName), "_temp.grn");
			}

			try
			{
				TextWriter textWriter = new StreamWriter(grainFileName);

				GranularSubmix.audGrainDataStruct saveData = new GranularSubmix.audGrainDataStruct();
				saveData.grainData = grainDataList;
				saveData.originalPitchData = m_OriginalPitchData;

				if (m_OriginalLoopData != null)
				{
					saveData.loopDefinitions = m_OriginalLoopData;
				}
				else
				{
					saveData.loopDefinitions = new List<GranularSubmix.audLoopDefinition>();
				}

				XmlSerializer serializer = new XmlSerializer(typeof(GranularSubmix.audGrainDataStruct));
				serializer.Serialize(textWriter, saveData);

				textWriter.Close();
				m_DataSaved = true;

				if(!temp)
					MessageBox.Show("Grain data has been saved successfully.", "Save Complete", MessageBoxButtons.OK);

				success = true;
			}
			catch (System.UnauthorizedAccessException e)
			{
				MessageBox.Show("Could not get permission to write to grain file " + grainFileName + ". If the file already exists, ensure that it has been checked out in Perforce, and does not have the read-only flag set.", "Error saving grain data", MessageBoxButtons.OK);
			}

			return success;
		}

		private void saveAndQuitBtn_Click(object sender, EventArgs e)
		{
			SaveData(false);
		}

		private void AutoSmoothPitchBtn_Click(object sender, EventArgs e)
		{
			Backup();

			int numSteps = 50;
			int pitchValuesInWindow = (int)(m_PitchData.Count / m_Xzoom);

			for(int loop = 0; loop < numSteps; loop++)
			{
				float fractionSelected = loop / (float) numSteps;
				float nextFractionSelected = loop + 1 / (float) numSteps; 

				m_RegionSelectCursorStart = (int)(pitchValuesInWindow * fractionSelected);
				m_RegionSelectCursorEnd = (int)(pitchValuesInWindow * nextFractionSelected);

				SmoothSelection(0.01f);
			}
		}

		private void loadBlendedDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			loadWaveFileDialog.ShowDialog();
		}

		private void loadWaveFileDialog_FileOk(object sender, CancelEventArgs e)
		{
			/*
			OpenFileDialog dialog = sender as OpenFileDialog;
			m_BlendedWaveFile = new bwWaveFile(dialog.FileName);

			string grainFileName = dialog.FileName.Replace(Path.GetExtension(dialog.FileName), ".grn");
			m_BlendedGrainData = new List<GranularSubmix.audGrainData>();

			if (System.IO.File.Exists(grainFileName))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(List<GranularSubmix.audGrainData>));
				TextReader textReader = new StreamReader(grainFileName);
				m_BlendedGrainData = (List<GranularSubmix.audGrainData>)serializer.Deserialize(textReader);
				textReader.Close();

				blendedDataUpdDown.Enabled = true;
			}
			 */
		}

		private void loadBlendedData_Click(object sender, EventArgs e)
		{
			loadWaveFileDialog.ShowDialog();
		}

		private void displayGrainPitches_CheckedChanged(object sender, EventArgs e)
		{

		}

		private void revertAllChangesToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (m_OriginalPitchData.Count > 0)
			{
				m_PitchData.Clear();

				foreach (GranularSubmix.audPitchData pitchData in m_OriginalPitchData)
				{
					GranularSubmix.audPitchData copiedData = new GranularSubmix.audPitchData();
					copiedData.pitch = pitchData.pitch;
					copiedData.sampleIndex = pitchData.sampleIndex;
					m_PitchData.Add(copiedData);
				}
			}
		}

		private void previewGrainsBtn_Click(object sender, EventArgs e)
		{
			if(SaveData(true))
			{
				string grainFileName = m_WaveFile.FileName.Replace(Path.GetExtension(m_WaveFile.FileName), "_temp.grn");
				GrainView view = new GrainView(m_WaveFile.FileName, grainFileName, false);
				view.HideRecalculateGrainButton();
				File.Delete(grainFileName);
				view.Show(this);
				view.Focus();
			}
		}

		private void loadPitchDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			loadPitchDataDlg.ShowDialog();
		}

		private void richTextBox1_TextChanged(object sender, EventArgs e)
		{

		}

		private void scaleToFitCheckBox_CheckedChanged(object sender, EventArgs e)
		{

		}

		private void fixedWidthComboBox_CheckedChanged(object sender, EventArgs e)
		{

		}

		private void richTextBox5_TextChanged(object sender, EventArgs e)
		{

		}

		private void expectedMatchBiasUpDown_ValueChanged(object sender, EventArgs e)
		{

		}

		private void numCylindersUpDown_ValueChanged(object sender, EventArgs e)
		{

		}

		private void label2_Click(object sender, EventArgs e)
		{

		}
	}
}
