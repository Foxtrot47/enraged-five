﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using Wavelib;
using NAudio.Dsp;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;

namespace GranularPlayback2
{
	public class GrainPlayer
	{
		public enum audGrainPlaybackStyle
		{
			PlaybackStyleLoopsAndGrains,
			PlaybackStyleLoopsOnly,
			PlaybackStyleGrainsOnly,
			PlaybackStyleDebug,
		};

		int kBufferSize = 20;
		int kMixBufNumSamples = 256;
		int kExpectedSampleRate = 44100;
		int kAverageBytesPerSec = 88200;

		Smoother m_loopVolumeScaleSmoother;
		int m_PlaybackBufferBytes;	
		int m_LastPlayPos;
		public bool m_IsPlaying;
		public float m_VolumeScale = 1.0f;
		public float m_PitchScale = 1.0f;

		List<GranularMix> m_GranularMixers;
		public audGrainPlaybackStyle m_GrainPlaybackStyle = audGrainPlaybackStyle.PlaybackStyleDebug;
		public float m_HertzStart;
		public float m_HertzEnd;

		float m_GranuleFractionPerSample;
		float m_XPrev;
		float m_XTarget;
		float m_XCurrent;
		float m_XValueStepPerSec;
		float m_PrevLoopVolumeScale;
		float m_CurrentLoopVolumeScale;
		float m_TargetLoopVolumeScale;
		float m_CurrentGranularFraction;
		float m_LoopVolumeChangePerSecond;
		float m_XValueSmoothRate = 1.0f;

		public float m_AccelVolume = 1.0f;
		public float m_DecelVolume = 1.0f;

		public float m_ChangeRateForMaxLoops = 0.0f;
		public float m_ChangeRateForMaxGrains = 1.0f;

		WaveOut m_WaveOut = new WaveOut();
		GrainProvider32 m_GrainProvider;

		public class GrainProvider32 : WaveProvider32
		{
			GrainPlayer m_GrainPlayer;

			public GrainProvider32(GrainPlayer grainPlayer)
			{
				m_GrainPlayer = grainPlayer;
			}

			public override int Read(float[] buffer, int offset, int sampleCount)
			{
				int playedSize = sampleCount;

				while (playedSize > 256)
				{
					m_GrainPlayer.GenerateFrame(256, buffer, offset);
					playedSize -= 256;
					offset += 256;
				}

				m_GrainPlayer.GenerateFrame(playedSize, buffer, offset);

				return sampleCount;
			}
		}

		/// <summary>
		/// Grain player constructor
		/// </summary>
		public GrainPlayer(Control owner)
		{
			m_HertzStart = 10.0f;
			m_HertzEnd = 70.0f;
			
			m_loopVolumeScaleSmoother = new Smoother();
			m_loopVolumeScaleSmoother.Init(1.0f, 0.1f);			

			m_GranularMixers = new List<GranularMix>();
		}

		public void Kill()
		{
			m_WaveOut.Stop();
		}

		/// <summary>
		/// Init a mix
		/// </summary>
		public void InitMix(string waveFileName, string grainFileName)
		{
			GranularMix granularMix = new GranularMix(waveFileName, grainFileName);
			m_GranularMixers.Add(granularMix);

			if(m_GranularMixers.Count == 1)
			{
				m_HertzStart = granularMix.GetGranularSubmix().m_GrainTable[0].pitch;
				m_HertzEnd = granularMix.GetGranularSubmix().m_GrainTable[granularMix.GetGranularSubmix().m_GrainTable.Count - 1].pitch;
			}
			
			if (System.IO.File.Exists("config.xml"))
			{
				XmlDocument configFile = null;
				configFile = new XmlDocument();
				configFile.Load("config.xml");
				m_WaveOut.DesiredLatency = int.Parse(configFile.DocumentElement.SelectSingleNode("Latency").InnerText);
				m_WaveOut.NumberOfBuffers = int.Parse(configFile.DocumentElement.SelectSingleNode("NumBuffers").InnerText);
			}
			else
			{
				m_WaveOut.DesiredLatency = 60;
				m_WaveOut.NumberOfBuffers = 2;
			}
			
			m_GrainProvider = new GrainProvider32(this);
			m_GrainProvider.SetWaveFormat(48000, 1);			
			m_WaveOut.Init(m_GrainProvider);

			m_WaveOut.Play();
		}

		/// <summary>
		/// Get a given mix
		/// </summary>
		public GranularMix GetGranularMix(int index)
		{
			return m_GranularMixers[index];
		}

		/// <summary>
		/// Get a given mix
		/// </summary>
		public int GetNumGranularMixes()
		{
			return m_GranularMixers.Count;
		}
				
		/// <summary>
		/// Generate a frame of data
		/// </summary>
		public void GenerateFrame(int playedSize, float[] outputBuffer, int offset)
		{
			if (playedSize == 0)
				return;
			
			// Tool only - variable sized buffer means we need to recalculate this
			float buffersPerSec = kExpectedSampleRate / (float) playedSize;
			float xValueStepPerBuffer = m_XValueStepPerSec / buffersPerSec;
			float loopVolumeChangePerBuffer = m_LoopVolumeChangePerSecond / buffersPerSec;
			float loopVolumeChangePerSample = loopVolumeChangePerBuffer / (float) playedSize;

			float[] destBuffer = new float[playedSize];

			// Given our input, work out what this corresponds to in terms of step rate
			float currentRateHz = 0.0f;

			if (m_HertzStart < m_HertzEnd)
			{
				currentRateHz = m_HertzStart + ((m_HertzEnd - m_HertzStart) * m_XCurrent);
			}
			else
			{
				currentRateHz = m_HertzStart - ((m_HertzStart - m_HertzEnd) * m_XCurrent);
			}

			float samplesPerGranule = kExpectedSampleRate / currentRateHz;
			float granuleFractionPerBuffer = playedSize / samplesPerGranule;
			m_GranuleFractionPerSample = granuleFractionPerBuffer / playedSize;
			m_GranuleFractionPerSample *= m_PitchScale;

			// Work out desired volumes for loops/grains
			float desiredLoopVolumeScale = m_CurrentLoopVolumeScale;
			float desiredGrainVolumeScale = 1.0f - m_CurrentLoopVolumeScale;

			if (m_GrainPlaybackStyle == audGrainPlaybackStyle.PlaybackStyleGrainsOnly)
			{
				desiredLoopVolumeScale = 0.0f;
				desiredGrainVolumeScale = 1.0f;
			}
			else if (m_GrainPlaybackStyle == audGrainPlaybackStyle.PlaybackStyleLoopsOnly)
			{
				desiredLoopVolumeScale = 1.0f;
				desiredGrainVolumeScale = 0.0f;
			}
			else if (m_GrainPlaybackStyle == audGrainPlaybackStyle.PlaybackStyleDebug)
			{
				desiredLoopVolumeScale = 0.0f;
				desiredGrainVolumeScale = 0.0f;
			}

			if (m_IsPlaying)
			{
				for (int loop = 0; loop < m_GranularMixers.Count; loop++)
				{
					float accelDecelVolume = m_AccelVolume;

					if (m_GranularMixers[loop].GetGranularSubmix().m_GrainTable[0].pitch > m_GranularMixers[loop].GetGranularSubmix().m_GrainTable[m_GranularMixers[loop].GetGranularSubmix().m_GrainTable.Count - 1].pitch)
					{
						accelDecelVolume = m_DecelVolume;
					}

					m_GranularMixers[loop].GenerateFrame(m_VolumeScale * accelDecelVolume, m_GrainPlaybackStyle, destBuffer, currentRateHz, desiredLoopVolumeScale, desiredGrainVolumeScale, m_CurrentGranularFraction, m_GranuleFractionPerSample);
				}
			}

			m_CurrentGranularFraction += (m_GranuleFractionPerSample * playedSize);

			while (m_CurrentGranularFraction > 1.0f)
			{
				m_CurrentGranularFraction -= 1.0f;
			}

			// Okay, we've done our mixing, so now move the X position onwards
			m_XCurrent += xValueStepPerBuffer;

			// Clamp back to the target position if we overshoot
			if (m_XTarget > m_XPrev)
			{
				if (m_XCurrent > m_XTarget)
				{
					m_XCurrent = m_XTarget;
				}
			}
			else
			{
				if (m_XCurrent < m_XTarget)
				{
					m_XCurrent = m_XTarget;
				}
			}

			// Do the same for the loop/grain volume mix
			m_CurrentLoopVolumeScale += loopVolumeChangePerSample * playedSize;

			// Clamp back to the target position if we overshoot
			if (m_TargetLoopVolumeScale > m_PrevLoopVolumeScale)
			{
				if (m_CurrentLoopVolumeScale > m_TargetLoopVolumeScale)
				{
					m_CurrentLoopVolumeScale = m_TargetLoopVolumeScale;
				}
			}
			else
			{
				if (m_CurrentLoopVolumeScale < m_TargetLoopVolumeScale)
				{
					m_CurrentLoopVolumeScale = m_TargetLoopVolumeScale;
				}
			}

			WriteToBuffer(destBuffer, outputBuffer, offset);
		}

		/// <summary>
		/// Write the given data to the output buffer
		/// </summary>
		void WriteToBuffer(float[] data, float[] outputbuffer, int offset)
		{
			List<byte> finalData = new List<byte>();

			float clipFix = 1.0f / GetNumGranularMixes();
			clipFix *= 0.8f;

			for (int loop = 0; loop < data.Length; loop++)
			{				
				outputbuffer[offset + loop] = data[loop] * clipFix;
			}						
		}

		/// <summary>
		/// Set the x value on the mixer
		/// </summary>
		public void SetXValue(float val, float timeStep)
		{
			m_XPrev = m_XCurrent;
			m_XTarget = val;
			m_XValueStepPerSec = (((m_XTarget - m_XPrev) * m_XValueSmoothRate)) / timeStep;
			float hzChangeRate = (m_HertzEnd - m_HertzStart) * m_XValueStepPerSec;
			m_PrevLoopVolumeScale = m_CurrentLoopVolumeScale;
			m_TargetLoopVolumeScale = m_loopVolumeScaleSmoother.CalculateValue(1.0f - Clamp((Math.Abs(hzChangeRate) - m_ChangeRateForMaxLoops) / (m_ChangeRateForMaxGrains - m_ChangeRateForMaxLoops), 0.0f, 1.0f));
			m_LoopVolumeChangePerSecond = (m_TargetLoopVolumeScale - m_PrevLoopVolumeScale) / timeStep;
		}

		/// <summary>
		/// Clamp a value
		/// </summary>
		public static float Clamp(float value, float min, float max)
		{
			return (value < min) ? min : (value > max) ? max : value;
		}
	}
}
