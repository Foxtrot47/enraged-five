﻿namespace GranularPlayback2
{
	partial class Main
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
			this.generateGrainsButton = new System.Windows.Forms.Button();
			this.auditionGrainsButton = new System.Windows.Forms.Button();
			this.simulateEngineButton = new System.Windows.Forms.Button();
			this.loadWaveFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.loadFileForAuditionDialog = new System.Windows.Forms.OpenFileDialog();
			this.button1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.blendButton = new System.Windows.Forms.Button();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.convertMetadataFormat = new System.Windows.Forms.OpenFileDialog();
			this.generateNPCDialog = new System.Windows.Forms.OpenFileDialog();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			this.SuspendLayout();
			// 
			// generateGrainsButton
			// 
			this.generateGrainsButton.Location = new System.Drawing.Point(6, 19);
			this.generateGrainsButton.Name = "generateGrainsButton";
			this.generateGrainsButton.Size = new System.Drawing.Size(314, 28);
			this.generateGrainsButton.TabIndex = 7;
			this.generateGrainsButton.Text = "Perform Frequency Analysis";
			this.generateGrainsButton.UseVisualStyleBackColor = true;
			this.generateGrainsButton.Click += new System.EventHandler(this.generateGrainsButton_Click);
			// 
			// auditionGrainsButton
			// 
			this.auditionGrainsButton.Location = new System.Drawing.Point(6, 121);
			this.auditionGrainsButton.Name = "auditionGrainsButton";
			this.auditionGrainsButton.Size = new System.Drawing.Size(314, 28);
			this.auditionGrainsButton.TabIndex = 8;
			this.auditionGrainsButton.Text = "Audition Granular Data";
			this.auditionGrainsButton.UseVisualStyleBackColor = true;
			this.auditionGrainsButton.Click += new System.EventHandler(this.auditionGrainsButton_Click);
			// 
			// simulateEngineButton
			// 
			this.simulateEngineButton.Location = new System.Drawing.Point(6, 221);
			this.simulateEngineButton.Name = "simulateEngineButton";
			this.simulateEngineButton.Size = new System.Drawing.Size(314, 28);
			this.simulateEngineButton.TabIndex = 9;
			this.simulateEngineButton.Text = "Simulate Engine";
			this.simulateEngineButton.UseVisualStyleBackColor = true;
			this.simulateEngineButton.Click += new System.EventHandler(this.simulateEngineButton_Click);
			// 
			// loadWaveFileDialog
			// 
			this.loadWaveFileDialog.Filter = "Wave files (*.wav)|*.wav";
			this.loadWaveFileDialog.Title = "Select .wav file";
			this.loadWaveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.loadWaveFileDialog_FileOk);
			// 
			// loadFileForAuditionDialog
			// 
			this.loadFileForAuditionDialog.FileName = "openFileDialog1";
			this.loadFileForAuditionDialog.Filter = "Wave files (*.wav)|*.wav";
			this.loadFileForAuditionDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.loadFileForAuditionDialog_FileOk);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(6, 56);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(314, 28);
			this.button1.TabIndex = 10;
			this.button1.Text = "Generate Test Sweep";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(23, 50);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(281, 13);
			this.label1.TabIndex = 11;
			this.label1.Text = "Use the frequency tracking tool to analyse a wave file and";
			this.label1.Click += new System.EventHandler(this.label1_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(24, 63);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(280, 13);
			this.label2.TabIndex = 12;
			this.label2.Text = "generate the raw grain data required for granular playback";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(11, 152);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(293, 13);
			this.label3.TabIndex = 13;
			this.label3.Text = "Preview the generated grain data, edit playback settings and";
			this.label3.Click += new System.EventHandler(this.label3_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(41, 165);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(242, 13);
			this.label4.TabIndex = 14;
			this.label4.Text = " manually mark up any desired synchronised loops";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(27, 252);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(286, 13);
			this.label5.TabIndex = 15;
			this.label5.Text = "Play multiple granularised sounds simultaneously to simulate";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(48, 265);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(245, 13);
			this.label6.TabIndex = 16;
			this.label6.Text = "the behaviour of the in-game vehicle engine model";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.pictureBox3);
			this.groupBox1.Controls.Add(this.pictureBox1);
			this.groupBox1.Controls.Add(this.generateGrainsButton);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.auditionGrainsButton);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.simulateEngineButton);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Location = new System.Drawing.Point(12, 335);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(326, 293);
			this.groupBox1.TabIndex = 17;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Granularisation Process";
			// 
			// pictureBox3
			// 
			this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
			this.pictureBox3.Location = new System.Drawing.Point(155, 181);
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size(18, 36);
			this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox3.TabIndex = 18;
			this.pictureBox3.TabStop = false;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(155, 79);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(18, 36);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 17;
			this.pictureBox1.TabStop = false;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.button3);
			this.groupBox2.Controls.Add(this.button2);
			this.groupBox2.Controls.Add(this.blendButton);
			this.groupBox2.Controls.Add(this.button1);
			this.groupBox2.Location = new System.Drawing.Point(12, 637);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(326, 158);
			this.groupBox2.TabIndex = 18;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Debug Tools";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(6, 22);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(314, 28);
			this.button3.TabIndex = 13;
			this.button3.Text = "Generate NPC Engines";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(6, 124);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(314, 28);
			this.button2.TabIndex = 12;
			this.button2.Text = "Convert Metadata To New Format";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// blendButton
			// 
			this.blendButton.Location = new System.Drawing.Point(6, 90);
			this.blendButton.Name = "blendButton";
			this.blendButton.Size = new System.Drawing.Size(314, 28);
			this.blendButton.TabIndex = 11;
			this.blendButton.Text = "Blend Recordings";
			this.blendButton.UseVisualStyleBackColor = true;
			this.blendButton.Click += new System.EventHandler(this.blendButton_Click);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(355, 24);
			this.menuStrip1.TabIndex = 19;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
			this.aboutToolStripMenuItem.Text = "About";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// pictureBox2
			// 
			this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
			this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pictureBox2.Location = new System.Drawing.Point(12, 27);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(326, 302);
			this.pictureBox2.TabIndex = 6;
			this.pictureBox2.TabStop = false;
			// 
			// convertMetadataFormat
			// 
			this.convertMetadataFormat.Filter = "Wave files (*.wav)|*.wav";
			this.convertMetadataFormat.Title = "Select .wav file";
			this.convertMetadataFormat.FileOk += new System.ComponentModel.CancelEventHandler(this.convertMetadataFormat_FileOk);
			// 
			// generateNPCDialog
			// 
			this.generateNPCDialog.Filter = "Wave files (*.wav)|*.wav";
			this.generateNPCDialog.Title = "Select .wav file";
			this.generateNPCDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.generateNPCDialog_FileOk);
			// 
			// Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(355, 807);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Main";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "GEARS Tool";
			this.Load += new System.EventHandler(this.Main_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Button generateGrainsButton;
		private System.Windows.Forms.Button auditionGrainsButton;
		private System.Windows.Forms.Button simulateEngineButton;
		private System.Windows.Forms.OpenFileDialog loadWaveFileDialog;
		private System.Windows.Forms.OpenFileDialog loadFileForAuditionDialog;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.Button blendButton;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.OpenFileDialog convertMetadataFormat;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.OpenFileDialog generateNPCDialog;

	}
}