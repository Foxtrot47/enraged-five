﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GranularPlayback2
{
	public class Smoother
	{
		float m_IncreaseRate;
		float m_DecreaseRate;
		float m_Maximum;
		float m_Minimum;

		float m_LastValue;
		int m_LastTime;

		bool m_Initialised;
		bool m_FirstRequest;
		bool m_BoundsSet;

		public Smoother()
		{
			m_Initialised = false;
			m_FirstRequest = true;
			m_BoundsSet = false;

			m_IncreaseRate = 0.0f;
			m_DecreaseRate = 0.0f;
			m_Minimum = 0.0f;
			m_Maximum = 0.0f;

			m_LastValue = 0.0f;
			m_LastTime = 0;
		}

		public void Init(float increaseRate, float decreaseRate)
		{
			m_IncreaseRate = increaseRate;
			m_DecreaseRate = decreaseRate;

			m_FirstRequest = true;
			m_BoundsSet = false;
			m_Initialised = true;
		}

		public void Init(float increaseRate, float decreaseRate, float minimum, float maximum)
		{
			m_IncreaseRate = increaseRate;
			m_DecreaseRate = decreaseRate;
			m_Minimum = minimum;
			m_Maximum = maximum;

			m_FirstRequest = true;
			m_BoundsSet = true;
			m_Initialised = true;
		}

		public void SetRates(float increaseRate, float decreaseRate)
		{
			m_IncreaseRate = increaseRate;
			m_DecreaseRate = decreaseRate;
		}

		public void SetBounds(float minimum, float maximum)
		{
			m_Minimum = minimum;
			m_Maximum = maximum;
			m_BoundsSet = true;
		}

		public float CalculateValue(float input)
		{
			// Ensure we're initialised
			if (!m_Initialised)
			{
				return 0.0f;
			}

			// If it's our first request, return the input, so we don't smooth up from zero. If you want to do that, do a
			// CalculateValue(0.0f - or whatever) straight after initialisation.
			if (m_FirstRequest)
			{
				m_LastValue = input;
				m_FirstRequest = false;
				return input;
			}

			float newValueIncreasing = Math.Min(input, (m_LastValue + m_IncreaseRate));
			float newValueDecreasing = Math.Max(input, (m_LastValue - m_DecreaseRate));
			float newValue = Selectf(input - m_LastValue, newValueIncreasing, newValueDecreasing);

			if (m_BoundsSet)
			{
				newValue = Clamp(newValue, m_Minimum, m_Maximum);
			}

			m_LastValue = newValue;

			return newValue;
		}

		public float CalculateValue(float input, int time)
		{
			// Ensure we're initialised
			if (!m_Initialised)
			{
				return 0.0f;
			}

			// If it's our first request, return the input, so we don't smooth up from zero. If you want to do that, do a
			// CalculateValue(0.0f - or whatever) straight after initialisation.
			if (m_FirstRequest)
			{
				m_LastValue = input;
				m_LastTime = time;
				m_FirstRequest = false;
				return input;
			}


			float maxIncrease = (time - m_LastTime) * m_IncreaseRate;
			float maxDecrease = (time - m_LastTime) * m_DecreaseRate;
			float newValueIncreasing = Math.Min(input, (m_LastValue + maxIncrease));
			float newValueDecreasing = Math.Max(input, (m_LastValue - maxDecrease));
			float newValue = Selectf(input - m_LastValue, newValueIncreasing, newValueDecreasing);

			if (m_BoundsSet)
			{
				newValue = Clamp(newValue, m_Minimum, m_Maximum);
			}

			m_LastValue = newValue;
			m_LastTime = time;

			return newValue;
		}

		public float Clamp(float t, float a, float b)
		{
			return (t < a) ? a : (t > b) ? b : t;
		}

		public int Clamp(int t, int a, int b)
		{
			return (t < a) ? a : (t > b) ? b : t;
		}

		float Selectf(float condition, float valGE, float valLT)
		{
			return (condition >= 0.0f ? valGE : valLT);
		}
	}
}
