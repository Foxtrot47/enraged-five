﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;

namespace GranularPlayback2
{
	public partial class Main : Form
	{
		public Main()
		{
			InitializeComponent();
		}

		private void generateGrainsButton_Click(object sender, EventArgs e)
		{
			loadWaveFileDialog.ShowDialog();
		}

		private void loadWaveFileDialog_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;

			if (dialog != null)
			{
				PitchAnalysis anaylsis = new PitchAnalysis(dialog.FileName);
				anaylsis.Show(this);
				anaylsis.Focus();
			}
		}

		private void auditionGrainsButton_Click(object sender, EventArgs e)
		{
			loadFileForAuditionDialog.ShowDialog();
		}

		private void loadFileForAuditionDialog_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;

			if (dialog != null)
			{
				string waveFileName = dialog.FileName;
				string grainFileName = waveFileName.Replace(Path.GetExtension(waveFileName), ".grn");
				GrainView view = new GrainView(waveFileName, grainFileName, true);
				view.Show(this);
				view.Focus();
			}
		}

		private void simulateEngineButton_Click(object sender, EventArgs e)
		{
			EngineSim sim = new EngineSim();
			sim.Show(this);
			sim.Focus();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			SweepGenerator generator = new SweepGenerator();
			generator.Show();
			generator.Focus();
		}

		private void Main_Load(object sender, EventArgs e)
		{

		}

		private void label1_Click(object sender, EventArgs e)
		{

		}

		private void label3_Click(object sender, EventArgs e)
		{

		}

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			About about = new About();
			about.Show(this);
			about.Focus();
		}

		private void blendButton_Click(object sender, EventArgs e)
		{
			GranularBlender blender = new GranularBlender();
			blender.Show(this);
			blender.Focus();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			convertMetadataFormat.ShowDialog();
		}

		private void convertMetadataFormat_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;

			if (dialog != null)
			{
				string waveFileName = dialog.FileName;
				string grainFileName = waveFileName.Replace(Path.GetExtension(waveFileName), ".grn");

				if (System.IO.File.Exists(grainFileName))
				{
					GranularSubmix.audGrainDataStruct saveStruct = new GranularSubmix.audGrainDataStruct();

					XmlSerializer serializer = new XmlSerializer(typeof(List<GranularSubmix.audGrainData>));
					TextReader textReader = new StreamReader(grainFileName);
					saveStruct.grainData = (List<GranularSubmix.audGrainData>)serializer.Deserialize(textReader);
					textReader.Close();

					saveStruct.originalPitchData = new List<GranularSubmix.audPitchData>();
					int numSamples = saveStruct.grainData.Last().startSample;

					for (int sampleIndex = 0; sampleIndex < numSamples; sampleIndex += 250)
					{
						for (int loop = 0; loop < saveStruct.grainData.Count - 1; loop++)
						{
							if (sampleIndex > saveStruct.grainData[loop].startSample &&
							   sampleIndex <= saveStruct.grainData[loop + 1].startSample)
							{
								GranularSubmix.audPitchData newPitchData = new GranularSubmix.audPitchData();
								float fractionBetween = (sampleIndex - saveStruct.grainData[loop].startSample) / (float)(saveStruct.grainData[loop + 1].startSample - saveStruct.grainData[loop].startSample);
								newPitchData.pitch = saveStruct.grainData[loop].pitch + ((saveStruct.grainData[loop + 1].pitch - saveStruct.grainData[loop].pitch) * fractionBetween);
								newPitchData.sampleIndex = sampleIndex;
								saveStruct.originalPitchData.Add(newPitchData);
							}
						}
					}

					saveStruct.loopDefinitions = new List<GranularSubmix.audLoopDefinition>();

					string loopFileName = waveFileName.Replace(Path.GetExtension(waveFileName), ".lpd");

					if (System.IO.File.Exists(loopFileName))
					{
						textReader = new System.IO.StreamReader(loopFileName);
						string line = textReader.ReadLine();
						int numLoops = Convert.ToInt32(line);

						for (int loop = 0; loop < numLoops; loop++)
						{
							GranularSubmix.audLoopDefinition loopDefinition = new GranularSubmix.audLoopDefinition();

							line = textReader.ReadLine();
							loopDefinition.playbackOrder = (GranularSubmix.audGrainPlaybackOrder)Convert.ToInt32(line);
							loopDefinition.validGrains = new List<int>();

							line = textReader.ReadLine();
							int numGrains = Convert.ToInt32(line);

							line = textReader.ReadLine();
							uint submixTypeHash = Convert.ToUInt32(line);

							List<int> selectedGrains = new List<int>();
							for (int grain = 0; grain < numGrains; grain++)
							{
								line = textReader.ReadLine();
								int grainIndex = Convert.ToInt32(line);
								loopDefinition.validGrains.Add(grainIndex);
							}

							loopDefinition.submixIdentifier = "Loop";
							saveStruct.loopDefinitions.Add(loopDefinition);
						}

						textReader.Close();
					}

					TextWriter textWriter = new StreamWriter(grainFileName);
					XmlSerializer serializer2 = new XmlSerializer(typeof(GranularSubmix.audGrainDataStruct));
					serializer2.Serialize(textWriter, saveStruct);

					textWriter.Close();
					MessageBox.Show("Metadata converted succesfully!", "Success!", MessageBoxButtons.OK);
				}
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			generateNPCDialog.ShowDialog();
		}

		private void generateNPCDialog_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;
			string fileName = dialog.FileName;

			string directoryName = fileName.Replace(Path.GetFileName(fileName), "");
			directoryName += "NPC\\";

			if(!Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}

			string waveFileName = fileName.Replace(Path.GetFileNameWithoutExtension(fileName), "ENGINE_ACCEL");
			if(File.Exists(waveFileName))
			{
				string grainFileName = waveFileName.Replace(Path.GetExtension(waveFileName), ".grn");
				GrainView view = new GrainView(waveFileName, grainFileName, true);
				view.CreateNPCVersion(directoryName + "ENGINE_ACCEL.WAV");
			}

			waveFileName = fileName.Replace(Path.GetFileNameWithoutExtension(fileName), "EXHAUST_ACCEL");
			if (File.Exists(waveFileName))
			{
				string grainFileName = waveFileName.Replace(Path.GetExtension(waveFileName), ".grn");
				GrainView view = new GrainView(waveFileName, grainFileName, true);
				view.CreateNPCVersion(directoryName + "EXHAUST_ACCEL.WAV");
			}

			waveFileName = fileName.Replace(Path.GetFileNameWithoutExtension(fileName), "ENGINE_IDLE");
			if(File.Exists(waveFileName))
			{
				string grainFileName = waveFileName.Replace(Path.GetExtension(waveFileName), ".grn");
				File.Copy(waveFileName, directoryName + "ENGINE_IDLE.WAV");
				File.Copy(grainFileName, directoryName + "ENGINE_IDLE.grn");
			}

			waveFileName = fileName.Replace(Path.GetFileNameWithoutExtension(fileName), "EXHAUST_IDLE");
			if (File.Exists(waveFileName))
			{
				string grainFileName = waveFileName.Replace(Path.GetExtension(waveFileName), ".grn");
				File.Copy(waveFileName, directoryName + "EXHAUST_IDLE.WAV");
				File.Copy(grainFileName, directoryName + "EXHAUST_IDLE.grn");
			}
		}
	}
}
