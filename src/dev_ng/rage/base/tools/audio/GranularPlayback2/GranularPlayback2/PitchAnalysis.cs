﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using System.Threading;

using MathNet.Numerics;
using MathNet.Numerics.Transformations;
using Wavelib;

namespace GranularPlayback2
{
	public partial class PitchAnalysis : Form
	{
		public enum WindowType
		{
			BlackmanHarris,
			Hann,
			Hamming,
			SineCubed,
			Sine,
			Triangle,
			Rectangle,
			Max
		};

		private bwWaveFile m_WaveFile;
		private float[] m_YFourierSignal;
		private float[] m_PrevYFourierSignal;
		private float m_FourierGraphStepRate;
		private int m_PreviewCursorPosInSamples = 0;
		private float m_RPMCursor; 
		private int m_LastFFTDrawPos;
		private float m_LastFourierScaleValue;
		private int m_WindowingFunctionIndex = 0;
		private float m_PitchResolution = 0.0f;
		private List<GranularSubmix.audPitchData> m_PitchData;
		private WindowType m_WindowType = WindowType.BlackmanHarris;

		int m_FftSampleLength = 2048;
		bool m_WaveCursorBeingDragged = false;
		bool m_RpmCursorBeingDragged = false;
		bool m_ShowGrainDialog = false;
		Thread m_GenerationThread;
		bool m_GenerationThreadCancelled = false;
		private ComplexFourierTransformation m_ComplexFourierTransform;
		private RealFourierTransformation m_RealFourierTransform;

		float m_HannWindowStrengthLastRedraw = 0.0f;
		float m_HannWindowCentreLastRedraw = 0.0f;
		float m_HannWindowWidthLastRedraw = 0.0f;

		int m_ThreadPitchTrackingStart = 0;
		int m_ThreadPitchTrackingEnd = 0;

		public static List<AnalysisFrame> s_AnalysisFrames = new List<AnalysisFrame>();
		float[] m_SpectrogramSampleData;
		float[] m_WaveSampleData;
		int m_Padding;
		int m_SpectrogramVisibleFrequency = -1;
		float m_SpectrogramWidth;
		float m_SpectrogramHeight;
		float m_SpectrogramVolumeScale;

		double[] m_WindowValues;

		public PitchAnalysis(string fileName)
		{
			m_WaveFile = new bwWaveFile(fileName);
			m_PitchData = new List<GranularSubmix.audPitchData>();
			m_ComplexFourierTransform = new ComplexFourierTransformation();
			m_RealFourierTransform = new RealFourierTransformation();
			m_WindowValues = GenerateAnalysisWindow(2048, false);
			InitializeComponent();
			AnalyseCursorPos();
			this.Text = "Frequency Tracking - " + fileName;
			UpdateAnalysisFrames();

			int padding;
			m_WaveSampleData = GetWaveSampleData(m_WaveFile, 0, 0, false, out padding);
		}

		private void UpdateAnalysisFrames()
		{
			s_AnalysisFrames.Clear();
			int frameSize = m_FftSampleLength;
			int hopSize = 1024;			
			m_Padding = 0;

			m_SpectrogramSampleData = GetWaveSampleData(m_WaveFile, frameSize, hopSize, true, out m_Padding);
			double[] analysisWindow = GenerateAnalysisWindow(frameSize, true);
			
			int expectedFrames = (int)Math.Ceiling((m_SpectrogramSampleData.Length - frameSize) / (float)hopSize);
			s_AnalysisFrames.Clear();

			int sampleIndex = 0;

			while (sampleIndex + frameSize < m_SpectrogramSampleData.Length)
			{
				AnalysisFrame analysisFrame = new AnalysisFrame();
				analysisFrame.Analyse(m_SpectrogramSampleData, analysisWindow, sampleIndex, frameSize, 2, m_WaveFile.Format.SampleRate);
				s_AnalysisFrames.Add(analysisFrame);
				sampleIndex += hopSize;
			}

			// Force a re-draw of the spectrogram
			m_SpectrogramVisibleFrequency = -1;
		}
		
		private static float[] GetWaveSampleData(bwWaveFile waveFile, int frameSize, int hopSize, bool pad, out int padding)
		{
			uint numSamples = waveFile.Data.NumSamples;

			if (pad)
			{
				padding = frameSize;
			}
			else
			{
				padding = 0;
			}

			float[] sampleData = new float[numSamples + frameSize + frameSize];

			// Pad the start of the source data with silence
			for (int sample = 0; sample < numSamples; sample++)
			{
				sampleData[sample + padding] = waveFile.Data.ChannelData[0, sample];
			}

			return sampleData;
		}

		static ulong upper_power_of_two(ulong v)
		{
			v--;
			v |= v >> 1;
			v |= v >> 2;
			v |= v >> 4;
			v |= v >> 8;
			v |= v >> 16;
			v++;
			return v;
		}

		double[] GenerateAnalysisWindow(int length)
		{
			double[] windowingFunction = new double[length];

			// Apply a window to the data
			switch (m_WindowingFunctionIndex)
			{
				case 0: // Blackman-Harris
					double fConst = 2.0 * Math.PI / (length);

					for (int loop = 0; loop < length; loop++)
					{
						windowingFunction[loop] = 0.35875 - 0.48829 * Math.Cos(fConst * (loop + 1)) + 0.14128 * Math.Cos(fConst * 2.0 * (loop + 1)) - 0.01168 * Math.Cos(fConst * 3.0 * (loop + 1));
					}
					break;
				case 1: // Hann
					for (int loop = 0; loop < length; loop++)
					{
						windowingFunction[loop] = 0.5f * (1 - Math.Cos((2.0f * Math.PI * loop) / (length)));
					}
					break;
				case 2: // Hamming
					for (int loop = 0; loop < length; loop++)
					{
						windowingFunction[loop] = 0.54 - 0.46 * (Math.Cos((2.0f * Math.PI * loop) / (length)));
					}
					break;
				case 3: // SineCubed
					for (int loop = 0; loop < length; loop++)
					{
						windowingFunction[loop] = Math.Pow(Math.Sin((Math.PI * (loop / (double)length))), 3.0);
					}
					break;
				case 4: // Sine
					for (int loop = 0; loop < length; loop++)
					{
						windowingFunction[loop] = Math.Sin((Math.PI * (loop / (double)length)));
					}
					break;
				case 5: // Triangle
					for (int loop = 0; loop < length; loop++)
					{
						windowingFunction[loop] = 1.0 - (Math.Abs((loop - length / 2.0) / (double)length) / 0.5);
					}
					break;
				case 6: // Rectangle
					for (int loop = 0; loop < length; loop++)
					{
						windowingFunction[loop] = 0.97f;
					}
					break;
			}

			return windowingFunction;
		}

		/// <summary>
		/// Fourier analyse the given sets of data
		/// </summary>
		/// <param name="data"></param>
		/// <param name="pastData"></param>
		public void Analyse(double[] data, double[] pastData)
		{
			ulong numFFTPoints = (ulong) m_WaveFile.Format.SampleRate;
			numFFTPoints = upper_power_of_two(numFFTPoints);
			float fourierScale = (float)fourierScaleUpDown.Value;
			double[] hannWindow = GenerateAnalysisWindow(data.Length, false);// new double[data.Length];

			Complex[] complexData = new Complex[numFFTPoints];
			Complex[] prevComplexData = new Complex[numFFTPoints];

			// Apply a hann window to the data
			if (m_ApplyHannWindow)
			{
				for (int loop = 0; loop < data.Length; loop++)
				{
					//hannWindow[loop] = 0.5f * (1 - Math.Cos((2.0f * Math.PI * loop) / (data.Length)));
					complexData[loop] = data[loop] * hannWindow[loop];
					prevComplexData[loop] = pastData[loop] * hannWindow[loop];
				}
			}
			else
			{
				complexData[loop] = data[loop] * windowingFunction[loop];
				prevComplexData[loop] = pastData[loop] * windowingFunction[loop];
			}

			m_ComplexFourierTransform.Convention = TransformationConvention.Matlab;
			m_ComplexFourierTransform.TransformForward(complexData);
			m_ComplexFourierTransform.TransformForward(prevComplexData);

			m_YFourierSignal = new float[numFFTPoints / 2];
			m_PrevYFourierSignal = new float[numFFTPoints / 2];
			m_FourierGraphStepRate = m_WaveFile.Format.SampleRate / (float)numFFTPoints;

			for (int loop = 0; loop < (int)numFFTPoints / 2; loop++)
			{
				m_YFourierSignal[loop] = (float)complexData[loop].Modulus;
				m_YFourierSignal[loop] *= 2.0f;
				m_YFourierSignal[loop] *= fourierScale;

				m_PrevYFourierSignal[loop] = (float)prevComplexData[loop].Modulus;
				m_PrevYFourierSignal[loop] *= 2.0f;
				m_PrevYFourierSignal[loop] *= fourierScale;
			}
		}

		/// <summary>
		/// Convert a hz value to a number of samples
		/// </summary>
		/// <param name="hz"></param>
		/// <param name="sampleRate"></param>
		/// <returns></returns>
		private static int hzToPeriodInSamples(double hz, int sampleRate)
		{
			return (int)(1 / (hz / (double)sampleRate));
		}

		/// <summary>
		/// Convert a number of samples to hertz
		/// </summary>
		/// <param name="period"></param>
		/// <param name="sampleRate"></param>
		/// <returns></returns>
		private static double periodInSamplesToHz(int period, int sampleRate)
		{
			return 1 / (period / (double)sampleRate);
		}

		/// <summary>
		/// Round to a given value
		/// </summary>
		/// <param name="input"></param>
		/// <param name="roundValue"></param>
		/// <returns></returns>
		private static float Round2(double input, double roundValue)
		{
			return (float) (Math.Round(input/roundValue) * roundValue);
		}

		/// <summary>
		/// Threaded pitch tracking process
		/// </summary>
		public void TrackPitch()
		{
			m_PitchData.Clear();

			if(m_PitchResolution > 0.0f)
			{
				m_ThreadPitchTrackingStart = m_PreviewCursorPosInSamples;
				m_ThreadPitchTrackingEnd = m_PreviewCursorPosInSamples;

				double[] windowingFunction = GenerateAnalysisWindow(m_FftSampleLength, false);

				TrackPitchDirection(false, windowingFunction);
				TrackPitchDirection(true, windowingFunction);

				m_ThreadPitchTrackingStart = 0;
				m_ThreadPitchTrackingEnd = 0;
			}
		}
		
		/// <summary>
		/// Track the pitch in a given direction (forward or back) from the marked position
		/// </summary>
		/// <param name="forwards"></param>
		public void TrackPitchDirection(bool forwards, double[] windowingFunction)
		{
			// HL: This function is written in a slightly odd way, as I've basically delved into the original
			// matlab code and reproduced exactly what it was doing there line by line so as to ensure this tool behaves identically.
			// At some point it might be worth tidying this up (reducing the for loops, better variable names, etc.), but only once
			// we're sure that everything is behaving as expected
			List<int> pitchMapOffsets = new List<int>();
			List<double> pitchMapValues = new List<double>();

			int stepSize = (int)stepSizeUpDown.Value;
			int frameSizeSamples = m_FftSampleLength;

			int startIndex = m_PreviewCursorPosInSamples;
			int endIndex = startIndex + frameSizeSamples;

			int upperBoundHz = (int)upperBoundUpDown.Value;
			int lowerBoundHz = (int)lowerBoundUpDown.Value;			

			float maxShift = (float)Math.Round(m_WaveFile.Format.SampleRate / (m_PitchResolution/(float)numCylindersUpDown.Value)) * 2;
			float maxFreq = (float)Math.Round(m_WaveFile.Format.SampleRate / ((m_PitchResolution / (float)numCylindersUpDown.Value) + upperBoundHz));
			float minFreq = (float)Math.Round(m_WaveFile.Format.SampleRate / ((m_PitchResolution / (float)numCylindersUpDown.Value) - lowerBoundHz));
			int numFFTPoints = (int)upper_power_of_two((ulong)((2 * frameSizeSamples) - 1));

			double[] complexDataModulusSquaredInput = new double[numFFTPoints];
			double[] complexDataModulusSquared = new double[numFFTPoints];
			double[] complexDataDummy = new double[numFFTPoints];

			while (startIndex >= 0 && endIndex < m_WaveFile.Data.ChannelData.Length)
			{
				// Start of MATLAB pitchEstimateXcorr() implementation
				Complex[] complexData = new Complex[numFFTPoints];

				for(int loop = 0; loop < frameSizeSamples; loop++)
				{
					complexData[loop] = m_WaveFile.Data.ChannelData[0, startIndex + loop] * windowingFunction[loop];
				}

				// Start of MATLAB xCorr() implementationX
				m_ComplexFourierTransform.Convention = TransformationConvention.Matlab;
				m_ComplexFourierTransform.TransformForward(complexData);

				for (int loop = 0; loop < numFFTPoints; loop++)
				{
					complexDataModulusSquaredInput[loop] = complexData[loop].ModulusSquared;
				}

				m_RealFourierTransform.Convention = TransformationConvention.Matlab;
				m_RealFourierTransform.TransformBackward(complexDataModulusSquaredInput, complexDataDummy, out complexDataModulusSquared);

				List<double> finalValues = new List<double>();

				if(maxShift >= frameSizeSamples)
				{
					for (int loop = 0; loop < maxShift - frameSizeSamples; loop++)
					{
						finalValues.Add(0.0f);
					}

					for (int loop = numFFTPoints - frameSizeSamples; loop < numFFTPoints; loop++)
					{
						finalValues.Add(complexDataModulusSquared[loop]);
					}

					for (int loop = 0; loop < frameSizeSamples; loop++)
					{
						finalValues.Add(complexDataModulusSquared[loop]);
					}

					for (int loop = 0; loop < maxShift - frameSizeSamples; loop++)
					{
						finalValues.Add(0.0f);
					}
				}
				else
				{
					for (int loop = (int)(complexDataModulusSquared.Length - maxShift); loop < complexDataModulusSquared.Length; loop++)
					{
						finalValues.Add(complexDataModulusSquared[loop]);
					}

					for (int loop = 0; loop < maxShift; loop++)
					{
						finalValues.Add(complexDataModulusSquared[loop]);
					}
				}

				for (int loop = 0; loop < finalValues.Count; loop++)
				{
					finalValues[loop] /= finalValues[(int)maxShift];
				}
				// End of MATLAB xCorr() implementation

				List<double> testValues = new List<double>();

				for(int loop = (int)maxShift; loop < 2 * maxShift; loop++ )
				{
					testValues.Add(finalValues[loop]);
				}

				double maxOffsetValue = double.MinValue;
				int maxOffsetIndex = -1;
				int index = 0;
				for (int loop = (int)maxFreq; loop < minFreq; loop++)
				{
					if (testValues[loop] > maxOffsetValue)
					{
						maxOffsetValue = testValues[loop];
						maxOffsetIndex = index;
					}

					index++;
				}

				double xCorrOffset = Math.Max(2, maxFreq + maxOffsetIndex);
				double finalPitchValue = (m_WaveFile.Format.SampleRate / (xCorrOffset - 1));
				pitchMapOffsets.Add(startIndex);
				pitchMapValues.Add(finalPitchValue);
				// End of MATLAB pitchEstimateXcorr() implementation

				GranularSubmix.audPitchData data = new GranularSubmix.audPitchData();
				data.pitch = (float)finalPitchValue;
				data.sampleIndex = startIndex + (frameSizeSamples / 2);

				if (forwards)
				{
					m_PitchData.Add(data);
					startIndex += stepSize;
				}
				else
				{
					m_PitchData.Insert(0, data);
					startIndex -= stepSize;
				}

				endIndex = startIndex + frameSizeSamples;
				maxShift = (float)Math.Round(m_WaveFile.Format.SampleRate / Round2(finalPitchValue, 0.1)) * 4;
				maxFreq = (float)Math.Round(m_WaveFile.Format.SampleRate / (finalPitchValue + upperBoundHz));
				minFreq = (float)Math.Round(m_WaveFile.Format.SampleRate / (finalPitchValue - lowerBoundHz));

				// This is just for debug rendering purposes
				if (startIndex < m_ThreadPitchTrackingStart)
				{
					m_ThreadPitchTrackingStart = startIndex;
				}

				if (endIndex > m_ThreadPitchTrackingEnd)
				{
					m_ThreadPitchTrackingEnd = endIndex;
				}
			}
		}

		/// <summary>
		/// Analyse the data at the current cursor position
		/// </summary>
		void AnalyseCursorPos()
		{
			if(m_WaveFile != null)
			{
				m_FftSampleLength = Convert.ToInt32(fftSampleLengthCombo.Text.ToString());
				double[] data = new double[m_FftSampleLength];
				double[] pastData = new double[m_FftSampleLength];
				int index = 0;

				for (int loop = m_PreviewCursorPosInSamples; loop < m_PreviewCursorPosInSamples + m_FftSampleLength; loop++)
				{
					data[index] = m_WaveFile.Data.ChannelData[0, loop];

					int prevDataIndex = loop - m_FftSampleLength;

					if (prevDataIndex >= 0)
					{
						pastData[index] = m_WaveFile.Data.ChannelData[0, loop - m_FftSampleLength];
					}
					else
					{
						pastData[index] = 0.0f;
					}

					index++;
				}

				Analyse(data, pastData);
			}
		}

		/// <summary>
		/// Redraw the form, reanalyse cursor pos if needed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void redrawTimer_Tick(object sender, EventArgs e)
		{			
			fourierTransform.Refresh();			
			wavePreviewBox.Refresh();
			waveSpectrogram.Refresh();

			if(m_GenerationThread == null ||
			   !m_GenerationThread.IsAlive )
			{
				if (m_ShowGrainDialog)
				{
					m_ShowGrainDialog = false;
					trackPitchBtn.Text = "Track Pitch";
					suggestFrequencyBtn.Enabled = true;
					numCylindersUpDown.Enabled = true;
					fourierScaleUpDown.Enabled = true;
					upperBoundUpDown.Enabled = true;
					lowerBoundUpDown.Enabled = true;
					stepSizeUpDown.Enabled = true;
					fftSampleLengthCombo.Enabled = true;
					visibleFreqRangeUpDown.Enabled = true;
					windowingFnCombo.Enabled = true;
					displayRPMValues.Enabled = true;

					if(!m_GenerationThreadCancelled)
					{
						GrainGenerator generator = new GrainGenerator(m_WaveFile, m_PitchData, null, null, (int)numCylindersUpDown.Value);
						generator.Show(this);
						generator.Focus();
					}
					else
					{
						m_GenerationThreadCancelled = false;
						m_ThreadPitchTrackingStart = 0;
						m_ThreadPitchTrackingEnd = 0;
					}
				}
				else if (m_LastFFTDrawPos != m_PreviewCursorPosInSamples ||
						 m_LastFourierScaleValue != (float)fourierScaleUpDown.Value)
				{
					AnalyseCursorPos();
					m_LastFFTDrawPos = m_PreviewCursorPosInSamples;
					m_LastFourierScaleValue = (float)fourierScaleUpDown.Value;
				}
			}
		}

		/// <summary>
		/// Adjust the wave preview cursor position
		/// </summary>
		/// <param name="xCoord"></param>
		private void AdjustWavePreviewCursor(int xCoord, int boxWidth)
		{
			m_FftSampleLength = Convert.ToInt32(fftSampleLengthCombo.Text.ToString()); ;
			float fractionSelected = xCoord / (float)boxWidth;
			m_PreviewCursorPosInSamples = (int)(m_WaveFile.Data.NumSamples * fractionSelected);

			if (m_PreviewCursorPosInSamples < 0)
			{
				m_PreviewCursorPosInSamples = 0;
			}
			else if (m_PreviewCursorPosInSamples + m_FftSampleLength >= m_WaveFile.Data.NumSamples)
			{
				m_PreviewCursorPosInSamples = (int)(m_WaveFile.Data.NumSamples - m_FftSampleLength);
			}
		}

		/// <summary>
		/// Callback for dragging the wave view cursor
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void wavePreview_MouseMove(object sender, MouseEventArgs e)
		{
			if(m_WaveCursorBeingDragged)
			{
				AdjustWavePreviewCursor(e.Location.X - 40, wavePreviewBox.Width - 40);
			}
		}

		/// <summary>
		/// Callback for clicking the wave view
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void wavePreview_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_WaveCursorBeingDragged = false;
			}
		}

		/// <summary>
		/// Callback for clicking the wave view
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void wavePreview_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_WaveCursorBeingDragged = true;
				AdjustWavePreviewCursor(e.Location.X - 40, wavePreviewBox.Width - 40);
			}
		}

		/// <summary>
		/// Callback for dragging the wave view cursor
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void waveSpectrogram_MouseMove(object sender, MouseEventArgs e)
		{
			if (m_WaveCursorBeingDragged)
			{
				AdjustRPMCursor(waveSpectrogram.Height - 40 - e.Location.Y, waveSpectrogram.Height - 40);
				AdjustWavePreviewCursor(e.Location.X - 40, waveSpectrogram.Width - 40);
			}
		}

		/// <summary>
		/// Callback for clicking the wave view
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void waveSpectrogram_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_WaveCursorBeingDragged = false;
			}
		}

		/// <summary>
		/// Callback for clicking the wave view
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void waveSpectrogram_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_WaveCursorBeingDragged = true;
				AdjustRPMCursor(waveSpectrogram.Height - 40 - e.Location.Y, waveSpectrogram.Height - 40);
				AdjustWavePreviewCursor(e.Location.X - 40, waveSpectrogram.Width - 40);
			}
		}

		/// <summary>
		/// Callback for clicking the fourier transform
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fourierTransform_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				m_RpmCursorBeingDragged = true;
				AdjustRPMCursor(e.Location.X - 40, fourierTransform.Width - 40);
			}
		}

		/// <summary>
		/// Callback for clicking the fourier transform
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fourierTransform_MouseUp(object sender, MouseEventArgs e)
		{
			m_RpmCursorBeingDragged = false;
		}

		/// <summary>
		/// Callback for dragging the mouse over the fourier transform
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void fourierTransform_MouseMove(object sender, MouseEventArgs e)
		{
			if(m_RpmCursorBeingDragged)
			{
				AdjustRPMCursor(e.Location.X - 40, fourierTransform.Width - 40);
			}
		}

		/// <summary>
		/// Adjust the RPM cursor
		/// </summary>
		/// <param name="xCoord"></param>
		private void AdjustRPMCursor(int xCoord, int windowWidth)
		{
			float fractionSelected = xCoord / (float)(windowWidth);
			float selectedHertz = (float)visibleFreqRangeUpDown.Value * fractionSelected;

			if (selectedHertz < 0)
			{
				selectedHertz = 0;
			}
			else if (selectedHertz > 1000)
			{
				selectedHertz = 1000;
			}

			m_PitchResolution = selectedHertz;
			m_RPMCursor = selectedHertz * 120.0f;
		}

		/// <summary>
		/// Update the fourier transform view
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="pea"></param>
		private void fourierTransform_Paint(object sender, PaintEventArgs pea)
		{
			if(m_YFourierSignal == null)
			{
				return;
			}

			Graphics g = pea.Graphics;
			System.Windows.Forms.PictureBox parent = sender as System.Windows.Forms.PictureBox;
			Drawing.DrawHorizontalFrequencyAxis(g, parent, (int)visibleFreqRangeUpDown.Value, true, displayRPMValues.Checked, (int)numCylindersUpDown.Value);
			Drawing.DrawAmplitudeAxis(g, parent);

			Int32 lastx = 0, lasty = 0, lastyInv = 0;
			var p = Pens.DarkBlue;
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;	

			var gp = new GraphicsPath();
			var invgp = new GraphicsPath();

			float maxFourierX = (float)visibleFreqRangeUpDown.Value;

			for (var i = 0; i * m_FourierGraphStepRate < maxFourierX; i++)
			{
				float transform = m_YFourierSignal[i];
				transform /= 1600.0f;
				transform = AnalysisFrame.Clamp(transform, 0.0f, 0.5f);

				// now 0 -> 1.0f
				var y = (Int32)((0.5f + transform) * (visBounds.Height - 40));
				var yInv = (Int32)((0.5f - transform) * (visBounds.Height - 40));
				var x = (Int32)(((i * m_FourierGraphStepRate) / maxFourierX) * (visBounds.Width - 40)) + 40;

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
					invgp.AddLine(lastx, lastyInv, x, yInv);
				}

				lastx = x;
				lasty = y;
				lastyInv = yInv;
			}

			// draw paths
			grfx.DrawPath(p, gp);
			grfx.DrawPath(p, invgp);

			Pen transparentBlue = new Pen(Color.FromArgb(96, 0, 0, 255));

			if (m_PitchResolution > 0.0f)
			{
				for (float xCoord = 0; xCoord < maxFourierX; xCoord += (m_PitchResolution / (float)numCylindersUpDown.Value))
				{
					int linePos = (int)((xCoord / maxFourierX) * (visBounds.Width - 40)) + 40;

					grfx.DrawLine(transparentBlue,
							   linePos,
							   0,
							   linePos,
							   visBounds.Height - 40);
				}
			}

			// draw cursor position
			int cursorXPos = (int)(((m_RPMCursor / 120.0f) / maxFourierX) * (visBounds.Width - 40)) + 40;

			grfx.DrawLine(Pens.Red,
					   cursorXPos,
					   0,
					   cursorXPos,
					   visBounds.Height - 40);

			Font arial = new Font(new FontFamily("Arial"), 16, FontStyle.Bold);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(210, 255, 255, 255)),
					42,
					2,
					150.0f,
					20.0f);

			float sampleTime = m_PreviewCursorPosInSamples / (float)m_WaveFile.Format.SampleRate;
			grfx.DrawString(((int)((m_PitchResolution * 120.0f) /  (float)numCylindersUpDown.Value)).ToString() + " RPM", arial, textBrush, new PointF(44.0f, 1.0f));			
		}

		private void windowingFnGraph_Paint(object sender, PaintEventArgs pea)
		{
			var p = Pens.DarkBlue;
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			var prevX = 0.0f;
			var prevY = 0.0f;

			grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(255, 255, 255)),
							0,
							0,
							visBounds.Width,
							visBounds.Height);

			var gp = new GraphicsPath();

			for (int sample = 0; sample < m_WindowValues.Length; sample++)
			{
				float xFraction = sample / (float)m_WindowValues.Length;
				float xCoord = xFraction * visBounds.Width;

				var yFraction = 1.0f - (float)m_WindowValues[sample];
				float yCoord = yFraction * visBounds.Height;

				if (sample == 0)
				{
					prevX = xCoord;
					prevY = yCoord;
				}

				gp.AddLine(prevX, prevY, xCoord, yCoord);
				prevX = xCoord;
				prevY = yCoord;
			}

			grfx.DrawPath(p, gp);
		}

		/// <summary>
		/// Update the fourier view Hz axis
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="pea"></param>
		private void fourierXAxis_Paint(object sender, PaintEventArgs pea)
		{
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			Font arial = new Font(new FontFamily("Arial"), 10, FontStyle.Regular);
			SolidBrush textBrush = new SolidBrush(Color.Black);

			for (int loop = 0; loop < 10; loop++)
			{
				float xCoord = visBounds.Width * loop / 10.0f;
				float thisHz = ((float)visibleFreqRangeUpDown.Value/10.0f) * loop;

				string timeString = String.Format("{0:0.###}", thisHz);
				grfx.DrawString(timeString, arial, textBrush, new PointF(xCoord + 1, 5));
				grfx.DrawLine(Pens.Black, xCoord, 5 + visBounds.Height / 2, xCoord, 0);
			}
		}

		/// <summary>
		/// Update the spectrogram view 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="pea"></param>
		private void waveSpectrogram_Paint(object sender, PaintEventArgs pea)
		{
			if (m_WaveFile == null)
			{
				return;
			}

			int maxVisibleFrequency = (int) visibleFreqRangeUpDown.Value;
			Graphics g = pea.Graphics;
			Drawing.DrawMillisecondsAxis(g, sender as System.Windows.Forms.PictureBox, (uint)m_WaveSampleData.Length, false, m_WaveFile.Format.SampleRate);
			Drawing.DrawFrequencyAxis(g, sender as System.Windows.Forms.PictureBox, (uint)maxVisibleFrequency * 2, displayRPMValues.Checked, (int)numCylindersUpDown.Value);

			// draw cursor position			
			var visBounds = g.VisibleClipBounds;

			if ((int)visibleFreqRangeUpDown.Value != m_SpectrogramVisibleFrequency ||
				visBounds.Width != m_SpectrogramWidth ||
				visBounds.Height != m_SpectrogramHeight ||
				(float)fourierScaleUpDown.Value != m_SpectrogramVolumeScale)
			{
				Drawing.DrawSpectrogram(s_AnalysisFrames, waveSpectrogram, m_WaveFile.Format.SampleRate, (uint)visibleFreqRangeUpDown.Value, (float)fourierScaleUpDown.Value);
				m_SpectrogramVisibleFrequency = (int) visibleFreqRangeUpDown.Value;

				m_SpectrogramWidth = visBounds.Width;
				m_SpectrogramHeight = visBounds.Height;
				m_SpectrogramVolumeScale = (float)fourierScaleUpDown.Value;
			}

			int cursorXPos = (int)((m_PreviewCursorPosInSamples / (float)m_WaveFile.Data.NumSamples) * (visBounds.Width - 40)) + 40;

			g.DrawLine(Pens.Black,
					   cursorXPos,
					   0,
					   cursorXPos,
					   visBounds.Height - 40);


			if (m_PitchResolution > 0.0f)
			{
				float maxFourierX = (float)visibleFreqRangeUpDown.Value;

				for (float frequency = 0; frequency < maxVisibleFrequency; frequency += m_PitchResolution / (float)numCylindersUpDown.Value)
				{
					int linePos = (int)((1.0f - (frequency / (float)maxVisibleFrequency)) * (visBounds.Height - 40));

					g.DrawLine(Pens.Black,
							   Math.Max(40, cursorXPos - 3),
							   linePos,
							   cursorXPos + 3,
							   linePos);
				}

				{
					int linePos = (int)((1.0f - ((m_RPMCursor / 120.0f) / (float)maxVisibleFrequency)) * (visBounds.Height - 40));

					g.DrawLine(Pens.Black,
							   cursorXPos - 10,
							   linePos,
							   cursorXPos + 10,
							   linePos);
				}				
			}

			g.DrawLine(Pens.Black,
					   cursorXPos,
					   0,
					   cursorXPos,
					   visBounds.Height - 40);
		}

		/// <summary>
		/// Update the wave view 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="pea"></param>
		private void wavePreview_Paint(object sender, PaintEventArgs pea)
		{
			if(m_WaveFile == null)
			{
				return;
			}

			Drawing.DrawWaveView(m_WaveSampleData, sender as System.Windows.Forms.PictureBox, pea, 0, m_WaveFile.Format.SampleRate);

			// draw cursor position
			var g = pea.Graphics;
			var visBounds = g.VisibleClipBounds;

			int cursorXPos = (int)((m_PreviewCursorPosInSamples / (float)m_WaveFile.Data.NumSamples) * (visBounds.Width - 40)) + 40;

			g.DrawLine(Pens.Black,
					   cursorXPos,
					   0,
					   cursorXPos,
					   visBounds.Height - 40);

			float processingStart = m_ThreadPitchTrackingStart / (float)m_WaveFile.Data.NumSamples;
			float processingEnd = m_ThreadPitchTrackingEnd / (float)m_WaveFile.Data.NumSamples;

			float processingStartX = (processingStart * (visBounds.Width - 40)) + 40;
			float processingEndX = (processingEnd * (visBounds.Width - 40)) + 40;

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 0, 255, 0)),
									processingStartX,
									0,
									processingEndX - processingStartX,
									visBounds.Height - 40);			
		}
		
		/// <summary>
		/// Callback for changing the cylinder count
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void numCylindersUpDown_ValueChanged(object sender, EventArgs e)
		{
			m_PitchResolution = (m_RPMCursor/120.0f);
		}

		/// <summary>
		/// Kick off a new pitch tracking thread
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void trackPitchBtn_Click(object sender, EventArgs e)
		{
			if (m_GenerationThread != null &&
				m_GenerationThread.IsAlive)
			{
				m_GenerationThreadCancelled = true;
				m_GenerationThread.Abort();
				m_GenerationThread = null;
			}
			else
			{
				trackPitchBtn.Text = "Cancel";
				suggestFrequencyBtn.Enabled = false;
				numCylindersUpDown.Enabled = false;
				fourierScaleUpDown.Enabled = false;
				upperBoundUpDown.Enabled = false;
				lowerBoundUpDown.Enabled = false;
				stepSizeUpDown.Enabled = false;
				fftSampleLengthCombo.Enabled = false;
				visibleFreqRangeUpDown.Enabled = false;
				windowingFnCombo.Enabled = false;
				displayRPMValues.Enabled = false;
				m_GenerationThread = new Thread(new ThreadStart(TrackPitch));
				m_GenerationThread.Start();
				m_ShowGrainDialog = true;
			}
		}

		private void suggestFrequencyBtn_Click(object sender, EventArgs e)
		{
			float proportionThroughWave = m_PreviewCursorPosInSamples / (float) m_WaveFile.Data.NumSamples;

			// At a rough estimate, most of our recording will probably be vehicles revving steadily from idle up to 8000-odd RPM,
			// so we know roughly the area in which we want to be looking
			//float estimatedRPM = 1500 + (7000 * proportionThroughWave);
			//int estimatedHz = (int)(estimatedRPM / 120.0f) * (int)numCylindersUpDown.Value;

			int estimatedHz = (int)m_PitchResolution;
			float maxTransform = 0.0f;
			int maxTransformHz = 0;

			for (var i = 0; i * m_FourierGraphStepRate < 1000; i++)
			{
				float transform = m_YFourierSignal[i];

				if (transform > maxTransform)
				{
					int hzValue = (Int32)((i * m_FourierGraphStepRate));

					// Make sure this lies near our expected results
					if(hzValue > estimatedHz - 50 &&
					   hzValue < estimatedHz + 50)
					{
						maxTransform = transform;
						maxTransformHz = hzValue;
					}
				}
			}

			m_PitchResolution = maxTransformHz;
			m_RPMCursor = maxTransformHz * 120.0f;
		}

		private void fftSampleLengthCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			AnalyseCursorPos();
			UpdateAnalysisFrames();			
		}

		private double[] GenerateAnalysisWindow(int frameSize, bool scaleBySum)
		{
			double[] analysisWindow = new double[frameSize];
			double fConst = 2.0 * Math.PI / (frameSize);
			double sumValue = 0.0;

			switch (m_WindowType)
			{
				case WindowType.Rectangle:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = 1.0f;
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowType.BlackmanHarris:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = 0.35875 - 0.48829 * Math.Cos(fConst * (sample + 1)) + 0.14128 * Math.Cos(fConst * 2.0 * (sample + 1)) - 0.01168 * Math.Cos(fConst * 3.0 * (sample + 1));
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowType.Hamming:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = 0.54 - 0.46 * (Math.Cos((2.0f * Math.PI * sample) / (frameSize)));
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowType.Hann:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = 0.5 * (1 - Math.Cos((2.0f * Math.PI * sample) / (frameSize)));
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowType.Sine:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = Math.Sin((Math.PI * (sample / (double)frameSize)));
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowType.SineCubed:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = Math.Pow(Math.Sin((Math.PI * (sample / (double)frameSize))), 3.0);
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowType.Triangle:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = 1.0 - (Math.Abs((sample - frameSize / 2.0) / (double)frameSize) / 0.5);
						sumValue += analysisWindow[sample];
					}
					break;
			}

			if (scaleBySum)
			{
				for (int sample = 0; sample < frameSize; sample++)
				{
					analysisWindow[sample] /= sumValue;
					analysisWindow[sample] *= 2.0f;
				}
			}			

			return analysisWindow;
		}

		private void windowingFnCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			m_WindowType = (WindowType)windowingFnCombo.SelectedIndex;
			m_WindowValues = GenerateAnalysisWindow(2048, false);
			windowingFnGraph.Refresh();
			UpdateAnalysisFrames();
			AnalyseCursorPos();
		}

		private void PitchAnalysis_Load(object sender, EventArgs e)
		{

		}

		private void windowingFnCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			m_WindowingFunctionIndex = windowingFnCombo.SelectedIndex;
			AnalyseCursorPos();
		}
	}
}
