﻿namespace GranularPlayback2
{
	partial class PitchAnalysis
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PitchAnalysis));
			this.redrawTimer = new System.Windows.Forms.Timer(this.components);
			this.wavePreviewBox = new System.Windows.Forms.PictureBox();
			this.visibleFreqRangeUpDown = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.suggestFrequencyBtn = new System.Windows.Forms.Button();
			this.fourierScaleUpDown = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.fourierTransform = new System.Windows.Forms.PictureBox();
			this.trackPitchBtn = new System.Windows.Forms.Button();
			this.numCylindersUpDown = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.upperBoundUpDown = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.lowerBoundLabel = new System.Windows.Forms.Label();
			this.lowerBoundUpDown = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.stepSizeUpDown = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this.fftSampleLengthCombo = new System.Windows.Forms.ComboBox();
			this.waveSpectrogram = new System.Windows.Forms.PictureBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.windowingFnGraph = new System.Windows.Forms.PictureBox();
			this.windowingFnCombo = new System.Windows.Forms.ComboBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.displayRPMValues = new System.Windows.Forms.CheckBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			((System.ComponentModel.ISupportInitialize)(this.wavePreviewBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.visibleFreqRangeUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fourierScaleUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fourierTransform)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numCylindersUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.upperBoundUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lowerBoundUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.stepSizeUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.waveSpectrogram)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.windowingFnGraph)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.SuspendLayout();
			// 
			// redrawTimer
			// 
			this.redrawTimer.Enabled = true;
			this.redrawTimer.Interval = 250;
			this.redrawTimer.Tick += new System.EventHandler(this.redrawTimer_Tick);
			// 
			// wavePreviewBox
			// 
			this.wavePreviewBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.wavePreviewBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.wavePreviewBox.Location = new System.Drawing.Point(6, 19);
			this.wavePreviewBox.Name = "wavePreviewBox";
			this.wavePreviewBox.Size = new System.Drawing.Size(1046, 152);
			this.wavePreviewBox.TabIndex = 1;
			this.wavePreviewBox.TabStop = false;
			this.wavePreviewBox.Paint += new System.Windows.Forms.PaintEventHandler(this.wavePreview_Paint);
			this.wavePreviewBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.wavePreview_MouseDown);
			this.wavePreviewBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.wavePreview_MouseMove);
			this.wavePreviewBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.wavePreview_MouseUp);
			// 
			// visibleFreqRangeUpDown
			// 
			this.visibleFreqRangeUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.visibleFreqRangeUpDown.Location = new System.Drawing.Point(139, 25);
			this.visibleFreqRangeUpDown.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
			this.visibleFreqRangeUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.visibleFreqRangeUpDown.Name = "visibleFreqRangeUpDown";
			this.visibleFreqRangeUpDown.Size = new System.Drawing.Size(65, 20);
			this.visibleFreqRangeUpDown.TabIndex = 9;
			this.visibleFreqRangeUpDown.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.BackColor = System.Drawing.SystemColors.Control;
			this.label6.Location = new System.Drawing.Point(6, 27);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(125, 13);
			this.label6.TabIndex = 8;
			this.label6.Text = "Visible Frequency Range";
			// 
			// suggestFrequencyBtn
			// 
			this.suggestFrequencyBtn.Location = new System.Drawing.Point(213, 19);
			this.suggestFrequencyBtn.Name = "suggestFrequencyBtn";
			this.suggestFrequencyBtn.Size = new System.Drawing.Size(110, 49);
			this.suggestFrequencyBtn.TabIndex = 7;
			this.suggestFrequencyBtn.Text = "Suggest Frequency";
			this.suggestFrequencyBtn.UseVisualStyleBackColor = true;
			this.suggestFrequencyBtn.Click += new System.EventHandler(this.suggestFrequencyBtn_Click);
			// 
			// fourierScaleUpDown
			// 
			this.fourierScaleUpDown.DecimalPlaces = 1;
			this.fourierScaleUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.fourierScaleUpDown.Location = new System.Drawing.Point(139, 51);
			this.fourierScaleUpDown.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
			this.fourierScaleUpDown.Name = "fourierScaleUpDown";
			this.fourierScaleUpDown.Size = new System.Drawing.Size(65, 20);
			this.fourierScaleUpDown.TabIndex = 6;
			this.fourierScaleUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.SystemColors.Control;
			this.label1.Location = new System.Drawing.Point(6, 53);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(83, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Amplitude Scale";
			// 
			// fourierTransform
			// 
			this.fourierTransform.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.fourierTransform.Location = new System.Drawing.Point(6, 19);
			this.fourierTransform.Margin = new System.Windows.Forms.Padding(3, 3, 6, 3);
			this.fourierTransform.Name = "fourierTransform";
			this.fourierTransform.Size = new System.Drawing.Size(1274, 296);
			this.fourierTransform.TabIndex = 0;
			this.fourierTransform.TabStop = false;
			this.fourierTransform.Paint += new System.Windows.Forms.PaintEventHandler(this.fourierTransform_Paint);
			this.fourierTransform.MouseDown += new System.Windows.Forms.MouseEventHandler(this.fourierTransform_MouseDown);
			this.fourierTransform.MouseMove += new System.Windows.Forms.MouseEventHandler(this.fourierTransform_MouseMove);
			this.fourierTransform.MouseUp += new System.Windows.Forms.MouseEventHandler(this.fourierTransform_MouseUp);
			// 
			// trackPitchBtn
			// 
			this.trackPitchBtn.Location = new System.Drawing.Point(213, 74);
			this.trackPitchBtn.Name = "trackPitchBtn";
			this.trackPitchBtn.Size = new System.Drawing.Size(110, 71);
			this.trackPitchBtn.TabIndex = 5;
			this.trackPitchBtn.Text = "Track Frequency";
			this.trackPitchBtn.UseVisualStyleBackColor = true;
			this.trackPitchBtn.Click += new System.EventHandler(this.trackPitchBtn_Click);
			// 
			// numCylindersUpDown
			// 
			this.numCylindersUpDown.Location = new System.Drawing.Point(131, 125);
			this.numCylindersUpDown.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.numCylindersUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numCylindersUpDown.Name = "numCylindersUpDown";
			this.numCylindersUpDown.Size = new System.Drawing.Size(66, 20);
			this.numCylindersUpDown.TabIndex = 5;
			this.numCylindersUpDown.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
			this.numCylindersUpDown.ValueChanged += new System.EventHandler(this.numCylindersUpDown_ValueChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 127);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(74, 13);
			this.label2.TabIndex = 7;
			this.label2.Text = "Num Cylinders";
			// 
			// upperBoundUpDown
			// 
			this.upperBoundUpDown.Location = new System.Drawing.Point(131, 48);
			this.upperBoundUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.upperBoundUpDown.Name = "upperBoundUpDown";
			this.upperBoundUpDown.Size = new System.Drawing.Size(66, 20);
			this.upperBoundUpDown.TabIndex = 1;
			this.upperBoundUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 50);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(103, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Max Deviation (+Hz)";
			// 
			// lowerBoundLabel
			// 
			this.lowerBoundLabel.AutoSize = true;
			this.lowerBoundLabel.Location = new System.Drawing.Point(6, 27);
			this.lowerBoundLabel.Name = "lowerBoundLabel";
			this.lowerBoundLabel.Size = new System.Drawing.Size(100, 13);
			this.lowerBoundLabel.TabIndex = 11;
			this.lowerBoundLabel.Text = "Max Deviation (-Hz)";
			// 
			// lowerBoundUpDown
			// 
			this.lowerBoundUpDown.Location = new System.Drawing.Point(131, 22);
			this.lowerBoundUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.lowerBoundUpDown.Name = "lowerBoundUpDown";
			this.lowerBoundUpDown.Size = new System.Drawing.Size(66, 20);
			this.lowerBoundUpDown.TabIndex = 0;
			this.lowerBoundUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 74);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(99, 13);
			this.label5.TabIndex = 13;
			this.label5.Text = "Step Size (samples)";
			// 
			// stepSizeUpDown
			// 
			this.stepSizeUpDown.Location = new System.Drawing.Point(131, 72);
			this.stepSizeUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
			this.stepSizeUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.stepSizeUpDown.Name = "stepSizeUpDown";
			this.stepSizeUpDown.Size = new System.Drawing.Size(66, 20);
			this.stepSizeUpDown.TabIndex = 2;
			this.stepSizeUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(6, 101);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(100, 13);
			this.label7.TabIndex = 15;
			this.label7.Text = "FFT Sample Length";
			// 
			// fftSampleLengthCombo
			// 
			this.fftSampleLengthCombo.FormattingEnabled = true;
			this.fftSampleLengthCombo.Items.AddRange(new object[] {
            "1024",
            "2048",
            "4096",
            "8192",
            "16384",
            "32768",
            "65536"});
			this.fftSampleLengthCombo.Location = new System.Drawing.Point(131, 98);
			this.fftSampleLengthCombo.Name = "fftSampleLengthCombo";
			this.fftSampleLengthCombo.Size = new System.Drawing.Size(66, 21);
			this.fftSampleLengthCombo.TabIndex = 16;
			this.fftSampleLengthCombo.Text = "8192";
			this.fftSampleLengthCombo.SelectedIndexChanged += new System.EventHandler(this.fftSampleLengthCombo_SelectedIndexChanged);
			// 
			// waveSpectrogram
			// 
			this.waveSpectrogram.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.waveSpectrogram.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.waveSpectrogram.Location = new System.Drawing.Point(6, 177);
			this.waveSpectrogram.Name = "waveSpectrogram";
			this.waveSpectrogram.Size = new System.Drawing.Size(1274, 248);
			this.waveSpectrogram.TabIndex = 0;
			this.waveSpectrogram.TabStop = false;
			this.waveSpectrogram.Paint += new System.Windows.Forms.PaintEventHandler(this.waveSpectrogram_Paint);
			this.waveSpectrogram.MouseDown += new System.Windows.Forms.MouseEventHandler(this.waveSpectrogram_MouseDown);
			this.waveSpectrogram.MouseMove += new System.Windows.Forms.MouseEventHandler(this.waveSpectrogram_MouseMove);
			this.waveSpectrogram.MouseUp += new System.Windows.Forms.MouseEventHandler(this.waveSpectrogram_MouseUp);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.groupBox3);
			this.groupBox1.Controls.Add(this.wavePreviewBox);
			this.groupBox1.Controls.Add(this.waveSpectrogram);
			this.groupBox1.Location = new System.Drawing.Point(12, 13);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 3, 6, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(1289, 431);
			this.groupBox1.TabIndex = 18;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Source Asset";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.windowingFnGraph);
			this.groupBox3.Controls.Add(this.windowingFnCombo);
			this.groupBox3.Location = new System.Drawing.Point(1058, 19);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(222, 152);
			this.groupBox3.TabIndex = 18;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Windowing Function";
			// 
			// windowingFnGraph
			// 
			this.windowingFnGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.windowingFnGraph.Location = new System.Drawing.Point(6, 19);
			this.windowingFnGraph.Name = "windowingFnGraph";
			this.windowingFnGraph.Size = new System.Drawing.Size(210, 100);
			this.windowingFnGraph.TabIndex = 1;
			this.windowingFnGraph.TabStop = false;
			this.windowingFnGraph.Paint += new System.Windows.Forms.PaintEventHandler(this.windowingFnGraph_Paint);
			// 
			// windowingFnCombo
			// 
			this.windowingFnCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.windowingFnCombo.FormattingEnabled = true;
			this.windowingFnCombo.Items.AddRange(new object[] {
            "Blackman-Harris",
            "Hann",
            "Hamming",
            "SineCubed",
            "Sine",
            "Triangle",
            "Rectangle"});
			this.windowingFnCombo.Location = new System.Drawing.Point(6, 125);
			this.windowingFnCombo.Name = "windowingFnCombo";
			this.windowingFnCombo.Size = new System.Drawing.Size(110, 21);
			this.windowingFnCombo.TabIndex = 0;
			this.windowingFnCombo.Text = "Blackman-Harris";
			this.windowingFnCombo.SelectedIndexChanged += new System.EventHandler(this.windowingFnCombo_SelectedIndexChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.fourierTransform);
			this.groupBox2.Location = new System.Drawing.Point(12, 450);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(1289, 321);
			this.groupBox2.TabIndex = 19;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Analysis Frame";
			// 
			// displayRPMValues
			// 
			this.displayRPMValues.AutoSize = true;
			this.displayRPMValues.Location = new System.Drawing.Point(9, 77);
			this.displayRPMValues.Name = "displayRPMValues";
			this.displayRPMValues.Size = new System.Drawing.Size(122, 17);
			this.displayRPMValues.TabIndex = 20;
			this.displayRPMValues.Text = "Display RPM Values";
			this.displayRPMValues.UseVisualStyleBackColor = true;
			// 
			// groupBox4
			// 
			this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.groupBox4.Controls.Add(this.label6);
			this.groupBox4.Controls.Add(this.displayRPMValues);
			this.groupBox4.Controls.Add(this.label1);
			this.groupBox4.Controls.Add(this.fourierScaleUpDown);
			this.groupBox4.Controls.Add(this.visibleFreqRangeUpDown);
			this.groupBox4.Location = new System.Drawing.Point(18, 777);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(216, 108);
			this.groupBox4.TabIndex = 21;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Display Options";
			// 
			// groupBox5
			// 
			this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox5.Controls.Add(this.stepSizeUpDown);
			this.groupBox5.Controls.Add(this.upperBoundUpDown);
			this.groupBox5.Controls.Add(this.label3);
			this.groupBox5.Controls.Add(this.lowerBoundUpDown);
			this.groupBox5.Controls.Add(this.trackPitchBtn);
			this.groupBox5.Controls.Add(this.suggestFrequencyBtn);
			this.groupBox5.Controls.Add(this.lowerBoundLabel);
			this.groupBox5.Controls.Add(this.label2);
			this.groupBox5.Controls.Add(this.numCylindersUpDown);
			this.groupBox5.Controls.Add(this.fftSampleLengthCombo);
			this.groupBox5.Controls.Add(this.label5);
			this.groupBox5.Controls.Add(this.label7);
			this.groupBox5.Location = new System.Drawing.Point(972, 777);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(329, 160);
			this.groupBox5.TabIndex = 22;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Pitch Tracking";
			// 
			// groupBox6
			// 
			this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox6.Controls.Add(this.richTextBox1);
			this.groupBox6.Location = new System.Drawing.Point(240, 777);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(726, 160);
			this.groupBox6.TabIndex = 23;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Instructions";
			// 
			// richTextBox1
			// 
			this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.richTextBox1.Location = new System.Drawing.Point(6, 19);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.ReadOnly = true;
			this.richTextBox1.Size = new System.Drawing.Size(714, 135);
			this.richTextBox1.TabIndex = 0;
			this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
			// 
			// PitchAnalysis
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1313, 949);
			this.Controls.Add(this.groupBox6);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "PitchAnalysis";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Frequency Tracking";
			this.Load += new System.EventHandler(this.PitchAnalysis_Load);
			((System.ComponentModel.ISupportInitialize)(this.wavePreviewBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.visibleFreqRangeUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fourierScaleUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fourierTransform)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numCylindersUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.upperBoundUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lowerBoundUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.stepSizeUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.waveSpectrogram)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.windowingFnGraph)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.groupBox6.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.PictureBox fourierTransform;
		private System.Windows.Forms.Timer redrawTimer;
		private System.Windows.Forms.PictureBox wavePreviewBox;
		private System.Windows.Forms.Button trackPitchBtn;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown fourierScaleUpDown;
		private System.Windows.Forms.NumericUpDown numCylindersUpDown;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown upperBoundUpDown;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lowerBoundLabel;
		private System.Windows.Forms.NumericUpDown lowerBoundUpDown;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown stepSizeUpDown;
		private System.Windows.Forms.Button suggestFrequencyBtn;
		private System.Windows.Forms.NumericUpDown visibleFreqRangeUpDown;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox fftSampleLengthCombo;
		private System.Windows.Forms.PictureBox waveSpectrogram;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.PictureBox windowingFnGraph;
		private System.Windows.Forms.ComboBox windowingFnCombo;
		private System.Windows.Forms.CheckBox displayRPMValues;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.RichTextBox richTextBox1;
	}
}