﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wavelib;

namespace GranularPlayback2
{
	public class GranularSubmix
	{
		float GRANULAR_CROSSFADE_AMOUNT = 0.05f;
		float s_oneOverCrossfadeAmount = 1.0f / 0.05f;
		float s_s16ToFloatDivisor = 1.0f / 32767.0f;

		public enum audGranularSubmixType
		{
			SubmixTypeSynchronisedLoop,
			SubmixTypePureGranular,
			SubmixTypeDebugFullFile,
			SubmixTypeDebugSingleGranule,
		};

		public enum audGrainPlaybackOrder
		{
			Sequential,
			Random,
			Walk,
			Reverse,
			Mixed,
			PlaybackOrderMax = Mixed,
		};

		struct audGrainState
		{
			public int startSample;
			public int grainLength;
			public int grainIndex;
		};

		public class audGrainData
		{
			public int startSample;
			public float pitch;
		};

		public class audPitchData
		{
			public int sampleIndex;
			public float pitch;
		}

		public struct audLoopDefinition
		{
			public audGrainPlaybackOrder playbackOrder;
			public List<int> validGrains;
			public string submixIdentifier;
		}

		public struct audGrainDataStruct
		{
			public List<audGrainData> grainData;
			public List<audLoopDefinition> loopDefinitions;
			public List<audPitchData> originalPitchData;
		}

		bwWaveFile m_WaveData;
		public List<audGrainData> m_GrainTable;
		public List<int> m_ValidGrains;
		List<int> m_GrainHistory;
		Random m_Rand;

		audGranularSubmixType m_SubmixType;
		audGrainPlaybackOrder m_MixNMatchPlaybackOrder;
		public audGrainPlaybackOrder m_PlaybackOrder;
		audGrainState[] m_ActiveGrains;
		int m_ActiveGrainIndex;
		int m_PrevGrainIndex;
		public int m_PrevSelectedGrain;
		int m_NumMixMatchGrainsToPlay;
		int m_SelectedValidGrainIndex;
		float m_DebugSampleIndex;
		float m_DebugGranularFraction;
		bool m_WalkDirectionForward;
		public bool m_Enabled;
		public bool m_DebugPitchFlatten = false;

		public uint m_SubmixIdentifierHash;
		public string m_SubmixIdentifierString;

		float m_AverageValidGrainFraction;
		float m_AverageValidGrainHertz;

		public int m_MinGrainRepeateRate = 2;
		public float m_SlidingGrainWindowSize = 0.1f;
		public int m_MinGranularWindowOffset = 1;
		public int m_MaxGranularWindowOffset = 5;

		public float m_LastVolumeScale = 0.0f;

		/// <summary>
		/// GranularSubmix Constructor
		/// </summary>
		public GranularSubmix(bwWaveFile waveData, List<audGrainData> grainTable, audGranularSubmixType submixType, audGrainPlaybackOrder playbackOrder)
		{
			m_SubmixType = submixType;
			m_WaveData = waveData;
			m_GrainTable = grainTable;
			m_ActiveGrains = new audGrainState[2];
			m_ActiveGrainIndex = 0;
			m_PrevGrainIndex = 0;
			m_Enabled = true;
			m_Rand = new Random();
			m_PlaybackOrder = playbackOrder;
			m_GrainHistory = new List<int>();
			m_ValidGrains = new List<int>();
			SetSubmixIdentifier("Loop");

			m_ActiveGrains[m_ActiveGrainIndex].startSample = m_GrainTable[0].startSample;
			m_ActiveGrains[m_ActiveGrainIndex].grainLength = m_GrainTable[1].startSample - m_GrainTable[0].startSample;
		}

		/// <summary>
		/// Set the size of the intergranular crossfade
		/// </summary>
		/// <param name="crossfade"></param>
		public void SetCrossfadeAmount(float crossfade)
		{
			GRANULAR_CROSSFADE_AMOUNT = crossfade;
			s_oneOverCrossfadeAmount = 1.0f / GRANULAR_CROSSFADE_AMOUNT;
		}

		/// <summary>
		/// Set the submix type
		/// </summary>
		public void SetSubmixIdentifier(string type)
		{
			m_SubmixIdentifierString = type;
			m_SubmixIdentifierHash = GrainView.ComputeHash(type);
		}

		/// <summary>
		/// Set the list of valid grains that can be used
		/// </summary>
		public void SetValidGrains(List<int> validGrains)
		{
			m_ValidGrains = new List<int>();
			
			for(int loop = 0; loop < validGrains.Count; loop++)
			{
				if(validGrains[loop] >= 0)
				{
					m_ValidGrains.Add(validGrains[loop]);
				}
			}

			int validGrainIndexSum = 0;
			int validGrainSampleIndexSum = 0;
			float validGrainPitchSum = 0.0f;

			for (int loop = 0; loop < m_ValidGrains.Count; loop++)
			{
				validGrainIndexSum += m_ValidGrains[loop];
				validGrainSampleIndexSum += m_GrainTable[m_ValidGrains[loop]].startSample;
				validGrainPitchSum += m_GrainTable[m_ValidGrains[loop]].pitch;
			}

			float averageValidGrainIndex = validGrainIndexSum / (float)m_ValidGrains.Count;
			int averageValidGrainSampleIndex = (int)(validGrainSampleIndexSum / (float)m_ValidGrains.Count);
			m_AverageValidGrainFraction = averageValidGrainIndex / m_GrainTable.Count;
			m_AverageValidGrainHertz = validGrainPitchSum / (float)m_ValidGrains.Count;
		}

		/// <summary>
		/// Read a sample from a given point in the data
		/// </summary>
		float ReadSampleFromGranularFraction(int grainIndex, float granularFraction)
		{
			float sampleIndex = m_ActiveGrains[grainIndex].startSample + (m_ActiveGrains[grainIndex].grainLength * granularFraction);
			int floorSample = (int)Math.Floor(sampleIndex);

			if(floorSample + 1 >= m_WaveData.Data.NumSamples)
			{
				return m_WaveData.Data.ChannelData[0, m_WaveData.Data.NumSamples - 1];
			}
			else
			{
				float prevSample16 = m_WaveData.Data.ChannelData[0, floorSample];
				float nextSample16 = m_WaveData.Data.ChannelData[0, floorSample + 1];
				return (prevSample16 + ((nextSample16 - prevSample16) * (sampleIndex - floorSample)));
			}
		}

		/// <summary>
		/// Check if the given grain was used recently
		/// </summary>
		bool WasGrainRecentlyUsed(int grainIndex)
		{
			for(int loop = 0; loop < m_GrainHistory.Count; loop++)
			{
				if(m_GrainHistory[loop] == grainIndex)
				{
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Generate a frame of data
		/// </summary>
		public void GenerateFrame(float[] destBuffer,
								  float nextGrainAsFraction, 
								  float currentRateHz,
								  float volumeScale, 
								  float currentGranularFraction, 
								  float granuleFractionPerSample)
		{
			if (m_SubmixType == audGranularSubmixType.SubmixTypeDebugFullFile ||
				m_SubmixType == audGranularSubmixType.SubmixTypeDebugSingleGranule)
			{
				GenerateFrameDebug(destBuffer);
				return;
			}

			for(int i = 0; i < destBuffer.Length; i++)
			{
				bool currentGrainFinished = currentGranularFraction > 1.0f;

				// is the active grain finished?
				if(currentGrainFinished)
				{
					currentGranularFraction -= 1.0f;

					// next grain becomes the active grain
					m_PrevGrainIndex = m_ActiveGrainIndex;
					m_ActiveGrainIndex = (m_ActiveGrainIndex + 1) % 2;
					int grainIndex = 0;

					if(m_SubmixType == audGranularSubmixType.SubmixTypePureGranular)
					{
						int actualGrainIndex = (int)(nextGrainAsFraction * m_GrainTable.Count);

						if (nextGrainAsFraction < 0.0f)
						{
							actualGrainIndex = CalculateGrainIndexFromPitchTable(currentRateHz);
						}

						actualGrainIndex = ClampSelectedGrain(actualGrainIndex);
						grainIndex = actualGrainIndex;

						int numAttempts = 0;

						float currentGrainPitch = m_GrainTable[grainIndex].pitch;
						int maxGrainAbove = 0;
						int maxGrainBelow = 0;

						if (m_GrainTable[0].pitch < m_GrainTable[m_GrainTable.Count - 1].pitch)
						{
							for (int loop = grainIndex + 1; loop < m_GrainTable.Count && maxGrainAbove < m_MaxGranularWindowOffset; loop++)
							{
								if (m_GrainTable[loop].pitch < currentGrainPitch + m_SlidingGrainWindowSize)
								{
									maxGrainAbove++;
								}
							}

							for (int loop = grainIndex - 1; loop > 0 && maxGrainBelow < m_MaxGranularWindowOffset; loop--)
							{
								if (m_GrainTable[loop].pitch > currentGrainPitch - m_SlidingGrainWindowSize)
								{
									maxGrainBelow++;
								}
							}
						}
						else
						{
							for (int loop = grainIndex - 1; loop > 0 && maxGrainAbove < m_MaxGranularWindowOffset; loop--)
							{
								if (m_GrainTable[loop].pitch < currentGrainPitch + m_SlidingGrainWindowSize)
								{
									maxGrainAbove++;
								}
							}

							for (int loop = grainIndex + 1; loop < m_GrainTable.Count && maxGrainBelow < m_MaxGranularWindowOffset; loop++)
							{
								if (m_GrainTable[loop].pitch > currentGrainPitch - m_SlidingGrainWindowSize)
								{
									maxGrainBelow++;
								}
							}
						}
	
						if(maxGrainAbove > m_MaxGranularWindowOffset)
						{
							maxGrainAbove = m_MaxGranularWindowOffset;
						}
						else if (maxGrainAbove < m_MinGranularWindowOffset)
						{
							maxGrainAbove = m_MinGranularWindowOffset;
						}

						if (maxGrainBelow > m_MaxGranularWindowOffset)
						{
							maxGrainBelow = m_MaxGranularWindowOffset;
						}
						else if (maxGrainBelow < m_MinGranularWindowOffset)
						{
							maxGrainBelow = m_MinGranularWindowOffset;
						}

						while (WasGrainRecentlyUsed(grainIndex) && numAttempts < m_MinGrainRepeateRate)
						{
							grainIndex = actualGrainIndex + m_Rand.Next(-maxGrainBelow, maxGrainAbove + 1);
							grainIndex = ClampSelectedGrain(grainIndex);
							numAttempts++;
						}
					}
					else 
					{
						grainIndex = SelectNewGrain();
					}

					grainIndex = ClampSelectedGrain(grainIndex);
					m_ActiveGrains[m_ActiveGrainIndex].startSample = m_GrainTable[grainIndex].startSample;
					m_ActiveGrains[m_ActiveGrainIndex].grainLength = m_GrainTable[grainIndex + 1].startSample - m_GrainTable[grainIndex].startSample;
					m_PrevSelectedGrain = grainIndex;

					m_GrainHistory.Add(grainIndex);

					while (m_GrainHistory.Count > 0 && m_GrainHistory.Count > m_MinGrainRepeateRate)
					{
						m_GrainHistory.RemoveAt(0);
					}
				}

				float activeGrainSampleS16 = ReadSampleFromGranularFraction(m_ActiveGrainIndex, currentGranularFraction);

				// Do a quick crossfade to the start of the next grain
				if(currentGranularFraction < GRANULAR_CROSSFADE_AMOUNT)
				{
					float prevGrainSampleS16 = ReadSampleFromGranularFraction(m_PrevGrainIndex, 1.0f + currentGranularFraction);
					float activeSampleVolume = currentGranularFraction * s_oneOverCrossfadeAmount;
					float prevSampleVolume = 1.0f - activeSampleVolume;
					destBuffer[i] += ((activeGrainSampleS16 * activeSampleVolume) + (prevGrainSampleS16 * prevSampleVolume)) * volumeScale;
				}
				else
				{
					destBuffer[i] += activeGrainSampleS16 * volumeScale;
				}

				// move through interp
				currentGranularFraction += granuleFractionPerSample;
			}

			m_LastVolumeScale = volumeScale;
		}

		/// <summary>
		/// Generate a frame filled with debug output
		/// </summary>
		public void GenerateFrameDebug(float[] destBuffer)
		{
			if(m_SubmixType == audGranularSubmixType.SubmixTypeDebugSingleGranule)
			{
				if (m_PrevSelectedGrain >= 0)
				{
					if (m_PrevSelectedGrain + 1 < m_GrainTable.Count)
					{
						int grainLength = m_ActiveGrains[m_ActiveGrainIndex].grainLength;

						for (int i = 0; i < destBuffer.Length; i++)
						{
							bool newGrain = false;

							if(m_DebugPitchFlatten)
							{
								if(m_DebugGranularFraction > 1.0f)
								{
									newGrain = true;
								}
							}
							else
							{
								if (m_DebugSampleIndex >= grainLength)
								{
									newGrain = true;
								}
							}

							if (newGrain)
							{
								if(m_DebugPitchFlatten)
								{
									m_DebugGranularFraction -= 1.0f;
								}
								else
								{
									m_DebugSampleIndex = 0;
								}
								
								int grainIndex = ClampSelectedGrain(SelectNewGrain());
								m_ActiveGrainIndex = (m_ActiveGrainIndex + 1) % 2;
								m_ActiveGrains[m_ActiveGrainIndex].startSample = m_GrainTable[grainIndex].startSample;
								m_ActiveGrains[m_ActiveGrainIndex].grainLength = m_GrainTable[grainIndex + 1].startSample - m_GrainTable[grainIndex].startSample;
								m_ActiveGrains[m_ActiveGrainIndex].grainIndex = grainIndex;
								m_PrevSelectedGrain = grainIndex;
							}

							float currentGranularFraction = m_DebugSampleIndex / (float)m_ActiveGrains[m_ActiveGrainIndex].grainLength;

							if(m_DebugPitchFlatten)
							{
								currentGranularFraction = m_DebugGranularFraction;
							}

							if (currentGranularFraction < GRANULAR_CROSSFADE_AMOUNT)
							{
								float prevGrainSampleS16 = 0.0f;
								float currentGrainSampleS16 = 0.0f;

								if(m_DebugPitchFlatten)
								{
									prevGrainSampleS16 = ReadSampleFromGranularFraction(m_PrevGrainIndex, 1.0f + m_DebugGranularFraction);
									currentGrainSampleS16 = ReadSampleFromGranularFraction(m_ActiveGrainIndex, m_DebugGranularFraction);
								}
								else
								{
									prevGrainSampleS16 = m_WaveData.Data.ChannelData[0, m_ActiveGrains[m_PrevGrainIndex].startSample + (int)m_DebugSampleIndex];
									currentGrainSampleS16 = m_WaveData.Data.ChannelData[0, m_ActiveGrains[m_ActiveGrainIndex].startSample + (int)m_DebugSampleIndex];
								}

								float activeSampleVolume = currentGranularFraction * s_oneOverCrossfadeAmount;
								float prevSampleVolume = 1.0f - activeSampleVolume;

								destBuffer[i] += ((currentGrainSampleS16 * activeSampleVolume) + (prevGrainSampleS16 * prevSampleVolume));
							}
							else
							{
								if(m_DebugPitchFlatten)
								{
									destBuffer[i] = ReadSampleFromGranularFraction(m_ActiveGrainIndex, m_DebugGranularFraction);
								}
								else
								{
									destBuffer[i] = m_WaveData.Data.ChannelData[0, m_ActiveGrains[m_ActiveGrainIndex].startSample + (int)m_DebugSampleIndex];
									
								}
							}

							if (m_DebugPitchFlatten)
							{
								float samplesPerGranule = 44100.0f / m_AverageValidGrainHertz;
								m_DebugGranularFraction += 1.0f / samplesPerGranule;
							}
							else
							{
								m_DebugSampleIndex++;
							}
						}
					}
				}
				else if(m_ValidGrains.Count > 0)
				{
					m_PrevSelectedGrain = ClampSelectedGrain(SelectNewGrain());
				}
			}
			else if(m_SubmixType == audGranularSubmixType.SubmixTypeDebugFullFile)
			{
				for (int i = 0; i < destBuffer.Length; i++)
				{
					destBuffer[i] = m_WaveData.Data.ChannelData[0, (int)m_DebugSampleIndex];
					m_DebugSampleIndex++;

					if (m_DebugSampleIndex >= m_WaveData.Data.NumSamples)
					{
						m_DebugSampleIndex = 0;
					}
				}
			}
		}

		/// <summary>
		/// Get the average valid grain hertz
		/// </summary>
		public float GetAverageValidGrainHertz()
		{
			return m_AverageValidGrainHertz;
		}

		/// <summary>
		/// Get the average valid grain fraction
		/// </summary>
		/// <returns></returns>
		public float GetAverageValidGrainFraction()
		{
			return m_AverageValidGrainFraction;
		}

		/// <summary>
		/// Is this submix enabled?
		/// </summary>
		public bool IsEnabled()
		{
			return m_Enabled;
		}

		/// <summary>
		/// Select a new grain
		/// </summary>
		int SelectNewGrain()
		{
			audGrainPlaybackOrder currentPlaybackOrder = m_PlaybackOrder;

			if(m_PlaybackOrder == audGrainPlaybackOrder.Mixed)
			{
				if(m_NumMixMatchGrainsToPlay < 0)
				{
					m_MixNMatchPlaybackOrder = (audGrainPlaybackOrder)m_Rand.Next(0, (int)(audGrainPlaybackOrder.PlaybackOrderMax) - 1);

					if(m_MixNMatchPlaybackOrder == audGrainPlaybackOrder.Walk)
					{
						m_NumMixMatchGrainsToPlay = m_ValidGrains.Count * 2;
					}
					else
					{
						m_NumMixMatchGrainsToPlay = m_ValidGrains.Count;
					}
				}

				currentPlaybackOrder = m_MixNMatchPlaybackOrder;
				m_NumMixMatchGrainsToPlay--;
			}

			switch(currentPlaybackOrder)
			{
			case audGrainPlaybackOrder.Random:
				{
					int originalIndex = m_SelectedValidGrainIndex;

					if (m_ValidGrains.Count == 1)
					{
						m_SelectedValidGrainIndex = 0;
					}
					else if(m_ValidGrains.Count > 1)
					{
						while (m_SelectedValidGrainIndex == originalIndex)
						{
							m_SelectedValidGrainIndex = (int)m_Rand.Next(0, m_ValidGrains.Count);
						}
					}
				}
				break;
			case audGrainPlaybackOrder.Reverse:
				m_SelectedValidGrainIndex--;

				if(m_SelectedValidGrainIndex < 0)
				{
					m_SelectedValidGrainIndex = m_ValidGrains.Count - 1;
				}
				break;
			case audGrainPlaybackOrder.Sequential:
				m_SelectedValidGrainIndex++;

				if (m_SelectedValidGrainIndex >= (int)m_ValidGrains.Count)
				{
					m_SelectedValidGrainIndex = 0;
				}
				break;
			case audGrainPlaybackOrder.Walk:
				if(m_WalkDirectionForward)
				{
					m_SelectedValidGrainIndex++;

					if (m_SelectedValidGrainIndex >= (int)m_ValidGrains.Count - 1)
					{
						m_SelectedValidGrainIndex = m_ValidGrains.Count - 1;
						m_WalkDirectionForward = false;
					}
				}
				else
				{
					m_SelectedValidGrainIndex--;

					if(m_SelectedValidGrainIndex <= 0)
					{
						m_SelectedValidGrainIndex = 0;
						m_WalkDirectionForward = true;
					}
				}
				break;
			default:
				break;
			}

			if (m_ValidGrains.Count > 0 && m_SelectedValidGrainIndex < m_ValidGrains.Count)
			{
				return m_ValidGrains[m_SelectedValidGrainIndex];
			}

			return -1;
		}

		/// <summary>
		/// Clamp grain to a sensible value
		/// </summary>
		/// <param name="grainIndex"></param>
		int ClampSelectedGrain(int grainIndex)
		{
			if(grainIndex < 0)
			{
				grainIndex = 0;
			}
			else if(grainIndex >= ((int)m_GrainTable.Count)-1)
			{
				grainIndex = m_GrainTable.Count - 2;	
			}

			return grainIndex;
		}

		/// <summary>
		/// Calculate a grain using the pitch table
		/// </summary>
		int CalculateGrainIndexFromPitchTable(float currentRateHz)
		{
			int lowerBound = 0;
			int upperBound = m_GrainTable.Count - 1;
			int position = (lowerBound + upperBound) / 2;
			int numPitchValues = m_GrainTable.Count;
			bool invertSearch = m_GrainTable[0].pitch > m_GrainTable[numPitchValues - 1].pitch;

			// Do a quick binary search to match pitch->grain
			while (lowerBound <= upperBound && lowerBound >= 0 && upperBound < numPitchValues)
			{
				if (invertSearch ? m_GrainTable[position].pitch < currentRateHz : m_GrainTable[position].pitch > currentRateHz)
				{
					upperBound = position - 1;
				}
				else
				{
					lowerBound = position + 1;
				}

				position = (lowerBound + upperBound) / 2;
			}

			return position;
		}
	}
}
