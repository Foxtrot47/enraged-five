﻿namespace GranularPlayback2
{
	partial class GrainView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GrainView));
			this.syncLoopBox = new System.Windows.Forms.GroupBox();
			this.selectLoopButton = new System.Windows.Forms.Button();
			this.deleteLoopBtn = new System.Windows.Forms.Button();
			this.loopGridView = new System.Windows.Forms.DataGridView();
			this.waveViewBox = new System.Windows.Forms.GroupBox();
			this.recalcGrainBtn = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.label10 = new System.Windows.Forms.Label();
			this.pitchStyleCombo = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.grainSelectionStyle = new System.Windows.Forms.ComboBox();
			this.drawSamplesCheck = new System.Windows.Forms.CheckBox();
			this.drawLoopsCheck = new System.Windows.Forms.CheckBox();
			this.drawGrainsCheck = new System.Windows.Forms.CheckBox();
			this.waveView = new System.Windows.Forms.PictureBox();
			this.waveViewAxis = new System.Windows.Forms.PictureBox();
			this.waveScrollBar = new System.Windows.Forms.HScrollBar();
			this.loadWaveFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.redrawTimer = new System.Windows.Forms.Timer(this.components);
			this.engineSimBox = new System.Windows.Forms.GroupBox();
			this.label15 = new System.Windows.Forms.Label();
			this.crossfadeUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.MaxGranularOffset = new System.Windows.Forms.NumericUpDown();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.MinGranularOffset = new System.Windows.Forms.NumericUpDown();
			this.label12 = new System.Windows.Forms.Label();
			this.pitchScaleUpDown = new System.Windows.Forms.NumericUpDown();
			this.minRepeatRateUpDown = new System.Windows.Forms.NumericUpDown();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.granularWindowSizeUpDown = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.playbackStyleCombo = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.loopBelowBiasUpDown = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.changeRateForMaxLoops = new System.Windows.Forms.NumericUpDown();
			this.changeRateForMaxGrains = new System.Windows.Forms.NumericUpDown();
			this.simulateEngineButton = new System.Windows.Forms.Button();
			this.throttleSlider = new System.Windows.Forms.VScrollBar();
			this.intertiaSlider = new System.Windows.Forms.VScrollBar();
			this.engineSimView = new System.Windows.Forms.PictureBox();
			this.audGrainPlaybackStyleBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.grainViewContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.selectAllGrainsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteSelectedGrainsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.createLoopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.selectAllGrainsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.reduceSelectionBy75ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.reduceSelectionBy66ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.reduceSelectionBy50ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.reduceSelectionBy33ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.reduceSelectionBy25ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.removeGrainsBtn = new System.Windows.Forms.Button();
			this.preserveLoopsCheck = new System.Windows.Forms.CheckBox();
			this.label11 = new System.Windows.Forms.Label();
			this.grainReductionCombo = new System.Windows.Forms.ComboBox();
			this.saveChoppedGrainDialog = new System.Windows.Forms.SaveFileDialog();
			this.saveRemovedGrainDialog = new System.Windows.Forms.SaveFileDialog();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.syncLoopBox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.loopGridView)).BeginInit();
			this.waveViewBox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.waveView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.waveViewAxis)).BeginInit();
			this.engineSimBox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.crossfadeUpDown1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.MaxGranularOffset)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.MinGranularOffset)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchScaleUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.minRepeatRateUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.granularWindowSizeUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.loopBelowBiasUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxLoops)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxGrains)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.engineSimView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.audGrainPlaybackStyleBindingSource)).BeginInit();
			this.grainViewContextMenu.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// syncLoopBox
			// 
			this.syncLoopBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.syncLoopBox.Controls.Add(this.selectLoopButton);
			this.syncLoopBox.Controls.Add(this.deleteLoopBtn);
			this.syncLoopBox.Controls.Add(this.loopGridView);
			this.syncLoopBox.Location = new System.Drawing.Point(1169, 21);
			this.syncLoopBox.Name = "syncLoopBox";
			this.syncLoopBox.Size = new System.Drawing.Size(352, 345);
			this.syncLoopBox.TabIndex = 0;
			this.syncLoopBox.TabStop = false;
			this.syncLoopBox.Text = "Synchronised Loops";
			// 
			// selectLoopButton
			// 
			this.selectLoopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.selectLoopButton.Location = new System.Drawing.Point(6, 282);
			this.selectLoopButton.Name = "selectLoopButton";
			this.selectLoopButton.Size = new System.Drawing.Size(340, 26);
			this.selectLoopButton.TabIndex = 8;
			this.selectLoopButton.Text = "Select Loop Grains";
			this.selectLoopButton.UseVisualStyleBackColor = true;
			this.selectLoopButton.Click += new System.EventHandler(this.selectLoopButton_Click);
			// 
			// deleteLoopBtn
			// 
			this.deleteLoopBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.deleteLoopBtn.Location = new System.Drawing.Point(6, 314);
			this.deleteLoopBtn.Name = "deleteLoopBtn";
			this.deleteLoopBtn.Size = new System.Drawing.Size(340, 25);
			this.deleteLoopBtn.TabIndex = 7;
			this.deleteLoopBtn.Text = "Delete Selected Loop";
			this.deleteLoopBtn.UseVisualStyleBackColor = true;
			this.deleteLoopBtn.Click += new System.EventHandler(this.deleteLoopBtn_Click);
			// 
			// loopGridView
			// 
			this.loopGridView.AllowUserToAddRows = false;
			this.loopGridView.AllowUserToDeleteRows = false;
			this.loopGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.loopGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.loopGridView.Location = new System.Drawing.Point(6, 19);
			this.loopGridView.MultiSelect = false;
			this.loopGridView.Name = "loopGridView";
			this.loopGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.loopGridView.RowHeadersVisible = false;
			this.loopGridView.Size = new System.Drawing.Size(340, 257);
			this.loopGridView.TabIndex = 0;
			this.loopGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.loopGridView_CellContentClick);
			// 
			// waveViewBox
			// 
			this.waveViewBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.waveViewBox.Controls.Add(this.waveView);
			this.waveViewBox.Controls.Add(this.waveScrollBar);
			this.waveViewBox.Controls.Add(this.waveViewAxis);
			this.waveViewBox.Location = new System.Drawing.Point(12, 12);
			this.waveViewBox.Name = "waveViewBox";
			this.waveViewBox.Size = new System.Drawing.Size(1151, 585);
			this.waveViewBox.TabIndex = 1;
			this.waveViewBox.TabStop = false;
			this.waveViewBox.Text = "Grain Data";
			// 
			// recalcGrainBtn
			// 
			this.recalcGrainBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.recalcGrainBtn.Location = new System.Drawing.Point(1175, 923);
			this.recalcGrainBtn.Name = "recalcGrainBtn";
			this.recalcGrainBtn.Size = new System.Drawing.Size(191, 53);
			this.recalcGrainBtn.TabIndex = 15;
			this.recalcGrainBtn.Text = "Recalculate Grains";
			this.recalcGrainBtn.UseVisualStyleBackColor = true;
			this.recalcGrainBtn.Click += new System.EventHandler(this.button2_Click);
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(1175, 864);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(191, 53);
			this.button1.TabIndex = 14;
			this.button1.Text = "Save Grain Data";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(6, 49);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(57, 13);
			this.label10.TabIndex = 13;
			this.label10.Text = "Pitch Style";
			this.label10.Click += new System.EventHandler(this.label10_Click);
			// 
			// pitchStyleCombo
			// 
			this.pitchStyleCombo.FormattingEnabled = true;
			this.pitchStyleCombo.Items.AddRange(new object[] {
            "Native",
            "Flattened"});
			this.pitchStyleCombo.Location = new System.Drawing.Point(265, 46);
			this.pitchStyleCombo.Name = "pitchStyleCombo";
			this.pitchStyleCombo.Size = new System.Drawing.Size(81, 21);
			this.pitchStyleCombo.TabIndex = 12;
			this.pitchStyleCombo.Text = "Native";
			this.pitchStyleCombo.SelectedIndexChanged += new System.EventHandler(this.pitchStyleCombo_SelectedIndexChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 76);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(77, 13);
			this.label5.TabIndex = 11;
			this.label5.Text = "Selection Style";
			// 
			// grainSelectionStyle
			// 
			this.grainSelectionStyle.FormattingEnabled = true;
			this.grainSelectionStyle.Items.AddRange(new object[] {
            "Sequential",
            "Random",
            "Walk",
            "Reverse",
            "Mix n Match"});
			this.grainSelectionStyle.Location = new System.Drawing.Point(265, 73);
			this.grainSelectionStyle.Name = "grainSelectionStyle";
			this.grainSelectionStyle.Size = new System.Drawing.Size(81, 21);
			this.grainSelectionStyle.TabIndex = 11;
			this.grainSelectionStyle.Text = "Sequential";
			this.grainSelectionStyle.SelectedIndexChanged += new System.EventHandler(this.grainSelectionStyle_SelectedIndexChanged);
			// 
			// drawSamplesCheck
			// 
			this.drawSamplesCheck.AutoSize = true;
			this.drawSamplesCheck.Location = new System.Drawing.Point(147, 19);
			this.drawSamplesCheck.Name = "drawSamplesCheck";
			this.drawSamplesCheck.Size = new System.Drawing.Size(134, 17);
			this.drawSamplesCheck.TabIndex = 3;
			this.drawSamplesCheck.Text = "Draw Sample Positions";
			this.drawSamplesCheck.UseVisualStyleBackColor = true;
			this.drawSamplesCheck.CheckedChanged += new System.EventHandler(this.drawSamplesCheck_CheckedChanged);
			// 
			// drawLoopsCheck
			// 
			this.drawLoopsCheck.AutoSize = true;
			this.drawLoopsCheck.Checked = true;
			this.drawLoopsCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.drawLoopsCheck.Location = new System.Drawing.Point(6, 42);
			this.drawLoopsCheck.Name = "drawLoopsCheck";
			this.drawLoopsCheck.Size = new System.Drawing.Size(83, 17);
			this.drawLoopsCheck.TabIndex = 4;
			this.drawLoopsCheck.Text = "Draw Loops";
			this.drawLoopsCheck.UseVisualStyleBackColor = true;
			this.drawLoopsCheck.CheckedChanged += new System.EventHandler(this.drawLoopsCheck_CheckedChanged);
			// 
			// drawGrainsCheck
			// 
			this.drawGrainsCheck.AutoSize = true;
			this.drawGrainsCheck.Checked = true;
			this.drawGrainsCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.drawGrainsCheck.Location = new System.Drawing.Point(6, 19);
			this.drawGrainsCheck.Name = "drawGrainsCheck";
			this.drawGrainsCheck.Size = new System.Drawing.Size(135, 17);
			this.drawGrainsCheck.TabIndex = 2;
			this.drawGrainsCheck.Text = "Draw Grain Boundaries";
			this.drawGrainsCheck.UseVisualStyleBackColor = true;
			this.drawGrainsCheck.CheckedChanged += new System.EventHandler(this.drawGrainsCheck_CheckedChanged);
			// 
			// waveView
			// 
			this.waveView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.waveView.Location = new System.Drawing.Point(12, 19);
			this.waveView.Name = "waveView";
			this.waveView.Size = new System.Drawing.Size(1126, 505);
			this.waveView.TabIndex = 0;
			this.waveView.TabStop = false;
			this.waveView.Click += new System.EventHandler(this.waveView_Click);
			this.waveView.Paint += new System.Windows.Forms.PaintEventHandler(this.waveView_Paint);
			this.waveView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.waveView_MouseDown);
			this.waveView.MouseEnter += new System.EventHandler(this.waveView_MouseEnter);
			this.waveView.MouseLeave += new System.EventHandler(this.waveView_MouseLeave);
			this.waveView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.waveView_MouseMove);
			this.waveView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.waveView_MouseUp);
			// 
			// waveViewAxis
			// 
			this.waveViewAxis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.waveViewAxis.Location = new System.Drawing.Point(12, 524);
			this.waveViewAxis.Name = "waveViewAxis";
			this.waveViewAxis.Size = new System.Drawing.Size(1126, 22);
			this.waveViewAxis.TabIndex = 2;
			this.waveViewAxis.TabStop = false;
			this.waveViewAxis.Click += new System.EventHandler(this.waveViewAxis_Click);
			// 
			// waveScrollBar
			// 
			this.waveScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.waveScrollBar.LargeChange = 1;
			this.waveScrollBar.Location = new System.Drawing.Point(9, 553);
			this.waveScrollBar.Maximum = 0;
			this.waveScrollBar.Name = "waveScrollBar";
			this.waveScrollBar.Size = new System.Drawing.Size(1129, 26);
			this.waveScrollBar.TabIndex = 1;
			this.waveScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.waveScrollBar_Scroll);
			// 
			// loadWaveFileDialog
			// 
			this.loadWaveFileDialog.FileName = "openFileDialog1";
			this.loadWaveFileDialog.Filter = "Wave files (*.wav)|*.wav";
			this.loadWaveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.loadWaveFileDialog_FileOk);
			// 
			// redrawTimer
			// 
			this.redrawTimer.Enabled = true;
			this.redrawTimer.Interval = 10;
			this.redrawTimer.Tick += new System.EventHandler(this.redrawTimer_Tick);
			// 
			// engineSimBox
			// 
			this.engineSimBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.engineSimBox.Controls.Add(this.label7);
			this.engineSimBox.Controls.Add(this.label6);
			this.engineSimBox.Controls.Add(this.throttleSlider);
			this.engineSimBox.Controls.Add(this.intertiaSlider);
			this.engineSimBox.Controls.Add(this.engineSimView);
			this.engineSimBox.Location = new System.Drawing.Point(12, 603);
			this.engineSimBox.Name = "engineSimBox";
			this.engineSimBox.Size = new System.Drawing.Size(1151, 373);
			this.engineSimBox.TabIndex = 8;
			this.engineSimBox.TabStop = false;
			this.engineSimBox.Text = "Engine Simulator";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(6, 206);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(79, 13);
			this.label15.TabIndex = 31;
			this.label15.Text = "Granular x-fade";
			this.label15.Click += new System.EventHandler(this.label15_Click);
			// 
			// crossfadeUpDown1
			// 
			this.crossfadeUpDown1.DecimalPlaces = 2;
			this.crossfadeUpDown1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.crossfadeUpDown1.Location = new System.Drawing.Point(282, 204);
			this.crossfadeUpDown1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.crossfadeUpDown1.Name = "crossfadeUpDown1";
			this.crossfadeUpDown1.Size = new System.Drawing.Size(64, 20);
			this.crossfadeUpDown1.TabIndex = 30;
			this.crossfadeUpDown1.Value = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.crossfadeUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
			// 
			// MaxGranularOffset
			// 
			this.MaxGranularOffset.Location = new System.Drawing.Point(282, 308);
			this.MaxGranularOffset.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.MaxGranularOffset.Name = "MaxGranularOffset";
			this.MaxGranularOffset.Size = new System.Drawing.Size(64, 20);
			this.MaxGranularOffset.TabIndex = 29;
			this.MaxGranularOffset.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.MaxGranularOffset.ValueChanged += new System.EventHandler(this.MaxGranularOffset_ValueChanged);
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(6, 310);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(143, 13);
			this.label13.TabIndex = 28;
			this.label13.Text = "Max Granular Window Offset";
			this.label13.Click += new System.EventHandler(this.label13_Click);
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(6, 284);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(140, 13);
			this.label14.TabIndex = 27;
			this.label14.Text = "Min Granular Window Offset";
			this.label14.Click += new System.EventHandler(this.label14_Click);
			// 
			// MinGranularOffset
			// 
			this.MinGranularOffset.Location = new System.Drawing.Point(282, 282);
			this.MinGranularOffset.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.MinGranularOffset.Name = "MinGranularOffset";
			this.MinGranularOffset.Size = new System.Drawing.Size(64, 20);
			this.MinGranularOffset.TabIndex = 26;
			this.MinGranularOffset.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.MinGranularOffset.ValueChanged += new System.EventHandler(this.MinGranularOffset_ValueChanged);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(6, 180);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(61, 13);
			this.label12.TabIndex = 19;
			this.label12.Text = "Pitch Scale";
			this.label12.Click += new System.EventHandler(this.label12_Click);
			// 
			// pitchScaleUpDown
			// 
			this.pitchScaleUpDown.DecimalPlaces = 2;
			this.pitchScaleUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.pitchScaleUpDown.Location = new System.Drawing.Point(282, 178);
			this.pitchScaleUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.pitchScaleUpDown.Name = "pitchScaleUpDown";
			this.pitchScaleUpDown.Size = new System.Drawing.Size(64, 20);
			this.pitchScaleUpDown.TabIndex = 18;
			this.pitchScaleUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
			this.pitchScaleUpDown.ValueChanged += new System.EventHandler(this.pitchScaleUpDown_ValueChanged);
			// 
			// minRepeatRateUpDown
			// 
			this.minRepeatRateUpDown.Location = new System.Drawing.Point(282, 256);
			this.minRepeatRateUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.minRepeatRateUpDown.Name = "minRepeatRateUpDown";
			this.minRepeatRateUpDown.Size = new System.Drawing.Size(64, 20);
			this.minRepeatRateUpDown.TabIndex = 16;
			this.minRepeatRateUpDown.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.minRepeatRateUpDown.ValueChanged += new System.EventHandler(this.minRepeatRateUpDown_ValueChanged);
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(6, 258);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(88, 13);
			this.label9.TabIndex = 15;
			this.label9.Text = "Min Repeat Rate";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(6, 232);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(134, 13);
			this.label8.TabIndex = 14;
			this.label8.Text = "Granular Window Size (Hz)";
			// 
			// granularWindowSizeUpDown
			// 
			this.granularWindowSizeUpDown.DecimalPlaces = 2;
			this.granularWindowSizeUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.granularWindowSizeUpDown.Location = new System.Drawing.Point(282, 230);
			this.granularWindowSizeUpDown.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.granularWindowSizeUpDown.Name = "granularWindowSizeUpDown";
			this.granularWindowSizeUpDown.Size = new System.Drawing.Size(64, 20);
			this.granularWindowSizeUpDown.TabIndex = 13;
			this.granularWindowSizeUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.granularWindowSizeUpDown.ValueChanged += new System.EventHandler(this.granularWindowSizeUpDown_ValueChanged);
			// 
			// label7
			// 
			this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(1073, 354);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(30, 13);
			this.label7.TabIndex = 12;
			this.label7.Text = "Rate";
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(1103, 354);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(43, 13);
			this.label6.TabIndex = 11;
			this.label6.Text = "Throttle";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 22);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(77, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Playback Style";
			// 
			// playbackStyleCombo
			// 
			this.playbackStyleCombo.FormattingEnabled = true;
			this.playbackStyleCombo.Items.AddRange(new object[] {
            "Loops + Grains",
            "Loops Only",
            "Grains Only"});
			this.playbackStyleCombo.Location = new System.Drawing.Point(244, 19);
			this.playbackStyleCombo.Name = "playbackStyleCombo";
			this.playbackStyleCombo.Size = new System.Drawing.Size(102, 21);
			this.playbackStyleCombo.TabIndex = 9;
			this.playbackStyleCombo.Text = "Loops + Grains";
			this.playbackStyleCombo.SelectedIndexChanged += new System.EventHandler(this.playbackStyleCombo_SelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 154);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(86, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Loop Below Bias";
			// 
			// loopBelowBiasUpDown
			// 
			this.loopBelowBiasUpDown.DecimalPlaces = 2;
			this.loopBelowBiasUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.loopBelowBiasUpDown.Location = new System.Drawing.Point(282, 152);
			this.loopBelowBiasUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.loopBelowBiasUpDown.Name = "loopBelowBiasUpDown";
			this.loopBelowBiasUpDown.Size = new System.Drawing.Size(64, 20);
			this.loopBelowBiasUpDown.TabIndex = 8;
			this.loopBelowBiasUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.loopBelowBiasUpDown.ValueChanged += new System.EventHandler(this.loopBelowBiasUpDown_ValueChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 128);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(141, 13);
			this.label2.TabIndex = 7;
			this.label2.Text = "Change Rate for Max Grains";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 102);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(140, 13);
			this.label1.TabIndex = 6;
			this.label1.Text = "Change Rate for Max Loops";
			// 
			// changeRateForMaxLoops
			// 
			this.changeRateForMaxLoops.DecimalPlaces = 2;
			this.changeRateForMaxLoops.Location = new System.Drawing.Point(284, 100);
			this.changeRateForMaxLoops.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.changeRateForMaxLoops.Name = "changeRateForMaxLoops";
			this.changeRateForMaxLoops.Size = new System.Drawing.Size(62, 20);
			this.changeRateForMaxLoops.TabIndex = 5;
			this.changeRateForMaxLoops.ValueChanged += new System.EventHandler(this.changeRateForMaxLoops_ValueChanged);
			// 
			// changeRateForMaxGrains
			// 
			this.changeRateForMaxGrains.DecimalPlaces = 2;
			this.changeRateForMaxGrains.Location = new System.Drawing.Point(284, 126);
			this.changeRateForMaxGrains.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.changeRateForMaxGrains.Name = "changeRateForMaxGrains";
			this.changeRateForMaxGrains.Size = new System.Drawing.Size(62, 20);
			this.changeRateForMaxGrains.TabIndex = 4;
			this.changeRateForMaxGrains.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.changeRateForMaxGrains.ValueChanged += new System.EventHandler(this.changeRateForMaxGrains_ValueChanged);
			// 
			// simulateEngineButton
			// 
			this.simulateEngineButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.simulateEngineButton.Location = new System.Drawing.Point(1372, 864);
			this.simulateEngineButton.Name = "simulateEngineButton";
			this.simulateEngineButton.Size = new System.Drawing.Size(149, 112);
			this.simulateEngineButton.TabIndex = 3;
			this.simulateEngineButton.Text = "Play";
			this.simulateEngineButton.UseVisualStyleBackColor = true;
			this.simulateEngineButton.Click += new System.EventHandler(this.simulateEngineButton_Click);
			// 
			// throttleSlider
			// 
			this.throttleSlider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.throttleSlider.Location = new System.Drawing.Point(1120, 19);
			this.throttleSlider.Name = "throttleSlider";
			this.throttleSlider.Size = new System.Drawing.Size(18, 326);
			this.throttleSlider.TabIndex = 2;
			this.throttleSlider.Value = 100;
			this.throttleSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.throttleSlider_Scroll);
			// 
			// intertiaSlider
			// 
			this.intertiaSlider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.intertiaSlider.Location = new System.Drawing.Point(1081, 19);
			this.intertiaSlider.Name = "intertiaSlider";
			this.intertiaSlider.Size = new System.Drawing.Size(18, 326);
			this.intertiaSlider.TabIndex = 1;
			this.intertiaSlider.Value = 80;
			// 
			// engineSimView
			// 
			this.engineSimView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.engineSimView.Location = new System.Drawing.Point(6, 19);
			this.engineSimView.Name = "engineSimView";
			this.engineSimView.Size = new System.Drawing.Size(1061, 348);
			this.engineSimView.TabIndex = 0;
			this.engineSimView.TabStop = false;
			this.engineSimView.Paint += new System.Windows.Forms.PaintEventHandler(this.engineSimView_Paint);
			// 
			// grainViewContextMenu
			// 
			this.grainViewContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllGrainsToolStripMenuItem});
			this.grainViewContextMenu.Name = "grainViewContextMenu";
			this.grainViewContextMenu.Size = new System.Drawing.Size(123, 26);
			// 
			// selectAllGrainsToolStripMenuItem
			// 
			this.selectAllGrainsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteSelectedGrainsToolStripMenuItem,
            this.createLoopToolStripMenuItem,
            this.selectAllGrainsToolStripMenuItem1,
            this.reduceSelectionBy75ToolStripMenuItem,
            this.reduceSelectionBy66ToolStripMenuItem,
            this.reduceSelectionBy50ToolStripMenuItem,
            this.reduceSelectionBy33ToolStripMenuItem,
            this.reduceSelectionBy25ToolStripMenuItem});
			this.selectAllGrainsToolStripMenuItem.Name = "selectAllGrainsToolStripMenuItem";
			this.selectAllGrainsToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
			this.selectAllGrainsToolStripMenuItem.Text = "Selection";
			// 
			// deleteSelectedGrainsToolStripMenuItem
			// 
			this.deleteSelectedGrainsToolStripMenuItem.Name = "deleteSelectedGrainsToolStripMenuItem";
			this.deleteSelectedGrainsToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
			this.deleteSelectedGrainsToolStripMenuItem.Text = "Delete Selected Grains";
			this.deleteSelectedGrainsToolStripMenuItem.Click += new System.EventHandler(this.deleteSelectedGrainsToolStripMenuItem_Click);
			// 
			// createLoopToolStripMenuItem
			// 
			this.createLoopToolStripMenuItem.Name = "createLoopToolStripMenuItem";
			this.createLoopToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
			this.createLoopToolStripMenuItem.Text = "Create Loop";
			this.createLoopToolStripMenuItem.Click += new System.EventHandler(this.createLoopToolStripMenuItem_Click);
			// 
			// selectAllGrainsToolStripMenuItem1
			// 
			this.selectAllGrainsToolStripMenuItem1.Name = "selectAllGrainsToolStripMenuItem1";
			this.selectAllGrainsToolStripMenuItem1.Size = new System.Drawing.Size(205, 22);
			this.selectAllGrainsToolStripMenuItem1.Text = "Select all Grains";
			this.selectAllGrainsToolStripMenuItem1.Click += new System.EventHandler(this.selectAllGrainsToolStripMenuItem1_Click);
			// 
			// reduceSelectionBy75ToolStripMenuItem
			// 
			this.reduceSelectionBy75ToolStripMenuItem.Name = "reduceSelectionBy75ToolStripMenuItem";
			this.reduceSelectionBy75ToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
			this.reduceSelectionBy75ToolStripMenuItem.Text = "Reduce Selection by 75%";
			this.reduceSelectionBy75ToolStripMenuItem.Click += new System.EventHandler(this.reduceSelectionBy75ToolStripMenuItem_Click);
			// 
			// reduceSelectionBy66ToolStripMenuItem
			// 
			this.reduceSelectionBy66ToolStripMenuItem.Name = "reduceSelectionBy66ToolStripMenuItem";
			this.reduceSelectionBy66ToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
			this.reduceSelectionBy66ToolStripMenuItem.Text = "Reduce Selection by 66%";
			this.reduceSelectionBy66ToolStripMenuItem.Click += new System.EventHandler(this.reduceSelectionBy66ToolStripMenuItem_Click);
			// 
			// reduceSelectionBy50ToolStripMenuItem
			// 
			this.reduceSelectionBy50ToolStripMenuItem.Name = "reduceSelectionBy50ToolStripMenuItem";
			this.reduceSelectionBy50ToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
			this.reduceSelectionBy50ToolStripMenuItem.Text = "Reduce Selection by 50%";
			this.reduceSelectionBy50ToolStripMenuItem.Click += new System.EventHandler(this.reduceSelectionBy50ToolStripMenuItem_Click);
			// 
			// reduceSelectionBy33ToolStripMenuItem
			// 
			this.reduceSelectionBy33ToolStripMenuItem.Name = "reduceSelectionBy33ToolStripMenuItem";
			this.reduceSelectionBy33ToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
			this.reduceSelectionBy33ToolStripMenuItem.Text = "Reduce Selection by 33%";
			this.reduceSelectionBy33ToolStripMenuItem.Click += new System.EventHandler(this.reduceSelectionBy33ToolStripMenuItem_Click);
			// 
			// reduceSelectionBy25ToolStripMenuItem
			// 
			this.reduceSelectionBy25ToolStripMenuItem.Name = "reduceSelectionBy25ToolStripMenuItem";
			this.reduceSelectionBy25ToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
			this.reduceSelectionBy25ToolStripMenuItem.Text = "Reduce Selection by 25%";
			this.reduceSelectionBy25ToolStripMenuItem.Click += new System.EventHandler(this.reduceSelectionBy25ToolStripMenuItem_Click);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.removeGrainsBtn);
			this.groupBox1.Controls.Add(this.preserveLoopsCheck);
			this.groupBox1.Controls.Add(this.label11);
			this.groupBox1.Controls.Add(this.grainReductionCombo);
			this.groupBox1.Location = new System.Drawing.Point(1169, 372);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(352, 73);
			this.groupBox1.TabIndex = 9;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Grain Reduction";
			// 
			// removeGrainsBtn
			// 
			this.removeGrainsBtn.Location = new System.Drawing.Point(124, 19);
			this.removeGrainsBtn.Name = "removeGrainsBtn";
			this.removeGrainsBtn.Size = new System.Drawing.Size(222, 44);
			this.removeGrainsBtn.TabIndex = 4;
			this.removeGrainsBtn.Text = "Remove Grains";
			this.removeGrainsBtn.UseVisualStyleBackColor = true;
			this.removeGrainsBtn.Click += new System.EventHandler(this.removeGrainsBtn_Click);
			// 
			// preserveLoopsCheck
			// 
			this.preserveLoopsCheck.AutoSize = true;
			this.preserveLoopsCheck.Checked = true;
			this.preserveLoopsCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.preserveLoopsCheck.Location = new System.Drawing.Point(9, 46);
			this.preserveLoopsCheck.Name = "preserveLoopsCheck";
			this.preserveLoopsCheck.Size = new System.Drawing.Size(100, 17);
			this.preserveLoopsCheck.TabIndex = 2;
			this.preserveLoopsCheck.Text = "Preserve Loops";
			this.preserveLoopsCheck.UseVisualStyleBackColor = true;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(6, 22);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(47, 13);
			this.label11.TabIndex = 1;
			this.label11.Text = "Remove";
			// 
			// grainReductionCombo
			// 
			this.grainReductionCombo.FormattingEnabled = true;
			this.grainReductionCombo.Items.AddRange(new object[] {
            "100%",
            "75%",
            "66%",
            "50%",
            "33%",
            "25%",
            "20%",
            "10%",
            "5%"});
			this.grainReductionCombo.Location = new System.Drawing.Point(59, 19);
			this.grainReductionCombo.Name = "grainReductionCombo";
			this.grainReductionCombo.Size = new System.Drawing.Size(59, 21);
			this.grainReductionCombo.TabIndex = 0;
			this.grainReductionCombo.Text = "50%";
			this.grainReductionCombo.SelectedIndexChanged += new System.EventHandler(this.grainReductionCombo_SelectedIndexChanged);
			// 
			// saveChoppedGrainDialog
			// 
			this.saveChoppedGrainDialog.Filter = "Wave files (*.wav)|*.wav";
			this.saveChoppedGrainDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveChoppedGrainDialog_FileOk);
			// 
			// saveRemovedGrainDialog
			// 
			this.saveRemovedGrainDialog.Filter = "Wave files (*.wav)|*.wav";
			this.saveRemovedGrainDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveRemovedGrainDialog_FileOk);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.drawGrainsCheck);
			this.groupBox2.Controls.Add(this.drawSamplesCheck);
			this.groupBox2.Controls.Add(this.drawLoopsCheck);
			this.groupBox2.Location = new System.Drawing.Point(1169, 451);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(352, 68);
			this.groupBox2.TabIndex = 16;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Rendering Options";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.MaxGranularOffset);
			this.groupBox3.Controls.Add(this.label15);
			this.groupBox3.Controls.Add(this.label13);
			this.groupBox3.Controls.Add(this.label5);
			this.groupBox3.Controls.Add(this.label14);
			this.groupBox3.Controls.Add(this.MinGranularOffset);
			this.groupBox3.Controls.Add(this.crossfadeUpDown1);
			this.groupBox3.Controls.Add(this.label10);
			this.groupBox3.Controls.Add(this.minRepeatRateUpDown);
			this.groupBox3.Controls.Add(this.grainSelectionStyle);
			this.groupBox3.Controls.Add(this.label9);
			this.groupBox3.Controls.Add(this.label4);
			this.groupBox3.Controls.Add(this.pitchStyleCombo);
			this.groupBox3.Controls.Add(this.playbackStyleCombo);
			this.groupBox3.Controls.Add(this.label8);
			this.groupBox3.Controls.Add(this.granularWindowSizeUpDown);
			this.groupBox3.Controls.Add(this.label1);
			this.groupBox3.Controls.Add(this.changeRateForMaxGrains);
			this.groupBox3.Controls.Add(this.label12);
			this.groupBox3.Controls.Add(this.changeRateForMaxLoops);
			this.groupBox3.Controls.Add(this.pitchScaleUpDown);
			this.groupBox3.Controls.Add(this.label2);
			this.groupBox3.Controls.Add(this.loopBelowBiasUpDown);
			this.groupBox3.Controls.Add(this.label3);
			this.groupBox3.Location = new System.Drawing.Point(1169, 525);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(352, 333);
			this.groupBox3.TabIndex = 17;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Playback Options";
			// 
			// GrainView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1533, 988);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.recalcGrainBtn);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.engineSimBox);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.waveViewBox);
			this.Controls.Add(this.syncLoopBox);
			this.Controls.Add(this.simulateEngineButton);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "GrainView";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Grain View";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.window_Closed);
			this.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.waveView_MouseWheel);
			this.syncLoopBox.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.loopGridView)).EndInit();
			this.waveViewBox.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.waveView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.waveViewAxis)).EndInit();
			this.engineSimBox.ResumeLayout(false);
			this.engineSimBox.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.crossfadeUpDown1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.MaxGranularOffset)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.MinGranularOffset)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchScaleUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.minRepeatRateUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.granularWindowSizeUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.loopBelowBiasUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxLoops)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxGrains)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.engineSimView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.audGrainPlaybackStyleBindingSource)).EndInit();
			this.grainViewContextMenu.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox syncLoopBox;
		private System.Windows.Forms.DataGridView loopGridView;
		private System.Windows.Forms.GroupBox waveViewBox;
		private System.Windows.Forms.PictureBox waveView;
		private System.Windows.Forms.HScrollBar waveScrollBar;
		private System.Windows.Forms.CheckBox drawGrainsCheck;
		private System.Windows.Forms.CheckBox drawSamplesCheck;
		private System.Windows.Forms.CheckBox drawLoopsCheck;
		private System.Windows.Forms.Button deleteLoopBtn;
		private System.Windows.Forms.PictureBox waveViewAxis;
		private System.Windows.Forms.Button selectLoopButton;
		private System.Windows.Forms.OpenFileDialog loadWaveFileDialog;
		private System.Windows.Forms.Timer redrawTimer;
		private System.Windows.Forms.GroupBox engineSimBox;
		private System.Windows.Forms.VScrollBar throttleSlider;
		private System.Windows.Forms.VScrollBar intertiaSlider;
		private System.Windows.Forms.PictureBox engineSimView;
		private System.Windows.Forms.Button simulateEngineButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown changeRateForMaxLoops;
		private System.Windows.Forms.NumericUpDown changeRateForMaxGrains;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown loopBelowBiasUpDown;
		private System.Windows.Forms.ComboBox playbackStyleCombo;
		private System.Windows.Forms.BindingSource audGrainPlaybackStyleBindingSource;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ContextMenuStrip grainViewContextMenu;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem selectAllGrainsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem selectAllGrainsToolStripMenuItem1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox grainSelectionStyle;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ToolStripMenuItem reduceSelectionBy75ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem reduceSelectionBy66ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem reduceSelectionBy50ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem reduceSelectionBy33ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem reduceSelectionBy25ToolStripMenuItem;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.NumericUpDown granularWindowSizeUpDown;
		private System.Windows.Forms.NumericUpDown minRepeatRateUpDown;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.ToolStripMenuItem createLoopToolStripMenuItem;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ComboBox pitchStyleCombo;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox preserveLoopsCheck;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.ComboBox grainReductionCombo;
		private System.Windows.Forms.Button removeGrainsBtn;
		private System.Windows.Forms.SaveFileDialog saveChoppedGrainDialog;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.NumericUpDown pitchScaleUpDown;
		private System.Windows.Forms.NumericUpDown MaxGranularOffset;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.NumericUpDown MinGranularOffset;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.NumericUpDown crossfadeUpDown1;
		private System.Windows.Forms.ToolStripMenuItem deleteSelectedGrainsToolStripMenuItem;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button recalcGrainBtn;
		private System.Windows.Forms.SaveFileDialog saveRemovedGrainDialog;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
	}
}

