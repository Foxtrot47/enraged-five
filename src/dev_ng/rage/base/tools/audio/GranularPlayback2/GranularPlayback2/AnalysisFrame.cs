﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MathNet.Numerics;
using MathNet.Numerics.Transformations;

namespace GranularPlayback2
{
	public class AnalysisFrame
	{				
		public Complex[] ComplexFrameData	{ get { return m_ComplexFrameData; } }		
		public int CentralSample			{ get { return m_CentralSample; } }

		Complex[] m_ComplexFrameData;		
		int m_CentralSample;

		public AnalysisFrame()
		{
		}

		public static T Clamp<T>(T value, T min, T max)
		 where T : System.IComparable<T>
		{
			T result = value;
			if (value.CompareTo(max) > 0)
				result = max;
			if (value.CompareTo(min) < 0)
				result = min;
			return result;
		}

		public static double UnwrapPhase(double input)
		{
			input = input - Math.Floor(input / 2 / Math.PI) * 2 * Math.PI;
			return input - (input >= Math.PI ? 1.0f : 0.0f) * 2 * Math.PI;
		}

		private static double[] GrabFrame(float[] sampleData, double[] analysisWindow, int sampleStartIndex, int frameSize)
		{
			double[] frameData = new double[frameSize];

			for (int sample = 0; sample < frameSize && sampleStartIndex + sample < sampleData.Length; sample++)
			{
				frameData[sample] = sampleData[sampleStartIndex + sample] * analysisWindow[sample];
			}

			return frameData;
		}

		private static double[] PadFrameData(double[] frameData, int frameSize, int zeroPaddingCoefficient)
		{
			int paddedFrameSize = frameSize * zeroPaddingCoefficient;
			double[] paddedFrameData = new double[paddedFrameSize];

			for (int sample = 0; sample < (frameSize / 2); sample++)
			{
				paddedFrameData[sample] = frameData[(frameSize / 2) + sample];
			}

			int sampleIndex = 0;
			for (int sample = paddedFrameSize - (frameSize / 2); sample < paddedFrameSize; sample++)
			{
				paddedFrameData[sample] = frameData[sampleIndex];
				sampleIndex++;
			}

			return paddedFrameData;
		}

		private static Complex[] ConvertToComplex(double[] data)
		{
			Complex[] complexData = new Complex[data.Length];

			for (int i = 0; i < data.Length; i++)
			{
				complexData[i] = new Complex(data[i], 0.0f);
			}

			return complexData;
		}

		public void Analyse(float[] sampleData, double[] analysisWindow, int sampleStartIndex, int frameSize, int zeroPaddingCoefficient, uint sampleRate)
		{
			m_CentralSample = sampleStartIndex + (frameSize / 2);
			double[] frameData = GrabFrame(sampleData, analysisWindow, sampleStartIndex, frameSize);
			double[] paddedFrameData = PadFrameData(frameData, frameSize, zeroPaddingCoefficient);
			m_ComplexFrameData = ConvertToComplex(paddedFrameData);

			ComplexFourierTransformation complexFourierTransform = new ComplexFourierTransformation();
			complexFourierTransform.Convention = TransformationConvention.Matlab;
			complexFourierTransform.TransformForward(m_ComplexFrameData);			
		}
	}
}
