﻿namespace GranularPlayback2
{
	partial class GrainGenerator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GrainGenerator));
			this.zeroCrossingBiasCombo = new System.Windows.Forms.NumericUpDown();
			this.startEndBiasCombo = new System.Windows.Forms.NumericUpDown();
			this.generateGranulesBtn = new System.Windows.Forms.Button();
			this.loadPitchDataDlg = new System.Windows.Forms.OpenFileDialog();
			this.searchPercentageUpDown = new System.Windows.Forms.NumericUpDown();
			this.redrawTimer = new System.Windows.Forms.Timer(this.components);
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.revertAllChangesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadPitchDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fixedWidthComboBox = new System.Windows.Forms.CheckBox();
			this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.smoothSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.smoothMoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.flattenSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.lowerPitchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.raisePitchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.expectedMatchBiasUpDown = new System.Windows.Forms.NumericUpDown();
			this.saveAndQuitBtn = new System.Windows.Forms.Button();
			this.prevGrainView = new System.Windows.Forms.PictureBox();
			this.pitchView = new System.Windows.Forms.PictureBox();
			this.AutoSmoothPitchBtn = new System.Windows.Forms.Button();
			this.loadWaveFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.pitchScaleUpDown = new System.Windows.Forms.NumericUpDown();
			this.previousMatchBiasUpDown = new System.Windows.Forms.NumericUpDown();
			this.scaleToFitCheckBox = new System.Windows.Forms.CheckBox();
			this.previousMatchStartPreferenceBiasUpDown = new System.Windows.Forms.NumericUpDown();
			this.generateFromCursorPos = new System.Windows.Forms.CheckBox();
			this.displayGrainPitches = new System.Windows.Forms.CheckBox();
			this.previewGrainsBtn = new System.Windows.Forms.Button();
			this.interGrainPitchDifference = new System.Windows.Forms.NumericUpDown();
			this.displaySmoothedPitches = new System.Windows.Forms.CheckBox();
			this.displayCalculatedPitches = new System.Windows.Forms.CheckBox();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.richTextBox2 = new System.Windows.Forms.RichTextBox();
			this.richTextBox3 = new System.Windows.Forms.RichTextBox();
			this.richTextBox4 = new System.Windows.Forms.RichTextBox();
			this.richTextBox5 = new System.Windows.Forms.RichTextBox();
			this.richTextBox6 = new System.Windows.Forms.RichTextBox();
			this.richTextBox7 = new System.Windows.Forms.RichTextBox();
			this.richTextBox8 = new System.Windows.Forms.RichTextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.grainPreviewLabel = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.granuleProgressBar = new System.Windows.Forms.ProgressBar();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.richTextBox9 = new System.Windows.Forms.RichTextBox();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.numCylindersUpDown = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.zeroCrossingBiasCombo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.startEndBiasCombo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.searchPercentageUpDown)).BeginInit();
			this.menuStrip1.SuspendLayout();
			this.contextMenuStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.expectedMatchBiasUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.prevGrainView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchScaleUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.previousMatchBiasUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.previousMatchStartPreferenceBiasUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.interGrainPitchDifference)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numCylindersUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// zeroCrossingBiasCombo
			// 
			this.zeroCrossingBiasCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.zeroCrossingBiasCombo.DecimalPlaces = 2;
			this.zeroCrossingBiasCombo.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.zeroCrossingBiasCombo.Location = new System.Drawing.Point(295, 28);
			this.zeroCrossingBiasCombo.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.zeroCrossingBiasCombo.Name = "zeroCrossingBiasCombo";
			this.zeroCrossingBiasCombo.Size = new System.Drawing.Size(61, 20);
			this.zeroCrossingBiasCombo.TabIndex = 6;
			this.zeroCrossingBiasCombo.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
			// 
			// startEndBiasCombo
			// 
			this.startEndBiasCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.startEndBiasCombo.DecimalPlaces = 2;
			this.startEndBiasCombo.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.startEndBiasCombo.Location = new System.Drawing.Point(294, 282);
			this.startEndBiasCombo.Name = "startEndBiasCombo";
			this.startEndBiasCombo.Size = new System.Drawing.Size(61, 20);
			this.startEndBiasCombo.TabIndex = 5;
			// 
			// generateGranulesBtn
			// 
			this.generateGranulesBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.generateGranulesBtn.Location = new System.Drawing.Point(1221, 766);
			this.generateGranulesBtn.Name = "generateGranulesBtn";
			this.generateGranulesBtn.Size = new System.Drawing.Size(209, 85);
			this.generateGranulesBtn.TabIndex = 0;
			this.generateGranulesBtn.Text = "Generate Grains";
			this.generateGranulesBtn.UseVisualStyleBackColor = true;
			this.generateGranulesBtn.Click += new System.EventHandler(this.generateGranulesBtn_Click);
			// 
			// loadPitchDataDlg
			// 
			this.loadPitchDataDlg.Filter = "Grain file (*.grn)|*grn";
			this.loadPitchDataDlg.FileOk += new System.ComponentModel.CancelEventHandler(this.loadPitchDataDlg_FileOk);
			// 
			// searchPercentageUpDown
			// 
			this.searchPercentageUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.searchPercentageUpDown.DecimalPlaces = 2;
			this.searchPercentageUpDown.Location = new System.Drawing.Point(295, 175);
			this.searchPercentageUpDown.Name = "searchPercentageUpDown";
			this.searchPercentageUpDown.Size = new System.Drawing.Size(61, 20);
			this.searchPercentageUpDown.TabIndex = 4;
			this.searchPercentageUpDown.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
			// 
			// redrawTimer
			// 
			this.redrawTimer.Enabled = true;
			this.redrawTimer.Interval = 10;
			this.redrawTimer.Tick += new System.EventHandler(this.redrawTimer_Tick);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1441, 24);
			this.menuStrip1.TabIndex = 13;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.revertAllChangesToolStripMenuItem,
            this.loadPitchDataToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "Edit";
			// 
			// undoToolStripMenuItem
			// 
			this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
			this.undoToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
			this.undoToolStripMenuItem.Text = "Undo Last Change";
			this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
			// 
			// revertAllChangesToolStripMenuItem
			// 
			this.revertAllChangesToolStripMenuItem.Name = "revertAllChangesToolStripMenuItem";
			this.revertAllChangesToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
			this.revertAllChangesToolStripMenuItem.Text = "Revert All Changes";
			this.revertAllChangesToolStripMenuItem.Click += new System.EventHandler(this.revertAllChangesToolStripMenuItem_Click);
			// 
			// loadPitchDataToolStripMenuItem
			// 
			this.loadPitchDataToolStripMenuItem.Name = "loadPitchDataToolStripMenuItem";
			this.loadPitchDataToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
			this.loadPitchDataToolStripMenuItem.Text = "Load Pitch Data";
			this.loadPitchDataToolStripMenuItem.Click += new System.EventHandler(this.loadPitchDataToolStripMenuItem_Click);
			// 
			// fixedWidthComboBox
			// 
			this.fixedWidthComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.fixedWidthComboBox.AutoSize = true;
			this.fixedWidthComboBox.Checked = true;
			this.fixedWidthComboBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.fixedWidthComboBox.Location = new System.Drawing.Point(240, 210);
			this.fixedWidthComboBox.Name = "fixedWidthComboBox";
			this.fixedWidthComboBox.Size = new System.Drawing.Size(110, 17);
			this.fixedWidthComboBox.TabIndex = 7;
			this.fixedWidthComboBox.Text = "Draw Fixed Width";
			this.fixedWidthComboBox.UseVisualStyleBackColor = true;
			this.fixedWidthComboBox.CheckedChanged += new System.EventHandler(this.fixedWidthComboBox_CheckedChanged);
			// 
			// contextMenuStrip
			// 
			this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.smoothSelectionToolStripMenuItem,
            this.smoothMoreToolStripMenuItem,
            this.flattenSelectionToolStripMenuItem,
            this.lowerPitchToolStripMenuItem,
            this.raisePitchToolStripMenuItem});
			this.contextMenuStrip.Name = "contextMenuStrip";
			this.contextMenuStrip.Size = new System.Drawing.Size(148, 114);
			// 
			// smoothSelectionToolStripMenuItem
			// 
			this.smoothSelectionToolStripMenuItem.Name = "smoothSelectionToolStripMenuItem";
			this.smoothSelectionToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
			this.smoothSelectionToolStripMenuItem.Text = "Smooth";
			this.smoothSelectionToolStripMenuItem.Click += new System.EventHandler(this.smoothSelectionToolStripMenuItem_Click);
			// 
			// smoothMoreToolStripMenuItem
			// 
			this.smoothMoreToolStripMenuItem.Name = "smoothMoreToolStripMenuItem";
			this.smoothMoreToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
			this.smoothMoreToolStripMenuItem.Text = "Smooth More";
			this.smoothMoreToolStripMenuItem.Click += new System.EventHandler(this.smoothMoreToolStripMenuItem_Click);
			// 
			// flattenSelectionToolStripMenuItem
			// 
			this.flattenSelectionToolStripMenuItem.Name = "flattenSelectionToolStripMenuItem";
			this.flattenSelectionToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
			this.flattenSelectionToolStripMenuItem.Text = "Flatten";
			this.flattenSelectionToolStripMenuItem.Click += new System.EventHandler(this.flattenSelectionToolStripMenuItem_Click);
			// 
			// lowerPitchToolStripMenuItem
			// 
			this.lowerPitchToolStripMenuItem.Name = "lowerPitchToolStripMenuItem";
			this.lowerPitchToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
			this.lowerPitchToolStripMenuItem.Text = "Lower Pitch";
			this.lowerPitchToolStripMenuItem.Click += new System.EventHandler(this.lowerPitchToolStripMenuItem_Click);
			// 
			// raisePitchToolStripMenuItem
			// 
			this.raisePitchToolStripMenuItem.Name = "raisePitchToolStripMenuItem";
			this.raisePitchToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
			this.raisePitchToolStripMenuItem.Text = "Raise Pitch";
			this.raisePitchToolStripMenuItem.Click += new System.EventHandler(this.raisePitchToolStripMenuItem_Click);
			// 
			// expectedMatchBiasUpDown
			// 
			this.expectedMatchBiasUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.expectedMatchBiasUpDown.DecimalPlaces = 2;
			this.expectedMatchBiasUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.expectedMatchBiasUpDown.Location = new System.Drawing.Point(294, 222);
			this.expectedMatchBiasUpDown.Name = "expectedMatchBiasUpDown";
			this.expectedMatchBiasUpDown.Size = new System.Drawing.Size(61, 20);
			this.expectedMatchBiasUpDown.TabIndex = 3;
			this.expectedMatchBiasUpDown.ValueChanged += new System.EventHandler(this.expectedMatchBiasUpDown_ValueChanged);
			// 
			// saveAndQuitBtn
			// 
			this.saveAndQuitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.saveAndQuitBtn.Location = new System.Drawing.Point(1088, 743);
			this.saveAndQuitBtn.Name = "saveAndQuitBtn";
			this.saveAndQuitBtn.Size = new System.Drawing.Size(127, 30);
			this.saveAndQuitBtn.TabIndex = 1;
			this.saveAndQuitBtn.Text = "Save Data";
			this.saveAndQuitBtn.UseVisualStyleBackColor = true;
			this.saveAndQuitBtn.Click += new System.EventHandler(this.saveAndQuitBtn_Click);
			// 
			// prevGrainView
			// 
			this.prevGrainView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.prevGrainView.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.prevGrainView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.prevGrainView.Location = new System.Drawing.Point(6, 19);
			this.prevGrainView.Name = "prevGrainView";
			this.prevGrainView.Size = new System.Drawing.Size(344, 185);
			this.prevGrainView.TabIndex = 14;
			this.prevGrainView.TabStop = false;
			this.prevGrainView.Paint += new System.Windows.Forms.PaintEventHandler(this.prevGrainView_Paint);
			// 
			// pitchView
			// 
			this.pitchView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pitchView.BackColor = System.Drawing.SystemColors.Control;
			this.pitchView.Location = new System.Drawing.Point(6, 19);
			this.pitchView.Name = "pitchView";
			this.pitchView.Size = new System.Drawing.Size(1038, 551);
			this.pitchView.TabIndex = 9;
			this.pitchView.TabStop = false;
			this.pitchView.Paint += new System.Windows.Forms.PaintEventHandler(this.pitchView_Paint);
			this.pitchView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pitchView_MouseDown);
			this.pitchView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pitchView_MouseMove);
			this.pitchView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pitchView_MouseUp);
			// 
			// AutoSmoothPitchBtn
			// 
			this.AutoSmoothPitchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.AutoSmoothPitchBtn.Location = new System.Drawing.Point(1088, 782);
			this.AutoSmoothPitchBtn.Name = "AutoSmoothPitchBtn";
			this.AutoSmoothPitchBtn.Size = new System.Drawing.Size(127, 30);
			this.AutoSmoothPitchBtn.TabIndex = 22;
			this.AutoSmoothPitchBtn.Text = "Smooth Frequencies";
			this.AutoSmoothPitchBtn.UseVisualStyleBackColor = true;
			this.AutoSmoothPitchBtn.Click += new System.EventHandler(this.AutoSmoothPitchBtn_Click);
			// 
			// loadWaveFileDialog
			// 
			this.loadWaveFileDialog.Filter = "Wave files (*.wav)|*.wav";
			this.loadWaveFileDialog.Title = "Select .wav file";
			this.loadWaveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.loadWaveFileDialog_FileOk);
			// 
			// pitchScaleUpDown
			// 
			this.pitchScaleUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pitchScaleUpDown.DecimalPlaces = 2;
			this.pitchScaleUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.pitchScaleUpDown.Location = new System.Drawing.Point(294, 395);
			this.pitchScaleUpDown.Name = "pitchScaleUpDown";
			this.pitchScaleUpDown.Size = new System.Drawing.Size(61, 20);
			this.pitchScaleUpDown.TabIndex = 25;
			this.pitchScaleUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// previousMatchBiasUpDown
			// 
			this.previousMatchBiasUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.previousMatchBiasUpDown.DecimalPlaces = 2;
			this.previousMatchBiasUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.previousMatchBiasUpDown.Location = new System.Drawing.Point(294, 63);
			this.previousMatchBiasUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.previousMatchBiasUpDown.Name = "previousMatchBiasUpDown";
			this.previousMatchBiasUpDown.Size = new System.Drawing.Size(61, 20);
			this.previousMatchBiasUpDown.TabIndex = 39;
			this.previousMatchBiasUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// scaleToFitCheckBox
			// 
			this.scaleToFitCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.scaleToFitCheckBox.AutoSize = true;
			this.scaleToFitCheckBox.Location = new System.Drawing.Point(151, 210);
			this.scaleToFitCheckBox.Name = "scaleToFitCheckBox";
			this.scaleToFitCheckBox.Size = new System.Drawing.Size(83, 17);
			this.scaleToFitCheckBox.TabIndex = 42;
			this.scaleToFitCheckBox.Text = "Scale To Fit";
			this.scaleToFitCheckBox.UseVisualStyleBackColor = true;
			this.scaleToFitCheckBox.CheckedChanged += new System.EventHandler(this.scaleToFitCheckBox_CheckedChanged);
			// 
			// previousMatchStartPreferenceBiasUpDown
			// 
			this.previousMatchStartPreferenceBiasUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.previousMatchStartPreferenceBiasUpDown.DecimalPlaces = 2;
			this.previousMatchStartPreferenceBiasUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.previousMatchStartPreferenceBiasUpDown.Location = new System.Drawing.Point(295, 105);
			this.previousMatchStartPreferenceBiasUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.previousMatchStartPreferenceBiasUpDown.Name = "previousMatchStartPreferenceBiasUpDown";
			this.previousMatchStartPreferenceBiasUpDown.Size = new System.Drawing.Size(61, 20);
			this.previousMatchStartPreferenceBiasUpDown.TabIndex = 47;
			// 
			// generateFromCursorPos
			// 
			this.generateFromCursorPos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.generateFromCursorPos.AutoSize = true;
			this.generateFromCursorPos.Location = new System.Drawing.Point(1221, 743);
			this.generateFromCursorPos.Name = "generateFromCursorPos";
			this.generateFromCursorPos.Size = new System.Drawing.Size(209, 17);
			this.generateFromCursorPos.TabIndex = 48;
			this.generateFromCursorPos.Text = "Generate Granules from cursor position";
			this.generateFromCursorPos.UseVisualStyleBackColor = true;
			// 
			// displayGrainPitches
			// 
			this.displayGrainPitches.AutoSize = true;
			this.displayGrainPitches.BackColor = System.Drawing.SystemColors.Control;
			this.displayGrainPitches.Checked = true;
			this.displayGrainPitches.CheckState = System.Windows.Forms.CheckState.Checked;
			this.displayGrainPitches.Location = new System.Drawing.Point(6, 75);
			this.displayGrainPitches.Name = "displayGrainPitches";
			this.displayGrainPitches.Size = new System.Drawing.Size(195, 17);
			this.displayGrainPitches.TabIndex = 49;
			this.displayGrainPitches.Text = "Display generated grain frequencies";
			this.displayGrainPitches.UseVisualStyleBackColor = false;
			this.displayGrainPitches.CheckedChanged += new System.EventHandler(this.displayGrainPitches_CheckedChanged);
			// 
			// previewGrainsBtn
			// 
			this.previewGrainsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.previewGrainsBtn.Location = new System.Drawing.Point(1088, 821);
			this.previewGrainsBtn.Name = "previewGrainsBtn";
			this.previewGrainsBtn.Size = new System.Drawing.Size(127, 30);
			this.previewGrainsBtn.TabIndex = 50;
			this.previewGrainsBtn.Text = "Preview Grains";
			this.previewGrainsBtn.UseVisualStyleBackColor = true;
			this.previewGrainsBtn.Click += new System.EventHandler(this.previewGrainsBtn_Click);
			// 
			// interGrainPitchDifference
			// 
			this.interGrainPitchDifference.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.interGrainPitchDifference.DecimalPlaces = 2;
			this.interGrainPitchDifference.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.interGrainPitchDifference.Location = new System.Drawing.Point(294, 339);
			this.interGrainPitchDifference.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.interGrainPitchDifference.Name = "interGrainPitchDifference";
			this.interGrainPitchDifference.Size = new System.Drawing.Size(61, 20);
			this.interGrainPitchDifference.TabIndex = 51;
			this.interGrainPitchDifference.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
			// 
			// displaySmoothedPitches
			// 
			this.displaySmoothedPitches.AutoSize = true;
			this.displaySmoothedPitches.BackColor = System.Drawing.SystemColors.Control;
			this.displaySmoothedPitches.Location = new System.Drawing.Point(6, 52);
			this.displaySmoothedPitches.Name = "displaySmoothedPitches";
			this.displaySmoothedPitches.Size = new System.Drawing.Size(193, 17);
			this.displaySmoothedPitches.TabIndex = 52;
			this.displaySmoothedPitches.Text = "Display smoothed grain frequencies";
			this.displaySmoothedPitches.UseVisualStyleBackColor = false;
			// 
			// displayCalculatedPitches
			// 
			this.displayCalculatedPitches.AutoSize = true;
			this.displayCalculatedPitches.BackColor = System.Drawing.SystemColors.Control;
			this.displayCalculatedPitches.Checked = true;
			this.displayCalculatedPitches.CheckState = System.Windows.Forms.CheckState.Checked;
			this.displayCalculatedPitches.Location = new System.Drawing.Point(6, 29);
			this.displayCalculatedPitches.Name = "displayCalculatedPitches";
			this.displayCalculatedPitches.Size = new System.Drawing.Size(196, 17);
			this.displayCalculatedPitches.TabIndex = 53;
			this.displayCalculatedPitches.Text = "Display calculated grain frequencies";
			this.displayCalculatedPitches.UseVisualStyleBackColor = false;
			// 
			// richTextBox1
			// 
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox1.Location = new System.Drawing.Point(7, 28);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.ReadOnly = true;
			this.richTextBox1.Size = new System.Drawing.Size(275, 32);
			this.richTextBox1.TabIndex = 54;
			this.richTextBox1.Text = "Zero Crossing Bias - How important it is to end each grain on a zero crossing.";
			this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
			// 
			// richTextBox2
			// 
			this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox2.Location = new System.Drawing.Point(6, 63);
			this.richTextBox2.Name = "richTextBox2";
			this.richTextBox2.ReadOnly = true;
			this.richTextBox2.Size = new System.Drawing.Size(275, 37);
			this.richTextBox2.TabIndex = 55;
			this.richTextBox2.Text = "Previous Grain Bias - How important it is for each grain to match the waveform of" +
    " the previous one.";
			// 
			// richTextBox3
			// 
			this.richTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox3.Location = new System.Drawing.Point(7, 105);
			this.richTextBox3.Name = "richTextBox3";
			this.richTextBox3.ReadOnly = true;
			this.richTextBox3.Size = new System.Drawing.Size(275, 62);
			this.richTextBox3.TabIndex = 56;
			this.richTextBox3.Text = "Previous Grain Start Bias - Used in conjunction with previous grain bias. Biases " +
    "us towards caring more about the first part of the grain matching the previous o" +
    "ne compared to the end.";
			// 
			// richTextBox4
			// 
			this.richTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox4.Location = new System.Drawing.Point(7, 175);
			this.richTextBox4.Name = "richTextBox4";
			this.richTextBox4.ReadOnly = true;
			this.richTextBox4.Size = new System.Drawing.Size(275, 39);
			this.richTextBox4.TabIndex = 57;
			this.richTextBox4.Text = "Search Freedom - The error % allowed in each grain\'s frequency compared to the ex" +
    "pected frequency.";
			// 
			// richTextBox5
			// 
			this.richTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox5.Location = new System.Drawing.Point(6, 222);
			this.richTextBox5.Name = "richTextBox5";
			this.richTextBox5.ReadOnly = true;
			this.richTextBox5.Size = new System.Drawing.Size(275, 54);
			this.richTextBox5.TabIndex = 58;
			this.richTextBox5.Text = "Expected Match Bias - How much bias is given to grains with frequencies close to " +
    "the expected frequencies vs those at the extremes of the search freedom range.";
			this.richTextBox5.TextChanged += new System.EventHandler(this.richTextBox5_TextChanged);
			// 
			// richTextBox6
			// 
			this.richTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox6.Location = new System.Drawing.Point(6, 282);
			this.richTextBox6.Name = "richTextBox6";
			this.richTextBox6.ReadOnly = true;
			this.richTextBox6.Size = new System.Drawing.Size(275, 39);
			this.richTextBox6.TabIndex = 59;
			this.richTextBox6.Text = "Start/End Bias - How important it is to end each grain on a sample with the same " +
    "value as the end sample of the previous grain.";
			// 
			// richTextBox7
			// 
			this.richTextBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox7.Location = new System.Drawing.Point(6, 339);
			this.richTextBox7.Name = "richTextBox7";
			this.richTextBox7.ReadOnly = true;
			this.richTextBox7.Size = new System.Drawing.Size(275, 39);
			this.richTextBox7.TabIndex = 60;
			this.richTextBox7.Text = "Jitter Correction Factor - Controls the max allowable difference of each grain\'s " +
    "length compared to the preceeding one (lower value = stricter)";
			// 
			// richTextBox8
			// 
			this.richTextBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox8.Location = new System.Drawing.Point(6, 395);
			this.richTextBox8.Name = "richTextBox8";
			this.richTextBox8.ReadOnly = true;
			this.richTextBox8.Size = new System.Drawing.Size(275, 39);
			this.richTextBox8.TabIndex = 61;
			this.richTextBox8.Text = "Incoming Frequency Scale - Used to scale the calculated frequency values. Can be " +
    "used to calculate sub grain boundaries on certain waveforms";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.grainPreviewLabel);
			this.groupBox1.Controls.Add(this.prevGrainView);
			this.groupBox1.Controls.Add(this.scaleToFitCheckBox);
			this.groupBox1.Controls.Add(this.fixedWidthComboBox);
			this.groupBox1.Location = new System.Drawing.Point(1069, 37);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(356, 233);
			this.groupBox1.TabIndex = 62;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Grain Preview";
			// 
			// grainPreviewLabel
			// 
			this.grainPreviewLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.grainPreviewLabel.AutoSize = true;
			this.grainPreviewLabel.Location = new System.Drawing.Point(16, 29);
			this.grainPreviewLabel.Name = "grainPreviewLabel";
			this.grainPreviewLabel.Size = new System.Drawing.Size(102, 13);
			this.grainPreviewLabel.TabIndex = 43;
			this.grainPreviewLabel.Text = "Grain Preview Label";
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.richTextBox1);
			this.groupBox2.Controls.Add(this.zeroCrossingBiasCombo);
			this.groupBox2.Controls.Add(this.richTextBox8);
			this.groupBox2.Controls.Add(this.startEndBiasCombo);
			this.groupBox2.Controls.Add(this.richTextBox7);
			this.groupBox2.Controls.Add(this.searchPercentageUpDown);
			this.groupBox2.Controls.Add(this.richTextBox6);
			this.groupBox2.Controls.Add(this.pitchScaleUpDown);
			this.groupBox2.Controls.Add(this.expectedMatchBiasUpDown);
			this.groupBox2.Controls.Add(this.richTextBox5);
			this.groupBox2.Controls.Add(this.previousMatchBiasUpDown);
			this.groupBox2.Controls.Add(this.richTextBox4);
			this.groupBox2.Controls.Add(this.previousMatchStartPreferenceBiasUpDown);
			this.groupBox2.Controls.Add(this.richTextBox3);
			this.groupBox2.Controls.Add(this.interGrainPitchDifference);
			this.groupBox2.Controls.Add(this.richTextBox2);
			this.groupBox2.Location = new System.Drawing.Point(1069, 276);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(361, 458);
			this.groupBox2.TabIndex = 63;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Generation Options";
			// 
			// granuleProgressBar
			// 
			this.granuleProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.granuleProgressBar.Location = new System.Drawing.Point(6, 576);
			this.granuleProgressBar.Name = "granuleProgressBar";
			this.granuleProgressBar.Size = new System.Drawing.Size(1038, 23);
			this.granuleProgressBar.Step = 1;
			this.granuleProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.granuleProgressBar.TabIndex = 0;
			// 
			// groupBox4
			// 
			this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox4.Controls.Add(this.pitchView);
			this.groupBox4.Controls.Add(this.granuleProgressBar);
			this.groupBox4.Location = new System.Drawing.Point(13, 28);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(1050, 608);
			this.groupBox4.TabIndex = 65;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Grain Generation";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.richTextBox9);
			this.groupBox3.Location = new System.Drawing.Point(12, 635);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(834, 216);
			this.groupBox3.TabIndex = 66;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Instructions";
			// 
			// richTextBox9
			// 
			this.richTextBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.richTextBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox9.Location = new System.Drawing.Point(6, 19);
			this.richTextBox9.Name = "richTextBox9";
			this.richTextBox9.ReadOnly = true;
			this.richTextBox9.Size = new System.Drawing.Size(822, 191);
			this.richTextBox9.TabIndex = 0;
			this.richTextBox9.Text = resources.GetString("richTextBox9.Text");
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.label2);
			this.groupBox5.Controls.Add(this.numCylindersUpDown);
			this.groupBox5.Controls.Add(this.displayGrainPitches);
			this.groupBox5.Controls.Add(this.displayCalculatedPitches);
			this.groupBox5.Controls.Add(this.displaySmoothedPitches);
			this.groupBox5.Location = new System.Drawing.Point(852, 642);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(211, 131);
			this.groupBox5.TabIndex = 67;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Display Options";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 100);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(74, 13);
			this.label2.TabIndex = 69;
			this.label2.Text = "Num Cylinders";
			this.label2.Click += new System.EventHandler(this.label2_Click);
			// 
			// numCylindersUpDown
			// 
			this.numCylindersUpDown.Location = new System.Drawing.Point(133, 98);
			this.numCylindersUpDown.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.numCylindersUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numCylindersUpDown.Name = "numCylindersUpDown";
			this.numCylindersUpDown.Size = new System.Drawing.Size(66, 20);
			this.numCylindersUpDown.TabIndex = 68;
			this.numCylindersUpDown.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
			this.numCylindersUpDown.ValueChanged += new System.EventHandler(this.numCylindersUpDown_ValueChanged);
			// 
			// GrainGenerator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
			this.ClientSize = new System.Drawing.Size(1441, 863);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.previewGrainsBtn);
			this.Controls.Add(this.generateFromCursorPos);
			this.Controls.Add(this.AutoSmoothPitchBtn);
			this.Controls.Add(this.menuStrip1);
			this.Controls.Add(this.saveAndQuitBtn);
			this.Controls.Add(this.generateGranulesBtn);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "GrainGenerator";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Grain Generator";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.window_Closed);
			((System.ComponentModel.ISupportInitialize)(this.zeroCrossingBiasCombo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.startEndBiasCombo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.searchPercentageUpDown)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.contextMenuStrip.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.expectedMatchBiasUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.prevGrainView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pitchScaleUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.previousMatchBiasUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.previousMatchStartPreferenceBiasUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.interGrainPitchDifference)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numCylindersUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.NumericUpDown zeroCrossingBiasCombo;
		private System.Windows.Forms.NumericUpDown startEndBiasCombo;
		private System.Windows.Forms.Button generateGranulesBtn;
		private System.Windows.Forms.OpenFileDialog loadPitchDataDlg;
		private System.Windows.Forms.NumericUpDown searchPercentageUpDown;
		private System.Windows.Forms.Timer redrawTimer;
		private System.Windows.Forms.PictureBox pitchView;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.PictureBox prevGrainView;
		private System.Windows.Forms.CheckBox fixedWidthComboBox;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem flattenSelectionToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem smoothSelectionToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem smoothMoreToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
		private System.Windows.Forms.NumericUpDown expectedMatchBiasUpDown;
		private System.Windows.Forms.ToolStripMenuItem lowerPitchToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem raisePitchToolStripMenuItem;
		private System.Windows.Forms.Button saveAndQuitBtn;
		private System.Windows.Forms.Button AutoSmoothPitchBtn;
		private System.Windows.Forms.OpenFileDialog loadWaveFileDialog;
		private System.Windows.Forms.NumericUpDown pitchScaleUpDown;
		private System.Windows.Forms.NumericUpDown previousMatchBiasUpDown;
		private System.Windows.Forms.CheckBox scaleToFitCheckBox;
		private System.Windows.Forms.NumericUpDown previousMatchStartPreferenceBiasUpDown;
		private System.Windows.Forms.CheckBox generateFromCursorPos;
		private System.Windows.Forms.CheckBox displayGrainPitches;
		private System.Windows.Forms.ToolStripMenuItem revertAllChangesToolStripMenuItem;
		private System.Windows.Forms.Button previewGrainsBtn;
		private System.Windows.Forms.NumericUpDown interGrainPitchDifference;
		private System.Windows.Forms.ToolStripMenuItem loadPitchDataToolStripMenuItem;
		private System.Windows.Forms.CheckBox displaySmoothedPitches;
		private System.Windows.Forms.CheckBox displayCalculatedPitches;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.RichTextBox richTextBox2;
		private System.Windows.Forms.RichTextBox richTextBox3;
		private System.Windows.Forms.RichTextBox richTextBox4;
		private System.Windows.Forms.RichTextBox richTextBox5;
		private System.Windows.Forms.RichTextBox richTextBox6;
		private System.Windows.Forms.RichTextBox richTextBox7;
		private System.Windows.Forms.RichTextBox richTextBox8;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.ProgressBar granuleProgressBar;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label grainPreviewLabel;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.RichTextBox richTextBox9;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numCylindersUpDown;
	}
}