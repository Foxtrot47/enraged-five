﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Wavelib;

namespace GranularPlayback2
{
	public partial class GranularBlender : Form
	{
		GrainPlayer m_GrainPlayer;
		int m_LastLoadButtonClicked;

		int[] m_SubmixIndexes = new int[2];
		Bitmap[] m_WaveImages = new Bitmap[2];
		PictureBox[] m_PictureBoxes = new PictureBox[2];
		GroupBox[] m_GroupBoxes = new GroupBox[2];
		float[] m_Xscale = new float[2];

		private float m_Xzoom = 1.0f;
		private float m_Yscale = 1.0f;
		private float m_Yzoom = 1.0f;
		private int m_Xoffset = 0;

		public class audBlendedGrainData : IComparable<audBlendedGrainData>
		{
			public int startSample;
			public int finalStartSample;
			public float pitch;
			public int submixIndex;
			public int grainIndex;

			public int CompareTo(audBlendedGrainData other)
			{
				if (pitch == other.pitch)
				{
					return startSample < other.startSample ? -1 : 1;
				}
				else
				{
					return pitch < other.pitch ? -1 : 1;
				}
			}
		};

		bwWaveFile m_BlendedWaveFile;

		public GranularBlender()
		{
			InitializeComponent();			
			m_BlendedWaveFile = new bwWaveFile();

			for (int loop = 0; loop < m_SubmixIndexes.Length; loop++)
			{
				m_SubmixIndexes[loop] = -1;
				m_WaveImages[loop] = new Bitmap(grainSource1Pic.Width, grainSource1Pic.Height);
			}

			m_PictureBoxes[0] = grainSource1Pic;
			m_PictureBoxes[1] = grainSource2Pic;

			m_GroupBoxes[0] = groupBox1;
			m_GroupBoxes[1] = groupBox2;

			for (int loop = 0; loop < 2; loop++)
			{
				m_Xscale[loop] = 1.0f;
			}
		}

		private void window_Closed(object sender, FormClosedEventArgs e)
		{
			m_GrainPlayer.Kill();
		}

		private void loadSource1Btn_Click(object sender, EventArgs e)
		{
			m_LastLoadButtonClicked = 0;
			loadWaveFileDialog.ShowDialog();
		}

		private void loadSource2Btn_Click(object sender, EventArgs e)
		{
			m_LastLoadButtonClicked = 1;
			loadWaveFileDialog.ShowDialog();
		}

		private void loadWaveFileDialog_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;

			if (dialog != null)
			{
				string waveFileName = dialog.FileName;
				string grainFileName = waveFileName.Replace(Path.GetExtension(waveFileName), ".grn");
				m_GrainPlayer.InitMix(waveFileName, grainFileName);

				m_SubmixIndexes[m_LastLoadButtonClicked] = m_GrainPlayer.GetNumGranularMixes() - 1;
				RenderWaveView(m_SubmixIndexes[m_LastLoadButtonClicked], m_LastLoadButtonClicked);
				m_PictureBoxes[m_LastLoadButtonClicked].Image = m_WaveImages[m_LastLoadButtonClicked];
				m_PictureBoxes[m_LastLoadButtonClicked].Refresh();
				m_GroupBoxes[m_LastLoadButtonClicked].Text = dialog.FileName;
			}
		}

		private void GenerateBlendedFile()
		{
			if(m_GrainPlayer.GetNumGranularMixes() == 2)
			{
				m_BlendedWaveFile = new bwWaveFile();
				m_BlendedWaveFile.Format = m_GrainPlayer.GetGranularMix(0).m_WaveData.Format;

				List<audBlendedGrainData> blendedGrainTable = new List<audBlendedGrainData>();

				bwWaveFile waveDataSource1 = m_GrainPlayer.GetGranularMix(0).m_WaveData;
				List<float> rawDataSource1 = new List<float>((int)waveDataSource1.Data.NumSamples);
				List<GranularSubmix.audGrainData> grainDataSource1 = m_GrainPlayer.GetGranularMix(0).GetGranularSubmix().m_GrainTable;

				for (int loop = 0; loop < grainDataSource1.Count; loop++)
				{
					audBlendedGrainData blendedGrainData = new audBlendedGrainData();
					blendedGrainData.submixIndex = 0;
					blendedGrainData.grainIndex = loop;
					blendedGrainData.startSample = grainDataSource1[loop].startSample;
					blendedGrainData.pitch = grainDataSource1[loop].pitch;
					blendedGrainTable.Add(blendedGrainData);
				}

				for (int loop = 0; loop < waveDataSource1.Data.NumSamples; loop++)
				{
					rawDataSource1.Add(waveDataSource1.Data.ChannelData[0, loop]);
				}

				bwWaveFile waveDataSource2 = m_GrainPlayer.GetGranularMix(1).m_WaveData;
				List<float> rawDataSource2 = new List<float>((int)waveDataSource2.Data.NumSamples);
				List<GranularSubmix.audGrainData> grainDataSource2 = m_GrainPlayer.GetGranularMix(1).GetGranularSubmix().m_GrainTable;

				for (int loop = 0; loop < grainDataSource2.Count; loop++)
				{
					audBlendedGrainData blendedGrainData = new audBlendedGrainData();
					blendedGrainData.submixIndex = 1;
					blendedGrainData.grainIndex = loop;
					blendedGrainData.startSample = grainDataSource2[loop].startSample;
					blendedGrainData.pitch = grainDataSource2[loop].pitch;
					blendedGrainTable.Add(blendedGrainData);
				}

				for (int loop = 0; loop < waveDataSource2.Data.NumSamples; loop++)
				{
					rawDataSource2.Add(waveDataSource2.Data.ChannelData[0, loop]);
				}

				blendedGrainTable.Sort();
				List<float> blendedRawData = new List<float>();
				List<byte> blendedRawDataBytes = new List<byte>();

				for (int loop = 0; loop < waveDataSource1.Data.NumSamples + waveDataSource2.Data.NumSamples; loop++ )
				{
					blendedRawData.Add(0.0f);
				}

				int sampleIndex = 0;
				int prevGrainLength = 0;
				float crossfadeAmount = 0.05f;

				for (int loop = 0; loop < blendedGrainTable.Count; loop++)
				{
					blendedGrainTable[loop].finalStartSample = sampleIndex;

					List<float> rawData = null;
					List<GranularSubmix.audGrainData> grainData = null;

					if(blendedGrainTable[loop].submixIndex == 0)
					{
						rawData = rawDataSource1;
						grainData = grainDataSource1;
					}
					else
					{
						rawData = rawDataSource2;
						grainData = grainDataSource2;
					}

					int grainStart = grainData[blendedGrainTable[loop].grainIndex].startSample;

					if (blendedGrainTable[loop].grainIndex + 1 < grainData.Count)
					{
						int grainEnd = grainData[blendedGrainTable[loop].grainIndex + 1].startSample;
						int grainLength = grainEnd - grainStart;
						int crossfadeSamples = (int)(prevGrainLength * crossfadeAmount);

						for (int sampleLoop = grainStart; sampleLoop < grainEnd; sampleLoop++)
						{
							float crossfadeMultiplier = 1.0f;

							if (sampleLoop - grainStart < crossfadeSamples)
							{
								crossfadeMultiplier = (sampleLoop - grainStart) / (float)crossfadeSamples;
							}

							float sample = rawData[sampleLoop] * crossfadeMultiplier;
							blendedRawData[sampleIndex] += sample;
							sampleIndex++;
						}

						int crossfadeIndex = 0;
						crossfadeSamples = (int)(grainLength * crossfadeAmount);

						for (int sampleLoop = grainEnd; sampleLoop < grainEnd + crossfadeSamples && sampleLoop < rawData.Count; sampleLoop++)
						{
							float crossfadeMultiplier = 1.0f - ((sampleLoop - grainEnd) / (float)crossfadeSamples);
							float sample = rawData[sampleLoop] * crossfadeMultiplier;
							blendedRawData[sampleIndex + crossfadeIndex] += sample;
							crossfadeIndex++;
						}

						prevGrainLength = grainLength;
					}
				}

				/*
				float lastAverageVolume = -1.0f;

				for (int loop = 0; loop < blendedGrainTable.Count; loop++)
				{
					int grainStart = blendedGrainTable[loop].finalStartSample;

					if(loop + 1 < blendedGrainTable.Count)
					{
						int grainEnd = blendedGrainTable[loop + 1].finalStartSample;
						int grainLength = grainEnd - grainStart;

						float averageVolume = 0.0f;

						for (int sampleLoop = grainStart; sampleLoop < grainEnd; sampleLoop++)
						{
							averageVolume += Math.Abs(blendedRawData[sampleLoop]);
						}

						averageVolume /= ((float)(grainEnd - grainStart));

						if (blendedGrainTable[loop].submixIndex == 0)
						{
							lastAverageVolume = averageVolume;

							for (int sampleLoop = grainStart; sampleLoop < grainEnd; sampleLoop++)
							{
								blendedRawData[sampleLoop] *= 0.9f;
							}
						}
						else
						{
							if (lastAverageVolume < 0.0f)
							{
								lastAverageVolume = averageVolume;
							}

							float volumeCompensationRequired = lastAverageVolume / averageVolume;

							for (int sampleLoop = grainStart; sampleLoop < grainEnd; sampleLoop++)
							{
								blendedRawData[sampleLoop] *= 0.9f;
								blendedRawData[sampleLoop] *= volumeCompensationRequired;

								if(blendedRawData[sampleLoop] > 1.0f)
								{
									blendedRawData[sampleLoop] = 1.0f;
								}
								else if (blendedRawData[sampleLoop] < -1.0f)
								{
									blendedRawData[sampleLoop] = -1.0f;
								}
							}
						}
					}
				}
				*/

				for(int loop = 0; loop < blendedRawData.Count; loop++)
				{
					float sample = blendedRawData[loop];
					byte[] byteArray = BitConverter.GetBytes((Int16)(Int16.MaxValue * sample));
					blendedRawDataBytes.AddRange(byteArray);
				}

				bwDataChunk dataChunk = new bwDataChunk(blendedRawDataBytes.ToArray(), (uint)blendedRawData.Count);
				m_BlendedWaveFile.AddChunk(dataChunk);
				string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(waveDataSource1.FileName);
				string blendedFileName = waveDataSource1.FileName.Replace(fileNameWithoutExtension, fileNameWithoutExtension + "_blended");
				m_BlendedWaveFile.Save(blendedFileName);
			}
		}

		private void redrawTimer_Tick(object sender, EventArgs e)
		{
			grainSource1Pic.Refresh();
			grainSource2Pic.Refresh();
			blendedGrainPic.Refresh();
		}

		private void grainSource1Pic_Paint(object sender, PaintEventArgs e)
		{
			if (m_SubmixIndexes[0] != -1)
			{
				WaveViewOnPaint(e.Graphics, m_SubmixIndexes[0]);
			}
		}

		private void grainSource2Pic_Paint(object sender, PaintEventArgs e)
		{
			if (m_SubmixIndexes[1] != -1)
			{
				WaveViewOnPaint(e.Graphics, m_SubmixIndexes[1]);
			}
		}

		private void blendedGrainPic_Paint(object sender, PaintEventArgs e)
		{
		}

		private void WaveViewOnPaint(Graphics g, int submixIndex)
		{
			if (m_GrainPlayer.m_IsPlaying)
			{
				List<GranularSubmix.audGrainData> grainData = m_GrainPlayer.GetGranularMix(submixIndex).GetGranularSubmix().m_GrainTable;

				int grainIndex = 0;
				float volumeScale = 1.0f;

				if (m_GrainPlayer.m_GrainPlaybackStyle == GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug)
				{
					grainIndex = m_GrainPlayer.GetGranularMix(submixIndex).GetDebugSubmix().m_PrevSelectedGrain;
				}
				else
				{
					grainIndex = m_GrainPlayer.GetGranularMix(submixIndex).GetGranularSubmix().m_PrevSelectedGrain;
					volumeScale = m_GrainPlayer.GetGranularMix(submixIndex).GetGranularSubmix().m_LastVolumeScale;
				}

				if (grainIndex + 1 < grainData.Count)
				{
					g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb((int)(200 * volumeScale), 0, 255, 0)),
						(grainData[grainIndex].startSample - m_Xoffset) * m_Xscale[submixIndex],
						0,
						(grainData[grainIndex + 1].startSample - grainData[grainIndex].startSample) * m_Xscale[submixIndex],
						m_PictureBoxes[submixIndex].Height);
				}

				if (m_GrainPlayer.m_GrainPlaybackStyle != GrainPlayer.audGrainPlaybackStyle.PlaybackStyleDebug)
				{
					for (int loop = 0; loop < m_GrainPlayer.GetGranularMix(submixIndex).GetNumSynchronisedLoops(); loop++)
					{
						grainIndex = m_GrainPlayer.GetGranularMix(submixIndex).GetSynchronisedLoop(loop).m_PrevSelectedGrain;
						volumeScale = m_GrainPlayer.GetGranularMix(submixIndex).GetSynchronisedLoop(loop).m_LastVolumeScale;

						if (grainIndex + 1 < grainData.Count)
						{
							g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb((int)(200 * volumeScale), 0, 0, 255)),
								(grainData[grainIndex].startSample - m_Xoffset) * m_Xscale[submixIndex],
								0,
								(grainData[grainIndex + 1].startSample - grainData[grainIndex].startSample) * m_Xscale[submixIndex],
								m_PictureBoxes[submixIndex].Height);
						}
					}
				}
			}
		}

		private void RenderWaveView(int submixIndex, int waveSlotIndex)
		{
			Graphics g = Graphics.FromImage(m_WaveImages[waveSlotIndex]);

			Int32 lastx = 0, lasty = 0;
			var gp = new GraphicsPath();
			var p = Pens.DarkBlue;

			bwWaveFile waveData = m_GrainPlayer.GetGranularMix(submixIndex).m_WaveData;
			List<GranularSubmix.audGrainData> grainData = m_GrainPlayer.GetGranularMix(submixIndex).GetGranularSubmix().m_GrainTable;

			// scale x so that the entire sample fits in the visbounds
			m_Xscale[waveSlotIndex] = (m_WaveImages[waveSlotIndex].Width / (float)waveData.Data.NumSamples) * m_Xzoom;

			g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
							0,
							0,
							m_WaveImages[waveSlotIndex].Width,
							m_WaveImages[waveSlotIndex].Height);

			// Default step rate
			int stepRate = 30;

			float zoomPercentage = (m_Xzoom - 100) / 100.0f;

			if (zoomPercentage > 1.0f)
				zoomPercentage = 1.0f;

			if (zoomPercentage < 0.0f)
				zoomPercentage = 0.0f;

			// As we zoom in more, increase the accuracy
			stepRate -= (int)((stepRate - 1) * zoomPercentage);

			for (var i = m_Xoffset; i < waveData.Data.NumSamples; i += stepRate)
			{
				float sample = waveData.Data.ChannelData[0, i];

				// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
				sample *= -1.0f;
				sample += 1.0f;
				sample /= 2.0f;

				// now 0 -> 1.0f
				var y = (Int32)(sample * m_WaveImages[waveSlotIndex].Height);
				var x = i;

				x = (Int32)((x - m_Xoffset) * m_Xscale[waveSlotIndex]);

				y = (Int32)(y * m_Yscale);

				if (x > m_WaveImages[waveSlotIndex].Width &&
				   lastx > m_WaveImages[waveSlotIndex].Width)
				{
					break;
				}

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
					//g.DrawLine(p, lastx, lasty, x, y);
				}

				lastx = x;
				lasty = y;
			}

			// draw path
			g.DrawPath(p, gp);

			for (var grainLoop = 0; grainLoop < grainData.Count; grainLoop++)
			{
				var x = (Int32)((grainData[grainLoop].startSample - m_Xoffset) * m_Xscale[waveSlotIndex]);

				if (x > m_WaveImages[waveSlotIndex].Width)
				{
					break;
				}

				g.DrawLine(Pens.HotPink, x, m_WaveImages[waveSlotIndex].Height, x, 0);
			}

			for (int loop = 0; loop < m_GrainPlayer.GetGranularMix(submixIndex).GetNumSynchronisedLoops(); loop++)
			{
				GranularSubmix syncLoop = m_GrainPlayer.GetGranularMix(submixIndex).GetSynchronisedLoop(loop);

				for (int grainLoop = 0; grainLoop < syncLoop.m_ValidGrains.Count; grainLoop++)
				{
					if (syncLoop.m_ValidGrains[grainLoop] + 1 < grainData.Count)
					{
						g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(96, 0, 0, 255)),
						(grainData[syncLoop.m_ValidGrains[grainLoop]].startSample - m_Xoffset) * m_Xscale[waveSlotIndex],
						0,
						(grainData[syncLoop.m_ValidGrains[grainLoop] + 1].startSample - grainData[syncLoop.m_ValidGrains[grainLoop]].startSample) * m_Xscale[waveSlotIndex],
						m_WaveImages[waveSlotIndex].Height);
					}
				}
			}

			// draw x axis
			g.DrawLine(p, 0, m_WaveImages[waveSlotIndex].Height / 2, m_WaveImages[waveSlotIndex].Width, m_WaveImages[waveSlotIndex].Height / 2);

			// draw a line to show the end of the wave
			g.DrawLine(p, lastx, 0, lastx, m_WaveImages[waveSlotIndex].Height);

			var zoom = (int)(m_Xzoom * 100);
		}

		private void GenerateBlendBtn_Click(object sender, EventArgs e)
		{
			GenerateBlendedFile();
		}
	}
}
