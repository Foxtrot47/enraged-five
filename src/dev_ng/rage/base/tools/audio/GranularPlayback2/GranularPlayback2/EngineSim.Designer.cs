﻿namespace GranularPlayback2
{
	partial class EngineSim
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EngineSim));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.slot1Load = new System.Windows.Forms.Button();
			this.slot1WaveView = new System.Windows.Forms.PictureBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.slot2Load = new System.Windows.Forms.Button();
			this.slot2WaveView = new System.Windows.Forms.PictureBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.slot3Load = new System.Windows.Forms.Button();
			this.slot3WaveView = new System.Windows.Forms.PictureBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.slot4Load = new System.Windows.Forms.Button();
			this.slot4WaveView = new System.Windows.Forms.PictureBox();
			this.loadWaveFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.engineSimBox = new System.Windows.Forms.GroupBox();
			this.label12 = new System.Windows.Forms.Label();
			this.pitchScaleUpDown = new System.Windows.Forms.NumericUpDown();
			this.playTestSweepButton = new System.Windows.Forms.Button();
			this.minRepeatRateUpDown = new System.Windows.Forms.NumericUpDown();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.granularWindowSizeUpDown = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.playbackStyleCombo = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.loopBelowBiasUpDown = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.changeRateForMaxLoops = new System.Windows.Forms.NumericUpDown();
			this.changeRateForMaxGrains = new System.Windows.Forms.NumericUpDown();
			this.simulateEngineButton = new System.Windows.Forms.Button();
			this.throttleSlider = new System.Windows.Forms.VScrollBar();
			this.intertiaSlider = new System.Windows.Forms.VScrollBar();
			this.engineSimView = new System.Windows.Forms.PictureBox();
			this.redrawTimer = new System.Windows.Forms.Timer(this.components);
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.MaxGranularOffset = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.MinGranularOffset = new System.Windows.Forms.NumericUpDown();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.slot1WaveView)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.slot2WaveView)).BeginInit();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.slot3WaveView)).BeginInit();
			this.groupBox4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.slot4WaveView)).BeginInit();
			this.engineSimBox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pitchScaleUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.minRepeatRateUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.granularWindowSizeUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.loopBelowBiasUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxLoops)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxGrains)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.engineSimView)).BeginInit();
			this.groupBox5.SuspendLayout();
			this.groupBox6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.MaxGranularOffset)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.MinGranularOffset)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.slot1Load);
			this.groupBox1.Controls.Add(this.slot1WaveView);
			this.groupBox1.Location = new System.Drawing.Point(6, 17);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(1390, 91);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Slot 1";
			// 
			// slot1Load
			// 
			this.slot1Load.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.slot1Load.Location = new System.Drawing.Point(1314, 19);
			this.slot1Load.Name = "slot1Load";
			this.slot1Load.Size = new System.Drawing.Size(70, 66);
			this.slot1Load.TabIndex = 1;
			this.slot1Load.Text = "Load";
			this.slot1Load.UseVisualStyleBackColor = true;
			this.slot1Load.Click += new System.EventHandler(this.slot1Load_Click);
			// 
			// slot1WaveView
			// 
			this.slot1WaveView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.slot1WaveView.Location = new System.Drawing.Point(6, 19);
			this.slot1WaveView.Name = "slot1WaveView";
			this.slot1WaveView.Size = new System.Drawing.Size(1302, 66);
			this.slot1WaveView.TabIndex = 0;
			this.slot1WaveView.TabStop = false;
			this.slot1WaveView.Paint += new System.Windows.Forms.PaintEventHandler(this.slot1WaveView_Paint);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.slot2Load);
			this.groupBox2.Controls.Add(this.slot2WaveView);
			this.groupBox2.Location = new System.Drawing.Point(6, 114);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(1390, 91);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Slot 2";
			// 
			// slot2Load
			// 
			this.slot2Load.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.slot2Load.Location = new System.Drawing.Point(1314, 19);
			this.slot2Load.Name = "slot2Load";
			this.slot2Load.Size = new System.Drawing.Size(70, 66);
			this.slot2Load.TabIndex = 1;
			this.slot2Load.Text = "Load";
			this.slot2Load.UseVisualStyleBackColor = true;
			this.slot2Load.Click += new System.EventHandler(this.slot2Load_Click);
			// 
			// slot2WaveView
			// 
			this.slot2WaveView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.slot2WaveView.Location = new System.Drawing.Point(6, 19);
			this.slot2WaveView.Name = "slot2WaveView";
			this.slot2WaveView.Size = new System.Drawing.Size(1302, 66);
			this.slot2WaveView.TabIndex = 0;
			this.slot2WaveView.TabStop = false;
			this.slot2WaveView.Paint += new System.Windows.Forms.PaintEventHandler(this.slot2WaveView_Paint);
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.slot3Load);
			this.groupBox3.Controls.Add(this.slot3WaveView);
			this.groupBox3.Location = new System.Drawing.Point(6, 19);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(1390, 91);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Slot 1";
			// 
			// slot3Load
			// 
			this.slot3Load.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.slot3Load.Location = new System.Drawing.Point(1314, 19);
			this.slot3Load.Name = "slot3Load";
			this.slot3Load.Size = new System.Drawing.Size(70, 66);
			this.slot3Load.TabIndex = 1;
			this.slot3Load.Text = "Load";
			this.slot3Load.UseVisualStyleBackColor = true;
			this.slot3Load.Click += new System.EventHandler(this.slot3Load_Click);
			// 
			// slot3WaveView
			// 
			this.slot3WaveView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.slot3WaveView.Location = new System.Drawing.Point(6, 19);
			this.slot3WaveView.Name = "slot3WaveView";
			this.slot3WaveView.Size = new System.Drawing.Size(1302, 66);
			this.slot3WaveView.TabIndex = 0;
			this.slot3WaveView.TabStop = false;
			this.slot3WaveView.Paint += new System.Windows.Forms.PaintEventHandler(this.slot3WaveView_Paint);
			// 
			// groupBox4
			// 
			this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox4.Controls.Add(this.slot4Load);
			this.groupBox4.Controls.Add(this.slot4WaveView);
			this.groupBox4.Location = new System.Drawing.Point(6, 116);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(1390, 91);
			this.groupBox4.TabIndex = 3;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Slot 2";
			// 
			// slot4Load
			// 
			this.slot4Load.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.slot4Load.Location = new System.Drawing.Point(1314, 19);
			this.slot4Load.Name = "slot4Load";
			this.slot4Load.Size = new System.Drawing.Size(70, 66);
			this.slot4Load.TabIndex = 1;
			this.slot4Load.Text = "Load";
			this.slot4Load.UseVisualStyleBackColor = true;
			this.slot4Load.Click += new System.EventHandler(this.slot4Load_Click);
			// 
			// slot4WaveView
			// 
			this.slot4WaveView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.slot4WaveView.Location = new System.Drawing.Point(6, 19);
			this.slot4WaveView.Name = "slot4WaveView";
			this.slot4WaveView.Size = new System.Drawing.Size(1302, 66);
			this.slot4WaveView.TabIndex = 0;
			this.slot4WaveView.TabStop = false;
			this.slot4WaveView.Paint += new System.Windows.Forms.PaintEventHandler(this.slot4WaveView_Paint);
			// 
			// loadWaveFileDialog
			// 
			this.loadWaveFileDialog.FileName = "openFileDialog1";
			this.loadWaveFileDialog.Filter = "Wave files (*.wav)|*.wav";
			this.loadWaveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.loadWaveFileDialog_FileOk);
			// 
			// engineSimBox
			// 
			this.engineSimBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.engineSimBox.Controls.Add(this.MaxGranularOffset);
			this.engineSimBox.Controls.Add(this.label5);
			this.engineSimBox.Controls.Add(this.label10);
			this.engineSimBox.Controls.Add(this.MinGranularOffset);
			this.engineSimBox.Controls.Add(this.label12);
			this.engineSimBox.Controls.Add(this.pitchScaleUpDown);
			this.engineSimBox.Controls.Add(this.playTestSweepButton);
			this.engineSimBox.Controls.Add(this.minRepeatRateUpDown);
			this.engineSimBox.Controls.Add(this.label9);
			this.engineSimBox.Controls.Add(this.label8);
			this.engineSimBox.Controls.Add(this.granularWindowSizeUpDown);
			this.engineSimBox.Controls.Add(this.label7);
			this.engineSimBox.Controls.Add(this.label6);
			this.engineSimBox.Controls.Add(this.label4);
			this.engineSimBox.Controls.Add(this.playbackStyleCombo);
			this.engineSimBox.Controls.Add(this.label3);
			this.engineSimBox.Controls.Add(this.loopBelowBiasUpDown);
			this.engineSimBox.Controls.Add(this.label2);
			this.engineSimBox.Controls.Add(this.label1);
			this.engineSimBox.Controls.Add(this.changeRateForMaxLoops);
			this.engineSimBox.Controls.Add(this.changeRateForMaxGrains);
			this.engineSimBox.Controls.Add(this.simulateEngineButton);
			this.engineSimBox.Controls.Add(this.throttleSlider);
			this.engineSimBox.Controls.Add(this.intertiaSlider);
			this.engineSimBox.Controls.Add(this.engineSimView);
			this.engineSimBox.Location = new System.Drawing.Point(15, 448);
			this.engineSimBox.Name = "engineSimBox";
			this.engineSimBox.Size = new System.Drawing.Size(1422, 348);
			this.engineSimBox.TabIndex = 9;
			this.engineSimBox.TabStop = false;
			this.engineSimBox.Text = "Engine Simulator";
			// 
			// label12
			// 
			this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(697, 301);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(61, 13);
			this.label12.TabIndex = 21;
			this.label12.Text = "Pitch Scale";
			// 
			// pitchScaleUpDown
			// 
			this.pitchScaleUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.pitchScaleUpDown.DecimalPlaces = 2;
			this.pitchScaleUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.pitchScaleUpDown.Location = new System.Drawing.Point(764, 299);
			this.pitchScaleUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.pitchScaleUpDown.Name = "pitchScaleUpDown";
			this.pitchScaleUpDown.Size = new System.Drawing.Size(64, 20);
			this.pitchScaleUpDown.TabIndex = 20;
			this.pitchScaleUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
			this.pitchScaleUpDown.ValueChanged += new System.EventHandler(this.pitchScaleUpDown_ValueChanged);
			// 
			// playTestSweepButton
			// 
			this.playTestSweepButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.playTestSweepButton.Location = new System.Drawing.Point(137, 299);
			this.playTestSweepButton.Name = "playTestSweepButton";
			this.playTestSweepButton.Size = new System.Drawing.Size(133, 44);
			this.playTestSweepButton.TabIndex = 17;
			this.playTestSweepButton.Text = "Play Test Sweep";
			this.playTestSweepButton.UseVisualStyleBackColor = true;
			this.playTestSweepButton.Click += new System.EventHandler(this.playTestSweepButton_Click);
			// 
			// minRepeatRateUpDown
			// 
			this.minRepeatRateUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.minRepeatRateUpDown.Location = new System.Drawing.Point(1061, 322);
			this.minRepeatRateUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.minRepeatRateUpDown.Name = "minRepeatRateUpDown";
			this.minRepeatRateUpDown.Size = new System.Drawing.Size(62, 20);
			this.minRepeatRateUpDown.TabIndex = 16;
			this.minRepeatRateUpDown.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.minRepeatRateUpDown.ValueChanged += new System.EventHandler(this.minRepeatRateUpDown_ValueChanged);
			// 
			// label9
			// 
			this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(967, 324);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(88, 13);
			this.label9.TabIndex = 15;
			this.label9.Text = "Min Repeat Rate";
			// 
			// label8
			// 
			this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(921, 301);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(134, 13);
			this.label8.TabIndex = 14;
			this.label8.Text = "Granular Window Size (Hz)";
			// 
			// granularWindowSizeUpDown
			// 
			this.granularWindowSizeUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.granularWindowSizeUpDown.DecimalPlaces = 2;
			this.granularWindowSizeUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.granularWindowSizeUpDown.Location = new System.Drawing.Point(1061, 299);
			this.granularWindowSizeUpDown.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.granularWindowSizeUpDown.Name = "granularWindowSizeUpDown";
			this.granularWindowSizeUpDown.Size = new System.Drawing.Size(62, 20);
			this.granularWindowSizeUpDown.TabIndex = 13;
			this.granularWindowSizeUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.granularWindowSizeUpDown.ValueChanged += new System.EventHandler(this.granularWindowSizeUpDown_ValueChanged);
			// 
			// label7
			// 
			this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(1343, 301);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(30, 13);
			this.label7.TabIndex = 12;
			this.label7.Text = "Rate";
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(1376, 301);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(43, 13);
			this.label6.TabIndex = 11;
			this.label6.Text = "Throttle";
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(499, 324);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(77, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Playback Style";
			// 
			// playbackStyleCombo
			// 
			this.playbackStyleCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.playbackStyleCombo.FormattingEnabled = true;
			this.playbackStyleCombo.Items.AddRange(new object[] {
            "Loops and Grains",
            "Loops Only",
            "Grains Only"});
			this.playbackStyleCombo.Location = new System.Drawing.Point(582, 321);
			this.playbackStyleCombo.Name = "playbackStyleCombo";
			this.playbackStyleCombo.Size = new System.Drawing.Size(109, 21);
			this.playbackStyleCombo.TabIndex = 9;
			this.playbackStyleCombo.Text = "Loops and Grains";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(490, 301);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(86, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Loop Below Bias";
			// 
			// loopBelowBiasUpDown
			// 
			this.loopBelowBiasUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.loopBelowBiasUpDown.DecimalPlaces = 2;
			this.loopBelowBiasUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.loopBelowBiasUpDown.Location = new System.Drawing.Point(582, 299);
			this.loopBelowBiasUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.loopBelowBiasUpDown.Name = "loopBelowBiasUpDown";
			this.loopBelowBiasUpDown.Size = new System.Drawing.Size(109, 20);
			this.loopBelowBiasUpDown.TabIndex = 8;
			this.loopBelowBiasUpDown.Value = new decimal(new int[] {
            15,
            0,
            0,
            65536});
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(276, 324);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(141, 13);
			this.label2.TabIndex = 7;
			this.label2.Text = "Change Rate for Max Grains";
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(276, 301);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(140, 13);
			this.label1.TabIndex = 6;
			this.label1.Text = "Change Rate for Max Loops";
			// 
			// changeRateForMaxLoops
			// 
			this.changeRateForMaxLoops.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.changeRateForMaxLoops.DecimalPlaces = 2;
			this.changeRateForMaxLoops.Location = new System.Drawing.Point(422, 299);
			this.changeRateForMaxLoops.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.changeRateForMaxLoops.Name = "changeRateForMaxLoops";
			this.changeRateForMaxLoops.Size = new System.Drawing.Size(62, 20);
			this.changeRateForMaxLoops.TabIndex = 5;
			// 
			// changeRateForMaxGrains
			// 
			this.changeRateForMaxGrains.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.changeRateForMaxGrains.DecimalPlaces = 2;
			this.changeRateForMaxGrains.Location = new System.Drawing.Point(423, 322);
			this.changeRateForMaxGrains.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.changeRateForMaxGrains.Name = "changeRateForMaxGrains";
			this.changeRateForMaxGrains.Size = new System.Drawing.Size(61, 20);
			this.changeRateForMaxGrains.TabIndex = 4;
			this.changeRateForMaxGrains.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// simulateEngineButton
			// 
			this.simulateEngineButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.simulateEngineButton.Location = new System.Drawing.Point(6, 298);
			this.simulateEngineButton.Name = "simulateEngineButton";
			this.simulateEngineButton.Size = new System.Drawing.Size(125, 44);
			this.simulateEngineButton.TabIndex = 3;
			this.simulateEngineButton.Text = "Play";
			this.simulateEngineButton.UseVisualStyleBackColor = true;
			this.simulateEngineButton.Click += new System.EventHandler(this.simulateEngineButton_Click);
			// 
			// throttleSlider
			// 
			this.throttleSlider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.throttleSlider.Location = new System.Drawing.Point(1391, 19);
			this.throttleSlider.Name = "throttleSlider";
			this.throttleSlider.Size = new System.Drawing.Size(18, 270);
			this.throttleSlider.TabIndex = 2;
			// 
			// intertiaSlider
			// 
			this.intertiaSlider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.intertiaSlider.Location = new System.Drawing.Point(1346, 19);
			this.intertiaSlider.Name = "intertiaSlider";
			this.intertiaSlider.Size = new System.Drawing.Size(18, 270);
			this.intertiaSlider.TabIndex = 1;
			// 
			// engineSimView
			// 
			this.engineSimView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.engineSimView.Location = new System.Drawing.Point(6, 19);
			this.engineSimView.Name = "engineSimView";
			this.engineSimView.Size = new System.Drawing.Size(1334, 273);
			this.engineSimView.TabIndex = 0;
			this.engineSimView.TabStop = false;
			this.engineSimView.Paint += new System.Windows.Forms.PaintEventHandler(this.engineSimView_Paint);
			// 
			// redrawTimer
			// 
			this.redrawTimer.Enabled = true;
			this.redrawTimer.Interval = 10;
			this.redrawTimer.Tick += new System.EventHandler(this.redrawTimer_Tick);
			// 
			// groupBox5
			// 
			this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox5.Controls.Add(this.groupBox1);
			this.groupBox5.Controls.Add(this.groupBox2);
			this.groupBox5.Location = new System.Drawing.Point(15, 12);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(1409, 216);
			this.groupBox5.TabIndex = 10;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Accel";
			// 
			// groupBox6
			// 
			this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox6.Controls.Add(this.groupBox3);
			this.groupBox6.Controls.Add(this.groupBox4);
			this.groupBox6.Location = new System.Drawing.Point(15, 228);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(1409, 214);
			this.groupBox6.TabIndex = 11;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Decel";
			// 
			// MaxGranularOffset
			// 
			this.MaxGranularOffset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.MaxGranularOffset.Location = new System.Drawing.Point(1275, 322);
			this.MaxGranularOffset.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.MaxGranularOffset.Name = "MaxGranularOffset";
			this.MaxGranularOffset.Size = new System.Drawing.Size(62, 20);
			this.MaxGranularOffset.TabIndex = 25;
			this.MaxGranularOffset.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.MaxGranularOffset.ValueChanged += new System.EventHandler(this.MaxGranularOffset_ValueChanged);
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(1129, 324);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(143, 13);
			this.label5.TabIndex = 24;
			this.label5.Text = "Max Granular Window Offset";
			this.label5.Click += new System.EventHandler(this.label5_Click);
			// 
			// label10
			// 
			this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(1129, 301);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(140, 13);
			this.label10.TabIndex = 23;
			this.label10.Text = "Min Granular Window Offset";
			this.label10.Click += new System.EventHandler(this.label10_Click);
			// 
			// MinGranularOffset
			// 
			this.MinGranularOffset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.MinGranularOffset.Location = new System.Drawing.Point(1275, 299);
			this.MinGranularOffset.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.MinGranularOffset.Name = "MinGranularOffset";
			this.MinGranularOffset.Size = new System.Drawing.Size(62, 20);
			this.MinGranularOffset.TabIndex = 22;
			this.MinGranularOffset.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.MinGranularOffset.ValueChanged += new System.EventHandler(this.MinGranularOffset_ValueChanged);
			// 
			// EngineSim
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1446, 808);
			this.Controls.Add(this.groupBox6);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.engineSimBox);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "EngineSim";
			this.Text = "Engine Simulator";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.window_Closed);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.slot1WaveView)).EndInit();
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.slot2WaveView)).EndInit();
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.slot3WaveView)).EndInit();
			this.groupBox4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.slot4WaveView)).EndInit();
			this.engineSimBox.ResumeLayout(false);
			this.engineSimBox.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pitchScaleUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.minRepeatRateUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.granularWindowSizeUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.loopBelowBiasUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxLoops)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.changeRateForMaxGrains)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.engineSimView)).EndInit();
			this.groupBox5.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.MaxGranularOffset)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.MinGranularOffset)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button slot1Load;
		private System.Windows.Forms.PictureBox slot1WaveView;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button slot2Load;
		private System.Windows.Forms.PictureBox slot2WaveView;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button slot3Load;
		private System.Windows.Forms.PictureBox slot3WaveView;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Button slot4Load;
		private System.Windows.Forms.PictureBox slot4WaveView;
		private System.Windows.Forms.OpenFileDialog loadWaveFileDialog;
		private System.Windows.Forms.GroupBox engineSimBox;
		private System.Windows.Forms.Button playTestSweepButton;
		private System.Windows.Forms.NumericUpDown minRepeatRateUpDown;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.NumericUpDown granularWindowSizeUpDown;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox playbackStyleCombo;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown loopBelowBiasUpDown;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown changeRateForMaxLoops;
		private System.Windows.Forms.NumericUpDown changeRateForMaxGrains;
		private System.Windows.Forms.Button simulateEngineButton;
		private System.Windows.Forms.VScrollBar throttleSlider;
		private System.Windows.Forms.VScrollBar intertiaSlider;
		private System.Windows.Forms.PictureBox engineSimView;
		private System.Windows.Forms.Timer redrawTimer;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.NumericUpDown pitchScaleUpDown;
		private System.Windows.Forms.NumericUpDown MaxGranularOffset;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.NumericUpDown MinGranularOffset;
	}
}