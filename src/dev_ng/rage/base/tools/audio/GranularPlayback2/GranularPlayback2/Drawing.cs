﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using System.Threading;

namespace GranularPlayback2
{
	public class Drawing
	{
		public static void DrawSpectrogram(List<AnalysisFrame> analysisFrames, System.Windows.Forms.PictureBox parent, uint sampleRate, uint maxVisibleFrequency, float volumeScale)
		{
			if (parent.Width == 0 || parent.Height == 0)
			{
				return;
			}

			if (analysisFrames.Count == 0)
			{
				return;
			}

			Bitmap waveImage = new Bitmap(parent.Width, (int)(parent.Height - 40));
			parent.Image = waveImage;

			double maxAmplitude = float.MinValue;

			for (int frame = 0; frame < analysisFrames.Count; frame++)
			{
				for (int complexDataIndex = 0; complexDataIndex < analysisFrames[0].ComplexFrameData.Length; complexDataIndex++)
				{
					maxAmplitude = Math.Max(maxAmplitude, analysisFrames[frame].ComplexFrameData[complexDataIndex].Modulus * volumeScale);
				}
			}

			double maxVolumeDB = 20.0 * Math.Log10(maxAmplitude);
			maxAmplitude = AnalysisFrame.Clamp(1.0f - (Math.Abs(maxVolumeDB) * 0.01f), 0.0f, 1.0f);

			float zoomFraction = maxVisibleFrequency / (float)sampleRate;

			for (int xPixel = 0; xPixel < (waveImage.Width - 40); xPixel++)
			{
				double xFraction = xPixel / (double)(waveImage.Width - 40);
				double frameIndex = xFraction * (analysisFrames.Count - 1);

				double frameFraction = frameIndex - (int)Math.Floor(frameIndex);
				int frameBeforeIndex = (int)Math.Floor(frameIndex);
				int frameAfterIndex = (int)Math.Ceiling(frameIndex);

				AnalysisFrame frameBefore = analysisFrames[frameBeforeIndex];
				AnalysisFrame frameAfter = analysisFrames[frameAfterIndex];

				for (int yPixel = 0; yPixel < waveImage.Height; yPixel++)
				{
					double yFraction = yPixel / (double)waveImage.Height;
					yFraction *= zoomFraction;

					double complexDataIndex = yFraction * (analysisFrames[0].ComplexFrameData.Length - 1);
					double complexDataFraction = complexDataIndex - (int)Math.Floor(complexDataIndex);

					double frameBeforeVolumeBelow = frameBefore.ComplexFrameData[(int)Math.Floor(complexDataIndex)].Modulus;
					double frameBeforeVolumeAbove = frameBefore.ComplexFrameData[(int)Math.Ceiling(complexDataIndex)].Modulus;
					double frameBeforeVolume = frameBeforeVolumeBelow + ((frameBeforeVolumeAbove - frameBeforeVolumeBelow) * complexDataFraction);

					double frameAfterVolumeBelow = frameAfter.ComplexFrameData[(int)Math.Floor(complexDataIndex)].Modulus;
					double frameAfterVolumeAbove = frameAfter.ComplexFrameData[(int)Math.Ceiling(complexDataIndex)].Modulus;
					double frameAfterVolume = frameAfterVolumeBelow + ((frameAfterVolumeAbove - frameAfterVolumeBelow) * complexDataFraction);

					double finalVolume = (frameBeforeVolume + ((frameAfterVolume - frameBeforeVolume) * frameFraction)) * volumeScale;
					double dbVolume = 20.0 * Math.Log10(finalVolume);
					finalVolume = AnalysisFrame.Clamp(1.0f - (Math.Abs(dbVolume) * 0.01f), 0.0f, 1.0f);

					int r, g, b;
					HSVColor.HsvToRgb(240 * (1.0f - (finalVolume / maxAmplitude)), 1.0f, 1.0f, out r, out g, out b);
					waveImage.SetPixel(xPixel + 40, waveImage.Height - 1 - yPixel, Color.FromArgb(r, g, b));
				}
			}
		}

		public static void DrawWaveView(float[] sampleData, System.Windows.Forms.PictureBox parent, PaintEventArgs pea, int padding, uint sampleRate)
		{
			Graphics g = pea.Graphics;
			Int32 lastx = 40, lasty = 0;
			var gp = new GraphicsPath();
			var p = Pens.DarkBlue;

			int offset = 0 + padding;
			float zoom = 1.0f;
			float yScale = 1.0f;
			float xScale = ((parent.Width - 40) / (float)sampleData.Length) * zoom;

			DrawMillisecondsAxis(g, parent, (uint)sampleData.Length, true, sampleRate);
			DrawAmplitudeAxis(g, parent);

			// Default step rate
			int stepRate = 10;

			float zoomPercentage = (zoom - 100) / 100.0f;

			if (zoomPercentage > 1.0f)
				zoomPercentage = 1.0f;

			if (zoomPercentage < 0.0f)
				zoomPercentage = 0.0f;

			// As we zoom in more, increase the accuracy
			stepRate -= (int)((stepRate - 1) * zoomPercentage);

			for (var i = offset; i < sampleData.Length; i += stepRate)
			{
				float sample = sampleData[i];

				// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
				sample *= -1.0f;
				sample += 1.0f;
				sample /= 2.0f;

				// now 0 -> 1.0f
				var y = (Int32)(sample * (parent.Height - 40));
				var x = i;

				x = (Int32)((x - offset) * xScale);
				x += 40;

				y = (Int32)(y * yScale);

				if (x > parent.Width &&
				   lastx > parent.Width)
				{
					break;
				}

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
				}

				lastx = x;
				lasty = y;
			}

			// draw path
			g.DrawPath(p, gp);

			// draw x axis
			g.DrawLine(p, 40, (parent.Height - 40) / 2.0f, parent.Width, (parent.Height - 40) / 2.0f );


			/*
			gp = new GraphicsPath();
			p = new Pen(Color.Black);

			float frameXFraction = frameIndexSlider.Value / (float)m_AnalysisFrames.Count;
			float frameXCoord = 40 + (frameXFraction * (parent.Width - 40));
			gp.AddLine(frameXCoord, 0.0f, frameXCoord, parent.Height - 40);
			g.DrawPath(p, gp);
			*/
		}

		public static void DrawMillisecondsAxis(Graphics g, System.Windows.Forms.PictureBox parent, uint numSamples, bool fillRectangle, uint sampleRate)
		{
			string drawString = "Time (ms)";
			System.Drawing.Font drawFont = new System.Drawing.Font("Microsoft Sans Serif", 8);
			System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
			System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();
			drawFormat.Alignment = StringAlignment.Center;
			drawFormat.LineAlignment = StringAlignment.Center;

			g.DrawString(drawString, drawFont, drawBrush, ((parent.Width - 40) * 0.5f) + 40, parent.Height - 10, drawFormat);
			drawFont.Dispose();
			drawBrush.Dispose();

			if (fillRectangle)
			{
				g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(255, 255, 255)),
							40,
							0,
							parent.Width - 40,
							(int)(parent.Height - 40));
			}

			var p = Pens.DarkBlue;
			g.DrawRectangle(p, new Rectangle(40, 0, parent.Width - 1 - 40, (int)(parent.Height - 40)));

			float assetDuration = numSamples / (float)sampleRate;
			float timeStepRate = assetDuration * 0.1f;
			timeStepRate = (float)Math.Round(timeStepRate / 0.1f) * 0.1f;

			if (timeStepRate == 0.0f)
			{
				timeStepRate = assetDuration * 0.1f;
				timeStepRate = (float)Math.Round(timeStepRate / 0.01f) * 0.01f;
			}

			System.Drawing.Font timeFont = new System.Drawing.Font("Microsoft Sans Serif", 7);
			System.Drawing.SolidBrush timeBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
			System.Drawing.StringFormat timeFormat = new System.Drawing.StringFormat();
			timeFormat.Alignment = StringAlignment.Center;
			timeFormat.LineAlignment = StringAlignment.Center;

			for (float time = timeStepRate; time < assetDuration; time += timeStepRate)
			{
				float timeFraction = time / assetDuration;

				if (timeFraction < 0.975f)
				{
					string timeString = ((int)(time * 1000)).ToString();
					g.DrawString(timeString, timeFont, timeBrush, ((parent.Width - 40) * timeFraction) + 40, parent.Height - 30, timeFormat);
					g.DrawLine(p, ((parent.Width - 40) * timeFraction) + 40, parent.Height - 40, ((parent.Width - 40) * timeFraction) + 40, parent.Height - 45);
				}
			}

			timeFont.Dispose();
			timeBrush.Dispose();
		}

		public static void DrawFrequencyAxis(Graphics g, System.Windows.Forms.PictureBox parent, uint sampleRate, bool drawRPMValue, int numCylinders)
		{
			var p = Pens.DarkBlue;
			float maxFrequency = sampleRate / 2;

			if (drawRPMValue)
			{
				maxFrequency = (maxFrequency * 120.0f) / (float)numCylinders;
			}


			float freqStepRate = maxFrequency * 0.1f;
			freqStepRate = (float)Math.Round(freqStepRate / 1000.0f) * 1000.0f;

			if (freqStepRate == 0.0f)
			{
				freqStepRate = maxFrequency * 0.1f;
				freqStepRate = (float)Math.Round(freqStepRate / 100.0f) * 100.0f;

				if (freqStepRate == 0.0f)
				{
					freqStepRate = maxFrequency * 0.1f;
					freqStepRate = (float)Math.Round(freqStepRate / 10.0f) * 10.0f;

					if (freqStepRate == 0.0f)
					{
						freqStepRate = maxFrequency * 0.1f;
						freqStepRate = (float)Math.Round(freqStepRate / 1.0f) * 1.0f;
					}
				}
			}

			if (freqStepRate == 0.0f)
			{
				return;
			}

			System.Drawing.Font freqFont = new System.Drawing.Font("Microsoft Sans Serif", drawRPMValue? 6 : 7);
			System.Drawing.SolidBrush freqBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
			System.Drawing.StringFormat freqFormat = new System.Drawing.StringFormat();
			freqFormat.Alignment = StringAlignment.Far;
			freqFormat.LineAlignment = StringAlignment.Center;

			System.Drawing.Font drawFont = new System.Drawing.Font("Microsoft Sans Serif", drawRPMValue ? 7 : 8);
			g.TranslateTransform(7, (parent.Height - 40) / 2);
			g.RotateTransform(-90);

			if (drawRPMValue)
			{
				SizeF textSize = g.MeasureString("RPM", drawFont);
				g.DrawString("RPM", drawFont, freqBrush, -(textSize.Width / 2), -(textSize.Height / 2));
			}
			else
			{
				if (maxFrequency < 10000)
				{
					maxFrequency *= 1000.0f;
					freqStepRate *= 1000.0f;

					SizeF textSize = g.MeasureString("Frequency (Hz)", freqFont);
					g.DrawString("Frequency (Hz)", drawFont, freqBrush, -(textSize.Width / 2), -(textSize.Height / 2));
				}
				else
				{
					SizeF textSize = g.MeasureString("Frequency (kHz)", freqFont);
					g.DrawString("Frequency (kHz)", drawFont, freqBrush, -(textSize.Width / 2), -(textSize.Height / 2));
				}
			}

			g.ResetTransform();

			for (float freq = freqStepRate; freq < maxFrequency; freq += freqStepRate)
			{
				float freqFraction = freq / maxFrequency;

				if (freqFraction < 0.95f)
				{
					string freqString;

					if (drawRPMValue)
					{
						freqString = freq.ToString("0");
					}
					else if (maxFrequency < 10000.0f)
					{
						freqString = (freq / 1000.0f).ToString("0.00");
					}
					else
					{
						freqString = ((int)(freq / 1000.0f)).ToString();
					}

					g.DrawString(freqString, freqFont, freqBrush, drawRPMValue? 38 : 35, (parent.Height - 40) * (1.0f - freqFraction), freqFormat);
					g.DrawLine(p, 40, (parent.Height - 40) * (1.0f - freqFraction), 45, (parent.Height - 40) * (1.0f - freqFraction));
				}
			}			

			freqFont.Dispose();
			freqBrush.Dispose();

			/*
			GraphicsPath gp = new GraphicsPath();
			p = new Pen(Color.Black);
			float frameXFraction = frameIndexSlider.Value / (float)m_AnalysisFrames.Count;
			float frameXCoord = 40 + (frameXFraction * (parent.Width - 40));
			gp.AddLine(frameXCoord, 0.0f, frameXCoord, parent.Height - 40);
			g.DrawPath(p, gp);
			*/
		}

		public static void DrawHorizontalFrequencyAxis(Graphics g, System.Windows.Forms.PictureBox parent, int maxFrequency, bool fillRectangle, bool drawRPMValue, int numCylinders)
		{
			if (drawRPMValue)
			{
				maxFrequency = (int)((maxFrequency * 120.0f) / (float)numCylinders);
			}

			string drawString = drawRPMValue? "RPM" : "Frequency (hz)";
			System.Drawing.Font drawFont = new System.Drawing.Font("Microsoft Sans Serif", 8);
			System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
			System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();
			drawFormat.Alignment = StringAlignment.Center;
			drawFormat.LineAlignment = StringAlignment.Center;

			g.DrawString(drawString, drawFont, drawBrush, ((parent.Width - 40) * 0.5f) + 40, parent.Height - 10, drawFormat);
			drawFont.Dispose();
			drawBrush.Dispose();

			if (fillRectangle)
			{
				g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(255, 255, 255)),
							40,
							0,
							parent.Width - 40,
							(int)(parent.Height - 40));
			}

			var p = Pens.DarkBlue;
			g.DrawRectangle(p, new Rectangle(40, 0, parent.Width - 1 - 40, (int)(parent.Height - 40)));

			float freqStepRate = maxFrequency * 0.1f;
			freqStepRate = (float)Math.Round(freqStepRate / 0.1f) * 0.1f;

			if (freqStepRate == 0.0f)
			{
				freqStepRate = maxFrequency * 0.1f;
				freqStepRate = (float)Math.Round(freqStepRate / 0.01f) * 0.01f;
			}

			System.Drawing.Font freqFont = new System.Drawing.Font("Microsoft Sans Serif", 7);
			System.Drawing.SolidBrush freqBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
			System.Drawing.StringFormat freqFormat = new System.Drawing.StringFormat();
			freqFormat.Alignment = StringAlignment.Center;
			freqFormat.LineAlignment = StringAlignment.Center;

			for (float frequency = freqStepRate; frequency < maxFrequency; frequency += freqStepRate)
			{
				float freqFraction = frequency / maxFrequency;

				if (freqFraction < 0.95f)
				{
					string freqString = ((int)(frequency)).ToString();
					g.DrawString(freqString, freqFont, freqBrush, ((parent.Width - 40) * freqFraction) + 40, parent.Height - 30, freqFormat);
					g.DrawLine(p, ((parent.Width - 40) * freqFraction) + 40, parent.Height - 40, ((parent.Width - 40) * freqFraction) + 40, parent.Height - 45);
				}
			}

			freqFont.Dispose();
			freqBrush.Dispose();
		}

		public static void DrawAmplitudeAxis(Graphics g, System.Windows.Forms.PictureBox parent)
		{
			var p = Pens.DarkBlue;
			System.Drawing.Font freqFont = new System.Drawing.Font("Microsoft Sans Serif", 7);
			System.Drawing.SolidBrush freqBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
			System.Drawing.StringFormat freqFormat = new System.Drawing.StringFormat();
			freqFormat.Alignment = StringAlignment.Far;
			freqFormat.LineAlignment = StringAlignment.Center;

			freqFormat.LineAlignment = StringAlignment.Near;
			g.DrawString("1", freqFont, freqBrush, 35, (parent.Height - 40) * 0.0f, freqFormat);

			freqFormat.LineAlignment = StringAlignment.Center;
			g.DrawString("0.5", freqFont, freqBrush, 35, (parent.Height - 40) * 0.25f, freqFormat);
			g.DrawLine(p, 40, (parent.Height - 40) * 0.25f, 45, (parent.Height - 40) * 0.25f);

			g.DrawString("0", freqFont, freqBrush, 35, (parent.Height - 40) * 0.5f, freqFormat);

			g.DrawString("-0.5", freqFont, freqBrush, 35, (parent.Height - 40) * 0.75f, freqFormat);
			g.DrawLine(p, 40, (parent.Height - 40) * 0.75f, 45, (parent.Height - 40) * 0.75f);

			freqFormat.LineAlignment = StringAlignment.Far;
			g.DrawString("-1", freqFont, freqBrush, 35, (parent.Height - 40) * 1.0f, freqFormat);

			System.Drawing.Font drawFont = new System.Drawing.Font("Microsoft Sans Serif", 8);
			g.TranslateTransform(7, (parent.Height - 40) / 2);
			g.RotateTransform(-90);
			SizeF textSize = g.MeasureString("Amplitude", freqFont);
			g.DrawString("Amplitude", drawFont, freqBrush, -(textSize.Width / 2), -(textSize.Height / 2));

			g.ResetTransform();
			freqFont.Dispose();
			freqBrush.Dispose();
		}
	}
}
