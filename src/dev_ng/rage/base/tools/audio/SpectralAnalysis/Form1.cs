﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Threading;

using MathNet.Numerics;
using MathNet.Numerics.Transformations;

using NAudio.Dsp;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;

using Wavelib; 

namespace SpectralAnalysis
{
	public partial class Form1 : Form
	{
		#region VARIABLES
		ProgressBar m_ProgressBarForm;
		Color[] m_RandomColors;
		WindowingFunction m_WindowingFunction = new WindowingFunction();
		List<AnalysisFrame> m_AnalysisFrames = new List<AnalysisFrame>();
		List<AnalysisFrame> m_AnalysisFramesResidual = new List<AnalysisFrame>();
		List<AnalysisFrame> m_AnalysisFramesSine = new List<AnalysisFrame>();
		PeakTracker m_PeakTracker = new PeakTracker();
		public Thread m_AnalysisThread;
		string m_WaveFileName;

		bwWaveFile m_WaveFileOriginal;

		public static double m_PlaybackFraction;

		float[] m_SampleDataOriginal;
		float[] m_SampleDataResidual;
		float[] m_SampleDataSine;
		float[] m_SampleDataOutput;

		public int m_AnalysisProgress = 0;
		int m_HopSize = 256;
		int m_FrameSize = 2048;
		int m_SampleRate = 44100;
		int m_NumPeaksToTrack = 100;
		int m_NumSinesToTrack = 50;
		int m_MinPeakSpace = 2;
		int m_Padding = 0;
		int m_MaxTrackedFrequency = -1;
		int m_ZeroPaddingCoefficient = 2;			
		#endregion

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			windowingFnGraph.Refresh();
			fftMagnitudeGraph.Refresh();
		}

		public void GenerateUniqueRandomColors(int count)
		{
			m_RandomColors = new Color[count];
			Random randomColor = new Random();

			for (int i = 0; i < count; i++)
			{
				m_RandomColors[i] = Color.FromArgb(255, randomColor.Next(0, 170), randomColor.Next(0, 170));
			}
		}

		void WindowSizeChanged(object sender, System.EventArgs e)
		{
			resizeTimer.Enabled = true;
			resizeTimer.Stop();
			resizeTimer.Start();
		}

		private void AnalyseFile()
		{
			m_Padding = 0;

			m_WaveFileOriginal = new bwWaveFile(m_WaveFileName);
			m_SampleDataOriginal = GetWaveSampleData(m_WaveFileOriginal, m_FrameSize, m_HopSize, true, out m_Padding);
			
			double[] analysisWindow = GenerateAnalysisWindow(m_FrameSize);
			int expectedFrames = (int)Math.Ceiling((m_SampleDataOriginal.Length - m_FrameSize) / (float)m_HopSize);
			m_AnalysisFrames.Clear();

			int sampleIndex = 0;
			int numNewPeaks = m_NumSinesToTrack;
			GenerateUniqueRandomColors(m_NumSinesToTrack);

			while (sampleIndex + m_FrameSize < m_SampleDataOriginal.Length)
			{
				AnalysisFrame analysisFrame = new AnalysisFrame();
				analysisFrame.Analyse(m_SampleDataOriginal, analysisWindow, sampleIndex, m_FrameSize, m_ZeroPaddingCoefficient, m_NumPeaksToTrack, m_MinPeakSpace, m_SampleRate, m_MaxTrackedFrequency);
				m_AnalysisFrames.Add(analysisFrame);

				sampleIndex += m_HopSize;
				m_AnalysisProgress = (int)((m_AnalysisFrames.Count / (float)expectedFrames) * 80.0f);
			}

			int peakTrackingStartFrame = (int)(m_AnalysisFrames.Count * (float)peakTrackingStartFraction.Value);
			int inactiveFrameTrackingLimit = (int)inactiveFrameTrackingLimitUpDown.Value;
			int inactiveFramePlaybackLimit = (int)inactiveFramePlaybackLimitUpDown.Value;
			double maxFrequencyDifference = (double)maxFreqDifference.Value;

			m_PeakTracker.m_TrackedPeaks.Clear();
			m_PeakTracker.TrackPeaksAdvanced(m_AnalysisFrames, m_NumSinesToTrack, m_SampleRate, (double)maxFreqDifference.Value, this, peakTrackingStartFrame, true, inactiveFrameTrackingLimit, logarithmicTrackingCheckbox.Checked);
			m_PeakTracker.TrackPeaksAdvanced(m_AnalysisFrames, m_NumSinesToTrack, m_SampleRate, (double)maxFreqDifference.Value, this, peakTrackingStartFrame - 1, false, inactiveFrameTrackingLimit, logarithmicTrackingCheckbox.Checked);
			m_PeakTracker.FixupPeaks(inactiveFramePlaybackLimit, maxFrequencyDifference);

			m_SampleDataSine = new float[m_SampleDataOriginal.Length];
			GeneratePhaseMatchedSineOutput(m_SampleDataSine, false);
			//GenerateSineOutput(m_SampleDataOriginal);

			m_SampleDataResidual = new float[m_SampleDataOriginal.Length];

			for (int loop = m_Padding; loop < m_SampleDataOriginal.Length; loop++)
			{
				m_SampleDataResidual[loop] = m_SampleDataOriginal[loop] - m_SampleDataSine[loop];
			}

			m_SampleDataOutput = new float[m_SampleDataOriginal.Length];

			for (int loop = 0; loop < m_SampleDataOriginal.Length; loop++)
			{
				m_SampleDataOutput[loop] = m_SampleDataSine[loop] + m_SampleDataResidual[loop];
			}

			Console.Out.WriteLine("Generated " + m_PeakTracker.m_TrackedPeaks[0].m_FrameData.Count.ToString() + " frames of data");

			m_AnalysisFramesResidual.Clear();
			m_AnalysisFramesSine.Clear();

			sampleIndex = 0;

			while (sampleIndex + m_FrameSize < m_SampleDataOriginal.Length)
			{
				AnalysisFrame analysisFrameSine = new AnalysisFrame();
				analysisFrameSine.Analyse(m_SampleDataSine, analysisWindow, sampleIndex, m_FrameSize, m_ZeroPaddingCoefficient, m_NumPeaksToTrack, m_MinPeakSpace, m_SampleRate, m_MaxTrackedFrequency);
				m_AnalysisFramesSine.Add(analysisFrameSine);

				AnalysisFrame analysisFrameResidual = new AnalysisFrame();
				analysisFrameResidual.Analyse(m_SampleDataResidual, analysisWindow, sampleIndex, m_FrameSize, m_ZeroPaddingCoefficient, m_NumPeaksToTrack, m_MinPeakSpace, m_SampleRate, m_MaxTrackedFrequency);
				m_AnalysisFramesResidual.Add(analysisFrameResidual);

				sampleIndex += m_HopSize;
				m_AnalysisProgress = (int)((m_AnalysisFrames.Count / (float)expectedFrames) * 80.0f);
			}

			m_AnalysisProgress = 100;
		}

		public void OnAnalysisThreadComplete()
		{
			this.Text = "Spectral Analysis - " + m_WaveFileName;
			frameIndexSlider.Value = 0;
			frameIndexSlider.Maximum = m_AnalysisFrames.Count - 1;
			frameTextBox.Text = frameIndexSlider.Value.ToString() + "/" + m_AnalysisFrames.Count.ToString();
			sineSelectionScrollBar.Maximum = m_PeakTracker.m_TrackedPeaks.Count - 1;
			showAllSinesCheckbox.Checked = true;
			selectedSineTextbox.Text = "N/A";
			selectedSineHzText.Text = "N/A";

			RefreshLayout();

			refreshDataBtn.Enabled = true;
			playInputButton.Enabled = true;
			playResidualButton.Enabled = true;
			playOutputButton.Enabled = true;
			playSynthesisedData.Enabled = true;
		}

		void RefreshLayout()
		{
			DrawSpectrogram(m_AnalysisFrames, inputSpectrogram);
			DrawSpectrogram(m_AnalysisFramesResidual, residualSpectrogram);
			DrawSpectrogram(m_AnalysisFramesSine, synthesisedSpectrogram);

			trackedPeakGraph.Refresh();
			fftMagnitudeGraph.Refresh();
			frameSampleViewGraph.Refresh();

			inputWaveView.Refresh();
			residualWaveView.Refresh();
			synthesisedWaveView.Refresh();

			inputSpectrogram.Refresh();
			residualSpectrogram.Refresh();
			synthesisedSpectrogram.Refresh();
		}

		public class FloatWaveProvider32 : WaveProvider32
		{
			float[] m_Samples;
			int readIndex = 0;

			public FloatWaveProvider32(float[] samples)
			{
				m_Samples = samples;
			}

			public override int Read(float[] buffer, int offset, int sampleCount)
			{
				int maxSamples = Math.Min(sampleCount, m_Samples.Length - readIndex);

				for (int loop = 0; loop < maxSamples; loop++)
				{
					buffer[loop] = m_Samples[loop + readIndex];
				}

				readIndex += maxSamples;
				return maxSamples;
			}
		}

		WaveOut waveOut;
		FloatWaveProvider32 floatWaveProvider;

		void GeneratePhaseMatchedSineOutput(float[] sampleDataSine, bool randomise)
		{
			double clipFix = (double)clippingVolumeFix.Value;

			float[] randomVolumes = new float[m_PeakTracker.m_TrackedPeaks.Count];
			Random random = new Random(System.DateTime.Now.Millisecond);

			for (int loop = 0; loop < m_PeakTracker.m_TrackedPeaks.Count; loop++)
			{
				randomVolumes[loop] = randomise ? -1.0f + ((float)random.NextDouble() * 2.0f * (float)randomVolumeUpDown.Value) : 1.0f;
			}

			int peakIndex = 0;

			foreach(PeakTracker.TrackedPeak trackedPeak in m_PeakTracker.m_TrackedPeaks)
			{
				for (int frame = 0; frame < trackedPeak.m_FrameData.Count - 1; frame++)
				{
					if(trackedPeak.m_FrameData[frame].m_ActiveThisFrame && trackedPeak.m_FrameData[frame + 1].m_ActiveThisFrame)
					{
						AnalysisFrame.PeakData peakDataStart = trackedPeak.m_FrameData[frame].m_PeakData;
						AnalysisFrame.PeakData peakDataEnd = trackedPeak.m_FrameData[frame + 1].m_PeakData;

						double linearVolumeStart = Math.Pow(10.0, peakDataStart.m_InterpolatedAmplitude / 20.0);
						double linearVolumeEnd = Math.Pow(10.0, peakDataEnd.m_InterpolatedAmplitude / 20.0);

						linearVolumeStart *= randomVolumes[peakIndex];
						linearVolumeEnd *= randomVolumes[peakIndex];

						int startSample = m_AnalysisFrames[frame].CentralSample;
						int endSample = m_AnalysisFrames[frame + 1].CentralSample;
						int samplesPerFrame = endSample - startSample;

						double startPhase = trackedPeak.m_FrameData[frame].m_PeakData.m_InterpolatedPhase;
						double endPhase = trackedPeak.m_FrameData[frame + 1].m_PeakData.m_InterpolatedPhase;

						// Cycles per second
						double startFrequency = Math.Max(0.0, (double)trackedPeak.m_FrameData[frame].m_PeakData.m_InterpolatedLocation / m_AnalysisFrames[0].ComplexFrameData.Length * m_SampleRate);
						double endFrequency = Math.Max(0.0, (double)trackedPeak.m_FrameData[frame + 1].m_PeakData.m_InterpolatedLocation / m_AnalysisFrames[0].ComplexFrameData.Length * m_SampleRate);

						// Cycles per sample
						startFrequency /= m_SampleRate;
						endFrequency /= m_SampleRate;

						// Radians per sample
						startFrequency *= (Math.PI * 2.0f);
						endFrequency *= (Math.PI * 2.0f);

						double mValue = (1.0/(2 * Math.PI) * ((startPhase + (startFrequency * samplesPerFrame) - endPhase) + ((endFrequency - startFrequency) * (samplesPerFrame/2.0))));
						mValue = Math.Round(mValue);

						double alphaValue = (3.0 / Math.Pow(samplesPerFrame, 2)) * (endPhase - startPhase - (startFrequency * samplesPerFrame) + (2.0 * Math.PI * mValue)) - ((1.0 / samplesPerFrame) * (endFrequency - startFrequency));
						double betaValue = (-2.0 / Math.Pow(samplesPerFrame, 2)) * (endPhase - startPhase - (startFrequency * samplesPerFrame) + (2.0 * Math.PI * mValue)) - ((1.0 / Math.Pow(samplesPerFrame, 2)) * (endFrequency - startFrequency));
						int sampleIndex = 0;
						double phase = 0.0;
						double phaseError = 0.0;

						/*
						double expectedFinalPhase = AnalysisFrame.UnwrapPhase(startPhase + (startFrequency * samplesPerFrame));
						phaseError = AnalysisFrame.UnwrapPhase(endPhase - expectedFinalPhase);
						phaseError /= samplesPerFrame;
						*/

						for (int outputSample = startSample; outputSample < endSample; outputSample++)
						{
							phase = startPhase + (startFrequency * sampleIndex) + (phaseError * sampleIndex) +(Math.Pow(alphaValue * sampleIndex, 2)) + (Math.Pow(sampleIndex * betaValue, 3));
							double linearVolume = linearVolumeStart + ((linearVolumeEnd - linearVolumeStart) * (sampleIndex / (double)samplesPerFrame));
							sampleDataSine[outputSample] += (float)(Math.Cos(phase) * linearVolume);
							sampleIndex++;
						}

						phase = AnalysisFrame.UnwrapPhase(phase);
					}
				}

				peakIndex++;
			}
		}

		void GenerateSineOutput()
		{
			m_SampleDataSine = new float[m_SampleDataOriginal.Length];
			double[] sineWavePhases = new double[m_PeakTracker.m_TrackedPeaks.Count];
			double clipFix = (double)clippingVolumeFix.Value;
			int prevFrameBefore = -1;

			for (int inputSampleIndex = m_Padding; inputSampleIndex < m_SampleDataOriginal.Length; inputSampleIndex++)
			{
				float sample = 0.0f;
				double sampleFraction = (inputSampleIndex - m_Padding) / (float)(m_SampleDataOriginal.Length - m_Padding);
				double analysisFrame = (m_PeakTracker.m_TrackedPeaks[0].m_FrameData.Count - 2) * sampleFraction;

				int frameBefore = (int)Math.Floor(analysisFrame);
				double frameFraction = analysisFrame - frameBefore;
				double lastGoodLocation = 0.0;

				for (int peakIndex = 0; peakIndex < m_PeakTracker.m_TrackedPeaks.Count; peakIndex++)
				{
					bool frameBeforeActive = m_PeakTracker.m_TrackedPeaks[peakIndex].m_FrameData[frameBefore].m_ActiveThisFrame;
					bool frameAfterActive = m_PeakTracker.m_TrackedPeaks[peakIndex].m_FrameData[frameBefore + 1].m_ActiveThisFrame;

					AnalysisFrame.PeakData peakDataBelow = m_PeakTracker.m_TrackedPeaks[peakIndex].m_FrameData[frameBefore].m_PeakData;
					AnalysisFrame.PeakData peakDataAbove = m_PeakTracker.m_TrackedPeaks[peakIndex].m_FrameData[frameBefore + 1].m_PeakData;

					double locationBelow = peakDataBelow.m_InterpolatedLocation;
					double locationAbove = peakDataAbove.m_InterpolatedLocation;

					double linearVolumeBelow = Math.Pow(10.0, peakDataBelow.m_InterpolatedAmplitude / 20.0);
					double linearVolumeAbove = Math.Pow(10.0, peakDataAbove.m_InterpolatedAmplitude / 20.0);

					if (!frameBeforeActive && !frameAfterActive)
					{
						linearVolumeAbove = 0.0;
						linearVolumeBelow = 0.0;

						// TODO - When we switch from an inactive to an active frame, immediately jump to the
						// the correct phase based on the calculated value
						//locationBelow = lastGoodLocation;
						//locationAbove = lastGoodLocation;
					}
					else if (!frameBeforeActive)
					{
						locationBelow = locationAbove;
						linearVolumeBelow = 0.0;

						if (prevFrameBefore != frameBefore)
						{
							// Todo - Not sure this is going to work nicely with fake values added by inactive frame controls
							double phaseBelow = peakDataBelow.m_InterpolatedPhase;
							double phaseAbove = peakDataAbove.m_InterpolatedPhase;
							double finalPhase = phaseBelow + ((phaseAbove - phaseBelow) * frameFraction);
							sineWavePhases[peakIndex] = finalPhase;
						}
					}
					else if (!frameAfterActive)
					{
						locationAbove = locationBelow;
						linearVolumeAbove = 0.0;
					}

					double finalLocation = locationBelow + ((locationAbove - locationBelow) * frameFraction);
					double frequency = Math.Max(0.0, finalLocation / m_AnalysisFrames[0].ComplexFrameData.Length * m_SampleRate);
					double linearVolume = linearVolumeBelow + ((linearVolumeAbove - linearVolumeBelow) * frameFraction);
					lastGoodLocation = finalLocation;

					sample += (float)(Math.Cos(sineWavePhases[peakIndex]) * linearVolume);
					sineWavePhases[peakIndex] += ((Math.PI * 2.0f) / (double)m_SampleRate) * frequency;

					m_AnalysisProgress = (int)(100.0f * (inputSampleIndex / (float)m_SampleDataOriginal.Length));
				}

				double finalSample = AnalysisFrame.Clamp(sample * clipFix, -1.0, 1.0);
				m_SampleDataSine[inputSampleIndex] = (float)finalSample;
				prevFrameBefore = frameBefore;
			}

			m_SampleDataResidual = new float[m_SampleDataOriginal.Length];

			for (int loop = m_Padding; loop < m_SampleDataOriginal.Length; loop++)
			{
				m_SampleDataResidual[loop] = m_SampleDataOriginal[loop] - m_SampleDataSine[loop];
			}
		}

		private static float[] GetWaveSampleData(bwWaveFile waveFile, int frameSize, int hopSize, bool pad, out int padding)
		{
			uint numSamples = waveFile.Data.NumSamples;

			if (pad)
			{
				padding = frameSize;
			}
			else
			{
				padding = 0;
			}

			float[] sampleData = new float[numSamples + frameSize + frameSize];
				
			// Pad the start of the source data with silence
			for (int sample = 0; sample < numSamples; sample++)
			{
				sampleData[sample + padding] = waveFile.Data.ChannelData[0, sample];
			}
			
			return sampleData;
		}

		private double[] GenerateAnalysisWindow(int frameSize)
		{
			double[] analysisWindow = new double[frameSize];
			double fConst = 2.0 * Math.PI / (frameSize);
			double sumValue = 0.0;

			switch (m_WindowingFunction.m_WindowType)
			{
				case WindowingFunction.WindowType.Rectangle:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = 1.0f;
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowingFunction.WindowType.BlackmanHarris:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = 0.35875 - 0.48829 * Math.Cos(fConst * (sample + 1)) + 0.14128 * Math.Cos(fConst * 2.0 * (sample + 1)) - 0.01168 * Math.Cos(fConst * 3.0 * (sample + 1));
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowingFunction.WindowType.Hamming:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = 0.54 - 0.46 * (Math.Cos((2.0f * Math.PI * sample) / (frameSize)));
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowingFunction.WindowType.Hann:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = 0.5 * (1 - Math.Cos((2.0f * Math.PI * sample) / (frameSize)));
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowingFunction.WindowType.Sine:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = Math.Sin((Math.PI * (sample / (double)frameSize)));
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowingFunction.WindowType.SineCubed:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = Math.Pow(Math.Sin((Math.PI * (sample / (double)frameSize))), 3.0);
						sumValue += analysisWindow[sample];
					}
					break;
				case WindowingFunction.WindowType.Triangle:
					for (int sample = 0; sample < frameSize; sample++)
					{
						analysisWindow[sample] = 1.0 - (Math.Abs((sample - frameSize / 2.0) / (double)frameSize) / 0.5);
						sumValue += analysisWindow[sample];
					}
					break;
			}

			for (int sample = 0; sample < frameSize; sample++)
			{
				analysisWindow[sample] /= sumValue;
				analysisWindow[sample] *= 2.0f;
			}

			return analysisWindow;
		}

		private void StartFileAnalysisThread(string fileName)
		{
			m_AnalysisProgress = 0;
			m_FrameSize = int.Parse(frameSizeCombo.Text);
			m_HopSize = int.Parse(hopSizeCombo.Text);
			m_NumPeaksToTrack = (int)trackedPeaksUpDown.Value;
			m_NumSinesToTrack = Math.Min((int)trackedSinesUpDown.Value, m_NumPeaksToTrack);
			m_MinPeakSpace = (int)minPeakSpaceUpDown.Value;
			m_MaxTrackedFrequency = (int)maxTrackedFrequencyUpDown.Value;
			m_ZeroPaddingCoefficient = (int)zeroPaddingCoefficient.Value;

			m_WaveFileName = fileName;
			m_AnalysisThread = new Thread(new ThreadStart(AnalyseFile));
			m_AnalysisThread.Priority = ThreadPriority.AboveNormal;
			m_AnalysisThread.Start();

			m_ProgressBarForm = new ProgressBar();
			m_ProgressBarForm.Owner = this;
			m_ProgressBarForm.ShowDialog();
			m_ProgressBarForm.Text = "Processing " + m_WaveFileName;
		}

		#region GRAPH_DRAWING
		private void windowingFnGraph_Paint(object sender, PaintEventArgs pea)
		{
			var p = Pens.DarkBlue;
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			var prevX = 0.0f;
			var prevY = 0.0f;

			grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
							0,
							0,
							visBounds.Width,
							visBounds.Height);

			var gp = new GraphicsPath();

			for (int sample = 0; sample < m_WindowingFunction.FrameSizeSamples; sample++)
			{
				float xFraction = sample / (float)m_WindowingFunction.FrameSizeSamples;
				float xCoord = xFraction * visBounds.Width;

				var yFraction = 1.0f - (float) m_WindowingFunction.m_WindowValues[sample];
				float yCoord = yFraction * visBounds.Height;

				if (sample == 0)
				{
					prevX = xCoord;
					prevY = yCoord;
				}

				gp.AddLine(prevX, prevY, xCoord, yCoord);
				prevX = xCoord;
				prevY = yCoord;
			}

			grfx.DrawPath(p, gp);
		}

		private void frameSampleViewGraph_Paint(object sender, PaintEventArgs pea)
		{
			if (m_AnalysisFrames.Count > 0)
			{
				int frameIndex = frameIndexSlider.Value;
				AnalysisFrame analysisFrame = m_AnalysisFrames[frameIndex];

				Graphics g = pea.Graphics;
				Int32 lastx = 0, lasty = 0;
				var gp = new GraphicsPath();
				var p = Pens.DarkBlue;
				var visBounds = g.VisibleClipBounds;

				float visibleSamplesFloat = m_FrameSize;
				visibleSamplesFloat *= 1.0f - (waveViewZoom.Value / 100.0f);
				int visibleSamples = (int)visibleSamplesFloat;

				float xScale = ((visBounds.Width) / (float)visibleSamples);

				float yScale = 1.0f;
				int index = 0;

				for (int sampleIndex = analysisFrame.CentralSample - (visibleSamples / 2); sampleIndex < analysisFrame.CentralSample + (visibleSamples / 2); sampleIndex++)
				{
					float sample = m_SampleDataOriginal[sampleIndex];

					// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
					sample *= -1.0f;
					sample += 1.0f;
					sample /= 2.0f;

					// now 0 -> 1.0f
					var y = (Int32)(sample * (visBounds.Height));
					var x = index;

					x = (Int32)(x * xScale);

					y = (Int32)(y * yScale);

					if (x > visBounds.Width &&
					   lastx > visBounds.Width)
					{
						break;
					}

					if (index >= 1)
					{
						gp.AddLine(lastx, lasty, x, y);
					}

					lastx = x;
					lasty = y;

					index++;
				}

				// draw path
				g.DrawPath(p, gp);

				double totalAmplitude = 0.0f;

				for (int loop = 0; loop < analysisFrame.FramePeakData.Count; loop++)
				{
					double linearVolume = Math.Pow(10.0, analysisFrame.FramePeakData[loop].m_InterpolatedAmplitude / 20.0);
					totalAmplitude += Math.Cos(analysisFrame.FramePeakData[loop].m_InterpolatedPhase) * linearVolume;
				}

				if (analysisFrame.FramePeakData.Count == 1)
				{
					double linearVolume = Math.Pow(10.0, analysisFrame.FramePeakData[0].m_InterpolatedAmplitude / 20.0);
					linearVolume *= -1.0f;
					linearVolume += 1.0f;
					linearVolume /= 2.0f;

					g.DrawLine(Pens.Red, 0, (Int32)(linearVolume * (visBounds.Height)), visBounds.Width, (Int32)(linearVolume * (visBounds.Height)));
				}

				var xCoord = visBounds.Width / 2.0f;

				// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
				totalAmplitude *= -1.0f;
				totalAmplitude += 1.0f;
				totalAmplitude /= 2.0f;

				// now 0 -> 1.0f
				var yCoord = (Int32)(totalAmplitude * (visBounds.Height));

				p = Pens.Black;
				g.DrawLine(p, 0, yCoord, visBounds.Width, yCoord);

				// draw x axis
				g.DrawLine(p, ((visBounds.Width) / 2.0f), (visBounds.Height), ((visBounds.Width) / 2.0f), 0.0f);
			}
		}

		private void fftMagnitudeGraph_Paint(object sender, PaintEventArgs pea)
		{
			var p = Pens.DarkBlue;
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			var prevX = 0.0f;
			var prevY = 0.0f;

			grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
							0,
							0,
							visBounds.Width,
							visBounds.Height);

			List<AnalysisFrame> analysisFrames = m_AnalysisFrames;

			if (soundTypeTab.SelectedIndex == 1)
			{
				analysisFrames = m_AnalysisFramesResidual;
			}
			else if (soundTypeTab.SelectedIndex == 2)
			{
				analysisFrames = m_AnalysisFramesSine;
			}

			if (analysisFrames.Count > 0)
			{
				var gp = new GraphicsPath();
				int frameIndex = frameIndexSlider.Value;
				AnalysisFrame analysisFrame = analysisFrames[frameIndex];
				int samplesToDraw = analysisFrame.ComplexFrameData.Length / 2;
				int visibleFrequency = m_SampleRate/2;
				int visibleSamples = samplesToDraw;

				double maxValue = 0.0f;

				for (int sample = 0; sample < samplesToDraw; sample++)
				{
					maxValue = Math.Max(maxValue, analysisFrame.ComplexFrameData[sample].Modulus);
				}

				for (int sample = 0; sample < samplesToDraw; sample++)
				{
					float thisFrequency = (sample / (float)m_FrameSize) * (m_SampleRate / 2);
					float xFraction = thisFrequency / visibleFrequency;
					float xCoord = xFraction * visBounds.Width;

					double dbVolume = Math.Max(-100, 20.0 * Math.Log10(analysisFrame.ComplexFrameData[sample].Modulus));
					float yFraction = (float)(dbVolume / -100.0);

					float yCoord = yFraction * visBounds.Height;

					if (sample == 0)
					{
						prevX = xCoord;
						prevY = yCoord;
					}

					gp.AddLine(prevX, prevY, xCoord, yCoord);
					prevX = xCoord;
					prevY = yCoord;
				}

				grfx.DrawPath(p, gp);
				p = Pens.Red;
				var pHighLight = Pens.LightGreen;

				for (int peak = 0; peak < analysisFrame.FramePeakData.Count; peak++)
				{
					float thisFrequency = (analysisFrame.FramePeakData[peak].m_Location / (float)m_FrameSize) * (m_SampleRate / 2);
					float xFraction = thisFrequency / visibleFrequency;
					float xCoord = xFraction * visBounds.Width;

					double dbVolume = Math.Max(-100.0, 20.0 * Math.Log10(analysisFrame.FramePeakData[peak].m_PeakValue.Modulus));
					float yFraction = (float)(dbVolume / -100.0);

					float yCoord = yFraction * visBounds.Height;
					bool highlight = false;
					int rectSize = 2;

					if (!showAllSinesCheckbox.Checked && frameIndex > 0)
					{
						if (analysisFrame.FramePeakData[peak].m_InterpolatedLocation == m_PeakTracker.m_TrackedPeaks[sineSelectionScrollBar.Value].m_FrameData[frameIndex - 1].m_PeakData.m_InterpolatedLocation)
						{
							highlight = true;
							rectSize = 3;
						}
					}

					grfx.DrawRectangle(highlight ? pHighLight : p, new Rectangle((int)xCoord, (int)yCoord, rectSize, rectSize));
				}
			}
		}

		private void trackedPeakGraph_Paint(object sender, PaintEventArgs pea)
		{
			var gp = new GraphicsPath();
			var p = Pens.DarkBlue;
			var grfx = pea.Graphics;
			var visBounds = grfx.VisibleClipBounds;
			var prevX = 0.0f;
			var prevY = 0.0f;
			bool spectrogramStyle = drawVolumeCheckbox.Checked;

			if (!spectrogramStyle)
			{
				grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(200, 200, 200)),
							0,
							0,
							visBounds.Width,
							visBounds.Height);
			}
			else
			{
				int r, g, b;
				HSVColor.HsvToRgb(240, 1.0f, 1.0f, out r, out g, out b);

				grfx.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(r, g, b)),
							0,
							0,
							visBounds.Width,
							visBounds.Height);
			}

			if (m_PeakTracker.m_TrackedPeaks != null && m_PeakTracker.m_TrackedPeaks.Count > 0)
			{
				double maxAmplitude = float.MinValue;

				for (int peakIndex = 0; peakIndex < m_PeakTracker.m_TrackedPeaks.Count; peakIndex++)
				{
					for (int frameIndex = 0; frameIndex < m_PeakTracker.m_TrackedPeaks[peakIndex].m_FrameData.Count; frameIndex++)
					{
						float amplitude = AnalysisFrame.Clamp(1.0f - (float)(Math.Abs(m_PeakTracker.m_TrackedPeaks[peakIndex].m_FrameData[frameIndex].m_PeakData.m_InterpolatedAmplitude) * 0.01f), 0.0f, 1.0f);
						maxAmplitude = Math.Max(maxAmplitude, amplitude);
					}
				}

				for (int peakIndex = 0; peakIndex < m_PeakTracker.m_TrackedPeaks.Count; peakIndex++)
				{
					if (showAllSinesCheckbox.Checked || peakIndex == sineSelectionScrollBar.Value)
					{
						PeakTracker.TrackedPeak trackedPeak = m_PeakTracker.m_TrackedPeaks[peakIndex];

						if (drawFrequencyCheckbox.Checked)
						{
							gp = new GraphicsPath();
							p = new Pen(m_RandomColors[peakIndex]);

							for (int frameIndex = 0; frameIndex < trackedPeak.m_FrameData.Count; frameIndex++)
							{
								float xFraction = frameIndex * (1.0f / (trackedPeak.m_FrameData.Count - 1));
								float xCoord = xFraction * visBounds.Width;

								double frequency = Math.Max(0.0, trackedPeak.m_FrameData[frameIndex].m_PeakData.m_InterpolatedLocation / m_AnalysisFrames[0].ComplexFrameData.Length * m_SampleRate);
								float yFraction = 1.0f - ((float)frequency / (m_SampleRate/2));
								float yCoord = yFraction * visBounds.Height;

								if (frameIndex == 0)
								{
									prevX = xCoord;
									prevY = yCoord;
								}

								if (spectrogramStyle)
								{
									if (trackedPeak.m_FrameData[frameIndex].m_ActiveThisFrame)
									{
										double finalVolume = trackedPeak.m_FrameData[frameIndex].m_PeakData.m_InterpolatedAmplitude;
										finalVolume = AnalysisFrame.Clamp(1.0f - (Math.Abs(finalVolume) * 0.01f), 0.0f, 1.0f);

										int r, g, b;
										HSVColor.HsvToRgb(240 * (1.0f - (finalVolume / maxAmplitude)), 1.0f, 1.0f, out r, out g, out b);
										p.Color = Color.FromArgb(r, g, b);

										gp.AddLine(prevX, prevY, xCoord, yCoord);
										grfx.DrawPath(p, gp);
										gp = new GraphicsPath();
									}
								}
								else
								{
									if (trackedPeak.m_FrameData[frameIndex].m_ActiveThisFrame)
									{
										gp.AddLine(prevX, prevY, xCoord, yCoord);
									}
									else
									{
										grfx.DrawPath(p, gp);
										gp = new GraphicsPath();
									}
								}

								prevX = xCoord;
								prevY = yCoord;
							}

							grfx.DrawPath(p, gp);
						}
					}
				}

				gp = new GraphicsPath();
				p = new Pen(Color.Black);
				float frameXFraction = frameIndexSlider.Value / (float)m_AnalysisFrames.Count;
				float frameXCoord = frameXFraction * visBounds.Width;
				gp.AddLine(frameXCoord, 0.0f, frameXCoord, visBounds.Height);
				grfx.DrawPath(p, gp);
			}
		}

		private void inputWaveView_Paint(object sender, PaintEventArgs pea)
		{
			if (m_SampleDataOriginal != null)
			{
				DrawWaveView(m_SampleDataOriginal, sender as System.Windows.Forms.PictureBox, pea, 0);
			}
		}

		private void residualWaveView_Paint(object sender, PaintEventArgs pea)
		{
			if (m_SampleDataResidual != null)
			{
				DrawWaveView(m_SampleDataResidual, sender as System.Windows.Forms.PictureBox, pea, 0);
			}
		}

		private void synthesisedWaveView_Paint(object sender, PaintEventArgs pea)
		{
			if (m_SampleDataSine != null)
			{
				DrawWaveView(m_SampleDataSine, sender as System.Windows.Forms.PictureBox, pea, 0);
			}
		}

		private void inputSpectrogram_Paint(object sender, PaintEventArgs pea)
		{
			if (m_SampleDataOriginal != null)
			{
				Graphics g = pea.Graphics;
				DrawMillisecondsAxis(g, sender as System.Windows.Forms.PictureBox, m_SampleDataOriginal, false);
				DrawFrequencyAxis(g, sender as System.Windows.Forms.PictureBox);
			}
		}

		private void residualSpectrogram_Paint(object sender, PaintEventArgs pea)
		{
			if (m_SampleDataResidual != null)
			{
				Graphics g = pea.Graphics;
				DrawMillisecondsAxis(g, sender as System.Windows.Forms.PictureBox, m_SampleDataResidual, false);
				DrawFrequencyAxis(g, sender as System.Windows.Forms.PictureBox);
			}
		}

		private void synthesisedSpectrogram_Paint(object sender, PaintEventArgs pea)
		{
			if (m_SampleDataSine != null)
			{
				Graphics g = pea.Graphics;
				DrawMillisecondsAxis(g, sender as System.Windows.Forms.PictureBox, m_SampleDataSine, false);
				DrawFrequencyAxis(g, sender as System.Windows.Forms.PictureBox);
			}
		}

		void DrawSpectrogram(List<AnalysisFrame> analysisFrames, System.Windows.Forms.PictureBox parent)
		{
			if (parent.Width == 0 || parent.Height == 0)
			{
				return;
			}

			Bitmap waveImage = new Bitmap(parent.Width, (int)(parent.Height - 40));
			parent.Image = waveImage;

			double maxAmplitude = float.MinValue;

			for (int frame = 0; frame < analysisFrames.Count; frame++)
			{
				for (int complexDataIndex = 0; complexDataIndex < analysisFrames[0].ComplexFrameData.Length; complexDataIndex++)
				{
					maxAmplitude = Math.Max(maxAmplitude, analysisFrames[frame].ComplexFrameData[complexDataIndex].Modulus);
				}
			}

			double maxVolumeDB = 20.0 * Math.Log10(maxAmplitude);
			maxAmplitude = AnalysisFrame.Clamp(1.0f - (Math.Abs(maxVolumeDB) * 0.01f), 0.0f, 1.0f);

			for (int xPixel = 0; xPixel < (waveImage.Width - 40); xPixel++)
			{
				double xFraction = xPixel / (double)(waveImage.Width - 40);
				double frameIndex = xFraction * (analysisFrames.Count - 1);

				double frameFraction = frameIndex - (int)Math.Floor(frameIndex);
				int frameBeforeIndex = (int)Math.Floor(frameIndex);
				int frameAfterIndex = (int)Math.Ceiling(frameIndex);

				AnalysisFrame frameBefore = analysisFrames[frameBeforeIndex];
				AnalysisFrame frameAfter = analysisFrames[frameAfterIndex];

				for (int yPixel = 0; yPixel < waveImage.Height; yPixel++)
				{
					double yFraction = yPixel / (double)waveImage.Height;
					yFraction *= 0.5f;

					double complexDataIndex = yFraction * (analysisFrames[0].ComplexFrameData.Length - 1);
					double complexDataFraction = complexDataIndex - (int)Math.Floor(complexDataIndex);

					double frameBeforeVolumeBelow = frameBefore.ComplexFrameData[(int)Math.Floor(complexDataIndex)].Modulus;
					double frameBeforeVolumeAbove = frameBefore.ComplexFrameData[(int)Math.Ceiling(complexDataIndex)].Modulus;
					double frameBeforeVolume = frameBeforeVolumeBelow + ((frameBeforeVolumeAbove - frameBeforeVolumeBelow) * complexDataFraction);

					double frameAfterVolumeBelow = frameAfter.ComplexFrameData[(int)Math.Floor(complexDataIndex)].Modulus;
					double frameAfterVolumeAbove = frameAfter.ComplexFrameData[(int)Math.Ceiling(complexDataIndex)].Modulus;
					double frameAfterVolume = frameAfterVolumeBelow + ((frameAfterVolumeAbove - frameAfterVolumeBelow) * complexDataFraction);

					double finalVolume = frameBeforeVolume + ((frameAfterVolume - frameBeforeVolume) * frameFraction);
					double dbVolume = 20.0 * Math.Log10(finalVolume);
					finalVolume = AnalysisFrame.Clamp(1.0f - (Math.Abs(dbVolume) * 0.01f), 0.0f, 1.0f);

					int r, g, b;
					HSVColor.HsvToRgb(240 * (1.0f - (finalVolume / maxAmplitude)), 1.0f, 1.0f, out r, out g, out b);
					waveImage.SetPixel(xPixel + 40, waveImage.Height - 1 - yPixel, Color.FromArgb(r, g, b));
				}
			}
		}

		void DrawWaveView(float[] sampleData, System.Windows.Forms.PictureBox parent, PaintEventArgs pea, int padding)
		{
			Graphics g = pea.Graphics;
			Int32 lastx = 40, lasty = 0;
			var gp = new GraphicsPath();
			var p = Pens.DarkBlue;

			int offset = 0 + padding;
			float zoom = 1.0f;
			float yScale = 1.0f;
			float xScale = ((parent.Width - 40) / (float)sampleData.Length) * zoom;

			DrawMillisecondsAxis(g, parent, sampleData, true);
			DrawAmplitudeAxis(g, parent);

			// Default step rate
			int stepRate = 10;

			float zoomPercentage = (zoom - 100) / 100.0f;

			if (zoomPercentage > 1.0f)
				zoomPercentage = 1.0f;

			if (zoomPercentage < 0.0f)
				zoomPercentage = 0.0f;

			// As we zoom in more, increase the accuracy
			stepRate -= (int)((stepRate - 1) * zoomPercentage);

			for (var i = offset; i < sampleData.Length; i += stepRate)
			{
				float sample = sampleData[i];

				// scale from -1.0f -> 1.0f to 0 -> visBounds.Height
				sample *= -1.0f;
				sample += 1.0f;
				sample /= 2.0f;

				// now 0 -> 1.0f
				var y = (Int32)(sample * (parent.Height - 40));
				var x = i;

				x = (Int32)((x - offset) * xScale);
				x += 40;

				y = (Int32)(y * yScale);

				if (x > parent.Width &&
				   lastx > parent.Width)
				{
					break;
				}

				if (i >= 1)
				{
					gp.AddLine(lastx, lasty, x, y);
				}

				lastx = x;
				lasty = y;
			}

			// draw path
			g.DrawPath(p, gp);

			// draw x axis
			g.DrawLine(p, 40, (parent.Height - 40) / 2.0f, parent.Width - 40, (parent.Height - 40) / 2.0f );

			gp = new GraphicsPath();
			p = new Pen(Color.Black);
			float frameXFraction = frameIndexSlider.Value / (float)m_AnalysisFrames.Count;
			float frameXCoord = 40 + (frameXFraction * (parent.Width - 40));
			gp.AddLine(frameXCoord, 0.0f, frameXCoord, parent.Height - 40);
			g.DrawPath(p, gp);
		}

		void DrawMillisecondsAxis(Graphics g, System.Windows.Forms.PictureBox parent, float[] sampleData, bool fillRectangle)
		{
			string drawString = "Time (ms)";
			System.Drawing.Font drawFont = new System.Drawing.Font("Microsoft Sans Serif", 8);
			System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
			System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();
			drawFormat.Alignment = StringAlignment.Center;
			drawFormat.LineAlignment = StringAlignment.Center;

			g.DrawString(drawString, drawFont, drawBrush, ((parent.Width - 40) * 0.5f) + 40, parent.Height - 10, drawFormat);
			drawFont.Dispose();
			drawBrush.Dispose();

			if (fillRectangle)
			{
				g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(255, 255, 255)),
							40,
							0,
							parent.Width - 40,
							(int)(parent.Height - 40));
			}

			var p = Pens.DarkBlue;
			g.DrawRectangle(p, new Rectangle(40, 0, parent.Width - 1 - 40, (int)(parent.Height - 40)));

			float assetDuration = sampleData.Length / (float)m_SampleRate;
			float timeStepRate = assetDuration * 0.1f;
			timeStepRate = (float)Math.Round(timeStepRate / 0.1f) * 0.1f;

			if (timeStepRate == 0.0f)
			{
				timeStepRate = assetDuration * 0.1f;
				timeStepRate = (float)Math.Round(timeStepRate / 0.01f) * 0.01f;
			}

			System.Drawing.Font timeFont = new System.Drawing.Font("Microsoft Sans Serif", 7);
			System.Drawing.SolidBrush timeBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
			System.Drawing.StringFormat timeFormat = new System.Drawing.StringFormat();
			timeFormat.Alignment = StringAlignment.Center;
			timeFormat.LineAlignment = StringAlignment.Center;

			for (float time = timeStepRate; time < assetDuration; time += timeStepRate)
			{
				float timeFraction = time / assetDuration;

				if (timeFraction < 0.95f)
				{
					string timeString = ((int)(time * 1000)).ToString();
					g.DrawString(timeString, timeFont, timeBrush, ((parent.Width - 40) * timeFraction) + 40, parent.Height - 30, timeFormat);
					g.DrawLine(p, ((parent.Width - 40) * timeFraction) + 40, parent.Height - 40, ((parent.Width - 40) * timeFraction) + 40, parent.Height - 45);
				}
			}

			timeFont.Dispose();
			timeBrush.Dispose();
		}

		void DrawFrequencyAxis(Graphics g, System.Windows.Forms.PictureBox parent)
		{
			var p = Pens.DarkBlue;
			float maxFrequency = m_SampleRate / 2;
			float freqStepRate = maxFrequency * 0.1f;
			freqStepRate = (float)Math.Round(freqStepRate / 1000.0f) * 1000.0f;

			if (freqStepRate == 0.0f)
			{
				freqStepRate = maxFrequency * 0.1f;
				freqStepRate = (float)Math.Round(freqStepRate / 100.0f) * 100.0f;

				if (freqStepRate == 0.0f)
				{
					freqStepRate = maxFrequency * 0.1f;
					freqStepRate = (float)Math.Round(freqStepRate / 10.0f) * 10.0f;
				}
			}

			System.Drawing.Font freqFont = new System.Drawing.Font("Microsoft Sans Serif", 7);
			System.Drawing.SolidBrush freqBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
			System.Drawing.StringFormat freqFormat = new System.Drawing.StringFormat();
			freqFormat.Alignment = StringAlignment.Far;
			freqFormat.LineAlignment = StringAlignment.Center;

			for (float freq = freqStepRate; freq < maxFrequency; freq += freqStepRate)
			{
				float freqFraction = freq / maxFrequency;

				if (freqFraction < 0.95f)
				{
					string freqString;

					if (maxFrequency < 10000.0f)
					{
						freqString = (freq / 1000.0f).ToString("0.00");
					}
					else
					{
						freqString = ((int)(freq / 1000.0f)).ToString();
					}
					
					g.DrawString(freqString, freqFont, freqBrush, 35, (parent.Height - 40) * (1.0f - freqFraction), freqFormat);
					g.DrawLine(p, 40, (parent.Height - 40) * (1.0f - freqFraction), 45, (parent.Height - 40) * (1.0f - freqFraction));
				}
			}

			System.Drawing.Font drawFont = new System.Drawing.Font("Microsoft Sans Serif", 8);
			g.TranslateTransform(7, (parent.Height - 40) / 2);
			g.RotateTransform(-90);
			SizeF textSize = g.MeasureString("Frequency (kHz)", freqFont);
			g.DrawString("Frequency (kHz)", drawFont, freqBrush, -(textSize.Width / 2), -(textSize.Height / 2));
			g.ResetTransform();

			freqFont.Dispose();
			freqBrush.Dispose();

			GraphicsPath gp = new GraphicsPath();
			p = new Pen(Color.Black);
			float frameXFraction = frameIndexSlider.Value / (float)m_AnalysisFrames.Count;
			float frameXCoord = 40 + (frameXFraction * (parent.Width - 40));
			gp.AddLine(frameXCoord, 0.0f, frameXCoord, parent.Height - 40);
			g.DrawPath(p, gp);
		}

		void DrawAmplitudeAxis(Graphics g, System.Windows.Forms.PictureBox parent)
		{
			var p = Pens.DarkBlue;
			System.Drawing.Font freqFont = new System.Drawing.Font("Microsoft Sans Serif", 7);
			System.Drawing.SolidBrush freqBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
			System.Drawing.StringFormat freqFormat = new System.Drawing.StringFormat();
			freqFormat.Alignment = StringAlignment.Far;
			freqFormat.LineAlignment = StringAlignment.Center;

			freqFormat.LineAlignment = StringAlignment.Near;
			g.DrawString("1", freqFont, freqBrush, 35, (parent.Height - 40) * 0.0f, freqFormat);

			freqFormat.LineAlignment = StringAlignment.Center;
			g.DrawString("0.5", freqFont, freqBrush, 35, (parent.Height - 40) * 0.25f, freqFormat);
			g.DrawLine(p, 40, (parent.Height - 40) * 0.25f, 45, (parent.Height - 40) * 0.25f);

			g.DrawString("0", freqFont, freqBrush, 35, (parent.Height - 40) * 0.5f, freqFormat);

			g.DrawString("-0.5", freqFont, freqBrush, 35, (parent.Height - 40) * 0.75f, freqFormat);
			g.DrawLine(p, 40, (parent.Height - 40) * 0.75f, 45, (parent.Height - 40) * 0.75f);

			freqFormat.LineAlignment = StringAlignment.Far;
			g.DrawString("-1", freqFont, freqBrush, 35, (parent.Height - 40) * 1.0f, freqFormat);

			System.Drawing.Font drawFont = new System.Drawing.Font("Microsoft Sans Serif", 8);
			g.TranslateTransform(7, (parent.Height - 40) / 2);
			g.RotateTransform(-90);
			SizeF textSize = g.MeasureString("Amplitude", freqFont);
			g.DrawString("Amplitude", drawFont, freqBrush, -(textSize.Width / 2), -(textSize.Height / 2));

			g.ResetTransform();
			freqFont.Dispose();
			freqBrush.Dispose();
		}
		#endregion

		#region CONTROLS
		private void soundTypeTab_SelectedIndexChanged(object sender, EventArgs e)
		{
			fftMagnitudeGraph.Refresh();
		}

		private void windowingFnCombo_SelectedIndexChanged(object sender, EventArgs e)
		{	
			ComboBox comboBox = sender as ComboBox;
			m_WindowingFunction.SetWindowType((WindowingFunction.WindowType)comboBox.SelectedIndex);

			/* if (m_SampleDataOriginalResampled != null)
			{
				int sampleIndex = m_HopSize * frameIndexSlider.Value;
				double[] analysisWindow = GenerateAnalysisWindow(m_FrameSize);
				m_AnalysisFrames[frameIndexSlider.Value].Analyse(m_SampleDataOriginalResampled, analysisWindow, sampleIndex, m_FrameSize, m_ZeroPaddingCoefficient, m_NumPeaksToTrack, m_MinPeakSpace, m_SampleRate, m_MaxTrackedFrequency);
			}
			*/

			windowingFnGraph.Refresh();
			//fftMagnitudeGraph.Refresh();
		}

		private void frameIndexSlider_Scroll(object sender, EventArgs e)
		{
			if (m_AnalysisFrames.Count > 0)
			{
				frameTextBox.Text = frameIndexSlider.Value.ToString() + "/" + m_AnalysisFrames.Count.ToString();
				frameTextBox.BackColor = m_AnalysisFrames[frameIndexSlider.Value].IsHarmonic ? Color.LightGreen : SystemColors.Control;
				fftMagnitudeGraph.Refresh();
				trackedPeakGraph.Refresh();
				inputWaveView.Refresh();
				residualWaveView.Refresh();
				synthesisedWaveView.Refresh();
				inputSpectrogram.Refresh();
				residualSpectrogram.Refresh();
				synthesisedSpectrogram.Refresh();
				frameSampleViewGraph.Refresh();

				if (!showAllSinesCheckbox.Checked)
				{
					int frameIndex = frameIndexSlider.Value;
					double peakLocation = m_AnalysisFrames[frameIndex].FramePeakData[(int)sineSelectionScrollBar.Value].m_InterpolatedLocation;
					double frequency = Math.Max(0.0, (double)peakLocation / m_AnalysisFrames[0].ComplexFrameData.Length * m_SampleRate);
					selectedSineHzText.Text = frequency.ToString("0.00") + " Hz";
				}
			}
		}

		private void fftViewZool_Scroll(object sender, EventArgs e)
		{
			fftMagnitudeGraph.Refresh();
		}

		private void loadToolStripMenuItem_Click(object sender, EventArgs e)
		{
			loadWaveFileDialog.ShowDialog();
		}

		private void loadWaveFileDialog_FileOk(object sender, CancelEventArgs e)
		{
			OpenFileDialog dialog = sender as OpenFileDialog;

			if (dialog != null)
			{
				StartFileAnalysisThread(dialog.FileName);
			}
		}

		private void frameTextBox_TextChanged(object sender, EventArgs e)
		{
			fftMagnitudeGraph.Refresh();
		}

		private void refreshDataBtn_Click(object sender, EventArgs e)
		{
			StartFileAnalysisThread(m_WaveFileName);
		}

		private void sineSelectionScrollBar_Scroll(object sender, EventArgs e)
		{
			if (m_PeakTracker.m_TrackedPeaks != null)
			{
				selectedSineTextbox.Text = (sineSelectionScrollBar.Value + 1).ToString() + "/" + m_PeakTracker.m_TrackedPeaks.Count.ToString();

				int frameIndex = frameIndexSlider.Value;

				if (sineSelectionScrollBar.Value < (int)m_AnalysisFrames[frameIndex].FramePeakData.Count)
				{
					double peakLocation = m_AnalysisFrames[frameIndex].FramePeakData[(int)sineSelectionScrollBar.Value].m_InterpolatedLocation;
					double frequency = Math.Max(0.0, (double)peakLocation / m_AnalysisFrames[0].ComplexFrameData.Length * m_SampleRate);
					selectedSineHzText.Text = frequency.ToString("0.00") + " Hz";

					trackedPeakGraph.Refresh();
					fftMagnitudeGraph.Refresh();
				}
			}
		}

		private void showAllSinesCheckbox_CheckedChanged(object sender, EventArgs e)
		{
			selectedSineTextbox.Text = (sineSelectionScrollBar.Value + 1).ToString() + "/" + m_PeakTracker.m_TrackedPeaks.Count.ToString();
			trackedPeakGraph.Refresh();
			fftMagnitudeGraph.Refresh();
		}

		private void drawFrequencyCheckbox_CheckedChanged(object sender, EventArgs e)
		{
			trackedPeakGraph.Refresh();
		}

		private void drawVolumeCheckbox_CheckedChanged(object sender, EventArgs e)
		{
			trackedPeakGraph.Refresh();
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			fftMagnitudeGraph.Refresh();
		}
		#endregion

		private void frameSizeCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			/*
			m_FrameSize = int.Parse(frameSizeCombo.Text);

			if (m_SampleDataOriginalResampled != null)
			{
				int sampleIndex = m_HopSize * frameIndexSlider.Value;
				double[] analysisWindow = GenerateAnalysisWindow(m_FrameSize);
				m_AnalysisFrames[frameIndexSlider.Value].Analyse(m_SampleDataOriginalResampled, analysisWindow, sampleIndex, m_FrameSize, m_ZeroPaddingCoefficient, m_NumPeaksToTrack, m_MinPeakSpace, m_SampleRate, m_MaxTrackedFrequency);
			}

			windowingFnGraph.Refresh();
			fftMagnitudeGraph.Refresh();
			*/
		}

		void PlaySampleData(float[] sampleData, int sampleRate)
		{
			if (waveOut != null && waveOut.PlaybackState == PlaybackState.Playing)
			{
				waveOut.Stop();
			}

			floatWaveProvider = new FloatWaveProvider32(sampleData);
			floatWaveProvider.SetWaveFormat(sampleRate, 1);
			waveOut = new WaveOut();
			waveOut.Init(floatWaveProvider);
			waveOut.Play();
		}

		private void playInputButton_Click(object sender, EventArgs e)
		{
			PlaySampleData(m_SampleDataOriginal, (int)m_WaveFileOriginal.Format.SampleRate);
		}

		private void playResidualButton_Click(object sender, EventArgs e)
		{
			PlaySampleData(m_SampleDataResidual, (int)m_WaveFileOriginal.Format.SampleRate);
		}

		private void playSynthesisedData_Click(object sender, EventArgs e)
		{
			PlaySampleData(m_SampleDataSine, (int)m_WaveFileOriginal.Format.SampleRate);
		}

		private void playOutputButton_Click(object sender, EventArgs e)
		{
			float[] sampleDataSine = new float[m_SampleDataOriginal.Length];

			GeneratePhaseMatchedSineOutput(sampleDataSine, true);
			m_SampleDataOutput = new float[m_SampleDataOriginal.Length];

			for (int loop = 0; loop < m_SampleDataOriginal.Length; loop++)
			{
				m_SampleDataOutput[loop] = sampleDataSine[loop] + m_SampleDataResidual[loop];
			}

			PlaySampleData(m_SampleDataOutput, (int)m_WaveFileOriginal.Format.SampleRate);
		}

		private void resizeTimer_Tick(object sender, EventArgs e)
		{
			resizeTimer.Enabled = false;
			RefreshLayout();
		}

		private void waveViewZoom_Scroll(object sender, EventArgs e)
		{
			frameSampleViewGraph.Refresh();
		}

		private void selectedSineTextbox_TextChanged(object sender, EventArgs e)
		{

		}

		private void frameSampleViewGraph_Click(object sender, EventArgs e)
		{

		}
	}
}
