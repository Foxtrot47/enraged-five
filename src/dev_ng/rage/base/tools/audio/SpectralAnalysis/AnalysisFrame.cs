﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MathNet.Numerics;
using MathNet.Numerics.Transformations;

namespace SpectralAnalysis
{
	public class AnalysisFrame
	{
		public class PeakData : IComparable<PeakData>
		{
			public double m_InterpolatedLocation; // (Bins)
			public double m_InterpolatedAmplitude;
			public double m_InterpolatedPhase;

			public int m_Location;
			public Complex m_PeakValue;

			public PeakData(int location, Complex peakValue)
			{
				m_Location = location;
				m_PeakValue = peakValue;
			}

			public int CompareTo(PeakData other)
			{
				if (m_Location == other.m_Location)
				{
					return 0;
				}
				else
				{
					if (m_PeakValue.Modulus - other.m_PeakValue.Modulus < 0)
					{
						return 1;
					}
					else if (m_PeakValue.Modulus - other.m_PeakValue.Modulus > 0)
					{
						return -1;
					}
					else
					{
						return m_Location - other.m_Location > 0 ? 1 : -1;
					}
				}
			}
		}

		public double[] FrameData			{ get { return m_FrameData; } }
		public double[] PaddedFrameData		{ get { return m_PaddedFrameData; } }
		public Complex[] ComplexFrameData	{ get { return m_ComplexFrameData; } }
		public List<PeakData> FramePeakData { get { return m_PeakData; } }
		public bool IsHarmonic				{ get { return m_IsHarmonic; } }
		public double Pitch					{ get { return m_Pitch; } }
		public int CentralSample			{ get { return m_CentralSample; } }

		List<PeakData> m_PeakData;
		double[] m_FrameData;
		double[] m_PaddedFrameData;
		Complex[] m_ComplexFrameData;
		double m_Pitch;
		double m_PitchError;
		bool m_IsHarmonic;
		int m_CentralSample;

		public AnalysisFrame()
		{
		}

		public static T Clamp<T>(T value, T min, T max)
		 where T : System.IComparable<T>
		{
			T result = value;
			if (value.CompareTo(max) > 0)
				result = max;
			if (value.CompareTo(min) < 0)
				result = min;
			return result;
		}

		public void Analyse(float[] sampleData, double[] analysisWindow, int sampleStartIndex, int frameSize, int zeroPaddingCoefficient, int numPeaks, int minPeakSpace, int sampleRate, int maxTrackedFrequency)
		{
			m_CentralSample = sampleStartIndex + (frameSize / 2);
			m_FrameData = GrabFrame(sampleData, analysisWindow, sampleStartIndex, frameSize);
			m_PaddedFrameData = PadFrameData(m_FrameData, frameSize, zeroPaddingCoefficient);
			m_ComplexFrameData = ConvertToComplex(m_PaddedFrameData);

			ComplexFourierTransformation complexFourierTransform = new ComplexFourierTransformation();
			complexFourierTransform.Convention = TransformationConvention.Matlab;
			complexFourierTransform.TransformForward(m_ComplexFrameData);

			// Using frameSize here - code uses N/2...
			PickPeaks(m_ComplexFrameData, frameSize, numPeaks, minPeakSpace, maxTrackedFrequency, sampleRate, frameSize);
			CalculateInterpolatedValues(zeroPaddingCoefficient);
			//PitchDetection(m_ComplexFrameData, sampleRate);
		}

		private void CalculateInterpolatedValues(int zeroPaddingCoefficient)
		{
			double[] m_InterpolatedLocation = new double[m_PeakData.Count];
			double[] m_InterpolatedValue = new double[m_PeakData.Count];
			double[] m_InterpolatedPhase = new double[m_PeakData.Count];

			for (int loop = 0; loop < m_PeakData.Count; loop++)
			{
				double peakBin = 20 * Math.Log10(m_ComplexFrameData[m_PeakData[loop].m_Location].Modulus);
				double leftValueBin = 20 * Math.Log10(m_ComplexFrameData[Math.Max(m_PeakData[loop].m_Location - 1, 0)].Modulus);
				double rightValueBin = 20 * Math.Log10(m_ComplexFrameData[Math.Min(m_PeakData[loop].m_Location + 1, (m_ComplexFrameData.Length / 2) - 1)].Modulus);

				double peakPhase = Math.Atan2(m_ComplexFrameData[m_PeakData[loop].m_Location].Imag, m_ComplexFrameData[m_PeakData[loop].m_Location].Real);
				double leftPhase = Math.Atan2(m_ComplexFrameData[m_PeakData[loop].m_Location - 1].Imag, m_ComplexFrameData[m_PeakData[loop].m_Location - 1].Real);
				double rightPhase = Math.Atan2(m_ComplexFrameData[m_PeakData[loop].m_Location + 1].Imag, m_ComplexFrameData[m_PeakData[loop].m_Location + 1].Real);

				double interpolatedPeakLocation = m_PeakData[loop].m_Location + (0.5 * (leftValueBin - rightValueBin)) / (leftValueBin - 2 * peakBin + rightValueBin);
				interpolatedPeakLocation = Clamp(interpolatedPeakLocation, 0, (m_ComplexFrameData.Length / zeroPaddingCoefficient) - 1);

				/*
				Complex complexDataBelow = m_ComplexFrameData[(int)Math.Floor(interpolatedPeakLocation)];
				Complex complexDataAbove = m_ComplexFrameData[(int)Math.Floor(interpolatedPeakLocation + 1)];

				double interpFactor = interpolatedPeakLocation - m_PeakData[loop].m_Location;

				if (interpFactor < 0)
				{
					interpFactor += 1.0f;
					complexDataBelow = m_ComplexFrameData[m_PeakData[loop].m_Location - 1];
					complexDataAbove = m_ComplexFrameData[m_PeakData[loop].m_Location];
				}
				else
				{
					complexDataBelow = m_ComplexFrameData[m_PeakData[loop].m_Location];
					complexDataAbove = m_ComplexFrameData[m_PeakData[loop].m_Location + 1];
				}

				double leftPhase = Math.Atan2(complexDataBelow.Imag, complexDataBelow.Real);
				double rightPhase = Math.Atan2(complexDataAbove.Imag, complexDataAbove.Real);

				double phaseDiff = UnwrapPhase(rightPhase - leftPhase);
				double interpolatedPhase = leftPhase + (interpFactor * phaseDiff);
				*/

				double interpolatedAmplitude = peakBin - 0.25 * (leftValueBin - rightValueBin) * (interpolatedPeakLocation - m_PeakData[loop].m_Location);
				double interpolatedPhase = peakPhase - 0.25 * (leftPhase - rightPhase) * (interpolatedPeakLocation - m_PeakData[loop].m_Location);

				m_PeakData[loop].m_InterpolatedLocation = interpolatedPeakLocation;
				m_PeakData[loop].m_InterpolatedPhase = interpolatedPhase;
				m_PeakData[loop].m_InterpolatedAmplitude = interpolatedAmplitude;
			}
		}

		public static double UnwrapPhase(double input)
		{
			input = input - Math.Floor(input / 2 / Math.PI) * 2 * Math.PI;
			return input - (input >= Math.PI ? 1.0f : 0.0f) * 2 * Math.PI;
		}

		private void PitchDetection(Complex[] complexFrameData, int sampleRate)
		{
			double highEnergy = 0.0;
			double lowEnergy = 0.0;

			int highEnergyMin = (int)Math.Round((5000.0/sampleRate) * complexFrameData.Length);
			int lowEnergyMin = (int)Math.Round((50.0/sampleRate) * complexFrameData.Length);
			int lowEnergyMax = Math.Min((int)Math.Round((2000.0/sampleRate) * complexFrameData.Length), complexFrameData.Length);

			for (int sample = highEnergyMin; sample < complexFrameData.Length / 2; sample++)
			{
				highEnergy += complexFrameData[sample].Modulus;
			}

			for (int sample = lowEnergyMin; sample < lowEnergyMax; sample++)
			{
				lowEnergy += complexFrameData[sample].Modulus;
			}

			m_IsHarmonic = highEnergy / lowEnergy < 0.6;

			if (m_IsHarmonic)
			{
				// Why 50? Max peaks/2 maybe?
				int numPitchPeaks = Math.Min(50, m_PeakData.Count);
				CalculatePitch(sampleRate, numPitchPeaks, out m_Pitch, out m_PitchError);
			}

			m_IsHarmonic &= m_PitchError <= 1.5;
		}

		private void CalculatePitch(int sampleRate, int numPitchPeaks, out double pitchValue, out double pitchError)
		{
			double[] peakFrequencies = new double[numPitchPeaks];
			double[] peakAmplitudes = new double[numPitchPeaks];
			double[] peakAmplitudesTemp = new double[numPitchPeaks];

			double minValue = double.MaxValue;
			int minValueIndex = -1;

			for (int loop = 0; loop < numPitchPeaks; loop++)
			{
				peakAmplitudes[loop] = m_PeakData[loop].m_InterpolatedAmplitude;
				peakFrequencies[loop] = (m_PeakData[loop].m_InterpolatedLocation / (double)m_ComplexFrameData.Length) * sampleRate;

				if (peakFrequencies[loop] < minValue)
				{
					minValue = peakFrequencies[loop];
					minValueIndex = loop;
				}
			}

			// Avoid zero frequency peak
			if (minValue == 0.0)
			{
				peakFrequencies[minValueIndex] = 1;
				peakAmplitudes[minValueIndex] = -100;
			}

			peakAmplitudes.CopyTo(peakAmplitudesTemp, 0);

			double[] maxValues = new double[3];
			int[] maxValueLocations = new int[3];
			int numMaxValuesFound = 0;

			double maxValue = double.MinValue;
			int maxValueIndex = 0;

			while (numMaxValuesFound < 3)
			{
				for (int loop = 0; loop < numPitchPeaks; loop++)
				{
					if (peakAmplitudesTemp[loop] > maxValue)
					{
						maxValue = peakAmplitudesTemp[loop];
						maxValueIndex = loop;
					}
				}

				maxValues[numMaxValuesFound] = maxValue;
				maxValueLocations[numMaxValuesFound] = maxValueIndex;
				peakAmplitudesTemp[maxValueIndex] = -100;

				numMaxValuesFound++;
				maxValue = double.MinValue;
				maxValueIndex = 0;
			}

			const int numPitchCandidates = 10;
			double[] pitchCandidateArray = new double[3 * numPitchCandidates];

			for (int loop = 0; loop < numPitchCandidates * 3; loop++)
			{
				if (loop < numPitchCandidates)
				{
					pitchCandidateArray[loop] = peakFrequencies[maxValueLocations[0]] / (numPitchCandidates - loop);
				}
				else if (loop < numPitchCandidates * 2)
				{
					pitchCandidateArray[loop] = peakFrequencies[maxValueLocations[1]] / (numPitchCandidates - (loop - numPitchCandidates));
				}
				else
				{
					pitchCandidateArray[loop] = peakFrequencies[maxValueLocations[2]] / (numPitchCandidates - (loop - (numPitchCandidates * 2)));
				}
			}

			// Predicted to measured mismatch error
			double[] harmonicsPM = new double[pitchCandidateArray.Length];
			double[] predictedErrorPM = new double[pitchCandidateArray.Length];
			int maxNPM = Math.Min(10, m_PeakData.Count);
			pitchCandidateArray.CopyTo(harmonicsPM, 0);

			for (int npmLoop = 0; npmLoop < maxNPM; npmLoop++)
			{
				double[] minFreqDistance = new double[harmonicsPM.Length];
				double[] pondDif = new double[harmonicsPM.Length];
				double[] peakMagnitudes = new double[harmonicsPM.Length];
				double[] magnitudeFactors = new double[harmonicsPM.Length];
				int[] minFreqIndexes = new int[harmonicsPM.Length];

				for (int loop = 0; loop < minFreqDistance.Length; loop++)
				{
					minFreqDistance[loop] = double.MaxValue;
				}

				for (int row = 0; row < harmonicsPM.Length; row++)
				{
					for (int column = 0; column < peakFrequencies.Length; column++)
					{
						double diffValue = Math.Abs(harmonicsPM[row] - peakFrequencies[column]);

						if (diffValue < minFreqDistance[row])
						{
							minFreqDistance[row] = diffValue;
							minFreqIndexes[row] = column;
						}
					}
				}

				for (int loop = 0; loop < harmonicsPM.Length; loop++)
				{
					pondDif[loop] = minFreqDistance[loop] * Math.Pow(harmonicsPM[loop], -0.5);
					peakMagnitudes[loop] = m_PeakData[minFreqIndexes[loop]].m_InterpolatedAmplitude;
					magnitudeFactors[loop] = Math.Max(0.0, maxValues[0] - peakMagnitudes[loop] + 20);
					magnitudeFactors[loop] = Math.Max(0.0, 1.0 - magnitudeFactors[loop] / 75.0);
					predictedErrorPM[loop] += pondDif[loop] + magnitudeFactors[loop] * (1.4 * pondDif[loop] - 0.5);
					harmonicsPM[loop] += pitchCandidateArray[loop];
				}
			}

			// Measured to predicted mismatch error
			double[] predictedErrorMP = new double[pitchCandidateArray.Length];
			int maxNMP = Math.Min(10, peakFrequencies.Length);

			for (int pitchCandidateLoop = 0; pitchCandidateLoop < pitchCandidateArray.Length; pitchCandidateLoop++)
			{
				double[] harmonics = new double[maxNMP];
				double[] freqDistance = new double[maxNMP];
				double[] pondDif = new double[maxNMP];
				double[] peakMagnitudes = new double[maxNMP];
				double[] magnitudeFactors = new double[maxNMP];

				for (int loop = 0; loop < maxNMP; loop++)
				{
					// Not entirely sure about this round here - needed to be equivalent with matlab, but suggests an 0/1 array indexing bug in the matlab code?
					harmonics[loop] = Math.Max(1.0, Math.Round(peakFrequencies[loop] / pitchCandidateArray[pitchCandidateLoop]));
					freqDistance[loop] = Math.Abs(peakFrequencies[loop] - harmonics[loop] * pitchCandidateArray[pitchCandidateLoop]);
					pondDif[loop] = freqDistance[loop] * Math.Pow(peakFrequencies[loop], -0.5);
					peakMagnitudes[loop] = m_PeakData[loop].m_InterpolatedAmplitude;
					magnitudeFactors[loop] = Math.Max(0.0, maxValues[0] - peakMagnitudes[loop] + 20);
					magnitudeFactors[loop] = Math.Max(0.0, 1.0 - magnitudeFactors[loop] / 75.0);
					predictedErrorMP[pitchCandidateLoop] += magnitudeFactors[loop] * (pondDif[loop] + magnitudeFactors[loop] * (1.4 * pondDif[loop] - 0.5));
				}
			}

			double minError = double.MaxValue;
			int minErrorIndex = 0;

			for (int loop = 0; loop < predictedErrorPM.Length; loop++)
			{
				double errorValue = (predictedErrorPM[loop] / (double)maxNPM) + (0.3 * predictedErrorMP[loop] / maxNMP);

				if (errorValue < minError)
				{
					minError = errorValue;
					minErrorIndex = loop;
				}
			}

			pitchError = minError;
			pitchValue = pitchCandidateArray[minErrorIndex];
		}

		private void PickPeaks(Complex[] values, int valuesToUse, int numPeaks, int minPeakSpace, int maxTrackedFrequency, int sampleRate, int frameSize)
		{
			List<PeakData> potentialPeakLocations = new List<PeakData>();
			m_PeakData = new List<PeakData>();

			// Identify the peaks
			for (int sample = 1; sample < valuesToUse - 1; sample++)
			{
				double thisModulus = values[sample].Modulus;
				double prevModulus = values[sample - 1].Modulus;
				double nextModulus = values[sample + 1].Modulus;

				if (thisModulus > prevModulus && thisModulus > nextModulus)
				{
					PeakData newPeakData = new PeakData(sample, values[sample]);
					potentialPeakLocations.Add(newPeakData);
				}
			}

			// Sort the peaks by amplitude
			potentialPeakLocations.Sort();

			// Add all peaks that aren't too close to existing peaks
			while (m_PeakData.Count < numPeaks && potentialPeakLocations.Count > 0)
			{
				PeakData potentialPeak = potentialPeakLocations[0];
				bool peakValid = true;

				for (int compare = 0; compare < m_PeakData.Count; compare++)
				{
					if (Math.Abs(m_PeakData[compare].m_Location - potentialPeak.m_Location) <= minPeakSpace)
					{
						peakValid = false;
						break;
					}
				}

				if (peakValid)
				{
					m_PeakData.Add(new PeakData(potentialPeak.m_Location, potentialPeak.m_PeakValue.Modulus));
				}

				potentialPeakLocations.RemoveAt(0);
			}
		}

		private static double[] GrabFrame(float[] sampleData, double[] analysisWindow, int sampleStartIndex, int frameSize)
		{
			double[] frameData = new double[frameSize];

			for (int sample = 0; sample < frameSize && sampleStartIndex + sample < sampleData.Length; sample++)
			{
				frameData[sample] = sampleData[sampleStartIndex + sample] * analysisWindow[sample];
			}

			return frameData;
		}

		private static double[] PadFrameData(double[] frameData, int frameSize, int zeroPaddingCoefficient)
		{
			int paddedFrameSize = frameSize * zeroPaddingCoefficient;
			double[] paddedFrameData = new double[paddedFrameSize];

			for (int sample = 0; sample < (frameSize / 2); sample++)
			{
				paddedFrameData[sample] = frameData[(frameSize / 2) + sample];
			}

			int sampleIndex = 0;
			for (int sample = paddedFrameSize - (frameSize / 2); sample < paddedFrameSize; sample++)
			{
				paddedFrameData[sample] = frameData[sampleIndex];
				sampleIndex++;
			}

			return paddedFrameData;
		}

		private static Complex[] ConvertToComplex(double[] data)
		{
			Complex[] complexData = new Complex[data.Length];

			for (int i = 0; i < data.Length; i++)
			{
				complexData[i] = new Complex(data[i], 0.0f);
			}

			return complexData;
		}
	}
}
