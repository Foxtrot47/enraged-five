﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SpectralAnalysis
{
	public partial class ProgressBar : Form
	{
		public ProgressBar()
		{
			InitializeComponent();
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			Form1 parent = Owner as Form1;
			progressBar1.Value = parent.m_AnalysisProgress;

			if (parent.m_AnalysisThread == null ||
				!parent.m_AnalysisThread.IsAlive)
			{
				parent.OnAnalysisThreadComplete();
				this.Close();
			}
		}
	}
}
