﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectralAnalysis
{
	public class PeakTracker
	{
		public class TrackedPeakData
		{
			public AnalysisFrame.PeakData m_PeakData;
			public bool m_ActiveThisFrame;

			public TrackedPeakData(AnalysisFrame.PeakData peakData, bool activeThisFrame)
			{
				m_PeakData = peakData;
				m_ActiveThisFrame = activeThisFrame;
			}
		}

		public class TrackedPeak
		{
			public List<TrackedPeakData> m_FrameData = new List<TrackedPeakData>();
		}

		public List<TrackedPeak> m_TrackedPeaks = new List<TrackedPeak>();

		public void TrackPeaks(List<AnalysisFrame> frames, int numPeaks, int sampleRate)
		{
			m_TrackedPeaks = new List<TrackedPeak>();

			for (int frameIndex = 1; frameIndex < frames.Count; frameIndex++)
			{
				AnalysisFrame analysisFrame = frames[frameIndex];
				List<AnalysisFrame.PeakData> peakData = new List<AnalysisFrame.PeakData>(analysisFrame.FramePeakData);
				peakData.Sort();

				if (frameIndex == 1)
				{
					while (m_TrackedPeaks.Count < numPeaks && peakData.Count > 0)
					{
						TrackedPeak newPeak = new TrackedPeak();
						TrackedPeakData frameData = new TrackedPeakData(peakData[0], true);
						newPeak.m_FrameData.Add(frameData);
						m_TrackedPeaks.Add(newPeak);
						peakData.RemoveAt(0);
					}
				}
				else
				{
					double harmonic = analysisFrame.Pitch;

					for (int trackedPeakIndex = 0; trackedPeakIndex < m_TrackedPeaks.Count; trackedPeakIndex++)
					{
						TrackedPeak trackedPeak = m_TrackedPeaks[trackedPeakIndex];
						AnalysisFrame.PeakData prevFrameData = trackedPeak.m_FrameData.Last().m_PeakData;
						double closestPeak = double.MaxValue;
						int closestPeakIndex = 0;

						for (int peakDataLoop = 0; peakDataLoop < peakData.Count; peakDataLoop++)
						{
							double peakDistance;

							if (analysisFrame.IsHarmonic)
							{
								peakDistance = Math.Abs((peakData[peakDataLoop].m_InterpolatedLocation - 1) / analysisFrame.ComplexFrameData.Length * sampleRate - harmonic);
							}
							else
							{
								peakDistance = Math.Abs(peakData[peakDataLoop].m_InterpolatedLocation - prevFrameData.m_InterpolatedLocation);
							}

							if (peakDistance < closestPeak)
							{
								closestPeak = peakDistance;
								closestPeakIndex = peakDataLoop;
							}
						}

						harmonic += analysisFrame.Pitch;

						TrackedPeakData trackedPeakData = new TrackedPeakData(peakData[closestPeakIndex], true);
						trackedPeak.m_FrameData.Add(trackedPeakData);
					}
				}
			}
		}

		public void TrackPeaksAdvanced(List<AnalysisFrame> frames, int numPeaks, int sampleRate, double maxFrequencyShift, Form1 parentForm, int startFrame, bool forwards, int inactiveFrameLimit, bool logarithmicTracking)
		{
			int frameIndex = startFrame;
			int trackDirection = forwards ? 1 : -1;

			for (; (forwards && frameIndex < frames.Count) || (!forwards && frameIndex >= 0); frameIndex += trackDirection)
			{
				AnalysisFrame analysisFrame = frames[frameIndex];
				List<AnalysisFrame.PeakData> peakData = new List<AnalysisFrame.PeakData>(analysisFrame.FramePeakData);
				peakData.Sort();

				if (m_TrackedPeaks.Count == 0)
				{
					while (m_TrackedPeaks.Count < numPeaks && peakData.Count > 0)
					{
						TrackedPeak newPeak = new TrackedPeak();
						TrackedPeakData trackedPeakData = new TrackedPeakData(peakData[0], false);
						newPeak.m_FrameData.Add(trackedPeakData);
						m_TrackedPeaks.Add(newPeak);
						peakData.RemoveAt(0);
					}
				}
				else
				{
					bool[] trackedPeakFlags = new bool[m_TrackedPeaks.Count];

					for (int loop = 0; loop < m_TrackedPeaks.Count; loop++)
					{
						double closestTrackedPeakDistance = double.MaxValue;
						int closestTrackedPeakIndex = -1;
						int closestTrackedPeakPeakIndex = -1;

						// Go through each peak and find the one that has the closest match with one from the previous frame
						for (int trackedPeakIndex = 0; trackedPeakIndex < m_TrackedPeaks.Count; trackedPeakIndex++)
						{
							if (!trackedPeakFlags[trackedPeakIndex])
							{
								TrackedPeak trackedPeak = m_TrackedPeaks[trackedPeakIndex];
								AnalysisFrame.PeakData prevFrameData = forwards ? trackedPeak.m_FrameData.Last().m_PeakData : trackedPeak.m_FrameData[0].m_PeakData;
								double closestPeak = double.MaxValue;
								int closestPeakIndex = -1;

								for (int peakDataLoop = 0; peakDataLoop < peakData.Count; peakDataLoop++)
								{
									double peakDistance;

									if (logarithmicTracking)
									{
										peakDistance = Math.Abs(Math.Log10(peakData[peakDataLoop].m_InterpolatedLocation) - Math.Log10(prevFrameData.m_InterpolatedLocation));
									}
									else
									{
										peakDistance = Math.Abs(peakData[peakDataLoop].m_InterpolatedLocation - prevFrameData.m_InterpolatedLocation);
									}

									if (peakDistance < closestPeak && peakDistance < maxFrequencyShift)
									{
										closestPeak = peakDistance;
										closestPeakIndex = peakDataLoop;
									}
								}

								if (closestPeak >= 0 && closestPeak < closestTrackedPeakDistance)
								{
									closestTrackedPeakDistance = closestPeak;
									closestTrackedPeakIndex = trackedPeakIndex;
									closestTrackedPeakPeakIndex = closestPeakIndex;
								}
							}
						}

						if (closestTrackedPeakIndex >= 0)
						{
							TrackedPeakData trackedPeakData = new TrackedPeakData(peakData[closestTrackedPeakPeakIndex], true);

							if (forwards)
							{
								m_TrackedPeaks[closestTrackedPeakIndex].m_FrameData.Add(trackedPeakData);
							}
							else
							{
								m_TrackedPeaks[closestTrackedPeakIndex].m_FrameData[0].m_ActiveThisFrame = true;
								m_TrackedPeaks[closestTrackedPeakIndex].m_FrameData.Insert(0, trackedPeakData);
							}

							trackedPeakFlags[closestTrackedPeakIndex] = true;
							peakData.RemoveAt(closestTrackedPeakPeakIndex);
						}
					}

					for (int trackedPeakIndex = 0; trackedPeakIndex < m_TrackedPeaks.Count; trackedPeakIndex++)
					{
						if (!trackedPeakFlags[trackedPeakIndex])
						{
							TrackedPeak trackedPeak = m_TrackedPeaks[trackedPeakIndex];
							int numInactiveFrames = 0;

							if (forwards)
							{
								for (int loop = 1; loop < trackedPeak.m_FrameData.Count; loop++)
								{
									if (!trackedPeak.m_FrameData[trackedPeak.m_FrameData.Count - 1 - loop].m_ActiveThisFrame)
									{
										numInactiveFrames++;
									}
									else
									{
										break;
									}
								}
							}
							else
							{
								for (int loop = 0; loop < trackedPeak.m_FrameData.Count; loop++)
								{
									if (!trackedPeak.m_FrameData[loop].m_ActiveThisFrame)
									{
										numInactiveFrames++;
									}
									else
									{
										break;
									}
								}
							}

							if (!forwards)
							{
								trackedPeak.m_FrameData[0].m_ActiveThisFrame = false;
							}

							// If the peak has only disappeared a few frames ago, keep tracking the same pitch in the event that
							// it reappears
							if (numInactiveFrames < Math.Min(inactiveFrameLimit, trackedPeak.m_FrameData.Count - 1))
							{
								TrackedPeakData trackedPeakData = new TrackedPeakData((forwards ? trackedPeak.m_FrameData.Last().m_PeakData : trackedPeak.m_FrameData[0].m_PeakData), false);

								if (forwards)
								{
									trackedPeak.m_FrameData.Add(trackedPeakData);
								}
								else
								{
									trackedPeak.m_FrameData.Insert(0, trackedPeakData);
								}
							}
							else if (peakData.Count > 0)
							{
								if (forwards)
								{
									trackedPeak.m_FrameData.Add(new TrackedPeakData(peakData[0], false));
								}
								else
								{
									trackedPeak.m_FrameData.Insert(0, new TrackedPeakData(peakData[0], false));
								}

								peakData.RemoveAt(0);
							}
							else
							{
								MathNet.Numerics.Complex dummyComplex = new MathNet.Numerics.Complex();

								if (forwards)
								{
									trackedPeak.m_FrameData.Add(new TrackedPeakData(new AnalysisFrame.PeakData(0, dummyComplex), false));
								}
								else
								{
									trackedPeak.m_FrameData.Insert(0, new TrackedPeakData(new AnalysisFrame.PeakData(0, dummyComplex), false));
								}
							}
						}
					}
				}

				if (forwards)
				{
					parentForm.m_AnalysisProgress = (int)(80.0f + ((frameIndex / (float)frames.Count) * 20.0f));
				}
			}
		}

		public void FixupPeaks(int inactivePeakLimit, double maxFrequencyShift)
		{
			if (inactivePeakLimit > 0)
			{
				foreach (TrackedPeak trackedPeak in m_TrackedPeaks)
				{
					int numInactivePeaks = 0;

					for (int frame = 0; frame < trackedPeak.m_FrameData.Count; frame++)
					{
						if (!trackedPeak.m_FrameData[frame].m_ActiveThisFrame)
						{
							numInactivePeaks++;
						}
						else
						{
							if (numInactivePeaks < inactivePeakLimit)
							{
								for (int prevFrame = frame - 1; prevFrame > frame - numInactivePeaks; prevFrame--)
								{
									double peakDistance = Math.Abs(trackedPeak.m_FrameData[frame].m_PeakData.m_InterpolatedLocation - trackedPeak.m_FrameData[prevFrame].m_PeakData.m_InterpolatedLocation);

									if (peakDistance < maxFrequencyShift)
									{
										if (prevFrame > 0)
										{
											double prevPrevPeakDistance = Math.Abs(trackedPeak.m_FrameData[frame].m_PeakData.m_InterpolatedLocation - trackedPeak.m_FrameData[prevFrame - 1].m_PeakData.m_InterpolatedLocation);

											if (prevPrevPeakDistance < maxFrequencyShift)
											{
												trackedPeak.m_FrameData[prevFrame].m_ActiveThisFrame = true;
											}
											else
											{
												break;
											}
										}
										else
										{
											trackedPeak.m_FrameData[prevFrame].m_ActiveThisFrame = true;
										}
									}
									else
									{
										break;
									}
								}
							}

							numInactivePeaks = 0;
						}
					}
				}
			}
		}
	}
}
