﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectralAnalysis
{
	public class WindowingFunction
	{
		public enum WindowType
		{
			BlackmanHarris,
			Hann,
			Hamming,
			SineCubed,
			Sine,
			Triangle,
			Rectangle,
			Max
		};

		public WindowType m_WindowType = WindowType.Max;
		public double[] m_WindowValues;
		public int FrameSizeSamples { get { return m_WindowValues.Length; } }

		public WindowingFunction()
		{
			SetWindowType(WindowType.BlackmanHarris);
		}

		public void SetWindowType(WindowType type)
		{
			if (type != m_WindowType)
			{
				Generate(4096, type);
				m_WindowType = type;
			}
		}

		public void Generate(int frameSizeSamples, WindowType type)
		{
			m_WindowValues = new double[frameSizeSamples];

			switch (type)
			{
				case WindowType.Rectangle:
					for (int loop = 0; loop < frameSizeSamples; loop++)
					{
						m_WindowValues[loop] = 0.97;
					}
					break;
				case WindowType.Triangle:
					for (int loop = 0; loop < frameSizeSamples; loop++)
					{
						m_WindowValues[loop] = 1.0 - (Math.Abs((loop - frameSizeSamples / 2.0) / (double)frameSizeSamples) / 0.5);
					}
					break;
				case WindowType.Sine:
					for (int loop = 0; loop < frameSizeSamples; loop++)
					{
						m_WindowValues[loop] = Math.Sin((Math.PI * (loop / (double)frameSizeSamples)));
					}
					break;
				case WindowType.SineCubed:
					for (int loop = 0; loop < frameSizeSamples; loop++)
					{
						m_WindowValues[loop] = Math.Pow(Math.Sin((Math.PI * (loop / (double)frameSizeSamples))), 3.0);
					}
					break;
				case WindowType.Hann:
					for (int loop = 0; loop < frameSizeSamples; loop++)
					{
						m_WindowValues[loop] = 0.5 * (1 - Math.Cos((2.0f * Math.PI * loop) / (frameSizeSamples)));
					}
					break;
				case WindowType.Hamming:
					for (int loop = 0; loop < frameSizeSamples; loop++)
					{
						m_WindowValues[loop] = 0.54 - 0.46 * (Math.Cos((2.0f * Math.PI * loop) / (frameSizeSamples)));
					}
					break;
				case WindowType.BlackmanHarris:

					const int n = 512;
					double theta = -4 * 2 * Math.PI / n;
					double[] constValues = { 0.35875, 0.48829, 0.14128, 0.01168 };
					double thetaIncr = 8 * 2 * Math.PI / n / frameSizeSamples;

					for (int i = 0; i < frameSizeSamples; i++)
					{
						for (int m = 0; m < 4; m++)
						{
							m_WindowValues[i] -= constValues[m]/2 * (sine2sine(theta-m*2*Math.PI/n, n) + sine2sine(theta + m * 2 * Math.PI/n, n));
						}

						theta += thetaIncr;
					}

					double divisor = m_WindowValues[frameSizeSamples / 2 + 1];

					for (int i = 0; i < frameSizeSamples; i++)
					{
						m_WindowValues[i] /= divisor;
					}
					break;
				default:
					break;
			}
		}

		double sine2sine(double x, int N)
		{
			return Math.Sin((N/2.0)*x) / Math.Sin(x/2);
		}
	}
}
