﻿namespace SpectralAnalysis
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.windowingFnCombo = new System.Windows.Forms.ComboBox();
			this.windowingFnGraph = new System.Windows.Forms.PictureBox();
			this.fftMagnitudeGraph = new System.Windows.Forms.PictureBox();
			this.frameIndexSlider = new System.Windows.Forms.TrackBar();
			this.trackedPeakGraph = new System.Windows.Forms.PictureBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadWaveFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.frameTextBox = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.selectedSineHzText = new System.Windows.Forms.TextBox();
			this.drawVolumeCheckbox = new System.Windows.Forms.CheckBox();
			this.drawFrequencyCheckbox = new System.Windows.Forms.CheckBox();
			this.showAllSinesCheckbox = new System.Windows.Forms.CheckBox();
			this.selectedSineTextbox = new System.Windows.Forms.TextBox();
			this.sineSelectionScrollBar = new System.Windows.Forms.TrackBar();
			this.trackedPeaksUpDown = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.trackedSinesUpDown = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.refreshDataBtn = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.frameSizeCombo = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.hopSizeCombo = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.minPeakSpaceUpDown = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.maxFreqDifference = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this.clippingVolumeFix = new System.Windows.Forms.NumericUpDown();
			this.label8 = new System.Windows.Forms.Label();
			this.windowingFunctionBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.label9 = new System.Windows.Forms.Label();
			this.maxTrackedFrequencyUpDown = new System.Windows.Forms.NumericUpDown();
			this.peakTrackingStartFraction = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.inactiveFrameTrackingLimitUpDown = new System.Windows.Forms.NumericUpDown();
			this.label11 = new System.Windows.Forms.Label();
			this.inactiveFramePlaybackLimitUpDown = new System.Windows.Forms.NumericUpDown();
			this.soundTypeTab = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.inputSpectrogram = new System.Windows.Forms.PictureBox();
			this.inputWaveView = new System.Windows.Forms.PictureBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.residualSpectrogram = new System.Windows.Forms.PictureBox();
			this.residualWaveView = new System.Windows.Forms.PictureBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.synthesisedSpectrogram = new System.Windows.Forms.PictureBox();
			this.synthesisedWaveView = new System.Windows.Forms.PictureBox();
			this.playInputButton = new System.Windows.Forms.Button();
			this.playResidualButton = new System.Windows.Forms.Button();
			this.playSynthesisedData = new System.Windows.Forms.Button();
			this.playOutputButton = new System.Windows.Forms.Button();
			this.resizeTimer = new System.Windows.Forms.Timer(this.components);
			this.label12 = new System.Windows.Forms.Label();
			this.sampleRateCombo = new System.Windows.Forms.NumericUpDown();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.waveViewZoom = new System.Windows.Forms.TrackBar();
			this.frameSampleViewGraph = new System.Windows.Forms.PictureBox();
			this.zeroPaddingCoefficient = new System.Windows.Forms.NumericUpDown();
			this.label13 = new System.Windows.Forms.Label();
			this.logarithmicTrackingCheckbox = new System.Windows.Forms.CheckBox();
			this.randomVolumeUpDown = new System.Windows.Forms.NumericUpDown();
			this.label14 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.windowingFnGraph)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fftMagnitudeGraph)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frameIndexSlider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackedPeakGraph)).BeginInit();
			this.menuStrip1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.sineSelectionScrollBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackedPeaksUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackedSinesUpDown)).BeginInit();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.minPeakSpaceUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.maxFreqDifference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.clippingVolumeFix)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.windowingFunctionBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.maxTrackedFrequencyUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.peakTrackingStartFraction)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.inactiveFrameTrackingLimitUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.inactiveFramePlaybackLimitUpDown)).BeginInit();
			this.soundTypeTab.SuspendLayout();
			this.tabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.inputSpectrogram)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.inputWaveView)).BeginInit();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.residualSpectrogram)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.residualWaveView)).BeginInit();
			this.tabPage3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.synthesisedSpectrogram)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.synthesisedWaveView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.sampleRateCombo)).BeginInit();
			this.groupBox4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.waveViewZoom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frameSampleViewGraph)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.zeroPaddingCoefficient)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.randomVolumeUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// windowingFnCombo
			// 
			this.windowingFnCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.windowingFnCombo.FormattingEnabled = true;
			this.windowingFnCombo.Items.AddRange(new object[] {
            "Blackman-Harris",
            "Hann",
            "Hamming",
            "SineCubed",
            "Sine",
            "Triangle",
            "Rectangle"});
			this.windowingFnCombo.Location = new System.Drawing.Point(6, 134);
			this.windowingFnCombo.Name = "windowingFnCombo";
			this.windowingFnCombo.Size = new System.Drawing.Size(110, 21);
			this.windowingFnCombo.TabIndex = 0;
			this.windowingFnCombo.Text = "Blackman-Harris";
			this.windowingFnCombo.SelectedIndexChanged += new System.EventHandler(this.windowingFnCombo_SelectedIndexChanged);
			// 
			// windowingFnGraph
			// 
			this.windowingFnGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.windowingFnGraph.Location = new System.Drawing.Point(6, 19);
			this.windowingFnGraph.Name = "windowingFnGraph";
			this.windowingFnGraph.Size = new System.Drawing.Size(220, 109);
			this.windowingFnGraph.TabIndex = 1;
			this.windowingFnGraph.TabStop = false;
			this.windowingFnGraph.Paint += new System.Windows.Forms.PaintEventHandler(this.windowingFnGraph_Paint);
			// 
			// fftMagnitudeGraph
			// 
			this.fftMagnitudeGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.fftMagnitudeGraph.Location = new System.Drawing.Point(8, 19);
			this.fftMagnitudeGraph.Name = "fftMagnitudeGraph";
			this.fftMagnitudeGraph.Size = new System.Drawing.Size(690, 237);
			this.fftMagnitudeGraph.TabIndex = 2;
			this.fftMagnitudeGraph.TabStop = false;
			this.fftMagnitudeGraph.Paint += new System.Windows.Forms.PaintEventHandler(this.fftMagnitudeGraph_Paint);
			// 
			// frameIndexSlider
			// 
			this.frameIndexSlider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.frameIndexSlider.Location = new System.Drawing.Point(98, 262);
			this.frameIndexSlider.Maximum = 100;
			this.frameIndexSlider.Name = "frameIndexSlider";
			this.frameIndexSlider.Size = new System.Drawing.Size(600, 45);
			this.frameIndexSlider.TabIndex = 3;
			this.frameIndexSlider.TickStyle = System.Windows.Forms.TickStyle.None;
			this.frameIndexSlider.Scroll += new System.EventHandler(this.frameIndexSlider_Scroll);
			// 
			// trackedPeakGraph
			// 
			this.trackedPeakGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.trackedPeakGraph.Location = new System.Drawing.Point(6, 19);
			this.trackedPeakGraph.Name = "trackedPeakGraph";
			this.trackedPeakGraph.Size = new System.Drawing.Size(692, 251);
			this.trackedPeakGraph.TabIndex = 6;
			this.trackedPeakGraph.TabStop = false;
			this.trackedPeakGraph.Paint += new System.Windows.Forms.PaintEventHandler(this.trackedPeakGraph_Paint);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1519, 24);
			this.menuStrip1.TabIndex = 8;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// loadToolStripMenuItem
			// 
			this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
			this.loadToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
			this.loadToolStripMenuItem.Text = "Load Wave File";
			this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
			// 
			// loadWaveFileDialog
			// 
			this.loadWaveFileDialog.FileName = "openFileDialog1";
			this.loadWaveFileDialog.Filter = "Wave files (*.wav)|*.wav";
			this.loadWaveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.loadWaveFileDialog_FileOk);
			// 
			// frameTextBox
			// 
			this.frameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.frameTextBox.BackColor = System.Drawing.SystemColors.Control;
			this.frameTextBox.Location = new System.Drawing.Point(6, 262);
			this.frameTextBox.Name = "frameTextBox";
			this.frameTextBox.ReadOnly = true;
			this.frameTextBox.Size = new System.Drawing.Size(86, 20);
			this.frameTextBox.TabIndex = 9;
			this.frameTextBox.Text = "0/0";
			this.frameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.frameTextBox.TextChanged += new System.EventHandler(this.frameTextBox_TextChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.fftMagnitudeGraph);
			this.groupBox1.Controls.Add(this.frameTextBox);
			this.groupBox1.Controls.Add(this.frameIndexSlider);
			this.groupBox1.Location = new System.Drawing.Point(555, 47);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(704, 314);
			this.groupBox1.TabIndex = 10;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Frame Data";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.selectedSineHzText);
			this.groupBox2.Controls.Add(this.drawVolumeCheckbox);
			this.groupBox2.Controls.Add(this.drawFrequencyCheckbox);
			this.groupBox2.Controls.Add(this.showAllSinesCheckbox);
			this.groupBox2.Controls.Add(this.selectedSineTextbox);
			this.groupBox2.Controls.Add(this.sineSelectionScrollBar);
			this.groupBox2.Controls.Add(this.trackedPeakGraph);
			this.groupBox2.Location = new System.Drawing.Point(555, 612);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(704, 327);
			this.groupBox2.TabIndex = 11;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Peak Tracking";
			// 
			// selectedSineHzText
			// 
			this.selectedSineHzText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.selectedSineHzText.BackColor = System.Drawing.SystemColors.Control;
			this.selectedSineHzText.Location = new System.Drawing.Point(98, 276);
			this.selectedSineHzText.Name = "selectedSineHzText";
			this.selectedSineHzText.ReadOnly = true;
			this.selectedSineHzText.Size = new System.Drawing.Size(86, 20);
			this.selectedSineHzText.TabIndex = 15;
			this.selectedSineHzText.Text = "0 Hz";
			this.selectedSineHzText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// drawVolumeCheckbox
			// 
			this.drawVolumeCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.drawVolumeCheckbox.AutoSize = true;
			this.drawVolumeCheckbox.Location = new System.Drawing.Point(210, 302);
			this.drawVolumeCheckbox.Name = "drawVolumeCheckbox";
			this.drawVolumeCheckbox.Size = new System.Drawing.Size(89, 17);
			this.drawVolumeCheckbox.TabIndex = 14;
			this.drawVolumeCheckbox.Text = "Draw Volume";
			this.drawVolumeCheckbox.UseVisualStyleBackColor = true;
			this.drawVolumeCheckbox.CheckedChanged += new System.EventHandler(this.drawVolumeCheckbox_CheckedChanged);
			// 
			// drawFrequencyCheckbox
			// 
			this.drawFrequencyCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.drawFrequencyCheckbox.AutoSize = true;
			this.drawFrequencyCheckbox.Checked = true;
			this.drawFrequencyCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.drawFrequencyCheckbox.Location = new System.Drawing.Point(98, 302);
			this.drawFrequencyCheckbox.Name = "drawFrequencyCheckbox";
			this.drawFrequencyCheckbox.Size = new System.Drawing.Size(104, 17);
			this.drawFrequencyCheckbox.TabIndex = 13;
			this.drawFrequencyCheckbox.Text = "Draw Frequency";
			this.drawFrequencyCheckbox.UseVisualStyleBackColor = true;
			this.drawFrequencyCheckbox.CheckedChanged += new System.EventHandler(this.drawFrequencyCheckbox_CheckedChanged);
			// 
			// showAllSinesCheckbox
			// 
			this.showAllSinesCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.showAllSinesCheckbox.AutoSize = true;
			this.showAllSinesCheckbox.Checked = true;
			this.showAllSinesCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.showAllSinesCheckbox.Location = new System.Drawing.Point(6, 302);
			this.showAllSinesCheckbox.Name = "showAllSinesCheckbox";
			this.showAllSinesCheckbox.Size = new System.Drawing.Size(67, 17);
			this.showAllSinesCheckbox.TabIndex = 12;
			this.showAllSinesCheckbox.Text = "Show All";
			this.showAllSinesCheckbox.UseVisualStyleBackColor = true;
			this.showAllSinesCheckbox.CheckedChanged += new System.EventHandler(this.showAllSinesCheckbox_CheckedChanged);
			// 
			// selectedSineTextbox
			// 
			this.selectedSineTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.selectedSineTextbox.BackColor = System.Drawing.SystemColors.Control;
			this.selectedSineTextbox.Location = new System.Drawing.Point(6, 276);
			this.selectedSineTextbox.Name = "selectedSineTextbox";
			this.selectedSineTextbox.ReadOnly = true;
			this.selectedSineTextbox.Size = new System.Drawing.Size(86, 20);
			this.selectedSineTextbox.TabIndex = 11;
			this.selectedSineTextbox.Text = "0/0";
			this.selectedSineTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.selectedSineTextbox.TextChanged += new System.EventHandler(this.selectedSineTextbox_TextChanged);
			// 
			// sineSelectionScrollBar
			// 
			this.sineSelectionScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.sineSelectionScrollBar.Location = new System.Drawing.Point(190, 276);
			this.sineSelectionScrollBar.Maximum = 100;
			this.sineSelectionScrollBar.Name = "sineSelectionScrollBar";
			this.sineSelectionScrollBar.Size = new System.Drawing.Size(508, 45);
			this.sineSelectionScrollBar.TabIndex = 10;
			this.sineSelectionScrollBar.TickStyle = System.Windows.Forms.TickStyle.None;
			this.sineSelectionScrollBar.Scroll += new System.EventHandler(this.sineSelectionScrollBar_Scroll);
			// 
			// trackedPeaksUpDown
			// 
			this.trackedPeaksUpDown.Location = new System.Drawing.Point(1417, 346);
			this.trackedPeaksUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.trackedPeaksUpDown.Name = "trackedPeaksUpDown";
			this.trackedPeaksUpDown.Size = new System.Drawing.Size(80, 20);
			this.trackedPeaksUpDown.TabIndex = 12;
			this.trackedPeaksUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(1262, 348);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(80, 13);
			this.label2.TabIndex = 13;
			this.label2.Text = "Tracked Peaks";
			// 
			// trackedSinesUpDown
			// 
			this.trackedSinesUpDown.Location = new System.Drawing.Point(1417, 372);
			this.trackedSinesUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.trackedSinesUpDown.Name = "trackedSinesUpDown";
			this.trackedSinesUpDown.Size = new System.Drawing.Size(80, 20);
			this.trackedSinesUpDown.TabIndex = 14;
			this.trackedSinesUpDown.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(1262, 374);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(76, 13);
			this.label3.TabIndex = 15;
			this.label3.Text = "Tracked Sines";
			// 
			// refreshDataBtn
			// 
			this.refreshDataBtn.Enabled = false;
			this.refreshDataBtn.Location = new System.Drawing.Point(1265, 596);
			this.refreshDataBtn.Name = "refreshDataBtn";
			this.refreshDataBtn.Size = new System.Drawing.Size(163, 23);
			this.refreshDataBtn.TabIndex = 16;
			this.refreshDataBtn.Text = "Refresh";
			this.refreshDataBtn.UseVisualStyleBackColor = true;
			this.refreshDataBtn.Click += new System.EventHandler(this.refreshDataBtn_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.windowingFnGraph);
			this.groupBox3.Controls.Add(this.windowingFnCombo);
			this.groupBox3.Location = new System.Drawing.Point(1265, 47);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(232, 161);
			this.groupBox3.TabIndex = 17;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Windowing Function";
			// 
			// frameSizeCombo
			// 
			this.frameSizeCombo.FormattingEnabled = true;
			this.frameSizeCombo.Items.AddRange(new object[] {
            "32768",
            "16384",
            "8192",
            "4096",
            "2048",
            "1024",
            "512",
            "256",
            "128",
            "64",
            "32"});
			this.frameSizeCombo.Location = new System.Drawing.Point(1417, 266);
			this.frameSizeCombo.Name = "frameSizeCombo";
			this.frameSizeCombo.Size = new System.Drawing.Size(80, 21);
			this.frameSizeCombo.TabIndex = 18;
			this.frameSizeCombo.Text = "2048";
			this.frameSizeCombo.SelectedIndexChanged += new System.EventHandler(this.frameSizeCombo_SelectedIndexChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(1262, 269);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(59, 13);
			this.label4.TabIndex = 19;
			this.label4.Text = "Frame Size";
			// 
			// hopSizeCombo
			// 
			this.hopSizeCombo.FormattingEnabled = true;
			this.hopSizeCombo.Items.AddRange(new object[] {
            "1024",
            "512",
            "256",
            "128",
            "64",
            "32",
            "16",
            "8",
            "4",
            "2",
            "1"});
			this.hopSizeCombo.Location = new System.Drawing.Point(1417, 293);
			this.hopSizeCombo.Name = "hopSizeCombo";
			this.hopSizeCombo.Size = new System.Drawing.Size(80, 21);
			this.hopSizeCombo.TabIndex = 20;
			this.hopSizeCombo.Text = "256";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(1262, 296);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(50, 13);
			this.label5.TabIndex = 21;
			this.label5.Text = "Hop Size";
			// 
			// minPeakSpaceUpDown
			// 
			this.minPeakSpaceUpDown.Location = new System.Drawing.Point(1417, 240);
			this.minPeakSpaceUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.minPeakSpaceUpDown.Name = "minPeakSpaceUpDown";
			this.minPeakSpaceUpDown.Size = new System.Drawing.Size(80, 20);
			this.minPeakSpaceUpDown.TabIndex = 22;
			this.minPeakSpaceUpDown.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(1262, 242);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(86, 13);
			this.label6.TabIndex = 23;
			this.label6.Text = "Min Peak Space";
			// 
			// maxFreqDifference
			// 
			this.maxFreqDifference.DecimalPlaces = 3;
			this.maxFreqDifference.Location = new System.Drawing.Point(1417, 398);
			this.maxFreqDifference.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
			this.maxFreqDifference.Name = "maxFreqDifference";
			this.maxFreqDifference.Size = new System.Drawing.Size(80, 20);
			this.maxFreqDifference.TabIndex = 24;
			this.maxFreqDifference.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(1262, 400);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(75, 13);
			this.label7.TabIndex = 25;
			this.label7.Text = "Max Freq Shift";
			// 
			// clippingVolumeFix
			// 
			this.clippingVolumeFix.DecimalPlaces = 2;
			this.clippingVolumeFix.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.clippingVolumeFix.Location = new System.Drawing.Point(1417, 424);
			this.clippingVolumeFix.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.clippingVolumeFix.Name = "clippingVolumeFix";
			this.clippingVolumeFix.Size = new System.Drawing.Size(80, 20);
			this.clippingVolumeFix.TabIndex = 26;
			this.clippingVolumeFix.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(1262, 426);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(60, 13);
			this.label8.TabIndex = 27;
			this.label8.Text = "Clipping Fix";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(1262, 216);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(80, 13);
			this.label9.TabIndex = 30;
			this.label9.Text = "Max Frequency";
			// 
			// maxTrackedFrequencyUpDown
			// 
			this.maxTrackedFrequencyUpDown.Location = new System.Drawing.Point(1417, 214);
			this.maxTrackedFrequencyUpDown.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
			this.maxTrackedFrequencyUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
			this.maxTrackedFrequencyUpDown.Name = "maxTrackedFrequencyUpDown";
			this.maxTrackedFrequencyUpDown.Size = new System.Drawing.Size(80, 20);
			this.maxTrackedFrequencyUpDown.TabIndex = 29;
			this.maxTrackedFrequencyUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
			// 
			// peakTrackingStartFraction
			// 
			this.peakTrackingStartFraction.DecimalPlaces = 2;
			this.peakTrackingStartFraction.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.peakTrackingStartFraction.Location = new System.Drawing.Point(1417, 450);
			this.peakTrackingStartFraction.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.peakTrackingStartFraction.Name = "peakTrackingStartFraction";
			this.peakTrackingStartFraction.Size = new System.Drawing.Size(80, 20);
			this.peakTrackingStartFraction.TabIndex = 32;
			this.peakTrackingStartFraction.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(1262, 452);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(145, 13);
			this.label1.TabIndex = 33;
			this.label1.Text = "Peak Tracking Start Frame %";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(1262, 478);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(101, 13);
			this.label10.TabIndex = 35;
			this.label10.Text = "Inactive Frame Limit";
			// 
			// inactiveFrameTrackingLimitUpDown
			// 
			this.inactiveFrameTrackingLimitUpDown.Location = new System.Drawing.Point(1417, 476);
			this.inactiveFrameTrackingLimitUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.inactiveFrameTrackingLimitUpDown.Name = "inactiveFrameTrackingLimitUpDown";
			this.inactiveFrameTrackingLimitUpDown.Size = new System.Drawing.Size(80, 20);
			this.inactiveFrameTrackingLimitUpDown.TabIndex = 34;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(1262, 504);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(148, 13);
			this.label11.TabIndex = 37;
			this.label11.Text = "Inactive Frame Playback Limit";
			// 
			// inactiveFramePlaybackLimitUpDown
			// 
			this.inactiveFramePlaybackLimitUpDown.Location = new System.Drawing.Point(1417, 502);
			this.inactiveFramePlaybackLimitUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.inactiveFramePlaybackLimitUpDown.Name = "inactiveFramePlaybackLimitUpDown";
			this.inactiveFramePlaybackLimitUpDown.Size = new System.Drawing.Size(80, 20);
			this.inactiveFramePlaybackLimitUpDown.TabIndex = 36;
			// 
			// soundTypeTab
			// 
			this.soundTypeTab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.soundTypeTab.Controls.Add(this.tabPage1);
			this.soundTypeTab.Controls.Add(this.tabPage2);
			this.soundTypeTab.Controls.Add(this.tabPage3);
			this.soundTypeTab.Location = new System.Drawing.Point(12, 25);
			this.soundTypeTab.Name = "soundTypeTab";
			this.soundTypeTab.SelectedIndex = 0;
			this.soundTypeTab.Size = new System.Drawing.Size(537, 854);
			this.soundTypeTab.TabIndex = 43;
			this.soundTypeTab.TabIndexChanged += new System.EventHandler(this.soundTypeTab_SelectedIndexChanged);
			// 
			// tabPage1
			// 
			this.tabPage1.BackColor = System.Drawing.Color.Transparent;
			this.tabPage1.Controls.Add(this.inputSpectrogram);
			this.tabPage1.Controls.Add(this.inputWaveView);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(529, 828);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Input";
			// 
			// inputSpectrogram
			// 
			this.inputSpectrogram.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.inputSpectrogram.Location = new System.Drawing.Point(6, 267);
			this.inputSpectrogram.Name = "inputSpectrogram";
			this.inputSpectrogram.Size = new System.Drawing.Size(517, 558);
			this.inputSpectrogram.TabIndex = 3;
			this.inputSpectrogram.TabStop = false;
			this.inputSpectrogram.Paint += new System.Windows.Forms.PaintEventHandler(this.inputSpectrogram_Paint);
			// 
			// inputWaveView
			// 
			this.inputWaveView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.inputWaveView.Location = new System.Drawing.Point(6, 6);
			this.inputWaveView.Name = "inputWaveView";
			this.inputWaveView.Size = new System.Drawing.Size(517, 255);
			this.inputWaveView.TabIndex = 2;
			this.inputWaveView.TabStop = false;
			this.inputWaveView.Paint += new System.Windows.Forms.PaintEventHandler(this.inputWaveView_Paint);
			// 
			// tabPage2
			// 
			this.tabPage2.BackColor = System.Drawing.Color.Transparent;
			this.tabPage2.Controls.Add(this.residualSpectrogram);
			this.tabPage2.Controls.Add(this.residualWaveView);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(529, 828);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Residual";
			// 
			// residualSpectrogram
			// 
			this.residualSpectrogram.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.residualSpectrogram.Location = new System.Drawing.Point(6, 267);
			this.residualSpectrogram.Name = "residualSpectrogram";
			this.residualSpectrogram.Size = new System.Drawing.Size(517, 558);
			this.residualSpectrogram.TabIndex = 4;
			this.residualSpectrogram.TabStop = false;
			this.residualSpectrogram.Paint += new System.Windows.Forms.PaintEventHandler(this.residualSpectrogram_Paint);
			// 
			// residualWaveView
			// 
			this.residualWaveView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.residualWaveView.Location = new System.Drawing.Point(6, 6);
			this.residualWaveView.Name = "residualWaveView";
			this.residualWaveView.Size = new System.Drawing.Size(517, 255);
			this.residualWaveView.TabIndex = 3;
			this.residualWaveView.TabStop = false;
			this.residualWaveView.Paint += new System.Windows.Forms.PaintEventHandler(this.residualWaveView_Paint);
			// 
			// tabPage3
			// 
			this.tabPage3.BackColor = System.Drawing.Color.Transparent;
			this.tabPage3.Controls.Add(this.synthesisedSpectrogram);
			this.tabPage3.Controls.Add(this.synthesisedWaveView);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(529, 828);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Synthesised";
			// 
			// synthesisedSpectrogram
			// 
			this.synthesisedSpectrogram.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.synthesisedSpectrogram.Location = new System.Drawing.Point(6, 267);
			this.synthesisedSpectrogram.Name = "synthesisedSpectrogram";
			this.synthesisedSpectrogram.Size = new System.Drawing.Size(517, 558);
			this.synthesisedSpectrogram.TabIndex = 6;
			this.synthesisedSpectrogram.TabStop = false;
			this.synthesisedSpectrogram.Paint += new System.Windows.Forms.PaintEventHandler(this.synthesisedSpectrogram_Paint);
			// 
			// synthesisedWaveView
			// 
			this.synthesisedWaveView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.synthesisedWaveView.Location = new System.Drawing.Point(6, 6);
			this.synthesisedWaveView.Name = "synthesisedWaveView";
			this.synthesisedWaveView.Size = new System.Drawing.Size(517, 255);
			this.synthesisedWaveView.TabIndex = 5;
			this.synthesisedWaveView.TabStop = false;
			this.synthesisedWaveView.Paint += new System.Windows.Forms.PaintEventHandler(this.synthesisedWaveView_Paint);
			// 
			// playInputButton
			// 
			this.playInputButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.playInputButton.Enabled = false;
			this.playInputButton.Location = new System.Drawing.Point(12, 881);
			this.playInputButton.Name = "playInputButton";
			this.playInputButton.Size = new System.Drawing.Size(134, 51);
			this.playInputButton.TabIndex = 44;
			this.playInputButton.Text = "Play Input";
			this.playInputButton.UseVisualStyleBackColor = true;
			this.playInputButton.Click += new System.EventHandler(this.playInputButton_Click);
			// 
			// playResidualButton
			// 
			this.playResidualButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.playResidualButton.Enabled = false;
			this.playResidualButton.Location = new System.Drawing.Point(152, 881);
			this.playResidualButton.Name = "playResidualButton";
			this.playResidualButton.Size = new System.Drawing.Size(128, 51);
			this.playResidualButton.TabIndex = 45;
			this.playResidualButton.Text = "Play Residual";
			this.playResidualButton.UseVisualStyleBackColor = true;
			this.playResidualButton.Click += new System.EventHandler(this.playResidualButton_Click);
			// 
			// playSynthesisedData
			// 
			this.playSynthesisedData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.playSynthesisedData.Enabled = false;
			this.playSynthesisedData.Location = new System.Drawing.Point(286, 882);
			this.playSynthesisedData.Name = "playSynthesisedData";
			this.playSynthesisedData.Size = new System.Drawing.Size(129, 50);
			this.playSynthesisedData.TabIndex = 45;
			this.playSynthesisedData.Text = "Play Synthesised";
			this.playSynthesisedData.UseVisualStyleBackColor = true;
			this.playSynthesisedData.Click += new System.EventHandler(this.playSynthesisedData_Click);
			// 
			// playOutputButton
			// 
			this.playOutputButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.playOutputButton.Enabled = false;
			this.playOutputButton.Location = new System.Drawing.Point(421, 882);
			this.playOutputButton.Name = "playOutputButton";
			this.playOutputButton.Size = new System.Drawing.Size(128, 50);
			this.playOutputButton.TabIndex = 46;
			this.playOutputButton.Text = "Play Output";
			this.playOutputButton.UseVisualStyleBackColor = true;
			this.playOutputButton.Click += new System.EventHandler(this.playOutputButton_Click);
			// 
			// resizeTimer
			// 
			this.resizeTimer.Tick += new System.EventHandler(this.resizeTimer_Tick);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(1262, 323);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(68, 13);
			this.label12.TabIndex = 47;
			this.label12.Text = "Sample Rate";
			// 
			// sampleRateCombo
			// 
			this.sampleRateCombo.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.sampleRateCombo.Location = new System.Drawing.Point(1417, 320);
			this.sampleRateCombo.Maximum = new decimal(new int[] {
            96000,
            0,
            0,
            0});
			this.sampleRateCombo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
			this.sampleRateCombo.Name = "sampleRateCombo";
			this.sampleRateCombo.Size = new System.Drawing.Size(80, 20);
			this.sampleRateCombo.TabIndex = 48;
			this.sampleRateCombo.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.waveViewZoom);
			this.groupBox4.Controls.Add(this.frameSampleViewGraph);
			this.groupBox4.Location = new System.Drawing.Point(555, 368);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(701, 238);
			this.groupBox4.TabIndex = 49;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Wave View";
			// 
			// waveViewZoom
			// 
			this.waveViewZoom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.waveViewZoom.Location = new System.Drawing.Point(6, 187);
			this.waveViewZoom.Maximum = 100;
			this.waveViewZoom.Name = "waveViewZoom";
			this.waveViewZoom.Size = new System.Drawing.Size(689, 45);
			this.waveViewZoom.TabIndex = 8;
			this.waveViewZoom.TickStyle = System.Windows.Forms.TickStyle.None;
			this.waveViewZoom.Scroll += new System.EventHandler(this.waveViewZoom_Scroll);
			// 
			// frameSampleViewGraph
			// 
			this.frameSampleViewGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.frameSampleViewGraph.Location = new System.Drawing.Point(6, 19);
			this.frameSampleViewGraph.Name = "frameSampleViewGraph";
			this.frameSampleViewGraph.Size = new System.Drawing.Size(689, 162);
			this.frameSampleViewGraph.TabIndex = 7;
			this.frameSampleViewGraph.TabStop = false;
			this.frameSampleViewGraph.Click += new System.EventHandler(this.frameSampleViewGraph_Click);
			this.frameSampleViewGraph.Paint += new System.Windows.Forms.PaintEventHandler(this.frameSampleViewGraph_Paint);
			// 
			// zeroPaddingCoefficient
			// 
			this.zeroPaddingCoefficient.Location = new System.Drawing.Point(1417, 529);
			this.zeroPaddingCoefficient.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.zeroPaddingCoefficient.Name = "zeroPaddingCoefficient";
			this.zeroPaddingCoefficient.Size = new System.Drawing.Size(80, 20);
			this.zeroPaddingCoefficient.TabIndex = 50;
			this.zeroPaddingCoefficient.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(1263, 531);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(124, 13);
			this.label13.TabIndex = 51;
			this.label13.Text = "Zero Padding Coefficient";
			// 
			// logarithmicTrackingCheckbox
			// 
			this.logarithmicTrackingCheckbox.AutoSize = true;
			this.logarithmicTrackingCheckbox.Checked = true;
			this.logarithmicTrackingCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.logarithmicTrackingCheckbox.Location = new System.Drawing.Point(1265, 555);
			this.logarithmicTrackingCheckbox.Name = "logarithmicTrackingCheckbox";
			this.logarithmicTrackingCheckbox.Size = new System.Drawing.Size(153, 17);
			this.logarithmicTrackingCheckbox.TabIndex = 52;
			this.logarithmicTrackingCheckbox.Text = "Logarithmic Peak Tracking";
			this.logarithmicTrackingCheckbox.UseVisualStyleBackColor = true;
			// 
			// randomVolumeUpDown
			// 
			this.randomVolumeUpDown.DecimalPlaces = 2;
			this.randomVolumeUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.randomVolumeUpDown.Location = new System.Drawing.Point(1417, 695);
			this.randomVolumeUpDown.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.randomVolumeUpDown.Name = "randomVolumeUpDown";
			this.randomVolumeUpDown.Size = new System.Drawing.Size(80, 20);
			this.randomVolumeUpDown.TabIndex = 53;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(1262, 695);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(85, 13);
			this.label14.TabIndex = 54;
			this.label14.Text = "Random Volume";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1519, 946);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.randomVolumeUpDown);
			this.Controls.Add(this.logarithmicTrackingCheckbox);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.zeroPaddingCoefficient);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.sampleRateCombo);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.playOutputButton);
			this.Controls.Add(this.playResidualButton);
			this.Controls.Add(this.playInputButton);
			this.Controls.Add(this.playSynthesisedData);
			this.Controls.Add(this.soundTypeTab);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.inactiveFramePlaybackLimitUpDown);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.inactiveFrameTrackingLimitUpDown);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.peakTrackingStartFraction);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.maxTrackedFrequencyUpDown);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.clippingVolumeFix);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.maxFreqDifference);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.minPeakSpaceUpDown);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.hopSizeCombo);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.frameSizeCombo);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.refreshDataBtn);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.trackedSinesUpDown);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.trackedPeaksUpDown);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Form1";
			this.Text = "Spectral Analysis";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.SizeChanged += new System.EventHandler(this.WindowSizeChanged);
			((System.ComponentModel.ISupportInitialize)(this.windowingFnGraph)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fftMagnitudeGraph)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frameIndexSlider)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackedPeakGraph)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.sineSelectionScrollBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackedPeaksUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackedSinesUpDown)).EndInit();
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.minPeakSpaceUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.maxFreqDifference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.clippingVolumeFix)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.windowingFunctionBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.maxTrackedFrequencyUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.peakTrackingStartFraction)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.inactiveFrameTrackingLimitUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.inactiveFramePlaybackLimitUpDown)).EndInit();
			this.soundTypeTab.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.inputSpectrogram)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.inputWaveView)).EndInit();
			this.tabPage2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.residualSpectrogram)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.residualWaveView)).EndInit();
			this.tabPage3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.synthesisedSpectrogram)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.synthesisedWaveView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.sampleRateCombo)).EndInit();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.waveViewZoom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frameSampleViewGraph)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.zeroPaddingCoefficient)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.randomVolumeUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.BindingSource windowingFunctionBindingSource;
		private System.Windows.Forms.ComboBox windowingFnCombo;
		private System.Windows.Forms.PictureBox windowingFnGraph;
		private System.Windows.Forms.PictureBox fftMagnitudeGraph;
		private System.Windows.Forms.TrackBar frameIndexSlider;
		private System.Windows.Forms.PictureBox trackedPeakGraph;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
		private System.Windows.Forms.OpenFileDialog loadWaveFileDialog;
		private System.Windows.Forms.TextBox frameTextBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.NumericUpDown trackedPeaksUpDown;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown trackedSinesUpDown;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button refreshDataBtn;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.ComboBox frameSizeCombo;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox hopSizeCombo;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.CheckBox showAllSinesCheckbox;
		private System.Windows.Forms.TextBox selectedSineTextbox;
		private System.Windows.Forms.TrackBar sineSelectionScrollBar;
		private System.Windows.Forms.CheckBox drawVolumeCheckbox;
		private System.Windows.Forms.CheckBox drawFrequencyCheckbox;
		private System.Windows.Forms.NumericUpDown minPeakSpaceUpDown;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.NumericUpDown maxFreqDifference;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.NumericUpDown clippingVolumeFix;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.NumericUpDown maxTrackedFrequencyUpDown;
		private System.Windows.Forms.NumericUpDown peakTrackingStartFraction;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.NumericUpDown inactiveFrameTrackingLimitUpDown;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.NumericUpDown inactiveFramePlaybackLimitUpDown;
		private System.Windows.Forms.TabControl soundTypeTab;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.PictureBox inputSpectrogram;
		private System.Windows.Forms.PictureBox inputWaveView;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.PictureBox residualSpectrogram;
		private System.Windows.Forms.PictureBox residualWaveView;
		private System.Windows.Forms.PictureBox synthesisedSpectrogram;
		private System.Windows.Forms.PictureBox synthesisedWaveView;
		private System.Windows.Forms.Button playInputButton;
		private System.Windows.Forms.Button playResidualButton;
		private System.Windows.Forms.Button playSynthesisedData;
		private System.Windows.Forms.Button playOutputButton;
		private System.Windows.Forms.Timer resizeTimer;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.NumericUpDown sampleRateCombo;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.PictureBox frameSampleViewGraph;
		private System.Windows.Forms.TrackBar waveViewZoom;
		private System.Windows.Forms.NumericUpDown zeroPaddingCoefficient;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox selectedSineHzText;
		private System.Windows.Forms.CheckBox logarithmicTrackingCheckbox;
		private System.Windows.Forms.NumericUpDown randomVolumeUpDown;
		private System.Windows.Forms.Label label14;
	}
}

