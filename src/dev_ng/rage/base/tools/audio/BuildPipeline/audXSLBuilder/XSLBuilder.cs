﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Xsl;
using audAssetPostBuilderCommon;

namespace audXSLBuilder
{
    public class XSLBuilder : PostBuildBase
    {
        private const string XML_SEARCH_PATTERN = "*.xml";
        private readonly XslCompiledTransform m_xslt;
        private XDocument m_document;

        public XSLBuilder(string[] args) : base(args)
        {
            m_document = new XDocument();
            m_xslt = new XslCompiledTransform();
        }

        public override void Run()
        {
            if (InputFiles.Count > 0 && OutputFiles.Count > 0 &&
                !string.IsNullOrEmpty(XSLFile) && LoadXml())
            {
                LoadXslt();
                Transform();
            }
        }

        private void LoadXslt()
        {
            var settings = new XsltSettings {EnableDocumentFunction = true, EnableScript = true};
            m_xslt.Load(XSLFile, settings, null);
        }

        private bool LoadXml()
        {
            var path = InputFiles[0];
            if (File.Exists(path))
            {
                m_document = XDocument.Load(path);
                return true;
            }

            if (Directory.Exists(path))
            {
                var xmlFiles = Directory.GetFiles(path, XML_SEARCH_PATTERN, SearchOption.AllDirectories);
                var root = new XElement("Objects");

                if (TypeFilters.Count > 0)
                {
                    foreach (var xmlFile in xmlFiles)
                    {
                        try
                        {
                            var doc = XDocument.Load(xmlFile);
                            if (doc.Root != null && doc.Root.Name == "Objects")
                            {
                                foreach (var typeFilter in TypeFilters)
                                {
                                    foreach (var element in doc.Root.Elements(typeFilter))
                                    {
                                        var newElement = new XElement(element);
                                        root.Add(newElement);
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            var error = string.Format("XSLBuilder: Failed to process input file - {0}", xmlFile);
                            Console.Out.WriteLine(error);
                        }
                    }
                }
                else
                {
                    foreach (var xmlFile in xmlFiles)
                    {
                        try
                        {
                            var doc = XDocument.Load(xmlFile);
                            if (doc.Root != null && doc.Root.Name == "Objects")
                            {
                                foreach (var element in doc.Root.Elements())
                                {
                                    var newElement = new XElement(element);
                                    root.Add(newElement);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            var error = string.Format("XSLBuilder: Failed to process input file - {0}", xmlFile);
                            Console.Out.WriteLine(error);
                        }
                    }
                }

                m_document = new XDocument(root);
                return true;
            }
            return false;
        }

        private void Transform()
        {
            var xmlNav = m_document.CreateNavigator();
            var writer = new XmlTextWriter(OutputFiles[0], null) {Formatting = Formatting.Indented};

            try
            {
                m_xslt.Transform(xmlNav, writer);
                writer.Close();
            }
            catch (Exception)
            {
                writer.Close();
                throw;
            }
        }
    }
}