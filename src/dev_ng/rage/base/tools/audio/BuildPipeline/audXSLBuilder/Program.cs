﻿using System;

namespace audXSLBuilder
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var xslBuilder = new XSLBuilder(args);
            try
            {
                xslBuilder.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Environment.ExitCode = -1;
            }
        }
    }
}