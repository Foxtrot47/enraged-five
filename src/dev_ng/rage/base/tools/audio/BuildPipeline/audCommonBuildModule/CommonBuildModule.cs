﻿using System;
using System.Xml;
using System.Xml.Linq;
using audAssetBuilderModuleCommon;
using rage;

namespace audCommonBuildModule
{
    using Rockstar.AssetManager.Interfaces;

    public class CommonBuildModule : BuildModule
    {
        private string m_dependsOn;
        private string m_inputFile;
        private string m_outputFileMask;
        private string m_outputPath;
        private string m_toolName;
        private string m_toolPath;

        protected override bool Initialise(XmlNode node)
        {
            foreach (XmlNode childNode in node.ChildNodes)
            {
                switch (childNode.Name)
                {
                    case "DependsOn":
                        m_dependsOn = childNode.InnerText;
                        break;
                    case "Tool":
                        m_toolPath = childNode.InnerText;
                        m_toolName = childNode.Attributes["name"].Value;
                        break;
                    case "SourceFile":
                        m_inputFile = childNode.InnerText;
                        break;
                    case "OutputDirectory":
                        m_outputPath = childNode.InnerText;
                        break;
                    case "OutputFileMasks":
                        m_outputFileMask = childNode.InnerText;
                        break;
                }
            }

            return (!String.IsNullOrEmpty(m_toolName) && !String.IsNullOrEmpty(m_toolPath) &&
                    !String.IsNullOrEmpty(m_inputFile) && !String.IsNullOrEmpty(m_outputPath));
        }

        public override bool CreateJobs(XElement toolNode,
                                        XElement projectNode,
                                        IAssetManager assetManager,
                                        IChangeList buildChangeList,
                                        audProjectSettings projectSettings,
                                        string pack,
                                        string syncChangelistNumber,
                                        string tempPath)
        {
            try
            {
                assetManager.GetLatest(m_toolPath, false);
                var localToolPath = assetManager.GetLocalPath(m_toolPath);

                var toolEl = new XElement("Tool",
                                          new XAttribute("Name", m_toolName),
                                          new XAttribute("Path", localToolPath),
                                          new XAttribute("AllowRemote", "True"),
                                          new XAttribute("GroupPrefix", String.Concat(m_toolName, "...")),
                                          new XAttribute("OutputFileMasks",
                                                         String.IsNullOrEmpty(m_outputFileMask)
                                                             ? "*.*"
                                                             : m_outputFileMask));
                toolNode.Add(toolEl);

                var taskEl = new XElement("Task",
                                          new XAttribute("Name", Name),
                                          new XAttribute("SourceFile", m_inputFile),
                                          new XAttribute("Params", String.Concat("$(SourcePath) ", m_outputPath)),
                                          new XAttribute("Tool", m_toolName));

                if (!String.IsNullOrEmpty(m_dependsOn))
                {
                    taskEl.Add(new XAttribute("DependsOn", m_dependsOn));
                }
                projectNode.Add(taskEl);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}