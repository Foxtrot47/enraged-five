﻿using System;

namespace audPeakNormalise
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var pn = new PeakNormalise(args);
                pn.Execute();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                Environment.ExitCode = -1;
            }
        }
    }
}
