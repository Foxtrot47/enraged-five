﻿using System;
using System.Collections.Generic;
using audAssetBuilderCommon;

using Wavelib;

namespace audPeakNormalise
{
    using rage.ToolLib;

    public class PeakNormalise : WaveBuildBase
    {
        private const double DESIRED_PEAK_LEVEL = -2.0;

        public PeakNormalise(string[] args)
            : base(args)
        {
        }

        struct SilentRegion
        {
           public int startPos;
           public int endPos;
        }

        public override void Run()
        {
            int dataSize = WaveFile.Data.RawData.Length;

            if (dataSize <= 0)
            {
                throw new ApplicationException("Length is less than or equal to 0. What's going on here?");
            }

            string markSilence = WaveProperties.BuiltAsset.MarkSilence;

            bool addSilenceMarkers = markSilence != null;

            float silenceThresholdVolumeDecibels = -64.0f;
            float silenceRegionLengthThresholdSeconds =  0.15f;

            const string VOLUME_THRESHOLD = "VOLUME";
            const string LENGTH_THRESHOLD = "DURATION";

            if(!string.IsNullOrEmpty(markSilence))
            {
                var silenceParams = Utility.ParseKeyValueString(markSilence);
                if (silenceParams.ContainsKey(VOLUME_THRESHOLD))
                {
                    silenceThresholdVolumeDecibels = float.Parse(silenceParams[VOLUME_THRESHOLD]);
                }
                if (silenceParams.ContainsKey(LENGTH_THRESHOLD))
                {
                    silenceRegionLengthThresholdSeconds = float.Parse(silenceParams[LENGTH_THRESHOLD]);
                }

                Console.WriteLine("Params: \"{0}\", using volume threshold: {1} and min duration: {2}", markSilence, silenceThresholdVolumeDecibels, silenceRegionLengthThresholdSeconds);
            }
            
            int maxSampleLevel = 0;
            int silenceRegionStart = -1;
            int silenceThreshold = (int)(short.MaxValue * Math.Pow(10.0, silenceThresholdVolumeDecibels / 20.0));
            int silenceRegionLengthThreshold = (int)(WaveFile.Format.SampleRate * silenceRegionLengthThresholdSeconds);

            List < SilentRegion > silentRegions = new List<SilentRegion>();            
            
            for (int i = 0; i < dataSize; i += 2)
            {
                int currentSample = BitConverter.ToInt16(WaveFile.Data.RawData, i);
                int absValue = Math.Abs(currentSample);
                maxSampleLevel = Math.Max(maxSampleLevel, absValue);

                if(addSilenceMarkers)
                {
                    int currentSampleIndex = i / 2;
    
                    if (absValue <= silenceThreshold)
                    {
                        if (silenceRegionStart == -1)
                        {
                            silenceRegionStart = currentSampleIndex;
                        }
                    }
                    else if (silenceRegionStart != -1)
                    {
                        if (currentSampleIndex - silenceRegionStart > silenceRegionLengthThreshold)
                        {
                            Console.WriteLine("Silent region from {0} to {1} - length {2} (threshold length {3}", silenceRegionStart, currentSampleIndex, currentSampleIndex - silenceRegionStart, silenceRegionLengthThreshold);
                            silentRegions.Add(new SilentRegion() { startPos = silenceRegionStart, endPos = currentSampleIndex });
                        }

                        silenceRegionStart = -1;
                    }
                }
            }

            //Calculate scaling factor
            double maxNormalisedLevel = Math.Floor(32767.0 * Math.Pow(10.0, DESIRED_PEAK_LEVEL / 20.0));
            double scalingFactor = maxNormalisedLevel / maxSampleLevel;

            //Scale audio data if it's not exactly 1.0
            if (scalingFactor != 1.0)
            {
                for (int i = 0; i < dataSize; i += 2)
                {
                    short currentSample = BitConverter.ToInt16(WaveFile.Data.RawData, i);
                    double fAdjustedSample = ((double)currentSample * scalingFactor);
                    fAdjustedSample = UseDither ? rage.ToolLib.Utility.DitherAndRoundSample16Bit(fAdjustedSample) : rage.ToolLib.Utility.RoundSample16Bit(fAdjustedSample);
                    short adjustedSample = (short)fAdjustedSample;

                    byte[] temp = BitConverter.GetBytes(adjustedSample);
                    WaveFile.Data.RawData[i] = temp[0];
                    WaveFile.Data.RawData[i + 1] = temp[1];
                }
            }

            int headRoom = (int)Math.Floor((Math.Log10(scalingFactor) * 20.0) * 100);
            WaveProperties.BuiltAsset.HeadRoom = headRoom;

            if (addSilenceMarkers)
            {
                foreach (SilentRegion r in silentRegions)
                {
                    WaveFile.Markers.Add(new bwMarker(string.Format("SILENCE:{0}", (r.endPos - r.startPos) / (float)WaveFile.Format.SampleRate), (uint)r.startPos));
                }
            }
        }
    }
}

