﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wavelib;
//using audAssetBuilderProperties;
using System.Diagnostics;

namespace audCutsceneStripper
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                CutsceneStripper cs = new CutsceneStripper(args);
                cs.Execute();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                Environment.ExitCode = -1;
            }
        }
    }
}
