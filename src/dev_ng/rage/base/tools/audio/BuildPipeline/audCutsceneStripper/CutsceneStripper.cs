﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wavelib;
using audAssetBuilderProperties;
using System.IO;
using audAssetBuilderCommon;
using System.Xml;

namespace audCutsceneStripper
{
	public class CutsceneStripper : WaveBuildBase
	{
		private readonly String ASSOC_FILE = "X:\\gta5\\tools\\bin\\audio\\pipeline\\CutsceneCameraWaveAssoc.txt";
		private readonly String TIMING_FILE = "X:\\gta5\\tools\\bin\\audio\\pipeline\\CutsceneTrimTimings.txt";
		//private readonly String LOG_FILE1 = "C:\\CutsceneStripLog1.txt";
		//private readonly String LOG_FILE2 = "C:\\CutsceneStripLog2.txt";
		//private readonly String LOG_FILE3 = "C:\\CutsceneStripLog3.txt";
		//private readonly String LOG_FILE4 = "C:\\CutsceneStripLog4.txt";

		public CutsceneStripper(string[] args)
			: base(args)
		{
		}

		private String GetCameraFileNameFromSourceAssetName(String waveName)
		{
			try
			{
				using (StreamReader sr = new StreamReader(ASSOC_FILE))
				{
					String line;
					while ((line = sr.ReadLine()) != null)
					{
						String[] items = line.Split('\t');
						if (items.Length != 2)
							throw new System.Exception("Badly formatted wav->cutxml association file.");
						if (waveName.Contains(items[1]))
						{
							Console.WriteLine("Got " + items[0] + " for " + waveName);
							return items[0];
						}
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

			return "";
		}

		private List<int> GetTimesListFromCameraFileName(String CameraFileName)
		{
			List<int> timesList = new List<int>();
			try
			{

				using (StreamReader sr = new StreamReader(TIMING_FILE))
				{
					String line;
					while ((line = sr.ReadLine()) != null)
					{
						String[] items = line.Split(',');
						if (items.Length % 2 != 1)
							throw new System.Exception("Badly formatted cutscene timing file for cutscene " + CameraFileName);
						if (items[0] == CameraFileName)
						{
							for (int i = 1; i < items.Length; ++i)
								timesList.Add(Int32.Parse(items[i]));
						}
					}
				}
			}
			catch (System.Exception e)
			{
				Console.WriteLine(e.Message);
			}
			return timesList;
		}


		public override void Run()
		{
			uint dataSize = (uint)WaveFile.Data.RawData.Length;
			uint sampleRate = WaveFile.Format.SampleRate;
			uint bitsPerSample = WaveFile.Format.SignificantBitsPerSample;
			uint srOverThirty = sampleRate / 30;


			if (dataSize <= 0)
			{
				throw new System.Exception("Length is less than or equal to 0. What's going on here?");
			}

			//System.IO.StreamWriter logFile;
			//try
			//{
			//    logFile = new System.IO.StreamWriter(LOG_FILE1, true);
			//}
			//catch (System.Exception ex)
			//{
			//    try
			//    {
			//        logFile = new System.IO.StreamWriter(LOG_FILE2, true);
			//    }
			//    catch (System.Exception ex1)
			//    {
			//        try
			//        {
			//            logFile = new System.IO.StreamWriter(LOG_FILE3, true);
			//        }
			//        catch (System.Exception ex2)
			//        {
			//            logFile = new System.IO.StreamWriter(LOG_FILE4, true);
			//        }
			//    }
			//}

			String CameraFileName = GetCameraFileNameFromSourceAssetName(WaveProperties.SourceAsset.Name);
			if (CameraFileName.CompareTo("") == 0)
			{
				//logFile.WriteLine("No root for  " + WaveProperties.SourceAsset.Name);
				//logFile.Close();
				return;
			}

			List<int> times = GetTimesListFromCameraFileName(CameraFileName);

			//uint RangeStart = 0;
			//TODO - Figure out where start strip time comes from.

			//bool IsSeq = true;
			//String dataStreamPath = "x:\\payne\\payne_art\\anim\\exportcutscene\\" + dataStreamRoot + "_concat.cutxml";
			//if (!File.Exists(dataStreamPath))
			//{
			//    dataStreamPath = "x:\\payne\\payne_art\\anim\\exportcutscene\\" + dataStreamRoot + "_seq.cutxml";
			//    if (!File.Exists(dataStreamPath))
			//    {
			//        dataStreamPath = "x:\\payne\\payne_art\\anim\\exportcutscene\\" + dataStreamRoot + ".cutxml";
			//        IsSeq = false;
			//        if (!File.Exists(dataStreamPath))
			//        {
			//            //logFile.WriteLine("No file for root " + dataStreamRoot);
			//            //logFile.Close();
			//            return;
			//        }
			//    }

			//}

			//Console.WriteLine("Loading file " + dataStreamPath);

			//try
			//{
			//    if (!IsSeq)
			//    {
			//        using (XmlTextReader toListReader = new XmlTextReader(dataStreamPath))
			//        {
			//            toListReader.ReadToFollowing("iRangeStart");
			//            if (toListReader.HasAttributes)
			//            {
			//                RangeStart = Convert.ToUInt32(toListReader.GetAttribute(0));
			//            }
			//            toListReader.Close();
			//        }
			//    }
			//    else
			//    {
			//        using (XmlTextReader toListReader = new XmlTextReader(dataStreamPath))
			//        {
			//            toListReader.ReadToFollowing("concatDataList");

			//            XmlReader concatItemReader = toListReader.ReadSubtree();
			//            if (!concatItemReader.IsEmptyElement)
			//            {
			//                concatItemReader.ReadToFollowing("Item");

			//                XmlReader startRangeReader = concatItemReader.ReadSubtree();
			//                startRangeReader.ReadToFollowing("iRangeStart");

			//                if (startRangeReader.HasAttributes)
			//                {
			//                    RangeStart = Convert.ToUInt32(startRangeReader.GetAttribute(0));
			//                }
			//            }
			//        }
			//    }
			//}
			//catch (System.Exception ex)
			//{
			//    Console.WriteLine(ex);
			//    //logFile.WriteLine(ex);
			//    //logFile.WriteLine("For " + WaveFile.FileName);
			//    //logFile.Close();
			//    return; 
			//}



			if (WaveProperties.SourceAsset.Name.ToUpper().Contains("_AUTO_EDIT.WAV"))
			{
				uint numBytesToSkip = 0;
				uint newBuffLength = Convert.ToUInt32(WaveFile.Data.RawData.Length);
				byte[] newData = new byte[newBuffLength];

				uint startNextTrimSamples = 0;
				uint startNextTrimBytes = 0;
				uint stopNextTrimSamples = newBuffLength;
				uint stopNextTrimBytes = newBuffLength;
				if (times.Count != 0)
				{
					//startNextTrimSamples = (uint)(times[0] * srOverThirty);
					//uint startNextTrimBits = (startNextTrimSamples * bitsPerSample);
					//if (startNextTrimBits % 8 != 0)
					//    throw new System.Exception("Number of bytes to skip isn't a whole number!");
					//startNextTrimBytes = startNextTrimBits / 8;
					//if (startNextTrimBytes % 2 == 1)
					//    startNextTrimBytes++;

					startNextTrimBytes = 0;

					stopNextTrimSamples = (uint)(times[1] * srOverThirty);
					uint stopNextTrimBits = (stopNextTrimSamples * bitsPerSample);
					if (stopNextTrimBits % 8 != 0)
						throw new System.Exception("Number of bytes to skip isn't a whole number!");
					stopNextTrimBytes = stopNextTrimBits / 8;
					if (stopNextTrimBytes % 2 == 1)
						stopNextTrimBytes++;
				}
				int nextTimeIndex = 2;


				uint i = 0;
				uint writeIndex = 0;
				bool exitNow = false;
				for (; !exitNow && writeIndex < newBuffLength && writeIndex + numBytesToSkip < dataSize; ++i)
				{
					if (i < startNextTrimBytes)
					{
						numBytesToSkip++;
					}
					else if (i >= stopNextTrimBytes)
					{
						numBytesToSkip++;
						if (nextTimeIndex >= times.Count)
						{
							exitNow = true;
							continue;
						}
						else
						{
							startNextTrimSamples = (uint)(times[nextTimeIndex++] * srOverThirty);
							uint startNextTrimBits = (startNextTrimSamples * bitsPerSample);
							if (startNextTrimBits % 8 != 0)
								throw new System.Exception("Number of bytes to skip isn't a whole number!");
							startNextTrimBytes = startNextTrimBits / 8;
							if (startNextTrimBytes % 2 == 1)
								startNextTrimBytes++;

							stopNextTrimSamples = (uint)(times[nextTimeIndex++] * srOverThirty);
							uint stopNextTrimBits = (stopNextTrimSamples * bitsPerSample);
							if (stopNextTrimBits % 8 != 0)
								throw new System.Exception("Number of bytes to skip isn't a whole number!");
							stopNextTrimBytes = stopNextTrimBits / 8;
							if (stopNextTrimBytes % 2 == 1)
								stopNextTrimBytes++;
						}
					}
					else
					{
						newData[writeIndex] = WaveFile.Data.RawData[writeIndex + numBytesToSkip];
						writeIndex++;
					}
				}

				uint newBuffLength2 = writeIndex - 1;
				byte[] newData2 = new byte[newBuffLength2];
				i = 0;
				for (; i < newBuffLength2; ++i)
				{
					newData2[i] = newData[i];
				}

				WaveFile.Data = new bwDataChunk(newData2, newBuffLength2, bwDataChunk.DataFormat.S16);
			}
			else if (WaveProperties.SourceAsset.Name.ToUpper().Contains("_TRIMMED.WAV"))
			{
				uint samplesToSkip = (uint)(times[0] * srOverThirty);
				uint bitsToSkip = (samplesToSkip * bitsPerSample);
				if (bitsToSkip % 8 != 0)
					throw new System.Exception("Number of bytes to skip isn't a whole number!");
				uint numBytesToSkip = bitsToSkip / 8;

				uint newBuffLength = Convert.ToUInt32(dataSize - numBytesToSkip);

				byte[] newData = new byte[newBuffLength];
				uint i = 0;
				for (; i < newBuffLength && i + numBytesToSkip < dataSize; ++i)
				{
					newData[i] = WaveFile.Data.RawData[i + numBytesToSkip];
				}

				WaveFile.Data = new bwDataChunk(newData, newBuffLength, bwDataChunk.DataFormat.S16);
			}

			//WaveFile.Data.Trim(numSamplesToSkip, dataSize);
			//Console.WriteLine("Trimming " + RangeStart + " frames from " + WaveFile.FileName);

			//logFile.Close();
		}
	}
}

