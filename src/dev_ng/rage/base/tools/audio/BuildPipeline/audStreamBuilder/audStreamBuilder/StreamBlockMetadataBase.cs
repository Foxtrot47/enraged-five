﻿using System;
using System.IO;
using audAssetBuilderCommon;

namespace audStreamBuilder
{
    public class StreamBlockMetadataBase
    {
        private readonly DataStreamBase m_stream;

        public StreamBlockMetadataBase(DataStreamBase stream)
        {
            m_stream = stream;
            StartOffsetPackets = 0;
            NumPackets = 0;
            SamplesToSkip = 0;
            NumSamples = 0;
        }

        public UInt32 StartOffsetPackets { get; set; }
        public UInt32 NumPackets { get; set; }
        public UInt32 SamplesToSkip { get; set; }
        public UInt32 NumSamples { get; set; }

        public virtual int Size
        {
            get { return 16; }
        }

        public static StreamBlockMetadataBase CreateStreamMetadata(DataStreamBase stream)
        {
            if (stream.GetType() == typeof (DataStreamPS3))
            {
                return new StreamBlockMetadataPS3(stream);
            }
            if (stream.GetType() == typeof(DataStreamMP3))
            {
                return new StreamBlockMetadataMP3(stream);
            }
            if (stream.GetType() == typeof(DataStreamATRAC9))
            {
                return new StreamBlockMetadataATRAC9(stream);
            }
            if (stream.GetType() == typeof(DataStreamADPCM))
            {
                return new StreamBlockMetadataADPCM(stream);
            }
            if (stream.GetType() == typeof(DataStreamPC)) 
            {
                return new StreamBlockMetadataPCM(stream);
            }
            if (stream.GetType() == typeof(DataStreamXMA2))
            {
                return new StreamBlockMetadataXMA2(stream);
            }
            return new StreamBlockMetadataBase(stream);
        }

        public virtual void Serialize(BinaryWriter br, string platformId)
        {
            uint identifier = 0xffffffff;
            // StartOffset is not used by runtime; for now replacing it with a known value to be used to identify new format assets (ie NumberOfFrames and NumberOfBytes included)
            br.Write(PlatformSpecific.FixEndian(identifier, platformId));
            br.Write(PlatformSpecific.FixEndian(NumPackets, platformId));
            br.Write(PlatformSpecific.FixEndian(SamplesToSkip, platformId));
            br.Write(PlatformSpecific.FixEndian(NumSamples, platformId));
        }

        public virtual void RemovePacket()
        {
            NumPackets--;
            // NOTE: this is a bit dodgy; assume the stream's next packet index has already been updated, so the 'next' packet
            // is the one we're removing.
            NumSamples -= (UInt32) (m_stream.NextEndSample() - m_stream.NextStartSample()) + 1;
        }

        public virtual void AddPacket()
        {
            if (m_stream.BlockPacketIndex == 0)
            {
                SamplesToSkip = m_stream.SamplesToSkip;
            }
            NumSamples += (uint) (m_stream.NextEndSample() - m_stream.NextStartSample() + 1);
            NumPackets++;
        }
    }
}