﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using audAssetBuilderCommon;
using rage.ToolLib.Encryption;

namespace audStreamBuilder
{
    public class LoadBlock
    {
        private int m_PacketsPerBlock;
        private List<DataStreamBase> m_DataStreams;
        private int m_NumStreams;
        private int m_NumOfPackets;
        private string m_Platform;
        private string m_CompressionType;
        private List<PacketDataList> m_Packets;
        private List<StreamBlockMetadataBase> m_StreamBlockMetadataList;
        private List<PacketSeekEntryList> m_PacketSeekTable;
        private uint m_StreamingBlockBytes;
        private int m_StreamingPacketSize;

        public uint BlockStartSample { get; private set; }
        public uint BlockSampleRate { get; private set; }

        public LoadBlock(uint streamingBlockBytes, int streamingPacketSize, 
            List<DataStreamBase> streams, string platform, string compressionType)
        {
            m_StreamingBlockBytes = streamingBlockBytes;
            m_StreamingPacketSize = streamingPacketSize;
            m_Platform = platform;
            m_CompressionType = compressionType;
            m_DataStreams = streams;
            m_NumStreams = streams.Count;            
            m_NumOfPackets = 0;

            //initialise lists
            m_Packets = new List<PacketDataList>();
            m_StreamBlockMetadataList = new List<StreamBlockMetadataBase>();
            m_PacketSeekTable = new List<PacketSeekEntryList>();

            BlockStartSample = 0;
            BlockSampleRate = 0;

            int headerSize = 0;
            for(int i = 0; i < m_DataStreams.Count; i++)
            {
                //reset Block packet index
                m_DataStreams[i].BlockPacketIndex = 0;
                //initialise packet list for current stream
                m_Packets.Add(new PacketDataList());
                //initialise packet seek entry list for current stream
                m_PacketSeekTable.Add(new PacketSeekEntryList());
                //Create stream dependant block metadata
                StreamBlockMetadataBase sm = StreamBlockMetadataBase.CreateStreamMetadata(m_DataStreams[i]);
                //get size of stream block metadata
                headerSize += sm.Size;
                m_StreamBlockMetadataList.Add(sm);
            }
                        
            m_PacketsPerBlock = (int)Math.Floor((float)(streamingBlockBytes - headerSize) / (float)(streamingPacketSize + PacketSeekEntry.Size));
            // block header needs to be aligned to the packet size
            m_PacketsPerBlock--;
        }

        public bool Build(StreamWriter debugOut)
        {
            int blockEarliestStartSamples = int.MaxValue;
            int startTimeSamples;
            bool addedPacket = false;

            //add packets trimmed from the previous block first
            for (int i = 0; i < m_NumStreams; i++)
            {
                if (m_DataStreams[i].SamplesToSkip > 0)
                {
                    //add the trimmed packets first
                    if (debugOut != null)
                    {
                        debugOut.WriteLine("LoadBlock.Build; Stream {0} has samplesToSkip {1}", i, m_DataStreams[i].SamplesToSkip);
                    }

                    AddPacket(i, debugOut);
                    addedPacket = true;
                    m_StreamBlockMetadataList[i].SamplesToSkip = m_DataStreams[i].SamplesToSkip;
                    m_DataStreams[i].SamplesToSkip = 0;
                }
                else
                {
                    // we want the earliest complete (non-skipped) packet for the block seek table

                    startTimeSamples = m_DataStreams[i].NextStartSample();
                    if (startTimeSamples < blockEarliestStartSamples)
                    {
                        //keep a note of the first sample and samplerate for the block seek table
                        blockEarliestStartSamples = startTimeSamples;
                        BlockStartSample = (uint)startTimeSamples;
                        BlockSampleRate = (uint)m_DataStreams[i].SampleRate;
                    }
                }
            }

            //add other packets
            while (m_NumOfPackets < m_PacketsPerBlock)
            {
                int earliestIndex = -1;
                int earliestStartSample = int.MaxValue;

                //for each stream find the earliest starting packet and add it to the Packets
                for (int i = 0; i < m_NumStreams; i++)
                {
                    startTimeSamples = m_DataStreams[i].NextStartSample();

                    if (startTimeSamples < 0)
                    {
                        m_DataStreams[i].IsComplete = true;
                    }                    
                    else if(startTimeSamples < earliestStartSample)
                    {
                        earliestStartSample = startTimeSamples;
                        earliestIndex = i;
                    }
                }

                if (earliestIndex < 0)
                {
                    //all done
                    break;
                }

                //adding packet to Packets
                AddPacket(earliestIndex, debugOut);
                addedPacket = true;
            }
            
            //update stream block metadata
            uint offset = 0;

            for (int i = 0; i < m_NumStreams; i++)
            {
               m_StreamBlockMetadataList[i].StartOffsetPackets = offset;
               offset += m_StreamBlockMetadataList[i].NumPackets;
            }

            //Once all packets have been added trim the last packets 
            //so all streams have the same play time for the block
            TrimPackets(debugOut);

            if (addedPacket)
            {
                // Ensure this block includes data from all streams
                for(int i = 0; i < m_NumStreams; i++)
                {
                    if (m_Packets[i].Count == 0)
                    {
                        throw new Exception(string.Format("Stream with no data: {0}", i));
                    }
                }
            }

            // did we add any data?
            return (addedPacket);
        }

      
        public void AddPacket(int streamIndex, StreamWriter debugOut)
        {        
            //add packet lookup entry
            PacketSeekEntry pse = new PacketSeekEntry();
            pse.StartSampleIndex = (uint)m_DataStreams[streamIndex].NextStartSample();
            pse.EndSampleIndex = (uint)m_DataStreams[streamIndex].NextEndSample();
            m_PacketSeekTable[streamIndex].Add(pse);

            if (debugOut != null)
            {
                debugOut.WriteLine("AddPacket: stream {0} start: {1} end: {2}", streamIndex, pse.StartSampleIndex, pse.EndSampleIndex);
            }

            if (m_Packets[streamIndex].Count == 0)
            {
                pse.StartSampleIndex += m_DataStreams[streamIndex].PacketSeekOffset;
            }

            //update stream metadata
            m_StreamBlockMetadataList[streamIndex].AddPacket();

            //add Packet Data
            m_Packets[streamIndex].AddPacket(m_DataStreams[streamIndex].NextPacket());
            m_NumOfPackets++;
        }
      
        private void TrimPackets(StreamWriter debugOut)
        {
            uint earliestEndTimeSamples = uint.MaxValue;
                       
            int earliestStreamIndex = -1;

            //get earliest time
            for (int i = 0; i < m_NumStreams; i++)
            {
                if (!m_DataStreams[i].IsComplete)
                {
                    // NOTE: this relies on all streams in a bank having the same sample rate
                    uint endTimeSamples = m_PacketSeekTable[i].Last().EndSampleIndex;

                    if (endTimeSamples < earliestEndTimeSamples)
                    {
                        earliestEndTimeSamples = endTimeSamples;
                        earliestStreamIndex = i;
                    }
                }
            }
            if (debugOut != null)
            {
                debugOut.WriteLine("TrimPackets; earliest stream: {0} ({1} samples) ", earliestStreamIndex, earliestEndTimeSamples);
            }

            //trim packets
            if (earliestStreamIndex != -1)
            {
                for (int i = 0; i < m_NumStreams; i++)
                {
                    if (i != earliestStreamIndex && m_Packets[i].Count >= 1)
                    {
                        uint extraSamples = m_PacketSeekTable[i].Last().EndSampleIndex - earliestEndTimeSamples;

                        if (extraSamples > 0)
                        {
                            if (m_DataStreams[i].IsComplete && debugOut != null)
                            {
                                debugOut.WriteLine("TrimPackets; stream {0} was complete; trimming anyway ({1} extra samples)", i, extraSamples);
                            }

                            // repeat this packet in the next block to enable seeking
                            m_DataStreams[i].NextPacketIndex--;                        

                            // SamplesToSkip stores the number of samples we need to skip into the repeated packet to get to the real block start time
                            // since this stream will start before the block start time
                            uint startSampleIndexForThisPacket = m_PacketSeekTable[i].Last().StartSampleIndex;
                            if (startSampleIndexForThisPacket > earliestEndTimeSamples + 1)
                            {
                                throw new Exception(string.Format("TrimPackets; stream {0} startSampleIndexForThisPacket {1} but earlierEndTimeSamples {2}", i, startSampleIndexForThisPacket, earliestEndTimeSamples));
                            }
                            m_DataStreams[i].SamplesToSkip = (earliestEndTimeSamples + 1 - startSampleIndexForThisPacket);
                           
                            if(debugOut != null)
                            {
                                debugOut.WriteLine("TrimPackets; stream {0} extraSamples: {1} samplesToSkip: {2}", i, extraSamples, m_DataStreams[i].SamplesToSkip);
                            }
                            
                            m_DataStreams[i].PacketSeekOffset = m_DataStreams[i].SamplesToSkip;
                            
                            //whole packet is to be removed
                            if (m_DataStreams[i].SamplesToSkip == 0)
                            {
                                if (debugOut != null)
                                {
                                    debugOut.WriteLine("TrimPackets; Removing whole packet");
                                }
                                m_StreamBlockMetadataList[i].RemovePacket();
                                m_PacketSeekTable[i].RemoveAt(m_PacketSeekTable[i].Count - 1);
                                m_Packets[i].RemovePacket(m_Packets[i].Count - 1);
                            }
                            else
                            {
                                // Reduce the size of this packet, so we can trim at runtime to free up this block for all streams at the same time
                                
                                var ps3Meta = m_StreamBlockMetadataList[i] as StreamBlockMetadataPS3;
                                if (ps3Meta != null)
                                {
                                    // Need to calculate the number of bytes to trim from this packet, since we can't work it out at runtime if the data
                                    // is in vram
                                    var stream = m_DataStreams[i] as DataStreamPS3;
                                    //Console.WriteLine("Looking for sample index {0} (currentEndByte)", m_PacketSeekTable[i].Last().EndSampleIndex + 1);

                                    uint currentEndByte = stream.GetSampleOffsetBytes(m_PacketSeekTable[i].Last().EndSampleIndex + 1);

                                    //Console.WriteLine("Looking for sample index {0} (newEndByte)", m_PacketSeekTable[i].Last().EndSampleIndex +1 - extraSamples);

                                    uint newEndByte = stream.GetSampleOffsetBytes(m_PacketSeekTable[i].Last().EndSampleIndex + 1 - extraSamples);
                                    uint numBytesToTrim = currentEndByte - newEndByte;
                                    ps3Meta.NumBytes -= numBytesToTrim;
                                    m_Packets[i].TrimLastPacket(numBytesToTrim);
                                    if (debugOut != null)
                                    {
                                        debugOut.WriteLine("TrimPackets; stream {0} Removing {1} bytes from block", i, numBytesToTrim);
                                    }
                                }


                                var mp3Meta = m_StreamBlockMetadataList[i] as StreamBlockMetadataMP3;
                                if (mp3Meta != null)
                                {
                                    // Need to calculate the number of bytes to trim from this packet, since we can't work it out at runtime if the data
                                    // is in vram
                                    var stream = m_DataStreams[i] as DataStreamMP3;
                                    Console.WriteLine("Looking for sample index {0} (currentEndByte)", m_PacketSeekTable[i].Last().EndSampleIndex + 1);

                                    uint currentEndByte = stream.GetSampleOffsetBytes(m_PacketSeekTable[i].Last().EndSampleIndex + 1);

                                    Console.WriteLine("Looking for sample index {0} (newEndByte)", m_PacketSeekTable[i].Last().EndSampleIndex +1 - extraSamples);

                                    uint newEndByte = stream.GetSampleOffsetBytes(m_PacketSeekTable[i].Last().EndSampleIndex + 1 - extraSamples);
                                    uint numBytesToTrim = currentEndByte - newEndByte;
                                    mp3Meta.NumBytes -= numBytesToTrim;
                                    m_Packets[i].TrimLastPacket(numBytesToTrim);
                                    if (debugOut != null)
                                    {
                                        debugOut.WriteLine("TrimPackets; stream {0} Removing {1} bytes from block", i, numBytesToTrim);
                                    }
                                }
                                

                                m_PacketSeekTable[i].Last().EndSampleIndex -= extraSamples;
                                m_StreamBlockMetadataList[i].NumSamples -= extraSamples;
                            }
                        }
                    }                    
                }
            }

        }


        public void Serialize(BinaryWriter bw, IEncrypter crytpo, bool doPadBlock=true)
        {
            // Serialise to an in-memory stream and encrypt before writing to disk
            var tempMs = new MemoryStream();
            var tempBw = new BinaryWriter(tempMs);

             //write out metadata
            foreach (StreamBlockMetadataBase sm in m_StreamBlockMetadataList)
            {
                sm.Serialize(tempBw, m_Platform);
            }
            //write out packet seek table
            foreach (PacketSeekEntryList psel in m_PacketSeekTable)
            {
                psel.Serialize(tempBw, m_Platform);
            }

            var currentPosition = (int)(bw.BaseStream.Position + tempBw.BaseStream.Position);
            int padding = m_StreamingPacketSize - (currentPosition % m_StreamingPacketSize);

            if(padding > 0 && padding <m_StreamingPacketSize)
            {
                tempBw.Write(new byte[padding]);
            }

            long position = tempBw.BaseStream.Position+bw.BaseStream.Position;
            //write out packets
            foreach (PacketDataList pl in m_Packets)
            {
                int packetSize = pl.ComputeSize();
                pl.Serialize(tempBw);

                // pad to 16 bytes, so a channel block always starts aligned
                // Should only be necessary for PS3 streams as XMA packets are always
                // 2k
                if (m_Platform == "PS3" || m_CompressionType == "MP3")
                {
                    while (packetSize % 16 != 0)
                    {
                        byte padValue = 0;
                        tempBw.Write(padValue);
                        packetSize++;
                    }
                }
            }

            currentPosition = (int)(bw.BaseStream.Position + tempBw.BaseStream.Position);
            long packetBytesWritten = currentPosition - position;

            padding = (int)(m_StreamingBlockBytes - (currentPosition % m_StreamingBlockBytes));

            //add padding if needed
            if(padding > 0 && padding < m_StreamingBlockBytes && doPadBlock)
            {
                tempBw.Write(new byte[padding]);
            }

            // Encrypt temp stream buffer
            tempBw.Flush();
            var bufToEncrypt = new byte[tempMs.Position];
            var sourceBuf = tempMs.GetBuffer();
            for(long l = 0; l < tempMs.Position; l++)
            {
                bufToEncrypt[l] = sourceBuf[l];
            }
            
            bw.Write(crytpo.Encrypt(bufToEncrypt));
        }
    }
}
