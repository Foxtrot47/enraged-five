﻿using System;
using System.Collections.Generic;
using System.IO;
using audAssetBuilderCommon;
using audAssetBuilderProperties;
using rage;
using rage.ToolLib.Encryption;

namespace audStreamBuilder
{
    public class StreamBuilder : BankBuildBase
    {
        public StreamBuilder(string[] args) : base(args)
        {
        }

        public override void Run()
        {
            var debugOut = EnableDebug ? new StreamWriter(@"c:\streambuilder.txt") : null;

            var projectSettings = new audProjectSettings(ProjectSettingsPath);
            projectSettings.SetCurrentPlatformByTag(Platform);

            
            var dataStreams = new List<DataStreamBase>();
            var waveProperties = new List<WaveProperties>();

            var dir = new DirectoryInfo(InputDirectory);

            if (projectSettings.FailBuildAtStage != null)
            {
                string[] tokens = projectSettings.FailBuildAtStage.Split(',');
                if (tokens[0] == "audStreamBuilder" && dir.Name == tokens[1])
                {
                    throw new Exception("Failing on command - " + projectSettings.FailBuildAtStage);
                }
            }

            var encoder = projectSettings.GetCurrentPlatform().Encoder;

            foreach (var fileInfo in dir.GetFiles())
            {
                if (String.Compare(fileInfo.Extension, ".DATA", true) == 0)
                {
                    var waveProp = new WaveProperties(fileInfo.FullName.ToUpper().Replace(".DATA", ".XML"));
                    waveProperties.Add(waveProp);

                    if (encoder == "PS3ENCODER")
                    {
                        dataStreams.Add(new DataStreamPS3(fileInfo.FullName,
                                                          waveProp,
                                                          projectSettings.GetCurrentPlatform().StreamingPacketBytes,
                                                          projectSettings.GetCurrentPlatform().PlatformTag));
                    }
                    else if (encoder == "XMAENCODER")
                    {
                        dataStreams.Add(new DataStreamXenon(fileInfo.FullName,
                                                            waveProp,
                                                            projectSettings.GetCurrentPlatform().StreamingPacketBytes,
                                                            projectSettings.GetCurrentPlatform().PlatformTag));
                    }
                    else if (encoder == "PCMENCODER")
                    {
                        if (waveProp.BuiltAsset.Compression == 1 || waveProp.BuiltAsset.CompressionType.Equals("ADPCM", StringComparison.OrdinalIgnoreCase))
                        {
                            dataStreams.Add(new DataStreamADPCM(fileInfo.FullName,
                                                                waveProp, 
                                                                projectSettings.GetCurrentPlatform().StreamingPacketBytes,
                                                                projectSettings.GetCurrentPlatform().PlatformTag));
                        }
                        else if (waveProp.BuiltAsset.CompressionType.Equals("ATRAC9", StringComparison.OrdinalIgnoreCase))
                        {
                            dataStreams.Add(new DataStreamATRAC9(fileInfo.FullName,
                                                                waveProp,
                                                                projectSettings.GetCurrentPlatform().StreamingPacketBytes));
                        }
                        else if (waveProp.BuiltAsset.CompressionType.Equals("MP3", StringComparison.OrdinalIgnoreCase))
                        {
                            dataStreams.Add(new DataStreamMP3(fileInfo.FullName,
                                                                waveProp,
                                                                projectSettings.GetCurrentPlatform().StreamingPacketBytes,
                                                                projectSettings.GetCurrentPlatform().PlatformTag));
                        }
                        else if (waveProp.BuiltAsset.CompressionType.Equals("XMA2", StringComparison.OrdinalIgnoreCase))
                        {
                            dataStreams.Add(new DataStreamXMA2(fileInfo.FullName,
                                                                waveProp,
                                                                projectSettings.GetCurrentPlatform().StreamingPacketBytes,
                                                                projectSettings.GetCurrentPlatform().PlatformTag));
                        }
                        else
                        {
                            dataStreams.Add(new DataStreamPC(fileInfo.FullName,
                                                             waveProp,
                                                             projectSettings.GetCurrentPlatform().StreamingPacketBytes));
                        }
                    }
                }
            }

            if (dataStreams.Count > 0)
            {
                uint lengthSamples = dataStreams[0].LengthInSamples;
                uint sampleRate = dataStreams[0].SampleRate;
                foreach (var s in dataStreams)
                {
                    if (lengthSamples != s.LengthInSamples)
                    {
                        throw new Exception(string.Format("Mismatched stream lengths: {0} / {1}", lengthSamples, s.LengthInSamples));
                    }
                    if (sampleRate != dataStreams[0].SampleRate)
                    {
                        throw new Exception(string.Format("Mismatched stream sample rates: {0} / {1}", sampleRate, s.SampleRate));
                    }
                }
            }

            var loadBlocks = new List<LoadBlock>();
            var seekTable = new List<uint>();

            var blocksToBuilt = true;
            var blockNumber = 0;
            while (blocksToBuilt)
            {
                var lb = new LoadBlock(StreamingBlockBytes,
                                       projectSettings.GetCurrentPlatform().StreamingPacketBytes,
                                       dataStreams,
                                       Platform,
                                       waveProperties[0].BuiltAsset.CompressionType); // assumes all waves in a stream will be of the same compression type, so use the first

                if (lb.Build(debugOut))
                {
                    loadBlocks.Add(lb);
                    if (debugOut != null)
                    {
                        debugOut.WriteLine("block {0} start sample {1}", blockNumber++, lb.BlockStartSample);
                    }
                    seekTable.Add(lb.BlockStartSample);
                }
                else
                {
                    blocksToBuilt = false;
                }
            }

            string encryptionKey = null;

            if (EncryptionKey == null)
            {
                encryptionKey = projectSettings.GetCurrentPlatform().EncryptionKey;
            }
            else
            {
                encryptionKey = EncryptionKey;
                Console.WriteLine("StreamBuilder using custom encryption key: " + encryptionKey);
            }

            //create data chunk - this is the serialized list of blocks
            IEncrypter crypto = EncrypterFactory.Create(encryptionKey);           

            using (var fs = new FileStream(String.Concat(OutputDirectory, Name, ".DATA"), FileMode.Create, FileAccess.Write))
            {
                using (var bw = new BinaryWriter(fs))
                {
                    for(int i = 0; i < loadBlocks.Count; i++)
                    {
                        bool isLastElement = (i == loadBlocks.Count - 1);
                        //Dont pad last block to save disk space
                        loadBlocks[i].Serialize(bw, crypto, !isLastElement);
                    }
                }
            }

            //create seekTable
            using (var fs = new FileStream(String.Concat(OutputDirectory, Name, ".SEEKTABLE"), FileMode.Create, FileAccess.Write))
            {
                using (var bw = new BinaryWriter(fs))
                {
                    foreach (var offset in seekTable)
                    {
                        bw.Write(PlatformSpecific.FixEndian(offset, Platform));
                    }
                }
            }

            //create format chunk
            using (var fs = new FileStream(String.Concat(OutputDirectory, Name, ".STREAMFORMAT"), FileMode.Create, FileAccess.Write))
            {
                using (var bw = new BinaryWriter(fs))
                {
                    //num of blocks in data
                    bw.Write(PlatformSpecific.FixEndian((uint)loadBlocks.Count, Platform));
                    //size of blocks
                    bw.Write(PlatformSpecific.FixEndian(StreamingBlockBytes, Platform));
                    //num of streams
                    bw.Write(PlatformSpecific.FixEndian((uint)dataStreams.Count, Platform));

                    foreach (DataStreamBase dataStream in dataStreams)
                    {
                        //namehash
                        bw.Write(PlatformSpecific.FixEndian(dataStream.NameHash, Platform));
                        //Length in samples
                        bw.Write(PlatformSpecific.FixEndian(dataStream.LengthInSamples, Platform));
                        //Headroom
                        bw.Write(PlatformSpecific.FixEndian(dataStream.HeadRoom, Platform));
                        //sample rate
                        bw.Write(PlatformSpecific.FixEndian(dataStream.SampleRate, Platform));
                        //format
                        bw.Write(dataStream.Format);
                        //reserved (Temporarily used to specify that the per channel block alignment)
                        // to allow the runtime code to maintain backwards compatibility
                        if (Platform == "PS3" || waveProperties[0].BuiltAsset.CompressionType.Equals("MP3", StringComparison.OrdinalIgnoreCase))
                        {
                            bw.Write((byte)16);
                        }
                        else
                        {
                            bw.Write((byte)0);
                        }
                        // needed for seamless looping on xbox360
                        bw.Write(dataStream.LoopBegin);
                    }
                }
            }

            foreach (var waveProp in waveProperties)
            {
                waveProp.Save(String.Concat(OutputDirectory, waveProp.BuiltAsset.Name.ToUpper().Replace(".WAV", ".XML")));
            }

            if (debugOut != null)
            {
                debugOut.Close();
            }
        }
    }
}