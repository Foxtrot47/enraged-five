﻿using System;
using System.Collections.Generic;
using System.IO;
using audAssetBuilderCommon;

namespace audStreamBuilder
{
    public class PacketSeekEntryList : List<PacketSeekEntry>
    {
        public void Serialize(BinaryWriter bw, string platformId)
        {
            foreach (var pse in this)
            {
                pse.Serialize(bw, platformId);
            }
        }
    }

    public class PacketSeekEntry
    {
        public static readonly int Size = 4;
        public UInt32 StartSampleIndex { get; set; }
        public UInt32 EndSampleIndex { get; set; }

        //we do not write out the end sample, we only have this as we need it
        //to help us trim the packets

        public void Serialize(BinaryWriter bw, string platformId)
        {
            bw.Write(PlatformSpecific.FixEndian(StartSampleIndex, platformId));
        }
    }
}