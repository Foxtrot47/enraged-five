﻿using System;
using System.IO;
using audAssetBuilderCommon;

namespace audStreamBuilder
{
    public class StreamBlockMetadataPCM : StreamBlockMetadataBase
    {
        private readonly DataStreamPC m_stream;

        public StreamBlockMetadataPCM(DataStreamBase stream) : base(stream)
        {
            m_stream = stream as DataStreamPC;
            NumFrames = 0;
            NumBytes = 0;
        }

        public UInt32 NumFrames { get; set; }
        public UInt32 NumBytes { get; set; }

        public override int Size
        {
            get { return base.Size + 8; }
        }

        public override void Serialize(BinaryWriter bw, string platformId)
        {
            base.Serialize(bw, platformId);
            bw.Write(PlatformSpecific.FixEndian(NumFrames, platformId));
            bw.Write(PlatformSpecific.FixEndian(NumBytes, platformId));
        }

        public override void RemovePacket()
        {
            base.RemovePacket();
        }

        public override void AddPacket()
        {
            base.AddPacket();
        }
    }
}