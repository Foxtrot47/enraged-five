﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using audAssetBuilderProperties;

namespace audStreamBuilder
{
    public class DataStreamADPCM : DataStreamBase
    {
        private uint m_LengthInBytes;
        private uint m_LengthInSamples;

        public DataStreamADPCM(string fileName, WaveProperties waveProperties, int streamingPacketBytes, string platform)
            : base(fileName, waveProperties, streamingPacketBytes, platform)
        {
            m_LengthInBytes = waveProperties.BuiltAsset.Size;
            m_LengthInSamples = waveProperties.BuiltAsset.NumberOfSamples;
        }

        public override int NextStartSample()
        {
            int returnVal = NextPacketIndex * (((StreamingPacketBytes - 4) * 4) / 2); // the -4 is for the 4bytes header per packet
            if (returnVal >= m_LengthInSamples)
            {
                return -1;
            }

            return returnVal;
        }

        public override int NextEndSample()
        {
            int returnVal = ((NextPacketIndex + 1) * (((StreamingPacketBytes - 4) * 4) / 2)) - 1;
            if (returnVal >= m_LengthInSamples)
            {
                // check for sub packet end sample on last packet
                if (NextStartSample() != -1)
                {
                    return (int)m_LengthInSamples;
                }
                return -1;
            }

            return returnVal;
        }

        public override float NextStartTime()
        {
            int nextStartSample = NextStartSample();
            if (nextStartSample < 0)
            {
                return -1;
            }
            return GetTimeMs((uint)nextStartSample);
        }

        public override float NextEndTime()
        {
            int nextEndSample = NextEndSample();
            if (nextEndSample < 0)
            {
                return -1;
            }
            return GetTimeMs((uint)nextEndSample);
        }
        
        public override List<byte> NextPacket()
        {
            int offset = NextPacketIndex * (int)StreamingPacketBytes;
            int numberOfBytes = (int)Math.Max(Math.Min(StreamingPacketBytes, m_LengthInBytes - offset),0);
            List<byte> packetData = Data.GetRange(offset, numberOfBytes);
            List<byte> padding = new List<byte>();
            for (int i = 0; i < StreamingPacketBytes - numberOfBytes; i++)
            {
                padding.Add(new byte());
            }
            packetData.AddRange(padding);
            NextPacketIndex++;
            BlockPacketIndex++;
            return packetData;
        }

    }
}
