﻿using System;
using System.Collections.Generic;
using System.IO;
using audAssetBuilderCommon;
using audAssetBuilderProperties;

namespace audStreamBuilder
{
    public abstract class DataStreamBase
    {
        private readonly string m_platform;

        protected DataStreamBase(string fileName,
                                 WaveProperties waveProperties,
                                 int streamingPacketBytes,
                                 string platform)
        {
            m_platform = platform;
            NameHash = waveProperties.BuiltAsset.NameHash;
            HeadRoom = (short) waveProperties.BuiltAsset.HeadRoom;
            StreamingPacketBytes = streamingPacketBytes;
            Data = new List<byte>();

            LoadDataChunk(fileName);
            LoadFormatChunk(fileName);
        }

        public uint NameHash { get; set; }

        public List<byte> Data { get; private set; }

        public byte Format { get; private set; }

        public int StreamingPacketBytes { get; private set; }

        public ushort SampleRate { get; private set; }

        public uint LengthInSamples { get; set; }

        public short HeadRoom { get; set; }

        public uint LengthInBytes { get; set; }
        public int NextPacketIndex { get; set; }
        public uint SamplesToSkip { get; set; }

        //samples to skip for the PacketSeekTable, only pc uses this
        public virtual uint PacketSeekOffset { get; set; }

        //Determines if all packet have been processed
        public bool IsComplete { get; set; }

        //packet index relative to current block
        public uint BlockPacketIndex { get; set; }

        public ushort LoopBegin { get; set; }

        private void LoadDataChunk(string fileName)
        {
            var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            var br = new BinaryReader(fs);
            var len = fs.Length;
            while (br.BaseStream.Position < len)
            {
                Data.Add(br.ReadByte());
            }

            br.Close();
            fs.Close();

            LengthInBytes = (uint) len;

            File.Delete(fileName);
        }

        private void LoadFormatChunk(string fileName)
        {
            var formatFilePath = fileName.ToUpper().Replace(".DATA", ".FORMAT");

            if (!File.Exists(formatFilePath))
            {
                throw new Exception("Unable to find format chunk");
            }

            var fs = new FileStream(formatFilePath, FileMode.Open, FileAccess.Read);
            var br = new BinaryReader(fs);

            LengthInSamples = PlatformSpecific.FixEndian(br.ReadUInt32(), m_platform);
            br.ReadInt32(); //Loop point in samples
            SampleRate = PlatformSpecific.FixEndian(br.ReadUInt16(), m_platform);
            HeadRoom = PlatformSpecific.FixEndian(br.ReadInt16(), m_platform);
            LoopBegin = br.ReadUInt16(); // LoopBegin
            br.ReadUInt16(); //LoopEnd
            br.ReadUInt16(); //PlayEnd
            br.ReadByte(); //PlayBegin
            Format = br.ReadByte();

            Console.WriteLine("{0}: length {1}, sampleRate {2}, headroom {3}, format {4}",
                              fileName,
                              LengthInSamples,
                              SampleRate,
                              HeadRoom,
                              Format);

            br.Close();
            fs.Close();

            File.Delete(formatFilePath);
        }

        public float GetTimeMs(uint sampleIndex)
        {
            return sampleIndex / (SampleRate * 0.001f);
        }

        public uint GetSamples(float timeInMS)
        {
            return (uint) Math.Round(timeInMS * SampleRate * 0.001f);
        }

        public abstract int NextStartSample();

        public abstract int NextEndSample();

        public abstract float NextStartTime();

        public abstract float NextEndTime();

        //Gets the next packet
        public abstract List<byte> NextPacket();

        //used for the StreamMetadata
    }
}