﻿using System;
using System.Diagnostics;

namespace audStreamBuilder
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var sb = new StreamBuilder(args);
                sb.Execute();
                Environment.ExitCode = 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                EventLog.WriteEntry("Audio Build", ex.ToString());
                Environment.ExitCode = -1;
            }
        }
    }
}