﻿using System;
using System.Collections.Generic;
using audAssetBuilderProperties;

namespace audStreamBuilder
{
    public class DataStreamPC : DataStreamBase
    {
        private readonly uint m_lengthInBytes;
        private readonly uint m_lengthInSamples;

        public DataStreamPC(string fileName, WaveProperties waveProperties, int streamingPacketBytes)
            : base(fileName, waveProperties, streamingPacketBytes, "PC")
        {
            m_lengthInBytes = waveProperties.BuiltAsset.Size;
            m_lengthInSamples = waveProperties.BuiltAsset.NumberOfSamples;
        }

        public override int NextStartSample()
        {
            var returnVal = NextPacketIndex * StreamingPacketBytes / 2;
            if (returnVal >= m_lengthInSamples)
            {
                return -1;
            }

            return returnVal;
        }

        public override int NextEndSample()
        {
            var returnVal = ((NextPacketIndex + 1) * StreamingPacketBytes / 2) - 1;
            if (returnVal >= m_lengthInSamples)
            {
                // check for sub packet end sample on last packet
                if (NextStartSample() !=
                    -1)
                {
                    return (int) m_lengthInSamples;
                }
                return -1;
            }

            return returnVal;
        }

        public override float NextStartTime()
        {
            var nextStartSample = NextStartSample();
            if (nextStartSample < 0)
            {
                return -1;
            }
            return GetTimeMs((uint) nextStartSample);
        }

        public override float NextEndTime()
        {
            var nextEndSample = NextEndSample();
            if (nextEndSample < 0)
            {
                return -1;
            }
            return GetTimeMs((uint) nextEndSample);
        }

        public override List<byte> NextPacket()
        {
            var offset = NextPacketIndex * StreamingPacketBytes;
            var numberOfBytes = (int) Math.Max(Math.Min(StreamingPacketBytes, m_lengthInBytes - offset), 0);
            var packetData = Data.GetRange(offset, numberOfBytes);
            var padding = new List<byte>();
            for (var i = 0; i < StreamingPacketBytes - numberOfBytes; i++)
            {
                padding.Add(new byte());
            }
            packetData.AddRange(padding);
            NextPacketIndex++;
            BlockPacketIndex++;
            return packetData;
        }
    }
}