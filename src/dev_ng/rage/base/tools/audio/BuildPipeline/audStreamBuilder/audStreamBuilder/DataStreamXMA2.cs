﻿using System;
using System.Collections.Generic;
using System.IO;
using audAssetBuilderCommon;
using audAssetBuilderProperties;

namespace audStreamBuilder
{
    public class DataStreamXMA2 : DataStreamBase
    {
        private readonly List<UInt32> m_seekTable;

        public DataStreamXMA2(string fileName, WaveProperties waveProperties, int streamingPacketBytes, string platform)
            : base(fileName, waveProperties, streamingPacketBytes, platform)
        {
            this.Platform = platform;
            LengthInBytes = waveProperties.BuiltAsset.Size;
            // Use the trimmed length in samples from the XMA encoder (loading from the format chunk)
            //LengthInSamples = waveProperties.BuiltAsset.NumberOfSamples;
            m_seekTable = new List<UInt32>();

            LoadSeekTable(fileName);
        }

        public override uint PacketSeekOffset
        {
            get { return base.PacketSeekOffset; }
            set { base.PacketSeekOffset = 0; }
        }

        public string Platform { get; private set; }

        private void LoadSeekTable(string fileName)
        {
            var seekFilePath = fileName.ToUpper().Replace(".DATA", ".SEEKTABLE");

            if (!File.Exists(seekFilePath))
            {
                throw new Exception("Unable to find seek table");
            }
            var fs = new FileStream(seekFilePath, FileMode.Open, FileAccess.Read);
            var br = new BinaryReader(fs);
            var len = fs.Length;
            m_seekTable.Add(0);
            while (br.BaseStream.Position < len)
            {
                m_seekTable.Add(PlatformSpecific.FlipEndian(br.ReadUInt32())); // the seektable is created big endian by the encoder so flip it for small endian platforms
            }
            br.Close();
            fs.Close();

            File.Delete(seekFilePath);
        }

        public override int NextStartSample()
        {
            if (NextPacketIndex == m_seekTable.Count - 1)
            {
                return -1;
            }
            return (int) PlatformSpecific.FixEndian(m_seekTable[NextPacketIndex], this.Platform);
        }

        public override int NextEndSample()
        {
            if (NextPacketIndex + 1 ==
                m_seekTable.Count)
            {
                return (int) LengthInSamples - 1;
            }
            return ((int)PlatformSpecific.FixEndian(m_seekTable[NextPacketIndex + 1], this.Platform)) - 1;
        }

        public override float NextStartTime()
        {
            var nextStartSample = NextStartSample();
            if (nextStartSample < 0)
            {
                return -1;
            }
            return GetTimeMs((uint) nextStartSample);
        }

        public override float NextEndTime()
        {
            var nextEndSample = NextEndSample();
            if (nextEndSample < 0)
            {
                return -1;
            }
            return GetTimeMs((uint) nextEndSample);
        }

        public override List<byte> NextPacket()
        {
            var offset = NextPacketIndex * StreamingPacketBytes;
            var numberOfBytes = (int) Math.Max(Math.Min(StreamingPacketBytes, LengthInBytes - offset), 0);
            var packetData = Data.GetRange(offset, numberOfBytes);
            var padding = new List<byte>();
            for (var i = 0; i < StreamingPacketBytes - numberOfBytes; i++)
            {
                padding.Add(new byte());
            }
            packetData.AddRange(padding);
            NextPacketIndex++;
            BlockPacketIndex++;
            return packetData;
        }
    }
}