﻿using System.Collections.Generic;
using System.IO;

namespace audStreamBuilder
{
    public class PacketDataList
    {
        private readonly List<List<byte>> m_packetData;

        public PacketDataList()
        {
            Count = 0;
            m_packetData = new List<List<byte>>();
        }

        public int Count { get; private set; }

        public void RemovePacket(int index)
        {
            m_packetData.RemoveAt(index);
            Count--;
        }

        public void TrimLastPacket(uint numBytesToTrim)
        {
            var byteList = m_packetData[Count-1];
            byteList.RemoveRange((int)(byteList.Count - numBytesToTrim), (int)numBytesToTrim);
        }

        public void AddPacket(IEnumerable<byte> collection)
        {
            var packet = new List<byte>(collection);
            m_packetData.Add(packet);
            Count++;
        }

        public int ComputeSize()
        {
            var size = 0;
            foreach (var packet in m_packetData)
            {
                size += packet.Count;
            }
            return size;
        }

        public void Serialize(BinaryWriter bw)
        {
            var i = 0;
            foreach (var packet in m_packetData)
            {
                foreach (var packetByte in packet)
                {
                    i++;
                    bw.Write(packetByte);
                }
            }
        }
    }
}