﻿using System;
using System.Collections.Generic;
using System.IO;
using audAssetBuilderCommon;
using audAssetBuilderProperties;

namespace audStreamBuilder
{
    public class DataStreamPS3 : DataStreamBase
    {
        private const int SAMPLES_PER_FRAME = 1152;

        private readonly List<UInt16> m_seekTable;
        private readonly int m_streamingPacketBytes;
        private int m_numFrames;

        public DataStreamPS3(string fileName, WaveProperties waveProperties, int streamingPacketBytes, string platform)
            : base(fileName, waveProperties, streamingPacketBytes, platform)
        {
            this.Platform = platform;
            m_streamingPacketBytes = streamingPacketBytes;
            m_seekTable = new List<ushort>();
            var seekFilePath = fileName.ToUpper().Replace(".DATA", ".SEEKTABLE");

            if (!File.Exists(seekFilePath))
            {
                throw new Exception("Unable to find seek table");
            }

            LoadSeekTable(fileName);
        }

        public override uint PacketSeekOffset
        {
            get { return base.PacketSeekOffset; }
            set { base.PacketSeekOffset = 0; }
        }

        public string Platform { get; private set; }

        private void LoadSeekTable(string fileName)
        {
            var seekFilePath = fileName.ToUpper().Replace(".DATA", ".SEEKTABLE");

            if (!File.Exists(seekFilePath))
            {
                throw new Exception("Unable to find seek table");
            }
            var fs = new FileStream(seekFilePath, FileMode.Open, FileAccess.Read);
            var br = new BinaryReader(fs);
            var len = fs.Length;
            while (br.BaseStream.Position < len)
            {
                m_seekTable.Add(br.ReadUInt16());
            }
            br.Close();
            fs.Close();

            File.Delete(seekFilePath);

            m_numFrames = m_seekTable.Count;
        }

        public override float NextStartTime()
        {
            var startSample = NextStartSample();
            if (startSample >= 0)
            {
                return GetTimeMs((uint) startSample);
            }
            return -1.0f;
        }

        public override float NextEndTime()
        {
            var endSample = NextEndSample();
            if (endSample > 0)
            {
                return GetTimeMs((uint) endSample);
            }
            return -1.0f;
        }

        public override List<byte> NextPacket()
        {
            var offset = GetFrameOffset(NextPacketIndex);
            var numBytes = GetPacketSizeBytes(NextPacketIndex);
            if (offset < 0 ||
                numBytes < 0)
            {
                throw new Exception("Unable to retrieve next packet");
            }

            NextPacketIndex++;
            BlockPacketIndex++;

            return Data.GetRange(offset, numBytes);
        }

        public int GetPacketSizeBytes(int packetIndex)
        {
            var sizeBytes = 0;
            var frameIndex = ComputeFrameIndexFromPacket(packetIndex);

            if (frameIndex >= 0)
            {
                uint totalBytes = 0;
                for (var i = frameIndex; i < m_numFrames; i++)
                {
                    uint numFrameBytes = PlatformSpecific.FixEndian(m_seekTable[i], this.Platform);
                    if (totalBytes + numFrameBytes > m_streamingPacketBytes)
                    {
                        break;
                    }
                    totalBytes += numFrameBytes;
                }

                sizeBytes = (int) totalBytes;
            }

            return sizeBytes;
        }

        public uint GetSampleOffsetBytes(uint sampleOffset)
        {

            /*if ((sampleOffset % SAMPLES_PER_FRAME) != 0)
            {
                throw new Exception(string.Format("Requested sample offset {0} - not multiple of mp3 frame size (1152)", sampleOffset));
            }*/
            uint totalBytes = 0;
            for (var i = 0; i < m_numFrames; i++)
            {
                if (sampleOffset == 0)
                {
                    return totalBytes;
                }

                uint numFrameBytes = PlatformSpecific.FixEndian(m_seekTable[i], this.Platform);
                totalBytes += numFrameBytes;
                sampleOffset -= SAMPLES_PER_FRAME;
            }

            return totalBytes;
        }

        public uint GetPacketSizeFrames(int packetIndex)
        {
            uint numFrames = 0;
            var frameIndex = ComputeFrameIndexFromPacket(packetIndex);

            if (frameIndex >= 0)
            {
                uint totalBytes = 0;
                for (var i = frameIndex; i < m_numFrames; i++)
                {
                    uint numFrameBytes = PlatformSpecific.FixEndian(m_seekTable[i], this.Platform);
                    if (totalBytes + numFrameBytes > m_streamingPacketBytes)
                    {
                        break;
                    }
                    totalBytes += numFrameBytes;
                    numFrames++;
                }
            }

            return numFrames;
        }

        private int GetFrameOffset(int packetIndex)
        {
            if (packetIndex == 0)
            {
                return 0;
            }

            var frameOffset = -1;
            var frameIndex = ComputeFrameIndexFromPacket(packetIndex);
            if (frameIndex >= 0)
            {
                frameOffset = 0;
                for (var i = 0; i < frameIndex; i++)
                {
                    var numFrameBytes = PlatformSpecific.FixEndian(m_seekTable[i], this.Platform);
                    frameOffset += numFrameBytes;
                }
            }

            return frameOffset;
        }

        public override int NextStartSample()
        {
            if (NextPacketIndex == 0)
            {
                return 0;
            }
            
            var frameIndex = ComputeFrameIndexFromPacket(NextPacketIndex);
            if (frameIndex >= 0)
            {
                return (frameIndex * SAMPLES_PER_FRAME);
            }
            return -1;
        }

        public override int NextEndSample()
        {
            var sampleIndex = -1;
            var frameIndex = ComputeFrameIndexFromPacket(NextPacketIndex);
            if (frameIndex >= 0)
            {
                uint totalBytes = 0;
                int i;
                for (i = frameIndex; i < m_numFrames; i++)
                {
                    var numFrameBytes = PlatformSpecific.FixEndian(m_seekTable[i], this.Platform);
                    if (totalBytes + numFrameBytes > m_streamingPacketBytes)
                    {
                        sampleIndex = (i * SAMPLES_PER_FRAME) - 1;
                        break;
                    }
                    totalBytes += numFrameBytes;
                }

                if (i == m_numFrames)
                {
                    sampleIndex = (int) LengthInSamples - 1;
                }
            }

            return sampleIndex;
        }

        private int ComputeFrameIndexFromPacket(int packetIndex)
        {
            if (packetIndex == 0)
            {
                return 0;
            }

            uint totalBytes = 0;
            uint packetCount = 0;
            var frameIndex = -1;

            for (var i = 0; i < m_numFrames; i++)
            {
                var numFrameBytes = PlatformSpecific.FixEndian(m_seekTable[i], this.Platform);

                if (totalBytes + numFrameBytes > m_streamingPacketBytes)
                {
                    //this is a new streaming packet
                    totalBytes = numFrameBytes;
                    packetCount++;
                }
                else
                {
                    //keep filling up the current streaming packet
                    totalBytes += numFrameBytes;
                }

                if (packetCount == packetIndex)
                {
                    frameIndex = i;
                    break;
                }
            }

            return frameIndex;
        }
    }
}