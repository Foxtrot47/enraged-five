﻿using System;
using System.IO;
using audAssetBuilderCommon;

namespace audStreamBuilder
{
    public class StreamBlockMetadataPS3 : StreamBlockMetadataBase
    {
        private readonly DataStreamPS3 m_stream;

        public StreamBlockMetadataPS3(DataStreamBase stream) : base(stream)
        {
            m_stream = stream as DataStreamPS3;
            NumFrames = 0;
            NumBytes = 0;
            NumFramesInLastPacket = 0;
            NumBytesInLastPacket = 0;
        }

        public UInt32 NumFrames { get; set; }
        public UInt32 NumBytes { get; set; }

        //these properties do not get serialised
        public UInt32 NumFramesInLastPacket { get; set; }
        public UInt32 NumBytesInLastPacket { get; set; }

        public override int Size
        {
            get { return base.Size + 8; }
        }

        public override void Serialize(BinaryWriter bw, string platformId)
        {
            base.Serialize(bw, platformId);
            bw.Write(PlatformSpecific.FixEndian(NumFrames, platformId));
            bw.Write(PlatformSpecific.FixEndian(NumBytes, platformId));
        }

        public override void RemovePacket()
        {
            base.RemovePacket();
            NumFrames -= NumFramesInLastPacket;
            NumBytes -= NumBytesInLastPacket;
        }

        public override void AddPacket()
        {
            base.AddPacket();
            //keep track of number of packets frames in case we trim this packet completely
            NumFramesInLastPacket = m_stream.GetPacketSizeFrames(m_stream.NextPacketIndex);
            NumBytesInLastPacket = (uint) m_stream.GetPacketSizeBytes(m_stream.NextPacketIndex);

            NumFrames += NumFramesInLastPacket;
            NumBytes += NumBytesInLastPacket;
        }
    }
}