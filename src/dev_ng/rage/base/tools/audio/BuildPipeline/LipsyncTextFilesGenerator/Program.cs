﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LipsyncTextFilesGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            string excelInputDir = "";
            string textOutputDir = "";

            if (args.Length != 4)
            {
                Console.Out.WriteLine("Please specify the input directory containing the Excel files with -excelInputDir and the output directory with -textOutputDir.");
                return;
            }

            for (var i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "-excelInputDir":
                        excelInputDir = args[++i].ToUpper();
                        break;
                    case "-textOutputDir":
                        textOutputDir = args[++i].ToUpper();
                        break;
                }
            }

            if (!Directory.Exists(excelInputDir))
            {
                Console.Out.WriteLine("Couldn't find the specified input directory ("+excelInputDir+")");
                return;
            }

            if (!Directory.Exists(textOutputDir))
            {
                Console.Out.WriteLine("Couldn't find the specified output directory ("+textOutputDir+")");
            }

            foreach (string fileName in Directory.GetFiles(excelInputDir))
            {
                string connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Extended Properties=Excel 12.0;", fileName);

                using (OleDbConnection conn = new OleDbConnection(connectionString))
                {
                    try
                    {
                        conn.Open();
                        DataTable dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,
                            new object[] {null, null, null, "TABLE"});
                        string Sheet1Name = dtSchema.Rows[0].Field<string>("TABLE_NAME");

                        using (var da = new OleDbDataAdapter(string.Format("SELECT * FROM [{0}]", Sheet1Name), conn))
                        {
                            DataTable table = new DataTable(Sheet1Name);
                            da.Fill(table);
                            if (table.Rows.Count == 0) continue;

                            using (
                                StreamWriter writer =
                                    new StreamWriter(
                                        Path.Combine(textOutputDir,
                                            Path.GetFileNameWithoutExtension(fileName)) + ".txt", false))
                            {
                                foreach (DataRow row in table.Rows)
                                {
                                    writer.WriteLine(string.Join("\t", row.ItemArray));
                                }
                            }
                        }
                        Console.WriteLine(fileName + " processed");
                    }
                    catch (OleDbException e)
                    {
                        Console.WriteLine(fileName+" omitted: not a valid Excel file");
                    }
                }
            }
        }
    }
}
