﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using audAssetBuilderProperties;

namespace WaveXMLBuilder
{
	class Program
	{
		private static XDocument m_BuiltWaves;

		static void Main(string[] args)
		{
			m_BuiltWaves = XDocument.Load(args[0]);
			DirectoryInfo buildOutputPath = new DirectoryInfo(args[1]);

			foreach (DirectoryInfo pack in buildOutputPath.GetDirectories())
			{
				Console.Out.WriteLine("Scanning {0}", pack.Name);
				FindBanks(pack);
			}

			m_BuiltWaves.Save(args[0]);
		}

		private static void FindBanks(DirectoryInfo packDir)
		{
			string packName = packDir.Name;

			foreach (FileInfo file in packDir.GetFiles())
			{
				if (file.Extension.ToUpper() == ".XML")
				{
					Console.Out.WriteLine("Found {0}", file.FullName);
					if (file.FullName != string.Empty)
					{
						BankProperties bankProperties = new BankProperties(file.FullName);

						XElement packNode = (from pack in m_BuiltWaves.Root.Descendants("Pack")
											 where pack.Attribute("name").Value == packName
											 select pack).FirstOrDefault();

						string bankName = Path.GetFileNameWithoutExtension(file.Name);

						XElement bankNode = (from bank in packNode.Descendants("Bank")
											 where bank.Attribute("name").Value == bankName
											 select bank).FirstOrDefault();

						if (bankNode == null)
						{
							continue;
						}

						Console.Out.WriteLine("Updating BuiltWaves for {0}\\{1}", packName, bankName);

						bankNode.Attributes().Remove();

						bankNode.Add(new XAttribute("name", bankName),
										new XAttribute("headerSize", bankProperties.HeaderSize),
										new XAttribute("timeStamp", bankProperties.DateTime),
										new XAttribute("builtSize", bankProperties.Size));

						if (!string.IsNullOrWhiteSpace(bankProperties.AwcMd5))
						{
							bankNode.Add(new XAttribute("awcMd5", bankProperties.AwcMd5));
						}

						//remove old chunk xml
						bankNode.Elements("Chunk").Remove();
						foreach (ChunkProperties chunk in bankProperties.Chunks)
						{
							var chunkElem = new XElement(
								"Chunk", new XAttribute("name", chunk.Name), new XAttribute("size", chunk.Size));
							bankNode.Add(chunkElem);

							foreach (var chunkMarker in chunk.ChunkMarkers)
							{
								var chunkMarkerElem = new XElement("Marker");
								chunkMarkerElem.SetAttributeValue("timeMs", chunkMarker.TimeOffsetMs);
								chunkMarkerElem.SetValue(chunkMarker.Value);
								chunkElem.Add(chunkMarkerElem);
							}
						}

						foreach (WaveProperties waveProperties in bankProperties.WaveProperties)
						{
							XElement waveNode = (from wave in bankNode.Descendants("Wave")
												 where wave.Attribute("name").Value == waveProperties.SourceAsset.Name
												 select wave).FirstOrDefault();

							waveNode.Attributes().Remove();

							waveNode.SetAttributeValue("name", waveProperties.SourceAsset.Name);

							waveNode.SetAttributeValue("nameHash", waveProperties.BuiltAsset.NameHash);
							waveNode.SetAttributeValue("sourceSize", waveProperties.SourceAsset.Size);
							if (!waveProperties.SourceAsset.Name.ToUpper().EndsWith(".MID") &&
								!waveProperties.SourceAsset.Name.ToUpper().EndsWith(".TXT"))
							{
								waveNode.SetAttributeValue("sourceSampleRate", waveProperties.SourceAsset.SampleRate);
								waveNode.SetAttributeValue("builtSampleRate", waveProperties.BuiltAsset.SampleRate);
                                double? integratedLoudness = waveProperties.BuiltAsset.IntegratedLoudness;
                                if (integratedLoudness != null)  waveNode.SetAttributeValue("integratedLoudness", integratedLoudness);
							    double? loudnessRange = waveProperties.BuiltAsset.LoudnessRange;
                                if(loudnessRange!=null) waveNode.SetAttributeValue("loudnessRange",loudnessRange);
							}
							waveNode.SetAttributeValue("builtName", waveProperties.BuiltAsset.Name);

							// interleaved streams dont have discrete built size
							if (waveProperties.BuiltAsset.HasValidSize())
							{
								waveNode.SetAttributeValue("builtSize", waveProperties.BuiltAsset.Size);
							}

							//remove old chunk xml
							waveNode.Elements("Chunk").Remove();

							foreach (ChunkProperties chunk in waveProperties.BuiltAsset.Chunks)
							{
								var chunkElem = new XElement(
									"Chunk", new XAttribute("name", chunk.Name), new XAttribute("size", chunk.Size));
								waveNode.Add(chunkElem);

								foreach (var chunkMarker in chunk.ChunkMarkers)
								{
									var chunkMarkerElem = new XElement("Marker");
									chunkMarkerElem.SetAttributeValue("timeMs", chunkMarker.TimeOffsetMs);
									chunkMarkerElem.SetValue(chunkMarker.Value);
									chunkElem.Add(chunkMarkerElem);
								}
							}


							// File.Delete(properties);
						}
					}
				}
			}
		}


	}
}
