﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using audAssetBuilderModuleCommon;
using rage;
using rage.ToolLib;

namespace audPostBuildModule
{
    using Rockstar.AssetManager.Interfaces;

    public class PostBuildModule : BuildModule
    {
        private IAssetManager m_assetMgr;
        private bool m_allowRemote;
        private IChangeList m_changeList;
        private string m_exePath;
        private string m_fileMasks;
        private List<BuildFile> m_inputFiles;
        private XElement m_jobNode;
        private List<BuildFile> m_outputFiles;
        private string m_params;
        private audProjectSettings m_projectSettings;
        private XElement m_toolsNode;
        private List<string> m_typeFilters;
        private string m_xslFile;

        protected override bool Initialise(XmlNode node)
        {
            m_typeFilters = new List<string>();
            m_inputFiles = new List<BuildFile>();
            m_outputFiles = new List<BuildFile>();
            m_allowRemote = true;
            m_fileMasks = "*.*";

            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "InputFile":
                        m_inputFiles.Add(new BuildFile(child));
                        break;
                    case "OutputFile":
                        m_outputFiles.Add(new BuildFile(child));
                        break;
                    case "XSLT":
                        m_xslFile = child.InnerText;
                        break;
                    case "TypeFilter":
                        m_typeFilters.Add(child.InnerText);
                        break;
                    case "ExePath":
                        m_exePath = child.InnerText;
                        break;
                    case "FileMasks":
                        m_fileMasks = child.InnerText;
                        break;
                    case "AllowRemote":
                        m_allowRemote = child.InnerText == "yes" ? true : false;
                        break;
                    case "Params":
                        m_params = child.InnerText;
                        break;
                }
            }
            return true;
        }

        public override bool CreateJobs(XElement toolNode,
                                        XElement jobNode,
                                        IAssetManager assetManager,
                                        IChangeList buildChangeList,
                                        audProjectSettings projectSettings,
                                        string pack,
                                        string syncChangelistNumber,
                                        string tempPath)
        {
            m_toolsNode = toolNode;
            m_jobNode = jobNode;
            m_assetMgr = assetManager;
            m_projectSettings = projectSettings;
            m_changeList = buildChangeList;

            CreateTools();
            CreateJobs();

            return true;
        }

        private void CreateTools()
        {
            var tool = new XElement("Tool",
                                    new XAttribute("Name", Name),
                                    new XAttribute("Path", m_assetMgr.GetLocalPath(m_exePath)),
                                    new XAttribute("AllowRemote", m_allowRemote),
                                    new XAttribute("GroupPrefix", String.Concat(Name, "...")),
                                    new XAttribute("OutputFileMasks", m_fileMasks));
            m_toolsNode.Add(tool);
        }

        private void CreateJobs()
        {
            FixPathsAndCheckout(m_inputFiles);
            FixPathsAndCheckout(m_outputFiles);

            //create params
            var parameterBuilder = new StringBuilder();
            foreach (var buildFile in m_inputFiles)
            {
                parameterBuilder.Append("-inputFile");
                parameterBuilder.Append(" ");
                //add quotes to source path
                parameterBuilder.Append("\"");
                parameterBuilder.Append(buildFile.File);
                parameterBuilder.Append("\"");
                parameterBuilder.Append(" ");
            }

            foreach (var buildFile in m_outputFiles)
            {
                parameterBuilder.Append("-outputFile");
                parameterBuilder.Append(" ");
                //add quotes to source path
                parameterBuilder.Append("\"");
                parameterBuilder.Append(buildFile.File);
                parameterBuilder.Append("\"");
                parameterBuilder.Append(" ");
            }

            if (m_xslFile != null)
            {
                var resolvedDepotPath = m_projectSettings.ResolvePath(m_xslFile);
                var localPath = m_assetMgr.GetLocalPath(resolvedDepotPath);
                m_assetMgr.GetLatest(resolvedDepotPath, false);

                parameterBuilder.Append("-xsl \"");
                parameterBuilder.Append(localPath);
                parameterBuilder.Append("\" ");
            }

            foreach (var typeFilter in m_typeFilters)
            {
                parameterBuilder.Append("-typeFilter ");
                parameterBuilder.Append(typeFilter);
                parameterBuilder.Append(" ");
            }

            parameterBuilder.Append("-platform ");
            parameterBuilder.Append(m_projectSettings.GetCurrentPlatform().PlatformTag);
            parameterBuilder.Append(" ");
            parameterBuilder.Append("-projectSettings ");
            parameterBuilder.Append(m_projectSettings.Path);
            parameterBuilder.Append(" ");
            parameterBuilder.Append(m_params);

            var task = new XElement("Task",
                                    new XAttribute("Name", Name),
                                    String.IsNullOrEmpty(DependsOn) ? null : new XAttribute("DependsOn", DependsOn),
                                    new XAttribute("Params", parameterBuilder.ToString()),
                                    new XAttribute("Tool", Name),
                                    new XAttribute("StopOnErrors", "true"),
                                    new XAttribute("Caption", Name));
            m_jobNode.Add(task);
        }

        private void FixPathsAndCheckout(IEnumerable<BuildFile> buildFiles)
        {
            foreach (var buildFile in buildFiles)
            {
                buildFile.File = m_assetMgr.GetLocalPath(m_projectSettings.ResolvePath(buildFile.File));
                if (m_assetMgr.ExistsAsAsset(buildFile.File))
                {
                    try
                    {
                        m_assetMgr.GetLatest(buildFile.File, false);
                    }
                    catch
                    {
                        m_assetMgr.GetLatest(buildFile.File, true);
                    }

                    if (buildFile.Checkout && !m_assetMgr.IsCheckedOut(buildFile.File))
                    {
                        if (null == m_changeList.CheckoutAsset(buildFile.File, buildFile.Lock))
                        {
                            throw new Exception("Failed to checkout file: " + buildFile.File);
                        }
                    }
                }
                else if (buildFile.Add && !m_assetMgr.IsMarkedForAdd(buildFile.File))
                {
                    m_changeList.MarkAssetForAdd(buildFile.File);
                }
            }
        }
    }
}