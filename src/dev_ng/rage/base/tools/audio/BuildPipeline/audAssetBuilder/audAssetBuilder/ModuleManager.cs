﻿using System.Collections.Generic;
using System.Xml;
using System;
using System.Reflection;

using audAssetBuilderModuleCommon;

namespace audAssetBuilder
{
    using Rockstar.AssetManager.Interfaces;

    public class ModuleManager
    {
        public List<BuildModule> Modules { get; private set; }
        private readonly List<string> m_moduleNames;

        public ModuleManager()
        {
            Modules = new List<BuildModule>();            
            m_moduleNames = new List<string>();
        }

        public void LoadModules(XmlNode[] moduleNodes, IAssetManager assetManager)
        {
            foreach(XmlNode moduleNode in moduleNodes)
            {
                string path = (moduleNode.Attributes["path"]== null)? string.Empty : moduleNode.Attributes["path"].Value;

                if (path != string.Empty && assetManager.ExistsAsAsset(path))
                {
                    Console.WriteLine("Loading module {0}", path);
                    assetManager.GetLatest(path, false);
                    string localPath = assetManager.GetLocalPath(path);
                    Console.WriteLine("Local path: {0}", localPath);
                    Assembly assembly = Assembly.LoadFile(localPath);
                    if (assembly != null)
                    {
                        foreach (Type t in assembly.GetExportedTypes())
                        {
                            if (t.BaseType == typeof(BuildModule))
                            {
                                var buildModule = (BuildModule)Activator.CreateInstance(t);
                                if (buildModule.Init(moduleNode))
                                {                                   
                                    if (!m_moduleNames.Contains(buildModule.Name))
                                    {
                                        Console.Out.WriteLine("Loaded module {0}", buildModule.Name);
                                        Modules.Add(buildModule);
                                    }
                                    else
                                    {
                                        throw new Exception(string.Format("Duplicate module name found {0} ", buildModule.Name));
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new Exception(string.Format("Unable to load assembly {0} ",localPath));
                    }
                }
                else
                {
                    throw new Exception(string.Format("Module's path attribute {0} is either missing or incorrect",path));
                }
            }
        }

        public void UnloadModules()
        {
            foreach (var module in Modules)
            {
                module.Quit();
            }
        }
    }
}
