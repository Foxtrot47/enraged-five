﻿using System;
using System.Diagnostics;
using System.IO;
using rage;

namespace audAssetBuilder
{
    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Main;

    using audAssetBuilderCommon;

    internal class Program
    {
        private static void Main(string[] args)
        {
            //Parse args
            string assetProject, assetUserName, assetPassword, projectSettingsPath, platform, changeListNo, pack, tempPath;
            var assetServer =
                assetProject =
                assetUserName = assetPassword = projectSettingsPath = platform = changeListNo = pack = tempPath = string.Empty;
            var depotRoot = "//depot";
            var additionalIncredibuildArgs = string.Empty;
            var syncChangelistNumber = string.Empty;
            var stayOpenOnError = false;
            var runSpeechBuild = false;

            for (var i = 0; i < args.Length; i++)
            {
                switch (args[i].ToUpper())
                {
                    case "-ASSETSERVER":
                        assetServer = args[++i];
                        break;
                    case "-ASSETPROJECT":
                        assetProject = args[++i];
                        break;
                    case "-ASSETUSERNAME":
                        assetUserName = args[++i];
                        break;
                    case "-ASSETPASSWORD":
                        assetPassword = args[++i];
                        break;
                    case "-PROJECTSETTINGS":
                        projectSettingsPath = args[++i];
                        break;
                    case "-PLATFORM":
                        platform = args[++i];
                        break;
                    case "-CHANGE":
                        changeListNo = args[++i];
                        break;
                    case "-DEPOTROOT":
                        depotRoot = args[++i];
                        break;
                    case "-PACK":
                        pack = args[++i].ToUpper();
                        break;
                    case "-TEMPPATH":
                        tempPath = args[++i].ToUpper();
                        break;
                    case "-ADDITIONAL_INCREDIBUILD_ARGS":
                        additionalIncredibuildArgs = args[++i];
                        Console.WriteLine("Additional Incredibuild arguments were: {0}", additionalIncredibuildArgs);
                        break;
                    case "-SYNCNUMBER":
                        syncChangelistNumber = args[++i];
                        break;
                    case "-STAY_OPEN_ON_ERROR":
                        bool.TryParse(args[++i], out stayOpenOnError);
                        break;
                    case "-RUN_SPEECH_BUILDER":
                        bool.TryParse(args[++i], out runSpeechBuild);
                        break;
                }
            }

            if (assetServer == string.Empty || assetProject == string.Empty ||
                assetUserName == string.Empty)
            {
                Console.Error.WriteLine();
                Console.Error.WriteLine("Error: Incorrect arguments passed");
                Environment.ExitCode = -1;

                if (stayOpenOnError)
                {
                    Console.Error.WriteLine("Press any key to continue...");
                    Console.Error.WriteLine();
                    Console.ReadKey();
                }
                return;
            }

            //Load Asset Manager
            try
            {
                var assetMgr = AssetManagerFactory.GetInstance(
                    AssetManagerType.Perforce, null, assetServer, assetProject, assetUserName, assetPassword, depotRoot);

                projectSettingsPath = assetMgr.GetLocalPath(projectSettingsPath);
                assetMgr.GetLatest(projectSettingsPath, false);

                if (string.IsNullOrEmpty(changeListNo))
                {
                    Console.WriteLine("Asset Builder: Creating new change list");
                }
                else
                {
                    Console.WriteLine("Asset Builder: Using change list " + changeListNo);
                }

                //create a build manager list
                var buildChangeList = string.IsNullOrEmpty(changeListNo)
                                          ? assetMgr.CreateChangeList("Build Manager")
                                          : assetMgr.GetChangeList(changeListNo);

                if (null == buildChangeList)
                {
                    Console.WriteLine("ERROR: Build change list is null");
                }

                //Load Projects Settings
                var projectSettings = new audProjectSettings(projectSettingsPath);
                projectSettings.SetCurrentPlatformByTag(platform);

                //Load Modules
                Console.WriteLine("Loading build modules...");
                var moduleManager = new ModuleManager();
                moduleManager.LoadModules(projectSettings.BuildModules, assetMgr);

                var buildFile = new XmlBuildFile();

                //Add jobs and tools for each module
                foreach (var buildModule in moduleManager.Modules)
                {
                    // Skipping the "SpeechBuilder" step from the project settings if building from Rave and there were no changes to any packs that contain "voice" tags
                    if (string.Compare(buildModule.Name, "SpeechBuilder", true) == 0 && !runSpeechBuild)
                    {
                        continue;
                    }

                    Console.WriteLine("Creating jobs for {0}", buildModule.Name);
                    buildModule.CreateJobs(buildFile.ToolsNode,
                                           buildFile.ProjectNode,
                                           assetMgr,
                                           buildChangeList,
                                           projectSettings,
                                           pack,
                                           syncChangelistNumber,
                                           tempPath);
                }

                //Save XML
                var buildXmlPath = string.IsNullOrEmpty(tempPath) ? Path.GetTempPath() : tempPath;
                buildXmlPath = Path.Combine(buildXmlPath, "AudioBuild.xml");

                buildFile.Save(buildXmlPath);
                Console.WriteLine("Invoking Incredibuild...");
                var psi = new ProcessStartInfo("xgconsole.exe")
                              {
                                  Arguments = "\"" + buildXmlPath + "\" /ShowCmd /OpenMonitor",
                                  UseShellExecute = false,
                                  CreateNoWindow = true
                              };
                if (!string.IsNullOrEmpty(additionalIncredibuildArgs))
                {
                    psi.Arguments = string.Format("{0} {1}", psi.Arguments, additionalIncredibuildArgs);
                }

                var p = new Process {StartInfo = psi};
                Console.WriteLine("{0} {1}", psi.FileName, psi.Arguments);

                p.Start();
                p.WaitForExit();

                moduleManager.UnloadModules();

                if (p.ExitCode != 0)
                {
                    Environment.ExitCode = p.ExitCode;
                    if (stayOpenOnError)
                    {
                        Console.Error.WriteLine("Press any key to continue...");
                        Console.Error.WriteLine();
                        Console.ReadKey();
                    }
                }
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine();
                Console.Error.WriteLine("Error: {0}", exception);
                if (Environment.ExitCode == 0)
                {
                    Environment.ExitCode = -1;
                }

                if (stayOpenOnError)
                {
                    Console.Error.WriteLine("Press any key to continue...");
                    Console.Error.WriteLine();
                    Console.ReadKey();
                }
            }
        }
    }
}