﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.IO;

namespace audAssetBuilderCommon
{
    [StructLayout(LayoutKind.Explicit)]
    struct FloatConverter
    {
        [FieldOffset(0)]
        public float floatNumber;
        [FieldOffset(0)]
        public UInt32 fixedNumber;
    }

    [StructLayout(LayoutKind.Explicit)]
    struct MarkerNameValue
    {
        [FieldOffset(0)]
        public float Value;
        [FieldOffset(0)]
        public UInt32 Name;
    }

    public class WaveMetadataMarker
    {
        private MarkerNameValue m_MarkerNameValue;

        public UInt32 CategoryHash { get; set; }
        public UInt32 NameHash
        {
            get{ return m_MarkerNameValue.Name; }
            set { m_MarkerNameValue.Name = value; }
        }
        public float Value
        {
            get { return m_MarkerNameValue.Value; }
            set { m_MarkerNameValue.Value = value; }
        }
        public UInt32 Data { get; set; }
        public UInt32 TimeOffset { get; set; }

        public UInt32 SortKey { get; set; }

        public WaveMetadataMarker()
        {
            m_MarkerNameValue = new MarkerNameValue();
        }
    }

    public class WaveMetadataMarkerChunk:List<WaveMetadataMarker>
    {
        private string m_Platform;

        public WaveMetadataMarkerChunk(string platform)
        {
            m_Platform = platform.ToUpper();
        }

        public void Add(string label, UInt32 timeOffset)
        {
            List<string> splitStrings = label.Split(':').ToList();
            WaveMetadataMarker wmm = new WaveMetadataMarker();

            if(splitStrings.Count >= 1)
            {
                wmm.CategoryHash = PlatformSpecific.FixEndian(Hash.Generate(splitStrings[0]), m_Platform);
            }
            if (splitStrings.Count >= 2)
            {
                float f;
                if(float.TryParse(splitStrings[1], out f))
                {
                    FloatConverter fc = new FloatConverter();
                    fc.floatNumber = f;

                    // Note: NameHash/Value is a union
                    wmm.NameHash = PlatformSpecific.FixEndian(fc.fixedNumber, m_Platform);
                }
                else
                {
                    wmm.NameHash = PlatformSpecific.FixEndian(Hash.Generate(splitStrings[1]), m_Platform);
                }
            }
            if (splitStrings.Count >= 3)
            {
                UInt32 valOut;
                if (UInt32.TryParse(splitStrings[2], out valOut))
                {
                    wmm.Data = PlatformSpecific.FixEndian(valOut, m_Platform);
                }
            }

            wmm.TimeOffset = PlatformSpecific.FixEndian(timeOffset, m_Platform);
            wmm.SortKey = timeOffset;

            this.Add(wmm);
        }

        public void Serialize(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write);
            BinaryWriter bw = new BinaryWriter(fs);

            var sortedList = from m in this
                             orderby m.SortKey
                             select m;

            foreach (WaveMetadataMarker marker in sortedList)
            {
                bw.Write(marker.CategoryHash);
                bw.Write(marker.NameHash);
                bw.Write(marker.TimeOffset);
                bw.Write(marker.Data);                
            }

            bw.Flush();
            bw.Close();
            fs.Close();
        }
    }    
}
 