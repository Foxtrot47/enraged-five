﻿using System;
using System.Text;

namespace audAssetBuilderCommon
{
    public static class Hash
    {
        public static UInt32 Generate(string str)
        {

            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }

            byte[] ut8f = Encoding.UTF8.GetBytes(str);

            uint key = 0;

            for (int i = 0; i < str.Length; i++)
            {
                byte character = ut8f[i];
                if (character >= 'A' && character <= 'Z')
                {
                    character += 'a' - 'A';
                }
                else if (character == '\\')
                {
                    character = (byte)'/';
                }

                key += character;
                key += (key << 10);
                key ^= (key >> 6);
            }

            key += (key << 3);
            key ^= (key >> 11);
            key += (key << 15);


            return key;

        }
    }
}
