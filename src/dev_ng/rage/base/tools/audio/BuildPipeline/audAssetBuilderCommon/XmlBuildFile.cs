﻿namespace audAssetBuilderCommon
{
    using System.Xml.Linq;

    public class XmlBuildFile
    {
        private XElement m_Root;
        
        public XElement ToolsNode { get; private set; }
        public XElement ProjectNode { get; private set; }

        public XmlBuildFile()
        {
            this.m_Root = new XElement("BuildSet", new XAttribute("FormatVersion","1"));
            
            XElement environements = new XElement("Environments");
            this.m_Root.Add(environements);

            XElement environment = new XElement("Environment", new XAttribute("Name", "Default"));
            environements.Add(environment);

            this.ToolsNode = new XElement("Tools");
            environment.Add(this.ToolsNode);

            this.ProjectNode = new XElement("Project",new XAttribute("Name","Rave Asset Build"), new XAttribute("Env","Default"));
            this.m_Root.Add(this.ProjectNode);
        }

        public void Save(string path)
        {
            this.m_Root.Save(path);
        }
    }
}
