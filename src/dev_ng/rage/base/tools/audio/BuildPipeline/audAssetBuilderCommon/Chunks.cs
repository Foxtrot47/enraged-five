﻿namespace audAssetBuilderCommon
{
    public static class Chunks
    {
        #region TYPE enum

        public enum TYPE
        {
            DATA,
            FORMAT,
            MARKERS,
            SEEKTABLE,
            STREAMFORMAT,
            LIPSYNC,
            LIPSYNC64,
            //CUSTOM_LIPSYNC,
            GRANULARGRAINS,
            GRANULARLOOPS,
            GRANULARPITCH,
            MID,
            PEAK,
            GESTURE,
			EDL
        }

        #endregion
    }
}