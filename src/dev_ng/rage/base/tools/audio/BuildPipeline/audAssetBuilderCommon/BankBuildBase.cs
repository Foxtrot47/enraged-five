﻿using System;

namespace audAssetBuilderCommon
{
    public abstract class BankBuildBase : BuildBase
    {
        protected BankBuildBase(string[] args) : base(args)
        {
            for (var i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "-inputDir":
                        {
                            InputDirectory = args[++i];
                            if (!InputDirectory.EndsWith(@"\"))
                            {
                                InputDirectory = String.Concat(InputDirectory, @"\");
                            }
                            break;
                        }
                    case "-name":
                        {
                            Name = args[++i];
                            break;
                        }
                    case "-waveSlotSettings":
                        {
                            WaveSlotSettingsPath = args[++i];
                            break;
                        }
                    case "-bankFolderName":
                        {
                            BankFolderName = args[++i];
                            break;
                        }
                    case "-bankPackingType":
                        {
                            BankPackingType = args[++i];
                            break;
                        }
                    case "-encryptionKey":
                        {
                            EncryptionKey = args[++i];
                            break;
                        }
                }
            }
        }

        public string InputDirectory { get; private set; }
        public string WaveSlotSettingsPath { get; private set; }
        public string Name { get; private set; }
        public string BankFolderName { get; private set; }
        public string BankPackingType { get; private set; }
        public string EncryptionKey { get; private set; }
    }
}