﻿using System;
using System.IO;

namespace audAssetBuilderCommon
{
    public enum audStreamFormat
    {
        // All platforms
        AUD_FORMAT_PCM_S16_LITTLE = 0,
        AUD_FORMAT_PCM_S16_BIG,
        AUD_FORMAT_PCM_F32_LITTLE,
        AUD_FORMAT_PCM_F32_BIG,
        AUD_FORMAT_ADPCM,
        // Xenon only
        AUD_FORMAT_XMA2,
        AUD_FORMAT_XWMA,
        // PS3/PC
        AUD_FORMAT_MP3,
        // PC only
        AUD_FORMAT_OGG,
        AUD_FORMAT_AAC,
        AUD_FORMAT_WMA,
        // ORBIS only
        AUD_FORMAT_ATRAC9,
        //to permit range checking
        AUD_NUMFORMATS,
    }

    public class WaveFormatChunk
    {
        /// <summary>
        /// Length in samples
        /// </summary>
        public UInt32 LengthSamples { get; set; }
        /// <summary>
        /// Loop point in samples
        /// </summary>
        public Int32 LoopPointSamples { get; set; }
        /// <summary>
        /// Sample rate
        /// </summary>
        public UInt16 SampleRate { get; set; }
        /// <summary>
        /// Headroom
        /// </summary>
        public UInt16 Headroom { get; set; }
        /// <summary>
        /// Loop start bits, XMA only
        /// </summary>
        public UInt16 LoopBegin { get; set; }
        /// <summary>
        /// Loop end bits, XMA only
        /// </summary>
        public UInt16 LoopEnd{ get; set; }
        /// <summary>
        /// Play end, XMA only
        /// </summary>
        public UInt16 PlayEnd{ get; set; }
        /// <summary>
        /// Play begin, XMA only
        /// </summary>
        public Byte PlayBegin { get; set; }
        /// <summary>
        /// Format as specified in audiohardware/decoder.h
        /// </summary>
        public Byte Format { get; set; }

        /// <summary>
        /// Peak sample value [0,65535] for the first sample block
        /// </summary>
        public UInt16 FirstPeakSample { get; set; }

        /// <summary>
        /// Set whether we should mark padding with a value to indicate custom lipsync (for in-game debugging)
        /// </summary>
        public bool HasCustomLipsync { get; set; }

        /// <summary>
        /// Write WaveFormatChunk to specified path
        /// </summary>
        /// <param name="path"></param>
        public void Serialize(string path)
        {
           FileStream fs = new FileStream(path,FileMode.Create,FileAccess.Write);
           BinaryWriter bw = new BinaryWriter(fs);

           bw.Write(LengthSamples);
           bw.Write(LoopPointSamples);
           bw.Write(SampleRate);          
           bw.Write(Headroom);
        
           //XMA
           
           bw.Write(LoopBegin);
           bw.Write(LoopEnd);
           bw.Write(PlayEnd);
           bw.Write(PlayBegin);

           bw.Write(Format);

           bw.Write(FirstPeakSample);

           // Pad to dword alignment
           bw.Write((ushort)(HasCustomLipsync ? 1 : 0));

           bw.Flush();
           bw.Close();
           fs.Close();
        }
    }
}
