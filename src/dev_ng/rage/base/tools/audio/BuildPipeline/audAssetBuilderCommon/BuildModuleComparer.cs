﻿using System.Collections.Generic;

namespace audAssetBuilderCommon
{
    public class BuildModuleComparer: IComparer<BuildModule>
    {
        #region IComparer<BuildModule> Members

        public int Compare(BuildModule x, BuildModule y)
        {
            if (x.BuildOrder == y.BuildOrder) { return 0; }
            else if (x.BuildOrder > y.BuildOrder) { return 1; }
            else { return -1; }
        }

        #endregion
    }
}
