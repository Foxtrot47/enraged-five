﻿using System;
//using rage;

namespace audAssetBuilderCommon
{
    public abstract class BuildBase
    {
        protected BuildBase(string[] args)
        {
            //audEngineUtilWrapper.InitClass();

            EnableDebug = false;
            UseDither = false;

            StreamingBlockBytes = 0;

            for (var i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "-outputDir":
                        {
                            OutputDirectory = args[++i];
                            if (!OutputDirectory.EndsWith(@"\"))
                            {
                                OutputDirectory = String.Concat(OutputDirectory, @"\");
                            }
                            break;
                        }
                    case "-platform":
                        {
                            var p = args[++i].ToUpper();
                            if (p.Contains("X360") ||
                                p.Contains("XENON"))
                            {
                                Platform = "XBOX360";
                            }
                            else
                            {
                                Platform = p;
                            }
                            break;
                        }
                    case "-projectSettings":
                        {
                            ProjectSettingsPath = args[++i];
                            break;
                        }
                    case "-useDither":
                        {
                            UseDither = true;
                            break;
                        }
                    case "-debug":
                        {
                            EnableDebug = true;
                            break;
                        }
                    case "-streamingBlockBytes":
                        {
                            StreamingBlockBytes = UInt32.Parse(args[++i]);
                            break;
                        }
                    case "-compressionType":
                        {
                            var p = args[++i].ToUpper();
                            CompressionType = p;
                            break;
                        }

                }
            }
        }

        public bool EnableDebug { get; private set; }
        public bool UseDither { get; private set; }
        public string OutputDirectory { get; private set; }
        public string Platform { get; private set; }
        public string ProjectSettingsPath { get; private set; }
        public uint StreamingBlockBytes { get; private set; }
        public string CompressionType { get; private set; }

        public virtual void Execute()
        {
            Run();
        }

        public abstract void Run();
    }
}