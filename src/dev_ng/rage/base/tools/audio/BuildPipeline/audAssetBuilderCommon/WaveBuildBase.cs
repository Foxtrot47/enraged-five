﻿using System.IO;
using audAssetBuilderProperties;
using Wavelib;

namespace audAssetBuilderCommon
{
	using System;

	public abstract class WaveBuildBase : BuildBase
	{
		protected WaveBuildBase(string[] args)
			: base(args)
		{
			for (var i = 0; i < args.Length; i++)
			{
				switch (args[i])
				{
					case "-inputFile":
						{
							InputFile = args[++i].ToUpper();
							if (Path.GetExtension(InputFile) == ".WAV")
							{
								WaveFile = new bwWaveFile(InputFile, false);
								WaveProperties = new WaveProperties(InputFile.Replace(".WAV", ".XML"));
							}
							else if (Path.GetExtension(InputFile) == ".MID")
							{
								IsMidi = true;
								WaveProperties = new WaveProperties(InputFile.Replace(".MID", ".XML"));
							}
							else if (Path.GetExtension(InputFile) == ".TXT")
							{
								IsText = true;
								WaveProperties = new WaveProperties(InputFile.Replace(".TXT", ".XML"));
							}
							break;
						}
				}
			}
		}

		public bwWaveFile WaveFile { get; protected set; }
		public WaveProperties WaveProperties { get; private set; }
		public string InputFile { get; private set; }
		private bool IsMidi { get; set; }
		private bool IsText { get; set; }

		public override void Execute()
		{
			if (!IsText)
			{
				if (!IsMidi)
				{
					base.Execute();
				}
				Save();
			}
		}

		private void Save()
		{
			if (IsMidi)
			{
				var srcFile = new FileInfo(InputFile);
				var destFileName = string.Concat(OutputDirectory, WaveProperties.SourceAsset.Name);
				var destFile = new FileInfo(destFileName);
				if (!destFile.Exists)
				{
					srcFile.CopyTo(destFileName);
					if (destFile.IsReadOnly)
					{
						destFile.IsReadOnly = false;
					}
				}

				try
				{
					// Test if size is set (exception thrown if it's not)
					var size = WaveProperties.SourceAsset.Size;
				}
				catch (Exception ex)
				{
					WaveProperties.SourceAsset.Size = (int)destFile.Length;
					WaveProperties.Save(
						string.Concat(
							OutputDirectory, WaveProperties.SourceAsset.Name.ToUpper().Replace(".MID", ".XML")));
				}
			}
			else
			{
				WaveFile.Save(string.Concat(OutputDirectory, WaveProperties.SourceAsset.Name));
				WaveProperties.Save(string.Concat(OutputDirectory,
												  WaveProperties.SourceAsset.Name.ToUpper().Replace(".WAV", ".XML")));
			}
		}
	}
}