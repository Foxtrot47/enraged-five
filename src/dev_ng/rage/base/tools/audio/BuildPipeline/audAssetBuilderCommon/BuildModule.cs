﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using audAssetManagement2;
using rage;

namespace audAssetBuilderCommon
{
    public abstract class BuildModule
    {

        public bool Init(System.Xml.XmlNode node)
        {
            //pull out Name
            if (node.Attributes["name"] != null && node.Attributes["name"].Value != string.Empty)
            {
                Name = node.Attributes["name"].Value;
            }
            else
            {
                return false;
            }

            //pull out build order
            if (node.Attributes["buildOrder"] != null && node.Attributes["buildOrder"].Value != string.Empty)
            {
                BuildOrder = Int32.Parse(node.Attributes["buildOrder"].Value);
            }
            else
            {
                return false;
            }

            return Initialise(node);

        }

        protected abstract bool Initialise(System.Xml.XmlNode node);

        public abstract bool CreateJobs(XElement toolNode, XElement jobNode, assetManager assetManager, audProjectSettings projectSettings);

        public string Name { get; private set; }

        public int BuildOrder { get; private set; }
    }

}
