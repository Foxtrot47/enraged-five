﻿using System;
using System.Collections.Generic;

namespace audAssetBuilderCommon
{    
    public class PlatformSpecific
    {
        private static Dictionary<string, bool> m_PlatformIsBigEndian = new Dictionary<string, bool>();

        /// <summary>
        /// static contructor to set up "is endian" dictionary
        /// </summary>
        static PlatformSpecific()
        {
            m_PlatformIsBigEndian.Add("PC", false);
            m_PlatformIsBigEndian.Add("XENON", true);
            m_PlatformIsBigEndian.Add("XBOX360", true);
            m_PlatformIsBigEndian.Add("X360", true);
            m_PlatformIsBigEndian.Add("PS3", true);
            m_PlatformIsBigEndian.Add("PS4", false);
            m_PlatformIsBigEndian.Add("Durango", false);
            m_PlatformIsBigEndian.Add("XBOXONE", false);
            m_PlatformIsBigEndian.Add("PS5", false);
            m_PlatformIsBigEndian.Add("XBOXSERIES", false);
        }

        public static bool IsBigEndian(string platform)
        {
            return m_PlatformIsBigEndian[platform];
        }

        /// <summary>
        /// Flips endianness depending on the platform
        /// </summary>
        /// <param name="value"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static UInt16 FixEndian(UInt16 value, string platform)
        {
            platform = platform.ToUpper();
            if (m_PlatformIsBigEndian[platform])
            {
                value = FlipEndian(value);
            }

            return value;
        }

        /// <summary>
        /// Flips endianness
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static UInt16 FlipEndian(UInt16 value)
        {
            Byte b1, b2;
            b1 = (Byte)(value & 255);
            b2 = (Byte)((value >> 8) & 255);
            value = (UInt16)((b1 << 8) + b2);
            return value;
        }

        /// <summary>
        /// Flips endianness depending on the platform
        /// </summary>
        /// <param name="value"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static Int16 FixEndian(Int16 value, string platform)
        {
            platform = platform.ToUpper();
            if (m_PlatformIsBigEndian[platform])
            {
                value = FlipEndian(value);
            }

            return value;
        }

        /// <summary>
        /// Flips endianness
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Int16 FlipEndian(Int16 value)
        {
            Byte b1, b2;
            b1 = (Byte)(value & 255);
            b2 = (Byte)((value >> 8) & 255);
            value = (Int16)((b1 << 8) + b2);
            return value;
        }

        /// <summary>
        /// Flips endianness depending on the platform
        /// </summary>
        /// <param name="value"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static UInt32 FixEndian(UInt32 value, string platform)
        {
            platform = platform.ToUpper();
            if (m_PlatformIsBigEndian[platform])
            {
                value = FlipEndian(value);
            }

            return value;
        }

        /// <summary>
        /// Flips endianness
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static UInt32 FlipEndian(UInt32 value)
        {
            Byte b1, b2, b3, b4;

            b1 = (Byte)(value & 255);
            b2 = (Byte)((value >> 8) & 255);
            b3 = (Byte)((value >> 16) & 255);
            b4 = (Byte)((value >> 24) & 255);
            value = (((UInt32)b1 << 24) + ((UInt32)b2 << 16)+ ((UInt32)b3 << 8) + b4);
            return value;
        }

        /// <summary>
        /// Flips endianness depending on the platform
        /// </summary>
        /// <param name="value"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static UInt64 FixEndian(UInt64 value, string platform)
        {
            platform = platform.ToUpper();
            if (m_PlatformIsBigEndian[platform])
            {
                value = FlipEndian(value);
            }

            return value;
        }

        /// <summary>
        /// Flips endianness
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static UInt64 FlipEndian(UInt64 value)
        {
            Byte b1, b2, b3, b4, b5, b6, b7, b8;

            b1 = (Byte)(value & 255);
            b2 = (Byte)((value >> 8) & 255);
            b3 = (Byte)((value >> 16) & 255);
            b4 = (Byte)((value >> 24) & 255);
            b5 = (Byte)((value >> 32) & 255);
            b6 = (Byte)((value >> 40) & 255);
            b7 = (Byte)((value >> 48) & 255);
            b8 = (Byte)((value >> 56) & 255);
            value = (((UInt64)b1 << 56) + ((UInt64)b2 << 48) + ((UInt64)b3 << 40) + ((UInt64)b4 << 32) + 
                        ((UInt64)b5 << 24) + ((UInt64)b6 << 16) + ((UInt64)b7 << 8) + b8);
            return value;
        }
        
    }
}
