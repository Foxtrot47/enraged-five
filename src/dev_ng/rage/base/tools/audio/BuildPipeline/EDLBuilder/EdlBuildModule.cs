﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using audAssetBuilderCommon;
using audAssetBuilderProperties;
using RSG.Metadata.Model.Tunables;
using RSG.Text.Model;

namespace EDLBuilder
{
    public class EdlBuildModule : BuildBase
    {
        private XDocument _psoMetaXDocument = new XDocument();

        public string InputFile { get; private set; }

        private bool IsText { get; set; }
        public List<string> WaveList { get; private set; }

        public EdlBuildModule(string[] args)
            : base(args)
        {
            for (var i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "-inputFile":
                        {
                            InputFile = args[++i].ToUpper();
                            if (Path.GetExtension(InputFile) == ".TXT")
                            {
                                IsText = true;
                                WaveProperties = new WaveProperties(InputFile.Replace(".TXT", ".XML"));
                            }
                            break;
                        }
                    case "-waveList":
                        {
                            WaveList = args[++i].Split(',').ToList();
                            break;
                        }

                    case "-converter":
                        {
                            ConverterPath = args[++i];
                            break;
                        }
                }

            }
        }
        public override void Execute()
        {
            if (IsText)
            {
                base.Execute();
            }
        }

        public override void Run()
        {
            BuildPso();
            Save();
        }

        private XDocument CreateConversationPsoDocument(TrackListings trackListings)
        {
            XElement array = new XElement("LineList");
            foreach (Track track in trackListings.Tracks)
            {
                foreach (LineRecordedData recordedData in track.RecordedData)
                {
                    TimeSpan startTime = TimeSpan.FromMilliseconds(0);
                    if (recordedData.StartDateTime != null)
                    {
                        startTime = recordedData.StartDateTime.Value.TimeOfDay;
                    }

                    TimeSpan endTime = TimeSpan.FromMilliseconds(0);
                    if (recordedData.EndDateTime != null)
                    {
                        endTime = recordedData.EndDateTime.Value.TimeOfDay;
                    }
                    if (WaveList != null && WaveList.Count > 0)
                    {

                        if (WaveList.Select(GetContext).Contains(GetContext(recordedData.ClipName)))
                        {
                            array.Add(CreateLineMetadataArrayItem(recordedData.ClipName, startTime, endTime));
                        }
                    }
                    else
                    {
                        array.Add(CreateLineMetadataArrayItem(recordedData.ClipName, startTime, endTime));
                    }

                }
            }

            return TunableStructure.CreateXDocument("naConvMetadata",
                array,
                BoolTunable.CreateXElement("IsInterruptible", false),
                BoolTunable.CreateXElement("IsRandom", false),
                BoolTunable.CreateXElement("IsPlaceholder", false),
                EnumTunable.CreateXElement("SubtitleType", "AUD_CONV_SUBTITLE_NUM"));
        }

        private string GetContext(string filename)
        {
            int varationStartIndex = Math.Max(filename.LastIndexOf('_'), 0);
            string context = filename.Substring(0, varationStartIndex);
            return context == string.Empty ? filename : context;
        }

        private XElement CreateLineMetadataArrayItem(string filename, TimeSpan startTime, TimeSpan endTime)
        {
            string context = GetContext(filename);
            string subtitle = String.Empty;
            string audibility = "AUD_AUDIBILITY_NORMAL";
            string audioType = "AUD_SPEECH_NORMAL";
            uint startTimeInMilliseconds = (uint)startTime.TotalMilliseconds;
            uint endTimeInMilliseconds = (uint)endTime.TotalMilliseconds;

            XElement soundMetadata = StructTunable.CreateXElement("SoundMetadata",
                StringTunable.CreateXElement("Context", context),
                StringTunable.CreateXElement("Subtitle", subtitle),
                StringTunable.CreateXElement("VolumeType", audioType),
                Unsigned32Tunable.CreateXElement("StartTime", startTimeInMilliseconds),
                Unsigned32Tunable.CreateXElement("EndTime", endTimeInMilliseconds));

            return ArrayTunable.CreateItemXElement(null,
                soundMetadata,
                StringTunable.CreateXElement("Audibility", audibility),
                Unsigned32Tunable.CreateXElement("SpeakerIdx", 1),
                Unsigned32Tunable.CreateXElement("ListenerIdx", 0),
                BoolTunable.CreateXElement("DucksRadio", false),
                BoolTunable.CreateXElement("DucksScore", false),
                BoolTunable.CreateXElement(
                "DoNotPauseForSpecialAbility", false));
        }


        private TrackListings GetTrackListings(string filePath)
        {
            TrackListings trackListings = new TrackListings();
            trackListings.LoadRecordingFile(filePath);
            return trackListings;
        }
        public WaveProperties WaveProperties { get; private set; }
        public string ConverterPath { get; set; }

        private void Save()
        {
            var destFileName = Path.Combine(OutputDirectory, Path.GetFileName(InputFile.ToUpper().Replace(".TXT", ".pso.meta")));
            var destFile = new FileInfo(destFileName);
            _psoMetaXDocument.Save(destFile.FullName);
            _psoMetaXDocument.Save(@"c:\" + destFile.Name);
            try
            {
                // Test if size is set (exception thrown if it's not)
                var size = WaveProperties.SourceAsset.Size;
            }
            catch (Exception ex)
            {
                WaveProperties.SourceAsset.Size = (int)destFile.Length;
                WaveProperties.Save(
                    string.Concat(
                        OutputDirectory, WaveProperties.SourceAsset.Name.ToUpper().Replace(".TXT", ".XML")));
            }
            Task task = new Task(() => Convert(destFile.FullName));
            task.Start();

            while (task.Status != TaskStatus.RanToCompletion)
            {
                Thread.Sleep(50);
            }
        }

        private void Convert(string input)
        {
            string extension = ".omt";
            switch (Platform)
            {
                case "PC":
                    extension = ".ymt";
                    break;
                case "PS4":
                    extension = ".omt";
                    break;
                case "XBOXONE":
                    extension = ".dmt";
                    break;
            }

            string output = input.Replace(".pso.meta", extension);

            ProcessStartInfo processStartInfo = new ProcessStartInfo(ConverterPath, string.Format(@" --input {0} --output {1} --proc RSG.Pipeline.Processor.Platform.Rage --rebuild --nopopups --nocontent", input, output));

            Process process = new Process { StartInfo = processStartInfo };

            process.Start();
            Console.WriteLine(process.ProcessName);
            process.WaitForExit();
            Console.WriteLine("Process complete, copying file to " + Path.ChangeExtension(output, "EDL"));
            File.Copy(output, Path.ChangeExtension(output, "EDL"));
            Console.WriteLine("Done");
        }

        private void BuildPso()
        {
            TrackListings trackListings = GetTrackListings(InputFile);
            XDocument doc = CreateConversationPsoDocument(trackListings);
            _psoMetaXDocument = doc;
        }
    }
}