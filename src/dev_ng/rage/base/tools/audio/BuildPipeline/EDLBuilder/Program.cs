﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using audAssetBuilderCommon;

namespace EDLBuilder
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			try
			{
				var encoder = new EdlBuildModule(args);
				encoder.Execute();
				Environment.ExitCode = 0;
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine(ex.ToString());
				var argsCombined = "";
				foreach (var s in args)
				{
					argsCombined += s + " ";
				}
				EventLog.WriteEntry("Audio Build", "EDLBuilder: " + argsCombined + " - " + ex);
				Environment.ExitCode = -1;
			}
		}
	}
}
