﻿using System;
using System.Collections.Generic;

namespace audSpeechBuilder
{
    public class VoiceEntry
    {
        public UInt32 NameHash { get; set; }
        public byte PainHash { get; set; }
        public UInt32 ContextOffsetBytes { get; set; }
        public List<VoiceContextEntry> Contexts { get; set; }
    }
}   
