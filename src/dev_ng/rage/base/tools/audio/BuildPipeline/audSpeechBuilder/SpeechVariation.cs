﻿namespace audSpeechBuilder
{
    public class VariationDataEntry
    {
        public byte Data { get; set; }
        public byte Number { get; set; }
    }
}
