﻿using System;

namespace audSpeechBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SpeechBuilder sb = new SpeechBuilder(args);
                sb.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Environment.ExitCode = -1;
            }
        }
    }
}
