﻿using System;
using System.Collections.Generic;

namespace audSpeechBuilder
{
    public class VoiceContextEntry
    {
        public UInt32 BankNameIndex { get; set; }
        public string BankName { get; set; }
        public Int32 VariationDataOffsetBytes { get; set; }
        
        public UInt32 NameHash { get; set; }
        public Byte ContextData { get; set; }
        public Byte NumVariations;
        public List<VariationDataEntry> VariationData { get; set; }
    }
}
