﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Xsl;
using audAssetBuilderCommon;
using audAssetPostBuilderCommon;
using rage.Compiler;
using rage.ToolLib.Writer;

namespace audSpeechBuilder
{
    public class SpeechBuilder : PostBuildBase
    {
        private readonly Dictionary<string, UInt32> m_contextLookup;
        private readonly List<VoiceEntry> m_speechEntries;
        private readonly IStringTable m_stringTable;
        private readonly Dictionary<string, UInt32> m_voiceLookup;
        private XDocument m_doc;
        private uint m_totalNumberOfContexts;
        private uint m_totalNumberVariationElements;

        public SpeechBuilder(string[] args) : base(args)
        {
            m_doc = XDocument.Load(InputFiles[0]);
            m_voiceLookup = new Dictionary<string, uint>();
            m_contextLookup = new Dictionary<string, uint>();
            m_speechEntries = new List<VoiceEntry>();

            m_totalNumberOfContexts = 0;
            m_totalNumberVariationElements = 0;

            m_stringTable = new StringTable();
        }

        public override void Run()
        {
            PerformXsl();
            if (!ErrorCheckAllNodes())
            {
                throw new Exception("Errors detected in one or more speech nodes.");
            }
            ParseNodes();
            WriteLookupTables();
            if (OutputFiles.Count >= 2)
            {
                WriteLookupFile(m_voiceLookup, OutputFiles[1]);
            }
            if (OutputFiles.Count >= 3)
            {
                WriteLookupFile(m_contextLookup, OutputFiles[2]);
            }
        }

        private void PerformXsl()
        {
            if (!string.IsNullOrEmpty(XSLFile))
            {
                var xmlTransform = new XslCompiledTransform();
                var buffer = new StringBuilder();
                var writer = new StringWriter(buffer);
                xmlTransform.Load(XSLFile);
                xmlTransform.Transform(m_doc.CreateReader(), null, writer);
                TextReader tr = new StringReader(buffer.ToString());
                m_doc = XDocument.Load(tr);
            }
        }

        private bool ErrorCheckAllNodes()
        {
            var bSuccess = true;

            foreach (var voiceTag in
                (from tag in m_doc.Descendants("Tag")
                 where String.Compare(tag.Attribute("name").Value, "voice", true) == 0
                 select tag).ToList())
            {
                var pack = voiceTag.Ancestors("Pack").FirstOrDefault();
                var bank = voiceTag.Ancestors("Bank").FirstOrDefault();
                var parent = voiceTag.Parent;
                if (bank != null && pack != null)
                {
                    bSuccess &= ErrorCheckVoiceContext(pack.Attribute("name").Value + "\\" + bank.Attribute("name").Value,
                                                   parent);
                }
            }
            return bSuccess;
        }

        private static bool ErrorCheckVoiceContext(string bankName, XElement parentNode)
        {
            var bSuccess = true;

            foreach (var waveNode in (from wave in parentNode.Descendants("Wave") select wave).ToList())
            {
                var name = waveNode.Attribute("name").Value;
                var index = name.LastIndexOf("_");
                
                try
                {
                    var variationString = name.Substring(index + 1, ((name.IndexOf('.') - index) - 1));
                    byte.Parse(variationString);
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid variation number for wave: " + name + " bank: " + bankName);
                    bSuccess = false;
                }
            }
            return bSuccess;
        }

        private void ParseNodes()
        {
            foreach (var voiceTag in
                (from tag in m_doc.Descendants("Tag")
                 where string.Compare(tag.Attribute("name").Value, "voice", true) == 0
                 select tag).ToList())
            {
                var pack = voiceTag.Ancestors("Pack").FirstOrDefault();
                var bank = voiceTag.Ancestors("Bank").FirstOrDefault();
                var parent = voiceTag.Parent;

                if (pack != null && bank != null)
                {
                    var voiceName = voiceTag.Attribute("value").Value;
                    var voiceNameHash = Hash.Generate(voiceName);
                    //Add hash/voice key value pair to dictionary
                    if (!m_voiceLookup.ContainsKey(voiceName))
                    {
                        m_voiceLookup.Add(voiceName, voiceNameHash);
                    }

                    var speechEntry =
                        (from se in m_speechEntries where se.NameHash == voiceNameHash select se).FirstOrDefault();
                    if (speechEntry == null)
                    {
                        speechEntry = new VoiceEntry();
                        speechEntry.NameHash = voiceNameHash;
                        String painType = "";
                        foreach (XElement descendant in parent.Descendants("Tag"))
                        {
                            if(string.Compare(descendant.Attribute("name").Value, "pain", true) == 0)
                            {
                                painType = descendant.Attribute("value").Value;
                                break;
                            }
                        }

                        speechEntry.PainHash = (byte)(Hash.Generate(painType) & 0xff);
                        speechEntry.Contexts = new List<VoiceContextEntry>();
                        m_speechEntries.Add(speechEntry);
                    }
                    else
                    {
                        String painType = "";
                        foreach (XElement descendant in parent.Descendants("Tag"))
                        {
                            if (string.Compare(descendant.Attribute("name").Value, "pain", true) == 0)
                            {
                                painType = descendant.Attribute("value").Value;
                                break;
                            }
                        }
                        if (!painType.Equals("") && (byte)(Hash.Generate(painType) & 0xff) != speechEntry.PainHash)
                        {
                            Console.WriteLine("Mismatched pain tags for voice {0}", voiceTag.Value);
                            throw new Exception("Mismatched pain tags for voice.");
                        }
                    }

                    AddVoiceContext(pack.Attribute("name").Value + "\\" + bank.Attribute("name").Value,
                                    parent,
                                    speechEntry.Contexts);
                }
                else
                {
                    Console.WriteLine("Ignoring voice tag {0}", voiceTag.Value);
                }
            }
        }

        private void AddVoiceContext(string bankName, XElement parentNode, List<VoiceContextEntry> contexts)
        {
            foreach (var waveNode in (from wave in parentNode.Descendants("Wave") select wave).ToList())
            {
                if (FindTag(waveNode, "DoNotBuild") != null)
                {
                    continue;
                }
                var name = waveNode.Attribute("name").Value;
                var index = name.LastIndexOf("_");
                var contextName = name.Substring(0, index);
                var variationString = name.Substring(index + 1, ((name.IndexOf('.') - index) - 1));
                byte variationNum;

                try
                {
                    variationNum = byte.Parse(variationString);
                }
                catch (FormatException)
                {
                    throw new Exception("Invalid variation number for wave: " + name + " bank: " + bankName);
                }

                var nameHash = Hash.Generate(contextName);

                var speechContext = (from sc in contexts where sc.NameHash == nameHash select sc).FirstOrDefault();

                // if this voice doesn't already have an entry for this context we need to create one
                if (speechContext == null)
                {
                    if (!m_contextLookup.ContainsKey(contextName))
                    {
                        m_contextLookup.Add(contextName, nameHash);
                    }

                    speechContext = new VoiceContextEntry();
                    speechContext.NameHash = nameHash;
                    speechContext.VariationData = new List<VariationDataEntry>();

                    //variation and context data produced by xsl. (This only applies to radio speech atm)
                    speechContext.ContextData = FindContextData(waveNode);                   

                    var hash = new rage.ToolLib.Hash {Value = bankName};
                    m_stringTable.Add(hash);
                    var bankNameIndex = m_stringTable.IndexOf(bankName);
                    speechContext.BankNameIndex = (uint) bankNameIndex;

                    contexts.Add(speechContext);
                    m_totalNumberOfContexts++;
                }

                // search for optional per-variation data (generated by Xsl)
                byte data;
                if (FindVariationData(waveNode, out data))
                {
                    m_totalNumberVariationElements++;
                    speechContext.VariationData.Add(new VariationDataEntry { Data = data, Number = variationNum });
                }

                speechContext.NumVariations++;
            }
        }

        private static byte FindContextData(XElement node)
        {
            var contextTag =
                (from el in node.Descendants("Tag") where el.Attribute("name").Value == "contextData" select el).
                    FirstOrDefault();

            return contextTag == null ? (byte) 0 : Byte.Parse(contextTag.Attribute("value").Value);
        }

        private static bool FindVariationData(XElement node, out byte ret)
        {
            var varTag =
                (from el in node.Descendants("Tag") where el.Attribute("name").Value == "variationData" select el).
                    FirstOrDefault();
            ret = 0;
            if (varTag == null)
            {
                foreach (var ancestor in node.Ancestors())
                {
                    foreach (var el in ancestor.Elements("Tag"))
                    {
                        if (el.Attribute("name").Value == "variationData")
                        {
                            varTag = el;
                            break;
                        }
                    }
                }
            }

            if (varTag != null)
            {
                ret = Byte.Parse(varTag.Attribute("value").Value);
                return true;
            }

            return false;
        }

        private void WriteLookupTables()
        {
            using (var file = new FileStream(OutputFiles[0], FileMode.Create))
            {
                using (var writer = new rage.ToolLib.Writer.BinaryWriter(file, PlatformSpecific.IsBigEndian(Platform)))
                {
                    SerializeVoiceContextVariationData(writer);
                    SerializeVoiceContextLookupTable(writer);
                    SerializeVoiceLookupTable(writer);
                    m_stringTable.Serialize(writer);
                }
            }
        }

        private void SerializeVoiceContextVariationData(IWriter writer)
        {
            writer.Write(m_totalNumberVariationElements);
            var variationDataStartPos = writer.Tell();

            foreach (var speechEntry in m_speechEntries)
            {
                foreach (var speechContext in speechEntry.Contexts)
                {
                    if (speechContext.VariationData.Count > 0)
                    {
                        //store start offset
                        speechContext.VariationDataOffsetBytes = (int) (writer.Tell() - variationDataStartPos);
                        //order by variation number
                        foreach (var speechVar in speechContext.VariationData.OrderBy(x => x.Number))
                        {
                            writer.BinWriter.Write(speechVar.Data);
                        }
                    }
                    else
                    {
                        speechContext.VariationDataOffsetBytes = -1;
                    }
                }
            }
        }

        private void SerializeVoiceContextLookupTable(IWriter writer)
        {
            writer.Write(m_totalNumberOfContexts);

            var contextStartPosition = writer.Tell();

            foreach (var speechEntry in m_speechEntries)
            {
                //store offset
                speechEntry.ContextOffsetBytes = writer.Tell() - contextStartPosition;
                //order by name hash
                foreach (var speechContext in speechEntry.Contexts.OrderBy(x => x.NameHash))
                {
                    writer.Write(speechContext.BankNameIndex);
                    writer.Write(speechContext.VariationDataOffsetBytes);
                    writer.Write(speechContext.NameHash);
                    writer.BinWriter.Write(speechContext.ContextData);
                    writer.BinWriter.Write(speechContext.NumVariations);
                }
            }
        }

        private void SerializeVoiceLookupTable(IWriter writer)
        {
            writer.Write((uint) m_speechEntries.Count);

            foreach (var speechEntry in m_speechEntries.OrderBy(x => x.NameHash))
            {
                writer.Write(speechEntry.ContextOffsetBytes);
                writer.Write(speechEntry.NameHash);
                writer.Write((ushort) speechEntry.Contexts.Count);
                writer.Write(speechEntry.PainHash);
            }
        }

        private static void WriteLookupFile(Dictionary<string, UInt32> lookup, string outputFile)
        {
            using (var fs = new FileStream(outputFile, FileMode.Create))
            {
                using (var sw = new StreamWriter(fs))
                {
                    foreach (var kvp in lookup)
                    {
                        sw.Write(kvp.Key);
                        sw.Write(",");
                        sw.WriteLine(kvp.Value);
                    }
                    sw.Flush();
                }
            }
        }

        private static XElement FindTag(XElement element, string tagName)
        {
            foreach (var e in element.AncestorsAndSelf())
            {
                foreach (var tag in e.Elements("Tag"))
                {
                    if (string.Compare(tag.Attribute("name").Value, tagName, true) == 0)
                    {
                        return tag;
                    }
                }
            }
            return null;
        }
    }
}