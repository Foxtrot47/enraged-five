﻿using System;
using System.Diagnostics;

namespace audGestureBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var encoder = new GestureBuilder(args);
                encoder.Execute();
                Environment.ExitCode = 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                var argsCombined = "";
                foreach (var arg in args)
                {
                    argsCombined += arg + " ";
                }
                EventLog.WriteEntry("Audio Build", "GestureBuilder: " + argsCombined + " - " + ex);
                Environment.ExitCode = -1;
            }
        }
    }
}
