﻿using System;
using System.IO;
using Gesturelib;
using audAssetBuilderCommon;

namespace audGestureBuilder
{
    using System.Linq;
    using System.Text.RegularExpressions;

    class GestureBuilder : WaveBuildBase
    {
        public GestureBuilder(string[] args)
            : base(args)
        {
        }

        public override void Run()
        {
            if (null == InputFile) return;

            // The .processed waves should just use the gesture file associated
            // with the original wave
            var srcWavePath = Regex.Replace(InputFile, @"\.processed", string.Empty, RegexOptions.IgnoreCase);
            var srcGestureName = Path.GetFileNameWithoutExtension(srcWavePath) + ".gesture";
            var srcGesturePath = Path.Combine(Path.GetDirectoryName(srcWavePath), srcGestureName);

            var destGestureName = Path.GetFileNameWithoutExtension(InputFile) + ".gesture";
            var destGesturePath = Path.Combine(OutputDirectory, destGestureName);
            
            // Gesture file is optional, so okay to
            // just return if file not found
            if (!File.Exists(srcGesturePath)) return;

            var serializer = new bwGestureSerializer();
            var gestureFile = serializer.DeSerializeObject(srcGesturePath);

            if (!gestureFile.Markers.Any())
            {
                Console.WriteLine("Skipping gesture file with no markers: " + destGesturePath);

                // Need to clear the output file, if it exists, to make sure it's not picked up later
                if (File.Exists(destGesturePath))
                {
                    Console.WriteLine("Clearing temp file: " + destGesturePath);
                    var writer = new StreamWriter(destGesturePath, false);
                    writer.Close();
                }

                return;
            }

            Console.WriteLine("Building gesture file: " + destGesturePath);
            
            // Initialise file stream and writer for temp file
            var fs = new FileStream(destGesturePath, FileMode.Create, FileAccess.Write);
            var bw = new rage.ToolLib.Writer.BinaryWriter(fs, PlatformSpecific.IsBigEndian(Platform));

            // Order markers by position
            var markers = from marker in gestureFile.Markers 
                          orderby marker.CropPosition 
                          select marker;

            var resamplingRatio = WaveProperties.BuiltAsset.SampleRate / (float)WaveProperties.SourceAsset.SampleRate;

            uint isSpeakerFlag = 1U;
            uint isFacialFlag = 2U;

            // Write gesture data out to temp file
            foreach (var marker in markers)
            {
                // Rescale the position
                var position = (uint)(marker.CropPosition * resamplingRatio);

                // Strip prefix from name (up to and including the ':')
                var name = marker.Name.Substring(marker.Name.IndexOf(':') + 1);
                // Name requires hashing
                var hash = new rage.ToolLib.Hash {Value = name};
                bw.Write(hash.Key);
                bw.Write(position);
                bw.Write(marker.Clip.StartFrame);
                bw.Write(marker.Clip.EndFrame);
                bw.Write(marker.Clip.Rate);
                bw.Write(marker.Clip.MaxWeight);
                bw.Write(marker.Clip.BlendInTime);
                bw.Write(marker.Clip.BlendOutTime);
                uint flags = 0U;
                // Determine if gesture is for speaker or listener and set flag
                if (marker.Name.ToUpper().StartsWith("G_S"))
                {
                    flags = isSpeakerFlag;
                }
                if (marker.Name.ToUpper().StartsWith("F_S"))
                {
                    flags = isFacialFlag | isSpeakerFlag;
                }
                if (marker.Name.ToUpper().StartsWith("F_L"))
                {
                    flags = isFacialFlag;
                }
                bw.Write(flags);
            }
            
            // Close writer and stream
            bw.Flush();
            bw.Close();
            fs.Close();
        }
    }
}
