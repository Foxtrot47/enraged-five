﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SourceAsset.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The source asset.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace audAssetBuilderProperties
{
    using System;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// The source asset.
    /// </summary>
    public class SourceAsset
    {
        #region Fields

        /// <summary>
        /// The built node.
        /// </summary>
        private readonly XElement m_builtNode;

        /// <summary>
        /// The granular.
        /// </summary>
        private XElement m_granular;

        /// <summary>
        /// The name.
        /// </summary>
        private XElement m_name;

        /// <summary>
        /// The number of samples.
        /// </summary>
        private XElement m_numOfSamples;

        /// <summary>
        /// The path.
        /// </summary>
        private XElement m_path;

        /// <summary>
        /// The sample rate.
        /// </summary>
        private XElement m_sampleRate;

        /// <summary>
        /// The size.
        /// </summary>
        private XElement m_size;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceAsset"/> class.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        public SourceAsset(XElement node)
        {
            this.m_builtNode = node;
            this.m_name = node.Descendants("Name").FirstOrDefault();
            this.m_path = node.Descendants("Path").FirstOrDefault();
            this.m_size = node.Descendants("Size").FirstOrDefault();
            this.m_sampleRate = node.Descendants("SampleRate").FirstOrDefault();
            this.m_numOfSamples = node.Descendants("NumberOfSamples").FirstOrDefault();
            this.m_granular = node.Descendants("Granular").FirstOrDefault();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether granular.
        /// </summary>
        public bool Granular
        {
            get
            {
                return bool.Parse(this.m_granular.Value);
            }

            set
            {
                if (this.m_granular == null)
                {
                    this.m_granular = new XElement("Granular");
                    this.m_builtNode.Add(this.m_granular);
                }

                this.m_granular.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get
            {
                // NOTE: this will intentionally throw an exception if m_Name is null;
                // code shouldn't be reading a value that doesn't exist, so fix at a higher level rather
                // than patching this code to deal with NULL.
                return this.m_name.Value;
            }

            set
            {
                if (this.m_name == null)
                {
                    this.m_name = new XElement("Name");
                    this.m_builtNode.Add(this.m_name);
                }

                this.m_name.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of samples.
        /// </summary>
        public uint NumberOfSamples
        {
            get
            {
                return uint.Parse(this.m_numOfSamples.Value);
            }

            set
            {
                if (this.m_numOfSamples == null)
                {
                    this.m_numOfSamples = new XElement("NumberOfSamples");
                    this.m_builtNode.Add(this.m_numOfSamples);
                }

                this.m_numOfSamples.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        public string Path
        {
            get
            {
                // NOTE: this will intentionally throw an exception if m_Name is null;
                // code shouldn't be reading a value that doesn't exist, so fix at a higher level rather
                // than patching this code to deal with NULL.
                return this.m_path.Value;
            }

            set
            {
                if (this.m_path == null)
                {
                    this.m_path = new XElement("Path");
                    this.m_builtNode.Add(this.m_path);
                }

                this.m_path.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the sample rate.
        /// </summary>
        public int SampleRate
        {
            get
            {
                // NOTE: this will intentionally throw an exception if m_SampleRate is null;
                // code shouldn't be reading a value that doesn't exist, so fix at a higher level rather
                // than patching this code to deal with NULL.
                return int.Parse(this.m_sampleRate.Value);
            }

            set
            {
                if (this.m_sampleRate == null)
                {
                    this.m_sampleRate = new XElement("SampleRate");
                    this.m_builtNode.Add(this.m_sampleRate);
                }

                if (value < 1000 || value > 96000)
                {
                    throw new ArgumentException(
                        "Sample rate out-with expected range: expected 1000 - 96000, got " + value);
                }

                this.m_sampleRate.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        public int Size
        {
            get
            {
                // NOTE: this will intentionally throw an exception if m_Size is null;
                // code shouldn't be reading a value that doesn't exist, so fix at a higher level rather
                // than patching this code to deal with NULL.
                return int.Parse(this.m_size.Value);
            }

            set
            {
                if (this.m_size == null)
                {
                    this.m_size = new XElement("Size");
                    this.m_builtNode.Add(this.m_size);
                }

                if (value <= 0 || value > 600 * 1024 * 1024)
                {
                    throw new ArgumentException("Source Asset - Size out-with expected range: " + value);
                }

                this.m_size.Value = value.ToString();
            }
        }

        #endregion
    }
}