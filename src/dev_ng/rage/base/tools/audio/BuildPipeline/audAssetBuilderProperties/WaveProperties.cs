﻿using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace audAssetBuilderProperties
{
    using System;

    public class WaveProperties
    {
        private BuiltAsset m_builtAsset;
        private SourceAsset m_sourceAsset;

        public WaveProperties()
        {
            RootNode = new XElement("WaveProperties");
            var doc = new XDocument();
            doc.Add(RootNode);
        }

        public WaveProperties(string xmlPath)
        {
            var doc = XDocument.Load(xmlPath);
            ParseNode(doc.Root);
        }

        public WaveProperties(XElement root)
        {
            ParseNode(root);
        }

        public XElement RootNode { get; private set; }

        public SourceAsset SourceAsset
        {
            get
            {
                if (m_sourceAsset == null)
                {
                    var sourceAsset = new XElement("SourceAsset");
                    RootNode.Add(sourceAsset);
                    m_sourceAsset = new SourceAsset(sourceAsset);
                }
                return m_sourceAsset;
            }
        }

        public BuiltAsset BuiltAsset
        {
            get
            {
                if (m_builtAsset == null)
                {
                    var builtAsset = new XElement("BuiltAsset");
                    RootNode.Add(builtAsset);
                    m_builtAsset = new BuiltAsset(builtAsset);
                }
                return m_builtAsset;
            }
        }

        private void ParseNode(XElement root)
        {
            RootNode = root;
            m_sourceAsset = new SourceAsset(root.Descendants("SourceAsset").FirstOrDefault());
            m_builtAsset = new BuiltAsset(root.Descendants("BuiltAsset").FirstOrDefault());
        }

        public bool Save(string xmlPath, bool force = false)
        {
            // Check file is writable, if it exists
            if (File.Exists(xmlPath))
            {
                var fileInfo = new FileInfo(xmlPath);
                if (fileInfo.IsReadOnly)
                {
                    if (!force)
                    {
                        throw new Exception("File is writable - use force=true to force save");
                    }

                    fileInfo.IsReadOnly = false;
                }
            }

            if (RootNode == RootNode.Document.Root)
            {
                var directory = xmlPath.Substring(0, xmlPath.LastIndexOf('\\'));
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                RootNode.Document.Save(xmlPath);
            }
            else
            {
                var doc = new XDocument(RootNode);
                doc.Save(xmlPath);
            }
            return true;
        }
    }
}