﻿// -----------------------------------------------------------------------
// <copyright file="ChunkMarkerProperties.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace audAssetBuilderProperties
{
    using System.Xml.Linq;

    /// <summary>
    /// Chunk marker properties.
    /// </summary>
    public class ChunkMarkerProperties
    {
        /// <summary>
        /// The element.
        /// </summary>
        private readonly XElement element;

        /// <summary>
        /// The time offset in milliseconds.
        /// </summary>
        private uint timeOffsetMs;

        /// <summary>
        /// The value.
        /// </summary>
        private string value;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChunkMarkerProperties"/> class.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public ChunkMarkerProperties(XElement element)
        {
            this.element = element;

            var timeOffsetAttr = element.Attribute("timeMs");
            if (null != timeOffsetAttr)
            {
                uint.TryParse(timeOffsetAttr.Value, out this.timeOffsetMs);
            }

            this.value = element.Value;
        }

        /// <summary>
        /// Gets or sets the time offset in milliseconds.
        /// </summary>
        public uint TimeOffsetMs
        {
            get
            {
                return this.timeOffsetMs;
            }

            set
            {
                this.timeOffsetMs = value;
                this.element.SetAttributeValue("timeMs", value);
            }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.value = value;
                this.element.Value = value;
            }
        }
    }
}
