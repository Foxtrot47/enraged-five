﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace audAssetBuilderProperties
{
    using System.Linq;

    public class ChunkProperties
    {
        private XElement m_Node;
        private string m_Name;
        private int m_Size;

        public ChunkProperties(XElement element)
        {
            m_Node = element;
            m_Name = m_Node.Attribute("name").Value;
            m_Size = Int32.Parse(m_Node.Attribute("size").Value);
            this.ChunkMarkers = new List<ChunkMarkerProperties>();

            foreach (var markerElem in element.Elements("Marker"))
            {
                this.ChunkMarkers.Add(new ChunkMarkerProperties(markerElem));
            }
        }

        public string Name
        {
            get
            {
                return m_Name;
            }
            set
            {
                m_Node.SetAttributeValue("name", value);
            }
        }

        public int Size
        {
            get
            {
                return m_Size;
            }
            set
            {
                m_Node.SetAttributeValue("size", value.ToString());
            }
        }

        public IList<ChunkMarkerProperties> ChunkMarkers { get; private set; }

        public bool AddMarker(string name, string timeOffset)
        {
            var newChunk = new XElement("Marker", new XAttribute("timeMs", timeOffset)) { Value = name };
            ChunkMarkers.Add(new ChunkMarkerProperties(newChunk));
            m_Node.Add(newChunk);

            return true;
        }
    }
}
