﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace audAssetBuilderProperties
{
    public interface IChunkContainer
    {
        bool AddOrUpdateChunk(string name, int size);
        List<ChunkProperties> Chunks
        {
            get;
        }
    }
}
