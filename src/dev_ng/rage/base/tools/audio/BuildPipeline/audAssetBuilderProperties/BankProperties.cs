﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace audAssetBuilderProperties
{
    public class BankProperties : IChunkContainer
    {
        private readonly List<ChunkProperties> m_chunks;
        private readonly XDocument m_document;
        private readonly string m_loadedXmlPath;
        private readonly List<WaveProperties> m_waves;
        private XElement m_containerSizeNode;
        private XElement m_dateTimeNode;
        private XElement m_headerSizeNode;
        private XElement m_metadataSizeNode;
        private XElement m_wavesNode;
        private XElement m_awcMd5Node;

        public BankProperties()
        {
            m_loadedXmlPath = "(n/a)";
            m_document = new XDocument();
            m_document.Add(new XElement("BankProperties"));
            m_waves = new List<WaveProperties>();
            m_chunks = new List<ChunkProperties>();
            m_wavesNode = new XElement("Waves");
            m_document.Root.Add(m_wavesNode);
        }

        public BankProperties(string xmlPath)
        {
            m_loadedXmlPath = xmlPath;
            m_document = XDocument.Load(xmlPath);
            m_waves = new List<WaveProperties>();
            m_chunks = new List<ChunkProperties>();

            m_dateTimeNode = m_document.Root.Descendants("DateTime").FirstOrDefault();
            m_headerSizeNode = m_document.Root.Descendants("HeaderSize").FirstOrDefault();
            m_containerSizeNode = m_document.Root.Descendants("ContainerSize").FirstOrDefault();
            m_awcMd5Node = m_document.Root.Descendants("AwcMd5").FirstOrDefault();
            m_wavesNode = m_document.Root.Descendants("Waves").FirstOrDefault();

            foreach (var child in m_document.Root.Descendants("WaveProperties"))
            {
                m_waves.Add(new WaveProperties(child));
            }

            foreach (var chunk in m_document.Root.Elements("Chunk").ToList())
            {
                m_chunks.Add(new ChunkProperties(chunk));
            }
        }

        public List<WaveProperties> WaveProperties
        {
            get { return m_waves; }
        }

        public DateTime DateTime
        {
            get
            {
                if (m_dateTimeNode == null)
                {
                    return DateTime.MinValue;
                }

                DateTime res;

                var succeeded = DateTime.TryParse(m_dateTimeNode.Value, out res);
                if (succeeded)
                {
                    return res;
                }
                else
                {
                    throw new FormatException(
                        string.Format("Failed to parse DateTime from bank properties {0} - value {1}",
                                      m_loadedXmlPath,
                                      m_dateTimeNode.Value));
                }
            }
            set
            {
                if (m_dateTimeNode == null)
                {
                    m_dateTimeNode = new XElement("DateTime");
                    m_document.Root.Add(m_dateTimeNode);
                }

                m_dateTimeNode.SetValue(value);
            }
        }

        public uint HeaderSize
        {
            get
            {
                if (m_headerSizeNode == null)
                {
                    return 0;
                }

                return UInt32.Parse(m_headerSizeNode.Value);
            }
            set
            {
                if (m_headerSizeNode == null)
                {
                    m_headerSizeNode = new XElement("HeaderSize");
                    m_document.Root.Add(m_headerSizeNode);
                }

                m_headerSizeNode.Value = value.ToString();
            }
        }

        public uint MetadataSize
        {
            get
            {
                if (m_metadataSizeNode == null)
                {
                    // default to 8kb for now
                    return 8 * 1024;
                }

                return UInt32.Parse(m_metadataSizeNode.Value);
            }
            set
            {
                if (m_metadataSizeNode == null)
                {
                    m_metadataSizeNode = new XElement("MetadataSize");
                    m_document.Root.Add(m_metadataSizeNode);
                }

                m_metadataSizeNode.Value = value.ToString();
            }
        }

        public long Size
        {
            get
            {
                if (m_containerSizeNode == null)
                {
                    return 0;
                }

                return long.Parse(m_containerSizeNode.Value);
            }
            set
            {
                if (m_containerSizeNode == null)
                {
                    m_containerSizeNode = new XElement("ContainerSize");
                    m_document.Root.Add(m_containerSizeNode);
                }

                m_containerSizeNode.Value = value.ToString();
            }
        }

        public string AwcMd5
        {
            get
            {
                if (null == m_awcMd5Node)
                {
                    return null;
                }

                return m_awcMd5Node.Value;
            }

            set
            {
                if (null == m_awcMd5Node)
                {
                    m_awcMd5Node = new XElement("AwcMd5");
                    m_document.Root.Add(m_awcMd5Node);
                }

                m_awcMd5Node.Value = value;
            }
        }

        #region IChunkContainer Members

        public List<ChunkProperties> Chunks
        {
            get { return m_chunks; }
        }

        public bool AddOrUpdateChunk(string name, int size)
        {
            var key = name.ToUpper();

            var existingElem =
                m_document.Elements("Chunk")
                          .FirstOrDefault(e => null != e.Attribute("name") && e.Attribute("name").Value == key);

            if (null == existingElem)
            {
                var newChunk = new XElement("Chunk", new XAttribute("name", key), new XAttribute("size", size));
                m_chunks.Add(new ChunkProperties(newChunk));
                m_document.Root.Add(newChunk);
            }
            else
            {
                existingElem.SetAttributeValue("size", size);
            }

            return true;
        }

        #endregion

        public void AddWaveProperties(string wavePropertiesPath)
        {
            var doc = XDocument.Load(wavePropertiesPath);
            AddWaveProperties(doc.Root);
        }

        public WaveProperties AddWaveProperties(XElement node)
        {
            if (node.Name == "WaveProperties")
            {
                if (m_wavesNode == null)
                {
                    m_wavesNode = new XElement("Waves");
                    m_document.Root.Add(m_wavesNode);
                }

                var e = new XElement(node);
                m_wavesNode.Add(e);
                var wp = new WaveProperties(e);
                m_waves.Add(wp);
                return wp;
            }

            return null;
        }

        public bool Save(string filePath)
        {
            try
            {
                m_document.Save(filePath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}