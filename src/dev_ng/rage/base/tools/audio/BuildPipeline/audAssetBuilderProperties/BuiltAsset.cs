﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using rage.ToolLib;

namespace audAssetBuilderProperties
{
    public class BuiltAsset : IChunkContainer
    {
        private readonly XElement m_builtNode;
        private XElement m_blockSize;
        private XElement m_compression;
        private XElement m_headRoom;
        private XElement m_integratedLoudness;
        private XElement m_loudnessRange;
        private XElement m_name;
        private XElement m_nameHash;
        private XElement m_numOfFrames;
        private XElement m_numOfSamples;
		private XElement m_numOfSamplesPostResampling;
        private XElement m_preserveTransient;
        private XElement m_disableLooping;
        private XElement m_forceInternalEncoder;
        private XElement m_markSilence;
        private XElement m_sampleRate;
        private XElement m_samplesPerFrame;
        private XElement m_size;
        private XElement m_firstPeakSample;
        private XElement m_compressionType;
        private XElement m_iterativeEncoding;
        private XElement m_customLipsync;

        public BuiltAsset(XElement node)
        {
            m_builtNode = node;

            m_name = node.Descendants("Name").FirstOrDefault();
            m_nameHash = node.Descendants("NameHash").FirstOrDefault();
            m_size = node.Descendants("Size").FirstOrDefault();
            m_sampleRate = node.Descendants("SampleRate").FirstOrDefault();
            m_headRoom = node.Descendants("HeadRoom").FirstOrDefault();
            m_integratedLoudness = node.Descendants("IntegratedLoudness").FirstOrDefault();
            m_loudnessRange = node.Descendants("LoudnessRange").FirstOrDefault();
            m_compression = node.Descendants("Compression").FirstOrDefault();
            m_compressionType = node.Descendants("CompressionType").FirstOrDefault();
            m_blockSize = node.Descendants("BlockSize").FirstOrDefault();
            m_samplesPerFrame = node.Descendants("SamplesPerFrame").FirstOrDefault();
            m_numOfFrames = node.Descendants("NumberOfFrames").FirstOrDefault();
            m_numOfSamples = node.Descendants("NumberOfSamples").FirstOrDefault();
			m_numOfSamplesPostResampling = node.Descendants("NumberOfSamplesPostResampling").FirstOrDefault();
            m_preserveTransient = node.Descendants("PreserveTransient").FirstOrDefault();
            m_disableLooping = node.Descendants("DisableLooping").FirstOrDefault();
            m_forceInternalEncoder = node.Descendants("ForceInternalEncoder").FirstOrDefault();
            m_markSilence = node.Descendants("MarkSilence").FirstOrDefault();
            m_firstPeakSample = node.Descendants("FirstPeakSample").FirstOrDefault();
            m_iterativeEncoding = node.Descendants("IterativeEncoding").FirstOrDefault();
            m_customLipsync = node.Descendants("CustomLipsync").FirstOrDefault();

            Chunks = new List<ChunkProperties>();
            foreach (var chunk in node.Descendants("Chunk").ToList())
            {
                Chunks.Add(new ChunkProperties(chunk));
            }
        }

        public string Name
        {
            get { return m_name.Value; }
            set
            {
                if (m_name == null)
                {
                    m_name = new XElement("Name");
                    m_builtNode.Add(m_name);
                }
                m_name.Value = value;
            }
        }

        public uint NameHash
        {
            get
            {
                if (m_nameHash == null)
                {
                    if (m_name != null)
                    {
                        var objectName = m_name.Value.Replace(".WAV", string.Empty).Replace(".MID", string.Empty);
                        var h = new Hash {Value = objectName};
                        //enum {kObjectNameHashWidth = 29};
                        //enum {kObjectNameHashMask = (1<<kObjectNameHashWidth)-1};
                        const uint MASK = (1 << 29) - 1;
                        NameHash = h.Key & MASK;
                    }
                    else
                    {
                        throw new Exception("Name is null");
                    }
                }
                return uint.Parse(m_nameHash.Value);
            }
            set
            {
                if (m_nameHash == null)
                {
                    m_nameHash = new XElement("NameHash");
                    m_builtNode.Add(m_nameHash);
                }
                m_nameHash.Value = value.ToString();
            }
        }

        public uint Size
        {
            get { return uint.Parse(m_size.Value); }
            set
            {
                if (m_size == null)
                {
                    m_size = new XElement("Size");
                    m_builtNode.Add(m_size);
                }
                if (value == 0 ||
                    value > 100 * 1024 * 1024)
                {
                    throw new ArgumentException("Built Asset - Size out-with expected range: " + value);
                }
                m_size.Value = value.ToString();
            }
        }

        public int SampleRate
        {
            get { return int.Parse(m_sampleRate.Value); }
            set
            {
                if (m_sampleRate == null)
                {
                    m_sampleRate = new XElement("SampleRate");
                    m_builtNode.Add(m_sampleRate);
                }
                if (value < 1000 ||
                    value > 96000)
                {
                    throw new ArgumentException("Sample rate out-with expected range: expected 1000 - 96000, got " +
                                                value);
                }
                m_sampleRate.Value = value.ToString();
            }
        }

        public int HeadRoom
        {
            get { return int.Parse(m_headRoom.Value); }
            set
            {
                if (m_headRoom == null)
                {
                    m_headRoom = new XElement("HeadRoom");
                    m_builtNode.Add(m_headRoom);
                }

                m_headRoom.Value = value.ToString();
            }
        }

        public double? IntegratedLoudness
        {
            get
            {
                if (m_integratedLoudness == null) return null;
                return double.Parse(m_integratedLoudness.Value);
            }
            set
            {
                if (m_integratedLoudness == null)
                {
                    m_integratedLoudness = new XElement("IntegratedLoudness");
                    m_builtNode.Add(m_integratedLoudness);
                }

                m_integratedLoudness.Value = value.ToString();
            }
        }

        public double? LoudnessRange
        {
            get
            {
                if (m_loudnessRange == null) return null;
                else return double.Parse(m_loudnessRange.Value);
            }
            set
            {
                if (m_loudnessRange == null)
                {
                    m_loudnessRange = new XElement("LoudnessRange");
                    m_builtNode.Add(m_loudnessRange);
                }

                m_loudnessRange.Value = value.ToString();
            }
        }

        public int FirstPeakSample
        {
            get { return int.Parse(m_firstPeakSample.Value); }
            set
            {
                if (m_firstPeakSample == null)
                {
                    m_firstPeakSample = new XElement("FirstPeakSample");
                    m_builtNode.Add(m_firstPeakSample);
                }

                m_firstPeakSample.Value = value.ToString();
            }
        }

        public int Compression
        {
            get
            {
                if (m_compression == null ||
                    m_compression.Value == null)
                {
                    Console.WriteLine("Warning: Asset {0} has no valud compression tag, defaulting to 60", Name);
                    return 60;
                }
                return int.Parse(m_compression.Value);
            }
            set
            {
                if (m_compression == null)
                {
                    m_compression = new XElement("Compression");
                    m_builtNode.Add(m_compression);
                }

                m_compression.Value = value.ToString();
            }
        }

        public string CompressionType
        {
            get 
            { 
                if(m_compressionType == null)
                {
                    return string.Empty;
                }
                return m_compressionType.Value; 
            }
            set
            {
                if (m_compressionType == null)
                {
                    m_compressionType = new XElement("CompressionType");
                    m_builtNode.Add(m_compressionType);
                }
                m_compressionType.Value = value;
            }
        }

        public uint NumberOfSamples
        {
            get { return uint.Parse(m_numOfSamples.Value); }
            set
            {
                if (m_numOfSamples == null)
                {
                    m_numOfSamples = new XElement("NumberOfSamples");
                    m_builtNode.Add(m_numOfSamples);
                }

                m_numOfSamples.Value = value.ToString();
            }
        }

		public uint NumberOfSamplesPostResampling
		{
			get { return uint.Parse(m_numOfSamplesPostResampling.Value); }
			set
			{
				if (m_numOfSamplesPostResampling == null)
				{
					m_numOfSamplesPostResampling = new XElement("NumberOfSamplesPostResampling");
					m_builtNode.Add(m_numOfSamplesPostResampling);
				}

				m_numOfSamplesPostResampling.Value = value.ToString();
			}
		}

        public uint NumberOfFrames
        {
            get { return uint.Parse(m_numOfFrames.Value); }
            set
            {
                if (m_numOfFrames == null)
                {
                    m_numOfFrames = new XElement("NumberOfFrames");
                    m_builtNode.Add(m_numOfFrames);
                }

                m_numOfFrames.Value = value.ToString();
            }
        }

        public uint SamplesPerFrame
        {
            get { return uint.Parse(m_samplesPerFrame.Value); }
            set
            {
                if (m_samplesPerFrame == null)
                {
                    m_samplesPerFrame = new XElement("SamplesPerFrame");
                    m_builtNode.Add(m_samplesPerFrame);
                }

                m_samplesPerFrame.Value = value.ToString();
            }
        }

        public int BlockSize
        {
            get { return int.Parse(m_blockSize.Value); }
            set
            {
                if (m_blockSize == null)
                {
                    m_blockSize = new XElement("BlockSize");
                    m_builtNode.Add(m_blockSize);
                }

                m_blockSize.Value = value.ToString();
            }
        }

        public bool PreserveTransient
        {
            get
            {
                if (m_preserveTransient != null)
                {
                    return m_preserveTransient.Value.Equals("yes");
                }
                return false;
            }
            set
            {
                if (m_preserveTransient == null)
                {
                    m_preserveTransient = new XElement("PreserveTransient");
                    m_builtNode.Add(m_preserveTransient);
                }
                m_preserveTransient.Value = value ? "yes" : "no";
            }
        }

        public bool ForceInternalEncoder
        {
            get
            {
                if (m_forceInternalEncoder != null)
                {
                    return m_forceInternalEncoder.Value.Equals("yes");
                }
                return false;
            }
            set
            {
                if (m_forceInternalEncoder == null)
                {
                    m_forceInternalEncoder = new XElement("ForceInternalEncoder");
                    m_builtNode.Add(m_forceInternalEncoder);
                }
                m_forceInternalEncoder.Value = value ? "yes" : "no";
            }
        } 

        public bool DisableLooping
        {
            get
            {
                if (m_disableLooping != null)
                {
                    return m_disableLooping.Value.Equals("yes");
                }
                return false;
            }
            set
            {
                if (m_disableLooping == null)
                {
                    m_disableLooping = new XElement("DisableLooping");
                    m_builtNode.Add(m_disableLooping);
                }
                m_disableLooping.Value = value ? "yes" : "no";
            }
        }   

        public string MarkSilence
        {
            get
            {
                if (m_markSilence != null)
                {
                    return m_markSilence.Value;
                }
                return null;
            }
            set
            {
                if (m_markSilence == null)
                {
                    m_markSilence = new XElement("MarkSilence");
                    m_builtNode.Add(m_markSilence);
                }
                m_markSilence.Value = value;
            }
        }

        public string IterativeEncoding
        {
            get
            {
                if (m_iterativeEncoding != null)
                {
                    return m_iterativeEncoding.Value;
                }
                return null;
            }
            set
            {
                if (m_iterativeEncoding == null)
                {
                    m_iterativeEncoding = new XElement("IterativeEncoding");
                    m_builtNode.Add(m_iterativeEncoding);
                }
                m_iterativeEncoding.Value = value;
            }
        }

        public bool CustomLipsync
        {
             get
            {
                if (m_customLipsync != null)
                {
                    return m_customLipsync.Value.Equals("yes");
                }
                return false;
            }
            set
            {
                if (m_customLipsync == null)
                {
                    m_customLipsync = new XElement("CustomLipsync");
                    m_builtNode.Add(m_customLipsync);
                }
                m_customLipsync.Value = value ? "yes" : "no";
            }
        }

        #region IChunkContainer Members

        public List<ChunkProperties> Chunks { get; private set; }

        public bool AddOrUpdateChunk(string name, int size)
        {
            var key = name.ToUpper();

            var existingElem =
                m_builtNode.Elements("Chunk")
                          .FirstOrDefault(e => null != e.Attribute("name") && e.Attribute("name").Value == key);

            if (null == existingElem)
            {
                var newChunk = new XElement("Chunk", new XAttribute("name", key), new XAttribute("size", size));
                Chunks.Add(new ChunkProperties(newChunk));
                m_builtNode.Add(newChunk);
            }
            else
            {
                existingElem.SetAttributeValue("size", size);
            }

            return true;
        }

        #endregion

        public bool HasValidSize()
        {
            return m_size != null;
        }

        public bool HasValidSampleRate()
        {
            return m_sampleRate != null;
        }
    }
}