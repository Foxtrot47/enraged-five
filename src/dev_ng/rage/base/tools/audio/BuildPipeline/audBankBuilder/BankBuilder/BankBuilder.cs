﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using audAssetBuilderCommon;
using audAssetBuilderProperties;
using rage;
using rage.ToolLib;
using rage.ToolLib.Encryption;

namespace BankBuilder
{
	public class BankBuilder : BankBuildBase
	{
		private readonly BankProperties m_bankProperties;
		private readonly mContainerBuilder m_containerBuilder;
		private readonly string m_containerPath;
		private readonly bool m_isStreamingBank;
		private readonly Dictionary<uint, WaveProperties> m_objectIndex;
		private readonly int m_packingType;
		private readonly audProjectSettings m_projectSettings;
		private LoadType m_loadType;
		private IEncrypter m_Encrypter;

		public BankBuilder(string[] args)
			: base(args)
		{
			var sb = new StringBuilder();
			sb.Append(OutputDirectory);
			sb.Append(Name);
			sb.Append(".awc");
			m_containerPath = sb.ToString();

			m_projectSettings = new audProjectSettings(ProjectSettingsPath);
			m_projectSettings.SetCurrentPlatformByTag(Platform);

			m_bankProperties = new BankProperties();

			m_isStreamingBank = (StreamingBlockBytes > 0);

			m_packingType = GetPackingType();
			m_containerBuilder = new mContainerBuilder(m_packingType);
			m_objectIndex = new Dictionary<uint, WaveProperties>();

            string encryptionKey = null;

            if (EncryptionKey == null)
            {
                encryptionKey = m_projectSettings.GetCurrentPlatform().EncryptionKey;
            }
            else
            {
                encryptionKey = EncryptionKey;
                Console.WriteLine("BankBuilder using custom encryption key: " + encryptionKey);
            }

			m_Encrypter = EncrypterFactory.Create(encryptionKey);
			// Note: we're assuming that other pipeline stages will take care of encrypting non-data chunks, for example
			// the stream builder will encrypt STREAMDATA itself.
			m_containerBuilder.SetEncryptedData(m_Encrypter.IsEncrypting);
		}

		private int GetPackingType()
		{
			var tokens = InputDirectory.ToUpper().Split('\\');
			var pack = tokens[tokens.Length - 3];
			var bankPath = pack + "\\" + tokens[tokens.Length - 2];

			var waveSlotSettings = XDocument.Load(WaveSlotSettingsPath);
			var loadables = waveSlotSettings.Root.Descendants("Loadable");
			var loadTypes = new List<string>();
			foreach (var loadable in loadables)
			{
				var name = loadable.Attribute("name").Value;
				if (name.Equals(bankPath, StringComparison.OrdinalIgnoreCase) ||
					name.Equals(pack, StringComparison.OrdinalIgnoreCase) ||
					name.Equals(BankFolderName, StringComparison.OrdinalIgnoreCase) ||
					IsUnderBankFolder(name))
				{
					loadTypes.Add(loadable.Parent.Attribute("loadType").Value);
				}
			}

			if (loadTypes.Count == 0)
			{
				throw new ApplicationException(string.Format("Bank: {0} is not associated with a wave slot", bankPath));
			}

			loadTypes = loadTypes.Distinct().ToList();
			if (loadTypes.Count > 1)
			{
				throw new ApplicationException(string.Format("More than one load type associated with bank: {0}",
															 bankPath));
			}

			m_loadType = Enum<LoadType>.Parse(loadTypes[0]);
			if (m_isStreamingBank && m_loadType != LoadType.Stream)
			{
				throw new ApplicationException(
					string.Format("Streaming Bank: {0} is not associated with a streaming wave slot", bankPath));
			}

			if (m_isStreamingBank)
			{
				// Streaming banks require special packing
				return mContainer.STREAMING;
			}
			return (m_loadType == LoadType.Bank ? mContainer.OPTIMISED : mContainer.NORMAL);
		}

		private bool IsUnderBankFolder(string name)
		{
			if (!string.IsNullOrEmpty(BankFolderName))
			{
				var nameComponents = name.Split('\\');
				var bankFolderNameComponents = BankFolderName.Split('\\');

				if (nameComponents.Length >
					bankFolderNameComponents.Length)
				{
					return false;
				}

				for (var i = 0; i < nameComponents.Length; ++i)
				{
					if (!nameComponents[i].Equals(bankFolderNameComponents[i], StringComparison.OrdinalIgnoreCase))
					{
						return false;
					}
				}
				return true;
			}
			return false;
		}

		public override void Run()
		{
			Build();
		}

		private void Build()
		{
			if (m_isStreamingBank)
			{
				AddStreamingObject();
			}

			AddWaveObjects();

			WriteBankProperties();
		}

		private void AddStreamingObject()
		{
			var identifier = m_containerBuilder.AddObject(mContainerBuilder.ComputeObjectNameHash(""));

			if (identifier == -1)
			{
				throw new Exception("Failed to add object " + Name);
			}

			AddChunksforObject(identifier, InputDirectory + Name, m_bankProperties);
		}

		private void AddWaveObjects()
		{
			var dir = new DirectoryInfo(InputDirectory);

			foreach (var file in dir.GetFiles())
			{
				//grab wave properties
				if (String.Compare(file.Extension, ".xml", true) == 0)
				{
					var waveProperties = new WaveProperties(file.FullName);

					var name = Path.GetFileNameWithoutExtension(waveProperties.BuiltAsset.Name);

					var nameHash = mContainerBuilder.ComputeObjectNameHash(name);
					waveProperties.BuiltAsset.NameHash = nameHash;

					var identifier = m_containerBuilder.AddObject(nameHash);
					if (identifier == -1)
					{
						throw new Exception(string.Concat("Failed to create object for ", waveProperties.BuiltAsset.Name));
					}

					if (file.FullName.ToUpper().Contains(".PROCESSED"))
					{
						name = String.Concat(name, ".PROCESSED");
					}

					AddChunksforObject(identifier, InputDirectory + name, waveProperties.BuiltAsset);

					m_objectIndex.Add(nameHash, m_bankProperties.AddWaveProperties(waveProperties.RootNode));
				}
			}
		}

		private void AddChunksforObject(int identifier, string filePathNoExt, IChunkContainer chunkContainer)
		{
			filePathNoExt = filePathNoExt.ToUpper();

			//add data chunk if it exists
			var dataFilePath = filePathNoExt + ".data";
			if (File.Exists(dataFilePath) &&
				m_loadType != LoadType.Stream)
			{
				var dataChunkLength = AddChunk(dataFilePath, identifier);
				if (dataChunkLength > 0)
				{
					chunkContainer.AddOrUpdateChunk(Chunks.TYPE.DATA.ToString(), dataChunkLength);
				}
			}

			bool isWave = File.Exists(filePathNoExt + ".wav");

			var midiFilePath = filePathNoExt + ".mid";
			if (File.Exists(midiFilePath))
			{
				isWave = false;
				if (m_loadType != LoadType.Stream)
				{
					var midiChunkLength = AddChunk(midiFilePath, identifier);
					if (midiChunkLength > 0)
					{
						chunkContainer.AddOrUpdateChunk(Chunks.TYPE.MID.ToString(), midiChunkLength);
					}
				}
			}

			string edlfilepath = filePathNoExt.ToLower() + ".edl";
			if (File.Exists(edlfilepath))
			{
				isWave = false;
				if (m_loadType != LoadType.Stream)
				{

					var edlChunkLength = AddChunk(edlfilepath, identifier);
					if (edlChunkLength > 0)
					{
						chunkContainer.AddOrUpdateChunk(Chunks.TYPE.EDL.ToString(), edlChunkLength);
					}
				}
			}

			var isPC = string.Compare(m_projectSettings.GetCurrentPlatform().PlatformTag, "PC", StringComparison.OrdinalIgnoreCase) == 0;
			var isPS4 = string.Compare(m_projectSettings.GetCurrentPlatform().PlatformTag, "PS4", StringComparison.OrdinalIgnoreCase) == 0;
			var isXBOXONE = string.Compare(m_projectSettings.GetCurrentPlatform().PlatformTag, "XBOXONE", StringComparison.OrdinalIgnoreCase) == 0;
            var isPS5 = string.Compare(m_projectSettings.GetCurrentPlatform().PlatformTag, "PS5", StringComparison.OrdinalIgnoreCase) == 0;
            var isXBSX = string.Compare(m_projectSettings.GetCurrentPlatform().PlatformTag, "XBOXSERIES", StringComparison.OrdinalIgnoreCase) == 0;

			var hasFormatChunk = false;
			foreach (var ext in Enum.GetNames(typeof(Chunks.TYPE)))
			{
				var filePath = filePathNoExt + "." + ext;
				//skip data, mid or edl chunk, add this last
				if (File.Exists(filePath) &&
					string.Compare(ext, Chunks.TYPE.DATA.ToString(), true) != 0 &&
					string.Compare(ext, Chunks.TYPE.MID.ToString(), true) != 0 &&
					string.Compare(ext, Chunks.TYPE.EDL.ToString(), true) != 0)
				{
					if (ext == Chunks.TYPE.GESTURE.ToString())
					{
						var fileInfo = new FileInfo(filePath);
						if (fileInfo.Length == 0)
						{
							// Ignore empty gesture files
							continue;
						}
					}

					// we don't include LIPSYNC file for these platforms, only use LIPSYNC64 
					if (ext == Chunks.TYPE.LIPSYNC.ToString() && (isPC || isPS4 || isXBOXONE || isPS5 || isXBSX))
					{
						continue;
					}

					var chunkLength = AddChunk(filePath, identifier);
					if (chunkLength > 0)
					{
						chunkContainer.AddOrUpdateChunk(ext.ToUpper(), chunkLength);
						if (ext == Chunks.TYPE.FORMAT.ToString())
						{
							hasFormatChunk = true;
						}
					}
				}
			}

			if (m_loadType != LoadType.Stream && isWave &&
				!hasFormatChunk)
			{
				throw new Exception("Wave missing format chunk - " + filePathNoExt);
			}

			// Only the global object gets a data chunk in streaming banks.
			if (m_loadType == LoadType.Stream &&
				identifier == 0)
			{
				if (File.Exists(dataFilePath))
				{
					var dataChunkLength = AddChunk(dataFilePath, identifier);
					if (dataChunkLength > 0)
					{
						chunkContainer.AddOrUpdateChunk(Chunks.TYPE.DATA.ToString(), dataChunkLength);
					}
				}

				if (File.Exists(midiFilePath))
				{
					var midiChunkLength = AddChunk(midiFilePath, identifier);
					if (midiChunkLength > 0)
					{
						chunkContainer.AddOrUpdateChunk(Chunks.TYPE.MID.ToString(), midiChunkLength);
					}
				}

				if (File.Exists(edlfilepath))
				{
					var textfileLength = AddChunk(edlfilepath, identifier);
					if (textfileLength > 0)
					{
						chunkContainer.AddOrUpdateChunk(Chunks.TYPE.EDL.ToString(), textfileLength);
					}
				}
			}
		}

		private int AddChunk(string path, int identifier)
		{
			var fs = new FileStream(path, FileMode.Open, FileAccess.Read);
			var br = new BinaryReader(fs);

			var len = fs.Length;
			if (len == 0)
			{
				Console.WriteLine(string.Format("Warning: File {0} with 0 length", path));
				return -1;
			}

			var byteBuffer = new byte[len];
			br.Read(byteBuffer, 0, (int)len);
			br.Close();
			fs.Close();

			var ext = Path.GetExtension(path).ToUpper().Remove(0, 1);
			var returnVal = 0;
			var added = false;

			var isDataChunk = string.Compare(ext, Chunks.TYPE.DATA.ToString()) == 0;

			if (isDataChunk && m_loadType == LoadType.Wave)
			{
				// Wave-loaded banks need their wave chunks encrypted individually                
				byteBuffer = m_Encrypter.Encrypt(byteBuffer, true);
			}

			if (isDataChunk ||
				String.CompareOrdinal(ext, Chunks.TYPE.MID.ToString()) == 0 ||
				String.CompareOrdinal(ext, Chunks.TYPE.EDL.ToString()) == 0)
			{
				if (Platform == "XBOX360" || Platform == "XBOXONE" || Platform == "XBOXSERIES")
				{
					// XMA waves must be aligned to 2k boundaries, unless we're loading individual waves from this bank
					returnVal = m_containerBuilder.AddDataChunk(identifier,
																mContainerBuilder.ComputeDataTypeHash(ext),
																byteBuffer,
																(uint)(m_loadType == LoadType.Wave ? 1 : 2048));
				}
				else
				{
					returnVal = m_containerBuilder.AddDataChunk(identifier,
																mContainerBuilder.ComputeDataTypeHash(ext),
																byteBuffer,
																(uint)(m_loadType == LoadType.Wave ? 1 : 16));
				}
				added = true;
			}
			else if (ext.Equals(Chunks.TYPE.MARKERS.ToString()) || ext.Equals(Chunks.TYPE.GESTURE.ToString()))
			{
				returnVal = m_containerBuilder.AddDataChunk(identifier,
															mContainerBuilder.ComputeDataTypeHash(ext),
															byteBuffer,
															4);
				added = true;
			}
			else if (ext.Equals(Chunks.TYPE.LIPSYNC.ToString()))
			{
				returnVal = m_containerBuilder.AddDataChunk(identifier, mContainerBuilder.ComputeDataTypeHash(ext), byteBuffer, 16);
				added = true;
			}
			else if (ext.Equals(Chunks.TYPE.LIPSYNC64.ToString()))
			{
				returnVal = m_containerBuilder.AddDataChunk(identifier, mContainerBuilder.ComputeDataTypeHash(ext), byteBuffer, 16);
				added = true;
			}
			//else if (ext.Equals(Chunks.TYPE.CUSTOM_LIPSYNC.ToString())) // Katz said it was ok to comment out
			//{
			//    returnVal = m_containerBuilder.AddDataChunk(identifier, mContainerBuilder.ComputeDataTypeHash(ext), byteBuffer, 16);
			//    added = true;
			//}
			else if (ext.Equals(Chunks.TYPE.GRANULARGRAINS.ToString()) ||
					 ext.Equals(Chunks.TYPE.GRANULARLOOPS.ToString()) ||
					 ext.Equals(Chunks.TYPE.GRANULARPITCH.ToString()))
			{
				returnVal = m_containerBuilder.AddDataChunk(identifier,
															mContainerBuilder.ComputeDataTypeHash(ext),
															byteBuffer,
															4);
				added = true;
			}
			else if (Enum.GetNames(typeof(Chunks.TYPE)).Contains(ext))
			{
				returnVal = m_containerBuilder.AddDataChunk(identifier,
															mContainerBuilder.ComputeDataTypeHash(ext),
															byteBuffer,
															1);
				added = true;
			}

			if (returnVal == -1)
			{
				throw new Exception(String.Format("Error adding {0} chunk for {1}.", ext, path));
			}

			if (!added)
			{
				return -1;
			}

			return (int)len;
		}

		private void WriteBankProperties()
		{
			if (!m_containerBuilder.Serialize(m_containerPath, m_projectSettings.GetCurrentPlatform().IsBigEndian))
			{
				throw new Exception("Failed to serialise bank " + m_containerPath);
			}

			// load the newly serialized container back up
			var container = new mContainer(m_containerPath);

			// Waves within interleaved streams don't exist as discrete objects
			if (m_loadType != LoadType.Stream)
			{
				foreach (var kvp in m_objectIndex)
				{
					var size = m_packingType == mContainer.NORMAL
						? container.ComputeContiguousObjectSize(kvp.Key)
						: container.ComputeObjectDataSize(kvp.Key);
					if (size != 0)
					{
						m_objectIndex[kvp.Key].BuiltAsset.Size = size;
					}
				}
				m_bankProperties.HeaderSize = container.GetHeaderSize();
			}
			else
			{
				// the header size for a streaming bank includes everything except the streaming data chunk
				var globalObjectId = container.FindObjectId("");
				if (globalObjectId != 0)
				{
					throw new Exception(string.Format(
						"Streaming bank {0} global object id {1} (runtime code expects 0)",
						m_containerPath,
						globalObjectId));
				}

				var dataChunkId = container.FindDataChunkId(globalObjectId, "DATA");

				uint dataTypeHash = 0, dataOffsetBytes = 0, dataSizeBytes = 0;
				container.GetChunkData(globalObjectId,
									   dataChunkId,
									   ref dataTypeHash,
									   ref dataOffsetBytes,
									   ref dataSizeBytes);

				// header size for streaming banks is whatever gets us to the start of the data chunk
				// Note that this will include whatever padding was necessary to align the data 
				// chunk, in practise this should only waste a few bytes so probably isn't worth worrying 
				// about.
				m_bankProperties.HeaderSize = dataOffsetBytes;

				Console.WriteLine("Streaming bank, header size: {0}", dataOffsetBytes);
			}

			m_bankProperties.Size = container.GetSize();
			m_bankProperties.DateTime = DateTime.Now;

			m_bankProperties.MetadataSize = container.ComputeMetadataSize();
			m_bankProperties.AwcMd5 = Utility.FileMd5(m_containerPath);

			// bank properties must be written to build folder, not final output path
			var bankPropertiesPath = InputDirectory.Trim('\\') + ".xml";
			m_bankProperties.Save(bankPropertiesPath);

			// Optional whole-file encryption for bank-loaded containers
			if (m_loadType == LoadType.Bank && m_Encrypter.IsEncrypting)
			{
				EncryptFile(m_containerPath, m_Encrypter);
			}
		}

		#region Nested type: LoadType

		private enum LoadType
		{
			Wave,
			Bank,
			Stream
		}

		#endregion

		static void EncryptFile(string inputFile, IEncrypter encrypter)
		{
			byte[] data;
			using (var f = new FileStream(inputFile, FileMode.Open, FileAccess.Read))
			{
				data = new byte[f.Length];
				f.Read(data, 0, (int)f.Length);
				f.Close();
			}

			data = encrypter.Encrypt(data, true);

			using (var f = new FileStream(inputFile, FileMode.Create, FileAccess.Write))
			{
				f.Write(data, 0, data.Length);
				f.Close();
			}
		}
	}


}
