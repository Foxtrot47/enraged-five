﻿using System;

namespace BankBuilder
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var bankBuilder = new BankBuilder(args);
                bankBuilder.Execute();
                return;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(String.Concat("Error: ", ex.Message));
                Environment.Exit(-1);
            }
        }
    }
}