﻿using System;
using System.Linq;
using System.Xml.Linq;
using audAssetPostBuilderCommon;
using rage;
using rage.ToolLib.Logging;

namespace audWaveSlotBuilder
{
    public class audWaveSlotBuilder : PostBuildBase
    {
        public audWaveSlotBuilder(string[] args) : base(args)
        {
        }

        public override void Run()
        {
            var projectSettings = new audProjectSettings(ProjectSettingsPath);
            projectSettings.SetCurrentPlatformByTag(Platform);

            var builtWaves = XDocument.Load(InputFiles[0]);
            var slotSettingsXml = XDocument.Load(InputFiles[1]);
            var waveSlotSettings = new WaveSlotSettings(slotSettingsXml, builtWaves, projectSettings);

            var totalSize = waveSlotSettings.Slots.Sum(waveSlot => waveSlot.Size);
            if (totalSize <= projectSettings.GetCurrentPlatform().MaxWaveMemory)
            {
                var root = new XElement("WaveSlots");
                foreach (var waveSlot in waveSlotSettings.Slots)
                {
                    waveSlot.SerializeAsWaveSlotObject(root);
                }
                var outputDoc = new XDocument(root);
                outputDoc.Save(OutputFiles[0]);
            }
            else
            {
                throw new Exception(string.Format("Wave memory limit exceeded! Memory limit: {0} Memory used {1}",
                                                  projectSettings.GetCurrentPlatform().MaxWaveMemory,
                                                  totalSize));
            }
        }


    }
}