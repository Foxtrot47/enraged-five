﻿using System;

namespace audWaveSlotBuilder
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var waveSlotBuilder = new audWaveSlotBuilder(args);
                waveSlotBuilder.Run();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                Environment.ExitCode = -1;
            }
        }
    }
}