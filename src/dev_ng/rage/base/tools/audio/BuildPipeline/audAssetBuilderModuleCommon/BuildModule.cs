﻿using System.Xml;
using System.Xml.Linq;
using rage;

namespace audAssetBuilderModuleCommon
{
    using Rockstar.AssetManager.Interfaces;

    public abstract class BuildModule
    {
        public string Name { get; private set; }

        public string DependsOn { get; private set; }

        public bool Init(XmlNode node)
        {
            //pull out Name
            if (node.Attributes["name"] != null &&
                node.Attributes["name"].Value != string.Empty)
            {
                Name = node.Attributes["name"].Value;
            }
            else
            {
                return false;
            }

            if (node.Attributes["dependsOn"] != null &&
                node.Attributes["dependsOn"].Value != string.Empty)
            {
                DependsOn = node.Attributes["dependsOn"].Value;
            }

            return Initialise(node);
        }

        protected abstract bool Initialise(XmlNode node);

        public abstract bool CreateJobs(XElement toolNode,
                                        XElement jobNode,
                                        IAssetManager assetManager,
                                        IChangeList buildChangeList,
                                        audProjectSettings projectSettings,
                                        string pack,
                                        string syncChangelistNumber,
                                        string tempPath);

        public virtual void Quit() {}
    }
}