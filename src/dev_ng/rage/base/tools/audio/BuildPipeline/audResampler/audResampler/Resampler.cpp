
#include "Stdafx.h"
#include "Resampler.h"
#include "samplerate.h"
#include <math.h>
#include "iostream"

enum {SRC_ERR_NO_ERROR = 0};

using namespace std;
#define roundf(a) ((fmod(a,1)<0.5)?floor(a):ceil(a))

#define Clamp(a,min,max) (a>max ? max : a < min ? min : a)

double DitherAndRoundSample16Bit(double fSample)
{
	double fDither = ((double) rand() / (RAND_MAX)) - ((double) rand() / (RAND_MAX));
	fSample += fDither;
	fSample = floor(0.5 + fSample);
	fSample = Clamp(fSample, -32768.0, +32767.0);
	return fSample;
}


int Resampler::Resample(short* data, short** dataOut, int sampleLength, int &dataOutLength, double resamplingRatio, int &loopStartOffset, bool useDither)
{
	int resamplerTargetSamplesIn = sampleLength;
	int resamplerTargetSamplesOut = dataOutLength;

	//Allocating floating-point buffers for resampler.
	float *rawData = new float[resamplerTargetSamplesIn];
	float *resampledData = new float[resamplerTargetSamplesOut];

	//Convert raw samples to floating-point.
	for(int i=0; i<resamplerTargetSamplesIn; i++)
	{
		rawData[i] = (float)data[i] / 32768.0f;
	}

	SRC_DATA src_data;

	src_data.data_in = rawData;
	src_data.data_out = resampledData;

	src_data.input_frames = (long)resamplerTargetSamplesIn;
	src_data.output_frames = (long)resamplerTargetSamplesOut;

	src_data.src_ratio = resamplingRatio;

	int returnVal = src_simple(&src_data, SRC_SINC_BEST_QUALITY, 1);
	
	delete [] rawData;
	rawData = NULL;
	if((src_data.output_frames_gen != src_data.output_frames)||(src_data.input_frames_used != src_data.input_frames))
	{
		delete[] resampledData;
		return -1;
	}

	short *out = new short[src_data.output_frames];
	for(int i=0; i < src_data.output_frames; i++)
	{
		double fSample = resampledData[i] * 32768.0;
		fSample = DitherAndRoundSample16Bit(fSample);
		out[i] = (short)fSample;
	}
	*dataOut = out;
	dataOutLength = (int)src_data.output_frames;

	if(loopStartOffset>0)
	{
		loopStartOffset = (int)roundf((float)loopStartOffset * src_data.src_ratio);
	}
	
	delete [] resampledData;

	return returnVal;
}

bool Resampler::AlignLoop(short *data, short **dataOut, int numSamples, int &numSamplesOut, 
						  unsigned int sampleRate, unsigned int &sampleRateOut, int &loopStartOffset, 
						  unsigned int alignmentSamples, bool useDither)
{
	unsigned int loopLengthSamples = numSamples - loopStartOffset;

	//Check that loop is integrally divisible by blockSize
	unsigned int loopPadSamples = alignmentSamples - (loopLengthSamples % alignmentSamples);

	if((loopPadSamples > 0) && loopPadSamples < alignmentSamples)
	{
		//resample wave to fit the block alignment
		unsigned int resampledLoopLengthSamples = 0;


		if(loopLengthSamples > alignmentSamples && loopPadSamples > alignmentSamples/2)
		{
			//resample down
			resampledLoopLengthSamples = loopLengthSamples - (alignmentSamples - loopPadSamples);
		}
		else
		{
			//resample up
			resampledLoopLengthSamples = loopLengthSamples + loopPadSamples;
		}

		float resamplingRatio = (float)resampledLoopLengthSamples/(float)loopLengthSamples;

		//Ensure that resampling will not result in a sample rate above 48kHz
		if((int)ceil(resamplingRatio * (float)sampleRate)>48000)
		{
			//resample down instead
			resampledLoopLengthSamples = loopLengthSamples - (alignmentSamples - loopPadSamples);
			resamplingRatio = (float)resampledLoopLengthSamples/(float)loopLengthSamples;
		}		

		long resamplerTargetSamplesIn = (long)ceil(resampledLoopLengthSamples / resamplingRatio)+1;

		float *loopData = new float[resamplerTargetSamplesIn];
		float *resampledLoopData = new float[resampledLoopLengthSamples];

		//convert loop samples to floating point
		for(int i=0; i<resamplerTargetSamplesIn; i++)
		{
			loopData[i] = (float)data[loopStartOffset + i%loopLengthSamples] / 32767.0f;
		}

		int resamplerTargetSamplesOut = resampledLoopLengthSamples;

		SRC_DATA src_data;

		src_data.data_in = loopData;
		src_data.data_out = resampledLoopData;

		src_data.input_frames = (long)resamplerTargetSamplesIn;
		src_data.output_frames = (long)resamplerTargetSamplesOut;

		src_data.src_ratio = (double)resamplingRatio;

		int returnVal = src_simple(&src_data, SRC_SINC_BEST_QUALITY, 1);

		if(returnVal != SRC_ERR_NO_ERROR)
		{
			printf("Error: resampler error %d: %s\n", returnVal, src_strerror(returnVal));
			printf("Ratio: %f (%d/%d) %d\n", resamplingRatio, resampledLoopLengthSamples, loopLengthSamples, sampleRate);
			printf("NumSamples: %d loopStartOffset: %d\n",numSamples, loopStartOffset);
			delete[] resampledLoopData;
			return false;
		}

		if((src_data.output_frames_gen != src_data.output_frames)||(src_data.input_frames_used != src_data.input_frames) 
			||(src_data.output_frames_gen % alignmentSamples !=0))
		{
			printf("Error: aligning loop: %d,%d,%d,%d,%d\n", src_data.output_frames_gen, src_data.output_frames, src_data.input_frames_used, src_data.input_frames, alignmentSamples);
			delete[] resampledLoopData;
			return false;
		}

		unsigned int resampledPreloopLengthSamples = 0;
		float *resampledPreloopData = 0;

		if(loopStartOffset > 0)
		{
			//Resample preloop
			resampledPreloopLengthSamples = (unsigned int)roundf((float)loopStartOffset * resamplingRatio);

			//Check resampledPreLoopLengthSamples is integrally divisible by blocksize
			unsigned int resampledPreloopPadSamples = alignmentSamples - (resampledPreloopLengthSamples % alignmentSamples);

			if(resampledPreloopPadSamples == alignmentSamples)
			{
				resampledPreloopPadSamples = 0;
			}

			unsigned int preloopPadding = resampledPreloopPadSamples;

			resamplerTargetSamplesIn = (long)ceilf(resampledPreloopLengthSamples / resamplingRatio)+1;

			//Allocating floating-point buffers to resampler
			float *preloopData = new float[resamplerTargetSamplesIn];
			resampledPreloopData = new float[resampledPreloopLengthSamples + resampledPreloopPadSamples];
			if(resampledPreloopPadSamples>0)
			{
				//zero pad the start
				memset(resampledPreloopData, 0, resampledPreloopPadSamples * sizeof(float));
			}			

			//Convert preloop samples to floating point
			for(int i=0; i<resamplerTargetSamplesIn; i++)
			{
				preloopData[i] = (float)data[i]/32767.0f;
			}
			
			resamplerTargetSamplesOut = (long)resampledPreloopLengthSamples;

			//resample preloop
			src_data.data_in = preloopData;
			src_data.data_out = resampledPreloopData + resampledPreloopPadSamples;

			src_data.input_frames = (long)resamplerTargetSamplesIn;
			src_data.output_frames = (long)resamplerTargetSamplesOut;

			src_data.src_ratio = resamplingRatio;

			int returnVal = src_simple(&src_data, SRC_SINC_BEST_QUALITY, 1);
			if(returnVal != SRC_ERR_NO_ERROR)
			{
				printf("Error: resampler error %d: %s\n", returnVal, src_strerror(returnVal));
				delete[] resampledLoopData;
				return false;
			}

			delete[] preloopData;

			if((src_data.output_frames_gen != src_data.output_frames) || (src_data.input_frames_used!=src_data.input_frames_used))
			{
				printf("Error: aligning preloop: %d,%d,%d,%d,%d", src_data.output_frames_gen, src_data.output_frames, src_data.input_frames_used, src_data.input_frames, alignmentSamples);

				delete[] resampledPreloopData;
				delete[] resampledLoopData;
				return false;
			}

			resampledPreloopLengthSamples += resampledPreloopPadSamples;
		}

		numSamplesOut = resampledPreloopLengthSamples + resampledLoopLengthSamples;
		
		if(numSamplesOut % alignmentSamples != 0)
		{
			printf("Error: failed to align: %d,%d,%d", resampledPreloopLengthSamples, resampledLoopLengthSamples, alignmentSamples);
			return false;
		}

		loopStartOffset = resampledPreloopLengthSamples;
		sampleRateOut = (unsigned int)ceil((float)sampleRate * resamplingRatio);

		//convert resampled data back
		short *out = new short[numSamplesOut*2];
		for(unsigned int i=0; i<resampledPreloopLengthSamples; i++)
		{
			double fSample = resampledPreloopData[i] * 32768.0;
			fSample = DitherAndRoundSample16Bit(fSample);
			out[i] = (short)fSample;
		}
		for(unsigned int i=0; i<resampledLoopLengthSamples; i++)
		{
			double fSample = resampledLoopData[i] * 32768.0;
			fSample = DitherAndRoundSample16Bit(fSample);
			out[resampledPreloopLengthSamples+i] = (short)fSample;
		}

		*dataOut = out;

		if(resampledPreloopData)
		{
			delete[] resampledPreloopData;
		}
		if(resampledLoopData)
		{
			delete[] resampledLoopData;
		}
	}
	else if(loopStartOffset > 0)
	{
		//check preloop is integrally divisible by block size
		unsigned int preloopPadSamples = alignmentSamples-(loopStartOffset % alignmentSamples);
		if((preloopPadSamples >0) && preloopPadSamples !=alignmentSamples)
		{
			short *out = new short[numSamples + preloopPadSamples];
			memset(out,0,preloopPadSamples * sizeof(short));
			memcpy(out+preloopPadSamples,data,numSamples * sizeof(short));
			*dataOut = out;
			numSamplesOut = numSamples+preloopPadSamples;
			loopStartOffset += preloopPadSamples;
		}
	}
	return true;
}

void Resampler::DeleteBuffer(short *data)
{
	delete[] data;
}