#include "stdafx.h"
#include "MResampler.h"

using namespace System;
using namespace System::IO;
using namespace System::Collections;
using namespace System::Diagnostics;

int main(array<System::String ^> ^args)
{
	try
	{
		MResampler^ resampler = gcnew MResampler(args);
		resampler->Execute();
	}
	catch(Exception^ e)
	{
		TextWriter^ tw = Console::Error::get();
		EventLog::WriteEntry("Audio Build", e->ToString());
		tw->WriteLine(e->ToString());
		return -1;
	}
	return 0;
}
