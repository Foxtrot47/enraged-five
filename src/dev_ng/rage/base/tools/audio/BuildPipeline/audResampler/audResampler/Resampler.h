#pragma once

public class Resampler
{
	public:
		int Resample(short* data, short** dataOut, int numSamplesIn, int &numSamplesOut, double sampleRate, int &loopStartOffset, bool useDither);
		bool AlignLoop(short * data, short** dataOut, int numSamplesIn, int &numSamplesOut, unsigned int sampleRate, unsigned int &sampleRateOut, int &loopStartOffset, unsigned int alignmentSamples, bool useDither);

		void DeleteBuffer(short *buffer);
};

