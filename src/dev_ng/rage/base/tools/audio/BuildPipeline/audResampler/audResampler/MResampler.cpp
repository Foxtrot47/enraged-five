// This is the main DLL file.

#include "stdafx.h"
#include "MResampler.h"

using namespace System::Collections;
using namespace System::Runtime::InteropServices;
using namespace System::IO;
using namespace System::Collections::Generic;
using namespace rage;

// For MessageBoxA function used in RageCore
#pragma comment(lib, "user32.lib")
// Various win32 functions used by bkManager code
#pragma comment(lib, "advapi32.lib")
#pragma comment(lib, "comdlg32.lib")
#pragma comment(lib, "gdi32.lib")

#undef GetCurrentDirectory

void MResampler::Run()
{
	audProjectSettings^ projectSettings = gcnew audProjectSettings(ProjectSettingsPath);
	projectSettings->SetCurrentPlatformByTag(Platform);

	m_AlignmentSamples = projectSettings->GetCurrentPlatform()->AlignmentSamples;
	m_Encoder = projectSettings->GetCurrentPlatform()->Encoder;

	if(!WaveProperties)
	{
		throw gcnew Exception(String::Concat("No wave properties file found for ",WaveFile->FileName));
	}		

	// Populate the source asset properties
	WaveProperties->SourceAsset->SampleRate = (int)WaveFile->Format->SampleRate;
	WaveProperties->SourceAsset->Size = WaveFile->Data->RawData->Length;
	WaveProperties->SourceAsset->NumberOfSamples = WaveFile->Data->NumSamples;

	// Set a default value for this
	WaveProperties->BuiltAsset->NumberOfSamplesPostResampling = WaveFile->Data->NumSamples;

	// Is this really a platform based value, it's more of a compression type value? 
	// Currently set in projectsettings in platform specific section.
	if(String::Compare(WaveProperties->BuiltAsset->CompressionType, "MP3", true) == 0)
	{
		m_AlignmentSamples = 1152;
	}
	
	// if there was no sampleRate tag then use the source sample rate
	if (!WaveProperties->BuiltAsset->HasValidSampleRate())
	{
		WaveProperties->BuiltAsset->SampleRate = WaveProperties->SourceAsset->SampleRate;
	}

	if(WaveFile->Data->NumSamples > (unsigned int)m_AlignmentSamples)
	{
		Resample(WaveFile,WaveProperties);

		if((WaveFile->Sample && WaveFile->Sample->Loops->Count == 1) && 	
			(String::Compare(projectSettings->GetCurrentPlatform()->Encoder,"PCMENCODER",true)!=0 &&
			WaveProperties->BuiltAsset->Compression != 101))
			//&& String::Compare(projectSettings->GetCurrentPlatform()->Encoder,"XMAENCODER",true)!=0)
		{
			AlignLoop(WaveFile,WaveProperties);
		}
	}
}

void MResampler::Resample(bwWaveFile^ waveFile, audAssetBuilderProperties::WaveProperties^ waveProperties)
{
	if(!waveProperties)
	{
		throw gcnew Exception(String::Concat("No wave properties file found for ",waveFile->FileName));
	}

	int targetSampleRate = waveProperties->BuiltAsset->SampleRate == 0 ? waveProperties->SourceAsset->SampleRate: waveProperties->BuiltAsset->SampleRate;

	//ensure its a valid sample rate for the ps3encoder
	if(StreamingBlockBytes != 0 && (String::Compare(m_Encoder,"PS3ENCODER",true) == 0 || String::Compare(waveProperties->BuiltAsset->CompressionType, "MP3", true) == 0))
	{
		int sampleRate = g_ValidSampleRates[0];
		
		// TODO: Investigate this, as we're clamping to 32kHz as a minimum to ensure 1152 sample frames
		// this is really a temp work around, since support for 576 sample frames would allow us to utilize
		// sample rates all the way down to 8kHz.
		for(int i=6; i<g_NumValidSampleRates; i++)
		{
			if(targetSampleRate <= g_ValidSampleRates[i])
			{
				sampleRate = g_ValidSampleRates[i];
				break;
			}
		}

		targetSampleRate = sampleRate;
		waveProperties->BuiltAsset->SampleRate = targetSampleRate;
	}

	array<unsigned char>^ rawData = waveFile->Data->RawData;
	pin_ptr<unsigned char> pinnedRawData = &rawData[0];

	int rawSampleRate = waveFile->Format->SampleRate;

	if(targetSampleRate == rawSampleRate && (String::Compare(m_Encoder,"PCMENCODER",true) == 0 &&
		!(String::Compare(waveProperties->BuiltAsset->CompressionType, "MP3", true) == 0 || String::Compare(waveProperties->BuiltAsset->CompressionType, "ATRAC9", true) == 0 ||
		String::Compare(waveProperties->BuiltAsset->CompressionType, "XMA2", true) == 0)))
	{
		return;
	}

	int numSamples = waveFile->Data->NumSamples;

	double resamplingRatio = (double)targetSampleRate / (double)rawSampleRate;

	short *dataOut = NULL;
	int numSamplesOut = (int)Math::Round(resamplingRatio * numSamples);
	int loopStartOffset = 0;

	if(waveFile->Sample && waveFile->Sample->Loops->Count == 1)
	{
		loopStartOffset = waveFile->Sample->Loops[0]->Start;
	}	

	if(String::Compare(m_Encoder,"XMAENCODER",true) == 0 || String::Compare(waveProperties->BuiltAsset->CompressionType, "XMA2", true) == 0)
	{
		unsigned int padSamples = m_AlignmentSamples - (numSamplesOut % m_AlignmentSamples);

		if((padSamples > 0) && padSamples < (unsigned int)m_AlignmentSamples)
		{
			//resample wave to fit the block alignment
			unsigned int resampledLengthSamples = 0;


			if(padSamples > (unsigned)m_AlignmentSamples/2)
			{
				//resample down
				numSamplesOut = numSamplesOut - (m_AlignmentSamples - padSamples);
			}
			else
			{
				//resample up
				numSamplesOut = numSamplesOut + padSamples;
			}
			
		}
	}

	resamplingRatio = ((double)numSamplesOut/(double)numSamples) + (0.5/(double)numSamples);

	float f = (float)resamplingRatio;

	if(resamplingRatio == 1)
	{
		//Do nothing, the sample rate does not change
		return;
	}

	int returnVal = m_Resampler->Resample((short*)pinnedRawData,
											&dataOut,
											numSamples,
											numSamplesOut,
											resamplingRatio, 
											loopStartOffset, 
											UseDither);

	if(returnVal!=0)
	{
		throw gcnew Exception(String::Format("Resampler Error: {0} numSamples {1} numSamplesOut: {2} ratio: {3} loopStartOffset: {4}", returnVal, numSamples, numSamplesOut, resamplingRatio, loopStartOffset));
	}

	if((String::Compare(m_Encoder,"XMAENCODER",true) == 0 || String::Compare(waveProperties->BuiltAsset->CompressionType, "XMA2", true) == 0) && (numSamplesOut % m_AlignmentSamples != 0))
	{
		throw gcnew Exception("Samples are not 128 aligned");
	}

	unsigned char* outputchars = (unsigned char*)dataOut;
	array<unsigned char>^ outputData = gcnew array<unsigned char>(numSamplesOut*2);
	for(int i=0; i<numSamplesOut*2; i++)
	{
		outputData[i] = outputchars[i];
	}

	int sampleRateOut = (int)Math::Floor((float)rawSampleRate * resamplingRatio);
	WaveProperties->BuiltAsset->NumberOfSamplesPostResampling = numSamplesOut;

	//create new data and format chunks
	waveFile->Data = gcnew bwDataChunk(outputData, numSamplesOut);
	waveFile->Format = gcnew bwFormatChunk(waveFile->Format->CompressionCode,
											waveFile->Format->NumChannels,
											sampleRateOut,
											sampleRateOut*2,
											waveFile->Format->BlockAlign,
											waveFile->Format->SignificantBitsPerSample,
											waveFile->Format->ExtraFormatBytes);

	for(int i=0; i< waveFile->Markers->Count ; i++)
	{
		bwRegion^ region = dynamic_cast<bwRegion^>(waveFile->Markers[i]);
		if(region)
		{
			region->Length = (int)Math::Floor(region->Length * resamplingRatio);
		}
		waveFile->Markers[i]->Position = (int)Math::Floor(waveFile->Markers[i]->Position * resamplingRatio);
	}

	if(waveFile->Sample && waveFile->Sample->Loops->Count == 1)
	{
		waveFile->Sample->Loops[0]->Start = loopStartOffset;
		waveFile->Sample->Loops[0]->End = numSamplesOut-1;
	}

	if(dataOut)
	{
		m_Resampler->DeleteBuffer(dataOut);
	}
}

void MResampler::AlignLoop(bwWaveFile^ waveFile, audAssetBuilderProperties::WaveProperties^ waveProperties)
{
	//in
	int numSamples = waveFile->Data->NumSamples;	
	int loopStartOffset = 0;
	if(waveFile->Sample && waveFile->Sample->Loops->Count == 1)
	{
		loopStartOffset = waveFile->Sample->Loops[0]->Start;
	}
	unsigned int sampleRate = waveFile->Format->SampleRate;
	array<unsigned char>^ rawData = waveFile->Data->RawData;
	pin_ptr<unsigned char> pinnedRawData = &rawData[0];

	//out
	short* dataOut = 0;
	int numSamplesOut = 0;
	unsigned int sampleRateOut = 0;

	if(!m_Resampler->AlignLoop((short*)pinnedRawData,&dataOut,numSamples,numSamplesOut,sampleRate,sampleRateOut,loopStartOffset,m_AlignmentSamples, UseDither))
	{
		throw gcnew Exception("Failed to align loop");
	}

	//if !data out the loop is already aligned
	if(dataOut)
	{
		unsigned char* outputchars = (unsigned char*)dataOut;
		array<unsigned char>^ outputData = gcnew array<unsigned char>(numSamplesOut*2);
		for(int i=0; i<numSamplesOut*2; i++)
		{
			outputData[i] = outputchars[i];
		}

		//create new data and format chunks
		waveFile->Data = gcnew bwDataChunk(outputData, numSamplesOut);
		waveFile->Format = gcnew bwFormatChunk(	waveFile->Format->CompressionCode,
												waveFile->Format->NumChannels,
												sampleRateOut,
												sampleRateOut*2,
												waveFile->Format->BlockAlign,
												waveFile->Format->SignificantBitsPerSample,
												waveFile->Format->ExtraFormatBytes);

		float resamplingRatio = (float)sampleRateOut/(float)(sampleRate);

		for(int i=0; i< waveFile->Markers->Count ; i++)
		{
			bwRegion^ region = dynamic_cast<bwRegion^>(waveFile->Markers[i]);
			if(region)
			{
				region->Length = (int)Math::Floor(region->Length * resamplingRatio);
			}
			waveFile->Markers[i]->Position = (int)Math::Floor(waveFile->Markers[i]->Position * resamplingRatio);
		}

		if(waveFile->Sample && waveFile->Sample->Loops->Count == 1)
		{
			waveFile->Sample->Loops[0]->Start = loopStartOffset;
			waveFile->Sample->Loops[0]->End = numSamplesOut-1;

		}

		if(dataOut)
		{
			m_Resampler->DeleteBuffer(dataOut);
		}
	}
}
