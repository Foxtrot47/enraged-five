#pragma once

#include "Resampler.h"

using namespace System;
using namespace audAssetBuilderCommon;
using namespace audAssetBuilderProperties;
using namespace Wavelib;

const int g_NumValidSampleRates = 9;
const int g_ValidSampleRates[g_NumValidSampleRates] =
{
	8000,
	11025,
	12000,
	16000,
	22050,
	24000,
	32000,
	44100,
	48000
};

public ref class MResampler: public WaveBuildBase
	{
	private:
		Resampler* m_Resampler;
		void Resample(bwWaveFile^ waveFile, audAssetBuilderProperties::WaveProperties^ waveProperties);
		void AlignLoop(bwWaveFile^ waveFile, audAssetBuilderProperties::WaveProperties^ waveProperties);
		
		int m_AlignmentSamples;
		String^ m_Encoder;

	public:

		MResampler(array<System::String ^> ^args)
			:WaveBuildBase(args)
		{ 
			m_Resampler = new Resampler();
		};

		virtual void Run()override;

		~MResampler(){
			delete(m_Resampler);
		};


	};

