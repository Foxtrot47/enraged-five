﻿using System;
using System.Diagnostics;
using System.Linq;

namespace audVisemeBuilder
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var visemeBuilder = new VisemeBuilder(args);
                visemeBuilder.Execute();
                Environment.ExitCode = 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                var argsCombined = args.Aggregate(string.Empty, (current, s) => current + (s + " "));
                EventLog.WriteEntry("Audio Build", "VisemeBuilder: " + argsCombined + " - " + ex);
                Environment.ExitCode = -1;
            }
        }
    }
}