﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VisemeBuilder.cs" company="Rockstar North>
//   
// </copyright>
// <summary>
//   The viseme builder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace audVisemeBuilder
{
    using System;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Text.RegularExpressions;

    using audAssetBuilderCommon;

    /// <summary>
    /// The Viseme builder.
    /// </summary>
    public class VisemeBuilder : WaveBuildBase
    {
        #region Fields

        /// <summary>
        /// The temp folder.
        /// </summary>
        private readonly string tempFolder = string.Empty;

        /// <summary>
        /// The dic name.
        /// </summary>
        private string dicName = string.Empty;

        /// <summary>
        /// The asset viseme path with no extension.
        /// </summary>
        private string assetVisemePathNoExt;

        /// <summary>
        /// The src asset path.
        /// </summary>
        private string srcAssetPath;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VisemeBuilder"/> class.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public VisemeBuilder(string[] args)
            : base(args)
        {
            this.tempFolder = this.OutputDirectory;

            // Remove any spaces and odd characters as rexFaceFxRage won't handle them
            var pathBuilder = new StringBuilder(300);
            var shortPathName = GetShortPathName(this.tempFolder, pathBuilder, 300);
            if (shortPathName != 0)
            {
                this.tempFolder = pathBuilder.ToString();
            }
            else
            {
                Console.WriteLine("ERROR: couldn't resolve output path " + this.tempFolder);
            }

            this.srcAssetPath = this.WaveProperties.SourceAsset.Path;

            // Determine the asset viseme path (with no extension)
            this.assetVisemePathNoExt = Path.ChangeExtension(this.srcAssetPath, null);
            this.assetVisemePathNoExt = this.assetVisemePathNoExt.Replace("\\", "/");
            this.assetVisemePathNoExt = Regex.Replace(this.assetVisemePathNoExt, "/WAVES/", "/LIPSYNCANIMS/", RegexOptions.IgnoreCase);

            var isPs3 = string.Compare(this.Platform, "Playstation 3", StringComparison.OrdinalIgnoreCase) == 0
                          || string.Compare(this.Platform, "PS3", StringComparison.OrdinalIgnoreCase) == 0;
            var isPC = string.Compare(this.Platform, "PC", StringComparison.OrdinalIgnoreCase) == 0;
            var isPS4 = string.Compare(this.Platform, "PS4", StringComparison.OrdinalIgnoreCase) == 0;
            var isXBOXONE = string.Compare(this.Platform, "XBOXONE", StringComparison.OrdinalIgnoreCase) == 0;
            var isPS5 = string.Compare(this.Platform, "PS5", StringComparison.OrdinalIgnoreCase) == 0;
            var isXBS = string.Compare(this.Platform, "XBOXSERIES", StringComparison.OrdinalIgnoreCase) == 0;

            if (isPs3)
            {
                this.dicName = this.assetVisemePathNoExt + ".ccd";
            }
            else if (isPC || isPS4 || isXBOXONE || isPS5 || isXBS)
            {
                this.dicName = this.assetVisemePathNoExt + ".ycd";
            }
            else
            {
                this.dicName = this.assetVisemePathNoExt + ".xcd";
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Run the Viseme builder.
        /// </summary>
        public override void Run()
        {
            var pathNoExt = Path.GetFileNameWithoutExtension(this.srcAssetPath);
            var lipsyncPath = this.tempFolder + pathNoExt + ".lipsync";
            var lipsync64Path = this.tempFolder + pathNoExt + ".lipsync64";
            var customMarkerPath = this.assetVisemePathNoExt + ".CUSTOM_LIPSYNC";
            var newCustomMarkerPath = this.tempFolder + pathNoExt + ".CUSTOM_LIPSYNC";

            //remove 32bit lipsync support from pipeline
            /*
            if (File.Exists(this.dicName))
            {
                var info = new FileInfo(this.dicName);
                if (info.Length == 0)
                {
                    return;
                }

                Console.WriteLine("Copying file " + this.dicName + " to " + lipsyncPath);
                File.Copy(this.dicName, lipsyncPath, true);
                File.SetAttributes(lipsyncPath, FileAttributes.Normal);
            }
            else
            {
                Console.WriteLine("Failed to find anim file: " + this.dicName);
                return;
            }
             * */

            if (string.Compare(this.Platform, "PC", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(this.Platform, "PS4", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(this.Platform, "XBOXONE", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(this.Platform, "PS5", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(this.Platform, "XBOXSERIES", StringComparison.OrdinalIgnoreCase) == 0)
            {
                this.dicName = this.dicName.Replace(".wcd", ".ycd");
                if (File.Exists(this.dicName))
                {
                    var info = new FileInfo(this.dicName);
                    if (info.Length == 0)
                    {
                        return;
                    }

                    Console.WriteLine("Copying file " + this.dicName + " to " + lipsync64Path);
                    File.Copy(this.dicName, lipsync64Path, true);
                    File.SetAttributes(lipsync64Path, FileAttributes.Normal);
                }
                else
                {
                    Console.WriteLine("Failed to find anim file: " + this.dicName);
                    return;
                }
            }

            if (File.Exists(customMarkerPath))
            {
                Console.WriteLine("Copying file " + customMarkerPath + " to " + newCustomMarkerPath);
                File.Copy(customMarkerPath, newCustomMarkerPath, true);
                File.SetAttributes(newCustomMarkerPath, FileAttributes.Normal);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the short path name.
        /// </summary>
        /// <param name="pathName">
        /// The path name.
        /// </param>
        /// <param name="shortName">
        /// The short name.
        /// </param>
        /// <param name="cbShortName">
        /// The length.
        /// </param>
        /// <returns>
        /// The short path name <see cref="int"/>.
        /// </returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern int GetShortPathName(string pathName, StringBuilder shortName, int cbShortName);

        #endregion
    }
}