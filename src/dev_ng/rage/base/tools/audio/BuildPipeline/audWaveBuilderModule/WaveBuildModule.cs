﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WaveBuildModule.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The wave builder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Threading;

namespace WaveBuilderModule
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.XPath;

    using Rockstar.AssetManager.Interfaces;

    using audAssetBuilderModuleCommon;

    using audAssetBuilderProperties;

    using rage;
    using rage.ToolLib;
    using rage.ToolLib.ProcessTimer;
    using rage.ToolLib.WavesFiles;

    /// <summary>
    /// The wave builder.
    /// </summary>
    public class WaveBuilder : BuildModule
    {
        // modules
        #region Fields

        /// <summary>
        /// The m_ base working path.
        /// </summary>
        private string m_BaseWorkingPath;

        /// <summary>
        /// The m_ custom facial anim path.
        /// </summary>
        private string m_CustomFacialAnimPath;

        /// <summary>
        /// The m_ cutscene export data path.
        /// </summary>
        private string m_CutsceneExportDataPath;

        /// <summary>
        /// The m_ cutscene stripper exe path.
        /// </summary>
        private string m_CutsceneStripperExePath;

        /// <summary>
        /// The m_ should run cutscene stripper.
        /// </summary>
        private bool m_ShouldRunCutsceneStripper;

        /// <summary>
        /// The m_asset manager.
        /// </summary>
        private IAssetManager m_assetManager;

        /// <summary>
        /// The m_autogen banks being deleted.
        /// </summary>
        private List<string> m_autogenBanksBeingDeleted;

        /// <summary>
        /// The m_bank builder exe path.
        /// </summary>
        private string m_bankBuilderExePath;

        /// <summary>
        /// The m_build folder.
        /// </summary>
        private string m_buildFolder;

        // Wave XML & paths
        /// <summary>
        /// The m_built waves.
        /// </summary>
        private XDocument m_builtWaves;

        /// <summary>
        /// The m_built waves folder.
        /// </summary>
        private string m_builtWavesFolder;

        /// <summary>
        /// The m_built waves path.
        /// </summary>
        private string m_builtWavesPath;

        /// <summary>
        /// The m_change list.
        /// </summary>
        private IChangeList m_changeList;

        /// <summary>
        /// The m_encoder exe path.
        /// </summary>
        private string m_encoderExePath;

        /// <summary>
        /// The m_extra parameters.
        /// </summary>
        private string m_extraParameters;

        /// <summary>
        /// The m_gesture exe path.
        /// </summary>
        private string m_gestureExePath;

        /// <summary>
        /// The m_granular exe path.
        /// </summary>
        private string m_granularExePath;

        /// <summary>
        /// The m_job node.
        /// </summary>
        private XElement m_jobNode;

        // List of affected Banks
        /// <summary>
        /// The m_modified banks.
        /// </summary>
        private List<string> m_modifiedBanks;

        /// <summary>
        /// The m_peak normalise exe path.
        /// </summary>
        private string m_peakNormaliseExePath;

        /// <summary>
        /// The m_project settings.
        /// </summary>
        private audProjectSettings m_projectSettings;

        /// <summary>
        /// The m_resample exe path.
        /// </summary>
        private string m_resampleExePath;

        /// <summary>
        /// The m_stream builder exe path.
        /// </summary>
        private string m_streamBuilderExePath;

        /// <summary>
        /// The m_sync changelist number.
        /// </summary>
        private string m_syncChangelistNumber;

        /// <summary>
        /// The m_temp path.
        /// </summary>
        private string m_tempPath;

        /// <summary>
        /// The m_tools node.
        /// </summary>
        private XElement m_toolsNode;

        /// <summary>
        /// The m_viseme exe path.
        /// </summary>
        private string m_visemeExePath;

        /// <summary>
        /// The m_xml builder exe path.
        /// </summary>
        private string m_xmlBuilderExePath;

		private string m_edlBuilderExePath;

        /// <summary>
        /// A list of banks in the CUTSCENE_ONLY pack that no longer have associated banks in CUTSCENE and so need to be deleted.
        /// </summary>
        private List<XmlNode> m_CutsceneOnlyBanksToDelete;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Overrides the method in the base class, allows the module to add tools and tasks to the xge xml document
        /// </summary>
        /// <param name="toolNode">
        /// node passed in from the xge xml document
        /// </param>
        /// <param name="jobNode">
        /// "project node passed in from xge xml document
        /// </param>
        /// <param name="assetManager">
        /// assetmanager for current user
        /// </param>
        /// <param name="buildChangeList">
        /// The changelist being used during this build
        /// </param>
        /// <param name="projectSettings">
        /// project settings for current project
        /// </param>
        /// <param name="pack">
        /// The pack being built, an empty string signifies that we aren't building a particular pack
        /// </param>
        /// <param name="syncChangelistNumber">
        /// The changelist to sync to
        /// </param>
        /// <param name="tempPath">
        /// Temp path to use during process (optional - will use default if not specified)
        /// </param>
        /// <returns>
        /// bool to determine success
        /// </returns>
        public override bool CreateJobs(
            XElement toolNode, 
            XElement jobNode, 
            IAssetManager assetManager, 
            IChangeList buildChangeList, 
            audProjectSettings projectSettings, 
            string pack, 
            string syncChangelistNumber, 
            string tempPath)
        {
            ProcessTimer.StartTimer("CreateJobs (override)");
            this.m_projectSettings = projectSettings;
            this.m_assetManager = assetManager;
            this.m_changeList = buildChangeList;
            this.m_toolsNode = toolNode;
            this.m_jobNode = jobNode;
            this.m_syncChangelistNumber = syncChangelistNumber;
            this.m_tempPath = tempPath;

            this.m_BaseWorkingPath = this.m_assetManager.GetLocalPath(this.m_projectSettings.GetWaveInputPath());

            var pendingWavesFiles = this.LoadXml(pack, out this.m_builtWavesFolder);
            this.ParseXml(pendingWavesFiles);
            this.CreateTools();
            this.CreateJobs();
            ProcessTimer.StopTimer("CreateJobs (override)");
            ProcessTimer.SaveTimings();
            return true;
        }

        /// <summary>
        /// The quit.
        /// </summary>
        public override void Quit()
        {
            if (!string.IsNullOrEmpty(this.m_builtWavesFolder))
            {
                var splitter = new WavesFileSplitter();
                splitter.Run(this.m_builtWavesPath, this.m_builtWavesFolder);
            }

            var pendingCutsceneAutoXml = (this.m_builtWavesFolder + "CUTSCENE_MASTERED_ONLY.XML").Replace(
                "Built", "Pending");
            if (File.Exists(pendingCutsceneAutoXml))
            {
                File.Delete(pendingCutsceneAutoXml);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initialises various variable depending on the xml specified in the projectSettings.
        /// </summary>
        /// <param name="node">
        /// </param>
        /// <returns>
        /// bool to determine success
        /// </returns>
        protected override bool Initialise(XmlNode node)
        {
            ProcessTimer.RecordTimings = true;
            ProcessTimer.OutputPath = Path.Combine(Path.GetTempPath(), "WaveBuildModule_Timings.csv");
            ProcessTimer.StartTimer("Initialize");
            this.m_modifiedBanks = new List<string>();
            this.m_autogenBanksBeingDeleted = new List<string>();
            this.m_ShouldRunCutsceneStripper = false;

            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "Gesture":
                        this.m_gestureExePath = child.InnerText;
                        break;
                    case "Viseme":
                        this.m_visemeExePath = child.InnerText;
                        break;
                    case "CutsceneStripper":
                        this.m_CutsceneStripperExePath = child.InnerText;
                        this.m_ShouldRunCutsceneStripper = true;
                        break;
                    case "CutsceneExportPath":
                        this.m_CutsceneExportDataPath = child.InnerText;
                        break;
                    case "Granular":
                        this.m_granularExePath = child.InnerText;
                        break;
                    case "Resample":
                        this.m_resampleExePath = child.InnerText;
                        break;
                    case "PeakNormalise":
                        this.m_peakNormaliseExePath = child.InnerText;
                        break;
                    case "Encoder":
                        this.m_encoderExePath = child.InnerText;
                        break;
                    case "StreamBuilder":
                        this.m_streamBuilderExePath = child.InnerText;
                        break;
                    case "BankBuilder":
                        this.m_bankBuilderExePath = child.InnerText;
                        break;
                    case "XMLBuilder":
                        this.m_xmlBuilderExePath = child.InnerText;
                        break;
					case "EDLBuilder":
						this.m_edlBuilderExePath = child.InnerText;
						break;
                    case "CustomFacialAnim":
                        this.m_CustomFacialAnimPath = child.InnerText;
                        break;
                    case "BuildFolder":
                        this.m_buildFolder = child.InnerText;
                        if (!this.m_buildFolder.EndsWith("\\"))
                        {
                            this.m_buildFolder = this.m_buildFolder + "\\";
                        }

                        break;
                    case "ExtraParameters":
                        this.m_extraParameters = child.InnerText;
                        break;
                }
            }

            m_CutsceneOnlyBanksToDelete = new List<XmlNode>();

            ProcessTimer.StopTimer("Initialize");
            return !string.IsNullOrEmpty(this.m_resampleExePath) && !string.IsNullOrEmpty(this.m_peakNormaliseExePath)
                    && !string.IsNullOrEmpty(this.m_bankBuilderExePath)
                    && !string.IsNullOrEmpty(this.m_xmlBuilderExePath);
        }

        /// <summary>
        /// The find tag.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="tagName">
        /// The tag name.
        /// </param>
        /// <returns>
        /// The <see cref="XElement"/>.
        /// </returns>
        private static XElement FindTag(XElement element, string tagName)
        {
            ProcessTimer.StartTimer("FindTag");
            foreach (var e in element.AncestorsAndSelf())
            {
                foreach (var tag in e.Elements("Tag"))
                {
                    if (String.Compare(tag.Attribute("name").Value, tagName, StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        ProcessTimer.StopTimer("FindTag");
                        return tag;
                    }
                }
            }

            ProcessTimer.StopTimer("FindTag");
            return null;
        }


        private static bool hasPreserveTransientTag(XElement element)
        {
            if (element == null) return false;
			foreach (XElement tag in element.Elements("Tag"))
            {
                if (tag.Attribute("name") != null && tag.Attribute("name").Value.Equals("preserveTransient"))
                {
                    if (tag.Attribute("value") != null && tag.Attribute("value").Value.Equals("0")) return false;
                    else return true;
                }
            }
            return hasPreserveTransientTag(element.Parent);
        }

        /// <summary>
        /// The find tag value.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="tagName">
        /// The tag name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string FindTagValue(XElement element, string tagName)
        {
            ProcessTimer.StartTimer("FindTagValue");
            foreach (var e in element.AncestorsAndSelf())
            {
                foreach (var tag in e.Elements("Tag"))
                {
                    if (String.Compare(tag.Attribute("name").Value, tagName, true) == 0)
                    {
                        if (tag.Attribute("value") != null)
                        {
                            ProcessTimer.StopTimer("FindTagValue");
                            return tag.Attribute("value").Value;
                        }
                    }
                }
            }

            ProcessTimer.StopTimer("FindTagValue");
            return string.Empty;
        }

        /// <summary>
        /// The get path.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetPath(XElement element)
        {
            ProcessTimer.StartTimer("GetPath");
            var pathBuilder = new StringBuilder();
            foreach (var parent in element.AncestorsAndSelf().InDocumentOrder())
            {
                if (parent.Attribute("name") != null)
                {
                    // Don't want backslash at start of path
                    if (!string.IsNullOrEmpty(pathBuilder.ToString()))
                    {
                        pathBuilder.Append("\\");
                    }

                    pathBuilder.Append(parent.Attribute("name").Value);
                }
            }

            ProcessTimer.StopTimer("GetPath");
            return pathBuilder.ToString();
        }

        /// <summary>
        /// Helper method for adding builder tool.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="allowRemote">
        /// The allow Remote.
        /// </param>
        /// <param name="allowRestartOnLocal">
        /// The allow Restart On Local.
        /// </param>
        /// <param name="groupPrefix">
        /// The group Prefix.
        /// </param>
        /// <param name="outputMasks">
        /// The output Masks.
        /// </param>
        private void AddBuilderTool(
            string name, string path, bool allowRemote, bool allowRestartOnLocal, string groupPrefix, string outputMasks)
        {
            var tool = new XElement(
                "Tool", 
                new XAttribute("Name", name), 
                new XAttribute("Path", path), 
                new XAttribute("AllowRemote", allowRemote ? "True" : "False"), 
                new XAttribute("AllowRestartOnLocal", allowRestartOnLocal ? "True" : "False"), 
                new XAttribute("GroupPrefix", groupPrefix), 
                new XAttribute("OutputFileMasks", outputMasks));
            this.m_toolsNode.Add(tool);
        }

        /// <summary>
        /// The bank folder contains gestures.
        /// </summary>
        /// <param name="bankWaveFilePath">
        /// The bank wave file path.
        /// </param>
        /// <returns>
        /// Value indicating whether bank folder or any of its sub folders
        /// contains gesture files <see cref="bool"/>.
        /// </returns>
        private bool DirectoryContainsGestures(string bankWaveFilePath)
        {
            if (!Directory.Exists(bankWaveFilePath))
            {
                throw new Exception("Bank wave directory not found: " + bankWaveFilePath);
            }

            return Directory.GetFiles(bankWaveFilePath, "*.gesture", SearchOption.AllDirectories).Any();
        }

        /// <summary>
        /// The combine xml.
        /// </summary>
        /// <param name="pendingElement">
        /// The pending element.
        /// </param>
        /// <param name="parentNodePath">
        /// The parent node path.
        /// </param>
        /// <param name="hasHitBankLevel">
        /// The has hit bank level.
        /// </param>
        private void CombineXml(XElement pendingElement, string parentNodePath, bool hasHitBankLevel)
        {
            ProcessTimer.StartTimer("CombineXml");

            // update nodePath
            var sb = new StringBuilder(parentNodePath);
            sb.Append("/");
            sb.Append(pendingElement.Name);
            sb.Append("[@name='");
            sb.Append(pendingElement.Attribute("name").Value);
            sb.Append("']");

            var nodePath = sb.ToString();

            // use nodePath to find corresponding
            var builtElement = this.m_builtWaves.Root.XPathSelectElement(nodePath);

            if (!hasHitBankLevel)
            {
                switch (pendingElement.Name.ToString())
                {
                    case "Bank":
                        hasHitBankLevel = true;
                        if (pendingElement.Attribute("operation") != null
                            && pendingElement.Attribute("operation").Value == "remove")
                        {
                            var bankName = pendingElement.Attribute("name").Value;
                            var bankPath = Path.Combine(
                                pendingElement.Ancestors("Pack").First().Attribute("name").Value, bankName);

                            // Output Directory for bank, contains bank and xml
                            var bankOuputPath =
                                Path.Combine(
                                    this.m_assetManager.GetLocalPath(this.m_projectSettings.GetBuildOutputPath()), 
                                    "SFX", 
                                    bankPath);

                            // Output File path for bank
                            var bankOutputFile = String.Concat(bankOuputPath, ".awc");
                            this.m_changeList.MarkAssetForDelete(bankOutputFile);

                            // Ensure that any prior operations that would have led to this bank being rebuilt are ignored
                            if (this.m_modifiedBanks.Contains(nodePath))
                            {
                                this.m_modifiedBanks.Remove(nodePath);
                            }
                        }
                        else if (!this.m_modifiedBanks.Contains(nodePath)
                                 && !this.m_autogenBanksBeingDeleted.Contains(nodePath))
                        {
                            this.m_modifiedBanks.Add(nodePath);
                        }

                        break;
                    case "Tag":
                        var element = this.m_builtWaves.Root.XPathSelectElement(parentNodePath);
                        if (element != null)
                        {
                            this.FindBuiltBankPaths(
                                this.m_builtWaves.Root.XPathSelectElement(parentNodePath), parentNodePath);
                        }

                        break;
                }
            }

            var opAttribute = pendingElement.Attribute("operation");
            if (opAttribute != null)
            {
                switch (opAttribute.Value)
                {
                    case "add":
                        if (!(pendingElement.Name == "Tag" && pendingElement.Attribute("name").Value == "rebuild"))
                        {
                            // the element we are trying to add should not exist
                            if (builtElement != null)
                            {
                                Console.WriteLine(
                                    "Warning: Item being added already exists: {0}", 
                                    pendingElement.Attribute("name").Value);
                            }
                            else
                            {
                                var newElement = new XElement(pendingElement.Name, pendingElement.Attributes());
                                newElement.Attribute("operation").Remove();
                                
                                var builtParent = parentNodePath == "/"
                                                      ? this.m_builtWaves.Root
                                                      : this.m_builtWaves.Root.XPathSelectElement(parentNodePath);
                                if (builtParent != null)
                                {
                                    Console.WriteLine(
                                        "Adding node with parent: {0} , element: {1}", parentNodePath, newElement);
                                    if (pendingElement.Attribute("name").Value == "DoNotBuild")
                                    {
                                        builtParent.SetAttributeValue("builtSize", "0");
                                        foreach (var element in builtParent.Descendants())
                                        {
                                            if (element.Name == "Chunk")
                                            {
                                                element.SetAttributeValue("size", "0");
                                            }
                                        }
                                    }

                                    builtParent.Add(newElement);
                                }
                                else
                                {
                                    Console.WriteLine(
                                        "Warning: trying to add node with no parent: {0}, {1}", 
                                        parentNodePath, 
                                        newElement);
                                }
                            }
                        }

                        break;
                    case "remove":

                        // If the element we are deleting does not exist its ancestor must have been deleted
                        if (builtElement != null)
                        {
                            Console.WriteLine(
                                "Removing built element {0}, with parent: {1}", builtElement, parentNodePath);
                            builtElement.Remove();
                        }

                        break;
                    case "modify":
                        if (pendingElement.Name == "Tag")
                        {
                            // need to copy the modified tag value over to built waves
                            var szTag = pendingElement.Attribute("value").Value;
                            if (builtElement != null)
                            {
                                Console.WriteLine("Setting tag {0} on built element {1})", szTag, builtElement);
                                builtElement.Attribute("value").SetValue(pendingElement.Attribute("value").Value);
                            }
                            else
                            {
                                Console.WriteLine("Warning: Modify tag with no builtElement: {0}", pendingElement);
                            }
                        }
                        else
                        {
                            var nameAttribute = pendingElement.Attribute("name");
                            Console.WriteLine(
                                "Operation: {0} on parent node {1}, wave: {2}", 
                                opAttribute.Value, 
                                parentNodePath, 
                                nameAttribute != null ? nameAttribute.Value : string.Empty);
                        }

                        break;
                }
            }

            var children = pendingElement.Elements().ToList();
            ProcessTimer.StopTimer("CombineXml");
            foreach (var child in children)
            {
                this.CombineXml(child, nodePath, hasHitBankLevel);
            }

            ProcessTimer.StartTimer("CombineXml");
            if (!pendingElement.Nodes().Any())
            {
                pendingElement.Remove();
            }
            else if (pendingElement.Attribute("operation") != null)
            {
                pendingElement.Attribute("operation").Remove();
            }

            ProcessTimer.StopTimer("CombineXml");
        }

        /// <summary>
        /// The copy audio to auto gen cutscene waves.
        /// </summary>
        /// <param name="wavePath">
        /// The wave path.
        /// </param>
        /// <param name="alteredWavePath">
        /// The altered wave path.
        /// </param>
        /// <param name="alteredWaveExt">
        /// The altered wave ext.
        /// </param>
        private void CopyAudioToAutoGenCutsceneWaves(string wavePath, string xmlPath, string alteredWavePathOnly, string alteredWaveExtOnly)
        {
            ProcessTimer.StartTimer("CopyAudioToAutoGenCutsceneWaves");

            // Save wave properties
            alteredWavePathOnly = alteredWavePathOnly.ToUpper();
            var alteredXmlPathOnly = xmlPath.Replace("\\CUTSCENE\\", "\\CUTSCENE_MASTERED_ONLY\\");
            if (!Directory.Exists(Path.GetDirectoryName(alteredXmlPathOnly)))
                Directory.CreateDirectory(Path.GetDirectoryName(alteredXmlPathOnly));
            string[] lines = File.ReadAllLines(xmlPath);
            var finOnly = new StreamWriter(alteredXmlPathOnly);
            foreach (var line in lines)
            {
                finOnly.Write(line.Replace(".WAV", alteredWaveExtOnly));
            }

            finOnly.Close();

            // Finally lets make doubly sure we actually really have this wave file, and if not, force get it
            if (!this.m_assetManager.ExistsAsAsset(alteredWavePathOnly))
            {
                this.m_changeList.MarkAssetForAdd(alteredWavePathOnly);
            }
            else if (!File.Exists(alteredWavePathOnly))
            {
                this.m_assetManager.GetLatest(alteredWavePathOnly, true);
            }

            FileStream f = File.Create(alteredWavePathOnly);
            f.Close();
            File.Delete(alteredWavePathOnly);
            File.Copy(wavePath, alteredWavePathOnly, true);
            File.SetAttributes(alteredWavePathOnly, FileAttributes.Normal);
            f.Close();
            ProcessTimer.StopTimer("CopyAudioToAutoGenCutsceneWaves");
        }

        /// <summary>
        ///     Go through the modified bank list and creates the wave jobs.
        /// </summary>
        private void CreateJobs()
        {
            ProcessTimer.StartTimer("CreateJobs");
            Console.Out.WriteLine("Creating wave jobs");

            var buildtask = new XElement("TaskGroup", new XAttribute("Name", this.Name));
            if (!string.IsNullOrEmpty(this.DependsOn))
            {
                // task depends on a previous step
                buildtask.Add(new XAttribute("DependsOn", this.DependsOn));
            }

            // Wave task group, allows post steps to depend on it
            var taskGroup = new XElement("TaskGroup", new XAttribute("Name", "Rave_Wave_Build"));
            buildtask.Add(taskGroup);

            // add task group to job root node
            this.m_jobNode.Add(buildtask);

            // create temporary build output directory
            if (string.IsNullOrEmpty(this.m_buildFolder))
            {
                // Use default temp path unless explicity specified in params
                var tempPath = string.IsNullOrEmpty(this.m_tempPath) ? Path.GetTempPath() : this.m_tempPath;
                this.m_buildFolder = Path.Combine(tempPath, "Build");
            }

            if (Directory.Exists(this.m_buildFolder))
            {
                Console.WriteLine("Deleting temporary build folder: {0}", this.m_buildFolder);
                Utility.ForceDeleteDirectory(this.m_buildFolder);
            }

            Console.WriteLine("Creating temporary build folder: {0}", this.m_buildFolder);
            Directory.CreateDirectory(this.m_buildFolder);

            // cycle through each modified bank
            foreach (var bankNodePath in this.m_modifiedBanks)
            {
                ProcessTimer.StartTimer("CreateJobs (bank loop)");

                // Get the bank node from the saved Xpath expression
                var bank = this.m_builtWaves.Root.XPathSelectElement(bankNodePath);
                if (bank == null)
                {
                    throw new InvalidDataException(
                        "Bank in modified list but not found in built waves: " + bankNodePath);
                }

                var pack = bank.Ancestors("Pack").First();

                // Extract the bank name
                var bankName = bank.Attribute("name").Value;

                // The path under which all the original wave files for the bank are stored
                var bankWaveFilePath = Path.Combine(this.m_BaseWorkingPath, GetPath(bank));

                // The bank path relative to the output directory i.e. PackName\BankName
                var bankPath = Path.Combine(pack.Attribute("name").Value, bankName);

                // construct bank folder path
                string bankFolderName = null;
                var parent = bank.Parent;
                if (parent != pack)
                {
                    var bankFolderBuilder = new StringBuilder(pack.Attribute("name").Value);
                    var bankFolders = new LinkedList<string>();
                    while (parent != pack)
                    {
                        bankFolders.AddFirst(parent.Attribute("name").Value);
                        parent = parent.Parent;
                    }

                    foreach (var bankFolder in bankFolders)
                    {
                        bankFolderBuilder.Append("\\");
                        bankFolderBuilder.Append(bankFolder);
                    }

                    bankFolderName = bankFolderBuilder.ToString();
                }

                // pull out streaming tag if one exists
                var streamingTag = FindTag(bank, "streamingBlockBytes");
                var streamingBlockBytes = streamingTag == null ? string.Empty : streamingTag.Attribute("value").Value;
                var compressionType = string.Empty;
                //pull out bank packing type tag if one exists
			    var bankPackingTypeTag = FindTag(bank, "bankPackingType");
                var bankPackingType = bankPackingTypeTag == null ? string.Empty : bankPackingTypeTag.Attribute("value").Value;
				//pull out encryption type tag if one exists
			    var encryptionKeyTag = FindTag(bank, "encryptionKey");
                var encryptionKey = encryptionKeyTag == null ? string.Empty : encryptionKeyTag.Attribute("value").Value;

                // Output Directory for bank, contains bank and xml
                var bankOutputBuilder = new StringBuilder();
                bankOutputBuilder.Append(
                    this.m_assetManager.GetLocalPath(this.m_projectSettings.GetBuildOutputPath()));
                bankOutputBuilder.Append("SFX\\");
                bankOutputBuilder.Append(pack.Attribute("name").Value);
                var bankOutputPath = bankOutputBuilder.ToString();

                // Output File path for bank
                bankOutputBuilder.Append("\\");
                bankOutputBuilder.Append(bankName);
                bankOutputBuilder.Append(".awc");
                var bankOutputFile = bankOutputBuilder.ToString();
                {
                    // Lets continue to keep our users up to date on what's going on
                    var szStatus = "Adding bank job: " + bankOutputFile;
                    Console.Out.WriteLine(szStatus);
                }

                // create bank output directory
                if (!Directory.Exists(bankOutputPath))
                {
                    Directory.CreateDirectory(bankOutputPath);
                }

                var tempBankPath = Path.Combine(this.m_buildFolder, bankPath);

                // if the file does not exist create an empty file and mark it to be added to source control
                // this allows the exe to have no knowledge of the asset manager
                if (!this.m_assetManager.ExistsAsAsset(bankOutputFile))
                {
                    // ensure existing files are writeable
                    if (File.Exists(bankOutputFile))
                    {
                        File.SetAttributes(bankOutputFile, FileAttributes.Normal);
                    }

                    var sw = new StreamWriter(bankOutputFile);
                    sw.Close();
                    this.m_changeList.MarkAssetForAdd(bankOutputFile, "binary");
                }
                else
                {
                    // file exists so check it out
                    try
                    {
                        this.m_assetManager.GetLatest(bankOutputFile, false);
                    }
                    catch (Exception)
                    {
                        this.m_assetManager.GetLatest(bankOutputFile, true);
                    }

                    if (null == this.m_changeList.CheckoutAsset(bankOutputFile, true))
                    {
                        throw new Exception("Failed to checkout bank output file: " + bankOutputFile);
                    }
                }

                // Get latest on bank contents, ignoring auto-generated cutscene sounds, as these will be copied over from the
                // regular CUTSCENE bank
                if (!m_ShouldRunCutsceneStripper || (!bankWaveFilePath.Contains("CUTSCENE_MASTERED_TRIMMED") && !bankWaveFilePath.Contains("CUTSCENE_MASTERED_ONLY")))
                {
                    try
                    {
                        this.m_assetManager.GetLatest(
                            String.Concat(bankWaveFilePath, "..."), false, this.m_syncChangelistNumber);
                    }
                    catch (Exception)
                    {
                        this.m_assetManager.GetLatest(
                            String.Concat(bankWaveFilePath, "..."), true, this.m_syncChangelistNumber);
                    }
                }

                // Create XML wave Properties
                var waveList = new List<string>();
                var bankNeedsGranularProcessing = false;
                var bankNeedsGestureProcessing = false;
                var bankNeedsVisemeProcessing = false;
                var bankNeedsCutsceneStripping = false;

                var fetchedLatestLipsyncAnims = false;

                foreach (var wave in bank.Descendants("Wave"))
                {
                    Console.WriteLine(wave.Attribute("name").Value);
                    if (wave.Attribute("name").Value.EndsWith(".TXT"))
                    {
                        
                        continue;
                    }
                    ProcessTimer.StartTimer("CreateJobs (wave loop)");
                    if(FindTag(wave, "DoNotBuild") == null && !GetPath(wave).ToUpper().EndsWith(".GRN"))
                    {
                        // pull out various tags
                        var compressionString = FindTagValue(wave, "compression");
                        var sampleRateString = FindTagValue(wave, "sampleRate");
                        var builtName = FindTagValue(wave, "rename");
                        var blockSize = FindTagValue(wave, "blockSize");
                        var wavePath = Path.Combine(this.m_BaseWorkingPath, GetPath(wave));

                        // Altered info for use in generating auto-generated cutscene info
                        var alteredWavePathOnly = string.Empty;
                        var alteredWaveExtOnly = string.Empty;
                        compressionType = FindTagValue(wave, "compressionType");

                        if (!string.IsNullOrEmpty(this.m_gestureExePath) && this.DirectoryContainsGestures(bankWaveFilePath))
                        {
                            bankNeedsGestureProcessing = true;
                        }

                        if (FindTag(wave, "voice") != null)
                        {
                            if (!string.IsNullOrEmpty(this.m_visemeExePath) && FindTag(wave, "noLipsync") == null)
                            {
                                bankNeedsVisemeProcessing = true;

                                // Get latest lipsync anims, as these are
                                // created outside of this build process.
                                if (!fetchedLatestLipsyncAnims)
                                {
                                    var lipsyncAnimsPath = Regex.Replace(
                                        bankWaveFilePath, "\\\\WAVES\\\\", "\\LIPSYNCANIMS\\", RegexOptions.IgnoreCase);
                                    lipsyncAnimsPath = Path.Combine(lipsyncAnimsPath, "...");
                                    try
                                    {
                                        this.m_assetManager.GetLatest(lipsyncAnimsPath, false);
                                        if (!String.IsNullOrEmpty(m_projectSettings.LipsyncTextPath))
                                            m_assetManager.GetLatest(m_assetManager.GetDepotPath(m_projectSettings.LipsyncTextPath), false);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine(
                                            "Warning: Failed to get latest lipsync anim files " + lipsyncAnimsPath);
                                        Console.WriteLine(ex.Message);
                                    }

                                    // Only need to get latest once per pack
                                    fetchedLatestLipsyncAnims = true;
                                }
                            }
                        }

                        var waveNeedsGranularProcessing = false;
                        if (FindTag(wave, "granular") != null)
                        {
                            bankNeedsGranularProcessing = true;

                            if (FindTag(wave, "nongranular") == null)
                            {
                                waveNeedsGranularProcessing = true;
                            }
                        }

                        var preserveTransient = bankNeedsGranularProcessing
                                                || (hasPreserveTransientTag(wave));
                        var markSilence = FindTag(wave, "markSilence");
                        var iterativeEncoding = FindTag(wave, "iterativeEncoding");

                        // Create XML for wave properties i.e. sample rate etc.
                        var waveProperties = new WaveProperties();
                        waveProperties.SourceAsset.Name = wave.Attribute("name").Value;
                        waveProperties.SourceAsset.Path = wavePath;
                        waveProperties.SourceAsset.Granular = waveNeedsGranularProcessing;
                        waveProperties.BuiltAsset.CustomLipsync = false;

                        if (!string.IsNullOrEmpty(sampleRateString))
                        {
                            waveProperties.BuiltAsset.SampleRate = int.Parse(sampleRateString);
                        }

                        if (!string.IsNullOrEmpty(compressionString))
                        {
                            waveProperties.BuiltAsset.Compression = int.Parse(compressionString);
                        }

                        if (!string.IsNullOrEmpty(compressionType))
                        {
                            waveProperties.BuiltAsset.CompressionType = compressionType;
                        }

                        waveProperties.BuiltAsset.Name = builtName == string.Empty
                                                             ? waveProperties.SourceAsset.Name
                                                             : builtName;
                        waveProperties.BuiltAsset.BlockSize = blockSize == string.Empty ? 2 : int.Parse(blockSize);
                        waveProperties.BuiltAsset.PreserveTransient = preserveTransient;

                        waveProperties.BuiltAsset.DisableLooping = null != FindTag(wave, "disableLooping");
                        waveProperties.BuiltAsset.ForceInternalEncoder = null != FindTag(wave, "forceInternalEncoder");
                        
                        if (markSilence != null)
                        {
                            var attr = markSilence.Attribute("value");
                            if (attr != null)
                            {
                                waveProperties.BuiltAsset.MarkSilence = attr.Value;
                            }
                            else
                            {
                                waveProperties.BuiltAsset.MarkSilence = string.Empty;
                            }
                        }

                        if (iterativeEncoding != null)
                        {
                            var attr = iterativeEncoding.Attribute("value");
                            if (attr != null)
                            {
                                waveProperties.BuiltAsset.IterativeEncoding = attr.Value;
                            }
                            else
                            {
                                waveProperties.BuiltAsset.IterativeEncoding = string.Empty;
                            }
                        }

						if (m_assetManager.ExistsAsAsset(wavePath.ToUpper().Replace("\\WAVES\\", "\\LIPSYNCANIMS\\").Replace(".WAV", ".CUSTOM_LIPSYNC")))
                        {
                            waveProperties.BuiltAsset.CustomLipsync = true;
                        }

                        waveList.Add(wavePath);
                        
                        // Save wave properties
                        wavePath = wavePath.ToUpper();
                        var tempPath = Path.Combine(tempBankPath, Path.GetFileName(wavePath));
                       
						var xmlPath = tempPath.Replace(".WAV", ".XML").Replace(".MID", ".XML");//ignoring txt for now .Replace(".TXT", ".XML");
                        waveProperties.Save(xmlPath, true);

                        if (pack.Attribute("name").Value == "CUTSCENE" && wave.Attribute("name").Value.Contains("MASTERED") && this.m_ShouldRunCutsceneStripper)
                        {
                            this.CreateOrCheckOutAutoGenCutsceneWaves(wave, wavePath, ref alteredWavePathOnly, ref alteredWaveExtOnly);
                        }

                        // Finally lets make doubly sure we actually really have this wave file, and if not, force get it
                        // Dont' do this for CUTSCENE_MASTERED_TRIMMED, as we copy the wavs currently in CUTSCENE
                        if ((!File.Exists(wavePath)) && (!m_ShouldRunCutsceneStripper || (!bankWaveFilePath.Contains("CUTSCENE_MASTERED_TRIMMED") && !bankWaveFilePath.Contains("CUTSCENE_MASTERED_ONLY"))))
                        {
                            this.m_assetManager.GetLatest(wavePath, true);
                        }

                        // Be sure to add jobs for auto-generated cutscene wavs
                        if (alteredWavePathOnly != string.Empty &&  this.m_ShouldRunCutsceneStripper)
                        {
                            this.CopyAudioToAutoGenCutsceneWaves(wavePath, xmlPath, alteredWavePathOnly, alteredWaveExtOnly);
                        }

						if (!wavePath.EndsWith(".MID") && !wavePath.EndsWith(".WAV") && !wavePath.EndsWith(".TXT"))
                        {
							throw new ApplicationException("Expected a .WAV, .MID or .TXT file");
                        }
                    }

                    ProcessTimer.StopTimer("CreateJobs (wave loop)");
                }

                if (waveList.Count > 0)
                {
                    // Check for multiple files with the same name in a bank
                    var waveNames = new HashSet<string>();
                    foreach (var wave in waveList)
                    {
                        var fileName = Path.GetFileName(wave);
                        if (waveNames.Contains(fileName))
                        {
                            throw new ApplicationException(
                                string.Format(
                                    "There are multiple files in the \"{0}\" bank with the file name \"{1}\".", 
                                    bankName, 
                                    fileName));
                        }

                        waveNames.Add(fileName);
                    }

                    if (!Directory.Exists(tempBankPath))
                    {
                        Directory.CreateDirectory(tempBankPath);
                    }

                    if (bankNeedsGranularProcessing)
                    {
                        foreach (var wave in waveList)
                        {
                            var grainFileNameFull = wave.Replace(".WAV", ".grn");
                            var grainFileName = Path.GetFileName(grainFileNameFull);

                            if (File.Exists(grainFileNameFull))
                            {
                                var targetFile = tempBankPath + "\\" + grainFileName;
                                Console.Out.WriteLine("Copying " + grainFileNameFull + " to " + targetFile);
                                File.Copy(grainFileNameFull, targetFile, true);

                                FileAttributes attributes = File.GetAttributes(targetFile);
                                if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                                {
                                    attributes = attributes & ~FileAttributes.ReadOnly;
                                    File.SetAttributes(targetFile, attributes);
                                }
                            }
                        }
                    }

                    var buildResponse = Path.Combine(tempBankPath, "XGReponseFile.txt");
                    var fs = new FileStream(buildResponse, FileMode.Create, FileAccess.Write);
                    var sw = new StreamWriter(fs);
                    


                    foreach (var wavePath in waveList)
                    {
                        sw.Write("\"");
                        var tempWavePath = Path.Combine(tempBankPath, Path.GetFileName(wavePath));
                        tempWavePath = tempWavePath.Replace(@"\", @"\\");
                        sw.Write(tempWavePath);
                        sw.WriteLine("\"");

                        // Copy source waves to temp location
                        Console.Out.WriteLine("Copying " + wavePath + " to " + tempWavePath);
                        File.Copy(wavePath, tempWavePath, true);

                        var attributes = File.GetAttributes(tempWavePath);
                        if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        {
                            attributes = attributes & ~FileAttributes.ReadOnly;
                            File.SetAttributes(tempWavePath, attributes);
                        }
                    }

                    sw.Close();
                    fs.Close();

                    var bankWaveTasks = new XElement(
                        "TaskGroup", new XAttribute("Name", String.Concat("waveTasks_", bankPath)));

                    taskGroup.Add(bankWaveTasks);
                    ProcessTimer.StartTimer("CreateJobs (add tasks)");

                    // ***Add wave jobs****
                    var isStreamingBank = !string.IsNullOrEmpty(streamingBlockBytes);

                    // create params
                    var parameterBuilder = new StringBuilder();
                    parameterBuilder.Append("-inputFile ");

                    // add quotes to source path
                    parameterBuilder.Append("\"$(SourcePath)\" ");
                    parameterBuilder.Append("-outputDir ");

                    // add quotes to bank path
                    parameterBuilder.Append("\"");
                    parameterBuilder.Append(tempBankPath);
                    parameterBuilder.Append("\" ");
                    parameterBuilder.Append("-platform ");
                    parameterBuilder.Append(this.m_projectSettings.GetCurrentPlatform().PlatformTag);
                    parameterBuilder.Append(" ");
                    parameterBuilder.Append("-projectSettings ");
                    parameterBuilder.Append(this.m_projectSettings.Path);
                    if (isStreamingBank)
                    {
                        parameterBuilder.Append(" -streamingBlockBytes ");
                        parameterBuilder.Append(streamingBlockBytes);
                    }

                    if (!string.IsNullOrEmpty(compressionType))
                    {
                        parameterBuilder.Append(" -compressionType ");
                        parameterBuilder.Append(compressionType);
                    }

                    if (!string.IsNullOrEmpty(this.m_extraParameters))
                    {
                        parameterBuilder.Append(" " + this.m_extraParameters);
                    }


					//Waves in bank to pass to EDL
					string wavesInBank = string.Join(",", waveList.Select(Path.GetFileName));
                    bool containsEdl = false; //ignor text files for now ---  wavesInBank.ToUpper().Contains(".TXT");
                    
                    var parameters = parameterBuilder.ToString();

					if (containsEdl)
					{
						foreach (string textfile in waveList.Where(p => p.Contains(".TXT") || p.Contains(".txt")))
						{
							Console.WriteLine(textfile);
							string newTextFile = Path.Combine(tempBankPath, Path.GetFileName(textfile));
							File.Copy(textfile, newTextFile, true);

							string converterPath = (string.Join("\\", m_projectSettings.GetBuildToolsPath().Split('\\').Take(3)) + "\\ironlib\\lib\\RSG.Pipeline.Convert.exe").Replace("\\","/");
							this.m_assetManager.GetLatest(converterPath, false);
							ProcessStartInfo processStartInfo = new ProcessStartInfo(this.m_assetManager.GetLocalPath(m_edlBuilderExePath.Replace("\\","/")), parameters.Replace("$(SourcePath)", newTextFile) + " -waveList " + wavesInBank + " -converter " + this.m_assetManager.GetLocalPath(converterPath));

							Process process = new Process {StartInfo = processStartInfo};

							process.Start();
							process.WaitForExit();
						}
					}
                    // Add Peak Normalizer
                    var wavePeakNormaliser = new XElement(
                        "Task", 
                        new XAttribute("Name", String.Concat("peakNormalise_", bankPath)),
                        new XAttribute("SourceFile", String.Concat("@", buildResponse)), 
                        new XAttribute("Params", parameters), 
                        new XAttribute("Tool", "PeakNormalise"), 
                        new XAttribute("StopOnErrors", "true"), 
                        new XAttribute("Caption", "PeakNormalise $(SourcePath)"));

                    bankWaveTasks.Add(wavePeakNormaliser);

                    // Add Cutscene Stripper
                    if (bankNeedsCutsceneStripping)
                    {
                        var cutsceneStripper = new XElement(
                            "Task",
                            new XAttribute("Name", String.Concat("cutsceneStripper_", bankPath)),
                            new XAttribute("SourceFile", String.Concat("@", buildResponse)),
                            new XAttribute("Params", parameters),
                            new XAttribute("DependsOn", String.Concat("peakNormalise_", bankPath)),
                            new XAttribute("Tool", "CutsceneStripper"),
                            new XAttribute("StopOnErrors", "true"),
                            new XAttribute("Caption", "Cutscene Strip $(SourcePath)"));
                        bankWaveTasks.Add(cutsceneStripper);
                    }

                    // Add Resampler
                    var waveResample = new XElement(
                        "Task",
                        new XAttribute("Name", String.Concat("resample_", bankPath)),
                        new XAttribute("SourceFile", String.Concat("@", buildResponse)),
                        new XAttribute("Params", parameters),
                        new XAttribute("DependsOn", String.Concat(bankNeedsCutsceneStripping ? "cutsceneStripper_" : "peakNormalise_", bankPath)),
                        new XAttribute("Tool", "Resample"),
                        new XAttribute("StopOnErrors", "true"),
                        new XAttribute("Caption", "Resample $(SourcePath)"));
                    bankWaveTasks.Add(waveResample);

                    // Add Gesture Builder
                    if (bankNeedsGestureProcessing)
                    {
                        XElement waveGestureBuilder = new XElement(
                            "Task",
                            new XAttribute("Name", String.Concat("gesture_", bankPath)),
                            new XAttribute("SourceFile", String.Concat("@", buildResponse)),
                            new XAttribute("Params", parameters),
                            new XAttribute("DependsOn", String.Concat("resample_", bankPath)),
                            new XAttribute("Tool", "Gesture"),
                            new XAttribute("StopOnErrors", "true"),
                            new XAttribute("Caption", "Gesture $(SourcePath)"));

                        bankWaveTasks.Add(waveGestureBuilder);

                        // Copy .gesture files to temp path so they are available to the gesture builder
                        foreach (var wave in waveList)
                        {

                            var gestureFileNameFull = wave.Replace(Path.GetExtension(wave), ".gesture");
                            var gestureFileName = Path.GetFileName(gestureFileNameFull);

                            if (File.Exists(gestureFileNameFull))
                            {
                                var targetFile = Path.Combine(tempBankPath, gestureFileName);
                                Console.Out.WriteLine("Copying " + gestureFileNameFull + " to " + targetFile);
                                File.Copy(gestureFileNameFull, targetFile);

                                new FileInfo(targetFile) { IsReadOnly = false };
                            }

                            // If wave is a processed wave version, we also want to copy the gesture
                            // file associated with the original wave file
                            if (wave.ToUpper().Contains(".PROCESSED"))
                            {
                                var srcGesturePath = Regex.Replace(
                                    gestureFileNameFull, @"\.PROCESSED", string.Empty, RegexOptions.IgnoreCase);

                                if (File.Exists(srcGesturePath))
                                {
                                    gestureFileName = Path.GetFileName(srcGesturePath);
                                    var destGesturePath = Path.Combine(tempBankPath, gestureFileName);
                                    Console.WriteLine("Copying " + srcGesturePath + " to " + destGesturePath);
                                    File.Copy(srcGesturePath, destGesturePath, true);

                                    new FileInfo(destGesturePath) { IsReadOnly = false };
                                }
                            }
                        }
                    }

                    // Add Viseme Builder
                    if (bankNeedsVisemeProcessing)
                    {
                        XElement waveVisemeBuilder = new XElement(
                            "Task",
                            new XAttribute("Name", String.Concat("lipsync_", bankPath)),
                            new XAttribute("SourceFile", String.Concat("@", buildResponse)),
                            new XAttribute("Params", parameters),
                            new XAttribute("Tool", "Lipsync"),
                            new XAttribute("StopOnErrors", "true"),
                            new XAttribute("Caption", "Lipsync $(SourcePath)"));

                        bankWaveTasks.Add(waveVisemeBuilder);

                        if (bankNeedsGestureProcessing)
                        {
                            waveVisemeBuilder.Add(new XAttribute("DependsOn", String.Concat("gesture_", bankPath)));
                        }
                        else
                        {
                            waveVisemeBuilder.Add(new XAttribute("DependsOn", String.Concat("resample_", bankPath)));
                        }
                    }

                    // Add Encoder                
                    var waveEncoder = new XElement(
                        "Task", 
                        new XAttribute("Name", String.Concat("encoder_", bankPath)), 
                        new XAttribute("SourceFile", String.Concat("@", buildResponse)), 
                        new XAttribute("Params", parameters), 
                        new XAttribute("Tool", "Encode"), 
                        new XAttribute("StopOnErrors", "true"), 
                        new XAttribute("Caption", "Encode $(SourcePath)"),
                        new XAttribute("OutputFileMasks", "*.GRN"));
                    bankWaveTasks.Add(waveEncoder);

                    if (bankNeedsVisemeProcessing)
                    {
                        waveEncoder.Add(new XAttribute("DependsOn", String.Concat("lipsync_", bankPath)));
                    }
                    else if (bankNeedsGestureProcessing)
                    {
                        waveEncoder.Add(new XAttribute("DependsOn", String.Concat("gesture_", bankPath)));
                    }
                    else
                    {
                        waveEncoder.Add(new XAttribute("DependsOn", String.Concat("resample_", bankPath)));
                    }

                    if (bankNeedsGranularProcessing)
                    {
                        // Run granular data builder a second time using the buildResponse file instead. This time it will detect
                        // the encoded asset and do any additional processing required (eg. precomputing packets on X360)
                        var grainPacketBuilder = new XElement(
                            "Task", 
                            new XAttribute("Name", String.Concat("granular_packet_generator", bankPath)), 
                            new XAttribute("SourceFile", String.Concat("@", buildResponse)), 
                            new XAttribute("Params", parameters), 
                            new XAttribute("DependsOn", String.Concat("encoder_", bankPath)), 
                            new XAttribute("Tool", "GranularBuilder"), 
                            new XAttribute("StopOnErrors", "true"), 
                            new XAttribute("Caption", "Compute granular packets $(SourcePath)"));
                        bankWaveTasks.Add(grainPacketBuilder);
                    }

                    if (isStreamingBank)
                    {
                        StringBuilder streamBuildParams = new StringBuilder(string.Format(
                                    "-inputDir \"{0}\" -outputDir \"{1}\" -name {2} -platform {3} -projectSettings {4} -streamingBlockBytes {5}", 
                                    tempBankPath, 
                                    tempBankPath, 
                                    bankName, 
                                    this.m_projectSettings.GetCurrentPlatform().PlatformTag, 
                                    this.m_projectSettings.Path, 
                                    streamingBlockBytes));

                        if (!string.IsNullOrEmpty(encryptionKey))
                        {
                            streamBuildParams.Append(string.Format(" -encryptionKey {0}", encryptionKey));
                        }

                        var streamBuilder = new XElement(
                            "Task", 
                            new XAttribute("Name", String.Concat("streamBuild_", bankPath)), 
                            new XAttribute("Params", streamBuildParams),
                            new XAttribute("DependsOn", String.Concat("waveTasks_", bankPath)), 
                            new XAttribute("Tool", "StreamBuilder"), 
                            new XAttribute("StopOnErrors", "true"), 
                            new XAttribute("Caption", String.Concat("StreamBuild ", bankName)));

                        taskGroup.Add(streamBuilder);
                    }

                    var waveSlotSettings =
                        this.m_assetManager.GetLocalPath(this.m_projectSettings.GetWaveSlotSettings());
                    var bankBuilderParams = new StringBuilder();
                    bankBuilderParams.Append(string.Format("-inputDir \"{0}\"", tempBankPath));
                    bankBuilderParams.Append(string.Format(" -outputDir \"{0}\"", bankOutputPath));
                    bankBuilderParams.Append(string.Format(" -name {0}", bankName));
                    bankBuilderParams.Append(
                        string.Format(" -platform {0}", this.m_projectSettings.GetCurrentPlatform().PlatformTag));
                    bankBuilderParams.Append(string.Format(" -projectSettings {0}", this.m_projectSettings.Path));
                    bankBuilderParams.Append(
                        string.Format(
                            " -streamingBlockBytes {0}", 
                            !string.IsNullOrEmpty(streamingBlockBytes) ? streamingBlockBytes : "0"));
                    bankBuilderParams.Append(string.Format(" -waveSlotSettings {0}", waveSlotSettings));
                    if (!string.IsNullOrEmpty(bankFolderName))
                    {
                        bankBuilderParams.Append(string.Format(" -bankFolderName {0}", bankFolderName));
                    }
                    if (!string.IsNullOrEmpty(encryptionKey))
                    {
                        bankBuilderParams.Append(string.Format(" -encryptionKey {0}", encryptionKey));
                    }
                    if (!string.IsNullOrEmpty(bankPackingType))
                    {
                        parameterBuilder.Append(" -bankPackingType ");
                        parameterBuilder.Append(bankPackingType);
                    }

                    var bankBuilder = new XElement(
                        "Task", 
                        new XAttribute("Name", String.Concat("bankBuild_", bankPath)), 
                        new XAttribute("Params", bankBuilderParams.ToString()), 
                        new XAttribute(
                            "DependsOn", 
                            String.Concat(
                                string.IsNullOrEmpty(streamingBlockBytes) ? "waveTasks_" : "streamBuild_", bankPath)), 
                        new XAttribute("Tool", "BankBuilder"), 
                        new XAttribute("StopOnErrors", "true"), 
                        new XAttribute("Caption", String.Concat("BankBuild ", bankName)));
                    taskGroup.Add(bankBuilder);
                    ProcessTimer.StopTimer("CreateJobs (add tasks)");
                }

                ProcessTimer.StopTimer("CreateJobs (bank loop)");
            }

            var builtWavesPath = this.m_assetManager.GetLocalPath(this.m_builtWavesPath);

            var xmlBuilder = new XElement(
                "Task", 
                new XAttribute("Name", "XMLBuilder"), 
                new XAttribute("Params", string.Format("\"{0}\" \"{1}", builtWavesPath, this.m_buildFolder)), 
                new XAttribute("DependsOn", "Rave_Wave_Build"), 
                new XAttribute("Tool", "XMLBuilder"), 
                new XAttribute("StopOnErrors", "true"), 
                new XAttribute("Caption", "XMLBuild"));

            buildtask.Add(xmlBuilder);
            ProcessTimer.StopTimer("CreateJobs");
        }

        /// <summary>
        /// The create or check out auto gen cutscene waves.
        /// </summary>
        /// <param name="wave">
        /// The wave.
        /// </param>
        /// <param name="wavePath">
        /// The wave path.
        /// </param>
        /// <param name="alteredWavePath">
        /// The altered wave path.
        /// </param>
        /// <param name="alteredWaveExt">
        /// The altered wave ext.
        /// </param>
        private void CreateOrCheckOutAutoGenCutsceneWaves(
            XElement wave, string wavePath, ref string alteredWavePathOnly, ref string alteredWaveExtOnly)
        {
            ProcessTimer.StartTimer("CreateOrCheckOutAutoGenCutsceneWaves");
            Match match = Regex.Match(wavePath, @"CUTSCENE\\(.*)\\");
            string directory = match.Groups[1].Value;

            alteredWavePathOnly = wavePath.Replace("\\CUTSCENE\\", "\\CUTSCENE_MASTERED_ONLY\\")
                                .Replace(String.Concat("\\", directory, "\\"), String.Concat("\\", directory, "_ONLY\\"))
                                .Replace(".WAV", "_ONLY.WAV");
            alteredWaveExtOnly = "_ONLY.WAV";

            if (!this.m_assetManager.ExistsAsAsset(alteredWavePathOnly))
            {
                // ensure existing files are writable
                if (!File.Exists(alteredWavePathOnly))
                {
                    Directory.CreateDirectory(alteredWavePathOnly.Remove(alteredWavePathOnly.LastIndexOf("\\")));
                    FileStream fs = File.Create(alteredWavePathOnly);
                    fs.Close();
                }

                File.SetAttributes(alteredWavePathOnly, FileAttributes.Normal);
                var sw = new StreamWriter(alteredWavePathOnly);
                sw.Close();
                this.m_changeList.MarkAssetForAdd(alteredWavePathOnly, "binary");
            }
            else
            {
                // file exists so check it out
                try
                {
                    this.m_assetManager.GetLatest(alteredWavePathOnly, false);
                }
                catch (Exception)
                {
                    this.m_assetManager.GetLatest(alteredWavePathOnly, true);
                }

                if (null == this.m_changeList.CheckoutAsset(alteredWavePathOnly, true))
                {
                    throw new Exception("Failed to checkout wave file: " + alteredWavePathOnly);
                }
            }

            ProcessTimer.StopTimer("CreateOrCheckOutAutoGenCutsceneWaves");
        }

        /// <summary>
        ///     Creates the tool nodes needed in the xge tools xml
        /// </summary>
        private void CreateTools()
        {
            ProcessTimer.StartTimer("CreateTools");
            Console.Out.WriteLine("Getting latest tools for wave build");

            var gestureToolPath = string.Empty;
            if (!string.IsNullOrEmpty(this.m_gestureExePath))
            {
                gestureToolPath = this.m_assetManager.GetLocalPath(this.m_gestureExePath);
                this.m_assetManager.GetLatest(this.m_gestureExePath, false);
            }

            var visemeToolPath = string.Empty;
            if (!string.IsNullOrEmpty(this.m_visemeExePath))
            {
                this.m_assetManager.GetLatest(this.m_visemeExePath, false);
                visemeToolPath = this.m_assetManager.GetLocalPath(this.m_visemeExePath);
            }
			string edlToolPath = string.Empty;
			if (!string.IsNullOrEmpty(this.m_edlBuilderExePath))
			{
				this.m_assetManager.GetLatest(this.m_edlBuilderExePath, false);
				edlToolPath = this.m_assetManager.GetLocalPath(this.m_edlBuilderExePath);
			}
            var cutsceneStripperToolPath = string.Empty;
            if (this.m_ShouldRunCutsceneStripper)
            {
                this.m_assetManager.GetLatest(this.m_CutsceneStripperExePath, false);
                cutsceneStripperToolPath = this.m_assetManager.GetLocalPath(this.m_CutsceneStripperExePath);

                // Get latest cutscene trimming info
                this.m_assetManager.GetLatest(this.m_CutsceneExportDataPath, false);
            }

            this.m_assetManager.GetLatest(this.m_granularExePath, false);
            var granularToolPath = this.m_assetManager.GetLocalPath(this.m_granularExePath);

            this.m_assetManager.GetLatest(this.m_peakNormaliseExePath, false);
            var peakNormaliseToolPath = this.m_assetManager.GetLocalPath(this.m_peakNormaliseExePath);

            this.m_assetManager.GetLatest(this.m_resampleExePath, false);
            var resampleToolPath = this.m_assetManager.GetLocalPath(this.m_resampleExePath);

            this.m_assetManager.GetLatest(this.m_encoderExePath, false);
            var encoderPath = this.m_assetManager.GetLocalPath(this.m_encoderExePath);

            this.m_assetManager.GetLatest(this.m_bankBuilderExePath, false);
            var bankBuilderPath = this.m_assetManager.GetLocalPath(this.m_bankBuilderExePath);

            this.m_assetManager.GetLatest(this.m_streamBuilderExePath, false);
            var streamBuilderPath = this.m_assetManager.GetLocalPath(this.m_streamBuilderExePath);

            this.m_assetManager.GetLatest(this.m_xmlBuilderExePath, false);
            var xmlBuilderPath = this.m_assetManager.GetLocalPath(this.m_xmlBuilderExePath);

            Console.Out.WriteLine("Writing tool Xml");

            if (!string.IsNullOrEmpty(gestureToolPath))
            {
                this.AddBuilderTool("Gesture", gestureToolPath, true, true, "Generating Gesture Data...", "*.GESTURE");
            }

            if (!string.IsNullOrEmpty(visemeToolPath))
            {
                // we need to make sure we have latest on the custom anims folder if we have a lip-sync step to run
                if (!string.IsNullOrEmpty(this.m_CustomFacialAnimPath))
                {
                    Console.Out.WriteLine("Getting latest on " + this.m_CustomFacialAnimPath);
                    this.m_assetManager.GetLatestOnFileType(this.m_CustomFacialAnimPath, false, "anim");
                }

                this.AddBuilderTool(
                    "LipSync", 
                    visemeToolPath, 
                    true, 
                    true, 
                    "Generating Lipsync Data...", 
                    "*.LIPSYNC,*.LIPSYNC64,*.WAV");
            }
			if (!string.IsNullOrEmpty(edlToolPath))
			{
				this.AddBuilderTool
					(
					"EDLBuilder", edlToolPath, true, true, "Generating EDL files...", "*.PSO.META,*.XML, *.omt, *.dmt, *.ymt"
					);
			}
            if (!string.IsNullOrEmpty(cutsceneStripperToolPath))
            {
                this.AddBuilderTool(
                    "CutsceneStripper", cutsceneStripperToolPath, false, true, "Stripping Cutscene...", "*.XML,*.WAV");
            }

            if (!string.IsNullOrEmpty(granularToolPath))
            {
                this.AddBuilderTool(
                    "GranularBuilder", granularToolPath, false, true, "Extracting Granular Data...", "*.GRAIN,*.WAV");
            }

            this.AddBuilderTool(
                "PeakNormalise", peakNormaliseToolPath, true, true, "PeakNormalise...", "*.XML,*.WAV,*.MID");
            this.AddBuilderTool("Resample", resampleToolPath, true, true, "Resample...", "*.XML,*.WAV,*.MID");
            this.AddBuilderTool(
                "Encode", 
                encoderPath, 
                true, 
                true, 
                "Encode...", 
                "*.DATA,*.SEEKTABLE,*.XML,*.FORMAT,*.MARKERS,*.HEADER,*.PEAK");
            this.AddBuilderTool(
                "StreamBuilder", 
                streamBuilderPath, 
                false, 
                true, 
                "StreamBuilder...", 
                "*.DATA,*.SEEKTABLE,*.XML,*.FORMAT,*.MARKERS,*.STREAMFORMAT,*.HEADER");
            this.AddBuilderTool("BankBuilder", bankBuilderPath, false, true, "BankBuilder...", "*.awc, *.XML");
            this.AddBuilderTool("XMLBuilder", xmlBuilderPath, false, true, "XMLBuilder...", string.Empty);
            ProcessTimer.StopTimer("createTools");
        }

        /// <summary>
        /// The find built bank paths.
        /// </summary>
        /// <param name="builtElement">
        /// The built element.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        private void FindBuiltBankPaths(XElement builtElement, string path)
        {
            ProcessTimer.StartTimer("FindBuiltBankPaths");
            if (builtElement.Name == "Bank")
            {
                if (!this.m_modifiedBanks.Contains(path)
                        && !this.m_autogenBanksBeingDeleted.Contains(path))
                {
                    this.m_modifiedBanks.Add(path);
                }
            }
            else
            {
                foreach (XElement child in builtElement.Nodes())
                {
                    var sb = new StringBuilder(path);
                    sb.Append("/");
                    sb.Append(child.Name);
                    sb.Append("[@name='");
                    sb.Append(child.Attribute("name").Value);
                    sb.Append("']");

                    this.FindBuiltBankPaths(child, sb.ToString());
                }
            }

            ProcessTimer.StopTimer("FindBuiltBankPaths");
        }

        /// <summary>
        /// The get pending waves files.
        /// </summary>
        /// <param name="pendingWavesFolder">
        /// The pending waves folder.
        /// </param>
        /// <param name="pack">
        /// The pack.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<string> GetPendingWavesFiles(string pendingWavesFolder, string pack)
        {
            ProcessTimer.StartTimer("GetPendingWavesFiles");
            var pendingWavesFiles = new List<string>();
            var files = new List<string>();
            if (string.IsNullOrEmpty(pack))
            {
                files.AddRange(Directory.GetFiles(pendingWavesFolder, "*.xml", SearchOption.TopDirectoryOnly).ToList());
            }
            else
            {
                files.Add(Path.Combine(pendingWavesFolder, string.Format("{0}.XML", pack)));
            }

            foreach (var file in files)
            {
                if (this.m_assetManager.ExistsAsAsset(file))
                {
                    var doc = XDocument.Load(file);
                    if (doc.Root != null && doc.Root.Name == "PendingWaves" && doc.Root.Elements("Pack").Any())
                    {
                        pendingWavesFiles.Add(file);
                    }
                }
            }

            ProcessTimer.StopTimer("GetPendingWavesFiles");
            return pendingWavesFiles;
        }

        /// <summary>
        /// The handle cutscene xml.
        /// </summary>
        /// <param name="pendingWavesFiles">
        /// The pending waves files.
        /// </param>
        private void HandleCutsceneXml(ref IEnumerable<string> pendingWavesFiles)
        {
            ProcessTimer.StartTimer("HandleCutsceneXml");
            var pendingCutsceneXml = (this.m_builtWavesFolder.ToUpper() + "CUTSCENE.XML").Replace("BUILT", "PENDING");
            var pendingCutsceneOnlyXml = (this.m_builtWavesFolder.ToUpper() + "CUTSCENE_MASTERED_ONLY.XML").Replace(
                "BUILT", "PENDING");
            var lines = File.ReadAllLines(pendingCutsceneXml);
            var foutOnly = new StreamWriter(pendingCutsceneOnlyXml);

            uint lineNum = 0;

            foutOnly.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            foutOnly.WriteLine("<PendingWaves>");
            foutOnly.WriteLine("\t<Pack name=\"CUTSCENE_MASTERED_ONLY\">");

            foreach (XmlNode node in m_CutsceneOnlyBanksToDelete)
            {
                String bank = node.Attributes["name"].Value;
                XmlNodeList waves = node.SelectNodes("Wave");

                foutOnly.WriteLine("\t\t<Bank name=\"" + bank + "\" operation=\"remove\">");

                foreach (XmlNode wave in waves)
                {
                    String waveName = m_assetManager.GetLocalPath(this.m_projectSettings.GetWaveInputPath()) + "CUTSCENE_MASTERED_ONLY" + "\\" + bank + "\\" + wave.Attributes["name"].Value;
                    if (m_assetManager.ExistsAsAsset(waveName))
                    {
                        m_changeList.MarkAssetForDelete(waveName);
                    }
                    foutOnly.WriteLine("\t\t\t<Wave name=\"" + waveName + "\" operation=\"remove\" />");

                }
                foutOnly.WriteLine("\t\t</Bank>");               
            }

            lineNum = 0;
            for (; lineNum < lines.Length; lineNum++)
            {
                string line = lines[lineNum];
                if (line.Contains("Bank name=") || line.Contains("Bank operation=\"add\""))
                {
                    // Check all the lines until the next bank name and make sure this isn't being removed.
                    bool originalWaveMarkedForRemoval = false;
                    bool waveNotMarkedForRemovalExists = false;
                    List<String> pendingWavesToRemove = new List<String>();
                    List<String> pendingWavesToAdd = new List<String>();
                    for (int i = 1; lineNum + i < lines.Length; i++)
                    {
                        if (lines[lineNum + i].Contains("Bank name="))
                        {
                            break;
                        }

                        if (lines[lineNum + i].Contains("<Wave "))
                        {
                            if (lines[lineNum + i].Contains("operation=\"remove\""))
                            {
                                if (!pendingWavesToRemove.Contains(lines[lineNum + i]))
                                    pendingWavesToRemove.Add(lines[lineNum + i]);
                                originalWaveMarkedForRemoval = true;
                                continue;
                            }
                            else if (lines[lineNum + i].Contains("operation=\"add\""))
                            {
                                if (!pendingWavesToAdd.Contains(lines[lineNum + i]))
                                    pendingWavesToAdd.Add(lines[lineNum + i]);
                            }

                            waveNotMarkedForRemovalExists = true;
                        }
                    }

					if (waveNotMarkedForRemovalExists)
                        originalWaveMarkedForRemoval = false;

                    bool addingBank = false;
                    Match match = Regex.Match(line, @"Bank name=(.*) header");
                    string bankName = match.Groups[1].Value.Replace("\"", string.Empty).Replace("\"", string.Empty);
                    if (bankName.Equals(string.Empty))
                    {
                        match = Regex.Match(line, "Bank operation=\"add\" name=\"(.*)\">");
                        bankName = match.Groups[1].Value;
                        if (bankName.Equals(string.Empty))
                        {
                            match = Regex.Match(line, "Bank name=\"(.*)\" operation=\"add\">");
                            bankName = match.Groups[1].Value;
                        }

                        addingBank = true;
                    }

                    var bankNodePath = String.Concat("//Pack[@name='CUTSCENE']/Bank[@name='" + bankName + "']");
                    XElement bank = this.m_builtWaves.Root.XPathSelectElement(bankNodePath);
                    if (line.ToUpper().Contains("MASTERED"))
                    {
                        var newBankNodePathOnly = String.Concat("//Pack[@name='CUTSCENE_MASTERED_ONLY']/Bank[@name='",
                                                             bankName, "_ONLY']");

                        if (!originalWaveMarkedForRemoval)
                        {
                            XElement newBankOnly = this.m_builtWaves.Root.XPathSelectElement(newBankNodePathOnly);

                            if (newBankOnly != null)
                            {
                                foutOnly.WriteLine(line.Replace("MASTERED", "MASTERED_ONLY").Replace("/>", ">"));
                                foutOnly.WriteLine(String.Concat("\t\t\t<Tag operation=\"add\" name=\"rebuild\" value=\"\" />"));

                                List<String> wavsToDeleteFromOnly = new List<string>();
                                List<String> wavsToAddToOnly = new List<string>();
                                foreach (var element in bank.Elements())
                                {
                                    var waveName = element.Attribute("name").Value;
                                    if (waveName.ToUpper().Contains(".WAV"))
                                    {
                                        bool foundNameInOnlyBank = false;
                                        foreach (var onlyElement in newBankOnly.Elements())
                                        {
                                            var onlyWaveName = onlyElement.Attribute("name").Value;
                                            if (onlyWaveName.ToUpper().Contains(".WAV"))
                                            {
                                                if (onlyWaveName.ToUpper().Equals(waveName.ToUpper().Replace(".WAV", "_ONLY.WAV")))
                                                {
                                                    foundNameInOnlyBank = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!foundNameInOnlyBank)
                                        {
                                            String waveToAdd = m_assetManager.GetLocalPath(this.m_projectSettings.GetWaveInputPath()) + "CUTSCENE_MASTERED_ONLY" + "\\" + bankName + "_ONLY\\" + waveName.ToUpper().Replace(".WAV", "_ONLY.WAV");
                                            if (!m_assetManager.ExistsAsAsset(waveToAdd))
                                                this.m_changeList.MarkAssetForAdd(waveToAdd);
                                            foutOnly.WriteLine(String.Concat("\t\t\t<Wave operation=\"add\" name=\"", waveName.ToUpper().Replace(".WAV", "_ONLY.WAV"), "\" />"));
                                        }
                                    }
                                }

                                foreach (String waveToAdd in pendingWavesToAdd)
                                {
                                    foutOnly.WriteLine(waveToAdd.Replace(".WAV", "_ONLY.WAV"));
                                }

                                foreach (String waveToRemove in pendingWavesToRemove)
                                {
                                    Match waveMatch = Regex.Match(waveToRemove, "Wave name=\"(.*)\" operation");
                                    string waveNameFromMatch = waveMatch.Groups[1].Value;
                                    if (!waveNameFromMatch.Equals(string.Empty))
                                    {
                                        String waveToDelete = m_assetManager.GetLocalPath(this.m_projectSettings.GetWaveInputPath()) + "CUTSCENE_MASTERED_ONLY" + "\\" + bankName + "_ONLY\\" + waveNameFromMatch.Replace(".WAV", "_ONLY.WAV");
                                        if (m_assetManager.ExistsAsAsset(waveToDelete))
                                            this.m_changeList.MarkAssetForDelete(waveToDelete);
                                    }
                                    foutOnly.WriteLine(waveToRemove.Replace(".WAV", "_ONLY.WAV"));
                                }

                                foreach (var onlyElement in newBankOnly.Elements())
                                {
                                    var onlyWaveName = onlyElement.Attribute("name").Value;
                                    if (onlyWaveName.ToUpper().Contains(".WAV"))
                                    {
                                        bool foundNameInOriginalBank = false;
                                        foreach (var element in bank.Elements())
                                        {
                                            var waveName = element.Attribute("name").Value;
                                            if (waveName.ToUpper().Contains(".WAV"))
                                            {
                                                if (onlyWaveName.ToUpper().Equals(waveName.ToUpper().Replace(".WAV", "_ONLY.WAV")))
                                                {
                                                    foundNameInOriginalBank = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!foundNameInOriginalBank)
                                        {
                                            String waveToDelete = m_assetManager.GetLocalPath(this.m_projectSettings.GetWaveInputPath()) + "CUTSCENE_MASTERED_ONLY" + "\\" + bankName + "_ONLY\\" + onlyWaveName;
                                            if (m_assetManager.ExistsAsAsset(waveToDelete))
                                                this.m_changeList.MarkAssetForDelete(waveToDelete);
                                            foutOnly.WriteLine("\t\t\t<Wave name=\"" + onlyWaveName + "\" operation=\"remove\" />");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foutOnly.WriteLine(String.Concat("\t\t<Bank operation=\"add\" name=\"", bankName, "_ONLY\" >"));

                                if (addingBank)
                                {
                                    for (lineNum++; lineNum < lines.Length; lineNum++)
                                    {
                                        line = lines[lineNum];
                                        if (line.Contains("</Bank>"))
                                        {
                                            break;
                                        }

                                        foutOnly.WriteLine(line.Replace(".WAV", "_ONLY.WAV"));
                                    }
                                }
                                else
                                {
                                    foreach (var element in bank.Elements())
                                    {
                                        var waveName = element.Attribute("name").Value;
                                        if (waveName.ToUpper().Contains(".WAV"))
                                        {
                                            waveName = waveName.ToUpper().Replace(
                                                ".WAV", "_ONLY.WAV");

                                            foutOnly.WriteLine(
                                                String.Concat("\t\t\t<Wave operation=\"add\" name=\"", waveName, "\" />"));
                                        }
                                    }
                                }
                            }

                            foutOnly.WriteLine("\t\t</Bank>");
                        }
                        else
                        {
                            // Output Directory for bank, contains bank and xml
                            var bankOuputPathOnly =
                                Path.Combine(
                                    this.m_assetManager.GetLocalPath(this.m_projectSettings.GetBuildOutputPath()),
                                    "SFX",
                                    "CUTSCENE_MASTERED_ONLY",
                                    bankName);

                            // Output File path for bank
                            var bankOutputFileOnly = String.Concat(bankOuputPathOnly, ".awc");

                            if (this.m_assetManager.ExistsAsAsset(bankOutputFileOnly))
                            {
                                this.m_changeList.MarkAssetForDelete(bankOutputFileOnly);
                            }

                            if (!this.m_autogenBanksBeingDeleted.Contains(newBankNodePathOnly))
                            {
                                this.m_autogenBanksBeingDeleted.Add(newBankNodePathOnly);
                            }
                        }
                    }
                }
            }

            foutOnly.WriteLine("\t</Pack >");
            foutOnly.WriteLine("</PendingWaves >");
            foutOnly.Close();

            var pendingWavesList = pendingWavesFiles.ToList();
            pendingWavesList.Add(pendingCutsceneOnlyXml);
            pendingWavesFiles = pendingWavesList;
            ProcessTimer.StopTimer("HandleCutsceneXml");
        }

        /// <summary>
        /// The load pending waves files.
        /// </summary>
        /// <param name="pack">
        /// The pack.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<string> LoadPendingWavesFiles(string pack)
        {
            ProcessTimer.StartTimer("LoadPendingWavesFiles");
            Console.Out.WriteLine("Getting latest PendingWaves...");
            var pendingWavesFolder = Path.Combine(
                this.m_projectSettings.GetCurrentPlatform().BuildInfo, "PendingWaves\\");
            this.m_assetManager.GetLatest(pendingWavesFolder, false);
            pendingWavesFolder = this.m_assetManager.GetLocalPath(pendingWavesFolder);
            var pendingWaveFiles = this.GetPendingWavesFiles(pendingWavesFolder, pack);
            ProcessTimer.StopTimer("LoadPendingWavesFiles");
            return pendingWaveFiles;
        }

        /// <summary>
        /// The load xml.
        /// </summary>
        /// <param name="pack">
        /// The pack.
        /// </param>
        /// <param name="builtWavesFolder">
        /// The built waves folder.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private IEnumerable<string> LoadXml(string pack, out string builtWavesFolder)
        {
            ProcessTimer.StartTimer("LoadXml");
            Console.Out.WriteLine("Getting latest BuiltWaves...");
            builtWavesFolder = Path.Combine(this.m_projectSettings.GetCurrentPlatform().BuildInfo, "BuiltWaves\\");
            this.m_assetManager.GetLatest(builtWavesFolder, false);
            builtWavesFolder = this.m_assetManager.GetLocalPath(builtWavesFolder);

            Console.Out.WriteLine("Combining BuiltWaves...");
            var combiner = new WavesFileCombiner();
            var builtWavesPackListPath = Path.Combine(builtWavesFolder, "BuiltWaves_PACK_LIST.xml");
            this.m_builtWavesPath = combiner.Run(builtWavesPackListPath, Path.Combine(builtWavesFolder, "Temp"), null);

            Console.Out.WriteLine("Loading BuiltWaves...");
            this.m_builtWaves = XDocument.Load(this.m_builtWavesPath);

            var pendingWavesFiles = this.LoadPendingWavesFiles(pack);
            if (pendingWavesFiles.Count == 0)
            {
                Console.Out.WriteLine("No pending waves to build.");
                builtWavesFolder = null;
            }

            ProcessTimer.StopTimer("LoadXml");
            return pendingWavesFiles;
        }

        /// <summary>
        /// The parse xml.
        /// </summary>
        /// <param name="pendingWavesFiles">
        /// The pending waves files.
        /// </param>
        private void ParseXml(IEnumerable<string> pendingWavesFiles)
        {
            ProcessTimer.StartTimer("ParseXml");
            Console.Out.WriteLine("Parsing XML");
            if (this.m_ShouldRunCutsceneStripper)
            {
                foreach (string file in pendingWavesFiles)
                {
                    if (file.ToUpper().Contains((this.m_builtWavesFolder + "CUTSCENE.XML").ToUpper().Replace("BUILT", "PENDING")))
                    {
                        BuildCutsceneToDeleteLists();
                        

                        this.HandleCutsceneXml(ref pendingWavesFiles);
                        break;
                    }
                }
            }

            this.ProcessWavesFiles(pendingWavesFiles);
            this.m_builtWaves.Save(this.m_builtWavesPath);
            ProcessTimer.StopTimer("ParseXml");
        }


        /// <summary>
        /// Builds the lists of banks in cutscenes ONLY and TRIMMED packs
        /// </summary>
        /// <exception cref="ApplicationException">
        /// </exception>
        private void BuildCutsceneToDeleteLists()
        {
            XmlDocument cutsceneBuiltDoc = new XmlDocument();
            XmlDocument cutsceneOnlyBuiltDoc = new XmlDocument();
            var builtWavesFolder = Path.Combine(this.m_projectSettings.GetCurrentPlatform().BuildInfo, "BuiltWaves\\");
            this.m_assetManager.GetLatest(builtWavesFolder, false);
            builtWavesFolder = this.m_assetManager.GetLocalPath(builtWavesFolder);
            String cutsceneBuiltFile = Path.Combine(builtWavesFolder, "CUTSCENE_PACK_FILE.XML");
            String cutsceneOnlyBuiltFile = Path.Combine(builtWavesFolder, "CUTSCENE_MASTERED_ONLY_PACK_FILE.XML");
            if (this.m_assetManager.ExistsAsAsset(cutsceneBuiltFile) && this.m_assetManager.ExistsAsAsset(cutsceneOnlyBuiltFile))
            {
                cutsceneBuiltDoc.Load(cutsceneBuiltFile);
                cutsceneOnlyBuiltDoc.Load(cutsceneOnlyBuiltFile);

                XmlNodeList cutsceneBanks = cutsceneBuiltDoc.DocumentElement.SelectNodes("//Bank");
                XmlNodeList cutsceneOnlyBanks = cutsceneOnlyBuiltDoc.DocumentElement.SelectNodes("//Bank");
                foreach (XmlNode onlyBank in cutsceneOnlyBanks)
                {
                    String onlyBankName = onlyBank.Attributes["name"].Value;
                    bool bankFound = false;
                    foreach (XmlNode bank in cutsceneBanks)
                    {
                        if (String.Equals(bank.Attributes["name"].Value.ToUpper() + "_ONLY", onlyBankName.ToUpper()))
                        {
                            bankFound = true;
                            break;
                        }
                    }
                    if (!bankFound)
                    {
                        m_CutsceneOnlyBanksToDelete.Add(onlyBank);
                    }
                }
            }
        }


        /// <summary>
        /// The process waves files.
        /// </summary>
        /// <param name="pendingWavesFiles">
        /// The pending waves files.
        /// </param>
        /// <exception cref="ApplicationException">
        /// </exception>
        private void ProcessWavesFiles(IEnumerable<string> pendingWavesFiles)
        {
            ProcessTimer.StartTimer("ProcessWavesFiles");
            Console.Out.WriteLine("Processing wave files...");
            foreach (var pendingWavesFile in pendingWavesFiles)
            {
                Console.Out.WriteLine("Processing wave files: " + pendingWavesFile);
                IAsset pendingWavesAsset = null;
                if (m_assetManager.ExistsAsAsset(pendingWavesFile))
                {
                    pendingWavesAsset = this.m_changeList.CheckoutAsset(pendingWavesFile, true);
                }
                if (null == pendingWavesAsset && !pendingWavesFile.Contains("CUTSCENE_MASTERED_TRIMMED") && !pendingWavesFile.Contains("CUTSCENE_MASTERED_ONLY"))
                {
                   throw new Exception("Failed to checkout pending waves file: " + pendingWavesFile);
                }

                var pendingWavesDoc = XDocument.Load(pendingWavesFile);

                if (null == pendingWavesDoc.Root)
                {
                    Console.WriteLine("Pending wave doc invalid: " + pendingWavesFile);
                    Console.WriteLine(pendingWavesDoc.ToString());
                    continue;
                }

                foreach (var pack in pendingWavesDoc.Root.Elements("Pack"))
                {
                    var packName = pack.Attribute("name").Value;
                    Console.Out.WriteLine("Processing pack: " + packName);

                    // if (packName.ToUpper() == "CUTSCENE")
                    // {
                    // ProcessWaveFilesForCutscenePack(pendingWavesFile, pendingWavesAsset, pendingWavesDoc, pack);
                    // return;
                    // }
                    var builtWavesFilePath = Path.Combine(
                        this.m_builtWavesFolder, string.Format("{0}_PACK_FILE.XML", packName.ToUpper()));

                    var assetProcessed = false;
                    var operationAttrib = pack.Attribute("operation");
                    if (operationAttrib != null)
                    {
                        var builtWavesPackListPath = Path.Combine(this.m_builtWavesFolder, "BuiltWaves_PACK_LIST.xml");
                        if (operationAttrib.Value == "add")
                        {
                            if (!this.m_assetManager.IsCheckedOut(builtWavesPackListPath))
                            {
                                if (null == this.m_changeList.CheckoutAsset(builtWavesPackListPath, true))
                                {
                                    throw new Exception("Failed to checkout built waves pack list: " + builtWavesPackListPath);
                                }
                            }

                            if (File.Exists(builtWavesFilePath))
                            {
                                var fileInfo = new FileInfo(builtWavesFilePath);
                                if (fileInfo.IsReadOnly)
                                {
                                    fileInfo.IsReadOnly = false;
                                }
                            }

                            var doc = new XDocument(new XElement("BuiltWaves"));
                            doc.Save(builtWavesFilePath);
                            this.m_changeList.MarkAssetForAdd(builtWavesFilePath);
                            assetProcessed = true;
                        }
                        else if (operationAttrib.Value == "remove")
                        {
                            if (!this.m_assetManager.IsCheckedOut(builtWavesPackListPath))
                            {
                                if (null == this.m_changeList.CheckoutAsset(builtWavesPackListPath, true))
                                {
                                    throw new Exception("Failed to checkout built waves pack list: " + builtWavesPackListPath);
                                }
                            }

                            this.m_changeList.MarkAssetForDelete(builtWavesFilePath);
                            assetProcessed = true;
                        }
                    }

                    if (!assetProcessed)
                    {
                        if (this.m_assetManager.ExistsAsAsset(builtWavesFilePath))
                        {
                            if (!this.m_assetManager.IsCheckedOut(builtWavesFilePath))
                            {
                                if (null == this.m_changeList.CheckoutAsset(builtWavesFilePath, true))
                                {
                                    throw new Exception("Failed to checkout built waves file: " + builtWavesFilePath);
                                }
                            }
                        }
                        else if (!builtWavesFilePath.Contains("CUTSCENE_MASTERED_TRIMMED") && !builtWavesFilePath.Contains("CUTSCENE_MASTERED_ONLY"))
                        {
                            throw new ApplicationException(
                                string.Format(
                                    "Existing pack: {0} does not have a corresponding built waves file: {1}", 
                                    packName, 
                                    builtWavesFilePath));
                        }
                    }

                    this.CombineXml(pack, "/", false);
                }

                if (pendingWavesDoc.Root.Elements().Count() > 0)
                {
                    pendingWavesDoc.Save(pendingWavesFile);
                }
                else if (pendingWavesAsset != null)
                {
                    pendingWavesAsset.Revert();
                    this.m_changeList.MarkAssetForDelete(pendingWavesFile);
                }
            }

            ProcessTimer.StopTimer("ProcessWavesFiles");
        }

        #endregion
    }
}
