﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   The program.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Linq;
using System.Net.Configuration;

namespace audVisemeBuilderStandAlone
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    using audAssetBuilderCommon;

    using rage;
    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    /// <summary>
    /// The program.
    /// </summary>
    public class Program
    {

        #region Static Fields

        /// <summary>
        /// The m_asset mgr.
        /// </summary>
        private static IAssetManager m_assetMgr;

        /// <summary>
        /// The m_build change list.
        /// </summary>
        private static IChangeList m_buildChangeList;

        /// <summary>
        /// The project settings.
        /// </summary>
        private static audProjectSettings m_ProjectSettings;

        /// <summary>
        /// The log writer.
        /// </summary>
        private static StreamWriter logWriter = null;

        #endregion

        #region Methods

        /// <summary>
        /// The add marker file for custom anim.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="outputDir">
        /// The output dir.
        /// </param>
        private static void AddMarkerFileForCustomAnim(string file, string outputDir)
        {
            var useCustomAnim = File.Exists(file.ToUpper().Replace(".PROCESSED", ""));
            if (!useCustomAnim)
            {
                string dir = Path.GetDirectoryName(file);
                string fileName = Path.GetFileName(file.ToUpper().Replace(".PROCESSED", ""));
                if (Directory.Exists(dir))
                {
                    foreach (string subDir in Directory.GetDirectories(dir))
                    {
                        useCustomAnim = File.Exists(subDir + "\\" + fileName);
                        if (useCustomAnim)
                            break;
                    }
                }
            }
            var customAnimFlag = outputDir + "\\" + Path.GetFileNameWithoutExtension(file) + ".CUSTOM_LIPSYNC";
            
            if (useCustomAnim)
            {
                if (!m_assetMgr.ExistsAsAsset(customAnimFlag))
                {
                    if (File.Exists(customAnimFlag))
                    {
                        File.SetAttributes(customAnimFlag, FileAttributes.Normal);
                        File.Delete(customAnimFlag);
                    }

                    var sw = File.CreateText(customAnimFlag);
                    sw.Write("12"); // just write a little something so we don't get zero-size errors
                    sw.Close();

                    m_buildChangeList.MarkAssetForAdd(customAnimFlag);

                    // Console.WriteLine("Adding custom lip-sync anim.");
                    // customMarkerPath = CustomAnimFlag;
                }
            }
            else if (m_assetMgr.ExistsAsAsset(customAnimFlag))
            {
                m_buildChangeList.MarkAssetForDelete(customAnimFlag);
            }
        }

        /// <summary>
        /// The check out or mark for add.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        private static void CheckOutOrMarkForAdd(string fileName)
        {
            if (!File.Exists(fileName))
            {
                var fs = File.Create(fileName);
                fs.Close();
                File.SetAttributes(fileName, FileAttributes.Normal);
            }

            if (m_assetMgr.ExistsAsAsset(fileName))
            {
                m_buildChangeList.CheckoutAsset(fileName, false);
            }
            else
            {
                m_buildChangeList.MarkAssetForAdd(fileName, "binary");
            }
        }


        private static audScriptedSpeechConfig getScriptedSpeechConfigForLocalWavePath(string path)
        {
            return m_ProjectSettings.LipsyncDialogueSettings.ScriptedSpeechConfigs.FirstOrDefault(
                scriptedSpeechConfig => path.ToUpper().Contains(m_assetMgr.GetLocalPath(m_ProjectSettings.LipsyncDialogueSettings.WavesBaseDir + (m_ProjectSettings.LipsyncDialogueSettings.WavesBaseDir.EndsWith("/") ? "" : "/") + scriptedSpeechConfig.Path).ToUpper()));
        }

        private static audAmbientSpeechConfig getAmbientSpeechConfigForLocalWavePath(string path)
        {
            return m_ProjectSettings.LipsyncDialogueSettings.AmbientSpeechConfigs.FirstOrDefault(
                ambientSpeechConfig => path.ToUpper().Contains(m_assetMgr.GetLocalPath(m_ProjectSettings.LipsyncDialogueSettings.WavesBaseDir +(m_ProjectSettings.LipsyncDialogueSettings.WavesBaseDir.EndsWith("/") ? "" : "/")+ ambientSpeechConfig.Path).ToUpper()));
        }

        /// <summary>
        /// The create text files.
        /// </summary>
        /// <param name="dir">
        /// The dir.
        /// </param>
        private static void CreateTextFiles(string dir)
        {
            audScriptedSpeechConfig correspondingScriptedSpeechConfig = getScriptedSpeechConfigForLocalWavePath(dir);
            audAmbientSpeechConfig correspondingAmbientSpeechConfig = getAmbientSpeechConfigForLocalWavePath(dir);
            

            //directory contains scripted speech
            if (correspondingScriptedSpeechConfig!=null)
            {
                foreach (var wavFile in Directory.GetFiles(dir))
                {
                    if (!wavFile.ToUpper().Contains(".WAV"))
                    {
                        continue;
                    }

                    var context = Path.GetFileNameWithoutExtension(wavFile.Replace(".PROCESSED", string.Empty));
                    var contextNoVariation = context.Substring(0, context.Length - 3);
                    var variation = context.Substring(context.Length - 3, 3);
                    var srDialogueFiles =
                        new StreamReader(
                            new FileStream(m_assetMgr.GetLocalPath(correspondingScriptedSpeechConfig.DialogueFilesFile), FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                    var line = srDialogueFiles.ReadLine();
                    var previousLine = line;
                    line = srDialogueFiles.ReadLine();
                    while (line != null)
                    {
                        if (line.ToUpper().Contains(contextNoVariation.ToUpper()))
                        {
                            var textFileName = Path.Combine(Path.GetDirectoryName(wavFile), (context+=".TXT").ToUpper());
                            if (!File.Exists(textFileName))
                            {
                                var fsTxt = File.Create(textFileName);
                                File.SetAttributes(textFileName, FileAttributes.Normal);
                                fsTxt.Close();
                            }
                            StreamWriter textFileWriter = new StreamWriter(textFileName, false, Encoding.UTF8);

                            if (!String.IsNullOrEmpty(previousLine))
                            {
                                var dialogueFileSearchString =
                                 Regex.Replace(previousLine.Substring(1, previousLine.Length - 1), "A:.*", string.Empty);
                                var dialogueFileReader =
                                    new StreamReader(
                                        new FileStream(m_assetMgr.GetLocalPath(correspondingScriptedSpeechConfig.DialogueFile), FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                                var dialogueFileLine = dialogueFileReader.ReadLine();

                                while (dialogueFileLine != null)
                                {
                                    if (dialogueFileLine.ToUpper().Contains(dialogueFileSearchString.ToUpper()))
                                    {
                                        var textToRead = dialogueFileReader.ReadLine();
                                        var textToWrite = Regex.Replace(textToRead, "~.~", string.Empty);

                                        // if its a line with variations
                                        if (Regex.IsMatch(dialogueFileLine, dialogueFileSearchString + "_[0-9][0-9]"))
                                        {
                                            // if it's the line we've already found, the textToWrite is what we want.
                                            // otherwise, search til we find it.
                                            if (!Regex.IsMatch(dialogueFileLine, dialogueFileSearchString + variation))
                                            {
                                                dialogueFileLine = dialogueFileReader.ReadLine();
                                                while (dialogueFileLine != null)
                                                {
                                                    if (Regex.IsMatch(
                                                        dialogueFileLine, dialogueFileSearchString + variation))
                                                    {
                                                        textToRead = dialogueFileReader.ReadLine();
                                                        textToWrite = Regex.Replace(textToRead, "~.~", string.Empty);
                                                        textFileWriter.WriteLine(cleanUpSpeechLine(textToWrite));
                                                        break;
                                                    }
                                                    dialogueFileLine = dialogueFileReader.ReadLine();
                                                }

                                                break;
                                            }
                                        }
                                        // if line has no variations
                                        textFileWriter.WriteLine(cleanUpSpeechLine(textToWrite));
                                        break;
                                        
                                    }
                                    
                                    dialogueFileLine = dialogueFileReader.ReadLine();
                                }
                                dialogueFileReader.Close();
                            }

                            textFileWriter.Flush();
                            textFileWriter.Close();
                            break;
                        }

                        previousLine = line;
                        line = srDialogueFiles.ReadLine();
                    }

                    srDialogueFiles.Close();
                }
            } //directory contains ambient speech
            else if(correspondingAmbientSpeechConfig!=null)
            {
                foreach (var file in Directory.GetFiles(dir))
                {
                    foreach (var lipsyncTextfile in Directory.GetFiles(m_assetMgr.GetLocalPath(m_ProjectSettings.LipsyncDialogueSettings.LipsyncTextPath)))
                    {
                        if (!file.ToUpper().Contains(string.Concat("\\", Path.GetFileNameWithoutExtension(lipsyncTextfile), "\\").ToUpper())
                            && !file.ToUpper().Contains(string.Concat("/", Path.GetFileNameWithoutExtension(lipsyncTextfile), "\\").ToUpper()))
                        {
                            continue;
                        }

                        var lipsyncTextFileReader = new StreamReader(new FileStream(lipsyncTextfile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                        var line = lipsyncTextFileReader.ReadLine();
                        while (line != null)
                        {
                            var lineParts = line.Split('\t');
                            if (lineParts[0].ToUpper().Equals(Path.GetFileNameWithoutExtension(file).ToUpper()))
                            {
                                var textFileName = file.ToUpper().Replace(".WAV", ".TXT");
                                if (!File.Exists(textFileName))
                                {
                                    FileStream fsTxt = File.Create(textFileName);
                                    File.SetAttributes(textFileName, FileAttributes.Normal);
                                    fsTxt.Close();
                                    var swTxt = new StreamWriter(textFileName, false, Encoding.UTF8);
                                    swTxt.WriteLine(cleanUpSpeechLine(lineParts[1]));
                                    swTxt.Flush();
                                    swTxt.Close();
                                }

                                break;
                            }

                            line = lipsyncTextFileReader.ReadLine();
                        }

                        lipsyncTextFileReader.Close();
                        break;
                    }
                }
            }
        }

        //Do some cleaning up
        private static string cleanUpSpeechLine(string line) 
        { 
            //if a line has only one word which contains an apostroph FaceFx crashes. We just add a space to avoid the crash
            if (line.Trim().Split('\'').Length == 2) return line.Replace("\'", " \'");
            else return line;
        }

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        private static void Main(string[] args)
        {
            // Parse args
            string assetProject = "";
            string assetUserName = "";
            string assetPassword = "";
            string projectSettingsPath = "";
            string changeListNo = "";
            string speechFolder = "";
            string wavesFolder = "";
            string exePath = "";
            string faceFXexePath = "";
            string assetServer = "";
            string depotRoot = "//depot";
            string logFile = @"C:\lipsyncLog.txt";

            for (var i = 0; i < args.Length; i++)
            {
                switch (args[i].ToUpper())
                {
                    case "-ASSETSERVER":
                        assetServer = args[++i];
                        break;
                    case "-ASSETPROJECT":
                        assetProject = args[++i];
                        break;
                    case "-ASSETUSERNAME":
                        assetUserName = args[++i];
                        break;
                    case "-ASSETPASSWORD":
                        assetPassword = args[++i];
                        break;
                    case "-PROJECTSETTINGS":
                        projectSettingsPath = args[++i];
                        break;
                    case "-DEPOTROOT":
                        depotRoot = args[++i];
                        break;
                    case "-CHANGE":
                        changeListNo = args[++i];
                        break;
                    case "-SPEECHFOLDER":
                        speechFolder = args[++i];
                        break;
                    case "-WAVESFOLDER":
                        wavesFolder = args[++i];
                        break;
                    case "-MODULEEXEPATH":
                        exePath = args[++i];
                        break;
                    case "-FACEFXEXEPATH":
                        faceFXexePath = args[++i];
                        break;
                    case "-LOGFILE":
                        logFile = args[++i];
                        break;
                }
            }

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            try
            {
                if (!File.Exists(logFile))
                {
                    var fs = File.Create(logFile);
                    File.SetAttributes(logFile, FileAttributes.Normal);
                    fs.Close();
                }

                logWriter = File.AppendText(logFile);
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("Unable to open log file for appending: " + ex);
            }

            if (logWriter != null)
            {
                logWriter.WriteLine("Processing " + speechFolder);
            }

            if (assetServer == string.Empty || assetProject == string.Empty || assetUserName == string.Empty)
            {
                Console.Error.WriteLine();
                Console.Error.WriteLine("Error: Incorrect arguments passed");
                if (logWriter != null)
                {
                    logWriter.WriteLine();
                    logWriter.WriteLine("Error: Incorrect arguments passed");
                }

                Environment.ExitCode = -1;
                return;
            }


            m_assetMgr = AssetManagerFactory.GetInstance(
                AssetManagerType.Perforce, null, assetServer, assetProject, assetUserName, assetPassword, depotRoot);

            projectSettingsPath = m_assetMgr.GetLocalPath(projectSettingsPath);
            m_assetMgr.GetLatest(projectSettingsPath, false);
            m_ProjectSettings = new audProjectSettings(projectSettingsPath);

            string localWaveBaseDir = m_assetMgr.GetLocalPath(m_ProjectSettings.LipsyncDialogueSettings.WavesBaseDir);
            string localStandardCustomAnimDir = m_assetMgr.GetLocalPath(m_ProjectSettings.LipsyncDialogueSettings.CustomAnimationDir);
            string localLipsyncAnimBaseDir = m_assetMgr.GetLocalPath(m_ProjectSettings.LipsyncDialogueSettings.LipsyncAnimsBaseDir);

            var pathToSpeechWaves = string.Concat(wavesFolder, speechFolder);
            Console.Out.WriteLine("Getting latest on " + pathToSpeechWaves + " ... ");
            Console.Out.WriteLine(
                "This might take a little while. Maybe you should go get a snack. You deserve a break, after all.");
            m_assetMgr.GetLatest(m_assetMgr.GetLocalPath(string.Concat(pathToSpeechWaves, "\\...")), true);
            m_assetMgr.GetLatest(m_assetMgr.GetLocalPath(string.Concat(m_ProjectSettings.LipsyncDialogueSettings.CustomAnimationDir, "...")), true);
            //get latest on scripted speech text files & custom anims for dlcs
            foreach (audScriptedSpeechConfig ssConfig in m_ProjectSettings.LipsyncDialogueSettings.ScriptedSpeechConfigs)
            {
                m_assetMgr.GetLatest(m_assetMgr.GetLocalPath(ssConfig.DialogueFile), false);
                m_assetMgr.GetLatest(m_assetMgr.GetLocalPath(ssConfig.DialogueFilesFile), false);
                if(!string.IsNullOrEmpty(ssConfig.CustomAnimationDir)) m_assetMgr.GetLatest(m_assetMgr.GetLocalPath(string.Concat(ssConfig.CustomAnimationDir, "...")), true);
            }
            //get latest on ambient speech text files
            m_assetMgr.GetLatest(m_assetMgr.GetLocalPath(m_ProjectSettings.LipsyncDialogueSettings.LipsyncTextPath), false);
            

            var dirs = Directory.GetDirectories(pathToSpeechWaves).Length != 0
                                ? Directory.GetDirectories(pathToSpeechWaves)
                                : new[] { pathToSpeechWaves };

            // create a build manager list
            foreach (var dir in dirs)
            {
                try
                {
                    // create a build manager list
                    m_buildChangeList = string.IsNullOrEmpty(changeListNo)
                                            ? m_assetMgr.CreateChangeList("Auto-Generation of Lipsync files")
                                            : m_assetMgr.GetChangeList(changeListNo);

                    if (!Directory.Exists(dir))
                    {
                        Console.Out.WriteLine("Failed to find folder " + dir);
                        Environment.ExitCode = -1;
                        return;
                    }


                    string lipsyncAnimsOutputDirectory = dir.ToUpper().Replace(
                        localWaveBaseDir.ToUpper(), 
                        localLipsyncAnimBaseDir.ToUpper()
                        );

                    if (!Directory.Exists(lipsyncAnimsOutputDirectory))
                    {
                        Directory.CreateDirectory(lipsyncAnimsOutputDirectory);
                    }

                    CreateTextFiles(dir);

                    string localCustomAnimDir = localStandardCustomAnimDir;
                    audScriptedSpeechConfig ssConf = getScriptedSpeechConfigForLocalWavePath(dir);
                    if (ssConf != null && !string.IsNullOrEmpty(ssConf.CustomAnimationDir)) localCustomAnimDir = m_assetMgr.GetLocalPath(ssConf.CustomAnimationDir);

                    foreach(String subDir in Directory.GetDirectories(lipsyncAnimsOutputDirectory))
                    {
                        Console.Out.WriteLine("Deleting " + subDir);
                        foreach (String subDirfile in Directory.GetFiles(subDir))
                        {
                            if (File.Exists(subDirfile))
                            {
                                File.SetAttributes(subDirfile, FileAttributes.Normal);
                                File.Delete(subDirfile);
                            }
                        }
                        Directory.Delete(subDir, true);
                    }

                    string[] platformsArray = { /*"ps3", "pc", */"pc64"/*, "xenon"*/ };

                    foreach (var file in Directory.GetFiles(dir))
                    {
                        if (!file.ToUpper().Contains(".WAV"))
                        {
                            continue;
                        }

                        Console.Out.WriteLine("Handling assets for " + file);

                        string pathToAnimation = Path.Combine(localCustomAnimDir, speechFolder, Path.GetFileNameWithoutExtension(file) + ".anim");
                        AddMarkerFileForCustomAnim(pathToAnimation, lipsyncAnimsOutputDirectory);

                        string fileName = Path.GetFileNameWithoutExtension(file);
                        string waveFileDirectory = Path.GetDirectoryName(file);
                        string lipsyncFileDirectory =
                            waveFileDirectory.ToUpper().Replace(localWaveBaseDir.ToUpper(), localLipsyncAnimBaseDir.ToUpper());
                        

                        var dicFilePS3 = Path.Combine(lipsyncFileDirectory, fileName + ".ccd");
                        var dicFileXBOX = Path.Combine(lipsyncFileDirectory, fileName + ".xcd");
                        var dicFilePC = Path.Combine(lipsyncFileDirectory, fileName + ".wcd");
                        var dicFilePC64 = Path.Combine(lipsyncFileDirectory, fileName + ".ycd");
                        var clipFile = Path.Combine(lipsyncFileDirectory, fileName + ".clip");
                        var animFile = Path.Combine(lipsyncFileDirectory, fileName + ".anim");
                        CheckOutOrMarkForAdd(dicFilePS3);
                        CheckOutOrMarkForAdd(dicFileXBOX);
                        CheckOutOrMarkForAdd(dicFilePC);
                        CheckOutOrMarkForAdd(dicFilePC64);
                        CheckOutOrMarkForAdd(clipFile);
                        CheckOutOrMarkForAdd(animFile);

                        foreach (var plat in platformsArray)
                        {
                            var platformClipFile = Path.Combine(lipsyncFileDirectory, fileName + plat + ".clip");
                            var platformAnimFile = Path.Combine(lipsyncFileDirectory, fileName + plat + ".anim");

                            MarkForDelete(platformClipFile);
                            MarkForDelete(platformAnimFile);
                        }
                    }

                    foreach (var plat in platformsArray)
                    {
                        var buildFile = new XmlBuildFile();
                        var visemeTool = new XElement(
                            "Tool", 
                            new XAttribute("Name", "LipSync"), 
                            new XAttribute("Path", exePath), 
                            new XAttribute("AllowRemote", "True"), 
                            new XAttribute("AllowRestartOnLocal", "True"), 
                            new XAttribute("GroupPrefix", "Generating Lipsync Data..."), 
                            new XAttribute("OutputFileMasks", "*.CCD, *.YCD, *.XCD,*.WCD,*.CUSTOM_LIPSYNC,*.WAV,*.ANIM,*.CLIP"));
                        buildFile.ToolsNode.Add(visemeTool);

                        var buildtask = new XElement("TaskGroup", new XAttribute("Name", "LipSyncGen"));

                        // add task group to job root node
                        buildFile.ProjectNode.Add(buildtask);

                        var responseFile = string.Concat(lipsyncAnimsOutputDirectory, "\\XGReponseFile.txt");
                        if (File.Exists(responseFile))
                        {
                            new FileInfo(responseFile) { IsReadOnly = false };
                        }
                        var fs = new FileStream(responseFile, FileMode.Create, FileAccess.Write);
                        var sw = new StreamWriter(fs);
                        foreach (var file in Directory.GetFiles(dir))
                        {
                            if (!file.ToUpper().Contains(".WAV") || File.Exists(file.ToUpper().Replace(".WAV", ".PROCESSED.WAV")))
                            {
                                continue;
                            }

                            sw.WriteLine(file);
                        }

                        sw.Close();
                        fs.Close();

                        var parameterBuilder = new StringBuilder();
                        parameterBuilder.Append("-input ");

                        // add quotes to source path
                        parameterBuilder.Append("\"$(SourcePath)\" ");

                        parameterBuilder.Append("-output ");

                        // add quotes to bank path
                        parameterBuilder.Append("\"");
                        parameterBuilder.Append(lipsyncAnimsOutputDirectory);
                        parameterBuilder.Append("\\\\");
                        parameterBuilder.Append("\" ");

                        parameterBuilder.Append("-CUSTOMANIMDIR ");
                        parameterBuilder.Append("\"");

                        parameterBuilder.Append(dir.ToUpper().Replace(
                           localWaveBaseDir.ToUpper(), 
                           localCustomAnimDir.ToUpper()));
                        parameterBuilder.Append("\\\\");
                        parameterBuilder.Append("\" ");

                        parameterBuilder.Append("-FACEFXEXEPATH ");
                        parameterBuilder.Append("\"");
                        parameterBuilder.Append(faceFXexePath);
                        parameterBuilder.Append("\" ");

                        parameterBuilder.Append("-platform ");
                        parameterBuilder.Append(plat);

                        var parameters = parameterBuilder.ToString();

                        var waveVisemeBuilder = new XElement(
                            "Task", 
                            new XAttribute("Name", string.Concat("lipsync_", dir)), 
                            new XAttribute("SourceFile", string.Concat("@", responseFile)), 
                            new XAttribute("Params", parameters), 
                            new XAttribute("Tool", "Lipsync"), 
                            new XAttribute("StopOnErrors", "true"), 
                            new XAttribute("Caption", "Lipsync $(SourcePath)"));

                        buildtask.Add(waveVisemeBuilder);

                        var ibOutput = new StringBuilder();
                        var ibOutputWaitHandle = new System.Threading.AutoResetEvent(false);

                        string BuildXmlPath = m_ProjectSettings.LipsyncDialogueSettings.LipsyncBuildXMLPath;
                        buildFile.Save(BuildXmlPath);
                        Console.WriteLine("Invoking Incredibuild...");
                        var psi = new ProcessStartInfo("xgconsole.exe")
                                      {
                                          Arguments =
                                              "\"" + BuildXmlPath
                                              + "\" /ShowCmd /OpenMonitor", 
                                          UseShellExecute = false, 
                                          CreateNoWindow = true, 
                                          RedirectStandardOutput = true
                                      };

                        var p = new Process { StartInfo = psi };
                        Console.WriteLine("{0} {1}", psi.FileName, psi.Arguments);
                        p.OutputDataReceived += (sender, e) =>
                            {
                                if (e.Data == null)
                                {
                                    ibOutputWaitHandle.Set();
                                }
                                else
                                {
                                    ibOutput.AppendLine(e.Data);
                                }
                            };

                        p.Start();
                        p.BeginOutputReadLine();
                        if (!p.WaitForExit(int.MaxValue) || !ibOutputWaitHandle.WaitOne(int.MaxValue))
                        {
                            throw new Exception("Problem waiting for Incredibuild to exit.");
                        }

                        if (p.ExitCode == 0)
                        {
                            continue;
                        }

                        var outputlines = ibOutput.ToString().Split('\n');
                        var errormsg = string.Empty;
                        var counter = 0;

                        foreach (var line in outputlines)
                        {
                            if (line.ToLower().Contains("error"))
                            {
                                counter = 8;
                            }

                            if (counter <= 0)
                            {
                                continue;
                            }

                            errormsg = errormsg + line + '\n';
                            counter--;
                        }

                        throw new Exception("Incredibuild exited badly." + errormsg);
                    }

                }
                catch (Exception ex)
                {
                    if (logWriter != null)
                    {
                        logWriter.WriteLine("Error processing {0}: {1}", dir, ex);
                    }

                    Console.Out.WriteLine(ex);

                    Environment.ExitCode = -1;
                }
                finally
                {
                    // delete text files
                    foreach (var file in Directory.GetFiles(dir, "*.txt"))
                    {
                        // oh just in case...
                        if (file.ToUpper().Contains(".WAV"))
                        {
                            continue;
                        }

                        File.Delete(file);
                    }
                }

                try
                {
                    m_buildChangeList.RevertUnchanged();
                    if (m_buildChangeList.Assets.Count != 0)
                    {
                        m_buildChangeList.Submit();
                    }
                    else
                    {
                        m_buildChangeList.Delete();
                    }

                    if (logWriter != null)
                    {
                        logWriter.WriteLine("Finished with " + dir);
                    }
                }
                catch (Exception ex)
                {
                    if (logWriter != null)
                    {
                        logWriter.WriteLine("Failed to submit. Probably didn't change anything.: " + ex);
                    }

                    Console.Out.WriteLine("Failed to submit. Probably didn't change anything.: " + ex);
                    Environment.ExitCode = -1;
                }
            }

            if(logWriter != null) {
                logWriter.Close();
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception) e.ExceptionObject;
            Console.Error.WriteLine("Unhandled exception: " + ex.Message);
            Console.Error.WriteLine(ex.StackTrace);
            if (logWriter != null) {
                logWriter.WriteLine("Unhandled exception: " + ex.Message);
                logWriter.WriteLine(ex.StackTrace);
            }

            Environment.Exit(1);
        }

        /// <summary>
        /// The mark for delete.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        private static void MarkForDelete(string fileName)
        {
            if (m_assetMgr.ExistsAsAsset(fileName))
            {
                m_buildChangeList.MarkAssetForDelete(fileName);
            }
        }

        #endregion
    }
}
