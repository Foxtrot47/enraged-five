#include "stdafx.h"
#include "MP3Encoder.h"

using namespace System;

int main(array<System::String ^> ^args)
{
	String^ file;
	String^ platform;
	int compression;
	int samplerate;
	bool loopWholeFile = false;
	bool preserveTransients = false;
	for(int i=0; i<args->Length; i++)
	{

		if(String::Compare(args[i],"-f",false)==0)
		{
			file = args[++i];
		}
		else if(String::Compare(args[i],"-c",false)==0)
		{
			compression = Int32::Parse(args[++i]);
		}
		else if(String::Compare(args[i],"-s",false)==0)
		{
			//we use the target sample rate NOT the actual one after loop alignment.
			//Lame has to have a valid sample rate as described in MP3Encoder.h
			samplerate = Int32::Parse(args[++i]);
		}
		else if(String::Compare(args[i],"-p",false)==0)
		{
			platform = args[++i];
		}
		else if(String::Compare(args[i],"-loopWholeFile",false) == 0)
		{
			loopWholeFile = true;
		}
		else if(String::Compare(args[i],"-preserveTransient",false) == 0)
		{
			preserveTransients = true;
		}
	}

	if(String::IsNullOrEmpty(file) || String::IsNullOrEmpty(platform))
	{
		Console::WriteLine("Incorrect usage, expected: -f <file> -c <compression> -s<samplerate> -p <platform> [-loopWholeFile -preserveTransient]");
		return -1;
	}

	MP3Encoder^ encoder = gcnew MP3Encoder(file, compression, samplerate, platform, loopWholeFile, preserveTransients);
	if(encoder->Run())
	{
		int headroomMillibels = (int)(encoder->GetPostEncodeHeadroom() * 100.f);
		Console::WriteLine("Headroom in mB: {0}", headroomMillibels);
		return headroomMillibels;
	}
	else
	{
		return -1;
	}
}