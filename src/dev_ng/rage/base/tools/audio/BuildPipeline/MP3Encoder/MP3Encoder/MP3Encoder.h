using namespace Wavelib;
using namespace System;

struct audWaveData
{
	void *waveData;
	unsigned int lengthBytes;
	unsigned int lengthSamples;
	signed int loopStartOffsetSamples;
	unsigned int sampleRate;			
	unsigned int compression;
};

const int g_NumValidSampleRates = 9;
const int g_ValidSampleRates[g_NumValidSampleRates] =
{
	8000,
	11025,
	12000,
	16000,
	22050,
	24000,
	32000,
	44100,
	48000
};

enum audMpegVersions
{
	MPEG1,
	MPEG2,
	MPEG2_5
};

const int g_MpegVersionTable[] =
{
	MPEG2_5,
	-1,
	MPEG2,
	MPEG1
};

enum audMpegLayers
{
	LAYER1,
	LAYER2,
	LAYER3
};

const int g_MpegLayerTable[] =
{
	-1,
	LAYER3,
	LAYER2,
	LAYER1
};

const int g_MpegSlotBytes[] =
{
	4,	//LAYER1
	1,	//LAYER2
	1	//LAYER3
};

const int g_Mpeg1BitrateTable[16][3] =
{
	-1,		-1,		-1,
	32,		32,		32,
	64,		48,		40,
	96,		56,		48,
	128, 	64,		56,
	160, 	80,		64,
	192, 	96,		80,
	224, 	112, 	96,
	256, 	128, 	112,
	288, 	160, 	128,
	320, 	192, 	160,
	352, 	224, 	192,
	384, 	256, 	224,
	416, 	320, 	256,
	448, 	384, 	320,
	-1,		-1,		-1
};

const int g_Mpeg2BitrateTable[16][3] =
{
	-1,		-1,		-1,
	32,		8,		8,
 	48,		16,		16,
 	56,		24,		24,
 	64,		32,		32,
 	80,		40,		40,
 	96,		48,		48,
 	112, 	56, 	56,
 	128, 	64, 	64,
 	144, 	80, 	80,
 	160, 	96, 	96,
 	176, 	112, 	112,
 	192, 	128, 	128,
 	224, 	144, 	144,
 	256, 	160, 	160,
	-1,		-1,		-1
};

const int g_MpegSampleRateTable[4][3] =
{
	44100,	22050,	11025,
	48000,	24000,	12000,
	32000,	16000,	8000,
	-1,		-1,		-1
};

static const unsigned int g_Mp3BlockSamples = 1152; //Align to the largest possible frame size for now.

const int g_Mp3FileBufferBytes = 5*1024*1024;

public ref class MP3Encoder
{
public:
	MP3Encoder(String^ waveName, int compression, int samplerate, String^ platform, bool loopWholeFile, bool preserveTransient);
	~MP3Encoder();
	bool Run();

	float GetPostEncodeHeadroom() { return m_PostEncodeHeadroom; }

private:
	audWaveData* m_WaveData;
	String^ m_DestinationMP3File;
	String^ m_DestinationSeekFile;
	String^ m_Platform;

	bool PreProcessLoop();

	bool EncodeMp3( void **waveSampleDataOut, unsigned int &waveSampleDataOutLengthBytes, 
					unsigned int &samplesPerFrame);

	bool GenerateSeekTable(unsigned char *waveSampleDataOut, unsigned int waveSampleDataOutLengthBytes, 
							unsigned int samplesPerFrame, unsigned short **seekTable,unsigned int &numFrames);

	void PostProcessMp3Frames(void **waveSampleDataOut,	unsigned int &waveSampleDataOutLengthBytes, 
								unsigned int samplesPerFrame,unsigned short *seekTable, unsigned int &numFrames, 
								unsigned short **trimmedSeekTable);

	void SerialiseData(void *waveSampleDataOut, unsigned int waveSampleDataOutLengthBytes,
					unsigned short* trimmedSeekTable, unsigned int numFrames, unsigned int samplesPerFrame);


	float ComputeMP3Headroom(unsigned char *waveSampleDataOut, unsigned int waveSampleDataOutLengthBytes, unsigned int samplesPerFrame);
	
	float m_PostEncodeHeadroom;
};

