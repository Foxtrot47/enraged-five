#include "stdafx.h"
#include "lame.h"
#include "lame_global_flags.h"
#include "MP3Encoder.h"
#include "memory.h"
#include "math.h"

using namespace System::Collections::Generic;
using namespace System::IO;
using namespace audAssetBuilderCommon;

MP3Encoder::MP3Encoder(String^ fileName, int compression, int sampleRate, String^ platform, bool loopWholeFile, bool preserveTransient)
{
	m_Platform = platform->ToUpper();
	m_DestinationMP3File = fileName->ToUpper()->Replace(".WAV",".DATA");
	m_DestinationSeekFile = fileName->ToUpper()->Replace(".WAV",".SEEKTABLE");

	Console::WriteLine("MP3 encoding {0}, compression {1}, sampleRate {2}, preserveTransient {3}", 
									fileName,
									compression,
									sampleRate,
									preserveTransient ? "yes" : "no");

	bwWaveFile ^wave = gcnew bwWaveFile(fileName,false);
	array<unsigned char>^ rawData = wave->Data->RawData;
	pin_ptr<unsigned char> pinnedRawData = &rawData[0];
		
	//Pull out wave data
	m_WaveData = new audWaveData;
	
	m_WaveData->compression = compression;
	m_WaveData->lengthBytes = wave->Data->RawData->Length;
	m_WaveData->lengthSamples = wave->Data->NumSamples;
	m_WaveData->sampleRate = sampleRate;
	if(wave->Sample && wave->Sample->Loops->Count == 1)
	{
		m_WaveData->loopStartOffsetSamples = wave->Sample->Loops[0]->Start;
	}
	else if(loopWholeFile)
	{
		m_WaveData->loopStartOffsetSamples = 0;
	}
	else
	{
		m_WaveData->loopStartOffsetSamples = -1;
	}

	bool shouldPadStart = m_WaveData->loopStartOffsetSamples == -1 && preserveTransient;
	
	char *waveData = NULL;
	if(shouldPadStart)
	{
		// Add one MP3 frame of silence to improve initial transient performance
		int samplesPadding = 1152 + 64;
		int bytesPadding = samplesPadding * 2;
		m_WaveData->lengthBytes += bytesPadding;
		m_WaveData->lengthSamples += samplesPadding;
		waveData = new char[m_WaveData->lengthBytes];
		memset(waveData, 0, bytesPadding);
		memcpy(waveData + bytesPadding, pinnedRawData, wave->Data->RawData->Length);
	}
	else
	{
		waveData = new char[wave->Data->RawData->Length];
		memcpy(waveData,pinnedRawData,wave->Data->RawData->Length);
	}

	m_WaveData->waveData = waveData;
}

MP3Encoder::~MP3Encoder()
{
	delete[] m_WaveData->waveData;
	delete m_WaveData;
}

bool MP3Encoder::Run()
{
	if(m_WaveData->loopStartOffsetSamples >= 0)
	{
		PreProcessLoop();
	}

	unsigned int samplesPerFrame;
	void *waveSampleDataOut;
	unsigned int waveSampleDataOutLengthBytes; 
	unsigned short *seekTable;
	unsigned int numFrames;

	if(!EncodeMp3(&waveSampleDataOut, waveSampleDataOutLengthBytes, samplesPerFrame))
	{
		return false;
	}

	// decode and compute post-encoder headroom
	m_PostEncodeHeadroom = ComputeMP3Headroom((unsigned char*)waveSampleDataOut, waveSampleDataOutLengthBytes, samplesPerFrame);
	Console::WriteLine("Post-encoding headroom: {0}dB", m_PostEncodeHeadroom);

	//Generate a seek table by parsing the generated data.	
	if(!GenerateSeekTable((unsigned char*)waveSampleDataOut, waveSampleDataOutLengthBytes, samplesPerFrame,
		&seekTable, numFrames))
	{
		delete[] waveSampleDataOut;
		return false;
	}

	//Trim sample and seek table frames.
	unsigned short *trimmedSeekTable;
	PostProcessMp3Frames(&waveSampleDataOut, waveSampleDataOutLengthBytes, samplesPerFrame,
							seekTable, numFrames, &trimmedSeekTable);

	//Serialise MP3 file and seek table
	SerialiseData(waveSampleDataOut, waveSampleDataOutLengthBytes, trimmedSeekTable, numFrames, samplesPerFrame);

	m_WaveData->lengthSamples = numFrames * samplesPerFrame;

	delete[] waveSampleDataOut;
	delete[] seekTable;

	return true;
}
#define Abs(x) (x>=0?x:-x)
#define Min(x,y) (x<y?x:y)

float MP3Encoder::ComputeMP3Headroom(unsigned char *waveSampleDataOut, unsigned int waveSampleDataOutLengthBytes, unsigned int samplesPerFrame)
{
	short pcmData_L[1152], pcmData_R[1152];

	memset(pcmData_L, 0, sizeof(pcmData_L));
	memset(pcmData_R, 0, sizeof(pcmData_R));

	lame_decode_init();

	short absPeakSample = 0;

	for(unsigned int byteOffset=0; byteOffset<waveSampleDataOutLengthBytes; byteOffset++)
	{
		unsigned char *frame = waveSampleDataOut + byteOffset;

		//Find the frame sync.
		if((frame[0] == 0xFF) && ((frame[1] & 0xE0) == 0xE0))
		{
			unsigned int versionIndex = (frame[1] >> 3) & 0x3;
			int version = g_MpegVersionTable[versionIndex];
			if(version < 0)
			{
				fprintf(stderr, "Error: Invalid MPEG version\r\n");				
				break;
			}

			unsigned int layerIndex = (frame[1] >> 1) & 0x3;
			int layer = g_MpegLayerTable[layerIndex];
			if(layer < 0)
			{
				fprintf(stderr, "Error: Invalid MPEG layer\r\n");
				break;
			}

			unsigned int bitrateIndex = (frame[2] >> 4) & 0xF;
			int bitrate;
			if(version == MPEG1)
			{
				bitrate = g_Mpeg1BitrateTable[bitrateIndex][layer] * 1000;
			}
			else
			{
				bitrate = g_Mpeg2BitrateTable[bitrateIndex][layer] * 1000;
			}

			if(bitrate < 0)
			{
				fprintf(stderr, "Error: Invalid MPEG bitrate\r\n");
				break;
			}

			unsigned int sampleRateIndex = (frame[2] >> 2) & 0x3;
			int sampleRate = g_MpegSampleRateTable[sampleRateIndex][version];
			if(sampleRate < 0)
			{
				fprintf(stderr, "Error: Invalid MPEG sample rate\r\n");
				break;
			}

			unsigned int padding = (frame[2] >> 1) & 0x1;
			int paddingBytes = padding * g_MpegSlotBytes[layer];

			int frameBytes = ((samplesPerFrame / 8 * bitrate) / sampleRate) + paddingBytes;

			unsigned int protection = frame[1] & 0x01;
			if(protection == 0)
			{
				//There should be a 16-bit CRC at the end of the header.
				frameBytes += 2;
			}

			int samplesGenerated = lame_decode1(frame, frameBytes, pcmData_L, pcmData_R);
			for(int i = 0; i < samplesGenerated; i++)
			{
				short absSample = Abs(pcmData_L[i]);
				if(absSample > absPeakSample)
				{
					absPeakSample = absSample;
				}
			}

			//Jump to the end of this frame.
			byteOffset += frameBytes - 1;
		}
	}

	lame_decode_exit();

	// convert to dB
	const float linearVolume = Min(1.f,absPeakSample / 32767.f);
	const float silenceVolumeLinear = 0.00001f;
	const float dbVolume = linearVolume > silenceVolumeLinear ? (20.f * log10(linearVolume)) : -100.f;
	// subtract peak from 0dB to get headroom
	return (0.f - dbVolume);
}

bool MP3Encoder::PreProcessLoop()
{
	if(m_WaveData->loopStartOffsetSamples == 0)
	{
		//Concatenate 3 full loops to present to the MP3 encoder.
		short *concatenatedLoops = new short[m_WaveData->lengthSamples * 3];
		for(int i=0; i<3; i++)
		{
			memcpy(concatenatedLoops + (i * m_WaveData->lengthSamples), m_WaveData->waveData,
				m_WaveData->lengthSamples * sizeof(short));
		}

		delete[] m_WaveData->waveData;
		m_WaveData->waveData = concatenatedLoops;
		m_WaveData->lengthSamples *= 3;
	}
	else
	{
		//Concatenate 3 full loops onto the preloop to present to the MP3 encoder.
		// - moving the loop point to the end of the first loop.
		unsigned int loopLengthSamples = m_WaveData->lengthSamples -
			(unsigned int)m_WaveData->loopStartOffsetSamples;
		unsigned int numConcatenatedSamples = (unsigned int)m_WaveData->loopStartOffsetSamples + 
			(loopLengthSamples * 3);
		short *concatenatedLoops = new short[numConcatenatedSamples];

		//Copy preloop.
		memcpy(concatenatedLoops, m_WaveData->waveData, m_WaveData->loopStartOffsetSamples * sizeof(short));
		//Copy loop 3 times.
		for(unsigned int i=0; i<3; i++)
		{
			memcpy(concatenatedLoops + m_WaveData->loopStartOffsetSamples +
				(i * loopLengthSamples), ((short *)m_WaveData->waveData) +
				m_WaveData->loopStartOffsetSamples, loopLengthSamples * sizeof(short));
		}

		delete[] m_WaveData->waveData;
		m_WaveData->waveData = concatenatedLoops;
		m_WaveData->lengthSamples = numConcatenatedSamples;
		//Move the loop point to the end of the first loop.
		m_WaveData->loopStartOffsetSamples += loopLengthSamples;
	}

	return true;
}



bool MP3Encoder::EncodeMp3( void **waveSampleDataOut, unsigned int &waveSampleDataOutLengthBytes, unsigned int &samplesPerFrame)
{
	//Map compression setting to MP3 VBR quality - 0(best) to 9(worst).
	int vbrQuality = 10 - (int)ceil((float)m_WaveData->compression / 10.0f);
	if(vbrQuality == 10)
	{
		vbrQuality = 9; //Compression setting 0 will give the same compression as 1 to 10.
	}

	lame_global_flags *globalFlags = lame_init();
	lame_set_bWriteVbrTag(globalFlags, 0); //No header.
	lame_set_num_samples(globalFlags, m_WaveData->lengthSamples);

	// Match to the closest valid samplerate
	// Don't allow MP3 sample rate to go below 32kHz, otherwise we end up with 576 sample frames which our
	// runtime doesn't support
	int mp3SampleRate = m_WaveData->sampleRate;
	if(mp3SampleRate <= 32000)
	{
		mp3SampleRate = 32000;
	}
	else if(mp3SampleRate <= 44100)
	{
		mp3SampleRate = 44100;
	}
	else
	{
		mp3SampleRate = 48000;
	}
	lame_set_in_samplerate(globalFlags, mp3SampleRate);
	lame_set_num_channels(globalFlags, 1); //We only support mono input.
	lame_set_out_samplerate(globalFlags, mp3SampleRate);
	lame_set_quality(globalFlags, 2); //Use a better (but slower) quality compression algorithm.
	lame_set_VBR(globalFlags, vbr_default);
	lame_set_VBR_q(globalFlags, vbrQuality);
	lame_set_lowpassfreq(globalFlags, -1); //Disable low-pass filtering.
	lame_set_highpassfreq(globalFlags, -1); //Disable high-pass filtering.
	lame_set_disable_reservoir(globalFlags, 1); //Disable the bit reservoir to allow seeking/looping to an arbitrary frame.

	lame_init_params(globalFlags);


	if(0 == lame_get_version(globalFlags))
	{
		// For MPEG-II, only 576 samples per frame per channel
		samplesPerFrame = 576;
		// We currently only support 1152 samples per frame
		fprintf(stderr, "Error: SamplesPerFrame 576\n");
		return false;
	}
	else
	{
		// For MPEG-I, 1152 samples per frame per channel
		samplesPerFrame = 1152;
	}

	//Zero-pad the raw wave data (into a new buffer) to ensure it aligns to the frame size.
	unsigned int paddingSamples = samplesPerFrame - (m_WaveData->lengthSamples % samplesPerFrame);
	if(paddingSamples == samplesPerFrame)
	{
		paddingSamples = 0;
	}

	m_WaveData->lengthBytes = m_WaveData->lengthSamples * 2;
	unsigned int unpaddedLengthBytes = m_WaveData->lengthBytes;
	m_WaveData->lengthSamples += paddingSamples;
	m_WaveData->lengthBytes += paddingSamples * 2;

	unsigned char *paddedBuffer = new unsigned char[m_WaveData->lengthBytes];
	memcpy(paddedBuffer, m_WaveData->waveData, unpaddedLengthBytes);
	if(paddingSamples > 0)
	{
		memset(paddedBuffer + unpaddedLengthBytes, 0, paddingSamples * 2);
	}

	int encodedBufferBytes = (5 * m_WaveData->lengthSamples / 4) + 7200;
	unsigned char *encodeBuffer = new unsigned char [encodedBufferBytes];

	int bytesEncoded;
	int totalBytesEncoded = 0;
	for(unsigned int inputSampleOffset=0; inputSampleOffset<m_WaveData->lengthSamples; inputSampleOffset+=samplesPerFrame)
	{
		bytesEncoded = lame_encode_buffer(globalFlags, ((const short *)paddedBuffer) + inputSampleOffset,
			NULL, samplesPerFrame, encodeBuffer + totalBytesEncoded, encodedBufferBytes - totalBytesEncoded);

		if(bytesEncoded < 0)
		{
			fprintf(stderr, "Error: LAME error encoding MP3\r\n");
			lame_close(globalFlags);
			delete[] paddedBuffer;
			delete[] encodeBuffer;
			return false;
		}

		totalBytesEncoded += bytesEncoded;
	}

	bytesEncoded = lame_encode_flush(globalFlags, encodeBuffer + totalBytesEncoded, encodedBufferBytes - totalBytesEncoded);

	delete[] paddedBuffer;

	if(bytesEncoded < 0)
	{
		fprintf(stderr, "Error: LAME error encoding MP3\r\n");
		lame_close(globalFlags);
		delete[] encodeBuffer;
		return false;
	}

	totalBytesEncoded += bytesEncoded;
	lame_close(globalFlags);

	*waveSampleDataOut = encodeBuffer;
	waveSampleDataOutLengthBytes = totalBytesEncoded;
	
	return true;
}

bool MP3Encoder::GenerateSeekTable(unsigned char *waveSampleDataOut,
	unsigned int waveSampleDataOutLengthBytes, unsigned int samplesPerFrame, unsigned short **seekTable,
	unsigned int &numFrames)
{
	//Let's go crazy and allocate enough memory for 1-byte frames - just to be on the safe side...
	*seekTable = new unsigned short [waveSampleDataOutLengthBytes];
	numFrames = 0;

	unsigned char *frame;
	for(unsigned int byteOffset=0; byteOffset<waveSampleDataOutLengthBytes; byteOffset++)
	{
		frame = waveSampleDataOut + byteOffset;

		//Find the frame sync.
		if((frame[0] == 0xFF) && ((frame[1] & 0xE0) == 0xE0))
		{
			/*unsigned int privateBit = frame[2] & 0x1;
			unsigned int channelMode = (frame[3] >> 6) & 0x3;
			unsigned int modeExtension = (frame[3] >> 4) & 0x3;
			unsigned int copyright = (frame[3] >> 3) & 0x1;
			unsigned int original = (frame[3] >> 2) & 0x1;
			unsigned int emphasis = frame[3] & 0x3;*/

			unsigned int versionIndex = (frame[1] >> 3) & 0x3;
			int version = g_MpegVersionTable[versionIndex];
			if(version < 0)
			{
				fprintf(stderr, "Error: Invalid MPEG version\r\n");
				delete[] seekTable;
				return false;
			}

			unsigned int layerIndex = (frame[1] >> 1) & 0x3;
			int layer = g_MpegLayerTable[layerIndex];
			if(layer < 0)
			{
				fprintf(stderr, "Error: Invalid MPEG layer\r\n");
				delete[] seekTable;
				return false;
			}

			unsigned int bitrateIndex = (frame[2] >> 4) & 0xF;
			int bitrate;
			if(version == MPEG1)
			{
				bitrate = g_Mpeg1BitrateTable[bitrateIndex][layer] * 1000;
			}
			else
			{
				bitrate = g_Mpeg2BitrateTable[bitrateIndex][layer] * 1000;
			}

			if(bitrate < 0)
			{
				fprintf(stderr, "Error: Invalid MPEG bitrate\r\n");
				delete[] seekTable;
				return false;
			}

			unsigned int sampleRateIndex = (frame[2] >> 2) & 0x3;
			int sampleRate = g_MpegSampleRateTable[sampleRateIndex][version];
			if(sampleRate < 0)
			{
				fprintf(stderr, "Error: Invalid MPEG sample rate\r\n");
				delete[] seekTable;
				return false;
			}

			unsigned int padding = (frame[2] >> 1) & 0x1;
			int paddingBytes = padding * g_MpegSlotBytes[layer];

			int frameBytes = ((samplesPerFrame / 8 * bitrate) / sampleRate) + paddingBytes;

			unsigned int protection = frame[1] & 0x01;
			if(protection == 0)
			{
				//There should be a 16-bit CRC at the end of the header.
				frameBytes += 2;
			}

			(*seekTable)[numFrames] = (unsigned short)frameBytes;
			numFrames++;

			//Jump to the end of this frame.
			byteOffset += frameBytes - 1;
		}
	}

	return true;
}

void MP3Encoder::PostProcessMp3Frames(void **waveSampleDataOut,	unsigned int &waveSampleDataOutLengthBytes, 
								unsigned int samplesPerFrame,unsigned short *seekTable, unsigned int &numFrames, 
								unsigned short **trimmedSeekTable)
{
	if(m_WaveData->loopStartOffsetSamples == 0)
	{
		//Extract the central loop from the 3 concatenated loops we encoded.
		unsigned int startFrame = ((numFrames - 2) / 3) + 1; //Trim the frame of silence at the start and end.
		unsigned int endFrame = ((numFrames - 2) * 2 / 3) + 1; //Trim the frame of silence at the start and end.

		unsigned int startOffsetBytes = 0;
		unsigned int sizeBytes = 0;
		for(unsigned int i=0; i<startFrame; i++)
		{
			startOffsetBytes += (unsigned int)seekTable[i];
		}

		for(unsigned int i=startFrame; i<endFrame; i++)
		{
			sizeBytes += (unsigned int)seekTable[i];
		}

		unsigned char *extractedLoop = new unsigned char[sizeBytes];
		memcpy(extractedLoop, (unsigned char *)(*waveSampleDataOut) + startOffsetBytes, sizeBytes);
		delete[] *waveSampleDataOut;
		*waveSampleDataOut = extractedLoop;

		*trimmedSeekTable = seekTable + startFrame;
		numFrames = endFrame - startFrame;

		waveSampleDataOutLengthBytes = sizeBytes;
	}
	else if(m_WaveData->loopStartOffsetSamples > 0)
	{
		//Cut the 3rd loop off the end of the preloop and 3 loops we encoded.
		unsigned int loopLengthSamples = (m_WaveData->lengthSamples - m_WaveData->loopStartOffsetSamples) / 2;
		unsigned int endSample = m_WaveData->loopStartOffsetSamples + loopLengthSamples;
		numFrames = (endSample / samplesPerFrame) + 1; //Take the frame of silence at the start into account.

		waveSampleDataOutLengthBytes = 0;
		//Strip the first frame of silent MP3 data.
		for(unsigned int i=1; i<numFrames; i++)
		{
			waveSampleDataOutLengthBytes += (unsigned int)seekTable[i];
		}

		unsigned char *trimmedEncodedBuffer = new unsigned char[waveSampleDataOutLengthBytes];
		memcpy(trimmedEncodedBuffer, (unsigned char *)(*waveSampleDataOut) + seekTable[0], waveSampleDataOutLengthBytes);
		delete[] *waveSampleDataOut;
		*waveSampleDataOut = trimmedEncodedBuffer;

		*trimmedSeekTable = seekTable + 1;
		numFrames--;
	}
	else if(numFrames > 2)
	{
		//Strip the first and last frames of silent MP3 data.
		waveSampleDataOutLengthBytes -= seekTable[0] + seekTable[numFrames - 1];
		unsigned char *trimmedEncodedBuffer = new unsigned char[waveSampleDataOutLengthBytes];
		memcpy(trimmedEncodedBuffer, (unsigned char *)(*waveSampleDataOut) + seekTable[0], waveSampleDataOutLengthBytes);
		delete[] *waveSampleDataOut;
		*waveSampleDataOut = trimmedEncodedBuffer;

		*trimmedSeekTable = seekTable + 1;
		numFrames -= 2;
	}
	else
	{
		*trimmedSeekTable = seekTable;
	}
}

void MP3Encoder::SerialiseData(void *waveSampleDataOut, unsigned int waveSampleDataOutLengthBytes, 
							   unsigned short* seekTable, unsigned int numFrames, unsigned int samplesPerFrame)
{
		FileStream ^fs = gcnew FileStream(m_DestinationMP3File, FileMode::Create);
		BinaryWriter^ bw = gcnew BinaryWriter(fs);
			
		unsigned char* data = (unsigned char*) waveSampleDataOut;

		for(unsigned int i=0; i<waveSampleDataOutLengthBytes; i++)
		{
			bw->Write(data[i]);			
		}

		bw->Flush();
		bw->Close();
		fs->Close();

		fs = gcnew FileStream(m_DestinationSeekFile, FileMode::Create);
		bw = gcnew BinaryWriter(fs);

		for(unsigned int i=0; i<numFrames; i++)
		{
			bw->Write(PlatformSpecific::FixEndian(seekTable[i],m_Platform));
		}

		bw->Flush();
		bw->Close();
		fs->Close();

		fs = gcnew FileStream(m_DestinationSeekFile->ToUpper()->Replace("SEEKTABLE","METADATA"), FileMode::Create);
		bw = gcnew BinaryWriter(fs);
		bw->Write(PlatformSpecific::FixEndian(numFrames,m_Platform));
		bw->Write(PlatformSpecific::FixEndian(samplesPerFrame,m_Platform));
		bw->Flush();
		bw->Close();
		fs->Close();
}