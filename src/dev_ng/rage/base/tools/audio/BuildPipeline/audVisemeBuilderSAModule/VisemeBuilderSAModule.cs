﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

using RSG.ManagedRage.ClipAnimation;

namespace audVisemeBuilderSAModule
{
    public class VisemeBuilderSAModule
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern int GetShortPathName(String pathName, StringBuilder shortName, int cbShortName);

        private String input = "";
        private String output = "";
        private String Platform = "";
        private String tempFolder = "";
        private const String groupName = "group";
        private String animSetPath = "";
        private String FaceFXActorSaveFile = "";
        private String FaceFXCollapsedActorFile = "";
        private String rexAnimSetPath = "";
        private String rexAnimPath = "";
        private String rexClipPath = "";
        private String rexAnimCopyToPath = "";
        private String rexClipCopyToPath = "";
        private String clipListFile = "";
        private String scriptFile = "";
        private String luaScriptFile = "";
        private String platformString = "";
        private String customAnimPath = "";
        private String customClipPath = "";
        private bool useCustomAnim = false;

        private bool isPs3 = false;
        private bool isPC = false;
        private bool isPC64 = false;

        private String FxStudioExePath = "";

        private String RexExePath = ConfigurationManager.AppSettings["RexExePath"];
        private String CompressExePath = ConfigurationManager.AppSettings["CompressExePath"];
        private String RagebuilderExePath = ConfigurationManager.AppSettings["RagebuilderExePath"];
        private String Ragebuilder64ExePath = ConfigurationManager.AppSettings["Ragebuilder64ExePath"];

        String CustomAnimDir = "";

        private String FaceFXActorOriginalFile = ConfigurationManager.AppSettings["FaceFXActorOriginalFile"];
        
        private TextWriter tw;

        private bool initFailed = false;
        
        public VisemeBuilderSAModule(string[] args)
        {
            for (var i = 0; i < args.Length; i++)
            {
                switch (args[i].ToUpper())
                {
                    case "-INPUT":
                        input = args[++i];
                        break;
                    case "-OUTPUT":
                        output = args[++i];
                        break;
                    case "-CUSTOMANIMDIR":
                        CustomAnimDir = args[++i];
                        break;
                    case "-PLATFORM":
                        Platform = args[++i];
                        break;
                    case "-FACEFXEXEPATH":
                        FxStudioExePath = args[++i];
                        break;
                }
            }

            tempFolder = output;

            isPs3 = (String.Compare(Platform, "Playstation 3", true) == 0 || String.Compare(Platform, "PS3", true) == 0);
            isPC = (String.Compare(Platform, "PC", true) == 0);
            isPC64 = (String.Compare(Platform, "PC64", true) == 0);

            if (!Directory.Exists(tempFolder + Platform))
                Directory.CreateDirectory(tempFolder + Platform);
            if (!Directory.Exists(output + Platform))
                Directory.CreateDirectory(output + Platform);

            animSetPath = output + Platform + "\\" + Path.GetFileNameWithoutExtension(input) + ".animset";
            rexAnimSetPath = tempFolder + Platform + "\\" + Path.GetFileNameWithoutExtension(input) + ".animset";
            rexAnimCopyToPath = tempFolder + Path.GetFileNameWithoutExtension(input) + ".anim";
            rexClipCopyToPath = tempFolder + Path.GetFileNameWithoutExtension(input) + ".clip";
            customAnimPath = CustomAnimDir + Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(input)) + ".anim";
            customClipPath = CustomAnimDir + Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(input)) + ".clip";
            FaceFXActorSaveFile = tempFolder + Platform + "\\" + Path.GetFileNameWithoutExtension(input) + ".facefx";
            FaceFXCollapsedActorFile = tempFolder+ Platform + "\\" + Path.GetFileNameWithoutExtension(input) + "-FG_collapsed.facefx";

            scriptFile = tempFolder + Platform + "\\" + Path.GetFileNameWithoutExtension(input) + ".fxl";
            clipListFile = tempFolder + Platform + "\\" + Path.GetFileNameWithoutExtension(input) + ".cliplist";

            luaScriptFile = tempFolder + Platform + "\\" + Path.GetFileNameWithoutExtension(input) + ".rbs";
            platformString = isPs3 ? "ps3" : (isPC ? "win32" : (isPC64 ? "win64" : "xenon"));

            useCustomAnim = File.Exists(customAnimPath);

            //If using custom anim, make sure we yank the .PROCESSED from the file name.  For non-custom, however, we need to keep this.
            if (useCustomAnim)
            {
                rexAnimPath = tempFolder + Platform + "\\" + Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(input)) + ".anim";
                rexClipPath = tempFolder + Platform + "\\" + Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(input)) + ".clip";
            }
            else
            {
                rexAnimPath = tempFolder + Platform + "\\" + Path.GetFileNameWithoutExtension(input) + ".anim";
                rexClipPath = tempFolder + Platform + "\\" + Path.GetFileNameWithoutExtension(input) + ".clip";
            }
        }

        public int Run()
        {
            int ret = 0;
            try
            {
                if (initFailed)
                {
                    Console.Out.WriteLine("Failed initializing" + input);
                    return 1;
                }

                if (!useCustomAnim)
                    BuildFXLScript();
                BuildLuaScript();
                ret = RunFXLAndRex() ? 0 : 1;
                if (isPC64)
                {
                    if (File.Exists(rexAnimCopyToPath))
                        File.SetAttributes(rexAnimCopyToPath, FileAttributes.Normal);
                    File.Copy(rexAnimPath, rexAnimCopyToPath, true);
                    File.SetAttributes(rexAnimCopyToPath, FileAttributes.ReadOnly);

                    if (File.Exists(rexClipCopyToPath))
                        File.SetAttributes(rexClipCopyToPath, FileAttributes.Normal);
                    File.Copy(rexClipPath, rexClipCopyToPath, true);
                    File.SetAttributes(rexClipCopyToPath, FileAttributes.ReadOnly);
                }
                Cleanup();
                return ret;
            }
            catch (System.Exception ex)
            {
                Console.Out.WriteLine(ex);
                return 1;
            }
        }

        private void BuildFXLScript()
        {
            try
            {

                tw = new StreamWriter(scriptFile);

                tw.WriteLine(String.Concat("loadActor -file \"", FaceFXActorOriginalFile, "\";"));
                tw.WriteLine(String.Concat("analyze -au \"", input, "\" -aa \"Default\" -group ", groupName, ";"));
                tw.WriteLine(String.Concat("saveActor -file \"", FaceFXActorSaveFile, "\";"));
                tw.WriteLine(String.Concat("fgcollapse -file \"", FaceFXActorSaveFile, "\";"));
                tw.WriteLine(String.Concat("closeActor;"));
                tw.WriteLine(String.Concat("loadActor -file \"", FaceFXCollapsedActorFile, "\";"));
                tw.WriteLine(String.Concat("select -type \"animgroup\" -names \"", groupName, "\";"));
                tw.WriteLine(String.Concat("animSet -export \"", groupName, "\" -to \"", animSetPath, "\";"));
                tw.Flush();
            }
            finally
            {
                tw.Close();
            }
        }

        private void BuildLuaScript()
        {
            try
            {
                tw = new StreamWriter(luaScriptFile);

                tw.WriteLine(String.Concat("set_platform( \"", platformString, "\" )"));
                tw.WriteLine("start_build() ");
                tw.WriteLine(String.Concat("load_clip( \"", rexClipPath.Replace("\\", "/"), "\" )"));
                tw.WriteLine(String.Concat("local result = save_clip_dictionary(\"", (tempFolder + Path.GetFileNameWithoutExtension(input) + (isPs3 ? ".ccd" : (isPC ? ".wcd" : (isPC64 ? ".ycd" : ".xcd")))).Replace("\\", "\\\\"), "\", 0)"));

                tw.Flush();
            }
            finally
            {
                tw.Close();
            }
        }

        private bool RunFXLAndRex()
        {
            ProcessStartInfo procInfo;
            Process proc;
            String output;
            int exitCode = 0;

            //Only worry about running faceFX if we don't have a custom anim already made.
            if (!useCustomAnim)
            {

                // Try to resolve FxStudio location - will be in different locations on different machines
                // (may want to pass this in as a parameter, or check the registry for the install location)
                String FullFxStudioExePath = Environment.ExpandEnvironmentVariables(FxStudioExePath);
                if (!File.Exists(FullFxStudioExePath))
                {
                    Console.WriteLine("Error - could not find FxStudio executable: " + Environment.ExpandEnvironmentVariables(FxStudioExePath));
                    return false;
                }

                procInfo = new ProcessStartInfo("\"" + FullFxStudioExePath + "\"");
                procInfo.RedirectStandardOutput = true;
                procInfo.UseShellExecute = false;
                procInfo.WindowStyle = ProcessWindowStyle.Hidden;
                procInfo.CreateNoWindow = true;
                procInfo.Arguments = "-exec \"" + scriptFile;

                Console.WriteLine("Running \"" + FullFxStudioExePath + "\" " + procInfo.Arguments);

                proc = new Process();
                proc.StartInfo = procInfo;

                proc.Start();
                output = proc.StandardOutput.ReadToEnd();
                proc.WaitForExit();
                exitCode = proc.ExitCode;
                proc.Close();
                if (exitCode != 0)
                {
                    Console.WriteLine("Error - FaceFX step failed for: " + input);
                    Console.WriteLine(output);
                    return false;
                }
                Console.WriteLine(output);

                procInfo = new ProcessStartInfo(RexExePath);
                procInfo.RedirectStandardOutput = true;
                procInfo.UseShellExecute = false;
                procInfo.CreateNoWindow = true;
                procInfo.WindowStyle = ProcessWindowStyle.Hidden;

                // rexFaceFxRage can't handle quotes or filenames with spaces right now
                procInfo.Arguments = String.Format("-outputdir={0}\\ -input={1}", Path.Combine(tempFolder, Platform), rexAnimSetPath);

                Console.WriteLine("Running " + RexExePath + " " + procInfo.Arguments);

                proc = new Process();
                proc.StartInfo = procInfo;
                proc.Start();
                output = proc.StandardOutput.ReadToEnd();
                proc.WaitForExit();
                exitCode = proc.ExitCode;
                proc.Close();
                if (exitCode != 0)
                {
                    Console.WriteLine("Error - rexFaceFX step failed for: " + input);
                    Console.WriteLine(output);
                    return false;
                }

            }

            //Compress the animation
            procInfo = new ProcessStartInfo(CompressExePath);
            procInfo.RedirectStandardOutput = true;
            procInfo.UseShellExecute = false;
            procInfo.CreateNoWindow = true;
            procInfo.WindowStyle = ProcessWindowStyle.Hidden;

            // rexFaceFxRage can't handle quotes or filenames with spaces right now
            if (!useCustomAnim)
                procInfo.Arguments = "-anim=" + rexAnimPath + " -out=" + rexAnimPath;
            else
            {
                //MClip.Init();
                //MClip managedClip = new MClip(MClip.ClipType.Normal);
                String templateFile = "";
                
                if(File.Exists(rexClipPath))
                    File.SetAttributes(rexClipPath, FileAttributes.Normal);
                File.Copy(customClipPath, rexClipPath, true);
                File.SetAttributes(rexClipPath, FileAttributes.ReadOnly);
                /*
                if (File.Exists(rexClipPath))
                {
                    if (Convert.ToBoolean(managedClip.Load(rexClipPath)))
                    {
                        foreach (MProperty prop in managedClip.GetProperties())
                        {
                            if (String.Compare(prop.GetName(), "Compressionfile_DO_NOT_RESOURCE", true) == 0)
                            {
                                foreach(MPropertyAttribute attribute in prop.GetPropertyAttributes())
                                {
                                    if (String.Compare(attribute.GetName(), "Compressionfile_DO_NOT_RESOURCE", true) == 0)
                                    {
                                        MPropertyAttributeString attString = attribute as MPropertyAttributeString;
                                        if (attString != null)
                                        {
                                            templateFile = Path.Combine(ConfigurationManager.AppSettings["CompressionTemplatesBaseDir"], attString.GetString());
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                */
                String extraCompressionArgs = "";
                if (File.Exists(templateFile))
                {
                    extraCompressionArgs = File.ReadAllText(templateFile).Replace(" ","");
                    extraCompressionArgs = Regex.Replace(extraCompressionArgs, "[ \n\r\t]", "");
                }
                if (String.IsNullOrEmpty(extraCompressionArgs))
                    procInfo.Arguments = "-anim=" + customAnimPath + " -out=" + rexAnimPath;
                else
                    procInfo.Arguments = "-anim=" + customAnimPath + " -out=" + rexAnimPath + " -compressionrules " + extraCompressionArgs;
            }

            Console.WriteLine("Running " + CompressExePath + " " + procInfo.Arguments);

            proc = new Process();
            proc.StartInfo = procInfo;
            proc.Start();
            output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();
            exitCode = proc.ExitCode;
            proc.Close();
            if (exitCode != 0)
            {
                Console.WriteLine("Error - compress step failed for: " + input);
                Console.WriteLine(output);
                return false;
            }


            bool isPs3 = (String.Compare(Platform, "Playstation 3", true) == 0 || String.Compare(Platform, "PS3", true) == 0);
            bool isPC = (String.Compare(Platform, "PC", true) == 0);

            if(isPC64)
                procInfo = new ProcessStartInfo(Ragebuilder64ExePath);
            else
                procInfo = new ProcessStartInfo(RagebuilderExePath);

            procInfo.RedirectStandardOutput = true;
            procInfo.UseShellExecute = false;
            procInfo.CreateNoWindow = true;
            procInfo.WindowStyle = ProcessWindowStyle.Hidden;

            procInfo.Arguments = luaScriptFile + " -uncompressedresources -nopopups -rebuild true -build "+ConfigurationManager.AppSettings["RageBuilderBuildDirParam"]+" -shader "+ConfigurationManager.AppSettings["RageBuilderShaderDirParam"]+" -shaderdb "+ConfigurationManager.AppSettings["RageBuilderShaderDBDirParam"];

            proc = new Process();
            proc.StartInfo = procInfo;
            proc.Start();
            output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();
            exitCode = proc.ExitCode;
            proc.Close();

            if (exitCode != 0)
            {
                Console.WriteLine("Error - ragebuilder step failed for: " + input);
                Console.WriteLine(output);
                //ignore failing ragebuilder for now - its failing due to hardcoded gta5 paths
                //return false;
            }

            return true;
        }

        private void Cleanup()
        {
            if (File.Exists(animSetPath))
                File.Delete(animSetPath);
            if (File.Exists(rexAnimSetPath))
                File.Delete(rexAnimSetPath);
            if (File.Exists(scriptFile))
                File.Delete(scriptFile);
            if (File.Exists(FaceFXActorSaveFile))
                File.Delete(FaceFXActorSaveFile);
            if (File.Exists(FaceFXCollapsedActorFile))
                File.Delete(FaceFXCollapsedActorFile);
            if (File.Exists(clipListFile))
                File.Delete(clipListFile);
            if (File.Exists(luaScriptFile))
                File.Delete(luaScriptFile);
        }
    }
}
