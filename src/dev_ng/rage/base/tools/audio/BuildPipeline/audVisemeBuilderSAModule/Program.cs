﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace audVisemeBuilderSAModule
{
    static class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var visemeBuilder = new VisemeBuilderSAModule(args);
                Environment.ExitCode = visemeBuilder.Run();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                //var argsCombined = args.Aggregate(string.Empty, (current, s) => current + (s + " "));
                //EventLog.WriteEntry("Audio Build", "VisemeBuilder: " + argsCombined + " - " + ex);
                Environment.ExitCode = -1;
            }
        }
    }
}
