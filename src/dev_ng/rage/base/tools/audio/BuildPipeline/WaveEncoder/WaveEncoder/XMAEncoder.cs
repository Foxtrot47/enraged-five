﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using audAssetBuilderCommon;
using audAssetBuilderProperties;
using Wavelib;
using rage.ToolLib;
namespace WaveEncoder
{
    using System.Linq;

    public class XMAEncoder : Encoder
    {
        private readonly string m_xmaFileName;
        private bool m_HasCustomLipsync;

        public XMAEncoder(bwWaveFile waveFile,
                          WaveProperties waveProperties,
                          string platform,
                          bool isBigEndian,
                          string outputDirectory,
                          uint streamingBlockBytes)
            : base(waveFile, waveProperties, platform, isBigEndian, outputDirectory, streamingBlockBytes)
        {
            m_xmaFileName = WaveFile.FileName.ToUpper().Replace(".WAV", ".XMA");
            m_HasCustomLipsync = waveProperties.BuiltAsset.CustomLipsync;
        }

        public override void Encode()
        {
            BuildXmaFile();
            //check xma file was produced
            if (!File.Exists(m_xmaFileName))
            {
                throw new Exception(String.Concat("XMA file not found ", m_xmaFileName));
            }
            WriteChunks();
        }

        private void EncodeXmaFileMultiple(string inputFile,
                                           int blockSize,
                                           int[] compressionSettings,
                                           string[] outputFiles)
        {
            var numFiles = compressionSettings.Length;
            var psi = new ProcessStartInfo[numFiles];

            var sb = new StringBuilder();
            sb.Append("\"");
            sb.Append(inputFile);
            sb.Append("\"");

            sb.Append(" /BlockSize ");
            sb.Append(blockSize);

            if (StreamingBlockBytes != 0)
            {
                // we want to be able to loop streams end to end
                sb.Append(" /LoopWholeFile");
            }
            else
            {
                sb.Append(" /UseLoopPoints");
            }
            sb.Append(" /Verbose");
            sb.Append(" /TargetFile");

            var sharedArgs = sb.ToString();

            var p = new Process[numFiles];

            for (var i = 0; i < numFiles; i++)
            {
                psi[i] = new ProcessStartInfo("XMA2Encode.exe");
                psi[i].Arguments = sharedArgs + " \"" + outputFiles[i] + "\"" + " /Quality " + compressionSettings[i];

                Console.WriteLine(psi[i].Arguments);

                psi[i].CreateNoWindow = true;
                psi[i].UseShellExecute = false;
                psi[i].RedirectStandardError = true;
                psi[i].RedirectStandardOutput = true;

                p[i] = new Process();
                p[i].OutputDataReceived += OutputDataReceived;
                p[i].ErrorDataReceived += ErrorDataReceived;
                p[i].StartInfo = psi[i];
                p[i].Start();
                p[i].BeginOutputReadLine();
                p[i].BeginErrorReadLine();
            }
            for (var i = 0; i < numFiles; i++)
            {
                p[i].WaitForExit();
                if (p[i].ExitCode != 0)
                {
                    throw new Exception("xma2encode.exe returned " + p[i].ExitCode);
                }
            }
        }

        private void BuildXmaFile()
        {
            var blockSize = WaveProperties.BuiltAsset.BlockSize;
            if (blockSize == 0)
            {
                // default to 2kb blocks
                blockSize = 2;
            }

            var compression = WaveProperties.BuiltAsset.Compression;
            string iterativeEncoding = WaveProperties.BuiltAsset.IterativeEncoding;
          
            var fileName = WaveFile.FileName;

            // Turn on iterative encoding for everything except streams
            var useIterativeEncoding = (StreamingBlockBytes == 0) && !string.Equals(iterativeEncoding, "disabled", StringComparison.OrdinalIgnoreCase);

            if (useIterativeEncoding)
            {
                // iterative encoding                               
                var lowerBound = compression - 3;
                var upperBound = compression + 15;

                // Allow parameters to be overridden via tag
                if (!string.IsNullOrEmpty(iterativeEncoding))
                {
                    var encodingParams = rage.ToolLib.Utility.ParseKeyValueString(iterativeEncoding);
                    if (encodingParams.ContainsKey("decrease"))
                    {
                        lowerBound = compression - Math.Abs(int.Parse(encodingParams["decrease"]));
                    }
                    if (encodingParams.ContainsKey("increase"))
                    {
                        upperBound = compression + Math.Abs(int.Parse(encodingParams["increase"]));
                    }
                }
                
                var fileNameBase = fileName.Substring(0, fileName.Length - 4);

                // Don't allow lowerBound to go below 1
                lowerBound = Math.Max(1, lowerBound);
                // Don't allow upperBound above 100
                upperBound = Math.Min(100, upperBound);

                // pick highest encoding in the requested range without increasing size ...
                var numEncodes = Math.Max(1, upperBound - lowerBound);
                var outputFiles = new string[numEncodes];
                var compressionSettings = new int[numEncodes];
                var requestedIndex = 0;
                for (var i = 0; i < numEncodes; i++)
                {
                    compressionSettings[i] = (lowerBound + i);
                    outputFiles[i] = fileNameBase + "_comp_" + compressionSettings[i] + ".xma";
                    if (compressionSettings[i] == compression)
                    {
                        requestedIndex = 0;
                    }
                }
                EncodeXmaFileMultiple(fileName, blockSize, compressionSettings, outputFiles);

                // start with lowest bound
                var fileInfoLowest = new FileInfo(outputFiles[0]);
                var fileToUse = outputFiles[0];

                var fileInfoReq = new FileInfo(outputFiles[requestedIndex]);
                for (var i = 0; i < numEncodes; i++)
                {
                    var fileInfoIntermediate = new FileInfo(outputFiles[i]);
                    if (fileInfoIntermediate.Length == fileInfoLowest.Length)
                    {
                        fileToUse = outputFiles[i];
                    }
                }

                Console.WriteLine("Using encoded file {0} - saved {1} bytes", fileToUse, fileInfoReq.Length - fileInfoLowest.Length);
                if (File.Exists(m_xmaFileName))
                {
                    Console.WriteLine("Deleting {0}", m_xmaFileName);
                    File.Delete(m_xmaFileName);
                }
                File.Copy(fileToUse, m_xmaFileName);
                Console.WriteLine("Copied to {0}", m_xmaFileName);

                // clean up extra files
                // dont worry too much if this fails for some reason; the entire build folder is wiped at the start of each
                // build.

                foreach (var s in outputFiles)
                {
                    try
                    {
                        Console.WriteLine("Deleting temp file {0}", s);
                        File.Delete(s);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Warning: XMA encoder failed to clean up temporary file: " + s + " " +
                                            e.Message);
                    }
                }                
            }
            else
            {
                var psi = new ProcessStartInfo("XMA2Encode.exe");
                var sb = new StringBuilder();
                sb.Append("\"");
                sb.Append(fileName);
                sb.Append("\"");

                sb.Append(" /BlockSize ");
                sb.Append(blockSize);

                if (compression != 0)
                {
                    sb.Append(" /Quality ");
                    sb.Append(compression);
                }

                if (StreamingBlockBytes != 0)
                {
                    // we want to be able to loop streams end to end
                    sb.Append(" /LoopWholeFile");
                }
                else
                {
                    sb.Append(" /UseLoopPoints");
                }
                sb.Append(" /Verbose");

                psi.Arguments = sb.ToString();
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                psi.RedirectStandardError = true;
                psi.RedirectStandardOutput = true;

                var p = new Process();
                p.OutputDataReceived += OutputDataReceived;
                p.ErrorDataReceived += ErrorDataReceived;
                p.StartInfo = psi;
                p.Start();
                p.BeginOutputReadLine();
                p.BeginErrorReadLine();
                p.WaitForExit();
                if (p.ExitCode != 0)
                {
                    throw new Exception("xma2encode.exe returned " + p.ExitCode);
                }
            }
        }

        private static void ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.Out.WriteLine(e.Data);
        }

        private static void OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.Out.WriteLine(e.Data);
        }

        private void WriteChunks()
        {
            var xmaFile = new bwWaveFile(m_xmaFileName, false);
            var fileNameNoExtension = Path.GetFileNameWithoutExtension(m_xmaFileName);

            var xmaChunk = (bwXMAFormatChunk) xmaFile.Format;

            //Save Built Size
            WaveProperties.BuiltAsset.Size = (uint) xmaFile.Data.RawData.Length;
            WaveProperties.BuiltAsset.NumberOfSamples = xmaChunk.SamplesEncoded;
            WaveProperties.BuiltAsset.SampleRate = (int) WaveFile.Format.SampleRate;

            //write wave format chunk
            var formatChunk = new WaveFormatChunk();

            formatChunk.LengthSamples = StreamingBlockBytes != 0
                                            ? PlatformSpecific.FixEndian(xmaChunk.LoopBegin + xmaChunk.LoopLength, this.Platform)
                                            : PlatformSpecific.FixEndian(xmaChunk.SamplesEncoded, this.Platform);

            if (WaveFile.Sample != null &&
                WaveFile.Sample.Loops.Count == 1)
            {
                formatChunk.LoopPointSamples = (int)PlatformSpecific.FixEndian(WaveFile.Sample.Loops[0].Start, this.Platform);
            }
            else
            {
                formatChunk.LoopPointSamples = -1;
            }
            formatChunk.SampleRate = PlatformSpecific.FixEndian((UInt16) WaveFile.Format.SampleRate, this.Platform);
            formatChunk.Format = (byte) audStreamFormat.AUD_FORMAT_XMA2;
            formatChunk.Headroom = PlatformSpecific.FixEndian((UInt16)WaveProperties.BuiltAsset.HeadRoom, this.Platform);
            formatChunk.FirstPeakSample = PlatformSpecific.FixEndian((UInt16)WaveProperties.BuiltAsset.FirstPeakSample, this.Platform);

            var loopBegin = xmaChunk.LoopBegin / 128;
            if (loopBegin > UInt16.MaxValue ||
                loopBegin * 128 != xmaChunk.LoopBegin)
            {
                // throw new Exception("Invalid Loop Begin");
                Console.Error.WriteLine("Warning: Invalid loop begin: " + loopBegin);
                loopBegin = 0;
            }
            formatChunk.LoopBegin = PlatformSpecific.FixEndian((UInt16)loopBegin, this.Platform);

            var loopEnd = (xmaChunk.LoopBegin + xmaChunk.LoopLength) / 128;
            if (loopEnd > UInt16.MaxValue ||
                loopEnd * 128 != (xmaChunk.LoopBegin + xmaChunk.LoopLength))
            {
                //throw new Exception("Invalid Loop End");
                Console.Error.WriteLine("Warning: Invalid loop end: " + loopEnd);
                loopEnd = 0;
            }
            formatChunk.LoopEnd = PlatformSpecific.FixEndian((UInt16)loopEnd, this.Platform);

            var playBegin = xmaChunk.PlayBegin;
            if (playBegin > Byte.MaxValue ||
                playBegin * 128 != xmaChunk.PlayBegin)
            {
                //throw new Exception(string.Format("Invalid Play Begin: {0}", playBegin));
                Console.WriteLine("Warning: Invalid play begin: " + playBegin);
                playBegin = 0;
            }
            formatChunk.PlayBegin = (Byte) playBegin;

            var playEnd = (xmaChunk.PlayBegin + xmaChunk.PlayLength) / 128;
            if (playEnd > UInt16.MaxValue ||
                playEnd * 128 != (xmaChunk.PlayBegin + xmaChunk.PlayLength))
            {
                //throw new Exception("Invalid Play end");
                Console.Out.WriteLine("warning: Invalid play end: " + playEnd);
                playEnd = 0;
            }

            Console.Out.WriteLine("Samples Encoded: {0} play begin: {1} play end: {2} loop begin {3} loop end {4}",
                                  xmaChunk.SamplesEncoded,
                                  xmaChunk.PlayBegin,
                                  (xmaChunk.PlayBegin + xmaChunk.PlayLength),
                                  xmaChunk.LoopBegin,
                                  (xmaChunk.LoopBegin + xmaChunk.LoopLength));

            formatChunk.PlayEnd = (PlatformSpecific.FixEndian((UInt16)playEnd, this.Platform));
            formatChunk.HasCustomLipsync = m_HasCustomLipsync;

            formatChunk.Serialize(String.Concat(OutputDirectory,
                                                fileNameNoExtension,
                                                ".",
                                                Chunks.TYPE.FORMAT.ToString("g")));

            //write data chunk
            //File.Copy(m_XMAFileName,String.Concat(OutputDirectory, fileNameNoExtension, ".", Chunks.TYPE.DATA.ToString("g")),true);
            var fs =
                new FileStream(
                    String.Concat(OutputDirectory, fileNameNoExtension, ".", Chunks.TYPE.DATA.ToString("g")),
                    FileMode.Create,
                    FileAccess.Write);
            var bw = new BinaryWriter(fs);
            bw.Write(xmaFile.Data.RawData);
            bw.Close();
            fs.Close();

            //write seek table chunk
            var seekChunk = (bwGenericChunk) xmaFile.GetChunkByName("seek");

            fs =
                new FileStream(
                    String.Concat(OutputDirectory, fileNameNoExtension, ".", Chunks.TYPE.SEEKTABLE.ToString("g")),
                    FileMode.Create,
                    FileAccess.Write);
            bw = new BinaryWriter(fs);

            bw.Write(seekChunk.Bytes);
            bw.Close();
            fs.Close();

            //Write Marker chunk
            if (this.WaveFile.Markers.Count <= 0)
            {
                return;
            }

            var markersChunkProperties = this.WaveProperties.BuiltAsset.Chunks.FirstOrDefault(c => c.Name == "MARKERS");
            if (null == markersChunkProperties)
            {
                if (this.WaveProperties.BuiltAsset.AddOrUpdateChunk("MARKERS", 0))
                {
                    markersChunkProperties = this.WaveProperties.BuiltAsset.Chunks.First(c => c.Name == "MARKERS");
                }
                else
                {
                    throw new Exception("Failed to add new MARKERS chunk");
                }
            }

            markersChunkProperties.ChunkMarkers.Clear();

            var markerChunk = new WaveMetadataMarkerChunk(this.Platform);
            foreach (var marker in this.WaveFile.Markers)
            {
                var timeOffset = (uint)(marker.Position / (this.WaveProperties.BuiltAsset.SampleRate / 1000.0f));
                markersChunkProperties.AddMarker(marker.Name, timeOffset.ToString());
                markerChunk.Add(marker.Name, marker.Position);
            }

            markerChunk.Serialize(String.Concat(this.OutputDirectory,
                fileNameNoExtension,
                ".",
                Chunks.TYPE.MARKERS.ToString("g")));
        }
    }
}