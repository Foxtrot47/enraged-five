﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using audAssetBuilderCommon;
using audAssetBuilderProperties;
using Wavelib;

namespace WaveEncoder
{
    using System.Linq;
    using System.Xml.Linq;

    public class MP3Parser
    {
        //private static int[] ms_validSampleRates = {8000, 11025, 12000, 16000, 22050, 24000, 32000, 44100, 48000};

        private static readonly int[] ms_mpegVersionTable = {
                                                               (int) audMpegVersions.Mpeg2_5, -1,
                                                               (int) audMpegVersions.Mpeg2, (int) audMpegVersions.Mpeg1
                                                           };

        private static readonly int[] ms_mpegLayerTable = {
                                                             -1, (int) audMpegLayers.Layer3, (int) audMpegLayers.Layer2,
                                                             (int) audMpegLayers.Layer1
                                                         };

        private static readonly int[] ms_mpegSlotBytes = {
                                                            4, //LAYER1
                                                            1, //LAYER2
                                                            1 //LAYER3
                                                        };

        private static readonly int[][] ms_mpeg1BitrateTable = {
                                                                  new[] {-1, -1, -1}, new[] {32, 32, 32},
                                                                  new[] {64, 48, 40}, new[] {96, 56, 48},
                                                                  new[] {128, 64, 56}, new[] {160, 80, 64},
                                                                  new[] {192, 96, 80}, new[] {224, 112, 96},
                                                                  new[] {256, 128, 112}, new[] {288, 160, 128},
                                                                  new[] {320, 192, 160}, new[] {352, 224, 192},
                                                                  new[] {384, 256, 224}, new[] {416, 320, 256},
                                                                  new[] {448, 384, 320}, new[] {-1, -1, -1}
                                                              };

        private static readonly int[][] ms_mpeg2BitrateTable = {
                                                                  new[] {-1, -1, -1}, new[] {32, 8, 8}, new[] {48, 16, 16},
                                                                  new[] {56, 24, 24}, new[] {64, 32, 32},
                                                                  new[] {80, 40, 40}, new[] {96, 48, 48},
                                                                  new[] {112, 56, 56}, new[] {128, 64, 64},
                                                                  new[] {144, 80, 80}, new[] {160, 96, 96},
                                                                  new[] {176, 112, 112}, new[] {192, 128, 128},
                                                                  new[] {224, 144, 144}, new[] {256, 160, 160},
                                                                  new[] {-1, -1, -1}
                                                              };

        private static readonly int[][] ms_mpegSampleRateTable = {
                                                                    new[] {44100, 22050, 11025},
                                                                    new[] {48000, 24000, 12000}, new[] {32000, 16000, 8000}
                                                                    , new[] {-1, -1, -1}
                                                                };

        public static void GenerateSeekTable(string mp3File, string seekFile, string platform)
        {
            var fs = new FileStream(mp3File, FileMode.Open, FileAccess.Read);
            var fileContents = new byte[fs.Length];
            fs.Read(fileContents, 0, (int) fs.Length);
            fs.Close();

            var seekTable = GenerateSeekTable(fileContents);

            fs = new FileStream(seekFile, FileMode.CreateNew, FileAccess.Write);
            var bw = new BinaryWriter(fs);
            for (var i = 0; i < seekTable.Count; i++)
            {
                bw.Write(PlatformSpecific.FixEndian(seekTable[i], platform));
            }
            bw.Close();
            fs.Close();
        }

        private static List<ushort> GenerateSeekTable(byte[] mp3Data)
        {
            var packetOffsets = new List<ushort>();
            for (var byteOffset = 0; byteOffset < mp3Data.Length; byteOffset++)
            {
                //Find the frame sync.
                if ((mp3Data[byteOffset + 0] == 0xFF) &&
                    ((mp3Data[byteOffset + 1] & 0xE0) == 0xE0))
                {
                    /*unsigned int privateBit = frame[2] & 0x1;
                    unsigned int channelMode = (frame[3] >> 6) & 0x3;
                    unsigned int modeExtension = (frame[3] >> 4) & 0x3;
                    unsigned int copyright = (frame[3] >> 3) & 0x1;
                    unsigned int original = (frame[3] >> 2) & 0x1;
                    unsigned int emphasis = frame[3] & 0x3;*/

                    var versionIndex = (mp3Data[byteOffset + 1] >> 3) & 0x3;
                    var version = ms_mpegVersionTable[versionIndex];
                    if (version == -1)
                    {
                        throw new Exception("Invalid MPEG version");
                    }

                    var layerIndex = (mp3Data[byteOffset + 1] >> 1) & 0x3;
                    var layer = ms_mpegLayerTable[layerIndex];
                    if (layer < 0)
                    {
                        throw new Exception("Error: Invalid MPEG layer");
                    }

                    var bitrateIndex = (mp3Data[byteOffset + 2] >> 4) & 0xF;
                    int bitrate;
                    if (version == (int) audMpegVersions.Mpeg1)
                    {
                        bitrate = ms_mpeg1BitrateTable[bitrateIndex][layer] * 1000;
                    }
                    else
                    {
                        bitrate = ms_mpeg2BitrateTable[bitrateIndex][layer] * 1000;
                    }

                    if (bitrate < 0)
                    {
                        throw new Exception("Error: Invalid MPEG bitrate");
                    }

                    var sampleRateIndex = (mp3Data[byteOffset + 2] >> 2) & 0x3;
                    var sampleRate = ms_mpegSampleRateTable[sampleRateIndex][version];
                    if (sampleRate < 0)
                    {
                        throw new Exception("Error: Invalid MPEG sample rate");
                    }

                    var padding = (mp3Data[byteOffset + 2] >> 1) & 0x1;
                    var paddingBytes = padding * ms_mpegSlotBytes[layer];

                    var samplesPerFrame = 1152;
                    var frameBytes = ((samplesPerFrame / 8 * bitrate) / sampleRate) + paddingBytes;

                    var protection = mp3Data[byteOffset + 1] & 0x01;
                    if (protection == 0)
                    {
                        //There should be a 16-bit CRC at the end of the header.
                        //   frameBytes += 2;
                    }

                    packetOffsets.Add((ushort) frameBytes);

                    //Jump to the end of this frame.
                    byteOffset += frameBytes - 1;
                }
            }
            return packetOffsets;
        }

        #region Nested type: audMpegLayers

        private enum audMpegLayers
        {
            Layer1,
            Layer2,
            Layer3
        } ;

        #endregion

        #region Nested type: audMpegVersions

        private enum audMpegVersions
        {
            Mpeg1,
            Mpeg2,
            Mpeg2_5
        } ;

        #endregion
    }

// class MP3Parser

    public class PS3Encoder : Encoder
    {
        private readonly string m_mp3FileName;
        private readonly string m_seekTable;
        private int m_postEncodeHeadroom;
        private bool m_HasCustomLipsync;

        public PS3Encoder(
            bwWaveFile waveFile,
            WaveProperties waveProperties,
            string platform,
            bool isBigEndian,
            string outputDirectory,
            uint streamingBlockBytes)
            : base(waveFile, waveProperties, platform, isBigEndian, outputDirectory, streamingBlockBytes)
        {
            m_mp3FileName = WaveFile.FileName.ToUpper().Replace(".WAV", ".DATA");
            m_seekTable = WaveFile.FileName.ToUpper().Replace(".WAV", ".SEEKTABLE");
            m_HasCustomLipsync = waveProperties.BuiltAsset.CustomLipsync;
        }

        public override void Encode()
        {
            BuildMP3File();
            //check output file was produced
            if (!File.Exists(m_mp3FileName))
            {
                throw new Exception(String.Concat("MP3 data chunk not found ", m_mp3FileName));
            }
            if (!File.Exists(m_seekTable))
            {
                throw new Exception(String.Concat("MP3 seek table chunk not found ", m_seekTable));
            }
            WriteChunks();
        }

        private void BuildMP3File()
        {
            var fileName = WaveFile.FileName;
            var compression = WaveProperties.BuiltAsset.Compression;

            if (StreamingBlockBytes == 0 || this.WaveProperties.BuiltAsset.ForceInternalEncoder)
            {
                var psi = new ProcessStartInfo("MP3Encoder.exe");
                var sb = new StringBuilder();

                sb.Append(" -f "); //file name
                sb.Append("\"");
                sb.Append(fileName);
                sb.Append("\"");

                sb.Append(" -c "); //compression 1-100
                sb.Append(compression);

                //this is the target samplerate, not the actual one.
                sb.Append(" -s ");
                sb.Append(WaveProperties.BuiltAsset.SampleRate);

                sb.Append(" -p PS3"); //platform

                if (WaveProperties.BuiltAsset.PreserveTransient)
                {
                    sb.Append(" -preserveTransient ");
                }

                psi.Arguments = sb.ToString();
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                psi.RedirectStandardError = true;
                psi.RedirectStandardOutput = true;

                var p = new Process();
                p.OutputDataReceived += OutputDataReceived;
                p.ErrorDataReceived += ErrorDataReceived;
                p.StartInfo = psi;
                p.Start();
                p.BeginOutputReadLine();
                p.BeginErrorReadLine();
                p.WaitForExit();

                // MP3 encoder returns post-encode headroom in millibels; if the return value is negative then
                // something went wrong
                if (p.ExitCode < 0)
                {
                    throw new ArgumentException("MP3 encoder returned " + p.ExitCode);
                }
                // debug: create copy of MP3 file for diagnostic purposes
                File.Copy(m_mp3FileName, m_mp3FileName + ".mp3");
                m_postEncodeHeadroom = p.ExitCode;
            }
            else
            {
                // process streams with msenc

                var psi = new ProcessStartInfo("msenc.exe");
                var sb = new StringBuilder();
                var quality = 10 - (int) Math.Ceiling(compression / 10.0f);
                if (quality == 10)
                {
                    quality = 9; //Compression setting 0 will give the same compression as 1 to 10.
                }

                int lowerBound;
                int upperBound;
                switch (quality)
                {
                        /* from lame VBR wikipedia
                     * -V 0 	 --preset fast extreme 	 245 	 220...260
                        -V 1 	 	225 	190...250
                        -V 2 	--preset fast standard 	190 	170...210
                        -V 3 	 	175 	150...195
                        -V 4 	--preset fast medium 	165 	140...185
                        -V 5 	 	130 	120...150
                        -V 6 	 	115 	100...130
                        -V 7 	 	100 	80...120
                        -V 8 	 	85 	70...105
                        -V 9 	 	65 	45...85 
                     * */
                    default:
                        // highest quality
                        lowerBound = 110;
                        upperBound = 130;
                        break;
                    case 1:
                        lowerBound = 95;
                        upperBound = 125;
                        break;
                    case 2:
                        lowerBound = 85;
                        upperBound = 105;
                        break;
                    case 3:
                        lowerBound = 75;
                        upperBound = 98;
                        break;
                    case 4:
                        lowerBound = 40;
                        upperBound = 96;
                        break;
                    case 5:
                        lowerBound = 48;
                        upperBound = 80;
                        break;
                    case 6:
                        lowerBound = 40;
                        upperBound = 64;
                        break;
                    case 7:
                        lowerBound = 40;
                        upperBound = 60;
                        break;
                    case 8:
                        lowerBound = 35;
                        upperBound = 50;
                        break;
                    case 9:
                        // lowest quality
                        lowerBound = 22;
                        upperBound = 42;
                        break;
                }

                sb.Append(" -in \""); //file name
                sb.Append(fileName);
                sb.Append("\"");

                sb.Append(" -out \"");
                sb.Append(m_mp3FileName);
                sb.Append("\"");

                sb.Append(" -mp3 "); //compression 1-100
                sb.Append(lowerBound);

                sb.Append(" -mp3vbr ");
                sb.Append(upperBound);

                sb.Append(" -nomsf ");

                if (!WaveProperties.BuiltAsset.DisableLooping)
                {
                    sb.Append(" -loop ");
                }

                psi.Arguments = sb.ToString();
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;

                Console.WriteLine("Requested compression of {0}kbps - {1}kbps", lowerBound, upperBound);

                var p = new Process();
                p.StartInfo = psi;
                p.Start();
                p.WaitForExit();
                m_postEncodeHeadroom = 0;
                MP3Parser.GenerateSeekTable(m_mp3FileName, m_mp3FileName.Replace(".DATA", ".SEEKTABLE"), this.Platform);
            }
        }

        private static void ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.Error.WriteLine(e.Data);
        }

        private static void OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.Out.WriteLine(e.Data);
        }

        private void WriteChunks()
        {
            var fileNameNoExtension = Path.GetFileNameWithoutExtension(WaveFile.FileName);

            //Write format chunk
            var formatChunk = new WaveFormatChunk
                                  {LengthSamples = PlatformSpecific.FixEndian(WaveFile.Data.NumSamples, this.Platform)};
            if (WaveFile.Sample != null &&
                WaveFile.Sample.Loops.Count == 1)
            {
                formatChunk.LoopPointSamples = (int)PlatformSpecific.FixEndian(WaveFile.Sample.Loops[0].Start, this.Platform);
            }
            else
            {
                formatChunk.LoopPointSamples = -1;
            }
            formatChunk.SampleRate = PlatformSpecific.FixEndian((UInt16) WaveFile.Format.SampleRate, this.Platform);
            formatChunk.Format = (byte) audStreamFormat.AUD_FORMAT_MP3;
            formatChunk.Headroom = PlatformSpecific.FixEndian((UInt16) WaveProperties.BuiltAsset.HeadRoom, this.Platform);

            // HACK: store post-encode headroom in LoopBegin field as millibels
            formatChunk.LoopBegin = PlatformSpecific.FixEndian((ushort) m_postEncodeHeadroom, this.Platform);
            // Flag to the runtime whether we have 'preserveTransient' turned on
            formatChunk.LoopEnd =
                PlatformSpecific.FixEndian((ushort) (WaveProperties.BuiltAsset.PreserveTransient ? 1 : 0), this.Platform);
            formatChunk.PlayBegin = 0;
            formatChunk.PlayEnd = 0;
            formatChunk.HasCustomLipsync = m_HasCustomLipsync;

            formatChunk.FirstPeakSample = PlatformSpecific.FixEndian((UInt16)WaveProperties.BuiltAsset.FirstPeakSample, this.Platform);

            formatChunk.Serialize(String.Concat(OutputDirectory,
                                                fileNameNoExtension,
                                                ".",
                                                Chunks.TYPE.FORMAT.ToString("g")));

            // Write Marker chunk
            if (this.WaveFile.Markers.Count <= 0)
            {
                return;
            }

            var markersChunkProperties = this.WaveProperties.BuiltAsset.Chunks.FirstOrDefault(c => c.Name == "MARKERS");
            if (null == markersChunkProperties)
            {
                if (this.WaveProperties.BuiltAsset.AddOrUpdateChunk("MARKERS", 0))
                {
                    markersChunkProperties = this.WaveProperties.BuiltAsset.Chunks.First(c => c.Name == "MARKERS");
                }
                else
                {
                    throw new Exception("Failed to add new MARKERS chunk");
                }
            }

            markersChunkProperties.ChunkMarkers.Clear();

            var markerChunk = new WaveMetadataMarkerChunk(this.Platform);
            foreach (var marker in this.WaveFile.Markers)
            {
                var timeOffset = (uint)(marker.Position / (this.WaveProperties.BuiltAsset.SampleRate / 1000.0f));
                markersChunkProperties.AddMarker(marker.Name, timeOffset.ToString());
                markerChunk.Add(marker.Name, marker.Position);
            }

            markerChunk.Serialize(
                string.Concat(
                    this.OutputDirectory,
                    fileNameNoExtension,
                    ".",
                Chunks.TYPE.MARKERS.ToString("g")));
        }
    }
}