﻿using System;
using System.Diagnostics;

namespace WaveEncoder
{
    public class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var encoder = new WaveEncoder(args);
                encoder.Execute();
                Environment.ExitCode = 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                EventLog.WriteEntry("Audio Build", ex.ToString());
                Environment.ExitCode = -1;
            }
        }
    }
}