﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using audAssetBuilderCommon;
using audAssetBuilderProperties;
using Wavelib;

namespace WaveEncoder
{
    using System.Linq;

    public class PCMEncoder : Encoder
    {
        private bool m_HasCustomLipsync;

        public PCMEncoder(bwWaveFile waveFile,
                          WaveProperties waveProperties,
                          string platform,
                          bool isBigEndian,
                          string outputDirectory,
                          uint streamingBlockBytes)
            : base(waveFile, waveProperties, platform, isBigEndian, outputDirectory, streamingBlockBytes)
        {
            String srcAssetPath = this.WaveProperties.SourceAsset.Path;
            m_HasCustomLipsync = waveProperties.BuiltAsset.CustomLipsync;
        }

        public override void Encode()
        {
            var name = Path.GetFileNameWithoutExtension(WaveFile.FileName);
            var outputFileNoExt = String.Concat(OutputDirectory, name);

            //write data
            Console.Out.WriteLine(String.Concat(outputFileNoExt, ".", Chunks.TYPE.DATA.ToString("g")));
            var fs = new FileStream(String.Concat(outputFileNoExt, ".", Chunks.TYPE.DATA.ToString("g")),
                                    FileMode.Create,
                                    FileAccess.Write);
            var bw = new BinaryWriter(fs);

            for (var i = 0; i < WaveFile.Data.RawData.Length; i += 2)
            {
                if (IsBigEndian)
                {
                    bw.Write(WaveFile.Data.RawData[i + 1]);
                    bw.Write(WaveFile.Data.RawData[i]);
                }
                else
                {
                    bw.Write(WaveFile.Data.RawData[i]);
                    bw.Write(WaveFile.Data.RawData[i + 1]);
                }
            }

            bw.Close();
            fs.Close();

            if (WaveProperties == null)
            {
                throw new Exception(String.Concat("Unable to find wave properties for wave ", WaveFile.FileName));
            }

            WaveProperties.BuiltAsset.Size = (uint) WaveFile.Data.RawData.Length;
            WaveProperties.Save(String.Concat(outputFileNoExt, ".XML"));
            WaveProperties.BuiltAsset.NumberOfSamples = WaveFile.Data.NumSamples;

            //write format chunk
            var formatChunk = new WaveFormatChunk();

            formatChunk.LengthSamples = IsBigEndian
                                            ? PlatformSpecific.FlipEndian(WaveFile.Data.NumSamples)
                                            : WaveFile.Data.NumSamples;

            if (WaveFile.Sample != null &&
                WaveFile.Sample.Loops.Count == 1)
            {
                formatChunk.LoopPointSamples =
                    (int)
                    (IsBigEndian
                         ? PlatformSpecific.FlipEndian(WaveFile.Sample.Loops[0].Start)
                         : WaveFile.Sample.Loops[0].Start);
            }
            else
            {
                formatChunk.LoopPointSamples = -1;
            }

            formatChunk.SampleRate = IsBigEndian
                                         ? PlatformSpecific.FlipEndian((UInt16) WaveFile.Format.SampleRate)
                                         : (UInt16) WaveFile.Format.SampleRate;
            formatChunk.Format =
                (byte)
                (IsBigEndian ? audStreamFormat.AUD_FORMAT_PCM_S16_BIG : audStreamFormat.AUD_FORMAT_PCM_S16_LITTLE);
            formatChunk.Headroom = IsBigEndian
                                       ? PlatformSpecific.FlipEndian((UInt16) WaveProperties.BuiltAsset.HeadRoom)
                                       : (UInt16) WaveProperties.BuiltAsset.HeadRoom;

            formatChunk.LoopBegin = 0;
            formatChunk.LoopEnd = 0;
            formatChunk.PlayBegin = 0;
            formatChunk.PlayEnd = 0;
            formatChunk.HasCustomLipsync = m_HasCustomLipsync;

            formatChunk.FirstPeakSample = IsBigEndian ?
                PlatformSpecific.FlipEndian((UInt16)WaveProperties.BuiltAsset.FirstPeakSample) :
                (UInt16)WaveProperties.BuiltAsset.FirstPeakSample;

            formatChunk.Serialize(String.Concat(outputFileNoExt, ".", Chunks.TYPE.FORMAT.ToString("g")));

            //write marker chunk
            if (this.WaveFile.Markers.Count <= 0)
            {
                return;
            }

            var markersChunkProperties = this.WaveProperties.BuiltAsset.Chunks.FirstOrDefault(c => c.Name == "MARKERS");
            if (null == markersChunkProperties)
            {
                if (this.WaveProperties.BuiltAsset.AddOrUpdateChunk("MARKERS", 0))
                {
                    markersChunkProperties = this.WaveProperties.BuiltAsset.Chunks.First(c => c.Name == "MARKERS");
                }
                else
                {
                    throw new Exception("Failed to add new MARKERS chunk");
                }
            }

            markersChunkProperties.ChunkMarkers.Clear();

            var markerChunk = new WaveMetadataMarkerChunk(this.Platform);
            foreach (var marker in this.WaveFile.Markers)
            {
                var timeOffset = (uint)(marker.Position / (this.WaveProperties.BuiltAsset.SampleRate / 1000.0f));
                markersChunkProperties.AddMarker(marker.Name, timeOffset.ToString());
                markerChunk.Add(marker.Name, marker.Position);
            }

            Console.Out.WriteLine((String.Concat(this.OutputDirectory, name, ".", Chunks.TYPE.MARKERS.ToString("g"))));
            markerChunk.Serialize(String.Concat(this.OutputDirectory, name, ".", Chunks.TYPE.MARKERS.ToString("g")));
        }
    }
}