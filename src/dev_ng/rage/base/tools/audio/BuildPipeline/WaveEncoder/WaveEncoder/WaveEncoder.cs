﻿using System;
using System.Collections.Generic;
using System.IO;
using audAssetBuilderCommon;
using rage;

namespace WaveEncoder
{
    public class WaveEncoder : WaveBuildBase
    {
        public WaveEncoder(string[] args) : base(args)
        {
        }

        public override void Run()
        {
            var projectSettings = new audProjectSettings(ProjectSettingsPath);
            projectSettings.SetCurrentPlatformByTag(Platform);

            Encoder encoder = null;

            var platformEncoder = projectSettings.GetCurrentPlatform().Encoder;
            //double check that wave does not need to be pcm
            //We are checking that the number of samples in a wave against (aligment samples * 1.5) so that the xma encoder 
            //doesn't resample it down 128 and cause us issues
            if (WaveProperties.BuiltAsset.Compression == 101)
            {
                //uncompressed files don't get encoded
                encoder = new PCMEncoder(WaveFile,
                                             WaveProperties,
                                             Platform,
                                             projectSettings.GetCurrentPlatform().IsBigEndian,
                                             OutputDirectory,
                                             StreamingBlockBytes);
            }
            else if (platformEncoder.Equals("PCMENCODER", StringComparison.OrdinalIgnoreCase) ||
                WaveFile.Data.NumSamples <= (projectSettings.GetCurrentPlatform().AlignmentSamples * 1.5))
            {
                if (WaveProperties.BuiltAsset.Compression == 1 || WaveProperties.BuiltAsset.CompressionType.Equals("ADPCM", StringComparison.OrdinalIgnoreCase))
                {
                    encoder = new ADPCMEncoder(WaveFile,
                                               WaveProperties,
                                               Platform,
                                               projectSettings.GetCurrentPlatform().IsBigEndian,
                                               OutputDirectory,
                                               StreamingBlockBytes);
                }
                else if (WaveFile.Data.NumSamples >= 1152 && WaveProperties.BuiltAsset.CompressionType.Equals("MP3", StringComparison.OrdinalIgnoreCase))
                {
                    encoder = new MP3Encoder(WaveFile,
                                               WaveProperties,
                                               Platform,
                                               projectSettings.GetCurrentPlatform().IsBigEndian,
                                               OutputDirectory,
                                               StreamingBlockBytes);
                }
                else if (WaveFile.Data.NumSamples >= 128 && WaveProperties.BuiltAsset.CompressionType.Equals("XMA2", StringComparison.OrdinalIgnoreCase))
                {
                    encoder = new XMAEncoder(WaveFile,
                                               WaveProperties,
                                               Platform,
                                               projectSettings.GetCurrentPlatform().IsBigEndian,
                                               OutputDirectory,
                                               StreamingBlockBytes);
                }
                else
                {
                    encoder = new PCMEncoder(WaveFile, 
                                             WaveProperties, 
                                             Platform, 
                                             projectSettings.GetCurrentPlatform().IsBigEndian, 
                                             OutputDirectory, 
                                             StreamingBlockBytes);
                }
            }
            else
            {
                switch (platformEncoder.ToUpper())
                {
                    case "XMAENCODER":
                        encoder = new XMAEncoder(WaveFile,
                                                 WaveProperties,
                                                 Platform,
                                                 projectSettings.GetCurrentPlatform().IsBigEndian,
                                                 OutputDirectory,
                                                 StreamingBlockBytes);
                        break;
                    case "PS3ENCODER":
                        encoder = new PS3Encoder(WaveFile,
                                                 WaveProperties,
                                                 Platform,
                                                 projectSettings.GetCurrentPlatform().IsBigEndian,
                                                 OutputDirectory,
                                                 StreamingBlockBytes);
                        break;
                }
            }
            if (encoder == null)
            {
                throw new Exception(String.Concat("Unable to initialise encoder of type ",
                                                  projectSettings.GetCurrentPlatform().Encoder.ToUpper()));
            }

            WaveProperties.BuiltAsset.FirstPeakSample = ComputePeakEnvelope();
            encoder.Encode();
        }

        int ComputePeakEnvelope()
        {
            int dataSize = WaveFile.Data.RawData.Length;
            int numSamples = dataSize / 2;

            const int windowSize = 4096;
            int overallPeak = 0;
            List<int> peakSamples = new List<int>();
            int numPeakSamples = Math.Max(1, numSamples / windowSize);
            for (int peakSampleIndex = 0; peakSampleIndex < numPeakSamples; peakSampleIndex++)
            {
                int maxSampleLevel = 0;
                for (int i = 0; i < windowSize; i++)
                {
                    int sampleByteOffset = 2 * (i + windowSize * peakSampleIndex);
                    if (sampleByteOffset >= dataSize)
                    {
                        break;
                    }
                    int currentSample = BitConverter.ToInt16(WaveFile.Data.RawData, sampleByteOffset);
                    int absValue = Math.Min(ushort.MaxValue, Math.Abs(currentSample) * 2);
                    maxSampleLevel = Math.Max(maxSampleLevel, absValue);

                    overallPeak = Math.Max(maxSampleLevel, overallPeak);
                }
                peakSamples.Add(maxSampleLevel);
            }

            // Don't generate peak file for streams
            bool generatePeakData = peakSamples.Count >= 2 && StreamingBlockBytes == 0;
            if (generatePeakData)
            {
                string outputFile = string.Concat(OutputDirectory, Path.GetFileNameWithoutExtension(WaveFile.FileName), ".peak");
                //overwrites peak file if it exists
                if(File.Exists(outputFile)) new FileInfo(outputFile) { IsReadOnly = false };
                FileStream outputStream = new FileStream(outputFile, FileMode.Create);
                BinaryWriter writer = new BinaryWriter(outputStream);

                // Don't repeat the first sample; start from index 1
                for (int i = 1; i < peakSamples.Count; i++)
                {
                    ushort sample = PlatformSpecific.FixEndian((UInt16)(peakSamples[i]), Platform);
                    writer.Write(sample);
                }
                writer.Close();
                outputStream.Close();
            }

            // If the full peak table is turned off for this asset (for example with streams) then store the highest
            // peak, not the first, in the header.
            return generatePeakData ? peakSamples[0] : overallPeak;
        }

    }
}
