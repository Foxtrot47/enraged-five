﻿using audAssetBuilderProperties;
using Wavelib;

namespace WaveEncoder
{
    public abstract class Encoder
    {
        protected Encoder(bwWaveFile waveFile,
                          WaveProperties waveProperties,
                          string platform,
                          bool isBigEndian,
                          string outputDirectory,
                          uint streamingBlockBytes)
        {
            OutputDirectory = outputDirectory;
            WaveFile = waveFile;
            WaveProperties = waveProperties;
            Platform = platform;
            IsBigEndian = isBigEndian;
            StreamingBlockBytes = streamingBlockBytes;
        }

        public uint StreamingBlockBytes { get; set; }

        public string OutputDirectory { get; set; }
        public bwWaveFile WaveFile { get; set; }
        public WaveProperties WaveProperties { get; set; }
        public string Platform { get; set; }
        public bool IsBigEndian { get; set; }

        public abstract void Encode();
    }
}