﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using audAssetBuilderCommon;
using Wavelib;
using audAssetBuilderProperties;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace WaveEncoder
{
    public class ADPCMEncoder:Encoder
    {
        private string m_ADPCMFileName;
        private string m_Header;
        private bool m_HasCustomLipsync;

        public ADPCMEncoder(bwWaveFile waveFile, WaveProperties waveProperties, string platform, bool isBigEndian, string outputDirectory, uint streamingBlockBytes) :
            base(waveFile, waveProperties, platform, isBigEndian, outputDirectory, streamingBlockBytes) 
        {
            m_ADPCMFileName = WaveFile.FileName.ToUpper().Replace(".WAV", ".DATA");
            m_Header = WaveFile.FileName.ToUpper().Replace(".WAV", ".HEADER");
            m_HasCustomLipsync = waveProperties.BuiltAsset.CustomLipsync;
        }

        public override void Encode()
        {
            BuildADPCMFile();

            if (!File.Exists(m_ADPCMFileName))
            {
                throw new Exception(String.Concat("ADPCM data chunk not found ", m_ADPCMFileName));
            }
            if (!File.Exists(m_Header))
            {
                throw new Exception(String.Concat("ADPCM header chunk not found ", m_Header));
            }

            uint originalWaveLengthBytes;
            uint lengthBytes;
            uint lengthSamples;
            uint sampleRate;
            int predictedValue;
            uint stepIndex;

            if (!File.Exists(m_Header))
            {
                throw new Exception(String.Concat("Unable to open wave .HEADER file ", m_Header));
            }
            FileStream fs = new FileStream(m_Header, FileMode.Open, FileAccess.Read);
            if (fs.Length == 0)
            {
                throw new Exception(String.Concat("File with length 0 (.HEADER file) ", m_Header));
            }

            BinaryReader br = new BinaryReader(fs);

            originalWaveLengthBytes = br.ReadUInt32();
            lengthBytes = br.ReadUInt32();
            lengthSamples = br.ReadUInt32();
            sampleRate = br.ReadUInt32();
            predictedValue = br.ReadInt32();
            stepIndex = br.ReadUInt32();

            br.Close();
            fs.Close();

            string name = Path.GetFileNameWithoutExtension(WaveFile.FileName);
            string outputFileNoExt = String.Concat(OutputDirectory, name);

            if (WaveProperties == null)
            {
                throw new Exception(String.Concat("Unable to find wave properties for wave ", WaveFile.FileName));
            }
            WaveProperties.BuiltAsset.Size = lengthBytes;
            WaveProperties.Save(String.Concat(outputFileNoExt, ".XML"));
            WaveProperties.BuiltAsset.NumberOfSamples = WaveFile.Data.NumSamples;

            //write format chunk
            WaveFormatChunk formatChunk = new WaveFormatChunk();

            formatChunk.LengthSamples = IsBigEndian ? PlatformSpecific.FlipEndian(lengthSamples) : lengthSamples;

            if (WaveFile.Sample != null && WaveFile.Sample.Loops.Count == 1)
            {
                formatChunk.LoopPointSamples = (int)(IsBigEndian ? PlatformSpecific.FlipEndian(WaveFile.Sample.Loops[0].Start) : WaveFile.Sample.Loops[0].Start);
            }
            else
            {
                formatChunk.LoopPointSamples = -1;
            }

            formatChunk.SampleRate = IsBigEndian ? PlatformSpecific.FlipEndian((UInt16)WaveFile.Format.SampleRate) : (UInt16)WaveFile.Format.SampleRate;
            formatChunk.Format = (byte)(IsBigEndian ? audStreamFormat.AUD_FORMAT_ADPCM : audStreamFormat.AUD_FORMAT_ADPCM);
            formatChunk.Headroom = IsBigEndian ? PlatformSpecific.FlipEndian((UInt16)WaveProperties.BuiltAsset.HeadRoom) : (UInt16)WaveProperties.BuiltAsset.HeadRoom;

            formatChunk.LoopBegin = (UInt16)predictedValue;
            formatChunk.LoopEnd = 0;
            formatChunk.PlayBegin = (Byte)stepIndex;
            formatChunk.PlayEnd = 0;
            formatChunk.HasCustomLipsync = m_HasCustomLipsync;

            formatChunk.FirstPeakSample = IsBigEndian ?
                PlatformSpecific.FlipEndian((UInt16)WaveProperties.BuiltAsset.FirstPeakSample) :
                (UInt16)WaveProperties.BuiltAsset.FirstPeakSample;

            Console.Out.WriteLine(String.Concat(outputFileNoExt, ".", Chunks.TYPE.FORMAT.ToString("g")));
            formatChunk.Serialize(String.Concat(outputFileNoExt, ".", Chunks.TYPE.FORMAT.ToString("g")));

            //write marker chunk
            if (this.WaveFile.Markers.Count <= 0)
            {
                return;
            }

            var markersChunkProperties = this.WaveProperties.BuiltAsset.Chunks.FirstOrDefault(c => c.Name == "MARKERS");
            if (null == markersChunkProperties)
            {
                if (this.WaveProperties.BuiltAsset.AddOrUpdateChunk("MARKERS", 0))
                {
                    markersChunkProperties = this.WaveProperties.BuiltAsset.Chunks.First(c => c.Name == "MARKERS");
                }
                else
                {
                    throw new Exception("Failed to add new MARKERS chunk");
                }
            }

            markersChunkProperties.ChunkMarkers.Clear();

            var markerChunk = new WaveMetadataMarkerChunk(this.Platform);
            for (var i = 0; i < this.WaveFile.Markers.Count; i++)
            {
                var marker = this.WaveFile.Markers[i];
                var timeOffset = (uint)(marker.Position / (this.WaveProperties.BuiltAsset.SampleRate / 1000.0f));
                markersChunkProperties.AddMarker(marker.Name, timeOffset.ToString());
                markerChunk.Add(marker.Name, marker.Position);
            }

            Console.Out.WriteLine((String.Concat(this.OutputDirectory, name, ".", Chunks.TYPE.MARKERS.ToString("g"))));
            markerChunk.Serialize(String.Concat(this.OutputDirectory, name, ".", Chunks.TYPE.MARKERS.ToString("g")));
        }

        void ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.Error.WriteLine(e.Data);
        }

        void OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.Out.WriteLine(e.Data);
        }

        private void BuildADPCMFile()
        {
            string fileName = WaveFile.FileName;
            ProcessStartInfo psi = new ProcessStartInfo("ADPCMEncoder.exe");
            StringBuilder sb = new StringBuilder();

            sb.Append(" -f ");//file name
            sb.Append("\"");
            sb.Append(fileName);
            sb.Append("\"");

            sb.Append(" -p PC");//platform

            if (StreamingBlockBytes != 0) // stream file
            {
                sb.Append(" -stream");//platform
            }

            psi.Arguments = sb.ToString();
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            psi.RedirectStandardError = true;
            psi.RedirectStandardOutput = true;

            Process p = new Process();
            p.OutputDataReceived += new DataReceivedEventHandler(OutputDataReceived);
            p.ErrorDataReceived += new DataReceivedEventHandler(ErrorDataReceived);
            p.StartInfo = psi;
            p.Start();
            p.BeginOutputReadLine();
            p.BeginErrorReadLine();
            p.WaitForExit();

            // if the return value is negative then something went wrong
            if (p.ExitCode < 0)
            {
                throw new ArgumentException("ADPCM encoder returned " + p.ExitCode.ToString());
            }
        }

    }
}
