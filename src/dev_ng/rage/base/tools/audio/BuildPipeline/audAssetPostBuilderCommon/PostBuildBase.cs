﻿using System.Collections.Generic;

namespace audAssetPostBuilderCommon
{
    public abstract class PostBuildBase
    {
        public List<string> TypeFilters { get; private set; }
        public List<string> InputFiles { get; private set; }
        public List<string> OutputFiles { get; private set; }
        public string XSLFile { get; private set; }
        public string Platform { get; private set; }
        public string ProjectSettingsPath { get; private set; }

        protected PostBuildBase(string[] args)
        {
            TypeFilters = new List<string>();
            InputFiles = new List<string>();
            OutputFiles = new List<string>();

            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "-inputFile":
                        {
                            InputFiles.Add(args[++i]);
                            break;
                        }
                    case "-outputFile":
                        {
                            OutputFiles.Add(args[++i]);
                            break;
                        }
                    case "-platform":
                        {
                            string p = args[i + 1].ToUpper();
                            if (p.Contains("XENON"))
                            {
                                Platform = "XBOX360";
                            }
                            else
                            {
                                Platform = p;
                            }
                            break;
                        }
                    case "-typeFilter":
                        {
                            TypeFilters.Add(args[++i]);
                            break;
                        }
                    case "-projectSettings":
                        {
                            ProjectSettingsPath = args[i + 1];
                            break;
                        }
                    case "-xsl":
                        {
                            XSLFile = args[i + 1];
                            break;
                        }
                }
            }
        }

        public abstract void Run();
    }
}