﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace audInterleavedDataReader
{
    class Program
    {
        static void Main(string[] args)
        {
            string formatChunk, seekChunk, dataChunk, projectSettings, platform;
            formatChunk = seekChunk = dataChunk = projectSettings = platform = string.Empty;

            float timeOffset = 0.0f;

            for(int i = 0; i< args.Length; i++)
            {
                switch (args[i])
                {
                    case "-f":
                        formatChunk = args[++i];
                        break;
                    case "-s":
                        seekChunk = args[++i];
                        break;
                    case "-d":
                        dataChunk = args[++i];
                        break;
                    case "-ps":
                        projectSettings = args[++i];
                        break;
                    case "-p":
                        platform = args[++i];
                        break;
                    case "-t":
                        timeOffset = float.Parse(args[++i]);
                        break;
                }
            }

            if (string.IsNullOrEmpty(formatChunk) || 
                string.IsNullOrEmpty(seekChunk) || 
                string.IsNullOrEmpty(dataChunk) ||
                string.IsNullOrEmpty(projectSettings) ||
                string.IsNullOrEmpty(platform))
            {
                Console.Out.WriteLine("Incorrect usage. Expected input:");
                Console.Out.WriteLine("-f <format chunk path>");
                Console.Out.WriteLine("-s <seek chunk path> ");
                Console.Out.WriteLine("-d <data chunk path>");
                Console.Out.WriteLine("-ps <platform setting path>");
                Console.Out.WriteLine("-p <platform tag>");
                Console.Out.WriteLine("Press any key to exit");
                Console.ReadKey();
                return;
            }


            InterleavedDataReader idr = new InterleavedDataReader(dataChunk, formatChunk,
                                                                    seekChunk, projectSettings, 
                                                                    platform, timeOffset);
            idr.Run();

        }

        
    }
}
