﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using audStreamBuilder;
using System.IO;

namespace audInterleavedDataReader
{
    public class DataStreamDefault: DataStreamBase
    {
        public DataStreamDefault(byte type, uint nameHash, uint lengthInSamples, ushort sampleRate, int streamingPacketBytes)
            : base(type, nameHash, lengthInSamples, sampleRate, streamingPacketBytes)
        { }

        public override void GetPacketData(BinaryReader br, LoadBlockMetadata metadata, 
                                                PacketSeekEntryList packetSeekEntryList, float timeOffset)
        {
            int byteOffset = 0;
            uint sampleOffset = GetSamples(timeOffset);

            //add as normal
            if (timeOffset <= 0 || sampleOffset < packetSeekEntryList[0].StartSampleIndex)
            {
                if (metadata.SamplesToSkip > 0)
                {
                    byteOffset = StreamingPacketBytes;
                }
            }
            else
            {
                int i = 0;
                for (; i <= metadata.NumPackets; i++)
                {
                    if (packetSeekEntryList[i].StartSampleIndex <= sampleOffset &&
                         packetSeekEntryList[i].EndSampleIndex >= sampleOffset)
                    {
                        SampleOffset = (int)(sampleOffset - packetSeekEntryList[i].StartSampleIndex);
                        break;
                    }
                }

                if (i > 0)
                {
                    byteOffset = StreamingPacketBytes*i;
                }

            }

            br.BaseStream.Seek(br.BaseStream.Position + byteOffset, SeekOrigin.Begin);
            byte[] bytes = br.ReadBytes((int)(metadata.NumPackets * StreamingPacketBytes) - byteOffset);
            Data.AddRange(bytes);
        }
    }
}
