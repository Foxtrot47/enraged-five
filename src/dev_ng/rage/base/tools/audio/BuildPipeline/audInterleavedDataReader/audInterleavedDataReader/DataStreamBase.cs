﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using audAssetBuilderCommon;
using System.IO;
using audStreamBuilder;

namespace audInterleavedDataReader
{
    public abstract class DataStreamBase
    {
        public uint NameHash { get; private set; }
        public uint LengthInSamples { get; set; }
        public ushort SampleRate { get; private set; }
        public byte WaveEncoder { get; private set; }
        public List<byte> Data { get; set; }
        public int SampleOffset { get; set; }
        public int StreamingPacketBytes { get; set; }

        protected DataStreamBase(byte type, uint nameHash, uint lengthInSamples, ushort sampleRate, int streamingPacketBytes)
        {
            WaveEncoder = type;
            NameHash = nameHash;
            LengthInSamples = lengthInSamples;
            SampleRate = sampleRate;
            StreamingPacketBytes = streamingPacketBytes;
            Data = new List<byte>();
        }

        public static DataStreamBase GetStream(byte type, uint nameHash, uint lengthInSamples,
                                               ushort sampleRate, int streamingPacketBytes)
        {
            switch (type)
            {
                case (byte)audStreamFormat.AUD_FORMAT_MP3:
                    return new DataStreamPS3(type, nameHash, lengthInSamples, sampleRate, streamingPacketBytes);
                default:
                    return new DataStreamDefault(type, nameHash, lengthInSamples, sampleRate, streamingPacketBytes);
            }
        }
 
        public float GetTimeMs(uint sampleIndex)
        {
            return (float)sampleIndex / ((float)SampleRate * 0.001f);
        }

        public uint GetSamples(float timeInMS)
        {
            return (uint)Math.Round(timeInMS * (float)SampleRate * 0.001f);
        }

        public abstract void GetPacketData(BinaryReader br, LoadBlockMetadata metadata, 
                                                PacketSeekEntryList packetSeekTable, float timeOffset);
    }
}
