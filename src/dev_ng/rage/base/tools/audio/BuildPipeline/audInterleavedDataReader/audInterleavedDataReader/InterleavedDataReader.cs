﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rage;
using System.IO;
using audAssetBuilderCommon;

namespace audInterleavedDataReader
{
    public class InterleavedDataReader
    {
        private audProjectSettings m_ProjectSettings;
        private uint m_NumOfBlocks;
        private uint m_StreamingBlockBytes;
        private uint m_NumDataStreams;
        private float m_TimeOffset;

        private string m_Platform;
        private List<DataStreamBase> m_DataStreams;
        private List<LoadBlockSeekTableEntry> m_BlockSeekTable;

        private string m_DataFile;
        private string m_FormatFile;
        private string m_SeekTableFile;

        private int m_StreamingPacketBytes;

        public InterleavedDataReader(string dataFile, string formatFile, string seekTableFile,
                                        string projectSettings, string platform, float timeOffset)
        {
            m_ProjectSettings = new audProjectSettings(projectSettings);
            m_ProjectSettings.SetCurrentPlatformByTag(platform);
            m_StreamingPacketBytes = m_ProjectSettings.GetCurrentPlatform().StreamingPacketBytes;
            m_Platform = platform;
            m_TimeOffset = timeOffset;
            m_DataStreams = new List<DataStreamBase>();
            m_BlockSeekTable = new List<LoadBlockSeekTableEntry>();
            m_DataFile = dataFile;
            m_FormatFile = formatFile;
            m_SeekTableFile = seekTableFile;
        }

        public void Run()
        {
            ReadFormat(m_FormatFile);
            ReadSeekTable(m_SeekTableFile);
            RecreateStreams(m_DataFile);
            WriteStreams();
        }

        private void ReadSeekTable(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);

            for (int i = 0; i < m_NumOfBlocks; i++)
            {
                m_BlockSeekTable.Add(new LoadBlockSeekTableEntry()
                {
                    StartSample = PlatformSpecific.FixEndian(br.ReadUInt32(), m_Platform)                
                });
            }

            br.Close();
            fs.Close();
        }

        private void ReadFormat(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);

            m_NumOfBlocks = PlatformSpecific.FixEndian(br.ReadUInt32(), m_Platform);
            m_StreamingBlockBytes = PlatformSpecific.FixEndian(br.ReadUInt32(), m_Platform);
            m_NumDataStreams = PlatformSpecific.FixEndian(br.ReadUInt32(), m_Platform);

            for (int i = 0; i < m_NumDataStreams; i++)
            {
                //name hash
                uint nameHash = PlatformSpecific.FixEndian(br.ReadUInt32(), m_Platform);
                //length in samples
                uint lengthInSamples = PlatformSpecific.FixEndian(br.ReadUInt32(), m_Platform);
                //headroom - we don't care about this
                br.ReadUInt16();
                //sample rate
                ushort sampleRate = PlatformSpecific.FixEndian(br.ReadUInt16(), m_Platform);
                //format
                byte format = br.ReadByte();
                //reserved 0 - we don't care about this
                br.ReadByte();
                //reserved 1 - we don't care about this
                br.ReadUInt16();
                

                m_DataStreams.Add(DataStreamBase.GetStream(format, nameHash, lengthInSamples, sampleRate, m_StreamingPacketBytes));
            }

            br.Close();
            fs.Close();
        }

        public void RecreateStreams(string dataPath)
        {
            FileStream fs = new FileStream(dataPath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);

            int numToSkip = 0;

            if (m_TimeOffset > 0)
            {
                for (; numToSkip < m_NumOfBlocks - 1; numToSkip++)
                {
                    float startTime = m_BlockSeekTable[numToSkip].StartSample == 0? 0: (float)m_BlockSeekTable[numToSkip].StartSample / ((float)m_DataStreams[0].SampleRate * 0.001f);
                    float endTime = (float)(m_BlockSeekTable[numToSkip + 1].StartSample - 1) / ((float)m_DataStreams[0].SampleRate * 0.001f);

                    if (m_TimeOffset >= startTime && m_TimeOffset <= endTime)
                    {
                        break;
                    }
                }
            }

            br.BaseStream.Position = br.BaseStream.Seek(br.BaseStream.Position + (numToSkip * m_StreamingBlockBytes), SeekOrigin.Begin);

            for (int i = 0; i < m_NumOfBlocks - numToSkip; i++)
            {
                System.Diagnostics.Debug.WriteLine("-------------------");
                System.Diagnostics.Debug.WriteLine("Block no " + (i + numToSkip));
                LoadBlock.ComputePackets(br, m_DataStreams, m_StreamingBlockBytes, m_ProjectSettings, m_TimeOffset);
                System.Diagnostics.Debug.WriteLine("-------------------");
            }

            br.Close();
            fs.Close();
        }

        public void WriteStreams()
        {
            
            FileStream offsetFS = new FileStream(m_DataFile.ToUpper().Replace(".DATA", ".OFFSET"), FileMode.Create, FileAccess.Write);
            StreamWriter offsetSW = new StreamWriter(offsetFS);

            string path = Path.GetDirectoryName(m_DataFile);

            foreach (DataStreamBase ds in m_DataStreams)
            {
                int index = ds.Data.Count-1;
                //trim data
                for (; index >= 0; index--)
                {
                    if (ds.Data[index] != 0)
                    {
                        break;
                    }
                }

                index ++;
                ds.Data.RemoveRange(index, ds.Data.Count - index);

                //write data
                FileStream fs = new FileStream(String.Concat(path, "\\", ds.NameHash, ".data"), FileMode.Create, FileAccess.Write);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(ds.Data.ToArray());
                bw.Close();
                fs.Close();

                offsetSW.Write(ds.NameHash);
                offsetSW.Write(" ");
                offsetSW.WriteLine(ds.SampleOffset);
            }

            offsetSW.Close();
            offsetFS.Close();            
        }
    }
}

