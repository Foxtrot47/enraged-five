﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using audAssetBuilderCommon;
using rage;
using audStreamBuilder;

namespace audInterleavedDataReader
{ 

    public class LoadBlock
    {

        public static void ComputePackets(BinaryReader br, List<DataStreamBase> dataStreams,
                                            uint streamingBlockBytes, audProjectSettings projectSettings, 
                                            float timeOffset)
        {
            string platform = projectSettings.GetCurrentPlatform().PlatformTag;
            int streamingPacketBytes = projectSettings.GetCurrentPlatform().StreamingPacketBytes;

            long pos = br.BaseStream.Position;

            List<LoadBlockMetadata> metadataList = LoadBlockMetadata(br, dataStreams, platform);

            System.Diagnostics.Debug.WriteLine("Metadata List Size " + (br.BaseStream.Position - pos));
            pos = br.BaseStream.Position;

            List<PacketSeekEntryList> packetSeekTable = LoadPacketSeekTable(br,platform,dataStreams.Count, metadataList);
            
            System.Diagnostics.Debug.WriteLine("Packet Seek Table Size " + (br.BaseStream.Position - pos));
            pos = br.BaseStream.Position;

            //padded to packet allignment
            int padding = (int)(streamingPacketBytes - (br.BaseStream.Position % streamingPacketBytes));
            if (padding > 0 && padding < streamingPacketBytes)
            {
                br.ReadBytes(padding);
            }

            pos = br.BaseStream.Position;

            LoadPacketData(br, dataStreams, metadataList, packetSeekTable, timeOffset);

            System.Diagnostics.Debug.WriteLine("Packet total data Size " + (br.BaseStream.Position - pos));
            pos = br.BaseStream.Position;

            //padded to block allignment
            padding = (int)(streamingBlockBytes - (br.BaseStream.Position % streamingBlockBytes));
            if (padding > 0 && padding < streamingBlockBytes)
            {
                br.ReadBytes(padding);
            }
        }

        private static void LoadPacketData(BinaryReader br, List<DataStreamBase> dataStreams,
                                            List<LoadBlockMetadata> metadataList, 
                                            List<PacketSeekEntryList> packetSeekTable,
                                            float timeOffset)
        {
            for (int i = 0; i < dataStreams.Count; i++)
            {
                dataStreams[i].GetPacketData(br, metadataList[i], packetSeekTable[i],timeOffset);
            }
        }

        private static List<LoadBlockMetadata> LoadBlockMetadata(BinaryReader br, List<DataStreamBase> dataStreams, 
                                                                  string platform)
        {
            List<LoadBlockMetadata> metadataList = new List<LoadBlockMetadata>();

            for (int i = 0; i < dataStreams.Count; i++)
            {
                switch (dataStreams[i].WaveEncoder)
                {
                    case (byte)audStreamFormat.AUD_FORMAT_MP3:
                        metadataList.Add(new LoadBlockMetadataPS3(br, platform)); 
                        break;
                    default:
                        metadataList.Add(new LoadBlockMetadata(br, platform));
                        break;
                }
            }

            return metadataList;
        }

        private static List<PacketSeekEntryList> LoadPacketSeekTable(BinaryReader br, string platform, 
                                                int numStreams, List<LoadBlockMetadata> metadataList)
       {
           List<PacketSeekEntryList> packetSeekTable = new List<PacketSeekEntryList>();

           for (int i = 0; i < numStreams; i++)
           {
               PacketSeekEntryList paketSeekEntryList = new PacketSeekEntryList();

               for (int j = 0; j < metadataList[i].NumPackets; j++)
               {
                  paketSeekEntryList.Add(new PacketSeekEntry()
                   {
                       StartSampleIndex = PlatformSpecific.FixEndian(br.ReadUInt32(), platform),
                       EndSampleIndex = PlatformSpecific.FixEndian(br.ReadUInt32(), platform)
                   });
               }

               packetSeekTable.Add(paketSeekEntryList);
           }

           return packetSeekTable;
       }

    }
}
