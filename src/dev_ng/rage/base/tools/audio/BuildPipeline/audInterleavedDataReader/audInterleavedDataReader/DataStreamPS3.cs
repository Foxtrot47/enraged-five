﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using audStreamBuilder;

namespace audInterleavedDataReader
{
    public class DataStreamPS3 : DataStreamBase
    {
        #region MPEG Definitions

        enum audMpegVersions
        {
            MPEG1,
            MPEG2,
            MPEG2_5
        };

        static int[] g_MpegVersionTable =
	{
		(int)audMpegVersions.MPEG2_5,
		-1,
		(int)audMpegVersions.MPEG2,
		(int)audMpegVersions.MPEG1
	};

        enum audMpegLayers
        {
            LAYER1,
            LAYER2,
            LAYER3
        };

        static int[] g_MpegLayerTable =
	{
		-1,
		(int)audMpegLayers.LAYER3,
		(int)audMpegLayers.LAYER2,
		(int)audMpegLayers.LAYER1
	};

        static int[] g_MpegSlotBytes =
	{
		4,	//LAYER1
		1,	//LAYER2
		1	//LAYER3
	};

        static int[,] g_Mpeg1BitrateTable =
	{
		{-1,	-1,		-1},
		{32,	32,		32},
		{64,	48,		40},
		{96,	56,		48},
		{128, 	64,		56},
		{160, 	80,		64},
		{192, 	96,		80},
		{224, 	112, 	96},
		{256, 	128, 	112},
		{288, 	160, 	128},
		{320, 	192, 	160},
		{352, 	224, 	192},
		{384, 	256, 	224},
		{416, 	320, 	256},
		{448, 	384, 	320},
		{-1,	-1,		-1}
	};

        static int[,] g_Mpeg2BitrateTable =
	{
		{-1,	-1,		-1},
		{32,	8,		8},
		{48,	16,		16},
		{56,	24,		24},
		{64,	32,		32},
		{80,	40,		40},
		{96,	48,		48},
		{112, 	56, 	56},
		{128, 	64, 	64},
		{144, 	80, 	80},
		{160, 	96, 	96},
		{176, 	112, 	112},
		{192, 	128, 	128},
		{224, 	144, 	144},
		{256, 	160, 	160},
		{-1,	-1,		-1}
	};

        static int[,] g_MpegSampleRateTable =
	{
		{44100,	22050,	11025},
		{48000,	24000,	12000},
		{32000,	16000,	8000},
		{-1,		-1,		-1}
	};

        #endregion

        public DataStreamPS3(byte type, uint nameHash, uint lengthInSamples, ushort sampleRate, int streamingPacketBytes)
            : base(type, nameHash, lengthInSamples, sampleRate, streamingPacketBytes)
        {
        }

        public override void GetPacketData(BinaryReader br, LoadBlockMetadata metadata, 
                                                  PacketSeekEntryList packetSeekEntryList,
                                                    float timeOffset)
        {
            //System.Diagnostics.Debug.WriteLine(br.BaseStream.Position.ToString("x"));
            LoadBlockMetadataPS3 metadataPS3 = metadata as LoadBlockMetadataPS3;
            uint sampleOffset = GetSamples(timeOffset);
            int byteOffset = 0;
            
            //add as normal
            if (timeOffset <= 0 || sampleOffset < packetSeekEntryList[0].StartSampleIndex)
            {       
                if(metadata.SamplesToSkip>0)
                {
                    System.Diagnostics.Debug.WriteLine("samples to skip " + metadata.SamplesToSkip);
                    byteOffset = (int)ComputeByteOffsetForPacket(br,1);
                }
            }
            else
            {
                int i = 0;
                for (; i <= metadata.NumPackets; i++)
                {
                    if (packetSeekEntryList[i].StartSampleIndex <= sampleOffset && 
                         packetSeekEntryList[i].EndSampleIndex >= sampleOffset)
                    {
                        SampleOffset = (int)(sampleOffset - packetSeekEntryList[i].StartSampleIndex);
                        break;
                    }
                }
                if (i > 0)
                {
                    byteOffset = (int)ComputeByteOffsetForPacket(br, i);
                }
            }

            //skip offset
            System.Diagnostics.Debug.WriteLine("Bytes skipped "+byteOffset);
            br.ReadBytes(byteOffset);
            //add remainder of bytes
            byte[] bytes = br.ReadBytes((int)metadataPS3.NumberOfBytes - byteOffset);
            System.Diagnostics.Debug.WriteLine("No of bytes for stream " + metadataPS3.NumberOfBytes);
            System.Diagnostics.Debug.WriteLine("Size of remaining packets " + bytes.Length);

            if (!(bytes[0] == 0xff && (bytes[1] & 0xE0) == 0xE0))
            {
                throw new Exception("Invalid Frame");
            }

            Data.AddRange(bytes);
        }

        private long ComputeByteOffsetForPacket(BinaryReader br, int packetsToSkip)
        {
            long originalStreamPosition = br.BaseStream.Position;

            if (!(br.ReadByte() == 0xff && (br.ReadByte() & 0xE0) == 0xE0))
            {
                throw new Exception("Invalid Frame");
            }

            br.BaseStream.Seek(originalStreamPosition, SeekOrigin.Begin);

            long offset = originalStreamPosition;
            long packetPosition = originalStreamPosition;

            for (int i = 0; i < packetsToSkip; i++)
            {
                while (br.BaseStream.Position - packetPosition < 2048)
                {
                    offset = br.BaseStream.Position;
                    byte[] frame = br.ReadBytes(3);
                    br.BaseStream.Seek(offset, SeekOrigin.Begin);

                    if (frame[0] == 0xFF && (frame[1] & 0xE0) == 0xE0)
                    {
                        int versionIndex = (frame[1] >> 3) & 0x3;
                        int version = g_MpegVersionTable[versionIndex];
                        if (version < 0)
                        {
                            throw new Exception("Invalid MPEG version");
                        }

                        int layerIndex = (frame[1] >> 1) & 0x3;
                        int layer = g_MpegLayerTable[layerIndex];
                        if (layer < 0)
                        {
                            throw new Exception("Invalid MPEG Layer");
                        }

                        int bitrateIndex = (frame[2] >> 4) & 0xF;
                        int bitrate;
                        if (version == (int)audMpegVersions.MPEG1)
                        {
                            bitrate = g_Mpeg1BitrateTable[bitrateIndex, layer] * 1000;
                        }
                        else
                        {
                            bitrate = g_Mpeg2BitrateTable[bitrateIndex, layer] * 1000;
                        }

                        if (bitrate < 0)
                        {
                            throw new Exception("Invalid bitrate");
                        }

                        int sampleRateIndex = (frame[2] >> 2) & 0x3;
                        int sampleRate = g_MpegSampleRateTable[sampleRateIndex, version];
                        if (sampleRate < 0)
                        {
                            throw new Exception("Invalid sampleRate");
                        }

                        int padding = (frame[2] >> 1) & 0x1;
                        int paddingBytes = padding * g_MpegSlotBytes[layer];

                        int frameBytes = (((1152 >> 3) * bitrate) / sampleRate) + paddingBytes;

                        int protection = frame[1] & 0x01;
                        if (protection == 0)
                        {
                            //There should be a 16-bit CRC at the end of the header.
                            frameBytes += 2;
                        }

                        //Jump to the end of this frame.
                        br.BaseStream.Seek(frameBytes, SeekOrigin.Current); ;
                    }
                }

                packetPosition = offset;
                br.BaseStream.Seek(packetPosition, SeekOrigin.Begin);
            }

            //reset stream
            br.BaseStream.Seek(originalStreamPosition, SeekOrigin.Begin);
            //return offset
            return offset - originalStreamPosition;
        }
    }
}
