﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using audAssetBuilderCommon;
using System.IO;

namespace audInterleavedDataReader
{
    public class LoadBlockMetadata
    {
        public UInt32 StartOffsetPackets { get; set; }
        public UInt32 NumPackets { get; set; }
        public UInt32 SamplesToSkip { get; set; }
        public UInt32 NumSamples { get; set; }

        public LoadBlockMetadata(BinaryReader br, string platform)
        {
            StartOffsetPackets = PlatformSpecific.FixEndian(br.ReadUInt32(), platform);
            NumPackets = PlatformSpecific.FixEndian(br.ReadUInt32(), platform);
            SamplesToSkip = PlatformSpecific.FixEndian(br.ReadUInt32(), platform);
            NumSamples = PlatformSpecific.FixEndian(br.ReadUInt32(), platform);
        }
    }

    public class LoadBlockMetadataPS3 : LoadBlockMetadata
    {
        public UInt32 NumberOfFrames { get; set; }
        public UInt32 NumberOfBytes { get; set; }

        public LoadBlockMetadataPS3(BinaryReader br, string platform)
            : base(br, platform)
        {
            NumberOfFrames = PlatformSpecific.FixEndian(br.ReadUInt32(), platform);
            NumberOfBytes = PlatformSpecific.FixEndian(br.ReadUInt32(), platform);
        }
    }
}
