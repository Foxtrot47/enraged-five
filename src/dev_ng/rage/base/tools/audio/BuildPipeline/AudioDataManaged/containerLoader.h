#pragma once

#include "audiodata/containerbuilder.h"

namespace rage
{
	public class containerLoader
	{
	public:
		containerLoader();
		~containerLoader();
		bool Load(const char* path);

		adatContainer* GetContainer() const { return m_Container; }

		unsigned int GetSize() const { return m_Size; }
		unsigned int GetHeaderSize() const { return m_HeaderSize; }

	private:
		char *m_Data;
		adatContainerHeader* m_Header;
		adatContainer* m_Container;
		unsigned int m_Size;
		unsigned int m_HeaderSize;
	};
}