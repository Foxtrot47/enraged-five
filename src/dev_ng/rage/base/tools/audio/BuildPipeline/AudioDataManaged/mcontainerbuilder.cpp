#include "mcontainerbuilder.h"
#include "string/stringhash.h"

using namespace System::Runtime::InteropServices;

namespace rage
{
	mContainerBuilder::mContainerBuilder(int packingType)
	{
		containerBuilder = new adatContainerBuilder((adatContainer::PackingType)packingType);
	}

	mContainerBuilder::~mContainerBuilder()
	{
		delete containerBuilder;
	}

	adatObjectId mContainerBuilder::AddObject(const u32 objectNameHash)
	{
		return containerBuilder->AddObject(objectNameHash);
	}

	bool mContainerBuilder::DeleteObject(const adatObjectId objectId)
	{
		return containerBuilder->DeleteObject(objectId);
	}

	adatObjectId mContainerBuilder::FindObject(const u32 objectNameHash)
	{
		return containerBuilder->FindObject(objectNameHash);
	}

	adatObjectDataChunkId mContainerBuilder::AddDataChunk(const adatObjectId objectId, const u32 dataTypeHash, array<System::Byte>^ data, const u32 alignment)
	{
		pin_ptr<System::Byte> pinnedData = &data[0];
		return containerBuilder->AddDataChunk(objectId,dataTypeHash,(u8*)pinnedData, data->Length, alignment);
	}

	adatObjectDataChunkId mContainerBuilder::FindDataChunk(const adatObjectId objectId, const u32 dataTypeHash)
	{
		return containerBuilder->FindDataChunk(objectId,dataTypeHash);
	}

	bool mContainerBuilder::DeleteDataChunk(const adatObjectId objectId, const adatObjectDataChunkId chunkId)
	{
		return containerBuilder->DeleteDataChunk(objectId,chunkId);
	}

	bool mContainerBuilder::PopulateFromPrebuiltContainer(array<System::Byte>^ data)
	{
		pin_ptr<Byte> pinnedData = &data[0];
		adatContainerHeader* header = (adatContainerHeader*)pinnedData;
		adatContainer* container = new adatContainer(header);
		bool returnVal = containerBuilder->PopulateFromPrebuiltContainer(container);
		delete header;
		delete container;
		return returnVal;
	}

	bool mContainerBuilder::Serialize(String ^filePath, const bool outputBigEndian)
	{
		IntPtr ptr = Marshal::StringToHGlobalAnsi(filePath);
		const char* str = (const char*)ptr.ToPointer();
		fiStream *stream = fiStream::Create(str);
		bool returnVal = false;
		if(stream)
		{
			returnVal =  containerBuilder->Serialize(stream, outputBigEndian);
			stream->Close();
		}

		Marshal::FreeHGlobal(ptr);
		
		return returnVal;
	}

	unsigned int mContainerBuilder::ComputeHeaderSize()
	{
		return containerBuilder->ComputeHeaderSize();
	}

	unsigned int mContainerBuilder::ComputeObjectNameHash(String^ string)
	{
		IntPtr ptr = Marshal::StringToHGlobalAnsi(string);
		const char* str =  (const char*)ptr.ToPointer();

		unsigned int key = atStringHash(str);

		Marshal::FreeHGlobal(ptr);

		const u32 hashMask = (1<<adatContainerObjectTableEntry::kObjectNameHashWidth)-1;
		return key & hashMask;
	}

	unsigned int mContainerBuilder::ComputeDataTypeHash(String^ string)
	{
		IntPtr ptr = Marshal::StringToHGlobalAnsi(string);
		const char* str =  (const char*)ptr.ToPointer();

		unsigned int key = atStringHash(str);

		Marshal::FreeHGlobal(ptr);

		const u32 hashMask = (1<<adatContainerObjectDataTableEntry::kDataTypeHashWidth)-1;
		return key & hashMask;
	}

	void mContainerBuilder::SetEncryptedData(const bool isEncrypted)
	{
		containerBuilder->SetEncryptedData(isEncrypted);
	}
}