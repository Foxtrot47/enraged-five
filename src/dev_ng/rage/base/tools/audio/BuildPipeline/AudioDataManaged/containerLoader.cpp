#include "containerLoader.h"
#include <iostream>
#include <fstream>
#include <sys/stat.h>

using namespace std;

// HACK: fix for unresolved external...don't know where its coming from
namespace stlpx_std
{
	void __cdecl __stl_throw_length_error(char const *) {}
}

namespace rage
{
	containerLoader::containerLoader()
	{
	}

	bool containerLoader::Load(const char* path)
	{
		struct stat results;
		//get size of file
		if (stat(path, &results) == 0)
		{
			m_Size = (unsigned int)results.st_size;
		}
		else
		{
			return false;
		}
		
		//open file to read
		std::ifstream myFile(path,std::ios::in |std::ios::binary);	

		if (!myFile.is_open()) 
		{
			return false;
		}

		// first read the standard header
		adatContainerHeader header;
		myFile.read((char*)&header, sizeof(header));
		myFile.seekg(0);

		// construct a temporary container to read the size of the entire header
		adatContainer container;
		container.SetHeader(&header);
		m_HeaderSize = container.GetHeaderSize();

		m_Data = new char[m_HeaderSize];

		myFile.read(m_Data, m_HeaderSize);
		myFile.close();


		m_Header = (adatContainerHeader *)m_Data;
		m_Container = new adatContainer(m_Header);

		// sanity check
		if(GetContainer()->GetHeaderSize() != m_HeaderSize)
		{
			return false;
		}

		return true;
	}

	containerLoader::~containerLoader()
	{		
		delete m_Container;
		delete m_Header;
		delete[] m_Data;
	}
}