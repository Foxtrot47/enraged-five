
#pragma once
#include "audiodata/containerbuilder.h"

#ifndef M_CONTAINERBUILDER_H
#define M_CONTAINERBUILDER_H

using namespace System;

namespace rage
{
	public ref class mContainerBuilder
	{
		public:
		mContainerBuilder(int packingType);
		~mContainerBuilder();

		adatObjectId AddObject(const u32 objectNameHash);
		
		bool DeleteObject(const adatObjectId objectId);
		
		adatObjectId FindObject(const u32 objectNameHash);

		adatObjectDataChunkId AddDataChunk(const adatObjectId objectId, const u32 dataTypeHash, array<System::Byte>^ data, const u32 allignment);

		adatObjectDataChunkId FindDataChunk(const adatObjectId objectId, const u32 dataTypeHash);		

		bool DeleteDataChunk(const adatObjectId objectId, const adatObjectDataChunkId chunkId);

		bool PopulateFromPrebuiltContainer(array<System::Byte>^ data);

		bool Serialize(String ^filePath, const bool outputBigEndian);

		static u32 ComputeObjectNameHash(String^ objectName);

		static u32 ComputeDataTypeHash(String^ objectName);

		u32 ComputeHeaderSize();

		void SetEncryptedData(const bool isEncrypted);

	private:
		adatContainerBuilder *containerBuilder;
	};
}
#endif
