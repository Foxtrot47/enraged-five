#ifndef __AUD_ENGINE_UTIL_WRAPPER
#define __AUD_ENGINE_UTIL_WRAPPER

#include "audioengine/engineutil.h"


namespace rage
{

public ref class audEngineUtilWrapper
{
public:
	static void InitClass() { audEngineUtil::InitClass(); }
	static double RoundSample16Bit(double fSample)
	{
		fSample = System::Math::Floor(0.5 + fSample);
		fSample = System::Math::Min(+32767.0, System::Math::Max(fSample, -32768.0) );
		return fSample;
	}
	static double DitherAndRoundSample16Bit(double fSample)
	{
		double fDither = audEngineUtil::GetRandomFloat() - audEngineUtil::GetRandomFloat();
		fSample += fDither;
		fSample = System::Math::Floor(0.5 + fSample);
		fSample = System::Math::Min(+32767.0, System::Math::Max(fSample, -32768.0) );
		return fSample;
	}
};

}

#endif
