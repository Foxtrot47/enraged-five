#pragma once
#include "containerLoader.h"

#ifndef M_CONTAINER_H
#define M_CONTAINER_H

using namespace System;
using namespace System;

namespace rage
{
	public ref class mContainer
	{

	public:
		mContainer(String^ path);
		~mContainer();
		unsigned int GetSize();
		unsigned int GetHeaderSize();
		unsigned int GetNumObjects();
		unsigned int ComputeMetadataSize();
		unsigned int ComputeContiguousObjectSize(unsigned int objectNameHash);
		unsigned int ComputeObjectDataSize(unsigned int objectNameHash);
		unsigned int GetObjectNameHash(unsigned int objectID);
		unsigned int GetNumDataChunksForObject(unsigned int objectID);

		int FindObjectId(String ^objectName);
		int FindDataChunkId(const int objectId, String ^chunkName);
		void GetChunkData(const int objectId, const int chunkId, unsigned int %dataTypeHash, unsigned int %offsetBytes, unsigned int %sizeBytes);

		static int OPTIMISED =0;
		static int NORMAL = 1;
		static int STREAMING = 2;

	private:
		adatContainer* m_Container;
		containerLoader* m_ContainerLoader;
	};
}
#endif