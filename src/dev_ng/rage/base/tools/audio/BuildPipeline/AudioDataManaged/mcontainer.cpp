#include "mcontainer.h"

using namespace System::Runtime::InteropServices;

namespace rage
{
	mContainer::mContainer(String^ path)
	{
		//load container
		m_ContainerLoader = new containerLoader();
		IntPtr pathPtr = Marshal::StringToHGlobalAnsi(path);
		const char* pathStr = (char *)pathPtr.ToPointer();
		m_ContainerLoader->Load(pathStr);
		Marshal::FreeHGlobal(pathPtr);
		//set container
		m_Container = m_ContainerLoader->GetContainer();
	}

	unsigned int mContainer::GetNumObjects()
	{
		//number of waves/objects
		return m_Container->GetNumObjects();
	}

	unsigned int mContainer::ComputeContiguousObjectSize(unsigned int objectNameHash)
	{
		unsigned int objectId = m_Container->FindObject(objectNameHash);
		return m_Container->ComputeContiguousObjectSize(objectId);
	}

	unsigned int mContainer::ComputeMetadataSize()
	{
		return m_Container->ComputeContiguousMetadataSize();
	}

	unsigned int mContainer::ComputeObjectDataSize(unsigned int objectNameHash)
	{
		unsigned int objectId = m_Container->FindObject(objectNameHash);
		return m_Container->ComputeObjectDataSize(objectId);
	}

	unsigned int mContainer::GetObjectNameHash(unsigned int objectID)
	{
		return m_Container->GetObjectNameHash(objectID);
	}

	unsigned int mContainer::GetNumDataChunksForObject(unsigned int objectID)
	{
		return m_Container->GetNumDataChunksForObject(objectID);
	}

	unsigned int mContainer::GetSize()
	{
		//get container/bank size
		return m_ContainerLoader->GetSize();
	}

	unsigned int mContainer::GetHeaderSize()
	{
		return m_ContainerLoader->GetHeaderSize();
	}

	mContainer::~mContainer()
	{
		delete m_ContainerLoader;
	}

	int mContainer::FindObjectId(String ^objectName) 
	{ 
		IntPtr objectNamePtr = Marshal::StringToHGlobalAnsi(objectName);
		const char* objectNameCStr = (char *)objectNamePtr.ToPointer();
		adatObjectId objectId = m_Container->FindObject(objectNameCStr); 
		Marshal::FreeHGlobal(objectNamePtr);
		return (int)objectId;
	}

	int mContainer::FindDataChunkId(const int objectId, String ^chunkName) 
	{ 
		IntPtr chunkNamePtr = Marshal::StringToHGlobalAnsi(chunkName);
		const char* chunkNameCStr = (char *)chunkNamePtr.ToPointer();
		adatObjectDataChunkId chunkId = m_Container->FindObjectData(objectId, chunkNameCStr); 
		Marshal::FreeHGlobal(chunkNamePtr);
		return (int)chunkId;
	}

	void mContainer::GetChunkData(const int objectId, const int chunkId, unsigned int %dataTypeHash, unsigned int %offsetBytes, unsigned int %sizeBytes) 
	{ 
		u32 offsetBytesU32, sizeBytesU32, dataTypeHashU32;
		m_Container->GetObjectDataChunk((adatObjectId)objectId, (adatObjectDataChunkId)chunkId, dataTypeHashU32, offsetBytesU32, sizeBytesU32); 

		sizeBytes = sizeBytesU32;
		offsetBytes = offsetBytesU32;
		dataTypeHash = dataTypeHashU32;
	}


}