#include "stdafx.h"
#include "ADPCMEncoder.h"

using namespace System;

int main(array<System::String ^> ^args)
{
	String^ file;
	String^ platform;
	bool isstream = false;
	for(int i=0; i<args->Length; i++)
	{

		if(String::Compare(args[i],"-f",false)==0)
		{
			file = args[++i];
		}
		else if(String::Compare(args[i],"-p",false)==0)
		{
			platform = args[++i];
		}
		else if(String::Compare(args[i],"-stream",false) == 0)
		{
			isstream = true;
		}

	}

	if(String::IsNullOrEmpty(file) || String::IsNullOrEmpty(platform))
	{
		Console::WriteLine("Incorrect usage, expected: -f <file> -p <platform> [-stream]");
		return -1;
	}

	ADPCMEncoder^ encoder = gcnew ADPCMEncoder(file, platform, isstream);
	if(encoder->Run())
	{
		return 0;
	}
	else
	{
		return -1;
	}
}