#include "IMA_ADPCM.h"

using namespace Wavelib;
using namespace System;

struct audWaveData
{
	void *waveData;
	unsigned int lengthBytes;
	unsigned int lengthSamples;
	unsigned int sampleRate;			
	unsigned int compression;
	bool isstream;
	int16_t predictedValue;
	uint16_t stepIndex;
};

const int g_NumValidSampleRates = 9;
const int g_ValidSampleRates[g_NumValidSampleRates] =
{
	8000,
	11025,
	12000,
	16000,
	22050,
	24000,
	32000,
	44100,
	48000
};

public ref class ADPCMEncoder
{
public:
	ADPCMEncoder(String^ waveName, String^ platform, bool isstream);
	~ADPCMEncoder();
	bool Run();

private:
	audWaveData* m_WaveData;
	String^ m_DestinationFile;
	String^ m_DestinationHeaderFile;
	String^ m_Platform;

	bool EncodeADPCM( void **waveSampleDataOut, unsigned int &waveSampleDataOutLengthBytes);
	bool EncodeADPCMStream( void **waveSampleDataOut, unsigned int &waveSampleDataOutLengthBytes);

	void SerialiseADPCM(void *waveSampleDataOut, unsigned int waveSampleDataOutLengthBytes);

};

