#include "stdafx.h"
#include "ADPCMEncoder.h"
#include "memory.h"
#include "math.h"

using namespace System::Collections::Generic;
using namespace System::IO;
using namespace audAssetBuilderCommon;

ADPCMEncoder::ADPCMEncoder(String^ fileName, String^ platform, bool isstream)
{
	m_Platform = platform->ToUpper();
	m_DestinationFile = fileName->ToUpper()->Replace(".WAV",".DATA");
	m_DestinationHeaderFile = fileName->ToUpper()->Replace(".WAV",".HEADER");

	Console::WriteLine("ADPCM encoding {0}", fileName);

	bwWaveFile ^wave = gcnew bwWaveFile(fileName,false);
	array<unsigned char>^ rawData = wave->Data->RawData;
	pin_ptr<unsigned char> pinnedRawData = &rawData[0];
	char* waveData = new char[wave->Data->RawData->Length];
	memcpy(waveData,pinnedRawData,wave->Data->RawData->Length);
	
	//Pull out wave data
	m_WaveData = new audWaveData;
	m_WaveData->waveData = waveData;
	m_WaveData->compression = 1;
	m_WaveData->isstream = isstream;
	m_WaveData->lengthBytes = wave->Data->RawData->Length;
	m_WaveData->lengthSamples = wave->Data->NumSamples;
	m_WaveData->sampleRate = wave->Format->SampleRate;
	m_WaveData->predictedValue = 0;
	m_WaveData->stepIndex = 0;
}

ADPCMEncoder::~ADPCMEncoder()
{
	delete[] m_WaveData->waveData;
	delete m_WaveData;
}

bool ADPCMEncoder::Run()
{
	void *waveSampleDataOut;
	unsigned int waveSampleDataOutLengthBytes; 

	if(m_WaveData->isstream)
	{
		if(!EncodeADPCMStream(&waveSampleDataOut, waveSampleDataOutLengthBytes))
		{
			return false;
		}
		SerialiseADPCM(waveSampleDataOut, waveSampleDataOutLengthBytes);

	}
	else
	{
		//if(!EncodeADPCM(&waveSampleDataOut, waveSampleDataOutLengthBytes))
		if(!EncodeADPCMStream(&waveSampleDataOut, waveSampleDataOutLengthBytes))
		{
			return false;
		}
		SerialiseADPCM(waveSampleDataOut, waveSampleDataOutLengthBytes);
	}

	delete[] waveSampleDataOut;

	return true;
}
#define Abs(x) (x>=0?x:-x)
#define Min(x,y) (x<y?x:y)

// This was used for speech in GTA IV and MP3, it encodes the entire wave with a single start predicted value and step index, not used in GTA V
bool ADPCMEncoder::EncodeADPCM( void **waveSampleDataOut, unsigned int &waveSampleDataOutLengthBytes)
{
	IMA_ADPCM adpcm;

	void* tempWaveSampleDataOut = new char[(m_WaveData->lengthBytes/2)]; //this is 2 times bigger than needed
	if( !tempWaveSampleDataOut )
		return false;

	int16_t* ptr = (int16_t*)m_WaveData->waveData;
	adpcm.EncodeInit(ptr[0], ptr[1]);

	m_WaveData->predictedValue = adpcm.PredictedValue;
	m_WaveData->stepIndex = adpcm.StepIndex;

	uint32_t length = 0;
	uint8_t* pIn = (uint8_t*)m_WaveData->waveData;
	length = adpcm.Encode((uint8_t*)tempWaveSampleDataOut, 0, (const int16_t*)pIn, m_WaveData->lengthBytes);
	length /= 8; // returned length was in bits, we need bytes

	*waveSampleDataOut = new char[length];
	if( !waveSampleDataOut ) 
	{
		delete[] tempWaveSampleDataOut;
		return false;
	}
	waveSampleDataOutLengthBytes = length;
	memcpy(*waveSampleDataOut, tempWaveSampleDataOut, length);

	//char fileName[256];
	//sprintf(fileName, "c:\\bm_wav%d.raw", g_AdpcmFileNo++);
	//FILE* fp = fopen(fileName, "wb");
	//if(fp)
	//{
	//	IMA_ADPCM a;
	//	u8* decode = (u8*)new char[length*4];
	//	memset( decode, 0, length*4 );
	//	adpcm.PredictedValue = m_WaveData->predictedValue;
	//	adpcm.StepIndex = m_WaveData->stepIndex;

	//	int bytesDecoded = a.Decode((int16_t*)decode, (uint8_t*)tempWaveSampleDataOut,0,length);
	//	fwrite(decode,bytesDecoded,1,fp);

	//	fclose(fp);
	//	delete [] decode;
	//}
	delete [] tempWaveSampleDataOut;

	return true;
}

// Every 2k packet has it's own predicted value and step index
bool ADPCMEncoder::EncodeADPCMStream( void **waveSampleDataOut, unsigned int &waveSampleDataOutLengthBytes)
{
	// For each 2048 bytes of encoded data we have a 4 byte header, 
	// that means we have 2048-4 bytes of adpcm data = 2044*4 bytes pcm = 8176 bytes pcm data

	IMA_ADPCM adpcm;

	char* tempWaveSampleDataOut = new char[(m_WaveData->lengthBytes/2)];	// allocate way too much memory for encoded file(double)
	if( !tempWaveSampleDataOut )
		return false;

	char* tempDataPtr = tempWaveSampleDataOut;

	// Calculate hoe many full blocks we need to encode.
	// Each block is 2048 bytes the first 4 bytes holds the Predicted Value and Step Index for the block.
	// That leaves 2044 bytes of encoded ADPCM data or 8176 bytes of PCM data which is 4088 PCM samples.
	int iBlocks = m_WaveData->lengthBytes / 8176;	 

	uint8_t* pIn = (uint8_t*)m_WaveData->waveData;

	uint32_t encodedBytesPCM = 0;
	uint32_t totalLength = 0; //adpcm bytes
	for( int i=0; i<iBlocks; i++ )
	{
		int16_t pv;
		uint8_t sv;

		uint32_t length = 0;

		int16_t* ptr = (int16_t*)pIn;

		adpcm.EncodeInit(ptr[0], ptr[1]);

		pv = adpcm.PredictedValue;
		sv = adpcm.StepIndex;

		// store predicted value and step index in first 4 bytes
		uint16_t* ppp = (uint16_t*)tempDataPtr;
		ppp[0] = (uint16_t)sv;
		tempDataPtr+=2;
		int16_t* ippp = (int16_t*)tempDataPtr;
		ippp[0] = (int16_t)pv;
		tempDataPtr+=2;

		length = adpcm.Encode((uint8_t*)tempDataPtr, 0, (const int16_t*)pIn, 8176);	// 8176 is PCM bytes we're encoding
		length /= 8; // returned length was in bits, we need bytes

		totalLength += (length+4);
		pIn += 8176;
		tempDataPtr += length;	// Length should be 2044, which is the number of ADPCM bytes output by the encoder.
		encodedBytesPCM += 8176;
	}

	// Check to see if we have any remaining data, this is our final non full block.
	if( encodedBytesPCM < m_WaveData->lengthBytes )	
	{
		int bytesToEncode = m_WaveData->lengthBytes - encodedBytesPCM;
		//Assert(bytesToEncode < 8176); // Make sure we have less than a full block

		int16_t pv;
		uint8_t sv;

		uint32_t length = 0;

		int16_t* ptr = (int16_t*)pIn;

		adpcm.EncodeInit(ptr[0], ptr[1]);

		pv = adpcm.PredictedValue;
		sv = adpcm.StepIndex;

		// store predicted value and step index in first 4 bytes
		uint16_t* ppp = (uint16_t*)tempDataPtr;
		ppp[0] = (uint16_t)sv;
		tempDataPtr+=2;
		int16_t* ippp = (int16_t*)tempDataPtr;
		ippp[0] = (int16_t)pv;
		tempDataPtr+=2;

		length = adpcm.Encode((uint8_t*)tempDataPtr, 0, (const int16_t*)pIn, bytesToEncode);	// 8176 is PCM bytes we're encoding
		length /= 8; // returned length was in bits, we need bytes

		totalLength += (length+4);
		pIn += bytesToEncode;
		tempDataPtr += length;	// Number of ADPCM bytes we have encoded.
		encodedBytesPCM += length*4;

	}

	*waveSampleDataOut = new char[totalLength];
	if( !waveSampleDataOut )
	{
		delete[] tempWaveSampleDataOut;
		return false;
	}
	waveSampleDataOutLengthBytes = totalLength;
	memcpy(*waveSampleDataOut, tempWaveSampleDataOut, totalLength);

	//tempDataPtr = tempWaveSampleDataOut;

	//char fileName[256];
	//sprintf(fileName, "c:\\bm_wav%d.raw", g_AdpcmFileNoBS++);
	//FILE* fp = fopen(fileName, "wb");
	//if(fp)
	//{
	//	u8* decode = (u8*)new char[totalLength*4];
	//	memset( decode, 0, totalLength*4 );
	//	for(int i=0; i<iBlocks; i++)
	//	{
	//		IMA_ADPCM a;
	//		s16* v = (s16*)tempDataPtr;
	//		a.PredictedValue = v[1];
	//		a.StepIndex = (u8)v[0];
	//		tempDataPtr += 4;
	//		int bytesDecoded = a.Decode((int16_t*)decode, (uint8_t*)tempDataPtr,0,2044);
	//		fwrite(decode,bytesDecoded,1,fp);
	//		tempDataPtr += 2044;
	//	}
	//	fclose(fp);
	//	delete [] decode;
	//}

	delete [] tempWaveSampleDataOut;

	return true;
}

void ADPCMEncoder::SerialiseADPCM(void *waveSampleDataOut, unsigned int waveSampleDataOutLengthBytes)
{
	FileStream ^fs = gcnew FileStream(m_DestinationFile, FileMode::Create);
	BinaryWriter^ bw = gcnew BinaryWriter(fs);

	unsigned char* data = (unsigned char*) waveSampleDataOut;

	for(unsigned int i=0; i<waveSampleDataOutLengthBytes; i++)
	{
		bw->Write(data[i]);			
	}

	bw->Flush();
	bw->Close();
	fs->Close();

	FileStream ^fs2 = gcnew FileStream(m_DestinationHeaderFile, FileMode::Create);
	BinaryWriter^ bw2 = gcnew BinaryWriter(fs2);

	bw2->Write(PlatformSpecific::FixEndian(m_WaveData->lengthBytes,m_Platform));
	bw2->Write(PlatformSpecific::FixEndian(waveSampleDataOutLengthBytes,m_Platform));
	bw2->Write(PlatformSpecific::FixEndian(m_WaveData->lengthSamples,m_Platform));
	bw2->Write(PlatformSpecific::FixEndian(m_WaveData->sampleRate,m_Platform));

	unsigned int foo = m_WaveData->predictedValue;
	bw2->Write(PlatformSpecific::FixEndian(foo,m_Platform));
	foo = m_WaveData->stepIndex;
	bw2->Write(PlatformSpecific::FixEndian(foo,m_Platform));

	bw2->Flush();
	bw2->Close();
	fs2->Close();

}


