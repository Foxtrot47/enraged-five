﻿using System.IO;
using System.Text;
using System.Collections;
using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using audAssetBuilderCommon;
using audAssetBuilderProperties;
using Wavelib;

namespace audGranularBuilder
{
    public class GranularBuilder : WaveBuildBase
    {
		enum GranularDataType
		{
			GrainOffsets,
			LoopData,
			PitchValues,
		};

		[StructLayout(LayoutKind.Explicit)]
		struct float_union
		{
			[FieldOffset(0)]
			public float pitchFloat;

			[FieldOffset(0)]
			public uint pitchUInt;
		}

		public class audGrainData
		{
			public uint startSample;
			public float pitch;
		};

		public class audLoopDefinition
		{
			public uint playbackType;
			public uint hashIdentifier;
			public List<int> validGrains = new List<int>();
		};

        public GranularBuilder(string[] args) : base(args)
        {
        }

        public override void Run()
        {
            ExtractGranularData();
        }

		private void GenerateFinalPackets()
		{
			const int crossfadeOverlapSize = 256;

			bool isXbox = (string.Compare(Platform, "Xbox 360", true) == 0 || string.Compare(Platform, "X360", true) == 0 || string.Compare(Platform, "XBOX360", true) == 0);
			bool isPS3 = string.Compare(Platform, "PS3", true) == 0;

			string grainFileName = WaveFile.FileName.Replace(".WAV", ".GRANULARGRAINS");

			if (File.Exists(grainFileName))
			{
				float sampleRateConversionRatio = (float)WaveProperties.BuiltAsset.NumberOfSamplesPostResampling/(float)WaveProperties.SourceAsset.NumberOfSamples;
				Console.Out.WriteLine("Adjusting grain sample rate by " + sampleRateConversionRatio);

				bwWaveFile xmaFile = null;
				string xmaFileName = WaveFile.FileName.Replace(".WAV", ".xma");

				if (isXbox)
				{
					if (File.Exists(xmaFileName))
					{
						xmaFile = new bwWaveFile(xmaFileName, false); 
					}
				}

				FileStream grainFileStream = new FileStream(grainFileName, FileMode.Open, FileAccess.Read);
				BinaryReader grainBinaryReader = new BinaryReader(grainFileStream);

				ArrayList grainStartSamples = new ArrayList();
				ArrayList grainPitches = new ArrayList();

				for(int loop = 0; loop < grainBinaryReader.BaseStream.Length/sizeof(UInt32); loop+=2)
				{
					UInt32 grainPos = grainBinaryReader.ReadUInt32();
					grainPos = (UInt32)(grainPos * sampleRateConversionRatio);

					UInt32 grainPitch = grainBinaryReader.ReadUInt32();

					grainStartSamples.Add(grainPos);
					grainPitches.Add(grainPitch);
				}

				grainFileStream.Close();
				grainBinaryReader.Close();

				FileStream outputFileStream = new FileStream(grainFileName, FileMode.Truncate, FileAccess.Write);
				BinaryWriter packetBinaryWriter = new BinaryWriter(outputFileStream);

				List<ushort> seekTable = null;

				if (isPS3)
				{
					string seektableFileName = WaveFile.FileName.Replace(".WAV", ".SEEKTABLE");

					if (System.IO.File.Exists(seektableFileName))
					{
						seekTable = new List<ushort>();

						var fs = new FileStream(seektableFileName, FileMode.Open, FileAccess.Read);
						var bw = new BinaryReader(fs);

						while (bw.BaseStream.Position < bw.BaseStream.Length)
						{
							seekTable.Add(PlatformSpecific.FixEndian(bw.ReadUInt16(), Platform));
						}

						bw.Close();
						fs.Close();
					}
				}

				ArrayList unsmoothedPeakSampleData = new ArrayList();
				ArrayList smoothedPeakSampleData = new ArrayList();

				for (int grainIndex = 0; grainIndex < grainStartSamples.Count; grainIndex++)
				{
					int nextGrain = grainIndex + 1;

					if (nextGrain >= grainStartSamples.Count)
					{
						unsmoothedPeakSampleData.Add((UInt16)unsmoothedPeakSampleData[unsmoothedPeakSampleData.Count - 1]);
						continue;
					}

					Int16 maxSampleLevel = 0;

					for (int i = (int)(((UInt32)grainStartSamples[grainIndex]) * 2); i < ((UInt32)grainStartSamples[nextGrain] + crossfadeOverlapSize) * 2; i += 2)
					{
						if (i < WaveFile.Data.RawData.Length)
						{
							Int16 currentSample = BitConverter.ToInt16(WaveFile.Data.RawData, i);
							Int16 absValue;

							if (currentSample != Int16.MinValue)
							{
								absValue = Math.Abs(currentSample);
							}
							else
							{
								absValue = Int16.MaxValue;
							}
	
							maxSampleLevel = Math.Max(maxSampleLevel, absValue);
						}
					}

					unsmoothedPeakSampleData.Add((UInt16)maxSampleLevel);
				}

				for (int grainIndex = 0; grainIndex < grainStartSamples.Count; grainIndex++)
				{
					int startIndex = grainIndex - 10;
					int endIndex = grainIndex + 10;

					int totalPeakData = 0;
					int numGrains = 0;

					for (int searchIndex = startIndex; searchIndex <= endIndex; searchIndex++)
					{
						if (searchIndex >= 0 && searchIndex < unsmoothedPeakSampleData.Count)
						{
							totalPeakData += (UInt16)unsmoothedPeakSampleData[searchIndex];
							numGrains++;
						}
					}

					smoothedPeakSampleData.Add((UInt16)(totalPeakData / (float)numGrains));
				}

				bool finalGrain = false;

				for (int grainIndex = 0; grainIndex < grainStartSamples.Count && !finalGrain; grainIndex++)
				{
					int currentGrain = grainIndex;
					int nextGrain = grainIndex + 1;

					// Make sure we don't run off the end of the array. The final grain position is just a marker for the length of the final
					// grain, so doesn't get valid packet data 
					if (nextGrain >= grainStartSamples.Count)
					{
						nextGrain = currentGrain;
					}

					int dataSize = WaveFile.Data.RawData.Length;

					// PC/PS3
					if (!isXbox)
					{
						int playBegin = (int)((UInt32)grainStartSamples[currentGrain]);
						int playEnd = (int)((UInt32)grainStartSamples[nextGrain] + crossfadeOverlapSize);

						if (isPS3)
						{
							// Preserve transients flag means we get an extra MP3 frame of data added to the file
							playBegin += (1152 + 16);
							playEnd += (1152 + 16);

							if (playBegin >= WaveProperties.BuiltAsset.NumberOfSamplesPostResampling + 1152 ||
								playEnd >= WaveProperties.BuiltAsset.NumberOfSamplesPostResampling + 1152)
							{
								Console.Out.WriteLine("Clamping final grain to " + grainIndex);
								finalGrain = true;
							}
						}
						else if (playBegin >= WaveProperties.BuiltAsset.NumberOfSamplesPostResampling ||
								 playEnd >= WaveProperties.BuiltAsset.NumberOfSamplesPostResampling)
						{
							Console.Out.WriteLine("Clamping final grain to " + grainIndex);
							finalGrain = true;
						}

						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)grainStartSamples[currentGrain], Platform)));
						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)grainPitches[currentGrain], Platform)));

						if (isPS3)
						{
							if (WaveProperties.BuiltAsset.Compression == 101)
							{
								packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian(0u, Platform)));
								packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian(0u, Platform)));	
							}
							else
							{
								if (seekTable == null)
								{
									throw new Exception("Attempting to build granular data for " + WaveProperties.SourceAsset.Name + " but no seek table exists yet!");
								}

								UInt32 startSampleFrameOffsetBytes = ComputeFrameOffsetFromSamples(seekTable, (UInt32)grainStartSamples[currentGrain]);
								packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian(startSampleFrameOffsetBytes, Platform)));

								UInt32 endSampleFrameOffsetBytes = ComputeFrameOffsetFromSamples(seekTable, (UInt32)grainStartSamples[currentGrain] + 1152 + 16 + crossfadeOverlapSize);
								packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian(endSampleFrameOffsetBytes, Platform)));
							}
						}
					}
					// X360, PCM
					else if (isXbox && WaveProperties.BuiltAsset.Compression == 101)
					{
						int playBegin = (int)((UInt32)grainStartSamples[currentGrain]);
						int playEnd = (int)((UInt32)grainStartSamples[nextGrain] + crossfadeOverlapSize);

						if (playBegin >= WaveProperties.BuiltAsset.NumberOfSamplesPostResampling ||
							playEnd >= WaveProperties.BuiltAsset.NumberOfSamplesPostResampling)
						{
							Console.Out.WriteLine("Clamping final grain to " + grainIndex);
							finalGrain = true;
						}

						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)grainStartSamples[currentGrain], Platform)));
						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)grainPitches[currentGrain], Platform)));
						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)0, Platform)));
						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt16)0, Platform)));
						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt16)0, Platform)));
					}
					// X360, XMA
					else if (isXbox)
					{
						if(xmaFile == null)
						{	
							throw new Exception("Attempting to build granular data for " + WaveProperties.SourceAsset.Name + " but no .xma file exists yet!");
						}

						// The XMA decoder lies about its decode point - its actually 384 samples behind where we set it to be. However, the first 128 samples of
						// decoded data also have a slight crossfade caused by the decompression FFT, which we don't want for granular playback. 
						// Therefore we set the start point to 256 samples ahead, and then skip the first 128 samples we read, thereby avoiding the crossfade and 
						// also reading from the correct position. The end point is set to the full 384 samples ahead in order to finish at the correct point
						int playBegin = (int)((UInt32)grainStartSamples[currentGrain] + 256);
						int playEnd = (int)((UInt32)grainStartSamples[nextGrain] + 384 + crossfadeOverlapSize);

						if (playBegin >= WaveProperties.BuiltAsset.NumberOfSamplesPostResampling ||
							playEnd >= WaveProperties.BuiltAsset.NumberOfSamplesPostResampling)
						{
							Console.Out.WriteLine("Clamping final grain to " + grainIndex);
							finalGrain = true;
						}

						int startSamplesSkipped = 0;
						int endSamplesSkipped = 0;

						int playBeginInBytes = 0;
						int playEndInBytes = 0;

						if (xmaFile != null)
						{
							playBeginInBytes = ComputeBytesToSkip(xmaFile.Data.RawData, xmaFile.Data.RawData.Length, playBegin, out startSamplesSkipped);
							playEndInBytes = ComputeBytesToSkip(xmaFile.Data.RawData, xmaFile.Data.RawData.Length, playEnd, out endSamplesSkipped);
						}

						// ComputeBytesToSkip will get us to the start of the XMA frame containing the sample. We want the frame *after*, so add on 2k to make sure we get there
						playEndInBytes += 2048;

						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)grainStartSamples[currentGrain], Platform)));
						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)grainPitches[currentGrain], Platform)));
						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)startSamplesSkipped, Platform)));
						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt16)(playBeginInBytes/2048), Platform)));
						packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt16)(playEndInBytes/2048), Platform)));
					}

					packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt16)unsmoothedPeakSampleData[currentGrain], Platform)));
					packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt16)smoothedPeakSampleData[currentGrain], Platform)));
				}

				float_union floatUnion;
				floatUnion.pitchUInt = 0;
				floatUnion.pitchFloat = sampleRateConversionRatio;
				packetBinaryWriter.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)floatUnion.pitchUInt, Platform)));

				packetBinaryWriter.Close();
				outputFileStream.Close();
			}
			else if (WaveProperties.SourceAsset.Granular)
			{
				throw new Exception("File " + WaveProperties.SourceAsset.Name + " marked as granular but no .GRANULARGRAIN file found!");
			}
		}

		int ComputeBytesToSkip(byte[] buffer, int bufferLengthBytes, int samplesToSkip, out int samplesSkipped)
		{
			int byteOffset = 0;
			int numSamples = 0;
			samplesSkipped = 0;

			while(byteOffset < bufferLengthBytes)
			{
				UInt32 packetHeader = BitConverter.ToUInt32(buffer, byteOffset);
				byte[] packetHeaderArray = BitConverter.GetBytes(packetHeader);

				int packetFrameCount = GetXmaPacketFrameCount(packetHeaderArray);
				int skip = 1 + GetXmaPacketSkipCount(packetHeaderArray);

				samplesSkipped = numSamples;
				numSamples += 512 * packetFrameCount;
				if(numSamples > samplesToSkip)
				{
					return byteOffset;
				}

				byteOffset += skip*2048;
			}

			return byteOffset;
		}

		int GetXmaPacketFrameCount(byte[] pPacket)
		{
			return (pPacket[0] >> 2);
		}

		int GetXmaPacketSkipCount(byte[] pPacket)
		{
			return (pPacket[3]);
		}

		UInt32 ComputeFrameOffsetFromSamples(List<ushort> seekTable, UInt32 sampleOffset)
		{
			Int32 numSeekTableFrames = seekTable.Count;
			UInt32 seekTableOffsetFrames = sampleOffset / 1152;
			UInt32 subFrameSampleOffset = sampleOffset - (seekTableOffsetFrames * 1152);
			UInt32 frameOffsetBytes = 0;

			for (int i = 0; i < Math.Min(seekTableOffsetFrames, numSeekTableFrames); i++)
			{
				frameOffsetBytes += seekTable[i];
			}

			return frameOffsetBytes;
		}

		private void ExtractGranularData()
        {
			string grainFileName = WaveFile.FileName.Replace(".WAV", ".grn");
			bool withinGrainData = false;
			bool withinLoopData = false;

			if (System.IO.File.Exists(grainFileName))
			{
				System.IO.StreamReader textReader = new System.IO.StreamReader(grainFileName);
				string line = null;
				List<audGrainData> grainDataList = new List<audGrainData>();
				List<audLoopDefinition> loopDefinitionList = new List<audLoopDefinition>();
				uint startSample = 0;
				audLoopDefinition loopDef = new audLoopDefinition();

				while ((line = textReader.ReadLine()) != null)
				{
					if (line.Contains("<grainData>"))
					{
						withinGrainData = true;
					}
					else if (line.Contains("</grainData>"))
					{
						withinGrainData = false;
					}
					if (line.Contains("<loopDefinitions>"))
					{
						withinLoopData = true;
					}
					else if (line.Contains("</loopDefinitions>"))
					{
						withinLoopData = false;
					}
					else if (withinGrainData)
					{
						if (line.Contains("<startSample>"))
						{
							line = line.Replace("<startSample>", "");
							line = line.Replace("</startSample>", "");
							line = line.Replace(" ", "");
							startSample = System.Convert.ToUInt32(line);
						}
						else if (line.Contains("<pitch>"))
						{
							line = line.Replace("<pitch>", "");
							line = line.Replace("</pitch>", "");
							line = line.Replace(" ", "");

							float pitch = System.Convert.ToSingle(line);
							audGrainData newGrainData = new audGrainData();
							newGrainData.startSample = startSample;
							newGrainData.pitch = pitch;
							grainDataList.Add(newGrainData);
						}
					}
					else if (withinLoopData)
					{
						if (line.Contains("<audLoopDefinition>"))
						{
							loopDef = new audLoopDefinition();
						}
						else if (line.Contains("<int>"))
						{
							line = line.Replace("<int>", "");
							line = line.Replace("</int>", "");
							line = line.Replace(" ", "");
							loopDef.validGrains.Add(System.Convert.ToInt32(line));
						}
						else if (line.Contains("<submixIdentifier>"))
						{
							line = line.Replace("<submixIdentifier>", "");
							line = line.Replace("</submixIdentifier>", "");
							line = line.Replace(" ", "");
							loopDef.hashIdentifier = Hash.Generate(line);
						}
						else if(line.Contains("<playbackOrder>"))
						{
							line = line.Replace("<playbackOrder>", "");
							line = line.Replace("</playbackOrder>", "");
							line = line.Replace(" ", "");

							if (line == "Sequential")
							{
								loopDef.playbackType = 0;
							}
							if (line == "Random")
							{
								loopDef.playbackType = 1;
							}
							if (line == "Walk")
							{
								loopDef.playbackType = 2;
							}
							if (line == "Reverse")
							{
								loopDef.playbackType = 3;
							}
							if (line == "Mixed")
							{
								loopDef.playbackType = 4;
							}
						}
						else if (line.Contains("</audLoopDefinition>"))
						{
							loopDefinitionList.Add(loopDef);
						}
					}
				}

				textReader.Close();

				string outputFile = OutputDirectory + Path.GetFileNameWithoutExtension(WaveFile.FileName) + ".GRANULARGRAINS";

				FileStream fs;

				if (File.Exists(outputFile))
				{
					fs = new FileStream(outputFile, FileMode.Truncate, FileAccess.Write);
				}
				else
				{
					fs = new FileStream(outputFile, FileMode.CreateNew, FileAccess.Write);
				}

				BinaryWriter bw = new BinaryWriter(fs);

				foreach (audGrainData grainData in grainDataList)
				{
					bw.Write(System.BitConverter.GetBytes((uint)grainData.startSample));

					float_union floatUnion;
					floatUnion.pitchUInt = 0;
					floatUnion.pitchFloat = grainData.pitch;
					bw.Write(System.BitConverter.GetBytes(floatUnion.pitchUInt));
				}

				bw.Close();
				fs.Close();

				outputFile = OutputDirectory + Path.GetFileNameWithoutExtension(WaveFile.FileName) + ".GRANULARLOOPS";

				if (File.Exists(outputFile))
				{
					fs = new FileStream(outputFile, FileMode.Truncate, FileAccess.Write);
				}
				else
				{
					fs = new FileStream(outputFile, FileMode.CreateNew, FileAccess.Write);
				}

				bw = new BinaryWriter(fs);

				bw.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)loopDefinitionList.Count, Platform)));

				foreach (audLoopDefinition loopDefinition in loopDefinitionList) 
				{
					bw.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)loopDefinition.playbackType, Platform)));
					bw.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)loopDefinition.validGrains.Count, Platform)));
					bw.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)loopDefinition.hashIdentifier, Platform)));

					for (int loop = 0; loop < loopDefinition.validGrains.Count; loop++)
					{
						bw.Write(System.BitConverter.GetBytes(PlatformSpecific.FixEndian((UInt32)loopDefinition.validGrains[loop], Platform)));
					}
				}

				bw.Close();
				fs.Close();
			}
			else if (WaveProperties.SourceAsset.Granular)
			{
				throw new Exception("File " + WaveProperties.SourceAsset.Name + " marked as granular but no .grn file found!");
			}

			GenerateFinalPackets();
        }
    }
}
