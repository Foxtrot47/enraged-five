﻿using System;
using System.Diagnostics;

namespace audGranularBuilder
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var encoder = new GranularBuilder(args);
                encoder.Execute();
                Environment.ExitCode = 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                var argsCombined = "";
                foreach (var s in args)
                {
                    argsCombined += s + " ";
                }
                EventLog.WriteEntry("Audio Build", "GranularBuilder: " + argsCombined + " - " + ex);
                Environment.ExitCode = -1;
            }
        }
    }
}