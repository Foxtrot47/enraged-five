﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using rage.Generator;
using rage.ToolLib;
using rage.ToolLib.Logging;

namespace rage
{
    class VoiceVariationInfo
    {
        public readonly string VoiceName;
        public readonly string WaveName;

        public VoiceVariationInfo(string voiceName, string waveName)
        {
            VoiceName = voiceName;
            WaveName = waveName;
        }

        public string ContextName
        {
            get
            {
                // strip off "_XX.wav"
                return WaveName.Substring(0, WaveName.Length - 7);
            }
        }
        public int Variation
        {
            get
            {
                string contextRegEx = @"\d+";
                Regex regex = new Regex(contextRegEx, RegexOptions.IgnoreCase);
                var match = regex.Match(WaveName.Substring(WaveName.Length - 7));
                if (match.Success)
                {
                    return int.Parse(match.Value);
                }
                return -1;
            }
        }
    }
    public class SpeechExclusionGenerator : IGenerator
    {
        #region IGenerator Members
        ILog m_Log;
        bool IGenerator.Init(rage.ToolLib.Logging.ILog log, string workingPath, rage.audProjectSettings projectSettings, rage.Types.ITypeDefinitionsManager typeDefManager, params string[] args)
        {
            m_Log = log;
            return true;
        }

        XDocument IGenerator.Generate(rage.ToolLib.Logging.ILog log, XDocument inputDoc)
        {
            var outputDoc = new XDocument();
            var objsElem = new XElement("Objects");
            outputDoc.Add(objsElem);

            // Look for any notForGirls tags and create variation masks for that voice/context

            var excludedVariations = new List<VoiceVariationInfo>();
            foreach (var bankNode in inputDoc.Descendants("Bank"))
            {
                var tags = from x in bankNode.Descendants("Tag") where x.Attribute("name").Value.Equals("notForGirls") select x;
                var voiceTags = (from x in bankNode.Descendants("Tag") where x.Attribute("name").Value.Equals("voice") select x.Attribute("value").Value);
                if (voiceTags.Any())
                {
                    var voiceName = voiceTags.First();
                    foreach (var tag in tags)
                    {
                        excludedVariations.Add(new VoiceVariationInfo(voiceName, tag.Ancestors("Wave").First().Attribute("builtName").Value));
                    }
                }
            }

            var voiceContextMasks = new Dictionary<string, uint>();
            foreach (var excludedVariation in excludedVariations)
            {
                var voiceContextMaskKey = string.Format("{0}:{1}", excludedVariation.VoiceName, excludedVariation.ContextName).ToUpper();
                if (voiceContextMasks.ContainsKey(voiceContextMaskKey))
                {
                    voiceContextMasks[voiceContextMaskKey] |= (uint)(1 << (excludedVariation.Variation-1));
                }
                else
                {
                    voiceContextMasks.Add(voiceContextMaskKey, (uint)(1 << (excludedVariation.Variation-1)));
                }
            }

            foreach (var kvp in voiceContextMasks)
            {
                var voiceNameHash = new ToolLib.Hash();
                var contextNameHash = new ToolLib.Hash();

                var tokens = kvp.Key.Split(':');
                string voiceName = tokens[0];
                string contextName = tokens[1];

                voiceNameHash.Value = voiceName;
                contextNameHash.Value = contextName;

                uint lookupHash = voiceNameHash.Key ^ contextNameHash.Key;
                if (lookupHash == 0)
                {
                    lookupHash = voiceNameHash.Key;
                }

                if (kvp.Value > UInt16.MaxValue)
                {
                    m_Log.Error("Invalid mask: {0} for {1}", kvp.Value, kvp.Key);
                }
                string newObjName = string.Format("NFG_{0:X8}", lookupHash);                
                objsElem.Add(new XElement("VoiceContextVariationMask", new XAttribute("name", newObjName),
                                        new XElement("variationMask", kvp.Value)));
            }

            return outputDoc;
        }

        void IGenerator.Shutdown()
        {
            
        }

        #endregion
    }
}
