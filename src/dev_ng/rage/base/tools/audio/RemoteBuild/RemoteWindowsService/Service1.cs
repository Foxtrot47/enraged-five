﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.ServiceModel;

namespace RemoteWindowsService
{
    public partial class Service1 : ServiceBase
    {
        ServiceHost serviceHost;
        string assemblyLocationFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            
            try
            {
                System.Diagnostics.Process.Start("cmd.exe", "/c subst " + System.Configuration.ConfigurationManager.AppSettings["driveMappingLetter"].Replace("\"", "\\\"") + " " + System.Configuration.ConfigurationManager.AppSettings["driveMappingPath"].Replace("\"", "\\\""));

                if (serviceHost != null)
                {
                    serviceHost.Close();
                }
                Uri uri = new Uri(System.Configuration.ConfigurationManager.AppSettings["serviceAddr"]);
                NetTcpBinding binding = new NetTcpBinding();
                serviceHost = new ServiceHost(typeof(RemoteBuildService.Builder), uri);
                serviceHost.AddServiceEndpoint(typeof(RemoteBuildService.IBuilder), binding, "");
                serviceHost.Open();
                if (string.Compare(Environment.CurrentDirectory, assemblyLocationFolder, StringComparison.OrdinalIgnoreCase) != 0)
                {
                    Environment.CurrentDirectory = assemblyLocationFolder;
                }
            }
            catch (Exception ex) {
                System.IO.File.AppendAllText(assemblyLocationFolder + @"\log.log", ex.Message + Environment.NewLine);
            }

        }

        protected override void OnStop()
        {
            try{
                if (serviceHost != null)
                {
                    serviceHost.Close();
                    serviceHost = null;
                }
            }
            catch(Exception ex){
                System.IO.File.AppendAllText(assemblyLocationFolder + @"\log.log", ex.Message + Environment.NewLine);
            }

        }
    }
}
