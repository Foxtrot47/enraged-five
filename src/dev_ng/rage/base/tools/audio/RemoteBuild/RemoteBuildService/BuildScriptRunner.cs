﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.ServiceModel;
using System.Configuration;

namespace RemoteBuildService
{
    public class BuildScriptRunner
    {

        private bool running = false;

        public delegate void BuildFinishedCallback();
        public delegate void BuildOutuptDataCallback(String message);

        private BuildFinishedCallback buildFinishedCallback;
        private BuildOutuptDataCallback buildOutuptDataCallback;

        public void runScript(bool doBuild, bool doIntegration, String branchSpec, bool doRPFgeneration, String username, BuildFinishedCallback buildFinishedCallback, BuildOutuptDataCallback buildOutuptDataCallback) {
            running = true;
            this.buildFinishedCallback = buildFinishedCallback;
            this.buildOutuptDataCallback = buildOutuptDataCallback;
            Process buildProcess = new Process();
            String parameters = "-tc \"Build invoked remotely by "+username+"\"";
            if (doBuild) parameters += " -b";
            if (doRPFgeneration) parameters += " -r";
            if (doIntegration) parameters += " -i " + branchSpec;
            String scriptCall = ConfigurationManager.AppSettings["buildScriptPath"] + " " + parameters;

            buildProcess.StartInfo = new ProcessStartInfo(ConfigurationManager.AppSettings["pythonShell"], "-u " + scriptCall);
            buildProcess.StartInfo.CreateNoWindow = true;
            buildProcess.StartInfo.UseShellExecute = false;
            buildProcess.StartInfo.RedirectStandardOutput = true;
            buildProcess.StartInfo.RedirectStandardError = true;

            buildProcess.OutputDataReceived += new DataReceivedEventHandler(buildProcess_OutputDataReceived);
            buildProcess.Exited += new EventHandler(buildProcess_Exited);


            try
            {
                buildProcess.Start();
            }
            catch (Exception ex) {
                Console.WriteLine("Exception occurred while starting the process ("+ex.Message+")");
                running = false;
            }
            buildProcess.EnableRaisingEvents = true;
            buildProcess.BeginOutputReadLine();
            Console.WriteLine("Build process started: "+scriptCall);
            buildProcess.WaitForExit();
            Console.Write("Process exited with code " + buildProcess.ExitCode);
        }

        public bool isRunning() {
            return running;
        }

        void buildProcess_Exited(object sender, EventArgs e)
        {
            running = false;
            try{
                if (buildFinishedCallback != null) buildFinishedCallback();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Lost connection to client ("+ex.Message+")");
            }
            Console.WriteLine("Process finished");
        }

        void buildProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            try
            {
                if (buildOutuptDataCallback != null) buildOutuptDataCallback(e.Data);
                Console.WriteLine(e.Data);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Lost connection to client ("+ex.Message+")");
            }
        }


    }
}