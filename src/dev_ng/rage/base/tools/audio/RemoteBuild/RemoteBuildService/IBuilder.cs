﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;

namespace RemoteBuildService
{
    [ServiceContract(CallbackContract = typeof(IConsoleCallback))]
    public interface IBuilder
    {

        [OperationContract]
        bool InvokeBuild(BuildParameter parameter);

        [OperationContract]
        IntegrationResult InvokeIntegration(IntegrationParameter parameter);
    }

    public interface IConsoleCallback
    {
        [OperationContract(IsOneWay = true)]
        void ReceiveConsoleMessage(string message);
        [OperationContract(IsOneWay = true)]
        void ProcessFinished();
    }

    [DataContract]
    public class BuildParameter
    {
        string username = "";
        bool createRpfFiles = false;

        [DataMember]
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        [DataMember]
        public bool CreateRpfFiles
        {
            get { return createRpfFiles; }
            set { createRpfFiles = value; }
        }
    }

    [DataContract]
    public class IntegrationParameter
    {
        bool unshelve = false;
        string branchSpec = "";
        bool createRpfFiles = false;

        [DataMember]
        public bool Unshelve
        {
            get { return unshelve; }
            set { unshelve = value; }
        }

        [DataMember]
        public string IntegrationBranchSpec
        {
            get { return branchSpec; }
            set { branchSpec = value; }
        }

        [DataMember]
        public bool CreateRpfFiles
        {
            get { return createRpfFiles; }
            set { createRpfFiles = value; }
        }
    }

    [DataContract]
    public class IntegrationResult {
        public IntegrationResult() {
            AlreadyRunning = false;
            ErrorMessage = "";
            ChangeListNumber = "";
        }
        [DataMember]
        public bool AlreadyRunning { get; set; }
        [DataMember]
        public String ErrorMessage { get; set; }
        [DataMember]
        public String ChangeListNumber { get; set; }
    }
}
