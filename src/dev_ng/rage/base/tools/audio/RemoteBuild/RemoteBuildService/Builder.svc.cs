﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Configuration;

namespace RemoteBuildService
{
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single, ConcurrencyMode=ConcurrencyMode.Multiple)]
    public class Builder : IBuilder
    {
        static Object lockObj = new Object();
        static BuildScriptRunner buildScriptRunner = null;

        public bool InvokeBuild(BuildParameter parameter)
        {
            lock (lockObj)
            {
                if (buildScriptRunner != null && buildScriptRunner.isRunning() == true) return false;
            }

            IConsoleCallback callback = System.ServiceModel.OperationContext.Current.GetCallbackChannel<IConsoleCallback>();
            
            buildScriptRunner = new BuildScriptRunner();
            buildScriptRunner.runScript(true, true, "", parameter.CreateRpfFiles, parameter.Username, new BuildScriptRunner.BuildFinishedCallback(callback.ProcessFinished), new BuildScriptRunner.BuildOutuptDataCallback(callback.ReceiveConsoleMessage));
            return true;
        }



        private bool integrationRunning = false;
        public IntegrationResult InvokeIntegration(IntegrationParameter parameter) {
            IntegrationResult result = new IntegrationResult();
            lock (lockObj) {
                if (integrationRunning == true) {
                    result.AlreadyRunning = true;
                    return result;
                }
                integrationRunning = true;
            }


            try {
                Rockstar.AssetManager.Interfaces.IAssetManager assetManager = Rockstar.AssetManager.Main.AssetManagerFactory.GetInstance(Rockstar.AssetManager.Infrastructure.Enums.AssetManagerType.Perforce,
                    "",
                    ConfigurationManager.AppSettings["p4host"]+":"+ConfigurationManager.AppSettings["p4port"],
                    ConfigurationManager.AppSettings["p4workspace"],
                    ConfigurationManager.AppSettings["p4username"],
                    ConfigurationManager.AppSettings["p4password"],
                    ConfigurationManager.AppSettings["p4depotRoot"],
                    null);
                PerforceIntegrate.PerforceIntegrate p4IntegrationHelper = new PerforceIntegrate.PerforceIntegrate(assetManager);
                String changeListNumber;
                if(p4IntegrationHelper.Integrate(parameter.IntegrationBranchSpec, Rockstar.AssetManager.Infrastructure.Enums.ResolveAction.AcceptTheirs, false, out changeListNumber)){
                    result.ChangeListNumber = changeListNumber;

                    if(changeListNumber==null) {
                        result.ErrorMessage="No changes to integrate";
                    } 
                    else{
                        callCmdCommand("p4 shelve -c " + result.ChangeListNumber);
                        callCmdCommand("p4 revert -c " + result.ChangeListNumber);                
                    }
                }
                else{
                    result.ErrorMessage="Integration failed";
                }

            }
            catch (Exception ex) {
                result.ErrorMessage = ex.Message;
                if (ex.InnerException != null) result.ErrorMessage += " " + ex.InnerException.Message;
            }
            finally {
                lock (lockObj) {
                    integrationRunning = false;
                }
            }
            return result;
        }

        public static void callCmdCommand(String command) {
            Process commandExecutionProcess = new Process();
            commandExecutionProcess.StartInfo = new ProcessStartInfo("cmd.exe", "/C \"" + command + "\"");
            commandExecutionProcess.StartInfo.CreateNoWindow = true;
            commandExecutionProcess.StartInfo.UseShellExecute = false;
            commandExecutionProcess.StartInfo.RedirectStandardOutput = true;
            commandExecutionProcess.StartInfo.RedirectStandardError = true;
            commandExecutionProcess.Start();
            commandExecutionProcess.WaitForExit();
        }
    }
}
