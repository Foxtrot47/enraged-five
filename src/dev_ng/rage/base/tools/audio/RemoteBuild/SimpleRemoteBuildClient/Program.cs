﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rage.ToolLib.CmdLine;
using System.IO;
using System.Diagnostics;
using System.ServiceModel;
using System.Configuration;
using RemoteBuildService;

namespace SimpleRemoteBuildClient {
    class Program {

        static void printHelp() {
            Console.WriteLine("Parameter b or i has to be specified");
            Console.WriteLine("Options:");
            Console.WriteLine("     -h  specify a hostname");
            Console.WriteLine("     -b  build and integrate into staging");
            Console.WriteLine("     -i  specify a branchspec to integrate (-u to unshelve)");
            Console.WriteLine("     -r  generate RPF files (requires -b)");
        }

        static void Main(string[] args) {

            String hostname = ConfigurationManager.AppSettings["defaultServiceHost"];
            bool buildAndIntegrate = false;
            String branchspec = "";
            bool unshelve = false;
            bool createRpfFiles = false;
            CmdLineParser parameterParser = new CmdLineParser(args);
            if(parameterParser.Arguments.ContainsKey("help") || (!parameterParser.Arguments.ContainsKey("b") && !parameterParser.Arguments.ContainsKey("i"))) {
                printHelp();
                return;
            }
            if(parameterParser.Arguments.ContainsKey("h")) {
                String parameterHostname = parameterParser.Arguments["h"];
                if(parameterHostname.Equals("true")) {
                    Console.WriteLine("No hostname specified, using default " + hostname);
                }
                else {
                    hostname = parameterHostname;
                }
            }
            if(parameterParser.Arguments.ContainsKey("b")) {
                buildAndIntegrate = true;
            }
            if(parameterParser.Arguments.ContainsKey("i")) {
                String parameterBranchspec = parameterParser.Arguments["i"];
                if(parameterBranchspec.Equals("true")) {
                    Console.WriteLine("No branchspec specified (-help for more info)");
                    return;
                }
                else {
                    branchspec = parameterBranchspec;
                }
            }
            if(parameterParser.Arguments.ContainsKey("u")) {
                if(String.IsNullOrEmpty(branchspec)) {
                    Console.WriteLine("No branchspec integration is specified (-help for more info)");
                    return;
                }
                unshelve = true;
            }
            if (parameterParser.Arguments.ContainsKey("r")) {
                if (!buildAndIntegrate) {
                    Console.WriteLine("-b isn't specified. (-help for more info)");
                    return;
                }
                createRpfFiles = true;
            }

            Console.WriteLine("call remote service...");

            Uri uri = new Uri(ConfigurationManager.AppSettings["serviceProtocol"] + "://" + hostname + ":" + ConfigurationManager.AppSettings["servicePort"]);
            //Kerberos authentication will fail with a dummy spn, but ntlm will be used as fallback
            EndpointIdentity dummyIdentity = EndpointIdentity.CreateSpnIdentity("This is a dummy"/*"/host/"+hostname+":"+ConfigurationManager.AppSettings["servicePort"]*/);

            NetTcpBinding binding = new NetTcpBinding();

            ConsoleCallbackClient callback = new ConsoleCallbackClient();

            BuilderClient client = new BuilderClient(callback, binding, new EndpointAddress(uri, dummyIdentity));
            try {
                IBuilder builderProxy = client.ChannelFactory.CreateChannel();
                ((IContextChannel)builderProxy).OperationTimeout = TimeSpan.FromHours(15);
                if(buildAndIntegrate) {
                    BuildParameter parameter = new BuildParameter();
                    parameter.Username = Environment.UserName;
                    parameter.CreateRpfFiles = createRpfFiles;
                    if(!builderProxy.InvokeBuild(parameter)) {
                        Console.WriteLine("Build is currently running");
                        return;
                    }
                }
                if(!branchspec.Equals("")) {
                    IntegrationParameter parameters = new IntegrationParameter();
                    parameters.IntegrationBranchSpec = branchspec;
                    parameters.Unshelve = unshelve;
                    IntegrationResult result = builderProxy.InvokeIntegration(parameters);
                    if(String.IsNullOrEmpty(result.ChangeListNumber)) {
                        Console.WriteLine("No change list number received");
                    }
                    else {
                        Console.WriteLine("Integration of "+branchspec+": Change list "+result.ChangeListNumber);
                    }
                    if(result.AlreadyRunning) {
                        Console.WriteLine("Integration is currently running");
                        return;
                    }
                    if(!result.ErrorMessage.Equals("")) {
                        Console.WriteLine("An error occurred: "+result.ErrorMessage);
                        return;
                    }
                    if(unshelve) {
                        Builder.callCmdCommand("p4 -s "+result.ChangeListNumber);
                        Console.WriteLine("Changelist "+result.ChangeListNumber+" unshelved");
                    }
                }

            }
            catch(Exception ex) {
                Console.WriteLine("Call failed: " + ex.Message);
                if(ex.InnerException!=null) Console.WriteLine("Details: " + ex.InnerException.Message);
                client.Abort();
            }

            try {
                client.Close();
            }
            catch(TimeoutException timeout) {
                // Handle the timeout exception
                client.Abort();
            }
            catch(CommunicationException communicationException) {
                // Handle the communication exception
                client.Abort();
            }

        }

    }
}
