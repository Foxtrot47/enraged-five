﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRemoteBuildClient
{
    class ConsoleCallbackClient: RemoteBuildService.IConsoleCallback
    {
        public void ReceiveConsoleMessage(string message)
        {
            Console.WriteLine(message);
        }

        public void ProcessFinished()
        {
            Console.WriteLine("Process finished");
        }
    }
}
