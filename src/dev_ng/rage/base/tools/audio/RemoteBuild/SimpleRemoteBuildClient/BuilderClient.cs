﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Channels;
using System.ServiceModel;

namespace SimpleRemoteBuildClient
{
    class BuilderClient: DuplexClientBase<RemoteBuildService.IBuilder>
    {
        public BuilderClient(object callbackInstance, Binding binding, EndpointAddress remoteAddress) : base(callbackInstance, binding, remoteAddress) { }
    }
}
