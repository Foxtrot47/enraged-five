﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteBuildService;
using System.ServiceModel;
using System.Configuration;


namespace RemoteBuildServiceCmd
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri uri = new Uri(ConfigurationManager.AppSettings["serviceAddr"]);
            NetTcpBinding binding = new NetTcpBinding();
            using (ServiceHost host = new ServiceHost(typeof(Builder), uri))
            {
                host.AddServiceEndpoint(typeof(IBuilder), binding, "");
                host.Open();
                Console.WriteLine("Service started at "+uri);

                Console.WriteLine("Press any key to terminate the build service");
                Console.WriteLine();
                Console.ReadKey();
            }
        }
    }
}
