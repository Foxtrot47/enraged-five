using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using rage.Fields;
using rage.ToolLib;
using rage.ToolLib.Logging;
using rage.ToolLib.Writer;
using rage.Transform;
using rage.Types;

namespace rage.Compiler
{
    using rage.Enums;

    public class ObjectCompiler : IObjectCompiler
    {
        private const uint DefaultTriStateFlagValue = 0xAAAAAAAA;
        private const string TOO_MANY_PARENT_OVERRIDE_FLAGS = "Too many parent override flags; need to increase size of parent override flag field.";
        private const string TOO_MANY_FLAGS = "Too many flags, need to increase size of flag field.";
        private const string OVERRIDE_PARENT = "overrideParent";

        private const string MISSING_NAME_ATTRIBUTE = "Object missing \"name\" attribute.";
        private const string OBJECT_ALREADY_EXISTS = "Object with this name already exists! Ignoring this instance...";

        private const string FORMAT_TYPE_DEFINITION_NOT_FOUND =
            "Could not find TypeDefinition: {0}. Skipping compilation of object.";

        private const string SERIALIZATION_FAILED = "Serialization failed!";
        private const string FORMAT_COMPILING = "Compiling: {0}.";
        private const string NAME = "name";

        private readonly bool m_isBigEndian;
        private readonly CompilationMode m_compilationMode;
        private readonly ICompiledObjectLookup m_compiledObjectLookup;
        private readonly ICompositeFieldInstanceAggregator m_compositeFieldInstanceAggregator;
        private readonly IDictionary<string, IFieldProcessor> m_fieldProcessors;
        private readonly Func<string, ITypeDefinition> m_findTypeDefinitionFunc;
        private readonly ILog m_log;
        private readonly IObjectSizeTracker m_objectSizeTracker;
        private readonly ITransformAndValidatorProcessor m_transformAndValidatorProcessor;
        private readonly EnumRuntimeMode runtimeMode;

        public ObjectCompiler(ILog log,
                              IObjectSizeTracker objectSizeTracker,
                              ITransformAndValidatorProcessor transformAndValidatorProcessor,
                              ICompositeFieldInstanceAggregator compositeFieldInstanceAggregator,
                              ICompiledObjectLookup compiledObjectLookup,
                              Func<string, ITypeDefinition> findTypeDefinitionFunc,
                              CompilationMode compilationMode,
                              bool isBigEndian,
                              EnumRuntimeMode runtimeMode)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (objectSizeTracker == null)
            {
                throw new ArgumentNullException("objectSizeTracker");
            }
            if (transformAndValidatorProcessor == null)
            {
                throw new ArgumentNullException("transformAndValidatorProcessor");
            }
            if (compositeFieldInstanceAggregator == null)
            {
                throw new ArgumentNullException("compositeFieldInstanceAggregator");
            }
            if (compiledObjectLookup == null)
            {
                throw new ArgumentNullException("compiledObjectLookup");
            }
            if (findTypeDefinitionFunc == null)
            {
                throw new ArgumentNullException("findTypeDefinitionFunc");
            }
            m_log = log;
            m_objectSizeTracker = objectSizeTracker;
            m_findTypeDefinitionFunc = findTypeDefinitionFunc;
            m_compilationMode = compilationMode;
            m_isBigEndian = isBigEndian;
            m_transformAndValidatorProcessor = transformAndValidatorProcessor;
            m_compositeFieldInstanceAggregator = compositeFieldInstanceAggregator;
            m_compiledObjectLookup = compiledObjectLookup;
            m_fieldProcessors = new Dictionary<string, IFieldProcessor>();
            this.runtimeMode = runtimeMode;
        }

        #region IObjectCompiler Members

        public bool CompileObjects(XDocument document, IWriter output, bool recordCompiledObject, bool shouldTransform)
        {
            try
            {
                m_compositeFieldInstanceAggregator.Process(document);

                if (!m_transformAndValidatorProcessor.Process(document, shouldTransform))
                {
                    return false;
                }

                if (document.Root == null ||
                    !document.Root.Elements().Any())
                {
                    return true;
                }

                if (m_compilationMode != CompilationMode.CompileOnly &&
                    m_compilationMode != CompilationMode.TransformAndCompile)
                {
                    return true;
                }

                if (document.Root.Elements().Any(element => !CompileObject(element, output, recordCompiledObject)))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Exception(ex);
            }
            return false;
        }

        #endregion

        private bool CompileObject(XElement element, IWriter output, bool recordCompiledObject)
        {
            var nameAttrib = element.Attribute(NAME);
            if (nameAttrib == null ||
                string.IsNullOrEmpty(nameAttrib.Value))
            {
                m_log.Error(MISSING_NAME_ATTRIBUTE);
                return Failed();
            }
            var name = nameAttrib.Value;

            m_log.PushContext(ContextType.Object, name);
            if (m_compiledObjectLookup.Exists(name))
            {
                m_log.Warning(OBJECT_ALREADY_EXISTS);
            }

            m_log.Information(FORMAT_COMPILING, element.Name);
            var typeDef = m_findTypeDefinitionFunc(element.Name.ToString());
            if (typeDef == null)
            {
                m_log.Warning(FORMAT_TYPE_DEFINITION_NOT_FOUND, element.Name);
                return Succeeded();
            }

            Utility.AlignToBoundary(output, typeDef.Alignment);
            Utility.AlignToBoundary(output, typeDef.LargestAlignment);

            var pos = output.Tell();
            if (!Compile(element, typeDef, output))
            {
                return Failed();
            }

            Utility.AlignToBoundary(output, typeDef.LargestAlignment);

            if (recordCompiledObject)
            {
                RecordCompiledObject(name, pos - Utility.HEADER_OFFSET, output.Tell() - pos);
            }
            return Succeeded();
        }

        private bool Succeeded()
        {
            m_log.PopContext();
            return true;
        }

        private bool Compile(XElement element, ITypeDefinition typeDef, IWriter output)
        {
            if (typeDef.RequiresHeader)
            {
                output.Write(typeDef.Id);

                // Don't want to include name table if in release mode and DevOnlyNameTable flag is set
                if (this.runtimeMode == EnumRuntimeMode.Dev || 
                    (this.runtimeMode == EnumRuntimeMode.Release && !typeDef.Loader.DevOnlyNameTable))
                {
                    m_compiledObjectLookup.WriteNextNameOffset(output);
                }
            }

            var compression = new CompressionBitField(output, sizeof(uint) * 8, m_isBigEndian);

            if (typeDef.IsBaseTypeCompressed && typeDef.AdditionalCompression)
            {
                compression.Init();
            }

            var wasSerialized = false;
            if (typeDef.RequiresFlags)
            {
                if (!GenerateAndWriteTriStateFlags(element, typeDef, output, out wasSerialized))
                {
                    return false;
                }
            }

            if (typeDef.AdditionalCompression)
            {
                compression.SetNextBit(wasSerialized);
            }

            if (typeDef.AdditionalCompression)
            {
                this.WriteOverrideFlagsWithAddedCompression(GenerateOverrideFlags(element, typeDef), output, typeDef.IsBaseTypeCompressed && !typeDef.ForceSerialization, out wasSerialized);
                compression.SetNextBit(wasSerialized);
            }
            else
            {
                this.WriteOverrideFlags(GenerateOverrideFlags(element, typeDef), output);
            }

            if (typeDef.IsBaseTypeCompressed && !typeDef.AdditionalCompression)
            {
                compression.Init();
            }

            IFieldProcessor fieldProcessor;
            if (!m_fieldProcessors.TryGetValue(typeDef.Name, out fieldProcessor))
            {
                fieldProcessor = new FieldProcessor(m_log, typeDef);
                m_fieldProcessors.Add(typeDef.Name, fieldProcessor);
            }

            if (!fieldProcessor.Process(element, output, compression))
            {
                m_log.Error(SERIALIZATION_FAILED);
                return false;
            }

            if (typeDef.IsBaseTypeCompressed)
            {
                compression.WriteOutput();
            }

            return true;
        }

        private bool GenerateAndWriteTriStateFlags(XContainer element, ITypeDefinition typeDef, IWriter output, out bool wasSerialized)
        {
            // ensure unused flags are set to 'unspecified' rather than 'no' (0)
            uint flags = DefaultTriStateFlagValue;
            var flagIndex = 0;
            foreach (var fieldDef in typeDef.AllFieldsInherited)
            {
                if (fieldDef.Type.TypeHandle.Equals(typeof(TriState).TypeHandle))
                {
                    var triState = TriStateConversion.ToTriState(element.Element(fieldDef.Name),
                                                                 fieldDef as IBasicFieldDefinition);
                    flags = TriStateConversion.SetTriStateValue(flags, flagIndex, triState);
                    flagIndex++;
                    if (flagIndex > 16)
                    {
                        this.m_log.Error(TOO_MANY_FLAGS);
                        wasSerialized = false;
                        return false;
                    }
                }
            }       
         
            // Only output if non-default value (and additional compression is enabled)
            var skipOutput = !typeDef.ForceSerialization && typeDef.AdditionalCompression && typeDef.IsBaseTypeCompressed && flags == DefaultTriStateFlagValue;
            if (!skipOutput)
            {
                output.Write(flags);
            }

            wasSerialized = !skipOutput;

            return true;
        }

        private static IList<bool> GenerateOverrideFlags(XContainer element, IFieldContainer typeDef)
        {
            var overrideFlags = new List<bool>();
            foreach (var fieldDef in typeDef.AllFieldsInherited)
            {
                if (!fieldDef.Ignore && fieldDef.AllowOverrideControl &&
                    !fieldDef.Type.TypeHandle.Equals(typeof(TriState).TypeHandle))
                {
                    var field = element.Element(fieldDef.Name);
                    if (field != null)
                    {
                        var overrideParentAttrib = field.Attribute(OVERRIDE_PARENT);
                        overrideFlags.Add(overrideParentAttrib != null && Utility.ToBoolean(overrideParentAttrib.Value));
                    }
                    else
                    {
                        overrideFlags.Add(false);
                    }
                }
            }
            return overrideFlags;
        }

        private void WriteOverrideFlags(IList<bool> overrideFlags, IWriter output)
        {
            var count = overrideFlags.Count;
            if (count > 0)
            {
                if (output.IsBigEndian)
                {
                    // This is accessed with a C++ bit field and the bit packing order for bitfields is reversed on BE architectures
                    byte curByte = 0;
                    var bitPos = 0;
                    for (var i = 0; i < overrideFlags.Count; i++)
                    {
                        bitPos = i % 8;

                        var overrideParent = overrideFlags[i] ? (byte)1 : (byte)0;
                        curByte |= (byte)(overrideParent << (7 - bitPos));

                        if (bitPos == 7)
                        {
                            output.Write(curByte);
                            curByte = 0;
                        }
                    }
                    if (bitPos != 7)
                    {
                        output.Write(curByte);
                    }
                }
                else
                {
                    var bits = new BitField(count);
                    for (var i = 0; i < count; ++i)
                    {
                        bits.Set(i, overrideFlags[i]);
                    }
                    output.Write(bits.Field, false);
                }
            }
        }

        private void WriteOverrideFlagsWithAddedCompression(IList<bool> overrideFlags, IWriter output, bool shouldCompress, out bool wasSerialized)
        {
            if (shouldCompress)
            {
                wasSerialized = false;
                foreach (var b in overrideFlags)
                {
                    if (b)
                    {
                        wasSerialized = true;
                    }
                }
            }
            else
            {
                wasSerialized = true;
            }

            var count = overrideFlags.Count;
            if (count > 0 && wasSerialized)
            {
                if (output.IsBigEndian)
                {
                    // This is accessed with a C++ bit field and the bit packing order for bitfields is reversed on BE architectures
                    byte curByte = 0;
                    var bitPos = 0;
                    for (var i = 0; i < overrideFlags.Count; i++)
                    {
                        bitPos = i % 8;

                        var overrideParent = overrideFlags[i] ? (byte)1 : (byte)0;
                        curByte |= (byte)(overrideParent << (7 - bitPos));

                        if (bitPos == 7)
                        {
                            output.Write(curByte);
                            curByte = 0;
                        }
                    }
                    if (bitPos != 7)
                    {
                        output.Write(curByte);
                    }
                }
                else
                {
                    var bits = new BitField(count);
                    for (var i = 0; i < count; ++i)
                    {
                        bits.Set(i, overrideFlags[i]);
                    }
                    output.Write(bits.Field, false);
                }
            }
        }
        
        private void RecordCompiledObject(string name, uint offset, uint size)
        {
            var compiledObject = new CompiledObject {Name = name, Offset = offset, Size = size};
            m_compiledObjectLookup.Record(compiledObject);
            m_objectSizeTracker.Record(compiledObject.Name, compiledObject.Size);
        }

        private bool Failed()
        {
            m_log.PopContext();
            return false;
        }
    }
}
