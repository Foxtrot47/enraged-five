using System.Collections.Generic;

namespace rage.Enums
{
    public interface IEnumDefinition : IDefinition
    {
        byte InitialValue { get; }

        bool BitsetStyle { get; }

        IDictionary<string, IEnumValue> Values { get; }
    }
}