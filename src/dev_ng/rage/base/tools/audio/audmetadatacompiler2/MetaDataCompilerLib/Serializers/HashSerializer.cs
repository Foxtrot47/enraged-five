using System.CodeDom.Compiler;
using System.IO;
using System.Xml.Linq;
using rage.Fields;
using rage.Serialization;
using rage.ToolLib;
using rage.ToolLib.Writer;

namespace rage.Serializers
{
    using rage.ToolLib.Logging;

    [CanSerialize(typeof (Hash))]
    public class HashSerializer : ISerializer
    {
        #region ISerializer Members

        public uint Alignment
        {
            get { return 4; }
        }

        public int Compare(XElement x, XElement y)
        {
            var hashX = new Hash { Value = x.Value };
            var hashY = new Hash { Value = y.Value };
            return hashX.Key.CompareTo(hashY.Key);
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            var hash = new Hash {Value = element.Value};
            output.Write(hash.Key);
            return true;
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            var hash = new Hash {Value = value};
            writer.Write(Utility.EncloseInBrackets(string.Concat(hash.Key, "U")));
            return hash.Key == 0 ? null : value;
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            writer.Write("rage::u32");
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            // do nothing
        }

        #endregion
    }
}