namespace rage.Fields
{
    public interface IBasicFieldDefinition : ICommonFieldDefinition
    {
        bool IsPacked { get; set; }

        string Min { get; }

        string Max { get; }

        string Default { get; }

        uint Length { get; }

        bool RequiresValue { get; }
    }
}