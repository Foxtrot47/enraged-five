using System;
using System.CodeDom.Compiler;
using rage.Types;

namespace rage.CodeGeneration
{
    public class SoundClassFactoryWriter
    {
        private readonly string m_classFactoryFunctionName;

        private readonly string m_classFactoryReturnType;

        private readonly ITypeDefinitionsManager m_typeDefinitionsManager;

        private readonly bool m_usesBucketAllocator;

        private readonly IndentedTextWriter m_writer;

        public SoundClassFactoryWriter(IndentedTextWriter writer,
                                       ITypeDefinitionsManager typeDefinitionsManager,
                                       string classFactoryFunctionName,
                                       string classFactoryReturnType,
                                       bool usesBucketAllocator)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (typeDefinitionsManager == null)
            {
                throw new ArgumentNullException("typeDefinitionsManager");
            }
            if (string.IsNullOrEmpty(classFactoryFunctionName))
            {
                throw new ArgumentNullException("classFactoryFunctionName");
            }
            if (string.IsNullOrEmpty(classFactoryReturnType))
            {
                throw new ArgumentNullException("classFactoryReturnType");
            }
            m_writer = writer;
            m_typeDefinitionsManager = typeDefinitionsManager;
            m_classFactoryFunctionName = classFactoryFunctionName;
            m_classFactoryReturnType = classFactoryReturnType;
            m_usesBucketAllocator = usesBucketAllocator;

            Write();
        }

        private void Write()
        {
            var functionDeclaration = string.Concat(m_classFactoryReturnType,
                                                    " ",
                                                    m_classFactoryFunctionName,
                                                    m_usesBucketAllocator ? "(rage::u32 bucketId, " : "(",
                                                    "rage::u32 classId)");
            using (new BlockWriter(m_writer, functionDeclaration, null, false))
            {
                m_writer.WriteLine(string.Concat(m_classFactoryReturnType, " obj = NULL;"));
                using (new BlockWriter(m_writer, "switch(classId)", null, false))
                {
                    foreach (var typeDefData in m_typeDefinitionsManager.TypeDefinitionsData)
                    {
                        foreach (var typeDef in typeDefData.TypeDefinitions)
                        {
                            if (typeDef.IsAbstract ||
                                !typeDef.IsFactoryCodeGenerated)
                            {
                                continue;
                            }
                            WriteSoundClassFactoryCase(typeDefData, typeDef);
                        }
                    }
                }
                m_writer.WriteLine("return obj;");
            }
        }

        private void WriteSoundClassFactoryCase(TypeDefData typeDefData, ITypeDefinition typeDef)
        {
            var caseString = string.Concat("case ", typeDef.Name, "::TYPE_ID :");
            using (new BlockWriter(m_writer, caseString, null, false))
            {
                if (m_usesBucketAllocator)
                {
                    var allocateSoundSlot = string.Concat("audSound::GetStaticPool().AllocateSoundSlot(sizeof(",
                                                          typeDefData.ClassPrefix,
                                                          typeDef.Name,
                                                          "), bucketId);");
                    m_writer.WriteLine(string.Concat("obj = (", m_classFactoryReturnType, ")", allocateSoundSlot));
                    using (new BlockWriter(m_writer, "if (NULL != obj)", null, false))
                    {
                        m_writer.WriteLine(string.Concat("::new(obj) ", typeDefData.ClassPrefix, typeDef.Name, "();"));
                    }
                }
                else
                {
                    m_writer.WriteLine(string.Concat("obj = rage_new ", typeDefData.ClassPrefix, typeDef.Name, "();"));
                }
                m_writer.WriteLine("break;");
            }
        }
    }
}