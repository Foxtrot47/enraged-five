namespace rage
{
    public interface IElementTypeProvider
    {
        string GetElementType();
    }
}