using rage.Types;

namespace rage.CodeGeneration
{
    public sealed class MetaDataCppFileGenerator : FileGenerator
    {
        public MetaDataCppFileGenerator(string fileName,
                                        string nameSpace,
                                        string metaDataType,
                                        ITypeDefinitionsManager typeDefinitionsManager)
            : base(
                fileName,
                nameSpace,
                metaDataType,
                typeDefinitionsManager,
                "Automatically generated metadata structure functions")
        {
            Generate();
        }

        protected override void Generate()
        {
            base.Generate();

            WriteIncludes();
            using (new BlockWriter(Writer, NAMESPACE, NameSpace, false))
            {
                new CppTypeDefinitionsWriter(Writer, TypeDefinitionsManager.AllTypeDefinitions);
                WriteEnumParsingMethods();
                new HelperMethodWriter(Writer, TypeDefinitionsManager, MetadataType);
            }
        }

        private void WriteIncludes()
        {
            var includes = new[] {FileName.Replace(".cpp", ".h"), "string/stringhash.h"};
            new IncludesWriter(Writer, includes);
            WriteBlankLine();
        }

        private void WriteEnumParsingMethods()
        {
            new BlockCommentWriter(Writer, "Enumeration Conversion");
            WriteBlankLine();

            foreach (var enumDef in TypeDefinitionsManager.EnumDefinitions)
            {
                new EnumParsingWriter(Writer, enumDef);
                WriteBlankLine();
            }
        }
    }
}