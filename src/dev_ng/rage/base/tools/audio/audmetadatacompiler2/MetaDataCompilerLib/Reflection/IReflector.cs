using System;
using System.Collections.Generic;
using rage.Generator;
using rage.Serialization;
using rage.Transform;

namespace rage.Reflection
{
    public interface IReflector
    {
        /// <summary>
        ///   Attempts to create an ITransformer instance of type <paramref name = "transformerType" />
        /// </summary>
        /// <param name = "transformerType">The type of ITransformer</param>
        /// <returns>An instance if successful, null otherwise</returns>
        ITransformer CreateTransformer(Type transformerType);

        /// <summary>
        ///   Attempts to create an ISerializer instance of type <paramref name = "serializerType" />
        /// </summary>
        /// <param name = "serializerType">The type of ISerializer</param>
        /// <returns>An instance if successful, null otherwise</returns>
        ISerializer CreateSerializer(Type serializerType);

        /// <summary>
        ///   Attempts to create an IValidator instances of type <paramref name = "validatorType" />
        /// </summary>
        /// <param name = "validatorType">The type of IValidator</param>
        /// <returns>An instance if successful, null otherwise</returns>
        IValidator CreateValidator(Type validatorType);

        /// <summary>
        ///   Attempts to create an IGenerator instance of type <paramref name = "generatorType" />
        /// </summary>
        /// <param name = "generatorType">The type of IGenerator</param>
        /// <returns>An instance if successful, null otherwise</returns>
        IGenerator CreateGenerator(Type generatorType);

        /// <summary>
        ///   Checks if an ISerializer of type <paramref name = "serializerType" />
        ///   can serialize an element of type <paramref name = "targetType" />
        /// </summary>
        /// <param name = "targetType">The Xml element type</param>
        /// <param name = "serializerType">The type of the ISerializer</param>
        /// <returns></returns>
        bool CanBeSerializedBy(Type targetType, Type serializerType);

        /// <summary>
        ///   Checks if an ITransformer of type <paramref name = "transformerType" /> 
        ///   can transform an element of type <paramref name = "elementType" />
        /// </summary>
        /// <param name = "elementType">The source Xml element type</param>
        /// <param name = "transformerType">The transformer type</param>
        /// <returns>true if the transform is possible, false otherwise</returns>
        bool CanBeTransformedBy(string elementType, Type transformerType);

        /// <summary>
        ///   Checks if an IValidator of type <paramref name = "validatorType" />
        ///   can validate an element of type <paramref name = "elementType" />
        /// </summary>
        /// <param name = "elementType">The Xml element type</param>
        /// <param name = "validatorType">The type of the IValidator</param>
        /// <returns></returns>
        bool CanBeValidatedBy(string elementType, Type validatorType);

        /// <summary>
        ///   Get the reflected types. Only types implementing one of the 
        ///   following interfaces are being reflected: 
        ///   IGenerator, ISerializer, IValidator, and ITransformer
        /// </summary>
        /// <returns></returns>
        List<Type> getReflectedTypes();
    }
}