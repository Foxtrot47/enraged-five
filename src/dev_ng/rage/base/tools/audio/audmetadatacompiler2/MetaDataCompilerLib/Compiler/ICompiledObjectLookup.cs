using System.IO;
using rage.ToolLib.Writer;

namespace rage.Compiler
{
    using System.Collections.Generic;

    public interface ICompiledObjectLookup
    {
        IList<CompiledObject> CompiledObjects { get; }

        void RecordObjectRef(uint position);

        void RecordStringRef(uint position);

        void Serialize(IWriter output, uint mode = 0);

        void Deserialize(Stream data, bool isBigEndian);

        void WriteNextNameOffset(IWriter output);

        void Record(CompiledObject compiledObject);

        bool Exists(string name);

        uint? GetObjectSize(string name);

        string GetNameFromOffset(uint offset);

        void Clear();

        void UpdateObjectSize(string name, uint offset, uint size);
    }
}