namespace rage.Fields
{
    public interface IAllocatedSpaceFieldDefinition : ICommonFieldDefinition
    {
        bool IsPacked { get; set; }

        uint Size { get; }

        byte Default { get; }

        string InitialValue { get; }
    }
}