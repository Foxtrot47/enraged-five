using System;
using rage.ToolLib;

namespace rage.Fields
{
    public interface ICompositeFieldDefinition : ICommonFieldDefinition,
                                                 IFieldContainer
    {
        bool NativeType { get; }

        bool IsFixedSize { get; }

        uint MaxOccurs { get; }

        string OrderBy { get; }

        Type NumType { get; }

        BitField BitField { get; }
    }
}