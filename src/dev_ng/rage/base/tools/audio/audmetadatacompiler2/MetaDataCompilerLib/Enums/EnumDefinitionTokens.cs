namespace rage.Enums
{
    // The members of this enum are used while parsing the TypeDef files
    // they are case sensitive and must match the case used in the Xml
    public enum EnumDefinitionTokens
    {
        Name,
        InitialValue,
        Bitset,
        Value,
        Display,
        Yes,
        No,
    }
}