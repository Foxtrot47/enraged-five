using System.CodeDom.Compiler;
using System.IO;
using System.Xml.Linq;
using rage.Fields;
using rage.Serialization;
using rage.ToolLib.Writer;

namespace rage.Serializers
{
    using rage.ToolLib.Logging;

    [CanSerialize(typeof (ushort))]
    public class UShortSerializer : ISerializer
    {
        #region ISerializer Members

        public uint Alignment
        {
            get { return 2; }
        }

        public int Compare(XElement x, XElement y)
        {
            ushort valueX, valueY;
            if (ushort.TryParse(x.Value, out valueX) && ushort.TryParse(y.Value, out valueY))
            {
                if (valueX > valueY) return 1;
                if (valueX < valueY) return -1;
            }
            return 0;
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            ushort value;
            if (ushort.TryParse(element.Value, out value))
            {
                output.Write(value);
                return true;
            }
            return false;
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            writer.Write(Utility.EncloseInBrackets(value));
            return null;
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            writer.Write("rage::u16");
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            // do nothing
        }

        #endregion
    }
}