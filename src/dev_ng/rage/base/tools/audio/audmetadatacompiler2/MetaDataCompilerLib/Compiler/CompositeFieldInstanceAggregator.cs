using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using rage.Fields;
using rage.Types;

namespace rage.Compiler
{
    public class CompositeFieldInstanceAggregator : ICompositeFieldInstanceAggregator
    {
        private readonly Func<string, ITypeDefinition> m_typeDefinitionFinderFunc;

        public CompositeFieldInstanceAggregator(Func<string, ITypeDefinition> typeDefinitionFinderFunc)
        {
            if (typeDefinitionFinderFunc == null)
            {
                throw new ArgumentNullException("typeDefinitionFinderFunc");
            }
            m_typeDefinitionFinderFunc = typeDefinitionFinderFunc;
        }

        #region ICompositeFieldInstanceAggregator Members

        public void Process(XDocument doc)
        {
            if (doc.Root == null || !doc.Root.Elements().Any())
            {
                return;
            }

            foreach (var element in doc.Root.Elements())
            {
                // This is a type definition so find the type def
                var typeDef = m_typeDefinitionFinderFunc(element.Name.ToString());
                if (typeDef == null)
                {
                    continue;
                }

                Process(element, typeDef, null);
            }
        }

        #endregion

        private static void Process(
            XContainer elementContainer,
            IFieldContainer fieldContainer,
            IFieldContainer parentFieldContainer)
        {
            var lookup = new Dictionary<string, List<XElement>>();

            // Find composite elements requiring conversion to <Instance> elements
            foreach (var element in elementContainer.Elements())
            {
                var elementName = element.Name.ToString();

                var isOldFormat = IsOldFormat(element);
                if (!isOldFormat)
                {
                    continue;
                }

                IFieldContainer foundInContainer;
                var isCompositeField = IsCompositeField(
                    elementName, fieldContainer, parentFieldContainer, out foundInContainer);

                if (!isCompositeField)
                {
                    continue;
                }

                List<XElement> lookupElements;
                if (!lookup.TryGetValue(elementName, out lookupElements))
                {
                    lookupElements = new List<XElement>();
                    lookup.Add(elementName, lookupElements);
                }

                lookupElements.Add(element);
            }

            // Create <Instance> elements for required composite types
            foreach (var entry in lookup)
            {
                var aggregatedCompositeFieldElement = new XElement(entry.Key);
                foreach (var element in entry.Value)
                {
                    var instanceElement = new XElement(FieldDefinitionTokens.Instance.ToString());
                    if (element.HasElements)
                    {
                        foreach (var childElement in element.Elements())
                        {
                            instanceElement.Add(childElement);
                        }
                    }
                    else
                    {
                        instanceElement.Value = element.Value;
                    }

                    aggregatedCompositeFieldElement.Add(instanceElement);
                }

                elementContainer.Add(aggregatedCompositeFieldElement);
                entry.Value.Remove();
            }

            var elementsToProcess = new List<XElement>();

            foreach (var element in elementContainer.Elements())
            {
                var containsInstances = false;

                foreach (var childElement in element.Elements())
                {
                    // We want to process all child <Instance> elements, if they exist
                    // And just ignore the new parent container for these <Instance> elements
                    if (childElement.Name.LocalName.Equals(FieldDefinitionTokens.Instance.ToString()))
                    {
                        elementsToProcess.Add(childElement);
                        containsInstances = true;
                    }
                }

                if (!containsInstances)
                {
                    // No child <Instance> elements, so process this element
                    elementsToProcess.Add(element);
                }
            }

            foreach (var element in elementsToProcess)
            {
                var elementName = element.Name.ToString();

                // If we are processing an <Instance> element, we want the
                // name of it's parent (i.e. the original name of the instance)
                if (elementName.Equals(FieldDefinitionTokens.Instance.ToString()))
                {
                    elementName = element.Parent.Name.ToString();
                }

                IFieldContainer foundInContainer;

                if (IsOldFormat(element)
                    && IsCompositeField(elementName, fieldContainer, parentFieldContainer, out foundInContainer))
                {
                    var typeDef = fieldContainer.CompositeFieldsInheritedLookup[elementName];
                    Process(element, typeDef, foundInContainer);
                }
            }
        }

        private static bool IsCompositeField(string elementName,
                                             IFieldContainer fieldContainer,
                                             IFieldContainer parentFieldContainer,
                                             out IFieldContainer foundInContainer)
        {
            foundInContainer = null;
            if (fieldContainer != parentFieldContainer)
            {
                if (fieldContainer.CompositeFieldsInheritedLookup.ContainsKey(elementName))
                {
                    foundInContainer = fieldContainer;
                    return true;
                }
            }

            foreach (var childFieldContainer in fieldContainer.CompositeFields)
            {
                if (IsCompositeField(elementName, childFieldContainer, fieldContainer, out foundInContainer))
                {
                    return true;
                }
            }
            return false;
        }

        private static bool IsOldFormat(XContainer element)
        {
            return element.Elements(FieldDefinitionTokens.Instance.ToString()).Count() == 0;
        }
    }
}