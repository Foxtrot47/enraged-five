using System.Xml.Linq;

namespace rage.Transform
{
    public class TransformResult
    {
        public XElement Result { get; set; }
    }
}