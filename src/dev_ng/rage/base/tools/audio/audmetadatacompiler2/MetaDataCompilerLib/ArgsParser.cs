using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace rage
{
    public class ArgsParser : IArgsParser
    {
        private const string ARGS_EXTRACTOR_REGEX = "\\$.+?\\$";

        private const string ARGS_MATCHER_REGEX = "Args=\"(" + ARGS_EXTRACTOR_REGEX + "){1,}\"";

        private static readonly Regex ms_argsMatcher;

        private static readonly Regex ms_argsExtractor;

        static ArgsParser()
        {
            ms_argsMatcher = new Regex(ARGS_MATCHER_REGEX);
            ms_argsExtractor = new Regex(ARGS_EXTRACTOR_REGEX);
        }

        #region IArgsParser Members

        public string[] GetArgs(string value)
        {
            var args = new List<string>();
            foreach (var match in ms_argsMatcher.Matches(value))
            {
                var matchStr = match.ToString();
                foreach (var arg in ms_argsExtractor.Matches(matchStr))
                {
                    args.Add(GetArg(arg.ToString()));
                }
            }
            return args.ToArray();
        }

        #endregion

        private static string GetArg(string argStr)
        {
            return argStr.Replace("$", string.Empty);
        }
    }
}