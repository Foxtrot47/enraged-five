using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using rage.Generator;
using rage.Reflection;
using rage.Templates;
using rage.ToolLib.Encryption;
using rage.ToolLib.Logging;
using rage.ToolLib.Writer;
using rage.Transform;
using rage.Types;

namespace rage.Compiler
{
    using System.IO;

    using rage.Enums;

    using BinaryWriter = rage.ToolLib.Writer.BinaryWriter;

    public class MetaDataCompiler : IMetaDataCompiler
    {
        private const string WRITING_STRING_TABLE = "\nWriting String Table...";
        private const string WRITING_OBJECT_LOOKUP = "Writing object lookup...\n";
        private const string OBJECT_NOT_FOUND = "Object not found";

        private readonly CompilationMode m_compilationMode;
        private readonly ICompiledObjectLookup m_compiledObjectLookup;
        private readonly ILog m_log;
        private readonly IObjectSizeTracker m_objectSizeTracker;
        private readonly audProjectSettings m_projectSettings;
        private readonly IReflector m_reflector;
        private readonly IStringTable m_stringTable;
        private readonly string m_workingDirectory;
        private readonly string m_workingPath;

        private bool m_compressionDisabled;
        private ITemplateManager m_templateManager;
        private ITransformAndValidatorProcessor m_transformAndValidatorProcessor;

        public MetaDataCompiler(ILog log,
                                string workingPath,
                                string workingDirectory,
                                audProjectSettings projectSettings,
                                IReflector reflector,
                                IObjectSizeTracker objectSizeTracker,
                                IStringTable stringTable,
                                ICompiledObjectLookup compiledObjectLookup,
                                CompilationMode compilationMode)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (string.IsNullOrEmpty(workingPath))
            {
                throw new ArgumentNullException("workingPath");
            }
            if (string.IsNullOrEmpty(workingDirectory))
            {
                throw new ArgumentNullException("workingDirectory");
            }
            if (projectSettings == null)
            {
                throw new ArgumentNullException("projectSettings");
            }
            if (reflector == null)
            {
                throw new ArgumentNullException("reflector");
            }
            if (objectSizeTracker == null)
            {
                throw new ArgumentNullException("objectSizeTracker");
            }
            if (stringTable == null)
            {
                throw new ArgumentNullException("stringTable");
            }
            if (compiledObjectLookup == null)
            {
                throw new ArgumentNullException("compiledObjectLookup");
            }
            m_log = log;
            m_workingPath = workingPath;
            m_workingDirectory = workingDirectory;
            m_projectSettings = projectSettings;
            m_reflector = reflector;
            m_objectSizeTracker = objectSizeTracker;
            m_stringTable = stringTable;
            m_compiledObjectLookup = compiledObjectLookup;
            m_compilationMode = compilationMode;
        }

        #region IMetaDataCompiler Members

        public ITypeDefinitionsManager TypeDefinitionsManager { get; private set; }

        public uint Version
        {
            get
            {
                if (this.TypeDefinitionsManager != null)
                {
                    return this.TypeDefinitionsManager.Version;
                }
                return 0;
            }
        }

        public IEnumerable<string> TypeDefinitionPaths
        {
            get
            {
                if (this.TypeDefinitionsManager != null)
                {
                    return this.TypeDefinitionsManager.LoadingPaths;
                }
                return null;
            }
        }

        public event Action<string, string, bool> LoggedError
        {
            add { m_log.ErrorLogged += value; }
            remove { m_log.ErrorLogged -= value; }
        }

        public event Action<string, Exception> LoggedException
        {
            add { m_log.ExceptionLogged += value; }
            remove { m_log.ExceptionLogged -= value; }
        }

        public bool Generate(List<DocumentEntry> documentEntries, audMetadataFile metaDataFile, bool editMode = false, bool instructionsWithOutputOnly = false)
        {
            if (m_templateManager != null)
            {
                if (documentEntries.Any(documentEntry => !m_templateManager.Process(documentEntry.Document)))
                {
                    return false;
                }
            }
            return DoGeneration(documentEntries, metaDataFile, editMode, instructionsWithOutputOnly);
        }

        private bool DoGeneration(List<DocumentEntry> documentEntries, audMetadataFile metaDataFile, bool editMode = false, bool instructionsWithOutputOnly = false)
        {
            if (metaDataFile.GenerateMetadataElements != null)
            {
                var generation = new MetaDataGeneration(m_log, m_workingPath, m_reflector, m_projectSettings, this.TypeDefinitionsManager);
                if (!generation.Run(metaDataFile, documentEntries, editMode, instructionsWithOutputOnly))
                {
                    return false;
                }
            }
            return true;
        }

        public bool Compile(XDocument document, IWriter output, audMetadataFile metaDataFile, EnumRuntimeMode runtimeMode)
        {
            if (output == null)
            {
                throw new ArgumentNullException("output");
            }

            if (metaDataFile == null)
            {
                throw new ArgumentNullException("metadataFile");
            }

            if (this.TypeDefinitionsManager == null)
            {
                throw new InvalidOperationException("Attempting to compile without loading type definitions first.");
            }

            var objectCompiler = CreateObjectCompiler(runtimeMode);
            if (!objectCompiler.CompileObjects(document, output, false, metaDataFile.ShouldTransform))
            {
                return false;
            }
            output.Flush();
            return true;
        }

        public bool Compile(
            List<DocumentEntry> documentEntries,
            IWriter output,
            audMetadataFile metaDataFile,
            uint changeListNumber,
            EnumRuntimeMode runtimeMode,
            string nameTablePath = null,
            bool editMode = false)
        {
            if ((m_compilationMode == CompilationMode.CompileOnly ||
                m_compilationMode == CompilationMode.TransformAndCompile) &&
                output == null)
            {
                throw new ArgumentNullException("output");
            }

            if (metaDataFile == null)
            {
                throw new ArgumentNullException("metadataFile");
            }

            if (this.TypeDefinitionsManager == null)
            {
                throw new InvalidOperationException("Attempting to compile without loading type definitions first.");
            }

            if (m_templateManager != null)
            {
                if (documentEntries.Any(documentEntry => !m_templateManager.Process(documentEntry.Document)))
                {
                    return false;
                }
            }

            var objectCompiler = CreateObjectCompiler(runtimeMode);

            if (m_compilationMode == CompilationMode.CompileOnly ||
                m_compilationMode == CompilationMode.TransformAndCompile)
            {
                if (!DoGeneration(documentEntries, metaDataFile, editMode))
                {
                    return false;
                }

                m_compiledObjectLookup.Clear();
                m_stringTable.Clear();

                output.Write(this.TypeDefinitionsManager.Version);
                using (new ChunkSizeWriter(output))
                {
                    output.Write(changeListNumber);

                    // Compile and serialize objects
                    if (!CompileObjects(documentEntries, objectCompiler, output, metaDataFile.ShouldTransform))
                    {
                        return false;
                    }
                }

                KeysPerPlatform encryptionKeys = null;
                if(TypeDefinitionsManager.EncryptionKeys.ContainsKey(m_projectSettings.GetCurrentPlatform().PlatformTag))
                {
                    encryptionKeys = TypeDefinitionsManager.EncryptionKeys[m_projectSettings.GetCurrentPlatform().PlatformTag];
                }

                // Serialize string table
                m_log.WriteFormatted(WRITING_STRING_TABLE);
                IEncrypter stringTableEncryptor = null;
                if(encryptionKeys != null && encryptionKeys.StringTableKey !=null) stringTableEncryptor = new TEAEncrypter(encryptionKeys.StringTableKey.ToString());
                using (new ChunkSizeWriter(output))
                {
                    m_stringTable.Serialize(output, stringTableEncryptor);
                }

                // Serialize object lookup table
                this.m_log.WriteFormatted(WRITING_OBJECT_LOOKUP);
                this.m_compiledObjectLookup.Serialize(output, this.TypeDefinitionsManager.Mode);

                output.Flush();

                // Generate separate object name table if running in mode 1 or higher
                if (this.TypeDefinitionsManager.Mode > 0)
                {
                    IEncrypter nameTableEncrypter = null;
                    if(encryptionKeys != null && encryptionKeys.NameTableKey !=null) nameTableEncrypter = new TEAEncrypter(encryptionKeys.NameTableKey.ToString());
                    if (!string.IsNullOrEmpty(nameTablePath))
                    {
                        // If a name table path is specified, output the name table to file
                        using (
                            var nameTableOutputFile = new BinaryWriter(
                                File.Create(nameTablePath), this.m_projectSettings.IsBigEndian()))
                        {
                            this.GenerateNameTable(nameTableOutputFile, nameTableEncrypter);
                        }
                    }
                    else
                    {
                        // Otherwise, append the name table to the end of the stream
                        // Need to tell the game how large the nametable is
                        using (new ChunkSizeWriter(output))
                        {
                            this.GenerateNameTable(output, nameTableEncrypter);
                        }
                    }
                }
            }
            else
            {
                // transforming
                if (!CompileObjects(documentEntries, objectCompiler, output))
                {
                    return false;
                }

                foreach (var documentEntry in documentEntries)
                {
                    documentEntry.Document.Save(documentEntry.TransformedXmlPath);
                }
            }
            return true;
        }

        public void GenerateNameTable(IWriter writer, IEncrypter encrypter=null)
        {
            EncryptionWriter encryptionWriter = new EncryptionWriter(encrypter, writer.IsBigEndian, writer.Encoding);
            foreach (var obj in this.m_compiledObjectLookup.CompiledObjects)
            {
                var objName = obj.Name;
                var bytes = System.Text.Encoding.ASCII.GetBytes(objName);

                encryptionWriter.Write(bytes, false);

                // NULL terminate the string
                encryptionWriter.Write((byte)0);
            }
            writer.Write(encryptionWriter.getOutputData(true), false);
        }

        public bool LoadTypeDefinitionsAndTemplates(string templatePath,
                                                    IEnumerable<audObjectDefinition> objectDefinitions)
        {
            if (LoadTypeDefinitions(objectDefinitions))
            {
                if (m_templateManager == null)
                {
                    m_templateManager = CreateTemplateManager();
                }
                return m_templateManager.Load(templatePath);
            }
            return false;
        }

        public bool LoadTypeDefinitions(IEnumerable<audObjectDefinition> objectDefinitions)
        {
            CreateTypeDefinitionsManager();
            if (this.TypeDefinitionsManager.Load(m_workingDirectory, objectDefinitions))
            {
                m_transformAndValidatorProcessor = new TransformAndValidatorProcessor(m_log,
                                                                                      this.TypeDefinitionsManager.
                                                                                          FindTypeDefinition);
                return true;
            }
            return false;
        }

        public bool ReloadTypeDefinitions()
        {
            if (this.TypeDefinitionsManager == null)
            {
                throw new InvalidOperationException("Attempting to reload type definitions without loading them first.");
            }

            var ret = this.TypeDefinitionsManager.Reload();
            if (m_compressionDisabled)
            {
                ForceSerialization();
            }
            return ret;
        }

        public ITemplateManager CreateTemplateManager()
        {
            if (this.TypeDefinitionsManager == null)
            {
                throw new InvalidOperationException(
                    "Attempting to create an instance of the template manager without loading type definitions first.");
            }
            return new TemplateManager(m_log, this.TypeDefinitionsManager);
        }

        public bool LoadTemplates(XDocument document)
        {
            if (m_templateManager == null)
            {
                m_templateManager = CreateTemplateManager();
            }
            return m_templateManager.Load(document);
        }

        public bool ProcessTemplates(XDocument document)
        {
            if (m_templateManager != null)
            {
                return m_templateManager.Process(document);
            }
            return true;
        }

        public void ClearTemplates()
        {
            m_templateManager = null;
        }

        public void ForceSerialization()
        {
            if (this.TypeDefinitionsManager == null)
            {
                throw new InvalidOperationException(
                    "Attempting to call ForceSerialization() without loading type definitions.");
            }

            foreach (var typeDef in this.TypeDefinitionsManager.AllTypeDefinitions)
            {
                typeDef.ForceSerialization = true;
            }

            m_compressionDisabled = true;
        }

        public string GetNameFromOffset(uint offset)
        {
            var name = m_compiledObjectLookup.GetNameFromOffset(offset);
            if (name == null)
            {
                return OBJECT_NOT_FOUND;
            }
            return name;
        }

        #endregion

        private bool CompileObjects(IEnumerable<DocumentEntry> documentEntries,
                                    IObjectCompiler objectCompiler,
                                    IWriter output,
                                    bool shouldTransform = true)
        {
            foreach (var documentEntry in documentEntries)
            {
                var hasFileName = !string.IsNullOrEmpty(documentEntry.DocumentPath);
                if (hasFileName)
                {
                    m_log.PushContext(ContextType.File, documentEntry.DocumentPath);
                }

                if (!objectCompiler.CompileObjects(documentEntry.Document, output, true, shouldTransform))
                {
                    if (hasFileName)
                    {
                        m_log.PopContext();
                    }
                    return false;
                }

                if (hasFileName)
                {
                    m_log.PopContext();
                }
            }
            return true;
        }

        private void CreateTypeDefinitionsManager()
        {
            if (this.TypeDefinitionsManager == null)
            {
                this.TypeDefinitionsManager = new TypeDefinitionsManager(m_log,
                                                                      m_reflector,
                                                                      m_compiledObjectLookup,
                                                                      m_stringTable);
            }
        }

        private IObjectCompiler CreateObjectCompiler(EnumRuntimeMode runtimeMode)
        {
            return new ObjectCompiler(m_log,
                                      m_objectSizeTracker,
                                      m_transformAndValidatorProcessor,
                                      new CompositeFieldInstanceAggregator(this.TypeDefinitionsManager.FindTypeDefinition),
                                      m_compiledObjectLookup,
                                      this.TypeDefinitionsManager.FindTypeDefinition,
                                      m_compilationMode,
                                      this.m_projectSettings.IsBigEndian(),
                                      runtimeMode);
        }
    }
}