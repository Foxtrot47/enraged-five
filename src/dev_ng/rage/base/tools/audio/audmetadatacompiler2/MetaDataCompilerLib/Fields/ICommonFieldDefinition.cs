using System;
using rage.Serialization;
using rage.Types;

namespace rage.Fields
{
    public interface ICommonFieldDefinition : IDefinition
    {
        ITypeDefinition TypeDefinition { get; }

        uint Alignment { get; }

        Type Type { get; }

        ISerializer Serializer { get; }

        Units Unit { get; }

        bool AllowOverrideControl { get; }

        string Description { get; }

        string DisplayGroup { get; }

        bool Ignore { get; }
    }
}