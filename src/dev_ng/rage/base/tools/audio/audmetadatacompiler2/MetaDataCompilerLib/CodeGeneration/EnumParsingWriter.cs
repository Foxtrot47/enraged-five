using System;
using System.CodeDom.Compiler;
using System.Globalization;
using rage.Enums;
using rage.ToolLib;

namespace rage.CodeGeneration
{
    public class EnumParsingWriter
    {
        private readonly IEnumDefinition m_enumDefinition;

        private readonly IndentedTextWriter m_writer;

        public EnumParsingWriter(IndentedTextWriter writer, IEnumDefinition enumDefinition)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (enumDefinition == null)
            {
                throw new ArgumentNullException("enumDefinition");
            }
            m_writer = writer;
            m_enumDefinition = enumDefinition;

            Write();
        }

        private void Write()
        {
            WriteConversionToString();
            m_writer.WriteLine();
            WriteConversionFromString();
        }

        private void WriteConversionFromString()
        {
            m_writer.WriteLine(string.Concat("// PURPOSE - Parse a string into ",
                                             GetArticle(),
                                             " ",
                                             m_enumDefinition.Name,
                                             " value."));
            var declaration = Utility.FormatStringToEnumParseMethodDeclaration(m_enumDefinition.Name);
            using (new BlockWriter(m_writer, declaration, null, false)) // function
            {
                m_writer.WriteLine("const rage::u32 hash = atStringHash(str);");
                using (new BlockWriter(m_writer, "switch(hash)", null, false)) // switch
                {
                    var hash = new Hash();
                    foreach (var enumValue in m_enumDefinition.Values.Keys)
                    {
                        hash.Value = enumValue;
                        m_writer.WriteLine(string.Concat("case ", hash.Key, "U: return ", enumValue, ";"));
                    }

                    if (!m_enumDefinition.BitsetStyle)
                    {
                        var enumNameUpper = m_enumDefinition.Name.ToUpper();
                        hash.Value = string.Concat("NUM_", enumNameUpper);
                        m_writer.WriteLine(string.Concat("case ", hash.Key, "U:"));
                        hash.Value = string.Concat(enumNameUpper, "_MAX");
                        m_writer.WriteLine(string.Concat("case ", hash.Key, "U: return NUM_", enumNameUpper, ";"));
                    }

                    m_writer.WriteLine("default: return defaultValue;");
                }
            }
        }

        private string GetArticle()
        {
            var name = m_enumDefinition.Name;
            if (name.StartsWith("a", true, CultureInfo.InvariantCulture) ||
                name.StartsWith("e", true, CultureInfo.InvariantCulture) ||
                name.StartsWith("i", true, CultureInfo.InvariantCulture) ||
                name.StartsWith("o", true, CultureInfo.InvariantCulture) ||
                name.StartsWith("u", true, CultureInfo.InvariantCulture))
            {
                return "an";
            }
            return "a";
        }

        private void WriteConversionToString()
        {
            m_writer.WriteLine(string.Concat("// PURPOSE - Convert ",
                                             GetArticle(),
                                             " ",
                                             m_enumDefinition.Name,
                                             " value into its string representation."));
            var declaration = Utility.FormatEnumToStringParseMethodDeclaration(m_enumDefinition.Name);
            using (new BlockWriter(m_writer, declaration, null, false)) // function
            {
                using (new BlockWriter(m_writer, "switch(value)", null, false)) // switch
                {
                    foreach (var enumValue in m_enumDefinition.Values.Keys)
                    {
                        m_writer.WriteLine(string.Concat("case ", enumValue, ": return \"", enumValue, "\";"));
                    }

                    if (!m_enumDefinition.BitsetStyle)
                    {
                        var enumNameUpper = string.Concat("NUM_", m_enumDefinition.Name.ToUpper());
                        m_writer.WriteLine(string.Concat("case ", enumNameUpper, ": return \"", enumNameUpper, "\";"));
                    }

                    m_writer.WriteLine("default: return NULL;");
                }
            }
        }
    }
}