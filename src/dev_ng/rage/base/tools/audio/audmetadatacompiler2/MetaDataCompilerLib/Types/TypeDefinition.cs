using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using rage.Compiler;
using rage.Enums;
using rage.Fields;
using rage.Reflection;
using rage.ToolLib.Logging;
using rage.Transform;

namespace rage.Types
{
    [DebuggerDisplay("Name = {Name}")]
    public class TypeDefinition : ITypeDefinition
    {
        private const string MISSING_NAME_ATTRIBUTE = "TypeDefinition missing \"name\" attribute.";
        private const string SUCCESSFULLY_PARSED_TYPE_DEF = "Successfully parsed TypeDefinition.";
        private const string FORMAT_BASE_OBJECT_NOT_FOUND = "Unable to inherit from base object (not found): \"{0}\".";
        private const string DUPLICATE_VALIDATOR = "Duplicate \"validator\" element on TypeDefinition";
        private const string DUPLICATE_TRANSFORMER = "Duplicate \"transformer\" element on TypeDefintion";

        private const string VALIDATOR_TYPE_NOT_SPECIFIED =
            "\"validator\" does not specify the type of IValidator to use.";

        private const string TRANSFORMER_TYPE_NOT_SPECIFIED =
            "\"transformer\" does not specify the type of ITransformer to use.";

        private const string TRANSFORMER_INIT_FAILED = "Failed to initialize transformer";

        private const string FORMAT_COULD_NOT_INSTANTIATE = "Could not create instance of {0}: {1}";

        private const string FORMAT_CANNOT_OPERATE_WITH_SPECIFIED_TYPE =
            "Cannot {0}: \"{1}\" with type: {2}. Check the \"{3}\" attribute(s) of the type.";

        private const string FORMAT_COULD_NOT_PROCESS_VALIDATOR =
            "Failed to process \"validator\" for TypeDefinition: {0}.";

        private const string FORMAT_COULD_NOT_PROCESS_TRANSFORMER =
            "Failed to process \"transformer\" for TypeDefinition: {0}.";

        private const string VALIDATE = "validate";
        private const string TRANSFORM = "transform";
        private const string NAME = "name";
        private static IValidator ms_xmlValidator;

        private readonly IArgsParser m_argsParser;
        private readonly string m_basePath;
        private readonly IFieldParser m_fieldParser;
        private readonly ILog m_log;
        private readonly IReflector m_reflector;
        private readonly Func<string, ITypeDefinition> m_typeDefinitionFinderFunc;

        private readonly ITypeParser m_typeParser;
        private bool m_addtionalCompression;
        private bool m_requiresFlags;
        private uint m_largestAlignment;
        private bool m_largestAlignmentCalculated;
        private readonly bool m_skipTransform;
        private readonly bool m_skipValidation;

        public TypeDefinition(ITypeDefinitionsLoader loader,
                              ILog log,
                              IReflector reflector,
                              IArgsParser argsParser,
                              ITypeParser typeParser,
                              ICompiledObjectLookup compiledObjectLookup,
                              IStringTable stringTable,
                              string basePath,
                              Func<string, ITypeDefinition> typeDefinitionFinderFunc,
                              Func<string, IEnumDefinition> enumDefinitionFinderFunc,
                              byte id,
                              bool skipTransform = false,
                              bool skipValidation = false)
        {
            if (loader == null)
            {
                throw new ArgumentNullException("loader");
            }
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (reflector == null)
            {
                throw new ArgumentNullException("reflector");
            }
            if (argsParser == null)
            {
                throw new ArgumentNullException("argsParser");
            }
            if (typeParser == null)
            {
                throw new ArgumentNullException("typeParser");
            }
            if (compiledObjectLookup == null)
            {
                throw new ArgumentNullException("compiledObjectLookup");
            }
            if (stringTable == null)
            {
                throw new ArgumentNullException("stringTable");
            }
            if (string.IsNullOrEmpty(basePath))
            {
                throw new ArgumentNullException("basePath");
            }
            if (typeDefinitionFinderFunc == null)
            {
                throw new ArgumentNullException("typeDefinitionFinderFunc");
            }
            if (enumDefinitionFinderFunc == null)
            {
                throw new ArgumentNullException("enumDefinitionFinderFunc");
            }

            Loader = loader;
            m_log = log;
            m_reflector = reflector;
            m_argsParser = argsParser;
            m_typeParser = typeParser;
            m_basePath = basePath;
            m_typeDefinitionFinderFunc = typeDefinitionFinderFunc;
            Alignment = 1;
            m_largestAlignment = 1;
            Id = id;
            m_skipTransform = skipTransform;
            m_skipValidation = skipValidation;
            IsFactoryCodeGenerated = true;
            RequiresHeader = true;

            if (ms_xmlValidator == null)
            {
				ms_xmlValidator = reflector.CreateValidator(typeof(XmlValidator));
            }
            m_fieldParser = new FieldParser(this, log, reflector, compiledObjectLookup, stringTable, enumDefinitionFinderFunc);
        }

        #region ITypeDefinition Members

        public ITypeDefinitionsLoader Loader { get; private set; }

        public string Name { get; private set; }

        
        public IEnumerable<ITypeDefinition> GetInheritanceHierarchy()
        {
            var inheritanceHierarchy = new List<ITypeDefinition> { this };
            var parent = this.InheritsFrom;
            while (parent != null)
            {
                inheritanceHierarchy.Insert(0, parent);
                parent = parent.InheritsFrom;
            }
            return inheritanceHierarchy;
        }

        public uint LargestAlignment
        {
            get
            {
                if (!IsPacked &&
                    !m_largestAlignmentCalculated)
                {
                    CalculateLargestAlignment();
                    m_largestAlignmentCalculated = true;
                }
                return m_largestAlignment;
            }
        }

        public ITypeDefinition InheritsFrom { get; private set; }

        public bool IsAncestorCompressed
        {
            get
            {
                var ancestorCompressed = false;
                var typeDefinition = InheritsFrom;
                while (typeDefinition != null)
                {
                    if (typeDefinition.IsCompressed)
                    {
                        ancestorCompressed = true;
                        break;
                    }
                    typeDefinition = typeDefinition.InheritsFrom;
                }
                return ancestorCompressed;
            }
        }

        public bool IsAbstract { get; private set; }

        public bool RequiresHeader { get; private set; }

		public bool AdditionalCompression
		{
            get
            {
                if (m_addtionalCompression)
                {
                    return true;
                }
                if (null != InheritsFrom)
                {
                    return InheritsFrom.AdditionalCompression;
                }
                return false;
            }
        }

		public int CompressionBitFieldLength
		{
			get
			{
				//use 32bit or 64bit depending on the number of fileds
				int compressionBitFieldLength = AdditionalCompression ? 2 : 0;
				//exclude tristate fields and fields with the ignore tag
				foreach (TypeDefinition typeDef in GetInheritanceHierarchy())
				{
					foreach (var fieldDefinition in typeDef.AllFields)
					{
						if (!((fieldDefinition.Type == typeof(TriState)) || fieldDefinition.Ignore) && typeDef.IsCompressed)
							compressionBitFieldLength++;
					}
				}

				return compressionBitFieldLength <= 32 ? 32 : 64;
			}
		}

        public bool SkipDeleteContainerOnRefDelete { get; private set; }

        public bool RequiresFlags
        {
            get
            {
                if (m_requiresFlags)
                {
                    return true;
                }
                if (null != InheritsFrom)
                {
                    return InheritsFrom.RequiresFlags;
                }
                return false;
            }
			set
			{
                m_requiresFlags = value;
                if (value && null != InheritsFrom)
                {
                    // Propagate up inheritance hierarchy
                    InheritsFrom.RequiresFlags = value;
                }
            }
        }

        public string Group { get; private set; }

        public bool IsFactoryCodeGenerated { get; private set; }

        public byte Id { get; private set; }

        public ITransformer Transformer { get; private set; }

        public IValidator Validator { get; private set; }

        public uint Version { get { return Loader.Version; } }

        public bool ForceSerialization
        {
            get { return m_fieldParser.ForceSerialization; }
            set
            {
                if (m_fieldParser.ForceSerialization != value)
                {
                    m_fieldParser.ForceSerialization = value;
                    foreach (var compositeField in CompositeFields)
                    {
                        compositeField.ForceSerialization = value;
                    }
                }
            }
        }

        public bool IsType
        {
            get { return true; }
        }

        public bool IsCompressed
        {
            get { return m_fieldParser.IsCompressed; }
            set { m_fieldParser.IsCompressed = value; }
        }

        public bool IsBaseTypeCompressed
        {
            get
            {
                if (this.IsCompressed)
                {
                    return true;
                }

                return null != this.InheritsFrom &&
                    this.InheritsFrom.IsBaseTypeCompressed;
            }
        }

        public bool IsPacked
        {
            get { return m_fieldParser.IsPacked; }
            set
            {
                m_fieldParser.IsPacked = value;
                foreach (var basicField in m_fieldParser.BasicFields)
                {
                    basicField.IsPacked = value;
                }

                foreach (var compositeField in m_fieldParser.CompositeFields)
                {
                    compositeField.IsPacked = value;
                }

                foreach (var allocatedSpaceField in m_fieldParser.AllocatedSpaceFields)
                {
                    allocatedSpaceField.IsPacked = value;
                }
            }
        }


        public bool Use16BitStringTableIndex { get; private set; }

        public uint Alignment { get; private set; }

        public IList<IBasicFieldDefinition> BasicFields
        {
            get { return m_fieldParser.BasicFields; }
        }

        public IList<ICompositeFieldDefinition> CompositeFields
        {
            get { return m_fieldParser.CompositeFields; }
        }

        public IDictionary<string, ICompositeFieldDefinition> CompositeFieldsInheritedLookup
        {
            get { return m_fieldParser.CompositeFieldsInheritedLookup; }
        }

        public IList<IAllocatedSpaceFieldDefinition> AllocatedSpaceFields
        {
            get { return m_fieldParser.AllocatedSpaceFields; }
        }

        public IList<ICommonFieldDefinition> AllFields
        {
            get { return m_fieldParser.AllFields; }
        }

        public IList<ICommonFieldDefinition> AllFieldsInherited
        {
            get { return m_fieldParser.AllFieldsInherited; }
        }

        public IDictionary<string, ICommonFieldDefinition> AllFieldsInheritedLookup
        {
            get { return m_fieldParser.AllFieldsInheritedLookup; }
        }

        public bool Parse(XElement typeDefinitionElement)
        {
            var nameAttrib = typeDefinitionElement.Attribute(NAME);
            Name = nameAttrib == null ? null : nameAttrib.Value;
            if (string.IsNullOrEmpty(Name))
            {
                m_log.Error(MISSING_NAME_ATTRIBUTE);
                return false;
            }

            m_log.PushContext(ContextType.Type, Name);
            if (!m_fieldParser.Parse(typeDefinitionElement.Elements()))
            {
                return Failed();
            }

            if (!ParseAttributes(typeDefinitionElement))
            {
                return Failed();
            }

            Inherit();

            SetRequiresFlags();

            m_log.Information(SUCCESSFULLY_PARSED_TYPE_DEF);
            m_log.PopContext();
            return true;
        }

        #endregion

        private void CalculateLargestAlignment()
        {
            foreach (var fieldDef in AllFieldsInherited)
            {
                if (fieldDef.Ignore)
                {
                    continue;
                }

                if (fieldDef.Alignment > m_largestAlignment)
                {
                    m_largestAlignment = fieldDef.Alignment;
                }
            }
        }

        private bool Failed()
        {
            m_log.PopContext();
            return false;
        }

        private bool ParseAttributes(XElement typeDefinitionElement)
        {
            foreach (var attribute in typeDefinitionElement.Attributes())
            {
                try
                {
                    var token =
                        (TypeDefinitionTokens)
						Enum.Parse(typeof(TypeDefinitionTokens), attribute.Name.ToString(), true);
                    switch (token)
                    {
                        case TypeDefinitionTokens.Name:
                            {
                                break;
                            }
                        case TypeDefinitionTokens.IsFactoryCodeGenerated:
                            {
                                IsFactoryCodeGenerated = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case TypeDefinitionTokens.InheritsFrom:
                            {
                                var baseDef = m_typeDefinitionFinderFunc(attribute.Value);
                                if (baseDef == null)
                                {
                                    m_log.Error(FORMAT_BASE_OBJECT_NOT_FOUND, attribute.Value);
                                    return false;
                                }
                                InheritsFrom = baseDef;
                                break;
                            }
                        case TypeDefinitionTokens.IsAbstract:
                            {
                                IsAbstract = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case TypeDefinitionTokens.IsCompressed:
                            {
                                IsCompressed = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case TypeDefinitionTokens.Group:
                            {
                                Group = attribute.Value;
                                break;
                            }
                        case TypeDefinitionTokens.Packed:
                            {
                                IsPacked = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case TypeDefinitionTokens.Validator:
                            {
                                if (!m_skipValidation && !ProcessValidator(attribute.Value))
                                {
                                    m_log.Error(FORMAT_COULD_NOT_PROCESS_VALIDATOR, Name);
                                    return false;
                                }
                                break;
                            }
                        case TypeDefinitionTokens.Transformer:
                            {
                                if (!m_skipTransform && !ProcessTransformer(attribute.Value))
                                {
                                    m_log.Error(FORMAT_COULD_NOT_PROCESS_TRANSFORMER, Name);
                                    return false;
                                }
                                break;
                            }
                        case TypeDefinitionTokens.Align:
                            {
                                Alignment = uint.Parse(attribute.Value);
                                break;
                            }
                        case TypeDefinitionTokens.RequiresHeader:
                            {
                                RequiresHeader = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case TypeDefinitionTokens.AdditionalCompression:
                            {
                                this.m_addtionalCompression = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case TypeDefinitionTokens.SkipDeleteContainerOnRefDelete:
                            {
                                this.SkipDeleteContainerOnRefDelete = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case TypeDefinitionTokens.Use16BitStringTableIndex:
                            {
                                this.Use16BitStringTableIndex = Utility.ToBoolean(attribute.Value);
                            }
                            break;
                        default:
                            {
                                throw new Exception();
                            }
                    }
                }
                catch (Exception ex)
                {
                    m_log.Exception(ex);
                }
            }
            return true;
        }

        private void Inherit()
        {
            if (InheritsFrom != null)
            {
                foreach (var entry in InheritsFrom.AllFieldsInheritedLookup)
                {
                    if (!AllFieldsInheritedLookup.ContainsKey(entry.Key))
                    {
                        AllFieldsInheritedLookup.Add(entry.Key, entry.Value);
                    }
                }

                foreach (var entry in InheritsFrom.CompositeFieldsInheritedLookup)
                {
                    if (!CompositeFieldsInheritedLookup.ContainsKey(entry.Key))
                    {
                        CompositeFieldsInheritedLookup.Add(entry);
                    }
                }

                for (var i = InheritsFrom.AllFieldsInherited.Count - 1; i >= 0; --i)
                {
                    AllFieldsInherited.Insert(0, InheritsFrom.AllFieldsInherited[i]);
                }
            }
        }

        private bool ProcessTransformer(string value)
        {
            if (Transformer == null)
            {
                if (IsNewSyntax(value))
                {
                    var type = m_typeParser.GetType(value);
                    if (type != null)
                    {
                        var transformer = CreateInstance(type,
                                                         m_reflector.CanBeTransformedBy,
                                                         m_reflector.CreateTransformer,
                                                         TRANSFORM,
														 typeof(CanTransformAttribute).ToString());
                        if (transformer != null)
                        {
                            var args = m_argsParser.GetArgs(value);
                            if (transformer.Init(m_log, args))
                            {
                                Transformer = transformer;
                                return true;
                            }
                        }
                        else
                        {
                            m_log.Error(TRANSFORMER_INIT_FAILED);
                        }
                    }
                    else
                    {
                        m_log.Error(TRANSFORMER_TYPE_NOT_SPECIFIED);
                    }
                }
            }
            else
            {
                m_log.Error(DUPLICATE_TRANSFORMER);
            }
            return false;
        }

        private bool ProcessValidator(string value)
        {
            if (Validator == null)
            {
                if (IsNewSyntax(value))
                {
                    var type = m_typeParser.GetType(value);
                    if (type != null)
                    {
                        var validator = CreateInstance(type,
                                                       m_reflector.CanBeValidatedBy,
                                                       m_reflector.CreateValidator,
                                                       VALIDATE,
													   typeof(CanValidateAttribute).ToString());
                        if (validator != null)
                        {
                            Validator = validator;
                            return true;
                        }
                    }
                    else
                    {
                        m_log.Error(VALIDATOR_TYPE_NOT_SPECIFIED);
                    }
                }
                else
                {
                    // This is an xml validator in the old format, so create the transformer as well
					var transformer = m_reflector.CreateTransformer(typeof(XmlTransformer));
                    if (transformer.Init(m_log, Path.Combine(m_basePath, value)))
                    {
                        Validator = ms_xmlValidator;
                        Transformer = transformer;
                        return true;
                    }
                }
            }
            else
            {
                m_log.Error(DUPLICATE_VALIDATOR);
            }
            return false;
        }

        private T CreateInstance<T>(Type type,
                                    Func<string, Type, bool> check,
                                    Func<Type, T> create,
                                    string operation,
                                    string attribute) where T : class
        {
            T instance = null;
            if (check(Name, type))
            {
                instance = create(type);
                if (instance == null)
                {
					m_log.Error(FORMAT_COULD_NOT_INSTANTIATE, typeof(T).Name, type.AssemblyQualifiedName);
                }
            }
            else
            {
                m_log.Error(FORMAT_CANNOT_OPERATE_WITH_SPECIFIED_TYPE,
                            operation,
                            Name,
                            type.AssemblyQualifiedName,
                            attribute);
            }
            return instance;
        }

        private static bool IsNewSyntax(string value)
        {
            return value.StartsWith("{") && value.EndsWith("}");
        }

        private void SetRequiresFlags()
        {
            // If type definition contains at least one
            // tristate  field then it requires Flags
            foreach (var fieldDef in m_fieldParser.AllFields)
            {
                if (fieldDef.Type.TypeHandle.Equals(typeof(TriState).TypeHandle) &&
                    !fieldDef.Ignore)
                {
                    RequiresFlags = true;
                    // Only require knowledge of at least one tristate field
                    break;
                }
            }
        }
    }
}
