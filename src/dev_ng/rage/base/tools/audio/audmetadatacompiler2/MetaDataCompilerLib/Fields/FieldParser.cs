using System;
using System.Collections.Generic;
using System.Xml.Linq;
using rage.Compiler;
using rage.Enums;
using rage.Reflection;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.Fields
{
    public class FieldParser : IFieldParser
    {
        private const string FORMAT_UNRECOGNISED_CHILD = "Unrecognised child element: \"{0}\" in TypeDefinition.";

        private readonly ICompiledObjectLookup m_compiledObjectLookup;

        private readonly Func<string, IEnumDefinition> m_enumDefinitionFinderFunc;

        private readonly ILog m_log;

        private readonly IReflector m_reflector;

        private readonly IStringTable m_stringTable;

        public FieldParser(ITypeDefinition typeDefinition,
                            ILog log,
                           IReflector reflector,
                           ICompiledObjectLookup compiledObjectLookup,
                           IStringTable stringTable,
                           Func<string, IEnumDefinition> enumDefinitionFinderFunc)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (reflector == null)
            {
                throw new ArgumentNullException("reflector");
            }
            if (compiledObjectLookup == null)
            {
                throw new ArgumentNullException("compiledObjectLookup");
            }
            if (stringTable == null)
            {
                throw new ArgumentNullException("stringTable");
            }
            if (enumDefinitionFinderFunc == null)
            {
                throw new ArgumentNullException("enumDefinitionFinderFunc");
            }

            TypeDefinition = typeDefinition;
            m_log = log;
            m_reflector = reflector;
            m_compiledObjectLookup = compiledObjectLookup;
            m_stringTable = stringTable;
            m_enumDefinitionFinderFunc = enumDefinitionFinderFunc;

            BasicFields = new List<IBasicFieldDefinition>();
            CompositeFields = new List<ICompositeFieldDefinition>();
            CompositeFieldsInheritedLookup = new Dictionary<string, ICompositeFieldDefinition>();
            AllocatedSpaceFields = new List<IAllocatedSpaceFieldDefinition>();
            AllFields = new List<ICommonFieldDefinition>();
            AllFieldsInherited = new List<ICommonFieldDefinition>();
            AllFieldsInheritedLookup = new Dictionary<string, ICommonFieldDefinition>();
            IsPacked = true;
        }

        #region IFieldParser Members

        public ITypeDefinition TypeDefinition { get; private set; }

        public IList<IBasicFieldDefinition> BasicFields { get; private set; }

        public IList<ICompositeFieldDefinition> CompositeFields { get; private set; }

        public IDictionary<string, ICompositeFieldDefinition> CompositeFieldsInheritedLookup { get; private set; }

        public IList<IAllocatedSpaceFieldDefinition> AllocatedSpaceFields { get; private set; }

        public IList<ICommonFieldDefinition> AllFields { get; private set; }

        public IList<ICommonFieldDefinition> AllFieldsInherited { get; private set; }

        public IDictionary<string, ICommonFieldDefinition> AllFieldsInheritedLookup { get; private set; }

        public bool IsCompressed { get; set; }

        public bool IsBaseTypeCompressed
        {
            get
            {
                if (this.IsCompressed)
                {
                    return true;
                }

                return null != this.TypeDefinition.InheritsFrom &&
                    this.TypeDefinition.InheritsFrom.IsBaseTypeCompressed;
            }
        }

        public bool IsPacked { get; set; }

        public bool ForceSerialization { get; set; }

        public bool IsType
        {
            get { return false; }
        }

        public bool Parse(IEnumerable<XElement> fieldElements)
        {
            foreach (var fieldElement in fieldElements)
            {
                if (!ParseFieldElement(fieldElement))
                {
                    return false;
                }
            }

            foreach (var compositeField in CompositeFields)
            {
                if (compositeField.MaxOccurs > 1 &&
                    !compositeField.IsFixedSize)
                {
                    var index = AllFields.IndexOf(compositeField);
                    AllFields.RemoveAt(index);
                    AllFieldsInherited.RemoveAt(index);
                    AllFields.Add(compositeField);
                    AllFieldsInherited.Add(compositeField);
                }
            }
            return true;
        }

        #endregion

        private bool ParseFieldElement(XElement fieldElement)
        {
            try
            {
                var fieldType =
                    (TypeDefinitionTokens) Enum.Parse(typeof (TypeDefinitionTokens), fieldElement.Name.ToString(), true);
                switch (fieldType)
                {
                    case TypeDefinitionTokens.Field:
                        {
                            if (!ParseBasicField(fieldElement))
                            {
                                return false;
                            }
                            break;
                        }
                    case TypeDefinitionTokens.CompositeField:
                        {
                            if (!ParseCompositeField(fieldElement))
                            {
                                return false;
                            }
                            break;
                        }
                    case TypeDefinitionTokens.AllocatedSpaceField:
                        {
                            if (!ParseAllocatedSpaceField(fieldElement))
                            {
                                return false;
                            }
                            break;
                        }
                    default:
                        {
                            throw new Exception();
                        }
                }
            }
            catch (Exception)
            {
                m_log.Error(FORMAT_UNRECOGNISED_CHILD, fieldElement.Name);
                return false;
            }
            return true;
        }

        private bool ParseAllocatedSpaceField(XElement allocatedSpaceFieldElement)
        {
            var fieldDef = new AllocatedSpaceFieldDefinition(TypeDefinition, 
                                                             m_log,
                                                             m_reflector,
                                                             m_enumDefinitionFinderFunc)
                               {IsPacked = IsPacked};
            if (!fieldDef.Parse(allocatedSpaceFieldElement))
            {
                return false;
            }
            AllocatedSpaceFields.Add(fieldDef);
            Add(fieldDef);
            return true;
        }

        private bool ParseCompositeField(XElement compositeFieldElement)
        {
            var fieldDef = new CompositeFieldDefinition(TypeDefinition,
                                                        m_log,
                                                        m_reflector,
                                                        m_compiledObjectLookup,
                                                        m_stringTable,
                                                        m_enumDefinitionFinderFunc) {IsPacked = IsPacked};
            if (!fieldDef.Parse(compositeFieldElement))
            {
                return false;
            }
            CompositeFields.Add(fieldDef);
            CompositeFieldsInheritedLookup.Add(fieldDef.Name, fieldDef);
            Add(fieldDef);
            return true;
        }

        private bool ParseBasicField(XElement basicFieldElement)
        {
            var fieldDef = new BasicFieldDefinition(TypeDefinition,
                                                    m_log,
                                                    m_reflector,
                                                    m_compiledObjectLookup,
                                                    m_stringTable,
                                                    m_enumDefinitionFinderFunc) {IsPacked = IsPacked};
            if (!fieldDef.Parse(basicFieldElement))
            {
                return false;
            }
            BasicFields.Add(fieldDef);
            Add(fieldDef);
            return true;
        }

        private void Add(ICommonFieldDefinition fieldDef)
        {
            AllFields.Add(fieldDef);
            AllFieldsInherited.Add(fieldDef);
            AllFieldsInheritedLookup.Add(fieldDef.Name, fieldDef);
        }
    }
}