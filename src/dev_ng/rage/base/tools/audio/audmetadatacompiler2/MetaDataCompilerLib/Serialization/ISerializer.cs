using System.CodeDom.Compiler;
using System.IO;
using System.Xml.Linq;
using rage.Fields;
using rage.ToolLib.Writer;

namespace rage.Serialization
{
    using rage.ToolLib.Logging;

    public interface ISerializer
    {
        uint Alignment { get; }

        int Compare(XElement x, XElement y);

        bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition);

        string WriteConstuctorInitializerListEntry(TextWriter writer, string value);

        void WriteStructureEntry(TextWriter writer);

        void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition);
    }
}