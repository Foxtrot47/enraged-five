using System.Xml.Linq;
using rage.Fields;
using rage.Transform;

namespace rage.Types
{
    public interface ITypeDefinition : IFieldContainer
    {
        ITypeDefinitionsLoader Loader { get; }

        string Name { get; }

        uint Alignment { get; }

        uint LargestAlignment { get; }

        ITypeDefinition InheritsFrom { get; }

        bool IsAncestorCompressed { get; }

        bool IsAbstract { get; }

        bool RequiresHeader { get; }

        bool AdditionalCompression { get; }

        bool RequiresFlags { get; set; }

        string Group { get; }

        bool IsFactoryCodeGenerated { get; }

        byte Id { get; }

        ITransformer Transformer { get; }

        IValidator Validator { get; }

        uint Version { get; }

        bool Parse(XElement typeDefinitionElement);
    }
}