using System.Xml.Linq;

namespace rage.Enums
{
    public interface IEnumValue
    {
        string Name { get; set; }

        byte Value { get; set; }

        bool Parse(XElement valueElement, byte value);
    }
}