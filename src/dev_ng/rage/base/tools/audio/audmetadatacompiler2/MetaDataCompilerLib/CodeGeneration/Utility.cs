namespace rage.CodeGeneration
{
    public static class Utility
    {
        public static string EncloseInBracketsWithComma(string value)
        {
            return string.Concat("(", value, "),");
        }

        public static string EncloseInBracketsWithCommaAndComment(string value, string comment)
        {
            return string.Concat(EncloseInBracketsWithComma(value), " // ", comment);
        }

        public static string FormatEnumToStringParseMethodDeclaration(string enumName)
        {
            return string.Concat("const char* ", enumName, "_ToString(const ", enumName, " value)");
        }

        public static string FormatStringToEnumParseMethodDeclaration(string enumName)
        {
            return string.Concat(enumName, " ", enumName, "_Parse(const char* str, const ", enumName, " defaultValue)");
        }

        public static string GetBaseTypeIdDeclaration(string metaDataType)
        {
            return string.Concat("u32 g", metaDataType, "GetBaseTypeId(const u32 classId)");
        }

        public static string GetMaxSizeDeclaration(string metaDataType)
        {
            return string.Concat("u32 g", metaDataType, "MaxSize()");
        }

        public static string GetTypeNameDeclaration(string metaDataType)
        {
            return string.Concat("const char* g", metaDataType, "GetTypeName(const u32 classId)");
        }

        public static string GetIsOfTypeIdWithIdDeclaration(string metaDataType)
        {
            return string.Concat("bool g", metaDataType, "IsOfType(const u32 objectTypeId, const u32 baseTypeId)");
        }
    }
}