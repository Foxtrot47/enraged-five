using System;
using System.Xml.Linq;
using rage.Enums;
using rage.Reflection;
using rage.Serialization;
using rage.Serializers;
using rage.ToolLib;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.Fields
{
    public abstract class CommonFieldDefinition : ICommonFieldDefinition
    {
        private const string NAME = "name";
        private const string MISSING_NAME_ATTRIBUTE = "Missing \"name\" attribute on field.";
        private const string TYPE_NOT_SPECIFIED = "Type not specified by field.";
        private const string FAILED_PARSING_SERIALIZER = "Unable to parse \"serializer\" on field.";
        private const string FORMAT_SERIALIZER_INCOMPATIBLE = "{0} cannot be serialized by {1}";
        private const string FIXED_POINT = "0.01units";
        private readonly Func<string, IEnumDefinition> m_enumDefinitionFinderFunc;
        protected readonly ILog m_log;
        protected readonly IReflector m_reflector;
        private uint m_alignment;

        protected CommonFieldDefinition(ITypeDefinition typeDefinition,
                                        ILog log,
                                        IReflector reflector,
                                        Func<string, IEnumDefinition> enumDefinitionFinderFunc)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (reflector == null)
            {
                throw new ArgumentNullException("reflector");
            }
            if (enumDefinitionFinderFunc == null)
            {
                throw new ArgumentNullException("enumDefinitionFinderFunc");
            }

            TypeDefinition = typeDefinition;
            m_log = log;
            m_reflector = reflector;
            m_enumDefinitionFinderFunc = enumDefinitionFinderFunc;
            m_alignment = 1;
            Unit = Units.Unspecified;
        }

        #region ICommonFieldDefinition Members

        public string Name { get; private set; }

        public ITypeDefinition TypeDefinition { get; private set; }

        public uint Alignment
        {
            get { return Math.Max(m_alignment, Serializer.Alignment); }
            protected set { m_alignment = value; }
        }

        public Type Type { get; protected set; }

        public ISerializer Serializer { get; protected set; }

        public Units Unit { get; private set; }

        public bool AllowOverrideControl { get; private set; }

        public string Description { get; private set; }

        public string DisplayGroup { get; private set; }

        public string DisplayName { get; private set; }

        public bool Ignore { get; private set; }

        public virtual bool Parse(XElement fieldElement)
        {
            var nameAttrib = fieldElement.Attribute(NAME);
            Name = nameAttrib == null ? null : nameAttrib.Value;
            if (string.IsNullOrEmpty(Name))
            {
                m_log.Error(MISSING_NAME_ATTRIBUTE);
                return false;
            }
            m_log.PushContext(ContextType.Field, Name);
            foreach (var attribute in fieldElement.Attributes())
            {
                try
                {
                    var token = Enum<FieldDefinitionTokens>.Parse(attribute.Name.ToString(), true);
                    switch (token)
                    {
                        case FieldDefinitionTokens.Name:
                            {
                                break;
                            }
                        case FieldDefinitionTokens.Ignore:
                            {
                                Ignore = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case FieldDefinitionTokens.Type:
                            {
                                if (!ParseType(attribute.Value, fieldElement))
                                {
                                    m_log.Error(TYPE_NOT_SPECIFIED);
                                    return Failed();
                                }
                                break;
                            }
                        case FieldDefinitionTokens.Serializer:
                            {
                                if (!ParseSerializer(attribute.Value))
                                {
                                    m_log.Error(FAILED_PARSING_SERIALIZER);
                                    return Failed();
                                }
                                break;
                            }
                        case FieldDefinitionTokens.Units:
                            {
                                Unit = ParseUnits(attribute.Value);
                                break;
                            }
                        case FieldDefinitionTokens.AllowOverrideControl:
                            {
                                AllowOverrideControl = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case FieldDefinitionTokens.Description:
                            {
                                Description = attribute.Value;
                                break;
                            }
                        case FieldDefinitionTokens.DisplayGroup:
                            {
                                DisplayGroup = attribute.Value;
                                break;
                            }
                        case FieldDefinitionTokens.Align:
                            {
                                m_alignment = uint.Parse(attribute.Value);
                                break;
                            }
                        case FieldDefinitionTokens.Display:
                            {
                                DisplayName = attribute.Value;
                                break;
                            }
                    }
                }
                catch
                {
                }
            }
            var ret = CheckSerializer();
            m_log.PopContext();
            return ret;
        }

        #endregion

        private static Units ParseUnits(string value)
        {
            if (value == FIXED_POINT)
            {
                return Units.FixedPoint;
            }
            return Enum<Units>.TryParse(value, true, Units.Unknown);               
        }

        private bool CheckSerializer()
        {
            var serializerType = Serializer.GetType();
            if (!m_reflector.CanBeSerializedBy(Type, serializerType))
            {
                m_log.Error(FORMAT_SERIALIZER_INCOMPATIBLE, Type.FullName, serializerType.FullName);
                return false;
            }
            return true;
        }

        private bool Failed()
        {
            m_log.PopContext();
            return false;
        }

        private bool ParseSerializer(string value)
        {
            var type = Type.GetType(value, true, true);
            if (type != null)
            {
                Serializer = m_reflector.CreateSerializer(type);
                return Serializer != null;
            }
            return false;
        }

        private bool ParseType(string value, XElement element)
        {
            var enumName = string.Empty;
            var enumAttrib = element.Attribute(FieldTypeTokens.Enum.ToString().ToLower());
            if (enumAttrib != null)
            {
                enumName = enumAttrib.Value;
            }
            var isBuiltInType = TryHandleBuiltInType(value, enumName);
            if (!isBuiltInType)
            {
                var type = Type.GetType(value, true, true);
                if (type == null)
                {
                    return false;
                }
                Type = type;
            }
            return true;
        }

        private bool TryHandleBuiltInType(string value, string enumName)
        {
            var isBuiltInType = true;
            try
            {
                var type = Enum<FieldTypeTokens>.TryParse(value, true, FieldTypeTokens.Unknown);

                switch (type)
                {
                    case FieldTypeTokens.S8:
                        {
                            Type = typeof (sbyte);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.U8:
                        {
                            Type = typeof (byte);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.S16:
                        {
                            Type = typeof (short);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.U16:
                        {
                            Type = typeof (ushort);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.S32:
                        {
                            Type = typeof (int);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.U32:
                        {
                            Type = typeof (uint);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.S64:
                        {
                            Type = typeof (long);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.U64:
                        {
                            Type = typeof (ulong);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.F32:
                        {
                            Type = typeof (float);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.CString:
                        {
                            Type = typeof (string);
                            Serializer = new StringSerializer(this as IBasicFieldDefinition, true);
                            break;
                        }
                    case FieldTypeTokens.String:
                        {
                            Type = typeof (string);
                            Serializer = new StringSerializer(this as IBasicFieldDefinition, false);
                            break;
                        }
                    case FieldTypeTokens.TriState:
                        {
                            Type = typeof (TriState);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.Bit:
                        {
                            Type = typeof (BitField);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.Hash:
                        {
                            Type = typeof (Hash);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.PartialHash:
                        {
                            Type = typeof(PartialHash);
                            Serializer = SerializerLookup.Get(Type);
                            break;
                        }
                    case FieldTypeTokens.Enum:
                        {
                            var enumDef = m_enumDefinitionFinderFunc(enumName);
                            if (enumDef != null)
                            {
                                Type = typeof (Enum);
                                Serializer = new EnumSerializer(enumDef);
                                break;
                            }
                            return false;
                        }
                    default:
                        {
                            isBuiltInType = false;
                            break;
                        }
                }
            }
            catch (Exception)
            {
                isBuiltInType = false;
            }
            return isBuiltInType;
        }
    }
}