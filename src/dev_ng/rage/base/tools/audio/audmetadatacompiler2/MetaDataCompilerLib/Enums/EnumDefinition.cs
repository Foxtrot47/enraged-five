using System;
using System.Collections.Generic;
using System.Xml.Linq;
using rage.ToolLib.Logging;

namespace rage.Enums
{
    public class EnumDefinition : IEnumDefinition
    {
        private const string NAME = "name";

        private const string MISSING_NAME_ATTRIBUTE = "EnumDefinition missing \"name\" attribute";

        private const string SUCCESS = "Successfully parsed.";

        private const string FAILURE = "Failed parsing.";

        private const string INVALID_BITSET_INITIAL_VALUE = "Bitset specified but initial value isn't 0.";

        private readonly ILog m_log;

        private readonly Dictionary<string, IEnumValue> m_values;

        private byte m_offset;

        public EnumDefinition(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
            Values = m_values = new Dictionary<string, IEnumValue>();
        }

        #region IEnumDefinition Members

        public string Name { get; private set; }

        public byte InitialValue { get; private set; }

        public bool BitsetStyle { get; private set; }

        public IDictionary<string, IEnumValue> Values { get; private set; }

        public bool Parse(XElement element)
        {
            var nameAttrib = element.Attribute(NAME);
            Name = nameAttrib == null ? null : nameAttrib.Value;
            if (string.IsNullOrEmpty(Name))
            {
                m_log.Error(MISSING_NAME_ATTRIBUTE);
                return false;
            }

            m_log.PushContext(ContextType.Enum, Name);
            foreach (var attrib in element.Attributes())
            {
                try
                {
                    var token =
                        (EnumDefinitionTokens) Enum.Parse(typeof (EnumDefinitionTokens), attrib.Name.ToString(), true);
                    switch (token)
                    {
                        case EnumDefinitionTokens.Name:
                            {
                                break;
                            }
                        case EnumDefinitionTokens.InitialValue:
                            {
                                byte val;
                                if (byte.TryParse(attrib.Value, out val))
                                {
                                    InitialValue = val;
                                }
                                break;
                            }
                        case EnumDefinitionTokens.Bitset:
                            {
                                BitsetStyle = Utility.ToBoolean(attrib.Value);
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    m_log.Exception(ex);
                }
            }

            if (BitsetStyle && InitialValue != 0)
            {
                m_log.Error(INVALID_BITSET_INITIAL_VALUE);
                return false;
            }

            var ret = ParseValueElements(element.Elements(EnumDefinitionTokens.Value.ToString()));
            if (ret)
            {
                m_log.Information(SUCCESS);
            }
            else
            {
                m_log.Error(FAILURE);
            }
            m_log.PopContext();
            return ret;
        }

        #endregion

        private bool ParseValueElements(IEnumerable<XElement> valueElements)
        {
            foreach (var valueElement in valueElements)
            {
                var enumValue = new EnumValue();
                if (enumValue.Parse(valueElement, (byte) (InitialValue + m_offset)))
                {
                    m_offset += 1;
                    m_values.Add(enumValue.Name, enumValue);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
    }
}