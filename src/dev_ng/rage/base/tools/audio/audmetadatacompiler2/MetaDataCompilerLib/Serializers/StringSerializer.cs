using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Text;
using System.Xml.Linq;
using rage.Fields;
using rage.Serialization;
using rage.ToolLib.Writer;

namespace rage.Serializers
{
    using rage.ToolLib.Logging;

    [CanSerialize(typeof (string), true)]
    public class StringSerializer : ISerializer
    {
        private readonly IBasicFieldDefinition m_basicFieldDefinition;

        private readonly bool m_isFixedString;

        public StringSerializer(IBasicFieldDefinition basicFieldDefinition, bool isFixedString)
        {
            if (basicFieldDefinition == null)
            {
                throw new ArgumentNullException("basicFieldDefinition");
            }
            m_basicFieldDefinition = basicFieldDefinition;
            m_isFixedString = isFixedString;
        }

        #region ISerializer Members

        public uint Alignment
        {
            get { return 1; }
        }

        public int Compare(XElement x, XElement y)
        {
            return x.Value.CompareTo(y.Value);
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            var str = element.Value;
            if (m_isFixedString)
            {
                var length = str.Length;
                byte[] pad = null;
                if (m_basicFieldDefinition.Length > 0)
                {
                    if (length > m_basicFieldDefinition.Length)
                    {
                        str = str.Substring(0, (int) m_basicFieldDefinition.Length);
                    }
                    else if (length < m_basicFieldDefinition.Length)
                    {
                        pad = new byte[m_basicFieldDefinition.Length - length];
                    }
                }
                output.Write(Encoding.ASCII.GetBytes(str), false);
                if (pad != null)
                {
                    output.Write(pad, false);
                }
            }
            else
            {
                output.Write(str);
            }
            return true;
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            // this should never be called
            throw new NotImplementedException();
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            if (!m_isFixedString)
            {
                writer.WriteLine(string.Concat("rage::u8 ", m_basicFieldDefinition.Name, "Len;"));
            }
            writer.WriteLine(string.Concat("char ",
                                           m_basicFieldDefinition.Name,
                                           "[",
                                           m_isFixedString ? m_basicFieldDefinition.Length : 255,
                                           "];"));
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            // do nothing
        }

        #endregion
    }
}