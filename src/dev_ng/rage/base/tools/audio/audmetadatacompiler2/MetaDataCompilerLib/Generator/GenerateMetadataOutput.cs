using System;
using System.IO;
using System.Xml.Linq;

namespace rage.Generator
{
    public class GenerateMetadataOutput
    {
        private const string XML_EXTENSION = ".xml";

        public GenerateMetadataOutput(string workingPath, XElement element)
        {
            if (string.IsNullOrEmpty(workingPath))
            {
                throw new ArgumentNullException("workingPath");
            }
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            Process(workingPath, element);
        }

        public string OutputFile { get; private set; }

        private void Process(string workingPath, XElement element)
        {
            if (element.Name !=
                ParseTokens.Output.ToString())
            {
                throw new ArgumentException(string.Format("Expected element of type \"{0}\"", ParseTokens.Output));
            }

            if (Path.GetExtension(element.Value).ToLower() != XML_EXTENSION)
            {
                throw new ArgumentException(
                    string.Format("If used, the \"{0}\" element must contain the path to an XML file.",
                                  ParseTokens.Output));
            }
            OutputFile = string.Concat(workingPath, element.Value).ToUpper();

            var directory = new DirectoryInfo(Path.GetDirectoryName(OutputFile));
            if (!directory.Exists)
            {
                directory.Create();
            }
            else
            {
                var file = new FileInfo(OutputFile);
                if (file.Exists)
                {
                    file.IsReadOnly = false;
                }
            }
        }

        #region Nested type: ParseTokens

        private enum ParseTokens
        {
            Output
        }

        #endregion
    }
}