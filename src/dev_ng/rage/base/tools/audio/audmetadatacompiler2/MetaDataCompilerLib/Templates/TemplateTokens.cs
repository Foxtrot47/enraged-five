namespace rage.Templates
{
    public enum TemplateTokens
    {
        Template,
        Name,
        Objects,
        ExposeAs,
        TemplateInstance
    }
}