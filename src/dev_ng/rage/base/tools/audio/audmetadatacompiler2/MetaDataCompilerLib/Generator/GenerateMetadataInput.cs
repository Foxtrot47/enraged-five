using System;
using System.Xml.Linq;
using rage.ToolLib;

namespace rage.Generator
{
    public class GenerateMetadataInput
    {
        private const string WILDCARD = "*";

        private const string FORMAT_MISSING_REQUIRED_ATTRIBUTE =
            "An \"{0}\" element of type \"{1}\" must specify a \"{2}\" attribute";

        private bool m_matchAll;

        public GenerateMetadataInput(XElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            Process(element);
        }

        public InputType Type { get; private set; }

        public string FileMask { get; private set; }

        public string MetadataType { get; private set; }

        public string MetadataNameFilter { get; private set; }

        public FilterCondition MetadataNameFilterCondition { get; private set; }

        public bool IsMatch(string value)
        {
            value = value.ToUpper();
            switch (MetadataNameFilterCondition)
            {
                case FilterCondition.Equals:
                    {
                        return m_matchAll || value.Equals(MetadataNameFilter);
                    }
                case FilterCondition.StartsWith:
                    {
                        return m_matchAll || value.StartsWith(MetadataNameFilter);
                    }
                case FilterCondition.Contains:
                    {
                        return m_matchAll || value.Contains(MetadataNameFilter);
                    }
                case FilterCondition.EndsWith:
                    {
                        return m_matchAll || value.EndsWith(MetadataNameFilter);
                    }
            }
            return false;
        }

        private void Process(XElement element)
        {
            if (element.Name != ParseTokens.Input.ToString())
            {
                throw new ArgumentException(string.Format("Expected element of type \"{0}\".", ParseTokens.Input));
            }

            var typeAttrib = element.Attribute(ParseTokens.type.ToString());
            if (typeAttrib == null)
            {
                throw new FormatException(string.Format("An \"{0}\" element must have a \"{1}\" attribute.",
                                                        ParseTokens.Input,
                                                        ParseTokens.type));
            }

            Type = Enum<InputType>.TryParse(typeAttrib.Value, true, InputType.None);
            if (Type == InputType.None)
            {
                throw new FormatException(string.Format("Unrecognised \"{0}\" value ({1}) on \"{2}\" element. Expected either \"{3}\" or \"{4}\".",
                                                        ParseTokens.type,
                                                        typeAttrib.Value,
                                                        ParseTokens.Input,
                                                        InputType.Objects,
                                                        InputType.Waves));
            }

            if (Type == InputType.Objects)
            {
                var metadataTypeAttrib = element.Attribute(ParseTokens.metadataType.ToString());
                if (metadataTypeAttrib == null)
                {
                    throw new FormatException(string.Format(FORMAT_MISSING_REQUIRED_ATTRIBUTE,
                                                            ParseTokens.Input,
                                                            InputType.Objects,
                                                            ParseTokens.metadataType));
                }
                MetadataType = metadataTypeAttrib.Value;

                var metadataNameFilterAttrib = element.Attribute(ParseTokens.metadataNameFilter.ToString());
                if (metadataNameFilterAttrib == null)
                {
                    throw new FormatException(string.Format(FORMAT_MISSING_REQUIRED_ATTRIBUTE,
                                                            ParseTokens.Input,
                                                            InputType.Objects,
                                                            ParseTokens.metadataNameFilter));
                }
                MetadataNameFilter = metadataNameFilterAttrib.Value.ToUpper();
                if (MetadataNameFilter == WILDCARD)
                {
                    m_matchAll = true;
                }

                var metadataNameFilterConditionAttrib =
                    element.Attribute(ParseTokens.metadataNameFilterCondition.ToString());
                MetadataNameFilterCondition = metadataNameFilterConditionAttrib != null
                                                  ? Enum<FilterCondition>.TryParse(
                                                      metadataNameFilterConditionAttrib.Value,
                                                      true,
                                                      FilterCondition.Equals)
                                                  : FilterCondition.Equals;
            }

            var fileMaskAttrib = element.Attribute(ParseTokens.filemask.ToString());
            if (null != fileMaskAttrib)
            {
                FileMask = fileMaskAttrib.Value;
            }
        }

        #region Nested type: ParseTokens

        private enum ParseTokens
        {
            Input,
// ReSharper disable InconsistentNaming
            filemask,
            type,
            metadataType,
            metadataNameFilter,
            metadataNameFilterCondition,
// ReSharper restore InconsistentNaming
        }

        #endregion
    }
}