using System;
using System.CodeDom.Compiler;
using rage.Types;

namespace rage.CodeGeneration
{
    public class GetMaxSizeFunctionWriter
    {
        private readonly IndentedTextWriter m_writer;

        public GetMaxSizeFunctionWriter(IndentedTextWriter writer,
                                        ITypeDefinitionsManager typeDefinitionsManager,
                                        string classFactoryFunctionName)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (typeDefinitionsManager == null)
            {
                throw new ArgumentNullException("typeDefinitionsManager");
            }
            if (string.IsNullOrEmpty(classFactoryFunctionName))
            {
                throw new ArgumentNullException("classFactoryFunctionName");
            }
            m_writer = writer;

            m_writer.WriteLine("// PURPOSE - Gets the size of the largest object");
            using (new BlockWriter(m_writer, Utility.GetMaxSizeDeclaration(classFactoryFunctionName), null, false))
            {
                m_writer.WriteLine("rage::u32 size = 0;");
                WriteBlankLine();

                WriteMaxSizeMacro();

                WriteBlankLine();
                foreach (var typeDefData in typeDefinitionsManager.TypeDefinitionsData)
                {
                    foreach (var typeDef in typeDefData.TypeDefinitions)
                    {
                        if (typeDef.IsFactoryCodeGenerated)
                        {
                            var qualifiedTypeName = string.Concat(typeDefData.ClassPrefix, typeDef.Name);
                            m_writer.WriteLine(string.Concat("MAXSIZE(", qualifiedTypeName, ")"));
                        }
                    }
                }
                WriteBlankLine();

                m_writer.WriteLine("#undef MAXSIZE");
                WriteBlankLine();

                WriteDisplayfStatement();
                m_writer.WriteLine("return size;");
            }
        }

        private void WriteBlankLine()
        {
            m_writer.WriteLine();
        }

        private void WriteDisplayfStatement()
        {
            m_writer.WriteLine("#if !__NO_OUTPUT");
            m_writer.Indent += 1;

            m_writer.WriteLine("Displayf(\"Largest object type: %s (%u bytes)\", largestObjectTypeName, size);");

            m_writer.Indent -= 1;
            m_writer.WriteLine("#endif");
        }

        private void WriteMaxSizeMacro()
        {
            m_writer.WriteLine("#if __NO_OUTPUT");
            m_writer.Indent += 1;

            m_writer.WriteLine("#define MAXSIZE(type) if (sizeof(type) > size) { size = sizeof(type); }");

            m_writer.Indent -= 1;
            m_writer.WriteLine("#else");
            m_writer.Indent += 1;

            m_writer.WriteLine("const char* largestObjectTypeName = NULL;");
            m_writer.WriteLine(
                "#define MAXSIZE(type) if (sizeof(type) > size) { size = sizeof(type); largestObjectTypeName = #type; }");

            m_writer.Indent -= 1;
            m_writer.WriteLine("#endif");
        }
    }
}