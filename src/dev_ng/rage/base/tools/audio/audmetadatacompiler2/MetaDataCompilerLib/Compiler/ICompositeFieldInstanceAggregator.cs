using System.Xml.Linq;

namespace rage.Compiler
{
    public interface ICompositeFieldInstanceAggregator
    {
        void Process(XDocument doc);
    }
}