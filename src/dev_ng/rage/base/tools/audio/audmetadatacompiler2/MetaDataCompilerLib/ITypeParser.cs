using System;

namespace rage
{
    public interface ITypeParser
    {
        Type GetType(string value);
    }
}