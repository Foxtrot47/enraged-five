using System.Xml.Linq;

namespace rage.Enums
{
    public class EnumValue : IEnumValue
    {
        #region IEnumValue Members

        public string Name { get; set; }

        public byte Value { get; set; }

        public bool Parse(XElement valueElement, byte value)
        {
            Value = value;
            Name = valueElement.Value;
            if (string.IsNullOrEmpty(Name))
            {
                return false;
            }
            return true;
        }

        #endregion

        public override string ToString()
        {
            return string.Concat(Name, " = ", Value.ToString());
        }
    }
}