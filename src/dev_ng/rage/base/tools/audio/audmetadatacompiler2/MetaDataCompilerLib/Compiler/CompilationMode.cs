namespace rage.Compiler
{
    public enum CompilationMode
    {
        TransformOnly,
        CompileOnly,
        TransformAndCompile
    }
}