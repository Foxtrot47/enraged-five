using System;
using System.IO;
using System.Linq;
using rage.Types;

namespace rage.CodeGeneration
{
    public sealed class ClassFactoryHeaderFileGenerator : FileGenerator
    {
        private readonly string m_classFactoryFunctionName;

        private readonly string m_classFactoryReturnType;

        private readonly bool m_usesBucketAllocator;

        public ClassFactoryHeaderFileGenerator(string fileName,
                                               string nameSpace,
                                               string metaDataType,
                                               ITypeDefinitionsManager typeDefinitionsManager,
                                               string classFactoryFunctionName,
                                               string classFactoryReturnType,
                                               bool usesBucketAllocator)
            : base(
                fileName,
                nameSpace,
                metaDataType,
                typeDefinitionsManager,
                "Automatically generated sound class factory functions")
        {
            classFactoryFunctionName = classFactoryFunctionName.Trim().Replace(" ", string.Empty);
            classFactoryReturnType = classFactoryReturnType.Trim().Replace(" ", string.Empty);

            if (string.IsNullOrEmpty(classFactoryFunctionName))
            {
                throw new ArgumentNullException("classFactoryFunctionName");
            }
            if (string.IsNullOrEmpty(classFactoryReturnType))
            {
                throw new ArgumentNullException("classFactoryReturnType");
            }
            m_classFactoryFunctionName = classFactoryFunctionName;
            m_classFactoryReturnType = classFactoryReturnType;
            m_usesBucketAllocator = usesBucketAllocator;

            Generate();
        }

        protected override void Generate()
        {
            base.Generate();

            var fileNameNoExtensionUpper = Path.GetFileNameWithoutExtension(FileName).ToUpper();
            using (new IncludeGuardWriter(Writer, string.Concat("AUD_", fileNameNoExtensionUpper, "_H")))
            {
                WriteBlankLine();
                WriteIncludes();
                WriteBlankLine();

                using (new BlockWriter(Writer, NAMESPACE, NameSpace, false))
                {
                    new SoundClassFactoryWriter(Writer,
                                                TypeDefinitionsManager,
                                                m_classFactoryFunctionName,
                                                m_classFactoryReturnType,
                                                m_usesBucketAllocator);
                    WriteBlankLine();
                    new GetMaxSizeFunctionWriter(Writer, TypeDefinitionsManager, m_classFactoryFunctionName);
                    WriteBlankLine();
                    WriteGetTypeNameFunction();
                }
            }
        }

        private void WriteGetTypeNameFunction()
        {
            Writer.WriteLine("// PURPOSE - Gets the name of the type given the type id");
            using (new BlockWriter(Writer, Utility.GetTypeNameDeclaration(m_classFactoryFunctionName), null, false))
            {
                using (new BlockWriter(Writer, "switch(classId)", null, false))
                {
                    foreach (var typeDefData in TypeDefinitionsManager.TypeDefinitionsData)
                    {
                        foreach (var typeDef in typeDefData.TypeDefinitions)
                        {
                            if (typeDef.IsFactoryCodeGenerated)
                            {
                                Writer.WriteLine(string.Concat("case ",
                                                               typeDef.Name,
                                                               "::TYPE_ID: return \"",
                                                               typeDefData.ClassPrefix,
                                                               typeDef.Name,
                                                               "\";"));
                            }
                        }
                    }
                    Writer.WriteLine("default: return NULL;");
                }
            }
        }

        private void WriteIncludes()
        {
            new IncludesWriter(Writer,
                               (from typeDefData in TypeDefinitionsManager.TypeDefinitionsData
                                from typeDef in typeDefData.TypeDefinitions
                                where typeDef.IsFactoryCodeGenerated
                                select string.Concat(typeDefData.ClassIncludePath, "/", typeDef.Name, ".h")).ToList());
        }
    }
}