using System;
using System.Collections.Generic;
using System.Linq;

namespace rage.Types
{
    public class TypeDefData
    {
        private readonly ITypeDefinitionsLoader m_loader;

        public TypeDefData(ITypeDefinitionsLoader loader)
        {
            if (loader == null)
            {
                throw new ArgumentNullException("loader");
            }
            m_loader = loader;
        }

        public string ClassPrefix
        {
            get { return m_loader.ClassPrefix; }
        }

        public string ClassIncludePath
        {
            get { return m_loader.ClassIncludePath; }
        }

        public IEnumerable<ITypeDefinition> TypeDefinitions
        {
            get { return from typeDefEntry in m_loader.TypeDefinitions select typeDefEntry.Value; }
        }
    }
}