using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using rage.ToolLib.Writer;

namespace rage.Compiler
{
    using System.Linq;

    public class CompiledObjectLookup : ICompiledObjectLookup
    {
        private readonly IList<uint> m_objectReferences;

        private readonly IList<CompiledObject> m_objects;

        private readonly IDictionary<string, CompiledObject> m_objectsLookup;

        private readonly IList<uint> m_stringReferences;

        private uint m_totalNameLength;

        public CompiledObjectLookup()
        {
            m_stringReferences = new List<uint>();
            m_objectReferences = new List<uint>();
            m_objectsLookup = new Dictionary<string, CompiledObject>();
            m_objects = new List<CompiledObject>();
        }

        #region ICompiledObjectLookup Members

        public IList<CompiledObject> CompiledObjects
        {
            get { return this.m_objects; }
        }

        public void RecordObjectRef(uint position)
        {
            m_objectReferences.Add(position);
        }

        public void RecordStringRef(uint position)
        {
            m_stringReferences.Add(position);
        }

        public void Serialize(IWriter output, uint mode = 0)
        {
            if (mode == 0)
            {
                // Original file format
                output.Write(m_objects.Count);
                output.Write(m_totalNameLength);
                foreach (var obj in m_objects)
                {
                    output.Write(obj.Name.ToUpper());
                    output.Write(obj.Offset);
                    output.Write(obj.Size);
                }

                SerializeReferences(m_objectReferences, output);
                SerializeReferences(m_stringReferences, output);
            }
            else
            {
                // New file format (hashed object names)
                output.Write(this.m_objects.Count);

                // Sort list by name hash, ascending
                var sortedObjects = new List<CompiledObject>();

                if (mode > 1)
                {
                    var buckets = new SortedDictionary<uint, List<CompiledObject>>();

                    // First put objects in correct bucket based on the low 8 bits of object name hash
                    foreach (var obj in this.m_objects)
                    {
                        var key = obj.NameHash & 0xFF;
                        if (!buckets.ContainsKey(key))
                        {
                            buckets.Add(key, new List<CompiledObject>());
                        }

                        buckets[key].Add(obj);
                    }

                    // Now sort each of the buckets by name hash and add to main sorted list
                    foreach (var bucket in buckets)
                    {
                        var objs = bucket.Value;
                        var sortedObjs = objs.OrderBy(x => x.NameHash);
                        sortedObjects.AddRange(sortedObjs);
                    }
                }
                else
                {
                    sortedObjects = this.m_objects.OrderBy(x => x.NameHash).ToList();
                }

                foreach (var obj in sortedObjects)
                {
                    output.Write(obj.NameHash);
                    output.Write(obj.Offset);
                    output.Write(obj.Size);
                }

                SerializeReferences(m_objectReferences, output);
                SerializeReferences(m_stringReferences, output);
            }
        }

        public void Deserialize(Stream data, bool isBigEndian)
        {
            // number of objects
            var buffer = new byte[4];
            data.Read(buffer, 0, 4);
            if (isBigEndian)
            {
                buffer = ToolLib.Utility.Reverse(buffer);
            }
            var numObjects = BitConverter.ToInt32(buffer, 0);

            // total name length
            data.Read(buffer, 0, 4);
            if (isBigEndian)
            {
                buffer = ToolLib.Utility.Reverse(buffer);
            }
            var totalNameLength = BitConverter.ToUInt32(buffer, 0);

            for (var i = 0; i < numObjects; ++i)
            {
                var compiledObject = new CompiledObject();

                // name
                var strLength = (byte) data.ReadByte();
                var strBuffer = new byte[strLength];
                data.Read(strBuffer, 0, strLength);

                var builder = new StringBuilder();
                foreach (var strByte in strBuffer)
                {
                    builder.Append((char) strByte);
                }
                compiledObject.Name = builder.ToString();

                // offset
                data.Read(buffer, 0, 4);
                if (isBigEndian)
                {
                    buffer = ToolLib.Utility.Reverse(buffer);
                }
                compiledObject.Offset = BitConverter.ToUInt32(buffer, 0);

                // size
                data.Read(buffer, 0, 4);
                if (isBigEndian)
                {
                    buffer = ToolLib.Utility.Reverse(buffer);
                }
                compiledObject.Size = BitConverter.ToUInt32(buffer, 0);

                Record(compiledObject);
            }

            if (totalNameLength != m_totalNameLength)
            {
                throw new ApplicationException("Failed deserialization!");
            }

            DeserializeReferences(RecordObjectRef, data, isBigEndian);
            DeserializeReferences(RecordStringRef, data, isBigEndian);
        }

        public void WriteNextNameOffset(IWriter output)
        {
            var nameTableOffsetBytes = BitConverter.GetBytes(m_totalNameLength);
            var nameTableOffset3Bytes = new[]
                                            {
                                                nameTableOffsetBytes[0], nameTableOffsetBytes[1],
                                                nameTableOffsetBytes[2]
                                            };
            output.Write(nameTableOffset3Bytes, true);
        }

        public void Record(CompiledObject compiledObject)
        {
            m_objects.Add(compiledObject);
            m_objectsLookup[compiledObject.Name] = compiledObject;
            m_totalNameLength += (uint) (compiledObject.Name.Length + 1);
        }

        public bool Exists(string name)
        {
            return m_objectsLookup.ContainsKey(name);
        }

        public uint? GetObjectSize(string name)
        {
            CompiledObject obj;
            if (m_objectsLookup.TryGetValue(name, out obj))
            {
                return obj.Size;
            }
            return null;
        }

        public string GetNameFromOffset(uint offset)
        {
            uint currentOffset = 0;
            foreach (var obj in m_objects)
            {
                if (currentOffset == offset)
                {
                    return obj.Name;
                }
                currentOffset = (uint) (currentOffset + (obj.Name.Length + 1));
            }
            return null;
        }

        public void Clear()
        {
            m_stringReferences.Clear();
            m_objectReferences.Clear();
            m_objectsLookup.Clear();
            m_objects.Clear();
            m_totalNameLength = 0;
        }

        public void UpdateObjectSize(string name, uint offset, uint size)
        {
            CompiledObject obj;
            if (m_objectsLookup.TryGetValue(name, out obj))
            {
                obj.Size = size;
            }
            else
            {
                obj = new CompiledObject {Name = name, Offset = offset, Size = size};
                m_objectsLookup[name] = obj;
                m_objects.Add(obj);
            }
        }

        #endregion

        private static void DeserializeReferences(Action<uint> recordFunc, Stream data, bool isBigEndian)
        {
            var buffer = new byte[4];
            data.Read(buffer, 0, 4);
            if (isBigEndian)
            {
                buffer = ToolLib.Utility.Reverse(buffer);
            }
            var numReferences = BitConverter.ToInt32(buffer, 0);

            for (var i = 0; i < numReferences; ++i)
            {
                data.Read(buffer, 0, 4);
                if (isBigEndian)
                {
                    buffer = ToolLib.Utility.Reverse(buffer);
                }
                recordFunc(BitConverter.ToUInt32(buffer, 0));
            }
        }

        private static void SerializeReferences(ICollection<uint> references, IWriter output)
        {
            output.Write(references.Count);
            foreach (var reference in references)
            {
                output.Write(reference);
            }
        }
    }
}