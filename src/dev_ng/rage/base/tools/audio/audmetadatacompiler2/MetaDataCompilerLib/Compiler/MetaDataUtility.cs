using System;
using System.Linq;

namespace rage.Compiler
{
    public class MetaDataUtility
    {
        public const string FORMAT_METADATA_NOT_FOUND =
            "Metadata type \"{0}\" was not found in the project settings file.";

        public const string FORMAT_CURRENT_DIRECTORY = "Current Directory = {0}";

        public static string GetNamespace(string nameSpace, audMetadataType metaDataType)
        {
            // Namespace in file overrides the namespace specified
            if (!string.IsNullOrEmpty(metaDataType.NameSpaceName))
            {
                return metaDataType.NameSpaceName;
            }
            return nameSpace;
        }

        public static audMetadataType GetMetaDataType(string metaData, audProjectSettings projectSettings)
        {
            return
                projectSettings.GetMetadataTypes().FirstOrDefault(
                    metaDataType => String.Compare(metaDataType.Type, metaData, true) == 0);
        }
    }
}