using System;
using System.CodeDom.Compiler;
using rage.Types;

namespace rage.CodeGeneration
{
    public class GetAddressMacroWriter
    {
        private readonly string m_fileName;

        private readonly ITypeDefinitionsManager m_typeDefinitionsManager;

        private readonly IndentedTextWriter m_writer;

        public GetAddressMacroWriter(IndentedTextWriter writer,
                                     string fileName,
                                     ITypeDefinitionsManager typeDefinitionsManager)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (typeDefinitionsManager == null)
            {
                throw new ArgumentNullException("typeDefinitionsManager");
            }
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }
            m_writer = writer;
            m_typeDefinitionsManager = typeDefinitionsManager;
            m_fileName = fileName.ToUpper();

            Write();
        }

        private void Write()
        {
            m_writer.WriteLine(string.Concat("#define AUD_DECLARE_",
                                             m_fileName,
                                             "_FUNCTION(fn) void* GetAddressOf_##fn(rage::u8 classId);"));
            m_writer.WriteLine();

            m_writer.WriteLine(string.Concat("#define AUD_DEFINE_", m_fileName, "_FUNCTION(fn) \\"));
            m_writer.WriteLine("void* GetAddressOf_##fn(rage::u8 classId) \\");
            m_writer.WriteLine("{ \\");

            m_writer.Indent += 1;
            m_writer.WriteLine("switch(classId) \\");
            m_writer.WriteLine("{ \\");
            m_writer.Indent += 1;

            // output even if abstract
            foreach (var typeDefData in m_typeDefinitionsManager.TypeDefinitionsData)
            {
                foreach (var typeDef in typeDefData.TypeDefinitions)
                {
                    if (!typeDef.IsAbstract &&
                        typeDef.IsFactoryCodeGenerated)
                    {
                        m_writer.WriteLine(string.Concat("case ",
                                                         typeDef.Name,
                                                         "::TYPE_ID: return (void*)&",
                                                         typeDefData.ClassPrefix,
                                                         typeDef.Name,
                                                         "::s##fn; \\"));
                    }
                }
            }

            m_writer.WriteLine("default: return NULL; \\");
            m_writer.Indent -= 1;
            m_writer.WriteLine("} \\");
            m_writer.Indent -= 1;

            m_writer.WriteLine("}");
        }
    }
}