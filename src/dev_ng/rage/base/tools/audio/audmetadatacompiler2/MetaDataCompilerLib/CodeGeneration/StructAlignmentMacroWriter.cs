using System;
using System.CodeDom.Compiler;

namespace rage.CodeGeneration
{
    public class StructAlignmentMacroWriter : IDisposable
    {
        private readonly IndentedTextWriter m_writer;

        private bool m_disposed;

        public StructAlignmentMacroWriter(IndentedTextWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            m_writer = writer;
            m_writer.WriteLine("// disable struct alignment");
            m_writer.WriteLine("#if !__SPU");
            m_writer.WriteLine("#pragma pack(push, r1, 1)");
            m_writer.WriteLine("#endif // !__SPU");
            m_writer.Indent += 1;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (!m_disposed)
            {
                m_disposed = true;
                m_writer.Indent -= 1;
                m_writer.WriteLine("// enable struct alignment");
                m_writer.WriteLine("#if !__SPU");
                m_writer.WriteLine("#pragma pack(pop, r1)");
                m_writer.WriteLine("#endif // !__SPU");
            }
        }

        #endregion
    }
}