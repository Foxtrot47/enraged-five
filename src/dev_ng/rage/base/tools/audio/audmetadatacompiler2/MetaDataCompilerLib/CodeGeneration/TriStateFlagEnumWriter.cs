using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using rage.Enums;
using rage.Fields;
using rage.Types;

namespace rage.CodeGeneration
{
    public class TriStateFlagEnumWriter
    {
        public TriStateFlagEnumWriter(IndentedTextWriter writer, ITypeDefinition typeDef)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (typeDef == null)
            {
                throw new ArgumentNullException("typeDef");
            }

            byte value = 0;
            var enumValues = new List<IEnumValue>();
            var triStateFields = from fieldDef in typeDef.AllFieldsInherited
                                 where fieldDef.Type.TypeHandle.Equals(typeof (TriState).TypeHandle)
                                 select fieldDef;
            foreach (var triStateField in triStateFields)
            {
                var name = string.Concat("FLAG_ID_", typeDef.Name.ToUpper(), "_", triStateField.Name.ToUpper());
                enumValues.Add(new EnumValue {Name = name, Value = value});
                value += 1;
            }

            // Add additional 'count' value (B* 1236575)
            var maxName = string.Format("FLAG_ID_{0}_MAX", typeDef.Name.ToUpper());
            enumValues.Add(new EnumValue { Name = maxName, Value = value });

            new EnumWriter(writer, string.Concat(typeDef.Name, "FlagIds"), enumValues);
        }
    }
}