using System.Collections.Generic;
using System.Xml.Linq;
using rage.Types;

namespace rage.Templates
{
    public interface ITemplate
    {
        string Name { get; }

        IEnumerable<KeyValuePair<ITypeDefinition, XElement>> Elements { get; }

        bool Parse(XElement templateElement);
    }
}