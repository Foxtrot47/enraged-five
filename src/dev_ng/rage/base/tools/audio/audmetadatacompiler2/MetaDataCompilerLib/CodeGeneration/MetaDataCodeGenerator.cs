using System;
using rage.Compiler;
using rage.Reflection;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.CodeGeneration
{
    public class MetaDataCodeGenerator : IMetaDataCodeGenerator
    {
        private readonly ILog m_log;

        private readonly IReflector m_reflector;

        private audProjectSettings m_projectSettings;

        private ITypeDefinitionsManager m_typeDefinitionsManager;

        public MetaDataCodeGenerator(ILog log, IReflector reflector, audProjectSettings projectSettings)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (reflector == null)
            {
                throw new ArgumentNullException("reflector");
            }
            if(projectSettings == null) 
            {
                throw new ArgumentNullException("projectSettings");
            }
            m_log = log;
            m_reflector = reflector;
            m_projectSettings = projectSettings;
        }

        #region IMetaDataCodeGenerator Members

        public bool LoadTypeDefinitions(audMetadataType metaDataType,
                                        ICompiledObjectLookup compiledObjectLookup,
                                        IStringTable stringTable,
                                        string workingDirectory)
        {
            m_typeDefinitionsManager = new TypeDefinitionsManager(m_log, m_reflector, compiledObjectLookup, stringTable);
            return m_typeDefinitionsManager.Load(workingDirectory, metaDataType.ObjectDefinitions);
        }

        public bool GenerateCode(audMetadataType metaDataType, string path, string nameSpace)
        {
            if (m_typeDefinitionsManager == null)
            {
                throw new InvalidOperationException(
                    "Attempting to generate code without loading type definitions first.");
            }

            var objectCodeGenerator = new ObjectCodeGenerator(m_log,
                                                              m_projectSettings,
                                                              m_typeDefinitionsManager,
                                                              metaDataType);
            return objectCodeGenerator.Generate(path, nameSpace);
        }

        #endregion
    }
}