using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.Transform
{
    public class TransformAndValidatorProcessor : ITransformAndValidatorProcessor
    {
        private const string FORMAT_COULD_NOT_FIND_TYPE_DEFINITION =
            "Could not find TypeDefinition for :{0}. This object will not be transformed or validated.";

        private const string FORMAT_TRANSFORMER_FAILED = "Transformer for TypeDefinition: \"{0}\" failed!";
        private const string WARNING = "Warning: ";
        private const string ERROR = "Error: ";
        private const string FORMAT_VALIDATOR_NO_RESULT = "Validator: {0} did not return a ValidationResult object.";
        private const string FORMAT_VALIDATION_WARNINGS = "Validation produced the following warnings:\n{0}";
        private const string FORMAT_VALIDATION_ERRORS = "Validation failed with the following errors:\n{0}";
        private const string FORMAT_TRANSFORMING_AND_VALIDATING = "Transforming and Validating: {0}.";
        private const string NAME = "name";

        private readonly ILog m_log;
        private readonly Func<string, ITypeDefinition> m_typeDefinitionFinderFunc;

        public TransformAndValidatorProcessor(ILog log, Func<string, ITypeDefinition> typeDefinitionFinderFunc)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (typeDefinitionFinderFunc == null)
            {
                throw new ArgumentNullException("typeDefinitionFinderFunc");
            }
            m_log = log;
            m_typeDefinitionFinderFunc = typeDefinitionFinderFunc;
        }

        #region ITransformAndValidatorProcessor Members

        public bool Process(XDocument doc, bool shouldTransform)
        {
            if (doc.Root == null ||
                doc.Root.Elements().Count() <= 0)
            {
                return true;
            }

            var elementsToRemove = new List<XElement>();
            var rootElements = doc.Root.Elements().ToList();
            foreach (var element in rootElements)
            {
                m_log.PushContext(ContextType.Object, element.Attribute(NAME).Value);
                m_log.Information(FORMAT_TRANSFORMING_AND_VALIDATING, element.Name);
                if (!TransformAndValidate(doc, element, shouldTransform))
                {
                    m_log.PopContext();
                    return false;
                }
                elementsToRemove.Add(element);
                m_log.PopContext();
            }

            foreach (var elementToRemove in elementsToRemove)
            {
                elementToRemove.Remove();
            }
            return true;
        }

        #endregion

        private bool TransformAndValidate(XDocument doc, XElement element, bool shouldTransform)
        {
            ITypeDefinition oldTypeDefinition;
            ITypeDefinition typeDefinition = null;
            var transformedElement = new TransformResult {Result = element};
            do
            {
                oldTypeDefinition = typeDefinition;

                typeDefinition = m_typeDefinitionFinderFunc(transformedElement.Result.Name.ToString());
                if (typeDefinition == null)
                {
                    m_log.Warning(FORMAT_COULD_NOT_FIND_TYPE_DEFINITION, transformedElement.Result.Name.ToString());
                    transformedElement.Result = element;
                    break;
                }

                if (!RunValidators(transformedElement, typeDefinition))
                {
                    return false;
                }

                if (typeDefinition == oldTypeDefinition ||
                    !shouldTransform)
                {
                    break;
                }

                // Transform in inheritance hierarchy order from top to bottom
                var transformerTypeDefinitions = GetTransformerTypeDefinitions(typeDefinition);
                foreach (var transformerTypeDefinition in transformerTypeDefinitions)
                {
                    transformedElement = transformerTypeDefinition.Transformer.Transform(m_log,
                                                                                         transformedElement.Result);
                    if (transformedElement == null)
                    {
                        m_log.Error(FORMAT_TRANSFORMER_FAILED, typeDefinition.Name);
                        return false;
                    }

                    if (transformedElement.Result == null)
                    {
                        // remove the object from the compilation
                        return true;
                    }

                    // Validate after every transform
                    if (!RunValidators(transformedElement, transformerTypeDefinition))
                    {
                        return false;
                    }
                }
            }
            while (typeDefinition != oldTypeDefinition);

            doc.Root.Add(transformedElement.Result);
            return true;
        }

        private static IEnumerable<ITypeDefinition> GetTransformerTypeDefinitions(ITypeDefinition typeDefinition)
        {
            var transformerTypeDefinitions = new LinkedList<ITypeDefinition>();
            while (typeDefinition != null)
            {
                if (typeDefinition.Transformer != null)
                {
                    transformerTypeDefinitions.AddFirst(typeDefinition);
                }
                typeDefinition = typeDefinition.InheritsFrom;
            }
            return transformerTypeDefinitions;
        }

        private bool RunValidators(TransformResult transformedElement, ITypeDefinition typeDef)
        {
            while (typeDef != null)
            {
                if (typeDef.Validator != null)
                {
                    var validationResult = typeDef.Validator.Validate(transformedElement.Result);
                    if (validationResult == null)
                    {
                        m_log.Error(FORMAT_VALIDATOR_NO_RESULT, typeDef.Validator.GetType().AssemblyQualifiedName);
                        return false;
                    }

                    if (validationResult.HasWarnings)
                    {
                        m_log.Warning(FORMAT_VALIDATION_WARNINGS, GetValidationWarnings(validationResult));
                    }

                    if (validationResult.HasErrors)
                    {
                        m_log.Error(FORMAT_VALIDATION_ERRORS, GetValidationErrors(validationResult));
                        return false;
                    }
                }
                typeDef = typeDef.InheritsFrom;
            }
            return true;
        }

        private static string GetValidationWarnings(ValidationResult validationResult)
        {
            var builder = new StringBuilder();
            foreach (var warning in validationResult.Warnings)
            {
                builder.Append(WARNING);
                builder.Append(warning);
                builder.Append(Environment.NewLine);
            }
            return builder.ToString();
        }

        private static string GetValidationErrors(ValidationResult validationResult)
        {
            var builder = new StringBuilder();
            foreach (var error in validationResult.Errors)
            {
                builder.Append(ERROR);
                builder.Append(error);
                builder.Append(Environment.NewLine);
            }
            return builder.ToString();
        }
    }
}