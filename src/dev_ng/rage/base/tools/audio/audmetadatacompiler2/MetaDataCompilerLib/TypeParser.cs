using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace rage
{
    public class TypeParser : ITypeParser
    {
        private const string TYPE_MATCHER_REGEX = "Type=\".+?\"";

        private static readonly Regex ms_typeMatcher;

        private List<Type> reflectedTypes = new List<Type>(); 

        static TypeParser()
        {
            ms_typeMatcher = new Regex(TYPE_MATCHER_REGEX);
        }

        public TypeParser(List<Type> reflectedTypes)
        {
            this.reflectedTypes.AddRange(reflectedTypes);
        }

        #region ITypeParser Members

        public Type GetType(string value)
        {
            foreach (var match in ms_typeMatcher.Matches(value))
            {
                var typeStr = GetTypeStr(match.ToString());

                foreach (Type reflectedType in reflectedTypes)
                {
                    if (reflectedType.AssemblyQualifiedName.Equals(typeStr)) return reflectedType;
                }

                
                var type = Type.GetType(typeStr, true, true);
                return type;
            }
            return null;
        }

        #endregion

        private static string GetTypeStr(string matchStr)
        {
            return matchStr.Replace("Type=", string.Empty).Replace("\"", string.Empty);
        }
    }
}