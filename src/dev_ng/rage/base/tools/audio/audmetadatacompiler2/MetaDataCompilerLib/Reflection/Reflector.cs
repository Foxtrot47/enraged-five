using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using rage.Generator;
using rage.Serialization;
using rage.ToolLib.Logging;
using rage.ToolLib.Reflection;
using rage.Transform;

namespace rage.Reflection
{
    public class Reflector : IReflector
    {
        private const string COMPILED_ASSEMBLY_NAME = "SerializerAndTransformerCompiledAssembly.dll";
        private const string REFLECTING_OVER_ASSEMBLY = "Reflecting over assembly.";
        private const string AUD_PROJECT_SETTINGS = "audprojectsettings";
        private const string TOOL_LIB = "ToolLib";
        private const string FORMAT_JIT_PATH_ADDED = "Added JIT Path: {0}";
        private const string FORMAT_NO_DEFAULT_CTOR_SERIALIZER = "Missing default constructor on ISerializer: {0}";
        private const string FORMAT_FOUND_SERIALIZER = "Found ISerializer: {0}";
        private const string FORMAT_NO_DEFAULT_CTOR_VALIDATOR = "Missing default constructor on IValidator: {0}";
        private const string FORMAT_FOUND_VALIDATOR = "Found IValidator: {0}";
        private const string FORMAT_NO_DEFAULT_CTOR_TRANSFORMER = "Missing default constructor on ITransformer: {0}";
        private const string FORMAT_FOUND_TRANSFORMER = "Found ITransformer: {0}";
        private const string FORMAT_NO_DEFAULT_CTOR_GENERATOR = "Missing default constructor on IGenerator: {0}";
        private const string FORMAT_FOUND_GENERATOR = "Found IGenerator: {0}";

        private const string REFLECTING_FOR_TYPES =
            "Reflecting for ISerializer, ITransformer, IValidator and IGenerator types...";

        private const string FORMAT_REFLECTION_COMPLETED =
            "Found {0} ISerializers, {1} ITransformers, {2} IValidators and {3} IGenerators";

        private const string FORMAT_MISSING_ATTRIBUTE_ON_SERIALIZER =
            "Missing \"CanSerialize\" attribute on ISerializer: {0}";

        private const string FORMAT_MISSING_ATTRIBUTE_ON_VALIDATOR =
            "Missing \"CanValidate\" attribute on IValidator: {0}";

        private const string FORMAT_MISSING_ATTRIBUTE_ON_TRANSFORMER =
            "Missing \"CanTransform\" attribute on ITransformer: {0}";

        private readonly HashSet<Type> m_generators;
        private readonly string[] m_jitPaths;
        private readonly ILog m_log;
        private readonly Dictionary<Type, Type> m_serializers;
        private readonly Dictionary<Type, HashSet<string>> m_transformers;
        private readonly Dictionary<Type, HashSet<string>> m_validators;

        public Reflector(ILog log, string[] jitPaths)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
            m_jitPaths = jitPaths;
            m_serializers = new Dictionary<Type, Type>();
            m_transformers = new Dictionary<Type, HashSet<string>>();
            m_validators = new Dictionary<Type, HashSet<string>>();
            m_generators = new HashSet<Type>();

            if (m_jitPaths != null)
            {
                foreach (var jitPath in m_jitPaths)
                {
                    m_log.WriteFormatted(FORMAT_JIT_PATH_ADDED, jitPath);
                }
            }
            Process(new Dictionary<Type, object>());
        }

        #region IReflector Members

        /// <summary>
        ///   Attempts to create an ITransformer instance of type <paramref name = "transformerType" />
        /// </summary>
        /// <param name = "transformerType">The type of ITransformer</param>
        /// <returns>An instance if successful, null otherwise</returns>
        public ITransformer CreateTransformer(Type transformerType)
        {
            return m_transformers.ContainsKey(transformerType) ? CreateInstance<ITransformer>(transformerType) : null;
        }

        /// <summary>
        ///   Attempts to create an ISerializer instance of type <paramref name = "serializerType" />
        /// </summary>
        /// <param name = "serializerType">The type of ISerializer</param>
        /// <returns>An instance if successful, null otherwise</returns>
        public ISerializer CreateSerializer(Type serializerType)
        {
            return m_serializers.ContainsKey(serializerType) ? CreateInstance<ISerializer>(serializerType) : null;
        }

        /// <summary>
        ///   Attempts to create an IValidator instances of type <paramref name = "validatorType" />
        /// </summary>
        /// <param name = "validatorType">The type of IValidator</param>
        /// <returns>An instance if successful, null otherwise</returns>
        public IValidator CreateValidator(Type validatorType)
        {
            return m_validators.ContainsKey(validatorType) ? CreateInstance<IValidator>(validatorType) : null;
        }

        /// <summary>
        ///   Attempts to create an IGenerator instance of type <paramref name = "generatorType" />
        /// </summary>
        /// <param name = "generatorType">The type of IGenerator</param>
        /// <returns>An instance if successful, null otherwise</returns>
        public IGenerator CreateGenerator(Type generatorType)
        {
            return m_generators.Contains(generatorType) ? CreateInstance<IGenerator>(generatorType) : null;
        }

        /// <summary>
        ///   Checks if an ISerializer of type <paramref name = "serializerType" />
        ///   can serialize an element of type <paramref name = "targetType" />
        /// </summary>
        /// <param name = "targetType">The Xml element type</param>
        /// <param name = "serializerType">The type of the ISerializer</param>
        /// <returns></returns>
        public bool CanBeSerializedBy(Type targetType, Type serializerType)
        {
            Type canSerialize;
            if (m_serializers.TryGetValue(serializerType, out canSerialize))
            {
                return canSerialize.TypeHandle.Equals(targetType.TypeHandle) || targetType.IsSubclassOf(canSerialize);
            }
            return false;
        }

        /// <summary>
        ///   Checks if an ITransformer of type <paramref name = "transformerType" /> 
        ///   can transform an element of type <paramref name = "elementType" />
        /// </summary>
        /// <param name = "elementType">The source Xml element type</param>
        /// <param name = "transformerType">The transformer type</param>
        /// <returns>true if the transform is possible, false otherwise</returns>
        public bool CanBeTransformedBy(string elementType, Type transformerType)
        {
            HashSet<string> canTransform;
            if (m_transformers.TryGetValue(transformerType, out canTransform))
            {
                // Check for '*' - indicates we can transform all types
                if (canTransform.Contains("*"))
                {
                    return true;
                }

                return canTransform.Contains(elementType);
            }
            return false;
        }

        /// <summary>
        ///   Checks if an ITransformer of type <paramref name = "validatorType" />
        ///   can validate an element of type <paramref name = "elementType" />
        /// </summary>
        /// <param name = "elementType">The Xml element type</param>
        /// <param name = "validatorType">The type of the ITransformer</param>
        /// <returns></returns>
        public bool CanBeValidatedBy(string elementType, Type validatorType)
        {
            HashSet<string> canValidate;
            if (m_validators.TryGetValue(validatorType, out canValidate))
            {
                return canValidate.Contains(elementType);
            }
            return false;
        }

        #endregion

        private void Process(IDictionary<Type, object> instanceLookup)
        {
            var assemblies = new AssemblyLoader(m_log).Load(m_jitPaths);

            List<string> cSharpFiles = CSharpCodeCompiler.GetCodeFiles(m_jitPaths);
            string assemblyInfo = null;
            foreach (string cSharpFile in cSharpFiles)
            {
                if (cSharpFile.EndsWith("AssemblyInfo.cs")) assemblyInfo = cSharpFile;
            }


            foreach (string cSharpFile in cSharpFiles)
            {
                List<string> filesToCompile = new List<string>();
                if (assemblyInfo != null)
                {
                    if(cSharpFile.Equals(assemblyInfo)) continue;
                    filesToCompile.Add(assemblyInfo);
                }

                filesToCompile.Add(cSharpFile);
                int lastIndexOfSeparator = cSharpFile.LastIndexOf("\\")+1;
                string filename = cSharpFile.Substring(lastIndexOfSeparator);
                filename = filename.Replace(".cs", ".dll");
                var compiledAssembly = new CSharpCodeCompiler(m_log).CompileFiles(filesToCompile,
                                                                         filename,
                                                                         AppDomain.CurrentDomain.GetAssemblies());
                if (compiledAssembly != null)
                {
                    assemblies.Add(compiledAssembly);
                }
            }
            
            
            Reflect(FilterUnsuitableAssemblies(assemblies), instanceLookup);
        }

        private static IEnumerable<Assembly> FilterUnsuitableAssemblies(IEnumerable<Assembly> assemblies)
        {
            return assemblies.Where(x => !IsUnsuitableAssembly(x.FullName));
        }

        private static bool IsUnsuitableAssembly(string name)
        {
            return (name.StartsWith(AUD_PROJECT_SETTINGS) || name.StartsWith(TOOL_LIB));
        }

        private void Reflect(IEnumerable<Assembly> assemblies, IDictionary<Type, object> instances)
        {
            m_log.WriteFormatted(string.Empty);
            m_log.WriteFormatted(REFLECTING_FOR_TYPES);
            foreach (var assembly in assemblies)
            {
                try
                {
                    m_log.PushContext(ContextType.File, assembly.GetName().ToString());
                    m_log.Information(REFLECTING_OVER_ASSEMBLY);
                    var types = assembly.GetTypes();
                    foreach (var type in types)
                    {
                        if (!CanBeInstantiated(type))
                        {
                            continue;
                        }

                        ProcessGeneratorType(type, instances);
                        ProcessSerializerType(type, instances);
                        ProcessType<ITransformer, CanTransformAttribute>(type,
                                                                         instances,
                                                                         m_transformers,
                                                                         FORMAT_NO_DEFAULT_CTOR_TRANSFORMER,
                                                                         FORMAT_FOUND_TRANSFORMER,
                                                                         FORMAT_MISSING_ATTRIBUTE_ON_TRANSFORMER);
                        ProcessType<IValidator, CanValidateAttribute>(type,
                                                                      instances,
                                                                      m_validators,
                                                                      FORMAT_NO_DEFAULT_CTOR_VALIDATOR,
                                                                      FORMAT_FOUND_VALIDATOR,
                                                                      FORMAT_MISSING_ATTRIBUTE_ON_VALIDATOR);
                    }
                }
                catch (Exception ex)
                {
                    m_log.Exception(ex);
                }
                m_log.PopContext();
            }
            m_log.WriteFormatted(FORMAT_REFLECTION_COMPLETED,
                                 m_serializers.Count,
                                 m_transformers.Count,
                                 m_validators.Count,
                                 m_generators.Count);
            m_log.WriteFormatted(string.Empty);
        }

        private void ProcessGeneratorType(Type type, IDictionary<Type, object> instances)
        {
            var interfaces = type.FindInterfaces((x, y) => x.TypeHandle.Equals(typeof (IGenerator).TypeHandle), null);
            if (interfaces.Length == 1)
            {
                var generator = TryGenerateInstance<IGenerator>(type, instances, FORMAT_NO_DEFAULT_CTOR_GENERATOR);
                if (generator != null)
                {
                    if (!m_generators.Contains(type))
                    {
                        m_generators.Add(type);
                        m_log.Information(FORMAT_FOUND_GENERATOR, type.FullName);
                    }
                }
            }
        }

        private void ProcessType<TInterface, TAttribute>(Type type,
                                                         IDictionary<Type, object> instances,
                                                         IDictionary<Type, HashSet<string>> lookup,
                                                         string formatMissingDefaultCtor,
                                                         string formatFound,
                                                         string formatMissingAttribute) where TInterface : class
            where TAttribute : class, IElementTypeProvider
        {
            var interfaces = type.FindInterfaces((x, y) => x.TypeHandle.Equals(typeof (TInterface).TypeHandle), null);
            if (interfaces.Length == 1)
            {
                var instance = TryGenerateInstance<TInterface>(type, instances, formatMissingDefaultCtor);
                if (instance != null)
                {
                    var attributes = type.GetCustomAttributes(typeof (TAttribute), true);
                    if (attributes.Length > 0)
                    {
                        lookup[type] =
                            new HashSet<string>(attributes.Select(x => (x as IElementTypeProvider).GetElementType()));
                        m_log.Information(formatFound, type.FullName);
                    }
                    else
                    {
                        m_log.Warning(formatMissingAttribute, type.FullName);
                    }
                }
            }
        }

        private void ProcessSerializerType(Type type, IDictionary<Type, object> instances)
        {
            var interfaces = type.FindInterfaces((x, y) => x.TypeHandle.Equals(typeof (ISerializer).TypeHandle), null);
            if (interfaces.Length == 1)
            {
                var attributes = type.GetCustomAttributes(typeof (CanSerializeAttribute), true);
                if (attributes.Length > 0)
                {
                    var canSerializeAttribute = attributes[0] as CanSerializeAttribute;
                    if (!canSerializeAttribute.IsSystemSerializer)
                    {
                        var serializer = TryGenerateInstance<ISerializer>(type,
                                                                          instances,
                                                                          FORMAT_NO_DEFAULT_CTOR_SERIALIZER);
                        if (serializer != null)
                        {
                            AddSerializer(type, canSerializeAttribute);
                        }
                    }
                    else
                    {
                        AddSerializer(type, canSerializeAttribute);
                    }
                }
                else
                {
                    m_log.Warning(FORMAT_MISSING_ATTRIBUTE_ON_SERIALIZER, type.FullName);
                }
            }
        }

        private void AddSerializer(Type type, CanSerializeAttribute canSerializeAttribute)
        {
            m_serializers[type] = canSerializeAttribute.GetSerializableType();
            m_log.Information(FORMAT_FOUND_SERIALIZER, type.FullName);
        }

        private static bool CanBeInstantiated(Type type)
        {
            return (type.IsClass && !type.IsAbstract);
        }

        private TInstanceType TryGenerateInstance<TInstanceType>(Type type,
                                                                 IDictionary<Type, object> instanceLookup,
                                                                 string formatWarning) where TInstanceType : class
        {
            TInstanceType instance = null;
            if (instanceLookup.ContainsKey(type))
            {
                instance = instanceLookup[type] as TInstanceType;
            }
            else
            {
                var ctor = type.GetConstructor(Type.EmptyTypes);
                if (ctor == null)
                {
                    m_log.Warning(formatWarning, type.FullName);
                }
                else
                {
                    instance = CreateInstance<TInstanceType>(type);
                    if (instance != null)
                    {
                        instanceLookup.Add(instance.GetType(), instance);
                    }
                }
            }
            return instance;
        }

        private static TInstanceType CreateInstance<TInstanceType>(Type type) where TInstanceType : class
        {
            return Activator.CreateInstance(type) as TInstanceType;
        }


        public List<Type> getReflectedTypes()
        {
            List<Type> reflectedTypes = new List<Type>();

            reflectedTypes.AddRange(m_generators);
            reflectedTypes.AddRange(m_serializers.Keys);
            reflectedTypes.AddRange(m_transformers.Keys);
            reflectedTypes.AddRange(m_validators.Keys);

            return reflectedTypes;
        }
    }
}