namespace rage.Generator
{
    using System.Xml.Linq;

    using rage.ToolLib.Logging;
    using rage.Types;

    public interface IGenerator
    {
        bool Init(ILog log, string workingPath, audProjectSettings projectSettings, ITypeDefinitionsManager typeDefManager, params string[] args);

        XDocument Generate(ILog log, XDocument inputDoc);

        void Shutdown();
    }
}