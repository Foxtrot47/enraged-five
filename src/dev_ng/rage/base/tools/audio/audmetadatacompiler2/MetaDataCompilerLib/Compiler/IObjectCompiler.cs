using System.Xml.Linq;
using rage.ToolLib.Writer;

namespace rage.Compiler
{
    public interface IObjectCompiler
    {
        bool CompileObjects(XDocument document, IWriter output, bool recordCompiledObject, bool shouldTransform);
    }
}