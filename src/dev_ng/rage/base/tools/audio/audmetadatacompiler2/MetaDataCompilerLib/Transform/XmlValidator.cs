using System.Linq;
using System.Xml.Linq;

namespace rage.Transform
{
    [CanValidate("*")]
    public class XmlValidator : IValidator
    {
        private const string WARNING = "Warning";

        private const string ERROR = "Error";

        #region IValidator Members

        public ValidationResult Validate(XElement element)
        {
            var result = new ValidationResult();
            var warningsAndErrors = from child in element.Descendants()
                                    where child.Name == WARNING || child.Name == ERROR
                                    select child;
            foreach (var warningOrError in warningsAndErrors)
            {
                switch (warningOrError.Name.ToString())
                {
                    case WARNING:
                        {
                            result.Warnings.Add(warningOrError.Value);
                            break;
                        }
                    case ERROR:
                        {
                            result.Errors.Add(warningOrError.Value);
                            break;
                        }
                }
            }
            return result;
        }

        #endregion
    }
}