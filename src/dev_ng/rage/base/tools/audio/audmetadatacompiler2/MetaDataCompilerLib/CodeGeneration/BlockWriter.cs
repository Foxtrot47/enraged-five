using System;
using System.CodeDom.Compiler;

namespace rage.CodeGeneration
{
    public class BlockWriter : IDisposable
    {
        private readonly string m_dataAfterClosingBracket;

        private readonly string m_name;

        private readonly bool m_semiColonRequired;

        private readonly string m_type;

        private readonly IndentedTextWriter m_writer;

        private bool m_disposed;

        public BlockWriter(IndentedTextWriter writer, string type, string name, bool semiColonRequired)
            : this(writer, type, name, null, semiColonRequired)
        {
        }

        public BlockWriter(IndentedTextWriter writer,
                           string type,
                           string name,
                           string dataAfterClosingBracket,
                           bool semiColonRequired)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (string.IsNullOrEmpty(type))
            {
                throw new ArgumentNullException("type");
            }

            m_writer = writer;
            m_type = type;
            m_name = name;
            m_dataAfterClosingBracket = dataAfterClosingBracket;
            m_semiColonRequired = semiColonRequired;

            var nameText = string.IsNullOrEmpty(name) ? string.Empty : string.Concat(" ", name);
            m_writer.WriteLine(string.Concat(type, nameText));
            m_writer.WriteLine("{");
            m_writer.Indent += 1;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (!m_disposed)
            {
                m_disposed = true;
                m_writer.Indent -= 1;
                var dataAfterClosingBracket = string.IsNullOrEmpty(m_dataAfterClosingBracket)
                                                  ? string.Empty
                                                  : string.Concat(" ", m_dataAfterClosingBracket);
                var dataAfterSemiColon = string.IsNullOrEmpty(m_name)
                                             ? string.Empty
                                             : string.Concat(" // ", m_type, " ", m_name);
                m_writer.WriteLine(string.Concat("}",
                                                 dataAfterClosingBracket,
                                                 m_semiColonRequired ? ";" : string.Empty,
                                                 dataAfterSemiColon));
            }
        }

        #endregion
    }
}