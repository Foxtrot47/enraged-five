using System;

namespace rage.Fields
{
    [Flags]
    public enum TriState : uint
    {
        False,
        True,
        Unspecified
    }
}