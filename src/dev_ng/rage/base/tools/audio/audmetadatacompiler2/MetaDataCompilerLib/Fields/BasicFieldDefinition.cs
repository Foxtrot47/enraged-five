using System;
using System.Diagnostics;
using System.Xml.Linq;
using rage.Compiler;
using rage.Enums;
using rage.Reflection;
using rage.Serializers;
using rage.ToolLib;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.Fields
{
    [DebuggerDisplay("Type= BasicFieldDefinition Name = {Name}")]
    public class BasicFieldDefinition : CommonFieldDefinition,
                                        IBasicFieldDefinition
    {
        private const string STRING_TABLE_INDEX_MUST_BE_UINT =
            "Fields whose units are \"stringtableindex\" must be of type uint.";

        private readonly ICompiledObjectLookup m_compiledObjectLookup;
        private readonly IStringTable m_stringTable;

        public BasicFieldDefinition(ITypeDefinition typeDefinition,
                                    ILog log,
                                    IReflector reflector,
                                    ICompiledObjectLookup compiledObjectLookup,
                                    IStringTable stringTable,
                                    Func<string, IEnumDefinition> enumDefinitionFinderFunc)
            : base(typeDefinition, log, reflector, enumDefinitionFinderFunc)
        {
            if (compiledObjectLookup == null)
            {
                throw new ArgumentNullException("compiledObjectLookup");
            }
            if (stringTable == null)
            {
                throw new ArgumentNullException("stringTable");
            }
            m_compiledObjectLookup = compiledObjectLookup;
            m_stringTable = stringTable;
        }

        #region IBasicFieldDefinition Members

        public bool IsPacked { get; set; }

        public string Min { get; private set; }

        public string Max { get; private set; }

        public string Default { get; private set; }

        public uint Length { get; private set; }

        public bool RequiresValue { get; private set; }

        public override bool Parse(XElement fieldElement)
        {
            if (base.Parse(fieldElement))
            {
                foreach (var attribute in fieldElement.Attributes())
                {
                    try
                    {
                        var token =
                            (FieldDefinitionTokens)
                            Enum.Parse(typeof (FieldDefinitionTokens), attribute.Name.ToString(), true);
                        switch (token)
                        {
                            case FieldDefinitionTokens.Min:
                                {
                                    Min = attribute.Value;
                                    break;
                                }
                            case FieldDefinitionTokens.Max:
                                {
                                    Max = attribute.Value;
                                    break;
                                }
                            case FieldDefinitionTokens.Default:
                                {
                                    Default = attribute.Value;
                                    break;
                                }
                            case FieldDefinitionTokens.Length:
                                {
                                    Length = uint.Parse(attribute.Value);
                                    break;
                                }
                            case FieldDefinitionTokens.RequiresValue:
                                {
                                    RequiresValue = Utility.ToBoolean(attribute.Value);
                                    break;
                                }
                        }
                    }
                    catch
                    {
                    }
                }
                if (Unit == Units.StringTableIndex &&
                    !Type.TypeHandle.Equals(typeof (uint).TypeHandle))
                {
                    m_log.Error(STRING_TABLE_INDEX_MUST_BE_UINT);
                    return false;
                }
                if (string.IsNullOrEmpty(Default))
                {
                    Default = TryGetDefaultValue(fieldElement.Attribute(FieldDefinitionTokens.Type.ToString().ToLower()));
                }
                Serializer = new BasicFieldSerializer(this, Serializer, m_compiledObjectLookup, m_stringTable);
                return true;
            }
            return false;
        }

        #endregion

        private static string TryGetDefaultValue(XAttribute typeAttribute)
        {
            if (typeAttribute != null)
            {
                var type = Enum<FieldTypeTokens>.TryParse(typeAttribute.Value, true, FieldTypeTokens.Unknown);
                switch (type)
                {
                    case FieldTypeTokens.S8:
                    case FieldTypeTokens.U8:
                    case FieldTypeTokens.S16:
                    case FieldTypeTokens.U16:
                    case FieldTypeTokens.S32:
                    case FieldTypeTokens.U32:
                    case FieldTypeTokens.S64:
                    case FieldTypeTokens.U64:
                    case FieldTypeTokens.F32:
                        {
                            return "0";
                        }
                }
            }
            return null;
        }
    }
}