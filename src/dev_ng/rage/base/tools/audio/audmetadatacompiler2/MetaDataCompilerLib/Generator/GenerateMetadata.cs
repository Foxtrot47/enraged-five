namespace rage.Generator
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using rage.Reflection;
    using rage.ToolLib;
    using rage.ToolLib.Logging;
    using rage.Types;

    public class GenerateMetadata
    {
        private const string FORMAT_DUPLICATE_CHILD = "\"{0}\" element contains more than one \"{1}\" child element";

        private readonly ITypeParser m_typeParser;

        private readonly IArgsParser m_argsParser;

        private readonly ILog m_log;

        private readonly IReflector m_reflector;

        private readonly ITypeDefinitionsManager typeDefinitionsManager;

        public GenerateMetadata(
            ILog log,
            string workingPath,
            IReflector reflector,
            audProjectSettings projectSettings,
            ITypeDefinitionsManager typeDefinitionsManager,
            XElement element)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (string.IsNullOrEmpty(workingPath))
            {
                throw new ArgumentNullException("workingPath");
            }
            if (reflector == null)
            {
                throw new ArgumentNullException("reflector");
            }
            if (projectSettings == null)
            {
                throw new ArgumentNullException("projectSettings");
            }
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            
            this.m_log = log;
            this.m_reflector = reflector;
            m_typeParser = new TypeParser(reflector.getReflectedTypes());
            m_argsParser = new ArgsParser();
            this.typeDefinitionsManager = typeDefinitionsManager;

            this.Process(workingPath, projectSettings, element);
        }

        public string Name { get; private set; }
        public IGenerator Generator { get; private set; }
        public GenerateMetadataInput Input { get; private set; }
        public GenerateMetadataOutput Output { get; private set; }

        private void Process(string workingPath, audProjectSettings projectSettings, XElement element)
        {
            if (element.Name !=
                ParseTokens.GenerateMetadata.ToString())
            {
                throw new ArgumentException(string.Format("Expected element of type \"{0}\"",
                                                          ParseTokens.GenerateMetadata));
            }

            var nameAttrib = element.Attribute(ParseTokens.name.ToString());
            if (nameAttrib == null)
            {
                throw new FormatException(string.Format("A \"{0}\" element must have a \"{1}\" attribute",
                                                        ParseTokens.GenerateMetadata,
                                                        ParseTokens.name));
            }
            Name = nameAttrib.Value;

            var targetAttrib = element.Attribute(ParseTokens.target.ToString());
            if (targetAttrib == null)
            {
                throw new FormatException(string.Format("A \"{0}\" element must have a \"{1}\" attribute",
                                                        ParseTokens.GenerateMetadata,
                                                        ParseTokens.target));
            }

            var type = m_typeParser.GetType(targetAttrib.Value);
            if (type != null)
            {
                var generator = m_reflector.CreateGenerator(type);
                if (generator != null)
                {
                    var args = m_argsParser.GetArgs(targetAttrib.Value);
                    if (generator.Init(this.m_log, workingPath, projectSettings, this.typeDefinitionsManager, args))
                    {
                        ParseChildren(workingPath, element.Elements());
                        Generator = generator;
                    }
                }
                else
                {
                    m_log.Error("Could not instantiate generator: {0}", type.AssemblyQualifiedName);
                }
            }
            else
            {
                m_log.Error("IGenerator type not specified on \"{0}\" element", ParseTokens.GenerateMetadata);
            }
        }

        private void ParseChildren(string workingPath, IEnumerable<XElement> elements)
        {
            foreach (var element in elements)
            {
                try
                {
                    var token = Enum<ParseTokens>.Parse(element.Name.ToString());
                    switch (token)
                    {
                        case ParseTokens.Input:
                            {
                                if (Input != null)
                                {
                                    throw new ApplicationException(string.Format(FORMAT_DUPLICATE_CHILD,
                                                                                 ParseTokens.GenerateMetadata,
                                                                                 token));
                                }
                                Input = new GenerateMetadataInput(element);
                                break;
                            }
                        case ParseTokens.Output:
                            {
                                if (Output != null)
                                {
                                    throw new ApplicationException(string.Format(FORMAT_DUPLICATE_CHILD,
                                                                                 ParseTokens.GenerateMetadata,
                                                                                 token));
                                }
                                Output = new GenerateMetadataOutput(workingPath, element);
                                break;
                            }
                    }
                }
                catch (Exception)
                {
                    throw new ApplicationException(string.Format("Unrecognised child element: {0}", element.Name));
                }
            }

            if (Input == null)
            {
                throw new ApplicationException(string.Format("A \"{0}\" element must have a child \"{1}\" element.",
                                                             ParseTokens.GenerateMetadata,
                                                             ParseTokens.Input));
            }
        }

        #region Nested type: ParseTokens

        private enum ParseTokens
        {
            Input,
            Output,
            GenerateMetadata,
// ReSharper disable InconsistentNaming
            name,
            target
// ReSharper restore InconsistentNaming
        }

        #endregion
    }
}