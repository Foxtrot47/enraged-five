using System.Collections.Generic;
using rage.Enums;

namespace rage.Types
{
    public interface ITypeDefinitionsLoader
    {
        string ClassPrefix { get; }

        string ClassIncludePath { get; }

        uint Version { get; }

        uint Mode { get; }

        bool IsPacked { get; }

        bool AlwaysGenerateFlags { get; }

        bool DevOnlyNameTable { get; }

        byte NextId { get; }

        IEnumerable<string> IncludePaths { get; }

        IDictionary<string, IEnumDefinition> EnumDefinitions { get; }

        IDictionary<string, ITypeDefinition> TypeDefinitions { get; }

        IDictionary<string, KeysPerPlatform> EncryptionKeys { get; }

        bool Load(string file);
    }

    public class KeysPerPlatform 
    {
        public EncryptionKey NameTableKey;
        public EncryptionKey StringTableKey;
    }
}