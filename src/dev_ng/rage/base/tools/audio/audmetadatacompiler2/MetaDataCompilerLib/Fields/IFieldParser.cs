using System.Collections.Generic;
using System.Xml.Linq;
using rage.Types;

namespace rage.Fields
{
    public interface IFieldParser : IFieldContainer
    {
        ITypeDefinition TypeDefinition { get; }
        
        bool Parse(IEnumerable<XElement> fieldElements);
    }
}