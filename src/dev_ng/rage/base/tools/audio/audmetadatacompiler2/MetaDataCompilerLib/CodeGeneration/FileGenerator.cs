using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Text;
using rage.Types;

namespace rage.CodeGeneration
{
    public abstract class FileGenerator : IDisposable
    {
        private const string INDENT = "\t";

        private const string COPYRIGHT = "Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.";

        private const string DO_NOT_EDIT = "do not edit.";

        protected const string NAMESPACE = "namespace";

        private readonly string m_fileName;

        private readonly string m_headerDescriptionText;

        private readonly string m_metadataType;

        private readonly string m_nameSpace;

        private readonly ITypeDefinitionsManager m_typeDefinitionsManager;

        private readonly IndentedTextWriter m_writer;

        private bool m_disposed;

        protected FileGenerator(string fileName,
                                string nameSpace,
                                string metaDataType,
                                ITypeDefinitionsManager typeDefinitionsManager,
                                string headerDescriptionText)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }
            if (string.IsNullOrEmpty(nameSpace))
            {
                throw new ArgumentNullException("nameSpace");
            }
            if (string.IsNullOrEmpty(metaDataType))
            {
                throw new ArgumentNullException("metaDataType");
            }
            if (typeDefinitionsManager == null)
            {
                throw new ArgumentNullException("typeDefinitionsManager");
            }
            m_fileName = Path.GetFileName(fileName);
            m_nameSpace = nameSpace;
            m_metadataType = metaDataType;
            m_typeDefinitionsManager = typeDefinitionsManager;
            m_headerDescriptionText = headerDescriptionText;

            var streamWriter = new StreamWriter(fileName, false, Encoding.ASCII);
            m_writer = new IndentedTextWriter(streamWriter, INDENT) {NewLine = Environment.NewLine, Indent = 0};
        }

        public string FileName
        {
            get { return m_fileName; }
        }

        public string MetadataType
        {
            get { return m_metadataType; }
        }

        public string NameSpace
        {
            get { return m_nameSpace; }
        }

        public ITypeDefinitionsManager TypeDefinitionsManager
        {
            get { return m_typeDefinitionsManager; }
        }

        public IndentedTextWriter Writer
        {
            get { return m_writer; }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (!m_disposed)
            {
                m_disposed = true;
                m_writer.Close();
                m_writer.Dispose();
            }
        }

        #endregion

        protected virtual void Generate()
        {
            new BlockCommentWriter(m_writer, CreateFileHeaderText());
            WriteBlankLine();
        }

        protected void WriteBlankLine()
        {
            m_writer.WriteLine();
        }

        private string[] CreateFileHeaderText()
        {
            return new[]
                       {
                           FileName, Environment.NewLine, COPYRIGHT, Environment.NewLine,
                           string.Concat(m_headerDescriptionText, " - ", DO_NOT_EDIT)
                       };
        }
    }
}