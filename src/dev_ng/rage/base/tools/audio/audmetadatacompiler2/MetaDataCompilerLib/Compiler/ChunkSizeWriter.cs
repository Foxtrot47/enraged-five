using System;
using System.IO;
using rage.ToolLib.Writer;

namespace rage.Compiler
{
    public class ChunkSizeWriter : IDisposable
    {
        private readonly uint m_chunkStartOffset;

        private readonly IWriter m_output;

        private readonly uint m_sizeOffset;

        private bool m_disposed;

        public ChunkSizeWriter(IWriter output)
        {
            if (output == null)
            {
                throw new ArgumentNullException("output");
            }
            m_output = output;
            m_sizeOffset = output.Tell();
            m_output.Write(0U);
            m_chunkStartOffset = output.Tell();
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion

        ~ChunkSizeWriter()
        {
            Dispose(false);
        }

        private void Dispose(bool disposing)
        {
            if (!m_disposed)
            {
                m_disposed = true;
                if (disposing)
                {
                    var size = (m_output.Tell() - m_chunkStartOffset);
                    m_output.Seek(m_sizeOffset, SeekOrigin.Begin);
                    m_output.Write(size);
                    m_output.Seek(0, SeekOrigin.End);
                    GC.SuppressFinalize(this);
                }
                else
                {
                    throw new Exception("ChunkSizeWriter was not disposed explicitly!");
                }
            }
        }
    }
}