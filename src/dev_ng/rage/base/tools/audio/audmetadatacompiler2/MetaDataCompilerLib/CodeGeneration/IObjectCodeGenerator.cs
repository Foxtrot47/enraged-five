namespace rage.CodeGeneration
{
    public interface IObjectCodeGenerator
    {
        bool Generate(string path, string nameSpace);
    }
}