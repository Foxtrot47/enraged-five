﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using rage.Fields;
using rage.Serialization;

namespace rage.Serializers.Utilities
{
    public class ElementComparer : IComparer<XElement>
    {
        private readonly ICommonFieldDefinition _fieldDefToCompare;
        private readonly ISerializer _serializer;

        public ElementComparer(ICommonFieldDefinition fieldDefToCompare)
        {
            _fieldDefToCompare = fieldDefToCompare;
            _serializer = fieldDefToCompare.Serializer;
        }

        public int Compare(XElement x, XElement y)
        {
            var fieldElementX = x.Elements(_fieldDefToCompare.Name).FirstOrDefault();
            var fieldElementY = y.Elements(_fieldDefToCompare.Name).FirstOrDefault();
            return _serializer.Compare(fieldElementX, fieldElementY);
        }
    }
}
