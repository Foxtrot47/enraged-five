using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;
using rage.Compiler;
using rage.Enums;
using rage.Reflection;
using rage.Serializers;
using rage.ToolLib;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.Fields
{
    [DebuggerDisplay("Type= CompositeFieldDefinition Name = {Name}")]
    public class CompositeFieldDefinition : CommonFieldDefinition,
                                            ICompositeFieldDefinition
    {
        private const string FORMAT_BIT_MIXED_WITH_REGULAR =
            "Detected mixture of bit field(s) and normal field(s) in CompositeField: {0}.";

        private readonly IFieldParser m_fieldParser;

        public CompositeFieldDefinition(ITypeDefinition typeDefinition, 
                                        ILog log,
                                        IReflector reflector,
                                        ICompiledObjectLookup compiledObjectLookup,
                                        IStringTable stringTable,
                                        Func<string, IEnumDefinition> enumDefinitionFinderFunc)
            : base(typeDefinition, log, reflector, enumDefinitionFinderFunc)
        {
            if (compiledObjectLookup == null)
            {
                throw new ArgumentNullException("compiledObjectLookup");
            }
            if (stringTable == null)
            {
                throw new ArgumentNullException("stringTable");
            }
            m_fieldParser = new FieldParser(typeDefinition, log, reflector, compiledObjectLookup, stringTable, enumDefinitionFinderFunc);
            MaxOccurs = 1;
        }

        #region ICompositeFieldDefinition Members

        public Type NumType { get; private set; }

        public BitField BitField { get; private set; }

        public bool NativeType { get; private set; }

        public bool IsFixedSize { get; private set; }

        public uint MaxOccurs { get; private set; }

        public string OrderBy { get; private set; }

        public override bool Parse(XElement fieldElement)
        {
            Serializer = new CompositeFieldSerializer(m_log, this);
            Type = typeof (ICompositeFieldDefinition);
            if (base.Parse(fieldElement))
            {
                ParseAttributes(fieldElement);
                if (Unit == Units.Vector3 ||
                    Unit == Units.Vector4)
                {
                    Alignment = 16;
                }
                if (m_fieldParser.Parse(fieldElement.Elements()))
                {
                    if (HandleBitFields())
                    {
                        return true;
                    }
                    m_log.Error(string.Format(FORMAT_BIT_MIXED_WITH_REGULAR, Name));
                }
            }
            return false;
        }

        public bool ForceSerialization
        {
            get { return m_fieldParser.ForceSerialization; }
            set
            {
                if (m_fieldParser.ForceSerialization != value)
                {
                    m_fieldParser.ForceSerialization = value;
                    foreach (var compositeField in CompositeFields)
                    {
                        compositeField.ForceSerialization = value;
                    }
                }
            }
        }

        public bool IsType
        {
            get { return false; }
        }

        public bool IsCompressed
        {
            get { return m_fieldParser.IsCompressed; }
            set { m_fieldParser.IsCompressed = value; }
        }

        public bool IsBaseTypeCompressed
        {
            get
            {
                if (this.IsCompressed)
                {
                    return true;
                }

                return null != this.TypeDefinition.InheritsFrom && 
                    this.TypeDefinition.InheritsFrom.IsBaseTypeCompressed;
            }
        }

        public bool IsPacked
        {
            get { return m_fieldParser.IsPacked; }
            set
            {
                m_fieldParser.IsPacked = value;
                foreach (var basicField in m_fieldParser.BasicFields)
                {
                    basicField.IsPacked = value;
                }

                foreach (var compositeField in m_fieldParser.CompositeFields)
                {
                    compositeField.IsPacked = value;
                }

                foreach (var allocatedSpaceField in m_fieldParser.AllocatedSpaceFields)
                {
                    allocatedSpaceField.IsPacked = value;
                }
            }
        }

        public IList<IBasicFieldDefinition> BasicFields
        {
            get { return m_fieldParser.BasicFields; }
        }

        public IList<ICompositeFieldDefinition> CompositeFields
        {
            get { return m_fieldParser.CompositeFields; }
        }

        public IDictionary<string, ICompositeFieldDefinition> CompositeFieldsInheritedLookup
        {
            get { return m_fieldParser.CompositeFieldsInheritedLookup; }
        }

        public IList<IAllocatedSpaceFieldDefinition> AllocatedSpaceFields
        {
            get { return m_fieldParser.AllocatedSpaceFields; }
        }

        public IList<ICommonFieldDefinition> AllFields
        {
            get { return m_fieldParser.AllFields; }
        }

        public IList<ICommonFieldDefinition> AllFieldsInherited
        {
            get { return m_fieldParser.AllFieldsInherited; }
        }

        public IDictionary<string, ICommonFieldDefinition> AllFieldsInheritedLookup
        {
            get { return m_fieldParser.AllFieldsInheritedLookup; }
        }

        #endregion

        private bool HandleBitFields()
        {
            var bitCount = BasicFields.Count(fieldEntry => fieldEntry.Type == typeof(BitField) && !fieldEntry.Ignore);
            if (bitCount > 0)
            {
                BitField = new BitField(bitCount);
            }
            return (bitCount == 0 || bitCount == AllFields.Count);
        }

        private void ParseAttributes(XElement element)
        {
            foreach (var attribute in element.Attributes())
            {
                try
                {
                    var token =
                        (FieldDefinitionTokens)
                        Enum.Parse(typeof (FieldDefinitionTokens), attribute.Name.ToString(), true);
                    switch (token)
                    {
                        case FieldDefinitionTokens.NativeType:
                            {
                                NativeType = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case FieldDefinitionTokens.FixedSize:
                            {
                                IsFixedSize = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case FieldDefinitionTokens.MaxOccurs:
                            {
                                MaxOccurs = uint.Parse(attribute.Value);
                                break;
                            }
                        case FieldDefinitionTokens.NumType:
                            {
                                NumType = ParseNumType(attribute.Value);
                                break;
                            }
                        case FieldDefinitionTokens.OrderBy:
                            {
                                OrderBy = attribute.Value;
                                break;
                            }
                    }
                }
                catch
                {
                }
            }
        }

        private static Type ParseNumType(string value)
        {
            try
            {
                var type = (FieldTypeTokens) Enum.Parse(typeof (FieldTypeTokens), value, true);
                switch (type)
                {
                    case FieldTypeTokens.U8:
                        {
                            return typeof (byte);
                        }
                    case FieldTypeTokens.U16:
                        {
                            return typeof (ushort);
                        }
                    case FieldTypeTokens.U32:
                        {
                            return typeof (uint);
                        }
                    case FieldTypeTokens.U64:
                        {
                            return typeof (ulong);
                        }
                    default:
                        {
                            return null;
                        }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}