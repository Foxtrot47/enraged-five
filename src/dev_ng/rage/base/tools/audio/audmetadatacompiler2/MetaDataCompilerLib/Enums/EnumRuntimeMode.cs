﻿// -----------------------------------------------------------------------
// <copyright file="EnumRuntimeMode.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace rage.Enums
{
    /// <summary>
    /// Runtime mode enum.
    /// </summary>
    public enum EnumRuntimeMode
    {
        /// <summary>
        /// Development mode.
        /// </summary>
        Dev,

        /// <summary>
        /// Release mode.
        /// </summary>
        Release
    }
}
