using System;
using System.Collections.Generic;
using rage.Fields;
using rage.Serializers;
using rage.ToolLib;

namespace rage.Serialization
{
    public static class SerializerLookup
    {
        private static readonly Dictionary<Type, ISerializer> ms_serializerLookup;

        static SerializerLookup()
        {
            ms_serializerLookup = new Dictionary<Type, ISerializer>();
            InitSerializerLookup();
        }

        private static void InitSerializerLookup()
        {
            ms_serializerLookup.Add(typeof (byte), new ByteSerializer());
            ms_serializerLookup.Add(typeof (float), new FloatSerializer());
            ms_serializerLookup.Add(typeof (int), new IntSerializer());
            ms_serializerLookup.Add(typeof (long), new LongSerializer());
            ms_serializerLookup.Add(typeof (sbyte), new SByteSerializer());
            ms_serializerLookup.Add(typeof (short), new ShortSerializer());
            ms_serializerLookup.Add(typeof (TriState), new TriStateSerializer());
            ms_serializerLookup.Add(typeof (BitField), new BitFieldSerializer());
            ms_serializerLookup.Add(typeof (uint), new UIntSerializer());
            ms_serializerLookup.Add(typeof (ulong), new ULongSerializer());
            ms_serializerLookup.Add(typeof (ushort), new UShortSerializer());
            ms_serializerLookup.Add(typeof (Hash), new HashSerializer());
            ms_serializerLookup.Add(typeof(PartialHash), new PartialHashSerializer());
        }

        public static ISerializer Get(Type t)
        {
            ISerializer serializer;
            ms_serializerLookup.TryGetValue(t, out serializer);
            return serializer;
        }

        public static Type GetTypeFromSize(ulong size)
        {
            if (size <= byte.MaxValue)
            {
                return typeof (byte);
            }

            if (size <= ushort.MaxValue)
            {
                return typeof (ushort);
            }

            if (size <= uint.MaxValue)
            {
                return typeof (uint);
            }

            return typeof (ulong);
        }
    }
}