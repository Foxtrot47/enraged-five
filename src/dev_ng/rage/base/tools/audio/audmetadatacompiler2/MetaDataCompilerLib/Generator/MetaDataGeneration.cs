using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using rage.Compiler;
using rage.Reflection;
using rage.ToolLib.Logging;
using rage.ToolLib.WavesFiles;

namespace rage.Generator
{
    using rage.Types;

    public class MetaDataGeneration
    {
        private const string OBJECTS = "Objects";
        private const string FORMAT_FAILED_METADATA_GENERATION = "Failed metadata generation of \"{0}\".";
        private const string FORMAT_NO_INPUT_OBJECTS = "There were no objects that match the input for \"{0}\".";
        private const string FORMAT_WRITING_OUTPUT = "Writing result of metadata generation \"{0}\" to {1}";

        private const string FORMAT_FAILED_BUILTWAVES_LOAD =
            "Failed to load built waves for platform \"{0}\" during metadata generation \"{1}\".";

        private readonly ILog m_log;

        private readonly audProjectSettings m_projectSettings;

        private readonly IReflector m_reflector;

        private readonly string m_workingPath;

        private readonly ITypeDefinitionsManager typeDefinitionsManager;

        public MetaDataGeneration(
            ILog log,
            string workingPath,
            IReflector reflector,
            audProjectSettings projectSettings,
            ITypeDefinitionsManager typeDefinitionsManager)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (string.IsNullOrEmpty(workingPath))
            {
                throw new ArgumentNullException("workingPath");
            }
            if (reflector == null)
            {
                throw new ArgumentNullException("reflector");
            }
            if (projectSettings == null)
            {
                throw new ArgumentNullException("projectSettings");
            }

            this.m_log = log;
            this.m_workingPath = workingPath;
            this.m_reflector = reflector;
            this.m_projectSettings = projectSettings;
            this.typeDefinitionsManager = typeDefinitionsManager;
        }

        public bool Run(audMetadataFile metaDataFile, List<DocumentEntry> documentEntries, bool editMode = false, bool instructionsWithOutputOnly = false)
        {
            var instructions = new List<GenerateMetadata>();
            foreach (var element in metaDataFile.GenerateMetadataElements)
            {
                // If in edit mode, only process instructions that are required
                // to be run when object has been edited
                var runOnEditAttr = element.Attribute("runOnEdit");
                if (editMode && (null == runOnEditAttr || runOnEditAttr.Value.ToLower().Equals("false")))
                {
                    continue;
                }

                var instruction = new GenerateMetadata(
                    m_log, m_workingPath, m_reflector, m_projectSettings, typeDefinitionsManager, element);

                if (!instructionsWithOutputOnly || null != instruction.Output)
                {
                    instructions.Add(instruction);
                }
            }
            
            foreach (var instruction in instructions)
            {
                var input = instruction.Input;
                switch (input.Type)
                {
                    case InputType.Objects:
                        {
                            var objectsXml = CombineMetadata(documentEntries, input);
                            if (objectsXml != null)
                            {
                                if (!Generate(instruction, objectsXml, documentEntries))
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                m_log.Information(FORMAT_NO_INPUT_OBJECTS, instruction.Name);
                            }

                            break;
                        }
                    case InputType.Waves:
                        {
                            var builtWaves = LoadBuiltWaves(input.FileMask);
                            if (builtWaves != null)
                            {
                                if (!Generate(instruction, builtWaves, documentEntries))
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                m_log.Error(FORMAT_FAILED_BUILTWAVES_LOAD,
                                            m_projectSettings.GetCurrentPlatform().PlatformTag,
                                            instruction.Name);
                                return false;
                            }
                            break;
                        }
                }
            }
            return true;
        }

        private bool Generate(GenerateMetadata instruction, XDocument inputDoc, IList<DocumentEntry> documentEntries)
        {
            var generatedDoc = instruction.Generator.Generate(m_log, inputDoc);
            if (generatedDoc != null)
            {
                var documentEntry = new DocumentEntry {Document = generatedDoc};

                var output = instruction.Output;
                if (output != null)
                {
                    documentEntry.DocumentPath = output.OutputFile;
                    generatedDoc.Save(output.OutputFile);
                    m_log.Information(FORMAT_WRITING_OUTPUT, instruction.Name, output.OutputFile);
                }

                UpdateDocumentEntries(documentEntry, documentEntries);
            }
            else
            {
                m_log.Error(FORMAT_FAILED_METADATA_GENERATION, instruction.Name);
                return false;
            }
            return true;
        }

        private static void UpdateDocumentEntries(DocumentEntry documentEntry, IList<DocumentEntry> documentEntries)
        {
            var index = -1;
            for (var i = 0; i < documentEntries.Count; ++i)
            {
                if (string.IsNullOrEmpty(documentEntry.DocumentPath) ||
                    string.IsNullOrEmpty(documentEntries[i].DocumentPath))
                {
                    continue;
                }

                if (string.Compare(documentEntry.DocumentPath, documentEntries[i].DocumentPath, true) == 0)
                {
                    index = i;
                    break;
                }
            }

            if (index >= 0)
            {
                documentEntries[index].Document = documentEntry.Document;
            }
            else
            {
                documentEntries.Add(documentEntry);
            }
        }

        private static XDocument CombineMetadata(IEnumerable<DocumentEntry> documentEntries, GenerateMetadataInput input)
        {
            var useAllTypes = input.MetadataType.Equals("*");
            var metadataTypes = input.MetadataType.Split(',');

            var objectsElement = new XElement(OBJECTS);
            foreach (var documentEntry in documentEntries)
            {
                var root = documentEntry.Document.Root;
                if (root != null && root.Name == OBJECTS)
                {
                    foreach (var element in root.Elements())
                    {
                        if (useAllTypes || metadataTypes.Contains(element.Name.ToString()))
                        {
                            var nameAttrib = element.Attribute("name");
                            if (nameAttrib != null && input.IsMatch(nameAttrib.Value))
                            {
                                objectsElement.Add(new XElement(element));
                            }
                        }
                    }
                }
            }
            return objectsElement.Elements().Any() ? new XDocument(objectsElement) : null;
        }

        private XDocument LoadBuiltWaves(string fileMask)
        {
            try
            {
                var builtWavesFolder = string.Concat(m_workingPath,
                                                     m_projectSettings.GetCurrentPlatform().BuildInfo,
                                                     "BuiltWaves\\");
                builtWavesFolder = builtWavesFolder.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                var combiner = new WavesFileCombiner();
                var builtWavesPackListPath = Path.Combine(builtWavesFolder, "BuiltWaves_PACK_LIST.xml");
                var builtWavesPath = combiner.Run(builtWavesPackListPath, fileMask);
                return builtWavesPath;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}