using System;
using System.Collections.Generic;
using System.Xml.Linq;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.Templates
{
    public class Template : ITemplate
    {
        private const string MISSING_NAME = "Template missing \"name\".";

        private const string LOADED_TEMPLATE = "Loaded template.";

        private const string FORMAT_TYPE_DEFINITION_NOT_FOUND =
            "Could not find TypeDefinition for: \"{0}\" referenced in Template: \"{1}\"";

        private readonly List<KeyValuePair<ITypeDefinition, XElement>> m_elements;

        private readonly ILog m_log;

        private readonly ITypeDefinitionsManager m_typeDefinitionsManager;

        public Template(ILog log, ITypeDefinitionsManager typeDefinitionsManager)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (typeDefinitionsManager == null)
            {
                throw new ArgumentNullException("typeDefinitionsManager");
            }
            m_log = log;
            m_typeDefinitionsManager = typeDefinitionsManager;
            m_elements = new List<KeyValuePair<ITypeDefinition, XElement>>();
        }

        #region ITemplate Members

        public string Name { get; private set; }

        public bool Parse(XElement templateElement)
        {
            var nameAttrib = templateElement.Attribute(TemplateTokens.Name.ToString().ToLower());
            if (nameAttrib != null)
            {
                Name = nameAttrib.Value;
            }
            else
            {
                m_log.Error(MISSING_NAME);
                return false;
            }

            m_log.PushContext(ContextType.Template, Name);
            foreach (var templateChildElement in templateElement.Elements())
            {
                var name = templateChildElement.Name.ToString();
                var typeDefinition = m_typeDefinitionsManager.FindTypeDefinition(name);
                if (typeDefinition == null)
                {
                    m_log.Error(FORMAT_TYPE_DEFINITION_NOT_FOUND, name, Name);
                    return Failed();
                }
                m_elements.Add(new KeyValuePair<ITypeDefinition, XElement>(typeDefinition, templateChildElement));
            }
            m_log.Information(LOADED_TEMPLATE);
            m_log.PopContext();
            return true;
        }

        public IEnumerable<KeyValuePair<ITypeDefinition, XElement>> Elements
        {
            get { return m_elements; }
        }

        #endregion

        private bool Failed()
        {
            m_log.PopContext();
            return false;
        }
    }
}