using System;
using System.Diagnostics;
using System.Xml.Linq;
using rage.Enums;
using rage.Reflection;
using rage.Serializers;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.Fields
{
    [DebuggerDisplay("Type= AllocatedSpaceFieldDefinition Name = {Name}")]
    public class AllocatedSpaceFieldDefinition : CommonFieldDefinition,
                                                 IAllocatedSpaceFieldDefinition
    {
        public AllocatedSpaceFieldDefinition(ITypeDefinition typeDefinition,
                                             ILog log,
                                             IReflector reflector,
                                             Func<string, IEnumDefinition> enumDefinitionFinderFunc)
            : base(typeDefinition, log, reflector, enumDefinitionFinderFunc)
        {
        }

        #region IAllocatedSpaceFieldDefinition Members

        public bool IsPacked { get; set; }

        public uint Size { get; private set; }

        public byte Default { get; private set; }

        public string InitialValue { get; private set; }

        public override bool Parse(XElement fieldElement)
        {
            if (base.Parse(fieldElement))
            {
                foreach (var attribute in fieldElement.Attributes())
                {
                    try
                    {
                        var token =
                            (FieldDefinitionTokens)
                            Enum.Parse(typeof (FieldDefinitionTokens), attribute.Name.ToString(), true);
                        switch (token)
                        {
                            case FieldDefinitionTokens.Size:
                            case FieldDefinitionTokens.Max:
                                {
                                    Size = uint.Parse(attribute.Value);
                                    break;
                                }
                            case FieldDefinitionTokens.Default:
                                {
                                    Default = byte.Parse(attribute.Value);
                                    break;
                                }
                            case FieldDefinitionTokens.InitialValue:
                                {
                                    InitialValue = attribute.Value;
                                    break;
                                }
                        }
                    }
                    catch
                    {
                    }
                }
                Serializer = new AllocatedSpaceFieldSerializer(this, Serializer);
                return true;
            }
            return false;
        }

        #endregion
    }
}