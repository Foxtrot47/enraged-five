using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Xml.Linq;
using rage.Enums;
using rage.Fields;
using rage.Serialization;
using rage.ToolLib.Writer;

namespace rage.Serializers
{
    using rage.ToolLib.Logging;

    [CanSerialize(typeof (Enum), true)]
    public class EnumSerializer : ISerializer
    {
        private readonly IEnumDefinition m_enumDefinition;

        public EnumSerializer(IEnumDefinition enumDefinition)
        {
            if (enumDefinition == null)
            {
                throw new ArgumentNullException("enumDefinition");
            }
            m_enumDefinition = enumDefinition;
        }

        #region ISerializer Members

        public uint Alignment
        {
            get { return 1; }
        }

        public int Compare(XElement x, XElement y)
        {
            IEnumValue valueX, valueY;
            if (m_enumDefinition.Values.TryGetValue(x.Value, out valueX) &&
                m_enumDefinition.Values.TryGetValue(y.Value, out valueY))
            {
                if (valueX.Value > valueY.Value) return 1;
                if (valueX.Value < valueY.Value) return -1;
            }
            return 0;
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            IEnumValue enumValue;
            if (m_enumDefinition.Values.TryGetValue(element.Value, out enumValue))
            {
                output.Write(enumValue.Value);
                return true;
            }
            return false;
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            writer.Write(Utility.EncloseInBrackets(value));
            return null;
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            writer.Write("rage::u8");
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            // do nothing
        }

        #endregion
    }
}