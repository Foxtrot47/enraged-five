namespace rage.Types
{
    // The members of this enum are used while parsing the TypeDef files
    // they are case sensitive and must match the case used in the Xml
    public enum TypeDefinitionTokens
    {
        TypeDefinition,
        TypeDefinitions,
        Version,
        Mode,
        DefaultPacked,
        AlwaysGenerateFlags,
        EncryptionKeys,
        platform,
        NameTableKey,
        StringTableKey,
        Yes,
        No,
        Enum,
        Include,
        Includes,
        Name,
        IsAbstract,
        InheritsFrom,
        Validator,
        Transformer,
        IsCompressed,
        Group,
        Packed,
        Align,
        Field,
        CompositeField,
        AllocatedSpaceField,
        IsFactoryCodeGenerated,
        RequiresHeader,
        AdditionalCompression,
        DevOnlyNameTable,
		SkipDeleteContainerOnRefDelete,
		Use16BitStringTableIndex
    }
}
