using System;
using System.IO;

namespace rage.CodeGeneration
{
    public class IncludeGuardWriter : IDisposable
    {
        private readonly string m_name;

        private readonly TextWriter m_writer;

        private bool m_disposed;

        public IncludeGuardWriter(TextWriter writer, string name)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
            m_writer = writer;
            m_name = name;

            m_writer.WriteLine(string.Concat("#ifndef ", m_name));
            m_writer.WriteLine(string.Concat("#define ", m_name));
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (!m_disposed)
            {
                m_disposed = true;
                m_writer.WriteLine(string.Concat("#endif // ", m_name));
            }
        }

        #endregion
    }
}