using System;
using System.Collections.Generic;
using System.Xml.Linq;
using rage.Templates;
using rage.ToolLib.Writer;

namespace rage.Compiler
{
    using rage.Enums;
    using rage.Types;

    public interface IMetaDataCompiler
    {
        uint Version { get; }

        ITypeDefinitionsManager TypeDefinitionsManager {get;}

        IEnumerable<string> TypeDefinitionPaths { get; }

        event Action<string, string, bool> LoggedError;

        event Action<string, Exception> LoggedException;

        bool Generate(List<DocumentEntry> documentEntries, audMetadataFile metaDataFile, bool editMode = false, bool instructionsWithOutputOnly = false);

        bool Compile(XDocument document, IWriter output, audMetadataFile metaDataFile, EnumRuntimeMode runtimeMode);

        bool Compile(List<DocumentEntry> documentEntries, IWriter output, audMetadataFile metaDataFile, uint changeListNumber, EnumRuntimeMode runtimeMode, string nameTablePath = null, bool editMode = false);

        bool LoadTypeDefinitionsAndTemplates(string templatePath, IEnumerable<audObjectDefinition> objectDefinitions);

        bool LoadTypeDefinitions(IEnumerable<audObjectDefinition> objectDefinitions);

        bool ReloadTypeDefinitions();

        ITemplateManager CreateTemplateManager();

        bool LoadTemplates(XDocument document);

        bool ProcessTemplates(XDocument document);

        void ClearTemplates();

        void ForceSerialization();

        string GetNameFromOffset(uint offset);
    }
}
