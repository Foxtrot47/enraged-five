using System.Xml.Linq;

namespace rage.Transform
{
    public interface ITransformAndValidatorProcessor
    {
        bool Process(XDocument doc, bool shouldTransform);
    }
}