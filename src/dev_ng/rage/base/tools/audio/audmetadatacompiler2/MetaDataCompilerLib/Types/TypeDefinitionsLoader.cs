using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using rage.Compiler;
using rage.Enums;
using rage.Reflection;
using rage.ToolLib.Logging;

namespace rage.Types
{
    public class TypeDefinitionsLoader : ITypeDefinitionsLoader
    {
        private const string MISSING_TYPE_DEFINITIONS_ELEMENT = "Missing <TypeDefinitions> element.";

        private const string FORMAT_SUCCEEDED_AND_FAILED = "{0} succeeded. {1} failed.";

        private const string FORMAT_COMPLETED_PARSING_ENUMS = "Completed parsing Enums. {0}";

        private const string PARSING_ENUM_DEFINITIONS = "Parsing Enum Definitions...";

        private const string PARSING_ENCRYPTION_KEYS = "Parsing Encryption Keys...";

        private const string NO_PLATFORM_FOR_ENCRYPTION_KEYS = "No platform has been specified for Encryption keys, ignoring keys.";

        private const string NO_STRINGTABLE_ENCRYPTION_KEY = "No String table Encryption key has been specified for platform {0}, String table will not be enctrypted.";

        private const string NO_NAMETABLE_ENCRYPTION_KEY = "No Name table Encryption key has been specified for platform {0}, Name table will not be enctrypted.";

        private const string MULTIPLE_ENCRYPTION_KEYS = "More than one {0} specified for platform {1}, using the first key.";

        private const string ERROR_PARSING_ENCRYPTION_KEY = "Error parsing {0} for platform {1}. {2}";

        private const string FORMAT_SCHEMA_VERSION = "Schema version: {0}";

        private const string PARSING_INCLUDES = "Parsing Includes...";

        private const string COMPLETED_PARSING_INCLUDES = "Completed parsing Includes.";

        private const string EMPTY_INCLUDE_ELEMENT = "Empty include element detected.";

        private const string FORMAT_FOUND_INCLUDE_PATH = "Found include path: {0}";

        private const string PARSING_TYPE_DEFINITIONS = "Parsing Type Definitions...";

        private const string FORMAT_COMPLETED_PARSING_TYPE_DEFS = "Completed parsing Type Definitions. {0}";

        private const string UNKNOWN_TYPE_DEFINTIONS_ATTRIBUTE = "Unknown attribute on <TypeDefinitions> element. {0}";

        private const string FORMAT_PROCESSING_FILE = "Processing file: {0}";

        private readonly IArgsParser m_argsParser;

        private readonly ICompiledObjectLookup m_compiledObjectLookup;

        private readonly Func<string, IEnumDefinition> m_enumDefinitionFinderFunc;

        private readonly List<string> m_includePaths;

        private readonly ILog m_log;

        private readonly IReflector m_reflector;

        private readonly IStringTable m_stringTable;

        private readonly Func<string, ITypeDefinition> m_typeDefinitionFinderFunc;

        private readonly ITypeParser m_typeParser;

        private readonly bool m_skipTransform;

        private readonly bool m_skipValidation;

        private string m_basePath;

        public TypeDefinitionsLoader(ILog log,
                                     IReflector reflector,
                                     ICompiledObjectLookup compiledObjectLookup,
                                     IStringTable stringTable,
                                     string classPrefix,
                                     string classIncludePath,
                                     Func<string, ITypeDefinition> typeDefinitionFinderFunc,
                                     Func<string, IEnumDefinition> enumDefinitionFinderFunc,
                                     byte nextId,
                                     bool skipTransform  = false,
                                     bool skipValidation = false)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (reflector == null)
            {
                throw new ArgumentNullException("reflector");
            }
            if (compiledObjectLookup == null)
            {
                throw new ArgumentNullException("compiledObjectLookup");
            }
            if (stringTable == null)
            {
                throw new ArgumentNullException("stringTable");
            }
            if (typeDefinitionFinderFunc == null)
            {
                throw new ArgumentNullException("typeDefinitionFinderFunc");
            }
            if (enumDefinitionFinderFunc == null)
            {
                throw new ArgumentNullException("enumDefinitionFinderFunc");
            }
            m_log = log;
            m_reflector = reflector;
            m_compiledObjectLookup = compiledObjectLookup;
            m_stringTable = stringTable;
            ClassPrefix = classPrefix;
            ClassIncludePath = classIncludePath;
            m_typeDefinitionFinderFunc = typeDefinitionFinderFunc;
            m_enumDefinitionFinderFunc = enumDefinitionFinderFunc;
            NextId = nextId;
            m_skipTransform = skipTransform;
            m_skipValidation = skipValidation;

            m_includePaths = new List<string>();
            EnumDefinitions = new Dictionary<string, IEnumDefinition>();
            TypeDefinitions = new Dictionary<string, ITypeDefinition>();
            m_argsParser = new ArgsParser();
            m_typeParser = new TypeParser(reflector.getReflectedTypes());
            EncryptionKeys = new Dictionary<string, KeysPerPlatform>();
            IsPacked = true;
            AlwaysGenerateFlags = true;
        }

        #region ITypeDefinitionsLoader Members

        public string ClassPrefix { get; private set; }

        public string ClassIncludePath { get; private set; }

        public uint Version { get; private set; }

        public uint Mode { get; private set; }

        public bool IsPacked { get; private set; }

        public bool AlwaysGenerateFlags { get; private set; }

        public bool DevOnlyNameTable { get; private set; }

        public byte NextId { get; private set; }

        public IEnumerable<string> IncludePaths
        {
            get { return m_includePaths; }
        }

        public IDictionary<string, IEnumDefinition> EnumDefinitions { get; private set; }

        public IDictionary<string, ITypeDefinition> TypeDefinitions { get; private set; }

        
        public IDictionary<string, KeysPerPlatform> EncryptionKeys { get; private set; }

        public bool Load(string file)
        {
            m_basePath = Path.GetDirectoryName(file);
            XDocument doc;
            if ((doc = ToolLib.Utility.LoadDocument(m_log, file)) != null)
            {
                m_log.WriteFormatted(string.Empty);
                m_log.WriteFormatted(FORMAT_PROCESSING_FILE, file);
                if (Parse(doc))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        private bool Parse(XContainer doc)
        {
            var typeDefElement = doc.Descendants(TypeDefinitionTokens.TypeDefinitions.ToString()).FirstOrDefault();
            if (typeDefElement != null)
            {
                ParseAttributes(typeDefElement.Attributes());
                ParseEncryptionKeys(typeDefElement.Descendants(TypeDefinitionTokens.EncryptionKeys.ToString()));
                ParseIncludeElements(typeDefElement.Descendants(TypeDefinitionTokens.Include.ToString()));
                var enumElements = typeDefElement.Descendants(TypeDefinitionTokens.Enum.ToString());
                var typeDefinitionElements = typeDefElement.Descendants(TypeDefinitionTokens.TypeDefinition.ToString());
                if (ParseEnumElements(enumElements) &&
                    ParseTypeDefinitionElements(typeDefinitionElements))
                {
                    return true;
                }
            }
            else
            {
                m_log.Error(MISSING_TYPE_DEFINITIONS_ELEMENT);
            }
            return false;
        }

        private void ParseEncryptionKeys(IEnumerable<XElement> encryptionKeysElements)
        {
            if(!encryptionKeysElements.Any())
            {
                return;
            }

            m_log.WriteFormatted(string.Empty);
            m_log.WriteFormatted(PARSING_ENCRYPTION_KEYS);
            foreach(var encryptionKeysElement in encryptionKeysElements)
            {
                var nameAttrib = encryptionKeysElement.Attribute(TypeDefinitionTokens.platform.ToString());
                if (nameAttrib == null)
                {
                    m_log.Warning(NO_PLATFORM_FOR_ENCRYPTION_KEYS);
                    continue;
                }
                string platform = nameAttrib.Value;
                if (!EncryptionKeys.ContainsKey(platform))
                {
                    EncryptionKeys.Add(platform, new KeysPerPlatform());
                }

                IEnumerable<XElement> nameTableKeyElemnts =
                    encryptionKeysElement.Descendants(TypeDefinitionTokens.NameTableKey.ToString());
                if (nameTableKeyElemnts.Any())
                {
                    if(nameTableKeyElemnts.Count()>1) m_log.Information(MULTIPLE_ENCRYPTION_KEYS, TypeDefinitionTokens.NameTableKey.ToString(), platform);
                    //parse nametable key
                    try
                    {
                        EncryptionKey key = new EncryptionKey(nameTableKeyElemnts.First().Value);
                        EncryptionKeys[platform].NameTableKey = key;
                    }
                    catch (Exception ex)
                    {
                        m_log.Warning(ERROR_PARSING_ENCRYPTION_KEY, TypeDefinitionTokens.NameTableKey.ToString(), platform, ex.Message);
                    }
                }
                else
                {
                    m_log.Warning(NO_NAMETABLE_ENCRYPTION_KEY, platform);
                }


                IEnumerable<XElement> stringTableKeyElemnts =
                    encryptionKeysElement.Descendants(TypeDefinitionTokens.StringTableKey.ToString());
                if (stringTableKeyElemnts.Any())
                {
                    if(stringTableKeyElemnts.Count()>1) m_log.Information(MULTIPLE_ENCRYPTION_KEYS, TypeDefinitionTokens.StringTableKey.ToString(), platform);
                    //parse stringtable key
                    try {
                        EncryptionKey key = new EncryptionKey(stringTableKeyElemnts.First().Value);
                        EncryptionKeys[platform].StringTableKey = key;
                    }
                    catch(Exception ex) {
                        m_log.Warning(ERROR_PARSING_ENCRYPTION_KEY, TypeDefinitionTokens.StringTableKey.ToString(), platform, ex.Message);
                    }
                }
                else
                {
                    m_log.Warning(NO_STRINGTABLE_ENCRYPTION_KEY, platform);
                }

            }
        }

        private void ParseIncludeElements(IEnumerable<XElement> includesElements)
        {
            if (includesElements.Count() == 0)
            {
                return;
            }

            m_log.WriteFormatted(string.Empty);
            m_log.WriteFormatted(PARSING_INCLUDES);
            foreach (var includesElement in includesElements)
            {
                var includeElements = includesElement.Descendants(TypeDefinitionTokens.Include.ToString());
                foreach (var includeElement in includeElements)
                {
                    var includePath = includeElement.Value;
                    if (string.IsNullOrEmpty(includePath))
                    {
                        m_log.Information(EMPTY_INCLUDE_ELEMENT);
                    }
                    else
                    {
                        m_includePaths.Add(includePath);
                        m_log.Information(FORMAT_FOUND_INCLUDE_PATH, includePath);
                    }
                }
            }
            m_log.WriteFormatted(COMPLETED_PARSING_INCLUDES);
        }

        private bool ParseTypeDefinitionElements(IEnumerable<XElement> typeDefinitionElements)
        {
            if (!typeDefinitionElements.Any())
            {
                return true;
            }

            m_log.WriteFormatted(string.Empty);
            m_log.WriteFormatted(PARSING_TYPE_DEFINITIONS);
            var success = true;
            var numFailedToParse = 0;
            foreach (var typeDefintionElement in typeDefinitionElements)
            {
                var typeDef = new TypeDefinition(this,
                                                 m_log,
                                                 m_reflector,
                                                 m_argsParser,
                                                 m_typeParser,
                                                 m_compiledObjectLookup,
                                                 m_stringTable,
                                                 m_basePath,
                                                 m_typeDefinitionFinderFunc,
                                                 m_enumDefinitionFinderFunc,
                                                 GetNextId(),
                                                 m_skipTransform,
                                                 m_skipValidation)
                                  {
                                      IsPacked = IsPacked,
                                      RequiresFlags = AlwaysGenerateFlags,
                                  };
                if (typeDef.Parse(typeDefintionElement))
                {
                    TypeDefinitions.Add(typeDef.Name, typeDef);
                }
                else
                {
                    numFailedToParse += 1;
                    success = false;
                }
            }
            var succeededAndFailedStr = string.Format(FORMAT_SUCCEEDED_AND_FAILED,
                                                      TypeDefinitions.Count,
                                                      numFailedToParse);
            m_log.WriteFormatted(FORMAT_COMPLETED_PARSING_TYPE_DEFS, succeededAndFailedStr);
            return success;
        }

        private byte GetNextId()
        {
            var id = NextId;
            NextId += 1;
            return id;
        }

        private bool ParseEnumElements(IEnumerable<XElement> enumElements)
        {
            if (!enumElements.Any())
            {
                return true;
            }

            m_log.WriteFormatted(string.Empty);
            m_log.WriteFormatted(PARSING_ENUM_DEFINITIONS);
            var success = true;
            var numFailedToParse = 0;
            foreach (var enumElement in enumElements)
            {
                var enumDef = new EnumDefinition(m_log);
                if (enumDef.Parse(enumElement))
                {
                    EnumDefinitions.Add(enumDef.Name, enumDef);
                }
                else
                {
                    numFailedToParse += 1;
                    success = false;
                }
            }
            var succeededAndFailedStr = string.Format(FORMAT_SUCCEEDED_AND_FAILED,
                                                      EnumDefinitions.Count,
                                                      numFailedToParse);
            m_log.WriteFormatted(FORMAT_COMPLETED_PARSING_ENUMS, succeededAndFailedStr);
            return success;
        }

        private void ParseAttributes(IEnumerable<XAttribute> attributes)
        {
            try
            {
                foreach (var attribute in attributes)
                {
                    var token =
                        (TypeDefinitionTokens)
                        Enum.Parse(typeof(TypeDefinitionTokens), attribute.Name.ToString(), true);
                    switch (token)
                    {
                        case TypeDefinitionTokens.Version:
                            {
                                Version = uint.Parse(attribute.Value);
                                m_log.Information(FORMAT_SCHEMA_VERSION, Version);
                                break;
                            }
                        case TypeDefinitionTokens.Mode:
                            {
                                Mode = uint.Parse(attribute.Value);
                                break;
                            }
                        case TypeDefinitionTokens.DefaultPacked:
                            {
                                IsPacked = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case TypeDefinitionTokens.AlwaysGenerateFlags:
                            {
                                AlwaysGenerateFlags = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        case TypeDefinitionTokens.DevOnlyNameTable:
                            {
                                DevOnlyNameTable = Utility.ToBoolean(attribute.Value);
                                break;
                            }
                        default:
                            {
                                m_log.Warning(UNKNOWN_TYPE_DEFINTIONS_ATTRIBUTE, attribute.Name);
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                m_log.Exception(ex);
            }
        }
    }
}