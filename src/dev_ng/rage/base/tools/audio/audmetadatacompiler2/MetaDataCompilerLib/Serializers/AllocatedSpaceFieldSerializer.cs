using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Xml.Linq;
using rage.Fields;
using rage.Serialization;
using rage.ToolLib.Writer;

namespace rage.Serializers
{
    using rage.ToolLib.Logging;

    [CanSerialize(typeof (IAllocatedSpaceFieldDefinition), true)]
    public class AllocatedSpaceFieldSerializer : ISerializer
    {
        private const string INITIAL_VALUE = "initialValue";

        private readonly IAllocatedSpaceFieldDefinition m_fieldDefinition;

        private readonly ISerializer m_serializer;

        public AllocatedSpaceFieldSerializer(IAllocatedSpaceFieldDefinition fieldDefinition, ISerializer serializer)
        {
            if (fieldDefinition == null)
            {
                throw new ArgumentNullException("fieldDefinition");
            }
            if (serializer == null)
            {
                throw new ArgumentNullException("serializer");
            }
            m_fieldDefinition = fieldDefinition;
            m_serializer = serializer;
        }

        #region ISerializer Members

        public uint Alignment
        {
            get { return m_serializer.Alignment; }
        }

        public int Compare(XElement x, XElement y)
        {
            throw new NotSupportedException("Comparison of Composite Field type not supported.");
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            byte numElements;
            if (!byte.TryParse(element.Value, out numElements))
            {
                numElements = m_fieldDefinition.Default;
            }
            output.Write(numElements);
            var initialValueElement = new XElement(INITIAL_VALUE, m_fieldDefinition.InitialValue);
            for (byte i = 0; i < numElements; ++i)
            {
                if (!m_fieldDefinition.IsPacked)
                {
                    Utility.AlignToBoundary(output, m_serializer.Alignment);
                }

                if (!m_serializer.Serialize(log, output, initialValueElement, fieldDefinition))
                {
                    return false;
                }
            }
            return true;
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            // this should never be called
            throw new NotImplementedException();
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            writer.WriteLine(string.Concat("rage::u8 num", m_fieldDefinition.Name, "Elems;"));
            m_serializer.WriteStructureEntry(writer);
            writer.WriteLine(string.Format(" {0}[{1}];", m_fieldDefinition.Name, m_fieldDefinition.Size));
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            // do nothing
        }

        #endregion
    }
}