using rage.Compiler;

namespace rage.CodeGeneration
{
    public interface IMetaDataCodeGenerator
    {
        bool LoadTypeDefinitions(audMetadataType metaDataType,
                                 ICompiledObjectLookup compiledObjectLookup,
                                 IStringTable stringTable,
                                 string workingDirectory);

        bool GenerateCode(audMetadataType metaDataType, string path, string nameSpace);
    }
}