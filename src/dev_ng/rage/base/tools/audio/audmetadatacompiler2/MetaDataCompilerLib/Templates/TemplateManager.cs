using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.Templates
{
    public class TemplateManager : ITemplateManager
    {
        private const string STARTED_LOADING_TEMPLATES = "Loading templates...";

        private const string FINISHED_LOADING_TEMPLATES = "Finished loading templates.";

        private readonly ILog m_log;

        private readonly ITemplateProcessor m_templateProcessor;

        private readonly IDictionary<string, ITemplate> m_templates;

        private readonly ITypeDefinitionsManager m_typeDefinitionsManager;

        public TemplateManager(ILog log, ITypeDefinitionsManager typeDefinitionsManager)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (typeDefinitionsManager == null)
            {
                throw new ArgumentNullException("typeDefinitionsManager");
            }
            m_log = log;
            m_typeDefinitionsManager = typeDefinitionsManager;
            m_templates = new Dictionary<string, ITemplate>();
            m_templateProcessor = new TemplateProcessor(log, m_templates);
        }

        #region ITemplateManager Members

        public bool Load(string templatePath)
        {
            if (PathIsValid(templatePath))
            {
                m_log.WriteFormatted(STARTED_LOADING_TEMPLATES);
                var templateFiles = Directory.GetFiles(templatePath,
                                                       Utility.XML_SEARCH_PATTERN,
                                                       SearchOption.AllDirectories);
                foreach (var templateFile in templateFiles)
                {
                    m_log.PushContext(ContextType.File, templateFile);
                    XDocument doc;
                    try
                    {
                        doc = ToolLib.Utility.LoadDocument(m_log, templateFile);
                    }
                    catch (Exception ex)
                    {
                        m_log.Exception(ex);
                        return Failed();
                    }

                    if (!Load(doc))
                    {
                        return Failed();
                    }
                    m_log.PopContext();
                }
                m_log.WriteFormatted(FINISHED_LOADING_TEMPLATES);
                m_log.WriteFormatted(string.Empty);
            }
            return true;
        }

        public bool Load(XContainer templateContainer)
        {
            var templateElements = templateContainer.Descendants(TemplateTokens.Template.ToString());
            foreach (var templateElement in templateElements)
            {
                var template = new Template(m_log, m_typeDefinitionsManager);
                if (!template.Parse(templateElement))
                {
                    return false;
                }
                if (m_templates.ContainsKey(template.Name))
                {
                    m_templates.Remove(template.Name);
                }
                m_templates.Add(template.Name, template);
            }
            return true;
        }

        public bool Process(XDocument doc)
        {
            return m_templateProcessor.Process(doc);
        }

        public void Clear()
        {
            m_templates.Clear();
        }

        #endregion

        private bool Failed()
        {
            m_log.PopContext();
            return false;
        }

        private static bool PathIsValid(string templatePath)
        {
            return !string.IsNullOrEmpty(templatePath) && Directory.Exists(templatePath);
        }
    }
}