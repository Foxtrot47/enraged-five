using System.Xml.Linq;

namespace rage.Templates
{
    public interface ITemplateProcessor
    {
        bool Process(XDocument doc);
    }
}