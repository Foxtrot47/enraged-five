using System;

namespace rage.Transform
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class CanValidateAttribute : Attribute,
                                        IElementTypeProvider
    {
        private readonly string m_elementType;

        public CanValidateAttribute(string elementType)
        {
            if (string.IsNullOrEmpty(elementType))
            {
                throw new ArgumentNullException("elementType");
            }
            m_elementType = elementType;
        }

        #region IElementTypeProvider Members

        public string GetElementType()
        {
            return m_elementType;
        }

        #endregion
    }
}