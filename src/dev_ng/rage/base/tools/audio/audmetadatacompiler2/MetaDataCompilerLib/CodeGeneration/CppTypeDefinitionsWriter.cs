using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using rage.Serializers;
using rage.ToolLib;
using rage.Types;

namespace rage.CodeGeneration
{
    public class CppTypeDefinitionsWriter
    {
        private readonly IEnumerable<ITypeDefinition> m_typeDefinitions;

        private readonly IndentedTextWriter m_writer;

        public CppTypeDefinitionsWriter(IndentedTextWriter writer, IEnumerable<ITypeDefinition> typeDefinitions)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (typeDefinitions == null)
            {
                throw new ArgumentNullException("typeDefinitions");
            }
            m_writer = writer;
            m_typeDefinitions = typeDefinitions;

            Write();
        }

        private void Write()
        {
            foreach (var typeDef in m_typeDefinitions)
            {
                var validFields = from fieldDef in typeDef.AllFields
                                  where CompositeFieldSerializer.IsValidField(fieldDef)
                                  select fieldDef;
                if (validFields.Count() > 0)
                {
                    new BlockCommentWriter(m_writer, typeDef.Name);

                    foreach (var fieldDef in validFields)
                    {
                        fieldDef.Serializer.WriteFunctions(m_writer, fieldDef);
                    }

                    var functionDeclaration = string.Concat("void* ",
                                                            typeDef.Name,
                                                            "::GetFieldPtr(const rage::u32 fieldNameHash)");
                    using (new BlockWriter(m_writer, functionDeclaration, null, false))
                    {
                        using (new BlockWriter(m_writer, "switch(fieldNameHash)", null, false))
                        {
                            var hash = new Hash();
                            foreach (var fieldDef in validFields)
                            {
                                hash.Value = fieldDef.Name;
                                m_writer.WriteLine(string.Concat("case ", hash.Key, "U: return &", fieldDef.Name, ";"));
                            }
                            m_writer.WriteLine("default: return NULL;");
                        }
                    }
                    m_writer.WriteLine();
                }
            }
        }
    }
}