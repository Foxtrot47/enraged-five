using System;
using System.CodeDom.Compiler;

namespace rage.CodeGeneration
{
    public class TriStateMacroWriter : IDisposable
    {
        private readonly IndentedTextWriter m_writer;

        private bool m_disposed;

        public TriStateMacroWriter(IndentedTextWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            m_writer = writer;

            m_writer.WriteLine("// macros for dealing with packed tristates");
            m_writer.WriteLine("#ifndef AUD_GET_TRISTATE_VALUE");

            m_writer.Indent += 1;
            m_writer.WriteLine(
                "#define AUD_GET_TRISTATE_VALUE(flagvar, flagid) (TristateValue)((flagvar >> (flagid<<1)) & 0x03)");
            m_writer.WriteLine(
                "#define AUD_SET_TRISTATE_VALUE(flagvar, flagid, trival) ((flagvar |= ((trival&0x03) << (flagid<<1))))");
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (!m_disposed)
            {
                m_disposed = true;

                m_writer.Indent -= 1;
                m_writer.WriteLine("#endif // !defined AUD_GET_TRISTATE_VALUE");
            }
        }

        #endregion
    }
}