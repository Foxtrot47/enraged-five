namespace rage
{
    public interface IArgsParser
    {
        string[] GetArgs(string value);
    }
}