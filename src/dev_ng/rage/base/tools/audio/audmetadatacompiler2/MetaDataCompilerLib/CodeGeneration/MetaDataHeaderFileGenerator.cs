using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using rage.Enums;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.CodeGeneration
{
    public sealed class MetaDataHeaderFileGenerator : FileGenerator
    {
        private const string RAGE = "rage";

        private audProjectSettings m_projectSettings;

        private ILog m_log;

        public MetaDataHeaderFileGenerator(string fileName,
                                           string nameSpace,
                                           string metaDataType,
                                           ITypeDefinitionsManager typeDefinitionsManager,
                                           audProjectSettings projectSettings,
                                           ILog log)
            : base(
                fileName,
                nameSpace,
                metaDataType,
                typeDefinitionsManager,
                "Automatically generated metadata structure definitions")
        {
            this.m_projectSettings = projectSettings;
            this.m_log = log;

            Generate();
        }

        protected override void Generate()
        {
            base.Generate();

            var fileNameNoExtensionUpper = Path.GetFileNameWithoutExtension(FileName).ToUpper();
            using (new IncludeGuardWriter(Writer, string.Concat("AUD_", fileNameNoExtensionUpper, "_H")))
            {
                WriteBlankLine();

                WriteIncludes();
                WriteTriStateMacros();
                WriteEncryptionKeyClasses();

                using (new BlockWriter(Writer, NAMESPACE, NameSpace, false))
                {
                    WriteSchemaVersionMacro(fileNameNoExtensionUpper);
                    WriteMetaDataCompilerVersionMacro(fileNameNoExtensionUpper);
                    WriteNumTypeDefsMacro(fileNameNoExtensionUpper);
                    new GetAddressMacroWriter(Writer, fileNameNoExtensionUpper, TypeDefinitionsManager);
                    WriteBlankLine();

                    WritePrototypes();

                    WriteEnums();
                    WriteTypeDefs();
                }
            }
        }

        private void WriteIncludes()
        {
            new IncludesWriter(Writer,
                               new List<string> {"vectormath/vec3v.h", "vectormath/vec4v.h", "math/float16.h", "audioengine/metadataref.h"});
            WriteBlankLine();

            if (this.TypeDefinitionsManager.IncludePaths.Any())
            {
                new IncludesWriter(Writer, TypeDefinitionsManager.IncludePaths);
                WriteBlankLine();
            }
        }

        private void WriteTriStateMacros()
        {
            using (new TriStateMacroWriter(Writer))
            {
                using (new BlockWriter(Writer, NAMESPACE, RAGE, null, false))
                {
                    // TriState enum
                    new EnumWriter(Writer, "TristateValue", GetTriStateEnumValues());
                }
            }
            WriteBlankLine();
        }

        private void WriteEncryptionKeyClasses()
        {
            foreach (KeyValuePair<string, KeysPerPlatform> keysPerPlatform in TypeDefinitionsManager.EncryptionKeys)
            {
                PlatformSetting platformSetting = null;
                foreach (PlatformSetting p in m_projectSettings.GetPlatformSettings())
                {
                    if (p.PlatformTag.Equals(keysPerPlatform.Key, StringComparison.OrdinalIgnoreCase)) platformSetting = p;
                }

                if (platformSetting == null)
                {
                    m_log.Warning("No platform settings found in projectSettings for platform "+keysPerPlatform.Key+". No encryption key class generated.");
                    continue;
                }

                if (string.IsNullOrEmpty(platformSetting.CodeSymbol))
                {
                    m_log.Warning("No code symbol found in projectSettings for platform "+keysPerPlatform.Key+". No encryption key class generated.");
                    continue;
                }

                Writer.WriteLine("#if "+platformSetting.CodeSymbol);
                Writer.WriteLine("class MetadataType_Keys");
                Writer.WriteLine("{");
                if (string.IsNullOrEmpty(keysPerPlatform.Value.NameTableKey.ToString()))
                {
                    m_log.Warning("No NameTable key specified for platform " + keysPerPlatform.Key);
                }
                else
                {
                    Writer.Indent += 1;
                    Writer.WriteLine("private:");
                    Writer.WriteLine("enum  NameKey {");
                    Writer.WriteLine("part0="+keysPerPlatform.Value.NameTableKey.part0);
                    Writer.WriteLine("part1="+keysPerPlatform.Value.NameTableKey.part1);
                    Writer.WriteLine("part2="+keysPerPlatform.Value.NameTableKey.part2);
                    Writer.WriteLine("part3="+keysPerPlatform.Value.NameTableKey.part3);
                    Writer.WriteLine("}");
                    Writer.Indent -= 1;
                }

                if(string.IsNullOrEmpty(keysPerPlatform.Value.StringTableKey.ToString())) {
                    m_log.Warning("No StringTable key specified for platform " + keysPerPlatform.Key);
                }
                else {
                    Writer.Indent += 1;
                    Writer.WriteLine("private:");
                    Writer.WriteLine("enum  StringKey {");
                    Writer.WriteLine("part0="+keysPerPlatform.Value.StringTableKey.part0);
                    Writer.WriteLine("part1="+keysPerPlatform.Value.StringTableKey.part1);
                    Writer.WriteLine("part2="+keysPerPlatform.Value.StringTableKey.part2);
                    Writer.WriteLine("part3="+keysPerPlatform.Value.StringTableKey.part3);
                    Writer.WriteLine("}");
                    Writer.Indent -= 1;
                }
                Writer.WriteLine("}");
                Writer.WriteLine("#endif");

            }
        }

        private void WriteTypeDefs()
        {
            IList<ITypeDefinition> packedTypeDefs, unpackedTypeDefs;
            SortTypeDefsByPacking(out packedTypeDefs, out unpackedTypeDefs);

            // Write packed type defs
            if (packedTypeDefs.Count > 0)
            {
                using (new StructAlignmentMacroWriter(Writer))
                {
                    var packedWriter = new HeaderTypeDefinitionsWriter(Writer, packedTypeDefs);
                    packedWriter.Write();
                }
            }

            // Write unpacked type defs
            var unpackedWriter = new HeaderTypeDefinitionsWriter(Writer, unpackedTypeDefs);
            unpackedWriter.Write();
        }

        private void WriteEnums()
        {
            if (this.TypeDefinitionsManager.EnumDefinitions.Any())
            {
                new BlockCommentWriter(Writer, "Enumerations");
                foreach (var enumDef in TypeDefinitionsManager.EnumDefinitions)
                {
                    new EnumWriter(Writer, enumDef);
                    WriteBlankLine();
                }
            }
        }

        private void WriteSchemaVersionMacro(string fileNameNoExtensionUpper)
        {
            Writer.WriteLine(string.Concat("#define ",
                                           fileNameNoExtensionUpper,
                                           "_SCHEMA_VERSION ",
                                           TypeDefinitionsManager.Version));
            WriteBlankLine();
        }

        private void WriteMetaDataCompilerVersionMacro(string fileNameNoExtensionUpper)
        {
            Writer.WriteLine(string.Concat("#define ", fileNameNoExtensionUpper, "_METADATA_COMPILER_VERSION 2"));
            WriteBlankLine();
        }

        private void WriteNumTypeDefsMacro(string fileNameNoExtensionUpper)
        {
            Writer.WriteLine("// NOTE: doesn't include abstract objects");
            Writer.WriteLine(string.Concat("#define AUD_NUM_",
                                           fileNameNoExtensionUpper,
                                           " ",
                                           TypeDefinitionsManager.ConcreteTypeDefinitions.Count()));
            WriteBlankLine();
        }

        private void WritePrototypes()
        {
            Writer.WriteLine("// PURPOSE - Gets the type id of the parent class from the given type id");
            Writer.WriteLine(string.Concat(Utility.GetBaseTypeIdDeclaration(MetadataType), ";"));
            WriteBlankLine();

            Writer.WriteLine("// PURPOSE - Determines if a type inherits from another type");
            Writer.WriteLine(string.Concat(Utility.GetIsOfTypeIdWithIdDeclaration(MetadataType), ";"));
            WriteBlankLine();

            Writer.WriteLine("// PURPOSE - Determines if a type inherits from another type");
            Writer.WriteLine("template<class _ObjectType>");
            var functionDeclaration = string.Concat("bool g",
                                                    MetadataType,
                                                    "IsOfType(const _ObjectType* const obj, const rage::u32 baseTypeId)");
            using (new BlockWriter(Writer, functionDeclaration, null, false))
            {
                Writer.WriteLine(string.Concat("return g", MetadataType, "IsOfType(obj->ClassId, baseTypeId);"));
            }

            WriteBlankLine();
        }

        private void SortTypeDefsByPacking(out IList<ITypeDefinition> packedTypeDefs,
                                           out IList<ITypeDefinition> unpackedTypeDefs)
        {
            packedTypeDefs = new List<ITypeDefinition>();
            unpackedTypeDefs = new List<ITypeDefinition>();
            foreach (var typeDef in TypeDefinitionsManager.AllTypeDefinitions)
            {
                if (typeDef.IsPacked)
                {
                    packedTypeDefs.Add(typeDef);
                }
                else
                {
                    unpackedTypeDefs.Add(typeDef);
                }
            }
        }

        private static IEnumerable<IEnumValue> GetTriStateEnumValues()
        {
            return new List<IEnumValue>
                       {
                           new EnumValue {Name = "AUD_TRISTATE_FALSE", Value = 0},
                           new EnumValue {Name = "AUD_TRISTATE_TRUE", Value = 1},
                           new EnumValue {Name = "AUD_TRISTATE_UNSPECIFIED", Value = 2}
                       };
        }
    }
}