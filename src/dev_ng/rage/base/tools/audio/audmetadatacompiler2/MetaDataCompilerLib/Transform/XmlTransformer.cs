using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using rage.ToolLib.Logging;

namespace rage.Transform
{
    [CanTransform("*")]
    public class XmlTransformer : ITransformer
    {
        private const string MISSING_ARGUMENT = "XmlTransformerAndValidator:Init was not given a .xsl file to load";
        private const string FORMAT_FAILED_TO_LOAD_XML_TRANSFORM = "Failed to load Xml transform: {0}";
        private const string FORMAT_LOADED_XML_TRANSFORM = "Loaded Xml transform: {0}";
        private const string FORMAT_TRANSFORM_FAILED = "Failed to tranform element \"{0}\" using XML transform: {1}";

        private const string TRANSFORM_CALLED_BEFORE_INIT =
            "XmlTransformer.Transform() called without calling Init() first.";

        private static readonly IDictionary<string, XslCompiledTransform> ms_transforms;
        private string m_xslFile;
        private XslCompiledTransform m_transform;

        static XmlTransformer()
        {
            ms_transforms = new Dictionary<string, XslCompiledTransform>();
        }

        #region ITransformer Members

        public bool Init(ILog log, params string[] args)
        {
            if (args.Length >= 1)
            {
                var xslFile = args[0].ToLower();
                try
                {
                    if (!ms_transforms.TryGetValue(xslFile, out m_transform))
                    {
                        m_transform = new XslCompiledTransform();
                        m_transform.Load(xslFile);
                        ms_transforms.Add(xslFile, m_transform);
                        log.Information(FORMAT_LOADED_XML_TRANSFORM, xslFile);
                    }
                    m_xslFile = xslFile;
                    return true;
                }
                catch (Exception ex)
                {
                    log.Error(FORMAT_FAILED_TO_LOAD_XML_TRANSFORM, xslFile);
                    log.Exception(ex);
                }
            }
            else
            {
                log.Error(MISSING_ARGUMENT);
            }
            return false;
        }

        public TransformResult Transform(ILog log, XElement element)
        {
            if (!string.IsNullOrEmpty(m_xslFile) &&
                m_transform != null)
            {
                using (var inputStream = new MemoryStream())
                {
                    using (var resultsStream = new MemoryStream())
                    {
                        try
                        {
                            var writer = XmlWriter.Create(inputStream);
                            element.Save(writer);
                            writer.Close();

                            inputStream.Seek(0, SeekOrigin.Begin);
                            var input = XmlReader.Create(inputStream);
                            var results = XmlWriter.Create(resultsStream);
                            m_transform.Transform(input, results);
                            results.Close();
                            resultsStream.Seek(0, SeekOrigin.Begin);

                            var output = XmlReader.Create(resultsStream);
                            var doc = XDocument.Load(output);
                            return new TransformResult {Result = doc.Root};
                        }
                        catch (XsltException ex)
                        {
                            log.Error(FORMAT_TRANSFORM_FAILED, element.Name, m_xslFile);
                            log.Exception(ex);
                        }
                        catch (Exception ex)
                        {
                            log.Exception(ex);
                        }
                    }
                }
            }
            else
            {
                log.Error(TRANSFORM_CALLED_BEFORE_INIT);
            }
            return null;
        }

        public void Shutdown()
        {
            m_transform = null;
            m_xslFile = null;
        }

        #endregion
    }
}