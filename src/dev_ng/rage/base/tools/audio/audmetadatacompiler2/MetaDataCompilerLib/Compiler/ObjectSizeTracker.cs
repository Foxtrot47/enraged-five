namespace rage.Compiler
{
    public class ObjectSizeTracker : IObjectSizeTracker
    {
        public ObjectSizeTracker()
        {
            LargestObjectName = string.Empty;
        }

        #region IObjectSizeTracker Members

        public string LargestObjectName { get; private set; }

        public uint LargestObjectSize { get; private set; }

        public uint AverageObjectSize
        {
            get
            {
                if (NumObjects == 0)
                {
                    return 0;
                }
                return TotalSize / NumObjects;
            }
        }

        public uint NumObjects { get; private set; }

        public uint TotalSize { get; private set; }

        public void Record(string name, uint size)
        {
            if (size > LargestObjectSize)
            {
                LargestObjectSize = size;
                LargestObjectName = name;
            }
            TotalSize += size;
            NumObjects += 1;
        }

        #endregion
    }
}