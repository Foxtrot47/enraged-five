using System;
using System.Collections.Generic;
using System.Text;
using rage.ToolLib;
using rage.ToolLib.Encryption;
using rage.ToolLib.Writer;

namespace rage.Compiler
{
    public class StringTable : IStringTable
    {
        private HashSet<uint> m_stringLookup;

        private List<string> m_stringTable;

        private List<uint> m_stringTableOffsets;

        public StringTable()
        {
            Clear();
        }

        #region IStringTable Members

        public uint Add(Hash hash)
        {
            if (hash.Key == 0)
            {
                throw new ArgumentException("hash should not be 0");
            }

            if (!m_stringLookup.Contains(hash.Key))
            {
                m_stringLookup.Add(hash.Key);
                m_stringTable.Add(hash.Value);

                var length = (uint) (hash.Value.Length + 1);
                var count = m_stringTableOffsets.Count - 1;
                m_stringTableOffsets.Add(count >= 0 ? m_stringTableOffsets[count] + length : length);
            }
            return hash.Key;
        }

        public int IndexOf(string item)
        {
            if (string.IsNullOrEmpty(item))
            {
                return -1;
            }
            return m_stringTable.IndexOf(item);
        }

        public void Serialize(IWriter output, IEncrypter encrypter=null)
        {
            EncryptionWriter encryptionWriter = new EncryptionWriter(encrypter, output.IsBigEndian, output.Encoding);
            var count = m_stringTable.Count;
            encryptionWriter.Write(count);
            for (var i = 0; i < count; ++i)
            {
                encryptionWriter.Write(m_stringTableOffsets[i]);
            }
            foreach (var entry in m_stringTable)
            {
                var encodedStr = Encoding.ASCII.GetBytes(entry);
                var outBuffer = new byte[encodedStr.Length + 1];
                Buffer.BlockCopy(encodedStr, 0, outBuffer, 0, encodedStr.Length);
                encryptionWriter.Write(outBuffer, false);
            }

            output.Write(encryptionWriter.getOutputData(true), false);
        }

        public void Clear()
        {
            m_stringLookup = new HashSet<uint>();
            m_stringTable = new List<string>();
            m_stringTableOffsets = new List<uint> {0};
        }

        #endregion
    }
}