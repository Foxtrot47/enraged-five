using System.Xml.Linq;

namespace rage
{
    public interface IDefinition
    {
        string Name { get; }

        bool Parse(XElement element);
    }
}