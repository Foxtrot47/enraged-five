namespace rage.Compiler
{
    public interface IObjectSizeTracker
    {
        string LargestObjectName { get; }

        uint LargestObjectSize { get; }

        uint AverageObjectSize { get; }

        uint NumObjects { get; }

        uint TotalSize { get; }

        void Record(string name, uint size);
    }
}