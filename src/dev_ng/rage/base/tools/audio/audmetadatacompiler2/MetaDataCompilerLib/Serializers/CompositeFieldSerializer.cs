using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using rage.CodeGeneration;
using rage.Fields;
using rage.Serialization;
using rage.Serializers.Utilities;
using rage.ToolLib;
using rage.ToolLib.Logging;
using rage.ToolLib.Writer;

namespace rage.Serializers
{
    [CanSerialize(typeof (ICompositeFieldDefinition), true)]
    public class CompositeFieldSerializer : ISerializer
    {
        private const string FORMAT_INSTANCES_EXCEED_MAXIMUM =
            "Instances of CompositeField: \"{0}\" exceeds maximum allowed in TypeDefinition.";

        private const string FAILED_TO_SERIALIZE_NUM_OCCURRENCES =
            "Failed to serialize the number of occurrences of a CompositeField";

        private const string INSTANCE_COUNT = "InstanceCount";

        private const string FORMAT_FAILED_PROCESSING_COMPOSITE_FIELD_INSTANCE =
            "Failed to process instance of CompositeField: {0}";

        private const string FORMAT_FAILED_DEFAULT_COMPOSITE_FIELD_SERIALIZATION =
            "Failed to serialize default instance of CompositeField: {0}";

        private const string FORMAT_NO_MAX_OCCURS_SERIALIZER =
            "Could not retrieve ISerializer that can serialize {0} occurrences of a CompositeField";

        private readonly ICompositeFieldDefinition m_fieldDefinition;
        private readonly IFieldProcessor m_fieldProcessor;
        private readonly ILog m_log;
        private uint m_alignment;
        private bool m_alignmentCalculated;
        private ISerializer m_maxOccursSerializer;

        public CompositeFieldSerializer(ILog log, ICompositeFieldDefinition fieldDefinition)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (fieldDefinition == null)
            {
                throw new ArgumentNullException("fieldDefinition");
            }
            m_log = log;
            m_fieldDefinition = fieldDefinition;
            m_fieldProcessor = new FieldProcessor(log, m_fieldDefinition);
            m_alignment = 1;
        }

        #region ISerializer Members

        public uint Alignment
        {
            get
            {
                if (!m_alignmentCalculated)
                {
                    CalculateAlignment();
                    m_alignmentCalculated = true;
                }
                return m_alignment;
            }
        }

        public int Compare(XElement x, XElement y)
        {
            throw new NotSupportedException("Comparison of Composite Field type not supported.");
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            var instanceCount = 0;
            IEnumerable<XElement> instances = null;
            if (element != null)
            {
                instances = element.Elements(FieldDefinitionTokens.Instance.ToString());
                instanceCount = instances.Count();
            }

            if (instanceCount > m_fieldDefinition.MaxOccurs)
            {
                m_log.Error(FORMAT_INSTANCES_EXCEED_MAXIMUM, element.Name);
                return false;
            }

            if (m_fieldDefinition.MaxOccurs > 1)
            {
                if (!SerializeMaxOccurs(instanceCount, output, fieldDefinition))
                {
                    return false;
                }
            }

            if (instanceCount > 1 && !string.IsNullOrEmpty(m_fieldDefinition.OrderBy))
            {
                //order instances by specified field
                ICommonFieldDefinition fieldDefToCompare = null;
                foreach (var fieldDef in m_fieldDefinition.AllFields)
                {
                    if (fieldDef.Ignore || !fieldDef.Name.Equals(m_fieldDefinition.OrderBy))
                    {
                        continue;
                    }
                    fieldDefToCompare = fieldDef;
                    break;
                }

                if (null != fieldDefToCompare)
                {
                    //sort instances if field to compare on is found
                    IComparer<XElement> comparer = new ElementComparer(fieldDefToCompare);
                    var instancesArray = instances.ToArray();
                    Array.Sort(instancesArray, comparer);
                    instances = instancesArray;
                }
                else
                {
                    throw new ArgumentException(
                        "Field with specified 'orderBy' value not found in Composite Field: " + 
                        m_fieldDefinition.OrderBy);
                }
            }
            return SerializeInstances(element, instances, instanceCount, output);
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            // this should never be called
            throw new NotImplementedException();
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            var maxOccurs = m_fieldDefinition.MaxOccurs;
            var fieldName = m_fieldDefinition.Name;
            var fieldNamePlural = fieldName;
            if (!fieldNamePlural.EndsWith("s"))
            {
                fieldNamePlural = String.Concat(fieldNamePlural, "s");
            }
            var fieldNamePluralUpper = fieldNamePlural.ToUpper();

            writer.WriteLine();
            if (maxOccurs > 1)
            {
                WriteMaxOccursStructureEntry(fieldNamePlural, writer, maxOccurs, fieldNamePluralUpper);
            }

            var dataAfterBracket = fieldName;
            if (maxOccurs > 1)
            {
                dataAfterBracket = String.Concat(dataAfterBracket, "[MAX_", fieldNamePluralUpper, "]");
            }
            WriteStruct(fieldName, writer, dataAfterBracket);
            writer.WriteLine();
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            foreach (var fieldDef in m_fieldDefinition.AllFields)
            {
                if (IsValidField(fieldDef))
                {
                    fieldDef.Serializer.WriteFunctions(writer, fieldDef);
                }
            }
        }

        #endregion

        private void CalculateAlignment()
        {
            uint bitCount = 0;
            foreach (var fieldDef in m_fieldDefinition.AllFieldsInherited)
            {
                if (fieldDef.Ignore)
                {
                    continue;
                }

                if (fieldDef.Alignment > m_alignment)
                {
                    m_alignment = fieldDef.Alignment;
                }

                // Bitfields get special treatment
                if (fieldDef.Serializer.GetType().TypeHandle.Equals(typeof (BitFieldSerializer)))
                {
                    bitCount += 1;
                }
            }

            if (bitCount > 0)
            {
                var bitFieldAlignment = bitCount / 8;
                if ((bitCount % 8) > 0)
                {
                    bitFieldAlignment += 1;
                }
                if (bitFieldAlignment > m_alignment)
                {
                    m_alignment = bitFieldAlignment;
                }
            }

            if (GetMaxOccursSerializer())
            {
                if (m_maxOccursSerializer.Alignment > m_alignment)
                {
                    m_alignment = m_maxOccursSerializer.Alignment;
                }
            }
        }

        private bool SerializeMaxOccurs(int instanceCount, IWriter output, ICommonFieldDefinition fieldDefinition)
        {
            if (!GetMaxOccursSerializer())
            {
                m_log.Error(String.Format(FORMAT_NO_MAX_OCCURS_SERIALIZER, m_fieldDefinition.MaxOccurs));
                return false;
            }

            if (!m_fieldDefinition.IsPacked)
            {
                Utility.AlignToBoundary(output, m_maxOccursSerializer.Alignment);
            }

            if (!m_maxOccursSerializer.Serialize(m_log, output, new XElement(INSTANCE_COUNT, instanceCount), fieldDefinition))
            {
                m_log.Error(FAILED_TO_SERIALIZE_NUM_OCCURRENCES);
                return false;
            }
            return true;
        }

        private bool SerializeInstances(XElement element,
                                        IEnumerable<XElement> instances,
                                        int instanceCount,
                                        IWriter output)
        {
            // We have instances
            if (instanceCount > 0)
            {
                if (m_fieldDefinition.BitField != null)
                {
                    foreach (var instance in instances)
                    {
                        ProcessInstanceBitFields(instance, output);
                    }
                }
                else
                {
                    foreach (var instance in instances)
                    {
                        if (!m_fieldDefinition.IsPacked)
                        {
                            Utility.AlignToBoundary(output, m_fieldDefinition.Alignment);
                        }

                        if (!m_fieldProcessor.Process(instance, output))
                        {
                            m_log.Error(FORMAT_FAILED_PROCESSING_COMPOSITE_FIELD_INSTANCE, element.Name);
                            return false;
                        }
                        if (!m_fieldDefinition.IsPacked)
                        {
                            Utility.AlignToBoundary(output, m_fieldDefinition.Alignment);
                        }
                    }
                }

                if (!SerializeDefaultInstances(instanceCount, output))
                {
                    return false;
                }
            }
            else
            {
                if (!SerializeDefaultInstances(0, output))
                {
                    return false;
                }
            }
            return true;
        }

        private void WriteStruct(string fieldName, TextWriter writer, string dataAfterBracket)
        {
            if (m_fieldDefinition.Unit == Units.Vector3)
            {
                writer.WriteLine(string.Concat("rage::Vec3V", " ", fieldName, ";"));
            }
            else if (m_fieldDefinition.Unit == Units.Vector4)
            {
                writer.WriteLine(string.Concat("rage::Vec4V", " ", fieldName, ";"));
            }
            else
            {
                var indentedWriter = writer as IndentedTextWriter;
                var name = String.Concat("t", fieldName);
                using (new BlockWriter(indentedWriter, "struct", name, dataAfterBracket, true))
                {
                    if (m_fieldDefinition.BitField != null)
                    {
                        WriteBitFieldStructureEntry(fieldName, indentedWriter);
                    }
                    else
                    {
                        foreach (var childFieldDef in m_fieldDefinition.AllFields)
                        {
                            if (!childFieldDef.Type.TypeHandle.Equals(typeof (TriState).TypeHandle) &&
                                !childFieldDef.Ignore)
                            {
                                childFieldDef.Serializer.WriteStructureEntry(writer);
                            }
                        }
                    }
                }
            }
        }

        private void WriteMaxOccursStructureEntry(string fieldNamePlural,
                                                  TextWriter writer,
                                                  uint maxOccurs,
                                                  string fieldNamePluralUpper)
        {
            if (!GetMaxOccursSerializer())
            {
                throw new Exception(String.Format(FORMAT_NO_MAX_OCCURS_SERIALIZER, maxOccurs));
            }

            writer.Write("static const ");
            m_maxOccursSerializer.WriteStructureEntry(writer);
            writer.WriteLine(String.Concat(" MAX_", fieldNamePluralUpper, " = ", maxOccurs, ";"));

            m_maxOccursSerializer.WriteStructureEntry(writer);
            writer.WriteLine(String.Concat(" num", fieldNamePlural, ";"));
        }

        private void WriteBitFieldStructureEntry(string fieldName, IndentedTextWriter writer)
        {
            if (!m_fieldDefinition.IsPacked)
            {
                writer.WriteLine(String.Concat("t", fieldName, "()"));
                writer.WriteLine(" : Value(0U)");
                writer.WriteLine("{");
                writer.WriteLine("}");
            }

            var bitField = m_fieldDefinition.BitField;
            using (new BlockWriter(writer, "union", null, true))
            {
                writer.WriteLine(String.Concat(bitField.DataType, " Value;"));
                using (new BlockWriter(writer, "struct", null, "BitFields", true))
                {
                    var padding = bitField.Field.Length * 8 - bitField.NumBits;
                    var paddingText = String.Concat("bool padding:", padding, "; // padding");

                    var codeStatements =
                        m_fieldDefinition.AllFields.Select(fieldDef => String.Concat("bool ", fieldDef.Name, ":1;")).
                            ToList();

                    writer.WriteLine("#if __BE");
                    writer.Indent += 1;

                    WriteBigEndianBitFieldStructureEntry(writer, padding, paddingText, codeStatements);

                    writer.Indent -= 1;
                    writer.WriteLine("#else // __BE");
                    writer.Indent += 1;

                    WriteLittleEndianBitFieldStructureEntry(writer, padding, paddingText, codeStatements);

                    writer.Indent -= 1;
                    writer.WriteLine("#endif // __BE");
                }
            }
        }

        private static void WriteLittleEndianBitFieldStructureEntry(TextWriter writer,
                                                                    int padding,
                                                                    string paddingText,
                                                                    IEnumerable<string> codeStatements)
        {
            foreach (var codeStatement in codeStatements)
            {
                writer.WriteLine(codeStatement);
            }

            if (padding > 0)
            {
                writer.WriteLine(paddingText);
            }
        }

        private static void WriteBigEndianBitFieldStructureEntry(TextWriter writer,
                                                                 int padding,
                                                                 string paddingText,
                                                                 IList<string> codeStatements)
        {
            if (padding > 0)
            {
                writer.WriteLine(paddingText);
            }

            for (var i = codeStatements.Count - 1; i >= 0; --i)
            {
                writer.WriteLine(codeStatements[i]);
            }
        }

        private bool GetMaxOccursSerializer()
        {
            if (m_maxOccursSerializer == null)
            {
                m_maxOccursSerializer =
                    SerializerLookup.Get(m_fieldDefinition.NumType ??
                                         SerializerLookup.GetTypeFromSize(m_fieldDefinition.MaxOccurs));
                if (m_maxOccursSerializer == null)
                {
                    return false;
                }
            }
            return true;
        }

        private bool SerializeDefaultInstances(int instanceCount, IWriter output)
        {
            if (m_fieldDefinition.IsFixedSize ||
                m_fieldDefinition.MaxOccurs == 1)
            {
                for (var i = instanceCount; i < m_fieldDefinition.MaxOccurs; ++i)
                {
                    if (!m_fieldDefinition.IsPacked)
                    {
                        Utility.AlignToBoundary(output, m_fieldDefinition.Alignment);
                    }

                    if (m_fieldDefinition.BitField != null)
                    {
                        ProcessInstanceBitFields(null, output);
                    }
                    else if (!m_fieldProcessor.Process(null, output))
                    {
                        m_log.Error(FORMAT_FAILED_DEFAULT_COMPOSITE_FIELD_SERIALIZATION, m_fieldDefinition.Name);
                        return false;
                    }

                    if (!m_fieldDefinition.IsPacked)
                    {
                        Utility.AlignToBoundary(output, m_fieldDefinition.Alignment);
                    }
                }
            }
            return true;
        }

        private void ProcessInstanceBitFields(XContainer instance, IWriter output)
        {
            var bitField = m_fieldDefinition.BitField;
            bitField.Clear();

            var index = 0;
            foreach (var bitFieldDef in m_fieldDefinition.AllFields)
            {
                var defaultValue = (bitFieldDef as IBasicFieldDefinition).Default;
                // TODO: Ask Ali about serializing default values here.....
                if (instance != null)
                {
                    var instanceElement = instance.Element(bitFieldDef.Name);
                    bitField.Set(index,
                                 Utility.ToBoolean(instanceElement != null ? instanceElement.Value : defaultValue));
                }
                else if (Utility.ToBoolean(defaultValue))
                {
                    bitField.Set(index, true);
                }
                index += 1;
            }
            output.Write(bitField.Field, true);
        }

        public static bool IsValidField(ICommonFieldDefinition fieldDef)
        {
            return !fieldDef.Ignore && !fieldDef.Type.TypeHandle.Equals(typeof (TriState).TypeHandle) &&
                   !fieldDef.Type.TypeHandle.Equals(typeof (BitField).TypeHandle);
        }
    }
}