using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using rage.Compiler;
using rage.ToolLib.Logging;
using rage.ToolLib.Writer;

namespace rage
{
    public static class Utility
    {
        public const string XML_SEARCH_PATTERN = "*.xml";

        public const uint HEADER_OFFSET = 8; // 8 bytes used to store version and size at the beginning of each file 

        private const string YES = "yes";

        public static bool ToBoolean(string value)
        {
            bool val;
            if (!Boolean.TryParse(value, out val))
            {
                val = String.Compare(value, YES, true) == 0;
            }
            return val;
        }

        public static string EncloseInBrackets(string value)
        {
            return String.Concat("(", value, ")");
        }

        public static string FormatEnumToStringParseMethodDeclaration(string enumName)
        {
            return String.Concat("const char* ", enumName, "_ToString(const ", enumName, " value)");
        }

        public static string FormatStringToEnumParseMethodDeclaration(string enumName)
        {
            return String.Concat(enumName, " ", enumName, "_Parse(const char* str, const ", enumName, " defaultValue)");
        }

        public static string GetBaseTypeIdDeclaration(string metaDataType)
        {
            return String.Concat("rage::u32 g", metaDataType, "GetBaseTypeId(const rage::u32 classId)");
        }

        public static string GetMaxSizeDeclaration(string classFactoryFunctionName)
        {
            return String.Concat("rage::u32 ", classFactoryFunctionName, "MaxSize()");
        }

        public static string GetTypeNameDeclaration(string classFactoryFunctionName)
        {
            return String.Concat("const char* ", classFactoryFunctionName, "GetTypeName(const rage::u32 classId)");
        }

        public static string GetIsOfTypeIdWithIdDeclaration(string metaDataType)
        {
            return String.Concat("bool g",
                                 metaDataType,
                                 "IsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId)");
        }

        public static void AlignToBoundary(IWriter output, uint alignmentBoundary)
        {
            if (alignmentBoundary > 1)
            {
                var streamPos = output.Tell() - HEADER_OFFSET;
                var overlap = streamPos % alignmentBoundary;
                if (overlap > 0)
                {
                    output.Write(new byte[alignmentBoundary - overlap], false);
                }
            }
        }

        public static List<DocumentEntry> LoadDocuments(ILog log, string directory)
        {
            if (!Directory.Exists(directory))
            {
                return new List<DocumentEntry>();
            }

            return (from documentPath in Directory.GetFiles(directory, XML_SEARCH_PATTERN, SearchOption.AllDirectories)
                    let document = ToolLib.Utility.LoadDocument(log, documentPath)
                    where document != null
                    select new DocumentEntry {DocumentPath = documentPath, Document = document}).ToList();
        }
    }
}