using System;
using System.Collections.Generic;
using System.IO;

namespace rage.CodeGeneration
{
    public class IncludesWriter
    {
        public IncludesWriter(TextWriter writer, IEnumerable<string> includePaths)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (includePaths == null)
            {
                throw new ArgumentNullException("includePaths");
            }

            Write(writer, includePaths);
        }

        private static void Write(TextWriter writer, IEnumerable<string> includePaths)
        {
            foreach (var includePath in includePaths)
            {
                writer.WriteLine(string.Format("#include \"{0}\"", includePath));
            }
        }
    }
}