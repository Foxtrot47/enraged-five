using System.Xml.Linq;
using rage.ToolLib.Writer;

namespace rage.Fields
{
    using rage.Compiler;

    public interface IFieldProcessor
    {
        bool Process(XElement element, IWriter output, CompressionBitField compression = null);
    }
}