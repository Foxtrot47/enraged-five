using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using rage.Fields;
using rage.ToolLib.Logging;

namespace rage.Templates
{
    public class TemplateProcessor : ITemplateProcessor
    {
        private const string FORMAT_TEMPLATE_NOT_FOUND = "Template: \"{0}\" could not be found.";

        private const string REQUIRED_ATTRIBUTES_MISSING =
            "TemplateInstance missing the required \"name\" and \"template\" attributes.";

        private const string FORMAT_MISSING_NAME_ATTRIBUTE = "Template element: {0} does not have a \"name\" attribute.";

        private const string FORMAT_PREPEND_INSTANCE_NAME = "{0}_{1}";

        private const string FORMAT_FAILED_PROCESSING_COMPOSITE_FIELD = "Failed to process composite field: {0}.";

        private const string FORMAT_FAILED_PROCESSING_COMPOSITE_FIELD_INSTANCE =
            "Failed to process an instance of composite field: {0}.";

        private const string FORMAT_UNKNOWN_FIELD_TYPE = "Unknown field type: \"{0}\" encountered in template: \"{1}\".";

        private const string FORMAT_PROCESSING_TEMPLATE_INSTANCE = "Processing TemplateInstance = {0} of template = {1}";

        private readonly ILog m_log;

        private readonly IDictionary<string, ITemplate> m_templates;

        private readonly HashSet<string> m_unknownFieldLookup;

        public TemplateProcessor(ILog log, IDictionary<string, ITemplate> templates)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (templates == null)
            {
                throw new ArgumentNullException("templates");
            }
            m_log = log;
            m_templates = templates;
            m_unknownFieldLookup = new HashSet<string>();
        }

        #region ITemplateProcessor Members

        public bool Process(XDocument doc)
        {
            var templateInstances =
                (from element in doc.Descendants()
                 where element.Name == TemplateTokens.TemplateInstance.ToString()
                 select element).ToList();
            if (templateInstances.Count() > 0)
            {
                if (templateInstances.All(templateInstance => ProcessTemplateInstance(doc, templateInstance)))
                {
                    foreach (var t in templateInstances)
                    {
                        t.Remove();
                    }
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        public bool ProcessTemplateInstance(XDocument doc, XElement templateInstanceElement)
        {
            string templateName, instanceName;
            if (!ParseTemplateInstanceAttributes(templateInstanceElement, out templateName, out instanceName))
            {
                return false;
            }
            ITemplate template;
            if (!m_templates.TryGetValue(templateName, out template))
            {
                m_log.Error(FORMAT_TEMPLATE_NOT_FOUND, templateName);
                return false;
            }
            m_log.Information(FORMAT_PROCESSING_TEMPLATE_INSTANCE, instanceName, templateName);
            var templateElements = new List<XElement>();
            var exposedElements = new Dictionary<string, XElement>();

            var internalFieldNames = template.Elements.Select(entry => entry.Value.Attribute("name").Value).ToList();

            foreach (var entry in template.Elements)
            {
                var typeDef = entry.Key;
                var rootElement = new XElement(entry.Value);
                if (!AdjustName(templateName, rootElement, instanceName))
                {
                    return false;
                }
                if (!ProcessFields(rootElement, typeDef, instanceName, templateName, exposedElements, internalFieldNames))
                {
                    return false;
                }
                templateElements.Add(rootElement);
            }
            if (!ProcessInstanceElements(templateInstanceElement, exposedElements))
            {
                return false;
            }
            var parent = templateInstanceElement.Parent ?? doc.Root;
            parent.Add(templateElements);
            return true;
        }

        private static bool ProcessInstanceElements(XContainer templateInstanceElement,
                                                    IDictionary<string, XElement> exposedElements)
        {
            foreach (var instanceElement in templateInstanceElement.Elements())
            {
                XElement realElement;
                if (exposedElements.TryGetValue(instanceElement.Name.ToString(), out realElement))
                {
                    if (instanceElement.HasElements)
                    {
                        foreach (var element in instanceElement.Elements())
                        {
                            realElement.Element(element.Name).Value = element.Value;
                        }
                    }
                    else
                    {
                        realElement.Value = instanceElement.Value;
                    }
                }
            }
            return true;
        }

        private bool AdjustName(string templateName, XElement rootElement, string instanceName)
        {
            var nameAttribute = rootElement.Attribute(TemplateTokens.Name.ToString().ToLower());
            if (nameAttribute != null)
            {
                nameAttribute.SetValue(nameAttribute.Value == templateName
                                           ? instanceName
                                           : string.Format(FORMAT_PREPEND_INSTANCE_NAME,
                                                           instanceName,
                                                           nameAttribute.Value));
            }
            else
            {
                m_log.Error(FORMAT_MISSING_NAME_ATTRIBUTE, rootElement.Name);
                return false;
            }
            return true;
        }

        private bool ProcessFields(XContainer rootElement,
                                   IFieldContainer fieldContainer,
                                   string instanceName,
                                   string templateName,
                                   IDictionary<string, XElement> exposedElements,
                                   IList<string> internalFieldNames)
        {
            foreach (var fieldElement in rootElement.Elements())
            {
                ICommonFieldDefinition fieldDef;
                if (fieldContainer.AllFieldsInheritedLookup.TryGetValue(fieldElement.Name.ToString(), out fieldDef))
                {
                    var compositeFieldDefinition = fieldDef as ICompositeFieldDefinition;
                    if (compositeFieldDefinition != null)
                    {
                        // This is a composite field so process it recursively
                        var compositeInstanceElements = fieldElement.Elements(FieldDefinitionTokens.Instance.ToString());
                        if (compositeInstanceElements.Count() > 0)
                        {
                            // We have the new format with multiple instances under one root
                            if (
                                !compositeInstanceElements.All(
                                    instanceElement =>
                                    ProcessFields(instanceElement,
                                                  compositeFieldDefinition,
                                                  instanceName,
                                                  templateName,
                                                  exposedElements,
                                                  internalFieldNames)))
                            {
                                m_log.Error(FORMAT_FAILED_PROCESSING_COMPOSITE_FIELD_INSTANCE,
                                            compositeFieldDefinition.Name);
                                return false;
                            }
                        }
                        else
                        {
                            // This is a standalone composite field (i.e., in the old format)
                            if (
                                !ProcessFields(fieldElement,
                                               compositeFieldDefinition,
                                               instanceName,
                                               templateName,
                                               exposedElements,
                                               internalFieldNames))
                            {
                                m_log.Error(FORMAT_FAILED_PROCESSING_COMPOSITE_FIELD, compositeFieldDefinition.Name);
                                return false;
                            }
                        }
                    }
                    else
                    {
                        // This is either a basic field or an allocated space field
                        if (fieldDef.Unit == Units.ObjectRef)
                        {
                            // Only prepend instance name if internal reference
                            if (internalFieldNames.Contains(fieldElement.Value))
                            {
                                fieldElement.Value = string.Format(FORMAT_PREPEND_INSTANCE_NAME,
                                                               instanceName,
                                                               fieldElement.Value);
                            }
                            else
                            {
                                fieldElement.Value = fieldElement.Value;
                            }
                        }
                    }
                }
                else
                {
                    var fieldName = fieldElement.Name.ToString();
                    if (!m_unknownFieldLookup.Contains(fieldName))
                    {
                        m_log.Warning(FORMAT_UNKNOWN_FIELD_TYPE, fieldName, templateName);
                        m_unknownFieldLookup.Add(fieldName);
                    }
                }
                var exposeAsAttribute = fieldElement.Attribute(TemplateTokens.ExposeAs.ToString());
                if (exposeAsAttribute != null)
                {
                    exposedElements.Add(exposeAsAttribute.Value, fieldElement);
                    fieldElement.SetAttributeValue(TemplateTokens.ExposeAs.ToString(), null);
                }
            }
            return true;
        }

        private bool ParseTemplateInstanceAttributes(XElement templateInstanceElement,
                                                     out string templateName,
                                                     out string instanceName)
        {
            templateName = instanceName = string.Empty;
            var nameAttrib = templateInstanceElement.Attribute(TemplateTokens.Name.ToString().ToLower());
            var templateNameAttrib = templateInstanceElement.Attribute(TemplateTokens.Template.ToString().ToLower());
            if (nameAttrib != null &&
                templateNameAttrib != null)
            {
                instanceName = nameAttrib.Value;
                templateName = templateNameAttrib.Value;
            }
            else
            {
                m_log.Error(REQUIRED_ATTRIBUTES_MISSING);
                return false;
            }
            return true;
        }
    }
}