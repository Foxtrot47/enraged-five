using rage.ToolLib;
using rage.ToolLib.Encryption;
using rage.ToolLib.Writer;

namespace rage.Compiler
{
    public interface IStringTable
    {
        uint Add(Hash hash);

        int IndexOf(string item);

        void Serialize(IWriter output, IEncrypter encrypter = null);

        void Clear();
    }
}