namespace rage.Fields
{
    public enum FieldTypeTokens
    {
        S8,
        U8,
        S16,
        U16,
        S32,
        U32,
        S64,
        U64,
        F32,
        CString,
        String,
        TriState,
        Bit,
        Hash,
        PartialHash,
        Enum,
        Unknown
    }
}