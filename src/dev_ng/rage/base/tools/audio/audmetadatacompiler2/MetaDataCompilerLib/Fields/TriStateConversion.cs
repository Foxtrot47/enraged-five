using System.Xml.Linq;

namespace rage.Fields
{
    public class TriStateConversion
    {
        public static TriState ToTriState(XElement element, IBasicFieldDefinition basicFieldDef)
        {
            if (element == null)
            {
                if (!string.IsNullOrEmpty(basicFieldDef.Default))
                {
                    return Utility.ToBoolean(basicFieldDef.Default) ? TriState.True : TriState.False;
                }
                return TriState.Unspecified;
            }
            return Utility.ToBoolean(element.Value) ? TriState.True : TriState.False;
        }

        public static uint SetTriStateValue(uint flags, int flagIndex, TriState trival)
        {
            int bitIndex = flagIndex * 2;            
            // we need to first clear both bits associated with this flag
            uint newFlags = flags & ~(3U << bitIndex);
            // set bits as requested
            newFlags |= ((uint)trival & 3U) << bitIndex;
            return newFlags;
        }
    }
}