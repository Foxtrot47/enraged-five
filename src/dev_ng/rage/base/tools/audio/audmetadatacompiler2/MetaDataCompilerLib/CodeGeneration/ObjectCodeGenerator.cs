using System;
using rage.ToolLib.Logging;
using rage.Types;

namespace rage.CodeGeneration
{
    public class ObjectCodeGenerator : IObjectCodeGenerator
    {
        private readonly ILog m_log;

        private readonly audMetadataType m_metadataType;

        private audProjectSettings m_projectSettings;

        private readonly ITypeDefinitionsManager m_typeDefinitionsManager;

        public ObjectCodeGenerator(ILog log,
                                   audProjectSettings projectSettings,
                                   ITypeDefinitionsManager typeDefinitionsManager,
                                   audMetadataType metaDataType)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (typeDefinitionsManager == null)
            {
                throw new ArgumentNullException("typeDefinitionsManager");
            }
            if (metaDataType == null)
            {
                throw new ArgumentNullException("metaDataType");
            }
            if(projectSettings == null) 
            {
                throw new ArgumentNullException("projectSettings");
            }

            m_log = log;
            m_typeDefinitionsManager = typeDefinitionsManager;
            m_metadataType = metaDataType;
            m_projectSettings = projectSettings;
        }

        #region IObjectCodeGenerator Members

        public bool Generate(string path, string nameSpace)
        {
            // Note: Instantiating objects write to the file. Objects are enclosed in a "using" directive to make sure 
            // Note: dispose is called if the object does some final processing during the dispose call
            var result = true;
            try
            {
                var fullCodePath = string.Concat(path, m_metadataType.CodePath);
                m_log.WriteFormatted("Writing metadata definition header file: {0}.", fullCodePath);
                using (
                    new MetaDataHeaderFileGenerator(fullCodePath,
                                                    nameSpace,
                                                    m_metadataType.TypeNotUpper,
                                                    m_typeDefinitionsManager,
                                                    m_projectSettings,
                                                    m_log))
                {
                    // constructing and disposing the object writes the file
                }

                fullCodePath = fullCodePath.Replace(".h", ".cpp");
                m_log.WriteFormatted("Writing metadata functions C++ file: {0}.", fullCodePath);
                using (
                    new MetaDataCppFileGenerator(fullCodePath,
                                                 nameSpace,
                                                 m_metadataType.TypeNotUpper,
                                                 m_typeDefinitionsManager))
                {
                    // constructing and disposing the object writes the file
                }

                if (!string.IsNullOrEmpty(m_metadataType.ClassFactoryCodePath) &&
                    !string.IsNullOrEmpty(m_metadataType.ClassFactoryFunctionName) &&
                    !string.IsNullOrEmpty(m_metadataType.ClassFactoryFunctionReturnType))
                {
                    fullCodePath = string.Concat(path, m_metadataType.ClassFactoryCodePath);
                    m_log.WriteFormatted("Writing sound class factory header file: {0}.", fullCodePath);
                    using (
                        new ClassFactoryHeaderFileGenerator(fullCodePath,
                                                            nameSpace,
                                                            m_metadataType.TypeNotUpper,
                                                            m_typeDefinitionsManager,
                                                            m_metadataType.ClassFactoryFunctionName,
                                                            m_metadataType.ClassFactoryFunctionReturnType,
                                                            m_metadataType.UsesBucketAllocator))
                    {
                        // constructing and disposing the object writes the file
                    }
                }
            }
            catch (Exception ex)
            {
                m_log.Exception(ex);
                result = false;
            }
            return result;
        }

        #endregion
    }
}