using System.Collections.Generic;
using rage.Enums;

namespace rage.Types
{
    public interface ITypeDefinitionsManager
    {
        uint Version { get; }

        uint Mode { get; }

        IDictionary<string, KeysPerPlatform> EncryptionKeys { get; }

        IEnumerable<string> IncludePaths { get; }

        IEnumerable<ITypeDefinition> ConcreteTypeDefinitions { get; }

        IEnumerable<ITypeDefinition> AllTypeDefinitions { get; }

        IEnumerable<TypeDefData> TypeDefinitionsData { get; }

        IEnumerable<IEnumDefinition> EnumDefinitions { get; }

        IEnumerable<string> LoadingPaths { get; }

        bool Load(string workingDirectory, IEnumerable<audObjectDefinition> objectDefinitions);

        bool Reload();

        ITypeDefinition FindTypeDefinition(string name);

        IEnumDefinition FindEnumDefinition(string name);
    }
}