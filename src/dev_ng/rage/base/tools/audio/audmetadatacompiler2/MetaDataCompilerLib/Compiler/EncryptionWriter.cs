﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rage.ToolLib.Encryption;

namespace rage.Compiler {
    public class EncryptionWriter
    {
        /**
         * This class encrypts data with the given endianness and encoding if an encryptor is set.
         * If no encryptor is set, the data gets only converted to the given endianness and encoding.
         */
        public EncryptionWriter(IEncrypter encrypter, bool isBigEndian, Encoding encoding)
        {
            IsBigEndian = isBigEndian;
            this.encrypter = encrypter;
            this.encoding = encoding;
            this.data = new List<byte>();
        }

        public bool IsBigEndian { get; private set; }
        private IEncrypter encrypter;
        private List<byte> data;
        private Encoding encoding;

        public void Write(sbyte val)
        {
            data.Add((byte)val);
        }

        public void Write(byte val)
        {
           data.Add(val);
        }

        public void Write(byte[] val, bool needsEndian)
        {
            if (needsEndian && IsBigEndian)
            {
                val = ToolLib.Utility.Reverse(val);
            }
            data.AddRange(val);
        }

        public void Write(short val)
        {
            if (IsBigEndian)
            {
                var bytes = ToolLib.Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToInt16(bytes, 0);
            }
            data.AddRange(BitConverter.GetBytes(val));
        }

        public void Write(ushort val)
        {
            if (IsBigEndian)
            {
                var bytes = ToolLib.Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToUInt16(bytes, 0);
            }
            data.AddRange(BitConverter.GetBytes(val));
        }

        public void Write(int val)
        {
            if (IsBigEndian)
            {
                var bytes = ToolLib.Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToInt32(bytes, 0);
            }
            data.AddRange(BitConverter.GetBytes(val));
        }

        public void Write(uint val)
        {
            if (IsBigEndian)
            {
                var bytes = ToolLib.Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToUInt32(bytes, 0);
            }
            data.AddRange(BitConverter.GetBytes(val));
        }

        public void Write(long val)
        {
            if (IsBigEndian)
            {
                var bytes = ToolLib.Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToInt64(bytes, 0);
            }
            data.AddRange(BitConverter.GetBytes(val));
        }

        public void Write(ulong val)
        {
            if (IsBigEndian)
            {
                var bytes = ToolLib.Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToUInt64(bytes, 0);
            }
            data.AddRange(BitConverter.GetBytes(val));
        }

        public void Write(float val)
        {
            if (IsBigEndian)
            {
                var bytes = ToolLib.Utility.Reverse(BitConverter.GetBytes(val));
                val = BitConverter.ToSingle(bytes, 0);
            }
            data.AddRange(BitConverter.GetBytes(val));
        }

        public void Write(string val)
        {
            data.AddRange(encoding.GetBytes(val));
        }

        /**
         * This method returns the encrypted data if an encryptor was set or the unencrypted data otherwise
         */
        public byte[] getOutputData(bool padAsRequired=false)
        {
            if (encrypter != null) return encrypter.Encrypt(data.ToArray(), padAsRequired);
            else return data.ToArray();
        }
    }
}
