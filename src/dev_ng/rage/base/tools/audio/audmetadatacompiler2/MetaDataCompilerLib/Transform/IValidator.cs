using System.Xml.Linq;

namespace rage.Transform
{
    public interface IValidator
    {
        ValidationResult Validate(XElement element);
    }
}