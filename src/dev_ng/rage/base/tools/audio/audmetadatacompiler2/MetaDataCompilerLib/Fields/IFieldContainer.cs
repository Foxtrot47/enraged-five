using System.Collections.Generic;

namespace rage.Fields
{
    public interface IFieldContainer
    {
        bool ForceSerialization { get; set; }

        bool IsType { get; }

        bool IsCompressed { get; set; }

        bool IsBaseTypeCompressed { get; }

        bool IsPacked { get; set; }

        IList<IBasicFieldDefinition> BasicFields { get; }

        IList<ICompositeFieldDefinition> CompositeFields { get; }

        IDictionary<string, ICompositeFieldDefinition> CompositeFieldsInheritedLookup { get; }

        IList<IAllocatedSpaceFieldDefinition> AllocatedSpaceFields { get; }

        IList<ICommonFieldDefinition> AllFields { get; }

        IList<ICommonFieldDefinition> AllFieldsInherited { get; }

        IDictionary<string, ICommonFieldDefinition> AllFieldsInheritedLookup { get; }
    }
}