using System.Xml.Linq;
using rage.ToolLib.Logging;

namespace rage.Transform
{
    public interface ITransformer
    {
        bool Init(ILog log, params string[] args);

        TransformResult Transform(ILog log, XElement element);

        void Shutdown();
    }
}