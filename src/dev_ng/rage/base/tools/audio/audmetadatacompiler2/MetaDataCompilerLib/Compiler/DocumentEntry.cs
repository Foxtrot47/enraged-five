using System.Xml.Linq;

namespace rage.Compiler
{
    public class DocumentEntry
    {
        public string DocumentPath { get; set; }
        public XDocument Document { get; set; }
        public string TransformedXmlPath { get; set; }
    }
}