using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using rage.ToolLib.Writer;
using rage.Types;

namespace rage.Fields
{
    using rage.Compiler;
    using rage.ToolLib.Logging;

    public class FieldProcessor : IFieldProcessor
    {
        private readonly ILog log;
        private readonly IFieldContainer m_fieldContainer;

        public FieldProcessor(ILog log, IFieldContainer fieldContainer)
        {
            this.log = log;

            if (fieldContainer == null)
            {
                throw new ArgumentNullException("fieldContainer");
            }
            m_fieldContainer = fieldContainer;
        }

        public bool Process(XElement element, IWriter output, CompressionBitField compression = null)
        {
            if (m_fieldContainer.IsType)
            {
                return (m_fieldContainer as TypeDefinition).GetInheritanceHierarchy().All(typeDef => ProcessTypeFields(output, element, typeDef, compression));
            }
            return ProcessCompositeFields(output, element, m_fieldContainer);
        }

        public static bool IsDefaultValue(XElement fieldElement, ICommonFieldDefinition fieldDef)
        {
            var compositeFieldDef = fieldDef as ICompositeFieldDefinition;
            if (compositeFieldDef != null)
            {
                if (fieldElement == null || compositeFieldDef.MaxOccurs > 1)
                {
                    return false;
                }
                // Check default values of each field in composite field against its definition
                foreach (ICommonFieldDefinition childFieldDef in compositeFieldDef.AllFields)
                {
                    // Safe to grab first element here due to MaxOccurs > 1 check above
                    XElement childFieldElement = fieldElement.Elements().First().Element(childFieldDef.Name);
                    var childBasicFieldDef = childFieldDef as IBasicFieldDefinition;
                    if (null == childBasicFieldDef || 
                        (null != childFieldElement && childFieldElement.Value != childBasicFieldDef.Default))
                    {
                        return false;
                    }

                }
                // All field values are using default value at this point
                return true;
            }

            var allocatedSpaceFieldDef = fieldDef as IAllocatedSpaceFieldDefinition;
            if (allocatedSpaceFieldDef != null)
            {
                return false;
            }

            if (fieldElement == null ||
                string.IsNullOrEmpty(fieldElement.Value))
            {
                return true;
            }

            var basicFieldDef = fieldDef as IBasicFieldDefinition;
            if (basicFieldDef != null &&
                string.Compare(fieldElement.Value, basicFieldDef.Default, true) == 0)
            {
                return true;
            }
            return false;
        }

        private bool ProcessCompositeFields(IWriter output, XContainer element, IFieldContainer fieldContainer)
        {
            foreach (var fieldDef in fieldContainer.AllFields)
            {
                if (fieldDef.Ignore)
                {
                    continue;
                }

                var fieldElement = element != null ? element.Elements(fieldDef.Name).FirstOrDefault() : null;
                if (!fieldDef.Serializer.Serialize(log, output, fieldElement, fieldDef))
                {
                    return false;
                }
            }
            return true;
        }
		
		private bool ProcessTypeFields(IWriter output, XContainer element, ITypeDefinition typeDefinition, CompressionBitField compression = null)
        {
            foreach (ICommonFieldDefinition fieldDef in typeDefinition.AllFields)
            {
                if (fieldDef.Ignore || fieldDef.Type.TypeHandle.Equals(typeof(TriState).TypeHandle))
                {
                    continue;
                }

                var wasSeralized = false;
                var fieldElement = element != null ? element.Elements(fieldDef.Name).FirstOrDefault() : null;
                if (!IsDefaultValue(fieldElement, fieldDef) || !typeDefinition.IsCompressed ||
                    typeDefinition.ForceSerialization)
                {
                    if (!fieldDef.Serializer.Serialize(log, output, fieldElement, fieldDef))
                    {
                        return false;
                    }

                    wasSeralized = true;
                }

                if (typeDefinition.IsCompressed)
                {
                    compression.SetNextBit(wasSeralized);
                }
            }
            
            return true;
        }
    }
}
