using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using rage.Fields;
using rage.ToolLib;
using rage.Types;

namespace rage.CodeGeneration
{
    public class HeaderTypeDefinitionsWriter
    {
        private readonly IEnumerable<ITypeDefinition> m_typeDefinitions;

        private readonly IndentedTextWriter m_writer;

        public HeaderTypeDefinitionsWriter(IndentedTextWriter writer,
                                           IEnumerable<ITypeDefinition> typeDefinitions)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (typeDefinitions == null)
            {
                throw new ArgumentNullException("typeDefinitions");
            }
            m_writer = writer;
            m_typeDefinitions = typeDefinitions;
        }

        public void Write()
        {
            foreach (var typeDef in m_typeDefinitions)
            {
                new BlockCommentWriter(m_writer, typeDef.Name);
                new TriStateFlagEnumWriter(m_writer, typeDef);
                m_writer.WriteLine();

                using (
                    new BlockWriter(m_writer,
                                    "struct",
                                    typeDef.InheritsFrom != null && typeDef.IsCompressed == false &&
                                    typeDef.IsAncestorCompressed == false
                                        ? string.Concat(typeDef.Name, " : ", typeDef.InheritsFrom.Name)
                                        : typeDef.Name,
                                    typeDef.IsPacked ? "SPU_ONLY(__attribute__((packed)))" : null,
                                    true))
                {
                    const string INVALID_ID = "0xFFFFFFFF";
                    m_writer.WriteLine(string.Concat("static const rage::u32 TYPE_ID = ", typeDef.Id, ";"));
                    m_writer.WriteLine(string.Concat("static const rage::u32 BASE_TYPE_ID = ",
                                                     typeDef.InheritsFrom != null
                                                         ? string.Concat(typeDef.InheritsFrom.Name, "::TYPE_ID")
                                                         : INVALID_ID,
                                                     ";"));

                    m_writer.WriteLine();
                    WriteDefaultConstructor(typeDef);
                    m_writer.WriteLine();

                    m_writer.WriteLine("// PURPOSE - Returns a pointer to the field whose name is specified by the hash");
                    m_writer.WriteLine("void* GetFieldPtr(const rage::u32 fieldNameHash);");
                    m_writer.WriteLine();

                    // Class ID & name table offset
                    if (typeDef.InheritsFrom == null)
                    {
                        this.WriteClassIdNameTableOffset(typeDef);
                    }

                    // Compression (with additional compression enabled)
                    if (typeDef.AdditionalCompression && typeDef.IsCompressed)
                    {
                        m_writer.WriteLine("rage::u32 Compression;");
                    }

                    // Flags
                    if (typeDef.InheritsFrom == null && typeDef.RequiresFlags)
                    {
                        m_writer.WriteLine("rage::u32 Flags;");
                    }

                    // ParentOverride
                    if (typeDef.InheritsFrom == null)
                    {
                        m_writer.WriteLine();
                        WriteParentOverrideStruct(typeDef);
                    }

                    // Compression (without additional compression enabled)
                    if (!typeDef.AdditionalCompression && typeDef.IsCompressed)
                    {
                        m_writer.WriteLine("rage::u32 Compression;");
                    }

                    // All other fields
                    foreach (var fieldDef in typeDef.AllFields)
                    {
                        if (!fieldDef.Type.TypeHandle.Equals(typeof (TriState).TypeHandle) &&
                            !fieldDef.Ignore)
                        {
                            fieldDef.Serializer.WriteStructureEntry(m_writer);
                        }
                    }
                }
                m_writer.WriteLine();
            }
        }

        private void WriteParentOverrideStruct(IFieldContainer fieldContainer)
        {
            var overriddenFields = from fieldDef in fieldContainer.AllFields
                                   where
                                       !fieldDef.Ignore && fieldDef.AllowOverrideControl
                                       && !fieldDef.Type.TypeHandle.Equals(typeof(TriState).TypeHandle)
                                   select fieldDef;

            var commonFieldDefinitions = overriddenFields as IList<ICommonFieldDefinition> ?? overriddenFields.ToList();

            var count = commonFieldDefinitions.Count();

            // only write this if we need to
            if (count <= 0)
            {
                return;
            }

            using (new BlockWriter(this.m_writer, "struct", "tParentOverrides", "ParentOverrides", true))
            {
                using (new BlockWriter(this.m_writer, "union", null, true))
                {
                    this.m_writer.WriteLine("rage::u16 Value;");

                    using (new BlockWriter(this.m_writer, "struct", null, "BitFields", true))
                    {
                        foreach (var overriddenField in commonFieldDefinitions)
                        {
                            this.m_writer.WriteLine(string.Concat("bool ", overriddenField.Name, "OverridesParent:1;"));
                        }

                        this.m_writer.WriteLine(
                            string.Concat("bool padding:", 8 - (count % 8), "; // padding to next byte boundary"));
                    }
                }
            }

            this.m_writer.WriteLine();
        }

        private void WriteClassIdNameTableOffset(ITypeDefinition typeDef)
        {
            if (typeDef.RequiresHeader)
            {
                if (typeDef.Loader.DevOnlyNameTable)
                {
                    m_writer.WriteLine("#if __BANK");
                    m_writer.WriteLine("rage::u32 ClassId : 8;");
                    m_writer.WriteLine("rage::u32 NameTableOffset : 24;");
                    m_writer.WriteLine("#else");
                    m_writer.WriteLine("rage::u8 ClassId;");
                    m_writer.WriteLine("#endif // __BANK");
                }
                else
                {
                    m_writer.WriteLine("rage::u32 ClassId : 8;");
                    m_writer.WriteLine("rage::u32 NameTableOffset : 24;");
                }
            }
        }

        private void WriteDefaultConstructor(ITypeDefinition typeDef)
        {
            if (!this.RequiresConstructor(typeDef))
            {
                return;
            }

            this.m_writer.WriteLine(string.Concat(typeDef.Name, "() :"));
            this.m_writer.Indent += 1;

            var writeComma = false;
            if (typeDef.InheritsFrom == null)
            {
                if (typeDef.RequiresHeader)
                {
                    this.m_writer.WriteLine("ClassId(0xFF),");

                    if (typeDef.Loader.DevOnlyNameTable)
                    {
                        this.m_writer.Write("BANK_ONLY(");
                    }

                    this.m_writer.Write("NameTableOffset(0XFFFFFF)");

                    if (typeDef.Loader.DevOnlyNameTable)
                    {
                        if (typeDef.RequiresFlags || typeDef.IsCompressed)
                        {
                            this.m_writer.Write(",");
                        }

                        this.m_writer.WriteLine(")");
                    }
                    else
                    {
                        writeComma = true;
                    }
                }

                // Compression comes before flags when additional compression is set
                if (typeDef.AdditionalCompression && typeDef.IsCompressed)
                {
                    if (writeComma)
                    {
                        this.m_writer.WriteLine(",");
                    }

                    this.m_writer.Write("Compression(0xFFFFFFFF)");
                    writeComma = true;
                }

                if (typeDef.RequiresHeader && typeDef.RequiresFlags)
                {
                    if (writeComma)
                    {
                        this.m_writer.WriteLine(",");
                    }

                    this.m_writer.Write("Flags(0xAAAAAAAA)");

                    writeComma = true;
                }

                // Compression comes after flags when additional compression is not set
                if (!typeDef.AdditionalCompression && typeDef.IsCompressed)
                {
                    if (writeComma)
                    {
                        this.m_writer.WriteLine(",");
                    }

                    this.m_writer.Write("Compression(0xFFFFFFFF)");
                    writeComma = true;
                }
            }

            var pendingComment = string.Empty;
            foreach (var fieldDef in typeDef.AllFields)
            {
                if (!FieldIsSuitableForInitializerList(fieldDef))
                {
                    continue;
                }

                if (writeComma)
                {
                    if (string.IsNullOrEmpty(pendingComment))
                    {
                        this.m_writer.WriteLine(",");
                    }
                    else
                    {
                        this.m_writer.WriteLine(string.Concat(", // ", pendingComment));
                        pendingComment = null;
                    }
                }
                else
                {
                    writeComma = true;
                }

                this.m_writer.Write(fieldDef.Name);

                var basicFieldDef = fieldDef as IBasicFieldDefinition;
                if (basicFieldDef != null)
                {
                    if (basicFieldDef.Unit == Units.ObjectRef &&
                        basicFieldDef.Default == "0")
                    {
                        pendingComment = fieldDef.Serializer.WriteConstuctorInitializerListEntry(this.m_writer, "~0");
                    }
                    else
                    {
                        pendingComment = fieldDef.Serializer.WriteConstuctorInitializerListEntry(
                            this.m_writer, basicFieldDef.Default);
                    }
                    continue;
                }

                var compositeFieldDef = fieldDef as ICompositeFieldDefinition;
                if (compositeFieldDef != null)
                {
                    pendingComment = compositeFieldDef.Serializer.WriteConstuctorInitializerListEntry(
                        this.m_writer, null);
                }
            }

            this.m_writer.Indent -= 1;
            this.m_writer.WriteLine(string.Empty);
            this.m_writer.WriteLine("{}");
        }

        private bool RequiresConstructor(ITypeDefinition typeDef)
        {
            if (typeDef.InheritsFrom != null && !typeDef.AllFields.Any(FieldIsSuitableForInitializerList))
            {
                return false;
            }

            if (typeDef.InheritsFrom == null && 
                (typeDef.RequiresHeader || typeDef.IsCompressed || (typeDef.RequiresHeader && typeDef.RequiresFlags)))
            {
                return true;
            }
            
            return typeDef.AllFields.Any(FieldIsSuitableForInitializerList);
        }

        private static bool FieldIsSuitableForInitializerList(ICommonFieldDefinition fieldDef)
        {
            var fieldType = fieldDef.Type.TypeHandle;
            if (fieldDef is IAllocatedSpaceFieldDefinition || fieldDef is ICompositeFieldDefinition ||
                fieldType.Equals(typeof (TriState).TypeHandle) || fieldType.Equals(typeof (string).TypeHandle) ||
                fieldDef.Ignore)
            {
                return false;
            }

            if (fieldType.Equals(typeof (Hash).TypeHandle))
            {
                return true;
            }

            var basicFieldDef = fieldDef as IBasicFieldDefinition;
            if (basicFieldDef.Default == null)
            {
                return false;
            }

            if (basicFieldDef.Unit ==
                Units.ObjectRef)
            {
                return false;
            }
            return true;
        }
    }
}