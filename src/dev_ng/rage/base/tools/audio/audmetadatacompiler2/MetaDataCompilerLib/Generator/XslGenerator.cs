using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using rage.ToolLib.Logging;

namespace rage.Generator
{
    using rage.Types;

    public class XslGenerator : IGenerator
    {
        private const string MISSING_ARGUMENT = "XslGenerator:Init was not given a .xsl file to load";
        private const string FORMAT_FAILED_TO_LOAD_XML_TRANSFORM = "Failed to load .xsl file: {0}";
        private const string FORMAT_LOADED_XML_TRANSFORM = "Loaded .xsl file: {0}";
        private const string FORMAT_TRANSFORM_FAILED = "Failed to tranform input using .xsl file: {0}";

        private static readonly IDictionary<string, XslCompiledTransform> ms_transforms;
        private XslCompiledTransform m_transform;
        private string m_xslFile;

        static XslGenerator()
        {
            ms_transforms = new Dictionary<string, XslCompiledTransform>();
        }

        #region IGenerator Members

        public bool Init(ILog log, string workingPath, audProjectSettings projectSettings, ITypeDefinitionsManager typeDefManager, params string[] args)
        {
            if (args.Length >= 1)
            {
                var xslFile = string.Concat(workingPath, args[0]).ToLower();
                try
                {
                    lock (ms_transforms)
                    {
                        if (!ms_transforms.TryGetValue(xslFile, out m_transform))
                        {
                            m_transform = new XslCompiledTransform();
                            m_transform.Load(xslFile);
                            ms_transforms.Add(xslFile, m_transform);
                            log.Information(FORMAT_LOADED_XML_TRANSFORM, xslFile);
                        }
                        m_xslFile = xslFile;
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    log.Error(FORMAT_FAILED_TO_LOAD_XML_TRANSFORM, xslFile);
                    log.Exception(ex);
                }
            }
            else
            {
                log.Error(MISSING_ARGUMENT);
            }
            return false;
        }

        public XDocument Generate(ILog log, XDocument inputDoc)
        {
            using (var inputStream = new MemoryStream())
            {
                using (var resultsStream = new MemoryStream())
                {
                    try
                    {
                        return XDocument.Load(Transform(resultsStream, CreateInput(inputDoc, inputStream)));
                    }
                    catch (XsltException ex)
                    {
                        log.Error(FORMAT_TRANSFORM_FAILED, m_xslFile);
                        log.Exception(ex);
                    }
                    catch (Exception ex)
                    {
                        log.Exception(ex);
                    }
                }
            }
            return null;
        }

        public void Shutdown()
        {
            m_transform = null;
            m_xslFile = null;
        }

        #endregion

        private XmlReader Transform(Stream resultsStream, XmlReader input)
        {
            var results = XmlWriter.Create(resultsStream);
            m_transform.Transform(input, results);
            results.Close();
            resultsStream.Seek(0, SeekOrigin.Begin);
            return XmlReader.Create(resultsStream);
        }

        private static XmlReader CreateInput(XDocument inputDoc, Stream inputStream)
        {
            var writer = XmlWriter.Create(inputStream);
            inputDoc.Save(writer);
            writer.Close();
            inputStream.Seek(0, SeekOrigin.Begin);
            return XmlReader.Create(inputStream);
        }
    }
}