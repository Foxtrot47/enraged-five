namespace rage.Compiler
{
    using rage.ToolLib;

    public class CompiledObject
    {
        private uint nameHash;

        public string Name { get; set; }

        public uint NameHash
        {
            get
            {
                if (this.nameHash == 0)
                {
                    var hash = new Hash { Value = this.Name };
                    this.nameHash = hash.Key;
                }

                return this.nameHash;
            }
        }

        public uint Offset { get; set; }

        public uint Size { get; set; }
    }
}