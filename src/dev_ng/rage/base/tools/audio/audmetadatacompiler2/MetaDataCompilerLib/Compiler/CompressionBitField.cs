﻿// -----------------------------------------------------------------------
// <copyright file="CompressionBitField.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace rage.Compiler
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using rage.ToolLib;
    using rage.ToolLib.Writer;

    /// <summary>
    /// Compression bit field.
    /// </summary>
    public class CompressionBitField : BitField
    {
        /// <summary>
        /// The start position.
        /// </summary>
        private uint? startPosition;

        /// <summary>
        /// Initializes a new instance of the <see cref="CompressionBitField"/> class.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        /// <param name="numBits">
        /// The number of bits.
        /// </param>
        /// <param name="isBigEndian">
        /// The is big endian flag.
        /// </param>
        public CompressionBitField(IWriter writer, int numBits, bool isBigEndian) :
            base(numBits)
        {
            this.Writer = writer;
            this.IsBigEndian = isBigEndian;
        }

        /// <summary>
        /// Gets the writer.
        /// </summary>
        public IWriter Writer { get; private set; }

        /// <summary>
        /// Gets a value indicating whether is big endian.
        /// </summary>
        public bool IsBigEndian { get; private set; }

        /// <summary>
        /// Gets the current bit position.
        /// </summary>
        public int CurrentBitPosition { get; private set; }

        /// <summary>
        /// Initialize the compression bit field by writing placeholder
        /// and setting position pointer.
        /// </summary>
        public void Init()
        {
            this.startPosition = this.Writer.Tell();

            // Output placeholder byte(s)
            foreach (var b in this.Field)
            {
                this.Writer.Write(b);
            }
        }

        /// <summary>
        /// Set the next bit.
        /// </summary>
        /// <param name="bitValue">
        /// The bit value.
        /// </param>
        public void SetNextBit(bool bitValue)
        {
            if (null == this.startPosition)
            {
                throw new Exception("CompressionBitField has not been initialized.");
            }

            this.Set(this.CurrentBitPosition++, bitValue);
        }

        /// <summary>
        /// Write the output.
        /// </summary>
        public void WriteOutput()
        {
            if (null == this.startPosition)
            {
                throw new Exception("CompressionBitField has not been initialized.");
            }

            var bytes = new List<byte>(this.Field);
            if (this.IsBigEndian)
            {
                bytes.Reverse();
            }

            this.Writer.Seek((uint)this.startPosition, SeekOrigin.Begin);
            foreach (var b in bytes)
            {
                this.Writer.Write(b);
            }
            
            this.Writer.Seek(0, SeekOrigin.End);
        }
    }
}
