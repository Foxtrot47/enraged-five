﻿using System.CodeDom.Compiler;
using System.IO;
using System.Xml.Linq;
using rage.Fields;
using rage.Serialization;
using rage.ToolLib;
using rage.ToolLib.Writer;

namespace rage.Serializers
{
    using rage.ToolLib.Logging;

    [CanSerialize(typeof (PartialHash))]
    public class PartialHashSerializer : ISerializer
    {
        #region ISerializer Members

        public uint Alignment
        {
            get { return 4; }
        }

        public int Compare(XElement x, XElement y)
        {
            var partialHashX = new PartialHash { Value = x.Value };
            var partialHashY = new PartialHash { Value = y.Value };
            return partialHashX.PartialKey.CompareTo(partialHashY.PartialKey);
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            var partialHash = new PartialHash { Value = element.Value };
            output.Write(partialHash.PartialKey);
            return true;
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            var partialHash = new PartialHash { Value = value };
            writer.Write(Utility.EncloseInBrackets(string.Concat(partialHash.PartialKey, "U")));
            return partialHash.PartialKey == 0 ? null : value;
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            writer.Write("rage::u32");
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            // do nothing
        }

        #endregion
    }
}
