using System.CodeDom.Compiler;
using System.IO;
using System.Xml.Linq;
using rage.Fields;
using rage.Serialization;
using rage.ToolLib.Writer;

namespace rage.Serializers
{
    using rage.ToolLib.Logging;

    [CanSerialize(typeof (float))]
    public class FloatSerializer : ISerializer
    {
        #region ISerializer Members

        public uint Alignment
        {
            get { return 4; }
        }

        public int Compare(XElement x, XElement y)
        {
            float valueX, valueY;
            if (float.TryParse(x.Value, out valueX) && float.TryParse(y.Value, out valueY))
            {
                if (valueX > valueY) return 1;
                if (valueX < valueY) return -1;
            }
            return 0;
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            float value;
            if (float.TryParse(element.Value, out value))
            {
                output.Write(value);
                return true;
            }
            return false;
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            writer.Write(Utility.EncloseInBrackets(string.Concat(value, value.Contains(".") ? "f" : ".0f")));
            return null;
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            writer.Write("float");
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            // do nothing
        }

        #endregion
    }
}