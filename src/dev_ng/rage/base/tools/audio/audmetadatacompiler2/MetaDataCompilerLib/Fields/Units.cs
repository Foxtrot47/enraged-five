namespace rage.Fields
{
    public enum Units
    {
        Ms = 0,
        Seconds,
        Cents,
        Mb,
        Decibels,
        FixedPoint, // 0.01units
        Degrees, // degrees
        Percent,
        Percentage = Percent, // percentage 0-100
        WaveRef, // wave reference
        ObjectRef, // object reference
        StringTableIndex, // 32 bit index into string table
        CategoryRef,
        RolloffCurveRef,
        Variable,
        Vector3,
        Vector4,
        CurveRef,
        Unspecified, // no units attrib specified
        Unknown // unrecognised units attrib
    }
}