using System;
using System.IO;

namespace rage.CodeGeneration
{
    public class BlockCommentWriter
    {
        private const string COMMENT = "// ";

        public BlockCommentWriter(TextWriter writer, params string[] lines)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (lines == null ||
                lines.Length == 0)
            {
                throw new ArgumentNullException("lines");
            }

            writer.WriteLine(COMMENT);
            foreach (var line in lines)
            {
                if (line == Environment.NewLine)
                {
                    writer.WriteLine(COMMENT);
                }
                else
                {
                    writer.WriteLine(string.Concat(COMMENT, line));
                }
            }
            writer.WriteLine(COMMENT);
        }
    }
}