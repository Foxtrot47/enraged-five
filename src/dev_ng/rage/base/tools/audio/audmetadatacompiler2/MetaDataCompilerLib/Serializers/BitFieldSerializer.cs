using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Xml.Linq;
using rage.Fields;
using rage.Serialization;
using rage.ToolLib;
using rage.ToolLib.Writer;

namespace rage.Serializers
{
    using rage.ToolLib.Logging;

    [CanSerialize(typeof (BitField))]
    public class BitFieldSerializer : ISerializer
    {
        #region ISerializer Members

        public uint Alignment
        {
            get { return 0; }
        }

        public int Compare(XElement x, XElement y)
        {
            throw new NotSupportedException("Comparison of Composite Field type not supported.");
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            // This object is only used so that we have a serializer for bitfields, they are a special case so 
            // the actual serialization occurs in the composite field serializer as we cannot write 1 bit at a time
            return true;
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            // this should never be called
            throw new NotImplementedException();
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            // this should never be called
            throw new NotImplementedException();
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            // this should never be called
            throw new NotImplementedException();
        }

        #endregion
    }
}