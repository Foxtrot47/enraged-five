using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Xml.Linq;
using rage.Fields;
using rage.Serialization;
using rage.ToolLib.Writer;

namespace rage.Serializers
{
    using rage.ToolLib.Logging;

    [CanSerialize(typeof (TriState))]
    public class TriStateSerializer : ISerializer
    {
        #region ISerializer Members

        public uint Alignment
        {
            get { return 1; }
        }

        public int Compare(XElement x, XElement y)
        {
            throw new NotSupportedException("Comparison of Composite Field type not supported.");
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            // Tristates are packed and serialized by the compiler as a special case, this object is only used so 
            // Tristate fields have an ISerializer.
            return true;
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            // this should never be called
            throw new NotImplementedException();
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            // this should never be called
            throw new NotImplementedException();
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            // this should never be called
            throw new NotImplementedException();
        }

        #endregion
    }
}