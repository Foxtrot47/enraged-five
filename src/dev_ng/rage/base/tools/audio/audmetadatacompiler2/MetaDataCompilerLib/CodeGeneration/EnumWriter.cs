using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using rage.Enums;

namespace rage.CodeGeneration
{
    public class EnumWriter
    {
        private readonly int m_initialValue;

        private readonly bool m_isBitSet;

        private readonly string m_name;

        private readonly bool m_outputPrototypes;

        private readonly IndentedTextWriter m_writer;

        public EnumWriter(IndentedTextWriter writer, IEnumDefinition enumDefinition)
            : this(
                writer,
                enumDefinition.Name,
                enumDefinition.Values.Values,
                enumDefinition.InitialValue,
                enumDefinition.BitsetStyle,
                true)
        {
        }

        public EnumWriter(IndentedTextWriter writer, string name, IEnumerable<IEnumValue> members)
            : this(writer, name, members, 0, false, false)
        {
        }

        private EnumWriter(IndentedTextWriter writer,
                           string name,
                           IEnumerable<IEnumValue> members,
                           int initialValue,
                           bool isBitSet,
                           bool outputPrototypes)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
            if (members == null)
            {
                throw new ArgumentNullException("members");
            }

            m_writer = writer;
            m_name = name;
            m_initialValue = initialValue;
            m_isBitSet = isBitSet;
            m_outputPrototypes = outputPrototypes;

            Write(members);
        }

        private void Write(IEnumerable<IEnumValue> members)
        {
            if (members.Count() > 0)
            {
                using (new BlockWriter(m_writer, "enum", m_name, null, true))
                {
                    WriteEnumMembers(members);
                }

                if (m_outputPrototypes)
                {
                    m_writer.WriteLine(string.Concat(Utility.FormatEnumToStringParseMethodDeclaration(m_name), ";"));
                    m_writer.WriteLine(string.Concat(Utility.FormatStringToEnumParseMethodDeclaration(m_name), ";"));
                }
            }
        }

        private void WriteEnumMembers(IEnumerable<IEnumValue> members)
        {
            if (m_isBitSet)
            {
                var i = 0;
                foreach (var member in members)
                {
                    m_writer.WriteLine(i == 0
                                           ? string.Concat(member.Name, " = 0,")
                                           : string.Concat(member.Name, " = (1 << ", i, "),"));
                    i += 1;
                }
            }
            else
            {
                foreach (var member in members)
                {
                    m_writer.WriteLine(member.Value == m_initialValue
                                           ? string.Concat(member.Name, " = ", m_initialValue, ",")
                                           : string.Concat(member.Name, ","));
                }

                if (m_outputPrototypes)
                {
                    var upperName = m_name.ToUpper();
                    m_writer.WriteLine(string.Concat("NUM_", upperName, ","));
                    m_writer.WriteLine(string.Concat(upperName, "_MAX = ", "NUM_", upperName, ","));
                }
            }
        }
    }
}