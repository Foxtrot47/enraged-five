using System;
using System.Collections.Generic;
using System.Linq;
using rage.Compiler;
using rage.Enums;
using rage.Reflection;
using rage.ToolLib.Logging;

namespace rage.Types
{
    public class TypeDefinitionsManager : ITypeDefinitionsManager
    {
        private const string FAILED = "Failed to load successfully.";

        private const string SUCCESS = "Loaded successfully.";

        private readonly ICompiledObjectLookup m_compiledObjectLookup;

        private readonly ILog m_log;

        private readonly IReflector m_reflector;

        private readonly IStringTable m_stringTable;

        private readonly List<ITypeDefinitionsLoader> m_typeDefinitionsLoaders;

        private readonly bool m_skipTransform;

        private readonly bool m_skipValidation;

        private byte m_id;

        private IEnumerable<audObjectDefinition> m_objectDefinitions;

        private string m_workingDirectory;

        public TypeDefinitionsManager(ILog log,
                                      IReflector reflector,
                                      ICompiledObjectLookup compiledObjectLookup,
                                      IStringTable stringTable,
                                      bool skipTransform = false,
                                      bool skipValidation = false)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (reflector == null)
            {
                throw new ArgumentNullException("reflector");
            }
            if (compiledObjectLookup == null)
            {
                throw new ArgumentNullException("compiledObjectLookup");
            }
            if (stringTable == null)
            {
                throw new ArgumentNullException("stringTable");
            }
            m_log = log;
            m_reflector = reflector;
            m_compiledObjectLookup = compiledObjectLookup;
            m_stringTable = stringTable;
            m_skipTransform = skipTransform;
            m_skipValidation = skipValidation;
            m_typeDefinitionsLoaders = new List<ITypeDefinitionsLoader>();
        }

        #region ITypeDefinitionsManager Members

        /*
         * In case there are multiple TypeDefinitionsLoader loaded we use the configuration of the first loader.
         * Currently there is only one loader used.
         */

        public uint Version
        {
            get
            {
                if (m_typeDefinitionsLoaders.Count > 0)
                {
                    return m_typeDefinitionsLoaders[0].Version;
                }
                return 0;
            }
        }

        public uint Mode
        {
            get
            {
                if (m_typeDefinitionsLoaders.Count > 0)
                {
                    return m_typeDefinitionsLoaders[0].Mode;
                }

                // Use original mode as default
                return 0;
            }
        }

        public IDictionary<string, KeysPerPlatform> EncryptionKeys
        {
            get {
                if(m_typeDefinitionsLoaders.Count > 0) 
                {
                    return m_typeDefinitionsLoaders[0].EncryptionKeys;
                }
                return new Dictionary<string, KeysPerPlatform>();
            }
        }

        public IEnumerable<string> IncludePaths
        {
            get { return m_typeDefinitionsLoaders.SelectMany(typeDefLoader => typeDefLoader.IncludePaths); }
        }

        public IEnumerable<ITypeDefinition> ConcreteTypeDefinitions
        {
            get { return from typeDef in AllTypeDefinitions where !typeDef.IsAbstract select typeDef; }
        }

        public IEnumerable<ITypeDefinition> AllTypeDefinitions
        {
            get { return m_typeDefinitionsLoaders.SelectMany(x => x.TypeDefinitions.Values); }
        }

        public IEnumerable<TypeDefData> TypeDefinitionsData
        {
            get { return from typeDefLoader in m_typeDefinitionsLoaders select new TypeDefData(typeDefLoader); }
        }

        public IEnumerable<IEnumDefinition> EnumDefinitions
        {
            get
            {
                return from typeDefLoader in m_typeDefinitionsLoaders
                       from enumDefEntry in typeDefLoader.EnumDefinitions
                       select enumDefEntry.Value;
            }
        }

        public IEnumerable<string> LoadingPaths
        {
            get
            {
                if (m_objectDefinitions != null &&
                    !string.IsNullOrEmpty(m_workingDirectory))
                {
                    foreach (var objectDef in m_objectDefinitions)
                    {
                        yield return string.Concat(m_workingDirectory, objectDef.DefinitionsFile);
                    }
                }
            }
        }

        public bool Load(string workingDirectory, IEnumerable<audObjectDefinition> objectDefinitions)
        {
            if (objectDefinitions == null)
            {
                throw new ArgumentNullException("objectDefinitions");
            }
            if (string.IsNullOrEmpty(workingDirectory))
            {
                throw new ArgumentNullException("workingDirectory");
            }

            m_objectDefinitions = null;
            m_workingDirectory = null;

            if (!LoadTypeDefinitions(workingDirectory, objectDefinitions))
            {
                return false;
            }

            m_objectDefinitions = objectDefinitions;
            m_workingDirectory = workingDirectory;

            m_log.WriteFormatted(string.Empty);
            return true;
        }

        public bool Reload()
        {
            if (m_objectDefinitions == null ||
                string.IsNullOrEmpty(m_workingDirectory))
            {
                throw new InvalidOperationException(
                    "Attempting to Reload() type definitions without calling Load() at least once");
            }
            return LoadTypeDefinitions(m_workingDirectory, m_objectDefinitions);
        }

        public ITypeDefinition FindTypeDefinition(string name)
        {
            ITypeDefinition found = null;
            foreach (var typeDefinitionsLoader in m_typeDefinitionsLoaders)
            {
                if (typeDefinitionsLoader.TypeDefinitions.TryGetValue(name, out found))
                {
                    break;
                }
            }
            return found;
        }

        public IEnumDefinition FindEnumDefinition(string name)
        {
            IEnumDefinition found = null;
            foreach (var typeDefinitionsLoader in m_typeDefinitionsLoaders)
            {
                if (typeDefinitionsLoader.EnumDefinitions.TryGetValue(name, out found))
                {
                    break;
                }
            }
            return found;
        }

        #endregion

        private bool LoadTypeDefinitions(string workingDirectory, IEnumerable<audObjectDefinition> objectDefinitions)
        {
            m_typeDefinitionsLoaders.Clear();
            m_id = 0;
            foreach (var objectDefinition in objectDefinitions)
            {
                var typeDefinitionsLoader = new TypeDefinitionsLoader(m_log,
                                                                      m_reflector,
                                                                      m_compiledObjectLookup,
                                                                      m_stringTable,
                                                                      objectDefinition.ClassPrefix,
                                                                      objectDefinition.ClassIncludePath,
                                                                      FindTypeDefinition,
                                                                      FindEnumDefinition,
                                                                      m_id,
                                                                      m_skipTransform,
                                                                      m_skipValidation);
                m_typeDefinitionsLoaders.Add(typeDefinitionsLoader);

                var file = string.Concat(workingDirectory, objectDefinition.DefinitionsFile);

                m_log.PushContext(ContextType.File, file);

                if (!typeDefinitionsLoader.Load(file))
                {
                    m_log.Error(FAILED);
                    m_typeDefinitionsLoaders.Remove(typeDefinitionsLoader);
                    m_log.PopContext();
                    return false;
                }
                m_id = typeDefinitionsLoader.NextId;

                m_log.Information(SUCCESS);
                m_log.PopContext();
            }
            return true;
        }
    }
}