using System.Xml.Linq;

namespace rage.Templates
{
    public interface ITemplateManager
    {
        bool Load(string templatePath);

        bool Load(XContainer templateContainer);

        bool Process(XDocument doc);

        void Clear();
    }
}