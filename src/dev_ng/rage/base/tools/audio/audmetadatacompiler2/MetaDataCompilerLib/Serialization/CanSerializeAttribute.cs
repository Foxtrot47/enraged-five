using System;

namespace rage.Serialization
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class CanSerializeAttribute : Attribute
    {
        private readonly Type m_type;

        public CanSerializeAttribute(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }
            m_type = type;
        }

        internal CanSerializeAttribute(Type type, bool isSystemSerializer) : this(type)
        {
            IsSystemSerializer = isSystemSerializer;
        }

        internal bool IsSystemSerializer { get; private set; }

        public Type GetSerializableType()
        {
            return m_type;
        }
    }
}