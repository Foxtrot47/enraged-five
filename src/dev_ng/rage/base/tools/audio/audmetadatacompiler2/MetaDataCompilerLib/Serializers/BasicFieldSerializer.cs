using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Xml.Linq;
using rage.Compiler;
using rage.Fields;
using rage.Serialization;
using rage.ToolLib;
using rage.ToolLib.Writer;

namespace rage.Serializers
{
    using rage.ToolLib.Logging;

    [CanSerialize(typeof (IBasicFieldDefinition), true)]
    public class BasicFieldSerializer : ISerializer
    {
        private const string DEFAULT = "Default";
        private readonly IBasicFieldDefinition m_basicFieldDefinition;
        private readonly ICompiledObjectLookup m_compiledObjectLookup;
        private readonly ISerializer m_serializer;
        private readonly IStringTable m_stringTable;

        public BasicFieldSerializer(IBasicFieldDefinition basicFieldDefinition,
                                    ISerializer serializer,
                                    ICompiledObjectLookup compiledObjectLookup,
                                    IStringTable stringTable)
        {
            if (basicFieldDefinition == null)
            {
                throw new ArgumentNullException("basicFieldDefinition");
            }
            if (serializer == null)
            {
                throw new ArgumentNullException("serializer");
            }
            if (compiledObjectLookup == null)
            {
                throw new ArgumentNullException("compiledObjectLookup");
            }
            if (stringTable == null)
            {
                throw new ArgumentNullException("stringTable");
            }
            m_basicFieldDefinition = basicFieldDefinition;
            m_serializer = serializer;
            m_compiledObjectLookup = compiledObjectLookup;
            m_stringTable = stringTable;
        }

        #region ISerializer Members

        public uint Alignment
        {
            get { return m_serializer.Alignment; }
        }

        public int Compare(XElement x, XElement y)
        {
            return m_serializer.Compare(x, y);
        }

        public bool Serialize(ILog log, IWriter output, XElement element, ICommonFieldDefinition fieldDefinition)
        {
            var value = GetValue(element);

            if (!this.Validate(log, element, m_basicFieldDefinition, value))
            {
                return false;
            }

            switch (m_basicFieldDefinition.Unit)
            {
                case Units.StringTableIndex:
                    {
                        var hash = new Hash {Value = value};
                        if (hash.Key != 0)
                        {
                            m_compiledObjectLookup.RecordStringRef(output.Tell());
                            output.Write(m_stringTable.Add(hash));
                        }
                        return true;
                    }
                case Units.ObjectRef:
                    {
                        // if this is not a hash
                        if (m_basicFieldDefinition.Type !=
                            typeof (Hash))
                        {
                            if (string.IsNullOrEmpty(value))
                            {
                                const uint INVALID_REF = 0xFFFFFFFF;
                                output.Write(INVALID_REF);
                            }
                            else
                            {
                                m_compiledObjectLookup.RecordObjectRef(output.Tell());
                                if (value == "0")
                                {
                                    output.Write(0U);
                                }
                                else
                                {
                                    var hash = new Hash {Value = value};
                                    output.Write(hash.Key);
                                }
                            }
                            return true;
                        }
                        break;
                    }
            }
            if (element == null)
            {
                element = new XElement(DEFAULT, value);
            }

            if (!m_basicFieldDefinition.IsPacked)
            {
                Utility.AlignToBoundary(output, m_serializer.Alignment);
            }
            return m_serializer.Serialize(log, output, element, fieldDefinition);
        }

        public string WriteConstuctorInitializerListEntry(TextWriter writer, string value)
        {
            return m_serializer.WriteConstuctorInitializerListEntry(writer, value);
        }

        public void WriteStructureEntry(TextWriter writer)
        {
            if (m_basicFieldDefinition.Type != typeof (Hash) &&
                m_basicFieldDefinition.Unit == Units.ObjectRef)
            {
                writer.Write("rage::audMetadataRef");
            }
            else
            {
                m_serializer.WriteStructureEntry(writer);
            }
            if (!m_basicFieldDefinition.Type.TypeHandle.Equals(typeof (string).TypeHandle))
            {
                writer.WriteLine(string.Concat(" ", m_basicFieldDefinition.Name, ";"));
            }
        }

        public void WriteFunctions(IndentedTextWriter writer, ICommonFieldDefinition fieldDefinition)
        {
            // do nothing
        }

        #endregion

        private string GetValue(XElement element)
        {
            return FieldProcessor.IsDefaultValue(element, m_basicFieldDefinition)
                       ? m_basicFieldDefinition.Default
                       : element.Value;
        }

        private bool Validate(ILog log, XElement element, IBasicFieldDefinition fieldDefinition, string value)
        {
            if (fieldDefinition.RequiresValue && (null == element || string.IsNullOrEmpty(value)))
            {
                log.Warning(string.Format("{0} field requires a value", fieldDefinition.Name));
            }

            return true;
        }
    }
}