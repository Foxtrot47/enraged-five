using System;
using System.CodeDom.Compiler;
using rage.Types;

namespace rage.CodeGeneration
{
    public class HelperMethodWriter
    {
        private readonly ITypeDefinitionsManager m_typeDefinitionsManager;

        private readonly IndentedTextWriter m_writer;

        public HelperMethodWriter(IndentedTextWriter writer,
                                  ITypeDefinitionsManager typeDefinitionsManager,
                                  string metaDataType)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (typeDefinitionsManager == null)
            {
                throw new ArgumentNullException("typeDefinitionsManager");
            }
            if (string.IsNullOrEmpty(metaDataType))
            {
                throw new ArgumentNullException("metaDataType");
            }
            m_writer = writer;
            m_typeDefinitionsManager = typeDefinitionsManager;

            Write(metaDataType);
        }

        private void Write(string metaDataType)
        {
            WriteGetBaseTypeIdFunction(metaDataType);
            m_writer.WriteLine();
            WriteGetIsOfTypeFunction(metaDataType);
        }

        private void WriteGetBaseTypeIdFunction(string metaDataType)
        {
            m_writer.WriteLine("// PURPOSE - Gets the type id of the parent class from the given type id");
            using (new BlockWriter(m_writer, Utility.GetBaseTypeIdDeclaration(metaDataType), null, false))
            {
                using (new BlockWriter(m_writer, "switch(classId)", null, false))
                {
                    foreach (var typeDef in m_typeDefinitionsManager.AllTypeDefinitions)
                    {
                        m_writer.WriteLine(string.Concat("case ",
                                                         typeDef.Name,
                                                         "::TYPE_ID: return ",
                                                         typeDef.Name,
                                                         "::BASE_TYPE_ID;"));
                    }
                    m_writer.WriteLine("default: return 0xFFFFFFFF;");
                }
            }
        }

        private void WriteGetIsOfTypeFunction(string metaDataType)
        {
            m_writer.WriteLine("// PURPOSE - Determines if a type inherits from another type");
            using (new BlockWriter(m_writer, Utility.GetIsOfTypeIdWithIdDeclaration(metaDataType), null, false))
            {
                using (new BlockWriter(m_writer, "if(objectTypeId == 0xFFFFFFFF)", null, false))
                {
                    m_writer.WriteLine("return false;");
                }
                using (new BlockWriter(m_writer, "else if(objectTypeId == baseTypeId)", null, false))
                {
                    m_writer.WriteLine("return true;");
                }
                using (new BlockWriter(m_writer, "else", null, false))
                {
                    m_writer.WriteLine(string.Concat("return g",
                                                     metaDataType,
                                                     "IsOfType(g",
                                                     metaDataType,
                                                     "GetBaseTypeId(objectTypeId), baseTypeId);"));
                }
            }
        }
    }
}