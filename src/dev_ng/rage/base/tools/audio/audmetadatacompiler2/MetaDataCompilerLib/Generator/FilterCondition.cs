﻿namespace rage.Generator
{
    public enum FilterCondition
    {
        Equals,
        StartsWith,
        EndsWith,
        Contains
    }
}