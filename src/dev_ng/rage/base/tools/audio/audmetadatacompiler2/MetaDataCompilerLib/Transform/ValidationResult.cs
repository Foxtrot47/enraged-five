using System.Collections.Generic;

namespace rage.Transform
{
    public class ValidationResult
    {
        public ValidationResult()
        {
            Warnings = new List<string>();
            Errors = new List<string>();
        }

        public ICollection<string> Warnings { get; private set; }

        public ICollection<string> Errors { get; private set; }

        public bool HasWarnings
        {
            get { return Warnings.Count > 0; }
        }

        public bool HasErrors
        {
            get { return Errors.Count > 0; }
        }

        public bool WasSuccessful
        {
            get { return Warnings.Count == 0 && Errors.Count == 0; }
        }
    }
}