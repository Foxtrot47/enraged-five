using System;
using System.IO;
using System.Text;
using rage.Compiler;
using rage.ToolLib;
using rage.ToolLib.CmdLine;
using rage.ToolLib.Logging;

namespace mc
{
    using rage.Enums;

    public class CmdLineArgs : ICmdLineArgs
    {
        private const string LOG_VERBOSITY = "logverbosity";
        private const string LOG_FILE = "logfile";
        private const string LOG_FORMAT = "logformat";
        private const string SOFT_ROOT = "softroot";
        private const string CHANGE_LIST_NUMBER = "cl";
        private const string NAMESPACE = "namespace";
        private const string RAGE = "rage";
        private const string RUNTIMEMODE = "runtimeMode";
        private const string MISSING_RUNTIMEMODE = "Missing \"-runtimemode\" command line argument";
        private const string PROJECT = "project";
        private const string MISSING_PROJECT = "Missing \"-project\" command line argument";
        private const string PLATFORM = "platform";
        private const string MISSING_PLATFORM = "Missing \"-platform\" command line argument";
        private const string METADATA = "metadata";
        private const string MISSING_METADATA = "Missing \"-metadata\" command line argument";
        private const string CODE_OUTPUT = "codeoutput";
        private const string TRUE = "true";
        private const string WORKING_PATH = "workingpath";
        private const string MISSING_WORKING_PATH = "Missing \"-workingpath\" command line argument";
        private const string HELP = "help";
        private const string QUESTION_MARK = "?";
        private const string COMPILE_MODE = "compileMode";
        private readonly ILog m_log;

        public CmdLineArgs(ILog log)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            m_log = log;
        }

        #region ICmdLineArgs Members

        public EnumRuntimeMode RuntimeMode { get; private set; }
        public string Project { get; private set; }
        public string Platform { get; private set; }
        public string MetaData { get; private set; }
        public bool CodeOutput { get; private set; }
        public string WorkingPath { get; private set; }
        public string Namespace { get; private set; }
        public string SoftRoot { get; private set; }
        public string ChangeListNumber { get; private set; }
        public LogVerbosity LogVerbosity { get; private set; }
        public LogOutputFormat LogFormat { get; private set; }
        public string LogFile { get; private set; }
        public CompilationMode CompileMode { get; private set; }

        public bool HelpRequested { get; private set; }

        public bool Parse(string[] args)
        {
            try
            {
                var cmdLineParser = new CmdLineParser(args);
                SetHelpRequested(cmdLineParser);
                if (HelpRequested)
                {
                    m_log.WriteFormatted(GetUsage());
                    return true;
                }

                if (!SetRuntimeMode(cmdLineParser))
                {
                    return false;
                }

                if (!SetProject(cmdLineParser))
                {
                    return false;
                }

                if (!SetPlatform(cmdLineParser))
                {
                    return false;
                }

                if (!SetMetaData(cmdLineParser))
                {
                    return false;
                }

                if (!SetWorkingPath(cmdLineParser))
                {
                    return false;
                }

                SetCodeOutput(cmdLineParser);
                SetNamespace(cmdLineParser);
                SoftRoot = cmdLineParser[SOFT_ROOT];
                ChangeListNumber = cmdLineParser[CHANGE_LIST_NUMBER];
                SetLogVerbosity(cmdLineParser);
                SetLogOutput(cmdLineParser);
                SetLogFile(cmdLineParser);
                SetCompilationMode(cmdLineParser);
            }
            catch (Exception ex)
            {
                m_log.Exception(ex);
                return false;
            }
            return true;
        }

        #endregion

        private void SetHelpRequested(CmdLineParser cmdLineParser)
        {
            if (cmdLineParser[QUESTION_MARK] != null ||
                cmdLineParser[HELP] != null)
            {
                HelpRequested = true;
            }
        }

        private void SetCompilationMode(CmdLineParser cmdLineParser)
        {
            CompileMode = CompilationMode.TransformAndCompile;
            if(cmdLineParser.Arguments.ContainsKey(COMPILE_MODE))
            {
                CompileMode = Enum<CompilationMode>.TryParse(cmdLineParser[COMPILE_MODE], true, CompilationMode.TransformAndCompile);
            }
        }

        private void SetLogFile(CmdLineParser cmdLineParser)
        {
            try
            {
                var logFile = cmdLineParser[LOG_FILE];
                if (Directory.Exists(Path.GetDirectoryName(logFile)))
                {
                    var file = File.Create(logFile);
                    file.Close();
                    LogFile = logFile;
                }
            }
            catch
            {
                LogFile = null;
            }
        }

        private void SetLogVerbosity(CmdLineParser cmdLineParser)
        {
            LogVerbosity = LogVerbosity.WarningsAndErrors;            
            if (cmdLineParser.Arguments.ContainsKey(LOG_VERBOSITY))
            {
                LogVerbosity = Enum<LogVerbosity>.Parse(cmdLineParser[LOG_VERBOSITY], true);
            }           
        }

        private void SetLogOutput(CmdLineParser cmdLineParser)
        {
            LogFormat = LogOutputFormat.Text;
            if (cmdLineParser.Arguments.ContainsKey(LOG_FORMAT))
            {
                LogFormat = (LogOutputFormat)Enum.Parse(typeof(LogOutputFormat), cmdLineParser[LOG_FORMAT], true);
            }
        }

        private void SetNamespace(CmdLineParser cmdLineParser)
        {
            Namespace = cmdLineParser[NAMESPACE];
            if (string.IsNullOrEmpty(Namespace))
            {
                Namespace = RAGE;
            }
        }

        private bool SetRuntimeMode(CmdLineParser cmdLineParser)
        {
            var runtimeMode = Enum<EnumRuntimeMode>.TryParse(cmdLineParser[RUNTIMEMODE], true);
            if (null == runtimeMode)
            {
                WriteError(MISSING_RUNTIMEMODE);
                return false;
            }

            this.RuntimeMode = (EnumRuntimeMode)runtimeMode;
            
            return true;
        }

        private bool SetProject(CmdLineParser cmdLineParser)
        {
            Project = cmdLineParser[PROJECT];
            if (string.IsNullOrEmpty(Project))
            {
                WriteError(MISSING_PROJECT);
                return false;
            }
            return true;
        }

        private bool SetPlatform(CmdLineParser cmdLineParser)
        {
            Platform = cmdLineParser[PLATFORM];
            if (string.IsNullOrEmpty(Platform))
            {
                WriteError(MISSING_PLATFORM);
                return false;
            }
            return true;
        }

        private bool SetMetaData(CmdLineParser cmdLineParser)
        {
            MetaData = cmdLineParser[METADATA];
            if (string.IsNullOrEmpty(MetaData))
            {
                WriteError(MISSING_METADATA);
                return false;
            }
            return true;
        }

        private void SetCodeOutput(CmdLineParser cmdLineParser)
        {
            try
            {
                CodeOutput = string.Compare(cmdLineParser[CODE_OUTPUT], TRUE, true) == 0;
            }
            catch
            {
                CodeOutput = false;
            }
        }

        private bool SetWorkingPath(CmdLineParser cmdLineParser)
        {
            WorkingPath = cmdLineParser[WORKING_PATH];
            if (string.IsNullOrEmpty(WorkingPath))
            {
                WriteError(MISSING_WORKING_PATH);
                return false;
            }
            return true;
        }

        private void WriteError(string msg)
        {
            m_log.Write(null);
            m_log.Error(msg);
            m_log.WriteFormatted(GetUsage());
        }

        private static string GetUsage()
        {
            var builder = new StringBuilder();
            builder.Append("Compiles metadata or outputs C++ metadata structure definitions.");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);

            builder.Append("Usage:");
            builder.Append(Environment.NewLine);
            builder.Append("MC -project <relative_path> -platform <platform_tag> -metadata <metadata_type>");
            builder.Append(Environment.NewLine);
            builder.Append("-workingPath <dir> [-codeoutput] [-namespace <namespace>] [-logfile <file>]");
            builder.Append(Environment.NewLine);
            builder.Append("[-softroot <path_segment>] [-logverbosity <verbosity_setting>]");
            builder.Append(Environment.NewLine);
            builder.Append("[-cl <change list number>]");
            builder.Append(Environment.NewLine);
            builder.Append("[-logformat <format_setting>] [-compileMode <mode_setting>] [-help or -?]");
            builder.Append(Environment.NewLine);
            builder.Append(Environment.NewLine);

            builder.Append("-project\tthe relative path to the project settings file.");
            builder.Append(Environment.NewLine);
            builder.Append("-platform\tthe platform to compile for PS3, PC or 360.");
            builder.Append(Environment.NewLine);
            builder.Append("-metadata\tthe metadata type to compile or generate code for.");
            builder.Append(Environment.NewLine);
            builder.Append("-workingPath\tthe root directory for all relative paths.");
            builder.Append(Environment.NewLine);
            builder.Append("-codeoutput\tgenerate code instead of compiling.");
            builder.Append(Environment.NewLine);
            builder.Append("-namespace\tthe namespace to use while generating code..");
            builder.Append(Environment.NewLine);
            builder.Append("-logfile\twill log to the file specified instead of the console.");
            builder.Append(Environment.NewLine);
            builder.Append("-softroot\ta root directory offset to be added while generating code.");
            builder.Append(Environment.NewLine);
            builder.Append("-cl\tThe change list number of data sync which is added to the metadata offset 0.");
            builder.Append(Environment.NewLine);
            builder.Append("-logverbosity\tcan be set to \"Everything\" to see information too.");
            builder.Append(Environment.NewLine);
            builder.Append("-logformat\tcan be set to XML for log output in this format.");
            builder.Append(Environment.NewLine);
            builder.Append("-compileMode\tset to \"TransformOnly\" to output transformed XML.");
            builder.Append(Environment.NewLine);
            builder.Append("-help or -?\tdisplay usage information.");
            builder.Append(Environment.NewLine);
            return builder.ToString();
        }
    }
}