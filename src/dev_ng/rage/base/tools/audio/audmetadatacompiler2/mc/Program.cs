﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Xml.Linq;
using rage;
using rage.CodeGeneration;
using rage.Compiler;
using rage.Fields;
using rage.Reflection;
using rage.ToolLib.Logging;
using rage.ToolLib.Writer;
using BinaryWriter = rage.ToolLib.Writer.BinaryWriter;

namespace mc
{
    using rage.Enums;

    internal class Program
    {
        private const string CONFIG_PATH = "config\\";
        private const string COMPILER_NAME = "RAGE Audio Metadata Compiler";
        private const string FORMAT_COMPILER_VERSION = "v{0}";
        private const string CODE_GENERATION_SUCCEEDED = "Code Generation Successful.";
        private const string CODE_GENERATION_FAILED = "Code Generation Failed!";
        private const string COMPILATION_SUCCEEDED = "Compilation Successful.";
        private const string COMPILATION_FAILED = "Compilation Failed!";
        private const string FORMAT_FINISHED = "\nFinished. See \"{0}\" for detailed log.";
        private const string FORMAT_COMPILING_METADATA_TO = "Compiling metadata to: {0}";
        private const string FORMAT_TRANSFORMING_METADATA_TO = "Transforming metadata to: {0}";
        private const string LOG_ROOT_ELEMENT_NAME = "MetadataCompilerOutput";
        private const string DLL_EXTENSION = ".dll";

        private static string[] ms_jitPaths;

        private static int Main(string[] args)
        {
            try
            {
                string logFile = Path.Combine(Path.GetTempPath(), "MetaDataCompilerLog.txt");
                if(File.Exists(logFile)) File.Delete(logFile);
                using(StreamWriter sw = File.AppendText(logFile)) {
                    sw.WriteLine("Metadata compiler arguments: "+String.Join(" ", args));
                }

                AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;

                using (var compilerLogWriter = new ConsoleLogWriter())
                {
                    using (
                        var compilerLog = new TextLog(compilerLogWriter, new ContextStack())
                                              {Verbosity = LogVerbosity.WarningsAndErrors})
                    {
                        ICmdLineArgs cmdLineArgs = new CmdLineArgs(compilerLog);
                        if (!cmdLineArgs.Parse(args))
                        {
                            return -1;
                        }
                        if (cmdLineArgs.HelpRequested)
                        {
                            return 0;
                        }

                        compilerLog.WriteFormatted(COMPILER_NAME);

                        var versionMsg = string.Format(FORMAT_COMPILER_VERSION,
                                                       Assembly.GetEntryAssembly().GetName().Version);
                        compilerLog.WriteFormatted(versionMsg);

                        bool loggingToFile;
                        var logWriter = CreateLogWriter(cmdLineArgs.LogFile, out loggingToFile);
                        using (logWriter)
                        {
                            var log = CreateLog(cmdLineArgs.LogFormat, logWriter);
                            using (log)
                            {
                                if (loggingToFile)
                                {
                                    log.WriteFormatted(COMPILER_NAME);
                                    log.WriteFormatted(versionMsg);
                                }

                                log.Verbosity = cmdLineArgs.LogVerbosity;

                                var projectSettings =
                                    new audProjectSettings(string.Concat(cmdLineArgs.WorkingPath, cmdLineArgs.Project));
                                projectSettings.SetCurrentPlatformByTag(cmdLineArgs.Platform);

                                try
                                {
                                    log.PushContext(ContextType.Application, projectSettings.GetProjectName());

                                    var jitPaths = projectSettings.MetadataCompilerJitPaths;
                                    if (jitPaths != null)
                                    {
                                        var count = jitPaths.Length;
                                        ms_jitPaths = new string[count];
                                        for (var i = 0; i < count; ++i)
                                        {
                                            ms_jitPaths[i] = string.Concat(cmdLineArgs.WorkingPath, jitPaths[i]);
                                        }
                                    }
                                    var reflector = new Reflector(log, ms_jitPaths);

                                    var workingDirectory =
                                        Path.GetFullPath(string.Concat(cmdLineArgs.WorkingPath,
                                                                       projectSettings.GetSoundXmlPath()));
                                    SetCurrentDirectory(log, workingDirectory);

                                    if (cmdLineArgs.CodeOutput)
                                    {
                                        GenerateCode(log, reflector, cmdLineArgs, projectSettings, workingDirectory);
                                    }
                                    else
                                    {
                                        Compile(log, workingDirectory, reflector, cmdLineArgs, projectSettings);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    log.Exception(ex);
                                    using(StreamWriter sw = File.AppendText(logFile)) {
                                        sw.WriteLine(ex.Message);
                                    }
                                }
                            }
                        }

                        if (loggingToFile)
                        {
                            compilerLog.WriteFormatted(FORMAT_FINISHED, cmdLineArgs.LogFile);
                        }
                    }
                }
            }
            finally
            {
                AppDomain.CurrentDomain.AssemblyResolve -= OnAssemblyResolve;
            }
            return 0;
        }

        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (ms_jitPaths != null)
            {
                var name = new AssemblyName(args.Name);
                foreach (var jitPath in ms_jitPaths)
                {
                    var assemblyFile = Path.Combine(jitPath, string.Concat(name.Name, DLL_EXTENSION));
                    if (File.Exists(assemblyFile))
                    {
                        return Assembly.LoadFile(assemblyFile);
                    }
                }
            }
            return null;
        }

        private static void Compile(ILog log,
                                    string workingDirectory,
                                    IReflector reflector,
                                    ICmdLineArgs cmdLineArgs,
                                    audProjectSettings projectSettings)
        {
            IEnumerable<audMetadataType> metaDataTypes = GetMetaDataTypes(log, projectSettings, cmdLineArgs.MetaData);
            if (metaDataTypes == null)
            {
                log.WriteFormatted(COMPILATION_FAILED);
                log.PopContext();
                return;
            }

            var outputPath = string.Concat(cmdLineArgs.WorkingPath, projectSettings.GetCurrentPlatform().BuildOutput, CONFIG_PATH);
            if(!Directory.Exists(outputPath)) {
                Directory.CreateDirectory(outputPath);
            }

            foreach(audMetadataType metaDataType in metaDataTypes)
            {
                log.PushContext(ContextType.MetadataType, metaDataType.TypeNotUpper);

                var objectSizeTracker = new ObjectSizeTracker();

                CompileHelper(
                    log, workingDirectory, reflector, cmdLineArgs, projectSettings, objectSizeTracker, metaDataType, cmdLineArgs.RuntimeMode);

                log.PopContext();
                WriteSummary(log, objectSizeTracker, cmdLineArgs.LogFormat);
                log.ResetLogCounts();
            }
        }

        private static void CompileHelper(
            ILog log,
            string workingDirectory,
            IReflector reflector,
            ICmdLineArgs cmdLineArgs,
            audProjectSettings projectSettings,
            ObjectSizeTracker objectSizeTracker,
            audMetadataType metadataType,
            EnumRuntimeMode runtimeMode)
        {
            var metaDataCompiler = new MetaDataCompiler(
                log,
                cmdLineArgs.WorkingPath,
                workingDirectory,
                projectSettings,
                reflector,
                objectSizeTracker,
                new StringTable(),
                new TagTable(),
                new CompiledObjectLookup(),
                cmdLineArgs.CompileMode);

            if (metaDataCompiler.LoadTypeDefinitionsAndTemplates(
                metadataType.TemplatePath, metadataType.ObjectDefinitions))
            {
                var changeListNumber = 0U;
                if (!string.IsNullOrEmpty(cmdLineArgs.ChangeListNumber))
                {
                    if (!uint.TryParse(cmdLineArgs.ChangeListNumber, out changeListNumber))
                    {
                        log.Error("Failed to parse change list number: " + cmdLineArgs.ChangeListNumber);
                    }
                }

                log.WriteFormatted(CompileMetaDataFiles(
                    log,
                    metaDataCompiler,
                    projectSettings,
                    metadataType,
                    workingDirectory,
                    cmdLineArgs.WorkingPath,
                    cmdLineArgs.CompileMode,
                    changeListNumber,
                    runtimeMode) ? COMPILATION_SUCCEEDED : COMPILATION_FAILED);
            }
            else
            {
                log.WriteFormatted(COMPILATION_FAILED);
            }
        }

        private static void WriteSummary(ILog log, IObjectSizeTracker objectSizeTracker, LogOutputFormat logFormat)
        {
            var summary = new Summary(log, objectSizeTracker, logFormat);
            summary.Write();
        }

        private static void GenerateCode(ILog log,
                                         IReflector reflector,
                                         ICmdLineArgs cmdLineArgs,
                                         audProjectSettings projectSettings,
                                         string workingDirectory)
        {
            var metaDataTypes = GetMetaDataTypes(log, projectSettings, cmdLineArgs.MetaData);
            if (metaDataTypes == null)
            {
                log.WriteFormatted(COMPILATION_FAILED);
                log.PopContext();
                return;
            }

            foreach (var metaDataType in metaDataTypes)
            {
                log.PushContext(ContextType.MetadataType, metaDataType.TypeNotUpper);

                var metaDataCodeGenerator = new MetaDataCodeGenerator(log, reflector, projectSettings);
                if (metaDataCodeGenerator.LoadTypeDefinitions(metaDataType,
                                                              new CompiledObjectLookup(),
                                                              new StringTable(),
                                                              new TagTable(),
                                                              workingDirectory))
                {
                    var path = string.Concat(cmdLineArgs.WorkingPath, cmdLineArgs.SoftRoot);
                    var nameSpace = MetaDataUtility.GetNamespace(cmdLineArgs.Namespace, metaDataType);
                    log.WriteFormatted(metaDataCodeGenerator.GenerateCode(metaDataType, path, nameSpace)
                                           ? CODE_GENERATION_SUCCEEDED
                                           : CODE_GENERATION_FAILED);
                }
                else
                {
                    log.WriteFormatted(CODE_GENERATION_FAILED);
                }

                log.PopContext();
                log.ResetLogCounts();

                log.Write(string.Empty);
                log.Write(string.Empty);
            }
        }

        private static IEnumerable<audMetadataType> GetMetaDataTypes(ILog log,
                                                                     audProjectSettings projectSettings,
                                                                     string metaData)
        {
            var metaDataTypeStrings = metaData.Split(new[] {","}, StringSplitOptions.None);
            var metaDataTypes = new audMetadataType[metaDataTypeStrings.Length];
            for (var i = 0; i < metaDataTypeStrings.Length; ++i)
            {
                var metaDataType = MetaDataUtility.GetMetaDataType(metaDataTypeStrings[i].Trim(), projectSettings);
                if (metaDataType == null)
                {
                    log.Error(MetaDataUtility.FORMAT_METADATA_NOT_FOUND, metaDataTypeStrings[i].Trim());
                    return null;
                }
                metaDataTypes[i] = metaDataType;
            }
            return metaDataTypes;
        }

        private static void SetCurrentDirectory(ILog log, string workingDirectory)
        {
            Directory.SetCurrentDirectory(workingDirectory);
            log.WriteFormatted(MetaDataUtility.FORMAT_CURRENT_DIRECTORY, workingDirectory);
        }

        private static bool CompileMetaDataFiles(
            ILog log,
            MetaDataCompiler metaDataCompiler,
            audProjectSettings projectSettings,
            audMetadataType metaDataType,
            string workingDirectory,
            string workingPath,
            CompilationMode compilationMode,
            uint chanageListNumber,
            EnumRuntimeMode runtimeMode)
        {
            IEnumerable<audMetadataFile> metaDataFiles =
                projectSettings.GetMetadataSettings().Where(
                    x => string.Compare(x.Type, metaDataType.TypeNotUpper, true) == 0);
            foreach(audMetadataFile metaDataFile in metaDataFiles)
            {
                string dataPath = string.Concat(workingDirectory, metaDataFile.DataPath);
                List<DocumentEntry> objectsXmlDocumentEntries = Utility.LoadDocuments(log, dataPath);
                bool isTaggingEnabled = false;
                isTaggingEnabled = (metaDataType.SchemaPath != null && File.Exists(metaDataType.SchemaPath) &&
                                    File.ReadAllText(metaDataType.SchemaPath).IndexOf(FieldDefinitionTokens.Tagged.ToString(), StringComparison.OrdinalIgnoreCase)>=0);
                isTaggingEnabled = isTaggingEnabled && projectSettings.MetadataTagsEnabled;

                if (compilationMode == CompilationMode.CompileOnly ||
                    compilationMode == CompilationMode.TransformAndCompile)
                {
                    string outputFile = string.Concat(workingPath, projectSettings.ResolvePath(metaDataFile.OutputFile));
                    if (metaDataType.SideBySideVersioning &&
                        metaDataCompiler.Version != 0)
                    {
                        outputFile = string.Concat(outputFile, metaDataCompiler.Version);
                    }

                    if (runtimeMode == EnumRuntimeMode.Release)
                    {
                        outputFile += ".rel";
                    }

                    var nameTablePath = string.Concat(outputFile, ".NAMETABLE");

                    log.WriteFormatted(FORMAT_COMPILING_METADATA_TO, outputFile);
                    using (IWriter output = new BinaryWriter(File.Create(outputFile), projectSettings.IsBigEndian()))
                    {
                        if (!metaDataCompiler.Compile(objectsXmlDocumentEntries, output, metaDataFile, chanageListNumber, runtimeMode, isTaggingEnabled, nameTablePath))
                        {
                            return false;
                        }
                    }

                    log.Write(string.Empty);
                }
                else
                {
                    log.WriteFormatted(FORMAT_TRANSFORMING_METADATA_TO,
                                       string.Concat(workingDirectory, metaDataFile.TransformedXmlPath));

                    foreach (var documentEntry in objectsXmlDocumentEntries)
                    {
                        var transformedXmlPath = string.Concat(workingDirectory,
                                                               metaDataFile.TransformedXmlPath,
                                                               documentEntry.DocumentPath.Replace(dataPath, string.Empty));
                        var transformedXmlDirectory = Path.GetDirectoryName(transformedXmlPath);
                        if (!Directory.Exists(transformedXmlDirectory))
                        {
                            Directory.CreateDirectory(transformedXmlDirectory);
                        }
                        documentEntry.TransformedXmlPath = transformedXmlPath;
                    }

                    if (!metaDataCompiler.Compile(objectsXmlDocumentEntries, null, metaDataFile, chanageListNumber, runtimeMode, isTaggingEnabled))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private static ILog CreateLog(LogOutputFormat format, ILogWriter logWriter)
        {
            ILog log = null;
            switch (format)
            {
                case LogOutputFormat.Text:
                    {
                        log = new TextLog(logWriter, new ContextStack());
                        break;
                    }
                case LogOutputFormat.Xml:
                    {
                        log = new XmlLog(logWriter, new ContextStack(), LOG_ROOT_ELEMENT_NAME);
                        break;
                    }
            }
            return log;
        }

        private static ILogWriter CreateLogWriter(string logFile, out bool loggingToFile)
        {
            loggingToFile = !string.IsNullOrEmpty(logFile);
            ILogWriter logWriter;
            if (loggingToFile)
            {
                logWriter = new FileLogWriter(logFile);
            }
            else
            {
                logWriter = new ConsoleLogWriter();
            }
            return logWriter;
        }
    }
}