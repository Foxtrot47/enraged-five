using rage.Compiler;
using rage.ToolLib.Logging;

namespace mc
{
    using rage.Enums;

    public interface ICmdLineArgs
    {
        EnumRuntimeMode RuntimeMode { get; }
        string Project { get; }
        string Platform { get; }
        string MetaData { get; }
        bool CodeOutput { get; }
        string WorkingPath { get; }
        string Namespace { get; }
        string SoftRoot { get; }
        string ChangeListNumber { get; }
        LogVerbosity LogVerbosity { get; }
        LogOutputFormat LogFormat { get; }
        string LogFile { get; }
        CompilationMode CompileMode { get; }
        bool HelpRequested { get; }

        bool Parse(string[] args);
    }
}