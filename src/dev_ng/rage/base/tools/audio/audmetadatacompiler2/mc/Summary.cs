using System;
using System.Xml.Linq;
using rage.Compiler;
using rage.ToolLib.Logging;

namespace mc
{
    public class Summary
    {
        private readonly ILog m_log;
        private readonly LogOutputFormat m_logOutputFormat;
        private readonly IObjectSizeTracker m_objectSizeTracker;

        public Summary(ILog log, IObjectSizeTracker objectSizeTracker, LogOutputFormat logOutputFormat)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }

            if (objectSizeTracker == null)
            {
                throw new ArgumentNullException("objectSizeTracker");
            }

            m_log = log;
            m_objectSizeTracker = objectSizeTracker;
            m_logOutputFormat = logOutputFormat;
        }

        public void Write()
        {
            switch (m_logOutputFormat)
            {
                case LogOutputFormat.Xml:
                    {
                        m_log.Write(CreateXmlSummary());
                        break;
                    }
                case LogOutputFormat.Text:
                    {
                        m_log.WriteFormatted(
                            "All done: {0} object(s) compiled, {1} exception(s), {2} error(s), {3} warning(s).\nAverage object size: {4}.\nLargest object name: \"{5}\", size: {6}.\n",
                            m_objectSizeTracker.NumObjects,
                            m_log.NumExceptionsLogged,
                            m_log.NumErrorsLogged,
                            m_log.NumWarningsLogged,
                            m_objectSizeTracker.AverageObjectSize,
                            m_objectSizeTracker.LargestObjectName,
                            m_objectSizeTracker.LargestObjectSize);
                        break;
                    }
            }
        }

        private XElement CreateXmlSummary()
        {
            var objectsCompiled = new XElement("ObjectsCompiled", m_objectSizeTracker.NumObjects);
            var exceptions = new XElement("Exceptions", m_log.NumExceptionsLogged);
            var errors = new XElement("Errors", m_log.NumErrorsLogged);
            var warnings = new XElement("Warnings", m_log.NumWarningsLogged);
            var avgObjectSize = new XElement("AverageObjectSize", m_objectSizeTracker.AverageObjectSize);
            var largestObject = new XElement("LargestObjectSize",
                                             new XAttribute("name", m_objectSizeTracker.LargestObjectName),
                                             m_objectSizeTracker.LargestObjectSize);
            return new XElement("Summary", objectsCompiled, exceptions, errors, warnings, avgObjectSize, largestObject);
        }
    }
}