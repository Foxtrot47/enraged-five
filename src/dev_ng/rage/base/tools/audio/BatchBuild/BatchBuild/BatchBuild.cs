using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using rage;
using Rave.WaveBuilderClient;
using System.Collections;
using System.Xml;
using Microsoft.Win32;
using audAssetManagement;

namespace BatchBuild
{
    public class BatchBuild
    {
        private static audProjectSettings projectSettings;
        private static WaveBuilderClient waveBuilderClient;
        private static outputLog log;
        private static DateTime start;
        private static TimeSpan duration;
        private static string settings, output;
        private static string outputPath, assetUser, assetPassword, assetServer;
        private static audAssetManager assetManager;
        private static int assetManagerType;

        public static void Main(string[] args)
        {

            if (args.Length != 0)
            {
                output = settings = outputPath = "";

                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] == "-settings")
                    {
                        settings = args[i + 1];
                    }
                    else if (args[i] == "-output")
                    {
                        outputPath = args[i + 1];
                    }
                }

                log = new outputLog(outputPath);
                bool bSettingsLoaded = false;

                //load project settings
                try
                {
                    projectSettings = new rage.audProjectSettings(settings);
                }
                catch
                {
                    log.addOutput("All", " Could not load project settings");
                }


                try
                {
                    //load settings                  
                    RegistryKey common = Registry.CurrentUser.OpenSubKey(@"Software\Rockstar\RAGEAudio\Common");

                    if (common != null)
                    {
                        assetUser = (string)common.GetValue("AssetUser");
                        assetPassword = (string)common.GetValue("AssetPassword");
                        assetServer = (string)common.GetValue("AssetServer");
                        assetManager = audAssetManagement.audAssetManager.CreateInstance((int)common.GetValue("AssetManagerType"));
                        assetUser = (string)common.GetValue("AssetUser");
                        assetPassword = (string)common.GetValue("AssetPassword");
                        assetManagerType = (int)common.GetValue("AssetManagerType");
                        assetManager.Init(projectSettings.GetAssetServer(), projectSettings.GetAssetProject(), assetUser, assetPassword, false);
                        bSettingsLoaded = true;
                    }
                }
                catch
                {
                    log.addOutput("All", "Could not load settings, process aborted");
                }


                if (bSettingsLoaded && projectSettings != null)
                {
                    waveBuilderClient = new WaveBuilderClient(projectSettings.GetServerHost(), projectSettings.GetServerPort());
                    //build each enabled platform
                    foreach (PlatformSetting ps in projectSettings.GetPlatformSettings())
                    {
                        if (ps.BuildEnabled)
                        {
                            start = DateTime.Now;
                            duration = new TimeSpan(0, 0, 0);
                            DoBuild(ps.PlatformTag);
                            System.Threading.Thread.Sleep(5000);
                        }
                    }

                }
            }
            else
            {
                log = new outputLog(Path.GetTempPath());
            }
               
           log.writeFile();
        }

        private static void DoBuild(string platformTag)
        {
            waveBuilderClient.Connect();     
            DateTime queryTime = new DateTime();
            bool bFinished = false;
            int attemptsQuery = 0;
            int attemptsError = 0;
            int attemptsConnect = 0;
            while (!bFinished)
            {
                if (!waveBuilderClient.IsActive())
                {
                    if (attemptsConnect < 5)
                    {
                        //Sleep for a second to give the build service a chance to clean-up if we get disconnected.
                        System.Threading.Thread.Sleep(5000);

                        if (!waveBuilderClient.Connect())
                        {
                            output += "Couldn't connect to wave build service\r\n";
                            bFinished = true;
                        }
                    }
                    else
                    {
                        bFinished = true;
                        output += "Lost connection with wave build service\r\n";
                        continue;
                    }
                }

                //get messages from server
                WaveBuilderClient.Message msg = waveBuilderClient.GetNextMessage();

                if (msg != null)
                {
                    switch (msg.type)
                    {
                        case WaveBuilderClient.MessageType.QUERY:
                            //Pending WaveLists Locked
                            //We assume that this is the only query
                            if (attemptsQuery == 0)
                            {
                                queryTime = DateTime.Now;
                                attemptsQuery++;
                            }
                            duration = DateTime.Now - queryTime;
                            //keep answering yes for 10mins
                            if (duration.Minutes < 10)
                            {
                                waveBuilderClient.SendMessage(WaveBuilderClient.MessageType.YES, "");
                            }
                            else
                            {
                                waveBuilderClient.SendMessage(WaveBuilderClient.MessageType.NO, "");
                                log.addOutput(platformTag, "Unable to resolve query " + msg.contents);
                            }
                            Console.Out.WriteLine("Pending waveLists Locked" + duration.Minutes);
                            break;
                        case WaveBuilderClient.MessageType.ERROR:
                            log.addOutput(platformTag, "Audio Build Server Error " + msg.contents);
                            //Two attempts per platform
                            if (attemptsError == 0)
                            {
                                waveBuilderClient.Disconnect();
                                System.Threading.Thread.Sleep(5000);
                                log.addOutput(platformTag, "Attempting to re-connect");
                                waveBuilderClient.Connect();
                                attemptsError++;
                            }
                            else
                            {
                                bFinished = true;
                            }
                            break;
                        case WaveBuilderClient.MessageType.BUSY:
                            log.addOutput(platformTag, "The Wave Builder Service is in the middle of a build " + msg.contents);
                            bFinished = true;
                            break;
                        case WaveBuilderClient.MessageType.CONNECTED:
                            string configString;
                            
                            configString = string.Format("server={0},project={1},username={2},password={3},projectsettings={4},buildplatform={5},localbuild={6},assetmanagertype={7},localhost={8},deferred={9}",
                                assetServer,
                                projectSettings.GetBuildAssetClient(),
                                projectSettings.GetBuildAssetUser(),
                                "",
                                settings,
                                platformTag,
                                "NO",
                                assetManagerType,
                                waveBuilderClient.GetLocalHostname(),
                                "YES"
                                );

                            waveBuilderClient.SendMessage(WaveBuilderClient.MessageType.CONFIG, configString);
                            break;

                        case WaveBuilderClient.MessageType.START:
                            log.addOutput(platformTag, "Build started " + DateTime.Now);
                            break;

                        case WaveBuilderClient.MessageType.PROGRESS:
                            Console.Out.WriteLine(msg.contents);
                            break;

                        case WaveBuilderClient.MessageType.DONE:
                            log.addOutput(platformTag, "Build completed successfully " + DateTime.Now);
                            bFinished = true;
                            break;



                    }
                }
            }
            waveBuilderClient.Disconnect();
        }

    }
}
