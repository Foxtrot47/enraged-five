using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BatchBuild
{
    
    public class outputLog
    {
        private string m_Output;
        private string m_OutputPath;

        public outputLog(string outputPath)
        {
            m_OutputPath = outputPath;
            //create html header for logger
            m_Output = "<html>";
            m_Output += "<head><title>Automated Wave Build</title></head>";
            m_Output += "<body><center>";
            m_Output += "<h2>Automated Wave Build"+" - "+ DateTime.Now+"</h2>";
            m_Output += "<table border=\"1\"><tr><th>Platform</th><th>Message</th></tr>";
        }

        public void addOutput(string platform, string msg)
        {
            //each message is a new rown in the table
            m_Output += "<tr><td>"+platform+"</td><td>" + msg + "</td></tr>";
        }

        public void writeFile()
        {
            //close html
            m_Output += "</table></center></body></html>";
            //check if the directory specified in the xml exists.. if not write file to the temporary path
            if (!Directory.Exists(m_OutputPath))
            {
                m_OutputPath = Path.GetTempPath();
            }
            string FileName = m_OutputPath + "\\Build" + "." + DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + ".html";
            FileStream File_Stream = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            StreamWriter FileWriter = new StreamWriter(File_Stream);
            try
            {
                FileWriter.WriteLine(m_Output);
                
            }
            catch (IOException)
            {
                //no where to write it to.. log failed
                System.Diagnostics.Debug.WriteLine("Writing log failed");
            }
            finally
            {
                FileWriter.Close();
            }
        }

    }
}
