namespace HashComputer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.txtInput = new System.Windows.Forms.TextBox();
			this.txtHash = new System.Windows.Forms.TextBox();
			this.copyTxtHash = new System.Windows.Forms.Button();
			this.txtHashHex = new System.Windows.Forms.TextBox();
			this.btnCopyHex = new System.Windows.Forms.Button();
			this.btnATSTRINGHASH = new System.Windows.Forms.Button();
			this.txtATSTRINGHASH = new System.Windows.Forms.TextBox();
			this.btnPaste = new System.Windows.Forms.Button();
			this.FinaliseHash = new System.Windows.Forms.CheckBox();
			this.WidthBits = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.uHashLabel = new System.Windows.Forms.Label();
			this.sHashLabel = new System.Windows.Forms.Label();
			this.txtHashSigned = new System.Windows.Forms.TextBox();
			this.copyTxtHashSigned = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.WidthBits)).BeginInit();
			this.SuspendLayout();
			// 
			// txtInput
			// 
			this.txtInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtInput.Location = new System.Drawing.Point(12, 12);
			this.txtInput.Name = "txtInput";
			this.txtInput.Size = new System.Drawing.Size(362, 20);
			this.txtInput.TabIndex = 0;
			this.txtInput.TextChanged += new System.EventHandler(this.txtInput_TextChanged);
			// 
			// txtHash
			// 
			this.txtHash.Location = new System.Drawing.Point(43, 65);
			this.txtHash.Name = "txtHash";
			this.txtHash.ReadOnly = true;
			this.txtHash.Size = new System.Drawing.Size(122, 20);
			this.txtHash.TabIndex = 1;
			this.txtHash.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// copyTxtHash
			// 
			this.copyTxtHash.Location = new System.Drawing.Point(171, 64);
			this.copyTxtHash.Name = "copyTxtHash";
			this.copyTxtHash.Size = new System.Drawing.Size(44, 23);
			this.copyTxtHash.TabIndex = 2;
			this.copyTxtHash.Text = "Copy";
			this.copyTxtHash.UseVisualStyleBackColor = true;
			this.copyTxtHash.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtHashHex
			// 
			this.txtHashHex.Location = new System.Drawing.Point(245, 66);
			this.txtHashHex.Name = "txtHashHex";
			this.txtHashHex.ReadOnly = true;
			this.txtHashHex.Size = new System.Drawing.Size(129, 20);
			this.txtHashHex.TabIndex = 3;
			this.txtHashHex.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// btnCopyHex
			// 
			this.btnCopyHex.Location = new System.Drawing.Point(380, 64);
			this.btnCopyHex.Name = "btnCopyHex";
			this.btnCopyHex.Size = new System.Drawing.Size(44, 23);
			this.btnCopyHex.TabIndex = 4;
			this.btnCopyHex.Text = "Copy";
			this.btnCopyHex.UseVisualStyleBackColor = true;
			this.btnCopyHex.Click += new System.EventHandler(this.btnCopyHex_Click);
			// 
			// btnATSTRINGHASH
			// 
			this.btnATSTRINGHASH.Location = new System.Drawing.Point(380, 122);
			this.btnATSTRINGHASH.Name = "btnATSTRINGHASH";
			this.btnATSTRINGHASH.Size = new System.Drawing.Size(44, 23);
			this.btnATSTRINGHASH.TabIndex = 6;
			this.btnATSTRINGHASH.Text = "Copy";
			this.btnATSTRINGHASH.UseVisualStyleBackColor = true;
			this.btnATSTRINGHASH.Click += new System.EventHandler(this.btnATSTRINGHASH_Click);
			// 
			// txtATSTRINGHASH
			// 
			this.txtATSTRINGHASH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtATSTRINGHASH.Location = new System.Drawing.Point(12, 122);
			this.txtATSTRINGHASH.Name = "txtATSTRINGHASH";
			this.txtATSTRINGHASH.ReadOnly = true;
			this.txtATSTRINGHASH.Size = new System.Drawing.Size(362, 20);
			this.txtATSTRINGHASH.TabIndex = 5;
			this.txtATSTRINGHASH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// btnPaste
			// 
			this.btnPaste.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnPaste.Location = new System.Drawing.Point(380, 9);
			this.btnPaste.Name = "btnPaste";
			this.btnPaste.Size = new System.Drawing.Size(44, 23);
			this.btnPaste.TabIndex = 7;
			this.btnPaste.Text = "Paste";
			this.btnPaste.UseVisualStyleBackColor = true;
			this.btnPaste.Click += new System.EventHandler(this.btnPaste_Click);
			// 
			// FinaliseHash
			// 
			this.FinaliseHash.AutoSize = true;
			this.FinaliseHash.Checked = true;
			this.FinaliseHash.CheckState = System.Windows.Forms.CheckState.Checked;
			this.FinaliseHash.Location = new System.Drawing.Point(12, 40);
			this.FinaliseHash.Name = "FinaliseHash";
			this.FinaliseHash.Size = new System.Drawing.Size(93, 17);
			this.FinaliseHash.TabIndex = 8;
			this.FinaliseHash.Text = "Finalise hash?";
			this.FinaliseHash.UseVisualStyleBackColor = true;
			this.FinaliseHash.CheckedChanged += new System.EventHandler(this.FinaliseHash_CheckedChanged);
			// 
			// WidthBits
			// 
			this.WidthBits.Location = new System.Drawing.Point(261, 39);
			this.WidthBits.Maximum = new decimal(new int[] {
            32,
            0,
            0,
            0});
			this.WidthBits.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.WidthBits.Name = "WidthBits";
			this.WidthBits.Size = new System.Drawing.Size(61, 20);
			this.WidthBits.TabIndex = 9;
			this.WidthBits.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
			this.WidthBits.ValueChanged += new System.EventHandler(this.WidthBits_ValueChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(190, 41);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(63, 13);
			this.label1.TabIndex = 10;
			this.label1.Text = "Width (bits):";
			// 
			// uHashLabel
			// 
			this.uHashLabel.AutoSize = true;
			this.uHashLabel.Location = new System.Drawing.Point(12, 68);
			this.uHashLabel.Name = "uHashLabel";
			this.uHashLabel.Size = new System.Drawing.Size(25, 13);
			this.uHashLabel.TabIndex = 11;
			this.uHashLabel.Text = "u32";
			// 
			// sHashLabel
			// 
			this.sHashLabel.AutoSize = true;
			this.sHashLabel.Location = new System.Drawing.Point(12, 98);
			this.sHashLabel.Name = "sHashLabel";
			this.sHashLabel.Size = new System.Drawing.Size(24, 13);
			this.sHashLabel.TabIndex = 12;
			this.sHashLabel.Text = "s32";
			// 
			// txtHashSigned
			// 
			this.txtHashSigned.Location = new System.Drawing.Point(43, 95);
			this.txtHashSigned.Name = "txtHashSigned";
			this.txtHashSigned.ReadOnly = true;
			this.txtHashSigned.Size = new System.Drawing.Size(122, 20);
			this.txtHashSigned.TabIndex = 13;
			this.txtHashSigned.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// copyTxtHashSigned
			// 
			this.copyTxtHashSigned.Location = new System.Drawing.Point(171, 93);
			this.copyTxtHashSigned.Name = "copyTxtHashSigned";
			this.copyTxtHashSigned.Size = new System.Drawing.Size(44, 23);
			this.copyTxtHashSigned.TabIndex = 14;
			this.copyTxtHashSigned.Text = "Copy";
			this.copyTxtHashSigned.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(429, 169);
			this.Controls.Add(this.copyTxtHashSigned);
			this.Controls.Add(this.txtHashSigned);
			this.Controls.Add(this.sHashLabel);
			this.Controls.Add(this.uHashLabel);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.WidthBits);
			this.Controls.Add(this.FinaliseHash);
			this.Controls.Add(this.btnPaste);
			this.Controls.Add(this.btnATSTRINGHASH);
			this.Controls.Add(this.txtATSTRINGHASH);
			this.Controls.Add(this.btnCopyHex);
			this.Controls.Add(this.txtHashHex);
			this.Controls.Add(this.copyTxtHash);
			this.Controls.Add(this.txtHash);
			this.Controls.Add(this.txtInput);
			this.Name = "Form1";
			this.Text = "Ali\'s Hash Computer";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.WidthBits)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.TextBox txtHash;
        private System.Windows.Forms.Button copyTxtHash;
        private System.Windows.Forms.TextBox txtHashHex;
        private System.Windows.Forms.Button btnCopyHex;
        private System.Windows.Forms.Button btnATSTRINGHASH;
        private System.Windows.Forms.TextBox txtATSTRINGHASH;
        private System.Windows.Forms.Button btnPaste;
        private System.Windows.Forms.CheckBox FinaliseHash;
        private System.Windows.Forms.NumericUpDown WidthBits;
        private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label uHashLabel;
		private System.Windows.Forms.Label sHashLabel;
		private System.Windows.Forms.TextBox txtHashSigned;
		private System.Windows.Forms.Button copyTxtHashSigned;
    }
}

