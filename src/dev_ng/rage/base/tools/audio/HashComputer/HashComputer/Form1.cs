using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HashComputer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // taken from rage atl/map.cpp
        uint ComputeHash(string str)
        {
	        if(str == null || str.Equals(""))
	        {
		        return 0;
	        }

	        //Convert .Net Wave name to a system string and then to a Rage hash code.
            byte[] utf8 = System.Text.Encoding.UTF8.GetBytes(str);
            

	        // This is the one-at-a-time hash from this page:
	        // http://burtleburtle.net/bob/hash/doobs.html
	        // (borrowed from /soft/swat/src/swcore/string2key.cpp)
	        uint key = 0;

	        for(int i = 0; i < str.Length; i++)
	        {
		        byte character = utf8[i];
		        if (character >= 'A' && character <= 'Z') 
			        character += 'a'-'A';
		        else if (character == '\\')
			        character = (byte)'/';

		        key += character;
		        key += (key << 10);	//lint !e701
		        key ^= (key >> 6);	//lint !e702
	        }

            if (FinaliseHash.Checked)
            {
                key += (key << 3);	//lint !e701
                key ^= (key >> 11);	//lint !e702
                key += (key << 15);	//lint !e701
            }

	        // The original swat code did several tests at this point to make
	        // sure that the resulting value was representable as a valid
	        // floating-point number (not a negative zero, or a NaN or Inf)
	        // and also reserved a nonzero value to indicate a bad key.
	        // We don't do this here for generality but the caller could
	        // obviously add such checks again in higher-level code.
	        return key;
        }

        private void RefreshHash()
        {
            uint uHash = ComputeHash(txtInput.Text);

            int numBits = (int)WidthBits.Value;
            if (numBits < 32)
            {
                long mask = (1 << numBits) - 1;
				uHash &= (uint)mask;
            }

			txtHash.Text = uHash.ToString();
			txtHashHex.Text = string.Format("0x{0:X}", uHash);

			int sHash = (int)uHash;
			txtHashSigned.Text = sHash.ToString();

            if (FinaliseHash.Checked && numBits == 32)
            {
				txtATSTRINGHASH.Text = string.Format("ATSTRINGHASH(\"{0}\", 0x{1:X})", txtInput.Text, uHash);
            }
            else if (numBits == 32)
            {
                txtATSTRINGHASH.Text = string.Format("ATPARTIALSTRINGHASH(\"{0}\", 0x{1:X})", txtInput.Text, uHash);
            }
            else
            {
                txtATSTRINGHASH.Text = string.Empty;
            }
        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            RefreshHash();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtHash.Text);
        }

        private void btnCopyHex_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtHashHex.Text);
        }

        private void btnATSTRINGHASH_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtATSTRINGHASH.Text);
        }

        private void btnPaste_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                txtInput.Text = Clipboard.GetText();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void FinaliseHash_CheckedChanged(object sender, EventArgs e)
        {
            RefreshHash();
        }

        private void WidthBits_ValueChanged(object sender, EventArgs e)
        {
            RefreshHash();
        }
    }
}