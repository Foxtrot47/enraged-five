using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ProjectLoader2;
using System.Collections;
using audAssetManagement2;
using System.Xml;
using Rave.Plugins;
using rage;

namespace FieldStripper
{
    public partial class frmMain : Form
    {
        private Dictionary<string, List<string>> m_Banks;
        private changelist m_Changelist;
        private StringBuilder m_Output;

        public frmMain(string[] args)
        {
            InitializeComponent();
            backgroundWorker1.WorkerReportsProgress = true;
            m_Output = new StringBuilder();
            try
            {
                frmProjectLoader pl = new frmProjectLoader(String.Concat(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"\Rave\ProjectList.xml"));
                pl.ShowDialog();

                Rave.Plugins.Configuration.InitProject(pl.Project);
                AssetManager.SetAssetManager(pl.AssetManager);
                m_Changelist = AssetManager.GetAssetManager().CreateNewChangelist("Field Stripper");
                Rave.Plugins.Configuration.LoadProjectSettings();
                LoadTypeDefinitions();
                LoadBanks();                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            m_TextBox.AppendText(e.UserState.ToString()+"\r\n");
        }

        void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_Output.AppendLine("Done");
            m_TextBox.AppendText(m_Output.ToString());
            btnGo.Enabled = true;
        }

        void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            ParseBanks(worker);
        }

        private void LoadBanks()
        {
            m_Banks = new Dictionary<string, List<string>>();
            foreach (audMetadataFile file in Rave.Plugins.Configuration.GetProjectSettings().GetMetadataSettings())
            {
                string type = file.Type.ToUpper();
                if (!m_Banks.ContainsKey(type))
                {
                    m_Banks.Add(type, new List<string>());
                }

                GetAllBanks(m_Banks[type], new DirectoryInfo(AssetManager.GetAssetManager().GetWorkingPath(Rave.Plugins.Configuration.ObjectXmlPath + file.DataPath)));
            }
        }

        private void LoadTypeDefinitions()
        {

            foreach (PlatformSetting ps in Rave.Plugins.Configuration.GetPlatformSettings())
            {

                foreach (audMetadataType metadataType in Rave.Plugins.Configuration.GetProjectSettings().GetMetadataTypes())
                {
                    ArrayList al = new ArrayList();
                    foreach (audObjectDefinition objDef in metadataType.ObjectDefinitions)
                    {
                        al.Add(AssetManager.GetAssetManager().GetWorkingPath(Rave.Plugins.Configuration.ObjectXmlPath + objDef.DefinitionsFile));
                    }
                    if (TypeDefinitions.AllTypeDefinitions.ContainsKey(metadataType.Type))
                    {
                        TypeDefinitions.AllTypeDefinitions[metadataType.Type].Load((string[])al.ToArray(typeof(string)));
                    }
                    else
                    {
                        TypeDefinitions typedefs = new TypeDefinitions((string[])al.ToArray(typeof(string)), metadataType.SchemaPath);
                        TypeDefinitions.AllTypeDefinitions.Add(metadataType.Type, typedefs);
                    }

                }

            }

        }

        private static void GetAllBanks(List<string> banks, DirectoryInfo dir)
        {
            foreach (FileInfo file in dir.GetFiles())
            {
                banks.Add(file.FullName);
            }

            foreach (DirectoryInfo childDir in dir.GetDirectories())
            {
                GetAllBanks(banks, childDir);
            }
        }

        private void ParseBanks(BackgroundWorker worker)
        {
            string path = Path.GetTempFileName();

            foreach (KeyValuePair<string, List<string>> kvp in m_Banks)
            {
                foreach (string file in kvp.Value)
                {
                    worker.ReportProgress(0, "Processing " + file);
                    ParseObjects(file, kvp.Key);
                }
            }
        }

        private void ParseObjects(string filePath, string type)
        {
            AssetManager.GetAssetManager().GetLatest(filePath, false);
            asset a = m_Changelist.CheckoutAsset(filePath, false);

            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);

            bool Save = false;

            foreach (XmlNode objectNode in doc.DocumentElement.ChildNodes)
            {
                TypeDefinition typeDef = TypeDefinitions.AllTypeDefinitions[type].FindTypeDefinition(objectNode.Name);
                if (CheckFields(objectNode, typeDef.Fields))
                {
                    Save = true;
                }
            }

            if (!Save)
            {
                a.Revert();
            }
            else
            {
                doc.Save(filePath);
                m_Output.AppendLine("Stripped File: " + filePath);
            }
        }

        private bool CheckFields(XmlNode objectNode, FieldDefinition [] fields)
        {
            bool returnVal = false;

            List<XmlNode> nodesToDelete = new List<XmlNode>();

            foreach (FieldDefinition field in fields)
            {
                bool found = false;

                foreach (XmlNode node in objectNode.ChildNodes)
                {
                    CompositeFieldDefinition comp = field as CompositeFieldDefinition;
                    if (comp != null)
                    {
                        if (CheckFields(node, comp.Fields))
                        {
                            returnVal = true;
                        }
                    }
                    if (node.Name == field.Name)
                    {
                        if (!found)
                        {
                            found = true;
                        }
                        else if (comp == null || comp != null && comp.MaxOccurs == 1)
                        {
                            nodesToDelete.Add(node);
                        }
                    }                

                }
            }

            foreach (XmlNode toDelete in nodesToDelete)
            {
                objectNode.RemoveChild(toDelete);
            }

            if (returnVal || nodesToDelete.Count > 0)
            {
                returnVal = true;
            }

            return returnVal;
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            btnGo.Enabled = false;
            m_TextBox.Text += "Starting Field Stripping...\r\n";
            backgroundWorker1.RunWorkerAsync();
  
        }
    }
}