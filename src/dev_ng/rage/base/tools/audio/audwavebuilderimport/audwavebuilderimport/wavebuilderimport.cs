using System;
using System.Xml;
using System.IO;
using System.Text;

namespace audwavebuilderimport
{
	class audWaveBuilderImport
	{

		static void AddWaves(XmlTextWriter output, string path, int level)
		{
			int nextLevel;

			string[] files = Directory.GetFiles(path, "*.wav");

			foreach(string file in files)
			{
				string fileName = file.Substring(file.LastIndexOf("\\") + 1);
				//fileName = fileName.Remove(fileName.Length-4, 4);

				output.WriteStartElement("Wave");
				
				output.WriteStartAttribute("name", "");
				output.WriteString(fileName);
				output.WriteEndAttribute();

				output.WriteStartAttribute("operation", "");
				output.WriteString("add");
				output.WriteEndAttribute();

				output.WriteEndElement();
			}

			string[] directories = Directory.GetDirectories(path);

			foreach(string directory in directories)
			{
				string directoryName = directory.Substring(directory.LastIndexOf("\\") + 1);
				switch(level)
				{
					case 0:
						// pack level
						output.WriteStartElement("Pack");

						output.WriteStartAttribute("name", "");
						output.WriteString(directoryName);
						output.WriteEndAttribute();

						output.WriteStartAttribute("operation", "");
						output.WriteString("add");
						output.WriteEndAttribute();

						nextLevel = level + 1;

						AddWaves(output, directory, nextLevel);

						output.WriteEndElement();
						break;
						
					case 1:
						// either bank or bank folder

						if(directoryName.StartsWith("#"))
						{
							output.WriteStartElement("BankFolder");
							nextLevel = level;
						}
						else
						{
							output.WriteStartElement("Bank");
							nextLevel = level + 1;
						}

						output.WriteStartAttribute("name", "");
						output.WriteString(directoryName);
						output.WriteEndAttribute();

						output.WriteStartAttribute("operation", "");
						output.WriteString("add");
						output.WriteEndAttribute();

						AddWaves(output, directory, nextLevel);
						output.WriteEndElement();
						break;

					case 2:
						// should only be a wave folder

						output.WriteStartElement("WaveFolder");

						output.WriteStartAttribute("name", "");
						output.WriteString(directoryName);
						output.WriteEndAttribute();

						output.WriteStartAttribute("operation", "");
						output.WriteString("add");
						output.WriteEndAttribute();
						
						nextLevel = level;
						AddWaves(output, directory, nextLevel);
						output.WriteEndElement();
						break;
				}
			}
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			if(args.Length != 2)
			{
				Console.WriteLine("usage: WaveBuilderImport <path to source waves> <path to PendingWaves.xml>");
				return;
			}

            string sourceWavePath = args[0];
            string pendingWavePath = args[1] + "\\PendingWaves.xml";

            XmlTextWriter output = new XmlTextWriter(pendingWavePath, Encoding.UTF8);
			output.WriteStartElement("PendingWaves");

            AddWaves(output, sourceWavePath, 0);

			output.WriteEndElement();
			output.Close();
		}
	}
}
