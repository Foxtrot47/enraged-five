﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rockstar.AssetManager.Interfaces;
using System.IO;

namespace ProjectLoader2 {
    class Logger: ILogger {

        private String filePath;

        public Logger(string filePath) {
            this.filePath = filePath;
            if(File.Exists(filePath)) {
                File.Delete(filePath); 
            }
        }

        StringBuilder tempLogContent = new StringBuilder();

        public void writeLine(string line) {
            File.AppendAllText(filePath, line+Environment.NewLine);
        }
    }
}
