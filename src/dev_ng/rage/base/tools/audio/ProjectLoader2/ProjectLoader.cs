using System;
using System.Collections.Generic;
using System.Text;
using rage;
using Microsoft.Win32;
using System.Windows.Forms;
using audAssetManagement2;

namespace audProjectLoader
{
    public class ProjectLoader
    {
        private string m_AssetUser;
        private string m_AssetPassword;
        private string m_AssetProject;
        private string m_AssetServer;
        private string m_ProjectListPath;
        private string m_DepotRoot;
        private int m_AssetManagerType;

        private audProjectEntry m_ProjectEntry;
        private audProjectSettings m_ProjectSettings;
        private assetManager m_AssetManager;


        public ProjectLoader()
        {
            m_ProjectSettings = null;
            m_AssetManager = null;
        }


        public bool Run()
        {
            try
            {
                LoadRegistrySettings();
                LoadProjectList();
                LoadAssetManager();
                LoadProjectSettings();
                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error Loading Project", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void LoadRegistrySettings()
        {
            RegistryKey common = Registry.CurrentUser.OpenSubKey(@"Software\Rockstar\RAGEAudio\Common");
				
				if(common != null)
				{
					m_AssetUser = (string)common.GetValue("AssetUser");
					m_AssetPassword = (string)common.GetValue("AssetPassword");
					m_AssetProject = (string)common.GetValue("AssetProject");
					m_AssetServer = (string)common.GetValue("AssetServer");
					m_ProjectListPath = (string)common.GetValue("ProjectListPath");
                    m_AssetManagerType = 2;// (int)common.GetValue("AssetManagerType");
                    m_DepotRoot = (string)common.GetValue("DepotRoot");
				}
				else
				{
					throw new Exception("Failed to finde registry key: Software\\Rockstar\\RAGEAudio\\Common");
				}
        }

        private void LoadProjectList()
        {
            audProjectList projectList = new audProjectList(m_ProjectListPath);
			frmProjectLoader projectLoader = new frmProjectLoader(projectList.Projects);
            projectLoader.ShowDialog();
            m_ProjectEntry = projectLoader.sm_Project;
        }

        private void LoadProjectSettings()
        {
            if (m_ProjectEntry == null)
            {
                throw new Exception("Failed to find project");
            }
            else
            {
                m_ProjectSettings = new audProjectSettings(m_AssetManager.GetWorkingPath(m_ProjectEntry.ProjectSettings));
                if (m_ProjectSettings == null)
                {
                    throw new Exception("Failed to Load Project Settings");
                }
            }
        }

        private void LoadAssetManager()
        {
            m_AssetManager = assetManager.CreateInstance(m_AssetManagerType);
            m_AssetManager.Init(m_AssetServer, m_AssetProject, m_AssetUser, m_AssetPassword, m_DepotRoot);
        }

        public audProjectSettings ProjectSettings
        {
            get{return m_ProjectSettings;}
        }

        public assetManager AssetManager
        {
            get { return m_AssetManager; }
        }

        public audProjectEntry ProjectEntry
        {
            get { return m_ProjectEntry; }
        }
    }
}
