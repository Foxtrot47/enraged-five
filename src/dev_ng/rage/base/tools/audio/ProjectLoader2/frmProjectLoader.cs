using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using rage;

namespace ProjectLoader2
{
    using Rockstar.AssetManager.Infrastructure.Enums;
    using Rockstar.AssetManager.Interfaces;
    using Rockstar.AssetManager.Main;

    /// <summary>
    ///   Summary description for frmPreferences.
    /// </summary>
    public class frmProjectLoader : Form
    {
        private readonly audProjectList m_projectList;
        private Button btnNew;
        private Button btnOk;
        private CheckBox chkGrabPS3;
        private CheckBox chkGrabX360;
        private CheckBox chkGrabPC;
        private CheckBox chkGrabx64XMA;
        private CheckBox chkGrabPS4;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label lblDepotPath;
        private Label lblSoftPath;
        private HelpProvider m_HelpProvider;
        private IAssetManager m_assetManager;
        private ComboBox m_assetManagerType;
        private TextBox m_assetProject;
        private audProjectEntry m_currentProject;
        private TextBox m_depotRoot;
        private Label m_grabPS3;
        private Label m_grabX360;
        private Label m_grabPC;
        private TextBox m_password;
        private ComboBox m_project;
        private TextBox m_projectSettings;
        private TextBox m_serverName;
        private TextBox m_softRoot;
        private TextBox m_userName;
        private Button btnCancel;
        private TextBox m_workingPath;
        private Label lblWorkingPath;
        private Label m_grabx64XMA;
        private Label m_grabPS4;
        private TextBox m_waveEditorPath;

        public frmProjectLoader(string path, string selectedProject=null)
        {
            InitializeComponent();

            // Default to cancelled action until user clicks 'OK'
            IsCancelled = true;

            m_projectList = new audProjectList(path);

            if (m_projectList.Projects.Count == 0)
            {
                EnableInput(false);
            }
            else
            {
                m_project.Items.AddRange(m_projectList.Projects.ToArray());
                bool selectedProjectLoaded = false;
                if (!string.IsNullOrEmpty(selectedProject))
                {
                    foreach (audProjectEntry pe in m_projectList.Projects)
                    {
                        if (String.Compare(pe.Name, selectedProject, true)==0)
                        {
                            m_project.SelectedItem = pe;
                            LoadProject(pe);
                            selectedProjectLoaded = true;
                            break;
                        }
                    }
                }
                if(!selectedProjectLoaded)
                {
                    m_project.SelectedItem = m_project.Items[0];
                    LoadProject((audProjectEntry)m_project.Items[0]);
                }
                ActiveControl = m_password;
            }

            m_HelpProvider.SetShowHelp(m_userName, true);
            m_HelpProvider.SetHelpString(m_userName, "Windows user name");

            m_HelpProvider.SetShowHelp(m_password, true);
            m_HelpProvider.SetHelpString(m_password, "Windows password");

            m_HelpProvider.SetShowHelp(m_serverName, true);
            m_HelpProvider.SetHelpString(m_serverName, "Server name followed by the port e.g. RSGEDIP4D1:1666");

            m_HelpProvider.SetShowHelp(m_assetProject, true);
            m_HelpProvider.SetHelpString(m_assetProject, "Perforce workspace");

            m_HelpProvider.SetShowHelp(m_projectSettings, true);
            m_HelpProvider.SetHelpString(m_projectSettings, "Project settings path relative to the depot root");

            m_HelpProvider.SetShowHelp(m_depotRoot, true);
            var sb = new StringBuilder();
            sb.AppendLine(
                "Perforce depot root, used to obtain a full depot path for the paths in the project settings e.g.");
            sb.AppendLine("\r\nPerforce path: //depot/jimmy/audio/dev/assets/Objects/Core/Audio/Categories/");
            sb.AppendLine("Project Settings Path: /jimmy/audio/dev/assets/Objects/Core/Audio/Categories/");
            sb.AppendLine("Depot Root: //depot");
            m_HelpProvider.SetHelpString(m_depotRoot, sb.ToString());

            m_HelpProvider.SetShowHelp(m_softRoot, true);
            m_HelpProvider.SetHelpString(m_softRoot,
                                         "Optional path used for header generation when building metadata code");

            m_HelpProvider.SetShowHelp(m_workingPath, true);
            m_HelpProvider.SetHelpString(m_workingPath, "Override working path if required");

            m_HelpProvider.SetShowHelp(m_waveEditorPath, true);
            m_HelpProvider.SetHelpString(m_waveEditorPath, "Path to wave editing software");

            m_HelpProvider.SetShowHelp(chkGrabPS3, true);
            m_HelpProvider.SetHelpString(chkGrabPS3, "Grab latest data on start up for PS3");

            m_HelpProvider.SetShowHelp(chkGrabX360, true);
            m_HelpProvider.SetHelpString(chkGrabX360, "Grab latest data on start up for Xbox 360");
            
            m_HelpProvider.SetShowHelp(chkGrabPC, true);
            m_HelpProvider.SetHelpString(chkGrabPC, "Grab latest data on start up for PC");

            m_HelpProvider.SetShowHelp(chkGrabx64XMA, true);
            m_HelpProvider.SetHelpString(chkGrabx64XMA, "Grab latest data on start up for Durango/XBOXONE");

            m_HelpProvider.SetShowHelp(this.chkGrabPS4, true);
            m_HelpProvider.SetHelpString(this.chkGrabPS4, "Grab latest data on start up for PS4");
        }

        public IAssetManager AssetManager
        {
            get { return m_assetManager; }
        }

        public audProjectEntry Project
        {
            get { return m_currentProject; }
        }

        public bool IsCancelled { get; private set; }

        private void EnableInput(bool enable)
        {
            m_project.Enabled =
                m_userName.Enabled =
                m_assetManagerType.Enabled =
                m_assetProject.Enabled =
                m_depotRoot.Enabled =
                m_softRoot.Enabled =
                m_workingPath.Enabled =
                m_password.Enabled =
                m_project.Enabled =
                m_projectSettings.Enabled =
                m_serverName.Enabled = 
                m_waveEditorPath.Enabled = 
                btnOk.Enabled = 
                chkGrabPS3.Enabled =
                chkGrabX360.Enabled =
                chkGrabPC.Enabled = 
                chkGrabx64XMA.Enabled =
                this.chkGrabPS4.Enabled = enable;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            ValidateAndClose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void ValidateAndClose()
        {
            if (!ValidateChildren() ||
                !CreateAssetManager() ||
                !ProjectSettingExist())
            {
                return;
            }

            IsCancelled = false;
            m_projectList.RemeberProject(m_currentProject.Name);
            m_projectList.Save();
            Close();
        }

        private bool ProjectSettingExist()
        {
            var succeed = true;

            try
            {
                var path = m_assetManager.GetLocalPath(m_currentProject.ProjectSettings);
                if (!File.Exists(path))
                {
                    succeed = false;
                }
            }
            catch (Exception)
            {
                succeed = false;
            }

            if (!succeed)
            {
                MessageBox.Show("The project settings path is incorrect");
                try
                {
                    m_assetManager.Disconnect();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }

            return succeed;
        }

        private bool CreateAssetManager()
        {
            try
            {
                m_assetManager = AssetManagerFactory.GetInstance(
                    AssetManagerType.Perforce,
                    null,
                    m_currentProject.AssetServer,
                    m_currentProject.AssetProject,
                    m_currentProject.UserName,
                    m_currentProject.Password,
                    m_currentProject.DepotRoot,
                    new Logger(
                                string.Concat(
                                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                @"\Rave\P4SessionLog.log")),
                    "Rave Audio Rockstar");

                return true;
            }
            catch (Exception e)
            {
                if (null != m_assetManager)
                {
                    try
                    {
                        m_assetManager.Disconnect();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }

                MessageBox.Show(e.Message);
                return false;
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            EnableInput(true);
            LoadProject(m_projectList.CreateProject());
        }

        private void ProjectValidating(object sender, CancelEventArgs e)
        {
            if (m_project.Text == "")
            {
                MessageBox.Show("You must provide a name for the project");
                e.Cancel = true;
            }
            else
            {
                m_currentProject.Name = m_project.Text;
            }
        }

        private void LoadProject(audProjectEntry projectEntry)
        {
            m_currentProject = projectEntry;
            m_assetProject.Text = projectEntry.AssetProject;
            m_depotRoot.Text = projectEntry.DepotRoot;
            m_softRoot.Text = projectEntry.SoftRoot;
            m_workingPath.Text = projectEntry.WorkingPath;
            m_project.Text = projectEntry.Name;
            m_projectSettings.Text = projectEntry.ProjectSettings;
            m_serverName.Text = projectEntry.AssetServer;
            m_userName.Text = projectEntry.UserName;
            m_waveEditorPath.Text = projectEntry.WaveEditor;
            switch (projectEntry.AssetManager)
            {
                case "3":
                    m_assetManagerType.Text = "Local";
                    break;
                default:
                    m_assetManagerType.Text = "Perforce";
                    break;
            }
            chkGrabPS3.Checked = projectEntry.GrabLatestPS3;
            chkGrabX360.Checked = projectEntry.GrabLatestX360;
            chkGrabPC.Checked = projectEntry.GrabLatestPC;
            chkGrabx64XMA.Checked = projectEntry.GrabLatestx64XMA;
            this.chkGrabPS4.Checked = projectEntry.GrabLatestPS4;
        }

        private void ServerName_Validating(object sender, CancelEventArgs e)
        {
            if (m_serverName.Text == "" ||
                !Regex.Match(m_serverName.Text, "[a-zA-Z0-9]:[0-9]").Success)
            {
                MessageBox.Show("You must provide a valid asset server e.g RSGEDIP4D1:1666");
                e.Cancel = true;
            }
            else
            {
                m_currentProject.AssetServer = m_serverName.Text;
            }
        }

        private void AssetProject_Validate(object sender, CancelEventArgs e)
        {
            if (m_assetProject.Text == "")
            {
                MessageBox.Show("You must provide a name for the project");
                e.Cancel = true;
            }
            else
            {
                m_currentProject.AssetProject = m_assetProject.Text;
            }
        }

        private void Password_Validating(object sender, CancelEventArgs e)
        {
            if (m_currentProject.Password == null ||
                m_password.Text != "")
            {
                m_currentProject.Password = m_password.Text;
            }
        }

        private void ProjectSettings_Validating(object sender, CancelEventArgs e)
        {
            var text = m_projectSettings.Text;
            if (string.IsNullOrEmpty(text) ||
                !text.ToUpper().EndsWith(".XML"))
            {
                MessageBox.Show("Please provide a valid project settings path");
                e.Cancel = true;
                return;
            }

            text = text.Replace("/", "\\");
            if (!text.StartsWith("\\"))
            {
                text = "\\" + text;
            }
            m_projectSettings.Text = text;
            m_currentProject.ProjectSettings = text;
        }

        private void DepotRoot_Validating(object sender, CancelEventArgs e)
        {
            if (m_depotRoot.Text == "")
            {
                MessageBox.Show("Please provide a valid asset manager depot root");
                e.Cancel = true;
            }
            m_depotRoot.Text = m_depotRoot.Text.Replace("\\", "/");

            m_currentProject.DepotRoot = m_depotRoot.Text;
        }

        private void SoftRoot_Validating(object sender, CancelEventArgs e)
        {
            m_currentProject.SoftRoot = m_softRoot.Text;
        }

        private void WorkingPath_Validating(object sender, CancelEventArgs e)
        {
            m_currentProject.WorkingPath = m_workingPath.Text;
        }

        private void WaveEditor_Validating(object sender, CancelEventArgs e)
        {
            if (m_waveEditorPath.Text == "" ||
                !m_waveEditorPath.Text.ToUpper().EndsWith(".EXE"))
            {
                MessageBox.Show("Please provide a valid wave editor");
                e.Cancel = true;
            }

            m_currentProject.WaveEditor = m_waveEditorPath.Text;
        }

        private void Project_Changed(object sender, EventArgs e)
        {
            LoadProject((audProjectEntry) m_project.SelectedItem);
        }

        private void UserName_Validating(object sender, CancelEventArgs e)
        {
            if (m_userName.Text == "")
            {
                MessageBox.Show("Please provide a valid user name");
                e.Cancel = true;
            }

            m_currentProject.UserName = m_userName.Text;
        }

        private void AssetManagerType_Validating(object sender, CancelEventArgs e)
        {
            switch (m_assetManagerType.Text)
            {
                case "Local":
                    m_currentProject.AssetManager = "3";
                    break;
                default:
                    m_assetManagerType.Text = "2";
                    break;
            }
        }

        private void chkGrabPS3_Validating(object sender, CancelEventArgs e)
        {
            m_currentProject.GrabLatestPS3 = chkGrabPS3.Checked;
        }

        private void chkGrabX360_Validating(object sender, CancelEventArgs e)
        {
            m_currentProject.GrabLatestX360 = chkGrabX360.Checked;
        }

        private void chkGrabPC_Validating(object sender, CancelEventArgs e)
        {
            m_currentProject.GrabLatestPC = chkGrabPC.Checked;
        }

        private void chkGrabx64XMA_Validating(object sender, CancelEventArgs e)
        {
            m_currentProject.GrabLatestx64XMA = chkGrabx64XMA.Checked;
        }

        private void chkGrabPS4_Validating(object sender, CancelEventArgs e)
        {
            m_currentProject.GrabLatestPS4 = this.chkGrabPS4.Checked;
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///   Required method for Designer support - do not modify
        ///   the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProjectLoader));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_workingPath = new System.Windows.Forms.TextBox();
            this.m_softRoot = new System.Windows.Forms.TextBox();
            this.lblWorkingPath = new System.Windows.Forms.Label();
            this.lblSoftPath = new System.Windows.Forms.Label();
            this.m_depotRoot = new System.Windows.Forms.TextBox();
            this.lblDepotPath = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_password = new System.Windows.Forms.TextBox();
            this.m_userName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_projectSettings = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.m_assetProject = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_serverName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_assetManagerType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_grabPS4 = new System.Windows.Forms.Label();
            this.m_grabx64XMA = new System.Windows.Forms.Label();
            this.chkGrabPS3 = new System.Windows.Forms.CheckBox();
            this.m_grabPS3 = new System.Windows.Forms.Label();
            this.chkGrabX360 = new System.Windows.Forms.CheckBox();
            this.m_grabX360 = new System.Windows.Forms.Label();
            this.chkGrabPC = new System.Windows.Forms.CheckBox();
            this.m_grabPC = new System.Windows.Forms.Label();
            this.chkGrabx64XMA = new System.Windows.Forms.CheckBox();
            this.chkGrabPS4 = new System.Windows.Forms.CheckBox();
            this.m_waveEditorPath = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.m_project = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.m_HelpProvider = new System.Windows.Forms.HelpProvider();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.m_workingPath);
            this.groupBox1.Controls.Add(this.m_softRoot);
            this.groupBox1.Controls.Add(this.lblWorkingPath);
            this.groupBox1.Controls.Add(this.lblSoftPath);
            this.groupBox1.Controls.Add(this.m_depotRoot);
            this.groupBox1.Controls.Add(this.lblDepotPath);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.m_password);
            this.groupBox1.Controls.Add(this.m_userName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.m_projectSettings);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.m_assetProject);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.m_serverName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.m_assetManagerType);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(8, 60);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 267);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Asset Manager Settings";
            // 
            // m_workingPath
            // 
            this.m_workingPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_workingPath.Location = new System.Drawing.Point(118, 235);
            this.m_workingPath.Name = "m_workingPath";
            this.m_workingPath.Size = new System.Drawing.Size(242, 20);
            this.m_workingPath.TabIndex = 15;
            this.m_workingPath.Validating += new System.ComponentModel.CancelEventHandler(this.WorkingPath_Validating);
            // 
            // m_softRoot
            // 
            this.m_softRoot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_softRoot.Location = new System.Drawing.Point(118, 209);
            this.m_softRoot.Name = "m_softRoot";
            this.m_softRoot.Size = new System.Drawing.Size(242, 20);
            this.m_softRoot.TabIndex = 15;
            this.m_softRoot.Validating += new System.ComponentModel.CancelEventHandler(this.SoftRoot_Validating);
            // 
            // lblWorkingPath
            // 
            this.lblWorkingPath.Location = new System.Drawing.Point(8, 235);
            this.lblWorkingPath.Name = "lblWorkingPath";
            this.lblWorkingPath.Size = new System.Drawing.Size(104, 16);
            this.lblWorkingPath.TabIndex = 14;
            this.lblWorkingPath.Text = "Working Path";
            // 
            // lblSoftPath
            // 
            this.lblSoftPath.Location = new System.Drawing.Point(8, 212);
            this.lblSoftPath.Name = "lblSoftPath";
            this.lblSoftPath.Size = new System.Drawing.Size(104, 16);
            this.lblSoftPath.TabIndex = 14;
            this.lblSoftPath.Text = "Soft Root";
            // 
            // m_depotRoot
            // 
            this.m_depotRoot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_depotRoot.Location = new System.Drawing.Point(118, 182);
            this.m_depotRoot.Name = "m_depotRoot";
            this.m_depotRoot.Size = new System.Drawing.Size(242, 20);
            this.m_depotRoot.TabIndex = 13;
            this.m_depotRoot.Validating += new System.ComponentModel.CancelEventHandler(this.DepotRoot_Validating);
            // 
            // lblDepotPath
            // 
            this.lblDepotPath.Location = new System.Drawing.Point(8, 186);
            this.lblDepotPath.Name = "lblDepotPath";
            this.lblDepotPath.Size = new System.Drawing.Size(104, 16);
            this.lblDepotPath.TabIndex = 12;
            this.lblDepotPath.Text = "Depot Root";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "Password";
            // 
            // m_password
            // 
            this.m_password.Font = new System.Drawing.Font("Wingdings", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.m_password.Location = new System.Drawing.Point(118, 44);
            this.m_password.Name = "m_password";
            this.m_password.PasswordChar = 'l';
            this.m_password.Size = new System.Drawing.Size(242, 20);
            this.m_password.TabIndex = 3;
            this.m_password.Validating += new System.ComponentModel.CancelEventHandler(this.Password_Validating);
            // 
            // m_userName
            // 
            this.m_userName.Location = new System.Drawing.Point(118, 20);
            this.m_userName.Name = "m_userName";
            this.m_userName.Size = new System.Drawing.Size(242, 20);
            this.m_userName.TabIndex = 1;
            this.m_userName.Validating += new System.ComponentModel.CancelEventHandler(this.UserName_Validating);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "User name";
            // 
            // m_projectSettings
            // 
            this.m_projectSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_projectSettings.Location = new System.Drawing.Point(118, 156);
            this.m_projectSettings.Name = "m_projectSettings";
            this.m_projectSettings.Size = new System.Drawing.Size(242, 20);
            this.m_projectSettings.TabIndex = 11;
            this.m_projectSettings.Validating += new System.ComponentModel.CancelEventHandler(this.ProjectSettings_Validating);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "Project Settings";
            // 
            // m_assetProject
            // 
            this.m_assetProject.Location = new System.Drawing.Point(118, 132);
            this.m_assetProject.Name = "m_assetProject";
            this.m_assetProject.Size = new System.Drawing.Size(242, 20);
            this.m_assetProject.TabIndex = 9;
            this.m_assetProject.Validating += new System.ComponentModel.CancelEventHandler(this.AssetProject_Validate);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Project/Workspace";
            // 
            // m_serverName
            // 
            this.m_serverName.Location = new System.Drawing.Point(118, 108);
            this.m_serverName.Name = "m_serverName";
            this.m_serverName.Size = new System.Drawing.Size(242, 20);
            this.m_serverName.TabIndex = 7;
            this.m_serverName.Validating += new System.ComponentModel.CancelEventHandler(this.ServerName_Validating);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Server";
            // 
            // m_assetManagerType
            // 
            this.m_assetManagerType.Items.AddRange(new object[] {
            "Perforce",
            "Local"});
            this.m_assetManagerType.Location = new System.Drawing.Point(118, 83);
            this.m_assetManagerType.Name = "m_assetManagerType";
            this.m_assetManagerType.Size = new System.Drawing.Size(242, 21);
            this.m_assetManagerType.TabIndex = 5;
            this.m_assetManagerType.Text = "Perforce";
            this.m_assetManagerType.Validating += new System.ComponentModel.CancelEventHandler(this.AssetManagerType_Validating);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Asset Manager";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(221, 555);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.m_grabPS4);
            this.groupBox2.Controls.Add(this.m_grabx64XMA);
            this.groupBox2.Controls.Add(this.chkGrabPS3);
            this.groupBox2.Controls.Add(this.m_grabPS3);
            this.groupBox2.Controls.Add(this.chkGrabX360);
            this.groupBox2.Controls.Add(this.m_grabX360);
            this.groupBox2.Controls.Add(this.chkGrabPC);
            this.groupBox2.Controls.Add(this.m_grabPC);
            this.groupBox2.Controls.Add(this.chkGrabx64XMA);
            this.groupBox2.Controls.Add(this.chkGrabPS4);
            this.groupBox2.Controls.Add(this.m_waveEditorPath);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(8, 335);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 205);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "RAVE Settings";
            // 
            // m_grabPS4
            // 
            this.m_grabPS4.AutoSize = true;
            this.m_grabPS4.Location = new System.Drawing.Point(8, 142);
            this.m_grabPS4.Name = "m_grabPS4";
            this.m_grabPS4.Size = new System.Drawing.Size(146, 13);
            this.m_grabPS4.TabIndex = 21;
            this.m_grabPS4.Text = "Get Latest on Startup for PS4";
            // 
            // m_grabx64XMA
            // 
            this.m_grabx64XMA.AutoSize = true;
            this.m_grabx64XMA.Location = new System.Drawing.Point(4, 112);
            this.m_grabx64XMA.Name = "m_grabx64XMA";
            this.m_grabx64XMA.Size = new System.Drawing.Size(224, 13);
            this.m_grabx64XMA.TabIndex = 20;
            this.m_grabx64XMA.Text = "Get Latest on Startup for Durango/XBOXONE";
            // 
            // chkGrabPS3
            // 
            this.chkGrabPS3.AutoSize = true;
            this.chkGrabPS3.Location = new System.Drawing.Point(239, 27);
            this.chkGrabPS3.Name = "chkGrabPS3";
            this.chkGrabPS3.Size = new System.Drawing.Size(15, 14);
            this.chkGrabPS3.TabIndex = 16;
            this.chkGrabPS3.UseVisualStyleBackColor = true;
            this.chkGrabPS3.Validating += new System.ComponentModel.CancelEventHandler(this.chkGrabPS3_Validating);
            // 
            // m_grabPS3
            // 
            this.m_grabPS3.AutoSize = true;
            this.m_grabPS3.Location = new System.Drawing.Point(10, 27);
            this.m_grabPS3.Name = "m_grabPS3";
            this.m_grabPS3.Size = new System.Drawing.Size(146, 13);
            this.m_grabPS3.TabIndex = 17;
            this.m_grabPS3.Text = "Get Latest on Startup for PS3";
            // 
            // chkGrabX360
            // 
            this.chkGrabX360.AutoSize = true;
            this.chkGrabX360.Location = new System.Drawing.Point(239, 56);
            this.chkGrabX360.Name = "chkGrabX360";
            this.chkGrabX360.Size = new System.Drawing.Size(15, 14);
            this.chkGrabX360.TabIndex = 17;
            this.chkGrabX360.UseVisualStyleBackColor = true;
            this.chkGrabX360.Validating += new System.ComponentModel.CancelEventHandler(this.chkGrabX360_Validating);
            // 
            // m_grabX360
            // 
            this.m_grabX360.AutoSize = true;
            this.m_grabX360.Location = new System.Drawing.Point(10, 56);
            this.m_grabX360.Name = "m_grabX360";
            this.m_grabX360.Size = new System.Drawing.Size(171, 13);
            this.m_grabX360.TabIndex = 18;
            this.m_grabX360.Text = "Get Latest on Startup for Xbox 360";
            // 
            // chkGrabPC
            // 
            this.chkGrabPC.AutoSize = true;
            this.chkGrabPC.Location = new System.Drawing.Point(239, 83);
            this.chkGrabPC.Name = "chkGrabPC";
            this.chkGrabPC.Size = new System.Drawing.Size(15, 14);
            this.chkGrabPC.TabIndex = 18;
            this.chkGrabPC.UseVisualStyleBackColor = true;
            this.chkGrabPC.Validating += new System.ComponentModel.CancelEventHandler(this.chkGrabPC_Validating);
            // 
            // m_grabPC
            // 
            this.m_grabPC.AutoSize = true;
            this.m_grabPC.Location = new System.Drawing.Point(8, 84);
            this.m_grabPC.Name = "m_grabPC";
            this.m_grabPC.Size = new System.Drawing.Size(140, 13);
            this.m_grabPC.TabIndex = 19;
            this.m_grabPC.Text = "Get Latest on Startup for PC";
            // 
            // chkGrabx64XMA
            // 
            this.chkGrabx64XMA.AutoSize = true;
            this.chkGrabx64XMA.Location = new System.Drawing.Point(239, 112);
            this.chkGrabx64XMA.Name = "chkGrabx64XMA";
            this.chkGrabx64XMA.Size = new System.Drawing.Size(15, 14);
            this.chkGrabx64XMA.TabIndex = 18;
            this.chkGrabx64XMA.UseVisualStyleBackColor = true;
            this.chkGrabx64XMA.Validating += new System.ComponentModel.CancelEventHandler(this.chkGrabx64XMA_Validating);
            // 
            // chkGrabPS4
            // 
            this.chkGrabPS4.AutoSize = true;
            this.chkGrabPS4.Location = new System.Drawing.Point(239, 141);
            this.chkGrabPS4.Name = "chkGrabPS4";
            this.chkGrabPS4.Size = new System.Drawing.Size(15, 14);
            this.chkGrabPS4.TabIndex = 18;
            this.chkGrabPS4.UseVisualStyleBackColor = true;
            this.chkGrabPS4.Validating += new System.ComponentModel.CancelEventHandler(this.chkGrabPS4_Validating);
            // 
            // m_waveEditorPath
            // 
            this.m_waveEditorPath.Location = new System.Drawing.Point(118, 174);
            this.m_waveEditorPath.Name = "m_waveEditorPath";
            this.m_waveEditorPath.Size = new System.Drawing.Size(242, 20);
            this.m_waveEditorPath.TabIndex = 1;
            this.m_waveEditorPath.Validating += new System.ComponentModel.CancelEventHandler(this.WaveEditor_Validating);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 177);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Wave Editor";
            // 
            // m_project
            // 
            this.m_project.FormattingEnabled = true;
            this.m_project.Location = new System.Drawing.Point(62, 12);
            this.m_project.Name = "m_project";
            this.m_project.Size = new System.Drawing.Size(257, 21);
            this.m_project.TabIndex = 4;
            this.m_project.SelectedIndexChanged += new System.EventHandler(this.Project_Changed);
            this.m_project.Validating += new System.ComponentModel.CancelEventHandler(this.ProjectValidating);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Project";
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(325, 10);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(51, 23);
            this.btnNew.TabIndex = 6;
            this.btnNew.Text = "New..";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(302, 555);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmProjectLoader
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(386, 591);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.m_project);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProjectLoader";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Project Preferences";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

    }
}