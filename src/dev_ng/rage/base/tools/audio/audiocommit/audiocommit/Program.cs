using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace audiocommit
{
    class audCommitLogger
    {
        static int Main(string[] args)
        {

            if(args.Length == 4)
            {
                string filePath = args[0];
                string client = args[1];
                string host = args[2];
                string changelist = args[3];

                try
                {
                    StreamWriter sw = new StreamWriter(filePath + @"\" + changelist);
                    sw.Write("{0}:{1}", client, host);
                    sw.Close();
                }
                catch(Exception ex)
                {
                    Console.Write(ex.Message);
                }
            }

            // must return 0 otherwise the commit will fail
            return 0;
        }
    }
}
