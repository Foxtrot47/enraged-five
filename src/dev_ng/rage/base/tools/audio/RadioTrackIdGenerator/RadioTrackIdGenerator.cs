﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using rage.Generator;

namespace rage
{
    struct TextMarker
    {
        public uint trackId;
        public uint position;
    }

    public class RadioTrackIdGenerator : IGenerator
    {

        #region IGenerator Members
        const string ARG_NO_TEXT = "notext";
        const string ARG_NO_BEATS = "nobeats";

        bool m_DisableTextIds;
        bool m_DisableBeats;

        public bool Init(rage.ToolLib.Logging.ILog log, string workingPath, rage.audProjectSettings projectSettings, Types.ITypeDefinitionsManager typeDefManager, params string[] args)
        {
            m_DisableBeats = false;
            m_DisableTextIds = false;

            foreach(var s in args)
            {
                if (s.Equals(ARG_NO_TEXT))
                {
                    m_DisableTextIds = true;
                }
                else if(s.Equals(ARG_NO_BEATS))
                {
                    m_DisableBeats = true;
                }
            }

            return true;
        }

        public System.Xml.Linq.XDocument Generate(rage.ToolLib.Logging.ILog log, System.Xml.Linq.XDocument inputDoc)
        {         
            var outputDoc = new XDocument();
            var objsElem = new XElement("Objects");
            outputDoc.Add(objsElem);

            if (!m_DisableTextIds)
            {
                GenerateTrackIds(objsElem, inputDoc);
            }
            if (!m_DisableBeats)
            {
                GenerateBeatMarkers(objsElem, inputDoc);
            }
                        
            return outputDoc;
        }

        void GenerateTrackIds(XElement objsElem, XDocument inputDoc)
        {
            // look for any waves containing TRACKID:X markers
            // build a list of markers in a bank

            Dictionary<string, List<TextMarker>> bankMarkers = new Dictionary<string, List<TextMarker>>();
            foreach (var n in inputDoc.Descendants("Marker"))
            {
                if (n.Value.StartsWith("TRACKID:"))
                {
                    TextMarker tm = new TextMarker();
                    tm.position = uint.Parse(n.Attribute("timeMs").Value);
                    tm.trackId = uint.Parse(n.Value.Split(':')[1]);

                    var bank = n.Ancestors("Bank").First();
                    string actualBankName = bank.Attribute("name").Value;

                    var pack = n.Ancestors("Pack").First();
                    string packName = pack.Attribute("name").Value;

                    string bankName = packName + "_" + actualBankName;

                    if (!bankMarkers.ContainsKey(bankName))
                    {
                        bankMarkers.Add(bankName, new List<TextMarker>());
                    }
                    bankMarkers[bankName].Add(tm);
                }
            }

            foreach (var key in bankMarkers.Keys)
            {
                ToolLib.Hash h = new ToolLib.Hash();
                h.Value = key;

                string newObjName = string.Format("RTT_{0:X8}", h.Key);
                var trackElem = new XElement("RadioTrackTextIds", new XAttribute("name", newObjName), new XAttribute("folder", key));
                objsElem.Add(trackElem);

                foreach (var m in bankMarkers[key].OrderBy(m=>m.position))
                {
                    var markerElem = new XElement("TextId",
                                                    new XElement("OffsetMs", m.position),
                                                    new XElement("TextId", m.trackId));
                    trackElem.Add(markerElem);
                }
            }
        }


        void GenerateBeatMarkers(XElement objsElem, XDocument inputDoc)
        {
            // look for any waves containing BEAT:X markers
            // build a list of markers in a bank

            Dictionary<string, List<TextMarker>> bankMarkers = new Dictionary<string, List<TextMarker>>();
            foreach (var n in inputDoc.Descendants("Marker"))
            {
                if (n.Value.StartsWith("BEAT:"))
                {
                    var bank = n.Ancestors("Bank").First();
                    string actualBankName = bank.Attribute("name").Value;

                    var pack = n.Ancestors("Pack").First();
                    string packName = pack.Attribute("name").Value;

                    string bankName = packName + "_" + actualBankName;

                    if (n.Value.Length <= 5)
                    {
                        objsElem.Add(new XElement("Warning", string.Format("Invalid beat marker: {0} ({1})", bankName, n.Value)));
                    }
                    else
                    {
                        TextMarker tm = new TextMarker();
                        tm.position = uint.Parse(n.Attribute("timeMs").Value);
                        tm.trackId = uint.Parse(n.Value.Split(':')[1]);

                        if (!bankMarkers.ContainsKey(bankName))
                        {
                            bankMarkers.Add(bankName, new List<TextMarker>());
                        }
                        bankMarkers[bankName].Add(tm);
                    }
                }
            }

            foreach (var key in bankMarkers.Keys)
            {
                ToolLib.Hash h = new ToolLib.Hash();
                h.Value = key;

                string newObjName = string.Format("RTB_{0:X8}", h.Key);
                var trackElem = new XElement("RadioTrackTextIds", new XAttribute("name", newObjName), new XAttribute("folder", key));
                objsElem.Add(trackElem);
                
                uint lastPosition = uint.MaxValue;
                uint lastTrackId = 0;

                foreach (var m in bankMarkers[key].OrderBy(m=>m.position))
                {
                    if (m.trackId != lastTrackId || m.position != lastPosition)
                    {
                        var markerElem = new XElement("TextId",
                                                    new XElement("OffsetMs", m.position),
                                                    new XElement("TextId", m.trackId));
                        trackElem.Add(markerElem);

                        lastPosition = m.position;
                        lastTrackId = m.trackId;                        
                    }
                }
            }
        }

        public void Shutdown()
        {
            
        }

        #endregion
    }
}
