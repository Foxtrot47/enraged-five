//
// SimpleFieldDef.cpp
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#include "ErrorManager.h"
#include "Enum.h"
#include "FieldDef.h"
#include "SimpleFieldDef.h"
#include "SoundDef.h"

namespace rage
{
CSimpleFieldDef::CSimpleFieldDef(void)
{
}

CSimpleFieldDef *CSimpleFieldDef::ParseSimpleFieldDef(XmlNode * node)
{
	CSimpleFieldDef *def = new CSimpleFieldDef();
	if(node->Attributes && node->Attributes->get_ItemOf(S"length"))
	{
		def->m_Length = Int32::Parse(node->Attributes->get_ItemOf("length")->Value);
	}
	else
	{
		def->m_Length = 0;
	}
	return def;
}

bool CSimpleFieldDef::Serialise(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *compiler)
{
	String *val = GetValue(node);

	if(ShouldResolveObjectReference())
	{
		if(val != NULL)
		{
			compiler->RecordObjectReference(output->BaseStream->Position);
			WriteConvertedValue(val, FIELD_TYPE_HASH, output, 0, objectDef->GetIsPacked(), compiler->IsOutputBigEndian());
		}
		else
		{
			// write an invalid reference
			output->Write(~0U);
		}
	
	}
	else if(m_type == FIELD_TYPE_ENUM)
	{
		CEnum *e = compiler->FindEnum(m_EnumName);	
		if(e == NULL)
		{
			ERRORMGR->HandleError("Invalid enum type specified");
			return false;
		}
		else
		{
			WriteConvertedValue(__box(e->GetEnumValue(val))->ToString(), FIELD_TYPE_INT8, output, 0, objectDef->GetIsPacked(), compiler->IsOutputBigEndian());
		}
	}
	else if(m_units == FIELD_UNIT_STRING_TABLE_INDEX)
	{
		if(m_type != FIELD_TYPE_UINT32)
		{
			ERRORMGR->HandleError("String table index fields must be of type u32");
			return false;
		}
		compiler->AddToStringTable(val);
		compiler->RecordStringTableReference(output->BaseStream->Position);
		// for now we write a hash of the string
		WriteConvertedValue(val, FIELD_TYPE_HASH, output, 0, objectDef->GetIsPacked(), compiler->IsOutputBigEndian());
	}
	else
	{
		if(m_type == FIELD_TYPE_FIXED_STRING)
		{
			if(val && val->Length + 1 > m_Length)
			{
				ERRORMGR->HandleError("Too many characters in C-string");
				return false;
			}
		}
		WriteConvertedValue(val, m_type, output, m_Length, objectDef->GetIsPacked(), compiler->IsOutputBigEndian());
	}
	
	return true;
}

String *CSimpleFieldDef::GetValue(XmlNode *node)
{
	if(IsDefaultValue(node))
	{
		return getDefaultValue();
	}
	else
	{
		return node->InnerText;
	}
}

bool CSimpleFieldDef::IsDefaultValue(XmlNode *node)
{
	if(node == NULL || node->InnerText == NULL || node->InnerText->Length == 0 || node->InnerText->Equals(getDefaultValue()))
	{
		return true;
	}
	return false;
}

bool CSimpleFieldDef::OutputStructure(TextWriter *output, int tabLevel)
{
	if(ShouldBeIgnored())
		return true;

	if(m_type == FIELD_TYPE_STRING)
	{
		// although a simple field strings need special handling - pascal format (one byte length followed by string characters)
		WriteTabs(output, tabLevel);
		output->Write("rage::u8 ");
		output->Write(getName());
		output->WriteLine("Len;");
		
		WriteTabs(output, tabLevel);
		output->Write("char ");
		output->Write(getName());
		output->WriteLine("[255];");
	}
	else
	{
		WriteTabs(output, tabLevel);
		output->Write(getTypeString(m_type));
		output->Write(" ");
		output->Write(getName());
		if(m_type == FIELD_TYPE_FIXED_STRING)
		{
			output->Write("[");
			output->Write(m_Length);
			output->Write("]");
		}
		output->WriteLine(";");
	}
	return true;
}

unsigned int CSimpleFieldDef::GetAlignment()
{
	unsigned int naturalAlignment = CFieldDef::GetAlignment(m_type);
	return naturalAlignment > m_Alignment ? naturalAlignment : m_Alignment;
}

}
