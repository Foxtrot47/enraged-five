//
// Enum.h
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#pragma once

#include "ErrorManager.h"

#using <mscorlib.dll>
#using <system.xml.dll>

using namespace System;
using namespace System::Xml;
using namespace System::Collections;
using namespace System::IO;

namespace rage
{

public __gc class CEnum
{

public:

	static CEnum *ParseEnum(XmlNode *node);

	CEnum();

	bool OutputStructure(TextWriter *output);
	bool OutputHelperFunctions(TextWriter *output);

	int GetEnumValue(String *valName)
	{
		for(int i = 0; i < m_NameList->Count; i++)
		{
			if(valName->Equals(dynamic_cast<String*>(m_NameList->get_Item(i))))
			{
				return i + GetInitialValue();
			}
		}
		ERRORMGR->HandleWarning(String::Concat("Invalid enum value: ", valName, " for enum ", GetName()));
		return -1;
	}

	String *GetName()
	{
		return m_Name;
	}

	int GetInitialValue()
	{
		return m_InitialValue;
	}

private:

	String *m_Name;
	ArrayList *m_NameList;
	ArrayList *m_DescriptionList;
	int m_InitialValue;
	bool m_BitsetStyle;

};

} // namespace rage
