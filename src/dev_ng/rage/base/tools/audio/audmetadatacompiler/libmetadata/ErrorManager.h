//
// ErrorManager.h
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#pragma once

#using <mscorlib.dll>

using namespace System;
using namespace System::Collections;
using namespace System::IO;

namespace rage
{
#define ERRORMGR CErrorManager::GetErrorManager()

public __value enum ContextType
{
	CONTEXT_TYPE_FILE,
	CONTEXT_TYPE_OBJECT,
	CONTEXT_TYPE_OBJECT_DEF,
	CONTEXT_TYPE_FIELD,
	CONTEXT_TYPE_APPLICATION
};

public __gc class ContextDescriptor
{
public:
	ContextDescriptor(ContextType type, String *name)
	{
		Type = type;
		Name = name;
	}

	String *ToString()
	{
		return String::Concat(GetTypeName(Type), ":", Name);
	}
	
	ContextType Type;
	String *Name;

private:
	static String *GetTypeName(ContextType type)
	{
		switch(type)
		{
		case CONTEXT_TYPE_FILE:
			return "file";
		case CONTEXT_TYPE_OBJECT:
			return "object";
		case CONTEXT_TYPE_OBJECT_DEF:
			return "object definition";
		case CONTEXT_TYPE_FIELD:
			return "field";
		case CONTEXT_TYPE_APPLICATION:
			return "application";
		}
		return "UNKNOWN_CONTEXT_TYPE";
	}

};

public __delegate void MessageHandler(String *msg, String *context);

public __gc class CErrorManager
{
public:

	// PURPOSE
	//	Initializes context, must be called before any of the HandleX functions
	// PARAMS
	//	isOutputXml - if true then all output is formatted as xml
	// NOTES
	//	Also outputs root node if output is xml
	static void InitClass(bool isOutputXml);

	// PURPOSE
	//	Closes root node when in xml output mode
	void ShutdownClass();

	// PURPOSE
	//	Outputs details of the supplied exception to the console output if bReportExceptions is true
	void HandleException(Exception *ex);
	// PURPOSE
	//	Outputs details of the supplied warning to the console output if bReportWarnings is true
	void HandleWarning(String *warning);
	// PURPOSE
	//	Outputs details of the supplied error to the console output if bReportErrors is true
	void HandleError(String *error);
	// PURPOSE
	//	Outputs the supplied info to the console output if bReportInfo is true
	void HandleInfo(String *info);

	// PURPOSE
	//	Outputs summary of warnings/errors
	void PrintSummary();

	int GetNumErrors()
	{
		return m_NumErrors;
	}

	int GetNumWarnings()
	{
		return m_NumWarnings;
	}

	int GetNumExceptions()
	{
		return m_NumExceptions;
	}

	void IncrementNumberOfObjectsCompiled()
	{
		m_NumObjects++;
	}

	// PURPOSE
	//	Returns the number of objects compiled
	int GetNumObjects()
	{
		return m_NumObjects;
	}

	// PURPOSE
	//	Set current context
	// PARAMS
	//	type - the type of context (file, object, field etc)
	//	name - the name of the context
	void PushContext(ContextType type, String *name);

	// PURPOSE
	//	Pop out of the current context (up one level in the heirarchy)
	void PopContext();

	// PURPOSE
	//	Returns a string containing the current context stack
	String *ComputeContextStackString();

	static CErrorManager *GetErrorManager()
	{
		return sm_Instance;
	}

	__event MessageHandler* OnErrorMessage;	
	__event MessageHandler* OnWarningMessage;
	__event MessageHandler* OnExceptionMessage;
	__event MessageHandler* OnInformationMessage;

	void RecordObjectSize(String *objectName, unsigned int size);
private:

	ArrayList *m_ContextStack;
	bool m_IsOutputXml;
	int m_NumErrors, m_NumWarnings, m_NumObjects, m_NumExceptions;
	unsigned int m_AverageObjectSize, m_AverageCount;
	unsigned int m_LargestObjectSize;
	String* m_LargestObjectName;

	static CErrorManager *sm_Instance;
};
}
