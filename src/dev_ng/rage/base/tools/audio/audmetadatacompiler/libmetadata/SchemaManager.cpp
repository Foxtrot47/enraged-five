//
// SchemaManager.cpp
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#include "CompositeFieldDef.h"
#include "Enum.h"
#include "ErrorManager.h"
#include "SchemaManager.h"
#include "SoundDef.h"

namespace rage
{
CSchemaManager::CSchemaManager(String *classFactoryFunctionName, String *classFactoryReturnType, bool usesBucketAllocator)
{
	m_NextClassId = 0;
	m_ObjectDefinitions = new Hashtable();
	m_Enums = new Hashtable();
	m_ExtComposites = new Hashtable();
	m_ObjectDefLookup = new ArrayList();
	m_EnumLookup = new ArrayList();
	m_ExtCompositeLookup = new ArrayList();
	m_IncludePaths = new ArrayList();

	m_ClassFactoryFunctionName = classFactoryFunctionName;
	m_ClassFactoryReturnType = classFactoryReturnType;
	if(m_ClassFactoryReturnType==NULL || m_ClassFactoryFunctionName==NULL)
	{
		m_GenerateFactoryCode = false;
	}
	else
	{
		m_GenerateFactoryCode = true;
	}

	m_DefaultPacked = m_PackingCurrentlyEnabled = true;

	m_SchemaVersion = ~0U;
	m_UsesBucketAllocator = usesBucketAllocator;
}

CSchemaManager::~CSchemaManager(void)
{
}

bool CSchemaManager::LoadObjectDefinitions(String *strFile, String *classPrefix, String *classIncludePath)
{
	XmlDocument *xmlDoc = NULL;

	ERRORMGR->PushContext(CONTEXT_TYPE_FILE, strFile);

	try
	{
		xmlDoc = new XmlDocument();
		xmlDoc->Load(strFile);
	}
	catch(Exception *ex)
	{
		ERRORMGR->HandleException(ex);
		ERRORMGR->PopContext();
		return false;
	}

	if(xmlDoc->DocumentElement->get_Name()->CompareTo(S"TypeDefinitions") != 0)
	{
		ERRORMGR->HandleError(S"Invalid metadata type definition xml file (missing <TypeDefinitions> root tag)");
		ERRORMGR->PopContext();
		return false;
	}
	ERRORMGR->HandleInfo(String::Concat(S"Loading type definitions from ", strFile));

	if(xmlDoc->DocumentElement->Attributes->get_ItemOf("version"))
	{
		m_SchemaVersion = UInt32::Parse(xmlDoc->DocumentElement->Attributes->get_ItemOf("version")->InnerText);
		ERRORMGR->HandleInfo(String::Format(S"Schema version: {0}", __box(m_SchemaVersion)));
	}

	if(xmlDoc->DocumentElement->Attributes->get_ItemOf("DefaultPacked"))
	{
		m_DefaultPacked = m_PackingCurrentlyEnabled = xmlDoc->DocumentElement->Attributes->get_ItemOf("DefaultPacked")->Value->Equals("yes");
	}

	bool bResult = true;

	for(int i = 0; i < xmlDoc->DocumentElement->ChildNodes->Count; i++)
	{
		XmlNode *node = xmlDoc->DocumentElement->ChildNodes->Item(i);
		if(node->Name->Equals(S"TypeDefinition"))
		{
			// parse this object type definition
			try
			{
				CObjectDef *baseClass = NULL;
				if(node->Attributes != NULL && node->Attributes->get_ItemOf("inheritsFrom") != NULL)
				{
					Object *objBaseClass = m_ObjectDefinitions->get_Item(node->Attributes->get_ItemOf("inheritsFrom")->Value);	
					if(objBaseClass != NULL)
					{
						baseClass = dynamic_cast<CObjectDef*>(objBaseClass);
					}
					else
					{
						ERRORMGR->HandleWarning("Couldn't find base class definition - ensure file order is valid");
					}
				}
				
				CObjectDef *def = CObjectDef::ParseObjectDef(node, baseClass, m_DefaultPacked);
				def->setClassPrefix(classPrefix);
				def->setClassIncludePath(classIncludePath);
				def->setClassID(m_NextClassId++);
				m_ObjectDefinitions->Add(def->getName(), def);
				m_ObjectDefLookup->Add(def->getName());

				ERRORMGR->HandleInfo(String::Concat(S"Added object type definition ", def->getName()));
			}
			catch(Exception *ex)
			{
				ERRORMGR->HandleException(ex);
				ERRORMGR->PopContext();
				bResult = false;
			}
		}
		else if(node->Name->Equals(S"Enum"))
		{
			CEnum *e = CEnum::ParseEnum(node);
			m_Enums->Add(e->GetName(), e);
			m_EnumLookup->Add(e->GetName());
		}
		else if(node->Name->Equals(S"CompositeField"))
		{
			CFieldDef *def = CFieldDef::ParseFieldDef(node);
			m_ExtComposites->Add(def->getName(), def);
			m_ExtCompositeLookup->Add(def->getName());
		}
		else if (node->Name->Equals(S"Includes"))
		{
			for(int iSubNode = 0; iSubNode < node->ChildNodes->Count; iSubNode++)
			{
				XmlNode* pSubNode = node->ChildNodes->Item(iSubNode);
				if (pSubNode->Name->Equals(S"include"))
					m_IncludePaths->Add(pSubNode->InnerText);
			}
		}
	}

	ERRORMGR->PopContext();
	return bResult;
}

CObjectDef *CSchemaManager::GetObjectDefinition(String *defName)
{
	if(m_ObjectDefinitions != NULL && m_ObjectDefinitions->ContainsKey(defName))
	{
		return dynamic_cast<CObjectDef*>(m_ObjectDefinitions->get_Item(defName));
	}
	ERRORMGR->HandleError(String::Concat(S"Couldn't find object definition: ", defName));
	return NULL;
}

bool CSchemaManager::OutputObjectDefs(TextWriter *output)
{
	for(int i = 0; i < m_ObjectDefLookup->Count; i++)
	{
		CObjectDef* object = dynamic_cast<CObjectDef*>(m_ObjectDefinitions->get_Item(m_ObjectDefLookup->get_Item(i)));
		if (object->GetIsPacked() && !m_PackingCurrentlyEnabled)
		{
			m_PackingCurrentlyEnabled = true;

			output->WriteLine("// disable struct alignment");
			output->WriteLine("#if !__SPU");
			output->WriteLine("#pragma pack(push, r1, 1)");
			output->WriteLine("#endif // !__SPU");
		}
		else if (!object->GetIsPacked() && m_PackingCurrentlyEnabled)
		{
			m_PackingCurrentlyEnabled = false;

			output->WriteLine("// re - enable struct alignment");
			output->WriteLine("#if !__SPU");
			output->WriteLine("#pragma pack(pop, r1)");
			output->WriteLine("#endif // __SPU");
		}
 
		object->OutputStructure(output);
	}
	return true;
}

bool CSchemaManager::OutputHelperFunctions(TextWriter *output)
{
	for(int i = 0; i < m_ObjectDefLookup->Count; i++)
	{
		dynamic_cast<CObjectDef*>(m_ObjectDefinitions->get_Item(m_ObjectDefLookup->get_Item(i)))->OutputHelperFunctions(output);
	}

	for(int i = 0; i < m_ExtCompositeLookup->Count; i++)
	{
		CCompositeFieldDef* def = dynamic_cast<CCompositeFieldDef*>(m_ExtComposites->get_Item(m_ExtCompositeLookup->get_Item(i)));
		if (!def->GetIsNativeType())
			def->OutputHelperFunctions(output);
	}

	for(int i = 0; i < m_Enums->Count; i++)
	{
		dynamic_cast<CEnum*>(m_Enums->get_Item(m_EnumLookup->get_Item(i)))->OutputHelperFunctions(output);
	}
	return true;
}

bool CSchemaManager::OutputBaseTypeLookups(TextWriter *output, String *metadataType, bool isDefinition)
{
	// base type table
	output->WriteLine("// PURPOSE");
	output->WriteLine("//	Gets the parent class type for the passed in class type");
	output->Write("rage::u32 g");
	output->Write(metadataType);
	output->Write("GetBaseTypeId(const rage::u32 classId)");
	if (isDefinition)
	{
		output->WriteLine();
		output->WriteLine("{");

		output->WriteLine("\tswitch(classId)");
		output->WriteLine("\t{");

		for(int i = 0; i < m_ObjectDefLookup->Count; i++)
		{
			CObjectDef *def = dynamic_cast<CObjectDef*>(m_ObjectDefinitions->get_Item(m_ObjectDefLookup->get_Item(i)));

			output->Write("\t\tcase ");
			output->Write(def->getName());
			output->Write("::TYPE_ID: return ");
			output->Write(def->getName());
			output->WriteLine("::BASE_TYPE_ID;");
		}

		output->WriteLine("\t\tdefault: return ~0U;");
		output->WriteLine("\t}");
		output->WriteLine("}");
	}
	else
		output->Write(";");

	output->WriteLine();
	return true;
}

bool CSchemaManager::OutputEnumDefs(TextWriter *output)
{
	for(int i = 0; i < m_EnumLookup->Count; i++)
	{
		dynamic_cast<CEnum*>(m_Enums->get_Item(m_EnumLookup->get_Item(i)))->OutputStructure(output);
	}
	return true;
}

bool CSchemaManager::OutputExtCompositeFields(TextWriter *output)
{
	for(int i = 0; i < m_ExtCompositeLookup->Count; i++)
	{
		CCompositeFieldDef* def = dynamic_cast<CCompositeFieldDef*>(m_ExtComposites->get_Item(m_ExtCompositeLookup->get_Item(i)));
		if (!def->GetIsNativeType())
			def->OutputStructure(output, 0);
	}
	return true;
}

bool CSchemaManager::OutputObjectDefIDs(TextWriter *output, String *fileName)
{
	/*for(int i = 0; i < m_ObjectDefLookup->Count; i++)
	{
		 CObjectDef *def = dynamic_cast<CObjectDef*>(m_ObjectDefinitions->get_Item(m_ObjectDefLookup->get_Item(i)));
		 
		 output->Write("#define AUD_OBJ_TYPE_");
		 output->Write(def->getName()->ToUpper());
		 output->Write(" ");
		 output->WriteLine(def->getClassID());
	}*/

	output->WriteLine("// NOTE: doesn't include base object");
	output->Write("#define AUD_NUM_");
	output->Write(fileName->ToUpper());
	output->Write(" ");
	output->WriteLine(m_ObjectDefLookup->Count-1);
	return true;
}

bool CSchemaManager::OutputClassFactoryFunction(TextWriter *output, String *namespaceName)
{
	if(m_GenerateFactoryCode)
	{
		// generate #include's
		for(int i = 0; i < m_ObjectDefLookup->Count; i++)
		{
			CObjectDef *def = dynamic_cast<CObjectDef*>(m_ObjectDefinitions->get_Item(m_ObjectDefLookup->get_Item(i)));

			// include abstract headers
			output->Write("#include \"");
			output->Write(def->getClassIncludePath());
			output->Write("/");
			output->Write(def->getName());
			output->WriteLine(".h\"");
		}

		output->Write("namespace ");
		output->WriteLine(namespaceName);
		output->WriteLine("{");
			
		output->Write(m_ClassFactoryReturnType);
		output->Write(m_ClassFactoryFunctionName);
		if(m_UsesBucketAllocator)
		{
			output->WriteLine("(rage::u32 bucketId, rage::u32 classId)");
		}
		else
		{
			output->WriteLine("(rage::u32 classId)");
		}
		output->WriteLine("{");

		output->Write("\t");
		output->Write(m_ClassFactoryReturnType);
		output->WriteLine("obj;");

		output->WriteLine("\tswitch(classId)");
		output->WriteLine("\t{");

		for(int i = 0; i < m_ObjectDefLookup->Count; i++)
		{
			CObjectDef *def = dynamic_cast<CObjectDef*>(m_ObjectDefinitions->get_Item(m_ObjectDefLookup->get_Item(i)));
			 
			if(!def->getIsAbstract())
			{
				output->Write("\t\tcase ");
				output->Write(def->getName());
				output->WriteLine("::TYPE_ID:");

				if(m_UsesBucketAllocator)
				{
					output->Write("\t\t\tobj = (");
					output->Write(m_ClassFactoryReturnType);
					output->Write(")audSound::GetStaticPool().AllocateSoundSlot(sizeof(");
					output->Write(def->getClassPrefix());
					output->Write(def->getName());
					output->WriteLine("), bucketId);");
					output->WriteLine("\t\t\tif(obj)");
					output->WriteLine("\t\t\t{");
					output->Write("\t\t\t\t::new(obj) ");
					output->Write(def->getClassPrefix());
					output->Write(def->getName());
					output->WriteLine("();");
					output->WriteLine("\t\t\t}");
				}
				else
				{
					output->Write("\t\t\tobj = rage_new ");
					output->Write(def->getClassPrefix());
					output->Write(def->getName());
					output->WriteLine("();");
				}
				
				output->WriteLine("\t\t\tbreak;");
			}
		}

		output->WriteLine("\t\tdefault: obj = NULL;");
		output->WriteLine("\t}");
		output->WriteLine("\treturn obj;");
		output->WriteLine("}");	

		OutputClassMaxSizeFunction(output);
		
		output->WriteLine();
		output->WriteLine();

		// object name table
		output->Write("const char *");
		output->Write(m_ClassFactoryFunctionName);
		output->WriteLine("GetTypeName(const rage::u32 classId)");
		output->WriteLine("{");

		output->WriteLine("\tswitch(classId)");
		output->WriteLine("\t{");

		for(int i = 0; i < m_ObjectDefLookup->Count; i++)
		{
			CObjectDef *def = dynamic_cast<CObjectDef*>(m_ObjectDefinitions->get_Item(m_ObjectDefLookup->get_Item(i)));

			output->Write("\t\tcase ");
			output->Write(def->getName());
			output->Write("::TYPE_ID: return \"");
			output->Write(def->getClassPrefix());
			output->Write(def->getName());
			output->WriteLine("\";");
		}

		output->WriteLine("\t\tdefault: return NULL;");
		output->WriteLine("\t}");
		output->WriteLine("}");	
		output->WriteLine();

		// base type table
		output->Write("rage::u32 ");
		output->Write(m_ClassFactoryFunctionName);
		output->WriteLine("GetBaseTypeId(const rage::u32 classId)");
		output->WriteLine("{");

		output->WriteLine("\tswitch(classId)");
		output->WriteLine("\t{");

		for(int i = 0; i < m_ObjectDefLookup->Count; i++)
		{
			CObjectDef *def = dynamic_cast<CObjectDef*>(m_ObjectDefinitions->get_Item(m_ObjectDefLookup->get_Item(i)));

			output->Write("\t\tcase ");
			output->Write(def->getName());
			output->Write("::TYPE_ID: return ");
			output->Write(def->getName());
			output->WriteLine("::BASE_TYPE_ID;");
		}

		output->WriteLine("\t\tdefault: return ~0U;");
		output->WriteLine("\t}");
		output->WriteLine("}");
		output->WriteLine();

		output->Write("bool ");
		output->Write(m_ClassFactoryFunctionName);
		output->WriteLine("IsOfType(const rage::u32 objectTypeId, const rage::u32 baseTypeId)");
		output->WriteLine("{");
		output->WriteLine("\tif(objectTypeId == ~0U)");
		output->WriteLine("\t{");
		output->WriteLine("\t\treturn false;");
		output->WriteLine("\t}");
		output->WriteLine("\telse if(objectTypeId == baseTypeId)");
		output->WriteLine("\t{");
		output->WriteLine("\t\treturn true;");
		output->WriteLine("\t}");
		output->WriteLine("\telse");
		output->WriteLine("\t{");
		output->Write("\t\treturn ");
		output->Write(m_ClassFactoryFunctionName);
		output->Write("IsOfType(");
		output->Write(m_ClassFactoryFunctionName);
		output->Write("GetBaseTypeId(objectTypeId), ");
		output->WriteLine("baseTypeId);");
		output->WriteLine("\t}");
		output->WriteLine("}");
		output->WriteLine();

		output->Write("template<class _T> bool ");
		output->Write(m_ClassFactoryFunctionName);
		output->WriteLine("IsOfType(_T *obj, const rage::u32 baseTypeId)");
		output->WriteLine("{");
		output->Write("\treturn ");
		output->Write(m_ClassFactoryFunctionName);
		output->WriteLine("IsOfType(obj->ClassID, baseTypeId);");
		output->WriteLine("}");
		output->WriteLine();

		output->Write("} // namespace ");
		output->WriteLine(namespaceName);
	}

	return true;
}

bool CSchemaManager::OutputClassMaxSizeFunction(TextWriter *output)
{
	output->WriteLine();
	output->Write("rage::u32 ");
	output->Write(m_ClassFactoryFunctionName);
	output->WriteLine("MaxSize()");
	output->WriteLine("{");

	output->Write("\t");

	output->WriteLine("rage::u32 size = 0;");
	output->WriteLine("char *largestObjectType = NULL;");

	for(int i = 0; i < m_ObjectDefLookup->Count; i++)
	{
		CObjectDef *def = dynamic_cast<CObjectDef*>(m_ObjectDefinitions->get_Item(m_ObjectDefLookup->get_Item(i)));
		// output size even if abstract - useful for cases like audEnvironmentSound		
		output->Write("\tif(sizeof(");
		output->Write(def->getClassPrefix());
		output->Write(def->getName());
		output->WriteLine(") > size)");
		output->WriteLine("\t{");
		output->Write("\t\tsize = sizeof(");
		output->Write(def->getClassPrefix());
		output->Write(def->getName());
		output->WriteLine(");");
		output->Write("\t\tlargestObjectType = \"");
		output->Write(def->getClassPrefix());
		output->Write(def->getName());
		output->WriteLine("\";");
		output->WriteLine("\t}");
	}

	output->WriteLine("\tDisplayf(\"Largest object type: %s (%u bytes)\", largestObjectType, size);");
	output->WriteLine("\treturn size;");
	output->WriteLine("}");	
	return true;
}

bool CSchemaManager::OutputGetAddressOfMacro(TextWriter *output, String *fileName)
{
	if(m_GenerateFactoryCode)
	{
		output->Write("#define AUD_DECLARE_");
		output->Write(fileName->ToUpper());
		output->WriteLine("_FUNCTION(fn)	 void *GetAddressOf_##fn(rage::u8 classId);");
		output->WriteLine();

		output->Write("#define AUD_DEFINE_");
		output->Write(fileName->ToUpper());
		output->WriteLine("_FUNCTION(fn)									\\");
		output->WriteLine("void *GetAddressOf_##fn(rage::u8 classId)										\\");
		output->WriteLine("{																		\\");
		output->WriteLine("\tswitch(classId)														\\");
		output->WriteLine("\t{																	\\");

		for(int i = 1; i < m_ObjectDefLookup->Count; i++)
		{
			CObjectDef *def = dynamic_cast<CObjectDef*>(m_ObjectDefinitions->get_Item(m_ObjectDefLookup->get_Item(i)));
			// output even if abstract	 
			output->Write("\t\tcase ");
			output->Write(def->getName());
			output->Write("::TYPE_ID: return (void*)&");
			output->Write(def->getClassPrefix());
			output->Write(def->getName());
			output->WriteLine("::s##fn;\t\\");
			
		}

		output->WriteLine("\t\tdefault: return NULL;\t\\");
		output->WriteLine("\t}\t\\");	
		output->WriteLine("}\t");	
	}
	return true;
}

}
