#pragma once

#using <mscorlib.dll>
#using <system.xml.dll>

using namespace System;
using namespace System::Xml;
using namespace System::Xml::Xsl;
using namespace System::Collections;
using namespace System::IO;

#include "FieldTypes.h"
#include "SoundCompiler.h"

namespace rage
{
public __gc class CObjectDef : public Object
{
public:
	CObjectDef(void);
	~CObjectDef(void);

	static CObjectDef *ParseObjectDef(XmlNode *node, CObjectDef *baseClass, bool bDefaultPacked);
	static CObjectDef *ParseObjectDef(XmlNode *node, bool bDefaultPacked)
	{
		return CObjectDef::ParseObjectDef(node, NULL, bDefaultPacked);
	}

	void setClassID(Byte classId)
	{
		m_classID = classId;
	}

	void setClassIncludePath(String *classIncludePath)
	{
		m_ClassIncludePath = classIncludePath;
	}

	Byte getClassID()
	{
		return m_classID;
	}

	String *getName()
	{
		return m_name;
	}

	String *getClassIncludePath()
	{
		return m_ClassIncludePath;
	}

	bool SerialiseInstance(XmlNode *node, BinaryWriter *output, CObjectCompiler *cs);
	bool OutputStructure(TextWriter *output);
	bool OutputHelperFunctions(TextWriter *output);
	String *getInstanceName(XmlNode *node);
	String *getInstanceDescription(XmlNode *node);

	void setClassPrefix(String *prefix)
	{
		m_classPrefix = prefix;
	}

	bool getIsAbstract()
	{
		return m_IsAbstract;
	}

	bool getIsCompressed()
	{
		return m_IsCompressed;
	}

	String *getClassPrefix()
	{
		return m_classPrefix;
	}

	ArrayList *GetValidators()
	{
		return m_Validators;
	}

	CObjectDef *getBaseClass()
	{
		return m_BaseClass;
	}

	bool BeginCompilation(XmlNode *node, BinaryWriter *output, CObjectCompiler *cs);
	bool Compile(XmlNode *node, BinaryWriter *output, CObjectCompiler *cs);
	bool FinishCompilation(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *cs);

	void GenerateOverrideFlags(XmlNode *node, ArrayList *overrideFlags, ArrayList *flags);

	void OutputFlagEnum(TextWriter *output, String *className);

	unsigned int GetAlignment()
	{
		return m_Alignment;
	}

	bool GetIsPacked()
	{
		return m_IsPacked;
	}

	//	If we're not packed, searches for the largest alignment value of all data members including ancestors
	unsigned int GetLargestAlignment();

	ArrayList *GetFields() { return m_fields; }

private:

	unsigned int m_Alignment;
	bool m_IsPacked;
	bool m_IsAbstract;
	bool m_IsCompressed;

	CObjectDef *m_BaseClass;

	ArrayList *m_fields;
	Hashtable *m_attributes;
	Byte m_classID;
	String *m_name;
	String *m_classPrefix;
	String *m_ClassIncludePath;

	ArrayList *m_Validators;
};

}
