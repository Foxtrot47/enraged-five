//
// ErrorManager.cpp
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#include "ErrorManager.h"

namespace rage
{

void CErrorManager::InitClass(bool isOutputXml)
{

	sm_Instance = new CErrorManager();
	sm_Instance->m_IsOutputXml = isOutputXml;
	sm_Instance->m_ContextStack = new ArrayList();
	if(sm_Instance->m_IsOutputXml)
	{
		Console::WriteLine("<ObjectCompilerOutput>");
	}
	sm_Instance->m_AverageObjectSize = 0;
	sm_Instance->m_LargestObjectSize = 0;
	sm_Instance->m_AverageCount = 0;

	sm_Instance->m_LargestObjectName = NULL;
}

void CErrorManager::ShutdownClass()
{
	if(m_IsOutputXml)
	{
		Console::WriteLine("</ObjectCompilerOutput>");
	}
}

void CErrorManager::HandleException(Exception *ex)
{
	m_NumExceptions++;

	if(m_IsOutputXml)
	{
		Console::Write("<Exception context=\"");
		Console::Write(ComputeContextStackString());
		Console::Write("\" message=\"");
		Console::Write(ex->Message->Replace("\"","'"));
		Console::WriteLine("\"/>");
	}
	else
	{
		Console::Write("Exception: ");
		Console::WriteLine(ComputeContextStackString());
		Console::WriteLine(ex->ToString());
	}

	if(OnExceptionMessage)
	{
		OnExceptionMessage(ex->Message, ComputeContextStackString());
	}
}

void CErrorManager::HandleWarning(String *warning)
{
	m_NumWarnings++;

	if(m_IsOutputXml)
	{
		Console::Write("<Warning context=\"");
		Console::Write(ComputeContextStackString());
		Console::Write("\" message=\"");
		Console::Write(warning);
		Console::WriteLine("\"/>");
	}
	else
	{
		Console::Write("Warning: ");
		Console::Write(ComputeContextStackString());
		Console::Write(": ");
		Console::WriteLine(warning);
	}

	if(OnWarningMessage)
	{
		OnWarningMessage(warning, ComputeContextStackString());
	}
}

void CErrorManager::HandleError(String *error)
{
	m_NumErrors++;

	if(m_IsOutputXml)
	{
		Console::Write("<Error context=\"");
		Console::Write(ComputeContextStackString());
		Console::Write("\" message=\"");
		Console::Write(error);
		Console::WriteLine("\"/>");
	}
	else
	{
		Console::Write("Error: ");
		Console::Write(ComputeContextStackString());
		Console::Write(": ");
		Console::WriteLine(error);
	}

	if(OnErrorMessage)
	{
		OnErrorMessage(error, ComputeContextStackString());
	}
}

void CErrorManager::HandleInfo(String *info)
{
	if(m_IsOutputXml)
	{
		Console::Write(String::Concat("<Information message=\"", info, "\"/>\n"));
	}
	else
	{
		Console::WriteLine(info);
	}

	if(OnInformationMessage)
	{
		OnInformationMessage(info, ComputeContextStackString());
	}
	
}

void CErrorManager::RecordObjectSize(String *objectName, unsigned int size)
{
	m_AverageObjectSize = ((m_AverageObjectSize*m_AverageCount) + size) / (++m_AverageCount);
	if (size > m_LargestObjectSize)
	{
		m_LargestObjectSize = size;
		m_LargestObjectName = objectName;
	}
}

void CErrorManager::PrintSummary()
{
	if(m_IsOutputXml)
	{
		Int32 *numObjects = new Int32(m_NumObjects);
		HandleInfo(String::Concat(S"Succesfully compiled ", numObjects->ToString(), " objects"));
	}
	else
	{
		Console::Write(S"all done:  ");
		Console::Write(m_NumObjects);
		Console::Write(" objects compiled, ");
		Console::Write(m_NumErrors);
		Console::Write(" errors, ");
		Console::Write(m_NumWarnings);
		Console::Write(" warnings, ");
		Console::Write(m_NumExceptions);
		Console::Write(" exceptions.");
		Console::WriteLine();
		Console::Write(S"Average object size: ");
		Console::WriteLine(m_AverageObjectSize);
		Console::Write(S"Largest object: ");
		Console::Write(m_LargestObjectName);
		Console::Write(S", size: ");
		Console::WriteLine(m_LargestObjectSize);
	}
}

void CErrorManager::PushContext(ContextType type, String *name)
{
	ContextDescriptor *ctxt = new ContextDescriptor(type, name);
	m_ContextStack->Add(ctxt);
}

void CErrorManager::PopContext()
{
	m_ContextStack->RemoveAt(m_ContextStack->Count-1);
}

String *CErrorManager::ComputeContextStackString()
{
	String *context = String::Empty;

	for(int i = 0; i < m_ContextStack->Count; i++)
	{
		ContextDescriptor *d = static_cast<ContextDescriptor*>(m_ContextStack->get_Item(i));
		context = String::Concat(context, "\\[", d->ToString(), "]");
	}

	return context;
}
}
