//
// SoundDef.cpp
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#include "ErrorManager.h"
#include "SoundCompiler.h"
#include "SoundDef.h"
#include "FieldDef.h"
#include "CompositeFieldDef.h"

using namespace System::IO;

namespace rage
{
// macro for dealing with packed tristate values
#define AUD_SET_TRISTATE_VALUE(flagvar, flagid, trival) ((flagvar |= ((trival&0x03) << (flagid<<1))))

CObjectDef::CObjectDef(void)
{
	m_IsPacked = true;
}

CObjectDef::~CObjectDef(void)
{
}

CObjectDef *CObjectDef::ParseObjectDef(XmlNode *node, CObjectDef *baseClass, bool bDefaultPacked)
{	
	CObjectDef *def = new CObjectDef();

	def->m_fields = new ArrayList();
	def->m_attributes = new Hashtable();
	def->m_Validators = new ArrayList();

	def->m_BaseClass = baseClass;

	if(baseClass!=NULL)
	{
		def->m_Validators->AddRange(baseClass->GetValidators());
	}
	for(int i = 0; i < node->Attributes->Count; i++)
	{
		def->m_attributes->Add(node->Attributes->Item(i)->Name, node->Attributes->Item(i)->Value);
	}

	def->m_IsAbstract = node->Attributes->get_ItemOf("isAbstract")!=NULL&&node->Attributes->get_ItemOf("isAbstract")->Value->Equals("yes");
	def->m_IsCompressed = node->Attributes->get_ItemOf("isCompressed")!=NULL&&node->Attributes->get_ItemOf("isCompressed")->Value->Equals("yes");
	def->m_Alignment = node->Attributes->get_ItemOf("align")!=NULL?UInt32::Parse(node->Attributes->get_ItemOf("align")->Value):1;

	def->m_IsPacked = bDefaultPacked;
	if (node->Attributes->get_ItemOf("packed"))
	{
		def->m_IsPacked = node->Attributes->get_ItemOf("packed")->Value->Equals("yes");
	}

 
	def->m_name = dynamic_cast<String*>(def->m_attributes->get_Item(S"name"));
	ERRORMGR->PushContext(CONTEXT_TYPE_OBJECT_DEF, def->m_name);

	if(node->Attributes->get_ItemOf("validator"))
	{
		XslCompiledTransform *validator;
		validator = new XslCompiledTransform();
		String *documentURI = node->get_OwnerDocument()->get_BaseURI();
		//strip off "file:///";
		System::IO::FileInfo *fi = new FileInfo(documentURI->Substring(8));
		String *validatorPath = String::Format("{0}\\{1}", fi->get_Directory(), node->Attributes->get_ItemOf(S"validator")->Value);
		ERRORMGR->HandleInfo(String::Format("Loading validator: {0}", validatorPath));
		validator->Load(validatorPath);
		def->m_Validators->Add(validator);
	}

	for(int i = 0; i < node->ChildNodes->Count; i++)
	{
		CFieldDef *field = CFieldDef::ParseFieldDef(node->ChildNodes->Item(i));
		bool isOverridingDefault = false;
		if(field)
		{
			// if this field has the same name as a previously defined field, remove the previous field
			// and store this one in its place.  This allows the inheriting class to override fields defined
			// in the base class.
			// Note:  care should be taken if overriding the base field type, since the compiled sound
			// metadata will not be binary compatible with the defined base sound type.
			// Also note that when overriding a composite field, all parts of the composite field must be
			// explicitly declared in the overridden version.
			for(int i = 0; i < def->m_fields->Count; i++)
			{
				if(dynamic_cast<CFieldDef *>(def->m_fields->get_Item(i))->getName()->Equals(field->getName()))
				{
					def->m_fields->RemoveAt(i);
					def->m_fields->Insert(i, field);
					isOverridingDefault = true;
					break;
				}
			}
			if(!isOverridingDefault)
			{
				// add this new field at the end of the list
				def->m_fields->Add(field); 
			}
		}
	}

	ERRORMGR->PopContext();
	return def;
}

bool CObjectDef::BeginCompilation(XmlNode *node, BinaryWriter *output, CObjectCompiler *cs)
{
	if(m_BaseClass)
	{
		if(!m_BaseClass->BeginCompilation(node, output, cs))
		{
			return false;
		}
	}
	// signal to all fields that a compilation is about to start
	for(int i = 0; i < m_fields->Count; i++)
	{
		if(!dynamic_cast<CFieldDef*>(m_fields->get_Item(i))->BeginCompilation(node, output, this, cs))
			return false;
	}
	return true;
}

bool CObjectDef::Compile(XmlNode *node, BinaryWriter *output, CObjectCompiler *cs)
{
	if(m_BaseClass)
	{
		if(!m_BaseClass->Compile(node,output,cs))
		{
			return false;
		}
	}
	
	UInt32 compression = 0;
	UInt32 fieldCount = 0;
	Int64 compressionPos = output->BaseStream->get_Position();
	if(getIsCompressed())
	{
		output->Write(compression);
	}
	///////////////
	// iterate through all fields in this sound type definition
	for(int i = 0; i < m_fields->Count; i++)
	{
		CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i));
		if(field->ShouldBeIgnored())
			continue;

		ERRORMGR->PushContext(CONTEXT_TYPE_FIELD, field->getName());

		if(field->getType() != FIELD_TYPE_TRISTATE) 
		{
			// iterate through all fields in the supplied node to find a match for this field
			XmlNode *matchedNode = NULL;
			for(int j = 0; j < node->ChildNodes->Count; j++)
			{
				if(field->getName()->Equals(node->ChildNodes->get_ItemOf(j)->Name))
				{
					// matched this field
					matchedNode = node->ChildNodes->get_ItemOf(j);
					break;
				}
			}
			
			if(!getIsCompressed() || cs->IsCompressionDisabled() || !field->IsDefaultValue(matchedNode))
			{
				if(!field->Serialise(matchedNode, output, this, cs))
				{
					ERRORMGR->PopContext();
					return false;
				}
				compression |= (1<<fieldCount);
			}
			fieldCount++;	
		}

		ERRORMGR->PopContext();
	} // foreach field

	// overwrite compression flags
	if(getIsCompressed())
	{
		Int64 endPos = output->BaseStream->Position;
		output->BaseStream->Seek(compressionPos, SeekOrigin::Begin);
		if(cs->IsOutputBigEndian())
		{
			compression = CUtility::SwapUInt32(compression);
		}
		output->Write(compression);
		output->BaseStream->Seek(endPos,SeekOrigin::Begin);
	}
	return true;
}

bool CObjectDef::FinishCompilation(System::Xml::XmlNode __gc *node, System::IO::BinaryWriter __gc *output, rage::CObjectDef* objectDef, rage::CObjectCompiler __gc *cs)
{
	if(m_BaseClass)
	{
		if(!m_BaseClass->FinishCompilation(node, output, objectDef, cs))
		{
			return false;
		}
	}
	////////
	// signal to all fields that a compilation is finished
	for(int i = 0; i < m_fields->Count; i++)
	{
		if(!dynamic_cast<CFieldDef*>(m_fields->get_Item(i))->FinishCompilation(node, output, objectDef, cs))
		{
			return false;
		}
	}
	return true;
}

void CObjectDef::GenerateOverrideFlags(XmlNode *node, ArrayList *overrideFlags, ArrayList *flags)
{
	if(m_BaseClass)
	{
		m_BaseClass->GenerateOverrideFlags(node, overrideFlags, flags);
	}
	/////////////
	// generated packed override data
	String *overrideParent;
	for(int i =0 ; i < m_fields->Count; i++)
	{
		CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i));

		if(field->getType() == FIELD_TYPE_TRISTATE)
		{
			flags->Add(__box(field->GetTristateValue(CUtility::FindChildNode(node, field->getName()))));
		}
		else
		{
			if(field->ShouldBeIgnored() || !field->ShouldAllowOverrideControl())
			{
				continue;
			}

			XmlNode *fieldNode = CUtility::FindChildNode(node, field->getName());

		
			if(fieldNode && fieldNode->Attributes->get_ItemOf(S"overrideParent") != NULL)
			{
				overrideParent = fieldNode->Attributes->get_ItemOf(S"overrideParent")->Value;
			}		
			else
			{
				overrideParent = S"no";
			}

			overrideFlags->Add(overrideParent);
		}
	}
}

template <typename U> inline U  unbox(System::Object *o) { return *(dynamic_cast<__box U*>(o)); }

// node is the xml instance, writes binary to output
bool CObjectDef::SerialiseInstance(XmlNode *node, BinaryWriter *output, CObjectCompiler *cs)
{
	UInt32 flags = 0;
	UInt32 curFlagID = 0;


	ArrayList *overrideFlags = new ArrayList();
	ArrayList *flagValues = new ArrayList();
	GenerateOverrideFlags(node, overrideFlags, flagValues);

	
	// flag values
	// tristates are handled as a special case
	for(int i = 0; i < flagValues->get_Count(); i++)
	{
		AUD_SET_TRISTATE_VALUE(flags, curFlagID++, unbox<TristateValue>(flagValues->get_Item(i)));

		// check we're not going to overflow - 2 bits per fag
		if(curFlagID > 15)
		{
			ERRORMGR->HandleError("Object has too many flags - increase size of flag field");
			ERRORMGR->PopContext();
			return false;
		}
	}
	if(cs->IsOutputBigEndian())
	{
		flags = CUtility::SwapUInt32(flags);
	}

	// first byte of sound meta data is class ID
	output->Write(getClassID());

	if(cs->Use3ByteNameTableOffset())
	{
		// followed by 3 byte name table offset
		UInt32 nameTableOffset = cs->GetNextNameTableOffset();
		if(cs->IsOutputBigEndian())
		{
			nameTableOffset = CUtility::SwapUInt32(nameTableOffset);
			// we're only writing the lowest 3 bytes from this dword; throw away the MSB
			nameTableOffset >>= 8;
		}	

		Byte b = (Byte)(nameTableOffset&0xff);
		output->Write(b);
		b = (Byte)(nameTableOffset>>8&0xff);
		output->Write(b);
		b = (Byte)(nameTableOffset>>16&0xff);
		output->Write(b);
	}
	else
	{
		UInt32 nameTableOffset = (cs->IsOutputBigEndian() ? CUtility::SwapUInt32(cs->GetNextNameTableOffset()) : cs->GetNextNameTableOffset());
		// 4 byte name table offset
		output->Write(nameTableOffset);
	}

	// followed by 4 bytes of flags
	output->Write(flags);

	/////////////
	// generated packed override data
	if (overrideFlags->Count > 0)
	{
		SByte curByte __gc[] = new SByte __gc [1];
		int bitPos = 0, k = 0;
		for(int i =0 ; i < overrideFlags->Count; i++)
		{
			const bool overrideParent = dynamic_cast<String*>(overrideFlags->get_Item(i))->Equals("yes");

			bitPos = k++ % 8;

			// bit packing order seems reversed on 360 (BE)
			if(cs->IsOutputBigEndian())
			{
				curByte[0] |= ((int)overrideParent << (7-bitPos));
			}
			else
			{
				curByte[0] |= ((int)overrideParent << (bitPos));
			}

			// finished this byte
			if(bitPos == 7)
			{
				output->Write(curByte, 0, 1);
				curByte[0] = 0;
			}
		}

		if(bitPos != 7)
		{
			// still more to write
			output->Write(curByte, 0, 1);
		}
	}

	if(!BeginCompilation(node, output, cs))
	{
		return false;
	}

	if(!Compile(node, output, cs))
	{
		return false;
	}
	
	if(!FinishCompilation(node, output, this, cs))
	{
		return false;
	}

	//	For unpacked data, we need to make sure we end on our largest alignment boundary as well as start
	if (!GetIsPacked())
	{
		CFieldDef::AlignOutputToField(output, GetLargestAlignment());
	}

	ERRORMGR->IncrementNumberOfObjectsCompiled();
	return true;
}

String *CObjectDef::getInstanceName(XmlNode *node)
{
	return node->Attributes->get_ItemOf(S"name")->Value;
}

String *CObjectDef::getInstanceDescription(XmlNode *node)
{
	for(int i = 0; i < node->ChildNodes->Count; i++)
	{
		if(node->ChildNodes->get_ItemOf(i)->Name->CompareTo(S"Description") == 0)
		{
			return node->ChildNodes->get_ItemOf(i)->InnerText;
		}
	}

	return NULL;
}

void CObjectDef::OutputFlagEnum(TextWriter *output, String *className)
{
	if(getBaseClass())
	{
		getBaseClass()->OutputFlagEnum(output, className);
	}
	for(int i = 0; i < m_fields->Count; i++)
	{
		CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i));
		if(field->getType() == FIELD_TYPE_TRISTATE)
		{
			output->Write("\t\tFLAG_ID_");
			output->Write(className);
			output->Write("_");
			output->Write(field->getName()->ToUpper());
			output->Write(", // ");
			output->WriteLine(field->getName());
		}
	}
}

bool CObjectDef::OutputHelperFunctions(TextWriter *output)
{
	// allow fields to generate any supporting/external functions
	for(int i = 0; i < m_fields->Count; i++)
	{
		CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i)); 
		field->OutputHelperFunctions(output);		
	}

	output->Write("void *");
	output->Write(getName());
	output->WriteLine(m_fields->Count == 0 ? "::GetFieldPtr(const u32)" : "::GetFieldPtr(const u32 fieldNameHash)");
	output->WriteLine("{");
	if(m_fields->Count == 0)
	{
		output->WriteLine("\treturn NULL;");
	}
	else
	{
		output->WriteLine("\tswitch(fieldNameHash)");
		output->WriteLine("\t{");
		for(int i = 0; i < m_fields->Count; i++)
		{
			CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i)); 
			if(field->getType() != FIELD_TYPE_TRISTATE && field->getType() != FIELD_TYPE_BIT && !field->ShouldBeIgnored())
			{
				output->Write("\t\tcase ");
				output->Write(UInt32(CObjectCompiler::ComputeHash(field->getName())));
				output->Write("U: return &");
				output->Write(field->getName());
				output->WriteLine(";");
			}
		}

		output->WriteLine("\t\tdefault: return NULL;");
		output->WriteLine("\t}");
	}

	output->WriteLine("}");
	
	
	return true;
}

bool CObjectDef::OutputStructure(TextWriter *output)
{
	// firstly we need an enum of flag IDs
	output->Write("enum ");
	output->Write(getName());
	output->WriteLine("FlagIds");
	output->WriteLine("{");

	OutputFlagEnum(output, getName()->ToUpper());

	output->WriteLine("};");
	output->WriteLine();

	// allow fields to generate any supporting/external structure
	for(int i = 0; i < m_fields->Count; i++)
	{
		CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i)); 
		field->OutputExternalStructure(output);		
	}

	// actual data structure
	output->Write("struct ");
	output->Write(getName());
	if(m_BaseClass!=NULL && !m_BaseClass->getIsCompressed())
	{
		output->Write(" : ");
		output->Write(m_BaseClass->getName());
	}
	output->WriteLine();
	output->WriteLine("{");

	output->Write("\tstatic const rage::u32 TYPE_ID = ");
	output->Write(m_classID);
	output->WriteLine(";");
	output->Write("\tstatic const rage::u32 BASE_TYPE_ID = ");
	if(m_BaseClass)
	{
		output->Write(m_BaseClass->getName());
		output->Write("::TYPE_ID");
	}
	else
	{
		output->Write("~0U");
	}
	
	output->WriteLine(";");
	output->WriteLine();

	// default constructor
	output->Write("\t");
	output->Write(getName());
	output->WriteLine("()");
	bool needComma = false;
	if(!m_BaseClass)
	{
		output->WriteLine("\t: ClassID(0xff)");
		output->WriteLine("\t,NameTableOffset(~0U)");
		output->WriteLine("\t,Flags(0xAAAAAAAA)");
		if(getIsCompressed())
		{
			output->WriteLine("\t,Compression(~0U)");
		}
		needComma = true;
	}
	for(int i = 0; i < m_fields->Count; i++)
	{
		CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i));
		if(field->CanGenerateDefaultConstructor() && field->getType() != FIELD_TYPE_TRISTATE && field->getType() != FIELD_TYPE_STRING && field->getDefaultValue() != NULL && (field->getDefaultValue()->Length > 0||field->getType() == FIELD_TYPE_HASH))
		{
			output->Write("\t");
			if(needComma)
			{
				output->Write(",");
			}
			else
			{
				output->Write(":");
			}
			output->Write(field->getName());
			output->Write("(");
			
			if(field->getType() == FIELD_TYPE_HASH)
			{
				output->Write(CObjectCompiler::ComputeHash(field->getDefaultValue()));
			}
			else
			{
				output->Write(field->getDefaultValue());
			}

			// float and unsigned int needs suffix to stop compile warning on ps3
			if(field->getType() == FIELD_TYPE_UINT32 || field->getType() == FIELD_TYPE_HASH)
			{
				output->Write("U");
			}
			else if(field->getType() == FIELD_TYPE_FLOAT)
			{
				String* szValue = field->getDefaultValue();
				
				//	Make sure there's a decimal point in our float
				output->Write(szValue->Contains(".") ? "f" : ".0f");
			}
			output->Write(")");
			if(field->getType() == FIELD_TYPE_HASH)
			{
				output->Write(" //");
				output->Write(field->getDefaultValue());
			}
			output->WriteLine();
			needComma = true;
		}
	}
	output->WriteLine("\t{");
	output->WriteLine("\t}");
	output->WriteLine();

	output->WriteLine("\t// PURPOSE");
	output->WriteLine("\t//  Returns a pointer the field requested by name hash");
	output->WriteLine("\tvoid *GetFieldPtr(const rage::u32 fieldNameHash);");
	output->WriteLine();
	

	if(!m_BaseClass)
	{
		// first byte is always class id
		output->WriteLine("\trage::u32 ClassID : 8;");

		// followed by 4 byte sound name table offset
		output->WriteLine("\trage::u32 NameTableOffset : 24;");

		// followed by 4 bytes of flags
		output->WriteLine("\trage::u32 Flags;");

		//	Add all the override flags to a list
		ArrayList *overrideFlags = new ArrayList();
		for(int i = 0; i < m_fields->Count; i++)
		{
			CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i));
			// tristate's don't need parent override
			if(!field->ShouldBeIgnored() && field->getType() != FIELD_TYPE_TRISTATE && field->ShouldAllowOverrideControl())
			{
				overrideFlags->Add(field);
			}
		}

		//	Only if our list is populated should we even bother to make this struct
		if (overrideFlags->Count > 1)
		{
			// output overrideParent bools before the actual fields so that variable
			// sized fields are still handled ok at the end of a struct
			output->WriteLine("\tstruct tParentOverrides");
			output->WriteLine("\t{");

			int i = 0;
			for(; i < overrideFlags->Count; i++)
			{
				CFieldDef *field = dynamic_cast<CFieldDef*>(overrideFlags->get_Item(i));
				output->Write("\t\t bool ");
				output->Write(field->getName());
				output->WriteLine("OverridesParent:1;");
			}
			// just to keep things explicit pad to next full byte
			while(i % 8)
			{
				output->Write("\t\t bool p");
				output->Write(i % 8);
				output->WriteLine(":1; // padding");
				i++;
			}
			output->WriteLine("\t}ParentOverrides;");
			output->WriteLine();
		}
	}

	if(getIsCompressed())
	{
		output->WriteLine("\trage::u32 Compression;");
	}

	for(int i = 0; i < m_fields->Count; i++)
	{
		CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i)); 
		// tristates are grouped into a single u32 so shouldnt exist
		// on their own
		// composites need to go at the end of the structure
		if(field->getType() != FIELD_TYPE_TRISTATE && !field->ShouldBeAtEndOfStruct())
		{
			field->OutputStructure(output, 1);	
		}
	}

	for(int i = 0; i < m_fields->Count; i++)
	{
		CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i)); 
		// composites need to go at the end of the structure
		if(field->ShouldBeAtEndOfStruct())
		{
			field->OutputStructure(output, 1);	
		}
	}

	if (GetIsPacked())
	{
		output->WriteLine("}SPU_ONLY(__attribute__((packed)));");
	}
	else
	{
		output->WriteLine("};");
	}

	output->WriteLine();
	return true;
}

unsigned int CObjectDef::GetLargestAlignment()
{
	if (GetIsPacked())
		return 1;
	
	//	Base class always has an alignment of 4 because of its flags
	unsigned int largestAligment = m_BaseClass ? m_BaseClass->GetLargestAlignment() : 4;

	for(int i = 0; i < m_fields->Count; i++)
	{
		CFieldDef *field = dynamic_cast<CFieldDef*>(m_fields->get_Item(i));
		if (field->ShouldBeIgnored())
			continue;

		const unsigned int alignment = field->GetAlignment();
		if (alignment > largestAligment)
			largestAligment = alignment;
		if (field->getType() == FIELD_TYPE_COMPOSITE)
		{
			CCompositeFieldDef* compField = dynamic_cast<CCompositeFieldDef*>(field);
			if (compField->GetMaxOccurs() > 1)
			{
				const unsigned int numTypeSize = CFieldDef::GetFieldSize(compField->GetNumType());
				if (numTypeSize > largestAligment)
					largestAligment = numTypeSize;
			}
		}
	}
	return largestAligment;
}

}
