#pragma once

#using <mscorlib.dll>
#using <system.xml.dll>

using namespace System;
using namespace System::Xml;
using namespace System::Collections;
using namespace System::IO;

#include "SchemaManager.h"
#include "TemplateManager.h"

namespace rage
{

typedef __gc struct CompiledObject
{
	Int32 offset;
	Int32 bytes;
	String *name;
}CompiledObject;

public __gc class CObjectCompiler
{

public:
	CObjectCompiler(bool bBigEndian,String *classFactoryName,String *classFactoryReturnType, bool use3ByteNameTableOffset);
	CObjectCompiler(bool bBigEndian,String *classFactoryName,String *classFactoryReturnType, bool usesBucketAllocator, bool use3ByteNameTableOffset);

	// PURPOSE
	//	Parses the supplied type definitions, sets up the compiler
	bool LoadTypeDefinitions(String *typeDefinitions, String *classPrefix, String *classIncludePath);
	bool ReLoadTypeDefinitions();

	// PURPOSE
	//	Parses any template definitions in the supplied Xml document
	bool LoadTemplates(XmlDocument *xmlDoc);

	 // PURPOSE
	//	Resets compiler state between compilations
	void Init();

	bool CompileObjects(String *strPath, BinaryWriter *output);
	bool CompileObjects(XmlDocument *doc, BinaryWriter *output);
	bool CompileObject(XmlNode *node, BinaryWriter *output);

	bool SerialiseObjectLookup(BinaryWriter *output);	
	bool OutputObjectDefs(TextWriter *output, String *metadataType, String *fileName, String *namespaceName);
	bool OutputClassFactory(TextWriter *output, String *fileName, String *namespaceName);
	bool OutputHelperFunctions(TextWriter *output, String *metadataType, String *fileName, String *headerFile, String *namespaceName);

	Int32 FindObjectOffset(String *name);
	Int32 FindObjectSize(String *name);

	bool IsOutputBigEndian()
	{
		return m_bBigEndian;
	}

	void SetBigEndian(const bool isBigEndian)
	{
		m_bBigEndian = isBigEndian;
	}

	void RecordObjectReference(__int64 offset)
	{
		m_ObjectReferences->Add(__box(offset));	
	}

	void RecordStringTableReference(__int64 offset)
	{
		m_BankReferences->Add(__box(offset));
	}

	Int32 AddToStringTable(String *bankName)
	{
		if(bankName == NULL)
		{
			return -1;
		}
		Int32 index = m_StringTable->IndexOf(bankName);

		if(index == -1)
		{
			index = m_StringTable->get_Count();

			//Add the new bank name.
			m_StringTable->Add(bankName);
		}

		return index;
	}

	// PURPOSE
	//	Compute a hash from the string
	static UInt32 ComputeHash(String *value);

	void SerialiseStringTable(BinaryWriter *output);

	void RecordNewObject(String *objectName, Int32 offset, Int32 size);
	void UpdateObjectSizeAndOffset(String *objectName, Int32 offset, Int32 size);

	CEnum *FindEnum(String *enumName)
	{
		return m_SchemaManager->FindEnum(enumName);
	}

	/*CCompositeFieldDef *FindExtCompositeField(String *fieldName)
	{
		return m_SchemaManager->FindExtCompositeField(fieldName);
	}*/

	CSchemaManager *GetSchemaManager() { return m_SchemaManager; }

	UInt32 GetNextNameTableOffset()
	{
		UInt32 offset = 0;
		// compute lookup offset
		for(Int32 i = 0; i < m_ObjectLookup->Count; i++)
		{
			offset += dynamic_cast<CompiledObject*>(m_ObjectLookup->get_Item(i))->name->Length + 1;
		}
		return offset;
	}

	String* GetObjectNameFromTableOffset(UInt32 requiredOffset)
	{
		UInt32 offset = 0;
		// compute lookup offset
		for(Int32 i = 0; i < m_ObjectLookup->Count; i++)
		{			
			if(offset==requiredOffset)
			{
				return dynamic_cast<CompiledObject*>(m_ObjectLookup->get_Item(i))->name;
			}

			offset += dynamic_cast<CompiledObject*>(m_ObjectLookup->get_Item(i))->name->Length + 1;
		}
		return "Unable to find object";
	}


	Int32 GetSchemaVersion()
	{
		return m_SchemaManager->GetSchemaVersion();
	}

	bool IsCompressionDisabled()
	{
		return m_IsCompressionDisabled;
	}

	bool IsOutputAligned()
	{
		return m_IsOutputAligned;
	}

	bool Use3ByteNameTableOffset()
	{
		return m_Use3ByteNameTableOffset;
	}

	String* GetTypeDefinitionsPath()
	{
		return m_TypeDefinitions;
	}

	void DisableCompression()
	{
		m_IsCompressionDisabled = true;
	}

	CTemplateManager *GetTemplateManager()
	{
		return m_TemplateManager;
	}

	void SetObjectNameFilter(String *objectName) { m_ObjectNameToCompile = objectName; }

private:
	void CheckForWarningsAndErrors(XmlNode *node);
	void RecordCompiledObject(String *name, Int32 offset, Int32 numBytes);
	static void OutputGeneratedString(TextWriter *output);

	CSchemaManager *m_SchemaManager;
	CTemplateManager *m_TemplateManager;
	
	ArrayList *m_ObjectLookup;
	Hashtable *m_ObjectHashLookup;
	ArrayList *m_StringTable;
	ArrayList *m_ObjectReferences;
	ArrayList *m_BankReferences;

	String *m_TypeDefinitions;
	String *m_ClassPrefix;
	String *m_ClassIncludePath;

	String *m_ObjectNameToCompile;

	bool m_bBigEndian;
	bool m_IsCompressionDisabled;
	bool m_IsOutputAligned;
	bool m_Use3ByteNameTableOffset;
};

}
