//
// TemplateManager.h
// 
// Copyright (C) 2001-2008 Rockstar Games.  All Rights Reserved.
//

#pragma once

#using <mscorlib.dll>
#using <system.xml.dll>

using namespace System;
using namespace System::Xml;
using namespace System::Collections;

namespace rage
{

	public __gc class CObjectCompiler;
	public __gc class CObjectDef;
	public __gc class CCompositeFieldDef;

	public __gc class CTemplate
	{
	public:
		CTemplate(XmlNode *node, CObjectCompiler *compiler);

		XmlNode *GetObjectXml(const int index)
		{
			return dynamic_cast<XmlNode*>(m_ObjectsXml->get_Item(index));
		}
		
		String *GetName() { return m_Name; }


		bool ContainsObject(String *objectName);

		CObjectDef *GetObjectType(const int index);

		int GetNumObjects() { return m_ObjectsXml->Count; }

	private:
		String *m_Name;
		ArrayList *m_ObjectsXml;
		ArrayList *m_ObjectsType;
	};

	public __gc class CTemplateManager
	{
	public:

		CTemplateManager();
	
		bool LoadTemplates(XmlDocument *doc, CObjectCompiler *compiler);
		void ClearTemplates();

		bool ProcessObject(XmlNode *node);
	
	private:

		void TransformNode(String *instanceName, XmlNode *newNode, XmlNode *paramNode, CObjectDef *type, CTemplate *templateDef);
		void TransformCompositeNode(String *instanceName, ArrayList *fieldList, XmlNode *paramNode, CCompositeFieldDef *fieldDef, CTemplate *templateDef);

		Hashtable *m_TemplateMap;

	};

} // namespace rage

