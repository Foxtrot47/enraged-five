//
// TemplateManager.cpp
// 
// Copyright (C) 2001-2008 Rockstar Games.  All Rights Reserved.
//

#include "TemplateManager.h"
#include "ErrorManager.h"
#include "FieldDef.h"
#include "CompositeFieldDef.h"
#include "SoundCompiler.h"
#include "SoundDef.h"
#include "Utility.h"

using namespace System::Xml;
using namespace System::Collections;

namespace rage
{

	CTemplate::CTemplate(XmlNode *node, CObjectCompiler *compiler)
	{
		m_ObjectsXml = new ArrayList();
		m_ObjectsType = new ArrayList();

		m_Name = node->Attributes->get_ItemOf("name")->Value;

		for(int i = 0; i < node->ChildNodes->Count; i++)
		{
			XmlNode *childNode = node->ChildNodes->get_ItemOf(i);
			m_ObjectsXml->Add(childNode);
			CObjectDef *objectDef = compiler->GetSchemaManager()->GetObjectDefinition(childNode->Name);
			m_ObjectsType->Add(objectDef);
		}
	}

	CObjectDef *CTemplate::GetObjectType(const int index)
	{
		return dynamic_cast<CObjectDef *>(m_ObjectsType->get_Item(index));		
	}

	bool CTemplate::ContainsObject(String *objectName)
	{
		for(int i = 0 ; i < m_ObjectsXml->Count; i++)
		{
			XmlNode *node = dynamic_cast<XmlNode*>(m_ObjectsXml->get_Item(i));
			if(node->Attributes->get_ItemOf("name") && node->Attributes->get_ItemOf("name")->Value->Equals(objectName))
			{
				return true;
			}
		}
		return false;
	}

	CTemplateManager::CTemplateManager()
	{
		m_TemplateMap = new Hashtable();
	}

	bool CTemplateManager::LoadTemplates(XmlDocument *doc, CObjectCompiler *compiler)
	{
		if(doc->get_DocumentElement()->Name->Equals(S"Templates"))
		{
			for(int i = 0; i < doc->get_DocumentElement()->get_ChildNodes()->Count; i++)
			{
				XmlNode *templateDefinitionNode = doc->get_DocumentElement()->get_ChildNodes()->get_ItemOf(i);
				if(templateDefinitionNode->Name->Equals(S"Template"))
				{
					CTemplate *templateDef = new CTemplate(templateDefinitionNode, compiler);
					ERRORMGR->HandleInfo(String::Concat("Added template ", templateDef->GetName()));
					m_TemplateMap->Add(templateDef->GetName(), templateDef);
				}
			}
		}

		return true;
	}

	void CTemplateManager::ClearTemplates()
	{
		// reset the map
		m_TemplateMap = new Hashtable();
	}

	bool CTemplateManager::ProcessObject(XmlNode *node)
	{
		if(node->Name->Equals(S"TemplateInstance"))
		{
			String *newInstanceName = node->Attributes->get_ItemOf("name")->Value;
			String *templateName = node->Attributes->get_ItemOf("template")->Value;
			CTemplate *templateDefinition = dynamic_cast<CTemplate*>(m_TemplateMap->get_Item(templateName));
			if(!templateDefinition)
			{
				ERRORMGR->HandleError(String::Format("Invalid template instance.  Instance name: {0}, requested template: {1}", newInstanceName, templateName));
				return false;
			}

			XmlDocument *ownerDocument = node->get_OwnerDocument();
			XmlNode *parentNode = node->get_ParentNode();
			parentNode->RemoveChild(node);
			
			for(int i = 0 ; i < templateDefinition->GetNumObjects(); i++)
			{
				XmlNode *newNode = ownerDocument->ImportNode(templateDefinition->GetObjectXml(i),true);
				String *originalNodeName = newNode->Attributes->get_ItemOf("name")->Value;
				if(i == 0)
				{
					// if this is the first object in the template then it gets the instance name
					newNode->Attributes->get_ItemOf("name")->Value = newInstanceName;
				}
				else
				{
					// otherwise we prepend the instance name to ensure we have a unique identifier for the object
					newNode->Attributes->get_ItemOf("name")->Value = String::Concat(newInstanceName,S"_",originalNodeName);
				}
				
				TransformNode(newInstanceName, newNode,node,templateDefinition->GetObjectType(i), templateDefinition);
				parentNode->AppendChild(newNode);
			}

			return true;
		}
		return false;
	}

	void CTemplateManager::TransformNode(String *instanceName, XmlNode *templatedNode, XmlNode *paramNode, CObjectDef *type, CTemplate *templateDef)
	{
		// look for any fields in the templated node marked as exposed, and replace with paramNode data
		CObjectDef *objectDef = type;
		while(objectDef)
		{
			for(int i = 0; i < objectDef->GetFields()->Count; i++)
			{
				CFieldDef *field = dynamic_cast<CFieldDef*>(objectDef->GetFields()->get_Item(i));
				XmlNode *fieldNode = CUtility::FindChildNode(templatedNode, field->getName());
				if(fieldNode)
				{
					if(fieldNode->Attributes->get_ItemOf(S"ExposeAs"))
					{
						XmlNode *nodeToCopy = CUtility::FindChildNode(paramNode,fieldNode->Attributes->get_ItemOf(S"ExposeAs")->Value);
						if(nodeToCopy)
						{
							if(nodeToCopy->ChildNodes->Count > 1)
							{
								fieldNode->set_InnerXml(nodeToCopy->InnerXml);
							}
							else
							{
								fieldNode->set_InnerText(nodeToCopy->InnerText);
							}
						}
						fieldNode->Attributes->RemoveNamedItem(S"ExposeAs");
					}
					else if(field->GetUnits() == FIELD_UNIT_OBJECT_REFERENCE)
					{
						// if this field is referencing another object in a composite template then
						// we need to update the reference
						if(templateDef->ContainsObject(fieldNode->InnerText))
						{
							fieldNode->InnerText = String::Concat(instanceName, "_", fieldNode->InnerText);
						}
					}
					else if(field->getType() == FIELD_TYPE_COMPOSITE)
					{
						CCompositeFieldDef *compositeFieldDef = dynamic_cast<CCompositeFieldDef*>(field);
						
						// gather all instance fields
						ArrayList *fieldList = new ArrayList();
						for(int j = 0; j < templatedNode->ChildNodes->Count; j++)
						{
							if(templatedNode->ChildNodes->get_ItemOf(j)->Name->Equals(compositeFieldDef->getName()))
							{
								fieldList->Add(templatedNode->ChildNodes->get_ItemOf(j));
							}
						}
						TransformCompositeNode(instanceName, fieldList, paramNode, compositeFieldDef, templateDef);
					}
				}
			}

			objectDef = objectDef->getBaseClass();
		}
	}


	void CTemplateManager::TransformCompositeNode(String *instanceName, ArrayList *fieldList, XmlNode *paramNode, CCompositeFieldDef *fieldDef, CTemplate *templateDef)
	{
		// NOTE: we're not dealing with the case where the entire composite field has been defined as part of the template; that would happen directly in TransformNode() if it has the
		// exposeAs attribute
		for(int i = 0; i < fieldDef->GetFields()->Count; i++)
		{
			CFieldDef *field = dynamic_cast<CFieldDef*>(fieldDef->GetFields()->get_Item(i));
			for(int j = 0 ; j < fieldList->Count; j++)
			{
				XmlNode *fieldNode = CUtility::FindChildNode(dynamic_cast<XmlNode*>(fieldList->get_Item(j)),field->getName());
				if(fieldNode)
				{
					if(fieldNode->Attributes->get_ItemOf(S"ExposeAs") && paramNode)
					{
						XmlNode *nodeToCopy = CUtility::FindChildNode(paramNode,fieldNode->Attributes->get_ItemOf(S"ExposeAs")->Value);
						if(nodeToCopy)
						{
							if(nodeToCopy->ChildNodes->Count > 1)
							{
								fieldNode->set_InnerXml(nodeToCopy->InnerXml);
							}
							else
							{
								fieldNode->set_InnerText(nodeToCopy->InnerXml);
							}
						}
					}
					else if(field->GetUnits() == FIELD_UNIT_OBJECT_REFERENCE)
					{		
						if(templateDef->ContainsObject(fieldNode->InnerText))
						{
							fieldNode->InnerText = String::Concat(instanceName, "_", fieldNode->InnerText);
						}
					}
					else if(field->getType() == FIELD_TYPE_COMPOSITE)
					{
						// gather composite field instances
						fieldNode = dynamic_cast<XmlNode*>(fieldList->get_Item(j));
						ArrayList *childFieldList = new ArrayList();
						for(int k = 0; k < fieldNode->ChildNodes->Count; k++)
						{
							if(fieldNode->ChildNodes->get_ItemOf(k)->Name->Equals(field->getName()))
							{
								childFieldList->Add(fieldNode->ChildNodes->get_ItemOf(k));
							}
						}
						TransformCompositeNode(instanceName, childFieldList, paramNode, dynamic_cast<CCompositeFieldDef*>(field), templateDef);
					}
				}
			}
		}
	}
} // namespace rage
