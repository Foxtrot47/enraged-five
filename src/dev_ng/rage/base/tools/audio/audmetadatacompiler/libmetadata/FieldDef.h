//
// FieldDef.h
// 
// Copyright (C) 2005 Rockstar Games.  All Rights Reserved.
//

#pragma once

#include "FieldTypes.h"
#include "SoundCompiler.h"
#include "Utility.h"

#using <mscorlib.dll>
#using <system.xml.dll>

using namespace System;
using namespace System::Xml;
using namespace System::Collections;
using namespace System::IO;

namespace rage
{

public __gc class CFieldDef abstract : public Object
{
public:
	CFieldDef(void);
	virtual ~CFieldDef(void);

	virtual bool OutputStructure(TextWriter *output, int tabLevel) = 0;
	virtual unsigned int GetAlignment() = 0;
	static unsigned int GetAlignment(FieldType type);
	virtual bool OutputExternalStructure(TextWriter * /*output*/){ return true; }
	virtual bool OutputHelperFunctions(TextWriter * /*output*/){ return true; }

	// PURPOSE
	//	Serialises a field instance from xml to binary metadata
	virtual bool Serialise(XmlNode *node, BinaryWriter *output, CObjectDef* objectDef, CObjectCompiler *compiler) = 0;
	
	virtual bool BeginCompilation(XmlNode * /*node*/, BinaryWriter * /*output*/, CObjectDef* /*objectDef*/, CObjectCompiler * /*compiler*/)
	{
		return true;
	}

	virtual bool FinishCompilation(XmlNode * /*node*/, BinaryWriter * /*output*/, CObjectDef* /*objectDef*/, CObjectCompiler * /*compiler*/)
	{
		return true;
	}

	static CFieldDef *ParseFieldDef(XmlNode *node);
	static FieldType GetFieldType(String *str);
	static FieldUnit GetFieldUnits(String *str);
	static unsigned int GetFieldSize(String *str);
	static unsigned int GetFieldSize(FieldType type);

	static bool WriteConvertedValue(String *str, FieldType type, BinaryWriter *output, int length, bool bPacked, bool bBigEndian);

	// PURPOSE
	//	Ensures that the file output writer aligns on the specified memory boundary
	//	Returns the updated stream position
	static unsigned int AlignOutputToField(BinaryWriter *output, unsigned int alignBytes);
	static unsigned int AlignOutputToField(BinaryWriter *output, FieldType type);

	String *getName()
	{
		return m_strName;
	}
	String *getTypeString(FieldType type);

	FieldType getType()
	{
		return m_type;
	}

	FieldUnit GetUnits()
	{
		return m_units;
	}

	virtual String *getDefaultValue()
	{
		return m_strDefault;
	}

	void setDefaultValue(String *defaultValue)
	{
		m_strDefault = defaultValue;
	}

	bool ShouldBeIgnored()
	{
		return m_Ignore;
	}

	bool ShouldOverrideParent()
	{
		return m_OverrideParent;
	}

	bool ShouldAllowOverrideControl()
	{
		return m_AllowOverrideControl;
	}

	bool ShouldResolveObjectReference()
	{
		// allow object references by hash
		return (m_units == FIELD_UNIT_OBJECT_REFERENCE) && (m_type != FIELD_TYPE_HASH);
	}

	TristateValue GetTristateValue(XmlNode *node);

	virtual bool ShouldBeAtEndOfStruct()
	{
		return false;
	}

	virtual bool IsDefaultValue(XmlNode *node)=0;

	virtual bool CanGenerateDefaultConstructor()
	{
		return (m_type != FIELD_TYPE_FIXED_STRING);
	}

protected:

	static void WriteTabs(TextWriter *output, int level);

	String *m_EnumName;
	String *m_CustomName;

	String *m_strName;
	String *m_strDefault;
	bool m_Ignore, m_OverrideParent, m_AllowOverrideControl;
	FieldType m_type;
	FieldUnit m_units;
	unsigned int m_Alignment; //Defaults to 1

};

}
