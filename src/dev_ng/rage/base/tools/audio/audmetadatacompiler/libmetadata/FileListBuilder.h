#pragma once

#using <mscorlib.dll>
#using <system.xml.dll>

using namespace System;
using namespace System::Xml;
using namespace System::Collections;
using namespace System::IO;
using namespace audAssetManagement2;

namespace rage
{

public __gc class CFileListBuilder
{
public:
	CFileListBuilder(void);
	~CFileListBuilder(void);

	static ArrayList *GetFileList(String *basePath, String *extension);
	static ArrayList *GetFileList(String *basePath, String *extension, assetManager *assetMgr);
};

}
