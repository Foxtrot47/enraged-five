#pragma once

using namespace System;
using namespace System::Xml;
using namespace System::Collections;
using namespace System::IO;

#include "Enum.h"

namespace rage
{
	public __gc class CObjectDef;

public __gc class CSchemaManager
{
public:
	CSchemaManager(String *classFactoryName,String *classFactoryReturnType, bool usesBucketAllocator);
	~CSchemaManager(void);

	bool LoadObjectDefinitions(String *strFile, String *classPrefix, String *classIncludePath);
	CObjectDef* GetObjectDefinition(String *name);
	bool OutputObjectDefs(TextWriter *output);
	bool OutputObjectDefIDs(TextWriter *output, String *fileName);
	bool OutputEnumDefs(TextWriter *output);
	bool OutputExtCompositeFields(TextWriter *output);
	bool OutputClassFactoryFunction(TextWriter *output, String *namespaceName);
	bool OutputClassMaxSizeFunction(TextWriter *output);
	bool OutputGetAddressOfMacro(TextWriter *output, String *fileName);
	bool OutputHelperFunctions(TextWriter *output);
	bool OutputBaseTypeLookups(TextWriter *output, String *metadataType, bool isDefinition);
	ArrayList* GetIncludePaths() { return m_IncludePaths; }

	CEnum *FindEnum(String *name)
	{
		return dynamic_cast<CEnum*>(m_Enums->get_Item(name));
	}

	/*CCompositeFieldDef *FindExtCompositeField(String *name)
	{
		return dynamic_cast<CCompositeFieldDef*>(m_ExtComposites->get_Item(name));
	}*/

	unsigned int GetSchemaVersion()
	{
		return m_SchemaVersion;
	}

	bool GetDefaultPacked()
	{
		return m_DefaultPacked;
	}

	bool GetPackingCurrentlyEnabled()
	{
		return m_PackingCurrentlyEnabled;
	}

private:
	String *m_ClassFactoryReturnType;
	String *m_ClassFactoryFunctionName;
	Hashtable *m_ObjectDefinitions, *m_Enums, *m_ExtComposites;
	ArrayList *m_ObjectDefLookup, *m_EnumLookup, *m_ExtCompositeLookup;
	ArrayList *m_IncludePaths;
	unsigned int m_SchemaVersion;
	Byte m_NextClassId;
	bool m_UsesBucketAllocator;
	bool m_GenerateFactoryCode;
	bool m_DefaultPacked;
	bool m_PackingCurrentlyEnabled;
};
}
